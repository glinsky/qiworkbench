




<?xml version="1.0" encoding="UTF-8"?>
<jnlp codebase="http://localhost:8080/qiWorkbench/jsp/"
          href="http://localhost:8080/qiWorkbench/jsp/qilauncher_invoker_jnlp.jsp?install=false">
    <information>
        <title>qiWorkbench v1.4</title>
        <vendor>BHP Billiton Petroleum</vendor>
        <description>Seismic toolkit for Quantitative Interpretation</description>
        <offline-allowed/>
        <icon href="images/QIW_icon.gif"/>
        <icon href="images/QIW_splash.gif" kind="splash"/>
    </information>
    <security>
        <all-permissions/>
    </security>
    <resources>
        <j2se version="1.5+" />
        <!--j2se version="1.5+" /-->
        <jar href="../lib/qiWorkbench.jar"/>
	<!-- Access property parameters with System.getProperty("param") -->
	<property name="install" value="false" />
	<property name=href value=http://localhost:8080/qiWorkbench/jsp/qilauncher_invoker_jnlp.jsp />
        <property name=codebaseBuffer value=http://localhost:8080/qiWorkbench/jsp/ />
    </resources>
    <application-desc main-class="com.bhpb.qiworkbench.util.ClientCapabilityDetector"/>
</jnlp>
