#!/bin/bash

[ -d /nfsdisk/data/project ] || {
  echo "can't access geophysics media. Try chmod a+rx /run/media/jfield";
  exit 1; 
}
[ `stat -c "%a" /run/media/jfield` == 755 ] || {
  echo "need to open up /run/media/jfield to others (tomcat.tomcat)" ;
  exit 1 ;
}
ps -A all | grep -q org.apache.catalina || {
  echo "start tomcat server ;  sudo systemctl start tomcat.service" ;
  exit 1 ;
}

#/usr/java/jdk-13.0.2/bin/java -Xbootclasspath/a:/opt/OpenWebStart/openwebstart.jar -Dicedtea-web.bin.location=/opt/OpenWebStart/javaws -Xmx1024m -Xincgc --add-exports=java.base/sun.net.www.protocol.jar=ALL-UNNAMED,java.desktop --add-exports=java.base/jdk.internal.util.jar=ALL-UNNAMED,java.desktop --add-exports=java.base/com.sun.net.ssl.internal.ssl=ALL-UNNAMED,java.desktop --add-reads=java.naming=ALL-UNNAMED,java.desktop --add-exports=java.desktop/sun.awt.X11=ALL-UNNAMED,java.desktop --add-exports=java.desktop/sun.applet=ALL-UNNAMED,java.desktop,jdk.jsobject --add-exports=java.base/sun.security.action=ALL-UNNAMED,java.desktop --add-reads=java.base=ALL-UNNAMED,java.desktop --add-exports=java.base/sun.net.www.protocol.http=ALL-UNNAMED,java.desktop --add-exports=java.naming/com.sun.jndi.toolkit.url=ALL-UNNAMED,java.desktop --add-exports=java.base/sun.security.util=ALL-UNNAMED,java.desktop --add-reads=java.desktop=ALL-UNNAMED,java.naming --add-exports=java.desktop/sun.awt=ALL-UNNAMED,java.desktop --add-exports=java.base/sun.security.x509=ALL-UNNAMED,java.desktop --add-exports=java.desktop/javax.jnlp=ALL-UNNAMED,java.desktop --add-exports=java.base/sun.security.provider=ALL-UNNAMED,java.desktop --add-exports=java.base/sun.security.validator=ALL-UNNAMED,java.desktop net.sourceforge.jnlp.runtime.Boot -Xnofork http://localhost:8080/qiWorkbench/jsp/qiw_httpinvoker_jnlp.jsp?install=false

# NOTE: -noverify option didn't stop signature checking
/usr/java/jdk-17.0.2/bin/java --illegal-access=permit -Xbootclasspath/a:/opt/OpenWebStart/openwebstart.jar -Dicedtea-web.bin.location=/opt/OpenWebStart/javaws -Xmx1024m --add-exports=java.base/sun.net.www.protocol.jar=ALL-UNNAMED,java.desktop --add-exports=java.base/jdk.internal.util.jar=ALL-UNNAMED,java.desktop --add-exports=java.base/com.sun.net.ssl.internal.ssl=ALL-UNNAMED,java.desktop --add-reads=java.naming=ALL-UNNAMED,java.desktop --add-exports=java.desktop/sun.awt.X11=ALL-UNNAMED,java.desktop --add-exports=java.desktop/sun.applet=ALL-UNNAMED,java.desktop,jdk.jsobject --add-exports=java.base/sun.security.action=ALL-UNNAMED,java.desktop --add-reads=java.base=ALL-UNNAMED,java.desktop --add-exports=java.base/sun.net.www.protocol.http=ALL-UNNAMED,java.desktop --add-exports=java.naming/com.sun.jndi.toolkit.url=ALL-UNNAMED,java.desktop --add-exports=java.base/sun.security.util=ALL-UNNAMED,java.desktop --add-reads=java.desktop=ALL-UNNAMED,java.naming --add-exports=java.desktop/sun.awt=ALL-UNNAMED,java.desktop --add-exports=java.base/sun.security.x509=ALL-UNNAMED,java.desktop --add-exports=java.desktop/javax.jnlp=ALL-UNNAMED,java.desktop --add-exports=java.base/sun.security.provider=ALL-UNNAMED,java.desktop --add-exports=java.base/sun.security.validator=ALL-UNNAMED,java.desktop \
  --add-opens=java.base/java.util=ALL-UNNAMED,java.desktop \
  --add-opens=java.base/java.text=ALL-UNNAMED,java.desktop \
  --add-opens=java.desktop/java.awt.font=ALL-UNNAMED,java.desktop \
  --add-opens=java.base/java.lang=ALL-UNNAMED,java.desktop \
  --add-opens=java.base/java.nio=ALL-UNNAMED,java.desktop \
net.sourceforge.jnlp.runtime.Boot -Xnofork http://localhost:8080/qiWorkbench/jsp/qiw_httpinvoker_jnlp.jsp?install=false

