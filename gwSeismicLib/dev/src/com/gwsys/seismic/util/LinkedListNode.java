//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.util;

/**
 * Implements a node used in a doubly linked list.<br>
 * Ref http://www.java2s.com/ExampleCode/Collections-Data-Structure/
 * Doublylinkedlistwithdatastructure.htm
 * 
 */
public class LinkedListNode {

	/**
	 * the previous node of this node in the linked list.
	 */
	private LinkedListNode _previous;

	/**
	 * the next node of this node in the linked list.
	 */
	private LinkedListNode _next;

	/**
	 * object associated with this node.
	 */
	private Object _object;

	/**
	 * Contructs an object node used in the linked list.
	 * 
	 * @param object
	 *            object associated with this node.
	 */
	public LinkedListNode(Object object) {
		_object = object;
		_previous = this;
		_next = this;
	}

	/**
	 * Sets the data object associated with this node.
	 * 
	 * @param object
	 *            object associated with this node.
	 */
	public void setObject(Object object) {
		_object = object;
	}

	/**
	 * Obtains the data object associated with this node.
	 * 
	 * @return object associated with this node.
	 */
	public Object getObject() {
		return _object;
	}

	/**
	 * Sets the previous node in list for this node.
	 * 
	 * @param previous
	 *            the previous node in list for this node.
	 */
	public void setPrevious(LinkedListNode previous) {

		if (previous == null) {
			_previous = this;
		} else {
			_previous = previous;
		}
	}

	/**
	 * Retrieves the previous node in list for this node.
	 * 
	 * @return the previous node in list for this node.
	 */
	public LinkedListNode getPrevious() {
		return _previous;
	}

	/**
	 * Sets the next node in list for this node.
	 * 
	 * @param next
	 *            the next node in list for this node.
	 */
	public void setNext(LinkedListNode next) {

		if (next == null) {
			_next = this;
		} else {
			_next = next;
		}
	}

	/**
	 * Gets the next node in list for this node.
	 * 
	 * @return the next node in list for this node.
	 */
	public LinkedListNode getNext() {
		return _next;
	}
}