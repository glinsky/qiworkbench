//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.util;

/**
 * Define the range of data values. May contains step value.
 * @author Hua
 *
 */
public interface DataRange {
	/**
	 * Returns the number of samples in this given range.
	 * @return
	 */
	public int getRange() ;

	/**
	 * Shift the range to new location.
	 * @param start The new min value.
	 */
	public void shift(int start);

	/**
	 * Retrieves the minimum number of this range.
	 *
	 * @return the minimum number.
	 */
	public Number getMin() ;

	/**
	 * Retrieves the maximum number of this range.
	 *
	 * @return the maximum number.
	 */
	public Number getMax();

	/**
	 * Sets the minimum number of this range.
	 *
	 * @param min the minimum number.
	 */
	public void setMin(double min);

	/**
	 * Sets the maximum number of this range.
	 *
	 * @param the maximum number.
	 */
	public void setMax(double max) ;

	public void setStep(double step);

	public Number getStep();

	

}
