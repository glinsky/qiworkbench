//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.util;

import java.text.DecimalFormat;
import java.util.Vector;

import com.gwsys.seismic.indexing.DataSliceType;
import com.gwsys.seismic.indexing.DataSourceKey;
import com.gwsys.seismic.indexing.DataSourceKeys;
import com.gwsys.seismic.indexing.DataSourcePath;
import com.gwsys.seismic.indexing.IndexedSegyTraceReader;
import com.gwsys.seismic.indexing.SegyBinaryEntry;
import com.gwsys.seismic.indexing.SegyDataset;
import com.gwsys.seismic.indexing.SegyHeaderFormat;
import com.gwsys.seismic.indexing.SeismicConstants;
import com.gwsys.seismic.io.SeismicMetaData;
import com.gwsys.seismic.reader.SeismicFormat;

/**
 * This original file belongs to bhpViewer.
 * This class provides easy-to-use methods for handling seismic data.
 */
public class SeismicDataUtil {

	// it shoudl be "time" or depths in the depth
	public static final String DefSampleName = "Time";

	/**
	 * Transpose keys
	 * @param segyFormat
	 * @param transposedKey
	 * @param transposedName
	 */
	public static void transposeKeys(SeismicFormat segyFormat,
			String transposedKey, String transposedName) {
		if (transposedKey != null && transposedKey.length() > 0
				&& transposedName != null && transposedName.length() > 0) {
			Vector formats = segyFormat.getTraceHeaderFormats();
			SegyHeaderFormat headerFormat = (SegyHeaderFormat) (formats
					.firstElement());
			Vector fields = headerFormat.getHeaderFields();

			SegyBinaryEntry headerField;

			for (int i = 0; i < fields.size(); i++) {
				headerField = (SegyBinaryEntry) (fields.elementAt(i));

				if (headerField.getName().equals(transposedKey)) {
					fields.remove(i);
					headerFormat.addFieldDescription(new SegyBinaryEntry(
							headerField.getByteOffset(),
							/*headerField.getType()*/
							SeismicConstants.DATA_FORMAT_FLOAT, headerField
									.getFieldId(), transposedName, headerField
									.getFieldType(), false));
					return;
				}

			}
		}
	}

	/**
	 * Gets the sample name
	 * @param dataset the dataset
	 * @return the sample name
	 */
	public static String getSampleKeyName(SegyDataset dataset) {
		if (dataset.getDataOrder() == DataSliceType.DATA_MAP
				&& dataset.getTransposedKey() != null
				&& dataset.getTransposedKey().length() > 0
				&& dataset.getTransposedName() != null
				&& dataset.getTransposedName().length() > 0) {
			return new String(dataset.getTransposedKey());
		}

		return DefSampleName;
	}

	public static String getSamplesParameters(SeismicMetaData metaData) {
		final double ROUND_VAL = 1000.0;
		double startTime = metaData.getStartValue();

		// it is necessary to find better way
		if (1.0 / ROUND_VAL > startTime) // hack
			startTime = Math.round(metaData.getStartValue() * ROUND_VAL)
					/ ROUND_VAL;

		double stepTime = metaData.getSampleRate();
		double endTime = startTime + (metaData.getSamplesPerTrace() - 1)
				* metaData.getSampleRate();

		DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat
				.getInstance();
		decimalFormat.setMaximumFractionDigits(5);

		String keyAttr = "";

		try {
			String strStart = ""
					+ decimalFormat.parse(decimalFormat.format(startTime))
							.doubleValue();
			String strEnd = ""
					+ decimalFormat.parse(decimalFormat.format(endTime))
							.doubleValue();
			String strStep = ""
					+ decimalFormat.parse(decimalFormat.format(stepTime))
							.doubleValue();

			keyAttr = strStart + "-" + strEnd;
			keyAttr = keyAttr + "[" + strStep + "]";
		} catch (java.text.ParseException e) {
			keyAttr = startTime + "-" + endTime;
			keyAttr = keyAttr + "[" + stepTime + "]";
		}

		return keyAttr;
	}

	public static SeismicMetaData createMetaData(SegyDataset dataset,
			DataSourceKeys keys) {
		SeismicMetaData metaData = new SeismicMetaData();
		metaData.setDataStatistics(Float.MIN_VALUE, Float.MAX_VALUE, 0.0, 0.2);

		try {
			int samplesPerTrace = dataset.getNumSamplesPerTrace();
			IndexedSegyTraceReader reader = dataset.getTraceReader(keys);
			int numberOfTraces = reader.getNumTraces();

			metaData.setNumberOfTraces(numberOfTraces);
			metaData.setSamplesPerTrace(samplesPerTrace);

			double sampleInterval = dataset.getSampleInterval();
			metaData.setStartValue(dataset.getStartValue());
			if (dataset.getDataOrder() == DataSliceType.DATA_MAP) {
				metaData.setSampleRate(sampleInterval);
			} else {

				if (sampleInterval <= 0) {
					metaData.setSampleRate((float) 0.004);
				} else {
					// Sample rate is given in terms of microseconds; convert to seconds.
					metaData
							.setSampleRate((float) (sampleInterval / 1000000.0));
				}
			}

			metaData.setSampleStart(0.0);
			metaData.setSampleUnits(dataset.getUnits());
			metaData.setDataStatistics(dataset.getSampleMin(), dataset
					.getSampleMax(), dataset.getSampleMean(), dataset
					.getSampleRms());
		} catch (java.lang.Exception ex) {
			ex.printStackTrace();
		}
		return metaData;
	}

	public static boolean isPathData(String parameters) {
		int keylistIndex = parameters.indexOf("keylist=");
		int keylistEndIndex = parameters.indexOf(' ', keylistIndex);

		String[] ranges = parameters.substring(
				keylistIndex + "keylist=".length(), keylistEndIndex).split(":");

		for (int i = 0; i < ranges.length; ++i) {
			if (ranges[i].indexOf("^") != -1)
				return true;
		}

		return false;
	}

	public static DataSourceKeys parseKeys(String parameters) {

		DataSourceKeys keys = new DataSourceKeys();
		if (isPathData(parameters))
			parameters = convertParameterFormat(parameters);

		//System.out.println("IntSeismicUtil.parseKeys: " + parameters);
		int keysIndex = parameters.indexOf("keys=");
		int keylistIndex = parameters.indexOf("keylist=");
		int keylistEndIndex = parameters.indexOf(' ', keylistIndex);

		String[] keynames = parameters.substring(keysIndex + "keys=".length(),
				keylistIndex - 1).trim().split(",");
		String[] ranges = parameters.substring(
				keylistIndex + "keylist=".length(), keylistEndIndex).split(":");

		for (int i = 0; i < keynames.length; ++i) {

			if (ranges[i].equals("*")) {
				continue;
			}

			DataSourceKey k = new DataSourceKey();

			k.setName(keynames[i].trim());

			// path data
			if (ranges[i].indexOf("^") == -1) {
				parseRange(ranges[i], k);
			} else {
				k.setMaxValue(ranges[i]);
			}
			/*
			 if( keynames[i].equals( _sampleName ) ) {

			 _timeStart = Double.valueOf( k.MinValue ).doubleValue();
			 _timeEnd   = Double.valueOf( k.MaxValue ).doubleValue();

			 continue;
			 }
			 */
			keys.addItem(k);
		}
		return keys;
	}

	public static DataSourcePath parsePath(DataSourceKeys keys) {

		DataSourceKey key1 = null;
		DataSourceKey key2 = null;

		if (keys == null && keys.getCount() < 2)
			return null;

		for (int i = 0; i < keys.getCount(); i++) {
			if (keys.getItemByIndex(i).getMaxValue() != null
					&& keys.getItemByIndex(i).getMaxValue().indexOf("^") != -1) {
				if (key1 == null)
					key1 = keys.getItemByIndex(i);
				else
					key2 = keys.getItemByIndex(i);
			}
		}
		if (key1 == null || key2 == null)
			return null;

		String[] key1_values = key1.getMaxValue().split("\\^");
		String[] key2_values = key2.getMaxValue().split("\\^");

		// check arrays
		if (key1_values == null || key2_values == null
				|| key1_values.length != key2_values.length)
			return null;

		DataSourcePath path = new DataSourcePath(key1, key2, key1_values.length);

		double max1 = 0, min1 = 0;
		double max2 = 0, min2 = 0;

		for (int i = 0; i < key1_values.length; i++) {

			Double val1 = Double.valueOf(key1_values[i]);
			Double val2 = Double.valueOf(key2_values[i]);

			if (i == 0) {
				max1 = min1 = val1.doubleValue();
				max2 = min2 = val2.doubleValue();
			} else {

				if (max1 < val1.doubleValue())
					max1 = val1.doubleValue();
				if (max2 < val2.doubleValue())
					max2 = val2.doubleValue();

				if (min1 > val1.doubleValue())
					min1 = val1.doubleValue();
				if (min2 > val2.doubleValue())
					min2 = val2.doubleValue();

			}
			path.setPoint(i, val1, val2);
		}

		path.getFirstKey().setMinValue(Double.toString(min1));
		path.getFirstKey().setMaxValue(Double.toString(max1));

		path.getSecondKey().setMinValue(Double.toString(min2));
		path.getSecondKey().setMaxValue(Double.toString(max2));

		return path;
	}

	public static void parseRange(String r, DataSourceKey k) {
		int i = r.indexOf('[');
		String[] minmax = ((i == -1) ? (r) : (r.substring(0, i))).split("-");
		k.setMinValue(minmax[0].trim());
		if (minmax.length > 1) {
			k.setMaxValue(minmax[1].trim());
		} else {
			k.setMaxValue(k.getMinValue());
		}
		if (i != -1) {
			k.setIncrement(r.substring(i + 1, r.indexOf(']')).trim());
		}
	}

	public static void checkFormatKeys(DataSourceKeys keys,
			SeismicFormat segyFormat) {
		Vector headers = segyFormat.getTraceHeaderFormats();

		for (int i = 0; i < keys.getCount(); i++) {
			DataSourceKey key = keys.getItemByIndex(i);
			if (key != null && key.getName() != null) {
				for (int k = 0; k < headers.size(); k++) {
					SegyHeaderFormat format = (SegyHeaderFormat) headers.get(k);
					Vector fields = format.getHeaderFields();

					for (int j = 0; j < fields.size(); j++) {
						SegyBinaryEntry field = (SegyBinaryEntry) fields.get(j);
						// H A C K
						if (key.getName().equalsIgnoreCase(field.getName())) {
							switch (field.getValueType()) {
							case SeismicConstants.DATA_FORMAT_INT:
							case SeismicConstants.DATA_FORMAT_SHORT:
							case SeismicConstants.DATA_FORMAT_UNSIGNEDINT:
							case SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT: {
								// remove after point
								if (key.getIncrement() != null)
									key.setIncrement(Integer.toString(Double
											.valueOf(key.getIncrement())
											.intValue()));
								else
									key.setMinValue(Integer.toString(Double
											.valueOf(key.getMinValue())
											.intValue()));
								key
										.setMaxValue(Integer.toString(Double
												.valueOf(key.getMaxValue())
												.intValue()));
								break;
							}
							}
						}
					}
				}
			}
		}
	}

	private static String convertParameterFormat(String parameter) {
		int keylistIndex = parameter.indexOf("keylist=");
		int tailIndex = parameter.indexOf(" ", keylistIndex);
		String header = parameter.substring(0, keylistIndex);
		String tail = parameter.substring(tailIndex);
		String keylist = parameter.substring(
				keylistIndex + "keylist=".length(), tailIndex);

		String[] ranges = keylist.split(":");
		StringBuffer newlist = new StringBuffer();
		StringBuffer v1 = new StringBuffer();
		StringBuffer v2 = new StringBuffer();
		for (int i = 0; i < ranges.length; i++) {
			if (i != 0)
				newlist.append(":");
			if (ranges[i].indexOf("^") > 0) {
				String[] vpairs = ranges[i].split("\\^");
				for (int j = 0; j < vpairs.length; j++) {
					if (vpairs[j].endsWith("\\"))
						vpairs[j] = vpairs[j].substring(0,
								vpairs[j].length() - 1);
					if (j != 0) {
						v1.append("^");
						v2.append("^");
					}
					String[] values = vpairs[j].split(",");
					v1.append(values[0]);
					v2.append(values[1]);
				}
				newlist.append(v1 + ":" + v2);
			} else
				newlist.append(ranges[i]);
		}

		StringBuffer result = new StringBuffer();
		result.append(header);
		result.append(" keylist=" + newlist.toString());
		result.append(tail);
		return result.toString();
	}
}