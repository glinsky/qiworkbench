//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.util;

import java.util.Hashtable;
import java.util.LinkedList;

/**
 * Make an object pool to store objects with key. If the used size is larger
 * than the initial size, the unused object is removed from pool.
 * 
 * @author Hua
 * 
 */
public class ObjectPool {

	private Hashtable<Object, KeyedObject> pool;

	private int totalSize, usedSize;

	private LinkedList<KeyedObject> list;

	/**
	 * Constructor.
	 * @param memorySize
	 *            Initialized size of bytes.
	 */
	public ObjectPool(int memorySize) {
		if (memorySize < 0)
			throw new IllegalArgumentException(
					"The size of memory cannot be a negative value");
		totalSize = memorySize;
		pool = new Hashtable<Object, KeyedObject>();
		list = new LinkedList<KeyedObject>();
	}

	/**
	 * Push the object into pool.
	 * @param object KeyedObject.
	 */
	public void push(KeyedObject object) {
		list.addLast(object);
		pool.put(object.getKey(), object);
		usedSize += object.getSize();
		while (usedSize > totalSize)
			pop();
	}

	/**
	 * Removes the item from the pool.
	 */
	private void pop() {

		KeyedObject node = list.removeFirst();

		if (node == null)
			return;

		// Removes the item from the hash table.
		pool.remove(node.getKey());

		usedSize -= node.getSize();
	}

	public void clear() {
		list.clear();
		pool.clear();
	}

	/**
	 * Retrieves an object from the pool to match given key.
	 * 
	 * @param key
	 *            the object key.
	 * 
	 * @return KeyedObject.
	 */
	public KeyedObject getObject(Object key) {

		KeyedObject item = pool.get(key);

		if (item != null) {
			list.remove(item);
			list.addLast(item);
			return item;
		}
		else
			return null;
	}

	/**
	 * Removes object from pool.
	 * 
	 * @param key Object as key.
	 */
	public void removeObject(Object key) {
		KeyedObject item = pool.get(key);

		if (item != null) {
			pool.remove(key);
			usedSize -= item.getSize();
			if (usedSize < 0)
				usedSize = 0;
			list.remove(item);
		}
	}
}
