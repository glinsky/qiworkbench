//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.util;

import java.util.Hashtable;

/**
 * Creates a cache that stores the most recently used items. 
 * This class uses the DoublyLinkedList to manage the objects.
 * 
 */
public class DataCache implements Cloneable {

	/** Store cacheable object */
	private Hashtable<Object, LinkedListNode> nodeTable;

	/** size of bytes. */
	private int totalBytes, consumedBytes;	

	/** The List holds most used item on top. */
	private DoublyLinkedList usedItemList;

	/**
	 * Creates a new cache object with the given cache size.
	 * The size of cache cannot be a negative value.
	 * @param cacheSize	the size of the cache in bytes.
	 *
	 */
	public DataCache(int cacheSize) {

		if (cacheSize < 0)
			throw new IllegalArgumentException(
					"The size of cache cannot be a negative value");

		nodeTable = new Hashtable<Object, LinkedListNode>();
		usedItemList = new DoublyLinkedList();
		consumedBytes = 0;
		totalBytes = cacheSize;
	}

	/**
	 * Clean up cache.
	 */
	public void clear() {
		nodeTable.clear();
		usedItemList.removeAll();
		consumedBytes = 0;
	}

	/**
	 * Retrieves an object from the cache by given key and
	 * makes this object as the most recently used.
	 *
	 * @param key the identifier for the requested object.
	 *
	 * @return Associated KeyedObject.
	 */
	public KeyedObject getObject(Object key) {

		LinkedListNode current = nodeTable.get(key);

		if (current != null) {
			usedItemList.remove(current);
			usedItemList.insertAfterLast(current);
			return (KeyedObject) (current.getObject());
		} else
			return null;
	}

	/**
	 * Removes the least recently used item from the cache.
	 */
	private void pop() {
		LinkedListNode node = usedItemList.removeTail();

		if (node == null)
			return;
		
		KeyedObject object = ((KeyedObject) node.getObject());
		nodeTable.remove(object.getKey());
		consumedBytes -= object.getSize();
	}

	/**
	 * Pushes a new object onto the head of the cache.
	 * @param object the KeyedObject object.
	 */
	public void push(KeyedObject object) {

		LinkedListNode node = new LinkedListNode(object);

		usedItemList.insertAfterLast(node);

		nodeTable.put(object.getKey(), node);
		consumedBytes += object.getSize();

		while (consumedBytes > totalBytes)
			pop();
	}

	/**
	 * Removes object from the cache by given key.
	 * @param key Key of the object
	 */
	public void removeObject(Object key) {
		LinkedListNode currentItem = nodeTable.get(key);

		if (currentItem != null) {
			nodeTable.remove(key);
			consumedBytes -= ((KeyedObject) currentItem.getObject()).getSize();
			if (consumedBytes < 0)
				consumedBytes = 0;
			usedItemList.remove(currentItem);
		}
	}

	/**
	 * Sets the maximum cache size in bytes. 
	 * The size of cache cannot be a negative value.
	 * If the new size is smaller than consumed size,
	 * the least recently used item will be removed.
	 *
	 * @param cacheSize	the size of the cache in bytes.
	 */
	public void setCacheSize(int cacheSize) {

		if (cacheSize < 0)
			throw new IllegalArgumentException(
					"The size of cache cannot be a negative value");

		totalBytes = cacheSize;

		while (consumedBytes > totalBytes)
			pop();
	}
}
