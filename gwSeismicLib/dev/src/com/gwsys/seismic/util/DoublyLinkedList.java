//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.util;

/**
 * Implementation of a doubly linked list.</p>
 * Ref http://www.java2s.com/ExampleCode/Collections-Data-Structure/
 * Doublylinkedlistwithdatastructure.htm
 */
public class DoublyLinkedList {

	/**
	 * Last node in the list.
	 */
	private LinkedListNode lastNode;

	DoublyLinkedList(){
		lastNode = null;
	}
	
	/**
	 * Adds a node on the head.
	 *
	 * @param node node to be added.
	 */
	public LinkedListNode insertAfterLast(LinkedListNode node) {

		if (lastNode != null) {
			node.setPrevious(lastNode.getPrevious());
			node.setNext(lastNode);

			lastNode.getPrevious().setNext(node);
			lastNode.setPrevious(node);
		}

		lastNode = node;
		return node;
	}

	/**
	 * Insert object before given node.
	 *
	 * @param object Object to be added.
	 */
	public LinkedListNode insertBefore(Object object, LinkedListNode after) {

		LinkedListNode current = lastNode;
		while (current != after) {
			current = current.getPrevious();
			if (current == null)
				return null;
		}

		LinkedListNode thisNode = new LinkedListNode(object);

		if (current == lastNode) {
			current.setNext(null);			
		}
		current.getPrevious().setNext(thisNode);
		thisNode.setPrevious(current.getPrevious());
		thisNode.setNext(current);
		current.setPrevious(thisNode);		

		return thisNode;
	}

	/**
	 * Removes given node from the linked list.
	 *
	 * @param node node to be removed
	 */
	public void remove(LinkedListNode node) {
		// check the previous node
		if (node.getPrevious() == node) {
			
			if (node != lastNode)
				return;

			lastNode = null;
			return;
		}

		LinkedListNode nextItem = node.getNext();
		node.getPrevious().setNext(nextItem);
		nextItem.setPrevious(node.getPrevious());

		if (lastNode == node)
			lastNode = nextItem;
	}

	/**
	 * Remove all nodes.
	 *
	 */
	public void removeAll() {
		while (removeTail() != null)
			;
	}

	/**
	 * Removes the tail node from list.
	 *
	 * @return removed node.
	 */
	public LinkedListNode removeTail() {
		//if the last node exists
		if (lastNode != null) {
			LinkedListNode tail = lastNode.getPrevious();
			remove(tail);
			return tail;
		} else
			return null;
	}
}