//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.util;

/**
 * Defines the range by two numbers.
 */
public class NumberRange implements DataRange {

	/**
	 * The minimum number.
	 */
	private Number min = 0;

	/**
	 * The maximum number.
	 */
	private Number max = 0;

	/**
	 * The step number.
	 */
	private Number step = 1;

	/**
	 * Creates a default range which is from zero to zero.
	 */
	public NumberRange() {

		min = 0;
		max = 0;
	}

	/**
	 * Creates a range with given two numbers.
	 *
	 * @param min	The minimum number for this range.
	 * @param max The maximum number for this range.
	 */
	public NumberRange(float min, float max) {
		setRange(min, max);
	}

	/**
	 * Creates a range with given two numbers.
	 *
	 * @param min	The minimum number for this range.
	 * @param max The maximum number for this range.
	 */
	public NumberRange(double min, double max) {
		setRange(min, max);
	}

	/**
	 * Creates a range with given two numbers.
	 *
	 * @param min	The minimum number for this range.
	 * @param max The maximum number for this range.
	 */
	public NumberRange(double min, double max, double step) {
		setRange(min, max);
		this.step = step;
	}

	/**
	 * Updates this range by copy values from another DataRange.
	 *
	 * @param range Another DataRange.
	 */
	public void setRange(DataRange range) {

		min = range.getMin();
		max = range.getMax();
	}

	/**
	 * Sets new range with two values.</p>
	 *
	 * @param min	The minimum number for this range.
	 * @param max The maximum number for this range.
	 */
	public void setRange(double min, double max) {

		if (min < max) {
			this.min = min;
			this.max = max;
		} else {
			this.max = min;
			this.min = max;
		}
	}

	/**
	 * Returns the number of samples in this given range.
	 * @return
	 */
	public int getRange() {
		return max.intValue() - min.intValue() + 1;
	}

	/**
	 * Shift the range to new location.
	 * @param start The new min value.
	 */
	public void shift(int start) {
		max = max.intValue() - min.intValue() + start;
		min = start;
	}

	/**
	 * Retrieves the minimum number of this range.
	 *
	 * @return the minimum number.
	 */
	public Number getMin() {
		return min;
	}

	/**
	 * Retrieves the maximum number of this range.
	 *
	 * @return the maximum number.
	 */
	public Number getMax() {
		return max;
	}

	/**
	 * Sets the minimum number of this range.
	 *
	 * @param min the minimum number.
	 */
	public void setMin(double min) {
		this.min = min;
	}

	/**
	 * Sets the maximum number of this range.
	 *
	 * @param the maximum number.
	 */
	public void setMax(double max) {
		this.max = max;
	}

	public void setStep(double step) {
		this.step = step;
	}

	public Number getStep() {
		return step;
	}

}
