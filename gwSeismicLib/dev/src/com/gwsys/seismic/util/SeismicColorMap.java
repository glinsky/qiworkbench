//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.util;

import java.awt.image.IndexColorModel;
import java.awt.Color;

import java.util.Arrays;
import java.util.Vector;
import java.util.Iterator;

import com.gwsys.gw2d.util.GWUtilities;

/**
 * This class defines the color pattern for seismic trace renderer. It contains
 * color array for density trace, fill color for wiggle trace. To match the API
 * with bhpViewer, this class use five colors for wiggle rendering, and 32
 * colors for density by default. The interpolated colors are used for density
 * rendering.
 * 
 * @author Team
 */
public class SeismicColorMap implements Cloneable {
	public final static int BACKGROUND_COLOR = 0;

	// Static members
	private final static int DEFAULT_COLOR_SIZE = 37;

	public final static int DEFAULT_WIGGLE_COLOR_SIZE = 5;

	public final static int FOREGROUND_COLOR = 1;

	public final static int HIGHLIGHT_COLOR = 4;

	private final static int MAX_COLOR_SIZE = 256;

	public final static int NEGATIVE_COLOR = 3;

	public final static int POSITIVE_COLOR = 2;

	private int bgColorIndex, fgColorIndex, highLightIndex;

	// private members
	private Vector<SeismicColorListener> colorListeners = new Vector<SeismicColorListener>();

	private int densityColorSize = DEFAULT_COLOR_SIZE
			- DEFAULT_WIGGLE_COLOR_SIZE;

	private IndexColorModel icolorModel = null;

	private boolean[] interpolatedFlag;

	private boolean isTransparent = false, notifyListeners = false;

	private int negaFillColorIndex, posiFillColorIndex = 0;

	private byte[] red, green, blue, alpha = null;

	/**
	 * Creates default 32 gray-scale colors and 5 wiggle colors map.
	 */
	public SeismicColorMap() {
		this(DEFAULT_COLOR_SIZE, true);
	}

	/**
	 * Creates new color map with given total size and background flag (
	 * transparent or opaque ).
	 * 
	 * @param mapsize
	 *            desired size of color map
	 * @param transparent
	 *            transparent or opaque background
	 * 
	 */
	public SeismicColorMap(int mapsize, boolean transparent) {
		int size = ((mapsize > MAX_COLOR_SIZE) ? MAX_COLOR_SIZE : mapsize);

		red = new byte[size];
		green = new byte[size];
		blue = new byte[size];
		alpha = new byte[size];

		interpolatedFlag = new boolean[size];

		isTransparent = transparent;

		setDensityColorSize(size - DEFAULT_WIGGLE_COLOR_SIZE);

		createDefaultColor();

		Arrays.fill(interpolatedFlag, false);

	}

	/**
	 * Adds the listener to this class.
	 * 
	 * @param lis
	 *            color map listener.
	 */
	public void addColorMapListener(SeismicColorListener lis) {
		colorListeners.addElement(lis);
	}

	/**
	 * Returns a copy of this color map. Listeners are not copyied.
	 * 
	 * @return copy of this color map.
	 */
	public Object clone() {
		try {
			SeismicColorMap clone = (SeismicColorMap) super.clone();

			clone.colorListeners = new Vector<SeismicColorListener>();
			// copy color elements
			int number = red.length;
			clone.red = new byte[number];
			System.arraycopy(red, 0, clone.red, 0, number);
			clone.green = new byte[number];
			System.arraycopy(green, 0, clone.green, 0, number);
			clone.blue = new byte[number];
			System.arraycopy(blue, 0, clone.blue, 0, number);
			clone.alpha = new byte[number];
			System.arraycopy(alpha, 0, clone.alpha, 0, number);
			clone.interpolatedFlag = new boolean[number];
			System.arraycopy(interpolatedFlag, 0, clone.interpolatedFlag, 0,
					number);

			clone.icolorModel = createIndexColorModel(8, clone.getSize(),
					clone.red, clone.green, clone.blue, clone.alpha);

			return clone;
		} catch (CloneNotSupportedException e) {
			GWUtilities.warning(e.getMessage());
		}
		return null;
	}

	/**
	 * Color array from blue-white-red colors
	 */
	private void createDefaultColor() {
		int end = getDensityColorSize() - 1;
		//use blue-white-red colors
		int step = 256*2/getDensityColorSize();
		for (int i = 0; i < getDensityColorSize()/2; i++) {
			blue[i] = (byte) 0xff; 
			green[i] = (byte)(step*i);
			red[i] = (byte)(step*i);
		}
		for (int i = 0; i < end/2; i++) {
			red[getDensityColorSize()/2+i] = (byte) 0xff; 
			green[getDensityColorSize()/2+i] = (byte)(241 - step*i);
			blue[getDensityColorSize()/2+i] = (byte)(241 - step*i);
		}
		red[end] = (byte) 0xff; green[end] = blue[end] = 0;

		for (int i = 0; i <= end; i++) {
			alpha[i] = (byte) 255;
		}
		initDefaultWiggleColor();
		icolorModel = createIndexColorModel(8, getSize(), red, green, blue,
				alpha);

		if (notifyListeners) {
			notifyListeners(new SeismicColorEvent(this,
					SeismicColorEvent.COLORMAP_CHANGED));
		}
	}

	private IndexColorModel createIndexColorModel(int bits, int size, byte r[],
			byte g[], byte b[], byte a[]) {
		if (isTransparent)
			return new IndexColorModel(bits, size, r, g, b, a);
		else
			return new IndexColorModel(bits, size, r, g, b);
	}

	/**
	 * Gets the background color.
	 * 
	 * @return the background color.
	 */
	public Color getBackground() {
		return getColor(bgColorIndex);
	}

	/**
	 * Returns index of background color in color map.
	 * 
	 * @return index of background color in color map.
	 */
	public byte getBackgroundIndex() {
		return (byte) bgColorIndex;
	}

	/**
	 * Gets the color in the specified index.
	 * 
	 * @param index
	 *            the desired index in the color map.
	 * @return the color value.
	 */
	public Color getColor(int index) {

		int a = (alpha[index] << 24) & 0xFF000000;
		int r = (red[index] << 16) & 0x00FF0000;
		int g = (green[index] << 8) & 0x0000FF00;
		int b = blue[index] & 0x000000FF;

		return new Color(a | r | g | b, true);
	}

	/**
	 * Returns copied color array.
	 * 
	 * @return copied color array.
	 */
	public Color[] getColorRamp() {

		Color[] colors = new Color[densityColorSize];

		for (int i = 0; i < densityColorSize; i++) {
			colors[i] = getColor(i);
		}

		return colors;
	}

	/**
	 * Returns the size of color map for density.
	 * 
	 * @return the size of color map for density.
	 */
	public int getDensityColorSize() {
		return densityColorSize;
	}

	/**
	 * Gets the foreground color.
	 * 
	 * @return the foreground color.
	 */
	public Color getForeground() {
		return getColor(fgColorIndex);
	}

	/**
	 * Returns index of foreground color in color map.
	 * 
	 * @return index of foreground color in color map.
	 */
	public byte getForegroundIndex() {
		return (byte) fgColorIndex;
	}

	/**
	 * Gets the highlight color.
	 * 
	 * @return the highlight color.
	 */
	public Color getHighlight() {
		return getColor(fgColorIndex);
	}

	/**
	 * Returns index of highlight color in color map.
	 * 
	 * @return index of highlight color in color map.
	 */
	public byte getHighlightIndex() {
		return (byte) fgColorIndex;
	}

	/**
	 * Returns color model used by this color map.
	 * 
	 * @return color model used by this color map
	 */
	public IndexColorModel getIndexColorModel() {
		return icolorModel;
	}

	/**
	 * Gets the negative fill color.
	 * 
	 * @return the negaitive fill color.
	 */
	public Color getNegativeFill() {
		return getColor(negaFillColorIndex);
	}

	/**
	 * Returns index of negative fill color in color map.
	 * 
	 * @return index of negative fill color in color map.
	 */
	public byte getNegativeFillIndex() {
		return (byte) negaFillColorIndex;
	}

	/**
	 * Gets the positive fill color.
	 * 
	 * @return the positive fill color.
	 */
	public Color getPositiveFill() {
		return getColor(posiFillColorIndex);
	}

	/**
	 * Returns index of positive fill color in color map.
	 * 
	 * @return index of positive fill color in color map.
	 */
	public byte getPositiveFillIndex() {
		return (byte) posiFillColorIndex;
	}

	/**
	 * Returns color map size.
	 * 
	 * @return color map size.
	 */
	public int getSize() {
		return red.length;
	}

	/**
	 * Init the wiggle colors with white and black.
	 * 
	 */
	public void initDefaultWiggleColor() {

		bgColorIndex = getSize() - DEFAULT_WIGGLE_COLOR_SIZE;
		fgColorIndex = bgColorIndex + 1;
		posiFillColorIndex = fgColorIndex + 1;
		negaFillColorIndex = posiFillColorIndex + 1;
		highLightIndex = negaFillColorIndex + 1;
		// init wiggle colors
		Color color = Color.black;
		red[fgColorIndex] = (byte) color.getRed();
		green[fgColorIndex] = (byte) color.getGreen();
		blue[fgColorIndex] = (byte) color.getBlue();
		alpha[fgColorIndex] = (byte) color.getAlpha();
		red[posiFillColorIndex] = (byte) color.getRed();
		green[posiFillColorIndex] = (byte) color.getGreen();
		blue[posiFillColorIndex] = (byte) color.getBlue();
		alpha[posiFillColorIndex] = (byte) color.getAlpha();
		red[negaFillColorIndex] = (byte) color.getRed();
		green[negaFillColorIndex] = (byte) color.getGreen();
		blue[negaFillColorIndex] = (byte) color.getBlue();
		alpha[negaFillColorIndex] = (byte) color.getAlpha();
		color = Color.white;
		red[bgColorIndex] = (byte) color.getRed();
		green[bgColorIndex] = (byte) color.getGreen();
		blue[bgColorIndex] = (byte) color.getBlue();
		alpha[bgColorIndex] = (byte) color.getAlpha();
		if (isTransparent) {
			alpha[bgColorIndex] = 0;
		}
		color = Color.RED;
		red[highLightIndex] = (byte) color.getRed();
		green[highLightIndex] = (byte) color.getGreen();
		blue[highLightIndex] = (byte) color.getBlue();
		alpha[highLightIndex] = (byte) color.getAlpha();
	}

	/**
	 * Notify Listeners.
	 * 
	 * @param e
	 *            SeismicColorEvent.
	 */
	private void notifyListeners(SeismicColorEvent e) {

		Iterator it = colorListeners.iterator();
		while (it.hasNext()) {
			((SeismicColorListener) it.next()).colorMapUpdated(e);
		}
	}

	/**
	 * Removes listener from this class.
	 * 
	 * @param lis
	 *            color map listener.
	 */
	public void removeColorMapListener(SeismicColorListener lis) {
		colorListeners.removeElement(lis);
	}

	/**
	 * Sets the background color.
	 * 
	 * @param background
	 *            the background color.
	 */
	public void setBackground(Color background) {
		setColor(bgColorIndex, background);
	}

	/**
	 * Sets the specified color into given index.
	 * 
	 * @param index
	 *            the desired index.
	 * @param color
	 *            the desired color.
	 */
	public void setColor(int index, Color color) {
		setColor(index, color, notifyListeners);
	}

	/**
	 * Sets the specified color into given index.
	 * 
	 * @param index
	 *            the desired index.
	 * @param color
	 *            the desired color.
	 * @param notify
	 *            indicates whether notify listener or not.
	 */
	public void setColor(int index, Color color, boolean notify) {

		if (index < getSize()) {
			red[index] = (byte) color.getRed();
			green[index] = (byte) color.getGreen();
			blue[index] = (byte) color.getBlue();
			alpha[index] = (byte) color.getAlpha();

			icolorModel = createIndexColorModel(8, getSize(), red, green, blue,
					alpha);
			if (notify)
				notifyListeners(new SeismicColorEvent(this,
						SeismicColorEvent.COLORMAP_CHANGED));
		}
	}

	/**
	 * Sets the special index of color in color map.
	 * 
	 * @param colorId
	 *            One of Constants defined in this class.
	 */
	public void setColorIndex(int colorId, int index) {
		switch (colorId) {
		case BACKGROUND_COLOR:
			bgColorIndex = index;
			break;
		case FOREGROUND_COLOR:
			fgColorIndex = index;
			break;
		case NEGATIVE_COLOR:
			negaFillColorIndex = index;
			break;
		case POSITIVE_COLOR:
			posiFillColorIndex = index;
			break;
		case HIGHLIGHT_COLOR:
			highLightIndex = index;
			break;
		default:
		}

	}

	/**
	 * Setsd new color array.
	 * 
	 * @param colors
	 */
	public void setColorRamp(Color[] colors) {
		setSize(colors.length);
		for (int index = 0; index < colors.length; index++) {
			red[index] = (byte) colors[index].getRed();
			green[index] = (byte) colors[index].getGreen();
			blue[index] = (byte) colors[index].getBlue();
			alpha[index] = (byte) colors[index].getAlpha();
		}

		icolorModel = createIndexColorModel(8, getSize(), red, green, blue,
				alpha);
		if (this.notifyListeners)
			notifyListeners(new SeismicColorEvent(this,
					SeismicColorEvent.COLORMAP_CHANGED));

	}

	/**
	 * Sets new size for density colors.
	 * 
	 * @param rsize
	 *            new ramp size
	 */
	public void setDensityColorSize(int rsize) {
		if (rsize > 0 && rsize < getSize()) {
			densityColorSize = rsize;

			if (notifyListeners)
				notifyListeners(new SeismicColorEvent(this,
						SeismicColorEvent.COLORMAP_CHANGED));
		}
	}

	/**
	 * Sets the foreground color.
	 * 
	 * @param foreground
	 *            the foreground color.
	 */
	public void setForeground(Color foreground) {
		setColor(fgColorIndex, foreground);
	}

	/**
	 * Sets the highlight color.
	 * 
	 * @param highlight
	 *            the highlight color.
	 */
	public void setHighlight(Color highlight) {
		setColor(highLightIndex, highlight);
	}

	/**
	 * Sets if the given index in color map is interpolation point.
	 * 
	 * @param index
	 *            The desired index into the color array.
	 * @param state
	 *            The new interpolation status for this index.
	 */
	public void setInterpolationPoint(int index, boolean state) {
		interpolatedFlag[index] = state;
	}

	/**
	 * Sets the negative fill color.
	 * 
	 * @param negative
	 *            the negative fill color.
	 */
	public void setNegativeFill(Color negative) {
		setColor(negaFillColorIndex, negative);
	}

	/**
	 * Sets the listener notification flag.
	 * 
	 * @param status
	 *            <code>true</code> to notify the listeners;
	 *            <code>false</code> otherwise.
	 */
	public void setNotify(boolean status) {
		notifyListeners = status;
	}

	/**
	 * Sets the positive fill color.
	 * 
	 * @param positive
	 *            the positive fill color.
	 */
	public void setPositiveFill(Color positive) {
		setColor(posiFillColorIndex, positive);
	}

	/**
	 * Sets new color map size.
	 * 
	 * @param csize
	 *            new color map size
	 */
	public void setSize(int csize) {
		if (csize <= DEFAULT_WIGGLE_COLOR_SIZE)
			throw new IllegalArgumentException(
					"This size is too small for rendering");
		int size = ((csize > MAX_COLOR_SIZE) ? MAX_COLOR_SIZE : csize);
		if (getSize() != size) {
			red = new byte[size];
			green = new byte[size];
			blue = new byte[size];
			alpha = new byte[size];

			interpolatedFlag = new boolean[size];
		}
		// reset color value
		for (int i = 0; i < size; i++) {
			red[i] = green[i] = blue[i] = 0;
			alpha[i] = (byte) 255;
			setInterpolationPoint(i, false);
		}

		densityColorSize = size - DEFAULT_WIGGLE_COLOR_SIZE;
		initDefaultWiggleColor();

		if (notifyListeners) {
			notifyListeners(new SeismicColorEvent(this,
					SeismicColorEvent.COLORMAP_CHANGED));
		}
	}
}
