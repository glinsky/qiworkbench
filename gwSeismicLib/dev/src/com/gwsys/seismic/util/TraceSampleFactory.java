//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.util;

import com.gwsys.seismic.reader.SeismicFormat;
import com.gwsys.util.ConvertionException;

/**
 * Provides the factory methods to convert trace sample value to float.
 * This function is based on the SU ibm_to_float C function.
 * 
 */
public class TraceSampleFactory {
	/**
	 * Converts the bytes in ibm format to float array.
	 * @param source the bytes in ibm format.
	 * @param dest float array.
	 * @param numSamples Number of samples.
	 */
	private static void convertIBM2Float(byte[] source, float[] dest,
			int numSamples) {
		for (int i = 0, j = 0; i < numSamples; i++, j += 4) {
			try {
				dest[i] = ibmFloatBytesToFloat(source, j);
			} catch (ConvertionException e) {
				// ignore
				e.printStackTrace();
			}
		}
	}

	/**
	 * Convert 4 bytes from an array to a float where the 4 bytes are
	 * interpreted as having an IBM 32 bit floating point format.
	 * 
	 * IBM floating point format is big endian with layout: 1) 1 sign bit 2) a 7
	 * bit exponent which is base 16 and offset 64 3) a 24 bit significand
	 * (mantissa) with no hidden bit from the normalisation (not possible with
	 * the base 16 exponent). Hence the decimal value = significand / 2^24 *
	 * 16^(exponent-64)
	 * 
	 * This function is based on the SU ibm_to_float C function to be compatible
	 * with su software. The relevant parts of the SU documentation are as
	 * follows:
	 * 
	 * ***********************************************************************
	 * ibm_to_float - convert between 32 bit IBM and IEEE floating numbers
	 * ***********************************************************************
	 * Notes: IBM -> IEEE may overflow or underflow, taken care of by
	 * substituting large number or zero Only integer shifting and masking are
	 * used.
	 * ************************************************************************
	 * Credits: CWP: Brian Sumner, c.1985
	 * ************************************************************************
	 * 
	 * 
	 * @param bArray
	 *            Input - array containing the bytes to convert
	 * @param offset
	 *            Input - the 32 bits bArray[offset] (the most significant bits)
	 *            up to bArray[offset+3] are converted make up the bit pattern
	 *            that is converted
	 * @return Returns the converted float
	 */
	private static float ibmFloatBytesToFloat(byte[] bArray, int offset)
			throws ConvertionException {

		int fconv = signedIntBytesToInt(bArray, offset);
		if (fconv != 0) {
			int fmant = 0x00ffffff & fconv; // get significand by masking out
											// the exponent
			if (fmant == 0) {
				throw new ConvertionException(
						"ByteConvert.ibmFloatBytesToFloat - error - the input value"
								+ " has a zero significand (mantissa) and hence is not in IBM floating point format");
			}
			// Get the IEEE exponent and normalise the significand (adjusting
			// the exponent in
			// the process)
			int t = (int) ((0x7f000000 & fconv) >> 22) - 130;
			while ((fmant & 0x00800000) == 0) {
				--t;
				fmant <<= 1;
			}
			if (t > 254) { // exponent overflow
				fconv = (0x80000000 & fconv) | 0x7f7fffff;
			} else if (t <= 0) { // exponent underflow
				fconv = 0;
			} else {
				fconv = (0x80000000 & fconv) | // set sign bit
						(t << 23) | // set exponent
						(0x007fffff & fmant); // set significand masking off
												// hidden bit
			}
		}
		return Float.intBitsToFloat(fconv);
	}

	/**
	 * Convert 4 bytes from an array to an int where the 4 bytes are interpreted
	 * as having a 32 bit (signed int) 2's complement bit pattern
	 * 
	 * @param bArray
	 *            Input - array containing the bytes to convert
	 * @param offset
	 *            Input - the 32 bits bArray[offset] (the most significant bits)
	 *            up to bArray[offset+3] make up the bit pattern that is
	 *            converted
	 * @return Returns the converted int
	 */
	private static int signedIntBytesToInt(byte[] bArray, int offset) {
		// The masking with 0xff is required to stop the sign extension
		// on widening of the bytes when they have negative values
		return bArray[offset] << 24 | (bArray[offset + 1] & 0xff) << 16
				| (bArray[offset + 2] & 0xff) << 8
				| (bArray[offset + 3] & 0xff);
	}

	/**
	 * Converts the bytes to floats.
	 * 
	 * @param type
	 *            The type that defined in SegySampleFormat.
	 * @param source
	 *            Sample array.
	 * @param dest
	 *            converted values.
	 * @param number
	 */
	public static void toFloatArray(int type, byte[] source, float[] dest,
			int number) {
		switch (type) {
		case SeismicFormat.DATA_SAMPLE_FLOAT:
			int value;

			for (int i = 0, offset = 0; i < number; i++, offset += 4) {
				value = (((source[offset] & 0xff) << 24)
						| ((source[offset + 1] & 0xff) << 16)
						| ((source[offset + 2] & 0xff) << 8) | (source[offset + 3] & 0xff));
				dest[i] = Float.intBitsToFloat(value);
			}
			break;

		case SeismicFormat.DATA_SAMPLE_BYTE:

			for (int i = 0, offset = 0; i < number; i++, offset++) {
				dest[i] = (float) source[offset];
			}
			break;
		case SeismicFormat.DATA_SAMPLE_SHORT:
			for (int i = 0, offset = 0; i < number; i++, offset += 2) {
				dest[i] = (short) (((source[offset] & 0xff) << 8) | (source[offset + 1] & 0xff));
			}
			break;
		case SeismicFormat.DATA_SAMPLE_INTEGER:

			for (int i = 0, offset = 0; i < number; i++, offset += 4) {
				dest[i] = (int) (((source[offset] & 0xff) << 24)
						| ((source[offset + 1] & 0xff) << 16)
						| ((source[offset + 2] & 0xff) << 8) | (source[offset + 3] & 0xff));
			}
			break;

		case SeismicFormat.DATA_SAMPLE_IBM_FLOAT:
			convertIBM2Float(source, dest, number);
			break;

		default:
			throw new UnsupportedOperationException(
					"This type is not supported in current version.");
		}
	}
}
