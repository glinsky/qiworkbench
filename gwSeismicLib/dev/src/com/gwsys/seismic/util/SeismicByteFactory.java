//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.util;

import java.io.UnsupportedEncodingException;

import com.gwsys.gw2d.util.GWUtilities;
import com.gwsys.seismic.indexing.SeismicConstants;

/**
 * Provides the factory methods to convert bytes to Number.
 */
public class SeismicByteFactory {

	/**
	 * Returns a Number instance.
	 *
	 * @param type The type that defined in SeismicConstants.
	 * @param bytes the byte array.
	 * @param offset The offset in the array
	 */
	public static Number toNumber(int type, byte[] source, int offset) {
		Number number = null;
		switch (type) {
		case SeismicConstants.DATA_FORMAT_DOUBLE:
			number = new Double(toDouble(source, offset));
			break;
		case SeismicConstants.DATA_FORMAT_FLOAT:
			number = new Float(toFloat(source, offset));
			break;
		case SeismicConstants.DATA_FORMAT_INT:
			number = new Integer(toInteger(source, offset));
			break;
		case SeismicConstants.DATA_FORMAT_UNSIGNEDINT:
			number = new Long((long) (toInteger(source, offset)));
			break;
		case SeismicConstants.DATA_FORMAT_SHORT:
			number = new Short(toShort(source, offset));
			break;
		case SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT:
			number = new Integer(toUnsignedShort(source, offset));
			break;
		case SeismicConstants.DATA_FORMAT_BYTE:
			number = new Byte(source[offset]);
			break;
		case SeismicConstants.DATA_FORMAT_UNSIGNEDBYTE:
			number = new Short((short) (source[offset] & 0xff));
			break;
		case SeismicConstants.DATA_FORMAT_IBM_FLOAT:
			number = ibmFloatBytesToFloat(source, offset);

		default:
			throw new UnsupportedOperationException(
					"This type is not supported in current version.");
		}
		return number;
	}

	public static short toUnsignedByte(byte[] source, int offset) {
		return (short) (source[offset] & 0xff);
	}

	public static long toUnsignedInt(byte[] source, int offset) {
		return (long) (((source[offset] & 0xff) << 24)
				| ((source[offset + 1] & 0xff) << 16)
				| ((source[offset + 2] & 0xff) << 8) | (source[offset + 3] & 0xff));
	}

	public static int toUnsignedShort(byte[] source, int offset) {
		return (int) (((source[offset] & 0xff) << 8) | (source[offset + 1] & 0xff));
	}

	public static short toShort(byte[] source, int offset) {
		return (short) (((source[offset] & 0xff) << 8) | (source[offset + 1] & 0xff));
	}

	public static int toInteger(byte[] source, int offset) {
		return (int) (((source[offset] & 0xff) << 24)
				| ((source[offset + 1] & 0xff) << 16)
				| ((source[offset + 2] & 0xff) << 8) | (source[offset + 3] & 0xff));
	}

	public static float toFloat(byte[] source, int offset) {
		return Float.intBitsToFloat(toInteger(source, offset));
	}

	public static double toDouble(byte[] source, int offset) {
		return (double) (((source[offset] & 0xff) << 56)
				| ((source[offset + 1] & 0xff) << 48)
				| ((source[offset + 2] & 0xff) << 40)
				| ((source[offset + 3] & 0xff) << 32)
				| ((source[offset + 4] & 0xff) << 24)
				| ((source[offset + 5] & 0xff) << 16)
				| ((source[offset + 6] & 0xff) << 8) | (source[offset + 7] & 0xff));
	}

	/**
	 * Convert 4 bytes from an array to a float where the 4 bytes are interpreted
	 * as having an IBM 32 bit floating point format.
	 * 
	 * IBM floating point format is big endian with layout:
	 * 1) 1 sign bit
	 * 2) a 7 bit exponent which is base 16 and offset 64
	 * 3) a 24 bit significand (mantissa) with no hidden
	 * bit from the normalisation (not possible with the base
	 * 16 exponent). 
	 * Hence the decimal value = 
	 *    significand / 2^24 * 16^(exponent-64)
	 * 
	 * This function is based on the SU ibm_to_float
	 * C function to be compatible with su software. The
	 * relevant parts of the SU documentation are as follows:
	 * 
	 ************************************************************************
	 ibm_to_float - convert between 32 bit IBM and IEEE floating numbers
	 ************************************************************************
	 Notes:
	 IBM -> IEEE may overflow or underflow, taken care of by 
	 substituting large number or zero
	 Only integer shifting and masking are used.
	 ************************************************************************* 
	 Credits: CWP: Brian Sumner,  c.1985
	 *************************************************************************
	 * 
	 * 
	 * @param bArray Input - array containing the bytes to convert
	 * @param offset Input - the 32 bits bArray[offset] (the most significant bits)
	 * up to bArray[offset+3] are converted make up the bit pattern that is 
	 * converted
	 * @return Returns the converted Number
	 */
	private static Number ibmFloatBytesToFloat(byte[] bArray, int offset) {

		int fconv = toInteger(bArray, offset);
		if (fconv != 0) {
			int fmant = 0x00ffffff & fconv; // get significand by masking out the exponent
			if (fmant == 0) {
				throw new IllegalArgumentException(
						"ByteConvert.ibmFloatBytesToFloat - error - the input value"
								+ " has a zero significand (mantissa) and hence is not in IBM floating point format");
			}
			// Get the IEEE exponent and normalise the significand (adjusting the exponent in
			// the process)
			int t = (int) ((0x7f000000 & fconv) >> 22) - 130;
			while ((fmant & 0x00800000) == 0) {
				--t;
				fmant <<= 1;
			}
			if (t > 254) { // exponent overflow
				fconv = (0x80000000 & fconv) | 0x7f7fffff;
			} else if (t <= 0) { // exponent underflow
				fconv = 0;
			} else {
				fconv = (0x80000000 & fconv) | // set sign bit
						(t << 23) | // set exponent
						(0x007fffff & fmant); // set significand masking off hidden bit
			}
		}
		return Float.intBitsToFloat(fconv);
	}

	/**
	 * Converts the header bytes to an array of strings using 
	 * <code>cp500</code> encoding. 
	 * @param bytes Ebcdic Bytes
	 * @return
	 */
	public static String[] ebcdicBytesToStringArray(byte[] bytes) {
		String ebcdicString = null;
		//convert to big string
		if ((bytes != null)) {
			try {
				ebcdicString = new String(bytes, "cp500");
			} catch (UnsupportedEncodingException e) {
				GWUtilities.warning("Could not convert bytes to string!");
				return null;
			}
		}else 
			return null;
		int sizeLine = 80;
		String[] header = new String[(int) Math.ceil(bytes.length / sizeLine)];

		for (int i = 0; i < header.length; i++) {
			header[i] = ebcdicString.substring(i*sizeLine, 
					Math.min((i+1)*sizeLine, bytes.length));
			
		}
		return header;
	}
}
