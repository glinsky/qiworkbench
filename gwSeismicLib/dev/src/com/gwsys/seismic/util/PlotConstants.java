//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.util;

public class PlotConstants {

	/**
	 * No annotations.
	 */
	public final static int NONE = 0;

	/**
	 * Put annotation on the left side of plot.
	 */
	public final static int LEFT = 1;

	/**
	 * Put annotation at the right side of plot.
	 */
	public final static int RIGHT = 2;

	/**
	 * Put annotation on the top of plot.
	 */
	public final static int TOP = 4;

	/**
	 * Put annotation at the bottom of plot.
	 */
	public final static int BOTTOM = 8;

	/**
	 * Put annotation at both the left and right sides of plot.
	 */
	public final static int LEFT_RIGHT = (LEFT | RIGHT);

	/**
	 * Put annotation at both the top and bottom of plot.
	 */
	public final static int TOP_BOTTOM = (TOP | BOTTOM);

	/**
	 * Put annotations at all sides of plot.
	 */
	public final static int ALL_SIDES = (LEFT | RIGHT | TOP | BOTTOM);

}
