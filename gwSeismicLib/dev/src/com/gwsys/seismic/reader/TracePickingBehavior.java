//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.reader;

import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;

/**
 * Defines the behavior to pick a value in seismic trace.
 * 
 * This api is extracted from BhpSelectionController.
 */
public class TracePickingBehavior {

	//Copied from BhpPickingManager to avoid retroactively tying the G&W library to the bhpViewer's UI.
        //Actually, this should be an enumerated type and the viewer should reference it.
	public static final int SNAP_NO = -1;
        public static final int SNAP_MAX = 0;
        public static final int SNAP_MIN = 1;
        public static final int SNAP_ZERO = 2;
        
        public static final int SNAP_DEFAULT = 20;
        
        private int snapType = SNAP_NO;
        private int snapLimit = SNAP_DEFAULT;
        
	private NumberRange _sampleRange = new NumberRange();

	private SeismicReader _seismicReader = null;

	private NearTraceValuePicking _snapCalc = null;

	private TraceData _traceData = new TraceData();

	/**
	 * Default constructor.
	 */
	public TracePickingBehavior() {
	}

	/**
	 * Constructor.
	 * @param reader SeismicReader.
	 */
	public TracePickingBehavior(SeismicReader reader) {
		setSeismicReader(reader);
	}

	/**
         * Below is a candidate for perhaps the best definition of a method and the worst comment ever - whf.
	 * Returns a value to match given algorithm.  
	 * @param valuein The inputed value.
	 * @param traceId Id of picked trace .
	 *
	 * @return value to match picking algorithm.
	 */
	public double getSnapValue(int traceId, double valuein) {
		if ((_seismicReader == null)) {
			throw new IllegalStateException("SeismicReader is not ready");
		}

		_sampleRange.setMin(0);
		_sampleRange.setMax(_seismicReader.getMetaData().getSamplesPerTrace());
		_seismicReader.process(traceId, _sampleRange, _traceData);
		if (_traceData.getSamples() != null) {
			double rate=_seismicReader.getMetaData().getSampleRate();
			int whichsample = (int) (valuein / rate);
			float[] samples = _traceData.getSamples();
			if ((whichsample < 0) || (whichsample >= samples.length))
				throw new IllegalStateException("There is no matched sample.");
			
                        if (_snapCalc == null) {
                            return valuein;
                        } else {
                            return _snapCalc.calculateSnapSample(whichsample, samples, 20) * rate;
                        }
		}else
			throw new IllegalStateException("There is no matched trace.");
	}

	/**
         * Sets the type of picking used by the snap operation (event drawing).
         * 
	 * @param snapType one of <br />
         * <ul>
         * <li>-1 PICK_NO</li>
         * <li> 0 PICK_MAX</li>
         * <li> 1 PICK_MIN</li>
         * <li> 2 PICK_0</li>
         * </ul>
	 */
	public void setPickMode(int snapType, int snapLimit) {
		if ((snapType == this.snapType) && (this.snapLimit == snapLimit)){
                    return;
                } else {
                    this.snapType = snapType;
                    this.snapLimit = snapLimit;
                    
                    switch (snapType) {
                    case SNAP_NO :
                        _snapCalc = null;
                        break;
                    case SNAP_MIN :
                        _snapCalc = new NearMinPicking();
                        break;
                    case SNAP_MAX :
                        _snapCalc = new NearMaxPicking();
                        break;
                    case SNAP_ZERO :
                        _snapCalc = new NearZeroPicking();
                        break;
                    default :
                        throw new UnsupportedOperationException("Unknown pickType: " + snapType);
                    }
                }
	}

	/**
	 * Sets the seismic reader to this behavior.
	 *
	 * @param reader Seismic reader.
	 */
	public void setSeismicReader(SeismicReader reader) {
		_seismicReader = reader;
	}
}