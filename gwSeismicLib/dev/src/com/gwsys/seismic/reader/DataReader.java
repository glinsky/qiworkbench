//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.reader;

import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.seismic.io.SeismicMetaData;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.util.NumberRange;

/**
 * Defines the common interface for DataReader.
 * @author
 *
 */
public interface DataReader {
	/**
	 * Process the trace with given trace id and sample range.
	 * @param traceId		the desired trace ID.
	 * @param sampleRange	the range of samples.
	 * @param traceData		the output trace data.
	 *
	 * @return Status of processing. True if successful.
	 */
	public boolean process(int traceId, NumberRange sampleRange,
			TraceData traceData);

	/**
	 * Retrieves the metadata for the dataset.
	 * 
	 * @return the metadata for the dataset.
	 */
	public abstract SeismicMetaData getMetaData();

	/**
	 * Retrieves the limit of the seismic data in model coordinates.
	 * By default it is defined by the number of traces and the number of samples.
	 *  
	 * @return Bound2D.
	 */
	public abstract Bound2D getModelLimits();
	/**
	 * Reads the trace header for the given trace ID.
	 * 
	 * @param traceId
	 *            the ID defines in the trace.
	 * 
	 * @return TraceHeader.
	 */
	public abstract TraceHeader getTraceMetaData(int traceId);
}
