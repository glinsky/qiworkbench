//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.reader;

import java.util.Vector;

/**
 * This interface defines some constants used in SEG-Y data format. Ref SU v37
 * release and Rev 1 SEG Y Standard. To be compatible with BHP Viewer, the terms
 * are copied from BHP Viewer codes (BhpSegyFormat.java).
 *
 */
public interface SeismicFormat {

	/**
	 * The line number for 3-D post-stack data.
	 */
	public static final int HEADER_LINE_NUMBER = 100;

	public static final String LINE_NUMBER = "Line Number";

	/**
	 * Data traces per ensemble.
	 */
	public static final int HEADER_TRACES_PER_ENSEMBLE = 101;

	public static final String TRACES_PER_ENSEMBLE = "Traces per Ensemble";

	/**
	 * The auxiliary traces per ensemble.
	 */
	public static final int HEADER_AUX_TRACES_PER_ENSEMBLE = 102;

	public static final String AUX_TRACES_PER_ENSEMBLE = "Aux Traces per Ensemble";

	/**
	 * The sample interval in microseconds.
	 */
	public static final int HEADER_SAMPLE_INTERVAL = 103;

	public static final String SAMPLE_INTERVAL = "Sample Interval";

	/**
	 * the number of samples per data trace.
	 */
	public static final int HEADER_SAMPLES_PER_TRACE = 104;

	public static final String SAMPLES_PER_TRACE = "Samples per Trace";

	/**
	 * The data sample format codes.
	 */
	public static final int HEADER_DATA_FORMAT_CODE = 105;

	public static final String DATA_FORMAT_CODE = "Data Format";

	/**
	 * The expected number of data traces per CDP ensemble.
	 */
	public static final int HEADER_TRACES_PER_CDP_ENSEMBLE = 106;

	public static final String TRACES_PER_CDP_ENSEMBLE = "Traces per CDP";

	/**
	 * the measurement system used (meters or feet).
	 */
	public static final int HEADER_MEASUREMENT_SYSTEM = 107;

	public static final String MEASUREMENT_SYSTEM = "Measurement System";

	/**
	 * the SEG-Y format revision number.
	 */
	public static final int HEADER_SEGY_REVISION_NUMBER = 108;

	public static final String SEGY_REVISION_NUMBER = "Segy Revision Number";

	/**
	 * A fixed length trace flag.
	 */
	public static final int HEADER_FIXED_LENGTH_FLAG = 109;

	public static final String FIXED_LENGTH_FLAG = "Fixed Length Flag";

	/**
	 * Indicates that this header binary field contains the maximum number of
	 * samples for all traces in the input file.
	 */
	public static final int HEADER_MAXIMUM_SAMPLES_PER_TRACE = 110;

	public static final String MAXIMUM_SAMPLES_PER_TRACE = "Max Samples per Trace";

	/**
	 * the number of header extension records following the binary header.
	 */
	public static final int HEADER_NUMBER_OF_EXTENSIONS = 111;
	public static final String NUMBER_OF_EXTENSIONS = "NUMBER_OF_EXTENSIONS";
	/**
	 * the trace sequence number.
	 */
	public static final int HEADER_TRACE_SEQUENCE_NUMBER = 200;

	public static final String TRACE_SEQUENCE_NUMBER = "Trace number in line";

	/**
	 * the trace sequence number in file.
	 */
	public static final int HEADER_TRACE_NUMBER_ONE = 201;

	public static final String TRACE_NUMBER_ONE = "Trace number in file";
	/**
	 * the field record.
	 */
	public static final int HEADER_FIELD_RECORD = 202;

	public static final String FIELD_RECORD = "Field record number";

	/**
	 * the field trace.
	 */
	public static final int HEADER_FIELD_TRACE = 203;

	public static final String FIELD_TRACE = "Trace number in field";

	/**
	 * the shotpoint ID.
	 */
	public static final int HEADER_SHOTPOINT_ID = 204;

	public static final String SHOTPOINT_ID = "Shotpoint number";

	/**
	 * the common depth point (CDP) number.
	 */
	public static final int HEADER_CDP_NUMBER = 205;

	public static final String CDP_NUMBER = "CDP";

	/**
	 * the common depth point (CDP) trace.
	 */
	public static final int HEADER_CDP_TRACE = 206;

	public static final String CDP_TRACE = "Trace number in CDP";

	/**
	 * the trace ID.
	 */
	public static final int HEADER_TRACE_ID = 207;

	public static final String TRACE_ID = "Trace ID";

	/**
	 * the record elevation.
	 */
	public static final int HEADER_REC_ELEVATION = 208;

	public static final String REC_ELEVATION = "Receiver elevation";

	/**
	 * the source x-location.
	 */
	public static final int HEADER_SOURCE_X_LOC = 209;

	public static final String SOURCE_X_LOC = "SRCX";

	/**
	 * the source y-location.
	 */
	public static final int HEADER_SOURCE_Y_LOC = 210;

	public static final String SOURCE_Y_LOC = "SRCY";

	/**
	 * the receiver x-location.
	 */
	public static final int HEADER_RECEIVER_X_LOC = 211;

	public static final String RECEIVER_X_LOC = "RCVX";

	/**
	 * the receiver y-location.
	 */
	public static final int HEADER_RECEIVER_Y_LOC = 212;

	public static final String RECEIVER_Y_LOC = "RCVY";

	/**
	 * the start time value.
	 */
	public static final int HEADER_START_TIME = 213;

	public static final String START_TIME = "START TIME";

	/**
	 * the number of samples in this trace.
	 */
	public static final int HEADER_SAMPLES_IN_TRACE = 214;

	public static final String SAMPLES_IN_TRACE = "SAMPLES IN TRACE";

	/**
	 * the sample rate of this trace.
	 */
	public static final int HEADER_SAMPLE_RATE = 215;

	public static final String SAMPLE_RATE = "SAMPLE RATE";

	/**
	 * the common depth point (CDP) x-location.
	 */
	public static final int HEADER_CDP_X_LOC = 216;

	public static final String CDP_X_LOC = "CDPX";

	/**
	 * the common depth point (CDP) y-location.
	 */
	public static final int HEADER_CDP_Y_LOC = 217;

	public static final String CDP_Y_LOC = "CDPY";

	/**
	 * the inline number.
	 */
	public static final int HEADER_INLINE_NUMBER = 218;

	public static final String INLINE_NUMBER = "Poststack inline";

	/**
	 * the xline number.
	 */
	public static final int HEADER_XLINE_NUMBER = 219;

	public static final String XLINE_NUMBER = "Poststack crossline";

	/**
	 * the shotpoint number.
	 */
	public static final int HEADER_SHOTPOINT_NUMBER = 220;

	public static final String SHOTPOINT_NUMBER = "Shotpoint number";

	/**
	 * the shotpoint number.
	 */
	public static final int HEADER_LOCATION_X = 221;

	public static final String LOCATION_X = "Location X";
	/**
	 * the shotpoint number.
	 */
	public static final int HEADER_LOCATION_Y = 222;

	public static final String LOCATION_Y = "Location Y";

	/**
	 * number of vertical summed trace
	 */
	public static final int HEADER_SUMMED_TRACE = 223;

	public static final String SUMMED_TRACE = "Summed traces";
	/**
	 * number of horizontal stacked trace
	 */
	public static final int HEADER_STACKED_TRACE = 224;

	public static final String STACKED_TRACE = "Stacked traces";
	/**
	 * the shotpoint number.
	 */
	public static final int HEADER_DATA_USE = 225;

	public static final String DATA_USE = "Data Use";

	/**
	 * the distance from source to receiver.
	 */
	public static final int HEADER_DISTANCE = 226;

	public static final String DISTANCE = "Distance";
	/**
	 * the elevation number.
	 */
	public static final int HEADER_SURFACE_ELEVATION = 227;

	public static final String SURFACE_ELEVATION = "Surface elevation";
	/**
	 * the DEPTH_BELOW_SURFACE number.
	 */
	public static final int HEADER_DEPTH_BELOW_SURFACE = 228;

	public static final String DEPTH_BELOW_SURFACE = "Depth below surface";
	/**
	 * the DATUM_ELEVATION number.
	 */
	public static final int HEADER_DATUM_ELEVATION = 229;

	public static final String DATUM_ELEVATION = "Datum at receiver";
	/**
	 * the DATUM_ELEVATION number.
	 */
	public static final int HEADER_DATUM_SOURCE = 230;

	public static final String DATUM_SOURCE = "Datum at source";
	/**
	 * the DATUM_ELEVATION number.
	 */
	public static final int HEADER_WATER_SOURCE = 231;

	public static final String WATER_SOURCE = "Water at source";
	/**
	 * the HEADER_WATER_GROUP number.
	 */
	public static final int HEADER_WATER_GROUP = 232;

	public static final String WATER_GROUP = "Water at group";

	/**
	 * the HEADER_WATER_GROUP number.
	 */
	public static final int HEADER_ELEVATION_SCALER = 233;

	public static final String ELEVATION_SCALER = "Elevation scaler";
	/**
	 * the Coordinate scaler number.
	 */
	public static final int HEADER_COORDINATE_SCALER = 234;

	public static final String COORDINATE_SCALER = "Coordinate scaler";

	/**
	 * the HEADER_WATER_GROUP number.
	 */
	public static final int HEADER_UNITS = 235;

	public static final String UNITS = "Coordinate units";
	/**
	 * the SHOTPOINT_SCALER number.
	 */
	public static final int HEADER_SHOTPOINT_SCALER = 236;

	public static final String SHOTPOINT_SCALER = "Shotpoint scaler";
	/**
	 * the SAMPLE_UNIT number.
	 */
	public static final int HEADER_SAMPLE_UNIT = 237;

	public static final String SAMPLE_UNIT = "Sample unit";
	/**
	 * the DEVICE_ID number.
	 */
	public static final int HEADER_DEVICE_ID = 238;

	public static final String DEVICE_ID = "Device ID";
	/**
	 * the TIME_SCALER number.
	 */
	public static final int HEADER_TIME_SCALER = 239;

	public static final String TIME_SCALER = "Time scaler";

	/**
	 * the source unit number.
	 */
	public static final int HEADER_SOURCE_UNIT = 240;

	public static final String SOURCE_UNIT = "Source units";

	/**
	 * the source unit number.
	 */
	public static final int HEADER_UNKNOWN = 241;

	public static final String UNKNOWN = "UNKNOWN";
	/**
	 * the user defined information.
	 */
	public static final int HEADER_USER_DEFINED = 1000;

	// *************Defined in the binary header 3225-3226 byte
	/**
	 * Trace sample is four bytes ibm float.
	 */
	public static final int DATA_SAMPLE_IBM_FLOAT = 1;

	/**
	 * Trace sample is four bytes standard integer.
	 */
	public static final int DATA_SAMPLE_INTEGER = 2;

	/**
	 * Trace sample is two bytes integer.
	 */
	public static final int DATA_SAMPLE_SHORT = 3;

	/**
	 * Trace sample is four bytes with gain.
	 */
	public static final int DATA_SAMPLE_FLOAT_GAIN = 4;

	/**
	 * Trace sample is four bytes ieee float.
	 */
	public static final int DATA_SAMPLE_FLOAT = 5;

	/**
	 * Trace sample is one byte.
	 */
	public static final int DATA_SAMPLE_BYTE = 8;

	/**
	 * Retrieves the list of data header descriptions.
	 *
	 * @return a list of data header descriptions.
	 */
	public Vector getDataHeaderFormats();

	/**
	 * Retrieves the list of trace header descriptions.
	 *
	 * @return a list of trace header descriptions.
	 */
	public Vector getTraceHeaderFormats();

	/**
	 * Returns the size of data headers.
	 *
	 * @return the size of data headers.
	 */
	public int getDataHeadersSize();

	/**
	 * Returns the size of trace headers.
	 *
	 * @return the size of trace headers.
	 */
	public int getTraceHeadersSize();

	// The offset values for the header names in the su trace header
	public final static int Offset_tracl = 0;

	public final static int Offset_tracr = 4;

	public final static int Offset_fldr = 8;

	public final static int Offset_tracf = 12;

	public final static int Offset_ep = 16;

	public final static int Offset_cdp = 20;

	public final static int Offset_cdpt = 24;

	public final static int Offset_trid = 28;

	public final static int Offset_nvs = 30;

	public final static int Offset_nhs = 32;

	public final static int Offset_duse = 34;

	public final static int Offset_offset = 36;

	public final static int Offset_gelev = 40;

	public final static int Offset_selev = 44;

	public final static int Offset_sdepth = 48;

	public final static int Offset_gdel = 52;

	public final static int Offset_sdel = 56;

	public final static int Offset_swdep = 60;

	public final static int Offset_gwdep = 64;

	public final static int Offset_scalel = 68;

	public final static int Offset_scalco = 70;

	public final static int Offset_sx = 72;

	public final static int Offset_sy = 76;

	public final static int Offset_gx = 80;

	public final static int Offset_gy = 84;

	public final static int Offset_counit = 88;

	public final static int Offset_wevel = 90;

	public final static int Offset_swevel = 92;

	public final static int Offset_sut = 94;

	public final static int Offset_gut = 96;

	public final static int Offset_sstat = 98;

	public final static int Offset_gstat = 100;

	public final static int Offset_tstat = 102;

	public final static int Offset_laga = 104;

	public final static int Offset_lagb = 106;

	public final static int Offset_delrt = 108;

	public final static int Offset_muts = 110;

	public final static int Offset_mute = 112;

	public final static int Offset_ns = 114;

	public final static int Offset_dt = 116;

	public final static int Offset_gain = 118;

	public final static int Offset_igc = 120;

	public final static int Offset_igi = 122;

	public final static int Offset_corr = 124;

	public final static int Offset_sfs = 126;

	public final static int Offset_sfe = 128;

	public final static int Offset_slen = 130;

	public final static int Offset_styp = 132;

	public final static int Offset_stas = 134;

	public final static int Offset_stae = 136;

	public final static int Offset_tatyp = 138;

	public final static int Offset_afilf = 140;

	public final static int Offset_afils = 142;

	public final static int Offset_nofilf = 144;

	public final static int Offset_nofils = 146;

	public final static int Offset_lcf = 148;

	public final static int Offset_hcf = 150;

	public final static int Offset_lcs = 152;

	public final static int Offset_hcs = 154;

	public final static int Offset_year = 156;

	public final static int Offset_day = 158;

	public final static int Offset_hour = 160;

	public final static int Offset_minute = 162;

	public final static int Offset_sec = 164;

	public final static int Offset_timbas = 166;

	public final static int Offset_trwf = 168;

	public final static int Offset_grnors = 170;

	public final static int Offset_grnofr = 172;

	public final static int Offset_grnlof = 174;

	public final static int Offset_gaps = 176;

	public final static int Offset_otrav = 178;

	public final static int Offset_d1 = 180;

	public final static int Offset_f1 = 184;

	public final static int Offset_d2 = 188;

	public final static int Offset_f2 = 192;

	public final static int Offset_ungpow = 196;

	public final static int Offset_unscale = 200;

	public final static int Offset_ntr = 204;

	public final static int Offset_mark = 208;

	public final static int Offset_shortpad = 210;
}
