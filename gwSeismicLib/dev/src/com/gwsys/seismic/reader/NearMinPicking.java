package com.gwsys.seismic.reader;

/**
 * Defines the snapping algorithm used by PickingTraceBehavior.
 */
public class NearMinPicking extends NearPicking implements NearTraceValuePicking {   
    /**
     * Finds the sample index closest to the index parameter which is a local
     * minimum.  If sign is 1, increasing indices are searched, otherwise
     * decreasing indices are searched.  If sign is not +/- 1 or the search
     * attempts to access an index outside of the sample array, or checks
     * more than 1 million indices while searching for a match, and exception
     * is thrown.
     * @param index the starting index of the search
     * @param samples the array of floating point values
     * @param sign the direction in which to search, must be +/- 1
     * @param snapLimit the maximum number of indices to search before returning the best match, must be >=0
     */
    protected int selectClosestIndex(int index, float[] samples, int sign, int snapLimit) {
        if ((sign != -1) && (sign != 1))
            throw new IllegalArgumentException("Skipping indices is not allowed, sign must be +/- 1");
        
        int finalIndex;
        
        if (sign == -1)
            finalIndex = 0;
        else
            finalIndex = samples.length - 1;
        
        int currentMinIndex = index;
        int searchedIndices = 0;
        for (int offset = 0; (index + offset) != finalIndex; offset += sign) {
            if (searchedIndices == snapLimit)
                    break;

            int currentIndex = index + offset;
            
            if (currentIndex < 0)
                throw new ArrayIndexOutOfBoundsException("Attempting to find index of closest minimum trace value with index <0, an infinite loop may have occurred.");
            
            if (currentIndex >= samples.length)
                throw new ArrayIndexOutOfBoundsException("Attempting to find index of closest minimum trace value with index >= sample.length, an error has occurred.");
            
            if (searchedIndices > 1000000)
                throw new RuntimeException("Attempting to search over 1 million indices for a local minimum, an infinite loop has probably occurred.");
            
            if (samples[currentIndex] < samples[currentMinIndex])
                currentMinIndex = currentIndex;
            
            searchedIndices++;
        }
        return currentMinIndex;
    }
}