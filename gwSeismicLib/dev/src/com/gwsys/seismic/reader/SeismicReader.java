//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.reader;

import com.gwsys.seismic.core.AbstractProcessor;
import com.gwsys.seismic.indexing.HeaderFieldEntry;
import com.gwsys.seismic.indexing.SegyHeaderFormat;

import java.util.Vector;

/**
 * Defines the basic methods to read seismic data and metadata.
 * 
 */
public abstract class SeismicReader extends AbstractProcessor implements
		DataReader {

	/**
	 * Default constructor
	 */
	public SeismicReader() {
	}

	/**
	 * Retrieves the description of seismic data format.
	 * 
	 * @return the description of seismic data format.
	 */
	public abstract SeismicFormat getDataFormat();

	/**
	 * Returns the list of descriptions of fields in the data header.
	 * 
	 * @return the list of HeaderFieldEntrys.
	 */
	public Vector getKeys() {
		return getListByType(HeaderFieldEntry.FIELD_TYPE_PRIMARY,
				HeaderFieldEntry.FIELD_TYPE_SECONDARY);
	}


	/**
	 * Returns the number of fields defined by user as primary key.
	 * 
	 * @return the number of fields defined by user as primary key.
	 */
	public int getPrimaryKeyCount() {

		Vector primary = getListByType(HeaderFieldEntry.FIELD_TYPE_ANY,
				HeaderFieldEntry.FIELD_TYPE_PRIMARY);

		return primary.size();
	}

	/**
	 * Returns the list of fields defined as primary key.
	 * 
	 * @return the list of fields defined as primary key.
	 * 
	 */
	public Vector getPrimaryKeys() {
		return getListByType(HeaderFieldEntry.FIELD_TYPE_ANY,
				HeaderFieldEntry.FIELD_TYPE_PRIMARY);
		
	}

	/**
	 * Returns the number of fields defined by user as secondary key.
	 * 
	 * @return the number of fields defined by user as secondary key.
	 */
	public int getSecondaryKeyCount() {
		Vector second = getSecondaryKeys();

		return second.size();
	}

	/**
	 * Returns the list of fields defined as secondary key.
	 * 
	 * @return the list of fields (HeaderFieldEntry) defined as secondary key.
	 * 
	 */
	public Vector getSecondaryKeys() {
		return getListByType(HeaderFieldEntry.FIELD_TYPE_ANY,
				HeaderFieldEntry.FIELD_TYPE_SECONDARY);
		
	}

	private Vector getListByType(int type1, int type2) {
		Vector<HeaderFieldEntry> keys = new Vector<HeaderFieldEntry>();

		Vector traceHeader = getDataFormat().getTraceHeaderFormats();

		for (int i = 0; i < traceHeader.size(); i++) {
			Vector fields = ((SegyHeaderFormat) traceHeader.get(i))
					.getHeaderFields();

			for (int j = 0; j < fields.size(); j++) {
				HeaderFieldEntry entry = (HeaderFieldEntry) fields.get(j);
				if (entry.getFieldType() == type1 
						|| entry.getFieldType() == type2 
						|| entry.getFieldType() == HeaderFieldEntry.FIELD_TYPE_ANY)
					keys.add(entry);
			}
		}

		return keys;
	}
}
