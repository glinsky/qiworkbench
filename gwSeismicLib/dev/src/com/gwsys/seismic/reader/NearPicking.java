package com.gwsys.seismic.reader;

/**
 *
 * @author folsw9
 */
public abstract class NearPicking implements NearTraceValuePicking {
    
    /** Creates a new instance of NearPicking */
    public NearPicking() {
    }
    
    /**
     * From Issue BHPVIEWER-2 (broken snap-to-min, snap-to-max)
     *
     * If an index with a suitable trace value (determined by the subclass'
     * selectClosestIndex implementation
     * can be found within a range equivalent to that of the snap-to-max
     * operation, the index of the closest such value will be selected instead
     * of the picked index. If the closest higher and lower indices are
     * equidistant, the lower index will be returnd.  If no such value can
     * be found, the selected index
     * will be returned and no snapping occurs.
     *
     * @param index
     *            The index associated with the picked sample.
     * @param samples
     *            Trace sample array
     * @return a new index.
     */
    public int calculateSnapSample(int index, float[] samples, int snapLimit) {
        if ((index < 0 ) || (index >= samples.length))
            throw new ArrayIndexOutOfBoundsException("Cannot calculateSnapSample: index is " + index + " but sample indices are 0 - " + (samples.length - 1));
        
        int closestMinLowerIndex = selectClosestIndex(index, samples, -1, snapLimit);
        int closestMinHigherIndex = selectClosestIndex(index, samples, 1, snapLimit);
        
        if ((index - closestMinLowerIndex) <= (closestMinHigherIndex - index)) {
            return closestMinLowerIndex;
        } else {
            return closestMinHigherIndex;
        }
    }
    
    /**
     * Finds the sample index closest to the index parameter which according to
     * the implementation's selection algorithm.
     *
     * If sign is 1, increasing indices are searched, otherwise
     * decreasing indices are searched.  If sign is not +/- 1 or the search
     * attempts to access an index outside of the sample array, or checks
     * more than 1 million indices while searching for a match, and exception
     * is thrown.
     */
    
    abstract protected int selectClosestIndex(int index, float[] samples, int sign, int snapLimit);
}
