//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.seismic.reader;

/**
 * Defines the Picking Algorithm used by PickingTraceBehavior.
 * 
 */
public class NearMaxPicking implements NearTraceValuePicking {

	/**
	 * Returns a new index that is the nearest maximum.
	 * 
	 * @param index
	 *            The index associated with the picked sample.
	 * @param samples
	 *            Trace sample array.
         * @param snapLimit
         *            Not used by this implementation of NearTraceValuePicking
	 * @return a new index.
	 */
	public int calculateSnapSample(int index, float[] samples, int snapLimit) {		
		//start with current index
		int maxSamples = samples.length - 1;
		int upIndex = index;
		int downIndex = index;		
		// Gos up to the first positive sample
		if ((samples[index] < 0)) {
			while (upIndex < maxSamples) {
				upIndex++;
				if (samples[upIndex] >= 0) {
					break;
				}
			}
		}
		//mark the sign of sample value with +1 and -1
		int sign = 1, orig_sign = samples[index] < 0 ? -1 : 1;
		//keep current index
		int biggerIndex = upIndex;
		float biggerValue = samples[upIndex];
		while ((upIndex < maxSamples)) {
			upIndex++;
			if ((samples[upIndex] > biggerValue)) {
				biggerValue = samples[upIndex];
				biggerIndex = upIndex;
			}
			if (samples[upIndex] * sign <= 0) {
				break;
			}
		}
		upIndex = biggerIndex;
		// look for another direction
		sign = orig_sign;
		if (orig_sign < 0) {			
			while ((downIndex > maxSamples)) {
				downIndex--;
				if (samples[downIndex] * sign <= 0) {
					break;
				}
			}
			sign = 1;
		}

		// Look for the value in reversed direction
		biggerIndex = downIndex;
		biggerValue = samples[downIndex];
		while ((downIndex > 1)) {
			downIndex--;
			if ((samples[downIndex] > biggerValue)) {
				biggerValue = samples[downIndex];
				biggerIndex = downIndex;
			}
			if (samples[downIndex] * sign <= 0) {
				break;
			}
		}
		downIndex = biggerIndex;
		if (orig_sign < 0) {
			//compare which index is close to picking index
			if (Math.abs(index - upIndex) < Math.abs(index - downIndex))
				return upIndex;
			else
				return downIndex;
		}

		if (samples[upIndex] > samples[downIndex])
			return upIndex;
		else
			return downIndex;
	}
}
