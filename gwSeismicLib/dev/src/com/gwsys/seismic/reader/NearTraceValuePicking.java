/*
 * NearTraceValuePicking.java
 *
 * Created on December 18, 2007, 3:44 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.gwsys.seismic.reader;

/**
 *
 * @author folsw9
 */
public interface NearTraceValuePicking {
    public int calculateSnapSample(int index, float[] samples, int snapLimit);
}
