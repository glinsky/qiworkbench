//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.reader;

import java.util.Vector;

import com.gwsys.seismic.indexing.HeaderFieldEntry;
import com.gwsys.seismic.indexing.SegyHeaderFormat;
import com.gwsys.seismic.indexing.SeismicConstants;
import com.gwsys.seismic.indexing.SegyBinaryEntry;

/**
 * Defines the default implementation to support the standard
 * SEG-Y tape format.
 *
 */
public class StandardSegyFormat implements SeismicFormat {

	private Vector<SegyHeaderFormat> dataHeaderFormats = new Vector<SegyHeaderFormat>();
	private Vector<SegyHeaderFormat> traceHeaderFormats = new Vector<SegyHeaderFormat>();

	/**
	 * Creates default SEG-Y format.
	 * EBCDIC header object with 3200 bytes.
	 * Binary header object with 400 bytes.
	 * Binary trace header object with 240 bytes.
	 */
	public StandardSegyFormat() {
		buildFileHeader();
		buildTraceHeader();
	}

	private void buildFileHeader(){
		SegyHeaderFormat ebcdicHeaderFormat = new SegyHeaderFormat(
				SegyHeaderFormat.EBCDIC_HEADER, 3200);

		SegyHeaderFormat binaryHeaderFormat = new SegyHeaderFormat(
				SegyHeaderFormat.BINARY_HEADER, 400);

		// Based on index of bytes of SEGY
		binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(4,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_LINE_NUMBER,
				LINE_NUMBER, HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false));

		binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(12,
				SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT, HEADER_TRACES_PER_ENSEMBLE,
				TRACES_PER_ENSEMBLE, HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false));

		binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(16,
				SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT, HEADER_SAMPLE_INTERVAL,
				SAMPLE_INTERVAL, HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false));

		binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(20,
				SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT, HEADER_SAMPLES_PER_TRACE,
				SAMPLES_PER_TRACE, HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false));

		binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(24,
				SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT, HEADER_DATA_FORMAT_CODE,
				DATA_FORMAT_CODE, HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false));

		binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(26,
				SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT,
				HEADER_TRACES_PER_CDP_ENSEMBLE, TRACES_PER_CDP_ENSEMBLE,
				HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false));

		binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(54,
				SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT, HEADER_MEASUREMENT_SYSTEM,
				MEASUREMENT_SYSTEM, HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false));

		binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(300,
				SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT, HEADER_SEGY_REVISION_NUMBER,
				SEGY_REVISION_NUMBER, HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false));

		binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(302,
				SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT, HEADER_FIXED_LENGTH_FLAG,
				FIXED_LENGTH_FLAG, HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false));

		binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(304,
				SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT,
				HEADER_MAXIMUM_SAMPLES_PER_TRACE, MAXIMUM_SAMPLES_PER_TRACE,
				HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false));

		dataHeaderFormats.addElement(ebcdicHeaderFormat);

		dataHeaderFormats.addElement(binaryHeaderFormat);

	}

	private void buildTraceHeader(){
		SegyHeaderFormat traceHeaderFormat = new SegyHeaderFormat(
				SegyHeaderFormat.BINARY_HEADER, 240);

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(0,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_TRACE_SEQUENCE_NUMBER,
				TRACE_SEQUENCE_NUMBER, HeaderFieldEntry.FIELD_TYPE_ANY, false));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(4,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_TRACE_NUMBER_ONE,
				TRACE_NUMBER_ONE, HeaderFieldEntry.FIELD_TYPE_ANY, false));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(8,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_FIELD_RECORD,
				FIELD_RECORD, HeaderFieldEntry.FIELD_TYPE_ANY, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(12,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_FIELD_TRACE, FIELD_TRACE,
				HeaderFieldEntry.FIELD_TYPE_ANY, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(16,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_SHOTPOINT_ID, SHOTPOINT_ID,
				HeaderFieldEntry.FIELD_TYPE_ANY, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(20,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_CDP_NUMBER, CDP_NUMBER,
				HeaderFieldEntry.FIELD_TYPE_PRIMARY, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(24,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_CDP_TRACE, CDP_TRACE,
				HeaderFieldEntry.FIELD_TYPE_SECONDARY, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(28,
				SeismicConstants.DATA_FORMAT_SHORT, HEADER_TRACE_ID, TRACE_ID,
				HeaderFieldEntry.FIELD_TYPE_ANY, false));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(30,
				SeismicConstants.DATA_FORMAT_SHORT, HEADER_SUMMED_TRACE, SUMMED_TRACE,
				HeaderFieldEntry.FIELD_TYPE_ANY, false));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(32,
				SeismicConstants.DATA_FORMAT_SHORT, HEADER_STACKED_TRACE, STACKED_TRACE,
				HeaderFieldEntry.FIELD_TYPE_ANY, false));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(34,
				SeismicConstants.DATA_FORMAT_SHORT, HEADER_DATA_USE,
				DATA_USE, HeaderFieldEntry.FIELD_TYPE_ANY, false));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(36,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_DISTANCE,
				DISTANCE, HeaderFieldEntry.FIELD_TYPE_ANY, true));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(40,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_REC_ELEVATION,
				REC_ELEVATION, HeaderFieldEntry.FIELD_TYPE_ANY, true));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(44,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_SURFACE_ELEVATION,
				SURFACE_ELEVATION, HeaderFieldEntry.FIELD_TYPE_ANY, true));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(48,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_DEPTH_BELOW_SURFACE,
				DEPTH_BELOW_SURFACE, HeaderFieldEntry.FIELD_TYPE_ANY, true));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(52,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_DATUM_ELEVATION,
				DATUM_ELEVATION, HeaderFieldEntry.FIELD_TYPE_ANY, true));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(56,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_DATUM_SOURCE,
				DATUM_SOURCE, HeaderFieldEntry.FIELD_TYPE_ANY, true));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(60,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_WATER_SOURCE,
				WATER_SOURCE, HeaderFieldEntry.FIELD_TYPE_ANY, true));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(64,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_WATER_GROUP,
				WATER_GROUP, HeaderFieldEntry.FIELD_TYPE_ANY, true));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(68,
				SeismicConstants.DATA_FORMAT_SHORT, HEADER_ELEVATION_SCALER,
				ELEVATION_SCALER, HeaderFieldEntry.FIELD_TYPE_ANY, true));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(70,
				SeismicConstants.DATA_FORMAT_SHORT, HEADER_COORDINATE_SCALER,
				COORDINATE_SCALER, HeaderFieldEntry.FIELD_TYPE_ANY, true));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(72,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_SOURCE_X_LOC, SOURCE_X_LOC,
				HeaderFieldEntry.FIELD_TYPE_ANY, true));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(76,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_SOURCE_Y_LOC, SOURCE_Y_LOC,
				HeaderFieldEntry.FIELD_TYPE_ANY, true));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(80,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_RECEIVER_X_LOC, RECEIVER_X_LOC,
				HeaderFieldEntry.FIELD_TYPE_ANY, true));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(84,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_RECEIVER_Y_LOC, RECEIVER_Y_LOC,
				HeaderFieldEntry.FIELD_TYPE_ANY, true));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(88,
				SeismicConstants.DATA_FORMAT_SHORT, HEADER_UNITS, UNITS,
				HeaderFieldEntry.FIELD_TYPE_ANY, true));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(108,
				SeismicConstants.DATA_FORMAT_SHORT, HEADER_START_TIME,
				START_TIME, HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(114,
				SeismicConstants.DATA_FORMAT_SHORT, HEADER_SAMPLES_IN_TRACE,
				SAMPLES_IN_TRACE, HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(116,
				SeismicConstants.DATA_FORMAT_SHORT, HEADER_SAMPLE_RATE,
				SAMPLE_RATE, HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(180,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_CDP_X_LOC, CDP_X_LOC,
				HeaderFieldEntry.FIELD_TYPE_ANY, true));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(184,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_CDP_Y_LOC, CDP_Y_LOC,
				HeaderFieldEntry.FIELD_TYPE_ANY, true));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(188,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_INLINE_NUMBER, INLINE_NUMBER,
				HeaderFieldEntry.FIELD_TYPE_ANY, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(192,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_XLINE_NUMBER, XLINE_NUMBER,
				HeaderFieldEntry.FIELD_TYPE_ANY, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(196,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_SHOTPOINT_NUMBER,
				SHOTPOINT_NUMBER, HeaderFieldEntry.FIELD_TYPE_ANY, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(200,
				SeismicConstants.DATA_FORMAT_SHORT, HEADER_SHOTPOINT_SCALER,
				SHOTPOINT_SCALER, HeaderFieldEntry.FIELD_TYPE_ANY, false));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(202,
				SeismicConstants.DATA_FORMAT_SHORT, HEADER_SAMPLE_UNIT,
				SAMPLE_UNIT, HeaderFieldEntry.FIELD_TYPE_ANY, false));
		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(212,
				SeismicConstants.DATA_FORMAT_SHORT, HEADER_DEVICE_ID,
				DEVICE_ID, HeaderFieldEntry.FIELD_TYPE_ANY, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(214,
				SeismicConstants.DATA_FORMAT_SHORT, HEADER_TIME_SCALER,
				TIME_SCALER, HeaderFieldEntry.FIELD_TYPE_ANY, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(220,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_LOCATION_X,
				LOCATION_X, HeaderFieldEntry.FIELD_TYPE_ANY, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(224,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_LOCATION_Y,
				LOCATION_Y, HeaderFieldEntry.FIELD_TYPE_ANY, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(230,
				SeismicConstants.DATA_FORMAT_SHORT, HEADER_SOURCE_UNIT,
				SOURCE_UNIT, HeaderFieldEntry.FIELD_TYPE_ANY, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(232,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_UNKNOWN,
				UNKNOWN, HeaderFieldEntry.FIELD_TYPE_ANY, false));

		traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(236,
				SeismicConstants.DATA_FORMAT_UNSIGNEDINT, HEADER_UNKNOWN,
				UNKNOWN, HeaderFieldEntry.FIELD_TYPE_ANY, false));
		traceHeaderFormats.addElement(traceHeaderFormat);
	}

	/**
	 * Gets the list of segy file headers.
	 *
	 * @return the list of segy file headers.
	 */
	public Vector getDataHeaderFormats() {
		return dataHeaderFormats;
	}

	/**
	 * Gets the list of trace binary headers.
	 *
	 * @return the list of trace binary headers.
	 */
	public Vector getTraceHeaderFormats() {
		return traceHeaderFormats;
	}

	/**
	 * Gets the size of segy file headers. (Default value is 3600)
	 * Subclass must overrid this method.
	 * @return the size of segy file headers.
	 */
	public int getDataHeadersSize() {
		return 3600;
	}

	/**
	 * Gets the size of trace headers. (DEfault value is 240)
	 * Subclass must overrid this method.
	 * @return a size of trace headers
	 */
	public int getTraceHeadersSize() {
		return 240;
	}
}
