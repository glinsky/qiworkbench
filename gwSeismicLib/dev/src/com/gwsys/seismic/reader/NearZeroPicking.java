package com.gwsys.seismic.reader;

/**
 * Defines the snapping algorithm used by PickingTraceBehavior.
 */
public class NearZeroPicking extends NearPicking implements NearTraceValuePicking {
    private static final float EPSILON = 0.000001f;
    /**
     * Finds the sample index closest to the index parameter which is a nearly 0.  
     * 
     * This is a naive implementation and if no trace value equal to 0.f exists,
     * It has the effect of selecting the index which has a value nearest to 0,
     * regardless of nearness to the picked index.
     * 
     * If sign is 1, increasing indices are searched, otherwise
     * decreasing indices are searched.  If sign is not +/- 1 or the search
     * attempts to access an index outside of the sample array, or checks
     * more than 1 million indices while searching for a match, and exception
     * is thrown.
     */
    protected int selectClosestIndex(int index, float[] samples, int sign, int snapLimit) {
        if ((sign != -1) && (sign != 1))
            throw new IllegalArgumentException("Skipping indices is not allowed, sign must be +/- 1");
        
        int finalIndex;
        
        if (sign == -1)
            finalIndex = 0; // algorithm requires a value prior to the current index,
                            // only search down to 1
        else
            finalIndex = samples.length - 1;
        
        int searchedIndices = 0;
        
        for (int offset = 1;  (index + offset) != finalIndex; offset += sign) {
            if (searchedIndices == snapLimit)
                    break;
            
            int currentIndex = index + offset;
            int prevIndex = currentIndex - sign;
            
            if (currentIndex < 0)
                throw new ArrayIndexOutOfBoundsException("Attempting to find index of closest minimum trace value with index <0, an infinite loop may have occurred.");
            
            if (currentIndex >= samples.length)
                throw new ArrayIndexOutOfBoundsException("Attempting to find index of closest minimum trace value with index >= sample.length, an error has occurred.");
            
            if (searchedIndices > 1000000)
                throw new RuntimeException("Attempting to search over 1 million indices for a local minimum, an infinite loop has probably occurred.");
            
            float currentSample = samples[currentIndex];
            if (Math.abs(currentSample) == 0.0f)
                return currentIndex; // current sample is sufficient close to 0 to be considered a crossing
            
            float prevSample = samples[prevIndex];
            
            //if the signs are different, there is a zero-crossing between these two indices
            if ((currentSample * prevSample) < 0.0f) {
                // if the current sample is smaller, it is closer to the crossing
                if (Math.abs(currentSample) < Math.abs(prevSample)) {
                    return currentIndex;
                } else {
                    //otherwise, the previous sample is closer
                    return prevIndex;
                }
            } // else keep searching for a 0-crossing
            searchedIndices++;
        }
        return index; // no zero-crossing or sample very close to zero was found, do not snap
    }
}