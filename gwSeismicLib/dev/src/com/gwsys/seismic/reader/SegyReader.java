//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.reader;

import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Map;
import java.util.Vector;

import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.seismic.indexing.SegyBinaryEntry;
import com.gwsys.seismic.indexing.SegyHeaderFormat;
import com.gwsys.seismic.indexing.SeismicConstants;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.io.SeismicMetaData;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.util.*;
//
import java.io.IOException;

/**
 * Creates a reader to read seismic data in the SEG-Y format.
 *
 */
public class SegyReader extends SeismicReader {

	private static final double DEFAULT_SAMPLE_RATE = 0.004;
	private static final double MICROSECONDS_PER_SECOND = 1000000;
	private static final int NumTrace4Calculation = 255;
	/**
	 * the limits of the data for traces and samples.
	 */
	private Bound2D dataBound;

	/** the length of the data headers. */
	private int dataHeadersLength = -1;

	/** trace header format */
	private SegyHeaderFormat headerFormat;

	/** the seismic metadata. */
	private SeismicMetaData headerInfo = new SeismicMetaData();

	/** the SEG-Y format description. */
	private SeismicFormat seismicFormat;

	private RandomAccessFile sourceFile;

	/** the byte array for the trace data buffer. */
	private byte[] traceDataBytes = null;

	/** byte array for trace header, file header */
	private byte[] traceHeaderByte, ebcdicHeaderByte;

	/** the cache for trace headers. */
	private DataCache traceHeaderCache;

	/** the length of the trace headers. */
	private int traceHeadersLength = -1;

	/**
	 * Constructs a SegyReader with given file.
	 * @param filename
	 *            The name of file.
	 *
	 * @throws FileNotFoundException
	 *             if the file does not exist.
	 */
	public SegyReader(String filename) throws FileNotFoundException {

		this(filename, new StandardSegyFormat());
	}

	/**
	 * Constructs a SegyReader with given file and customized format.
	 *
	 * @param filename
	 *            The name of file.
	 * @param segyFormat
	 *            The format of the input file.
	 *
	 * @throws FileNotFoundException
	 *             if the file does not exist.
	 */
	public SegyReader(String filename, SeismicFormat segyFormat)
			throws FileNotFoundException {

		sourceFile = new RandomAccessFile(filename, "r");
		seismicFormat = segyFormat;

		traceHeaderCache = new DataCache(100000);

		calculateValues();

		//init
		Vector traceHeaderFormats = seismicFormat.getTraceHeaderFormats();
		if (traceHeaderFormats.size() < 1) {
			throw new UnsupportedOperationException(
					"There is no trace header in this file: "+filename);
		}

		headerFormat = (SegyHeaderFormat)traceHeaderFormats.elementAt(0);

		traceHeaderByte = new byte[headerFormat.getLength()];
	}

	/**
	 * Calculates the bound of data.
	 */
	private void calculateBound() {
		try {
			int traceSize = seismicFormat.getTraceHeadersSize()
					+ headerInfo.getSamplesPerTrace() * getSampleInByte();
			long numberOfTraces = (sourceFile.length() - seismicFormat.getDataHeadersSize())
					/ traceSize;

			headerInfo.setNumberOfTraces((int)numberOfTraces);

			dataBound = new Bound2D(0, 0, headerInfo.getNumberOfTraces() - 1,
					(headerInfo.getSamplesPerTrace() - 1)* headerInfo.getSampleRate());
		} catch (IOException e) {
			dataBound = new Bound2D(0, 0, 1,1);
		}
	}

	/**
	 * Calculates amplitude minimum, maximum, average, RMS for the
	 * seismic dataset.
	 */
	private void calculateValues() {

		Bound2D bound = getModelLimits();
		int increment = (int) ((bound.getMaxX() - bound.getMinX())
				/ NumTrace4Calculation);
		if (increment < 1)
			increment = 1;

		int startTrace = (int) bound.getMinX();
		int endTrace = (int) bound.getMaxX();

		int startSample = (int) (bound.getMinY() / headerInfo.getSampleRate());
		int endSample = (int) (bound.getMaxY() / headerInfo.getSampleRate());

		TraceData traceData = new TraceData();
		traceData.setSamples(new float[getMetaData().getSamplesPerTrace()]);

		NumberRange sampleRange = new NumberRange(startSample, endSample);

		float minimum = Float.MAX_VALUE;
		float maximum = Float.MIN_VALUE;
		double sumAbs = 0;
		double sumSqr = 0;
		int total = 0, i, j;
		float[] samples = null;
		for (i = startTrace; i <= endTrace; i += increment) {
			//read one trace
			process(i, sampleRange, traceData);
			samples = traceData.getSamples();
			//compare values.
			for (j = 0; j < samples.length; j++) {

				if (samples[j] < minimum)
					minimum = samples[j];
				else if (samples[j] > maximum)
					maximum = samples[j];

				sumAbs += Math.abs(samples[j]);

				sumSqr += samples[j] * samples[j];
			}

			total += samples.length;
		}

		if (total == 0)
			headerInfo.setDataStatistics(minimum, maximum, 0, 0);
		else
			headerInfo.setDataStatistics(minimum, maximum,
					sumAbs / total, Math.sqrt(sumSqr / total));
	}

	/**
	 * Converts a byte array into trace header object.
	 * @param traceId
	 *            the unique trace identifier.
	 * @param header
	 *            the trace header byte.
	 * @param headerFields
	 *            the field definition.
	 *
	 * @return TraceHeader.
	 */
	private TraceHeader createTraceHeader(int traceId, byte[] header,
			Vector headerFields) {

		Integer numberOfSamples = parseFieldAsInt(
				SeismicFormat.HEADER_SAMPLES_IN_TRACE,
				header, headerFields);
		int numSamples = 0;
		if (numberOfSamples != null)
			numSamples = numberOfSamples.intValue();
		Integer startValue = parseFieldAsInt(
				SeismicFormat.HEADER_START_TIME,
				header, headerFields);
		double startInTime = 0.0;
		if (startValue != null)
			startInTime=startValue.intValue() / MICROSECONDS_PER_SECOND;

		TraceHeader traceMetaData = new TraceHeader(traceId,
				(double) traceId,
				startInTime, numSamples,
				SeismicConstants.NORMAL_TRACE);

		parseHeaderFields(header, headerFields,
				traceMetaData.getDataValues(), traceMetaData.getHeaderList());

		return traceMetaData;
	}

	/**
	 * For dialog.
	 * @return
	 */
	public String[] ebcdicBytesToStringArray() {
		return SeismicByteFactory.ebcdicBytesToStringArray(ebcdicHeaderByte);
	}

	/**
	 * @inheritDoc
	 */
	public SeismicFormat getDataFormat() {
		return seismicFormat;
	}

	/**
	 * Returns the entire header length in bytes.
	 *
	 * @return the entire header length in bytes.
	 */
	private int getDataHeadersLength() {

		if (dataHeadersLength == -1) {
			dataHeadersLength = 0;

			Vector header = seismicFormat.getDataHeaderFormats();
			Enumeration headerEnum = header.elements();

			while (headerEnum.hasMoreElements()) {
				SegyHeaderFormat headerFormat =
					(SegyHeaderFormat) headerEnum.nextElement();
				dataHeadersLength += headerFormat.getLength();
			}
		}

		return dataHeadersLength;
	}

	/**
	 * @inheritDoc
	 */
	public SeismicMetaData getMetaData() {

		if (headerInfo == null) {
			headerInfo = new SeismicMetaData();
			return readMetaData();
		} else
			return headerInfo;
	}

	/**
	 * @inheritDoc
	 */
	public Bound2D getModelLimits() {

		if (dataBound == null) {
			readMetaData();
			calculateBound();
		}

		return dataBound;
	}

	/**
	 * Get the length of a trace sample in bytes.
	 *
	 * @return the length of a trace sample in bytes.
	 */
	private int getSampleInByte() {

		int sampleLength;
		switch (headerInfo.getDataFormat()) {
			case SeismicFormat.DATA_SAMPLE_BYTE:
			sampleLength = 1;
			break;

			case SeismicFormat.DATA_SAMPLE_SHORT:
			sampleLength = 2;
			break;

			case SeismicFormat.DATA_SAMPLE_IBM_FLOAT:
			case SeismicFormat.DATA_SAMPLE_FLOAT:
			case SeismicFormat.DATA_SAMPLE_INTEGER:
			sampleLength = 4;
			break;
			default:
			throw new UnsupportedOperationException(
					"The sample code is not supported in this version.");
		}
		return sampleLength;
	}

	/**
	 * Returns the entire length in bytes.
	 *
	 * @return the entire length in bytes.
	 */
	private int getTraceHeadersLength() {

		if (traceHeadersLength == -1) {
			traceHeadersLength = 0;

			Vector header = seismicFormat.getTraceHeaderFormats();
			Enumeration headerEnum = header.elements();

			while (headerEnum.hasMoreElements()) {
				SegyHeaderFormat headerFormat =
					(SegyHeaderFormat) headerEnum.nextElement();
				traceHeadersLength += headerFormat.getLength();
			}
		}

		return traceHeadersLength;
	}

	private int getTraceLength() {

		return (getTraceHeadersLength()
				+ (headerInfo.getSamplesPerTrace() * getSampleInByte()));
	}



	/**
	 * Retrieves trace header for specified trace index.
	 * @param traceId
	 *            the ID of trace.
	 *
	 * @return TraceHeader.
	 */
	public TraceHeader getTraceMetaData(int traceId) {

		TraceHeader traceHeader = (TraceHeader) traceHeaderCache
		.getObject(new Integer(traceId));
		if (traceHeader == null){
			try {
				sourceFile.seek(getDataHeadersLength()
						+ (traceId * getTraceLength()));
				sourceFile.readFully(traceHeaderByte);
			} catch (IOException ie) {
				//ie.printStackTrace();
				return traceHeader;
			}

			traceHeader =  createTraceHeader(traceId, traceHeaderByte,
					headerFormat.getHeaderFields());

			if (traceHeader != null)
				traceHeaderCache.push(traceHeader);
		}

		return traceHeader;
	}


	/**
	 * Parses a given field offset from a binary header.
	 *
	 * @param fieldOffset
	 *            the constant indicating the offset value.
	 * @param header
	 *            the header bytes.
	 * @param headerFields
	 *            the fields definition.
	 *
	 * @return integer value.
	 */
	private Integer parseFieldAsInt(int fieldOffset, byte[] header,
			Vector headerFields) {

		Integer result = null;
		Enumeration fieldEnum = headerFields.elements();
		while ( fieldEnum.hasMoreElements()) {

			SegyBinaryEntry entry = (SegyBinaryEntry) fieldEnum.nextElement();

			if (entry.getFieldId() == fieldOffset) {

				result = new Integer(SeismicByteFactory.toNumber(
						entry.getValueType(), header, entry.getByteOffset())
						.intValue());
				break;
			}
		}

		return result;
	}

	/**
	 * Parses the header fields into Map.
	 *
	 * @param header
	 *            the binary header bytes.
	 * @param headerFields
	 *            the field definition.
	 * @param dataValues
	 *            the output hashtable.
	 */
	@SuppressWarnings("unchecked")
	private void parseHeaderFields(byte[] header, Vector headerFields,
			Map dataValues, ArrayList byteTable) {
		Enumeration enums = headerFields.elements();
		while ( enums.hasMoreElements()) {
			SegyBinaryEntry field = (SegyBinaryEntry) enums.nextElement();
			Number value=SeismicByteFactory.toNumber(field.getValueType(),
					header, field.getByteOffset());
			dataValues.put(new Integer(field.getFieldId()), value);
			//compatible with old version
			if (byteTable!=null)
				byteTable.add(new Object[]{field.getName(), field.getByteOffset(), value});
		}
	}

	/**
	 * Reads the trace data for the specified trace index and
	 * specified range of samples.
	 *
	 * @inheritDoc
	 */
	public boolean process(int traceId, NumberRange sampleRange,
			TraceData dataTraceOut) {
		int sampleLength = getSampleInByte();
		int sampleStart = (sampleRange == null) ? 0 : sampleRange.getMin()
				.intValue();
		if (sampleStart < 0)
			sampleStart = 0;
		int traceOffset = sampleStart * sampleLength;
		long fileSeekOffset = getDataHeadersLength() + traceId
				* (long) getTraceLength() + getTraceHeadersLength()
				+ traceOffset;
		int numSamples;
		try {
			sourceFile.seek(fileSeekOffset);

			if (sampleRange == null)
			numSamples = headerInfo.getSamplesPerTrace();
			else
			numSamples = sampleRange.getMax().intValue()
					- sampleRange.getMin().intValue() + 1;

			if (numSamples > headerInfo.getSamplesPerTrace() - sampleStart)
			numSamples = headerInfo.getSamplesPerTrace() - sampleStart;

			int numBytes = numSamples * sampleLength;

			if ((traceDataBytes == null)
					|| (traceDataBytes.length < numBytes))
				traceDataBytes = new byte[numBytes];

			sourceFile.readFully(traceDataBytes, 0, numBytes);
		} catch (IOException e) {
			return false;
		}

		dataTraceOut.setNumAppliedSamples(numSamples);
		float[] dataArray = dataTraceOut.getSamples();

		TraceSampleFactory.toFloatArray(headerInfo.getDataFormat(),
				traceDataBytes, dataArray, numSamples);

		dataTraceOut.setUniqueKey(new Integer(traceId));

		return true;
	}

	/**
	 * Reads the seismic metadata from the input file.
	 *
	 * @return SeismicMetaData
	 */
	private SeismicMetaData readMetaData() {

		try {
			sourceFile.seek(0);
		} catch (IOException ie) {
			return null;
		}

		Vector dataHeaderFormats = seismicFormat.getDataHeaderFormats();
		//there are two headers in this format
		if (dataHeaderFormats.size()<2)
			return null;

		SegyHeaderFormat ebcdheaderFormat = (SegyHeaderFormat)
		dataHeaderFormats.get(0);

		ebcdicHeaderByte = new byte[ebcdheaderFormat.getLength()];
		try {
			sourceFile.readFully(ebcdicHeaderByte);
		} catch (IOException ie) {
			return null;
		}

		if (headerInfo == null)
				headerInfo = new SeismicMetaData();

		SegyHeaderFormat headerFormat = (SegyHeaderFormat)
		dataHeaderFormats.get(1);
		byte[] header = new byte[headerFormat.getLength()];
		try {
			sourceFile.readFully(header);
		} catch (IOException ie) {
			return null;
		}
		Vector headerFields = headerFormat.getHeaderFields();

		parseHeaderFields(header, headerFields, headerInfo.getHeaderValues(), null);
		// check fixed length.
		Integer fixedLengthFlag = parseFieldAsInt(
				SeismicFormat.HEADER_FIXED_LENGTH_FLAG, header, headerFields);
		boolean fixedNumSamples = true;
		if (fixedLengthFlag == null || fixedLengthFlag.intValue() == 0)
			fixedNumSamples = false;
		headerInfo.setConstantSamplesPerTrace(fixedNumSamples);
		// check the number of samples per trace.
		Integer samplesPerTrace = parseFieldAsInt(
				SeismicFormat.HEADER_SAMPLES_PER_TRACE, header, headerFields);
		int numSamples = -1;
		if (samplesPerTrace != null)
			numSamples = samplesPerTrace.intValue();
		headerInfo.setSamplesPerTrace(numSamples);
		// check the sample rate.
		Integer sampleRate = parseFieldAsInt(
				SeismicFormat.HEADER_SAMPLE_INTERVAL, header, headerFields);
		double sampleStep = DEFAULT_SAMPLE_RATE;
		if (sampleRate != null) {
			if (sampleRate.intValue() > MICROSECONDS_PER_SECOND) {
				headerInfo.setSampleUnits(SeismicConstants.UNIT_DEPTH_FEET);
				sampleStep = sampleRate.doubleValue();
			} else
				sampleStep = sampleRate.doubleValue() / MICROSECONDS_PER_SECOND;
		}
		headerInfo.setSampleRate(sampleStep);
		Integer sampleDataFormat = parseFieldAsInt(
				SeismicFormat.HEADER_DATA_FORMAT_CODE, header, headerFields);
		int dataFormatCode = 1;
		if (sampleDataFormat != null)
			dataFormatCode = sampleDataFormat.intValue();
		headerInfo.setDataFormat(dataFormatCode);

		return headerInfo;
	}
}
