//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.gui;

import com.gwsys.seismic.core.SeismicWorkflow;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.ParseException;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SpringLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.Box;

/**
 * GUI panel to update the rasterized type , color map and space value.
 */
public class RasterizePanel extends ControlPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3617570492462936370L;

	private SeismicWorkflow workflow = null;

	private int numTypes = 5;

	/** Declares an array of plot type names */
	private static String[] _plotTypes = { ("WIGGLE_TRACE"), ("POSITIVE_FILL"),
			("NEGATIVE_FILL"), ("VARIABLE_DENSITY"), ("INTERPOLATED_DENSITY") };

	private int renderingType, oldRenderingType;

	private DecimalFormat decimalFormat = new DecimalFormat();
	
	private JCheckBox[] availableTypes;

	private JTextField clipField, thresholdField;

	private float oldClipValue, oldThresholdValue;

	private JButton colorButton;

	private ColorActionListener colorListener;

	/**
	 * Creates new rasterizer panel.
	 *
	 * @param workflow seismic pipeline to be changed
	 */
	public RasterizePanel(SeismicWorkflow workflow) {
		super();

		this.workflow = workflow;

		JPanel typePanel = new JPanel();
		typePanel.setLayout(new BoxLayout(typePanel, BoxLayout.Y_AXIS));
		typePanel.setBorder(BorderFactory.createTitledBorder(("RASTERIZING TYPE")));

		availableTypes = new JCheckBox[numTypes];
		CheckBoxListener l = new CheckBoxListener();

		for (int i = 0; i < numTypes; i++) {
			availableTypes[i] = new JCheckBox(_plotTypes[i], false);
			typePanel.add(Box.createVerticalStrut(5));
			typePanel.add(availableTypes[i]);
			availableTypes[i].addActionListener(l);
		}

		typePanel.add(Box.createGlue());

		JPanel panel1 = new JPanel();
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.X_AXIS));
		panel1.add(Box.createGlue());
		panel1.add(typePanel);
		panel1.add(Box.createGlue());
		
		//Clipping Panel

		JPanel clippingPanel = new JPanel(new SpringLayout());

		clippingPanel.add(new JLabel(("CLIPPING_FACTOR")));

		clipField = new JTextField(10);
		clipField.setMaximumSize(new Dimension(400, 30));
		clippingPanel.add(clipField);

		thresholdField = new JTextField(10);
		thresholdField.setMaximumSize(new Dimension(400, 30));
		clippingPanel.add(new JLabel(("DECIMATION_SPACING")));

		clippingPanel.add(thresholdField);
		
		SpringUtilities.makeCompactGrid(clippingPanel, 2, 2, 20, 20, 20, 20);
		//Colormap Button Panel
		colorButton = new JButton(("COLOR_MAP"));
		colorListener = new ColorActionListener(workflow, this);
		colorButton.addActionListener(colorListener);

		JPanel colorPanel = new JPanel();
		colorPanel.setLayout(new BoxLayout(colorPanel, BoxLayout.X_AXIS));
		colorPanel.add(Box.createGlue());
		colorPanel.add(colorButton);
		colorPanel.add(Box.createGlue());

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		add(Box.createVerticalStrut(10));
		add(panel1);
		add(Box.createVerticalStrut(10));
		add(clippingPanel);
		add(Box.createVerticalStrut(10));
		add(colorPanel);
		add(Box.createVerticalStrut(10));
	}

	/**
	 * Initializes the parameter values from the workflow.
	 */
	public void initializeUIValues() {
		if (workflow != null) {
			Float clip = new Float(workflow.getTraceRasterizer().getClippingValue());
			clipField.setText(decimalFormat.format(clip));
			oldClipValue = clip.floatValue();

			Float dec = new Float(workflow.getTraceRasterizer().getMinSpacing());
			thresholdField.setText(decimalFormat.format(dec));
			oldThresholdValue = dec.floatValue();

			renderingType = workflow.getTraceRasterizer().getPlotType();
			setCheckBoxes(renderingType);
			oldRenderingType = renderingType;

		}
	}

	/**
	 * Commit the inputed values into rendering process.
	 * @return status of commiting process. 
	 */
	public boolean commitUIValues() {

		boolean isChange = false;

		// check Plot type
		if (renderingType != oldRenderingType) {
			workflow.getTraceRasterizer().setPlotType(renderingType);
			oldRenderingType = renderingType;
			isChange = true;
		}

		// check Clipping value
		try {
			Number inputed = decimalFormat.parse(clipField.getText());
			if (inputed.floatValue() != oldClipValue) {
				workflow.getTraceRasterizer().setClippingValue(inputed.floatValue());
				oldClipValue = inputed.floatValue();
				isChange = true;
			}
		
			inputed = decimalFormat.parse(thresholdField.getText());
			if (inputed.floatValue() != oldThresholdValue) {
				workflow.getTraceRasterizer().setMinSpacing(inputed.floatValue());
				oldThresholdValue = inputed.floatValue();
				isChange = true;
			}
		} catch (ParseException nex) {
			//ignore
		}

		return isChange;
	}

	/**
	 * Sets the plot type to the given value. This value can be
	 * combination of plot types.</p>
	 *
	 * @param type the new plot type
	 */
	private void setCheckBoxes(int type) {

		for (int j = 0; j < numTypes; j++) {
			if (availableTypes[j].isSelected())
				availableTypes[j].setSelected(false);
		}

		for (int j = 0; j < numTypes; j++) {
			if ((renderingType & (1 << j)) != 0)
				availableTypes[j].setSelected(true);
		}
	}

	/**
	 * Adds a listener for the color map button. 
	 *
	 * @param e ActionListener to be used.
	 */
	public void setColorMapButtonListener(ActionListener e) {
		colorButton.removeActionListener(colorListener);
		colorButton.addActionListener(e);
	}

	private class CheckBoxListener implements ActionListener {

		public void actionPerformed(ActionEvent event) {

			if (!(event.getSource() instanceof JCheckBox))
				return;
			String label = ((JCheckBox) event.getSource()).getText();
			//check which box is selected
			int i = 0;
			for (i = 0; i < numTypes; i++) {
				if (label.equals(_plotTypes[i])) {
					break;
				}
			}

			if (availableTypes[i].isSelected()) {
				switch (i) {
				case 1:
				case 2:
					availableTypes[4].setSelected(false);
					availableTypes[3].setSelected(false);
					break;
				case 3:
					availableTypes[4].setSelected(false);
					break;
				case 4:
					availableTypes[3].setSelected(false);
					break;
				}
			} else {
				for (i = 0; i < numTypes; i++)
					if (availableTypes[i].isSelected())
						break;
				//if nothing is selected, selected wiggle as default
				if (i >= numTypes) {
					availableTypes[0].setSelected(true);
					availableTypes[1].setSelected(true);
				}
			}
			// check wiggle type
			boolean wiggleSelected = false;
			if ((availableTypes[3].isSelected() || availableTypes[4].isSelected())) {
				for (i = 0; i <= 2; i++) {
					if (availableTypes[i].isSelected())
						wiggleSelected = true;
				}
			} else
				wiggleSelected = true;
			
			clipField.setEnabled(wiggleSelected);

			thresholdField.setEnabled(wiggleSelected);
			//add all selected types together
			renderingType = 0;
			for (i = 0; i < numTypes; i++) {
				if (availableTypes[i].isSelected()) {
					renderingType = renderingType | (1 << i);
				}
			}
		}
	}

}
