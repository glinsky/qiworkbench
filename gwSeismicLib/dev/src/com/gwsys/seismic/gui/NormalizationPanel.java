//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import com.gwsys.seismic.core.TraceNormalizer;
import com.gwsys.seismic.core.SeismicWorkflow;

/**
 * GUI panel to control the normalization.
 */
public class NormalizationPanel extends ControlPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3617010858208080692L;

	private SeismicWorkflow workflow = null;

	private DecimalFormat decimalFormat = new DecimalFormat();
	
	private boolean isChanged = false;

	private float _oldScale;

	private float _oldMinLimit;

	private float _oldMaxLimit;

	private int normTypeId, oldNormTypeId;

	private JComboBox availableModes;

	private JTextField scaleField, minField, maxField;

	/**
	 * Creates new Normalization panel.
	 *
	 * @param pipeline seismic pipeline to be changed
	 */
	public NormalizationPanel(SeismicWorkflow pipeline) {
		super();

		workflow = pipeline;

		initGUI();
	}

	/**
	 * Actualy creates Normalization panel GUI.
	 */
	private void initGUI() {

		JPanel panel = new JPanel(new SpringLayout());
		panel.setBorder(BorderFactory.createTitledBorder(("NORMALIZATION")));
		
		// Create scale field
		scaleField = new JTextField(10);
		scaleField.setMaximumSize(new Dimension(400,30));
		panel.add(new JLabel(("SCALE")));
		panel.add(scaleField);

		// Create normalization type combo box
		availableModes = new JComboBox();
		availableModes.setMaximumSize(new Dimension(400,30));
		availableModes.setEditable(false);
		availableModes.addItem(("MAXIMUM"));
		availableModes.addItem(("TRACE_MAXIMUM"));
		availableModes.addItem(("AVERAGE"));
		availableModes.addItem(("TRACE_AVERAGE"));
		availableModes.addItem(("RMS"));
		availableModes.addItem(("TRACE_RMS"));
		availableModes.addItem(("LIMITS"));

		panel.add(new JLabel(("TYPE")));
		panel.add(availableModes);

		availableModes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox) e.getSource();

				boolean enable = false;
				if (cb.getSelectedIndex() == TraceNormalizer.AMPLITUDE_LIMITS)
					enable = true;

				minField.setEnabled(enable);
				maxField.setEnabled(enable);
			}
		});

		// Create min, max field
		minField = new JTextField(10);
		maxField = new JTextField(10);
		minField.setMaximumSize(new Dimension(400,30));
		maxField.setMaximumSize(new Dimension(400,30));
		if (workflow.getInterpretation().getTraceNormalization()
				.getNormalizationMode() != TraceNormalizer.AMPLITUDE_LIMITS) {
			minField.setEnabled(false);
			
			maxField.setEnabled(false);
		}
		panel.add(new JLabel(("MIN_VALUE")));
		panel.add(minField);

		panel.add(new JLabel(("MAX_VALUE")));
		panel.add(maxField);

		SpringUtilities.makeCompactGrid(panel, 4, 2, 15, 15, 15, 15);
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		add(panel);
	}

	/**
	 * Obtains the parameter values from the pipeline, initializes documents
	 * corresponding to fields and stores these values in "old value" variables
	 * to track changes in the future.</p>
	 * @see com.gwsys.seismic.gui.ControlPanel ControlPanel
	 */
	public void initializeUIValues() {

		if (workflow != null) {
			// For normalization process
			Float scale = new Float(workflow.getInterpretation()
					.getTraceNormalization().getScale());
			scaleField.setText(decimalFormat.format(scale));
			_oldScale = scale.floatValue();

			normTypeId = workflow.getInterpretation().getTraceNormalization()
					.getNormalizationMode();
			availableModes.setSelectedIndex(normTypeId);
			oldNormTypeId = normTypeId;

			Float minLimit = new Float((workflow.getInterpretation()
					.getTraceNormalization()).getNormLimitMin());
			minField.setText(decimalFormat.format(minLimit));
			_oldMinLimit = minLimit.floatValue();

			Float maxLimit = new Float((workflow.getInterpretation()
					.getTraceNormalization()).getNormLimitMax());
			maxField.setText(decimalFormat.format(maxLimit));
			_oldMaxLimit = maxLimit.floatValue();

		}
	}

	/**
	 * Commit the inputed values into rendering process.
	 * @return status of commiting process. 
	 */
	public boolean commitUIValues() {
		isChanged = false;
		//check scale value
		try {
			Number f = decimalFormat.parse(scaleField.getText());
			if (f.floatValue() != _oldScale) {
				workflow.getInterpretation().getTraceNormalization().setScale(
						f.floatValue());
				workflow.invalidate(workflow.getInterpretation()
						.getTraceNormalization());
				_oldScale = f.floatValue();
				isChanged = true;
			}
		} catch (ParseException nex) {
			//ignore 
		}

		//check type
		normTypeId = availableModes.getSelectedIndex();
		if (normTypeId != oldNormTypeId) {
			workflow.getInterpretation().getTraceNormalization()
					.setNormalizationMode(normTypeId);
			workflow.invalidate(workflow.getInterpretation()
					.getTraceNormalization());
			oldNormTypeId = normTypeId;
			isChanged = true;
		}

		// Min & max limits
		if (normTypeId == TraceNormalizer.AMPLITUDE_LIMITS) {
			try {
				//	        Float min = new Float( _normMinDocument.getText( 0, _normMinDocument.getLength() ));
				Number min = decimalFormat.parse(minField.getText());
				//	        Float max = new Float( _normMaxDocument.getText( 0, _normMaxDocument.getLength() ));
				Number max = decimalFormat.parse(maxField.getText());
				if ((min.floatValue() != _oldMinLimit)
						|| (max.floatValue() != _oldMaxLimit)) {
					workflow.getInterpretation().getTraceNormalization()
							.setNormalizationLimits(min.floatValue(),
									max.floatValue());
					workflow.invalidate(workflow.getInterpretation()
							.getTraceNormalization());
					_oldMinLimit = min.floatValue();
					_oldMaxLimit = max.floatValue();
					isChanged = true;
				}
			} catch (ParseException nex) {
				//ignore
			}

		}

		return isChanged;
	}
	
}
