///
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.gwsys.seismic.indexing.util.XMLFactory;
import com.gwsys.seismic.reader.SegyReader;

/**
 * Show trace header in the table.
 * @author Louis.
 *
 */
public class TraceHeaderDialog extends JDialog implements ChangeListener{
	private static final long serialVersionUID = 1L;

	private int totalTraces, orderOfTrace=0;
	private SegyReader reader;
	private TraceHeaderTable jtable;
	private String savedFormatFile;
	/**
	 * Creates the dialog.
	 * @param fullFileName
	 */
	public TraceHeaderDialog(String fullFileName){
		super();
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		try {
			reader = new SegyReader(fullFileName);
			totalTraces = reader.getMetaData().getNumberOfTraces();
			String name=fullFileName.substring(fullFileName.lastIndexOf(File.separator)+1);
			fillInfoPanel(name);
			addNorthButtons();
			fillDataInTable(reader.getTraceMetaData(orderOfTrace).getHeaderList());
			addSouthButtons();
			setTitle("Trace Headers of " + name);
			setModal(true);
			setSize(350, 500);
			this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("Wrong file name!");
		}

	}

	private void fillInfoPanel(String name){
		JPanel forInfo = new JPanel(new GridLayout(4, 1, 5, 5));
		JLabel info = new JLabel(totalTraces + " traces");
		info.setForeground(Color.blue);
		forInfo.add(info);
		info = new JLabel(reader.getMetaData().getSamplesPerTrace() + " samples per trace");
		info.setForeground(Color.blue);
		forInfo.add(info);
		info = new JLabel("Sample format: "+reader.getMetaData().getSampleFormatAsString());
		info.setForeground(Color.blue);
		forInfo.add(info);
		info = new JLabel("Byte order in Big endian");
		info.setForeground(Color.blue);
		forInfo.add(info);
		forInfo.setBorder(BorderFactory.createTitledBorder(name));
		getContentPane().add(forInfo);
	}

	private void fillDataInTable(List table){

		int size = table.size();
		//Arrays.sort(keys);
		if (jtable==null){

			Object[] treeHeader = new Object[]{"Items", "Byte Location", "Values"};

			Object[][] content = new Object[size][];
			for (int i=0; i<size; i++){

				content[i]=(Object[])table.get(i);
			}
			jtable = new TraceHeaderTable(treeHeader, content);
			JPanel forTable = new JPanel(new BorderLayout());
			forTable.setBorder(BorderFactory.createTitledBorder("Description"));
			forTable.add(new JScrollPane(jtable));
			getContentPane().add(forTable);
		}else{
			for (int i=0; i<size; i++){
				jtable.setValueAt(((Object[])table.get(i))[2], i, 2);
			}
		}

	}

	private void addSouthButtons(){
		JPanel sourth = new JPanel(new FlowLayout(FlowLayout.TRAILING));
		JButton close = new JButton("Close");
		close.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				reader = null;
				jtable = null;
				dispose();
			}
		});

		JButton save = new JButton("Save format descriptor...");
		save.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				saveFormat();
			}

		});
		sourth.add(save);
		sourth.add(close);
		getContentPane().add(sourth);
	}

	private void addNorthButtons(){
		//north panel
		final SpinnerNumberModel model=new SpinnerNumberModel(1, 1, totalTraces, 1);
		JPanel north = new JPanel(new GridLayout(1, 4, 8, 5));
		north.setBorder(BorderFactory.createTitledBorder("Order of Traces"));
		JButton first = new JButton("First");
		first.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {

				orderOfTrace=0;
				updateTable();
				setTitle("First Trace Header");
				model.setValue(orderOfTrace+1);
			}

		});
		JButton last = new JButton("Last");
		last.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {

				orderOfTrace=totalTraces-1;
				updateTable();
				setTitle("Last Trace Header");
				model.setValue(totalTraces);
			}

		});
		JButton next = new JButton("Next");
		next.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				//if reach last one, set zero
				orderOfTrace++;
				if (orderOfTrace>=totalTraces)
					orderOfTrace=0;
				updateTable();
				setTitle("Trace "+orderOfTrace);
				model.setValue(orderOfTrace+1);
			}

		});

		model.addChangeListener(this);
		JSpinner spin = new JSpinner(model);
		spin.setToolTipText("Total "+totalTraces+" traces");
		north.add(first);
		north.add(next);
		north.add(last);
		north.add(spin);
		getContentPane().add(north);
	}

	private void saveFormat(){
		int[] rows = jtable.getSelectedRows();
		if (rows.length<2){

			return;
		}
		String firstkey = jtable.getValueAt(rows[0], 1).toString();
		String secondkey = jtable.getValueAt(rows[1], 1).toString();
		JFileChooser fc = new JFileChooser();
		fc.setSelectedFile(new File("SegyFormat.xml"));
		int retVal = fc.showSaveDialog(this);
		if (retVal == JFileChooser.APPROVE_OPTION){
			File file = fc.getSelectedFile();
			savedFormatFile = file.getAbsolutePath();
			XMLFactory.createSegyFormat(file, firstkey, secondkey );
		}
	}

	private void updateTable(){
		if (orderOfTrace>=totalTraces)
			return;
		fillDataInTable(reader.getTraceMetaData(orderOfTrace)
				.getHeaderList());

	}
	public void stateChanged(ChangeEvent arg0) {
		if (arg0.getSource() instanceof SpinnerNumberModel){
			int numTrace=((Number)((SpinnerNumberModel)arg0.getSource())
					.getValue()).intValue();
			orderOfTrace=numTrace-1;
			updateTable();
			setTitle("Trace "+numTrace);
		}
	}

	public String getSavedFormatFile(){
		return savedFormatFile;
	}
}
