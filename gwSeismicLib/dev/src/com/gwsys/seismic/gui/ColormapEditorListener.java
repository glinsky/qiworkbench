//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.gui;

import java.awt.Color;

import com.gwsys.seismic.core.SeismicWorkflow;
import com.gwsys.seismic.util.SeismicColorMap;

import com.gwsys.util.colorbar.ColorBar;
import com.gwsys.util.colorbar.ColorChangeListener;
import com.gwsys.util.colorbar.ColorChangedEvent;

/**
 * Creates colormap editor listener for implementing ColorChangeListener.
 * 
 */
public class ColormapEditorListener implements ColorChangeListener {

	private SeismicWorkflow _workflow;

	public ColormapEditorListener(SeismicWorkflow workflow) {
		_workflow = workflow;
	}

	public void colormapChanged(ColorChangedEvent event) {
		ColorBar cbar = event.getColorBar();
		SeismicColorMap cmap = _workflow.getTraceRasterizer().getColorMap();
		cmap.setNotify(false);

		Color[] userColors = cbar.getColorArray();
		cmap.setDensityColorSize(userColors.length);

		int rsize = cmap.getDensityColorSize();

		for (int i = 0; i < rsize; i++) {
			if (i == rsize - 1)
				cmap.setColor(i, userColors[i], true);
			else
				cmap.setColor(i, userColors[i], false);

			cmap.setInterpolationPoint(i, false);
		}
		cmap.setNotify(true);

	}
}
