//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.gui;

import java.awt.GridLayout;
import java.awt.Dimension;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.Box;

import com.gwsys.seismic.core.DataChooser;
import com.gwsys.seismic.core.SeismicWorkflow;
import com.gwsys.seismic.indexing.HeaderFieldEntry;
import com.gwsys.seismic.indexing.SeismicConstants;

/**
 * Data Viewport Control Panel.
 * @author Team
 */
public class DataViewportPanel extends ControlPanel {

    private static final long serialVersionUID = 3257853181659592758L;

    private SeismicWorkflow workflow = null;

    private int oldFirstKey, oldSecondKey;

    private float firstKeyStart, firstKeyEnd, firstKeyStep;

    private boolean useGapValue,  lastGApStatus;

    private int oldGapValue;

    private float secondKeyStart, secondKeyEnd, secondKeyStep;

    private float lastSampleStart, lastSampleEnd;

    private JComboBox _primaryField;

    private JTextField _primaryStart, _primaryEnd, _primaryStep;

    private JCheckBox _applyGapsBox;

    private JTextField _gapSize;

    private JComboBox _secondaryField;

    private JTextField _secondaryStart, _secondaryEnd, _secondaryStep;

    private JTextField _sampleStart, _sampleEnd;

    private DecimalFormat decimalFormat = new DecimalFormat();
    /**
     * Creates Editing Panel.
     *
     * @param workflow SeismicWorkflow
     */
    public DataViewportPanel(SeismicWorkflow workflow) {
        super();

        this.workflow = workflow;

        initGUI();
    }

    /**
     * setup GUI.
     *
     */
    private void initGUI() {
        _primaryField = new JComboBox();
        _primaryField.setEditable(false);

        _primaryField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                JComboBox cb = (JComboBox) e.getSource();
                String item = (String) cb.getSelectedItem();
                boolean enable = true;

                if (item == null)
                    return;
                if (item.equals(("TRACE")))
                    enable = false;

                _applyGapsBox.setEnabled(enable);
                _gapSize.setEnabled(enable);
            }
        });

        _applyGapsBox = new JCheckBox("APPLY_GAPS");

        _primaryStart = new JTextField(10);
        _primaryEnd = new JTextField(10);
        _primaryStep = new JTextField(10);
        _gapSize = new JTextField(10);

        JPanel firstKP = new JPanel(new GridLayout(6, 2));
        firstKP.setBorder(BorderFactory.createTitledBorder("PRIMARY_KEY"));
        firstKP.setMaximumSize(new Dimension(300, 60));

        firstKP.add(new JLabel("KEY_FIELD"));
        firstKP.add(_primaryField);
        firstKP.add(new JLabel("START_VALUE"));
        firstKP.add(_primaryStart);
        firstKP.add(new JLabel("END_VALUE"));
        firstKP.add(_primaryEnd);
        firstKP.add(new JLabel("STEP_VALUE"));
        firstKP.add(_primaryStep);
        firstKP.add(_applyGapsBox);
        firstKP.add(new JLabel());
        firstKP.add(new JLabel("GAPS_SIZE"));
        firstKP.add(_gapSize);

        _secondaryField = new JComboBox();
        _secondaryField.setEditable(false);

        _secondaryField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                JComboBox cb = (JComboBox) e.getSource();
                String item = (String) cb.getSelectedItem();
                boolean enable = true;

                if (item == null)
                    return;
                if (item.equals("NONE"))
                    enable = false;

                _secondaryStart.setEnabled(enable);
                _secondaryEnd.setEnabled(enable);
                _secondaryStep.setEnabled(enable);
            }
        });

        _secondaryStart = new JTextField(10);
        _secondaryEnd = new JTextField(10);
        _secondaryStep = new JTextField(10);

        JPanel secondKP = new JPanel(new GridLayout(4, 2));
        secondKP.setBorder(BorderFactory.createTitledBorder("SECONDARY_KEY"));
        secondKP.setMaximumSize(new Dimension(300, 60));

        secondKP.add(new JLabel("KEY_FIELD"));
        secondKP.add(_secondaryField);
        secondKP.add(new JLabel("START_VALUE"));
        secondKP.add(_secondaryStart);
        secondKP.add(new JLabel("END_VALUE"));
        secondKP.add(_secondaryEnd);
        secondKP.add(new JLabel("STEP_VALUE"));
        secondKP.add(_secondaryStep);

        //sample panel

        JPanel sampleP = new JPanel(new GridLayout(2, 2));
        sampleP.setMaximumSize(new Dimension(300, 30));

        int sampleUnit = workflow.getDataLoader().getDataReader()
                .getMetaData().getSampleUnits();

        _sampleStart = new JTextField(10);
        _sampleEnd = new JTextField(10);
        if (sampleUnit==SeismicConstants.UNIT_TIME){
            sampleP.setBorder(BorderFactory.createTitledBorder(("TIME_RANGE")));
            sampleP.add(new JLabel("START_TIME"));
            sampleP.add(_sampleStart);
            sampleP.add(new JLabel("END_TIME"));
            sampleP.add(_sampleEnd);

        }else{
            sampleP.setBorder(BorderFactory.createTitledBorder("DEPTH_RANGE"));
            sampleP.add(new JLabel("START_DEPTH"));
            sampleP.add(_sampleStart);
            sampleP.add(new JLabel("END_DEPTH"));
            sampleP.add(_sampleEnd);
        }

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(Box.createGlue());
        add(firstKP);
        add(Box.createGlue());
        add(secondKP);
        add(Box.createGlue());
        add(sampleP);
        add(Box.createGlue());
    }

    /**
     * updates the parameter values from the workflow.
     *
     */
    public void initializeUIValues() {

        if (workflow != null) {
            float startValue = (float) workflow.getDataLoader()
                    .getDataReader().getMetaData().getStartValue();

            HeaderFieldEntry fldDesc;
            DataChooser selector = workflow.getDataLoader().getDataSelector();

            _primaryField.removeAllItems();
            _primaryField.addItem(("TRACE_ID"));
            Vector keyList = workflow.getDataLoader().getPrimaryKeys();
            for (int i = 0; i < keyList.size(); i++) {
                fldDesc = (HeaderFieldEntry) keyList.get(i);
                _primaryField.addItem(fldDesc.getName());
            }

            oldFirstKey = selector.getPrimaryKey();

            if (oldFirstKey == DataChooser.TRACE_ID_AS_KEY) {
                _primaryField.setSelectedItem(("TRACE_ID"));
            } else {
                for (int i = 0; i < keyList.size(); i++) {
                    fldDesc = (HeaderFieldEntry) keyList.get(i);
                    if (fldDesc.getFieldId() == oldFirstKey) {
                        _primaryField.setSelectedItem(fldDesc.getName());
                        break;
                    }
                }
            }
            double primaryKeyStart = selector.getPrimaryKeyStart();

            if (primaryKeyStart == -Double.MAX_VALUE)
                _primaryStart.setText("");
            else
                _primaryStart.setText(decimalFormat.format(primaryKeyStart));
            firstKeyStart = (float) primaryKeyStart;

            double primaryKeyEnd = selector.getPrimaryKeyEnd();

            if (primaryKeyEnd == Double.MAX_VALUE)
                _primaryEnd.setText("");
            else

                _primaryEnd.setText(decimalFormat.format(primaryKeyEnd));
            firstKeyEnd = (float) primaryKeyEnd;


            firstKeyStep = (float)selector.getPrimaryKeyStep();
            if (firstKeyStep == 0)
                _primaryStep.setText("");
            else
                _primaryStep.setText(decimalFormat.format(firstKeyStep));


            //gaps
            lastGApStatus = useGapValue = selector.isGapApplied();
            if (useGapValue && !_applyGapsBox.isSelected())
                _applyGapsBox.doClick();


            Integer gapSize = new Integer(selector.getGapInTraces());

            _gapSize.setText(decimalFormat.format(gapSize));
            oldGapValue = gapSize.intValue();

            _secondaryField.removeAllItems();
            _secondaryField.addItem(("NONE"));
            keyList = workflow.getDataLoader().getSecondaryKeys();
            for (int i = 0; i < keyList.size(); i++) {
                fldDesc = (HeaderFieldEntry) keyList.get(i);
                _secondaryField.addItem(fldDesc.getName());
            }

            oldSecondKey = selector.getSecondaryKey();

            if (oldSecondKey == DataChooser.NO_SECONDARY_KEY) {
                _secondaryField.setSelectedItem(("NONE"));
            } else {
                for (int i = 0; i < keyList.size(); i++) {
                    fldDesc = (HeaderFieldEntry) keyList.get(i);
                    if (fldDesc.getFieldId() == oldSecondKey) {
                        _secondaryField.setSelectedItem(fldDesc.getName());
                        break;
                    }
                }
            }


            secondKeyStart = (float)selector.getSecondaryKeyStart();
            if (secondKeyStart == -Float.MAX_VALUE)
                _secondaryStart.setText("");
            else
                _secondaryStart.setText(decimalFormat.format(secondKeyStart));


            secondKeyEnd = (float) selector.getSecondaryKeyEnd();

            if (secondKeyEnd == Float.MAX_VALUE)
                _secondaryEnd.setText("");
            else
                _secondaryEnd.setText(decimalFormat.format(secondKeyEnd));

            secondKeyStep = (float)selector.getSecondaryKeyStep();
            if (secondKeyStep == 0)
                _secondaryStep.setText("");
            else
                _secondaryStep.setText(decimalFormat.format(secondKeyStep));

            lastSampleStart = (float)(selector.getStartSampleValue()
                    + startValue);
            _sampleStart.setText(decimalFormat.format(lastSampleStart));

            lastSampleEnd = (float) (selector.getEndSampleValue() + startValue);
            _sampleEnd.setText(decimalFormat.format(lastSampleEnd));

        }
    }


    /**
     * Commit the inputed values into workflow.
     * @return status of commiting process.
     */
    public boolean commitUIValues() {

        DataChooser selector = workflow.getDataLoader().getDataSelector();
        selector.setNotify(true);

        int newPrimaryKey = DataChooser.TRACE_ID_AS_KEY;

        String selected = _primaryField.getSelectedItem().toString();

        Vector keyList = workflow.getDataLoader().getPrimaryKeys();

        HeaderFieldEntry fieldEntry;
        for (int i = 0; i < keyList.size(); i++) {
            fieldEntry = (HeaderFieldEntry) keyList.get(i);
            if (selected.equals(fieldEntry.getName())) {
                newPrimaryKey = fieldEntry.getFieldId();
                break;
            }
        }

        boolean isChanged = false;

        if (newPrimaryKey != oldFirstKey) {
            selector.setPrimaryKey(newPrimaryKey);
            updatePrimaryParameters();
            oldFirstKey = newPrimaryKey;
            isChanged = true;
        } else
            isChanged = updatePrimaryParameters();

        int newSecondaryKey = DataChooser.NO_SECONDARY_KEY;

        selected = _secondaryField.getSelectedItem().toString();

        keyList = workflow.getDataLoader().getSecondaryKeys();

        for (int i = 0; i < keyList.size(); i++) {
            fieldEntry = (HeaderFieldEntry) keyList.get(i);
            if (selected.equals(fieldEntry.getName())) {
                newSecondaryKey = fieldEntry.getFieldId();
                break;
            }
        }

        if (newSecondaryKey != oldSecondKey) {
            selector.setSecondaryKey(newSecondaryKey);
            updateSecondaryParameters();
            oldSecondKey = newSecondaryKey;
            isChanged = true;
        } else {
            isChanged |= updateSecondaryParameters();
        }

        double startValue = workflow.getDataLoader().getDataReader()
                .getMetaData().getStartValue();


        try {
            Float input = decimalFormat.parse(_sampleStart.getText()).floatValue();

            if (input != lastSampleStart) {
                selector.setStartSampleValue(input - startValue);
                lastSampleStart = input;
                isChanged = true;
            }
            input = decimalFormat.parse(_sampleEnd.getText()).floatValue();
            if (input != lastSampleEnd) {
                selector.setEndSampleValue(input - startValue);
                lastSampleEnd = input;
                isChanged = true;
            }
        } catch (ParseException nex) {
            JOptionPane.showMessageDialog(this, "Found Wrong Value in Time/Depth Range field.");
        }

        useGapValue = _applyGapsBox.isSelected();
        if (useGapValue != lastGApStatus) {
            selector.applyGaps(useGapValue);
            lastGApStatus = useGapValue;
            isChanged = true;
        }

        try {
            Integer input = new Integer(_gapSize.getText());
            if (input.intValue() != oldGapValue) {
                selector.setGapInTraces(input.intValue());
                oldGapValue = input.intValue();
                isChanged = true;
            }
        } catch (NumberFormatException nex) {
            JOptionPane.showMessageDialog(this, "Found Wrong Value in gap field.");
        }

        selector.setNotify(false);

        if (isChanged) {
            workflow.invalidate(selector);
        }

        return isChanged;
    }

    /**
     *
     * @return changed or not
     */
    private boolean updatePrimaryParameters() {

        boolean isChanged = false;
        DataChooser selector = workflow.getDataLoader().getDataSelector();

        try {
            float input = decimalFormat.parse(_primaryStart.getText()).floatValue();
            if (input != firstKeyStart) {
                firstKeyStart = input;
                selector.setPrimaryKeyStart(firstKeyStart);
                isChanged = true;
            }
            input = decimalFormat.parse(_primaryEnd.getText()).floatValue();
            if (input != firstKeyEnd) {
                firstKeyEnd = input;
                selector.setPrimaryKeyEnd(input);
                isChanged = true;
            }
            input = decimalFormat.parse(_primaryStep.getText()).floatValue();
            if (input != firstKeyStep) {
                firstKeyStep = input;
                selector.setPrimaryKeyStep(input);
                isChanged = true;
            }
        } catch (ParseException nex) {
            JOptionPane.showMessageDialog(this, "Found Wrong Value in primary key.");
        }

        return isChanged;
    }


    private boolean updateSecondaryParameters() {

        boolean isChanged = false;

        try {
            String input = _secondaryStart.getText();
            if (input.length() > 0) {
                float f = decimalFormat.parse(input).floatValue();
                if (f != secondKeyStart) {
                    secondKeyStart = f;
                    workflow.getDataLoader().getDataSelector()
                            .setSecondaryKeyStart(f);
                    isChanged = true;
                }
            }
            input = _secondaryEnd.getText();
            if (input.length() > 0) {
                float f = decimalFormat.parse(input).floatValue();
                if (f != secondKeyEnd) {
                    secondKeyEnd = f;
                    workflow.getDataLoader().getDataSelector()
                            .setSecondaryKeyEnd(f);
                    isChanged = true;
                }
            }
            input = _secondaryStep.getText();
            if (input.length() > 0) {
                float f = decimalFormat.parse(input).floatValue();
                if (f != secondKeyStep) {
                    secondKeyStep = f;
                    workflow.getDataLoader().getDataSelector()
                            .setSecondaryKeyStep(f);
                    isChanged = true;
                }
            }
        } catch (ParseException nex) {
            JOptionPane.showMessageDialog(this, "Found Wrong Value in second key.");
        }

        return isChanged;
    }

}
