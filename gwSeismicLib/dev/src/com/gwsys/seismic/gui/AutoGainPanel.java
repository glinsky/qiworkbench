///
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.gui;

import java.awt.Dimension;
import java.text.DecimalFormat;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import com.gwsys.seismic.core.AutoGainController;
import com.gwsys.seismic.core.SeismicWorkflow;

/**
 * Auto Gain Control Panel.
 */
public class AutoGainPanel extends ControlPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3258129159027700532L;

	private SeismicWorkflow workflow = null;

	private float oldLengthValue;

	private boolean enableState, oldEnableState;
	
	private JTextField agcLengthField;

	private JCheckBox _applyBox;

	private DecimalFormat decimalFormat = new DecimalFormat();

	/**
	 * Creates new AGC panel.
	 *
	 * @param workflow workflow to be controlled
	 */
	public AutoGainPanel(SeismicWorkflow workflow) {
		super();

		this.workflow = workflow;

		JPanel panel = new JPanel(new SpringLayout());
		panel.setBorder(BorderFactory.createTitledBorder(("Auto Gain Control")));
		
		agcLengthField = new JTextField();
		agcLengthField.setMaximumSize(new Dimension(300,25));
		_applyBox = new JCheckBox(("Apply"));

		panel.add(new JLabel(("Window Length")));
		panel.add(agcLengthField);
		panel.add(_applyBox);
		panel.add(new JLabel());
		SpringUtilities.makeCompactGrid(panel, 2, 2, 15, 15, 15, 15);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		add(panel);
	}

	/**
	 * Initializes the parameter values from the workflow.
	 * @see com.gwsys.seismic.gui.ControlPanel ControlPanel
	 */
	public void initializeUIValues() {

		AutoGainController tagc = workflow.getInterpretation().getTraceAGC();

		double win = (tagc.getWindowLength());
		agcLengthField.setText(decimalFormat.format(win));

		try {
			oldLengthValue = decimalFormat.parse(agcLengthField.getText())
					.floatValue();
		} catch (ParseException e) {
			oldLengthValue = 200;
		}

		oldEnableState = enableState = tagc.isEnabled();
		if (enableState)
			_applyBox.doClick();
	}

	/**
	 * Commits the inputed values to rendering process.
	 * @return status of this process. 
	 */
	public boolean commitUIValues() {
		boolean isChange = false;

		try {
			Number inputed = decimalFormat.parse(agcLengthField.getText());;
			
			if (inputed.floatValue() != oldLengthValue) {
					
				workflow.getInterpretation().getTraceAGC()
							.setWindowLength(inputed.floatValue());
					
				workflow.invalidate(workflow.getInterpretation().getTraceAGC());
					
				oldLengthValue = inputed.floatValue();
					
				isChange = true;
			}
		} catch (ParseException e) {
			//ignore
		} catch (NumberFormatException nex) {
			//ignore
		}

		enableState = _applyBox.isSelected();
		if (enableState != oldEnableState) {
			workflow.getInterpretation().getTraceAGC().setEnabled(enableState);
			workflow.invalidate(workflow.getInterpretation().getTraceAGC());
			oldEnableState = enableState;
			isChange = true;
		}

		return isChange;
	}

}
