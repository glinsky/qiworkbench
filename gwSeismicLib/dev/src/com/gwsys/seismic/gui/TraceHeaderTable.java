///
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.gui;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

/**
 * Defines a table without editable cells.
 * @author Loui
 *
 */
public class TraceHeaderTable extends JTable{

	private static final long serialVersionUID = 1L;

	TraceHeaderTable(Object[] header, Object[][] content){
		setModel(new TraceTableModel(header, content));
		setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	}

	/**
	 * We make cell uneditable.
	 * @author Team
	 *
	 */
	class TraceTableModel extends DefaultTableModel{

		/**
		 *
		 */
		private static final long serialVersionUID = 1L;

		TraceTableModel(Object[] header, Object[][] content){
			super(content, header);
		}

		public boolean isCellEditable(int row, int col){
			return false;
		}
	}

}
