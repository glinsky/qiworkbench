//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.gui;

import java.awt.Color;
import java.awt.Component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.gwsys.seismic.core.SeismicWorkflow;
import com.gwsys.seismic.util.SeismicColorMap;
import com.gwsys.util.colorbar.ColorBarDialog;

/**
 * Listener to create and display the color map dialog for
 * the seismic editor panels.
 */
public class ColorActionListener implements ActionListener {

	private SeismicWorkflow workflow;

	private Component frameParent;

	/**
	 * Constructor.
	 * @param workflow SeismicWorkflow
	 * @param parent The parent of dialog.
	 */
	public ColorActionListener(SeismicWorkflow workflow, Component parent) {
		this.workflow = workflow;
		this.frameParent = parent;
	}

	/**
	 * Show dialog.
	 * @param e ActionEvent
	 */
	public void actionPerformed(ActionEvent e) {
		SeismicColorMap cmap = workflow.getTraceRasterizer().getColorMap();
		if (cmap == null)
			throw new IllegalArgumentException("Colormap is null");

		Color[] ramp = new Color[cmap.getDensityColorSize()];

		for (int i = 0; i < cmap.getDensityColorSize(); i++) {
			ramp[i] = cmap.getColor(i);
		}

		ColorBarDialog editor = new ColorBarDialog();
		editor.setModal(true);
		if (frameParent != null)
			editor.setLocationRelativeTo(frameParent);
		editor.getColorBar().setColorArray(ramp);

		editor.addColorChangeListener(new ColormapEditorListener(workflow));
		editor.setVisible(true);
	}
}
