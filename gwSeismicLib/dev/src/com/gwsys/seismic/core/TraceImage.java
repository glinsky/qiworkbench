//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//  
package com.gwsys.seismic.core;

import com.gwsys.seismic.util.SeismicColorMap;
import com.gwsys.seismic.util.SeismicColorEvent;
import com.gwsys.seismic.util.SeismicColorListener;
import com.gwsys.gw2d.shape.ImageArea;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;

/**
 * This class represents seismic image as DataShape.
 * @author Li
 */
public abstract class TraceImage extends ImageArea {

	/**
	 * Draw traces from left to right
	 */
	public static final int DRAW_TRACE_LEFT_TO_RIGHT = 1;
	/**
	 * Draw traces from right to left
	 */
	public static final int DRAW_TRACE_RIGHT_TO_LEFT = 2;

	private DataPainter dataPainter = null;
	
	private int drawingDirection = DRAW_TRACE_LEFT_TO_RIGHT;
	
	private DataImageCreator imageCreator = null;

	private SeismicWorkflow workflow = null;


	/**
	 * Creates seismic image using given workflow.
	 * @param x    the x coordinate of the location point.
	 * @param y    the y coordinate of the location point.
	 * @param w	   the width of the bound of shape.
	 * @param h    the height of the bound of shape.
	 * @param workflow The SeismicWorkflow.
	 */
	public TraceImage(double x, double y, double w, double h,
			SeismicWorkflow workflow) {

		super(x, y, w, h, null, false, false);

		setPipeline(workflow);

		SeismicColorMap cmap = workflow.getTraceRasterizer().getColorMap();

		cmap.addColorMapListener(new SeismicColorListener() {
			public void colorMapUpdated(SeismicColorEvent e) {
				invalidateShape();
			}
		});
		dataPainter = new SegyTracePainter(workflow);
	}

	/**
	 * Gets draw direction.
	 * @return direction
	 */
	public int getDirection() {
		return drawingDirection;
	}

	/**
	 * Returns DataImageCreator.
	 *
	 * @return DataImageCreator.
	 */
	public DataImageCreator getImageCreator() {
		return imageCreator;
	}

	/**
	 * Returns the model limits of the seismic data.
	 * @return Bound2D the limit of the seismic data.
	 */
	public abstract Bound2D getModelLimits();

	/**
	 * Returns Workflow used in this object.
	 *
	 * @return Workflow used in this object.
	 */
	public SeismicWorkflow getPipeline() {
		return workflow;
	}

	/**
	 * Renders the data into painter.
	 * @param visBound Visible Bound.
	 * @param trans2d Transform2D.
	 */
	protected void renderToBitmap(Bound2D visBound, Transform2D trans2d) {
		workflow.getInterpretation().getTraceInterpolator()
				.setResamplingFactor(
						(float) (trans2d.getScaleY() * getPipeline()
								.getDataLoader().getDataReader().getMetaData()
								.getSampleRate()));

		TraceRasterizer rasterizer = workflow.getTraceRasterizer();

		rasterizer.setImageData(imageCreator.getData(), imageCreator.getWidth(),
				imageCreator.getHeight());

		Bound2D modelLimits = getModelLimits();

		if (!modelLimits.intersects(visBound.getMinX(), visBound.getMinY(),
				visBound.getWidth(), visBound.getHeight())) {
			return;
		} 
		Bound2D viewLimits = modelLimits.intersection(visBound);
		if (dataPainter==null)
			dataPainter = new SegyTracePainter(workflow);
		dataPainter.setBoundingBox(viewLimits, imageCreator.getHeight());
		dataPainter.draw(trans2d);
	}

	/**
	 * Sets up the drawing direction. 
	 * Such as DRAW_TRACE_RIGHT_TO_LEFT and DRAW_TRACE_LEFT_TO_RIGHT.
	 * 
	 * @param direction the drawing direction.
	 */
	public void setDirection(int direction) {
		boolean notificationEnabled = isNotificationEnabled();
		setNotification(false);

		drawingDirection = direction;
		
		dataPainter.setDirection(direction);

		setNotification(notificationEnabled);
	}

	/**
	 * Sets TraceImageCreator instance
	 * @param bitmap TraceImageCreator instance.
	 */
	protected void setImageCreator(TraceImageCreator bitmap) {
		imageCreator = bitmap;
	}

	/**
	 * Sets new workflow for this seismic image.
	 *
	 * @param workflow the pipeline to be used in this object.
	 */
	public void setPipeline(SeismicWorkflow workflow) {
		this.workflow = workflow;
		dataPainter = new SegyTracePainter(workflow);
	}
}
