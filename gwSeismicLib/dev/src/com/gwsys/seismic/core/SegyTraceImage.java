//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.core;

import com.gwsys.gw2d.model.ScenePainter;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.NoninvertibleTransformException;

/**
 * This class renders the seismic samples into buffered image.
 * The two parameters (modelUnitsPerTrace and modelUnitsPerSample)are required by bhpViewer.
 *
 */
public class SegyTraceImage extends TraceImage {

	private double _modelUnitsPerSample;

	private double _modelUnitsPerTrace;

	/**
	 * Creates seismic image using given SeismicWorkflow.<br>
	 *
	 * @param x x-coordinate of image anchor point in model space.
	 * @param y y-coordinate of image anchor point in model space.
	 * @param modelUnitsPerTrace Scale factor in X direction.
	 * @param modelUnitsPerSample Scale factor in Y direction.
	 * @param workflow SeismicWorkflow.
	 */
	public SegyTraceImage(double x, double y, double modelUnitsPerTrace,
			double modelUnitsPerSample, SeismicWorkflow workflow) {

		super(x, y, 0, 0, workflow);

		setScale(modelUnitsPerTrace, modelUnitsPerSample);
	}

	/**
	 * Returns the model limits of the seismic data.
	 * @return Bound2D
	 */
	public Bound2D getModelLimits() {
		return getPipeline().getVirtualModelLimits();
	}

	/**
	 * Returns Scale factor in Y direction.
	 *
	 * @return Scale factor in Y direction.
	 */
	public double getModelUnitsPerSample() {
		return _modelUnitsPerSample;
	}

	/**
	 * Returns Scale factor in X direction.
	 *
	 * @return Scale factor in X direction.
	 */
	public double getModelUnitsPerTrace() {
		return _modelUnitsPerTrace;
	}

	/**
	 * Gets the rotated shape.
	 *
	 * @return the rotated shape.
	 */
	private Bound2D getRotatedRect(Bound2D rect, float angle) {

		if (angle == 0.0)
			return new Bound2D(rect);

		Transform2D rotation = new Transform2D();
		rotation.rotate(angle, getLocationX(), getLocationY());

		return rotation.transform(rect);
	}

	/**
	 * Gets the rotated shape.
	 * @param tr Transform2D.
	 * @return The rotated shape.
	 */
	protected Shape getRotatedShape(Transform2D tr) {

		Bound2D bound = getBoundingBoxWithoutRotation(tr);

		return getRotatedRect(bound, this.getRotationAngle());
	}

	/**
	 * Invoked by the layer to render the seismic image.<br>
	 *
	 * @param painter  the shape renderer
	 * @param bound the bounding box of screen space
	 */
	public void render(ScenePainter painter, Bound2D bound) {
		//System.out.println("SegyTraceImage "+bound);
		if (isVisible() && getAttribute() != null &&
				painter.getGraphics() != null && getPipeline() != null) {

			boolean flipx = ((getDirection() & TraceImage.DRAW_TRACE_RIGHT_TO_LEFT)
					== TraceImage.DRAW_TRACE_RIGHT_TO_LEFT);
			boolean flipy = painter.getTransformation().getScaleY() < 0 ? true : false;

			setGenericSeismicImageSize();

			Transform2D transform = painter.getTransformation();
			Bound2D modelRotatedBound = getRotatedRect(bound, -this.getRotationAngle());
			Bound2D deviceBound = getBoundingBoxWithoutRotation(transform);

			if (!modelRotatedBound.intersects(deviceBound.x, deviceBound.y, deviceBound.width, deviceBound.height))
				return;

			Bound2D modelBound = getPipeline().getVirtualModelLimits();//data range
			Bound2D clipBox = new Bound2D(getLocationX(),getLocationY(), getWidth(), getHeight());
			Bound2D visible = new Bound2D(painter.getGraphics().getClipBounds());

			// Create TraceImageCreator
			setImageCreator(new TraceImageCreator((int) (Math.ceil(visible
					.getWidth())), (int) (Math.ceil(visible.getHeight())),
					getPipeline().getTraceRasterizer().getColorMap()));

			// Transformation for Image
			Transform2D transBitmap = null;
			if (flipy){
				transBitmap = new Transform2D(modelBound, clipBox, false, false);
			}else{
				transBitmap = (Transform2D) transform.clone();
				transBitmap.concatenate(new Transform2D(modelBound, deviceBound, false, false));
			}

			float newFactor = (float)(Math.abs(transBitmap.getScaleY())	*
			getPipeline().getDataLoader().getDataReader().getMetaData().getSampleRate());

			float oldFactor = getPipeline().getInterpretation().getTraceInterpolator()
					.getResamplingFactor();

			if (oldFactor != newFactor) {
				getPipeline().getInterpretation().getTraceInterpolator().setResamplingFactor(newFactor);
				getPipeline().invalidate(getPipeline().getInterpretation().getTraceInterpolator());
			}
			//painter.getGraphics().getClipBounds()
			renderToBitmap(transBitmap.inverseTransform(visible), transBitmap);//modelBound
			// Draw image to painter's graphics
			Graphics2D g2d = painter.getGraphics();

			if (getRotationAngle() != 0.0) {
				g2d.transform(painter.getTransformation());
				g2d.rotate(getRotationAngle(), getLocationX(), getLocationY());

				try {
					g2d.transform(painter.getTransformation().createInverse());
				} catch (NoninvertibleTransformException e) {
					g2d.setTransform(painter.getAffineTransform());
				}

			}

			Transform2D trans = new Transform2D();
			//clipBox to visible
			trans.translate(visible.getX(), visible.getY());
			trans.translate((flipx ? visible.getWidth() : 0), (flipy ? visible
					.getHeight() : 0));
			trans.scale((flipx ? -1 : 1), (flipy ? -1 : 1));

			g2d.drawImage(getImageCreator().getBufferedImage(), trans, null);

			g2d.setTransform(painter.getAffineTransform());

		}
	}

	/**
	 * Resets the size of the image.
	 *
	 * @return The status of he image size; true if changed.
	 */
	public boolean setGenericSeismicImageSize() {
		boolean sizeChanged = false;
		double w = getPipeline() != null ? getPipeline().getVirtualModelLimits().width
					* _modelUnitsPerTrace : 0;
		double h = getPipeline() != null ? getPipeline().getVirtualModelLimits().height /
					getPipeline().getDataLoader().getDataReader().getMetaData().getSampleRate() *
					_modelUnitsPerSample : 0;
		if ((w != getWidth()) || (h != getHeight())) {
			sizeChanged = true;
			setSize(w, h);
		}
		return sizeChanged;
	}

	/**
	 * Attaches new workflow for this seismic image.
	 *
	 * @param workflow The new workflow for this image.
	 */
	public void setPipeline(SeismicWorkflow workflow) {

		super.setPipeline(workflow);

		setGenericSeismicImageSize();

	}

	/**
	 * Sets the scaling factor for this image.
	 *
	 * @param modelUnitsPerTrace Scale factor in X direction.
	 * @param modelUnitsPerSample Scale factor in Y direction.
	 */
	public void setScale(double modelUnitsPerTrace, double modelUnitsPerSample) {

		_modelUnitsPerTrace = modelUnitsPerTrace;
		_modelUnitsPerSample = modelUnitsPerSample;

		setGenericSeismicImageSize();
	}
}
