//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.seismic.reader.SeismicReader;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;

import java.util.HashSet;

/**
 * Default implementation of SeismicWorkflow.
 * It contains loading process, normalizing process, interpolating process.
 * 
 */
public class DefaultWorkflow extends AbstractWorkflow {

	/** Current process in workflow */
	private TraceProcessor currentProcess;

	private int drawingDirection;

	/** Store highlighted traces */
	private HashSet highLightedTraces = new HashSet();

	/** DataLoader */
	private DataLoader loader;

	private DataPainter painter;

	private Interpretation processes = new ProcessingList();

	private TraceRasterizer raster;

	private boolean reversedSample;

	/**
	 * default constructor.
	 */
	protected DefaultWorkflow() {
	}

	/**
	 * Creates a new Workflow with given DataReader.
	 *
	 * @param seismicReader DataReader.
	 */
	public DefaultWorkflow(SeismicReader seismicReader) {

		if (seismicReader != null) {
			// init
			initializeProcesses(seismicReader);
		} else {
			throw new IllegalArgumentException("SeismicReader could not be null");
		}

		raster = new DefaultRasterizer();
		painter = new SegyTracePainter(this);

		reversedSample = false;

	}

	/**
	 * Inserts standard trace process into Workflow.
	 *
	 * @param prevProcess The process ahead of this process.
	 * @param thisProcess The process to be added.
	 */
	protected void addStandardProcess(AbstractProcessor prevProcess,
			AbstractProcessor thisProcess) {

		if (thisProcess == null)
			return;

		AbstractProcessor nextProcess = (prevProcess != null) ? 
				findNextProcess(prevProcess)
				: null;

		thisProcess.setWorkflow(this);
		thisProcess.setParentProcessor(prevProcess);

		if (nextProcess != null)
			nextProcess.setParentProcessor(thisProcess);

		//set current process
		if (prevProcess == currentProcess)
			currentProcess = thisProcess;
	}

	/**
	 * Finds the next process for given process.
	 * @param process given process.
	 *
	 * @return next process for given process.
	 */
	private AbstractProcessor findNextProcess(AbstractProcessor process) {

		if (process == currentProcess)
			return null;

		AbstractProcessor thisProcess = (AbstractProcessor) currentProcess;

		while (thisProcess.getParentProcessor() != process)
			thisProcess = (AbstractProcessor) thisProcess.getParentProcessor();

		return thisProcess;
	}

	/**
	 * @return DataLoader.
	 */
	public DataLoader getDataLoader() {
		return loader;
	}

	/**
	 * @inheritDoc
	 */
	public int getDirection() {
		return drawingDirection;
	}

	/**
	 * Returns the highlighted traces.
	 *
	 * @return HashSet containing the trace id's of highlighted traces.
	 */
	protected HashSet getHighlightSet() {
		return highLightedTraces;
	}

	/**
	 * Retrives the interpretation associated with this class.
	 */
	public Interpretation getInterpretation() {
		return processes;
	}

	/**
	 * Obtains the limits of the seismic data in model space.
	 *
	 * @return limits of seismic data in model space.
	 */
	public Bound2D getRealModelLimits() {

		return getDataLoader().getDataReader().getModelLimits();
	}

	/**
	 * @inheritDoc
	 */
	public DataPainter getTracePainter(){
		return painter;
	}

	/**
	 * Returns the trace rasterizer.
	 *
	 * @return the rasterizer.
	 */
	public TraceRasterizer getTraceRasterizer() {
		return raster;
	}

	/**
	 * Used to retreive the sample values for a specified real trace.
	 * This method does not use cached traces and asks directly the reader to give
	 * data using real trace number.
	 *
	 * @param traceId  the real trace starting from <code>1</code> whose sample
	 *                 values are to be retreived
	 *
	 * @return an array of sample values or null
	 */
	public float[] getTraceSamples(int traceId) {
		TraceData data = new TraceData();
		TraceProcessor processor = (TraceProcessor) getDataLoader().getDataReader();

		processor.process((traceId - 1), null, data);

		return (data.getSamples());

	}

	/**
	 * Obtains the limits of seismic data after trace mapping
	 * is applied (in virtual space).</p>
	 *
	 * @return limits of seismic data after trace mapping.
	 */
	public Bound2D getVirtualModelLimits() {

		return loader.getDataSelector().getVirtualModelLimits();
	}

	/**
	 * Initializes processes.
	 *
	 * @param seismicReader the seismicReader process.
	 */
	protected void initializeProcesses(SeismicReader seismicReader) {
		loader = new SeismicLoader();
		loader.setDataReader(seismicReader);

		addStandardProcess(null, loader.getDataReader());
		addStandardProcess(loader.getDataReader(), loader.getDataSelector());
		addStandardProcess(loader.getDataSelector(), processes
				.getTraceNormalization());
		addStandardProcess(processes.getTraceNormalization(), processes
				.getTraceInterpolator());
		
	}

	/**
	 * Invalidates the workflow after the given process.
	 *
	 * @param process the updated process in the workflow.
	 */
	public void invalidate(TraceProcessor process) {

		TraceProcessor thisProcess = currentProcess;

		while (!(thisProcess instanceof SeismicReader)
				&& (thisProcess != process)) {
			thisProcess = (TraceProcessor) ((thisProcess).getParentProcessor());
		}
	}

	/**
	 * Checks if trace with given virtual ID is highlighted.</p>
	 *
	 * @param virtTraceId virtual trace ID
	 *
	 * @return status of trace
	 */
	public boolean isTraceHighlighted(int virtTraceId) {
		return getHighlightSet().contains(
				new Integer(getDataLoader().getDataSelector().virtualToReal(
						virtTraceId)));
	}

	/**
	 * Performs this trace process, querying TraceData for given traceId.
	 *
	 * @param traceId    the desired trace ID to process
	 * @param sampleRange  the range of required samples from this process
	 * @param traceData  the destination trace data for the trace process
	 *
	 * @return <code>true</code> if the process was successfully completed,
	 *         <code>false</code> otherwise.
	 */
	public boolean process(int traceId, NumberRange sampleRange,
			TraceData traceData) {

		if (currentProcess.process(traceId, sampleRange, traceData)) {

			if (reversedSample) {
				float[] samples = traceData.getSamples();

				for (int i = 0; i < samples.length; i++) {
					samples[i] = -samples[i];
				}
			}

			return true;
		}

		return false;
	}

	/**
	 * Sets the bounding box limiting the display of seismic data.
	 *
	 * @param bound     the bounding box of the shown data.
	 * @param height	the height of the image.
	 */
	public void setBoundingBox(Bound2D bound, int height){

		painter.setBoundingBox(bound, height);
	}

	/**
	 * Sets new DataLoader into this workflow.
	 * @param loader The new DataLoader.
	 */
	public void setDataLoader(DataLoader loader) {
		this.loader = loader;
	}

	/**
	 * Sets the DataChooser.
	 * @param dataSelector DataChooser.
	 */
	public void setDataSelector(DataChooser dataSelector) {
		loader.setDataViewport((DataChooser) updateProcess(loader
				.getDataSelector(), dataSelector));
	}

	

	/**
	 * Sets up the direction that traces are drawn.  
	 *
	 * @param direction Plot direction, such as TraceImage.RIGHT_TO_LEFT.
	 */
	public void setDirection(int direction) {
		drawingDirection = direction;
		if (painter != null)
			painter.setDirection(direction);
	}

	/**
	 * Sets data interpretation into painter.
	 * @param process Interpretation.
	 */
	public void setInterpretation(Interpretation process) {

		if (process == null)
			return;
		processes = process;
	}

	/**
	 * Sets the trace ACG process.
	 *
	 * @param agcProcess the new AGC process.
	 */
	public void setTraceAGC(AutoGainController agcProcess) {
		AutoGainController _agc = processes.getTraceAGC();
		
		if (agcProcess == null) {
			if (_agc != null) {
				AbstractProcessor prevProcess = (AbstractProcessor) _agc
						.getParentProcessor();
				AbstractProcessor nextProcess = findNextProcess(_agc);

				nextProcess.setParentProcessor(prevProcess);

				_agc.setWorkflow(null);
				_agc.setParentProcessor(null);

				processes.setTraceAGC(agcProcess);
			}
		}
		else {
			if (_agc != null) {
				AbstractProcessor prevProcess = (AbstractProcessor) _agc
						.getParentProcessor();
				AbstractProcessor nextProcess = findNextProcess(_agc);

				nextProcess.setParentProcessor(agcProcess);

				agcProcess.setParentProcessor(prevProcess);
				agcProcess.setWorkflow(this);

				_agc.setWorkflow(null);
				_agc.setParentProcessor(null);

				_agc = agcProcess;
			}
			else {
				AbstractProcessor prevProcess = processes
						.getTraceNormalization();
				AbstractProcessor nextProcess = findNextProcess(prevProcess);

				agcProcess.setWorkflow(this);
				agcProcess.setParentProcessor(prevProcess);
				nextProcess.setParentProcessor(agcProcess);

				processes.setTraceAGC(agcProcess);
			}
		}
	}

	/**
	 * Sets the trace interpolation .
	 *
	 * @param interpolator TraceInterpolator
	 */
	public void setTraceInterpolator(TraceInterpolator interpolator) {
		processes.setTraceInterpolator((TraceInterpolator) updateProcess(processes
				.getTraceInterpolator(), interpolator));
	}

	/**
	 * Sets the TraceNormalizer.
	 *
	 * @param normProcess the new normalization process.
	 */
	public void setTraceNormalization(TraceNormalizer normProcess) {

		processes.setTraceNormalization((TraceNormalizer) updateProcess(processes
				.getTraceNormalization(), normProcess));
	}

	/**
	 * Sets the rasterizer.
	 *
	 * @param rasterizer  the rasterizer.
	 */
	public void setTraceRasterizer(TraceRasterizer rasterizer) {
		if (rasterizer == null)
			return;

		raster = rasterizer;
	}
	
	/**
	 * Update the one process in the workflow.
	 * @param newProcess New process.
	 */
	private AbstractProcessor updateProcess(AbstractProcessor lastProcess,
			AbstractProcessor newProcess) {

		if (newProcess == null)
			return lastProcess;

		newProcess.setWorkflow(this);
		if (lastProcess != null)
			newProcess.setParentProcessor(lastProcess.getParentProcessor());

		//replace the current process reference
		if (currentProcess == lastProcess) {
			currentProcess = newProcess;
		}else{
			AbstractProcessor nextProcess = findNextProcess(lastProcess);
			if (nextProcess != null) 
				nextProcess.setParentProcessor(newProcess);
		}
		
		//remove reference of last process
		if (lastProcess != null) {
			lastProcess.setWorkflow(null);
			lastProcess.setParentProcessor(null);
		}
		return newProcess;
	}

}
