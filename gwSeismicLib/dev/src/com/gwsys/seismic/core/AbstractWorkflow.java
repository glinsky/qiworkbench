//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;


/**
 * This is abstract implementation of SeismicWorkflow to make it extensible.
 * The <i>BhpEventPipeline</i> is an example to write customized workflow.
 * @author Hua
 *
 */
public abstract class AbstractWorkflow implements SeismicWorkflow {

	/**
	 * @docRoot
	 */
	public void setDataSelector(DataChooser dataSelector) {

	}

	/**
	 * @docRoot
	 */
	public void setTraceNormalization(TraceNormalizer normalizer) {

	}

	/**
	 * @docRoot
	 */
	public void setTraceAGC(AutoGainController agc) {

	}

	/**
	 * @docRoot
	 */
	public void setTraceInterpolator(TraceInterpolator interpolator) {

	}

	/**
	 * @docRoot
	 */
	public TraceRasterizer getTraceRasterizer() {
		return null;
	}

	/**
	 * @docRoot
	 */
	public void setTraceRasterizer(TraceRasterizer rasterizer) {

	}

	/**
	 * @docRoot
	 */
	public void setInterpretation(Interpretation process) {

	}

	/**
	 * @docRoot
	 */
	public Interpretation getInterpretation() {
		return null;
	}

	/**
	 * @docRoot
	 */
	public DataLoader getDataLoader() {
		return null;
	}

	/**
	 * @docRoot
	 */
	public void setDataLoader(DataLoader loader) {

	}

	/**
	 * @docRoot
	 */
	public Bound2D getRealModelLimits() {
		return null;
	}

	/**
	 * @docRoot
	 */
	public Bound2D getVirtualModelLimits() {
		return null;
	}

	/**
	 * @docRoot
	 */
	public void setBoundingBox(Bound2D bbox, int destinationHeight) {

	}

	/**
	 * @docRoot
	 */
	public void invalidate(TraceProcessor process) {

	}


	/**
	 * @docRoot
	 */
	public boolean isTraceHighlighted(int traceId) {
		return false;
	}

	/**
	 * @docRoot
	 */
	public boolean process(int traceIdx, NumberRange sampleRange,
			TraceData traceData) {
		return false;
	}

	/**
	 * @docRoot
	 */
	public int getDirection() {
		return 0;
	}

}
