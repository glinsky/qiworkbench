//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

import java.util.EventObject;

/**
 * Defines the event types used in DataChooser.
 * See <i>BhpListenerDataSelector</i>
 */
public class DataKeyEvent extends EventObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3257853190165772345L;

	/** Primary key has been changed */
	public static final int PRIMARY_KEY = 100;

	/** Primary key start value has been changed */
	public static final int PRIMARY_KEY_START = 101;

	/** Primary key end value has been changed */
	public static final int PRIMARY_KEY_END = 102;

	/** Primary key step value has been changed */
	public static final int PRIMARY_KEY_STEP = 103;

	/** Secondary key has been changed */
	public static final int SECONDARY_KEY = 104;

	/** Secondary key start value has been changed */
	public static final int SECONDARY_KEY_START = 105;

	/** Secondary key end value has been changed */
	public static final int SECONDARY_KEY_END = 106;

	/** Secondary key step value has been changed */
	public static final int SECONDARY_KEY_STEP = 107;

	/** Sample start value has been changed */
	public static final int SAMPLE_VALUE_START = 108;

	/** Sample end value has been changed */
	public static final int SAMPLE_VALUE_END = 109;

	/** Apply gaps flag has been changed */
	public static final int GAPS_APPLY = 110;

	/** Size of gaps has been changed */
	public static final int GAPS_SIZE = 111;

	private int eventId;

	/**
	 * Constructs new event object.</p>
	 *
	 * @param source source of event. 
	 * @param id ID of event.
	 */
	public DataKeyEvent(Object source, int id){
		super(source);
		eventId = id;
	}

	public int getID() {
		return eventId;
	}
}