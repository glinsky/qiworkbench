//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//  
package com.gwsys.seismic.core;

import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;

/**
 * Define the interface to draw workflow.
 * 
 *
 */
public interface DataPainter {
	/**
	 * Sets the bounding box limiting the display of seismic data.
	 *
	 * @param bound     the bounding box of the shown data.
	 * @param height	the height of the image.
	 */
	public void setBoundingBox(Bound2D bound, int height);

	/**
	 * Draws process with given transformation.
	 *
	 * @param t2d  the transformation.
	 */
	public void draw(Transform2D t2d);


	/**
	 * Sets the direction that traces are drawn. 
	 * Such as LEFT_TO_RIGHT or RIGHT_TO_LEFT.
	 *
	 * @param direction LEFT_TO_RIGHT or RIGHT_TO_LEFT. 
	 */
	public void setDirection(int direction);

}
