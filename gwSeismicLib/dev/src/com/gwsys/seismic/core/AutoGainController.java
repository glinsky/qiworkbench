//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;

/**
 * Defines algorithm to do Automatic Gain Control. 
 * Based on the average value to adjust gain.
 * Ref : sugain.c file in Seismic Unix (bhpio).
 * 
 */
public class AutoGainController extends AbstractProcessor {

	/** Declares the constant for the zero threshold. */
	private static final double ZERO = 1e-8;

	/** Number of samples and scaling factor to be used */
	private double affectiveSamples, scale=1;

	/** Enable Flag */
	private boolean enabled = true;


	/**
	 * Constructor.
	 */
	public AutoGainController() {
		affectiveSamples= 250;
	}

	/**
	 * Sets the window length used by the AGC process.</p>
	 *
	 * @param length	the length for the AGC window
	 */
	public void setWindowLength(double length) {
		affectiveSamples = length;
	}

	/**
	 * Returns the window length.</p>
	 *
	 * @return the window length
	 */
	public double getWindowLength() {
		return affectiveSamples;
	}

	/**
	 * Just keep this API. Do nothing.
	 */
	public void setUnits(int type) {
		
	}

	/**
	 * Just keep this API. 
	 * @return 1.
	 */
	public int getUnits() {
		return 1;
	}

	/**
	 * Enable or not for trace interpretation.
	 * @param enabled Value to be used.
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Returns the status of AGC.
	 * @return the status of AGC.
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Sets the scaling factor for AGC.
	 */
	public void setAGCScale(double scale){
		this.scale = scale;
	}

	/**
	 * Performs an AGC trace process, generating sample values
	 * for the given traceId.
	 *
	 * @param traceId	the desired trace ID to process.
	 * @param sampleRange	the range of required samples of this process.
	 * @param traceDataOut 	the target data for the trace process.
	 *
	 * @return <code>true</code> if the process was successfully completed,
	 *         <code>false</code> otherwise.
	 */
	public boolean process(int traceId, NumberRange sampleRange,
			TraceData traceDataOut) {

		int windowLength =  (int) affectiveSamples; //number of samples
		// check if it is enabled
		if (!enabled || windowLength <= 1)
			return (getParentProcessor()).process(traceId, sampleRange,
					traceDataOut);

		// check previous process to make sure input samples
		if (!(getParentProcessor().process(traceId, null,getTraceData())))
			return false;

		// gets the sample array
		float[] inputData = getTraceData().getSamples();
		float[] outputData = traceDataOut.getSamples();

		if ((outputData == null) || (outputData.length < inputData.length)) {
			outputData = new float[inputData.length];
			traceDataOut.setSamples(outputData);
		}
		traceDataOut.setUniqueKey(getTraceData().getUniqueKey());
		traceDataOut.setNumAppliedSamples(getTraceData().getNumAppliedSamples());
		
		// temp variables
		int windowStart = -windowLength / 2;
		int windowEnd = windowLength / 2;
		int sampleCount = 0, i=0;
		double multiplier = 1, sampleTotal = 0;
		

		// get info in this range
		for (i = windowStart; i < windowEnd; i++) {
			if ((i >= 0) && (i < inputData.length)) {
				sampleTotal += Math.abs(inputData[i]);
				sampleCount++;
			}
		}

		// calculate the new samples
		for (i = 0; i < outputData.length; i ++) { 
			if (sampleTotal > ZERO) {
				multiplier = (scale * sampleCount) / sampleTotal;
			} else
				multiplier = 0;

			if (i<inputData.length)
			outputData[i] = (float) (inputData[i] * multiplier);
			
			if ((windowStart >= 0)&& (windowStart < inputData.length)) {
				sampleCount--;
				sampleTotal -= Math.abs(inputData[windowStart]);
			}

			if ((windowEnd >= 0) && (windowEnd < inputData.length)) {
				sampleCount++;
				sampleTotal += Math.abs(inputData[windowEnd]);
			}
			
			windowStart ++;
			windowEnd ++;
		}

		return true;
	}

}