//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.Arrays;

import com.gwsys.seismic.util.SeismicColorMap;

/**
 * Creates image with indexed color model.
 * @author Hua.
 *
 */
public class TraceImageCreator implements DataImageCreator{

	private SeismicColorMap colorMap;

	private BufferedImage bimage;

	private byte[] dataBuffer;

	private int _width, _height;

	/**
	 * Constructs an image with colormap.
	 * @param width The width of the image.
	 * @param height The height of the image.
	 * @param cmap The colormap used for this image.
	 */
	public TraceImageCreator(int width, int height, SeismicColorMap cmap) {

		colorMap = cmap;
		_width = width;
		_height = height;

		int imageWidth = ((width - 1) / 4 + 1) * 4;
		bimage = new BufferedImage(imageWidth, _height,
				BufferedImage.TYPE_BYTE_INDEXED, colorMap.getIndexColorModel());
		dataBuffer = ((DataBufferByte) bimage.getRaster().getDataBuffer()).getData();

		Arrays.fill(dataBuffer, cmap.getBackgroundIndex());
		
	}

	/** 
	 * Returns buffered image.
	 * @return buffered image.
	 */
	public BufferedImage getBufferedImage() {
		return bimage;
	}

	/** 
	 * Returns byte array of image.
	 * @return Byte array of image.
	 */
	public byte[] getData() {
		return dataBuffer;
	}

	/** 
	 * Returns width of image.
	 * @return Width of image.
	 */
	public int getWidth() {
		return _width;
	}

	/** 
	 * Returns height of image.
	 * @return Height of image.
	 */
	public int getHeight() {
		return _height;
	}

}