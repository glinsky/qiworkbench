//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.core;

import com.gwsys.seismic.io.TraceData;

/**
 * Provides abstract implementation of TraceProcessor.
 * 
 * @author Hua
 */
public abstract class AbstractProcessor implements TraceProcessor {

	private TraceProcessor parentProcess;
	private TraceData traceData = new TraceData();
	private SeismicWorkflow workflow;

	/**
	 * Sets the Parent processor.
	 * @param parent the Parent processor.
	 */
	public void setParentProcessor(TraceProcessor parent) {
		parentProcess = parent;
	}

	/**
	 * Returns the Parent processor.
	 * @return the Parent processor.
	 */
	public TraceProcessor getParentProcessor() {
		return parentProcess;
	}

	/**
	 * Sets the workflow that contains this process.
	 *
	 * @param workflow SeismicWorkflow that contains this process.
	 */
	public void setWorkflow(SeismicWorkflow workflow) {
		this.workflow = workflow;
	}

	/**
	 * Returns the workflow that contains this process.
	 *
	 * @return workflow that contains this process.
	 */
	public SeismicWorkflow getWorkflow() {
		return workflow;
	}
	

	/**
	 * Returns the trace data.
	 * @return TraceData
	 */
	protected TraceData getTraceData() {
		return traceData;
	}
}
