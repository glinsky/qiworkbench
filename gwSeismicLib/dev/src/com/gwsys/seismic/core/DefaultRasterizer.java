//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

import java.awt.Color;
import java.awt.Dimension;

import com.gwsys.gw2d.util.GWUtilities;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;
import com.gwsys.seismic.util.SeismicColorMap;
import com.gwsys.seismic.util.SeismicColorEvent;
import com.gwsys.seismic.util.SeismicColorListener;
import java.util.logging.Logger;

/**
 * This class renders seismic traces into an image data array.
 * There is a default colormap for this rendering process.
 * User can provide customized colormap by calling setColormap().
 * Some codes are copied from BhpModelTraceRasterizer.
 * @author Hua
 */
public class DefaultRasterizer implements TraceRasterizer{
	private static double _sqrt[] = null;
        //private static final Logger logger = Logger.getLogger(DefaultRasterizer.class.toString());
	private byte _backgroundPixel;

	private float _clippingValue;

	private SeismicColorMap _colorMap = null;

	private float _decThreshold = 5;

	private double _deltaPixels;

	private double _densityDeltaPixels = 4;

	private boolean _densityPlot = false;

	private byte _foregroundPixel;

	private byte _highlightPixel;

	private byte[] _imageData = null;

	private int _imageHeight;

	private int _imageWidth;

	private boolean _interpolatedDensityPlot = false;

	private byte _negativeFillPixel;

	private boolean _negativeFillPlot = false;

	private int _numColors;

	private int _plotType;

	private byte _positiveFillPixel;

	private boolean _positiveFillPlot = false;

	private boolean _reverseFill;

	private int _scanLineSize;

	private int[][] _transArray1, _transArray2;

	private int _transIndex1, _transIndex2;

	private int _transNum1, _transNum2;

	private boolean _wigglePlot = false;

	private boolean _wiggleTraceSkip = true;

	/**
	 * Creates a trace rasterizer with <i>
	 * POSITIVE_FILL_TRACE | WIGGLE_TRACE </i> plot type.
	 *
	 */
	public DefaultRasterizer() {
		this(POSITIVE_FILL_TRACE | WIGGLE_TRACE);
	}

	/**
	 * Creates a trace rasterizer with specified plot type.
	 * 
	 *
	 * @param plotType Type constants from Interface.
	 * 
	 */
	public DefaultRasterizer(int plotType) {
		this(plotType, new SeismicColorMap());
	}

	/**
	 * Creates a trace rasterizer with specified plot type and color map.</p>
	 *
	 * @param plotType plot type used to draw the traces .
	 * @param colorMap the color map that will be used by this rasterizer
	 *
	 */
	public DefaultRasterizer(int plotType, SeismicColorMap colorMap) {

		setPlotType(plotType);
		setColorMap(colorMap);
		setClippingValue(4);
		setMinSpacing(5);
		setReverseFill(false);
		_wiggleTraceSkip = false;
	}

	private byte calcAntialiasingColorIndex(int fore, int back, double k, int d, 
			int tA[][]) {
		int bb = back;
		int ff = fore;
		if (back < 0)
			back += 256;
		if (fore < 0)
			fore += 256;
		if (k > d)
			k = d;
		if (k < 0)
			k = 0;
		int i = (int) (k / d * (tA.length + 1) + 0.5) - 1;
		if (i < 0)
			return (byte)ff;
		if (i >= tA.length)
			return (byte)bb;
		return (byte)tA[i][back];
	}

	/**
	 * Retrieves the maximum extent allowed for wiggles in trace units.
	 *
	 * @return the maximum trace extent.
	 */
	public float getClippingValue() {
		return _clippingValue;
	}

	/**
	 * Gets the color map currently used by this rasterizer.
	 *
	 * @return the color map being used by this rasterizer
	 */
	public SeismicColorMap getColorMap() {
		return _colorMap;
	}

	/**
	 * Gets the decimation threshold in pixels.
	 *
	 * @return Minimum trace spacing in pixels.
	 */
	public double getMinSpacing() {
		return _decThreshold;
	}

	/**
	 * Retrieves the plot type used to draw the traces. The plot type
	 * is a combination (using | operations) of constants defined in
	 * interface TraceRasterizer.
	 *
	 * @return the plot type
	 */
	public int getPlotType() {
		return _plotType;
	}

	/**
	 * Retrives the size of raster.
	 */
	public Dimension getRasterSize(){
		return new Dimension(_imageWidth, _imageHeight);
	}
	
	/**
	 * Return transition color indexes between the given index and another colors
	 * @param index the given
	 * @param n the count of indexes for every color
	 * @return array of indexes
	 */
	private int[][] getTransitionIndexes(int index, int n) {
		if (index < 0)
			index += 256;
		if (index == _transIndex1 && _transNum1 == n && _transArray1 != null)
			return _transArray1;
		if (index == _transIndex2 && _transNum2 == n && _transArray2 != null)
			return _transArray2;
		int length = _colorMap.getSize();
		int ti[][] = new int[n][length];
		java.awt.Color c_base = _colorMap.getColor(index);
		int r_base = c_base.getRed();
		int g_base = c_base.getGreen();
		int b_base = c_base.getBlue();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < length; j++) {
				java.awt.Color c = _colorMap.getColor(j);
				int rj = c.getRed();
				int gj = c.getGreen();
				int bj = c.getBlue();
				int r = (rj - r_base) * (i + 1) / (n + 1) + r_base;
				int g = (gj - g_base) * (i + 1) / (n + 1) + g_base;
				int b = (bj - b_base) * (i + 1) / (n + 1) + b_base;
				int md = 0;
				int idx = 0;
				for (int k = 0; k < length; k++) {
					java.awt.Color ck = _colorMap.getColor(k);
					int rk = ck.getRed();
					int gk = ck.getGreen();
					int bk = ck.getBlue();

					int d = (r > rk) ? r - rk : rk - r;
					d += (g > gk) ? g - gk : gk - g;
					d += (b > bk) ? b - bk : bk - b;
					if (k == 0 || md > d) {
						idx = k;
						md = d;
					}
				}
				ti[i][j] = idx;
			}
		}
		if (_transArray1 == null || _transIndex1 == index) {
			_transIndex1 = index;
			_transNum1 = n;
			_transArray1 = ti;
		} else {
			_transIndex2 = index;
			_transNum2 = n;
			_transArray2 = ti;
		}

		return ti;
	}
	/**
	 * Returns the reverse fill flag.
	 *
	 * @return <code>true</code>if using reverse filling, 
	 * <code>false</code> otherwise.
	 */
	public boolean isReverseFill() {
		return _reverseFill;
	}

	/**
	 * Rasterizes a seismic trace.
	 * This method is used internally and should not normally be called
	 * directly by the application.</p>
	 *
	 * @param rasterParameters RenderingParameters.
	 */
	public void rasterizeTrace(RenderingParameters rasterParameters) {

		_wiggleTraceSkip = rasterParameters.isWiggleTraceSkipped();
		rasterizeTrace(rasterParameters.getPreviousTrace(), 
                                rasterParameters.getCurrentTrace(), 
                                rasterParameters.getSampleRange(),
				rasterParameters.getPreviousTraceLoc(), 
                                rasterParameters.getCurrentTraceLoc(),
                                rasterParameters.getNextTraceLoc(),
                                rasterParameters.isTraceHighlighted(), 
                                rasterParameters.getWiggleColorIndex());
	}

	/**
	 * Rasterizes a seismic trace.  Cannot fill in horizontal gaps (nextTraceLoc = -1).
	 * @inheritDoc
	 */
	public void rasterizeTrace(TraceData prevTrace, TraceData thisTrace,
			NumberRange sampleRange, int lastTraceLoc, int thisTraceLoc,
			boolean isHighlighted) {
		rasterizeTrace(prevTrace, thisTrace, sampleRange, lastTraceLoc,
				thisTraceLoc, -1, isHighlighted, _foregroundPixel);
	}

	/**
	 * @docRoot
	 */
	public void rasterizeTrace(TraceData prevTrace, TraceData thisTrace,
			NumberRange sampleRange, int lastTraceLoc, int thisTraceLoc,
			int nextTraceLoc, boolean isHighlighted, int wiggleColorIndex) {

		int i, j, k;

		boolean plotType1;
		boolean plotType2;
		byte pixelType1;
		byte pixelType2;
		byte foregroundPixel = (byte) wiggleColorIndex;

		if (_colorMap == null)
			return;

		if ((lastTraceLoc == Integer.MIN_VALUE)
				|| (lastTraceLoc == Integer.MAX_VALUE)) {
			lastTraceLoc = thisTraceLoc - (int) _deltaPixels;
		}
		// Rasterize half of last trace for interpolation density
		if (thisTraceLoc == Integer.MAX_VALUE) {
			thisTraceLoc = lastTraceLoc + (int) _deltaPixels / 2;
			if (!_interpolatedDensityPlot)
				return;
			_wiggleTraceSkip = true;
		}

		// Initialization for antialiasing
		int[][] transitionForeground = null;
		int[][] transitionHighlight = null;
		boolean isAntialiased = java.awt.RenderingHints.VALUE_ANTIALIAS_ON
				.equals(GWUtilities.getRenderingHints().get(
						java.awt.RenderingHints.KEY_ANTIALIASING));
		
		if (isAntialiased) {
			transitionForeground = getTransitionIndexes(foregroundPixel, 4);
			transitionHighlight = getTransitionIndexes(_highlightPixel, 4);
		}

		// May be we need to draw the area that is less then bitmap height.
		if (sampleRange.getRange() + 1 < _imageHeight)
			_imageHeight = sampleRange.getRange() + 1;

		int deltaPixels = thisTraceLoc - lastTraceLoc;

		float[] dataArray = (thisTrace != null) ? thisTrace.getSamples()
				: prevTrace.getSamples();

		//density plot
		if (_densityPlot||(_interpolatedDensityPlot&& (deltaPixels <= 1))) {

			int lastDensityLoc = lastTraceLoc+ (int) (0.5 * _densityDeltaPixels);
			int thisDensityLoc = thisTraceLoc+ (int) (0.5 * _densityDeltaPixels);

			if (lastDensityLoc < 0)
				lastDensityLoc = 0;
			if (thisDensityLoc > _imageWidth)
				thisDensityLoc = _imageWidth;

			// draw bitmap
			byte pixel = 0;
			int sampleValue = 0;
			int bitmapLineIndex = 0;

			for (i = 0; i < _imageHeight; i++) {
				sampleValue = (int) ((dataArray[i] + 1) * _numColors / 2.0);

				if (_reverseFill) {
					sampleValue = _numColors - sampleValue - 1;
				}

				if (sampleValue < 0)
					sampleValue = 0;
				else if (sampleValue >= _numColors)
					sampleValue = _numColors - 1;

				pixel = (byte) sampleValue;

				for (j = lastDensityLoc; j < thisDensityLoc; j++) {
					if (/*_imageData[bitmapLineIndex + j] == _backgroundPixel*/!isAntialiased)
						_imageData[bitmapLineIndex + j] = pixel;
					else if (isAntialiased 
							&&	_imageData[bitmapLineIndex + j] != foregroundPixel
							&& _imageData[bitmapLineIndex + j] != _highlightPixel) {
						Color data = _colorMap.getColor(_imageData[bitmapLineIndex + j]);
						Color fore = _colorMap.getColor(foregroundPixel);
						Color back = _colorMap.getColor(_backgroundPixel);
						
						int foreint = fore.getRed() + fore.getGreen() + fore.getBlue();
						int backint = back.getRed() + back.getGreen() + back.getBlue();
						if (foreint != backint) {
							int dint = data.getRed() + data.getGreen() + data.getBlue();
							double kk = (foreint - dint) * 10 / (foreint - backint);
							
							_imageData[bitmapLineIndex + j] = calcAntialiasingColorIndex(
									foregroundPixel, pixel, Math.abs(kk), 10,
									transitionForeground);
						}
					}

				}
                                if (thisDensityLoc > 0) // cannot copy from left if rendering the 0th column was skipped
                                for (j = thisDensityLoc; j < Math.min(nextTraceLoc+1, _imageWidth); j++) {
                                    //if (i == 0)
                                    //    logger.warning("Next trace loc > thisDensityLoc, filling in extra vertical line(s)");
                                    _imageData[bitmapLineIndex + j] = _imageData[bitmapLineIndex + j - 1];
                                }
				bitmapLineIndex += _scanLineSize;
			}
		}
		// **********************		
		else if (_interpolatedDensityPlot) {

			int lastDensityLoc = lastTraceLoc;
			int thisDensityLoc = thisTraceLoc;

			int startOffset;
			if (lastDensityLoc < 0) {
				startOffset = -lastDensityLoc;
				lastDensityLoc = 0;
			} else
				startOffset = 0;

			if (thisDensityLoc > _imageWidth)
				thisDensityLoc = _imageWidth;

			float[] prevDataArray;
			if (prevTrace == null)
				prevDataArray = dataArray;
			else
				prevDataArray = prevTrace.getSamples();

			double[] varA = new double[deltaPixels];
			double[] varB = new double[deltaPixels];
			double varC = _numColors / 2.;
			double b;

			for (i = 0; i < deltaPixels; i++) {
				b = ((double) (i + startOffset)) / (deltaPixels - 1);
				varA[i] = (1. - b);
				varB[i] = b;
			}

			//Map ZData
			double prevSampleValue;
			double thisSampleValue;

			byte pixel = 0;
			int sampleValue = 0;
			int bitmapLineIndex = 0;

			for (i = 0; i < _imageHeight; i++) {

				// Calculate indexes of samples in colormap
				prevSampleValue = (prevDataArray[i] + 1) * varC;
				thisSampleValue = (dataArray[i] + 1) * varC;

				// Perform interpolation
				for (j = lastDensityLoc, k = 0; j < thisDensityLoc; j++, k++) {
					sampleValue = (int) (varA[k] * prevSampleValue + varB[k]
							* thisSampleValue);

					if (_reverseFill) {
						sampleValue = _numColors - sampleValue - 1;
					}

					if (sampleValue < 0)
						sampleValue = 0;
					else if (sampleValue >= _numColors)
						sampleValue = _numColors - 1;

					pixel = (byte) sampleValue;

					if (/*_imageData[bitmapLineIndex + j] == _backgroundPixel*/!isAntialiased) {
						_imageData[bitmapLineIndex + j] = pixel;
					} else if (isAntialiased) // This is HACK, because it may be drawn after antialiased wiggle
						if (_imageData[bitmapLineIndex + j] != foregroundPixel
								&& _imageData[bitmapLineIndex + j] != _highlightPixel) {
							java.awt.Color ap = _colorMap
									.getColor(_imageData[bitmapLineIndex + j]);
							java.awt.Color fp = _colorMap
									.getColor(foregroundPixel);
							java.awt.Color bp = _colorMap
									.getColor(_backgroundPixel);
							int ai = ap.getRed() + ap.getGreen() + ap.getBlue();
							int fi = fp.getRed() + fp.getGreen() + fp.getBlue();
							int bi = bp.getRed() + bp.getGreen() + bp.getBlue();
							if (fi != bi) {
								double kk = (fi - ai) * 10 / (fi - bi);
								if (kk < 0)
									kk = -kk;
								_imageData[bitmapLineIndex + j] = 
									calcAntialiasingColorIndex(
										foregroundPixel, pixel, kk, 10,
										transitionForeground);
							}
						}
				}
                                if (thisDensityLoc > 0) // cannot copy from left if rendering the 0th column was skipped
                                for (j = thisDensityLoc; j < Math.min(nextTraceLoc+1, _imageWidth); j++) {
                                    //if (i == 0)
                                    //    logger.warning("Next trace loc > thisDensityLoc, filling in extra vertical line(s)");
                                    _imageData[bitmapLineIndex + j] = _imageData[bitmapLineIndex + j - 1];
                                }
				bitmapLineIndex += _scanLineSize;
			}
		}
		

		// If we are skipping this wiggle trace, then there is no more to do.
		if (_wiggleTraceSkip)
			return;

		//   Wiggle plot. Both  negative and positive filling

		int clipLeft = thisTraceLoc
				- (int) (_clippingValue * _deltaPixels + 0.5);
		int clipRight = thisTraceLoc
				+ (int) (_clippingValue * _deltaPixels + 0.5);

		if (clipLeft < 0)
			clipLeft = 0;
		if (clipRight > _imageWidth)
			clipRight = _imageWidth;

		int wiggleLeft;
		int wiggleRight;
		int fillLeft;
		int fillRight;
		int bitmapLineIndex = 0;

		double scaler = _deltaPixels;

		int thisValue;
		int nextValue;
		int thisValuePixel;
		int tempValue;

		double amp = dataArray[0] * scaler;

		if (amp > 0)
			thisValue = (int) (amp + 0.5);
		else if (amp < 0)
			thisValue = (int) (amp - 0.5);
		else
			thisValue = (int) amp;

		if (dataArray.length > 1) {
			amp = dataArray[1] * scaler;
			if (amp > 0)
				tempValue = (int) (amp + 0.5);
			else if (amp < 0)
				tempValue = (int) (amp - 0.5);
			else
				tempValue = (int) amp;

			if (tempValue > thisValue)
				thisValue = thisValue + 1;
			else if (tempValue < thisValue)
				thisValue = thisValue - 1;
		}
		nextValue = thisValue;

		if (_wigglePlot) {

			for (i = 0; i < _imageHeight; i++) {
				if ((i + 1) < dataArray.length) {
					amp = dataArray[i + 1] * scaler;
					if (amp > 0)
						nextValue = (int) (amp + 0.5);
					else if (amp < 0)
						nextValue = (int) (amp - 0.5);
					else
						nextValue = (int) amp;
				}

				// For Antialiasing:
				double thisAmp = dataArray[i] * scaler;

				// These define the pixel range to fill with the foreground color
				if (thisValue < nextValue) {
					wiggleLeft = thisTraceLoc + thisValue;
					wiggleRight = thisTraceLoc + nextValue;
				} else {
					wiggleLeft = thisTraceLoc + nextValue;
					wiggleRight = thisTraceLoc + thisValue;
				}

				// Check to make sure we're not beyond our limits
				if (wiggleLeft < clipLeft) {
					wiggleLeft = clipLeft;
					if (wiggleRight < clipLeft) {
						wiggleRight = clipLeft - 1;
					}
				}
				if (wiggleRight >= clipRight) {
					wiggleRight = clipRight - 1;
					if (wiggleLeft >= clipRight) {
						wiggleLeft = clipRight;
					}
				}

				// If wiggle plot is also combined with negative or positive filling
				if (_positiveFillPlot || _negativeFillPlot) {

					if (_reverseFill) {
						plotType1 = _positiveFillPlot;
						plotType2 = _negativeFillPlot;
						pixelType1 = _positiveFillPixel;
						pixelType2 = _negativeFillPixel;
					} else {
						plotType1 = _negativeFillPlot;
						plotType2 = _positiveFillPlot;
						pixelType1 = _negativeFillPixel;
						pixelType2 = _positiveFillPixel;
					}
					thisValuePixel = thisTraceLoc + thisValue;

					// negative fill
					if (thisValue < 0 && plotType1) {
						// These define the pixel range to fill with the lobe fill color
						fillLeft = thisValuePixel;
						fillRight = thisTraceLoc;
						if (isAntialiased)
							fillLeft = (int) thisAmp + thisTraceLoc;

						// Check to make sure we'ot beyond our limits
						if (fillLeft < clipLeft)
							fillLeft = clipLeft;
						if (fillRight >= clipRight)
							fillRight = clipRight - 1;
						// The following is to prevent overflow errors.
						if ((fillRight + bitmapLineIndex) >= _imageData.length) {
							fillRight = _imageData.length - bitmapLineIndex - 1;
						}

						// draw the negative fill
						if (isHighlighted) {
							for (j = fillLeft; j <= fillRight; j++) {
								_imageData[bitmapLineIndex + j] = _highlightPixel;
							}
						} else {
							for (j = fillLeft; j <= fillRight; j++) {
								_imageData[bitmapLineIndex + j] = pixelType1;
							}
						}
					} else if (thisValue > 0 && plotType2) {
						// positive fill
						fillLeft = thisTraceLoc;
						fillRight = wiggleLeft - 1;
						if (isAntialiased)
							fillRight = (int) thisAmp + thisTraceLoc;
						// Check to make sure we're not beyond our limits
						if (fillLeft < clipLeft)
							fillLeft = clipLeft;
						if (fillRight >= clipRight)
							fillRight = clipRight - 1;
						// The following is to prevent overflow errors.
						if ((fillRight + bitmapLineIndex) >= _imageData.length) {
							fillRight = _imageData.length - bitmapLineIndex - 1;
						}

						// Draw the positive fill
						if (isHighlighted) {
							for (j = fillLeft; j <= fillRight; j++) {
								if (_imageData[bitmapLineIndex + j] != foregroundPixel) {
									_imageData[bitmapLineIndex + j] = _highlightPixel;
								}
							}
						} else {
							for (j = fillLeft; j <= fillRight; j++) {
								if (_imageData[bitmapLineIndex + j] != foregroundPixel) {
									_imageData[bitmapLineIndex + j] = pixelType2;
								}
							}
						}
					}
				}

				// draw the wiggle
				byte pixel = (isHighlighted) ? _highlightPixel
						: foregroundPixel;
				if (!isAntialiased)
					for (j = wiggleLeft; j <= wiggleRight; j++)
						_imageData[bitmapLineIndex + j] = pixel;
				else
				// *** Antialiased wiggle *** begin
				{
					int transitionIndexes[][] = (isHighlighted) ? transitionHighlight
							: transitionForeground;
					int nextValue2;
					if ((i + 2) < dataArray.length) {
						double nextAmp = dataArray[i + 2] * scaler;
						if (nextAmp > 0)
							nextValue2 = (int) (nextAmp + 0.5);
						else if (nextAmp < 0)
							nextValue2 = (int) (nextAmp - 0.5);
						else
							nextValue2 = (int) nextAmp;
					} else
						nextValue2 = nextValue * 2 - thisValue;
					int prevValue;
					if ((i - 1) < dataArray.length && i > 0) {
						double prevAmp = dataArray[i - 1] * scaler;
						if (prevAmp > 0)
							prevValue = (int) (prevAmp + 0.5);
						else if (prevAmp < 0)
							prevValue = (int) (prevAmp - 0.5);
						else
							prevValue = (int) prevAmp;
					} else
						prevValue = thisValue * 2 - nextValue;

					int wiggleLeft2 = thisTraceLoc
							+ (thisValue < nextValue ? thisValue : nextValue);
					int wiggleRight2 = thisTraceLoc
							+ (thisValue > nextValue ? thisValue : nextValue);
					int nextLeft = thisTraceLoc
							+ (nextValue2 < prevValue ? nextValue2 : prevValue);
					int nextRight = thisTraceLoc
							+ (nextValue2 > prevValue ? nextValue2 : prevValue);
					int ddl = (nextLeft < wiggleLeft2) ? (wiggleLeft2 - nextLeft) * 2 / 3
							: 0;
					int ddr = (nextRight > wiggleRight2) ? (nextRight - wiggleRight2) * 2 / 3
							: 0;
					int dl = ddl + 1;
					int dr = ddr + 1;
					if (_sqrt == null) {
						_sqrt = new double[100];
						for (int h = 0; h < 100; h++)
							_sqrt[h] = Math.sqrt(h);
					}

					int wLeft = (clipLeft > wiggleLeft2 - dl) ? clipLeft
							: wiggleLeft2 - dl;
					int wRight = (clipRight - 1 < wiggleRight2 + dr) ? clipRight - 1
							: wiggleRight2 + dr;
					for (j = wLeft; j <= wRight; j++) {
						byte pix = _imageData[bitmapLineIndex + j];
						double a = thisAmp - (j - thisTraceLoc);
						if (a < 0)
							a = -a;
						if (amp == thisAmp) {
							if (a == 0)
								_imageData[bitmapLineIndex + j] = pixel;
							else
								_imageData[bitmapLineIndex + j] = 
									(calcAntialiasingColorIndex(
										pixel, pix, a * 10, 10,
										transitionIndexes));
						} else {
							double d = ((j - thisTraceLoc) - thisAmp)
									/ (amp - thisAmp);
							if (a == 0 && d == 0)
								_imageData[bitmapLineIndex + j] = pixel;
							else {
								
								int ie = (int) (0.5 + a * a * d * d
										/ (a * a + d * d) * 8 * 8); 
								if (ie >= _sqrt.length)
									ie = _sqrt.length - 1;
								_imageData[bitmapLineIndex + j] = 
									(calcAntialiasingColorIndex(
										pixel, pix, _sqrt[ie], 8,
										transitionIndexes));
							}
						}
					}
				}

				tempValue = nextValue;

				if ((i + 2) < dataArray.length) {
					amp = dataArray[i + 2] * scaler;

					if (amp > 0)
						tempValue = (int) (amp + 0.5);
					else if (amp < 0)
						tempValue = (int) (amp - 0.5);
					else
						tempValue = (int) amp;
				}

				if (tempValue > nextValue)
					thisValue = nextValue + 1;
				else if (tempValue < nextValue)
					thisValue = nextValue - 1;
				else
					thisValue = nextValue;

				bitmapLineIndex += _scanLineSize;
			}
		}
		//********************************
		else if (_positiveFillPlot || _negativeFillPlot) {
			for (i = 0; i < _imageHeight; i++) {
				amp = dataArray[i] * scaler;
				if (amp > 0)
					thisValue = (int) (amp + 0.5);
				else if (amp < 0)
					thisValue = (int) (amp - 0.5);
				else
					thisValue = (int) amp;

				if (_reverseFill) {
					plotType1 = _positiveFillPlot;
					plotType2 = _negativeFillPlot;
					pixelType1 = _positiveFillPixel;
					pixelType2 = _negativeFillPixel;
				} else {
					plotType1 = _negativeFillPlot;
					plotType2 = _positiveFillPlot;
					pixelType1 = _negativeFillPixel;
					pixelType2 = _positiveFillPixel;
				}

				// negative fill
				if (thisValue < 0 && plotType1) {
					fillLeft = thisTraceLoc + thisValue;
					fillRight = thisTraceLoc;

					// Check to make sure we're not beyond our limits
					if (fillLeft < clipLeft)
						fillLeft = clipLeft;
					if (fillRight >= clipRight)
						fillRight = clipRight - 1;

					if (fillLeft != fillRight) {

						// draw the negative fill
						if (isHighlighted) {
							for (j = fillLeft; j <= fillRight; j++) {
								_imageData[bitmapLineIndex + j] = _highlightPixel;
							}
						} else {
							for (j = fillLeft; j <= fillRight; j++) {
								_imageData[bitmapLineIndex + j] = pixelType1;
							}
						}
					}

				} else if (thisValue >= 0 && plotType2) {
					fillLeft = thisTraceLoc;
					fillRight = thisTraceLoc + thisValue;

					// Check to make sure we're not beyond our limits
					if (fillLeft < clipLeft)
						fillLeft = clipLeft;
					if (fillRight >= clipRight)
						fillRight = clipRight - 1;

					if (fillLeft != fillRight) {

						// draw the positive fill
						if (isHighlighted) {
							for (j = fillLeft; j <= fillRight; j++) {
								if (_imageData[bitmapLineIndex + j] != foregroundPixel) {
									_imageData[bitmapLineIndex + j] = _highlightPixel;
								}
							}
						} else {
							for (j = fillLeft; j <= fillRight; j++) {
								if (_imageData[bitmapLineIndex + j] != foregroundPixel) {
									_imageData[bitmapLineIndex + j] = pixelType2;
								}
							}
						}
					}
				}

				bitmapLineIndex += _scanLineSize;
			}
		}
	}

	/**
	 * Specifies the maximum extent allowed for wiggles in trace units.
	 * By default this value is set to <code>4</code>.
	 *
	 * @param clippingValue  the maximum trace extent
	 */
	public void setClippingValue(float clippingValue) {

		if (clippingValue < 0) {
			GWUtilities.warning("Only positive clipping values are allowed");
			return;
		}

		_clippingValue = clippingValue;
	}

	/**
	 * Sets the color map that will be used by this rasterizer.
	 *
	 * @param colorMap		the new color map
	 */
	public void setColorMap(SeismicColorMap colorMap) {

		if (_colorMap != null)
			_colorMap.removeColorMapListener(colorListener);

		_colorMap = colorMap;

		if (_colorMap != null) {
			updateColorMap();
			_colorMap.addColorMapListener(colorListener);
		}
	}

	/**
	 * Defines the image data array tro be filled in the trace rasterizer.
	 *
	 * @param imageData    the destination image data array
	 * @param imageWidth   the width (in pixels) of the data array in bytes
	 * @param imageHeight  the height (in pixels) of the drawable area
	 */
	public void setImageData(byte[] imageData, int imageWidth, int imageHeight) {

		_imageData = imageData;
		_imageWidth = imageWidth;
		_imageHeight = imageHeight;

		_scanLineSize = ((imageWidth - 1) / 4 + 1) * 4;
	}

	/**
	 * Sets the decimation threshold in pixels.
	 *
	 * @param decThreshold  Minimum trace spacing in pixels.
	 */
	public void setMinSpacing(double decThreshold) {
		_decThreshold = (int) Math.round(decThreshold);
	}

	/**
	 * Defines the plot type for this trace rasterizer.</p>
	 * Possible values are defined in interface TraceRasterizer.
	 * 
	 * @param plotType The new plot type for this rasterizer. 
	 */
	public void setPlotType(int plotType) {
		_plotType = plotType;

		if ((_plotType & DENSITY_TRACE) == DENSITY_TRACE) {
			_densityPlot = true;
		} else {
			_densityPlot = false;
		}

		if ((_plotType & INTERPOLATED_DENSITY_TRACE) == INTERPOLATED_DENSITY_TRACE) {
			_interpolatedDensityPlot = true;
		} else {
			_interpolatedDensityPlot = false;
		}

		if ((_plotType & WIGGLE_TRACE) == WIGGLE_TRACE) {
			_wigglePlot = true;
		} else {
			_wigglePlot = false;
		}

		if ((_plotType & POSITIVE_FILL_TRACE) == POSITIVE_FILL_TRACE) {
			_positiveFillPlot = true;
		} else {
			_positiveFillPlot = false;
		}

		if ((_plotType & NEGATIVE_FILL_TRACE) == NEGATIVE_FILL_TRACE) {
			_negativeFillPlot = true;
		} else {
			_negativeFillPlot = false;
		}

		if (_densityPlot) {
			_interpolatedDensityPlot = false;
		} else if (_interpolatedDensityPlot) {
			_densityPlot = false;
		
		} 
	}

	/**
	 * Sets up reverse fill flag.
	 *
	 * @param status Flag to set up reverse filling.
	 */
	public void setReverseFill(boolean status) {
		_reverseFill = status;
	}

	/**
	 * Sets the spacing between traces in pixels.
	 *
	 * @param deltaPixels  the spacing (pixels) between traces
	 */
	public void setTraceSpace(double deltaPixels) {
		_deltaPixels = deltaPixels;
	}

	/**
	 * This method is called by SeismicColorListener.
	 */
	protected void updateColorMap() {

		_numColors = _colorMap.getDensityColorSize();
		_foregroundPixel = _colorMap.getForegroundIndex();
		_backgroundPixel = _colorMap.getBackgroundIndex();
		_positiveFillPixel = _colorMap.getPositiveFillIndex();
		_negativeFillPixel = _colorMap.getNegativeFillIndex();
		_highlightPixel = _colorMap.getHighlightIndex();
		_transArray1 = null;
		_transArray2 = null;
	}

	/** Creates the listener for color map. */
	private SeismicColorListener colorListener = new SeismicColorListener() {
		public void colorMapUpdated(SeismicColorEvent e) {
			updateColorMap();
		}
	};
}
