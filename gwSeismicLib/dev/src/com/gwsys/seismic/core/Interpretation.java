//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//  
package com.gwsys.seismic.core;

/**
 * This is a collection of DataProcessors.
 * User can do customized processing by implementing own class.
 * @author Hua
 *
 */
public interface Interpretation {

	/**
	   * Returns the trace normalization process.
	   *
	   * @return the trace normalization process.
	   */
	  public TraceNormalizer getTraceNormalization();

	  /**
	   * Sets the normalization process.
	   *
	   * @param normProcess the new normalization process.
	   */
	  public void setTraceNormalization( TraceNormalizer normProcess );

	  /**
	   * Gets the trace AGC process.
	   *
	   * @return the trace AGC process.
	   */
	  public AutoGainController getTraceAGC();

	  /**
	   * Sets the trace ACG process.
	   *
	   * @param agcProcess the new AGC process.
	   */
	  public void setTraceAGC( AutoGainController agcProcess );

	  /**
	   * Returns the trace interpolation process.
	   *
	   * @return the trace interpolation process.
	   */
		public TraceInterpolator getTraceInterpolator();

	  /**
	   * Sets the trace interpolator.
	   *
	   * @param interpolator the new trace interpolater.
	   */
	  public void setTraceInterpolator(TraceInterpolator interpolator);
	  
}
