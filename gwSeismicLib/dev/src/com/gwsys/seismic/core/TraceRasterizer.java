//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

import java.awt.Dimension;

import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;
import com.gwsys.seismic.util.SeismicColorMap;

/**
 * Defines methods for rendering seismic traces.
 * API is copied from bhpViewer.
 * 
 */
public interface TraceRasterizer {
	
	/** wiggle display. */
	public static final int WIGGLE_TRACE = 1;

	/** positive fill display. */
	public static final int POSITIVE_FILL_TRACE = 2;

	/** negative fill display. */
	public static final int NEGATIVE_FILL_TRACE = 4;

	/** density display. */
	public static final int DENSITY_TRACE = 8;

	/** the interpolated density display. */
	public static final int INTERPOLATED_DENSITY_TRACE = 16;

	//Methods
	/**
	 * Retrieves the color map currently used by rasterizer.
	 *
	 * @return the colormap for this rasterizer.
	 */
	public SeismicColorMap getColorMap();

	/**
	 * Returns the reverse fill flag.
	 * @return status of reverse filling.
	 */
	public boolean isReverseFill();

	/**
	 * Sets the color map to use for rasterizer.
	 *
	 * @param colorMap		the colormap.
	 */
	public void setColorMap(SeismicColorMap colorMap);

	/**
	 * Changes the order of filling.
	 *
	 * @param status the order of filling.
	 */
	public void setReverseFill(boolean status);

	/**
	 * Sets the space between traces in pixels.
	 *
	 * @param space	the space between traces in pixels.
	 */
	public void setTraceSpace(double space);

	/**
	 * Retrieves the clipping value that determines to which extent
	 * traces are displayed.
	 */
	public float getClippingValue();

	/**
	 * Sets the clipping value that determines to which extent
	 * traces are displayed.
	 *
	 * @param clippingValue the maximum trace extent
	 */
	public void setClippingValue(float clippingValue);

	/**
	 * Retrieves the plot type used to draw the traces.
	 *
	 * @return the plot type used to draw the traces
	 */
	public int getPlotType();

	/**
	 * Defines the plot type for this trace rasterizer.
	 *
	 * @param plotType the new plot type for this rasterizer; valid values are:</p>
	 */
	public void setPlotType(int plotType);

	/**
	 * Defines the image data array to be rasterized.
	 *
	 * @param imageData		the destination image data array.
	 * @param imageWidth	the width, in bytes, of the data array.
	 * @param imageHeight	the height, in pixels, of the drawable area.
	 */
	public void setImageData(byte[] imageData, int imageWidth, int imageHeight);

	
	/**
	 * Gets Minimum trace spacing in pixels.
	 *
	 * @return Minimum trace spacing in pixels.
	 */
	public double getMinSpacing();

	/**
	 * Sets the Minimum trace spacing in pixels.
	 *
	 * @param minSpace Minimum trace spacing in pixels.
	 */
	public void setMinSpacing(double minSpace);

	/**
	 * Rasterizes a seismic trace.
	 *
	 * @param prevTrace 	The previous trace previous.  
	 * @param thisTrace     The current trace.
	 * @param sampleRange   The sample range.
	 * @param lastTraceLoc  The trace location of the previous trace.
	 * @param thisTraceLoc  The trace location of this trace.
	 * @param isHighlighted <code>true</code> if this trace should be highlighted,
	 *                      <code>false</code> otherwise.
	 */
	public void rasterizeTrace(TraceData prevTrace, TraceData thisTrace,
			NumberRange sampleRange, int lastTraceLoc, int thisTraceLoc,
			boolean isHighlighted);

	public void rasterizeTrace( RenderingParameters rasterParameters );
	
	public Dimension getRasterSize();
}