//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

/**
 * Stores the trace id, and associated gap value.<br>
 * It used by TraceKeyRangeChooser.
 * @author Hua.
 *
 */
class TraceGapKey {

	  /** Trace ID */
	  private int traceId;

	  /** Number of traces between two traces */
	  private int gaps;

	  /**
	   * Creates the TraceGapKey.
	   * @param traceId ID.
	   * @param gaps Number of traces.
	   */
	  public TraceGapKey( int traceId, int gaps ) {
	    this.traceId = traceId;
	    this.gaps = gaps;
	  }

	  public int getGaps(){
		  return gaps;
	  }
	  
	  public int getTraceId(){
		  return traceId;
	  }
	  
	  public TraceGapKey clone(){
		  return new TraceGapKey(traceId, gaps);
	  }	  
}
