//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.seismic.reader.SeismicReader;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.util.NumberRange;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * This class defines algorithm to choose the samples from interested traces.
 */
public abstract class TraceSamplesChooser extends DataChooser {

	/** List to store listeners */
	private List<DataKeyListener> _listeners = new Vector<DataKeyListener>();

	/** Flag to notify the listeners */
	private boolean _notifyListener = false;;

	/** The sample rate, end value, start value */
	private double _sampleRate, _sampleEnd, _sampleStart;

	/**
	 * Constructor.
	 */
	public TraceSamplesChooser() {
	}

	/**
	 * Registers a DataKeyListener.
	 * 
	 * @param listener
	 *            DataKeyListener
	 */
	public void addDataSelectorListener(DataKeyListener listener) {
		_listeners.add(listener);
	}

	/**
	 * Fires the event.
	 * @param eventType
	 */
	protected void fireDataKeyEvent(int eventType) {

		DataKeyEvent e = new DataKeyEvent(getWorkflow(), eventType);

		Iterator it = _listeners.iterator();

		while (it.hasNext()) {
			((DataKeyListener) it.next()).dataKeyChanged(e);
		}
	}

	/**
	 * Returns the ending value of the trace sample.
	 * 
	 * @return the ending value of the trace sample.
	 */
	public double getEndSampleValue() {
		return _sampleEnd;
	}

	/**
	 * Returns status of event flag.
	 * 
	 * @return status of event flag.
	 */
	protected boolean getEventNotificationFlag() {
		return _notifyListener;
	}

	/**
	 * Returns the original limits of seismic data.
	 * 
	 * @return the original limits of seismic data.
	 */
	public Bound2D getRealModelLimits() {
		return getWorkflow().getDataLoader().getDataReader().getModelLimits();
	}

	/**
	 * Returns the start value of the trace sample.
	 * 
	 * @return the start value of the trace sample.
	 */
	public double getStartSampleValue() {
		return _sampleStart;
	}

	/**
	 * Returns the trace header for given trace ID.
	 * 
	 * @param mappedTraceId
	 *            mapped trace ID.
	 * 
	 * @return the trace header or null.
	 */
	public TraceHeader getTraceHeader(int mappedTraceId) {

		int orgTraceId = virtualToReal(mappedTraceId);
		if (orgTraceId != -1)
			return getWorkflow().getDataLoader().getDataReader()
					.getTraceMetaData(orgTraceId);
		else
			return null;
	}

	/**
	 * Process the trace samples with given mapped trace id.
	 * 
	 * @param mappedTraceId
	 *            the mapped trace ID as key of TraceData.
	 * @param sampleRange
	 *            the range of required samples.
	 * @param traceDataOut
	 *            the output trace data.
	 * 
	 * @return <code>true</code> if successful;
	 *         <code>false</code> otherwise.
	 */
	public boolean process(int mappedTraceId, NumberRange sampleRange,
			TraceData traceDataOut) {

		boolean success = false;
		int orgTraceId = virtualToReal(mappedTraceId);

		if (orgTraceId != -1) {

			double sampleIndexStart = _sampleStart / _sampleRate;
			double sampleIndexeEnd = _sampleEnd / _sampleRate;
			NumberRange orgSampleRange = new NumberRange(
					(float) (sampleIndexStart),
					(float) (sampleIndexeEnd));
			if (sampleRange != null) {
				double inputSampleStart = sampleRange.getMin().intValue()
						+ sampleIndexStart;
				double orgSampleStart = (inputSampleStart >= 0) 
						? inputSampleStart
						: sampleIndexStart;

				double orgSampleEnd = orgSampleStart+ sampleRange.getRange();
				int samplePerTrace = getWorkflow().getDataLoader().getDataReader()
						.getMetaData().getSamplesPerTrace();
				if (orgSampleEnd > samplePerTrace)
					orgSampleEnd = samplePerTrace;
				orgSampleRange.setRange(orgSampleStart,orgSampleEnd);
			} 

			success = getParentProcessor().process(orgTraceId,
					orgSampleRange, traceDataOut);
			traceDataOut.setUniqueKey(new Integer(mappedTraceId));
		}

		return success;
	}

	/**
	 * Removes DataKeyListener.
	 * 
	 * @param listener
	 *            DataKeyListener
	 */
	public void removeDataSelectorListener(DataKeyListener listener) {
		_listeners.remove(listener);
	}

	/**
	 * Sets the ending value of the trace sample.
	 * 
	 * @param end
	 *            the ending value of the trace sample.
	 */
	public void setEndSampleValue(double end) {

		SeismicReader reader = getWorkflow().getDataLoader().getDataReader();
		if (end < reader.getModelLimits().getMinY()) {
			_sampleEnd = reader.getModelLimits().getMinY();
		} else if (end > reader.getModelLimits().getMaxY()) {
			_sampleEnd = reader.getModelLimits().getMaxY();
		} else
			_sampleEnd = end;

		if (_notifyListener)
			fireDataKeyEvent(DataKeyEvent.SAMPLE_VALUE_END);
	}

	/**
	 * Updates status of event handlering flag.
	 * 
	 * @param status
	 *            <code>true</code> for processing event
	 */
	public void setNotify(boolean status) {
		_notifyListener = status;
	}

	/**
	 * Sets the start value of the trace sample in sample unit.
	 * @param start
	 *            the start value of the trace sample.
	 */
	public void setStartSampleValue(double start) {
		SeismicReader reader = getWorkflow().getDataLoader().getDataReader();
		
		start = Math.round(start / reader.getMetaData().getSampleRate());
		start = start * reader.getMetaData().getSampleRate();

		if (start < reader.getModelLimits().getMinY()) {
			_sampleStart = reader.getModelLimits().getMinY();
		} else if (start > reader.getModelLimits().getMaxY()) {
			_sampleStart = reader.getModelLimits().getMaxY();
		} else
			_sampleStart = start;

		if (_notifyListener)
			fireDataKeyEvent(DataKeyEvent.SAMPLE_VALUE_START);
	}

	/**
	 * Sets the workflow that contains this process. 
	 * 
	 * @param workflow
	 *            SeismicWorkflow that contains this process.
	 */
	public void setWorkflow(SeismicWorkflow workflow) {

		super.setWorkflow(workflow);

		if (workflow != null) {
			SeismicReader reader = workflow.getDataLoader().getDataReader();

			if (reader != null) {
				Bound2D dataLimits = reader.getModelLimits();
				//init the values
				_sampleStart = dataLimits.getMinY();
				_sampleEnd = dataLimits.getMaxY();
				_sampleRate = reader.getMetaData().getSampleRate();
			}
		}
	}
}
