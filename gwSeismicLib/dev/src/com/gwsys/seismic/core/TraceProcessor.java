//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;

/**
 * The interface to do trace processing.
 */
public interface TraceProcessor {

	/**
	 * Process the trace with given trace id and sample range.
	 * @param mappedtraceId		the mapped trace ID.
	 * @param sampleRange	the required range of samples.
	 * @param traceDataOut		the output trace data.
	 *
	 * @return Status of processing. True if successful.
	 */
	public boolean process(int mappedtraceId, NumberRange sampleRange,
			TraceData traceDataOut);

	/**
	 * Sets the workflow associated with this processor.
	 *
	 * @param workflow the workflow associated with this processor.
	 */
	public void setWorkflow(SeismicWorkflow workflow);

	/**
	 * Returns the workflow associated with this processor.
	 *
	 * @return workflow associated with this processor
	 */
	public SeismicWorkflow getWorkflow();

	/**
	 * Sets the previous processor. 
	 * @param parent  the previous processor.
	 */
	public void setParentProcessor(TraceProcessor parent);

	/**
	 * Returns the previous processor.
	 * @return previous processor.
	 */
	public TraceProcessor getParentProcessor();
}
