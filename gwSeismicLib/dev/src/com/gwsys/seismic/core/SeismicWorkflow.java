//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//  
package com.gwsys.seismic.core;

import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;

/**
 * Defines the interface of seismic data visualization API.
 * It based on the requirement of bhpViewer.
 * 
 */
public interface SeismicWorkflow {

	/**
	 * Sets the data chooser in this workflow.
	 *
	 * @param dataSelector reader in this workflow.
	 */
	public void setDataSelector(DataChooser dataSelector);

	/**
	 * Sets Normalizer in this workflow.
	 * @param normalizer Normalizer in this workflow.
	 */
	public void setTraceNormalization(TraceNormalizer normalizer);

	/**
	 * Sets AGC in this workflow.
	 *
	 * @param agc AGC in this workflow.
	 */
	public void setTraceAGC(AutoGainController agc);

	/**
	 * Sets interpolater in this workflow.
	 *
	 * @param interpolator Interpolater in this workflow.
	 */
	public void setTraceInterpolator(TraceInterpolator interpolator);

	/**
	 * Returns rasterizer in this workflow.
	 *
	 * @return TraceRasterizer in this workflow.
	 */
	public TraceRasterizer getTraceRasterizer();

	/**
	 * Sets the rasterizer in this workflow.
	 *
	 * @param rasterizer  TraceRasterizer in this workflow.
	 */
	public void setTraceRasterizer(TraceRasterizer rasterizer);

	/**
	 * Sets the Interpretation in this workflow.
	 *
	 * @param process  Interpretation in this workflow.
	 */
	public void setInterpretation(Interpretation process);

	/**
	 * Returns the Interpretation in this workflow.
	 *
	 * @return  Interpretation in this workflow.
	 */
	public Interpretation getInterpretation();

	/**
	 * Gets the DataLoader.
	 * @return DataLoader.
	 */
	public DataLoader getDataLoader();

	/**
	 * Sets the new DataLoader.
	 * @param loader DataLoader.
	 */
	public void setDataLoader(DataLoader loader);
	
	/**
	 * Returns the limits of the the entire seimic data set.
	 *
	 * @return the limits of the the entire seimic data set.
	 */
	public Bound2D getRealModelLimits();

	/**
	 * Returns the limit of seismic data.
	 *
	 * @return the limit of seismic data.
	 */
	public Bound2D getVirtualModelLimits();

	/**
	 * Sets the bounding box limiting the display of seismic data.
	 *
	 * @param bound     the bounding box of the shown data.
	 * @param height	the height of the image.
	 */
	public void setBoundingBox(Bound2D bound, int height);

	/**
	 * Invalidates the workflow from given process.
	 *
	 * @param process the updated process.
	 */
	public void invalidate(TraceProcessor process);

	/**
	 * Retreives the sample values for given trace ID.
	 *
	 * @param traceId  Trace ID.
	 *
	 * @return an array of sample values or null.
	 */
	public float[] getTraceSamples(int traceId);

	/**
	 * Returns the status of highlighted trace.
	 *
	 * @param traceId the desired trace ID.
	 *
	 * @return <code>true</code> if trace is highlighted;
	 *         <code>false</code> otherwise.
	 */
	public boolean isTraceHighlighted(int traceId);

	/**
	 * Performs the process for given given traceId and range.
	 *
	 * @param traceId    the desired trace ID.
	 * @param sampleRange  the range of required samples.
	 * @param traceData  the destination trace data to store the samples.
	 *
	 * @return status of processing.
	 */
	public boolean process(int traceId, NumberRange sampleRange,
			TraceData traceData);
	
	/**
	 * Returns the direction that traces are drawn. 
	 * Such as LEFT_TO_RIGHT or RIGHT_TO_LEFT.
	 *
	 * @return the direction that traces are drawn. 
	 */
	public int getDirection();
		
}
