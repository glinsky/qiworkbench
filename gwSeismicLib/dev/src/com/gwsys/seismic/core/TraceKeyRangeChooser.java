//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.core;

import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.seismic.reader.SeismicReader;
import com.gwsys.seismic.util.NumberRange;
import com.gwsys.seismic.io.TraceHeader;

import java.util.Vector;

/**
 * This class defines the algorithm of reading part of trace by specified keys
 * in the trace header.
 *
 */
public class TraceKeyRangeChooser extends TraceSamplesChooser {

    /** Flag about applying gap */
    private boolean _applyGaps;

    /** The size of gaps */
    private int _gapSize = 5;

    /** This flag for setting is changed */
    private boolean firstKeyChanged = false;

    private boolean secondKeyChanged = false;

    /** The list of primary keys */
    private Vector<TraceGapKey> _primary = new Vector<TraceGapKey>(240, 240);

    /** The list of secondary keys */
    private Vector<TraceGapKey> _secondary = new Vector<TraceGapKey>(240, 240);

    /** key ID: first, second. */
    private int firstKey, secondKey;

    private NumberRange firstKeyRange;

    private NumberRange secondKeyRange;

    /**
     * constructor. <br>
     * By default, use TRACE_ID as first key and without second key.
     */
    public TraceKeyRangeChooser() {
        firstKey = TRACE_ID_AS_KEY;
        firstKeyRange = new NumberRange(0, 0, 1);
        secondKey = NO_SECONDARY_KEY;
        secondKeyRange = new NumberRange(-Double.MAX_VALUE, Double.MAX_VALUE, 0);

        _gapSize = 5;
        _applyGaps = false;
    }

    /**
     * Set if gaps should be applied.
     *
     * @param status
     *            <code>true</code> if applied; <code>false</code>
     *            otherwise.
     */
    public void applyGaps(boolean status) {
        _applyGaps = status;

        if (firstKey != TRACE_ID_AS_KEY)
            firstKeyChanged = true;
        else
            secondKeyChanged = true;

        if (getEventNotificationFlag())
            fireDataKeyEvent(DataKeyEvent.GAPS_APPLY);
    }

    /**
     * Returns size of gaps.
     *
     * @return size of gaps
     */
    public int getGapInTraces() {
        return _gapSize;
    }

    /**
     * Returns gap values associated with trace.
     *
     * @param traceIndex
     *            Index of Trace.
     *
     * @return number of gaps between traces.
     */
    public int getNumberOfGapTraces(int traceIndex) {
        int gaps = 0;
        if (secondKey != NO_SECONDARY_KEY) {
            gaps =  _secondary.get(traceIndex).getGaps();
        } else if (firstKey != TRACE_ID_AS_KEY) {
            gaps =  _primary.get(traceIndex).getGaps();
        }
        return gaps;
    }

    /**
     * Returns primary key ID.
     *
     * @return primary key ID
     */
    public int getPrimaryKey() {
        return firstKey;
    }

    /**
     * Returns the end value for primary key.
     *
     * @return the end value for primary key
     */
    public double getPrimaryKeyEnd() {
        if (firstKey == TRACE_ID_AS_KEY
                && firstKeyRange.getMax().doubleValue() != Double.MAX_VALUE)
            return firstKeyRange.getMax().doubleValue() + 1;
        else
            return firstKeyRange.getMax().doubleValue();
    }

    /**
     * Returns the start value for primary key.
     *
     * @return the start value for primary key.
     */
    public double getPrimaryKeyStart() {
        if (firstKey == TRACE_ID_AS_KEY
                && firstKeyRange.getMin().doubleValue() != -Double.MAX_VALUE)
            return firstKeyRange.getMin().doubleValue() + 1;
        else
            return firstKeyRange.getMin().doubleValue();
    }

    /**
     * Returns the step for primary key.
     *
     * @return the step for primary key.
     */
    public double getPrimaryKeyStep() {
        return firstKeyRange.getStep().doubleValue();
    }

    /**
     * Returns secondary key ID.
     * @return secondary key ID.
     */
    public int getSecondaryKey() {
        return secondKey;
    }

    /**
     * Returns the end value for secondary key.
     *
     * @return the end value for secondary key.
     */
    public double getSecondaryKeyEnd() {
        return secondKeyRange.getMax().doubleValue();
    }

    /**
     * Returns the start value for secondary key.
     *
     * @return the start value for secondary key.
     */
    public double getSecondaryKeyStart() {
        return secondKeyRange.getMin().doubleValue();
    }

    /**
     * Returns the step for secondary key.
     *
     * @return the step for secondary key.
     */
    public double getSecondaryKeyStep() {
        return secondKeyRange.getStep().doubleValue();
    }

    /**
     * Returns the trace header for this mapped trace ID.
     *
     * @param mappedTraceId
     *            mapped trace ID.
     *
     * @return TraceHeader.
     */
    public TraceHeader getTraceHeader(int mappedTraceId) {

        int realTraceId = virtualToReal(mappedTraceId);

        if (realTraceId != -1)
            return getWorkflow().getDataLoader().getDataReader()
                    .getTraceMetaData(realTraceId);
        return null;
    }

    /**
     * Returns the limits of seismic data in model space.
     *
     * @return the limits of seismic data in model space.
     */
    public Bound2D getVirtualModelLimits() {
        //reselect keys if changed
        selectTraces();

        double primaryKeyStart = firstKeyRange.getMin().doubleValue();
        double primaryKeyEnd = firstKeyRange.getMax().doubleValue();
        double primaryKeyStep = firstKeyRange.getStep().doubleValue();

        Bound2D bound = new Bound2D(0, 0, Double.MIN_VALUE, Double.MIN_VALUE);
        // TRACE_ID_AS_KEY
        if (firstKey == TRACE_ID_AS_KEY) {

            Bound2D realModelBound = getRealModelLimits();

            Bound2D real = new Bound2D(realModelBound.getMinX(), 0, realModelBound
                    .getMaxX(), 1);

            if ((real.intersects(primaryKeyStart, 0, primaryKeyEnd + 1, 1)
                    || (primaryKeyStart == real.getMaxX())
                    || (primaryKeyEnd == real.getMinX()))
                    && (primaryKeyStart <= primaryKeyEnd)) {

                if (secondKey != NO_SECONDARY_KEY) {
                    if (_secondary.size() != 0)
                        return new Bound2D(0, 0, _secondary.size() - 1,
                                (getEndSampleValue() - getStartSampleValue()));
                }
                // check first key
                else {
                    if (primaryKeyStep == 0) {
                        if ((primaryKeyStart == -Double.MAX_VALUE)
                                && (primaryKeyEnd == Double.MAX_VALUE))
                            return realModelBound;
                        else if (primaryKeyStart == -Double.MAX_VALUE)
                            return new Bound2D(
                                    0,
                                    0,
                                    Math.abs(primaryKeyEnd) + 1,
                                    (getEndSampleValue() - getStartSampleValue()));
                        else if (primaryKeyEnd == Double.MAX_VALUE)
                            return new Bound2D(
                                    0,
                                    0,
                                    realModelBound.getMaxX()
                                            - Math.abs(primaryKeyStart) + 1,
                                    (getEndSampleValue() - getStartSampleValue()));
                        else
                            return new Bound2D(
                                    0,
                                    0,
                                    Math.abs(primaryKeyEnd - primaryKeyStart) + 1,
                                    (getEndSampleValue() - getStartSampleValue()));
                    } else {
                        if (firstKeyRange.getMax().doubleValue() == Double.MAX_VALUE)
                            return new Bound2D(
                                    0,
                                    0,
                                    (int) (((realModelBound.getMaxX() - Math
                                            .abs(primaryKeyStart)) / primaryKeyStep) + 1),
                                    (getEndSampleValue() - getStartSampleValue()));
                        else
                            return new Bound2D(
                                    0,
                                    0,
                                    (int) ((Math.abs(primaryKeyEnd
                                            - primaryKeyStart) / primaryKeyStep) + 1),
                                    (getEndSampleValue() - getStartSampleValue()));
                    }
                }
            }
        }else {
            if (secondKey != NO_SECONDARY_KEY) {
                if (_secondary.size() != 0)
                    return new Bound2D(0, 0, _secondary.size(),
                            (getEndSampleValue() - getStartSampleValue()));
            } else {
                if (_primary.size() != 0)
                    return new Bound2D(0, 0, _primary.size(),
                            (getEndSampleValue() - getStartSampleValue()));
            }
        }
        return bound;
    }

    /**
     * Checks if gaps are applied.
     *
     * @return <code>true</code> if gaps are applied;
     *          <code>false</code> otherwise.
     */
    public boolean isGapApplied() {
        return _applyGaps;
    }

    /**
     * Selects trace by given primary keys.
     */
    private void selectTraceByPrimaryKey() {

        double value;
        int gapsNum = 0;

        SeismicReader reader = getWorkflow().getDataLoader().getDataReader();
        TraceHeader header;

        int modelStart = (int) reader.getModelLimits().getMinX();
        int modelEnd = (int) reader.getModelLimits().getMaxX();

        _primary.removeAllElements();

        double primaryKeyStart = firstKeyRange.getMin().doubleValue();
        double primaryKeyEnd = firstKeyRange.getMax().doubleValue();
        double primaryKeyStep = firstKeyRange.getStep().doubleValue();
        //find the start value in the trace
        if ((primaryKeyStart == -Double.MAX_VALUE) && (primaryKeyStep != 0)) {
            primaryKeyStart = Double.MAX_VALUE;
            for (int i = modelStart; i <= modelEnd; i++) {
                header = reader.getTraceMetaData(i);
                value = header.getFieldValue(firstKey).doubleValue();

                if (value < primaryKeyStart)
                    primaryKeyStart = value;
            }
        }

        double oldValue = -Float.MAX_VALUE;
        //search the traces in desired range
        for (int i = modelStart; i <= modelEnd; i++) {
            header = reader.getTraceMetaData(i);
            if (header == null || header.getFieldValue(firstKey)==null)
                continue;
            value = header.getFieldValue(firstKey).doubleValue();

            if ((value >= primaryKeyStart) && (value <= primaryKeyEnd)) {

                if (oldValue == -Float.MAX_VALUE)
                    oldValue = value;

                // If the trace locates on the step grid
                if (primaryKeyStep == 0 ||
                        ((value - primaryKeyStart) % primaryKeyStep) == 0) {
                    if ((value != oldValue) && _applyGaps) {
                        for (int k = 0; k < _gapSize; k++) {
                            _primary.add(new TraceGapKey(-1, gapsNum));
                            gapsNum++;
                        }
                        oldValue = value;
                    }
                    _primary.add(new TraceGapKey(i, gapsNum));
                }
            }
        }
    }

    /**
     * Selects traces by secondary keys.
     */
    private void selectTraceBySecondaryKey() {

        SeismicReader reader = getWorkflow().getDataLoader().getDataReader();
        double keyValue;
        _secondary.removeAllElements();

        // Check the first key
        if (firstKey == TRACE_ID_AS_KEY) {

            Bound2D modelBound = new Bound2D(getRealModelLimits().getMinX(), 0,
                    getRealModelLimits().getMaxX(), 1);

            if (modelBound.intersects(firstKeyRange.getMin().doubleValue(), 0,
                    firstKeyRange.getMax().doubleValue(), 1)
                    && (firstKeyRange.getMin().doubleValue()
                            <= firstKeyRange.getMax().doubleValue())) {

                Bound2D range = modelBound.intersection(new Bound2D(firstKeyRange
                        .getMin().doubleValue(), 0, firstKeyRange.getMax()
                        .doubleValue(), 1));
                int min = (int) range.getMinX();
                int max = (int) range.getMaxX();
                int step = ((firstKeyRange.getStep().doubleValue() == 0)
                        ? 1
                        : firstKeyRange.getStep().intValue());

                if ((secondKeyRange.getMin().doubleValue() == -Double.MAX_VALUE)
                        && (secondKeyRange.getStep().doubleValue() != 0)) {
                    secondKeyRange.setMin(Double.MAX_VALUE);
                    for (int i = min; i <= max; i += step) {
                        keyValue = reader.getTraceMetaData(i).getFieldValue(secondKey)
                                .doubleValue();

                        if (keyValue < secondKeyRange.getMin().doubleValue())
                            secondKeyRange.setMin(keyValue);
                    }
                }

                for (int i = min; i <= max; i += step) {
                    keyValue = reader.getTraceMetaData(i).getFieldValue(secondKey)
                                .doubleValue();

                    if ((keyValue >= secondKeyRange.getMin().doubleValue())
                            && (keyValue <= secondKeyRange.getMax().doubleValue())) {
                        if (secondKeyRange.getStep().intValue() == 0 ||
                                ((keyValue - secondKeyRange.getMin().doubleValue())
                                        % secondKeyRange.getStep().doubleValue()) == 0)
                            _secondary.add(new TraceGapKey(i, 0));
                    }
                }
            }
        }else {
            int realTraceId;

            // Check the start value
            if ((secondKeyRange.getMin().doubleValue() == -Double.MAX_VALUE)
                    && (secondKeyRange.getStep().intValue() != 0)) {
                secondKeyRange.setMin(Double.MAX_VALUE);
                for (int i = 0; i <= _primary.size(); i++) {
                    realTraceId = _primary.get(i).getTraceId();
                    if (realTraceId == -1)
                        continue;

                    keyValue = reader.getTraceMetaData(realTraceId).
                                getFieldValue(secondKey).doubleValue();

                    if (keyValue < secondKeyRange.getMin().doubleValue())
                        secondKeyRange.setMin(keyValue);
                }
            }

            // add traces
            for (int i = 0; i < _primary.size(); i++) {
                realTraceId = _primary.get(i).getTraceId();
                if (realTraceId == -1 && _secondary.size() > 0){
                    _secondary.add(_primary.get(i).clone());
                    continue;
                }

                keyValue = reader.getTraceMetaData(realTraceId)
                            .getFieldValue(secondKey).doubleValue();

                if (keyValue >= secondKeyRange.getMin().doubleValue()
                        && keyValue <= secondKeyRange.getMax().doubleValue()) {
                    if (secondKeyRange.getStep().intValue() == 0 ||
                            ((keyValue - secondKeyRange.getMin().doubleValue()) %
                                    secondKeyRange.getStep().doubleValue()) == 0)
                        _secondary.add(_primary.get(i).clone());

                }
            }
            // remove last one without id value
            while (_secondary.size() > 0
                    && _secondary.get(_secondary.size() - 1).getTraceId() == -1) {

                _secondary.removeElementAt(_secondary.size() - 1);
            }
        }
    }

    /**
     * Sets new gap value.
     *
     * @param size
     *            new gap value.
     */
    public void setGapInTraces(int size) {
        _gapSize = size;

        if (firstKey != TRACE_ID_AS_KEY)
            firstKeyChanged = true;
        else
            secondKeyChanged = true;

        if (getEventNotificationFlag())
            fireDataKeyEvent(DataKeyEvent.GAPS_SIZE);
    }

    /**
     * Sets primary key ID with given field ID.
     *
     * @param fieldId
     *            ID of trace header field to be set as primary key.
     */
    public void setPrimaryKey(int fieldId) {
        firstKey = fieldId;

        if (firstKey != TRACE_ID_AS_KEY)
            firstKeyChanged = true;
        else
            secondKeyChanged = true;

        if (getEventNotificationFlag())
            fireDataKeyEvent(DataKeyEvent.PRIMARY_KEY);
    }

    /**
     * Sets the end value for primary key.
     *
     * @param end
     *            the end value for primary key.
     */
    public void setPrimaryKeyEnd(double end) {

        int traceStart = (int) getWorkflow().getDataLoader().getDataReader()
                .getModelLimits().getMinX();
        int traceEnd = (int) getWorkflow().getDataLoader().getDataReader()
                .getModelLimits().getMaxX();

        if (firstKey == TRACE_ID_AS_KEY) {
            end -= 1.0;
            if (end < traceStart)
                firstKeyRange.setMax(traceStart);
            else if (end > traceEnd)
                firstKeyRange.setMax(traceEnd);
            else
                firstKeyRange.setMax(end);

            secondKeyChanged = true;
        } else {
            firstKeyRange.setMax(end);
            firstKeyChanged = true;
        }

        if (getEventNotificationFlag())
            fireDataKeyEvent(DataKeyEvent.PRIMARY_KEY_END);
    }

    /**
     * Sets the start value for primary key.
     *
     * @param start
     *            the start value for primary key.
     */
    public void setPrimaryKeyStart(double start) {

        int traceStart = (int) getWorkflow().getDataLoader().getDataReader()
                .getModelLimits().getMinX();
        int traceEnd = (int) getWorkflow().getDataLoader().getDataReader()
                .getModelLimits().getMaxX();

        if (firstKey == TRACE_ID_AS_KEY) {
            start -= 1.0;
            if (start < traceStart)
                firstKeyRange.setMin(traceStart);
            else if (start > traceEnd)
                firstKeyRange.setMin(traceEnd);
            else
                firstKeyRange.setMin(start);

        } else {
            firstKeyRange.setMin(start);
        }
        firstKeyChanged = true;
        if (getEventNotificationFlag())
            fireDataKeyEvent(DataKeyEvent.PRIMARY_KEY_START);
    }

    /**
     * Sets the step for primary key.
     *
     * @param step
     *            the step for primary key.
     */
    public void setPrimaryKeyStep(double step) {

        if (firstKey == TRACE_ID_AS_KEY) {

            if (step == 0)
                firstKeyRange.setStep(1);
            else
                firstKeyRange.setStep(step);

            secondKeyChanged = true;
        } else {
            firstKeyRange.setStep(step);
            firstKeyChanged = true;
        }

        if (getEventNotificationFlag())
            fireDataKeyEvent(DataKeyEvent.PRIMARY_KEY_STEP);
    }

    /**
     * Sets secondary key ID with the given field ID.
     *
     * @param fieldId
     *            ID of trace header field to be set as secondary key.
     */
    public void setSecondaryKey(int fieldId) {

        if (fieldId != TRACE_ID_AS_KEY) {
            secondKey = fieldId;
            secondKeyChanged = true;

            if (secondKey == NO_SECONDARY_KEY) {
                secondKeyRange.setMin(-Double.MAX_VALUE);
                secondKeyRange.setMax(Double.MAX_VALUE);
                secondKeyRange.setStep(0);
            }

            if (getEventNotificationFlag())
                fireDataKeyEvent(DataKeyEvent.SECONDARY_KEY);
        }
    }

    /**
     * Sets the end value for secondary key.
     *
     * @param end
     *            the end value for secondary key.
     */
    public void setSecondaryKeyEnd(double end) {

        if (secondKey != NO_SECONDARY_KEY) {
            secondKeyRange.setMax(end);
            secondKeyChanged = true;

            if (getEventNotificationFlag())
                fireDataKeyEvent(DataKeyEvent.SECONDARY_KEY_END);
        }
    }

    /**
     * Sets the start value for secondary key.
     * @param start
     *            the start value for secondary key.
     */
    public void setSecondaryKeyStart(double start) {

        if (secondKey != NO_SECONDARY_KEY) {
            secondKeyRange.setMin(start);
            secondKeyChanged = true;

            if (getEventNotificationFlag())
                fireDataKeyEvent(DataKeyEvent.SECONDARY_KEY_START);
        }
    }

    /**
     * Sets the step for secondary key.
     *
     * @param step
     *            the step for secondary key.
     */
    public void setSecondaryKeyStep(double step) {

        if (secondKey != NO_SECONDARY_KEY) {
            secondKeyRange.setStep(step);
            secondKeyChanged = true;

            if (getEventNotificationFlag())
                fireDataKeyEvent(DataKeyEvent.SECONDARY_KEY_STEP);
        }
    }

    /**
     * Sets the workflow that contains this process. I
     *
     * @param workflow
     *            workflow that contains this process.
     */
    public void setWorkflow(SeismicWorkflow workflow) {

        super.setWorkflow(workflow);

        if (workflow != null) {
            SeismicReader reader = workflow.getDataLoader().getDataReader();

            if (reader != null) {
                Bound2D dataLimits = reader.getModelLimits();

                if (firstKey == TRACE_ID_AS_KEY
                        && (firstKeyRange.getMin().intValue() == 0)
                        && (firstKeyRange.getMax().intValue() == 0)) {
                    firstKeyRange.setMin(dataLimits.getMinX());
                    firstKeyRange.setMax(dataLimits.getMaxX());
                }
            }
        }
    }

    /**
     * Returns the real trace number from mapped trace id.
     *
     * @param mappedTraceId
     *            mapped trace ID to be converted.
     *
     * @return the real trace number.
     */
    public int virtualToReal(int mappedTraceId) {
        selectTraces();
        int realTraceId = -1;

        if (firstKey == TRACE_ID_AS_KEY) {

            Bound2D rcRealModel = getRealModelLimits();

            Bound2D real = new Bound2D(rcRealModel.getMinX(), 0, rcRealModel
                    .getMaxX(), 1);

            if (real.intersects(firstKeyRange.getMin().doubleValue(), 0,
                    firstKeyRange.getMax().doubleValue() + 1, 1)
                    || (firstKeyRange.getMin().doubleValue() == real.getMaxX())
                    || (firstKeyRange.getMax().doubleValue() == real.getMinX())) {

                if (secondKey != NO_SECONDARY_KEY) {
                    if (_secondary.size() == 0)
                        return realTraceId;
                    if (mappedTraceId >= _secondary.size()
                            || mappedTraceId < 0)
                        return -1;
                    TraceGapKey key = _secondary.get(mappedTraceId);
                    if (key != null)
                        realTraceId = key.getTraceId();
                }else {
                    if (firstKeyRange.getStep().doubleValue() == 0) {
                        if (firstKeyRange.getMin().doubleValue() == -Double.MAX_VALUE)
                            realTraceId = mappedTraceId;
                        else
                            realTraceId = firstKeyRange.getMin().intValue()
                                    + mappedTraceId;
                    } else
                        realTraceId = (int) (firstKeyRange.getMin()
                                .doubleValue() + firstKeyRange.getStep()
                                .doubleValue()* mappedTraceId);
                }
            } else
                return realTraceId;
        }else { //trace id is not the kay
            if (secondKey != NO_SECONDARY_KEY) {
                if (_secondary.size() == 0)
                    return realTraceId;
                if (mappedTraceId >= _secondary.size() || mappedTraceId < 0)
                    return -1;
                TraceGapKey key = _secondary.get(mappedTraceId);
                realTraceId = key.getTraceId();
            } else {
                if (_primary.size() == 0)
                    return realTraceId;
                if (mappedTraceId >= _primary.size() || mappedTraceId < 0)
                    return -1;
                realTraceId = _primary.get(mappedTraceId).getTraceId();
            }
        }

        return realTraceId;
    }

    private void selectTraces(){

        if (firstKeyChanged) {
            firstKeyChanged = false;
            secondKeyChanged = false;

            selectTraceByPrimaryKey();
            if (secondKey != NO_SECONDARY_KEY)
                selectTraceBySecondaryKey();
        } else if (secondKeyChanged) {
            secondKeyChanged = false;
            if (secondKey != NO_SECONDARY_KEY)
                selectTraceBySecondaryKey();
        }

    }
}
