//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

import com.gwsys.gw2d.util.Bound2D;

/**
 * Abstract Implementation of the DataPainter to draw the traces.
 * 
 * @author Hua
 * 
 */
public abstract class TracePainter implements DataPainter {

	/** the variable of bound: data and model. */
	private Bound2D _dataBound, _modelBound;

	/** the image height. */
	private int _imageHeight;

	/** Drawing direction. */
	protected int _direction = TraceImage.DRAW_TRACE_LEFT_TO_RIGHT;

	/** SeismicWorkflow. */
	private SeismicWorkflow _workflow;

	/** TraceRasterizer */
	private TraceRasterizer _rasterizer;

	//************Methods*****************
	/**
	 * Returns data bounding box.
	 * 
	 * @return data bounding box.
	 */
	protected Bound2D getDataBound() {
		return _dataBound;
	}

	/**
	 * Gets the destination height.
	 * 
	 * @return destination height.
	 */
	protected int getDestinationHeight() {
		return _imageHeight;
	}

	/**
	 * Returns model bounding box.
	 * 
	 * @return model bounding box.
	 */
	protected Bound2D getModelBound() {
		return _modelBound;
	}

	/**
	 * Returns the seismic workflow.
	 * 
	 * @return the seismic workflow.
	 */
	protected SeismicWorkflow getWorkflow() {
		return _workflow;
	}

	/**
	 * Returns the trace rasterizer.
	 * 
	 * @return the trace rasterizer.
	 */
	protected TraceRasterizer getRasterizer() {
		return _rasterizer;
	}

	/**
	 * Sets the bounding box of data.
	 * @param bound Bound of data in user space.
	 * @param height Height of Image Area.
	 */
	public void setBoundingBox(Bound2D bound, int height) {

		double sampleRate = _workflow.getDataLoader().getDataReader()
				.getMetaData().getSampleRate();
		double sampleStart = _workflow.getDataLoader().getDataReader()
				.getMetaData().getSampleStart();

		_modelBound = bound;

		_dataBound = new Bound2D(_modelBound.getMinX(),
				((_modelBound.getMinY() - sampleStart) / sampleRate), Math
						.ceil(_modelBound.getMaxX()),
				((_modelBound.getMaxY() - sampleStart) / sampleRate));

		_imageHeight = height;
	}

	/**
	 * Sets up the direction of image: RIGHT_TO_LEFT and LEFT_TO_RIGHT.
	 * 
	 * @param direction 
	 *            one of RIGHT_TO_LEFT and LEFT_TO_RIGHT.
	 */
	public void setDirection(int direction) {
		_direction = direction;
	}

	/**
	 * Sets Seismic Workflow.
	 * 
	 * @param workflow
	 *            SeismicWorkflow
	 */
	protected void setWorkflow(SeismicWorkflow workflow) {
		_workflow = workflow;
	}

	/**
	 * Sets Trace rasterizer.
	 * 
	 * @param rasterizer
	 *            Trace rasterizer
	 */
	protected void setRasterizer(TraceRasterizer rasterizer) {
		_rasterizer = rasterizer;
	}

}
