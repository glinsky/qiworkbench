//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

import com.gwsys.gw2d.util.GWUtilities;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;

/**
 * Defines the algorithm to do interpolation of trace data.
 * Only linear algorithm is implemented in this phase.
 * 
 * API is copied from bhpViewer.
 */
public class TraceInterpolator extends AbstractProcessor {

	public static final int STEP = 2;
	
	private NumberRange _inputRange = new NumberRange();

	private NumberRange _outputRange = new NumberRange();
	private int interpolatedType = 0;

	/** scaling factor*/
	private float resamplingFactor = 1;
	/**
	 * Constructs a trace interpolator.
	 */
	public TraceInterpolator() {
	}

	/**
	 * The interpolation algorithm if the interpolation factor < 1.0.
	 * This implementation preserves maximum and minimum values in preceding samples.
	 * @param src_start int the start index of source array
	 * @param src_end int the end index of source array
	 * @param src float the source array
	 * @param dst_start int the start index of destination array
	 * @param dst_end int the end index of destination array
	 * @param dst float the destination array
	 * @param offs double the offset of indexes of source array along sample axis
	 */
	protected void doDecimation(int src_start, int src_end, float[] src,
			int dst_start, int dst_end, float[] dst, double offs) {
		double zero = 1.e-15;
		double distance = 0.0;
		double newAmp = 0.0;
		double newAmp1 = 0.0;
		double newAmp2 = 0.0;
		double newAmpPrev = 0.0;
		double newAmpPrev1 = Double.MAX_VALUE;
		double newAmpPrev2 = Double.MIN_VALUE;
		boolean usePrev = false;

		double increment = 1.0 / getResamplingFactor();

		double src_pos = src_start + offs;
		int ioffs = (int) offs;
		int newSample1 = (int) src_pos;
		int prevSample = (int) (src_pos - increment) + 1;
		if (prevSample >= 0) {
			newSample1 = prevSample;
		}
		int newSample2 = (int) src_pos;

		// use previous sample or not
		if ((newSample2 >= ioffs) && ((src_pos - increment) < 0)) {
			usePrev = true;
		}

		for (int i = dst_start; i < dst_end; i++) {

			newSample2 = (int) src_pos;

			if (newSample2 < src_start)
				newSample2 = src_start;

			if (newSample2 > src_end) {
				newSample2 = src_end;
			}

			// get values
			newAmp = newAmp1 = newAmp2 = src[newSample2];

			distance = src_pos - newSample2;

			// find local min and max
			if ((newSample2 < src_end) && (distance > zero)) {

				newAmp2 = newAmp + distance * (src[newSample2 + 1] - newAmp);

				if (newAmp > newAmp2) {
					newAmp1 = newAmp2;
					newAmp2 = newAmp;
				} else {
					newAmp1 = newAmp;
				}
				
				if (usePrev) {
					
					for (int j = newSample2 - ioffs; j < newSample2; j++) {
						newAmpPrev = src[j];
						if (newAmpPrev < newAmpPrev1)
							newAmpPrev1 = newAmpPrev;
						if (newAmpPrev > newAmpPrev2)
							newAmpPrev2 = newAmpPrev;
					}
					if (newAmpPrev1 < newAmp1)
						newAmp1 = newAmpPrev1;
					if (newAmpPrev2 > newAmp2)
						newAmp2 = newAmpPrev2;
					usePrev = false;
				}
			}

			if (newSample2 != newSample1) {
				for (int j = newSample1; j <= newSample2; j++) {

					newAmp = src[j];

					if (newAmp < newAmp1)
						newAmp1 = newAmp;
					if (newAmp > newAmp2)
						newAmp2 = newAmp;
				}
			}

			dst[i] = (float) ((newAmp1 > 0 ? newAmp1 : -newAmp1) > (newAmp2 > 0 ? newAmp2
					: -newAmp2) ? newAmp1 : newAmp2);

			newSample1 = newSample2 + 1;
			src_pos += increment;
		}
	}

	/**
	 * The interpolation algorithm.
	 * 
	 * @param srcSample Sample float array.
	 * @param srcStart The start index of source array
	 * @param srcEnd The end index of source array
	 * 
	 * @param desSample Sample array after interpolation
	 * @param desStart The start index of destination array
	 * @param desEnd The end index of destination array
	 * @param offset The offset of sample start in source array.
	 */
	protected void doInterpolation(float[] srcSample, int srcStart, int srcEnd,
			float[] desSample, int desStart, int desEnd, double offset){
		double samStep = 1.0 / getResamplingFactor();
		
		double realLocation = srcStart + offset;
		
		desSample[desStart] = srcSample[srcStart];

		// do linear interpolation
		int index = 0;
		float y0, y1;

		int last_samples_start;

		if (srcSample.length > 2) {
			int max_sample = getMaxSample(srcEnd - 1 - srcStart - offset,
					desStart, desEnd, samStep);
			last_samples_start = max_sample + 1;

			for (int i = desStart; i <= max_sample; i++) {

				index = (int) realLocation;

				desSample[i] = (float) (srcSample[index] + (realLocation - index)
						* (srcSample[index + 1] - srcSample[index]));

				realLocation += samStep;
			}
		} else {
			last_samples_start = desStart;
		}

		for (int i = last_samples_start; i <= desEnd; i++) {

			index = (int) realLocation;
			y0 = (index <= srcEnd) ? srcSample[index] : srcSample[srcEnd];
			y1 = (index + 1 >= srcStart && index + 1 <= srcEnd) ? srcSample[index + 1]
					: y0;
			desSample[i] = (float) (y0 + (realLocation - index) * (y1 - y0));

			realLocation += samStep;
		}

	}

	/**
	 * overrid the interpolation algorithm.
	 */
	protected void doStepInterpolation(float[] srcSample, int srcStart, int srcEnd,
			float[] desSample, int desStart, int desEnd, double offset){
		if (desEnd <= 0)
			return;
		double samLocation = srcStart + offset;
		
		double samStep = 1/this.getResamplingFactor();
		int numSamples = getMaxSample(srcEnd - srcStart - offset,
				desStart, desEnd, samStep);
		
		for (int i =0; i<=numSamples; i++ ){
			desSample[i] = srcSample[(int)samLocation];
			samLocation += samStep;
		}
		
		//rest samples
		for (int i = numSamples+1; i <= desEnd; i++ ){
			desSample[i] = srcSample[srcEnd];
		}
	}

	/**
	 * Gets the type of interpolation.
	 * @return type of interpolation.
	 */
	public int getInterpolationType() {
		return interpolatedType;
	}

	protected int getMaxSample(double numSam, int desStart,
			int desEnd, double step) {

		int max = (int) Math.floor(((numSam) / step)+ desStart - 1);

		if (max > desEnd)
			max = desEnd;

		return (max >= 0) ? max : 0;
	}
	
	/**
	 * Gets the resampling factor.
	 *
	 * @return the resampling factor
	 */
	public float getResamplingFactor() {
		return resamplingFactor;
	}

	/**
	 * Apply the interpolation process for the specified trace.
	 *
	 * @param traceId    the desired trace ID to be processed
	 * @param sampleRange  the range of required samples from this process
	 * @param traceDataOut  the output trace data for the trace process
	 *
	 * @return status of processing.
	 */
	public synchronized boolean process(int traceId, NumberRange sampleRange,
			TraceData traceDataOut) {

		NumberRange traceRange = null;
		//if no interpolation is needed, just copy samples
		if ((Math.abs(getResamplingFactor() - 1) < 0.00001)) {

			if (!((getParentProcessor()).process(traceId, traceRange, traceDataOut)))
				return false;
			//check the sample range
			if (sampleRange != null && traceRange == null) {
				int start = sampleRange.getMin().intValue();
				if (start != 0&&(traceDataOut.getNumAppliedSamples()- start)>0) {

					float[] destSamples = traceDataOut.getSamples();
					
					System.arraycopy(destSamples, sampleRange.getMin()
							.intValue(), destSamples, 0, traceDataOut
							.getNumAppliedSamples()-start);

					traceDataOut.setNumAppliedSamples(traceDataOut
							.getNumAppliedSamples()-start);
				}
			}
			return true;
		}

		// use buffer trace data
		if (!(getParentProcessor().process(traceId, traceRange,	getTraceData())))
			return false;

		double offset = 0;
		
		// check trace range
		if (traceRange == null) {
			_inputRange.setRange(0, getTraceData().getNumAppliedSamples() - 1);

			if (sampleRange == null){
				int start = (int) Math.floor(_inputRange.getMin().intValue() * 
						getResamplingFactor());
				int end = (int) Math.ceil(_inputRange.getMax().intValue() * 
						getResamplingFactor());

				_outputRange.setRange(start, end);
			}else
				_outputRange.setRange(sampleRange);

			offset = (_outputRange.getMin().floatValue() / getResamplingFactor())
					- _inputRange.getMin().intValue();
		} else {
			if (getTraceData().getNumAppliedSamples() < _inputRange.getRange())
				_inputRange.setMax(_inputRange.getMin().intValue()
						+ getTraceData().getNumAppliedSamples() - 1);

			_inputRange.shift(0);
		}

		_outputRange.shift(0);

		// do resampling
		float[] inputData = getTraceData().getSamples();

		traceDataOut.setNumAppliedSamples(_outputRange.getRange());

		float[] outputData = traceDataOut.getSamples();

		if (getResamplingFactor() < 1)
			doDecimation(_inputRange.getMin().intValue(), _inputRange.getMax()
					.intValue(), inputData, _outputRange.getMin().intValue(),
					_outputRange.getMax().intValue(), outputData, offset);
		else{
			if (this.getInterpolationType()==STEP)
				doStepInterpolation(inputData, _inputRange.getMin().intValue(), 
						_inputRange.getMax().intValue(),  outputData,
						_outputRange.getMin().intValue(), 
						_outputRange.getMax().intValue(), 
						offset);
			else
			doInterpolation(inputData, _inputRange.getMin().intValue(), 
					_inputRange.getMax().intValue(),  outputData,
					_outputRange.getMin().intValue(), 
					_outputRange.getMax().intValue(), 
					offset);
		}
		traceDataOut.setUniqueKey(getTraceData().getUniqueKey());

		return true;
	}
	

	/**
	 * Sets the new type of interpolation.
	 *
	 * @param type New type of interpolation.
	 */
	public void setInterpolationType(int type) {
		interpolatedType = type;
	}

	/**
	 * Sets the resampling factor.
	 *
	 * @param factor the resampling factor
	 */
	public void setResamplingFactor(float factor) {
		if (factor <= 0) {
			GWUtilities.warning("The factor must be positive.");
			return;
		}
		
		if (factor != getResamplingFactor()&&getWorkflow()!=null ){
			resamplingFactor = factor;
			getWorkflow().invalidate(this);	
		}
		resamplingFactor = factor;
	}
}
