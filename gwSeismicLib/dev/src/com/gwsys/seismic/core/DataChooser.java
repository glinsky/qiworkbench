//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.seismic.io.TraceHeader;

/**
 * Declares interface for creation of workflow's process responsible for
 * selection of traces from seismic data storage. The data is selected based on
 * the two keys related to the fields from trace header.<br>
 * By default, the trace id is the primary key.
 * The API is based on the bhpViewer.
 * 
 */
public abstract class DataChooser extends AbstractProcessor {

	/** This value is default value for secondary key id. */
	public static final int NO_SECONDARY_KEY = -2;

	/** This value is default value for primary key. */
	public static final int TRACE_ID_AS_KEY = -1;

	/**
	 * Registers data listener.
	 * 
	 * @param listener
	 *            DataKeyListener
	 */
	public abstract void addDataSelectorListener(DataKeyListener listener);

	/**
	 * Set if gaps should be applied.
	 * </p>
	 * 
	 * @param status
	 *            of gap.
	 */
	public abstract void applyGaps(boolean status);

	/**
	 * Returns the ending value of the trace sample.
	 * 
	 * @return the ending value of the trace sample.
	 */
	public abstract double getEndSampleValue();

	/**
	 * Returns size of gaps.
	 * </p>
	 * 
	 * @return size of gaps
	 */
	public abstract int getGapInTraces();

	/**
	 * Returns the number of gaps with given mapped trace ID.<br>
	 * 
	 * @param traceId
	 *            mapped trace ID.
	 * 
	 * @return the number of gaps.
	 */
	public abstract int getNumberOfGapTraces(int traceId);

	/**
	 * Returns primary key ID.
	 * </p>
	 * 
	 * @return primary key ID
	 */
	public abstract int getPrimaryKey();

	/**
	 * Returns the end value for primary key.
	 * </p>
	 * 
	 * @return the end value for primary key
	 */
	public abstract double getPrimaryKeyEnd();

	/**
	 * Returns the start value for primary key.
	 * </p>
	 * 
	 * @return the start value for primary key
	 */
	public abstract double getPrimaryKeyStart();

	/**
	 * Returns the step for primary key.
	 * </p>
	 * 
	 * @return the step for primary key
	 */
	public abstract double getPrimaryKeyStep();

	/**
	 * Returns the limits of seismic data in model space before selection.
	 * </p>
	 * 
	 * @return the limits of seismic data in model space before selection
	 */
	public abstract Bound2D getRealModelLimits();

	/**
	 * Returns secondary key ID.
	 * </p>
	 * 
	 * @return secondary key ID
	 */
	public abstract int getSecondaryKey();

	/**
	 * Returns the end value for secondary key.
	 * </p>
	 * 
	 * @return the end value for secondary key
	 */
	public abstract double getSecondaryKeyEnd();

	/**
	 * Returns the start value for secondary key.
	 * </p>
	 * 
	 * @return the start value for secondary key
	 */
	public abstract double getSecondaryKeyStart();

	/**
	 * Returns the step for secondary key.
	 * </p>
	 * 
	 * @return the step for secondary key
	 */
	public abstract double getSecondaryKeyStep();

	/**
	 * Returns the starting value of the trace sample.
	 * 
	 * @return the starting value of the trace sample.
	 */
	public abstract double getStartSampleValue();

	/**
	 * Returns the trace binary header for this virtual trace ID.
	 * 
	 * @param virtualTraceId
	 *            The trace ID.
	 * @return the trace binary header.
	 */
	public abstract TraceHeader getTraceHeader(int virtualTraceId);

	/**
	 * Returns the limits of selected seismic data in model space.
	 * 
	 * @return the limits of selected seismic data in model space.
	 */
	public abstract Bound2D getVirtualModelLimits();

	/**
	 * Checks if gap value is applied.
	 * </p>
	 * 
	 * @return flag
	 */
	public abstract boolean isGapApplied();

	/**
	 * Removes data listener.
	 * 
	 * @param listener
	 *            DataKeyListener
	 */
	public abstract void removeDataSelectorListener(DataKeyListener listener);

	/**
	 * Sets the ending value of the trace sample.
	 * 
	 * @param end
	 *            the ending value of the trace sample.
	 */
	public abstract void setEndSampleValue(double end);

	/**
	 * Sets new gap value in traces.
	 * 
	 * @param size
	 *            new size of gaps.
	 */
	public abstract void setGapInTraces(int size);

	/**
	 * Sets the listener notification flag.
	 * 
	 * @param status
	 *            <code>true</code> to notify the listeners;
	 *            <code>false</code> otherwise
	 */
	public abstract void setNotify(boolean status);

	/**
	 * Sets primary key ID to the given trace header field ID. In order to use
	 * trace number as primary key the constant <code>TRACE_ID_AS_KEY</code>
	 * can be passed as an argument.<br>
	 * Normally, it is the offset of bytes in trace header.
	 * 
	 * @param key
	 *            ID of trace header field to be set as primary key.
	 */
	public abstract void setPrimaryKey(int key);

	/**
	 * Sets the end value for primary key.<br>
	 * 
	 * @param end
	 *            the end value for primary key
	 */
	public abstract void setPrimaryKeyEnd(double end);

	/**
	 * Sets the start value for primary key.<br>
	 * 
	 * @param start
	 *            the start value for primary key
	 */
	public abstract void setPrimaryKeyStart(double start);

	/**
	 * Sets the step for primary key.<br>
	 * 
	 * @param step
	 *            the step for primary key
	 */
	public abstract void setPrimaryKeyStep(double step);

	/**
	 * Sets secondary key ID to the given trace header field ID.
	 * 
	 * @param key
	 *            ID of trace header field to be set as secondary key
	 */
	public abstract void setSecondaryKey(int key);

	/**
	 * Sets the end value for secondary key.<br>
	 * 
	 * @param end
	 *            the end value for secondary key
	 */
	public abstract void setSecondaryKeyEnd(double end);

	/**
	 * Sets the start value for secondary key.<br>
	 * 
	 * @param start
	 *            the start value for secondary key
	 */
	public abstract void setSecondaryKeyStart(double start);

	/**
	 * Sets the step for secondary key.<br>
	 * 
	 * @param step
	 *            the step for secondary key
	 */
	public abstract void setSecondaryKeyStep(double step);

	/**
	 * Sets the starting value of the trace sample.<br>
	 * 
	 * @param start
	 *            the starting value of the trace sample.
	 */
	public abstract void setStartSampleValue(double start);

	/**
	 * Returns the real trace number from mapped trace id.
	 * 
	 * @param mappedTraceId
	 *            mapped trace ID.
	 * 
	 * @return the real trace number.
	 */
	public abstract int virtualToReal(int mappedTraceId);

}
