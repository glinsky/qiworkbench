//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//  
package com.gwsys.seismic.core;

import java.util.Hashtable;

/**
 * Implements the interface Interpretation.
 * User can add user-defined processor by calling addProcessor().
 * @author Hua
 *
 */
public class ProcessingList implements Interpretation {

	private Hashtable<String, AbstractProcessor> processingTable = 
		new Hashtable<String, AbstractProcessor>();
	private static final String KEY_NORMALIZATION="NOR";
	private static final String KEY_AGC="AGC";
	private static final String KEY_INTERPOLATION="POL";
	/**
	 * Constructor.
	 */
	public ProcessingList() {
		processingTable.put(KEY_NORMALIZATION,  new TraceNormalizer());
		processingTable.put(KEY_INTERPOLATION,  new TraceInterpolator());

	}

	/**
	 * @inheritDoc
	 */
	public TraceNormalizer getTraceNormalization() {
		
		return (TraceNormalizer)processingTable.get(KEY_NORMALIZATION);
	}

	/**
	 * @inheritDoc
	 */
	public void setTraceNormalization(TraceNormalizer normalization) {
		processingTable.put(KEY_NORMALIZATION, normalization);

	}

	/**
	 * @inheritDoc
	 */
	public AutoGainController getTraceAGC() {
		return (AutoGainController)processingTable.get(KEY_AGC);
	}

	/**
	 * @inheritDoc
	 */
	public void setTraceAGC(AutoGainController agcProcess) {
		
		processingTable.put(KEY_AGC, agcProcess);
	}

	/**
	 * @inheritDoc
	 */
	public TraceInterpolator getTraceInterpolator() {
		return (TraceInterpolator)processingTable.get(KEY_INTERPOLATION);
	}

	/**
	 * @inheritDoc
	 */
	public void setTraceInterpolator(TraceInterpolator interpolator) {
		processingTable.put(KEY_INTERPOLATION, interpolator);
	}
	
	/**
	 * Adds user-defined processor with key.
	 */
	public void addProcessor(String key, AbstractProcessor processor){
		processingTable.put(key, processor);
	}
	
	/**
	 * Gets user-defined processor with key.
	 * @return user-defined processor.
	 */
	public AbstractProcessor getProcessor(String key){
		return processingTable.get(key);
	}
}
