//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

import java.util.Vector;

import com.gwsys.seismic.reader.SeismicReader;

/**
 * Implements the DataLoader interface.
 * 
 * @author Hua
 * 
 */
public class SeismicLoader implements DataLoader {

	private SeismicReader reader;
	private DataChooser viewport;
	
	/**
	 * Constructor.
	 * Creates instance of TraceKeyRangeChooser as default.
	 */
	public SeismicLoader() {
		viewport = new TraceKeyRangeChooser();
	}

	/**
	 * Updates the Seismic Reader.
	 * @param reader Seismic Reader.
	 * @see SeismicReader
	 */
	public void setDataReader(SeismicReader reader) {
		this.reader = reader;
		if (viewport!=null)
			viewport.setParentProcessor(this.reader);
		if (reader!=null&&reader.getWorkflow()!=null)
			this.reader.getWorkflow().invalidate(reader);
	}

	/**
	 * Gets the Seismic Reader.
	 * @return SeismicReader
	 */
	public SeismicReader getDataReader() {
		return reader;
	}

	/**
	 * Sets the new Data Chooser.
	 * @see com.gwsys.seismic.core.DataLoader#setDataViewport()
	 */
	public void setDataViewport(DataChooser data) {
		viewport = data;
		if (viewport!=null)
			viewport.setParentProcessor(this.reader);
	}

	/**
	 * Gets the data chooser.
	 * @return DataChooser
	 */
	public DataChooser getDataSelector() {
		return viewport;
	}
	
	/**
	 * Gets the primary keys in data loader.
	 * It is used for editing panel.
	 * @return Vector
	 */
	public Vector getPrimaryKeys(){
		return reader.getPrimaryKeys();
	}
	  
	/**
	 * Gets the secondary keys in data loader.
	 * It is used for editing panel.
	 * @return Vector
	 */
	public Vector getSecondaryKeys(){
		return reader.getSecondaryKeys();
	}

}
