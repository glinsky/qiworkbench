//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;

/**
 * Rendering parameter carrier.
 * 
 */
public class RenderingParameters {

	private TraceData _currentTraceData = null;

	private int _currentTraceLoc = -1;
        private int _nextTraceLoc = -1;

	private boolean _isHighlighted = false;

	private int _lastTraceLoc = -1;

	private TraceData _lastTraceData = null;

	private NumberRange _sampleRange = null;

	private int _wiggleTraceColorIndex = 0;

	private boolean _wiggleTraceIgnored = false;

	/**
	 * Constructor.
	 */
	public RenderingParameters() {
	}

	/**
	 * Gets the current trace.
	 * 
	 * @return The current trace.
	 */
	public TraceData getCurrentTrace() {
		return _currentTraceData;
	}

	/**
	 * Returns the location of the current trace.
	 * 
	 * @return The horizontal location (in pixels) of the current trace
	 */
	public int getCurrentTraceLoc() {
		return _currentTraceLoc;
	}

        /**
	 * Returns the location of the next trace.
	 * 
	 * @return The horizontal location (in pixels) of the next trace
	 */
	public int getNextTraceLoc() {
		return _nextTraceLoc;
	}
        
	/**
	 * Gets the trace previous to the current trace. 
	 * 
	 * @return The trace previous to the current trace.
	 */
	public TraceData getPreviousTrace() {
		return _lastTraceData;
	}

	/**
	 * Returns the location of the previous trace.
	 * 
	 * @return The horizontal location (in pixels) of the previous trace
	 */
	public int getPreviousTraceLoc() {
		return _lastTraceLoc;
	}

	/**
	 * Returns the sample range of the current trace.
	 * 
	 * @return The sample range of the current trace (in pixels).
	 */
	public NumberRange getSampleRange() {
		return _sampleRange;
	}

	/**
	 * Returns the color index of the trace to be rasterized.
	 * 
	 * @return The trace color index.
	 */
	public int getWiggleColorIndex() {
		return _wiggleTraceColorIndex;
	}

	/**
	 * Returns whether the current trace is highlighted.
	 * 
	 * @return <code>true</code> if highlighted,
	 *         <code>false</code> otherwise.
	 */
	public boolean isTraceHighlighted() {
		return _isHighlighted;
	}

	/**
	 * Checks whether this trace should be skipped.
	 * 
	 * @return <code>true</code> if skipped, <code>false</code> otherwise.
	 */
	public boolean isWiggleTraceSkipped() {
		return _wiggleTraceIgnored;
	}

	/**
	 * Sets the current trace to be rendered.
	 * 
	 * @param currentTrace
	 *            The current trace to be rendered.
	 */
	public void setCurrentTrace(TraceData currentTrace) {
		_currentTraceData = currentTrace;
	}

	/**
	 * Sets the location of the current trace.
	 * 
	 * @param currentTraceLoc
	 *            The horizontal location (in pixels) of the current trace
	 */
	public void setCurrentTraceLoc(int currentTraceLoc) {
		_currentTraceLoc = currentTraceLoc;
	}

        /**
	 * Sets the location of the next trace.  Used to detect gaps in rasterization
         * due to scaling.
	 * 
	 * @param nextTraceLoc
	 *            The horizontal location (in pixels) of the current trace
	 */
	public void setNextTraceLoc(int nextTraceLoc) {
		_nextTraceLoc = nextTraceLoc;
	}
        
	/**
	 * Sets the trace previous to the current trace. 
	 * 
	 * @param prevTrace
	 *            The trace previous to the current trace.
	 */
	public void setPreviousTrace(TraceData prevTrace) {
		_lastTraceData = prevTrace;
	}

	/**
	 * Sets the location of the previous trace.
	 * 
	 * @param lastTraceLoc
	 *            The horizontal location (in pixels) of the previous trace
	 */
	public void setPreviousTraceLoc(int lastTraceLoc) {
		_lastTraceLoc = lastTraceLoc;
	}

	/**
	 * Sets the sample range of the current trace.
	 * 
	 * @param sampleRange
	 *            The sample range of the current trace (in pixels).
	 */
	public void setSampleRange(NumberRange sampleRange) {
		_sampleRange = sampleRange;
	}

	/**
	 * Sets whether the current trace should be highlighted.
	 * 
	 * @param isHighlighted
	 *            <code>true</code> if highlighted,
	 *            <code>false</code> otherwise.
	 */
	public void setTraceHighlighted(boolean isHighlighted) {
		_isHighlighted = isHighlighted;
	}

	/**
	 * Sets the color index of the trace to be rasterized.
	 * 
	 * @param wiggleColorIndex
	 *            Sets trace's color index.
	 */
	public void setWiggleColorIndex(int wiggleColorIndex) {
		_wiggleTraceColorIndex = wiggleColorIndex;
	}

	/**
	 * Sets whether the wiggle rasterization of this trace should be skipped.
	 * 
	 * @param skipped
	 *            <code>true</code> if wiggle traces should be
	 *            skipped, <code>false</code> otherwise. 
	 */
	public void setWiggleTraceSkip(boolean skipped) {
		_wiggleTraceIgnored = skipped;
	}
}