//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;

/**
 * Implements the segy trace rendering behavior based on the bhpviewer API.
 * @version 1.0
 */
public class SegyTracePainter extends TracePainter {
	//member variables
	private TraceData[] _traceData = { new TraceData(), new TraceData() };

	private NumberRange _devRange = new NumberRange();

	/**
	 * Creates a implementation of the default trace renderer.  
	 * 
	 * @param workflow SeismicWorkflow.
	 */
	public SegyTracePainter(SeismicWorkflow workflow) {
		this(workflow, workflow.getTraceRasterizer());
	}

	/**
	 * Creates a implementation of the default trace renderer.
	 *
	 * @param workflow SeismicWorkflow.
	 * @param rasterizer Rasterizer to be used when rendering traces.
	 */
	public SegyTracePainter(SeismicWorkflow workflow,
			TraceRasterizer rasterizer) {
		this.setWorkflow(workflow);
		this.setRasterizer(rasterizer);
	}

	/**
	 * @inheritDoc
	 */
	public synchronized void draw(Transform2D transformation) {
		//get the trace start and trace end
		int clippingValue = (int) Math.ceil(this.getRasterizer()
				.getClippingValue());
		int traceStart = (int) Math.floor(this.getDataBound().getMinX()
				- clippingValue);

		if (traceStart < 0)
			traceStart = 0;

		int traceEnd = (int) Math.ceil(this.getDataBound().getMaxX()
				+ clippingValue);
		Bound2D bound = this.getWorkflow().getVirtualModelLimits();
		if (traceEnd > bound.getMaxX() - 1)
			traceEnd = (int) bound.getMaxX() - 1;
		
		int pixelStart = (int) Math.round(this.getDataBound().getMinY()
				* this.getWorkflow().getInterpretation().getTraceInterpolator()
						.getResamplingFactor());

		int pixelEnd = (int) Math.ceil(this.getDataBound().getMaxY()
				* this.getWorkflow().getInterpretation().getTraceInterpolator()
						.getResamplingFactor()) + 1;

		_devRange.setRange(pixelStart, pixelEnd);
		_traceData[0].setNumAppliedSamples(this.getDestinationHeight());
		_traceData[1].setNumAppliedSamples(this.getDestinationHeight());
		//set raster space		
		int decimationValue = 1;

		if (getRasterizer().getMinSpacing() > 0) {
			decimationValue = (int) Math.round(getRasterizer().getMinSpacing() 
					/ transformation.getScaleX());
		}
		
		if (decimationValue < 1)
			decimationValue = 1;

		getRasterizer().setTraceSpace(transformation.getScaleX() * decimationValue);
		boolean flipped = false; 
		boolean origReverseFill = getRasterizer().isReverseFill();
		int direction = this.getWorkflow().getDirection();
		if (((direction & TraceImage.DRAW_TRACE_RIGHT_TO_LEFT) 
				== TraceImage.DRAW_TRACE_RIGHT_TO_LEFT)) {
			getRasterizer().setReverseFill(!origReverseFill);
			flipped = true;
		}

		boolean densityType = ((getRasterizer().getPlotType() & 
				TraceRasterizer.INTERPOLATED_DENSITY_TRACE) 
				== TraceRasterizer.INTERPOLATED_DENSITY_TRACE)
				|| ((getRasterizer().getPlotType() & 
						TraceRasterizer.DENSITY_TRACE) 
						== TraceRasterizer.DENSITY_TRACE);
		int traceIndex = 0;
		int tracesProcessed = 0;
		int lastTraceLoc = Integer.MIN_VALUE;
		int thisTraceLoc = 0;
                int lastRenderedWiggleLoc = Integer.MIN_VALUE;

		boolean traceSkipped = false;
		double dx = this.getModelBound().getMinX() * transformation.getScaleX();
		RenderingParameters _parameters = new RenderingParameters();
		//render the traces
		for (int traceId = traceStart; traceId <= traceEnd; traceId++) {

			thisTraceLoc = (int)Math.round(((traceId+0.5) * 
					transformation.getScaleX())	- dx);
                        int nextTraceLoc = (int)Math.round(((traceId+1.5) * 
					transformation.getScaleX())	- dx);
                        //Bug fix for BHPVIEWER-9 - this causes many wiggle traces to be skipped
                        //when rendering in a density mode.
			//skip the trace if two traces are too close
                        //Previously, this failed to check whether a trace was initial rendered at this location
                        //it may have been skipped due to wiggle decimation, so a subsequent trace should be
                        //rendered at that location instead.
			if ((((lastTraceLoc > Integer.MIN_VALUE) 
					&& (lastTraceLoc < Integer.MAX_VALUE)) 
					&& ((thisTraceLoc - lastTraceLoc) < 1))
					&& densityType
                                        && (lastRenderedWiggleLoc == thisTraceLoc)) {
                            continue;
			}

			traceSkipped = ((traceId % decimationValue) == 0) ? false : true;

			traceIndex = tracesProcessed % 2; //zero or one

			//get trace samples
			if (this.getWorkflow().process(traceId, _devRange,_traceData[traceIndex])) {

				tracesProcessed++;

				if (flipped) {
					float[] samples = _traceData[traceIndex].getSamples();
					for (int i = 0; i < samples.length; i++)
						samples[i] = -samples[i];
				}

				if ((traceId == traceStart)	|| (lastTraceLoc == Integer.MAX_VALUE)) {
					_parameters.setPreviousTrace(null);
				} else {
					_parameters.setPreviousTrace(_traceData[traceIndex ^ 0x0001]);
				}

				_parameters.setSampleRange(_devRange);
				_parameters.setWiggleColorIndex(getRasterizer()
						.getColorMap().getForegroundIndex());
				_parameters.setCurrentTrace(_traceData[traceIndex]);
				_parameters.setPreviousTraceLoc(lastTraceLoc);
				_parameters.setCurrentTraceLoc(thisTraceLoc);
                                _parameters.setNextTraceLoc(nextTraceLoc);
				_parameters.setTraceHighlighted(getWorkflow()
						.isTraceHighlighted(traceId));
				_parameters.setWiggleTraceSkip(traceSkipped);
				//render
				getRasterizer().rasterizeTrace(_parameters);
                                
                                if (traceSkipped == false)
                                    lastRenderedWiggleLoc = thisTraceLoc;
				
                                lastTraceLoc = thisTraceLoc;
			}
			else {
				lastTraceLoc = Integer.MAX_VALUE;
			}
		}
		getRasterizer().setReverseFill(origReverseFill);
	}
}
