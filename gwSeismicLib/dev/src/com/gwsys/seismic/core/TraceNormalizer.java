//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.seismic.core;

import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.io.SeismicMetaData;
import com.gwsys.seismic.reader.SeismicReader;
import com.gwsys.seismic.util.NumberRange;

/**
 * This class converts the trace samples into specific range.
 * The default implementation scales samples into (-1, 1) range.
 * 
 * The API is copied from bhpViewer. 
 */
public class TraceNormalizer extends AbstractProcessor {

	/** the dataset maximum amplitude. */
	public static final int AMPLITUDE_MAXIMUM = 0;

	/** the trace maximum amplitude. */
	public static final int TRACE_MAXIMUM = 1;

	/** the dataset average amplitude. */
	public static final int AMPLITUDE_AVERAGE = 2;

	/** the trace average amplitude. */
	public static final int TRACE_AVERAGE = 3;

	/** the dataset RMS amplitude. */
	public static final int AMPLITUDE_RMS = 4;

	/** the trace RMS amplitude. */
	public static final int TRACE_RMS = 5;

	/** the limits specified by user. */
	public static final int AMPLITUDE_LIMITS = 6;

	/** The variables of factors. */
	private float shiftByMode = 0, scaleByMode = 1, scaleByUser = 1;

	/**
	 * The min and max values are defined by user for AMPLITUDE_LIMITS.	
	 */ 
	private double limit_min, limit_max;

	/** variable for reused calculation. */
	private TraceData _dummyTraceData = new TraceData();

	/** variable for the normalization mode. */
	private int _normalizationMode = AMPLITUDE_RMS;

	/** The flag of notification */
	private boolean _notificationEnabled = true;

	/**
	 * Constructs with "AMPLITUDE_RMS" normalization mode.
	 */
	public TraceNormalizer() {
		_normalizationMode = AMPLITUDE_RMS;
		_notificationEnabled = true;
	}

	/**
	 * Sets the pipeline that contains this process.</p>
	 *
	 * @param workflow double that contains this process
	 */
	public void setWorkflow(SeismicWorkflow workflow) {
		super.setWorkflow(workflow);
		if (workflow != null) {
			SeismicReader reader = workflow.getDataLoader().getDataReader();
			// initialize the limit range
			if (reader != null) {
				limit_min = reader.getMetaData().getMinimumAmplitude();
				limit_max = reader.getMetaData().getMaximumAmplitude();
			}
		}
	}

	/**
	 * Gets the scaling factor.
	 *
	 * @return the scaling factor.
	 */
	public float getScale() {
		return scaleByUser;
	}

	/**
	 * Sets the scaling factor. The value must be positive.
	 * @param scale The new scaling factor.
	 */
	public void setScale(float scale) {
		if (scale > 0) {
			scaleByUser = scale;
			if (getWorkflow().getInterpretation().getTraceAGC()!=null)
				getWorkflow().getInterpretation().getTraceAGC().setAGCScale(scale);
		}
	}

	/**
	 * Sets the normalization mode for this normalization process.
	 * @param newMode the normalization mode.
	 */
	public void setNormalizationMode(int newMode) {

		_normalizationMode = newMode;
	}

	/**
	 * Gets the normalization mode for this normalization process.
	 * @return the normalization mode
	 */
	public int getNormalizationMode() {
		return _normalizationMode;
	}

	/**
	 * Returns whether notification is enabled or not.
	 *
	 * @return status of notification.
	 */
	public boolean isNotificationEnabled() {
		return _notificationEnabled;
	}

	/**
	 * Sets whether notification is enabled or not.
	 *
	 * @param enabled the flag indicating whether notification is enabled or not.
	 */
	public void setNotificationEnabled(boolean enabled) {
		_notificationEnabled = enabled;
	}

	/**
	 * Returns min normalization limit.</p>
	 *
	 * @return the min normalization limit
	 */
	public double getNormLimitMin() {
		return limit_min;
	}

	/**
	 * Returns max normalization limit.</p>
	 *
	 * @return the max normalization limit
	 */
	public double getNormLimitMax() {
		return limit_max;
	}

	/**
	 * Sets normalization limits. The first value should less than the second.
	 * @param min  the lower value.
	 * @param max  the upper value.
	 */
	public void setNormalizationLimits(double min, double max) {

		if (min > max) {
			throw new IllegalArgumentException(
					"The first value must be less than the second.");			
		}

		limit_min = min;
		limit_max = max;

	}

	/**
	 * Apply the normalization process for the specified trace.
	 *
	 * @param traceId		the mapped trace ID to be processed.
	 * @param sampleRange	the range of required samples.
	 * @param traceDataOut	the output trace data.
	 *
	 * @return <code>true</code> if successful,
	 *         <code>false</code> otherwise
	 */
	public boolean process(int traceId, NumberRange sampleRange,
			TraceData traceDataOut) {

		if (!(getParentProcessor().process(traceId, sampleRange,
				getTraceData())))
			return false;

		int numSamples = getTraceData().getNumAppliedSamples();
		float[] inputData = getTraceData().getSamples();

		float[] outputData = null;

		if ((numSamples) >= traceDataOut.getNumAppliedSamples()) {
			outputData = new float[numSamples];
			traceDataOut.setSamples(outputData);
			traceDataOut.setNumAppliedSamples(outputData.length);
		} else {
			outputData = traceDataOut.getSamples();
		}

		calculateNormalizationFactors(traceId);
		//get smallest value
		numSamples = Math.min(numSamples, inputData.length);
		numSamples = Math.min(numSamples, outputData.length);

		for (int i = 0, j = 0; i < numSamples; i++, j++)
			outputData[j] = (inputData[i] + shiftByMode) * scaleByMode;

		traceDataOut.setUniqueKey(getTraceData().getUniqueKey());

		return true;
	}

	private void calculateNormalizationFactors(int traceId) {
		
		NumberRange range = new NumberRange(getNormLimitMin(),
				getNormLimitMax());
				
		if (traceId==-1){
			doMaxAmplitudeRange(getWorkflow(), range);
		}else{
			switch (getNormalizationMode()) {
			case TraceNormalizer.AMPLITUDE_MAXIMUM:
			  doMaxAmplitudeRange(getWorkflow(), range);
			  break;
			case TraceNormalizer.AMPLITUDE_AVERAGE:
			  doAveAmplitudeRange(getWorkflow(), range);
			  break;
			case TraceNormalizer.AMPLITUDE_RMS:
			  doRmsAmplitudeRange(getWorkflow(), range);
			  break;
			case TraceNormalizer.TRACE_MAXIMUM:
			  doTraceMaxAmplitudeRange(getWorkflow(), range, _dummyTraceData,traceId);
			  break;
			case TraceNormalizer.TRACE_AVERAGE:
			  doTraceAveAmplitudeRange(getWorkflow(), range, _dummyTraceData,traceId);
			  break;
			case TraceNormalizer.TRACE_RMS:
			  doTraceRmsAmplitudeRange(getWorkflow(), range, _dummyTraceData,traceId);
			  break;
			}
		}
		if (range.getMin().floatValue() < 0 && range.getMax().floatValue() > 0)
			shiftByMode=0;
		else
			shiftByMode=(range.getMin().floatValue() + range.getMax().floatValue())
					/ (-2.0f);

		if (range.getMax() != range.getMin())
			scaleByMode=(2 * getScale())/ 
					(range.getMax().floatValue()-range.getMin().floatValue());
		else
			scaleByMode=0;
	}
	
	private void doMaxAmplitudeRange(SeismicWorkflow pipeline,
            NumberRange amplitudeRange){
		SeismicMetaData meta=
			pipeline.getDataLoader().getDataReader().getMetaData();
		if (meta.getMaximumAmplitude()>0&&meta.getMinimumAmplitude()<0) {
			double factor = Math.max(Math.abs(meta.getMinimumAmplitude()),
					meta.getMaximumAmplitude());
			amplitudeRange.setRange( -factor, factor);
		}else
			amplitudeRange.setRange(meta.getMinimumAmplitude(),
				meta.getMaximumAmplitude());
	}
	
	private void doAveAmplitudeRange(SeismicWorkflow pipeline,
            NumberRange amplitudeRange){
		SeismicMetaData meta=
			pipeline.getDataLoader().getDataReader().getMetaData();
		if (meta.getMaximumAmplitude()>0&&meta.getMinimumAmplitude()<0) {
			double factor = meta.getAverage();
			amplitudeRange.setRange( -factor, factor);
		}
	}
	
	private void doRmsAmplitudeRange(SeismicWorkflow pipeline,
            NumberRange amplitudeRange){
		SeismicMetaData meta=
			pipeline.getDataLoader().getDataReader().getMetaData();
		if (meta.getMaximumAmplitude()>0&&meta.getMinimumAmplitude()<0) {
			double factor = meta.getRMS();
			amplitudeRange.setRange( -factor, factor);
		}
	}
	
	private void doTraceRmsAmplitudeRange(SeismicWorkflow pipeline,
            NumberRange amplitudeRange, TraceData traceData, int traceId){
		TraceHeader meta = getTraceMetaData(pipeline, traceId,
		          traceData);
		if (meta.getMaximumAmplitude()>0&&meta.getMinimumAmplitude()<0) {
			float factor = (float) (meta.getRMS());
			amplitudeRange.setRange( -factor, factor);
		}
	}
	
	private void doTraceAveAmplitudeRange(SeismicWorkflow pipeline,
            NumberRange amplitudeRange, TraceData traceData, int traceId){
		TraceHeader meta = getTraceMetaData(pipeline, traceId,
		          traceData);
		if (meta.getMaximumAmplitude()>0&&meta.getMinimumAmplitude()<0) {
			double factor = meta.getAverage();
			amplitudeRange.setRange( -factor, factor);
		}
	}
	private void doTraceMaxAmplitudeRange(SeismicWorkflow pipeline,
            NumberRange amplitudeRange, TraceData traceData, int traceId){
		TraceHeader meta = getTraceMetaData(pipeline, traceId,
		          traceData);
		if (meta.getMaximumAmplitude()>0&&meta.getMinimumAmplitude()<0) {
			double factor = Math.max(-meta.getMinimumAmplitude(), 
					meta.getMaximumAmplitude());
			amplitudeRange.setRange( -factor, factor);
		}
	}
	
	private TraceHeader getTraceMetaData(SeismicWorkflow pipeline, 
			int traceId,
            TraceData traceData ) {
		TraceHeader traceMetaData = null;
		DataChooser test = pipeline.getDataLoader().getDataSelector();
		if (test != null)
			traceMetaData = test.getTraceHeader(traceId);
		else
			traceMetaData = pipeline.getDataLoader().getDataReader()
					.getTraceMetaData(traceId);
		if (!traceMetaData.isStatisticsCollected()) {
			int virtualTraceId = traceId;
			try {
				if (pipeline.getDataLoader().getDataSelector() != null) {
					virtualTraceId = pipeline.getDataLoader().getDataSelector()
							.virtualToReal(traceId);
				}
			} catch (UnsupportedOperationException e) {

			}
			pipeline.getDataLoader().getDataReader().process(virtualTraceId,
					null, traceData);
			calculateTraceStatistics(traceData, traceMetaData);
		}
		return traceMetaData;
	}
	/**
	 * Calculates trace statics and stores them in the corresponding trace meta
	 * data object.
	 * @param traceData Contains trace to perform statistics on.
	 * @param traceMetaData trace meta data object to store statistis.
	 */
	public static void calculateTraceStatistics(TraceData traceData,
			TraceHeader traceMetaData) {

		float min = Float.MAX_VALUE;
		float max = Float.MIN_VALUE;
		double sumX = 0;
		double sumX2 = 0;
		int count = traceData.getNumAppliedSamples();

		float[] samples = traceData.getSamples();
		for (int i = 0; i < count; i++) {
			if (samples[i] < min)
				min = samples[i];
			if (samples[i] > max)
				max = samples[i];

			if (samples[i] < 0)
				sumX -= samples[i];
			else
				sumX += samples[i];

			sumX2 += samples[i] * samples[i];
		}

		if (count == 0)
			traceMetaData.setTraceStatistics(min, max, 0, 0);
		else
			traceMetaData.setTraceStatistics(min, max, sumX / count, Math
					.sqrt(sumX2 / count));
	}

	/**
	 * Retrives the range of trace before normalization.
	 * @param range the normalized range of amplitude values.
	 * @return the pre-normalized range of amplitude values.
	 */
	public NumberRange getDataRangeFor(NumberRange range) {
		calculateNormalizationFactors(-1);
		return new NumberRange(range.getMin().floatValue() / scaleByMode
				- shiftByMode, range.getMax().floatValue() / scaleByMode - shiftByMode);

	}

}
