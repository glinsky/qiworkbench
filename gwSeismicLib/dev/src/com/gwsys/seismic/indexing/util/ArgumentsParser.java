//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.seismic.indexing.util;

import java.util.*;

/**
 * Parses array of arguments (usually, command line parameters) and returns them as a collection
 * 
 * @version 1.0
 */
public class ArgumentsParser {
	/**
	 * Parses array of arguments. Each argument must be something like this: arg=val1,val2,val3..valN
	 * assuming <code>delim1</code> is "=" and <code>delim2</code> is ","
	 * @param args array of arguments
	 * @param delim argument name delimiter
	 * @param delim2 argument values delimiter
	 * @return map collection, where key is argument name and value is a string array of argument values
	 */
	public static Map parse(String[] args, String delim, String delim2) {
		TreeMap m = new TreeMap();
		for (int i = 0; i < args.length; i++) {
			StringTokenizer s = new StringTokenizer(args[i], delim, false);

			if (s.countTokens() == 2) {
				String key = s.nextToken();
				Vector v = new Vector();
				String tok = s.nextToken();
				s = new StringTokenizer(tok, delim2, true);
				boolean got_token = true;
				String next_tok = null;
				while (s.hasMoreTokens()) {
					next_tok = s.nextToken();
					if (next_tok.compareToIgnoreCase(delim2) != 0) {
						v.addElement(next_tok);
						got_token = false;
					} else if (got_token) {
						v.addElement(null);
					} else {
						got_token = true;
					}
				}
				if (next_tok != null
						&& next_tok.compareToIgnoreCase(delim2) == 0) {
					v.addElement(null);
				}
				String[] valarray = new String[v.size()];
				for (int j = 0; j < v.size(); j++) {
					valarray[j] = (String) v.elementAt(j);
				}
				m.put(key, valarray);
			}
		}
		return m;
	}

	/**
	 * Same as above, assuming delim1 is "=" and delim2 is ","
	 * @param args array of arguments
	 * @return named map argument names to arrays of their values
	 */
	public static Map parse(String[] args) {
		return parse(args, "=", ",");
	}
}