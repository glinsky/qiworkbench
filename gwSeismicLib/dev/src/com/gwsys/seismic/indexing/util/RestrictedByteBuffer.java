//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.seismic.indexing.util;

/**
 * Defines common interface of custom data arrays.
 * 
 * @version 1.0
 */
public interface RestrictedByteBuffer {

  /**
   * Sets current position
   * @param pos position
   * @throws IllegalArgumentException
   */
  void setPosition(int pos) throws IllegalArgumentException;

  /**
   * Fills supplied array with the data, starting at current position.
   * @param buf target buffer
   */
  void get(byte[] buf);

  /**
   * Gets byte value at current position
   * @return byte value
   */
  byte get();

  /**
   * Gets short value at current position
   * @return short value
   */
  short getShort();

  /**
   * Gets integer value at current position
   * @return integer value
   */
  int getInt();

  /**
   * Gets float value at current position
   * @return float value
   */
  float getFloat();

  /**
   * Gets double value at current position
   * @return double value
   */
  double getDouble();
}
