//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.util;

import java.io.*;

/**
 * A helper class to supply creation and deleting a temporary files
 * and control their parent filesystem folder (namely, TEMP folder)
 * 
 * @version 1.0
 */
public class SegyTempFile extends File {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3256726195025754417L;

	private static String checkTempFolder(String tempFolder) {
		try {
			File tmp = null;
			if (tempFolder != null) {
				tmp = File.createTempFile("tmp", null, new File(tempFolder));
			} else {
				tmp = File.createTempFile("tmp", null);
			}
			if (tmp == null || !tmp.canRead() || !tmp.canWrite()) {
				return null;
			}
			tmp.deleteOnExit();
			return tmp.getParentFile().getPath();
		} catch (IOException e) {
			return null;
		} catch (SecurityException e) {
			return null;
		}
	}

	private static String TEMP_FOLDER = null;

	/**
	 * Use this property to control will instances of this class be deleted on exit or not.
	 * By default, they will be deleted.
	 */
	private static boolean DELETE_ON_EXIT = true;

	/**
	 * Returns the filesystem folder which actually will be used.
	 * @return temporary folder name
	 */
	public static String getTempFolder() {
		return checkTempFolder(TEMP_FOLDER);
	}

	/**
	 * Tries to set filesystem folder for temporary files creation.
	 * @param fld folder to use. If <code>fld</code> is <b>null</b>, default TEMP folder will be set.
	 * @return true if, and only if, folder was successfully set (it means folder exists and allows read/write operations).
	 */
	public static boolean setTempFolder(String fld) {
		String folder = checkTempFolder(fld);
		if (fld == null) {
			TEMP_FOLDER = null;
			return true;
		}
		boolean eq = (folder != null && (new File(fld))
				.equals(new File(folder)));
		if (eq) {
			TEMP_FOLDER = fld;
		}
		return eq;
	}

	/**
	 * Creates new instance of <code>SegyTempFile</code> with name equals to <code>name</code> and placed in <code>folder</code> folder
	 * @param folder folder to place new file
	 * @param name name of new file
	 * @see java.io.File#File(File, String)
	 */
	public SegyTempFile(File folder, String name) {
		super(folder, name);
		if (DELETE_ON_EXIT) {
			deleteOnExit();
		}
	}

	/**
	 * Creates new instance of <code>SegyTempFile</code> with name equals to <code>name</code> and placed in current TEMP folder.
	 * @param name name of new file
	 * @see tempFolder(String)
	 * @see java.io.File#File(String)
	 */
	public SegyTempFile(String name) {
		super(name);
		if (DELETE_ON_EXIT) {
			deleteOnExit();
		}
	}

	/**
	 * Creates new file with name equals to <code>name</code> and placed in <code>folder</code> folder.
	 * @param folder folder to place new file
	 * @param name name of new file
	 * @see java.io.File#File(String, String)
	 */
	public SegyTempFile(String folder, String name) {
		super(folder, name);
		if (DELETE_ON_EXIT) {
			deleteOnExit();
		}
	}

	/**
	 * Returns true if current TEMP folder is the default folder.
	 * @return true if current TEMP folder is the default folder.
	 */
	public static boolean isDefaultTempFolder() {
		return TEMP_FOLDER == null;
	}

	/**
	 * Creates new temporary instance of <code>File</code> file with given preffix and suffix.
	 * @param prefix prefix to use
	 * @param suffix suffix to use
	 * @return new instance of <code>File</code> file.
	 * @throws IOException
	 */
	public static File createTempFile(String prefix, String suffix)
			throws IOException {
		File newFile = null;
		String tmpFolder = checkTempFolder(TEMP_FOLDER);
		if (tmpFolder == null) {
			newFile = File.createTempFile(prefix, suffix);
		} else {
			newFile = File.createTempFile(prefix, suffix, new File(tmpFolder));
		}
		if (newFile != null && DELETE_ON_EXIT) {
			newFile.deleteOnExit();
		}
		return newFile;
	}

	protected void finalize() throws Throwable {
		delete();
	}
	
	/**
	 * Enables the temp file deletion when exiting.
	 * @param enable
	 */
	public void deleteFileonExit(boolean enable){
		DELETE_ON_EXIT = enable;
	}
}