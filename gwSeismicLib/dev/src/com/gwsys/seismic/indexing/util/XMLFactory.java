//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;


/**
 * Tools Handle XML Document.
 * @author Team
 *
 */
public class XMLFactory {


	public static Document createDocumentFromStream(InputStream ins)
		throws ParserConfigurationException, SAXException, IOException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		DocumentBuilder dbuilder = factory.newDocumentBuilder();
		if (ins==null)
			return dbuilder.newDocument();
		else
			return dbuilder.parse(ins);
	}

	public static Document createDocumentFromFile(File ins)
		throws ParserConfigurationException, SAXException, IOException{
		if (ins!=null)
			return createDocumentFromStream(ins.toURL().openStream());
		else
			return createDocumentFromStream(null);
	}

	public static void createSegyFormat(File file, String byteInline, String byteXline){
		Document doc=null;
		try {
			doc = createDocumentFromStream(null);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (doc==null)
			return;
		Element xmlRoot = doc.createElement("Segy");
		Element header = doc.createElement("Header");
		header.setAttribute("Name", "Ebcdic");
		header.setAttribute("Format", "EBCDIC");
		header.setAttribute("Size", "3200");
		header.setAttribute("Place", "Line");
		xmlRoot.appendChild(header);
		header = doc.createElement("Header");
		header.setAttribute("Name", "BinaryHeader");
		header.setAttribute("Format", "BINARY");
		header.setAttribute("Size", "400");
		header.setAttribute("Place", "Line");
		header.setAttribute("Fields", "Segy");
		xmlRoot.appendChild(header);
		header = doc.createElement("Header");
		header.setAttribute("Name", "TraceHeader");
		header.setAttribute("Format", "BINARY");
		header.setAttribute("Size", "240");
		header.setAttribute("Place", "Trace");
		xmlRoot.appendChild(header);
		Element field = doc.createElement("Field");
		field.setAttribute("Name", "INLINE");
		field.setAttribute("Format", "UINT32");
		field.setAttribute("Offset", byteInline);
		header.appendChild(field);
		field = doc.createElement("Field");
		field.setAttribute("Name", "XLINE");
		field.setAttribute("Format", "UINT32");
		field.setAttribute("Offset", byteXline);
		header.appendChild(field);
		doc.appendChild(xmlRoot);
		createSegyFormat(doc, file);
	}

	private static void createSegyFormat(Document doc, File file) {
		try {
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			DOMSource source = new DOMSource(doc);
			PrintWriter pw = new PrintWriter(new FileOutputStream(file));
			StreamResult result = new StreamResult(pw);
			transformer.transform(source, result);
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
