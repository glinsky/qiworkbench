//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.seismic.indexing.util;

import java.nio.*;

import com.gwsys.seismic.indexing.DataSliceType;
import com.gwsys.seismic.reader.SeismicFormat;
import com.gwsys.seismic.indexing.SeismicConstants;

/**
 * This class provides metods to convert various SEGY constants from/to their string representations
 *
 * @version 1.0
 */
public class SegyConstantsParser {
	/**
	 *  Gets the segyField attribute of the SegyConstantsParser class
	 *
	 * @param  s  Description of the Parameter
	 * @return    The segyField value
	 */
	public static Integer getSegyField(String s) {
		if (s.equalsIgnoreCase(SeismicFormat.LINE_NUMBER)) {
			return new Integer(SeismicFormat.HEADER_LINE_NUMBER);
		} else if (s.equalsIgnoreCase(SeismicFormat.TRACES_PER_ENSEMBLE)) {
			return new Integer(SeismicFormat.HEADER_TRACES_PER_ENSEMBLE);
		} else if (s.equalsIgnoreCase(SeismicFormat.AUX_TRACES_PER_ENSEMBLE)) {
			return new Integer(SeismicFormat.HEADER_AUX_TRACES_PER_ENSEMBLE);
		} else if (s.equalsIgnoreCase(SeismicFormat.SAMPLE_INTERVAL)) {
			return new Integer(SeismicFormat.HEADER_SAMPLE_INTERVAL);
		} else if (s.equalsIgnoreCase(SeismicFormat.SAMPLES_PER_TRACE)) {
			return new Integer(SeismicFormat.HEADER_SAMPLES_PER_TRACE);
		} else if (s.equalsIgnoreCase(SeismicFormat.DATA_FORMAT_CODE)) {
			return new Integer(SeismicFormat.HEADER_DATA_FORMAT_CODE);
		} else if (s.equalsIgnoreCase(SeismicFormat.TRACES_PER_CDP_ENSEMBLE)) {
			return new Integer(SeismicFormat.HEADER_TRACES_PER_CDP_ENSEMBLE);
		} else if (s.equalsIgnoreCase(SeismicFormat.MEASUREMENT_SYSTEM)) {
			return new Integer(SeismicFormat.HEADER_MEASUREMENT_SYSTEM);
		} else if (s.equalsIgnoreCase(SeismicFormat.SEGY_REVISION_NUMBER)) {
			return new Integer(SeismicFormat.HEADER_SEGY_REVISION_NUMBER);
		} else if (s.equalsIgnoreCase(SeismicFormat.FIXED_LENGTH_FLAG)) {
			return new Integer(SeismicFormat.HEADER_FIXED_LENGTH_FLAG);
		} else if (s.equalsIgnoreCase(SeismicFormat.MAXIMUM_SAMPLES_PER_TRACE)) {
			return new Integer(SeismicFormat.HEADER_MAXIMUM_SAMPLES_PER_TRACE);
		} else if (s.equalsIgnoreCase(SeismicFormat.NUMBER_OF_EXTENSIONS)) {
			return new Integer(SeismicFormat.HEADER_NUMBER_OF_EXTENSIONS);
		} else if (s.equalsIgnoreCase(SeismicFormat.TRACE_SEQUENCE_NUMBER)) {
			return new Integer(SeismicFormat.HEADER_TRACE_SEQUENCE_NUMBER);
		} else if (s.equalsIgnoreCase(SeismicFormat.TRACE_NUMBER_ONE)) {
			return new Integer(SeismicFormat.HEADER_TRACE_NUMBER_ONE);
		} else if (s.equalsIgnoreCase(SeismicFormat.FIELD_RECORD)) {
			return new Integer(SeismicFormat.HEADER_FIELD_RECORD);
		} else if (s.equalsIgnoreCase(SeismicFormat.FIELD_TRACE)) {
			return new Integer(SeismicFormat.HEADER_FIELD_TRACE);
		} else if (s.equalsIgnoreCase(SeismicFormat.SHOTPOINT_ID)) {
			return new Integer(SeismicFormat.HEADER_SHOTPOINT_ID);
		} else if (s.equalsIgnoreCase(SeismicFormat.CDP_NUMBER)) {
			return new Integer(SeismicFormat.HEADER_CDP_NUMBER);
		} else if (s.equalsIgnoreCase(SeismicFormat.CDP_TRACE)) {
			return new Integer(SeismicFormat.HEADER_CDP_TRACE);
		} else if (s.equalsIgnoreCase(SeismicFormat.TRACE_ID)) {
			return new Integer(SeismicFormat.HEADER_TRACE_ID);
		} else if (s.equalsIgnoreCase(SeismicFormat.REC_ELEVATION)) {
			return new Integer(SeismicFormat.HEADER_REC_ELEVATION);
		} else if (s.equalsIgnoreCase(SeismicFormat.COORDINATE_SCALER)) {
			return new Integer(SeismicFormat.HEADER_COORDINATE_SCALER);
		} else if (s.equalsIgnoreCase(SeismicFormat.SOURCE_X_LOC)) {
			return new Integer(SeismicFormat.HEADER_SOURCE_X_LOC);
		} else if (s.equalsIgnoreCase(SeismicFormat.SOURCE_Y_LOC)) {
			return new Integer(SeismicFormat.HEADER_SOURCE_Y_LOC);
		} else if (s.equalsIgnoreCase(SeismicFormat.RECEIVER_X_LOC)) {
			return new Integer(SeismicFormat.HEADER_RECEIVER_X_LOC);
		} else if (s.equalsIgnoreCase(SeismicFormat.RECEIVER_Y_LOC)) {
			return new Integer(SeismicFormat.HEADER_RECEIVER_Y_LOC);
		} else if (s.equalsIgnoreCase(SeismicFormat.START_TIME)) {
			return new Integer(SeismicFormat.HEADER_START_TIME);
		} else if (s.equalsIgnoreCase(SeismicFormat.SAMPLES_IN_TRACE)) {
			return new Integer(SeismicFormat.HEADER_SAMPLES_IN_TRACE);
		} else if (s.equalsIgnoreCase(SeismicFormat.SAMPLE_RATE)) {
			return new Integer(SeismicFormat.HEADER_SAMPLE_RATE);
		} else if (s.equalsIgnoreCase(SeismicFormat.CDP_X_LOC)) {
			return new Integer(SeismicFormat.HEADER_CDP_X_LOC);
		} else if (s.equalsIgnoreCase(SeismicFormat.CDP_Y_LOC)) {
			return new Integer(SeismicFormat.HEADER_CDP_Y_LOC);
		} else if (s.equalsIgnoreCase(SeismicFormat.INLINE_NUMBER)) {
			return new Integer(SeismicFormat.HEADER_INLINE_NUMBER);
		} else if (s.equalsIgnoreCase(SeismicFormat.XLINE_NUMBER)) {
			return new Integer(SeismicFormat.HEADER_XLINE_NUMBER);
		} else if (s.equalsIgnoreCase(SeismicFormat.SHOTPOINT_NUMBER)) {
			return new Integer(SeismicFormat.HEADER_SHOTPOINT_NUMBER);
		}
		return null;
	}

	/**
	 * Converts SEGY data byte order constants from string
	 * @param s string representation of data byte order constant
	 * @return new instance of <code>ByteOrder</code> corresponding to given string
	 */
	public static ByteOrder getDataFormat(String s) {
		if (s.equalsIgnoreCase("LITTLE_ENDIAN")) {
			return ByteOrder.LITTLE_ENDIAN;
		} else if (s.equalsIgnoreCase("BIG_ENDIAN")) {
			return ByteOrder.BIG_ENDIAN;
		}
		return null;
	}

	/**
	 * Converts SEGY sample data format constants from string
	 * @param s string representation of sample data format constant
	 * @return corresponding integer value
	 */
	public static Integer getFormat(String s) {
		if (s.equalsIgnoreCase("INT8")) {
			return new Integer(SeismicConstants.DATA_FORMAT_BYTE);
		} else if (s.equalsIgnoreCase("INT16")) {
			return new Integer(SeismicConstants.DATA_FORMAT_SHORT);
		} else if (s.equalsIgnoreCase("INT32")) {
			return new Integer(SeismicConstants.DATA_FORMAT_INT);
		} else if (s.equalsIgnoreCase("FLOAT32")) {
			return new Integer(SeismicConstants.DATA_FORMAT_FLOAT);
		} else if (s.equalsIgnoreCase("IBM_FLOAT32")) {
			return new Integer(SeismicConstants.DATA_FORMAT_IBM_FLOAT);
		} else if (s.equalsIgnoreCase("FLOAT64")) {
			return new Integer(SeismicConstants.DATA_FORMAT_DOUBLE);
		} else if (s.equalsIgnoreCase("INT4")) {
			return new Integer(SeismicConstants.DATA_FORMAT_4BIT);
		} else if (s.equalsIgnoreCase("UINT8")) {
			return new Integer(SeismicConstants.DATA_FORMAT_UNSIGNEDBYTE);
		} else if (s.equalsIgnoreCase("UINT16")) {
			return new Integer(SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT);
		} else if (s.equalsIgnoreCase("UINT32")) {
			return new Integer(SeismicConstants.DATA_FORMAT_UNSIGNEDINT);
		}
		return null;
	}

	/**
	 * Converts SEGY unit constants from string
	 * @param s string representation of unit constant
	 * @return corresponding integer value
	 */
	public static Integer getUnits(String s) {
		if (s.equalsIgnoreCase("TIME")) {
			return new Integer(SeismicConstants.UNIT_TIME);
		} else if (s.equalsIgnoreCase("DEPTH_FEET")) {
			return new Integer(SeismicConstants.UNIT_DEPTH_FEET);
		} else if (s.equalsIgnoreCase("DEPTH_METERS")) {
			return new Integer(SeismicConstants.UNIT_DEPTH_METERS);
		} else if (s.equalsIgnoreCase("TRACES")) {
			return new Integer(SeismicConstants.UNIT_TRACE_NUMBER);
		}
		return null;
	}

	/**
	 * Converts SEGY type constants from string
	 * @param s string representation of SEGY type constant
	 * @return corresponding integer value
	 */
	public static Integer getDataType(String s) {
		if (s.equalsIgnoreCase("HORIZON")) {
			return new Integer(DataSliceType.DATA_HORIZON);
		} else if (s.equalsIgnoreCase("SEISMIC")) {
			return new Integer(DataSliceType.DATA_SEISMIC);
		} else if (s.equalsIgnoreCase("MODEL")) {
			return new Integer(DataSliceType.DATA_MODEL);
		}
		return null;
	}

	/**
	 * Converts SEGY trace order constants from string
	 * @param s string representation of SEGY trace order constant
	 * @return corresponding integer value
	 */
	public static Integer getDataOrder(String s) {
		if (s.equalsIgnoreCase("CROSSECTION")) {
			return new Integer(DataSliceType.DATA_CROSSECTION);
		} else if (s.equalsIgnoreCase("MAP")) {
			return new Integer(DataSliceType.DATA_MAP);
		}
		return null;
	}

	/**
	 * Converts SEGY data byte order constants to string
	 * @param order data byte order
	 * @return corresponding string value
	 */
	public static String getDataFormat(ByteOrder order) {
		if (order == ByteOrder.LITTLE_ENDIAN) {
			return "LITTLE_ENDIAN";
		} else if (order == ByteOrder.BIG_ENDIAN) {
			return "BIG_ENDIAN";
		} else {
			return null;
		}
	}

	/**
	 * Converts SEGY sample data format constants to string
	 * @param fmt SEGY sample data format
	 * @return corresponding string value
	 */
	public static String getFormat(int fmt) {
		if (fmt == SeismicConstants.DATA_FORMAT_BYTE) {
			return "INT8";
		} else if (fmt == SeismicConstants.DATA_FORMAT_SHORT) {
			return "INT16";
		} else if (fmt == SeismicConstants.DATA_FORMAT_INT) {
			return "INT32";
		} else if (fmt == SeismicConstants.DATA_FORMAT_FLOAT) {
			return "FLOAT32";
		} else if (fmt == SeismicConstants.DATA_FORMAT_IBM_FLOAT) {
			return "IBM_FLOAT32";
		} else if (fmt == SeismicConstants.DATA_FORMAT_DOUBLE) {
			return "FLOAT64";
		} else if (fmt == SeismicConstants.DATA_FORMAT_4BIT) {
			return "INT4";
		} else if (fmt == SeismicConstants.DATA_FORMAT_UNSIGNEDBYTE) {
			return "UINT8";
		} else if (fmt == SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT) {
			return "UINT16";
		} else if (fmt == SeismicConstants.DATA_FORMAT_UNSIGNEDINT) {
			return "UINT32";
		} else {
			return null;
		}
	}

	/**
	 * Converts SEGY units constants to string
	 * @param u SEGY units
	 * @return corresponding string value
	 */
	public static String getUnits(int u) {
		if (u == SeismicConstants.UNIT_TIME) {
			return "TIME";
		} else if (u == SeismicConstants.UNIT_DEPTH_FEET) {
			return "DEPTH_FEET";
		} else if (u == SeismicConstants.UNIT_DEPTH_METERS) {
			return "DEPTH_METERS";
		} else if (u == SeismicConstants.UNIT_TRACE_NUMBER) {
			return "TRACES";
		} else {
			return null;
		}
	}

	/**
	 * Converts SEGY type constants to string
	 * @param type SEGY type
	 * @return corresponding string value
	 */
	public static String getDataType(int type) {
		if (type == DataSliceType.DATA_HORIZON) {
			return "HORIZON";
		} else if (type == DataSliceType.DATA_SEISMIC) {
			return "SEISMIC";
		} else if (type == DataSliceType.DATA_MODEL) {
			return "MODEL";
		} else {
			return null;
		}
	}

	/**
	 * Gets data order as string
	 * @param order the data order as int
	 * @return the data order as string
	 */
	public static String getDataOrder(int order) {
		if (order == DataSliceType.DATA_CROSSECTION) {
			return "CROSSECTION";
		} else if (order == DataSliceType.DATA_MAP) {
			return "MAP";
		} else {
			return null;
		}
	}
}