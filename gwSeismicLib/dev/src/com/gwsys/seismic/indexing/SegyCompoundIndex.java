//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

import java.io.*;
import java.util.*;

import com.gwsys.seismic.indexing.internal.*;

/**
 * Instances of this class composes single indexes to allow multi-dimensional queries.
 * <p>Copyright (c) 2003</p>
 * <p>Company: INT Inc.</p>
 */
public class SegyCompoundIndex {
  private TreeMap _indexMap = null;

  /**
   * Constructs new instance of SegyCompoundIndex from supplied indexes
   * @param indexes indexes to be composed
   */
  public SegyCompoundIndex(SegyIndex[] indexes) {
    _indexMap = new TreeMap();
    for (int i = 0; i < indexes.length; i++) {
      _indexMap.put(indexes[i].getName(), indexes[i]);
    }
  }

  private void adjustStepSign(IndexKey k_from, IndexKey k_to, IndexKeyStep s) {
    if (k_to.compareTo(k_from) * s.getSign() < 0) {
      s.changeSign();
    }
  }

  private class PathSegmentIteratorImpl
      implements Enumeration {

    private double x1, x2, y1, y2, x, y, dx, dy, Px = 0, Py = 0, e, P;
    private boolean swap = false, zero = false;

    public PathSegmentIteratorImpl(DataSourcePath.Point[] segment) throws
        IOException {

      x1 = segment[0].getX().doubleValue();
      x2 = segment[1].getX().doubleValue();

      dx = (x2 - x1) != 0 ? Double.parseDouble(key1.getIncrement()) : 0;

      if ( (x2 - x1) * dx < 0) {
        dx = -dx;

      }
      if (dx != 0) {
        Px = Math.abs( (x2 - x1) / dx);

      }
      y1 = segment[0].getY().doubleValue();
      y2 = segment[1].getY().doubleValue();

      dy = (y2 - y1) != 0 ? Double.parseDouble(key2.getIncrement()) : 0;

      if ( (y2 - y1) * dy < 0) {
        dy = -dy;

      }
      if (dy != 0) {
        Py = Math.abs( (y2 - y1) / dy);

      }
      if (Py > Px) {
        swap = true;
        P = Px / Py;
      }
      else if (Px == 0) {
        P = 0;
        zero = true;
      }
      else {
        P = Py / Px;
      }

      x = x1;
      y = y1;
      e = P - 1 / 2;
      has_more = true;
    }

    private void calc_next() {
      if (zero) {
        has_more = false;
        return;
      }
      int sgnX = sgn(dx), sgnY = sgn(dy);

      if (e >= 0) {
        x += dx;
        y += dy;
        e += P - 1;
      }
      else {
        if (swap) {
          y += dy;
        }
        else {
          x += dx;
        }
        e += P;
      }

      if ( (swap && y * sgnY > y2 * sgnY) || (!swap && x * sgnX > x2 * sgnX)) {
        has_more = false;
        return;
      }
    }

    private int sgn(double value) {
      return value < 0 ? -1 : value > 0 ? 1 : 0;
    }

    private boolean has_more;

    public Object nextElement() {
      IndexPoint p = new IndexPoint(x, y);
      calc_next();
      return p;
    }

    public boolean hasMoreElements() {
      return has_more;
    }
  }

  private class IndexPoint
      implements Comparable {
    public IndexKey[] index;

    public IndexPoint(double _x, double _y) {
      index = new IndexKey[2];
      index[0] = IndexKey.createKey(key1.getType());
      index[1] = IndexKey.createKey(key2.getType());
      index[0].setValue(Double.toString(_x));
      index[1].setValue(Double.toString(_y));
    }

    public int compareTo(Object o) throws ClassCastException {
      if (o instanceof IndexPoint) {
        int ix = index[0].compareTo( ( (IndexPoint) o).index[0]),
            iy = index[1].compareTo( ( (IndexPoint) o).index[1]);
        return (ix != 0) ? ix : iy;
      }
      else {
        throw new ClassCastException("Not an index point");
      }
    }
  }

  private DataSourceKey key1, key2;
  private IndexKey k1_from, k1_to, k2_from, k2_to;
  private IndexKeyStep s1, s2;
  private SegyIndex indexes[];

  private boolean prepare_index(DataSourcePath path) {

    key1 = path.getFirstKey();
    key2 = path.getSecondKey();

    indexes = new SegyIndex[2];

    indexes[0] = (SegyIndex) _indexMap.get(key1.getName());
    indexes[1] = (SegyIndex) _indexMap.get(key2.getName());

    if (indexes[0] == null || indexes[1] == null) {
      return false;
    }

    key1.setType(indexes[0].getKeyMin().getType());
    key2.setType(indexes[0].getKeyMin().getType());

    key1.setIncrement(new String(indexes[0].getKeyStep().getValue().toString()));
    key2.setIncrement(new String(indexes[0].getKeyStep().getValue().toString()));
    return true;
  }

  /**
   * Gets list of traces corresponding to supplied <code>path</code>
   * @param path datasource path, defined as set of points in 2D index space
   * @return array of trace IDs
   * @throws IOException
   */
  public int[] getTraceList(DataSourcePath path) throws IOException {
    if (!prepare_index(path)) {
      return null;
    }
    Enumeration segments = path.enumerateSegments();
    Vector index_points = new Vector();
    int[] traces = null;
    IndexPoint last_point = null;
    while (segments.hasMoreElements()) {
      DataSourcePath.Point segment[] = (DataSourcePath.Point[]) segments.
          nextElement();

      Enumeration segment_iterator = new PathSegmentIteratorImpl(segment);
      boolean begin_segment = true;
      while (segment_iterator.hasMoreElements()) {
        IndexPoint p = (IndexPoint) segment_iterator.nextElement();
        if (begin_segment && last_point != null && last_point.compareTo(p) == 0) {
          begin_segment = false;
          continue;
        }
        if (!segment_iterator.hasMoreElements()) {
          last_point = p;
        }
        index_points.addElement(p);
      }
      ;
    }

    if (index_points.size() == 0) {
      return null;
    }
    Iterator iter = index_points.iterator();

    Vector vtraces = new Vector();
    boolean round = path.getRound();

    IndexPoint prev_point = null;

    while (iter.hasNext()) {
      IndexPoint p = (IndexPoint) iter.next();
      if (round) {
        p.index[0].round(indexes[0].getKeyStep());
        p.index[1].round(indexes[1].getKeyStep());
      }
      if (prev_point != null && p.compareTo(prev_point) == 0) {
        continue;
      }
      else {
        prev_point = p;
        int tx[] = indexes[0].getTraceList(p.index[0], p.index[0]);
        int ty[] = indexes[1].getTraceList(p.index[1], p.index[1]);
        if (tx != null && ty != null) {
          Arrays.sort(tx);
          Arrays.sort(ty);
          int t[] = RestrictedArrays.retain(tx, ty);
          if (t != null && t.length != 0) {
            vtraces.addElement(t);
          }
        }
      }
    }
    if (vtraces.size() == 0) {
      return null;
    }
    else {
      return strip(vtraces);
    }
  }

  private int[] strip(Vector traces) {
    if (traces.size() == 0) {
      return null;
    }
    int total_traces = 0;
    Iterator i = traces.iterator();
    while (i.hasNext()) {
      total_traces += ( (int[]) i.next()).length;

    }
    int[] trace_array = new int[total_traces];
    i = traces.iterator();
    int array_pos = 0;
    while (i.hasNext()) {
      int[] current = (int[]) i.next();
      for (int j = 0; j < current.length; j++) {
        trace_array[array_pos++] = current[j];
      }
    }
    return trace_array;
  }

  /**
   * Gets the traces corresponding to supplied keys
   * @param keys datasource keys
   * @return array of trace IDs
   * @throws IOException
   */
  public int[] getTraceList(DataSourceKeys keys) throws IOException {
    DataSourceRequest req = new DataSourceRequest(keys, _indexMap);
    int[] tmp = req.getTraceList();
    return tmp;
  }
}
