//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

import java.io.*;
import java.nio.*;

import com.gwsys.seismic.indexing.util.*;

/**
 * Instances of this class wraps a SEGY files and provide some useful information
 * <p>Copyright (c) 2003</p>
 * <p>INT Inc.</p>
 * @version 1.0
 */
public class DataSourceFile {

  /**
   * Creates an empty instance of datasource file
   */
  public DataSourceFile() {}

  /**
   * Creates new instance of datasource file from supplied information
   * @param name relative SEGY file name
   * @param basepath base path of SEGY file
   * @param order byte order of SEGY file
   * @throws IOException
   */
  public DataSourceFile(String name, String basepath, String order) throws
      IOException {
    this(name, basepath, SegyConstantsParser.getDataFormat(order));
  }

  /**
   * Creates new instance of datasource file from supplied information
   * @param name relative SEGY file name
   * @param basepath base path of SEGY file
   * @param order byte order of SEGY file
   * @throws IOException
   */
  public DataSourceFile(String name, String basepath, ByteOrder order) throws
      IOException {
    this._order = order;
    this._name = new String(name);
    SegyReaderFactory factory = SegyReaderFactory.getDefaultFactory();
    _fileFullName = (basepath != null && basepath.length() > 0) ?
        basepath + File.separator + name : name;
    SegyReader r = factory.createSegyReader(_fileFullName, this._order, true);
    _endTrace = r.getNumberOfTraces() - 1;
  }

  /**
   * Gets full file path with file name
   * @return file path with name
   */
  public String getFileFullName() {
    return _fileFullName;
  }

  /**
   * Sets full file path with file name
   * @param name file path with name
   */
  public void setFileFullName(String name) {
    _fileFullName = name;
  }

  private String _fileFullName = null;

  /**
   * Gets file name
   * @return file name
   */
  public String getFileName() {
    return _name;
  }

  /**
   * Sets file name
   * @param s file name
   */
  public void setFileName(String s) {
    _name = s;
  }

  private String _name = null;

  /**
   * Gets start trace
   * @return start trace
   */
  public int getStartTrace() {
    return _startTrace;
  }

  /**
   * Sets start trace
   * @param trace start trace
   */
  public void setStartTrace(int trace) {
    _startTrace = trace;
  }

  private int _startTrace = 0;

  /**
   * Gets end trace
   * @return end trace
   */
  public int getEndTrace() {
    return _endTrace;
  }

  /**
   * Sets end trace
   * @param trace end trace
   */
  public void setEndTrace(int trace) {
    _endTrace = trace;
  }

  private int _endTrace = 0;

  /**
   * Gets ordinal of this SEGY file
   * @return ordinal of this SEGY file
   */
  public int getSegyOrdinal() {
    return _segyOrdinal;
  }

  /**
   * Sets ordinal of this SEGY file
   * @param ordinal ordinal of this SEGY file
   */
  public void setSegyOrdinal(int ordinal) {
    _segyOrdinal = ordinal;
  }

  private int _segyOrdinal = 0;

  /**
   * Gets byte order of this SEGY file
   * @return byte order
   */
  public ByteOrder getOrder() {
    return _order;
  }

  /**
   * Sets byte order of this SEGY file
   * @param order byte order
   */
  public void setOrder(ByteOrder order) {
    _order = order;
  }

  private ByteOrder _order = null;
}