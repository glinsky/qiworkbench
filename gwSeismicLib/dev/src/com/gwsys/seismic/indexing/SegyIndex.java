//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

import java.io.*;
import java.nio.*;
import java.util.*;

import com.gwsys.seismic.indexing.internal.*;

class IndexStatus {
  public static final int NODE_FOUND = 0;
  public static final int NODE_ADJUSTED = 1;
  public static final int NODE_NOT_FOUND = -1;
}

/**
 * Allows querying SEGY index for list of trace IDs
 * <p>Copyright (c) 2003</p>
 * <p>Company: INT Inc.</p>
 * @version 1.0
 */
public class SegyIndex {
  private short _dataType = 0;
  private short _segyHeaderOffset = 0;
  private int _numPages = 0;
  private int _index = 0;
  private int _numValues = 0;
  private int _nodesPerPage = 0;
  private int[] _traceOffsets = null;
  private int[] _traceListLengths = null;
  private int _found;
  private SegyIndexFolder _indexFolder = null;
  private byte[] _buffer = null;
  private IndexKey _keyFound = null, _minKey = null, _maxKey = null;
  private IndexKeyStep _keyStep = null;
  private ByteBuffer _byte_buffer = null;
  private String _name = null;
  private short _numIndexes;

  /**
   * Constructs new instance of <code>SegyIndex</code>
   * @param holder contains SEGY index information
   * @param folder supplies shared access to an appropriate index file
   */
  public SegyIndex(SegyIndexHolder holder, SegyIndexFolder folder) {
    _name = holder.name;
    _index = holder.index;
    _minKey = holder.keyMin;
    _maxKey = holder.keyMax;
    _keyStep = holder.step;
    _dataType = holder.dataType;
    _numPages = holder.numPages;
    _numValues = holder.numValues;
    _numIndexes = holder.numIndexes;
    _segyHeaderOffset = holder.headerOffset;
    _indexFolder = folder;
    _nodesPerPage = (folder.getPageSize() - 4) /
        (8 * _numIndexes + IndexKey.createKey(_dataType).getByteLen());
  }

  private boolean adjustRange(IndexKey key1, IndexKey key2, IndexKeyStep step) {
    IndexKey min = getKeyMin(), max = getKeyMax(),
        left = key1.compareTo(key2) < 0 ? key1 : key2,
        right = key1.compareTo(key2) < 0 ? key2 : key1;
    if (left.compareTo(min) < 0) {
      left = min;
    }
    if (right.compareTo(max) > 0) {
      right = max;

    }
    if (left.compareTo(right) > 0) { //range is out of min..max
      return false;
    }

    int sgn = step.getSign();
    if (sgn == 0 || key2.compareTo(key1) * sgn < 0) { //key2 is unreacheable thru the loop
      return false;
    }

    return true;
  }

  /**
   * Name of index as it was provided during indexing
   * @return name of index
   */
  public String getName() {
    return _name;
  }

  /**
   * Ordinal of this index in the index file
   * @return ordinal
   */
  public int getOrdinal() {
    return _index;
  }

  /**
   * Offset of this index key in the trace header
   * @return key offset
   */
  public short getSegyHeaderOffset() {
    return _segyHeaderOffset;
  }

  /**
   * Minimal value of this index keys found during indexing.
   * @return minimal key value
   */
  public IndexKey getKeyMin() {
    return _minKey;
  }

  /**
   * Maximal value of this index keys found during indexing.
   * @return maximal key value
   */
  public IndexKey getKeyMax() {
    return _maxKey;
  }

  /**
   * Step of key values of this index
   * @return key step
   */
  public IndexKeyStep getKeyStep() {
    return _keyStep;
  }

  /**
   * Total number of unique key values in this index
   * @return number of key values
   */
  public int getNumberOfKeys() {
    return _numValues;
  }

  private int[] _getTraceList(IndexKey from, IndexKey to, IndexKeyStep step,
                              boolean interpolate) throws IOException {
    Collection v = getIndexCollection(from, to, step, interpolate);
    Iterator iter = v.iterator();
    int len = 0;
    while (iter.hasNext()) {
      SegyIndexItem item = (SegyIndexItem) iter.next();
      len += item._lengths[0];
    }
    if(len == 0) return null;
    int[] traces = new int[len];
    int i = 0;
    iter = v.iterator();
    while (iter.hasNext()) {
      SegyIndexItem item = (SegyIndexItem) iter.next();
      int[] sub = readTraceList(item._offsets[0], item._lengths[0]);
      for (int j = 0; j < sub.length; j++) {
        traces[i++] = sub[j];
      }
    }
    Arrays.sort(traces);
    return traces;
  }

  /**
   * Retreiving an array of trace IDs corresponding to the set of key values defined by range [from..to] and given increment
   * @param from lower bound of key range
   * @param to upper bound of key range
   * @param step key step
   * @param interpolate controls whether there will be key values interpolation (in case then some of key values does not exist) or not
   * @return array of trace IDs
   * @throws IOException
   */
  public int[] getTraceList(IndexKey from, IndexKey to, IndexKeyStep step,
                            boolean interpolate) throws IOException {
    if (from.compareTo(to) == 0) {
      return getTraceList(from);
    }

    if (step == null || step.isSolid()) {
      return getTraceList(from, to);
    }

    return _getTraceList(from, to, step, interpolate);
  }

  /**
   * Retreiving an array of trace IDs corresponding to the set of key values defined by range [from..to] and given increment
   * @param from lower bound of key range
   * @param to upper bound of key range
   * @param step key step
   * @param interpolate controls whether there will be key values interpolation (in case then some of key values does not exist) or not
   * @return array of trace IDs
   * @throws IOException
   */
  public int[] getTraceList(String from, String to, String step,
                            boolean interpolate) throws IOException {

    IndexKey kFrom = IndexKey.createKey(_dataType),
        kTo = IndexKey.createKey(_dataType);

    kFrom.setValue(from);
    kTo.setValue(to);

    if (kFrom.compareTo(kTo) == 0) {
      return getTraceList(kFrom);
    }

    if (step == null) {
      return getTraceList(kFrom, kTo);
    }

    IndexKeyStep kStep = IndexKeyStep.createKeyStep(_dataType);
    kStep.setValue(step);
    if (kStep.isSolid()) {
      return getTraceList(kFrom, kTo);
    }

    return _getTraceList(kFrom, kTo, kStep, interpolate);
  }

  /**
   * Retreiving an array of trace IDs corresponding to the given index item
   * @param item instance of index item
   * @return array of trace IDs
   * @throws IOException
   */
  public int[] getTraceList(SegyIndexItem item) throws IOException {
    return readTraceList(item._offsets[0], item._lengths[0]);
  }

  /**
   * Retreiving an array of trace IDs corresponding to the set of key values defined by range [from..to]
   * @param from lower bound of key range
   * @param to upper bound of key range
   * @return array of trace IDs
   * @throws IOException
   */
  public int[] getTraceList(IndexKey from, IndexKey to) throws IOException {
    int revers = to.compareTo(from);
    int status = findTraceListOffset(0, 1, 0, from, revers);
    if (status == IndexStatus.NODE_NOT_FOUND) {
      return null;
    }
    int startOffset = _traceOffsets[0];
    int startLength = _traceListLengths[0];

    status = findTraceListOffset(0, 1, 0, to, -revers);
    if (status == IndexStatus.NODE_NOT_FOUND) {
      return null;
    }

    int[] traces = null;
    int offset, length;

    if (revers > 0) {
      length = (_traceOffsets[0] - startOffset) / 4 + _traceListLengths[0];
      offset = startOffset;
    }
    else {
      length = (startOffset - _traceOffsets[0]) / 4 + startLength;
      offset = _traceOffsets[0];
    }

    traces = readTraceList(offset, length);
    if (traces != null && traces.length != 0) {
      Arrays.sort(traces);

    }
    return traces;
  }

  /**
   * Retreiving an array of trace IDs corresponding to the given key value
   * @param key value of index key
   * @return array of trace IDs
   * @throws IOException
   */
  public int[] getTraceList(String key) throws IOException {
    IndexKey k = IndexKey.createKey(_dataType);
    return getTraceList(k);
  }

  /**
   * Retreiving an array of trace IDs corresponding to the given key value
   * @param key value of index key
   * @return array of trace IDs
   * @throws IOException
   */
  public int[] getTraceList(IndexKey key) throws IOException {
    int status = findTraceListOffset(0, 1, 0, key, 0);
    if (status == IndexStatus.NODE_NOT_FOUND) {
      return null;
    }
    else {
      int[] traces = new int[_traceListLengths[0]];
      readTraceList(traces, _traceOffsets[0], _traceListLengths[0]);
      return traces;
    }
  }

  /**
   * Fills given array with trace IDs corresponding to the given key value
   * @param traces this array should have enough capacity to accommodate query results
   * @param key value of index key
   * @return number of traces actually retreived
   * @throws IOException
   * @throws ArrayIndexOutOfBoundsException
   */
  public int getTraceList(int[] traces, IndexKey key) throws IOException,
      ArrayIndexOutOfBoundsException {
    int status = findTraceListOffset(0, 1, 0, key, 0);
    if (status == IndexStatus.NODE_NOT_FOUND) {
      return 0;
    }
    else {
      readTraceList(traces, _traceOffsets[0], _traceListLengths[0]);
      return _traceListLengths[0];
    }
  }

  private SegyIndexItem getNextKey(IndexKey i, int lookAround) throws
      IOException {
    int status = findTraceListOffset(0, 1, 0, i, lookAround);
    if (status == IndexStatus.NODE_NOT_FOUND) {
      return null;
    }
    SegyIndexItem curr = new SegyIndexItem(_keyFound, _traceOffsets,
                                           _traceListLengths);
    return curr;
  }

  /**
   * Retrieves collection of <code>SegyIndexItem</code> items corresponding to the given query
   * @param from lower bound of key range
   * @param to upper bound of key range
   * @param step key step
   * @param interpolate controls whether there will be key values interpolation (in case then some of key values does not exist) or not
   * @return collection of index items
   * @throws IOException
   */
  public Collection getIndexCollection(IndexKey from, IndexKey to,
                                       IndexKeyStep step, boolean interpolate) throws
      IOException {

    IndexKey kFrom = (IndexKey) from.clone(), kTo = (IndexKey) to.clone();
    IndexKeyStep kStep = (IndexKeyStep) step.clone();

    if (!adjustRange(kFrom, kTo, kStep)) {
      return null;
    }
    Vector trace_collection = new Vector();
    IndexKey i = (IndexKey) kFrom.clone();
    SegyIndexItem current = null, next = null, prev = null, interpolated = null;
    double distance, distance_next, distance_prev,
        dstep = Math.abs(step.getValue().doubleValue());

    while (i.compareTo(kTo) * kStep.getSign() <= 0) {
      current = getNextKey(i, 0);

      if (current != null) { //found exact match
        trace_collection.add(current);
        i.addStep(kStep);
        continue;
      }

      next = getNextKey(i, step.getSign());

      if (interpolate) { //try to interpolate if necessary
        interpolated = null;
        distance_next = distance_prev = 1.;
        prev = getNextKey(i, -step.getSign());
        if (next != null) {
          distance_next = i.getDistance(next._key) / dstep;
        }
        if (prev != null) {
          distance_prev = i.getDistance(prev._key) / dstep;

        }
        if (distance_next <= 0.5) {
          interpolated = next;
        }
        if (distance_next < 0.5 && distance_prev < distance_next) {
          interpolated = prev;

        }
        if (interpolated != null) {
          trace_collection.add(interpolated);
          i.addStep(kStep);
          continue;
        }
      }

      if (next == null) { //no more keys to iterate
        break;
      }

      i.snapToGrid(i, kStep, next._key);
    }
    return trace_collection;
  }

  /**
   * Retreiving an array of trace IDs corresponding to the set of key values defined by range [from..to]
   * @param from lower bound of key range
   * @param to upper bound of key range
   * @return array of trace IDs
   * @throws IOException
   */
  public int[] getTraceList(String from, String to) throws IOException {
    IndexKey kFrom = IndexKey.createKey(_dataType),
        kTo = IndexKey.createKey(_dataType);
    kFrom.setValue(from);
    kTo.setValue(to);
    if (kFrom.compareTo(kTo) == 0) {
      return getTraceList(kFrom);
    }
    else {
      return getTraceList(kFrom, kTo);
    }
  }

  private int[] readTraceList(int offset, int length) throws IOException {
    int[] buf = _indexFolder.rawReadInt(offset, length);
    return buf;
  }

  private int readTraceList(int[] buf, int offset, int length) throws
      IOException, ArrayIndexOutOfBoundsException {
    if (buf == null || buf.length < length) {
      throw new ArrayIndexOutOfBoundsException();
    }
    _indexFolder.rawReadInt(buf, offset, length);
    return length;
  }

  private int findTraceListOffset(int pageIndex, int thisLevelPages,
                                  int prevLevelsPages, IndexKey key,
                                  int lookAround) throws IOException {
    getPage(pageIndex);

    short nodes = _byte_buffer.getShort(0);
    short links = _byte_buffer.getShort(2);

    int linkRequired = 0;

    if (keyCompare(key, 0) < 0) {
      linkRequired = 0;
    }
    else if (keyCompare(key, nodes - 1) > 0) {
      linkRequired = nodes;
    }
    else if (findKey(key, 0, nodes - 1)) {
      saveTrace(_found, key);
      return IndexStatus.NODE_FOUND;
    }
    else {
      linkRequired = _found;
    }

    if (linkRequired < links) {
      int nextPage = calcPageIndex(pageIndex, linkRequired, thisLevelPages,
                                   prevLevelsPages);
      int nextLevelPages = calcLevelPages(thisLevelPages);
      boolean backup = false;
      int backupNode = 0;
      byte[] buf = null;
      if (lookAround != 0) {
        if (lookAround < 0) {
          backupNode = linkRequired - 1;
        }
        else {
          backupNode = linkRequired;
        }
        if (backupNode > -1 && backupNode < nodes) {
          backup = true;
          buf = new byte[8 * _numIndexes + key.getByteLen()];
          backupNode(keyOffset(backupNode, key.getByteLen()), buf);
        }
      }
      int nextLevelSearch = findTraceListOffset(nextPage, nextLevelPages,
                                                thisLevelPages +
                                                prevLevelsPages, key,
                                                lookAround);

      if (nextLevelSearch != IndexStatus.NODE_NOT_FOUND) {
        return nextLevelSearch;
      }

      if (backup) {
        restoreNode(keyOffset(backupNode, key.getByteLen()), buf);
      }
    }

    int adjustedNode;

    if (lookAround < 0) {
      adjustedNode = linkRequired - 1;
    }
    else if (lookAround > 0) {
      adjustedNode = linkRequired;
    }
    else {
      return IndexStatus.NODE_NOT_FOUND;
    }

    if (adjustedNode < 0 || adjustedNode > nodes - 1) {
      return IndexStatus.NODE_NOT_FOUND;
    }
    else {
      saveTrace(adjustedNode, key);
      return IndexStatus.NODE_ADJUSTED;
    }
  }

  private void backupNode(int offset, byte[] buf) {
    for (int i = 0; i < buf.length; i++) {
      buf[i] = _buffer[i + offset];
    }
  }

  private void restoreNode(int offset, byte[] buf) {
    for (int i = 0; i < buf.length; i++) {
      _buffer[i + offset] = buf[i];
    }
  }

  private void saveTrace(int index, IndexKey key) throws IOException {
    int keySize = key.getByteLen();
    int keyOffset = keyOffset(index, keySize);
    _byte_buffer.position(keyOffset(index, keySize) + keySize);
    _traceOffsets = new int[_numIndexes];
    _traceListLengths = new int[_numIndexes];
    for (int i = 0; i < _numIndexes; i++) {
      _traceOffsets[i] = _byte_buffer.getInt();
      _traceListLengths[i] = _byte_buffer.getInt();
    }
    _keyFound = (IndexKey) key.clone();
    _keyFound.setValue(_byte_buffer, keyOffset);
  }

  private int calcLevelPages(int levelPages) {
    return levelPages * (_nodesPerPage + 1);
  }

  private int calcPageIndex(int thisPageIndex, int linkRequired,
                            int thisLevelPages, int prevLevelsPages) {
    int prevLinks = 0;
    if (thisPageIndex > prevLevelsPages) {
      prevLinks = (thisPageIndex - prevLevelsPages) * (_nodesPerPage + 1);
    }
    return prevLevelsPages + thisLevelPages + prevLinks + linkRequired;
  }

  private boolean findKey(IndexKey key, int left, int right) throws IOException {
    if ( (right - left) < 2) {
      if (keyCompare(key, left) == 0) {
        _found = left;
        return true;
      }
      if (keyCompare(key, right) == 0) {
        _found = right;
        return true;
      }
      _found = right;
      return false;
    }
    int mid = (right + left) / 2;
    int comp = keyCompare(key, mid);
    if (comp == 0) {
      _found = mid;
      return true;
    }
    else if (comp < 0) {
      return findKey(key, left, mid);
    }
    else {
      return findKey(key, mid, right);
    }
  }

  private int keyCompare(IndexKey key, int index) {
    return key.compareToBuffer(_byte_buffer, keyOffset(index, key.getByteLen()));
  }

  private int keyOffset(int keyIndex, int keySize) {
    return 4 + (8 * _numIndexes + keySize) * keyIndex;
  }

  private void getPage(int index) throws IOException {
    _buffer = _indexFolder.readIndexPage(index);
    _byte_buffer = ByteBuffer.wrap(_buffer);
  }
}