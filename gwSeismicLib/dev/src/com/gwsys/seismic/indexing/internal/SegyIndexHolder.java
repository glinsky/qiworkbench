//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import com.gwsys.seismic.indexing.*;

public class SegyIndexHolder {
	public short dataType;

	public short headerOffset;

	public int numPages;

	public int index;

	public int numValues;

	public int indexOffset;

	public IndexKey keyMin, keyMax;

	public IndexKeyStep step;

	public short numIndexes;

	public String name;

	public SegyIndexHolder(String name, int index, short dtype, int npages,
			int num_values, int offset, short hoffset, IndexKey min,
			IndexKey max, IndexKeyStep step, short numIndexes) {
		this.name = name;
		this.index = index;
		keyMin = min;
		keyMax = max;
		this.step = step;
		dataType = dtype;
		numPages = npages;
		numValues = num_values;
		indexOffset = offset;
		headerOffset = hoffset;
		this.numIndexes = numIndexes;
	}
}
