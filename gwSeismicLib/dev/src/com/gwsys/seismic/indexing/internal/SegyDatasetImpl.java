//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;
import java.nio.*;
import java.util.*;

import com.gwsys.seismic.indexing.*;
import com.gwsys.seismic.indexing.SeismicConstants;

public class SegyDatasetImpl implements SegyDataset {

	private SegyDataFormat _format = null;

	private DataSourceProfile profile = null;

	private SegyIndexFile indexfile = null;

	private SegyReader[] readers = null;

	private TreeMap indexes = null;

	private double cacheRate;

	private int cacheTreshold;

	private static final double CACHE_RATE = 0.05;

	private static final int CACHE_TRESHOLD = 32;

	private final double MICROSECONDS_PER_SECOND = 1000000.0;

	public SegyDatasetImpl(InputStream xmlDocStream, SegyDataFormat format)
			throws IOException {
		_format = format;
		profile = DataSourceProfile.loadProfile(xmlDocStream);
	}

	public SegyDatasetImpl(String basepath, String[] segy,
			String[] segy_dataorder, SegyDataFormat format) throws IOException {
		_format = format;
		profile = DataSourceProfile.createProfile(
				DataSourceProfile.DEFAULT_PROFILE_NAME, "CROSSECTION",
				"SEISMIC", basepath, segy, segy_dataorder);
	}

	public SegyDatasetImpl(String filename, SegyDataFormat format)
			throws IOException, FileNotFoundException {
		this(filename, format, CACHE_RATE, CACHE_TRESHOLD);
	}

	public SegyDatasetImpl(String filename, SegyDataFormat format,
			double cache_rate, int cache_treshold) throws IOException,
			FileNotFoundException {
		_format = format;
		profile = DataSourceProfile.loadProfile(filename);
		createIndexFile();
		indexes = new TreeMap();
		cacheRate = cache_rate;
		cacheTreshold = cache_treshold;
	}

	public DataSourceKeys getDatasourceKeys() {
		return profile.getDatasourceKeys();
	}

	public DataSourceFiles getSegys() {
		return profile.getDatasourceFiles();
	}

	public double getStartValue() {
		try {
			createReaders();
			double startValue = readers[0].getStartValue();
			return (getDataOrder() == DataSliceType.DATA_MAP) ? startValue
					: (startValue / MICROSECONDS_PER_SECOND);
		} catch (IOException e) {
			return 0;
		}
	}

	public int getNumSamplesPerTrace() throws IOException {
		createReaders();
		return readers[0].getNumSamplesPerTrace();
	}

	public int getUnits() {
		return profile.getSampleUnits();
	}

	public String getProfileName() {
		return profile.getProfileName();
	}

	public String getSegyFormatFile() {
		return profile.getSegyFormatFile();
	}

	public ByteOrder getDataFormat() {
		return profile.getDatasourceFiles().getItemByIndex(0).getOrder();
	}

	public String getBasePath() {
		return profile.getBasePath();
	}

	public boolean isSegyFilesExist() {
		if (profile.getBasePath() != null && profile.getBasePath().length() > 0) {
			for (int i = 0; i < profile.getDatasourceFiles().getCount(); i++) {
				String fileName = (profile.getBasePath() != null && profile
						.getBasePath().length() > 0) ? profile.getBasePath()
						+ File.separator
						+ profile.getDatasourceFiles().getItemByIndex(i)
								.getFileName() : profile.getDatasourceFiles()
						.getItemByIndex(i).getFileName();
				File file = new File(fileName);
				if (!file.exists()) {
					return false;
				}
			}
		}
		return true;
	}

	public int getDataOrder() {
		return profile.getDataOrder();
	}

	public int getDataType() {
		return profile.getDataType();
	}

	public int getTraceFormat() {
		return profile.getFormat();
	}

	public int getNumTraces() {
		return profile.getNumberOfTraces();
	}

	public String getTransposedKey() {
		return profile.getTransposedKey() != null ? profile.getTransposedKey()
				: "";
	}

	public String getTransposedName() {
		return profile.getTransposedName() != null ? profile
				.getTransposedName() : "";
	}

	public int getSampleInterval() {
		return profile.getSampleInterval();
	}

	public IndexedSegyTraceReader getTraceReader(DataSourcePath path)
			throws IOException {
		return new IndexedSegyTracePathReaderImpl(this, path);
	}

	public IndexedSegyTraceReader getTraceReader(DataSourceKeys keys)
			throws IOException {

		if (keys == null || keys.getCount() == 0) {
			DataSourceKeys _keys = new DataSourceKeys(1);
			DataSourceKey _key = new DataSourceKey();
			_key.setIncrement("1");
			_key.setMinValue("1");
			_key.setMaxValue(Integer.toString(profile.getNumberOfTraces()));
			_key.setName(DataSourceProfile.TRACE_ID_KEY);
			_key.setType((short) SeismicConstants.DATA_FORMAT_INT);
			_keys.addItem(_key);
			return new DummySegyTraceReaderImpl(this, _keys);
		} else if (keys.getCount() == 1
				&& keys.getItemByIndex(0).getName().compareTo(
						DataSourceProfile.TRACE_ID_KEY) == 0) {
			return new DummySegyTraceReaderImpl(this, keys);
		} else {
			return new IndexedSegyTraceReaderImpl(this, keys);
		}
	}

	public float getSampleMin() throws IOException {
		if (indexfile != null) {
			return indexfile.getSampleMin();
		} else {
			return 0.0f;
		}
	}

	public float getSampleMax() throws IOException {
		if (indexfile != null) {
			return indexfile.getSampleMax();
		} else {
			return 0.0f;
		}
	}

	public float getSampleMean() throws IOException {
		if (indexfile != null) {
			return indexfile.getSampleMean();
		} else {
			return 0.0f;
		}
	}

	public float getSampleRms() throws IOException {
		if (indexfile != null) {
			return indexfile.getSampleRms();
		} else {
			return 0.0f;
		}
	}

	private synchronized void createIndexFile() throws IOException,
			FileNotFoundException {
		if (indexfile == null) {
			indexfile = new SegyIndexFile(profile.getBasePath()
					+ File.separator + profile.getIndexFile());
		}
	}

	protected void createReaders() throws IOException {
		if (readers != null) {
			return;
		}
		SegyReaderFactory factory = SegyReaderFactory
				.getDefaultFactory(_format);
		int filcount = profile.getDatasourceFiles().getCount();
		readers = new SegyReader[filcount];
		for (int i = 0; i < filcount; i++) {
			String fileName = (profile.getBasePath() != null && profile
					.getBasePath().length() > 0) ? profile.getBasePath()
					+ File.separator
					+ profile.getDatasourceFiles().getItemByIndex(i)
							.getFileName() : profile.getDatasourceFiles()
					.getItemByIndex(i).getFileName();
			readers[profile.getDatasourceFiles().getItemByIndex(i)
					.getSegyOrdinal()] = factory.createSegyReader(fileName,
					profile.getDatasourceFiles().getItemByIndex(i).getOrder(),
					true);
		}
	}

	protected int getTraceDataSize(int offset, int count, int start_sample,
			int num_samples) {
		try {
			createReaders();
		} catch (IOException e) {
			return 0;
		}

		int trace_size = readers[0].getTraceByteLen();

		if (start_sample == 0 && num_samples == -1) {
			return readers[0].getTraceByteLen() * count;
		} else {
			return (readers[0].getSampleSize() * num_samples + _format
					.getTraceHeaderSize())
					* count;
		}
	}

	protected void readTraceData(int[] tracelist, int offset, int count,
			int start_sample, int num_samples, byte[] buf) throws IOException,
			ArrayIndexOutOfBoundsException {
		if (offset + count > tracelist.length) {
			throw new ArrayIndexOutOfBoundsException();
		}

		createReaders();

		int trace_size = readers[0].getTraceByteLen();
		int current = offset;
		int last = offset + count;

		while (current < last) {

			DataSourceFile f = profile.getDatasourceFiles().getItemByTrace(
					tracelist[current]);
			SegyReader r = readers[f.getSegyOrdinal()];

			while (current < last && tracelist[current] <= f.getEndTrace()) {
				int run = current + 1;

				while (run < last && tracelist[run] - tracelist[run - 1] == 1
						&& tracelist[run] <= f.getEndTrace()) {
					run++;
				}

				if (start_sample == 0 && num_samples == -1) {
					r.readTraceBulk(buf, (current - offset) * trace_size,
							tracelist[current] - f.getStartTrace(), run
									- current);
				} else {
					r.readTraceBulk(buf, (current - offset) * trace_size,
							tracelist[current] - f.getStartTrace(), run
									- current, start_sample, num_samples);
				}
				current = run;
			}
		}
	}

	protected byte[] readTraceData(int[] tracelist, int offset, int count,
			int start_sample, int num_samples) throws IOException,
			ArrayIndexOutOfBoundsException {
		if (offset + count > tracelist.length) {
			throw new ArrayIndexOutOfBoundsException();
		}
		createReaders();
		int trace_size = readers[0].getTraceByteLen();
		byte[] buf;
		if (start_sample == 0 && num_samples == -1) {
			buf = new byte[readers[0].getTraceByteLen() * count];
		} else {
			buf = new byte[(readers[0].getSampleSize() * num_samples + _format
					.getTraceHeaderSize())
					* count];
		}
		int current = offset;
		int last = offset + count;

		while (current < last) {

			DataSourceFile f = profile.getDatasourceFiles().getItemByTrace(
					tracelist[current]);
			SegyReader r = readers[f.getSegyOrdinal()];

			while (current < last && tracelist[current] <= f.getEndTrace()) {
				int run = current + 1;

				while (run < last && tracelist[run] - tracelist[run - 1] == 1
						&& tracelist[run] <= f.getEndTrace()) {
					run++;
				}

				if (start_sample == 0 && num_samples == -1) {
					r.readTraceBulk(buf, (current - offset) * trace_size,
							tracelist[current] - f.getStartTrace(), run
									- current);
				} else {
					r.readTraceBulk(buf, (current - offset) * trace_size,
							tracelist[current] - f.getStartTrace(), run
									- current, start_sample, num_samples);
				}
				current = run;
			}
		}
		return buf;
	}

	/**
	 * Gets used indexed in the indexed segy data source
	 * 
	 * @return the collection of the indexes
	 */
	public DataSourceKeys getIndexes() {
		return profile.getDatasourceKeys();
	}

	public SegyIndex getIndex(String name) throws IOException {
		if (indexfile == null) {
			throw new IOException("Index is missed or invalid");
		}
		SegyIndex index = (SegyIndex) indexes.get(name);
		if (index == null) {
			index = indexfile.getIndex(name, cacheRate, cacheTreshold);
			indexes.put(name, index);
		}
		return index;
	}

	private class DummySegyTraceReaderImpl implements IndexedSegyTraceReader {
		private SegyDatasetImpl _dataset = null;

		private DataSourceKeys _keys = null;

		private int _from, _to, _step, _count;

		private int[] _traceid = null;

		public DummySegyTraceReaderImpl(SegyDatasetImpl dataset,
				DataSourceKeys keys) {
			_dataset = dataset;
			_keys = keys;

			DataSourceKey k = keys.getItemByIndex(0);

			if (k.getMinValue() != null && k.getMinValue().length() > 0) {
				_from = Integer.parseInt(k.getMinValue()) - 1;
			} else {
				_from = 0;
			}

			if (k.getMaxValue() != null && k.getMaxValue().length() > 0) {
				_to = Integer.parseInt(k.getMaxValue()) - 1;
			} else {
				_to = _from;
			}

			if (k.getIncrement() != null && k.getIncrement().length() > 0) {
				_step = Integer.parseInt(k.getIncrement());
				if (_step == 0) {
					_step = 1;
				}
			} else {
				_step = 1;
			}
			_count = (_to - _from) / _step + 1;
		}

		public DataSourceKeys getIndexKeys() {
			return _keys;
		}

		public SegyDataset getSegyDataset() {
			return _dataset;
		}

		public int getNumTraces() {
			return _count;
		}

		private boolean check_interval(int from, int count) {
			return (from >= 0 && count > 0 && from + count <= _count);
		}

		public byte[] getTracesData(int from, int count, int from_sample,
				int num_samples) throws IOException {
			int[] traces = prepareRequestArray(from, count);
			return _dataset.readTraceData(traces, 0, count, from_sample,
					num_samples);
		}

		public byte[] getTracesData(int from, int count) throws IOException {
			return getTracesData(from, count, 0, -1);
		}

		public int getTracesDataSize(int from, int count, int from_sample,
				int num_samples) {
			return _dataset
					.getTraceDataSize(0, count, from_sample, num_samples);
		}

		public int getTracesDataSize(int from, int count) {
			return getTracesDataSize(from, count, 0, -1);
		}

		public void getTracesData(byte[] buf, int from, int count,
				int from_sample, int num_samples) throws IOException {
			int[] traces = prepareRequestArray(from, count);
			_dataset.readTraceData(traces, 0, count, from_sample, num_samples,
					buf);
		}

		public void getTracesData(byte[] buf, int from, int count)
				throws IOException {
			getTracesData(buf, from, count, 0, -1);
		}

		private int[] prepareRequestArray(int from, int count) {

			if (_traceid == null || _traceid.length < count) {
				_traceid = new int[count];
			}
			for (int i = 0; i < count; i++) {
				_traceid[i] = _from + _step * (from + i);
			}
			return _traceid;
		}
	}

	class IndexedSegyTracePathReaderImpl implements IndexedSegyTraceReader {
		private SegyDatasetImpl _dataset = null;

		private DataSourcePath _path = null;

		private int[] _traceid = null;

		public IndexedSegyTracePathReaderImpl(SegyDatasetImpl dataset,
				DataSourcePath path) throws IOException {
			_dataset = dataset;
			_path = path;
			loadIndex();
		}

		public DataSourceKeys getIndexKeys() {
			return null;
		}

		public SegyDataset getSegyDataset() {
			return _dataset;
		}

		public byte[] getTracesData(int from, int count) throws IOException {
			if (_traceid != null) {
				return _dataset.readTraceData(_traceid, from, count, 0, -1);
			} else {
				return null;
			}
		}

		public byte[] getTracesData(int from, int count, int from_sample,
				int num_samples) throws IOException {
			if (_traceid != null) {
				return _dataset.readTraceData(_traceid, from, count,
						from_sample, num_samples);
			} else {
				return null;
			}
		}

		public int getTracesDataSize(int from, int count, int from_sample,
				int num_samples) {
			return _dataset.getTraceDataSize(from, count, from_sample,
					num_samples);
		}

		public int getTracesDataSize(int from, int count) {
			return getTracesDataSize(from, count, 0, -1);
		}

		public void getTracesData(byte[] buf, int from, int count,
				int from_sample, int num_samples) throws IOException {
			if (_traceid != null) {
				_dataset.readTraceData(_traceid, from, count, from_sample,
						num_samples, buf);
			}
		}

		public void getTracesData(byte[] buf, int from, int count)
				throws IOException {
			getTracesData(buf, from, count, 0, -1);
		}

		public int getNumTraces() {
			return _traceid == null ? 0 : _traceid.length;
		}

		private void loadIndex() throws IOException {
			_dataset.createReaders();
			SegyIndex[] indexes = new SegyIndex[2];
			indexes[0] = _dataset.getIndex(_path.getFirstKey().getName());
			indexes[1] = _dataset.getIndex(_path.getSecondKey().getName());
			SegyCompoundIndex _index = new SegyCompoundIndex(indexes);
			_traceid = _index.getTraceList(_path);
		}
	}

	class IndexedSegyTraceReaderImpl implements IndexedSegyTraceReader {
		private SegyDatasetImpl _dataset = null;

		private DataSourceKeys _keys = null;

		private int[] _traceid = null;

		public IndexedSegyTraceReaderImpl(SegyDatasetImpl dataset,
				DataSourceKeys keys) throws IOException {
			_dataset = dataset;
			_keys = keys;
			loadIndex();
		}

		public DataSourceKeys getIndexKeys() {
			return _keys;
		}

		public SegyDataset getSegyDataset() {
			return _dataset;
		}

		public byte[] getTracesData(int from, int count) throws IOException {
			return _dataset.readTraceData(_traceid, from, count, 0, -1);
		}

		public byte[] getTracesData(int from, int count, int from_sample,
				int num_samples) throws IOException {
			return _dataset.readTraceData(_traceid, from, count, from_sample,
					num_samples);
		}

		public int getTracesDataSize(int from, int count, int from_sample,
				int num_samples) {
			return _dataset.getTraceDataSize(from, count, from_sample,
					num_samples);
		}

		public int getTracesDataSize(int from, int count) {
			return getTracesDataSize(from, count, 0, -1);
		}

		public void getTracesData(byte[] buf, int from, int count,
				int from_sample, int num_samples) throws IOException {
			if (_traceid != null) {
				_dataset.readTraceData(_traceid, from, count, from_sample,
						num_samples, buf);
			}
		}

		public void getTracesData(byte[] buf, int from, int count)
				throws IOException {
			getTracesData(buf, from, count, 0, -1);
		}

		public int getNumTraces() {
			return _traceid == null ? 0 : _traceid.length;
		}

		private void loadIndex() throws IOException {
			_dataset.createReaders();
			SegyIndex[] indexes = new SegyIndex[_keys.getCount()];
			for (int i = 0; i < _keys.getCount(); i++) {
				String iname = _keys.getItemByIndex(i).getName();
				indexes[i] = _dataset.getIndex(iname);
				if (indexes[i] == null)
					throw new IOException("Unknown index: " + iname);
			}
			SegyCompoundIndex _index = new SegyCompoundIndex(indexes);
			_traceid = _index.getTraceList(_keys);
		}
	}
}
