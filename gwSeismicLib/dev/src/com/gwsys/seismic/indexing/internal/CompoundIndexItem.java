//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;
import java.nio.*;

import com.gwsys.seismic.indexing.*;

public class CompoundIndexItem
    implements Comparable, Writeable {
  public IndexKey[] _keys;
  public static int _driveIndex;
  public int _trace_id;

  public CompoundIndexItem(IndexKey[] keys, int trace_id) {
    this._trace_id = trace_id;
    this._keys = keys;
  }

  public int write(ByteBuffer s) throws IOException {
    s.putInt(_trace_id);
    _keys[_driveIndex].write(s);
    return 4 + _keys[_driveIndex].getByteLen();
  }

  public int write(DataOutput s) throws IOException {
    s.writeInt(_trace_id);
    _keys[_driveIndex].write(s);
    return 4 + _keys[_driveIndex].getByteLen();
  }

  public int compareTo(Object o) {
    return _keys[_driveIndex].compareTo( ( (CompoundIndexItem) o)._keys[
                                        _driveIndex]);
  }
}