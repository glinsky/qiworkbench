//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;
import java.nio.*;

import com.gwsys.seismic.indexing.*;
import com.gwsys.seismic.indexing.util.*;
import com.gwsys.seismic.indexing.SeismicConstants;

public class IndexKey_r64 extends IndexKey {
	private double _value;

	private static final short _type = SeismicConstants.DATA_FORMAT_DOUBLE;

	public void snapToGrid(IndexKey start, IndexKeyStep step, IndexKey key)
			throws ClassCastException {
		if (!(start instanceof IndexKey_r64) || !(key instanceof IndexKey_r64)
				|| !(step instanceof IndexKeyStep_r64)) {
			throw new ClassCastException(errcast);
		}

		double start_value = ((IndexKey_r64) start)._value;
		double key_value = ((IndexKey_r64) key)._value;
		double step_value = ((IndexKeyStep_r64) step)._value;
		double increment = Math.floor((key_value - start_value) / step_value);
		if (increment == 0) {
			increment = 1;
		}
		_value = (double) (start_value + increment * step_value);
	}

	public boolean isInGrid(IndexKey start, IndexKey end, IndexKeyStep step)
			throws ClassCastException {
		if (start instanceof IndexKey_r64 && end instanceof IndexKey_r64
				&& step instanceof IndexKeyStep_r64) {
			if (_value >= ((IndexKey_r64) start)._value
					&& _value <= ((IndexKey_r64) end)._value
					&& Math.IEEEremainder(
							(_value - ((IndexKey_r64) start)._value),
							((IndexKeyStep_r64) step)._value) == 0) {
				return true;
			} else {
				return false;
			}
		} else {
			throw new ClassCastException(errcast);
		}
	}

	protected void value(double v) {
		_value = v;
	}

	protected double _value() {
		return _value;
	}

	public void setRandom() {
		_value = (double) (Math.floor(((Math.random() * 2.d) - 1.d)
				* Double.MAX_VALUE));
	}

	public void round(IndexKeyStep step) throws ClassCastException {
		if (step instanceof IndexKeyStep_r64) {
			_value = round(_value, ((IndexKeyStep_r64) step)._value);
		} else {
			throw new ClassCastException(errcast);
		}
	}

	public short getType() {
		return _type;
	}

	public void setValue(IndexKey key) throws ClassCastException {
		if (key instanceof IndexKey_r64) {
			_value = ((IndexKey_r64) key)._value;
		} else {
			throw new ClassCastException(errcast);
		}
	}

	public Object getValue() {
		return new Double(_value);
	}

	public int compareTo(Object ob) throws ClassCastException {
		if (ob instanceof IndexKey_r64) {
			return _value < ((IndexKey_r64) ob)._value ? -1
					: _value == ((IndexKey_r64) ob)._value ? 0 : 1;
		} else {
			throw new ClassCastException(errcast);
		}
	}

	public int compareToBuffer(ByteBuffer b, int offset) {
		double val = _value(b, offset);
		return _value < val ? -1 : _value == val ? 0 : 1;
	}

	public double getDistance(IndexKey k) throws ClassCastException {
		if (k instanceof IndexKey_r64) {
			return Math.abs((double) _value
					- (double) ((IndexKey_r64) k)._value);
		} else {
			throw new ClassCastException(errcast);
		}
	}

	public void mul(double v) {
		_value *= v;
	}

	public void setValue(DataInput f) throws IOException {
		_value = f.readDouble();
	}

	public void setValue(RestrictedByteBuffer b, int offset) {
		b.setPosition(offset);
		_value = b.getDouble();
	}

	public void setValue(ByteBuffer b, int offset) {
		_value = _value(b, offset);
	}

	private double _value(ByteBuffer b, int offset) {
		b.position(offset);
		return b.getDouble();
	}

	public int getByteLen() {
		return 8;
	}

	public void addStep(IndexKeyStep step) throws ClassCastException {
		if (step instanceof IndexKeyStep_r64) {
			_value += ((IndexKeyStep_r64) step)._value;
		} else {
			throw new ClassCastException(errcast);
		}
	}

	public void sub(IndexKey key, IndexKeyStep step) throws ClassCastException {
		if (key instanceof IndexKey_r64 && step instanceof IndexKeyStep_r64) {
			((IndexKeyStep_r64) step)._value = (double) (_value - ((IndexKey_r64) key)._value);
		} else {
			throw new ClassCastException(errcast);
		}
	}

	public void setValue(String s) {
		//    _value = Double.parseDouble(s);
		_value = Double.valueOf(s).doubleValue();
	}

	public int write(DataOutput f) throws IOException {
		f.writeDouble(_value);
		return 8;
	}

	public int write(ByteBuffer f) throws IOException {
		f.putDouble(_value);
		return 1;
	}
}
