//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.nio.*;

/**
 * This callback interface supplies implementing of interactive indexing GUI
 * wrappers
 * 
 * @version 1.0
 */
public interface SegyProcessingCallback {
	/**
	 * Called by indexing framework in case of start processing new SEGY file
	 * 
	 * @param name
	 *            SEGY file name
	 * @param index
	 *            ordinal index of this SEGY file
	 * @param start_trace
	 *            start trace id of this SEGY file in a whole sequence
	 * @param end_trace
	 *            end trace id of this SEGY file in a whole sequence
	 * @param sample_units
	 *            sample units of this SEGY file
	 * @param sample_interval
	 *            sample interval of this SEGY file
	 * @param order
	 *            byte order of this SEGY file
	 */
	void addSegy(String name, int index, int start_trace, int end_trace,
			int sample_units, int sample_interval, ByteOrder order);

	/**
	 * Called by indexing framework to inform user about current processing
	 * status
	 * 
	 * @param msg
	 *            status message
	 */
	void printMessage(String msg);
}