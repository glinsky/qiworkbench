//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.util.TreeMap;

class PageQueue {
	private PageList _list;

	private TreeMap _map;

	private int _capacity;

	private ComparableInteger _key;

	public PageQueue(int capacity) {
		this._capacity = capacity;
		_list = new PageList();
		_map = new TreeMap();
		_key = new ComparableInteger(0);
	}

	public byte[] getPage(int index) {
		_key.setValue(index);
		PageListItem item = (PageListItem) _map.get(_key);
		if (item == null) {
			return null;
		} else {
			_list.reinsertItem(item);
			return item._page;
		}
	}

	public void addPage(int index, byte[] data) {
		PageListItem item = new PageListItem(index, data);
		_map.put(new ComparableInteger(index), item);
		_list.reinsertItem(item);
		if (_map.size() > _capacity) {
			item = _list.pushBack();
			_key.setValue(item._pageIndex);
			_map.remove(_key);
		}
	}
}
