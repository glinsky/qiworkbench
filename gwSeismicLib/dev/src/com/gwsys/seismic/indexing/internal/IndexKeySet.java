//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.util.*;

import com.gwsys.seismic.indexing.*;
import com.gwsys.seismic.indexing.SeismicConstants;

public abstract class IndexKeySet {
	protected int _i, _size = -1;

	protected static final String _err_noroom = "No room for an element";

	protected static final String _err_nosuch = "No more elements";

	protected static final String _err_cast = "Invalid index type";

	public static IndexKeySet createKeySet(int size, short format) {
		IndexKeySet k = null;
		switch (format) {
		case SeismicConstants.DATA_FORMAT_4BIT:
			k = new IndexKeySet_ui4(size);
			break;
		case SeismicConstants.DATA_FORMAT_BYTE:
			k = new IndexKeySet_i8(size);
			break;
		case SeismicConstants.DATA_FORMAT_UNSIGNEDBYTE:
			k = new IndexKeySet_ui8(size);
			break;
		case SeismicConstants.DATA_FORMAT_SHORT:
			k = new IndexKeySet_i16(size);
			break;
		case SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT:
			k = new IndexKeySet_ui16(size);
			break;
		case SeismicConstants.DATA_FORMAT_INT:
			k = new IndexKeySet_i32(size);
			break;
		case SeismicConstants.DATA_FORMAT_UNSIGNEDINT:
			k = new IndexKeySet_ui32(size);
			break;
		case SeismicConstants.DATA_FORMAT_FLOAT:
			k = new IndexKeySet_r32(size);
			break;
		case SeismicConstants.DATA_FORMAT_DOUBLE:
			k = new IndexKeySet_r64(size);
			break;
		}
		return k;
	}

	public abstract void addItem(IndexKey key) throws ClassCastException,
			ArrayIndexOutOfBoundsException;

	public abstract int getSize();

	public abstract IndexKeySetIterator iterator();
}

class IndexKeySet_r64 extends IndexKeySet {
	private static short _fmt = SeismicConstants.DATA_FORMAT_DOUBLE;

	private double[] _ar;

	private int _i = 0;

	protected IndexKeySet_r64(int size) {
		_ar = new double[size];
	}

	public void addItem(IndexKey key) throws ClassCastException,
			ArrayIndexOutOfBoundsException {
		if (_i == _ar.length) {
			throw new ArrayIndexOutOfBoundsException(_err_noroom);
		}

		if (!(key instanceof IndexKey_r64)) {
			throw new ClassCastException(_err_cast);
		}

		_ar[_i++] = ((IndexKey_r64) key)._value();
		_size = -1;
	}

	public int getSize() {
		if (_size == -1) {
			_size = 0;
			if (_i > 0) {
				Arrays.sort(_ar, 0, _i);
				double cur = _ar[0];
				_size++;
				for (int i = 1; i < _i; i++) {
					double c = _ar[i];
					if (c != cur) {
						cur = c;
						_size++;
					}
				}
			}
		}
		return _size;
	}

	public IndexKeySetIterator iterator() {
		if (_size == -1) {
			getSize();
		}
		return new IndexKeySetIterator_r64();
	}

	private class IndexKeySetIterator_r64 implements IndexKeySetIterator {
		private int _index;

		public boolean hasNext() {
			return _index < _i;
		}

		public void next(IndexKey key) throws ClassCastException,
				NoSuchElementException {
			if (!(key instanceof IndexKey_r64)) {
				throw new ClassCastException(_err_cast);
			}

			if (!hasNext()) {
				throw new NoSuchElementException(_err_nosuch);
			}

			((IndexKey_r64) key).value(_ar[_index]);
			shiftToNext();
		}

		private void shiftToNext() {
			double c = _ar[_index];
			while (++_index < _i && _ar[_index] == c) {
				;
			}
		}
	}
}

class IndexKeySet_r32 extends IndexKeySet {
	private static short _fmt = SeismicConstants.DATA_FORMAT_FLOAT;

	private float[] _ar;

	private int _i = 0;

	protected IndexKeySet_r32(int size) {
		_ar = new float[size];
	}

	public void addItem(IndexKey key) throws ClassCastException,
			ArrayIndexOutOfBoundsException {
		if (_i == _ar.length) {
			throw new ArrayIndexOutOfBoundsException(_err_noroom);
		}

		if (!(key instanceof IndexKey_r32)) {
			throw new ClassCastException(_err_cast);
		}

		_ar[_i++] = ((IndexKey_r32) key)._value();
		_size = -1;
	}

	public int getSize() {
		if (_size == -1) {
			_size = 0;
			if (_i > 0) {
				Arrays.sort(_ar, 0, _i);
				float cur = _ar[0];
				_size++;
				for (int i = 1; i < _i; i++) {
					float c = _ar[i];
					if (c != cur) {
						cur = c;
						_size++;
					}
				}
			}
		}
		return _size;
	}

	public IndexKeySetIterator iterator() {
		if (_size == -1) {
			getSize();
		}
		return new IndexKeySetIterator_r32();
	}

	private class IndexKeySetIterator_r32 implements IndexKeySetIterator {
		private int _index;

		public boolean hasNext() {
			return _index < _i;
		}

		public void next(IndexKey key) throws ClassCastException,
				NoSuchElementException {
			if (!(key instanceof IndexKey_r32)) {
				throw new ClassCastException(_err_cast);
			}

			if (!hasNext()) {
				throw new NoSuchElementException(_err_nosuch);
			}

			((IndexKey_r32) key).value(_ar[_index]);
			shiftToNext();
		}

		private void shiftToNext() {
			float c = _ar[_index];
			while (++_index < _i && _ar[_index] == c) {
				;
			}
		}
	}
}

class IndexKeySet_i32 extends IndexKeySet {
	private static short _fmt = SeismicConstants.DATA_FORMAT_INT;

	private int[] _ar;

	private int _i = 0;

	protected IndexKeySet_i32(int size) {
		_ar = new int[size];
	}

	public void addItem(IndexKey key) throws ClassCastException,
			ArrayIndexOutOfBoundsException {
		if (_i == _ar.length) {
			throw new ArrayIndexOutOfBoundsException(_err_noroom);
		}

		if (!(key instanceof IndexKey_i32)) {
			throw new ClassCastException(_err_cast);
		}

		_ar[_i++] = ((IndexKey_i32) key)._value();
		_size = -1;
	}

	public int getSize() {
		if (_size == -1) {
			_size = 0;
			if (_i > 0) {
				Arrays.sort(_ar, 0, _i);
				int cur = _ar[0];
				_size++;
				for (int i = 1; i < _i; i++) {
					int c = _ar[i];
					if (c != cur) {
						cur = c;
						_size++;
					}
				}
			}
		}
		return _size;
	}

	public IndexKeySetIterator iterator() {
		if (_size == -1) {
			getSize();
		}
		return new IndexKeySetIterator_i32();
	}

	private class IndexKeySetIterator_i32 implements IndexKeySetIterator {
		private int _index;

		public boolean hasNext() {
			return _index < _i;
		}

		public void next(IndexKey key) throws ClassCastException,
				NoSuchElementException {
			if (!(key instanceof IndexKey_i32)) {
				throw new ClassCastException(_err_cast);
			}

			if (!hasNext()) {
				throw new NoSuchElementException(_err_nosuch);
			}

			((IndexKey_i32) key).value(_ar[_index]);
			shiftToNext();
		}

		private void shiftToNext() {
			int c = _ar[_index];
			while (++_index < _i && _ar[_index] == c) {
				;
			}
		}
	}
}

class IndexKeySet_ui32 extends IndexKeySet {
	private static short _fmt = SeismicConstants.DATA_FORMAT_UNSIGNEDINT;

	private long[] _ar;

	private int _i = 0;

	protected IndexKeySet_ui32(int size) {
		_ar = new long[size];
	}

	public void addItem(IndexKey key) throws ClassCastException,
			ArrayIndexOutOfBoundsException {
		if (_i == _ar.length) {
			throw new ArrayIndexOutOfBoundsException(_err_noroom);
		}

		if (!(key instanceof IndexKey_ui32)) {
			throw new ClassCastException(_err_cast);
		}

		_ar[_i++] = ((IndexKey_ui32) key)._value();
		_size = -1;
	}

	public int getSize() {
		if (_size == -1) {
			_size = 0;
			if (_i > 0) {
				Arrays.sort(_ar, 0, _i);
				long cur = _ar[0];
				_size++;
				for (int i = 1; i < _i; i++) {
					long c = _ar[i];
					if (c != cur) {
						cur = c;
						_size++;
					}
				}
			}
		}
		return _size;
	}

	public IndexKeySetIterator iterator() {
		if (_size == -1) {
			getSize();
		}
		return new IndexKeySetIterator_ui32();
	}

	private class IndexKeySetIterator_ui32 implements IndexKeySetIterator {
		private int _index;

		public boolean hasNext() {
			return _index < _i;
		}

		public void next(IndexKey key) throws ClassCastException,
				NoSuchElementException {
			if (!(key instanceof IndexKey_ui32)) {
				throw new ClassCastException(_err_cast);
			}

			if (!hasNext()) {
				throw new NoSuchElementException(_err_nosuch);
			}

			((IndexKey_ui32) key).value(_ar[_index]);
			shiftToNext();
		}

		private void shiftToNext() {
			long c = _ar[_index];
			while (++_index < _i && _ar[_index] == c) {
				;
			}
		}
	}
}

class IndexKeySet_i16 extends IndexKeySet {
	private static short _fmt = SeismicConstants.DATA_FORMAT_SHORT;

	private short[] _ar;

	private int _i = 0;

	protected IndexKeySet_i16(int size) {
		_ar = new short[size];
	}

	public void addItem(IndexKey key) throws ClassCastException,
			ArrayIndexOutOfBoundsException {
		if (_i == _ar.length) {
			throw new ArrayIndexOutOfBoundsException(_err_noroom);
		}

		if (!(key instanceof IndexKey_i16)) {
			throw new ClassCastException(_err_cast);
		}

		_ar[_i++] = ((IndexKey_i16) key)._value();
		_size = -1;
	}

	public int getSize() {
		if (_size == -1) {
			_size = 0;
			if (_i > 0) {
				Arrays.sort(_ar, 0, _i);
				short cur = _ar[0];
				_size++;
				for (int i = 1; i < _i; i++) {
					short c = _ar[i];
					if (c != cur) {
						cur = c;
						_size++;
					}
				}
			}
		}
		return _size;
	}

	public IndexKeySetIterator iterator() {
		if (_size == -1) {
			getSize();
		}
		return new IndexKeySetIterator_i16();
	}

	private class IndexKeySetIterator_i16 implements IndexKeySetIterator {
		private int _index;

		public boolean hasNext() {
			return _index < _i;
		}

		public void next(IndexKey key) throws ClassCastException,
				NoSuchElementException {
			if (!(key instanceof IndexKey_i16)) {
				throw new ClassCastException(_err_cast);
			}

			if (!hasNext()) {
				throw new NoSuchElementException(_err_nosuch);
			}

			((IndexKey_i16) key).value(_ar[_index]);
			shiftToNext();
		}

		private void shiftToNext() {
			short c = _ar[_index];
			while (++_index < _i && _ar[_index] == c) {
				;
			}
		}
	}
}

class IndexKeySet_ui16 extends IndexKeySet {
	private static short _fmt = SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT;

	private int[] _ar;

	private int _i = 0;

	protected IndexKeySet_ui16(int size) {
		_ar = new int[size];
	}

	public void addItem(IndexKey key) throws ClassCastException,
			ArrayIndexOutOfBoundsException {
		if (_i == _ar.length) {
			throw new ArrayIndexOutOfBoundsException(_err_noroom);
		}

		if (!(key instanceof IndexKey_ui16)) {
			throw new ClassCastException(_err_cast);
		}

		_ar[_i++] = ((IndexKey_ui16) key)._value();
		_size = -1;
	}

	public int getSize() {
		if (_size == -1) {
			_size = 0;
			if (_i > 0) {
				Arrays.sort(_ar, 0, _i);
				int cur = _ar[0];
				_size++;
				for (int i = 1; i < _i; i++) {
					int c = _ar[i];
					if (c != cur) {
						cur = c;
						_size++;
					}
				}
			}
		}
		return _size;
	}

	public IndexKeySetIterator iterator() {
		if (_size == -1) {
			getSize();
		}
		return new IndexKeySetIterator_ui16();
	}

	private class IndexKeySetIterator_ui16 implements IndexKeySetIterator {
		private int _index;

		public boolean hasNext() {
			return _index < _i;
		}

		public void next(IndexKey key) throws ClassCastException,
				NoSuchElementException {
			if (!(key instanceof IndexKey_ui16)) {
				throw new ClassCastException(_err_cast);
			}

			if (!hasNext()) {
				throw new NoSuchElementException(_err_nosuch);
			}

			((IndexKey_ui16) key).value(_ar[_index]);
			shiftToNext();
		}

		private void shiftToNext() {
			int c = _ar[_index];
			while (++_index < _i && _ar[_index] == c) {
				;
			}
		}
	}
}

class IndexKeySet_i8 extends IndexKeySet {
	private static short _fmt = SeismicConstants.DATA_FORMAT_BYTE;

	private byte[] _ar;

	private int _i = 0;

	protected IndexKeySet_i8(int size) {
		_ar = new byte[size];
	}

	public void addItem(IndexKey key) throws ClassCastException,
			ArrayIndexOutOfBoundsException {
		if (_i == _ar.length) {
			throw new ArrayIndexOutOfBoundsException(_err_noroom);
		}

		if (!(key instanceof IndexKey_i8)) {
			throw new ClassCastException(_err_cast);
		}

		_ar[_i++] = ((IndexKey_i8) key)._value();
		_size = -1;
	}

	public int getSize() {
		if (_size == -1) {
			_size = 0;
			if (_i > 0) {
				Arrays.sort(_ar, 0, _i);
				byte cur = _ar[0];
				_size++;
				for (int i = 1; i < _i; i++) {
					byte c = _ar[i];
					if (c != cur) {
						cur = c;
						_size++;
					}
				}
			}
		}
		return _size;
	}

	public IndexKeySetIterator iterator() {
		if (_size == -1) {
			getSize();
		}
		return new IndexKeySetIterator_i8();
	}

	private class IndexKeySetIterator_i8 implements IndexKeySetIterator {
		private int _index;

		public boolean hasNext() {
			return _index < _i;
		}

		public void next(IndexKey key) throws ClassCastException,
				NoSuchElementException {
			if (!(key instanceof IndexKey_i8)) {
				throw new ClassCastException(_err_cast);
			}

			if (!hasNext()) {
				throw new NoSuchElementException(_err_nosuch);
			}

			((IndexKey_i8) key).value(_ar[_index]);
			shiftToNext();
		}

		private void shiftToNext() {
			byte c = _ar[_index];
			while (++_index < _i && _ar[_index] == c) {
				;
			}
		}
	}
}

class IndexKeySet_ui8 extends IndexKeySet {
	private static short _fmt = SeismicConstants.DATA_FORMAT_UNSIGNEDBYTE;

	private short[] _ar;

	private int _i = 0;

	protected IndexKeySet_ui8(int size) {
		_ar = new short[size];
	}

	public void addItem(IndexKey key) throws ClassCastException,
			ArrayIndexOutOfBoundsException {
		if (_i == _ar.length) {
			throw new ArrayIndexOutOfBoundsException(_err_noroom);
		}

		if (!(key instanceof IndexKey_ui8)) {
			throw new ClassCastException(_err_cast);
		}

		_ar[_i++] = ((IndexKey_ui8) key)._value();
		_size = -1;
	}

	public int getSize() {
		if (_size == -1) {
			_size = 0;
			if (_i > 0) {
				Arrays.sort(_ar, 0, _i);
				short cur = _ar[0];
				_size++;
				for (int i = 1; i < _i; i++) {
					short c = _ar[i];
					if (c != cur) {
						cur = c;
						_size++;
					}
				}
			}
		}
		return _size;
	}

	public IndexKeySetIterator iterator() {
		if (_size == -1) {
			getSize();
		}
		return new IndexKeySetIterator_ui8();
	}

	private class IndexKeySetIterator_ui8 implements IndexKeySetIterator {
		private int _index;

		public boolean hasNext() {
			return _index < _i;
		}

		public void next(IndexKey key) throws ClassCastException,
				NoSuchElementException {
			if (!(key instanceof IndexKey_ui8)) {
				throw new ClassCastException(_err_cast);
			}

			if (!hasNext()) {
				throw new NoSuchElementException(_err_nosuch);
			}

			((IndexKey_ui8) key).value(_ar[_index]);
			shiftToNext();
		}

		private void shiftToNext() {
			short c = _ar[_index];
			while (++_index < _i && _ar[_index] == c) {
				;
			}
		}
	}
}

class IndexKeySet_ui4 extends IndexKeySet {
	private static short _fmt = SeismicConstants.DATA_FORMAT_4BIT;

	private byte[] _ar;

	private int _i = 0;

	protected IndexKeySet_ui4(int size) {
		_ar = new byte[size];
	}

	public void addItem(IndexKey key) throws ClassCastException,
			ArrayIndexOutOfBoundsException {
		if (_i == _ar.length) {
			throw new ArrayIndexOutOfBoundsException(_err_noroom);
		}

		if (!(key instanceof IndexKey_ui4)) {
			throw new ClassCastException(_err_cast);
		}

		_ar[_i++] = ((IndexKey_ui4) key)._value();
		_size = -1;
	}

	public int getSize() {
		if (_size == -1) {
			_size = 0;
			if (_i > 0) {
				Arrays.sort(_ar, 0, _i);
				byte cur = _ar[0];
				_size++;
				for (int i = 1; i < _i; i++) {
					byte c = _ar[i];
					if (c != cur) {
						cur = c;
						_size++;
					}
				}
			}
		}
		return _size;
	}

	public IndexKeySetIterator iterator() {
		if (_size == -1) {
			getSize();
		}
		return new IndexKeySetIterator_ui4();
	}

	private class IndexKeySetIterator_ui4 implements IndexKeySetIterator {
		private int _index;

		public boolean hasNext() {
			return _index < _i;
		}

		public void next(IndexKey key) throws ClassCastException,
				NoSuchElementException {
			if (!(key instanceof IndexKey_ui4)) {
				throw new ClassCastException(_err_cast);
			}

			if (!hasNext()) {
				throw new NoSuchElementException(_err_nosuch);
			}

			((IndexKey_ui4) key).value(_ar[_index]);
			shiftToNext();
		}

		private void shiftToNext() {
			byte c = _ar[_index];
			while (++_index < _i && _ar[_index] == c) {
				;
			}
		}
	}
}
