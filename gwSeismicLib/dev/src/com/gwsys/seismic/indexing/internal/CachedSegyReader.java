//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;
import java.nio.*;

import com.gwsys.seismic.indexing.*;

public class CachedSegyReader
    extends SegyReaderBase {

  protected int adjustTraceBufferPosition(int pos) {
    return current_pos + pos;
  }

  public static boolean ADJUST_HEADER_CACHE = false;
  public static int BUFFER_SIZE = 32768;

  private int current_header_id = -1;
  private int current_id = -1;
  private int current_pos = -1;
  private int trace_cache_size;
  private int trace_buffer_size;

  public CachedSegyReader(String filename, ByteOrder order,
                          SegyDataFormat format) throws
      FileNotFoundException, IOException {
    super(filename, order, format);
    if (ADJUST_HEADER_CACHE) {
      trace_cache_size = adjustHeaderCacheSize();
    }
    else {
      trace_cache_size = (int) Math.floor( ( (double) (BUFFER_SIZE -
          _format.getTraceHeaderSize())) / trace_size) + 1;
    }
    prepareHeaderCache();
    _closed = false;
  }

  private int adjustHeaderCacheSize() throws IOException {

    if (number_of_traces < 400000) {
      return 2;
    }

    int numtraces = 5000;

    int num = Math.min(numtraces, number_of_traces), step = 14;
    long[] results = new long[step];
    for (int retries = 0; retries < 50; ++retries) {
      for (int istep = 0; istep < step; ++istep) {
        trace_cache_size = istep + 2;
        prepareHeaderCache();
        long start = new java.util.Date().getTime();
        for (int i = 0; i < num; ++i) {
          seekTraceHeader(i);
        }
        if (retries == 0 && istep == 0) {
          results[istep] = 0;
        }
        else {
          results[istep] += (new java.util.Date()).getTime() - start;
        }
      }
    }
    long min = Long.MAX_VALUE;
    int min_index = 0;
    for (int istep = 0; istep < step; ++istep) {
      if (results[istep] < min) {
        min = results[istep];
        min_index = istep;
      }
    }
    return min_index + 2;
  }

  private void prepareHeaderCache() {
    if (trace_cache_size == 1) {
      trace_buffer_size = _format.getTraceHeaderSize();
    }
    else {
      trace_buffer_size = trace_size * (trace_cache_size - 1) +
          _format.getTraceHeaderSize();
    }
    header = new byte[trace_buffer_size];
    current_header_id = -trace_cache_size;
    _buffer = ByteBuffer.wrap(header);
  }

  public void seekTraceHeader(int traceID) throws IOException {
    if (current_id != traceID) {
      current_id = traceID;
      if (traceID < current_header_id ||
          traceID >= current_header_id + trace_cache_size) {
        current_header_id = traceID;
        current_pos = 0;
        file.seek(tracePosition(traceID));
        try {
          file.readFully(header);
        }
        catch (EOFException e) {}
      }
      else {
        current_pos = (current_id - current_header_id) * trace_size;
      }
      _buffer.position(current_pos);
    }
  }
}