//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;

import com.gwsys.seismic.indexing.*;

public class SimpleCompoundIndexItem implements Comparable, Readable, Writeable {
	private IndexKey _key;

	private int _trace_id;

	public SimpleCompoundIndexItem(short format) {
		_key = IndexKey.createKey(format);
	}

	public int write(DataOutput s) throws IOException {
		s.writeInt(_trace_id);
		return 4 + _key.write(s);
	}

	public void read(DataInput s) throws IOException {
		_trace_id = s.readInt();
		_key.setValue(s);
	}

	public int compareTo(Object o) {
		return _key.compareTo(((SimpleCompoundIndexItem) o)._key);
	}

	public int getTraceId() {
		return _trace_id;
	}

	public IndexKey getKey() {
		return _key;
	}
}