//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;
import java.nio.*;
import java.nio.channels.*;

import com.gwsys.seismic.indexing.*;
import com.gwsys.seismic.indexing.util.*;
import com.gwsys.seismic.indexing.SeismicConstants;

public abstract class SegyReaderBase implements SegyReader {
	protected SegyDataFormat _format = null;

	protected RandomAccessFile file;

	protected ByteOrder _order;

	protected int number_of_traces, trace_size, number_of_samples,
			sample_units, sample_interval;

	protected short data_code, word_size;

	protected double start_value = 0;

	protected boolean _closed;

	protected byte[] header;

	protected ByteBuffer _buffer;

	protected RestrictedByteBuffer _restricted_buffer;

	class RestrictedByteBufferImpl implements RestrictedByteBuffer {

		public void setPosition(int pos) throws IllegalArgumentException {
			if (pos < 0 || pos >= _format.getTraceHeaderSize()) {
				throw new IllegalArgumentException("Invalid header position: "
						+ pos);
			} else {
				_buffer.position(adjustTraceBufferPosition(pos));
			}
		}

		public void get(byte[] buffer) {
			_buffer.get(buffer);
		}

		public byte get() {
			return _buffer.get();
		}

		public short getShort() {
			return _buffer.getShort();
		}

		public int getInt() {
			return _buffer.getInt();
		}

		public float getFloat() {
			return _buffer.getFloat();
		}

		public double getDouble() {
			return _buffer.getDouble();
		}
	}

	public SegyDataFormat getSegyDataFormat() {
		return _format;
	}

	protected SegyReaderBase(String filename, ByteOrder order,
			SegyDataFormat format) throws FileNotFoundException, IOException {
		_format = format;
		_restricted_buffer = new RestrictedByteBufferImpl();
		_order = order;
		data_code = 1;
		word_size = 4;
		number_of_samples = 0;
		number_of_traces = 0;
		trace_size = 0;
		file = new RandomAccessFile(filename, "r");
		init();
	}

	protected void init() throws IOException {
		FileChannel ch = file.getChannel();
		sample_units = SeismicConstants.UNIT_TIME;
		boolean segy = true;
		MappedByteBuffer mb ;
		if (_format.getEbcdicReelHeaderSize()!=0)
			mb = ch.map(FileChannel.MapMode.READ_ONLY,
				_format.getEbcdicReelHeaderSize(),
				_format.getBinaryReelHeaderSize());
		else{
			mb = ch.map(FileChannel.MapMode.READ_ONLY,
					0,
					_format.getTraceHeaderSize());
			segy = false;
		}
		mb.order(_order);
		if (segy)
			sample_interval = mb.getShort(16);
		else
			sample_interval = mb.getShort(116);
		if (sample_interval < 0) {
			sample_interval += 65536;

		}
		data_code = mb.getShort(24);

		if (data_code == 0) {
			data_code = 1; // default code
		}
		if (data_code > 5)
			data_code = 5; // ieee code
		word_size = (short) (data_code == 3 ? 2 : 4);
		if (segy)
			number_of_samples = mb.getShort(20);
		else
			number_of_samples = mb.getShort(114);

		number_of_samples = number_of_samples > 0 ? number_of_samples
				: number_of_samples + 65536;

		trace_size = word_size * number_of_samples
				+ _format.getTraceHeaderSize();

		long lineHeadersSize = _format.getEbcdicReelHeaderSize()
				+ _format.getBinaryReelHeaderSize();

		number_of_traces = (int) ((file.length() - lineHeadersSize) / trace_size);

		if (number_of_traces > 0) {
			file.seek((long) _format.getEbcdicReelHeaderSize()
					+ (long) _format.getBinaryReelHeaderSize()
					+ _format.getStartValueOffset());
			start_value = file.readInt();
		}
	}

	protected long tracePosition(long n) {
		return n * (long) trace_size + (long) _format.getEbcdicReelHeaderSize()
				+ (long) _format.getBinaryReelHeaderSize();
	}

	public ByteBuffer getSegyHeader() throws IOException {
		FileChannel ch = file.getChannel();
		ByteBuffer buf = ch.map(FileChannel.MapMode.READ_ONLY, 0, _format
				.getEbcdicReelHeaderSize()
				+ _format.getBinaryReelHeaderSize());
		buf.order(_order);
		return buf;
	}

	public void close() throws IOException {
		file.close();
		_closed = true;
	}

	public int getTraceByteLen() {
		return trace_size;
	}

	public int getNumSamplesPerTrace() {
		return number_of_samples;
	}

	public ByteOrder getByteOrder() {
		return _order;
	}

	public void readTraceBulk(byte[] buf, int offset, int start_trace_id,
			int num_traces) throws IOException {
		file.seek(tracePosition(start_trace_id));
		file.readFully(buf, offset, num_traces * trace_size);
	}

	public void readTraceBulk(byte[] buf, int offset, int start_trace_id,
			int num_traces, int start_sample, int num_samples)
			throws IOException {
		int sample_range = num_samples * word_size, skip_bytes = start_sample
				* word_size;

		long pos = tracePosition(start_trace_id);

		for (int i = 0; i < num_traces; i++, pos += trace_size) {
			file.seek(pos);
			file.readFully(buf, offset, _format.getTraceHeaderSize());
			file.skipBytes(skip_bytes);
			offset += _format.getTraceHeaderSize();
			file.readFully(buf, offset, sample_range);
			offset += sample_range;
		}
	}

	public void readTraceSamples(byte[] buf, int buff_offset, int trace_id)
			throws IOException {
		readTraceSamples(buf, buff_offset, trace_id, 0, number_of_samples);
	}

	public void readTraceSamples(byte[] buf, int buff_offset, int trace_id,
			int start_sample, int num_samples) throws IOException {
		long pos = tracePosition(trace_id)
				+ (long) _format.getTraceHeaderSize() + (long) start_sample
				* word_size;
		file.seek(pos);
		file.readFully(buf, buff_offset, num_samples * word_size);
	}

	public RestrictedByteBuffer getTraceHeader() {
		return _restricted_buffer;
	}

	public short getSampleSize() {
		return word_size;
	}

	public double getStartValue() {
		return start_value;
	}

	public int getSampleInterval() {
		return sample_interval;
	}

	public int getSampleUnit() {
		return sample_units;
	}

	public short getSampleType() {
		return data_code;
	}

	public int getNumberOfTraces() {
		return number_of_traces;
	}

	protected abstract int adjustTraceBufferPosition(int pos);

	/**
	 * Reset the byte order and read values again.
	 * @param order ByteOrder
	 * @throws IOException
	 */
	public void setByteOrder(ByteOrder order) throws IOException {
		_order = order;
		init();
	}
}
