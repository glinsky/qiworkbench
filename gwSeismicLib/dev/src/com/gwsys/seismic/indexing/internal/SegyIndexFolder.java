//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;

public abstract class SegyIndexFolder {
	protected int _pageSize = 0;

	protected int _indexOffset = 0;

	protected SegyIndexFile _indexFile = null;

	abstract public byte[] readIndexPage(int index) throws IOException;

	public SegyIndexFolder(SegyIndexFile iFile, int PageSize, int IndexOffset) {
		_indexFile = iFile;
		_pageSize = PageSize;
		_indexOffset = IndexOffset;
	}

	protected void readPage(int index, byte[] data) throws IOException {
		_indexFile.readIndex(_indexOffset + index * _pageSize, data);
	}

	public int getPageSize() {
		return _pageSize;
	}

	public int[] rawReadInt(int offset, int length) throws IOException {
		int[] buffer = new int[length];
		_indexFile.readTraceList(offset, length, buffer);
		return buffer;
	}

	public void rawReadInt(int[] buffer, int offset, int length)
			throws IOException, ArrayIndexOutOfBoundsException {
		if (buffer == null || buffer.length < length) {
			throw new ArrayIndexOutOfBoundsException();
		}
		_indexFile.readTraceList(offset, length, buffer);
	}

	public byte[] rawReadByte(int offset, int length) throws IOException {
		byte[] buffer = new byte[length];
		_indexFile.readIndex(offset, buffer);
		return buffer;
	}
}
