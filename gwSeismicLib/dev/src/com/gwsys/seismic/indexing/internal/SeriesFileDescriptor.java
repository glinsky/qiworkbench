//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;

import com.gwsys.seismic.indexing.util.*;

public class SeriesFileDescriptor {
	private File _file;

	private DataInputStream _is;

	private DataOutputStream _os;

	private FileInputStream _ifs = null;

	private FileOutputStream _ofs = null;

	private void init(File f) throws IOException {
		_file = f;
		_file.deleteOnExit();
		create_streams();
	}

	public SeriesFileDescriptor(File f) throws IOException {
		init(f);
	}

	public SeriesFileDescriptor() throws IOException {
		init(SegyTempFile.createTempFile("sgy", null));
	}

	public SeriesFileDescriptor(String prefix) throws IOException {
		init(SegyTempFile.createTempFile(prefix, null));
	}

	public DataInputStream getDataInputStream() {
		return _is;
	}

	public DataOutputStream getDataOutputStream() {
		return _os;
	}

	public String getFileName() {
		return (_file != null) ? _file.getName() : null;
	}

	protected void finalize() throws Throwable {
		delete();
		_file = null;
	}

	public void flush() throws IOException {
		check();
		_os.flush();
	}

	public int write(Writeable[] items, int numItems) throws IOException {
		check();
		int wrote = 0;
		for (int i = 0; i < numItems; i++) {
			wrote += items[i].write(_os);
		}
		return wrote;
	}

	public long getLength() throws IOException {
		flush();
		return _file.length();
	}

	public int write(Writeable item) throws IOException {
		check();
		return item.write(_os);
	}

	public void read(Readable[] items) throws IOException {
		check();
		for (int i = 0; i < items.length; i++) {
			items[i].read(_is);
		}
	}

	public void read(Readable item) throws IOException {
		check();
		item.read(_is);
	}

	public byte[] readAbsolute(long off, int len) throws IOException {
		check();
		if (off + len > getLength()) {
			throw new IOException();
		}
		byte[] buf = new byte[len];
		FileInputStream f = new FileInputStream(_file);
		f.reset();
		f.skip(off);
		f.read(buf);
		f.close();
		return buf;
	}

	public synchronized boolean delete() throws IOException {

		boolean isDeleted = true;

		if (_file != null) {
			close();

			if (_file.delete()) {
				_file = null;
			} else {
				isDeleted = false;
			}
		}

		_os = null;
		_is = null;

		return isDeleted;
	}

	public synchronized void close() throws IOException {

		if (_file != null) {

			if (_os != null) {
				_os.flush();
				_os.close();
				_os = null;
			}

			if (_is != null) {
				_is.close();
				_is = null;
			}
			if (_ifs != null) {
				_ifs.close();
				_ifs = null;
			}
			if (_ofs != null) {
				_ofs.close();
				_ofs = null;
			}
		}

	}

	public void reset() throws IOException {
		close();
		create_streams();
	}

	private void create_streams() throws IOException {

		_ifs = new FileInputStream(_file);
		_ofs = new FileOutputStream(_file, true);

		_is = new DataInputStream(new BufferedInputStream(_ifs, 65536));
		_os = new DataOutputStream(new BufferedOutputStream(_ofs, 65536));
	}

	private void check() throws IOException {
		if (_file == null || _is == null || _os == null) {
			throw new IOException();
		}
	}
}
