//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;
import java.util.*;

import com.gwsys.seismic.indexing.*;

public class SingleThreadFileDispatchDriver implements
		FileDispatchDriverInterface {

	public static int THRESHOLD = 8;

	private SeriesCollection[] _folders = null;

	private CompoundIndexTree[] _indexTree = null;

	private SeriesFileDescriptor[] _job = null;

	private int _jobSeries = 0;

	public void createFolders(CompoundIndexTree[] tree) throws IOException {

		if (_folders != null) {
			throw new IOException("Folders are already created!");
		}

		_indexTree = tree;

		int len = tree.length;

		_folders = new SeriesCollection[len];

		for (int i = 0; i < len; i++) {
			_folders[i] = new SeriesCollection(tree[i]);
		}
	}

	public void addSeries(CompoundIndexItem[] items, int num_items)
			throws IOException {
		for (int folder = 0; folder < _folders.length; folder++) {

			CompoundIndexItem._driveIndex = folder;

			Arrays.sort(items, 0, num_items);

			SeriesFileDescriptor newFile = new SeriesFileDescriptor();

			if (newFile == null) {
				throw new IOException("No free file slots");
			}

			newFile.write(items, num_items);
			newFile.flush();

			_folders[folder].add(newFile, 0);
			tryJoin(false);
		}
	}

	public void flush() throws IOException {
		for (int i = 0; i < _folders.length; i++) {
			_indexTree[i].getFileDescriptor(_folders[i].doFlush());
		}
		_indexTree = null;
		_folders = null;

	}

	private void tryJoin(boolean force) throws IOException {
		if (gotJoin(force)) {
			doJoin();
		}
	}

	private boolean gotJoin(boolean force) {
		for (int i = 0; i < _folders.length; i++) {
			_job = _folders[i].getSeries(THRESHOLD, force);
			if (_job != null) {
				_jobSeries = i;
				break;
			}
		}
		return _job != null;
	}

	private void doJoin() throws IOException {
		SeriesFileDescriptor f = _folders[_jobSeries].doJoin(_job);
		_folders[_jobSeries].replace(_job, f);
		for (int i = 0; i < _job.length; i++) {
			_job[i].delete();
		}
	}
}

class SeriesCollection {
	private Vector _series;

	private CompoundIndexTree _tree;

	public SeriesCollection(CompoundIndexTree tree) {
		_series = new Vector();
		_tree = tree;
	}

	public SeriesFileDescriptor doFlush() throws IOException {
		int size = 0;
		for (int i = 0; i < _series.size(); i++) {
			Vector v = (Vector) _series.get(i);
			size += v.size();
		}

		SeriesFileDescriptor[] d = new SeriesFileDescriptor[size];
		int current = 0;

		for (int i = 0; i < _series.size(); i++) {
			Vector v = (Vector) _series.get(i);
			for (int j = 0; j < v.size(); j++) {
				d[current++] = (SeriesFileDescriptor) v.get(j);
			}
		}
		SeriesFileDescriptor o = doJoin(d);
		_series.removeAllElements();
		for (int i = 0; i < d.length; i++) {
			d[i].delete();
		}
		return o;
	}

	public SeriesFileDescriptor doJoin(SeriesFileDescriptor[] f)
			throws IOException {
		SeriesFileDescriptor o = new SeriesFileDescriptor();
		int len = f.length;
		SimpleCompoundIndexItem[] items = new SimpleCompoundIndexItem[len];
		boolean[] hasNext = new boolean[len];
		boolean hasMore = false;
		for (int i = 0; i < len; i++) {
			items[i] = new SimpleCompoundIndexItem(_tree.getFormat());
			try {
				f[i].read(items[i]);
			} catch (IOException e) {
				items[i] = null;
			}
		}
		while (true) {
			SimpleCompoundIndexItem min = null;
			int minIndex = 0;
			for (int i = 0; i < len; i++) {
				if (items[i] == null) {
					continue;
				}
				if (min == null || items[i].compareTo(min) < 0) {
					min = items[i];
					minIndex = i;
				}
			}
			if (min == null) {
				break;
			}
			o.write(min);
			try {
				f[minIndex].read(items[minIndex]);
			} catch (IOException e) {
				items[minIndex] = null;
			}
		}
		o.flush();
		return o;
	}

	public void replace(SeriesFileDescriptor[] f, SeriesFileDescriptor t)
			throws IOException {
		int level = 0, i;
		for (int k = 0; k < f.length; k++) {
			SeriesFileDescriptor s = f[k];
			found: for (i = 0; i < _series.size(); i++) {
				Vector v = (Vector) _series.get(i);
				for (int j = 0; j < v.size(); j++) {
					if (s == (SeriesFileDescriptor) v.get(j)) {
						level = java.lang.Math.max(i, level);
						v.remove(j);
						break found;
					}
				}
			}
		}
		add(t, ++level);
	}

	public void add(SeriesFileDescriptor d, int level) throws IOException {
		if (level < 0) {
			throw new IOException("Invalid level");
		}
		Vector v = null;
		if (level < _series.size()) {
			v = (Vector) _series.get(level);
		} else {
			v = new Vector();
			_series.setSize(level + 1);
		}
		v.add(d);
		_series.set(level, v);
	}

	public SeriesFileDescriptor[] getSeries(int threshold, boolean force) {

		SeriesFileDescriptor[] f = getSeriesFlat(threshold);

		if (f != null || !force) {
			return f;
		} else {
			return getSeriesForced(threshold);
		}
	}

	private SeriesFileDescriptor[] getSeriesFlat(int threshold) {
		SeriesFileDescriptor[] f = null;
		for (int i = 0; i < _series.size(); i++) {
			Vector v = (Vector) _series.get(i);
			if (v.size() < threshold) {
				continue;
			}
			f = new SeriesFileDescriptor[v.size()];
			for (int j = 0; j < v.size(); j++) {
				f[j] = (SeriesFileDescriptor) v.get(j);
			}
			break;
		}
		return f;
	}

	private SeriesFileDescriptor[] getSeriesForced(int threshold) {
		int size = 0;
		for (int i = 0; i < _series.size(); i++) {
			Vector v = (Vector) _series.get(i);
			size += v.size();
			if (size >= threshold) {
				break;
			}
		}
		if (size < 2) {
			return null;
		}
		if (size > threshold) {
			size = threshold;
		}
		SeriesFileDescriptor[] f = new SeriesFileDescriptor[size];
		int got = 0;
		gone: {
			for (int i = 0; i < _series.size(); i++) {
				Vector v = (Vector) _series.get(i);
				for (int j = 0; j < v.size(); j++) {
					f[got++] = (SeriesFileDescriptor) v.get(j);
					if (got == size) {
						break gone;
					}
				}
			}
		}
		return f;
	}
}
