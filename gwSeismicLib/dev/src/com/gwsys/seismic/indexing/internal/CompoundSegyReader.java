//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;
import java.nio.*;

import com.gwsys.seismic.indexing.*;
import com.gwsys.seismic.indexing.util.*;

public class CompoundSegyReader
    implements SegyReader {
  private SegyReaderFactory _factory = null;
  private DataSourceFiles _files;
  private SegyReader _reader = null;
  private int _index = -1;
  private int _num_traces = 0;

  public SegyDataFormat getSegyDataFormat() {
    return _reader.getSegyDataFormat();
  }

  public ByteBuffer getSegyHeader() throws IOException {
    return _reader.getSegyHeader();
  }

  public int getSampleUnit() {
    return _reader.getSampleUnit();
  }

  public double getStartValue() {
    return _reader.getStartValue();
  }

  public SegyReader getReaderByTrace(int trace_id) throws IOException {
    set_reader(trace_id);
    return _reader;
  }

  public void readTraceSamples(byte[] buf, int buf_offset, int trace_id) throws
      IOException {
    set_reader(trace_id);
    readTraceSamples(buf, buf_offset, trace_id, 0,
                     _reader.getNumSamplesPerTrace());
  }

  public void readTraceSamples(byte[] buf, int buf_offset, int trace_id,
                               int start_sample, int num_samples) throws
      IOException {
    if (trace_id < 0 || trace_id >= _num_traces) {
      throw new IOException();
    }
    set_reader(trace_id);
    _reader.readTraceSamples(buf, buf_offset,
                             trace_id - current_ds().getStartTrace(),
                             start_sample, num_samples);
  }

  public int getSampleInterval() {
    return _reader.getSampleInterval();
  }

  public CompoundSegyReader(DataSourceFiles files, SegyReaderFactory factory) throws
      IOException {
    _factory = factory;
    _files = files;
    init();
  }

  public ByteOrder getByteOrder() {
    return _reader.getByteOrder();
  }

  public short getSampleSize() {
    return _reader.getSampleSize();
  }

  public short getSampleType() {
    return _reader.getSampleType();
  }

  public CompoundSegyReader(String[] files, ByteOrder[] orders,
                            SegyReaderFactory factory) throws IOException {
    _factory = factory;
    int len = files.length;
    if (len == 0) {
      throw new IOException();
    }
    _files = new DataSourceFiles(files.length);
    for (int i = 0; i < len; i++) {
      DataSourceFile f = new DataSourceFile(files[i], null, orders[i]);
      f.setSegyOrdinal(i);
      _files.addItem(f);
    }
    init();
  }

  public CompoundSegyReader(String[] files, ByteOrder[] orders, String basepath,
                            SegyReaderFactory factory) throws IOException {
    _factory = factory;
    int len = files.length;
    if (len == 0) {
      throw new IOException();
    }
    _files = new DataSourceFiles(files.length);
    for (int i = 0; i < len; i++) {
      DataSourceFile f = new DataSourceFile(files[i], basepath, orders[i]);
      f.setSegyOrdinal(i);
      _files.addItem(f);
    }
    init();
  }

  public CompoundSegyReader(String[] files, String[] orders, String basepath,
                            SegyReaderFactory factory) throws IOException {
    _factory = factory;
    int len = files.length;
    if (len == 0) {
      throw new IOException();
    }
    _files = new DataSourceFiles(files.length);
    for (int i = 0; i < len; i++) {
      DataSourceFile f = new DataSourceFile(files[i], basepath, orders[i]);
      f.setSegyOrdinal(i);
      _files.addItem(f);
    }
    init();
  }

  public CompoundSegyReader(String[] files, String order, String basepath,
                            SegyReaderFactory factory) throws IOException {
    _factory = factory;
    int len = files.length;
    if (len == 0) {
      throw new IOException();
    }
    _files = new DataSourceFiles(files.length);
    for (int i = 0; i < len; i++) {
      DataSourceFile f = new DataSourceFile(files[i], basepath, order);
      f.setSegyOrdinal(i);
      _files.addItem(f);
    }
    init();
  }

  public CompoundSegyReader(String[] files, ByteOrder order, String basepath,
                            SegyReaderFactory factory) throws IOException {
    _factory = factory;
    int len = files.length;
    if (len == 0) {
      throw new IOException();
    }
    _files = new DataSourceFiles(files.length);
    for (int i = 0; i < len; i++) {
      DataSourceFile f = new DataSourceFile(files[i], basepath, order);
      f.setSegyOrdinal(i);
      _files.addItem(f);
    }
    init();
  }

  public DataSourceFiles getDatasourceFiles() {
    return _files;
  }

  private void init() throws IOException {
    _num_traces = 0;
    for (int i = 0; i < _files.getCount(); i++) {
      DataSourceFile f = _files.getItemByIndex(i);
      f.setStartTrace(_num_traces);
      _num_traces += f.getEndTrace() + 1;
      f.setEndTrace(f.getEndTrace() + f.getStartTrace());
    }
    set_reader(0);
  }

  private void set_reader(int trace_id) throws IOException {
    DataSourceFile f = _files.getItemByTrace(trace_id);
    if (f.getSegyOrdinal() != _index) {
      if (_reader != null) {
        _reader.close();
      }
      _index = f.getSegyOrdinal();
      _reader = _factory.createSegyReader(f.getFileFullName(), f.getOrder(), true);
    }
  }

  private DataSourceFile current_ds() {
    return _files.getItemByIndex(_index);
  }

  private boolean check_reader(int trace_id) throws IOException {
    DataSourceFile f = _files.getItemByTrace(trace_id);
    return f != null && f.getSegyOrdinal() == _index;
  }

  public void close() throws IOException {
    _reader.close();
  }

  public int getTraceByteLen() {
    return _reader.getTraceByteLen();
  }

  public int getNumSamplesPerTrace() {
    return _reader.getNumSamplesPerTrace();
  }

  public int getNumberOfTraces() {
    return _num_traces;
  }

  public void readTraceBulk(byte[] buffer, int buffer_offset, int start_trace,
                            int num_traces) throws IOException {
    if (start_trace < 0 || num_traces < 0 ||
        start_trace + num_traces > getNumberOfTraces()) {
      throw new IOException();
    }
    int end_trace = start_trace + num_traces - 1;
    int start = start_trace, end, offset = buffer_offset;
    do {
      set_reader(start);
      DataSourceFile f = current_ds();
      end = Math.min(f.getEndTrace(), end_trace);
      _reader.readTraceBulk(buffer, offset, start - f.getStartTrace(),
                            end - start + 1);
      start = end + 1;
      offset += (end - start + 1) * getTraceByteLen();
    }
    while (!check_reader(end_trace));
  }

  public void readTraceBulk(byte[] buffer, int buffer_offset, int start_trace,
                            int num_traces, int start_sample, int num_samples) throws
      IOException {
    if (start_trace < 0 || num_traces < 0 ||
        start_trace + num_traces > getNumberOfTraces()) {
      throw new IOException();
    }
    int end_trace = start_trace + num_traces - 1;
    int start = start_trace, end, offset = buffer_offset;
    do {
      set_reader(start);
      DataSourceFile f = current_ds();
      end = Math.min(f.getEndTrace(), end_trace);
      _reader.readTraceBulk(buffer, offset, start - f.getStartTrace(),
                            end - start + 1, start_sample, num_samples);
      start = end + 1;
      offset += (end - start + 1) * num_samples * getSampleSize();
    }
    while (!check_reader(end_trace));
  }

  public void seekTraceHeader(int trace_id) throws IOException {
    set_reader(trace_id);
    _reader.seekTraceHeader(trace_id - current_ds().getStartTrace());
  }

  public RestrictedByteBuffer getTraceHeader() {
    return _reader.getTraceHeader();
  }

  /**
	 * Reset the byte order and read values again.
	 * @param order ByteOrder
	 */
	public void setByteOrder(ByteOrder order){
			throw new UnsupportedOperationException("Not imp yet!");
	}
}