//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;
import java.nio.*;

import com.gwsys.seismic.indexing.*;
import com.gwsys.seismic.indexing.util.*;
import com.gwsys.seismic.indexing.SeismicConstants;

public class IndexKey_ui16 extends IndexKey {
	private int _value;

	private static final short _type = SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT;

	public void snapToGrid(IndexKey start, IndexKeyStep step, IndexKey key)
			throws ClassCastException {
		if (!(start instanceof IndexKey_ui16)
				|| !(key instanceof IndexKey_ui16)
				|| !(step instanceof IndexKeyStep_ui16)) {
			throw new ClassCastException(errcast);
		}

		double start_value = ((IndexKey_ui16) start)._value;
		double key_value = ((IndexKey_ui16) key)._value;
		double step_value = ((IndexKeyStep_ui16) step)._value;
		double increment = Math.floor((key_value - start_value) / step_value);
		if (increment == 0) {
			increment = 1;
		}
		_value = (int) (start_value + increment * step_value);
	}

	public boolean isInGrid(IndexKey start, IndexKey end, IndexKeyStep step)
			throws ClassCastException {
		if (start instanceof IndexKey_ui16 && end instanceof IndexKey_ui16
				&& step instanceof IndexKeyStep_ui16) {
			if (_value >= ((IndexKey_ui16) start)._value
					&& _value <= ((IndexKey_ui16) end)._value
					&& Math
							.IEEEremainder(
									((double) (_value - ((IndexKey_ui16) start)._value)),
									((double) ((IndexKeyStep_ui16) step)._value)) == 0) {
				return true;
			} else {
				return false;
			}
		} else {
			throw new ClassCastException(errcast);
		}
	}

	protected void value(int v) {
		_value = v;
	}

	protected int _value() {
		return _value;
	}

	public void setRandom() {
		_value = (int) (Math.floor(Math.random() * 65536.));
	}

	public void round(IndexKeyStep step) throws ClassCastException {
		if (step instanceof IndexKeyStep_ui16) {
			_value = (int) round(_value, ((IndexKeyStep_ui16) step)._value);
		} else {
			throw new ClassCastException(errcast);
		}
	}

	public short getType() {
		return _type;
	}

	public void setValue(IndexKey key) throws ClassCastException {
		if (key instanceof IndexKey_ui16) {
			_value = ((IndexKey_ui16) key)._value;
		} else {
			throw new ClassCastException(errcast);
		}
	}

	public Object getValue() {
		return new Integer(_value);
	}

	public int compareTo(Object ob) throws ClassCastException {
		if (ob instanceof IndexKey_ui16) {
			return _value < ((IndexKey_ui16) ob)._value ? -1
					: _value == ((IndexKey_ui16) ob)._value ? 0 : 1;
		} else {
			throw new ClassCastException(errcast);
		}
	}

	public int compareToBuffer(ByteBuffer b, int offset) {
		int val = _value(b, offset);
		return _value < val ? -1 : _value == val ? 0 : 1;
	}

	public double getDistance(IndexKey k) throws ClassCastException {
		if (k instanceof IndexKey_ui16) {
			return Math.abs((double) _value
					- (double) ((IndexKey_ui16) k)._value);
		} else {
			throw new ClassCastException(errcast);
		}
	}

	public void mul(double v) {
		_value *= v;
	}

	public void setValue(DataInput f) throws IOException {
		_value = f.readShort();
		if (_value < 0) {
			_value += 65536;
		}
	}

	public void setValue(RestrictedByteBuffer b, int offset) {
		b.setPosition(offset);
		_value = b.getShort();
		if (_value < 0) {
			_value += 65536;
		}
	}

	public void setValue(ByteBuffer b, int offset) {
		_value = _value(b, offset);
	}

	private int _value(ByteBuffer b, int offset) {
		b.position(offset);
		int val = b.getShort();
		if (val < 0) {
			val += 65536;
		}
		return val;
	}

	public int getByteLen() {
		return 2;
	}

	public void addStep(IndexKeyStep step) throws ClassCastException {
		if (step instanceof IndexKeyStep_ui16) {
			_value += ((IndexKeyStep_ui16) step)._value;
		} else {
			throw new ClassCastException(errcast);
		}
	}

	public void sub(IndexKey key, IndexKeyStep step) throws ClassCastException {
		if (key instanceof IndexKey_ui16 && step instanceof IndexKeyStep_ui16) {
			((IndexKeyStep_ui16) step)._value = (int) (_value - ((IndexKey_ui16) key)._value);
		} else {
			throw new ClassCastException(errcast);
		}
	}

	public void setValue(String s) {
		_value = Double.valueOf(s).shortValue();
	}

	public int write(DataOutput f) throws IOException {
		f.writeShort(_value);
		return 2;
	}

	public int write(ByteBuffer f) throws IOException {
		f.putShort((short) _value);
		return 2;
	}
}
