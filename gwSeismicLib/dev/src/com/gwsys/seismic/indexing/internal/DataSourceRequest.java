//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;
import java.util.*;

import com.gwsys.seismic.indexing.*;

abstract class DataSourceRequestItem {
  protected DataSourceRequestItem _next = null;
  protected SegyIndex _index = null;
  protected DataSourceKey _key = null;
  protected boolean _gotcha = false;
  protected boolean _above_sorted = false;

  protected DataSourceRequestItem(DataSourceKey key, SegyIndex index) {
    _key = key;
    _index = index;
  }

  public static DataSourceRequestItem getItem(DataSourceKey key,
                                              SegyIndex index) {
    if (key.getSortOrder() == DataSourceKey.SortOrder.UNSORTED) {
      return new UnsortedDataSourceRequestItem(key, index);
    }
    else {
      return new SortedDataSourceRequestItem(key, index);
    }
  }

  public void addRequestItem(DataSourceRequestItem item) {
    if (_next == null) {
      _next = item;
    }
    else {
      _next.addRequestItem(item);
    }
    if (!_above_sorted &&
        item._key.getSortOrder() != DataSourceKey.SortOrder.UNSORTED) {
      _above_sorted = true;
    }
  }

  public abstract int[] getTraceList() throws IOException;

  protected abstract int[] retain(int[] tracelist) throws IOException;
}

class UnsortedDataSourceRequestItem
    extends DataSourceRequestItem {
  private int[] _tracelist;

  public UnsortedDataSourceRequestItem(DataSourceKey key, SegyIndex index) {
    super(key, index);
  }

  public int[] getTraceList() throws IOException {
    if (!_gotcha) {
      readTraceList();
    }
    return _tracelist;
  }

  private void readTraceList() throws IOException {
    if (_key.getIncrement() == null) {
      _tracelist = _index.getTraceList(_key.getMinValue(), _key.getMaxValue());
    }
    else {
      _tracelist = _index.getTraceList(_key.getMinValue(), _key.getMaxValue(),
                                       _key.getIncrement(),
                                       _key.getInterpolationMode());
    }
    if (_tracelist != null && _next != null) {
      _tracelist = _next.retain(_tracelist);
    }
    if (_tracelist != null && !_above_sorted) {
      Arrays.sort(_tracelist);
    }
    _gotcha = true;
  }

  protected int[] retain(int[] tracelist) throws IOException {
    if(tracelist == null)
      return null;
    Arrays.sort(tracelist);
    if (!_gotcha) {
      readTraceList();
    }
    if (_tracelist == null) {
      return null;
    }

    if (_above_sorted) {
      return RestrictedArrays.retainKeepOrder(tracelist, _tracelist);
    }
    else {
      return RestrictedArrays.retain(tracelist, _tracelist);
    }
  }
}

class SortedDataSourceRequestItem
    extends DataSourceRequestItem {
  private Vector _tracelists = null;

  public SortedDataSourceRequestItem(DataSourceKey key, SegyIndex index) {
    super(key, index);
  }

  public int[] getTraceList() throws IOException {
    if (!_gotcha) {
      readTraceList();

    }
    return getArray(_tracelists);
  }

  private int[] getArray(Vector v) {
    if (v == null || v.size() == 0) {
      return null;
    }

    int total = 0;

    for (int i = 0; i < v.size(); i++) {
      total += ( (int[]) v.elementAt(i)).length;
    }

    if (total == 0) {
      return null;
    }

    int[] traces = new int[total];
    int current = 0;

    for (int i = 0; i < v.size(); i++) {
      int[] item = (int[]) v.elementAt(i);
      copy(traces, item, current);
      current += item.length;
    }
    return traces;
  }

  private void copy(int[] dst, int[] src, int offset) {
    int i = 0;
    while (i < src.length) {
      dst[offset++] = src[i++];
    }
  }

  private void readTraceList() throws IOException {
    if (_tracelists == null) {
      _tracelists = new Vector();
    }
    else {
      _tracelists.clear();

    }
    IndexKey kFrom = (IndexKey) _index.getKeyMin().clone(),
        kTo = (IndexKey) kFrom.clone();
    IndexKeyStep step = (IndexKeyStep) _index.getKeyStep().clone();
    step.setValue(_key.getIncrement());
    kFrom.setValue(_key.getMinValue());
    kTo.setValue(_key.getMaxValue());

    Collection items = _index.getIndexCollection(kFrom, kTo, step,
                                                 _key.getInterpolationMode());
    if(items != null) {
      Iterator iter = items.iterator();

      while (iter.hasNext()) {
        SegyIndexItem item = (SegyIndexItem) iter.next();
        int[] traces = _index.getTraceList(item);
        if (_next != null && traces != null) {
          traces = _next.retain(traces);
        }
        if (traces != null) {
          _tracelists.addElement(traces);
        }
      }
    }
    _gotcha = true;
  }

  protected int[] retain(int[] tracelist) throws IOException {
    if(tracelist == null) return null;
    if (!_gotcha) {
      readTraceList();
    }
    Vector v = new Vector();
    Arrays.sort(tracelist);
    for (int i = 0; i < _tracelists.size(); i++) {
      int[] item = (int[]) _tracelists.elementAt(i);
      int[] retained;
      if (_above_sorted) {
        retained = RestrictedArrays.retainKeepOrder(tracelist, item);
      }
      else {
        retained = RestrictedArrays.retain(tracelist, item);
      }
      v.addElement(retained);
    }
    return getArray(v);
  }
}

public class DataSourceRequest {
  private DataSourceRequestItem _request;

  public DataSourceRequest(DataSourceKeys keys, Map indexes) throws
      IllegalArgumentException {
    for (int i = 0; i < keys.getCount(); i++) {
      DataSourceKey key = keys.getItemByIndex(i);
      SegyIndex index = (SegyIndex) indexes.get(key.getName());
      if (index == null) {
        throw new IllegalArgumentException("Index not found: " + key.getName());
      }
      DataSourceRequestItem item = DataSourceRequestItem.getItem(key, index);
      if (i == 0) {
        _request = item;
      }
      else {
        _request.addRequestItem(item);
      }
    }
  }

  public int[] getTraceList() throws IOException {
    return _request.getTraceList();
  }
}
