//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.seismic.indexing.internal;

class PageList {
	private PageListItem _head = null;

	private PageListItem _tail = null;

	public PageListItem pushBack() {
		PageListItem item = _tail;
		if (item == null) {
			return null;
		}

		_tail = item._prev;
		if (_tail != null) {
			item._prev._next = null;
			item._prev = null;
		}
		return item;
	}

	public void reinsertItem(PageListItem item) {
		if (item._next != null) {
			item._next._prev = item._prev;
		}
		if (item._prev != null) {
			item._prev._next = item._next;
			item._prev = null;
		}
		if (_head != null) {
			_head._prev = item;
		}
		item._next = _head;
		_head = item;
		if (_tail == null) {
			_tail = item;
		}
	}
}
