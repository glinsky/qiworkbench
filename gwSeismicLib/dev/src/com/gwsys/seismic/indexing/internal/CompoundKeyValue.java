//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;
import java.nio.*;
import java.util.*;

import com.gwsys.seismic.indexing.*;

public class CompoundKeyValue {

  private int _trace_offset = 0;
  private int _trace_num = 0;
  private int _num_keys = 0;

  private int[] _tracearray = null;
  private IndexKey _key;

  public CompoundKeyValue(IndexKey keyProducer) {
    _key = (IndexKey) keyProducer.clone();
  }

  public void traceOffset(int off) {
    _trace_offset = off;
  }

  public int traceOffset() {
    return _trace_offset;
  }

  public void numKeys(int n) {
    _num_keys = n;
  }

  private int writeKey(DataOutput op, int tracefolder_offset) throws
      IOException {
    int wrote = _key.write(op) + 8;
    op.writeInt(_trace_offset + tracefolder_offset);
    op.writeInt(_tracearray.length);
    return wrote;
  }

  public void readKey(DataInput ip) throws IOException {
    _key.setValue(ip);
    _trace_offset = ip.readInt();
    _trace_num = ip.readInt();
    _tracearray = new int[_trace_num];
  }

  public void readTracelist(ByteBuffer ip) throws IOException {
    for (int i = 0; i < _tracearray.length; i++) {
      _tracearray[i] = ip.getInt();
    }
  }

  private IndexKeySet[] refs = null;

  public void prepareRefArray(CompoundIndexTree[] trees, int index) {
    int num_trees = trees.length - 1;
    refs = new IndexKeySet[num_trees];
    Arrays.sort(_tracearray);
    for (int i = 0; i < num_trees; i++) {
      refs[i] = IndexKeySet.createKeySet(_tracearray.length,
                                         trees[i < index ? i : i + 1].getFormat());
    }
  }

  public void loadRefKeys(CompoundIndexTree[] trees, int tree_index,
                          int ref_index, ByteBuffer _ref_buffer) throws
      IOException {
    IndexKey key = trees[tree_index].getKeyProducer();
    for (int j = 0; j < _tracearray.length; j++) {
      refs[ref_index].addItem(trees[tree_index].getRefKey(_tracearray[j], key,
          _ref_buffer));
    }
  }

  private void writeRefs(DataOutput d, CompoundIndexTree[] tree, int index) throws
      IOException {
    for (int i = 0; i < refs.length; i++) {
      IndexKeySetIterator iter = refs[i].iterator();
      IndexKey key = tree[i < index ? i : i + 1].getKeyProducer();
          while (iter.hasNext()) {
        iter.next(key);
        key.write(d);
      }
    }
    refs = null;
  }

  public int writePageNode(RandomAccessFile index_file, DataOutput ref,
                           int node_offset, int ref_offset,
                           int tracefolder_offset, CompoundIndexTree[] trees,
                           int index) throws IOException {
    index_file.seek(node_offset);
    writeKey(index_file, tracefolder_offset);
    int offset = ref_offset;
    for (int i = 0; i < refs.length; i++) {
      int blen = refs[i].getSize() * trees[i < index ? i : i + 1].getKeySize();
      index_file.writeInt(offset);
      index_file.writeInt(blen);
      offset += blen;
    }
    writeRefs(ref, trees, index);
    return offset - ref_offset;
  }
}
