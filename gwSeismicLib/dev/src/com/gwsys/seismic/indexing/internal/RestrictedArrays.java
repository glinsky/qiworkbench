//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

/**
 * Array Search Utility
 * @author 
 *
 */
public class RestrictedArrays {
	public static int binarySearch(int[] array, int value) {
		if (array == null) {
			return -1;
		}
		int max = array.length - 1;
		if (max == -1) {
			return -1;
		}
		return binarySearch(array, value, 0, max);
	}

	public static int binarySearch(int[] array, int value, int left, int right) {
		int mid;
		if (array == null) {
			return -1;
		}
		int max = array.length - 1;

		if (max == -1 || left < 0 || right > max || left > right
				|| array[left] > value || array[right] < value) {
			return -1;
		}

		while (left <= right) {
			mid = (left + right) / 2;
			if (value < array[mid]) {
				right = mid - 1;
			} else if (value > array[mid]) {
				left = mid + 1;
			} else {
				return mid;
			}
		}
		return -1;
	}

	// a[] should be sorted
	public static int[] retainKeepOrder(int[] a, int[] b) {
		int count = 0;
		for (int i = 0; i < b.length; i++) {
			if (binarySearch(a, b[i]) >= 0) {
				count++;
			}
		}
		if (count == 0) {
			return null;
		}
		int[] c = new int[count];
		count = 0;
		for (int i = 0; i < b.length; i++) {
			int bi = b[i];
			if (binarySearch(a, bi) >= 0) {
				c[count++] = bi;
			}
		}
		return c;
	}

	// both a[] and b[] should be sorted, running faster than retainKeepOrder()
	public static int[] retain(int[] a, int[] b) {
		int count = 0;
		int[] min, max;
		if (a.length < b.length) {
			min = a;
			max = b;
		} else {
			min = b;
			max = a;
		}

		int current = 0, last = 0, num = max.length - 1;
		for (int i = 0; i < min.length; i++) {
			current = binarySearch(max, min[i], last, num);
			if (current >= 0) {
				count++;
				last = current + 1;
				if (last > num) {
					break;
				}
			}
		}

		if (count == 0) {
			return null;
		}
		int[] c = new int[count];

		count = current = last = 0;
		for (int i = 0; i < min.length; i++) {
			int j = min[i];
			current = binarySearch(max, j, last, num);
			if (current >= 0) {
				c[count++] = j;
				last = current + 1;
				if (last > num) {
					break;
				}
			}
		}
		return c;
	}
}
