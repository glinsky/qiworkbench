//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;
import java.nio.*;
import javax.xml.parsers.*;

import org.w3c.dom.*;

import com.gwsys.seismic.indexing.*;
import com.gwsys.seismic.indexing.util.*;
import com.gwsys.seismic.indexing.SeismicConstants;


/**
 * Data source profile is definition of the Indexed Segy format
 * and have information as a rule keeping in XGY file
 * <p>Copyright (c) 2003</p>
 * <p>INT Inc.</p>
 * @version 1.0
 */
public class DataSourceProfile {
  /**
   * Name of dummy index corresponding to trace ordinal number
   */
  public static final String TRACE_ID_KEY = "TraceNumber";

  /**
   * Default profile name
   */
  public static final String DEFAULT_PROFILE_NAME = "DefaultProfile";

  /**
   * Default name of transposed index
   */
  public static final String DEFAULT_TRANSPOSED_NAME = "Time";

  private int _sampleInterval = 0;
  private int _dataOrder = 0;
  private int _dataType = 0;
  private int _numberOfTraces = 0;
  private int _sampleUnits = 0;
  private int _format = 0;
  private String _basePath = null;
  private String _transposedName = DEFAULT_TRANSPOSED_NAME;
  private String _transposedKey = "";
  private String _profileName = null;
  private String _segyFormatFile = null;
  private String _indexFile = null;
  private DataSourceKeys _datasourceKeys = null;
  private DataSourceFiles _datasourceFiles = null;

  /**
   * Gets collection of datasource files of this profile
   * @return collection of datasource files of this profile
   */
  public DataSourceFiles getDatasourceFiles() {
    return _datasourceFiles;
  }

  /**
   * Sets collection of datasource files of this profile
   * @param datasourceFiles collection of datasource files of this profile
   */
  public void setDatasourceFiles(DataSourceFiles datasourceFiles) {
    _datasourceFiles = datasourceFiles;
  }

  /**
   * Gets collection of datasource keys of this profile
   * @return collection of datasource keys of this profile
   */
  public DataSourceKeys getDatasourceKeys() {
    return _datasourceKeys;
  }

  /**
   * Sets collection of datasource keys of this profile
   * @param datasourceKeys collection of datasource keys of this profile
   */
  public void setDatasourceKeys(DataSourceKeys datasourceKeys) {
    _datasourceKeys = datasourceKeys;
  }

  /**
   * Gets format of samples
   * @return format of samples
   */
  public int getFormat() {
    return _format;
  }

  /**
   * Sets format of samples
   * @param format format of samples
   */
  public void setFormat(int format) {
    _format = format;
  }

  /**
   * Gets sample units
   * @return sample units
   */
  public int getSampleUnits() {
    return _sampleUnits;
  }

  /**
   * Sets sample units
   * @param sampleUnits sample units
   */
  public void setSampleUnits(int sampleUnits) {
    _sampleUnits = sampleUnits;
  }

  /**
   * Gets total number of traces
   * @return total number of traces
   */
  public int getNumberOfTraces() {
    return _numberOfTraces;
  }

  /**
   * Sets total number of traces
   * @param numberOfTraces total number of traces
   */
  public void setNumberOfTraces(int numberOfTraces) {
    _numberOfTraces = numberOfTraces;
  }

  /**
   * Gets type of SEGY data
   * @return type of SEGY data
   */
  public int getDataType() {
    return _dataType;
  }

  /**
   * Sets type of SEGY data
   * @param dataType type of SEGY data
   */
  public void setDataType(int dataType) {
    _dataType = dataType;
  }

  /**
   * Gets data order
   * @return data order
   */
  public int getDataOrder() {
    return _dataOrder;
  }

  /**
   * Sets data order
   * @param dataOrder data order
   */
  public void setDataOrder(int dataOrder) {
    _dataOrder = dataOrder;
  }

  /**
   * Gets sample interval
   * @return sample interval
   */
  public int getSampleInterval() {
    return _sampleInterval;
  }

  /**
   * Sets sample interval
   * @param sampleInterval sample interval
   */
  public void setSampleInterval(int sampleInterval) {
    _sampleInterval = sampleInterval;
  }

  /**
   * Gets profile name
   * @return profile name
   */
  public String getProfileName() {
    return _profileName;
  }

  /**
   * Sets profile name
   * @param profileName profile name
   */
  public void setProfileName(String profileName) {
    _profileName = profileName;
  }

  /**
   * Gets base path of profile SEGY files
   * @return base path of profile SEGY files
   */
  public String getBasePath() {
    return _basePath;
  }

  /**
   * Sets base path of profile SEGY files
   * @param basePath base path of profile SEGY files
   */
  public void setBasePath(String basePath) {
    _basePath = basePath;
  }

  /**
   * Gets fully qualified name of index file
   * @return fully qualified name of index file
   */
  public String getIndexFile() {
    return _indexFile;
  }

  /**
   * Sets fully qualified name of index file
   * @param indexFile fully qualified name of index file
   */
  public void setIndexFile(String indexFile) {
    _indexFile = indexFile;
  }

  /**
   * Gets fully qualified name of SEGY format description file
   * @return fully qualified name of SEGY format description file
   */
  public String getSegyFormatFile() {
    return _segyFormatFile;
  }

  /**
   * Sets fully qualified name of SEGY format description file
   * @param segyFormatFile fully qualified name of SEGY format description file
   */
  public void setSegyFormatFile(String segyFormatFile) {
    _segyFormatFile = segyFormatFile;
  }

  /**
   * Gets name of transposed key
   * @return name of transposed key
   */
  public String getTransposedKey() {
    return _transposedKey;
  }

  /**
   * Sets name of transposed key
   * @param transposedKey name of transposed key
   */
  public void setTransposedKey(String transposedKey) {
    _transposedKey = transposedKey;
  }

  /**
   * Gets name of transposed key
   * @return name of transposed key
   */
  public String getTransposedName() {
    return _transposedName;
  }

  /**
   * Sets name of transposed key
   * @param transposedName name of transposed key
   */
  public void setTransposedName(String transposedName) {
    _transposedName = transposedName;
  }

  /**
   * Creates an empty datasource profile
   */
  public DataSourceProfile() {
    _datasourceFiles = new DataSourceFiles();
    _datasourceKeys = new DataSourceKeys();
  }

  /**
   * Creates new instance of datasource profile from supplied data
   * @param profileName profile name
   * @param dataOrder data order
   * @param dataType SEGY data type
   * @param basePath base path of SEGY files
   * @param segy array of relative SEGY file names
   * @param segyDataOrder data order
   * @return new instance of datasource profile
   * @throws IOException
   */
  public static DataSourceProfile createProfile(String profileName,
                                                String dataOrder,
                                                String dataType,
                                                String basePath, String[] segy,
                                                String[] segyDataOrder) throws
      IOException {
    if (segy == null || segyDataOrder == null || segy.length == 0 ||
        segy.length != segyDataOrder.length) {
      return null;
    }

    DataSourceProfile profile = new DataSourceProfile(segy.length, 1);
    profile._profileName = profileName;
    profile._datasourceFiles.createFiles(segy, segyDataOrder, basePath);
    DataSourceKey k = new DataSourceKey();
    k.setName(new String(TRACE_ID_KEY));
    k.setMinValue("1");

    int num_traces = 0;

    for (int i = 0; i < profile._datasourceFiles.getCount(); i++) {
      DataSourceFile f = profile._datasourceFiles.getItemByIndex(i);
      num_traces += f.getEndTrace() - f.getStartTrace() + 1;
    }

    k.setMaxValue(Integer.toString(num_traces));
    k.setIncrement("1");
    k.setType( (short) SeismicConstants.DATA_FORMAT_INT);

    SegyReaderFactory factory = SegyReaderFactory.getDefaultFactory();

    profile._datasourceKeys.addItem(k);
    profile._numberOfTraces = num_traces;
    profile._basePath = basePath;
    profile._dataOrder = SegyConstantsParser.getDataOrder(dataOrder).intValue();
    profile._dataType = SegyConstantsParser.getDataType(dataType).intValue();
    String segyFile = (basePath != null && basePath.length() > 0) ?
        basePath + File.separator + segy[0] : segy[0];
    SegyReader r = factory.createSegyReader(segyFile,
                                            SegyConstantsParser.
                                            getDataFormat(segyDataOrder[0]), true);
    profile._format = r.getSampleType();
    profile._sampleInterval = r.getSampleInterval();
    profile._sampleUnits = r.getSampleUnit();

    return profile;
  }

  /**
   * Creates new instance of datasource profile
   * @param numfiles number of SEGY files
   * @param numkeys number of indexes
   */
  public DataSourceProfile(int numfiles, int numkeys) {
    _datasourceFiles = new DataSourceFiles(numfiles);
    _datasourceKeys = new DataSourceKeys(numkeys);
  }

  /**
   * Load profile from file
   * @param xmlUri the file name
   * @return the new profile
   */
  public static DataSourceProfile loadProfile(String xmlUri) {
    try {
      return loadProfile(new FileInputStream(xmlUri));
    }
    catch (java.io.FileNotFoundException e) {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * Load profile from input stream
   * @param stream the input steram
   * @return profile
   */
  public static DataSourceProfile loadProfile(InputStream stream) {

    Document doc = loadProfileDescriptor(stream);

    if (doc == null) {
      return null;
    }

    NodeList filenodes = doc.getElementsByTagName("FILE");
    NodeList keynodes = doc.getElementsByTagName("KEY");

    if (filenodes == null || filenodes.getLength() == 0 || keynodes == null ||
        keynodes.getLength() == 0) {
      return null;
    }

    DataSourceProfile profile = new DataSourceProfile(filenodes.getLength(),
        keynodes.getLength());

    String[] profilenodes = {
        "BASEPATH", "FORMAT", "UNITS", "INDEX_FILE", "DATA_TYPE",
        "DATA_ORDER", "SAMPLE_INTERVAL"};

    Node root = doc.getDocumentElement();

    String[] profile_vals = getChildNodeValues(root, profilenodes);

    if (!isValidArray(profile_vals)) {
      return null;
    }

    String[] nocheckProfilenodes = {
        "SEGY_FORMAT_FILE", "DATA_FORMAT"};
    String[] nocheck_vals = getChildNodeValues(root, nocheckProfilenodes);

    profile._basePath = profile_vals[0];
    profile._sampleInterval = Integer.parseInt(profile_vals[6]);

    profile._segyFormatFile = nocheck_vals[0];
    profile._indexFile = profile_vals[3];

    Integer in;
    ByteOrder bo;

    in = SegyConstantsParser.getFormat(profile_vals[1]);
    if (in == null) {
      return null;
    }
    profile._format = in.intValue();

    in = SegyConstantsParser.getUnits(profile_vals[2]);

    if (in == null) {
      return null;
    }

    profile._sampleUnits = in.intValue();

    in = SegyConstantsParser.getDataType(profile_vals[4]);

    if (in == null) {
      return null;
    }

    profile._dataType = in.intValue();

    in = SegyConstantsParser.getDataOrder(profile_vals[5]);

    if (in == null) {
      return null;
    }

    profile._dataOrder = in.intValue();

    //=============================================
    // Gets transfosed keys
    String[] nocheckTransposed_nodes = {
        "TRANSPOSED_KEY", "TRANSPOSED_NAME"};
    String[] nocheckTransposed_vals = getChildNodeValues(root,
        nocheckTransposed_nodes);

    if (nocheckTransposed_vals != null && nocheckTransposed_vals.length > 0 &&
        nocheckTransposed_vals[0] != null) {
      profile._transposedKey = nocheckTransposed_vals[0];
    }
    if (nocheckTransposed_vals != null && nocheckTransposed_vals.length > 1 &&
        nocheckTransposed_vals[1] != null) {
      profile._transposedName = nocheckTransposed_vals[1];
    }

    String[] nameattr = {
        "name"};

    String[] name = getNodeAttributes(root, nameattr);

    if (!isValidArray(name)) {
      return null;
    }
    profile._profileName = name[0];

    String[] filenodenames = {
        "START_TRACE", "END_TRACE", "DATA_FORMAT"};
    for (int i = 0; i < filenodes.getLength(); i++) {
      Node filenode = filenodes.item(i);
      String[] attrs = getNodeAttributes(filenode, nameattr);
      if (!isValidArray(attrs)) {
        return null;
      }
      String[] vals = getChildNodeValues(filenode, filenodenames);
      if (!isValidArray(vals)) {
        return null;
      }
      DataSourceFile file = new DataSourceFile();
      file.setFileName(attrs[0]);

      file.setFileFullName( (profile._basePath != null &&
                             profile._basePath.length() > 0) ?
                           profile._basePath + File.separator +
                           file.getFileName() : file.getFileName());
      file.setStartTrace(Integer.parseInt(vals[0]));
      file.setEndTrace(Integer.parseInt(vals[1]));
      file.setSegyOrdinal(i);
      file.setOrder(SegyConstantsParser.getDataFormat(vals[2]));

      profile._datasourceFiles.addItem(file);
      profile._numberOfTraces += file.getEndTrace() - file.getStartTrace() + 1;
    }

    String[] keynodenames = {
        "MIN_VALUE", "MAX_VALUE", "INCREMENT", "TYPE"};
    for (int i = 0; i < keynodes.getLength(); i++) {
      Node keynode = keynodes.item(i);
      String[] attrs = getNodeAttributes(keynode, nameattr);
      if (!isValidArray(attrs)) {
        return null;
      }
      String[] vals = getChildNodeValues(keynode, keynodenames);

      if (!isValidArray(vals)) {
        return null;
      }
      DataSourceKey key = new DataSourceKey();
      key.setName(attrs[0]);
      key.setMinValue(vals[0]);
      key.setMaxValue(vals[1]);
      key.setIncrement(vals[2]);
      key.setType(SegyConstantsParser.getFormat(vals[3]).shortValue());
      profile._datasourceKeys.addItem(key);
    }
    return profile;
  }

  private static boolean isValidArray(String[] ar) {
    if (ar == null || ar.length == 0) {
      return false;
    }
    for (int i = 0; i < ar.length; i++) {
      if (ar[i] == null) {
        return false;
      }
    }
    return true;
  }

  /**
   * Gets profile as XML document
   * @return the document
   * @throws ParserConfigurationException
   */
  public Document getAsXMLDocument() throws ParserConfigurationException {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document doc = builder.newDocument();
    Element root = doc.createElement("DATASOURCE");
    root.setAttribute("name", _profileName);

    appendTextNode(root, "DATA_TYPE", SegyConstantsParser.getDataType(_dataType),
                   doc);
    appendTextNode(root, "DATA_ORDER",
                   SegyConstantsParser.getDataOrder(_dataOrder), doc);
    appendTextNode(root, "BASEPATH", _basePath, doc);
    appendTextNode(root, "FORMAT", SegyConstantsParser.getFormat(_format), doc);
    appendTextNode(root, "UNITS", SegyConstantsParser.getUnits(_sampleUnits),
                   doc);
    appendTextNode(root, "SAMPLE_INTERVAL", Integer.toString(_sampleInterval),
                   doc);
    appendTextNode(root, "INDEX_FILE", _indexFile, doc);
    if (_segyFormatFile != null) {
      appendTextNode(root, "SEGY_FORMAT_FILE", _segyFormatFile, doc);
    }
    if (_transposedKey != null && _transposedKey != "") {
      appendTextNode(root, "TRANSPOSED_KEY", _transposedKey, doc);
      if (_transposedName != null) {
        appendTextNode(root, "TRANSPOSED_NAME", _transposedName, doc);
      }
    }
    Element files = doc.createElement("FILES");
    for (int i = 0; i < _datasourceFiles.getCount(); i++) {
      DataSourceFile f = _datasourceFiles.getItemByIndex(i);
      Element file = doc.createElement("FILE");
      file.setAttribute("name", f.getFileName());
      appendTextNode(file, "START_TRACE", Integer.toString(f.getStartTrace()),
                     doc);
      appendTextNode(file, "END_TRACE", Integer.toString(f.getEndTrace()), doc);
      appendTextNode(file, "DATA_FORMAT",
                     SegyConstantsParser.getDataFormat(f.getOrder()), doc);
      files.appendChild(file);
    }
    root.appendChild(files);

    Element keys = doc.createElement("KEYS");
    for (int i = 0; i < _datasourceKeys.getCount(); i++) {
      Element key = doc.createElement("KEY");
      DataSourceKey k = _datasourceKeys.getItemByIndex(i);
      key.setAttribute("name", k.getName());
      appendTextNode(key, "MIN_VALUE", k.getMinValue(), doc);
      appendTextNode(key, "MAX_VALUE", k.getMaxValue(), doc);
      appendTextNode(key, "INCREMENT", k.getIncrement(), doc);
      appendTextNode(key, "TYPE", SegyConstantsParser.getFormat(k.getType()),
                     doc);
      keys.appendChild(key);
    }
    root.appendChild(keys);

    doc.appendChild(root);
    doc.normalize();
    return doc;
  }

  private void appendTextNode(Element dst, String node_name, String node_value,
                              Document node_factory) {
    Element new_elem = node_factory.createElement(node_name);
    Text text = node_factory.createTextNode(node_value);
    new_elem.appendChild(text);
    dst.appendChild(new_elem);
  }

  private static String[] getNodeAttributes(Node node, String[] attrNames) {
    if (!node.hasAttributes() || attrNames == null || attrNames.length == 0) {
      return null;
    }
    String[] attrValues = new String[attrNames.length];
    NamedNodeMap map = node.getAttributes();
    for (int i = 0; i < attrNames.length; i++) {
      Node attr = map.getNamedItem(attrNames[i]);
      if (attr != null) {
        attrValues[i] = attr.getNodeValue();
      }
    }
    return attrValues;
  }

  private static String[] getChildNodeValues(Node node, String[] nodeNames) {
    if (!node.hasChildNodes() || nodeNames == null || nodeNames.length == 0) {
      return null;
    }
    String[] nodeValues = new String[nodeNames.length];
    NodeList list = node.getChildNodes();
    for (int i = 0; i < nodeNames.length; i++) {
      nodeValues[i] = findNodeValue(list, nodeNames[i]);
    }
    return nodeValues;
  }

  private static String findNodeValue(NodeList list, String nodeName) {
    if (list == null || list.getLength() == 0 || nodeName == null) {
      return null;
    }
    int len = list.getLength();
    for (int i = 0; i < len; i++) {

      Node node = list.item(i);

      if (node != null && node.getNodeName().equalsIgnoreCase(nodeName)) {
        return getNodeValue(node);
      }

    }
    return null;
  }

  private static String getNodeValue(Node node) {
    Node child = node.getFirstChild();
    return child.getNodeValue();
  }

  private static Document loadProfileDescriptor(InputStream stream) {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    Document doc = null;
    try {
      DocumentBuilder builder = factory.newDocumentBuilder();
      doc = builder.parse(stream);
    }
    catch (Exception e) {
      e.printStackTrace();
      doc = null;
    }
    return doc;
  }
}
