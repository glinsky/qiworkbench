//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.util.*;

import com.gwsys.seismic.indexing.*;

public class SegyIndexFile {
	private BufferedRandomAccessFile _indexFile;

	private FileChannel _indexChannel;

	private int _pageSize = 0;

	private TreeMap _indexes = null;

	private byte _sampleFormat;

	private ByteOrder _endian;

	private float _sample_min, _sample_max, _sample_mean, _sample_rms;

	public static final int MAGIC_NUMBER = 653829714;

	public static short MAJOR_VERSION = 1;

	public static short MINOR_VERSION = 1;

	public float getSampleMin() {
		return _sample_min;
	}

	public float getSampleMax() {
		return _sample_max;
	}

	public float getSampleMean() {
		return _sample_mean;
	}

	public float getSampleRms() {
		return _sample_rms;
	}

	public SegyIndexFile(String indexFileName) throws FileNotFoundException,
			IOException {
		_indexFile = new BufferedRandomAccessFile(indexFileName, "r");
		_indexChannel = _indexFile.getChannel();
		if (!checkMagic() || !checkVersion() || !readIndexInfo()) {
			throw new IOException("Invalid index file");
		}
	}

	public SegyIndex getIndex(String Name, double cacheRate, int cacheTreshold)
			throws IOException {
		SegyIndexHolder holder = (SegyIndexHolder) _indexes.get(Name);
		if (holder == null)
			return null;

		SegyIndexFolder folder;
		if (cacheRate == 0) {
			folder = new SimpleSegyIndexFolder(this, _pageSize,
					holder.indexOffset);
		} else {
			int cacheSize = (int) Math.min(Math.max(
					holder.numPages * cacheRate, cacheTreshold),
					holder.numPages);
			folder = new CachedSegyIndexFolder(this, _pageSize,
					holder.indexOffset, cacheSize);
		}
		return new SegyIndex(holder, folder);
	}

	private boolean checkMagic() throws IOException {
		_indexFile.seek(0);
		return (_indexFile.readInt() == MAGIC_NUMBER) ? true : false;
	}

	private boolean checkVersion() throws IOException {
		_indexFile.seek(4);
		short major = _indexFile.readShort();
		short minor = _indexFile.readShort();
		return (major == MAJOR_VERSION && minor == MINOR_VERSION) ? true
				: false;
	}

	private boolean readIndexInfo() throws IOException {
		_indexFile.seek(8);
		_pageSize = _indexFile.readInt();
		_sampleFormat = _indexFile.readByte();
		_endian = _indexFile.readByte() == 0 ? ByteOrder.BIG_ENDIAN
				: ByteOrder.LITTLE_ENDIAN;
		if (_sampleFormat == 3) {
			_sample_min = _indexFile.readShort();
			_sample_max = _indexFile.readShort();
			_sample_mean = _indexFile.readShort();
			_sample_rms = _indexFile.readShort();
		} else {
			_sample_min = _indexFile.readFloat();
			_sample_max = _indexFile.readFloat();
			_sample_mean = _indexFile.readFloat();
			_sample_rms = _indexFile.readFloat();
		}
		short numIndexes = _indexFile.readShort();
		int[] ioffsets = new int[numIndexes];
		for (int i = 0; i < numIndexes; i++) {
			ioffsets[i] = _indexFile.readInt();

		}
		_indexes = new TreeMap();
		byte iname[] = new byte[32];
		byte val[] = new byte[8];
		String sname;
		short dtype, hoffset;
		int ioffset, npages, num_values;

		for (int i = 0; i < numIndexes; i++) {
			_indexFile.seek(ioffsets[i]);
			IndexKey min, max;
			IndexKeyStep step;
			_indexFile.readFully(iname);
			byte j = 0;
			while (j < 31 && iname[j] != 0) {
				j++;
			}
			StringBuffer b = new StringBuffer(j);
			for (int k = 0; k < j; k++) {
				b.insert(k, (char) iname[k]);
			}
			sname = b.toString();
			hoffset = _indexFile.readShort();
			dtype = _indexFile.readShort();
			num_values = _indexFile.readInt();
			min = IndexKey.createKey(dtype);
			max = IndexKey.createKey(dtype);
			step = IndexKeyStep.createKeyStep(dtype);
			min.setValue(_indexFile);
			max.setValue(_indexFile);
			step.setValue(_indexFile);
			ioffset = _indexFile.readInt();
			npages = _indexFile.readInt();
			_indexes.put(sname, new SegyIndexHolder(sname, i, dtype, npages,
					num_values, ioffset, hoffset, min, max, step, numIndexes));
		}
		return true;
	}

	protected void readTraceList(int offset, int length, int[] buf)
			throws IOException, ArrayIndexOutOfBoundsException {
		if (buf == null || buf.length < length) {
			throw new ArrayIndexOutOfBoundsException();
		}

		_indexFile.seek(offset);
		_indexFile.readIntBuf(buf, 0, length);
	}

	protected void readIndex(int offset, byte[] buf) throws IOException {
		_indexFile.seek(offset);
		_indexFile.readFully(buf);
	}
}

class BufferedRandomAccessFile extends RandomAccessFile {
	public static int BUFFER_SIZE = 65536;

	private byte[] _buffer = null;

	private IntBuffer _int_buffer = null;

	public BufferedRandomAccessFile(String name, String mode)
			throws FileNotFoundException {
		this(name, mode, BUFFER_SIZE);
	}

	public BufferedRandomAccessFile(String name, String mode, int buffer_size)
			throws FileNotFoundException {
		super(name, mode);
		_buffer = new byte[buffer_size];
		_int_buffer = ByteBuffer.wrap(_buffer).asIntBuffer();
	}

	public void readIntBuf(int[] buf, int offset, int length)
			throws IOException {
		int total_bytes = length * 4;
		int remaining = total_bytes, bytes_to_read = 0;
		while (remaining > 0) {
			bytes_to_read = Math.min(_buffer.length, remaining);
			readFully(_buffer, 0, bytes_to_read);
			_int_buffer.position(0);
			_int_buffer.get(buf, offset, bytes_to_read / 4);
			offset += bytes_to_read / 4;
			remaining -= bytes_to_read;
		}
	}
}
