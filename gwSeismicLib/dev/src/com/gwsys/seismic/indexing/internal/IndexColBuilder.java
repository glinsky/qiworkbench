//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;
import java.nio.*;
import javax.xml.parsers.*;

import org.w3c.dom.*;
import org.xml.sax.*;

import com.gwsys.seismic.indexing.*;
import com.gwsys.seismic.util.TraceSampleFactory;

/**
 * Collect information for indexing files
 * <p>Copyright (c) 2003</p>
 * <p>INT Inc.</p>
 * @version 1.0
 */
public class IndexColBuilder {

  /**
   * Number of traces per SEGY file used to calculate statistic values
   */
  public static long TRACES_PER_SEGY = 1000;
  private CompoundIndexTree[] _indexTree = null;
  private int _numUserKeys = 0;
  private int _num_items;
  private FileDispatchDriverInterface _driver = new
      SingleThreadFileDispatchDriver();
  private DataSourceFiles _files;
  private long _l1, _l2;
  private SegyProcessingCallback _callback;

  private byte[] _traceDataBuffer = null;
  private float[] _samples = null;
  private double _num_samples = 0.0d;
  private double _d_min = Float.POSITIVE_INFINITY,
      _d_max = Float.NEGATIVE_INFINITY, _d_mean = 0.0f, _d_rms = 0.0f;
  private short _stype;

  /**
   * Collect information about keys
   * @param callback  callback
   * @param indexTree index tree
   * @param segyfiles a list of the segy files
   * @param order     order of the bytes
   * @param basepath  base path
   * @param format  if not <code>null</code>, defines custom SEGY data format
   * @return offset
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   */
  public int buildIndex(SegyProcessingCallback callback,
                        CompoundIndexTree[] indexTree, String[] segyfiles,
                        ByteOrder order, String basepath, SegyDataFormat format) throws IOException,
      ParserConfigurationException, SAXException {

    _callback = callback;
    _indexTree = indexTree;
    _numUserKeys = (indexTree != null) ? indexTree.length : 0;

        // Creates folders for every keys
        // Driver creats folder for every key.
        // The folder stores temporary files
        // for the key.
    _driver.createFolders(_indexTree);

    _num_items = 0;

    //=================================================================
    // 1. Creates a list of the index items. We'll use the items for storing
    // values of the keys in the memory
    //=================================================================
    int max_items = SegyIndexBuilder.getMaxItems();
    CompoundIndexItem[] items = new CompoundIndexItem[max_items];

    int id_offset = 0;

    for (int j = 0; j < max_items; j++) {

      IndexKey[] keys = new IndexKey[_indexTree.length];

      for (int i = 0; i < _indexTree.length; i++) {
        keys[i] = IndexKey.createKey(_indexTree[i].getFormat());
      }

      items[j] = new CompoundIndexItem(keys, 0);
    }
    //=================================================================
    // 1. END
    //=================================================================

    SegyReaderFactory factory = format == null ? SegyReaderFactory.getDefaultFactory() : SegyReaderFactory.getDefaultFactory(format);
    // Looks through a list of the input segy files
    for (int i = 0; i < segyfiles.length; i++) {

      String segyFileName = (basepath != null && basepath.length() > 0) ?
          basepath + File.separator + segyfiles[i] : segyfiles[i];
      SegyReader r = factory.createSegyReader(segyFileName, order, true);

      int ntraces = r.getNumberOfTraces();

      callback.addSegy(segyfiles[i], i, id_offset, id_offset + ntraces - 1,
                       r.getSampleUnit(), r.getSampleInterval(), r.getByteOrder());

      long traces_per_segy = Math.min(TRACES_PER_SEGY, ntraces);

      _l1 = ntraces / traces_per_segy;
      _l2 = ntraces / 100;

      _callback.printMessage("Processing " + segyfiles[i] + " ... ");
      _callback.printMessage("Count of traces " + ntraces + " ... ");

      id_offset += addIndex(id_offset, items, r);

      r.close();
    }

    //
    if (_num_items != 0) {
      saveItems(items);
      _num_items = 0;
    }
    _driver.flush();

    items = null;
    _driver = null;
    _callback = null;
    segyfiles = null;

    finish_collect();
    System.gc();
    return id_offset;
  }

  /**
   * Return array of indexes
   * @return array of indexes
   */
  public CompoundIndexTree[] getIndexes() {
    return _indexTree;
  }

  protected void finalize() throws Throwable {
    _indexTree = null;
    _callback = null;
    _driver = null;
    _files = null;
  }

  /**
   * Collect information from segy reader about traces and keys
   * @param id_offset the current trace id
   * @param items     a array of the items
   * @param rd        input data source
   * @return          the count of the processed traces
   * @throws IOException if reader throws exception during reading operations
   */
  private int addIndex(int id_offset,
                       CompoundIndexItem[] items, SegyReader rd) throws
      IOException {

    // number traces in the input file
    int numtraces = rd.getNumberOfTraces();

    long start = System.currentTimeMillis();

    _traceDataBuffer = null;
    _samples = null;
    for (int i = 0; i < numtraces; i++) {

      // collect information about samples for statistics
      if (_l1 != 0 && i % _l1 == 0) {
        collect_samples(rd, i);

      }
      if (_l2 != 0 && i % _l2 == 0) {
        _callback.printMessage(" " + ( (long) i / (long) _l2) + "% done (" + i +
                               " traces )");

        // seek and read the current trace header
        //rd.seekTraceHeader( i );

        //  gets keys for the current item
      }
      IndexKey[] keys = items[_num_items]._keys;

      // Load the keys from header
      for (int j = 0; j < _numUserKeys; j++) {
        //_indexTree[ j ].loadKey( rd, keys[ j ] );
        _indexTree[j].loadKeyFromTrace(rd, keys[j], i); // load a key
      }

      // save trace id
      items[_num_items++]._trace_id = i + id_offset;

      // save trace items
      if (_num_items == SegyIndexBuilder.getMaxItems()) {
        saveItems(items);
        _num_items = 0;
      }
    }
    return numtraces;
  }

  /**
   * Save items to the folder
   * @param items the itesm
   * @throws IOException if operation is wrong
   */
  private void saveItems(CompoundIndexItem[] items) throws IOException {
    _driver.addSeries(items, _num_items);
  }

  /**
   * @return MIN samples value
   */
  public float getMinValue() {
    return (float) _d_min;
  }

  /**
   * @return MAX samples value
   */
  public float getMaxValue() {
    return (float) _d_max;
  }

  /**
   * @return MEAN samples value
   */
  public float getMeanValue() {
    return (float) _d_mean;
  }

  /**
   * @return RMS samples value
   */
  public float getRmsValue() {
    return (float) _d_rms;
  }

  private void collect_samples(SegyReader r, int trace_id) throws IOException {

    _stype = r.getSampleType();

    int nsamples = r.getNumSamplesPerTrace();
    _num_samples += nsamples;

    if (_traceDataBuffer == null) {
      _traceDataBuffer = new byte[r.getTraceByteLen()];

    }
    if (_samples == null) {
      _samples = new float[nsamples];

    }
    r.readTraceSamples(_traceDataBuffer, 0, trace_id, 0, nsamples);

    float sample;

	TraceSampleFactory.toFloatArray(_stype,_traceDataBuffer, _samples, nsamples);

    for (int i = 0; i < nsamples; i++) {

      sample = _samples[i];

      _d_min = Math.min(_d_min, sample);
      _d_max = Math.max(_d_max, sample);

      if (sample < 0) {
        _d_mean -= sample;
      }
      else {
        _d_mean += sample;

      }
      _d_rms += sample * sample;
    }
  }

  private void finish_collect() {
    _d_mean /= _num_samples;
    _d_rms = (float) Math.sqrt( ( (double) _d_rms / ( (double) _num_samples)));
  }

  /**
   * Gets sample format
   * @return sample format
   */
  public short getSampleFormat() {
    return _stype;
  }

  /**
   * Parse a format file and create empty indexes for specified keys
   * @param stringKeys key names array
   * @param segyFormatFile fully qualified format file name
   * @return array of indexes
   * @throws ParserConfigurationException
   * @throws IOException
   * @throws SAXException
   */
  public static CompoundIndexTree[] parseSegyKeys(String[] stringKeys,
                                                  String segyFormatFile) throws
      ParserConfigurationException, IOException, SAXException {
    if (stringKeys == null || stringKeys.length == 0) {
      return null;
    }

    Document doc = loadIndexDescriptor(segyFormatFile);

    CompoundIndexTree[] indicies = new CompoundIndexTree[stringKeys.length];

    NodeList list = doc.getElementsByTagName("Field");

    int totalFields = list.getLength();

    int j = 0;

    for (int s = 0; s < totalFields; s++) {
      Node fieldNode = list.item(s);

      if (fieldNode.getNodeType() == Node.ELEMENT_NODE) {
        //get attrs of "field"
        NamedNodeMap attrs = fieldNode.getAttributes();

        if (attrs == null || attrs.getLength() == 0) {
          continue;
        }
        String name = null, format = null;
        int offset = 0;

        for (int i = 0; i < attrs.getLength(); ++i) {
          Node node = attrs.item(i);

          if (node.getNodeName().compareToIgnoreCase("Name") == 0) {
            name = node.getNodeValue().trim();
          }
          if (node.getNodeName().compareToIgnoreCase("Format") == 0) {
            format = node.getNodeValue().trim();
          }
          if (node.getNodeName().compareToIgnoreCase("Offset") == 0) {
            offset = Integer.parseInt(node.getNodeValue().trim());
          }
        }

        for (int i = 0; i < stringKeys.length; i++) {
          if (stringKeys[i].compareToIgnoreCase(name) == 0 &&
              j < indicies.length) {
            indicies[j] = new CompoundIndexTree(name, offset, format, j,
                                                indicies);
            j++;
          }
        }
      }
    }
    if (j != indicies.length) {
      return null;
    }

    return indicies;
  }

  private static Document loadIndexDescriptor(String uri) throws
      ParserConfigurationException, IOException, SAXException {
    return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(uri);
  }
}
