//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing.internal;

import java.io.*;

import com.gwsys.seismic.indexing.*;
import com.gwsys.seismic.util.SeismicByteFactory;

public class IndexKeyStep_ui8 extends IndexKeyStep {
	protected short _value = 0;

	public boolean isSolid() {
		return (Math.abs(_value) == 1);
	}

	public void setOne() {
		_value = 1;
	}

	public int getSign() {
		return _value < 0 ? -1 : _value > 0 ? 1 : 0;
	}

	public void write(DataOutput d) throws IOException {
		d.writeByte(_value);
	}

	public Number getValue() {
		return new Short(_value);
	}

	public void changeSign() {
		_value *= -1;
	}

	public int compareTo(Object s) throws ClassCastException {
		if (s instanceof IndexKeyStep_ui8) {
			return _value < ((IndexKeyStep_ui8) s)._value ? -1
					: _value == ((IndexKeyStep_ui8) s)._value ? 0 : 1;
		} else {
			throw new ClassCastException();
		}
	}

	public double div(IndexKeyStep s) throws ClassCastException {
		if (s instanceof IndexKeyStep_ui8) {
			return (double) _value / (double) ((IndexKeyStep_ui8) s)._value;
		} else {
			throw new ClassCastException();
		}
	}

	public int getSize() {
		return 1;
	}

	public void setValue(DataInput f) throws IOException {
		_value = f.readByte();
		if (_value < 0) {
			_value += 256;
		}
	}

	public void setValue(byte[] data, int offset) {
		_value = SeismicByteFactory.toUnsignedByte(data, offset);
	}

	public void setValue(String s) {
		// _value = Short.parseShort(s);
		_value = Double.valueOf(s).shortValue();
	}
}
