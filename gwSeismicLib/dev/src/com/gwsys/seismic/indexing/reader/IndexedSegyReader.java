//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.seismic.indexing.reader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.GWUtilities;
import com.gwsys.seismic.indexing.DataSourceKeys;
import com.gwsys.seismic.indexing.DataSourcePath;
import com.gwsys.seismic.indexing.IndexedSegyTraceReader;
import com.gwsys.seismic.indexing.DataSliceType;
import com.gwsys.seismic.indexing.SegyDataset;
import com.gwsys.seismic.indexing.SegyDatasetFactory;
import com.gwsys.seismic.indexing.SegyBinaryEntry;
import com.gwsys.seismic.indexing.SegyHeaderFormat;
import com.gwsys.seismic.indexing.SeismicConstants;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.io.SeismicMetaData;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.reader.SeismicFormat;
import com.gwsys.seismic.util.NumberRange;
import com.gwsys.seismic.util.ObjectPool;
import com.gwsys.seismic.util.SeismicByteFactory;
import com.gwsys.seismic.util.TraceSampleFactory;

/**
 * Creates a indexed reader for seismic data stored in a disk file based on the indexed
 * the SEG-Y format. This class is used in the seismic pipeline to retrieve trace and header data.
 *
 * Because some companies can use variations of the SEG-Y format, the actual
 * description of the fields in the main header(s) and trace header is described
 * using interface class <code>SeismicFormat</code>.
 * implements this interface for the standard version of the SEG-Y format.
 */
public class IndexedSegyReader {

    /**
     * Default factor
     */
    private static final double DEFAULT_FACTOR = 1000000.0;

    /**
     * Default sample rate
     */
    private static final double DEFAULT_SAMPLE_RATE = 0.004;

    /** Declares the variable that determines the limits of the data for traces and samples. */
    private Bound2D _modelLimits;

    /** Declares the variable for the SEG-Y format description. */
    private SeismicFormat _segyFormat;

    /** Declares the pool to store trace metadata (trace headers). */
    private ObjectPool _traceMetaDataCache;

    /** Declares the length of the trace headers. */
    private int _traceHeadersLength = -1;

    /** Declares the byte array for the trace data buffer. */
    private byte[] _traceDataBuffer = null;

    /** Declares the seismic metadata variable. */
    private SeismicMetaData _metaData = null;

    /** byte array for trace header */
    private byte[] _header;

    private boolean _statistics = true;

    /** trace header format */
    private SegyHeaderFormat _headerFormat;

    private double _factor = DEFAULT_FACTOR;

    private double _sampleRate = DEFAULT_SAMPLE_RATE;

    private byte[] _buffer = null;

    private SegyDataset _dataset = null;

    private IndexedSegyTraceReader _reader = null;

    /**
     * Constructs a new SEG-Y reader that will read data from specified file
     * and use standard SEG-Y format definition.</p>
     *
     * This constructor can throw <code>FileNotFoundException</code>.
     * Therefore you should use <code>try... catch...</code> clause while
     * creating objects of this class.</p>
     *
     * @param filename name of file to be used as seismic data source
     *
     * @throws FileNotFoundException if the file exists but is a directory rather
     * than a regular file, or cannot be opened for any other reason
     * @throws java.io.IOException if the file can not be read.
     */
    public IndexedSegyReader(String filename) throws FileNotFoundException,
            java.io.IOException {
        this(filename, (SeismicFormat) null, (DataSourceKeys) null);
    }

    /**
     *
     * This constructor can throw <code>FileNotFoundException</code>.
     * Therefore you should use <code>try... catch...</code> clause while
     * creating objects of this class.</p>
     *
     * @param filename name of file to be used as seismic data source
     * @param keys collection of keys for selection
     * @throws FileNotFoundException if the file exists but is a directory rather
     * than a regular file, or cannot be opened for any other reason
     * @throws java.io.IOException if the file can not be read.
     */
    public IndexedSegyReader(String filename, DataSourceKeys keys)
            throws FileNotFoundException, java.io.IOException {
        this(filename, (SeismicFormat) null, keys);
    }

    /**
     * Constructs a new SEG-Y reader that will read data from the specified file.
     *
     * This constructor can throw <code>FileNotFoundException</code>.
     * Therefore you should use <code>try... catch...</code> clause while
     * creating objects of this class.</p>
     *
     * @param filename      name of file to be used as seismic data source
     * @param segyFormat    the format of the input file
     * @param keys collection of keys for selection
     *
     * @throws FileNotFoundException if the file exists but is a directory rather
     * than a regular file, or cannot be opened for any other reason
     * @throws java.io.IOException if the file can not be read.
     */
    public IndexedSegyReader(String filename, SeismicFormat segyFormat,
            DataSourceKeys keys) throws FileNotFoundException,
            java.io.IOException {
        this(filename, segyFormat, keys, true);
    }

    /**
     * Constructs a new SEG-Y reader that will read data from the specified file.
     *
     * This constructor can throw <code>FileNotFoundException</code>.
     * Therefore you should use <code>try... catch...</code> clause while
     * creating objects of this class.</p>
     *
     * @param filename      name of file to be used as seismic data source
     * @param keys collection of keys for selection
     * @param indexedFile defines that indexed segy is used or standard segy. if
     *                    <code>indexedFile</code> is <code>false</code> then
     *                    filename should be segy file otherwise XGY file
     *
     * @throws FileNotFoundException if the file exists but is a directory rather
     * than a regular file, or cannot be opened for any other reason
     * @throws java.io.IOException if the file can not be read.
     */
    public IndexedSegyReader(String filename, DataSourceKeys keys,
            boolean indexedFile) throws FileNotFoundException,
            java.io.IOException {
        this(filename, (SeismicFormat) null, keys, indexedFile);
    }

    /**
     * Constructs a new SEG-Y reader that will read data from the specified file.
     * It openes whole data set.
     *
     * This constructor can throw <code>FileNotFoundException</code>.
     * Therefore you should use <code>try... catch...</code> clause while
     * creating objects of this class.</p>
     *
     * @param filename      name of file to be used as seismic data source
     * @param indexedFile defines that indexed segy is used or standard segy. if
     *                    <code>indexedFile</code> is <code>false</code> then
     *                    filename should be segy file otherwise XGY file
     *
     * @throws FileNotFoundException if the file exists but is a directory rather
     * than a regular file, or cannot be opened for any other reason
     * @throws java.io.IOException if the file can not be read.
     */
    public IndexedSegyReader(String filename, boolean indexedFile)
            throws FileNotFoundException, java.io.IOException {
        this(filename, null, indexedFile);
    }

    /**
     * Constructs a new SEG-Y reader that will read data from the specified file.
     *
     * This constructor can throw <code>FileNotFoundException</code>.
     * Therefore you should use <code>try... catch...</code> clause while
     * creating objects of this class.</p>
     *
     * @param filename      name of file to be used as seismic data source
     * @param segyFormat    the format of the input file
     * @param keys collection of keys for selection
     * @param indexedFile defines that indexed segy is used or standard segy. if
     *                    <code>indexedFile</code> is <code>false</code> then
     *                    filename should be segy file otherwise XGY file
     *
     * @throws FileNotFoundException if the file exists but is a directory rather
     * than a regular file, or cannot be opened for any other reason
     * @throws java.io.IOException if the file can not be read.
     */
    public IndexedSegyReader(String filename, SeismicFormat segyFormat,
            DataSourceKeys keys, boolean indexedFile)
            throws FileNotFoundException, java.io.IOException {
        this(filename, segyFormat, keys, indexedFile, DEFAULT_SAMPLE_RATE);
    }

    /**
     * Constructs a new SEG-Y reader that will read data from the specified file.
     *
     * This constructor can throw <code>FileNotFoundException</code>.
     * Therefore you should use <code>try... catch...</code> clause while
     * creating objects of this class.</p>
     *
     * @param filename      name of file to be used as seismic data source
     * @param path data source path
     * @throws FileNotFoundException if the file exists but is a directory rather
     * than a regular file, or cannot be opened for any other reason
     * @throws java.io.IOException if the file can not be read.
     */
    public IndexedSegyReader(String filename, DataSourcePath path)
            throws FileNotFoundException, java.io.IOException {
        this(filename, (SeismicFormat) null, path);
    }

    /**
     * Constructs a new SEG-Y reader that will read data from the specified file.
     *
     * This constructor can throw <code>FileNotFoundException</code>.
     * Therefore you should use <code>try... catch...</code> clause while
     * creating objects of this class.</p>
     *
     * @param filename      name of file to be used as seismic data source
     * @param segyFormat    the format of the input file
     * @param path data source path
     * @throws FileNotFoundException if the file exists but is a directory rather
     * than a regular file, or cannot be opened for any other reason
     * @throws java.io.IOException if the file can not be read.
     */
    public IndexedSegyReader(String filename, SeismicFormat segyFormat,
            DataSourcePath path) throws FileNotFoundException,
            java.io.IOException {

        _sampleRate = DEFAULT_SAMPLE_RATE;

        _dataset = SegyDatasetFactory.getDefaultFactory().createDataset(
                filename);

        // load from file
        if (segyFormat == null) {
            segyFormat = loadSegyFormat(_dataset.getSegyFormatFile());
        }

        // time slice mode
        if (_dataset.getDataOrder() == DataSliceType.DATA_MAP) {
            _sampleRate = _dataset.getSampleInterval();
            _factor = 1;
        }

        _reader = _dataset.getTraceReader(path);

        _segyFormat = segyFormat;

        // Creates a cache with a size of 100,000 bytes.
        _traceMetaDataCache = new ObjectPool(100000);

        // Calculate dataset statistics
        calculateStatistics();

        // prepare buffer for trace header
        Vector traceHeaderFormats = _segyFormat.getTraceHeaderFormats();

        if (traceHeaderFormats.size() != 1) {
            GWUtilities.warning("Support only single trace header per trace");
        }

        _headerFormat = (SegyHeaderFormat) traceHeaderFormats.elementAt(0);

        _header = new byte[_headerFormat.getLength()];

    }

    /**
     * Constructs a new SEG-Y reader that will read data from the specified file.
     *
     * This constructor can throw <code>FileNotFoundException</code>.
     * Therefore you should use <code>try... catch...</code> clause while
     * creating objects of this class.</p>
     *
     * @param filename      name of file to be used as seismic data source
     * @param segyFormat    the format of the input file
     * @param keys collection of keys for selection
     * @param indexedFile defines that indexed segy is used or standard segy. if
     *                    <code>indexedFile</code> is <code>false</code> then
     *                    filename should be segy file otherwise XGY file
     * @param sampleRate the custom sample rate (System uses it instead of value from file )
     * @throws FileNotFoundException if the file exists but is a directory rather
     * than a regular file, or cannot be opened for any other reason
     * @throws java.io.IOException if the file can not be read.
     */
    public IndexedSegyReader(String filename, SeismicFormat segyFormat,
            DataSourceKeys keys, boolean indexedFile, double sampleRate)
            throws FileNotFoundException, java.io.IOException {

        // XGY indexed file
        if (indexedFile) {

            _dataset = SegyDatasetFactory.getDefaultFactory().createDataset(
                    filename);

            // load from file
            if (segyFormat == null) {
                segyFormat = loadSegyFormat(_dataset.getSegyFormatFile());
            }
        } else {
            _statistics = false; // It is segy file

            String[] files = { filename };
            String[] orders = { "BIG_ENDIAN" };
            _dataset = SegyDatasetFactory.getDefaultFactory().createDataset("",
                    files, orders);
        }

        // time slice mode
        if (_dataset.getDataOrder() == DataSliceType.DATA_MAP) {
            _sampleRate = _dataset.getSampleInterval();
            _factor = 1;
        }

        _reader = _dataset.getTraceReader(keys);

        _segyFormat = segyFormat;

        // Creates a cache with a size of 100,000 bytes.
        _traceMetaDataCache = new ObjectPool(100000);

        // Calculate dataset statistics
        calculateStatistics();

        // prepare buffer for trace header
        Vector traceHeaderFormats = _segyFormat.getTraceHeaderFormats();

        if (traceHeaderFormats.size() != 1) {
            System.err.println("At this time SEG-Y reader only supports data");
            System.err.println("with a single trace header per trace.");
        }

        _headerFormat = (SegyHeaderFormat) traceHeaderFormats.elementAt(0);

        _header = new byte[_headerFormat.getLength()];
    }

    /**
     * Gets a list of indexes for current file
     * @param filename file name
     * @return the list of the indexes
     */
    public static DataSourceKeys getIndexes(String filename) {
        try {

            SegyDataset dataset = SegyDatasetFactory.getDefaultFactory()
                    .createDataset(filename);

            return dataset.getDatasourceKeys();
        } catch (java.io.IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Reads the trace data for the specified trace index and specified range of samples.</p>
     *
     * @param traceId       the trace ID to process
     * @param sampleRange   the range of samples for this process
     * @param traceDataOut  the destination data for the trace process
     *
     * @return <code>true</code> if the process was successfully completed,
     *         <code>false</code> otherwise.
     */
    public boolean readTrace(int traceId, NumberRange sampleRange,
            TraceData traceDataOut) {

        return readTraceData(traceId, sampleRange, traceDataOut);
    }

    /**
     * Retrieves the dataset limits in model coordinates.
     * This method is for internal use only.</p>
     *
     * @return the limits of the seismic data in model coordinates
     */
    private Bound2D getModelLimits() {

        if (_modelLimits == null) {
            _metaData = readMetaData();
            calculateLimits();
        }

        return _modelLimits;
    }

    /**
     * Retrieves information about this dataset.
     *
     * @return the metadata for the dataset
     */
    public SeismicMetaData getMetaData() {

        if (_metaData == null) {
            _metaData = readMetaData();
            return _metaData;
        } else {
            return _metaData;
        }
    }

    /**
     * Retrieves header information for specified trace index.
     * @param traceId    the ID for the trace to read from the file
     *
     * @return trace metadata
     */
    public TraceHeader getTraceMetaData(int traceId) {

        Integer key = new Integer(traceId);

        TraceHeader traceMetaData = (TraceHeader) _traceMetaDataCache
                .getObject(key);
        if (traceMetaData != null) {
            return traceMetaData;
        }

        traceMetaData = readTraceMetaData(traceId, _header, _headerFormat);

        if (traceMetaData != null) {
            _traceMetaDataCache.push(traceMetaData);
        }

        return traceMetaData;
    }

    // ======================================================
    //   Methods for internal use
    // ======================================================

    /**
     * Reads the seismic metadata from the input file. The SEG-Y metadata
     * consists of an optional ASCII tape header, an EBCDIC header, and
     * zero or more optional extended EBCDIC headers.</p>
     *
     * @return the seismic metadata describing this line
     */
    private SeismicMetaData readMetaData() {

        if (_metaData != null) {
            return _metaData;
        }

        SeismicMetaData metaData = new SeismicMetaData();
        metaData.setDataStatistics(Float.MIN_VALUE, Float.MAX_VALUE, 0.0, 0.2);

        try {
            int samplesPerTrace = _dataset.getNumSamplesPerTrace();
            int numberOfTraces = _reader.getNumTraces();
            metaData.setNumberOfTraces(numberOfTraces);
            metaData.setSamplesPerTrace(samplesPerTrace);

            double sampleInterval = _dataset.getSampleInterval();

            if (sampleInterval <= 0) {
                metaData.setSampleRate((float) _sampleRate);
            } // convert to seconds only for default sample rate
            else if (_sampleRate == DEFAULT_SAMPLE_RATE) {
                // Sample rate is given in terms of microseconds; convert to seconds.
                metaData.setSampleRate((float) (sampleInterval / _factor));
            } else {
                metaData.setSampleRate((float) _sampleRate);
            }

            metaData.setSampleStart(0.0);
            metaData.setSampleUnits(_dataset.getUnits());
            metaData.setDataStatistics(_dataset.getSampleMin(), _dataset
                    .getSampleMax(), _dataset.getSampleMean(), _dataset
                    .getSampleRms());
        } catch (java.lang.Exception ex) {

        }

        return metaData;
    }

    /**
     * Reads the meta data for every trace in the input file.  For
     * SEG-Y data, the trace meta data consists of the data contained
     * in the trace header.</p>
     */
    private void calculateLimits() {

        _modelLimits = new Bound2D(0, 0, _metaData.getNumberOfTraces() - 1,
                _metaData.getSamplesPerTrace() * _metaData.getSampleRate());
    }

    /**
     * Reads the trace data for the given trace ID. This method requires
     * a data array corresponding to the trace ID.</p>
     *
     * @param traceId       the trace ID being read
     * @param dest          the destination trace data object
     * @param sampleRange   the sample range
     *
     * @return <code>true</code> if the read succeeded, <code>false</code> otherwise
     */
    private boolean readTraceData(int traceId, NumberRange sampleRange,
            TraceData dest) {

        int sampleLength = getTraceSampleLength();
        int traceOffset = (sampleRange == null) ? 0 : sampleRange.getMin()
                .intValue()
                * sampleLength;

        // Calculate number of samples we need
        int numSamples;
        if (sampleRange == null) {
            numSamples = _metaData.getSamplesPerTrace();
        } else {
            numSamples = sampleRange.getRange() + 1;

        }
        if (numSamples > _metaData.getSamplesPerTrace()) {
            numSamples = _metaData.getSamplesPerTrace();

        }
        int numBytes = numSamples * sampleLength;

        if ((_traceDataBuffer == null) || (_traceDataBuffer.length < numBytes)) {
            _traceDataBuffer = new byte[numBytes];
        }

        try {
            //_buffer
            int buffer_size = _reader.getTracesDataSize(traceId, 1, sampleRange
                    .getMin().intValue(), numSamples);
            if (buffer_size == 0) {
                return false;
            }

            if (_buffer == null || _buffer.length != buffer_size) {
                _buffer = new byte[buffer_size];
            }
            //byte[] _buffer = _reader.getTracesData( traceId, 1, sampleRange.getMin(), numSamples );
            _reader.getTracesData(_buffer, traceId, 1, sampleRange.getMin()
                    .intValue(), numSamples);

            System.arraycopy(_buffer, getTraceHeadersLength(),
                    _traceDataBuffer, 0, numBytes);

        } catch (Exception e) {
            return false;
        }

        dest.setNumAppliedSamples(numSamples);
        float[] dataArray = dest.getSamples();

        TraceSampleFactory.toFloatArray(_metaData.getDataFormat(),
                _traceDataBuffer, dataArray, numSamples);

        dest.setHeader(new Integer(traceId));

        return true;
    }

    /**
     * Parses the header fields.</p>
     *
     * @param header        the binary header to parse
     * @param headerFields  the field descriptions for the header
     * @param dataValues    the hashtable of the header values
     */
    @SuppressWarnings("unchecked")
    private void parseHeaderFields(byte[] header, Vector headerFields,
            Hashtable dataValues) {

        SegyBinaryEntry field;

        for (Enumeration e = headerFields.elements(); e.hasMoreElements();) {

            field = (SegyBinaryEntry) e.nextElement();

            dataValues.put(new Integer(field.getFieldId()),
                    SeismicByteFactory.toNumber(field.getValueType(), header, field
                            .getByteOffset()));
        }
    }

    /**
     * Returns the entire length, in bytes, of all the trace headers
     * for a single trace. This is used to determine offsets when
     * reading trace data.</p>
     *
     * @return the length, in bytes, of all headers for a single trace
     */
    private int getTraceHeadersLength() {

        if (_traceHeadersLength == -1) {
            _traceHeadersLength = 0;

            Vector traceHeaderFormats = _segyFormat.getTraceHeaderFormats();
            Enumeration headerEnum = traceHeaderFormats.elements();

            while (headerEnum.hasMoreElements()) {
                SegyHeaderFormat headerFormat = (SegyHeaderFormat) headerEnum
                        .nextElement();
                _traceHeadersLength += headerFormat.getLength();
            }
        }

        return _traceHeadersLength;
    }

    /**
     * Gets the length of the trace data for a single trace.</p>
     *
     * @return the length of the trace data for a single trace
     */
    private int getTraceDataLength() {
        return (_metaData.getSamplesPerTrace() * getTraceSampleLength());
    }

    /**
     * Get the length of a trace sample in bytes.</p>
     *
     * @return the length of a trace sample in bytes.
     */
    private int getTraceSampleLength() {

        int sampleLength;
        switch (_metaData.getDataFormat()) {
        case SeismicFormat.DATA_SAMPLE_BYTE:
            sampleLength = 1;
            break;

        case SeismicFormat.DATA_SAMPLE_SHORT:
            sampleLength = 2;
            break;

        case SeismicFormat.DATA_SAMPLE_IBM_FLOAT:
        case SeismicFormat.DATA_SAMPLE_FLOAT:
        case SeismicFormat.DATA_SAMPLE_INTEGER:
            sampleLength = 4;
            break;
        default:
            throw new UnsupportedOperationException(
                    "SEGY data sample format code " + _metaData.getDataFormat()
                            + " not supported ");
        }
        return sampleLength;
    }


    /**
     * Reads the trace metadata for the given trace ID from the input
     * file. You must allocate the data buffer for the header before
     * calling this method.</p>
     *
     * @param traceId       the ID for the trace to read from the file
     * @param header        the data buffer for the header
     * @param headerFormat  the description of the header format
     * @return the trace meta data
     */
    private TraceHeader readTraceMetaData(int traceId, byte[] header,
            SegyHeaderFormat headerFormat) {

        if (headerFormat.getType() != SegyHeaderFormat.BINARY_HEADER) {
            GWUtilities.warning("Does not contain non-binary headers");
            return null;
        }

        try {
            byte[] buffer = _reader.getTracesData(traceId, 1, 0, 1);
            System
                    .arraycopy(buffer, 0, header, 0, this
                            .getTraceHeadersLength());
        } catch (Exception e) {
            return null;
        }

        return parseTraceHeader(traceId, header, headerFormat.getHeaderFields());
    }

    /**
     * Parses a trace header. This methods retrieves a metadata object
     * created from the information inside the trace header.</p>
     *
     * @param traceId       the unique trace identifier, usually the TSSN
     * @param header        the trace header to parse
     * @param headerFields  the field descriptions for the header
     *
     * @return the metadata object created from the header information
     */
    private TraceHeader parseTraceHeader(int traceId, byte[] header,
            Vector headerFields) {

        TraceHeader traceMetaData = new TraceHeader(traceId,(double)traceId,
                parseStartValue(header, headerFields),
                parseNumberOfSamples(header, headerFields),
                SeismicConstants.NORMAL_TRACE);

        parseHeaderFields(header, headerFields, traceMetaData.getDataValues());

        return traceMetaData;
    }

    /**
     * Parses the trace header to determine the start sample.</p>
     *
     * @param header        the trace header to parse
     * @param headerFields  the field descriptions for the header
     *
     * @return the start sample for this trace
     */
    private double parseStartValue(byte[] header, Vector headerFields) {

        Integer startValue = parseFieldInt(SeismicFormat.HEADER_START_TIME,
                header, headerFields);

        if (startValue == null) {
            return 0.0;
        } else {
            // Sample rate is given in terms of microseconds; convert to seconds.

            return (_sampleRate == DEFAULT_SAMPLE_RATE) ? (startValue
                    .intValue() / _factor) : startValue.intValue();
        }
    }

    /**
     * Parses the trace header to determine the number of samples.</p>
     *
     * @param header        the trace header to parse
     * @param headerFields  the field descriptions for the header
     *
     * @return the number of samples for this trace
     */
    private int parseNumberOfSamples(byte[] header, Vector headerFields) {

        Integer numberOfSamples = parseFieldInt(
                SeismicFormat.HEADER_SAMPLES_IN_TRACE, header, headerFields);

        if (numberOfSamples == null) {
            return 0;
        } else {
            // Sample rate is given in terms of microseconds; convert to milliseconds.
            return (numberOfSamples.intValue());
        }
    }

    /**
     * Parses a given field from a binary header. Possible values are
     * <code>DATA_FORMAT_BYTE</code>, <code>DATA_FORMAT_UNSIGNEDBYTE</code>,
     * <code>DATA_FORMAT_SHORT</code>, <code>DATA_FORMAT_UNSIGNEDSHORT</code>,
     * <code>DATA_FORMAT_INT</code>, <code>DATA_FORMAT_UNSIGNEDINT</code></p>
     *
     * This method returns <code>false</code> if the specified fields do
     * not exist or cannot complete the parsing operation.</p>
     *
     * @param desiredField  the constant indicating the desired value
     * @param header        the header itself, stored as a stream of bytes
     * @param headerFields  the fields that describing this header
     *
     * @return <code>true</code> if the parsing is successfull;
     *         <code>false</code> otherwise
     */
    private Integer parseFieldInt(int desiredField, byte[] header,
            Vector headerFields) {

        Number convertedNumber;

        for (Enumeration fieldEnum = headerFields.elements(); fieldEnum
                .hasMoreElements();) {

            SegyBinaryEntry field = (SegyBinaryEntry) fieldEnum
                    .nextElement();

            if (field.getFieldId() == desiredField) {

                convertedNumber = SeismicByteFactory.toNumber(field.getValueType(),
                        header, field.getByteOffset());

                return new Integer(convertedNumber.intValue());

            }
        }

        return null;
    }

    /**
     * Calculates statistics (amplitude minimum, maximum, average, RMS)
     * for the seismic dataset. The <code>limits</code> variable
     * constrains the range over which the statistics are collected,
     * while the <code>increment</code> variable controls the increment
     * between each trace used for statistics.</p>
     */
    private void calculateStatistics() {

        Bound2D limits = getModelLimits();

        // Calculate trace increment for gathering dataset statistics so
        // that we will collect statistics using about 200 traces.
        int increment = (int) ((limits.getMaxX() - limits.getMinX()) / 200);

        // If we have less than 200 traces
        // we will collect statistics using whole dataset.
        if (increment < 1) {
            increment = 1;

        }
        int startTrace = (int) limits.getMinX();
        int endTrace = (int) limits.getMaxX();

        int startSample = (int) (limits.getMinY() / _metaData.getSampleRate());
        int endSample = (int) (limits.getMaxY() / _metaData.getSampleRate());

        TraceData data = new TraceData();
        float[] samples = new float[endSample - startSample];
        data.setSamples(samples);
        NumberRange sampleRange = new NumberRange(startSample, endSample);

        if (!_statistics) {

            float minimum = Float.MAX_VALUE;
            float maximum = Float.MIN_VALUE;
            double sumX = 0;
            double sumX2 = 0;
            int count = 0;

            for (int i = startTrace; i <= endTrace; i += increment) {

                readTrace(i, sampleRange, data);
                //samples = data.getSamples();

                // Gathers statistics.
                for (int j = 0; j < (endSample - startSample); j++) {

                    if (samples[j] < minimum) {
                        minimum = samples[j];
                    } else if (samples[j] > maximum) {
                        maximum = samples[j];

                    }
                    if (samples[j] < 0) {
                        sumX -= samples[j];
                    } else {
                        sumX += samples[j];

                    }
                    sumX2 += samples[j] * samples[j];
                }

                count += (endSample - startSample);
            }

            if (count == 0) {
                _metaData.setDataStatistics(minimum, maximum, 0, 0);
            } else {
                _metaData.setDataStatistics(minimum, maximum, sumX / count,
                        Math.sqrt(sumX2 / count));
            }
        } else {
            try {
                _metaData.setDataStatistics(_dataset.getSampleMin(), _dataset
                        .getSampleMax(), _dataset.getSampleMean(), _dataset
                        .getSampleRms());
            } catch (IOException e) {
                // ignore
            }

        }
    }

    /**
     * Load segy format from XML file
     * @param formatFileName the file name of the segy format
     * @return the segy format object or null
     * @throws java.io.IOException
     */
    private SeismicFormat loadSegyFormat(String formatFileName)
            throws java.io.IOException {
        DynamicSegyFormat format = null;

        if (formatFileName != null) {

            try {

                format = new DynamicSegyFormat();

                java.io.File file = new java.io.File(formatFileName);

                java.io.InputStream stream = new java.io.FileInputStream(file);

                //load segy format from XML
                format.load(stream);
                stream.close();
            } catch (java.io.IOException ex) {
                throw ex;
            }
        }

        return format;
    }

}
