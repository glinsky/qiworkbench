//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

import java.io.*;
import java.nio.*;

import com.gwsys.seismic.indexing.internal.*;

/**
 * This class implememnts SEGY reader factory
 * <p>Copyright (c) 2003</p>
 * <p>Company: INT Inc.</p>
 */
public class SegyReaderFactory {
  private SegyDataFormat _format = null;

  private SegyReaderFactory(SegyDataFormat format) {
    _format = format;
  }

  private SegyReaderFactory() {
    _format = new DefaultSegyDataFormat();
  }

  /**
   * Gets default SEGY reader factory based on default SEGY data format
   * @return default SEGY reader factory
   */
  public static SegyReaderFactory getDefaultFactory() {
    return new SegyReaderFactory();
  }

  /**
   * Gets default SEGY reader factory based on supplied SEGY data format
   * @param format SEGY data format
   * @return default SEGY reader factory
   */
  public static SegyReaderFactory getDefaultFactory(SegyDataFormat format) {
    return new SegyReaderFactory(format);
  }

  /**
   * Creates new instance of <code>SegyReader</code> based on filename and byte order supplied.
   * Whether this reader caching data or not is controlling by value of <code>USE_CACHED_READER</code> flag.
   * @param filename Fully qualified SEGY file name
   * @param order Assumed byte order of this SEGY file
   * @param useCache flag defining use caching or not
   * @return new instance of <code>SegyReader</code>
   * @throws FileNotFoundException
   * @throws IOException
   */
  public SegyReader createSegyReader(String filename, ByteOrder order,
                                     boolean useCache) throws
      FileNotFoundException, IOException {
    if (useCache) {
      return new CachedSegyReader(filename, order, _format);
    }
    else {
      return new SimpleSegyReader(filename, order, _format);
    }
  }

  /**
   * Creates new instance of SEGY reader based on supplied information
   * @param filenames array of SEGY file names
   * @param order byte order of source SEGY files
   * @param basepath base path of SEGY files
   * @return SEGY reader
   * @throws FileNotFoundException
   * @throws IOException
   */
  public SegyReader createSegyReader(String[] filenames, ByteOrder order,
                                     String basepath) throws
      FileNotFoundException, IOException {
    return new CompoundSegyReader(filenames, order, basepath, this);
  }

  /**
   * Creates new instance of SEGY reader based on supplied information
   * @param files datasource files
   * @return SEGY reader
   * @throws FileNotFoundException
   * @throws IOException
   */
  public SegyReader createSegyReader(DataSourceFiles files) throws
      FileNotFoundException, IOException {
    return new CompoundSegyReader(files, this);

  }

  /**
   * Gets SEGY data format using by this factory
   * @return SEGY data format
   */
  public SegyDataFormat getSegyDataFormat() {
    return _format;
  }
}
