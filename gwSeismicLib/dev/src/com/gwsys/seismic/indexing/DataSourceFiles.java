//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

import java.io.*;
import java.util.*;

/**
 * Instances of this class maintains an ordered collection of datasource files
 * <p>Copyright (c) 2003</p>
 * <p>INT Inc.</p>
 * @version 1.0
 */
public class DataSourceFiles {
  private Vector _files = null;

  /**
   * Populates internal datasource files collection using supplied information
   * @param segy array of relative SEGY file names
   * @param order array of corresponding byte orders
   * @param basepath base path of SEGY files
   * @throws IOException
   */
  public void createFiles(String[] segy, String[] order, String basepath) throws
      IOException {
    _files.clear();
    int trace_offset = 0;
    for (int i = 0; i < segy.length; i++) {
      DataSourceFile f = new DataSourceFile(segy[i], basepath, order[i]);
      f.setSegyOrdinal(i);
      f.setStartTrace(f.getStartTrace() + trace_offset);
      f.setEndTrace(f.getEndTrace() + trace_offset);
      trace_offset = f.getEndTrace() + 1;
      addItem(f);
    }
  }

  /**
   * Creates an empty datasource files collection
   */
  public DataSourceFiles() {
    _files = new Vector();
  }

  /**
   * Creates a new instance of datasource files collection and reserving a room for supplied number of datasource files
   * @param num number of datasource files
   */
  public DataSourceFiles(int num) {
    _files = new Vector(num);
  }

  /**
   * Gets a current number of datasource files in collection
   * @return number of datasource files
   */
  public int getCount() {
    return _files.size();
  }

  /**
   * Adds supplied datasource file to the end of collection
   * @param file datasource file
   */
  public void addItem(DataSourceFile file) {
    _files.addElement(file);
  }

  /**
   * Gets a datasource file by its index in collection
   * @param index index of datasource file
   * @return datasource file
   */
  public DataSourceFile getItemByIndex(int index) {
    if (getCount() - 1 < index) {
      return null;
    }
    return (DataSourceFile) _files.elementAt(index);
  }

  /**
   * Gets a datasource file by corresponding trace id
   * @param trace trace id
   * @return datasource file
   */
  public DataSourceFile getItemByTrace(int trace) {
    if (getCount() == 0) {
      return null;
    }
    Iterator i = _files.iterator();
    while (i.hasNext()) {
      DataSourceFile f = (DataSourceFile) i.next();
      if (f.getStartTrace() <= trace && f.getEndTrace() >= trace) {
        return f;
      }
    }
    return null;
  }

  /**
   * Gets a datasource file by its name
   * @param name name of datasource file
   * @return datasource file
   */
  public DataSourceFile getItemByName(String name) {
    if (getCount() == 0) {
      return null;
    }
    Iterator i = _files.iterator();
    while (i.hasNext()) {
      DataSourceFile f = (DataSourceFile) i.next();
      if (f.getFileName().equalsIgnoreCase(name)) {
        return f;
      }
    }
    return null;
  }
}
