//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

import java.io.*;

/**
 * Generic SEGY readers interface
 * <p>Copyright (c) 2003</p>
 * <p>INT Inc.</p>
 * @version 1.0
 */
public interface IndexedSegyTraceReader {

  /**
   * Return number of traces
   * @return number of traces
   */
  int getNumTraces();

  /**
   * Return raw traces data
   * @param from start trace id
   * @param count number of traces to return
   * @return traces data
   * @throws IOException
   */
  byte[] getTracesData(int from, int count) throws IOException;

  /**
   * Return raw traces data
   * @param from start trace id
   * @param count number of traces to return
   * @param from_sample sample to start
   * @param sample_count number of samples to return
   * @return traces data
   * @throws IOException
   */
  byte[] getTracesData(int from, int count, int from_sample, int sample_count) throws
      IOException;

  /**
   * Return SEGY dataset
   * @return SEGY dataset
   */
  SegyDataset getSegyDataset();

  /**
   * Return datasource keys describing this SEGY
   * @return datasource keys
   */
  DataSourceKeys getIndexKeys();

  /**
   * Get size of the buffer that must be for current part of the data
   * @param from start trace id
   * @param count number of traces to return
   * @param from_sample from_sample sample to start
   * @param sample_count sample_count number of samples to return
   * @return the size of the data
   */
  int getTracesDataSize(int from, int count, int from_sample, int sample_count);

  /**
   * Get size of the buffer that must be for current part of the data
   * @param from start trace id
   * @param count number of traces to return
   * @return the size of the data
   */
  int getTracesDataSize(int from, int count);

  /**
   * Return raw traces data
   * @param buf input buffer
   * @param from start trace id
   * @param count number of traces to return
   * @param from_sample sample to start
   * @param sample_count number of samples to return
   * @throws IOException
   */
  void getTracesData(byte[] buf, int from, int count, int from_sample,
                     int sample_count) throws IOException;

  /**
   * Return raw traces data
   * @param buf input buffer
   * @param from start trace id
   * @param count number of traces to return
   * @throws IOException
   */
  void getTracesData(byte[] buf, int from, int count) throws IOException;

}
