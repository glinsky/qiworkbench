//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

import java.io.*;
import java.nio.*;

/**
 * Segy dataset defines source of
 * the indexed segy data
 * <p>Copyright (c) 2003</p>
 * <p>INT Inc.</p>
 * @version 1.0
 */
public interface SegyDataset {

  /**
   * Gets used indexes in the indexed
   * segy data source
   * @return the collection of the indexes
   */
  public DataSourceKeys getDatasourceKeys();

  /**
   * Gets collection of the used segy files in
   * the data source
   * @return the collection of the segy files
   */
  public DataSourceFiles getSegys();

  /**
   * Gets used indexed in the indexed
   * segy data source
   * @return the collection of the indexes
   */
  public DataSourceKeys getIndexes();
  
  /**
   * Gets the start value
   * @return the start value
   * @throws IOException
   */
  public double getStartValue() throws IOException;

  /**
   * Gets number samples per trace. This data gets from
   * segy files directly.
   * @return the count samples per trace
   * @throws IOException
   */
  public int getNumSamplesPerTrace() throws IOException;

  /**
   * Gets the used units
   * @return the units
   */
  public int getUnits();

  /**
   * Gets the name of the data source
   * @return the name of the data source
   */
  public String getProfileName();

  /**
   * Gets segy format of the file
   * @return the segy format file. It is a reference to file that includes
   * description of the used segy format.
   */
  public String getSegyFormatFile();

  /**
   * Gets byte order of this dataset's SEGY files (assuming all SEGY files have the same data order)
   * @return byte order of this dataset's SEGY files
   */
  public ByteOrder getDataFormat();

  /**
   * Gets the base path of the data source
   * @return the base path
   */
  public String getBasePath();

  /**
   * Check if segy files are in the place
   * @return if segy files exist the return true
   */
  public boolean isSegyFilesExist();

  /**
   * Gets the data order
   * @return the data format
   * <table border="1" cellpaddding="3" cellspacing="0">
   *   <tr>
   *       <td><code>CROSSECTION</code></td>
   *       <td>crossection data</td>
   *   </tr>
   *   <tr>
   *     <td><code>MAP</code</td>
   *     <td>time slice data</td>
   *   </tr>
   * </table>
   */
  public int getDataOrder();

  /**
   * Gets the data type
   * If does not support now.
   * @return the data type.
   */
  public int getDataType();

  /**
   * Gets the trace format
   * @return the trace format
   * <table border="1" cellpadding="3" cellspacing="0">
   *  <tr><td>LITTLE_ENDIAN</td><td>ByteOrder.LITTLE_ENDIAN</td></tr>
   *  <tr><td>BIG_ENDIAN</td><td>ByteOrder.BIG_ENDIAN</td></tr>
   * </table>
   */
  public int getTraceFormat();

  /**
   * Gets count of the traces in hte data set
   * @return the count of the traces
   */
  public int getNumTraces();

  /**
   * Gets transposed key field
   * @return the transposed key
   */
  public String getTransposedKey();

  /**
   * Gets the transposed key name
   * @return the key name
   */
  public String getTransposedName();

  /**
   * Gets the sample interval
   * @return the sample interval
   */
  public int getSampleInterval();

  /**
   * Gets the segy trace reader for data source path
   * @param path the path
   * @return the new data indexed segy data source
   * @throws IOException
   */
  public IndexedSegyTraceReader getTraceReader(DataSourcePath path) throws
      IOException;

  /**
   * Gets the trace reader
   * @param keys the query keys
   * @return the trace reader
   * @throws IOException
   */
  public IndexedSegyTraceReader getTraceReader(DataSourceKeys keys) throws
      IOException;

  /**
   * Gets calculated MIN value of this dataset's SEGYs samples (approximated by processing small random number of samples)
   * @return MIN value of this dataset's SEGYs samples
   * @throws IOException
   */
  public float getSampleMin() throws IOException;

  /**
   * Gets calculated MAX value of this dataset's SEGYs samples (approximated by processing small random number of samples)
   * @return MAX value of this dataset's SEGYs samples
   * @throws IOException
   */
  public float getSampleMax() throws IOException;

  /**
   * Gets calculated MEAN value of this dataset's SEGYs samples (approximated by processing small random number of samples)
   * @return MEAN value of this dataset's SEGYs samples
   * @throws IOException
   */
  public float getSampleMean() throws IOException;

  /**
   * Gets calculated RMS value of this dataset's SEGYs samples (approximated by processing small random number of samples)
   * @return RMS value of this dataset's SEGYs samples
   * @throws IOException
   */
  public float getSampleRms() throws IOException;

  /**
   * Gets an instance of <code>SegyIndex</code> corresponding to the given name
   * @param name name of index
   * @return instance of <code>SegyIndex</code> corresponding to the given name
   * @throws IOException
   */
  public SegyIndex getIndex(String name) throws IOException;
}
