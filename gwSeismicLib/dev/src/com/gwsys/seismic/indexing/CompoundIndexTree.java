//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

import java.io.*;
import java.nio.*;
import java.util.*;

import com.gwsys.seismic.indexing.internal.*;
import com.gwsys.seismic.indexing.util.*;

/**
 * Maintains a tree of index keys and corresponding trace IDs
 * <p>Copyright (c) 2003</p>
 * <p>INT Inc.</p>
 * @version 1.0
 */
public class CompoundIndexTree
    implements Cloneable {

  private int _offset, _index;
  private short _format;
  private String _name = null;
  private IndexKey keyProducer = null;
  private IndexKey _min = null, _max = null;
  private IndexKeyStep _step = null;
  private SeriesFileDescriptor _filedesc = null;
  private CompoundIndexTree[] _tree;
  private int _num_keys = 0;
  private int _numtraces = 0;
  private SeriesFileDescriptor _ref_file = null;
  private int _ref_frame;

  /**
   * Gets offset of this index keys in a trace header
   * @return index offset
   */
  public int getOffset() {
    return _offset;
  }

  /**
   * Gets an empty instance of index key, usually for new keys producing purposes
   * @return index key
   */
  public IndexKey getKeyProducer() {
    return keyProducer;
  }

  /**
   * Gets a value of reference key
   * @param trace_id trace id
   * @param key index key to be filled with a value
   * @param _ref_buffer reference buffer
   * @return referenced index key
   * @throws IOException
   */
  public IndexKey getRefKey(int trace_id, IndexKey key, ByteBuffer _ref_buffer) throws
      IOException {
    key.setValue(_ref_buffer, buffer_pos(trace_id));
    return key;
  }

  /**
   * Prepares a tree for compacting
   */
  public void prepareCompact() {
    _ref_frame = 0;
    for (int i = 0; i < _tree.length; i++) {
      _ref_frame += _tree[i].getKeySize();
    }
  }

  /**
   * Gets a traces iterator
   * @param trace_buffer trace buffer
   * @param offset trace buffer offset
   * @param ref_file reference file
   * @return new instance of iterator
   * @throws IOException
   */
  public Iterator iterator(MappedByteBuffer trace_buffer, int offset,
                           SeriesFileDescriptor ref_file) throws
      IOException {
    _ref_file = ref_file;
    return new CompoundKeyValueIterator(trace_buffer, offset);
  }

  private class CompoundKeyValueIterator
      implements Iterator {
    private CompoundKeyValue _v;
    private MappedByteBuffer _tracef;
    private int _prepos;

    public void remove() {};

    public CompoundKeyValueIterator(MappedByteBuffer trace_buffer, int prepos) throws
        IOException {
      _prepos = prepos;
      _filedesc.reset();
      _tracef = trace_buffer;
      _v = new CompoundKeyValue(keyProducer);
      readNext();
    }

    public boolean hasNext() {
      return _v != null;
    }

    public Object next() throws NoSuchElementException {
      if (!hasNext()) {
        throw new NoSuchElementException();
      }
      CompoundKeyValue val = _v;
      _v = new CompoundKeyValue(keyProducer);
      readNext();
      return val;
    }

    private void readNext() {
      try {
        _v.readKey(_filedesc.getDataInputStream());
        _tracef.position(_v.traceOffset() - _prepos);
        _v.readTracelist(_tracef);
      }
      catch (IOException e) {
        _v = null;
      }
    }
  }

  /**
   * Writes a b-tree page to index file
   * @param values array of page keys
   * @param num_values number of keys
   * @param value_offset offest of page
   * @param tracefolder_offset offset of trace folder for this page
   * @param ref_offset offset of reference folder for this page
   * @param index_file index file to write
   * @param _ref_buffer reference buffer
   * @return length of referenced traces
   * @throws IOException
   */
  public int writePage(CompoundKeyValue[] values, int num_values,
                       int value_offset, int tracefolder_offset, int ref_offset,
                       RandomAccessFile index_file, ByteBuffer _ref_buffer) throws
      IOException {
    int reflen = 0;

    for (int i = 0; i < num_values; i++) {
      values[i].prepareRefArray(_tree, _index);

    }

    for (int j = 0; j < _tree.length - 1; j++) {
      int tree_index = j < _index ? j : j + 1;
      for (int i = 0; i < num_values; i++) {
        values[i].loadRefKeys(_tree, tree_index, j, _ref_buffer);
      }
    }

    DataOutputStream os = _ref_file.getDataOutputStream();

    for (int i = 0; i < num_values; i++) {
      int node_offset = value_offset +
          i * (keyProducer.getByteLen() + 8 * _tree.length);
      reflen +=
          values[i].writePageNode(index_file, os, node_offset,
                                  ref_offset + reflen, tracefolder_offset,
                                  _tree, _index);
    }
    return reflen;
  }

  private int buffer_pos(int trace_id) {
    return trace_id * _ref_frame + _own_offset;
  }

  private int runCompact(SeriesFileDescriptor trace_folder,
                         SeriesFileDescriptor key_folder, int trace_offset,
                         int num_traces, ByteBuffer _ref_buffer) throws
      IOException {
    IndexKey key = (IndexKey) keyProducer.clone();
    IndexKey
        tmp = (IndexKey) keyProducer.clone();
    IndexKeyStep tmp_step = IndexKeyStep.createKeyStep(keyProducer.getType());
    _step = null;
    boolean ambient = true;
    int trace_id;

    DataInput is = _filedesc.getDataInputStream();
    DataOutputStream tos = trace_folder.getDataOutputStream();
    DataOutputStream kos = key_folder.getDataOutputStream();

    trace_id = is.readInt();
    key.setValue(is);
    _min = (IndexKey) key.clone();

    int trace_len = 1;
    int i = 0;

    try {
      for (i = 1; i < num_traces; i++) {

        tos.writeInt(trace_id);

        _ref_buffer.position(buffer_pos(trace_id));

        key.write(_ref_buffer);

        trace_id = is.readInt();
        tmp.setValue(is);

        if (tmp.compareTo(key) != 0) {
          if (ambient) {
            if (_step == null) {
              _step = IndexKeyStep.createKeyStep(keyProducer.getType());
              tmp.sub(key, _step);
            }
            else {
              tmp.sub(key, tmp_step);
              if (_step.compareTo(tmp_step) != 0) {
                ambient = false;
              }
            }
          }
          _num_keys++;
          key.write(kos);
          kos.writeInt(trace_offset);
          kos.writeInt(trace_len);
          trace_offset += trace_len * 4;
          trace_len = 1;
          key.setValue(tmp);
        }
        else {
          trace_len++;
        }
      }
    }
    catch (IOException e) {
      CharArrayWriter c = new CharArrayWriter();
      e.printStackTrace(new PrintWriter(c));
      c.write(" Additional information: \n");
      c.write(" Trace number =" + i + " Count of traces=" + num_traces + "\n");
      c.write(" TraceId" + trace_id + "\n");
      c.write(" Length =" + _filedesc.getLength() + "\n");
      c.write(" SeriesFileDescriptor =" + _filedesc.getFileName() + "\n");

      throw new IOException(c.toString());
    }

    _max = (IndexKey) key.clone();

    _num_keys++;
    tos.writeInt(trace_id);
    _ref_buffer.position(buffer_pos(trace_id));
    key.write(_ref_buffer);

    key.write(kos);
    kos.writeInt(trace_offset);
    kos.writeInt(trace_len);
    trace_offset += trace_len * 4;

    if (!ambient || _step == null) {
      _step = IndexKeyStep.createKeyStep(keyProducer.getType());
      //Comment next line out if you want IndexKeyStep to have a "zero" step value for irregular indexes.
      _step.setOne();
    }

    tos = null;
    return trace_offset;
  }

  /**
   *  Compacts a list of referenced traces for this index
   *
   * @param  trace_folder  file contains a list of traces to compact
   * @param  index_offset  proposed offset of referenced traces list in an index file
   * @param  num_traces    total number of traces
   * @param  _ref_buffer   reference buffer
   * @return               updated offset of referenced traces list in an index file
   * @throws  IOException  Description of the Exception
   */
  public int compact(SeriesFileDescriptor trace_folder, int index_offset,
                     int num_traces, ByteBuffer _ref_buffer) throws IOException {
    SeriesFileDescriptor fkey = new SeriesFileDescriptor();
    index_offset = runCompact(trace_folder, fkey, index_offset, num_traces,
                              _ref_buffer);
    trace_folder.flush();
    fkey.flush();

    _filedesc.delete();
    _filedesc = fkey;
    return index_offset;
  }

  /**
   *  Writes an index header
   *
   * @param  f <code>DataOutput</code> to write
   * @throws  IOException
   */
  public void writeHeader(DataOutput f) throws IOException {
    int len = _name.length();
    int pad = 32 - len;
    if (len > 32) {
      throw new IOException("Index name too long");
    }
    f.writeBytes(_name);
    for (int i = 0; i < pad; i++) {
      f.writeByte(0);

    }
    f.writeShort(_offset);
    f.writeShort(_format);
    f.writeInt(_num_keys);
    getMinKey().write(f);
    getMaxKey().write(f);
    getKeyStep().write(f);
  }

  /**
   * Gets the byte size of this index key
   * @return key size
   */
  public int getKeySize() {
    return keyProducer.getByteLen();
  }

  /**
   * Gets a total number of unique keys
   * @return number of keys
   * @throws IOException
   */
  public int getNumberOfKeys() throws IOException {
    return _num_keys;
  }

  /**
   * Gets a key format
   * @return key format
   */
  public short getFormat() {
    return _format;
  }

  /**
   * Sets a series file descriptor of this index
   * @param desc file descriptor
   */
  public void getFileDescriptor(SeriesFileDescriptor desc) {
    _filedesc = desc;
  }

  private int _own_offset = 0;

  /**
   * Creates a new instance of index tree
   * @param name name of index
   * @param offset offset of this index keys in a trace header
   * @param format key format
   * @param index ordinal index of this tree in an index file
   * @param tree array contains all indexes being builded
   * @throws IOException
   */
  public CompoundIndexTree(String name, int offset, String format, int index,
                           CompoundIndexTree[] tree) throws IOException {
    if (index != 0) {
      _own_offset = tree[index - 1]._own_offset + tree[index - 1].getKeySize();
    }
    _tree = tree;
    _index = index;
    _name = name;
    _offset = offset;
    _format = SegyConstantsParser.getFormat(format).shortValue();
    keyProducer = IndexKey.createKey(_format);
  }

  /**
   * Loads a key value from current trace
   * @param r SEGY reader positioned to a current trace
   * @param key key to load
   * @throws IOException
   */
  public void loadKey(SegyReader r, IndexKey key) throws IOException {
    key.setValue(r.getTraceHeader(), _offset);
  }

  /**
   * Loads a key value from provided trace
   * @param r SEGY reader
   * @param key key to load
   * @param traceId index of trace to load from
   * @throws IOException
   */
  public void loadKeyFromTrace(SegyReader r, IndexKey key, int traceId) throws
      IOException {
    r.seekTraceHeader(traceId);
    key.setValue(r.getTraceHeader(), _offset);
  }

  /**
   * Gets a minimal value of this index keys
   * @return minimal key
   */
  public IndexKey getMinKey() {
    return _min;
  }

  /**
   * Gets a step of this index keys
   * @return key step
   */
  public IndexKeyStep getKeyStep() {
    return _step;
  }

  /**
   * Gets a maximal value of this index keys
   * @return maximal key
   */
  public IndexKey getMaxKey() {
    return _max;
  }

  /**
   * Gets a name of index
   * @return index name
   */
  public String getName() {
    return _name;
  }

  /**
   * Clones this index tree
   * @return a clone of tree
   */
  public CompoundIndexTree duplicate() {
    return (CompoundIndexTree) clone();
  }

  /**
   * Deletes a series file of this index
   */
  public void delete() {
    try {
      if (_filedesc != null) {
        _filedesc.delete();
      }
      _filedesc = null;
      _tree = null;
    }
    catch (IOException e) {
      _filedesc = null;
    }
  }

  public Object clone() {
    CompoundIndexTree clone = null;
    try {
      clone = (CompoundIndexTree)super.clone();
    }
    catch (CloneNotSupportedException e) {
      e.printStackTrace();
    }
    return clone;
  }
}