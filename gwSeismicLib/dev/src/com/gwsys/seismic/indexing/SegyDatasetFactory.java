//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

import java.io.*;

import com.gwsys.seismic.indexing.internal.*;

/**
 * Dataset factory.  Instances of this class used to create a various kinds of datasets
 * <p>Copyright (c) 2003</p>
 * <p>Company: INT Inc.</p>
 * @version 1.0
 */
public class SegyDatasetFactory {
  private SegyDataFormat _format;

  private SegyDatasetFactory() {
    this(new DefaultSegyDataFormat());
  }

  private SegyDatasetFactory(SegyDataFormat format) {
    _format = format;
  }

  /**
   * Gets default SEGY dataset factory based on default SEGY format
   * @return default SEGY dataset factory
   */
  public static SegyDatasetFactory getDefaultFactory() {
    return new SegyDatasetFactory();
  }

  /**
   * Gets default SEGY dataset factory based on supplied SEGY format
   * @param format SEGY format being used
   * @return default SEGY dataset factory
   */
  public static SegyDatasetFactory getDefaultFactory(SegyDataFormat format) {
    return new SegyDatasetFactory(format);
  }

  /**
   * Creates segy data set from XML input stream.
   * The input stream must contain data from XGY file
   * @param xmlDocStream the input stream
   * @return the new segy data set
   * @throws IOException
   */
  public SegyDataset createDataset(InputStream xmlDocStream) throws
      IOException {
    return new SegyDatasetImpl(xmlDocStream, _format);
  }

  /**
   * Creates data set for standard segy files
   * @param basepath the base path
   * @param segy the array of the segy files
   * @param segyDataOrder the data order array
   * @return the new segy dataset
   * @throws IOException
   */
  public SegyDataset createDataset(String basepath, String[] segy,
                                   String[] segyDataOrder) throws IOException {
    return new SegyDatasetImpl(basepath, segy, segyDataOrder, _format);
  }

  /**
   * Creates data set for indexed segy with default cache size
   * @param filename the file name of the indexed segy
   * @return the new segy dataset
   * @throws IOException
   * @throws FileNotFoundException
   */
  public SegyDataset createDataset(String filename) throws IOException,
      FileNotFoundException {
    return new SegyDatasetImpl(filename, _format);
  }

  /**
   * Creates data set for indexed segy with the defined cache size
   * @param filename the file name
   * @param cacheRate size of page cache as a part of total number of pages (value in range [0.0 - 1.0]
   * @param cacheTreshold cache all pages if its number less on equal to this value
   * @return the new segy dataset
   * @throws IOException
   * @throws FileNotFoundException
   */
  public SegyDataset createDataset(String filename, double cacheRate,
                                   int cacheTreshold) throws IOException,
      FileNotFoundException {
    return new SegyDatasetImpl(filename, _format, cacheRate, cacheTreshold);
  }

}
