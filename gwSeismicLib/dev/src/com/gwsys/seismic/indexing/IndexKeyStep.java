//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

import java.io.*;

import com.gwsys.seismic.indexing.internal.*;
import com.gwsys.seismic.indexing.SeismicConstants;


/**
 * Represents the value of index key step
 * <p>Copyright (c) 2003</p>
 * <p>INT Inc.</p>
 * @version 1.0
 */
public abstract class IndexKeyStep
    implements Comparable, Cloneable {

  abstract public int compareTo(Object o) throws ClassCastException;

  /**
   * Reads the data from given byte array at specified offset and assigns the value to this instance of key step
   * @param data byte array to read data from
   * @param offset byte offset to read this key data
   */
  abstract public void setValue(byte[] data, int offset);

  /**
   * Parses the given string and assigns the value to this instance of key step
   * @param s string to parse
   */
  abstract public void setValue(String s);

  /**
   * Returns an instance of <code>Number</code> contains value of this key step.
   * @return Value of this key step as a Number
   */
  abstract public Number getValue();

  /**
   * Writes the value of this key step to given <code>DataOutput</code>
   * @param d DataOutput where the value will be written to
   * @throws IOException
   */
  abstract public void write(DataOutput d) throws IOException;

  /**
   * Returns size (in bytes) of this key step
   * @return step size
   */
  abstract public int getSize();

  /**
   * Assigns the 1 of an appropriate type to this step
   */
  abstract public void setOne();

  /**
   * Reads the value of this key step from given <code>DataInput</code>
   * @param f DataInput where the value will be read from
   * @throws IOException
   */
  abstract public void setValue(DataInput f) throws IOException;

  /**
   * Returns the result of division this step value by given step
   * @param s divider step
   * @return result of division
   * @throws ClassCastException
   */
  abstract public double div(IndexKeyStep s) throws ClassCastException;

  /**
   * Inverts the sign of this step
   */
  abstract public void changeSign();

  /**
   * Returns integer constant describing the sign of this step value
   * @return sign of this step
   */
  abstract public int getSign();

  /**
   * Returns true if this step has minimal non-zero modulus value of number of this step type (1 for integer steps, for example),
   * in other words, this step represents solid grid.
   * @return solidity flag
   */
  abstract public boolean isSolid();

  public Object clone() {
    try {
      return super.clone();
    }
    catch (CloneNotSupportedException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Creates key step of the specified type
   * @param keyType type of step
   * @return new instance of key step
   * @throws IOException
   */
  public static IndexKeyStep createKeyStep(int keyType) throws IOException {
    switch (keyType) {
      case SeismicConstants.DATA_FORMAT_4BIT:
        return new IndexKeyStep_ui4();
      case SeismicConstants.DATA_FORMAT_BYTE:
        return new IndexKeyStep_i8();
      case SeismicConstants.DATA_FORMAT_UNSIGNEDBYTE:
        return new IndexKeyStep_ui8();
      case SeismicConstants.DATA_FORMAT_SHORT:
        return new IndexKeyStep_i16();
      case SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT:
        return new IndexKeyStep_ui16();
      case SeismicConstants.DATA_FORMAT_INT:
        return new IndexKeyStep_i32();
      case SeismicConstants.DATA_FORMAT_UNSIGNEDINT:
        return new IndexKeyStep_ui32();
      case SeismicConstants.DATA_FORMAT_FLOAT:
        return new IndexKeyStep_r32();
      case SeismicConstants.DATA_FORMAT_DOUBLE:
        return new IndexKeyStep_r64();
      default:
        throw new IOException("Unknown type");
    }
  }
}
