//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

import java.util.*;

/**
 * <p>Title: Data source path</p>
 * <p>Description: Defines data source path for query</p>
 * <p>Copyright (c) 2003</p>
 * <p>Int Inc</p>
 * @version 1.0
 */
public class DataSourcePath {

  private DataSourceKey _key1;
  private DataSourceKey _key2;
  private int _count;
  private Point[] _points = null;
  private boolean _round = true;

  /**
   * Represents a single point in 2D index space
   * <p>Copyright (c) 2003</p>
   * <p>INT Inc.</p>
   * @version 1.0
   */
  public class Point {
    private Number _x = null, _y = null;

    /**
     * Gets first index value
     * @return first index value
     */
    public Number getX() {
      return _x;
    }

    /**
     * Sets first index value
     * @param x first index value
     */
    public void setX(Number x) {
      _x = x;
    }


    /**
     * Gets second index value
     * @return second index value
     */
    public Number getY() {
      return _y;
    }

    /**
     * Sets second index value
     * @param y second index value
     */
    public void setY(Number y) {
      _y = y;
    }
  }

  private class PointEnumerator
      implements Enumeration {
    private int _pos;
    protected PointEnumerator() {
      _pos = 0;
    }

    public boolean hasMoreElements() {
      return (_pos < _count);
    }

    public Object nextElement() {
      return _points[_pos++];
    }
  }

  private class SegmentEnumerator
      implements Enumeration {
    private int _pos = 0;
    private Point _segment[] = null;

    protected SegmentEnumerator() {
      _segment = new Point[2];
    }

    public boolean hasMoreElements() {
      return (_pos + 1 < _count);
    }

    public Object nextElement() {
      _segment[0] = _points[_pos++];
      _segment[1] = _points[_pos];
      return _segment;
    }
  }

  /**
   * Gets a points enumerator
   * @return points enumerator
   */
  public Enumeration enumeratePoints() {
    return new PointEnumerator();
  }

  /**
   * Gets a segments enumerator. Each segment is an array defined as Point[2]
   * @return segments enumerator
   */
  public Enumeration enumerateSegments() {
    return new SegmentEnumerator();
  }

  /**
   * Creates data source 2D path
   * @param key1 the fist dimension
   * @param key2  the second dimension
   * @param count the count sgments in the path
   */
  public DataSourcePath(DataSourceKey key1, DataSourceKey key2, int count) {

    _key1 = key1;
    _key2 = key2;
    _count = count;
    _points = new Point[count];
  }

  /**
   * Gets the first key
   * @return the fist dimension
   */
  public DataSourceKey getFirstKey() {
    return _key1;
  }

  /**
   * Gets the second key
   * @return the second dimension
   */
  public DataSourceKey getSecondKey() {
    return _key2;
  }

  /**
   * Gets x value of the point by index
   * @param index the index of the point
   * @return the X value
   */
  public Number getX(int index) {
    return _points[index].getX();
  }

  /**
   * Gets number of points in this path
   * @return number of points
   */
  public int getCount() {
    return _count;
  }

  /**
   * Gets y value of the point by index
   * @param index the index of the point
   * @return the X value
   */
  public Number getY(int index) {
    return _points[index].getY();
  }

  /**
   * Sets the point by index
   * @param index the index of the point
   * @param x value
   * @param y value
   */
  public void setPoint(int index, Number x, Number y) {
    if (_points[index] == null) {
      _points[index] = new Point();
    }
    _points[index].setX(x);
    _points[index].setY(y);
  }

  /**
   * Gets the rounding flag
   * @return rounding
   */
  public boolean getRound() {
    return _round;
  }

  /**
   * Sets the rounding flag
   * @param round the rounding flag
   */
  public void setRound(boolean round) {
    _round = round;
  }

  /**
   * Creates data source path
   * @param key1 the first key
   * @param key2  the second key
   * @param count the counf of the poins
   * @return the new data source path
   */
  public static DataSourcePath createDataSourcePath(DataSourceKey key1,
      DataSourceKey key2, int count) {
    return new DataSourcePath(key1, key2, count);
  }
}