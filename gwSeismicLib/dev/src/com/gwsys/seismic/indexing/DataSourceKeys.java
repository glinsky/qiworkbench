//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

import java.util.*;

/**
 * Instances of this class representing ordered set of datasource keys using in index queries
 * <p>Copyright (c) 2003</p>
 * <p>INT Inc.</p>
 * @version 1.0
 */
public class DataSourceKeys {
  private Vector _keys = null;

  /**
   * Creates an empty set of datasource keys
   */
  public DataSourceKeys() {
    _keys = new Vector();
  }

  /**
   * Creates new instance of datasource keys and reserving the room for supplied number of keys
   * @param num number of keys
   */
  public DataSourceKeys(int num) {
    _keys = new Vector(num);
  }

  /**
   * Adds the datasource key to the end of collection
   * @param key datasiurce key
   */
  public void addItem(DataSourceKey key) {
    _keys.addElement(key);
  }

  /**
   * Gets a current number of datasource keys in this collection
   * @return number of keys
   */
  public int getCount() {
    return _keys.size();
  }

  /**
   * Gets a datasource key by its index
   * @param index index of the key
   * @return datasource key
   */
  public DataSourceKey getItemByIndex(int index) {
    if (getCount() - 1 < index) {
      return null;
    }

    return (DataSourceKey) _keys.elementAt(index);
  }

  /**
   * Gets a datasource key by its name
   * @param name name of the key
   * @return datasource key
   */
  public DataSourceKey getItemByName(String name) {
    Iterator i = _keys.iterator();
    while (i.hasNext()) {
      DataSourceKey k = (DataSourceKey) i.next();
      if (k.getName().equalsIgnoreCase(name)) {
        return k;
      }
    }
    return null;
  }
}
