//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

import java.io.*;
import java.nio.*;

import com.gwsys.seismic.indexing.internal.*;
import com.gwsys.seismic.indexing.util.*;
import com.gwsys.seismic.indexing.SeismicConstants;

/**
 * Abstract base class for all types of index keys
 * <p>Copyright (c) 2003</p>
 * <p>INT Inc.</p>
 * @version 1.0
 */
public abstract class IndexKey
    implements Comparable, Cloneable {

  /**
   * Casting error message
   */
  protected static final String errcast = "Incompatible key types";

  /**
   * Add value of specified key step to the key
   * @param step step value to be added
   * @throws ClassCastException
   */
  abstract public void addStep(IndexKeyStep step) throws ClassCastException;

  /**
   * Assing the substraction of this key and specified key to specified key step
   * @param key index key to be substracted
   * @param step key step to store result of substraction
   * @throws ClassCastException
   */
  abstract public void sub(IndexKey key, IndexKeyStep step) throws
      ClassCastException;

  /**
   * Write the value of the key to specified output stream
   * @param f output stream to write
   * @return number of bytes written
   * @throws IOException
   */
  abstract public int write(DataOutput f) throws IOException;

  /**
   * Write the value of the key to specified buffer
   * @param f byte buffer to write
   * @return number of bytes written
   * @throws IOException
   */
  abstract public int write(ByteBuffer f) throws IOException;

  /**
   * Returns byte length of the key
   * @return byte length of the key
   */
  abstract public int getByteLen();

  /**
   * Multiply the key to specified value
   * @param v value to multiply
   */
  abstract public void mul(double v);

  /**
   * Returns absolute value of distance between this key and specified one.
   * @param k index key
   * @return absolute value of distance between this key and specified one
   * @throws ClassCastException
   */
  abstract public double getDistance(IndexKey k) throws ClassCastException;

  /**
   * Round key value to the specified step
   * @param step key step
   * @throws ClassCastException
   */
  abstract public void round(IndexKeyStep step) throws ClassCastException;

  /**
   * Assign a random value to the key
   */
  abstract public void setRandom();

  /**
   * Parse the string and assign parsed value to the key
   * @param s string to parse
   */
  abstract public void setValue(String s);

  /**
   * Copy the value from specified key
   * @param key key to copy
   * @throws ClassCastException
   */
  abstract public void setValue(IndexKey key) throws ClassCastException;

  /**
   * Read key value from the specified data input stream
   * @param f data input stream
   * @throws IOException
   */
  abstract public void setValue(DataInput f) throws IOException;

  /**
   * Read key value from specified buffer at specified offset
   * @param b buffer to read
   * @param offset buffer offset
   */
  abstract public void setValue(RestrictedByteBuffer b, int offset);

  /**
   * Read key value from specified buffer at specified offset
   * @param b buffer to read
   * @param offset buffer offset
   */
  abstract public void setValue(ByteBuffer b, int offset);

  /**
   * Return key value as an Object
   * @return key value
   */
  abstract public Object getValue();

  /**
   * Return true if key value is in grid specified by start, end and step values
   * @param start start of grid
   * @param end end of grid
   * @param step grid step
   * @return True if key value is in grid
   * @throws ClassCastException
   */
  abstract public boolean isInGrid(IndexKey start, IndexKey end,
                                   IndexKeyStep step) throws ClassCastException;

  /**
   * Snaps key value to the specified grid, closely before specified key
   * @param start start of grid
   * @param step grid step
   * @param key key to snap before
   * @throws ClassCastException
   */
  abstract public void snapToGrid(IndexKey start, IndexKeyStep step,
                                  IndexKey key) throws ClassCastException;

  abstract public int compareTo(Object ob) throws ClassCastException;

  /**
   * Same as <code>compareTo</code>, but comparsion key will be retrieved from given instance of <code>ByteBuffer</code> at specified <code>offset</code>
   * @param b buffer to retrieve comparsion key data
   * @param offset value of buffer offset
   * @return result of comparsion
   */
  abstract public int compareToBuffer(ByteBuffer b, int offset);

  /**
   * Return type of this key
   * @return type of key
   */
  abstract public short getType();

  /**
   * Returns <code>value</code> rounded by <code>step</code>
   * @param value value to round
   * @param step step value to be rounded to
   * @return rounded value
   */
  protected double round(double value, double step) {
    return step * Math.floor(value / step + .5);
  }

  public Object clone() {
    try {
      return super.clone();
    }
    catch (CloneNotSupportedException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Create a key of specified type
   * @param keyType key type
   * @return index key
   */
  public static IndexKey createKey(int keyType) {
    IndexKey k = null;
    switch (keyType) {
      case SeismicConstants.DATA_FORMAT_4BIT:
        k = new IndexKey_ui4();
        break;
      case SeismicConstants.DATA_FORMAT_BYTE:
        k = new IndexKey_i8();
        break;
      case SeismicConstants.DATA_FORMAT_UNSIGNEDBYTE:
        k = new IndexKey_ui8();
        break;
      case SeismicConstants.DATA_FORMAT_SHORT:
        k = new IndexKey_i16();
        break;
      case SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT:
        k = new IndexKey_ui16();
        break;
      case SeismicConstants.DATA_FORMAT_INT:
        k = new IndexKey_i32();
        break;
      case SeismicConstants.DATA_FORMAT_UNSIGNEDINT:
        k = new IndexKey_ui32();
        break;
      case SeismicConstants.DATA_FORMAT_FLOAT:
        k = new IndexKey_r32();
        break;
      case SeismicConstants.DATA_FORMAT_DOUBLE:
        k = new IndexKey_r64();
        break;
      default:
        return null;
    }
    return k;
  }
}
