//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

import java.io.*;
import java.nio.*;

import com.gwsys.seismic.indexing.util.*;

/**
 * Generic interface of SEGY readers.
 * @version 1.0
 */
public interface SegyReader {

  /**
   * Returns internal instance of <code>RestrictedByteBuffer</code> wrapping header of current trace.
   * @return instance of <code>RestrictedByteBuffer</code>
   * @see seekTraceHeader(int traceID)
   */
  public RestrictedByteBuffer getTraceHeader();

  /**
   * Returns new instance of <code>ByteBuffer</code> wrapping SEGY file header.
   * @return SEGY header
   * @throws IOException
   */
  public ByteBuffer getSegyHeader() throws IOException;

  /**
   * Delay recording time. Time in ms between initiation time of energy source and time when recording of data
   * samples begins (for deep water work if recording does not start at zero time)
   * @return delay recording time
   */
  public double getStartValue();

  /**
   * Closes underlaying SEGY file
   * @throws IOException
   */
  public void close() throws IOException;

  /**
   * Size, in bytes, of single trace (including trace header).
   * @return trace size
   */
  public int getTraceByteLen();

  /**
   * Number of samples per trace
   * @return number of samples
   */
  public int getNumSamplesPerTrace();

  /**
   * Number of traces in underlaying SEGY file
   * @return number of traces
   */
  public int getNumberOfTraces();

  /**
   * Fills <code>buffer</code> with raw samples data starting from <code>buffer_offset</code>,
   * with range of samples (<code>num_samples</code> samples starting at <code>start_sample</code> sample)
   * from range of traces (<code>num_traces</code> traces starting at <code>start_trace_id</code> trace)
   * @param buffer data buffer
   * @param buffer_offset buffer offset in bytes
   * @param start_trace_id start trace
   * @param num_traces number of traces
   * @param start_sample start sample
   * @param num_samples number of samples
   * @throws IOException
   */
  public void readTraceBulk(byte[] buffer, int buffer_offset,
                            int start_trace_id,
                            int num_traces, int start_sample, int num_samples) throws
      IOException;

  /**
   * Fills <code>buffer</code> with raw samples data starting from <code>buffer_offset</code>,
   * from range of traces (<code>num_traces</code> traces starting at <code>start_trace_id</code> trace)
   * @param buffer data buffer
   * @param buffer_offset buffer offset in bytes
   * @param start_trace_id start trace
   * @param num_traces number of traces
   * @throws IOException
   */
  public void readTraceBulk(byte[] buffer, int buffer_offset,
                            int start_trace_id,
                            int num_traces) throws IOException;

  /**
   * Positions reader to the given trace.
   * @param traceID trace id
   * @throws IOException
   * @see header()
   */
  public void seekTraceHeader(int traceID) throws IOException;

  /**
   * Returns byte order of underlaying SEGY file
   * @return byte order
   */
  public ByteOrder getByteOrder();

  /**
   * Returns size, in bytes, of single sample
   * @return sample size
   */
  public short getSampleSize();

  /**
   * Returns a constant defining binary type of samples in underlaying SEGY file
   * @return sample type
   */
  public short getSampleType();

  /**
   * Returns interval between samples
   * @return sample interval
   */
  public int getSampleInterval();

  /**
   * Returns constant defining units of samples in underlaying SEGY file
   * @return sample units
   */
  public int getSampleUnit();

  /**
   * Fills <code>buffer</code> starting at <code>buffer_offset</code> with samples of given trace
   * @param buffer data buffer
   * @param buffer_offset buffer offset
   * @param trace_id trace id
   * @throws IOException
   */
  public void readTraceSamples(byte[] buffer, int buffer_offset, int trace_id) throws
      IOException;

  /**
   * Fills <code>buffer</code> starting at <code>buffer_offset</code>
   * with range of samples (<code>num_samples</code> samples starting at <code>start_sample</code> sample) of given trace.
   * @param buffer data buffer
   * @param buffer_offset buffer offset
   * @param trace_id trace id
   * @param start_sample start sample
   * @param num_samples number of samples
   * @throws IOException
   */
  public void readTraceSamples(byte[] buffer, int buffer_offset, int trace_id,
                               int start_sample, int num_samples) throws
      IOException;

  /**
   * Gets SEGY data format using by this reader
   * @return SEGY data format using by this reader
   */
  public SegyDataFormat getSegyDataFormat();

  /**
   * Reset the byte order and read values again.
   * @param order ByteOrder
   */
  public void setByteOrder(ByteOrder order) throws IOException;
}
