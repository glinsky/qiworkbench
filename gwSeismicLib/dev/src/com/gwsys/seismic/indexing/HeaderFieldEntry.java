//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

/**
 * 
 * This class holds a definition of each header field in Segy.
 *
 */
public interface HeaderFieldEntry {
	/**
	 * This field is not specified.
	 */
	public static final int FIELD_TYPE_NOT_SPECIFIED = 0;
	/**
	 * This field is other than primary/secondary key.
	 */
	public static final int FIELD_TYPE_ANY = 1;
	/**
	 * This field is primary key.
	 */
	public static final int FIELD_TYPE_PRIMARY = 2;
	/**
	 * This field is Secondary key.
	 */
	public static final int FIELD_TYPE_SECONDARY = 3;
	/**
	 * Returns the name of this field.
	 * @return the name of this field.
	 */
	public String getName();
	/**
	 * Returns the id number of this field.
	 * @return the id number of this field.
	 */
	public int getFieldId();

	/**
	 * Returns the one of Constants defined in this interface.
	 * @return the one of Constants defined in this interface.
	 */
	public int getFieldType();
	
	/**
	 * Returns the value type of this field. Such as short, int and so on.
	 * @return the value type of this field. Such as short, int and so on.
	 */
	public int getValueType();
	
	/**
	 * Return the offset in byte array. (Start with zero).
	 * @return
	 */
	public int getByteOffset();

	/**
	 * Returns the state of field. It is used for user to store specific value.
	 * @return the state of field.
	 */
	public boolean getFieldState();
}
