//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

/**
 * This file defines the constants from the SU v37 release and Rev 1 SEG Y Standard.
 * Those standards are Copyright of related company or organzation.
 *
 */

public class SeismicConstants {
//	************DEFINE DATA FORMAT***********************************
	/** A <code>byte</code> data format (8-bit signed integer). */
	public static final int DATA_FORMAT_BYTE = 0x0001;

	/** A <code>short</code> data format (16-bit signed integer). */
	public static final int DATA_FORMAT_SHORT = 0x0002;

	/** An <code>int</code> data format (32-bit signed integer). */
	public static final int DATA_FORMAT_INT = 0x0004;

	/** A <code>float</code> data format (32-bit IEEE floating point). */
	public static final int DATA_FORMAT_FLOAT = 0x0010;

	/** An IBM floating point data format (32-bit). */
	public static final int DATA_FORMAT_IBM_FLOAT = 0x0020;

	/** A double floating point data format (64-bit IEEE floating point). */
	public static final int DATA_FORMAT_DOUBLE = 0x0040;

	/** A nibble data format (4-bit unsigned integer). */
	public static final int DATA_FORMAT_4BIT = 0x0100;

	/** An unsigned byte data format (8-bit unsigned integer). */
	public static final int DATA_FORMAT_UNSIGNEDBYTE = 0x0200;

	/** An unsigned short data format (16-bit unsigned integer). */
	public static final int DATA_FORMAT_UNSIGNEDSHORT = 0x0400;

	/** An unsigned long data format (32-bit unsigned integer). */
	public static final int DATA_FORMAT_UNSIGNEDINT = 0x0800;

	/** An unit in time */
	public static final int UNIT_TIME = 0;
	/** An unit in depth */
	public static final int UNIT_DEPTH_FEET = 1;
	/** An unit in depth. */
	public static final int UNIT_DEPTH_METERS = 2;
	/** An unit in trace number. */
	public static final int UNIT_TRACE_NUMBER = 3;

	//State of trace
	public static final int NORMAL_TRACE   = 0;

	/**Reversed trace */  
	public static final int REVERSED_TRACE = 1;

	/**Missing trace */
	public static final int NULL_TRACE   = 2;
}
