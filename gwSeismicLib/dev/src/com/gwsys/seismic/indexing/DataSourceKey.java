//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

/**
 * Instances of this class collects an information about a single datasource key
 * <p>Copyright (c) 2003</p>
 * <p>INT Inc.</p>
 * @version 1.0
 */
public class DataSourceKey {

  private String _name = null;
  private String _minValue = null;
  private String _maxValue = null;
  private String _increment = null;
  private SortOrder _order = SortOrder.UNSORTED;
  private short _type;
  private boolean _interpolate = true;

  /**
   * Defines a sorting order of key values
   * <p>Copyright (c) 2003</p>
   * <p>INT Inc.</p>
   * @version 1.0
   */
  public static final class SortOrder {
    private String _order = null;
    private SortOrder(String order) {
      _order = order;
    }

    /**
     * String representation of sort order
     * @return sort order
     */
    public final String toString() {
      return _order;
    }

    /**
     * Ascending sort order
     */
    public static final SortOrder ASCENDING = new SortOrder("ASCENDING");

    /**
     * Descending sort order
     */
    public static final SortOrder DESCENDING = new SortOrder("DESCENDING");

    /**
     * No order
     */
    public static final SortOrder UNSORTED = new SortOrder("UNSORTED");
  }

  /**
   * Gets name of index
   * @return name of index
   */
  public String getName() {
    return _name;
  }

  /**
   * Sets name of index
   * @param name name of index
   */
  public void setName(String name) {
    _name = name;
  }

  /**
   * Gets min value of key
   * @return min value of key
   */
  public String getMinValue() {
    return _minValue;
  }

  /**
   * Sets min value of key
   * @param minValue min value of key
   */
  public void setMinValue(String minValue) {
    _minValue = minValue;
  }

  /**
   * Gets max value of key
   * @return max value of key
   */
  public String getMaxValue() {
    return _maxValue;
  }

  /**
   * Sets max value of key
   * @param maxValue max value of key
   */
  public void setMaxValue(String maxValue) {
    _maxValue = maxValue;
  }

  /**
   * Gets key increment
   * @return key increment
   */
  public String getIncrement() {
    return _increment;
  }

  /**
   * Sets key increment
   * @param increment key increment
   */
  public void setIncrement(String increment) {
    _increment = increment;
  }

  /**
   * Gets key type
   * @return key type
   */
  public short getType() {
    return _type;
  }

  /**
   * Sets key type
   * @param type key type
   */
  public void setType(short type) {
    _type = type;
  }

  /**
   * Gets key values sorting order
   * @return key values sorting order
   */
  public SortOrder getSortOrder() {
    return _order;
  }

  /**
   * Sets key values sorting order
   * @param order key values sorting order
   */
  public void setSortOrder(SortOrder order) {
    _order = order;
  }

  /**
   * Gets interpolation mode
   * @return interpolation mode
   */
  public boolean getInterpolationMode() {
    return _interpolate;
  }

  /**
   * Sets interpolation mode
   * @param interpolate interpolation mode
   */
  public void setIinterpolatioMode(boolean interpolate) {
    _interpolate = interpolate;
  }
}
