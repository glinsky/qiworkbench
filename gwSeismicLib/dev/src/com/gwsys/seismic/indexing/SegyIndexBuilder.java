//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.seismic.indexing;

import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.util.*;
import javax.xml.parsers.*;

import org.w3c.dom.*;
import org.xml.sax.*;

import com.gwsys.seismic.indexing.internal.*;
import com.gwsys.seismic.indexing.util.*;
import com.gwsys.seismic.indexing.SeismicConstants;

/**
 * Builds B-tree based index file
 * 
 * @version 1.0
 */
public class SegyIndexBuilder implements SegyProcessingCallback {
	private final static int COMMON_HEADER_SIZE = 16;

	private final static int INDEX_HSIZE = 48;

	private final static short major_version = 1;

	private final static short minor_version = 1;

	private int page_size;

	private int trace_folder_offset;

	private int crossindex_folder_offset;

	private int current_crossindex_folder_offset;

	private int num_traces;

	private int current_index;

	private int[] index_offsets = null;

	private int[] nodes_per_page = null;

	private File trace_file = null;

	private File rfl_file = null;

	private File ref_file = null;

	private RandomAccessFile index_file = null;

	private RandomAccessFile ref_random = null;

	private FileChannel ref_ch = null;

	private MappedByteBuffer ref_buffer = null;

	private CompoundIndexTree[] indexes = null;

	private SegyProcessingOutputCallback _output_callback;

	private SeriesFileDescriptor _traced = null;

	private void writeHeaders() throws IOException {
		index_offsets = new int[indexes.length];
		nodes_per_page = new int[indexes.length];

		int ksize = 0;
		int num_indexes = indexes.length;
		for (int i = 0; i < indexes.length; i++) {
			ksize += indexes[i].getKeySize();
		}
		int header_size = COMMON_HEADER_SIZE + num_indexes * INDEX_HSIZE + 3
				* ksize + 4 * num_indexes + 4 * (_sample_format == 3 ? 2 : 4);
		header_size = roundByPage(header_size);

		// Common header
		writeCommonHeader(num_indexes);

		// Index headers
		int index_folder_offset = header_size;
		for (int i = 0; i < indexes.length; i++) {
			index_offsets[i] = index_folder_offset;
			indexes[i].writeHeader(index_file);
			nodes_per_page[i] = (page_size - 4)
					/ (indexes[i].getKeySize() + 8 * indexes.length);
			int num_pages = new Double(Math.ceil(((double) indexes[i]
					.getNumberOfKeys())
					/ ((double) nodes_per_page[i]))).intValue();
			index_file.writeInt(index_folder_offset);
			index_file.writeInt(num_pages);
			index_folder_offset += num_pages * page_size;
		}
		trace_folder_offset = index_folder_offset;
		crossindex_folder_offset = roundByPage(trace_folder_offset + 4
				* indexes.length * num_traces);
		current_crossindex_folder_offset = crossindex_folder_offset;
	}

	private void writeCommonHeader(int num_indexes) throws IOException {
		index_file.seek(0);
		index_file.writeInt(SegyIndexFile.MAGIC_NUMBER);
		index_file.writeShort(SegyIndexFile.MAJOR_VERSION);
		index_file.writeShort(SegyIndexFile.MINOR_VERSION);
		index_file.writeInt(page_size);
		index_file.writeByte(_sample_format);
		index_file.writeByte(_order == ByteOrder.BIG_ENDIAN ? 0 : 1);
		if (_sample_format == 3) {
			for (int i = 0; i < 4; i++) {
				index_file.writeShort(new Float(_values[i]).intValue());
			}
		} else {
			for (int i = 0; i < 4; i++) {
				index_file.writeFloat(_values[i]);
			}
		}
		index_file.writeShort(num_indexes);
		int index_headers_offset = COMMON_HEADER_SIZE + 4
				* (num_indexes + (_sample_format == 3 ? 2 : 4));
		for (int i = 0; i < num_indexes; i++) {
			index_file.writeInt(index_headers_offset);
			index_headers_offset += INDEX_HSIZE + 3 * indexes[i].getKeySize();
		}
	}

	private short _sample_format;

	private float[] _values = new float[4];

	private ByteOrder _order;

	private String _basePath = null, _index_file_name = null;

	private int _dataOrder = DataSliceType.DATA_CROSSECTION;

	protected void finalize() throws Throwable {
		clear();
		trace_file = null;
		_traced = null;
	}

	private IndexColBuilder _builder = null;

	private String _segyFormatFile = null;

	/**
	 * Parses SEGY format file and builds an array of corresponding <code>CompoundIndexTree</code> trees
	 * @param keys Desired keys
	 * @param segyFormatFile fully qualified name of SEGY format file
	 * @return array of <code>CompoundIndexTree</code> trees
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public static CompoundIndexTree[] parseSegyKeys(String[] keys,
			String segyFormatFile) throws IOException,
			ParserConfigurationException, SAXException {
		return IndexColBuilder.parseSegyKeys(keys, segyFormatFile);
	}

	private static int MAX_ITEMS = 500000;

	/**
	 * Gets size of indexing buffer in indexing items.
	 * Each item consist of trace number and set of corresponding key values.
	 * Bigger buffer allows faster processing.
	 * @return size of indexing buffer
	 */
	public static int getMaxItems() {
		return MAX_ITEMS;
	}

	/**
	 * Sets size of indexing buffer
	 * @param items number of items
	 */
	public static void setMaxItems(int items) {
		if (items > 1000 && items < 1000000)
			MAX_ITEMS = items;
	}

	/**
	 * Gets current temporary folder
	 * @return temporary folder
	 */
	public static String getTempFolder() {
		return SegyTempFile.getTempFolder();
	}

	/**
	 * Sets folder used to store temporary files during indexing
	 * @param folder temporary folder
	 */
	public static void setTempFolder(String folder) {
		SegyTempFile.setTempFolder(folder);
	}

	/**
	 * Gets an array of index trees
	 * @return array of index trees
	 */
	public CompoundIndexTree[] getIndexes() {
		return indexes;
	}

	/**
	 *
	 * @param segyFiles array of relative SEGY file names
	 * @param basePath base path
	 * @param order byte order of SEGYs being processed
	 * @param format optional custom SEGY data format implementation
	 * @param lkeys array of <code>CompoundIndexTree</code> trees,
	 * each describes one of keys you want to index SEGY by.
	 * @param segyFormatFile SEGY format file
	 * @param dataOrder SEGY data order
	 * @return total number of traces
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public int buildIndexCollection(String[] segyFiles, String basePath,
			ByteOrder order, SegyDataFormat format, CompoundIndexTree[] lkeys,
			String segyFormatFile, Integer dataOrder) throws IOException,
			ParserConfigurationException, SAXException {

		_segyFormatFile = segyFormatFile;

		if (dataOrder != null) {
			_dataOrder = dataOrder.intValue();
		}

		_builder = new IndexColBuilder();

		indexes = lkeys;
		_order = order;
		_basePath = basePath;

		num_traces = _builder.buildIndex(this, indexes, segyFiles, _order,
				_basePath, format);

		_values[0] = _builder.getMinValue();
		_values[1] = _builder.getMaxValue();
		_values[2] = _builder.getMeanValue();
		_values[3] = _builder.getRmsValue();

		_sample_format = _builder.getSampleFormat();
		return num_traces;
	}

	/**
	 * Gets MIN statistically calculated sample value
	 * @return MIN samples value
	 */
	public float getMinValue() {
		return _values[0];
	}

	/**
	 * Gets MAX statistically calculated sample value
	 * @return MAX samples value
	 */
	public float getMaxValue() {
		return _values[1];
	}

	/**
	 * Gets MEAN statistically calculated sample value
	 * @return MEAN samples value
	 */
	public float getMeanValue() {
		return _values[2];
	}

	/**
	 * Gets RMS statistically calculated sample value
	 * @return RMS samples value
	 */
	public float getRmsValue() {
		return _values[3];
	}

	/**
	 * Builds B-tree based index and writes it into a file
	 * @param indexFileName index file name
	 * @param pageSize size of B-tree page
	 * @throws IOException
	 */
	public void writeIndexFile(String indexFileName, int pageSize)
			throws IOException {
		page_size = pageSize;
		_index_file_name = indexFileName;
		buildIndex();
	}

	/**
	 * Builds an instance of XML config describing the built index .
	 * @param profileName name of the XML profile
	 * @param transposedKey transposed key
	 * @param transposedName transposed name
	 * @param transposedRate transposed rate
	 * @return XML config
	 * @throws ParserConfigurationException
	 */
	public Document buildXMLConfig(String profileName, String transposedKey,
			String transposedName, String transposedRate)
			throws ParserConfigurationException {

		DataSourceProfile profile = new DataSourceProfile(_segy_info.size(),
				indexes.length);
		profile.setBasePath(_basePath);

		File localFile = new File(_index_file_name);
		profile.setIndexFile(localFile.getName());

		localFile = null;
		profile.setFormat(_sample_format == 3 ? SeismicConstants.DATA_FORMAT_SHORT
				: SeismicConstants.DATA_FORMAT_FLOAT);

		profile.setSampleUnits(_sample_units);

		profile.setSampleInterval(_sample_interval);

		profile.setDataType(DataSliceType.DATA_SEISMIC);

		profile.setDataOrder(_dataOrder);

		if (transposedKey != null) {
			profile.setTransposedKey(transposedKey);
		}
		if (transposedName != null) {
			profile.setTransposedName(transposedName);

		}
		profile.setProfileName((profileName != null) ? profileName : "Segy");

		profile.setSegyFormatFile(_segyFormatFile);

		for (int i = 0; i < _segy_info.size(); i++) {
			DataSourceFile file = (DataSourceFile) _segy_info.elementAt(i);
			profile.getDatasourceFiles().addItem(file);
			profile.setNumberOfTraces(profile.getNumberOfTraces()
					+ file.getEndTrace() - file.getStartTrace() + 1);
		}

		for (int i = 0; i < indexes.length; i++) {
			DataSourceKey key = new DataSourceKey();
			key.setName(indexes[i].getName());
			key.setMinValue(indexes[i].getMinKey().getValue().toString());
			key.setMaxValue(indexes[i].getMaxKey().getValue().toString());
			key.setIncrement(indexes[i].getKeyStep().getValue().toString());
			if (transposedName != null && transposedKey != null
					&& transposedRate != null) {
				if (key.getName().equals(transposedName)) {
					key.setIncrement(transposedRate);
				}
			}
			key.setType(indexes[i].getFormat());
			profile.getDatasourceKeys().addItem(key);
		}
		return profile.getAsXMLDocument();
	}

	/**
	 * <code>SegyProcessingCallback</code> interface implementation.
	 * Usually you don't need to call this method
	 * @param msg message
	 */
	public void printMessage(String msg) {
		if (_output_callback != null) {
			_output_callback.addProcessingMessage(msg);
		}
	}

	private Vector _segy_info = new Vector();

	private int _sample_units, _sample_interval;

	/**
	 * Interface <code>SegyProcessingCallback</code> implementation method.
	 * Allows tracking of SEGY files processing. During undexing, framework calls this methid
	 * internally to track processing of SEGY files. Usually you don't need to call this method.
	 * @param name SEGY file name
	 * @param index ordinal index of the SEGY file
	 * @param start_trace start trace value of the SEGY file
	 * @param end_trace end trace value of the SEGY file
	 * @param sample_units sample units of the SEGY file
	 * @param sample_interval sample interval of the SEGY file
	 * @param order byte order of the SEGY file
	 */
	public void addSegy(String name, int index, int start_trace, int end_trace,
			int sample_units, int sample_interval, ByteOrder order) {
		_sample_units = sample_units;
		_sample_interval = sample_interval;
		DataSourceFile f = new DataSourceFile();
		f.setOrder(order);
		f.setFileName(name);
		f.setStartTrace(start_trace);
		f.setEndTrace(end_trace);
		f.setSegyOrdinal(index);
		_segy_info.add(f);
	}

	/**
	 * Clear btree builder
	 */
	public void clear() {
		try {

			indexes = null;

			try {
				if (ref_ch != null && ref_ch.isOpen()) {
					ref_ch.close();
					ref_ch = null;
				}

				if (ref_random != null) {
					ref_random.close();
				}
				ref_random = null;
			} catch (Exception e) {
				ref_random = null;
				ref_ch = null;
			}
			try {
				if (ref_file != null) {
					boolean b = ref_file.delete();
					if (b) {
						ref_file = null;
					}
				}
			} catch (Exception e) {
				ref_file = null;
			}

			try {
				if (rfl_file != null) {
					if (rfl_file.delete()) {
						rfl_file = null;
					}
				}
			} catch (Exception e) {
				rfl_file = null;
			}

			try {
				if (trace_file != null) {
					if (trace_file.delete()) {
						trace_file = null;
					}
				}
			} catch (Exception e) {
				trace_file = null;
			}

			try {
				if (_traced != null) {
					if (_traced.delete()) {
						_traced = null;
					}
				}
			} catch (Exception e) {
				_traced = null;
			}

			index_file = null;
		} catch (Throwable e) {
		}
	}

	/**
	 * Creates new instance of b-tree builder
	 * @param output_callback callback object
	 */
	public SegyIndexBuilder(SegyProcessingOutputCallback output_callback) {
		_output_callback = output_callback;
	}

	/**
	 * Builds B-tree compound index file from supplied index trees
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void buildIndex() throws FileNotFoundException, IOException {

		index_file = new RandomAccessFile(_index_file_name, "rw");

		index_file.setLength(0);
		int offset = 0;

		trace_file = SegyTempFile.createTempFile("trc", null);

		if (trace_file == null) {
			throw new IOException(
					"Cannot create temporary file!! Check disk space!!!");
		}

		_traced = new SeriesFileDescriptor(trace_file);

		rfl_file = SegyTempFile.createTempFile("rfl", null);

		if (rfl_file == null) {
			throw new IOException(
					"Cannot create temporary file!! Check disk space!!!");
		}

		SeriesFileDescriptor _ref = new SeriesFileDescriptor(rfl_file);

		loadRefTable();

		for (int i = 0; i < indexes.length; i++) {
			indexes[i].prepareCompact();
		}

		for (int i = 0; i < indexes.length; i++) {
			offset = indexes[i]
					.compact(_traced, offset, num_traces, ref_buffer);
		}

		writeHeaders();

		RandomAccessFile _tracer = new RandomAccessFile(trace_file, "r");
		FileChannel _trace_ch = _tracer.getChannel();

		MappedByteBuffer _trace_buf;

		int frame = num_traces * 4;

		for (int i = 0; i < indexes.length; i++) {
			_trace_buf = _trace_ch.map(FileChannel.MapMode.READ_ONLY,
					i * frame, frame);
			runIndex(i, _trace_buf, i * frame, _ref);
		}
		unloadRefTable();

		if (_ref != null) {
			_ref.close();

		}
		joinFolders(_trace_ch, frame);

		if (_ref != null) {
			_ref.delete();
			_ref = null;
		}

		try {

			if (rfl_file != null && rfl_file.delete()) {
				rfl_file = null;
			}

			if (_trace_ch.isOpen()) {
				_trace_ch.close();
			}
			_trace_ch = null;

			if (_tracer != null) {
				_tracer.close();
				_tracer = null;
			}

			if (trace_file.delete()) {
				trace_file = null;
			}
		} catch (java.io.IOException e) {

		}
		if (_traced != null) {
			if (_traced.delete()) {
				_traced = null;
			}
		}
	}

	private void loadRefTable() throws IOException {
		if (ref_file == null) {
			ref_file = SegyTempFile.createTempFile("ref", null);
			ref_random = new RandomAccessFile(ref_file, "rw");
			ref_ch = ref_random.getChannel();
		}
		int len = 0;
		for (int i = 0; i < indexes.length; i++) {
			len += indexes[i].getKeySize();
		}
		ref_buffer = ref_ch.map(FileChannel.MapMode.READ_WRITE, 0, num_traces
				* len);
	}

	private void unloadRefTable() throws IOException {
		boolean readonly = ref_buffer.isReadOnly();
		ref_buffer = null;
		if (!readonly) {
			ref_ch.force(false);
		}
		System.gc();
	}

	private void joinFolders(FileChannel trace_ch, int frame)
			throws IOException {

		MappedByteBuffer src, dst;
		FileChannel index_ch = index_file.getChannel();
		for (int i = 0; i < indexes.length; i++) {
			src = trace_ch.map(FileChannel.MapMode.READ_ONLY, i * frame, frame);
			dst = index_ch.map(FileChannel.MapMode.READ_WRITE,
					trace_folder_offset + i * frame, frame);
			dst.put(src);
		}
		RandomAccessFile rfl = new RandomAccessFile(rfl_file, "r");

		long len = rfl.length(), wrote = 0;

		while (wrote < len) {

			long write = frame > (len - wrote) ? (len - wrote) : frame;

			src = rfl.getChannel().map(FileChannel.MapMode.READ_ONLY, wrote,
					write);
			dst = index_ch.map(FileChannel.MapMode.READ_WRITE,
					crossindex_folder_offset + wrote, write);
			dst.put(src);
			wrote += write;
		}
		rfl.close();
		rfl = null;
	}

	private void runIndex(int n, MappedByteBuffer trace_buffer, int prepos,
			SeriesFileDescriptor lref_file) throws IOException {
		current_index = n;
		int numKeys = indexes[n].getNumberOfKeys();
		BTreeLevel head = new BTreeLevel();
		BTreeLevel tail = head.populateTreeLevel(null, nodes_per_page[n], 0, 0,
				numKeys);
		Iterator iter = indexes[n].iterator(trace_buffer, prepos, lref_file);
		while (iter.hasNext()) {
			tail.addItem((CompoundKeyValue) iter.next());
		}
		lref_file = null;
	}

	private void flushPage(int page, CompoundKeyValue[] values, int nodes,
			int links) throws IOException {
		int page_offset = index_offsets[current_index] + page_size * page;
		index_file.seek(page_offset);
		index_file.writeShort(nodes);
		index_file.writeShort(links);
		current_crossindex_folder_offset += indexes[current_index].writePage(
				values, nodes, page_offset + 4, trace_folder_offset,
				current_crossindex_folder_offset, index_file, ref_buffer);
	}

	private int roundByPage(int val) {
		double d1 = val, d2 = page_size;
		return new Double(Math.ceil(d1 / d2) * d2).intValue();
	}

	private class BTreeLevel {
		private int _page_nodes_filled = 0;

		private int _nodes_per_page = 0;

		private int _next_level_links = 0;

		private int _pages_filled = 0;

		private long _remain_nodes = 0;

		private boolean _page_filled = false;

		private BTreeLevel _up_level = null;

		private int _mode;

		private CompoundKeyValue[] _values;

		public BTreeLevel populateTreeLevel(BTreeLevel up_level,
				int nodes_per_page, int prev_level_nodes, int total_prev_nodes,
				int nodes_required) {
			_up_level = up_level;
			_nodes_per_page = nodes_per_page;
			int pages = prev_level_nodes + 1;
			_pages_filled = total_prev_nodes / nodes_per_page;
			int level_nodes = pages * nodes_per_page;
			int total_nodes = total_prev_nodes + level_nodes;
			_values = new CompoundKeyValue[nodes_per_page];
			BTreeLevel level;
			if (total_nodes >= nodes_required) {
				if (total_nodes > nodes_required) {
					level_nodes = nodes_required - total_prev_nodes;
				}
				level = this;
				_next_level_links = 0;
			} else {
				level = new BTreeLevel().populateTreeLevel(this,
						nodes_per_page, level_nodes, total_nodes,
						nodes_required);
				double d1 = level._remain_nodes, d2 = nodes_per_page;
				_next_level_links = (int) Math.ceil(d1 / d2);
			}
			_remain_nodes = level_nodes;
			return level;
		}

		public void addItem(CompoundKeyValue value) throws IOException {
			if (_remain_nodes == 0) {
				_up_level.addItem(value);
				return;
			} else if (_page_filled) {
				_page_filled = false;
				_up_level.addItem(value);
				return;
			}

			_values[_page_nodes_filled++] = value;
			_remain_nodes--;

			if (_page_nodes_filled == _nodes_per_page || _remain_nodes == 0) {
				int links;
				if (_next_level_links > _nodes_per_page + 1) {
					links = _nodes_per_page + 1;
					_next_level_links -= links;
				} else if (_next_level_links > 0) {
					links = _next_level_links;
					_next_level_links = 0;
				} else {
					links = 0;
				}
				flushPage(_pages_filled, _values, _page_nodes_filled, links);
				_page_nodes_filled = 0;
				_page_filled = true;
				_pages_filled++;
			}
		}
	}
}
