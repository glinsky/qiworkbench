//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.indexing;

/**
 * The implementation of HeaderFieldEntry descs the segy binary field.
 *
 */
public class SegyBinaryEntry implements HeaderFieldEntry {

	// Member Variables
	/** byteOffset, valuetype, field id, field type*/
	private int byteOffset, valueType, fieldId, fieldType;

	/** Name of the field */
	private String fieldName;

	private boolean state;
	/**
	 * Constructes a Entry for Segy Binary Header.
	 * @param offset The byte order in its header.
	 * @param value_type The value type in its header.
	 * @param field_id The constant from SeismicConstants.
	 * @param field_name The name of the field in the header.
	 * @param field_Type The constant defined in this interface.
	 * @param fieldState User specific value. 
	 */
	public SegyBinaryEntry(int offset, int value_type, int field_id, String field_name,
			int field_Type, boolean fieldState) {
		byteOffset = offset;
		valueType = value_type;
		fieldName = field_name;
		fieldType = field_Type;
		fieldId = field_id;
		state = fieldState;
	}

	/**
	 * @inheritDoc
	 */
	public String getName() {
		return fieldName;
	}
	/**
	 * @inheritDoc
	 */
	public int getFieldId() {
		return fieldId;
	}
	/**
	 * @inheritDoc
	 */
	public int getByteOffset() {
		return byteOffset;
	}
	/**
	 * @inheritDoc
	 */
	public int getValueType() {
		return valueType;
	}
	/**
	 * @inheritDoc
	 */
	public int getFieldType() {
		return fieldType;
	}

	/**
	 * @inheritDoc
	 */
	public boolean getFieldState() {
		return state;
	}
}
