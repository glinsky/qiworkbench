//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.io;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import com.gwsys.seismic.indexing.SeismicConstants;

/**
 * Defines the meta data for seismic.
 * It stores the value from header and calculated values.
 *
 */
public class SeismicMetaData {
	/**
  	 * the array of strings ordered by segy standard.
  	 */
	public static String[] formatStrings=new String[]{"32-bit IBM float", "32-bit integer",
	          				"16-bit integer", "32-bit integer with gain",
	          				"32-bit IEEE float"};

	static Hashtable cloneHeaderValues(Iterator itMapEntry) {
		Hashtable<Integer, Number>  clone = new Hashtable<Integer, Number>();

		//create each set again
		while (itMapEntry.hasNext()) {
			Map.Entry entry = (Map.Entry) itMapEntry.next();

			Number number = 0;

			if (entry.getValue() instanceof Float)
				number = new Float(((Float) entry.getValue()).floatValue());
			else if (entry.getValue() instanceof Double)
				number = new Double(((Double) entry.getValue()).doubleValue());
			else if (entry.getValue() instanceof Integer)
				number = new Integer(((Integer) entry.getValue()).intValue());
			else if (entry.getValue() instanceof Long)
				number = new Long(((Long) entry.getValue()).longValue());

			clone.put(new Integer(((Integer) entry.getKey()).intValue()), number);

		}
		return clone;
	}

	/**
	 * Flag to identify if all traces have the same number of samples.
	 */
	private boolean _constantSamplesPerTrace;

	/**
	 * A Map to store header values.
	 */
	private Map _headerValues = new Hashtable();

	/**
	 * the number of traces in this dataset.
	 */
	private int _numberOfTraces = 0;

	/**
	 * the sample rate, the default value is set at <code>0.004 ms</code>.
	 */
	private double _sampleRate = 0.004;

	/**
	 * the number of samples per trace
	 */
	private int _samplesPerTrace = 0;

	/**
	 * the start sample value in sample units.
	 */
	private double _sampleStart;

	/**
	 * the sample rate units, time or depth.
	 */
	private int _sampleUnits;

	/**
	 * The start value for time or depth
	 */
	private double _startValue;

	/**
	 * Calculated value for the samples in the dataset.
	 */
	private double amplitudeMin, amplitudeMax, average, rootMeanSquared;

	/**
	 * The data sample format as defined by the SEGY standard.
	 * The standard values are as follows:
	 * 1 - floating point (4 bytes)
	 * 2 - fixed point (4 bytes)
	 * 3 - fixed point (2 bytes)
	 * 4 - fixed point w/gain control (4 bytes) (not currently supported)
	 * 5 - IEEE 4-byte format
	 * 6 - fixed point (1 byte)
	 * The defalut value is set at <code>4 bytes floating point</code>.
	 */
	private int sampleFormat = 1;
	/**
	 * Constructs a seismic metadata object.
	 */
	public SeismicMetaData() {
		amplitudeMin = Float.MAX_VALUE;
		amplitudeMax = Float.MIN_VALUE;
		average = -1;
		rootMeanSquared = -1;

		_constantSamplesPerTrace = true;

		_sampleUnits = SeismicConstants.UNIT_TIME;
		_sampleRate = 0.004; // milliseconds
		_sampleStart = 0;
		_startValue = 0;
		sampleFormat = 1;
	}

	/**
	 * Constructor.
	 * @param meta MetaData
	 */
	public SeismicMetaData(SeismicMetaData meta) {

		amplitudeMin = meta.amplitudeMin;
		amplitudeMax = meta.amplitudeMax;
		average = meta.average;
		rootMeanSquared = meta.rootMeanSquared;
		_constantSamplesPerTrace = meta._constantSamplesPerTrace;
		_samplesPerTrace = meta._samplesPerTrace;
		_numberOfTraces = meta._numberOfTraces;
		_sampleUnits = meta._sampleUnits;
		_sampleRate = meta._sampleRate;
		sampleFormat = meta.sampleFormat;
		_sampleStart = meta._sampleStart;
		_startValue = meta._startValue;
		_headerValues = cloneHeaderValues(meta._headerValues.entrySet().iterator());
	}

	/**
	 * Gets the average for the absolute sample values in this dataset.
	 *
	 * @return	the average for absolute sample values in this dataset
	 *  or -1 by default.
	 */
	public double getAverage() {
		return average;
	}

	/**
	 * Gets the trace sample data format.
	 *
	 * @return the value defined by the SEGY standard.
	 */
	public int getDataFormat() {
		return sampleFormat;
	}

	/**
	 * Gets the trace sample data format as readable string.
	 * The standard values are as follows:
	 * 1 - floating point (4 bytes)
	 * 2 - fixed point (4 bytes)
	 * 3 - fixed point (2 bytes)
	 * 4 - fixed point w/gain control (4 bytes) (not currently supported)
	 * 5 - IEEE 4-byte format
	 * 8 - fixed point (1 byte)
	 * @return the string defined by the SEGY standard.
	 */
	public String getSampleFormatAsString() {
		String format = "Unknown";
		switch( sampleFormat ){
		case 1:
			format = formatStrings[0];
			break;
		case 2:
			format = formatStrings[1];
			break;
		case 3:
			format = formatStrings[2];
			break;
		case 4:
			format = formatStrings[3];
			break;
		case 5:
			format = formatStrings[4];
			break;
		case 8:
			format = "8-bit integer";
			break;
		}
		return format;
	}

	/**
	 * Returns the array of strings ordered by segy standard.
	 * @return the array of strings.
	 */
	public static String[] getFormatStrings(){
		return new String[]{"32-bit IBM float", "32-bit integer",
				"16-bit integer", "32-bit integer with gain",
				"32-bit IEEE float"};
	}

	/**
	 * Retrieves the Map used to store values.
	 * @return Map containing binary header values.
	 */
	public Map getHeaderValues() {
		return _headerValues;
	}

	/**
	 * Retrieves the maximum amplitude value for the dataset.
	 *
	 * @return	the maximum amplitude for the dataset or Float.MIN_VALUE.
	 */
	public double getMaximumAmplitude() {
		return amplitudeMax;
	}

	/**
	 * Retrieves the minimum amplitude value for the dataset.
	 *
	 * @return	the minimum amplitude for the dataset or Float.MAX_VALUE.
	 */
	public double getMinimumAmplitude() {
		return amplitudeMin;
	}

	/**
	 * Retrieves the total number of traces in this dataset.
	 *
	 * @return the number of traces in this dataset
	 */
	public int getNumberOfTraces() {
		return _numberOfTraces;
	}

	/**
	 * Retrieves the dataset RMS value.
	 *
	 * @return	the dataset RMS value or -1.
	 */
	public double getRMS() {
		return rootMeanSquared;
	}

	/**
	 * Gets the sample rate for the seismic data.
	 *
	 * @return the sample rate.
	 */
	public double getSampleRate() {
		return _sampleRate;
	}

	/**
	 * Gets the (max) number of samples in each trace.
	 *
	 * @return the max number of samples in trace.
	 *
	 */
	public int getSamplesPerTrace() {
		return _samplesPerTrace;
	}

	/**
	 * Retrieves the start sample for each trace.
	 *
	 * @return the start sample for each trace
	 *
	 */
	public double getSampleStart() {
		return _sampleStart;
	}

	/**
	 * Obtains the units used along the sample values.
	 *
	 * @return the sample units; such as:
	 * DEPTH_FEET, TIME_SECOND, DEPTH_METERS
	 */
	public int getSampleUnits() {
		return _sampleUnits;
	}

	/**
	 * Retrieves the start value (time or depth) for dataset..</p>
	 *
	 * @return the start value (time or depth) for dataset
	 */
	public double getStartValue() {
		return _startValue;
	}

	/**
	 * Indicates if the number of samples per trace is constant for the dataset.
	 *
	 * @return	<code>true</code> for a constant number of samples per trace;
	 *         	<code>false</code> otherwise
	 */
	public boolean isConstantSamplesPerTrace() {
		return _constantSamplesPerTrace;
	}

	/**
	 * Tests whether the statistical values have been collected (RMS, average,
	 * minimum and maximum amplitudes).
	 *
	 * @return 	<code>true</code> if statistics have been collected;
	 *			<code>false</code> otherwise
	 */
	protected boolean isStatisticsCollected() {

		if (amplitudeMax == java.lang.Float.MIN_VALUE)
			return false;
		else
			return true;
	}

	/**
	 * Determines whether the number of samples per trace is constant for the dataset.
	 *
	 * @param constantSamplesPerTrace	<code>true</code> for a constant number
	 *									of samples per trace; <code>false</code> otherwise
	 */
	public void setConstantSamplesPerTrace(boolean constantSamplesPerTrace) {
		_constantSamplesPerTrace = constantSamplesPerTrace;
	}

	/**
	 * Sets the trace sample data format.</p>
	 *
	 * @param dataFormat the data format as defined by the SEGY standard.
	 */
	public void setDataFormat(int dataFormat) {
		sampleFormat = dataFormat;
	}

	/**
	 * Sets data statistics (minimum and maximum amplitude values, average, RMS)
	 * for this dataset.</p>
	 *
	 * @param min minimum amplitude
	 * @param max maximum amplitude
	 * @param avg average amplitude
	 * @param rms root mean square
	 */
	public void setDataStatistics(double min, double max, double avg, double rms) {
		amplitudeMin = min;
		amplitudeMax = max;
		average = avg;
		rootMeanSquared = rms;
	}

	/**
	 * Determines the total number of traces in this dataset.</p>
	 *
	 * @param numberOfTraces	the number of traces in this dataset
	 */
	public void setNumberOfTraces(int numberOfTraces) {
		_numberOfTraces = numberOfTraces;
	}

	/**
	 * Sets the sample rate for the seismic data.
	 *
	 * @param sampleRate	the sample rate
	 */
	public void setSampleRate(double sampleRate) {
		_sampleRate = sampleRate;
	}

	/**
	 * Sets the number of samples in each trace. If the number of samples
	 * is not constant for the dataset, then this value should be set to
	 * the number of samples in the largest trace in the dataset.</p>
	 *
	 * @param samplesPerTrace	the number of samples in each trace
	 */
	public void setSamplesPerTrace(int samplesPerTrace) {
		_samplesPerTrace = samplesPerTrace;
	}

	/**
	 * Sets the start sample for each trace.
	 *
	 * @param sampleStart	the start sample for each trace
	 *
	 */
	public void setSampleStart(double sampleStart) {
		_sampleStart = sampleStart;
	}

	/**
	 * Defines the units used along the sample values.
	 *
	 * @param sampleUnits	the sample units; such as:
	 * DEPTH_FEET, TIME_SECOND, DEPTH_METERS
	 */
	public void setSampleUnits(int sampleUnits) {
		_sampleUnits = sampleUnits;
	}

	/**
	 * Sets the start value (time or depth) for dataset.</p>
	 *
	 * @param startValue	the start value (time or depth) for dataset
	 */
	public void setStartValue(double startValue) {
		_startValue = startValue;
	}

}
