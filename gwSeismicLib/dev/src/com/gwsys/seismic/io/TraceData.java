//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.io;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.Arrays;

import com.gwsys.util.ConvertionException;

/**
 * Defines a class to represent the seismic trace.
 * That include header data and sample data.
 * The API is based on DownUnderGEO and G&S Indexing SEGY.
 * 
 * This class is the base class for a trace it holds a trace in the
 * Java standard big endian format.
 * 
 * A trace is composed of a header block and trace data in the same 
 * fashion as the SEG-Y/SU format. 
 */
public class TraceData {

	public static final double DEFAULT_NULL = -999.25;
	/**
	 * Array is used to store trace data samples.
	 */
	private float[] samples;

	/**
	 * Object is related to this samples.
	 */
	private Object header, traceKey;

	private boolean instanceIsInitialised;
	private int numAppliedSamples = -1;
	private ByteBuffer traceData;
	
	/**
	 * 
	 */
	public TraceData() {
	}

	/**
	 * 
	 */
	public TraceData(float[] samples, Object header) {
		this.samples = samples;
		this.header = header;
	}

	public void setHeader(Object header) {
		this.header = header;
	}

	public Object getHeader() {
		return header;
	}

	/**
	 * Make a Reference of this array to save memeory.
	 * @param samples
	 */
	public void setSamples(float[] samples) {
		this.samples = samples;
	}

	public float[] getSamples() {
		return samples;
	}
	
	public int getMemoryBytes(){
		return (4 * samples.length) + 12;
	}

	public final boolean isInitialised() {
		return instanceIsInitialised;
	}
	
	/**
	 * Swaps the endianness of a traces (sample) data 
	 * @throws ConvertionException
	 * @param nSamples Input - the number of samples to swap - must be less than the data capacity
	 * of the trace 
	 */
	public final void swapDataEndianess(int nSamples)
			throws ConvertionException {
		int nDataBytes = nSamples * 4;
		if (nSamples > traceData.capacity()) {
			throw new ConvertionException(
					"Trace.swapDataEndianess - error - header number of samples to swap is "
							+ nSamples + " but trace samples capacity is "
							+ traceData.capacity() / 4);
		}
		for (int i = 0; i < nDataBytes; i += 4) {
			byteSwap4(i, traceData);
		}
	}
	
	/* 
	 * @see com.downundergeo.pub.seismic.TraceInterface#getSamplesCapacity()
	 */
	public final int getSamplesCapacity() {
		return traceData.capacity() / 4;
	}

	/* 
	 * @see com.downundergeo.pub.seismic.TraceInterface#setSamplesCapacity(int)
	 */
	public final void setSamplesCapacity(int sampleCapacity) {
		traceData = ByteBuffer.allocate(sampleCapacity * 4);
	}
	
	/* 
	 * @see com.downundergeo.pub.seismic.TraceInterface#getSampleValue(int)
	 */
	public final float getSampleValue(int i) throws ConvertionException {
		return traceData.getFloat(i * 4);
	}

	/* 
	 * @see com.downundergeo.pub.seismic.TraceInterface#getSampleValues(int, int, float[])
	 */
	public final float[] getSampleValues(int first, int last, float[] outArray)
			throws ConvertionException {
		int len = last - first + 1;
		if (outArray.length < len) {
			throw new ConvertionException(
					"Trace.getSampleValues - error - the output array has length "
							+ outArray.length + " but it is required to be "
							+ len + " for transfer of samples " + first
							+ " to " + last);
		}

		int byteOffset = first * 4;
		for (int indx = 0; indx < len; ++indx, byteOffset += 4) {
			outArray[indx] = traceData.getFloat(byteOffset);
		}
		return outArray;
	}
	
	/* 
	 * @see com.downundergeo.pub.seismic.TraceInterface#setSampleValues(int, int, float)
	 */
	public final void setSampleValues(int startIndex, int endIndex, float value)
			throws ConvertionException {
		float[] values = new float[endIndex - startIndex + 1];
		Arrays.fill(values, value);

		FloatBuffer floatBuffer = traceData.asFloatBuffer();
		floatBuffer.position(startIndex);
		floatBuffer.put(values);
	}
	
	/* 
	 * @see com.downundergeo.pub.seismic.TraceInterface#setSampleValues(int, int, float[])
	 */
	public final void setSampleValues(int first, int last, float inArray[])
			throws ConvertionException {
		int nSamples = last - first + 1;
		if (inArray.length < nSamples) {
			throw new ConvertionException(
					"Trace.setSampleValues - error - input array has dimension "
							+ inArray.length
							+ " which is smaller than the range of samples to set ["
							+ first + ", " + last + "]");
		}
		int byteOffset = first * 4;
		for (int indx = 0; indx < nSamples; ++indx, byteOffset += 4) {
			traceData.putFloat(byteOffset, inArray[indx]);
		}
	}
	
	/**
	 * Get the square of the RMS of the trace over the [beginSample, endSample) sample range
	 * @param first Input - the first sample number to set values for 
	 * @param last Input - the last sample number to set values for 
	 * @return The required RMS value
	 */
	public final double getRMS(int beginSample, int endSample)
			throws ConvertionException {
		if (endSample - beginSample < 0) {
			return 0;
		}
		double rms = 0.0;
		double val;
		for (int i = beginSample; i <= endSample; ++i) {
			val = getSampleValue(i);
			rms += val * val;
		}
		return rms / (endSample - beginSample + 1);
	}
	
	/**
	 * Get the maximum absolute value of the trace over the [beginSample, endSample) sample range
	 * @param first Input - the first sample number to set values for 
	 * @param last Input - the last sample number to set values for 
	 * @return The required RMS value
	 */
	public final double getMaxAbVal(int beginSample, int endSample)
			throws ConvertionException {
		float maxAV = 0;
		for (int i = beginSample; i <= endSample; ++i) {
			maxAV = Math.max(maxAV, Math.abs(getSampleValue(i)));
		}
		return maxAV;
	}
	
	/**
	 * Performs an endian conversion on the 4 bytes offset,...,offset+3
	 * @param offset Input - the offset at which to perform the endian conversion
	 * @param bArray Input/Output - The bytes offset,...,offset+3 of this argument
	 * are byte swapped
	 */
	protected final void byteSwap4(int offset, ByteBuffer bArray) {
		// Swap 4 bytes
		byte b = bArray.get(offset);
		bArray.put(offset, bArray.get(offset + 3));
		bArray.put(offset + 3, b);
		b = bArray.get(offset + 1);
		bArray.put(offset + 1, bArray.get(offset + 2));
		bArray.put(offset + 2, b);
	}
	
	public int getSampleInterval() throws ConvertionException{
		return 0;
	}

	/**
	 * Gets the number of effective samples.
	 * Maybe less than or equal to length of samples array.
	 * @return the number of effective samples.
	 */
	public int getNumAppliedSamples() {
		return numAppliedSamples;
	}

	/**
	 * Sets the number of effective samples.
	 *
	 * @param  number  the number of effective samples.
	 */
	public void setNumAppliedSamples(int number) {

		numAppliedSamples = number;

		if (samples == null)
			samples = new float[number];
		else if (number > samples.length)
			samples = new float[number];
		else{
			for (int i = number; i < samples.length; i++)
				samples[i] = 0;
		}
	}

	/**
	 * Retrieves a unique key associated with this trace.
	 * @return  a unique key associated with this trace.
	 */
	public Object getUniqueKey() {
		return traceKey;
	}

	/**
	 * Sets the ID with Object.
	 * @param  key the key to identify the trace.
	 */
	public void setUniqueKey(Object key) {
		traceKey = key;
	}

}
