//
// gwSeismicLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.seismic.io;

import com.gwsys.seismic.util.KeyedObject;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Store header information of each trace.
 *
 */
public class TraceHeader implements KeyedObject {

	/**
	 * the trace id in the trace header, the number of samples, status.
	 */
	private int traceId, numSamples, traceStatus;

	/**
	 * the start value, location, Root Mean Square,the average absolute value
	 */
	private double startSample, location, rootms = -1, average = -1;

	/** the maximum/minimum sample value for this trace */
	private float maxAmplitude ,	minAmplitude;

	/** the calculated parameters are set or not.*/
	private boolean valueSet = false;

	/** A hashtable stored the header values defined for this trace. */
	private Hashtable headerValues = null;

	/** A hashtable stored the all header values. */
	private ArrayList fullheaderList = new ArrayList();
	/**
	 * Constructs a new trace metadata object.
	 *
	 * @param traceId
	 *            the unique trace id for this trace.
	 */
	public TraceHeader(int traceId) {
		headerValues = new Hashtable();
		this.traceId = traceId;
		location = traceId;
	}

	/**
	 * Constructs a new trace metadata object.
	 *
	 * @param traceId
	 *            the unique trace id for this trace
	 *
	 * @param traceLocation
	 *            the location of this trace.
	 * @param startValue
	 *            the starting value for the trace samples.
	 * @param numberOfSamples
	 *            the number of samples in this trace.
	 * @param traceStatus
	 *            One of the constants defined in the SeismicConstants (NORMAL,
	 *            REVERSED, NULL)
	 */
	public TraceHeader(int traceId, double traceLocation, double startValue,
			int numberOfSamples, int traceStatus) {
		headerValues = new Hashtable();
		this.traceId = traceId;
		startSample = startValue;
		location = traceLocation;
		numSamples = numberOfSamples;
		this.traceStatus = traceStatus;
	}

	/**
	 * Returns an unique key for this trace. It is Integer type of trace id.
	 *
	 * @return an object used as a key.
	 */
	public Object getKey() {
		return new Integer(traceId);
	}

	/**
	 * Returns the size in bytes to store trace values.
	 *
	 * @return the size of a trace instance.
	 */
	public int getSize() {

		return 61;
	}

	/**
	 * Retrieves the hashtable used to store values.
	 *
	 * @return the hashtable used to store the trace values.
	 */
	public Hashtable getDataValues() {
		return headerValues;
	}

	/**
	 * Retrieves the trace ID.
	 *
	 * @return the trace ID.
	 */
	public int getTraceId() {
		return traceId;
	}

	/**
	 * Retrieves the trace location.
	 *
	 * @return the location of the trace.
	 */
	public double getTraceLocation() {
		return location;
	}

	/**
	 * Retrieves the start sample value for this trace.
	 *
	 * @return the start sample value for this trace.
	 *
	 */
	public double getStartValue() {
		return startSample;
	}

	/**
	 * Retrieves the number of samples in the trace.
	 *
	 * @return the number of samples in the trace.
	 *
	 */
	public int getNumberOfSamples() {
		return numSamples;
	}

	/**
	 * Retrieves the status of the trace.
	 * @return the status of the trace. One of constants defined in SeismicConstants
	 * 			(NORMAL, REVERSED, NULL)
	 */
	public int getTraceState() {
		return traceStatus;
	}

	/**
	 * Returns a boolean indicating whether statistics has been
	 * calculated for this trace.
	 *
	 * @return <code>true</code> if calculated; otherwise false.
	 *
	 */
	public boolean isStatisticsCollected() {
		return valueSet;
	}

	/**
	 * Retrieves the minimum trace amplitude value.
	 *
	 * @return the minimum amplitude value for this trace.
	 */
	public float getMinimumAmplitude() {
		if (valueSet)
			return minAmplitude;
		else
			throw new RuntimeException("The value is not ready.");
	}

	/**
	 * Retrieves the maximum trace amplitude value.
	 *
	 * @return the maximum amplitude value for this trace
	 */
	public float getMaximumAmplitude() {
		if (valueSet)
			return maxAmplitude;
		else
			throw new RuntimeException("The value is not ready.");
	}

	/**
	 * Retrieves the average value for the absolute values of the samples in
	 * this trace.
	 *
	 * @return the average of the absolute values of the samples.
	 */
	public double getAverage() {
		if (valueSet)
			return average;
		else
			throw new RuntimeException("The value is not ready.");
	}

	/**
	 * Retrieves the root mean square (RMS) value for the samples in this trace.
	 *
	 * @return the trace RMS value for the sample in this trace.
	 */
	public double getRMS() {
		if (valueSet)
			return rootms;
		else
			throw new RuntimeException("The value is not ready.");
	}

	/**
	 * Stores the sample statistics (minimum and maximum
	 * amplitude values, average, RMS) for this trace.
	 *
	 * @param min
	 *            minimum amplitude value.
	 * @param max
	 *            maximum amplitude value.
	 * @param avg
	 *            absolute average amplitude value.
	 * @param rms
	 *            root mean square (RMS) value.
	 */
	public void setTraceStatistics(float min, float max, double avg, double rms) {
		minAmplitude = min;
		maxAmplitude = max;
		average = avg;
		rootms = rms;
		valueSet = true;
	}

	/**
	 * Retrieves the specified header field as a Number.
	 *
	 * @param fieldId
	 *            The identifier of field in header as it was defined in
	 *            SeismicFormat.
	 *
	 * @return value of header field with given identifier.
	 */
	public Number getFieldValue(int fieldId) {

		return (Number)headerValues.get(new Integer(fieldId));

	}

	/**
	 * Retrieves the header list Obejct[] (name, offset, value).
	 * @return A list of Obejct[] (name, offset, value).
	 */
	public ArrayList getHeaderList(){
		return fullheaderList;
	}

}
