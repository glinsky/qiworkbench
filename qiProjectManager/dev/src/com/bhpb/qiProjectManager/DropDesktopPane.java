package com.bhpb.qiProjectManager;

import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.File;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.util.logging.Logger;
import java.io.IOException;

import javax.swing.JDesktopPane;

import com.bhpb.qiProjectManager.PreviewWindow;
import com.bhpb.qiProjectManager.QiProjectManagerUI;
import com.bhpb.qiworkbench.compAPI.DataDataDescriptor;
import com.bhpb.qiworkbench.compAPI.DataDataDescriptorTransferable;

/*
 * DropDesktopPane.java
 *
 * Created on July 16, 2007, 5:17 PM
 */

/**
 * Extention of the JDesktopPane
 * @author Marcus Vaal
 */
public class DropDesktopPane extends JDesktopPane{
    
	private static Logger logger = Logger.getLogger(DropDesktopPane.class.getName());
	
    /**
     * DropDesktopPane Constructor
     * @param action Drag and Drop Action
     * @param qipmui QiProjectManagerUI
     */
    public DropDesktopPane(int action, QiProjectManagerUI qipmui) {
        super();
        if (action != DnDConstants.ACTION_NONE &&
        	action != DnDConstants.ACTION_COPY &&
        	action != DnDConstants.ACTION_MOVE &&
        	action != DnDConstants.ACTION_COPY_OR_MOVE &&      
        	action != DnDConstants.ACTION_LINK
            ) throw new IllegalArgumentException("action" + action);

        this.acceptableActions = action;
        this.qipmui = qipmui;
        this.dtListener = new DTListener();

        this.dropTarget = new DropTarget(this, this.acceptableActions, this.dtListener, true);
    }

    /**
     * Extends DropTargetListener for use by DropDesktopPane
     * @author Marcus Vaal
     */
    class DTListener implements DropTargetListener {
    
    	/**
    	 * Checks if the DataFlavor is supposed when being dragged
    	 * @param evt DropTargetDragEvent
    	 * @return
    	 */
		private boolean isDragFlavorSupported(DropTargetDragEvent evt) {
			DataFlavor chosen = null;
			if (evt.isDataFlavorSupported(DataDataDescriptorTransferable.dataDataDescriptorFlavor)) {
				chosen = DataDataDescriptorTransferable.dataDataDescriptorFlavor;
			} else if (evt.isDataFlavorSupported(DataDataDescriptorTransferable.localDataDataDescriptorFlavor)) {
				chosen = DataDataDescriptorTransferable.localDataDataDescriptorFlavor;
			} else if (evt.isDataFlavorSupported(new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType 
                    + "; class=com.bhpb.qiworkbench.compAPI.DataDataDescriptor", "Local DataDataDescriptor"))) {
				chosen = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType 
                        + "; class=com.bhpb.qiworkbench.compAPI.DataDataDescriptor", "Local DataDataDescriptor");
			} else if (evt.isDataFlavorSupported(new DataFlavor(DataDataDescriptor.class, "Non local DataDataDescriptor"))) {
				chosen = new DataFlavor(DataDataDescriptor.class, "Non local DataDataDescriptor");
			} else { //DropTargetDragEvent does not support any flavor known by this DropTargetListener
                return false;
            }

            Object data = null;
            try {
                data = evt.getTransferable().getTransferData(chosen);
            } // this should not happen because of the 4 checks above
              catch (UnsupportedFlavorException ufe) {
                logger.warning("getTransferData() threw UnsupportedFlavorException: "
                        + ufe.getMessage());
                return false;
            } catch (IOException ioe) {
                logger.warning("getTransferData() threw UnsupportedFlavorException: "
                        + ioe.getMessage());
                return false;
            }
            
            if (data == null) {
                logger.warning("Got NULL Transferable from DropTargetDragEvent, returning false from isDragFlavorSupported");
                return false;
            }

            //NOTE this is probably not a great way to handle drag events in the qiProjectManager,
            //by checking File.exists() every time a drag event is dispatched.
			if(data instanceof DataDataDescriptor) {
				if(!(new File(((DataDataDescriptor)data).getPath())).exists()) {
					return false;
				}
			}
			
			return true;
		}

		/**
		 * Returns your chosen Drop DataFlavor
		 * @param evt DropTargetDropEvent
		 * @return Returns the Drop DataFlavor
		 */
		private DataFlavor chooseDropFlavor(DropTargetDropEvent evt) {
			if (evt.isLocalTransfer() == true &&
					evt.isDataFlavorSupported(DataDataDescriptorTransferable.localDataDataDescriptorFlavor)) {
				return DataDataDescriptorTransferable.localDataDataDescriptorFlavor;
			}
			DataFlavor chosen = null;
			if (evt.isDataFlavorSupported(DataDataDescriptorTransferable.dataDataDescriptorFlavor)) {
				chosen = DataDataDescriptorTransferable.dataDataDescriptorFlavor;
			} else if (evt.isDataFlavorSupported(DataDataDescriptorTransferable.localDataDataDescriptorFlavor)) {
				chosen = DataDataDescriptorTransferable.localDataDataDescriptorFlavor;
			} else if (evt.isDataFlavorSupported(new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType + "; class=DataDataDescriptor", "Local DataDataDescriptor"))) {
				chosen = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType + "; class=DataDataDescriptor", "Local DataDataDescriptor");
			} else if (evt.isDataFlavorSupported(new DataFlavor(DataDataDescriptor.class, "Non local DataDataDescriptor"))) {
				chosen = new DataFlavor(DataDataDescriptor.class, "Non local DataDataDescriptor");
			}
			return chosen;
		}

		/**
		 * Checks if drag is ok
		 * @param evt DropTargetDragEvent
		 * @return Returns true if drag DataFlavor is supported, evt is a drop action and an exceptable action, false otherwise
		 */
		private boolean isDragOk(DropTargetDragEvent evt) {
			if(isDragFlavorSupported(evt) == false) {
				return false;
			}

			int da = evt.getDropAction();      
			if ((da & DropDesktopPane.this.acceptableActions) == 0) {
				return false;
			}
			
			return true;
		}

		/**
		 * Invoked to signify a Drag and Drop item is dragged into the DesktopPane
		 */
		public void dragEnter(DropTargetDragEvent evt) {
			if(isDragOk(evt) == false) {
				evt.rejectDrag();      
				return;
			}
			evt.acceptDrag(evt.getDropAction());      
		}

		/**
		 * Invoked to signify a Drag and Drop item is dragged over the DesktopPane
		 */
		public void dragOver(DropTargetDragEvent evt) {
			if(isDragOk(evt) == false) {
				evt.rejectDrag();      
				return;
			}
			evt.acceptDrag(evt.getDropAction());            
		}
		
		/**
		 * Called if the user has modified the current drop gesture 
	     */ 
		public void dropActionChanged(DropTargetDragEvent evt) {
			if(isDragOk(evt) == false) {
				evt.rejectDrag();      
				return;
			}
			evt.acceptDrag(evt.getDropAction());            
		}
    
		/**
		 * Invoked to signify a Drag and Drop item is dragged out of the DesktopPane
		 */
		public void dragExit(DropTargetEvent e) {
		}

		/**
		 * Invoked to signify a Drag and Drop item is dropped into the DesktopPane
		 */
		public void drop(DropTargetDropEvent evt) {
		  
			DataFlavor chosen = chooseDropFlavor(evt);
			if (chosen == null) {
				logger.finest("No flavor match found" );
				evt.rejectDrop();      	
				return;
			}
			
			int da = evt.getDropAction();
			int sa = evt.getSourceActions();      
			  
			if ((sa & DropDesktopPane.this.acceptableActions) == 0) {
				logger.finest("No action match found");
				evt.rejectDrop();      		
				return;
			}
			
			Object data=null;
			try {
				evt.acceptDrop(DropDesktopPane.this.acceptableActions);
				  
				data = evt.getTransferable().getTransferData(chosen);
				if (data == null) {
					throw new NullPointerException();
				}
			} catch ( Throwable thrown ) {
				logger.finest("Couldn't get transfer data: " + thrown.getMessage());
				logger.finest(thrown.toString());
				evt.dropComplete(false);
				return;
			}
			  
			if (data instanceof DataDataDescriptor) {
				//qipmui.createFrame((DataDataDescriptor)data, evt.getLocation());
				qipmui.createFrame(evt.getLocation());
			} else {
				evt.dropComplete(false);      	  	
				return;
			}
			  
			evt.dropComplete(true);
		}
    }

	private DropTarget dropTarget;
	private DropTargetListener dtListener;
	private QiProjectManagerUI qipmui = null;
	private int acceptableActions = DnDConstants.ACTION_COPY;  
  
}
