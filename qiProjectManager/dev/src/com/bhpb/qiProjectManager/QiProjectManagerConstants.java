/*
###########################################################################
# qiProjectManager - Manages a project's data files and their location.
# This program module Copyright (C) 2007  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiProjectManager;

/**
 * Constants used within the qiProjectManager
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class QiProjectManagerConstants {
    /**
     * Prevent object construction outside of this class.
     */
    private QiProjectManagerConstants(){}

    /** plugin's display name */
    public static final String QI_PROJECT_MANAGER_PLUGIN_NAME = "qiProjectManager";

    /** plugin GUI's display name */
    public static final String QI_PROJECT_MANAGER_GUI_NAME = "qiProjectManager GUI";
    // debug print: 0=none, 1=moderate, 2=verbose
    public static final int DEBUG_PRINT = 2;

    // Status
    public static final int OK_STATUS = 0;
    public static final int ERROR_STATUS = 1;
    public static final int JOB_UNSUBMITTED_STATUS = 0;
    public static final int JOB_RUNNING_STATUS = 1;
    public static final int JOB_ENDED_STATUS = 2;
    public static final int JOB_UNKNOWN_STATUS = 3;

    // Messages used for file chooser service
    public static final String PM_SELECT_QIPROJ = "selectQiProject";
    public static final String PM_SELECT_COPIED_QIPROJ = "selectCopiedQiProject";
    public static final String PM_SELECT_DATASET_DIR = "selectDatasetDir";
    public static final String PM_SELECT_HORIZON_DIR = "selectHorizonDir";
    public static final String PM_SELECT_SAVESET_DIR = "selectSavesetDir";
    public static final String PM_SELECT_SCRIPT_DIR = "selectScriptDir";
    public static final String PM_SELECT_TEMP_DIR = "selectTempDir";
    public static final String PM_SELECT_WELL_LOG_DIR = "selectWellLogDir";
    public static final String PM_SELECT_WORKBENCH_DIR = "selectWorkbenchDir";
    public static final String PM_SELECT_XML_DIR = "selectXmlDir";
    public static final String PM_SELECT_SEGY_DIR = "selectSegyDir";
    public static final String PM_SELECT_SEISMIC_DIR = "selectSeismicDir";

    // Names of data groups defined in project-defn.xml
    public static final String DATASETS_DATAGROUP = "Datasets";
    public static final String HORIZONS_DATAGROUP = "Horizons";
    public static final String SCRIPTS_DATAGROUP = "Scripts";
    public static final String SEISMIC_DATAGROUP = "Seismic";
    public static final String SESSIONS_DATAGROUP = "Sessions";
    public static final String TEMP_DATAGROUP = "Temp";
    public static final String WELLS_DATAGROUP = "Wells";
    public static final String WORKBENCHES_DATAGROUP = "Workbenches";
    public static final String XML_DATAGROUP = "XML";
    public static final String SEGY_DATAGROUP = "SEGY";
	
	// Miscellaneous
	//changed by Woody Folsom 6/21/2008 to avoid race condition in qiProjectMgr when
        //server is under heavy load (300+ open sockets)
        //public static final int MINUTE_WAIT_TIME = 60000;
    public static final int MINUTE_WAIT_TIME = 300000;
}
