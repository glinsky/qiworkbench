/*
###########################################################################
# qiProjectManager - Manage a project's data files and their location.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiProjectManager;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Set;
import java.util.logging.Logger;

import org.xml.sax.InputSource;
import org.apache.xerces.parsers.DOMParser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DataExplorerModel {
    private static Logger logger = Logger.getLogger(QiProjectManagerUI.class.getName());

    ArrayList<DataGroup> groups = new ArrayList<DataGroup>();
    HashMap<String, DataGroup> dataGroups = new HashMap<String, DataGroup>();

    DataGroup currentGroup;

    private static DataExplorerModel singleton = null;

    private DataExplorerModel() {};

    private static final String ROOT_NODE = "project-defn";
    private static final String GROUP_NODE = "group";
    private static final String GROUP_NAME_ATTR = "name";
    private static final String GROUP_DIR_ATTR = "dir";
    private static final String FILTER_NODE = "filter";
    private static final String FILTER_NAME_ATTR = "name";
    private static final String FILTER_SUFFIX_ATTR = "suffixes";
    private static final String ABSPATH_FILTER_NODE = "absPathFilter";
    private static final String ABSPATH_FILTERS_ATTR = "filters";

    /**
     * Get the singleton instance of this class. If the data explorer class doesn't
     * exist, create it.
     */
    public static DataExplorerModel getInstance() {
        if (singleton == null) {
            singleton = new DataExplorerModel();
        }

        return singleton;
    }

    /**
     * Parse the project definition XML file.
     * @param path URI of the XML file to parse.
     */
    public void parseProjectDefn(String path) {
        DOMParser parser = new DOMParser();

        try {
            InputStream in = this.getClass().getResourceAsStream(path);
            parser.parse(new InputSource(in));
            Document doc = parser.getDocument();

            walkTree(doc);
        } catch (FileNotFoundException fnfe) {
            System.out.println("DataExplorerModel: Cannot find file: "+fnfe.getMessage());
        } catch (IOException ioe) {
            System.out.println("DataExplorerModel: IO error: "+ioe.getMessage());
        } catch (Exception e) {
            System.out.println("DataExplorerModel: Parsing error: "+e.getMessage());
        }

        // set up a map between a group name and the information about the group
        for (int j=0; j<groups.size(); j++) {
            DataGroup dg = groups.get(j);
            dataGroups.put(dg.getGroupName(), dg);
        }

//        dumpGroups();
    }

    /**
     * Recursively walk the XML tree processing each node.
     * @param node Start node to begin walk.
     */
    private void walkTree(Node node) {
        NodeList children;
        NamedNodeMap attributes;
        ArrayList<String>attrNames = new ArrayList<String>();
        ArrayList<String>attrValues = new ArrayList<String>();
        String attrName, attrValue;

        switch (node.getNodeType()) {
            case Node.DOCUMENT_NODE:
                // Document object
                walkTree(((Document)node).getDocumentElement());
                break;

            case Node.ELEMENT_NODE:
                // element + attributes
                String name = node.getNodeName();
//                System.out.println("Element node: "+name);

                if (name.equals(GROUP_NODE)) {
                    // gather information for current data group
                    currentGroup = new DataGroup();
                    groups.add(currentGroup);

                    attributes = node.getAttributes();
                    attrNames.clear(); attrValues.clear();
                    // gather <group> attributes. NOTE: they can be in any order
                    for (int i=0; i<attributes.getLength(); i++) {
                        Node attr = attributes.item(i);
                        attrName = attr.getNodeName();
                        attrNames.add(attrName);
                        attrValue = attr.getNodeValue();
                        attrValues.add(attrValue);
//                        System.out.println("    Attribute: "+attrName+", Value: "+attrValue);
                    }
                    currentGroup.addGroupAttributes(attrNames, attrValues);

                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//                            System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }
                }
                else if (name.equals(FILTER_NODE)) {
                    attributes = node.getAttributes();
                    attrNames.clear(); attrValues.clear();
                    for (int i=0; i<attributes.getLength(); i++) {
                        Node attr = attributes.item(i);
                        attrName = attr.getNodeName();
                        attrNames.add(attrName);
                        attrValue = attr.getNodeValue();
                        attrValues.add(attrValue);
//                        System.out.println("    Attribute: "+attrName+", Value: "+attrValue);
                    }
                    currentGroup.addFilter(attrNames, attrValues);

                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//                            System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }
                }
                else if (name.equals(ABSPATH_FILTER_NODE)) {
                    attributes = node.getAttributes();
                    Node attr = attributes.item(0);
                    attrName = attr.getNodeName();
                    attrValue = attr.getNodeValue();
//                    System.out.println("    Attribute: "+attrName+", Value: "+attrValue);
                    if (attrName.equals(ABSPATH_FILTERS_ATTR))
                        currentGroup.setAbsPathFilters(attrValue);

                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//                            System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }
                }
                else if (name.equals(ROOT_NODE)) {
                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//                            System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }
                }
                break;

            case Node.TEXT_NODE:
            case Node.CDATA_SECTION_NODE:
                // textual data
                String text = node.getNodeValue();
                break;

            case Node.PROCESSING_INSTRUCTION_NODE:
                // processing instruction
                break;

            case Node.ENTITY_REFERENCE_NODE:
                // entity reference
                break;

            case Node.DOCUMENT_TYPE_NODE:
                // DTD declaration
                break;
        }
    }

    /**
     * Get the iterator for the list of groups.
     * @return An iterator of the elements in the groups list.
     */
    public ListIterator<DataGroup> groupIterator() {
        return groups.listIterator();
    }

    /**
     * Get the iterator for the filters of a group.
     * @param groupName Name of the group.
     * @return An iterator of the filters in the group.
     */
    public Iterator<String> filterIterator(String groupName) {
        DataGroup dg = (DataGroup)dataGroups.get(groupName);
        return dg.filterIterator();
    }

    /**
     * Get the comma separated list of suffixes associated with a filter for a data
     * group. If the filter is 'Scan', get the suffixes for all of the group's filters.
     * @param groupName Name of the data group.
     * @param filterName Name of the filter in the data group
     * @return Comma separated list of suffixes associated with the filter
     */
    public String getSuffixes(String groupName, String filterName) {
        DataGroup dg = (DataGroup)dataGroups.get(groupName);
        String suffixes = "";
        if (filterName.equals("Scan")) {
            Iterator<String> filterIterator = dg.filterIterator();
            while (filterIterator.hasNext()) {
                String fn = filterIterator.next();
                suffixes += suffixes.equals("") ? dg.getFilterSuffixes(fn) : ","+dg.getFilterSuffixes(fn);
            }
            return suffixes;
        } else return dg.getFilterSuffixes(filterName);
    }

    /**
     * Container for information about a data group
     */
    public class DataGroup {
        /** Unique name of the group. */
        String groupName = "";
        /** Relative path of the group within the project rooted in a qiSpace */
        String groupLoc = "";

        /** Filters for data files in this group. Key=filter name; Value=list of suffixes */
        HashMap<String, String> filters = new HashMap<String, String>();

        /** Comma separated list of filters for files containing absolute paths */
        String absPathFilters = "";

        /**
         * Get the name of the group.
         * @return The group's name.
         */
        public String getGroupName() {
            return this.groupName;
        }

        /**
         * Get the comma separated list of suffixes for a filter.
         * @param filterName The name of a filter in this data group.
         * @return Comma separated list of suffixes associated with the filter.
         */
        public String getFilterSuffixes(String filterName) {
            return filters.get(filterName);
        }

        /**
         * Get the comma separated list of filters for files containing absolute paths.
         * @return Comma separated list of filters for files containing absolute paths.
         */
        public String getAbsPathFilters() {
            return absPathFilters;
        }

        /**
         * Set the comma separated list of filters for files containing absolute paths.
         */
        public void setAbsPathFilters(String absPathFilters) {
            this.absPathFilters = absPathFilters;
        }

        public void addGroupAttributes(ArrayList<String> names, ArrayList<String> values) {
            for (int j=0; j<names.size(); j++) {
                String s = names.get(j);
                if (s.equals(GROUP_NAME_ATTR)) groupName = values.get(j);
                else if (s.equals(GROUP_DIR_ATTR)) groupLoc = values.get(j);
            }
        }

        public void addFilter(ArrayList<String> names, ArrayList<String> values) {
            String key="", val="";
            for (int j=0; j<names.size(); j++) {
                String s = names.get(j);
                if (s.equals(FILTER_NAME_ATTR)) key = values.get(j);
                else if (s.equals(FILTER_SUFFIX_ATTR)) val = values.get(j);
            }
            filters.put(key, val);
        }

        public Iterator<String> filterIterator() {
            Set<String> keys = filters.keySet();
            return keys.iterator();
        }

        public String toString() {
            StringBuffer buf  = new StringBuffer();
            buf.append(this.getClass().toString());
            buf.append(": \n Group Name=");
            buf.append(this.groupName);
            buf.append(", Group dir=");
            buf.append(this.groupLoc);

            Set<String> keys = filters.keySet();
            Iterator<String> iter = keys.iterator();
            while (iter.hasNext()) {
                String key = iter.next();
                String val = filters.get(key);
                buf.append("\n Filter=("+key+"; "+val+")");
            }
            return buf.toString();
        }
    }

    public void dumpGroups() {
/* alternative way
        Set<String> keys = dataGroups.keySet();
        Iterator<String> iter = keys.iterator();
        while (iter.hasNext()) {
            String key = iter.next();
            DataGroup dg = (DataGroup)dataGroups.get(key);
            System.out.println(dg.toString());
        }
*/
        for (int j=0; j<groups.size(); j++) {
            DataGroup dg = groups.get(j);
            System.out.println(dg.toString());
        }
    }
}
