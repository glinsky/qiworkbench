/*
###########################################################################
# qiProjectManager - Manage a project's data files and their location.
# This program module Copyright (C) 2007-2009 BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiProjectManager;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Dialog for qiComponent About. Displays version and license information.
 */
public class AboutListener extends JDialog {
    private ImageIcon _icon;
    private Component parent;
    private JScrollPane scrollPane = new JScrollPane();
    private JEditorPane aboutTextEditorPane = new JEditorPane();
    private JButton licenseAbout;
    private Properties props;
    public AboutListener(Component parent,Properties props) {
        this.parent = parent;
        this.props = props;
        _icon = new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    /**
     * Create the About dialog and display it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    public void createAndShowGUI(){
        aboutTextEditorPane.setEditable(false);
        aboutTextEditorPane.setContentType("text/html");
        aboutTextEditorPane.setAutoscrolls(true);

        scrollPane.setViewportView(aboutTextEditorPane);
        aboutTextEditorPane.setText(getAboutHtmlText());
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBackground(Color.WHITE);
        panel.add(new JLabel(_icon), BorderLayout.CENTER);
        getContentPane().add(panel, BorderLayout.WEST);
        getContentPane().add(scrollPane, BorderLayout.EAST);
        licenseAbout = new JButton("License");
        licenseAbout.setActionCommand("viewLicense");
        licenseAbout.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if (licenseAbout.getActionCommand().equals("viewLicense")) {
                    readFileIntoTextArea(aboutTextEditorPane);
                    licenseAbout = new JButton("About");
                    licenseAbout.setActionCommand("viewAbout");
                } else if (licenseAbout.getActionCommand().equals("viewAbout")) {
                    aboutTextEditorPane.setText(getAboutHtmlText());
                    licenseAbout = new JButton("License");
                    licenseAbout.setActionCommand("viewLicense");
                }
                // position to top of text
                aboutTextEditorPane.setCaretPosition(0);
            }
        });

        JButton okButton = new JButton("Close");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                dispose();
            }
        });

        JPanel bpanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        bpanel.add(licenseAbout);
        bpanel.add(okButton);
        getContentPane().add(bpanel, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(parent);
        setVisible(true);
    }

    private String getAboutHtmlText(){
        String viewerVersion = "qiProjectManager --";
        StringBuffer sb = new StringBuffer();
        sb.append("<font face=\"Arial\" size=3><b>");
        sb.append(viewerVersion+"</b> <br>manage a project's data files and their location<br><br>");
        String versionProp = "";
        String buildProp = "";

        if(props != null){
            versionProp = props.getProperty("project.version");
            buildProp = props.getProperty("project.build");
        }

        if(versionProp == null) {
             versionProp = "";
        }

        if(buildProp == null) {
            buildProp = "";
        }

        sb.append("Version: " + versionProp + "<br>");
        sb.append("Build id: " + buildProp + "<br><br>");

        sb.append("This program module Copyright (C) 2007-2009, ");
        sb.append("BHP Billiton Petroleum and is licensed <br>");
        sb.append("under the GNU General Public License Version 2.");
        sb.append("<br><br>");
        sb.append("qiProjectManager comes with ABSOLUTELY NO WARRANTY. ");
        sb.append("<br><br>");
        sb.append("Click on the <b>License</b> button for more details. ");
        sb.append("<br><br>");
        sb.append("<br><br>");
        sb.append("</font>");
        return sb.toString();

    }

    private String getLicense(){
        StringBuffer license = new StringBuffer();
        BufferedReader br = null;
        String str;
        try {
            InputStream io = this.getClass().getResourceAsStream("/license.txt");
            br = new BufferedReader(new InputStreamReader(io));
            while ((str = br.readLine()) != null)
                license.append(str + "\n");

            System.out.println("license" + license.toString());
        } catch (FileNotFoundException fnfe) {
            license.append(fnfe.getMessage());
        } catch (IOException ioe) {
            license.append(ioe.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception ignored) {
                    license.append(ignored.getMessage());
                }
            }
        }
        return license.toString();
    }

    private void readFileIntoTextArea(JEditorPane licensePane) {
        StringBuffer license = new StringBuffer();

        BufferedReader br = null;
        String str;
        try {
            InputStream io = this.getClass().getResourceAsStream("/license.txt");
            br = new BufferedReader(new InputStreamReader(io));
            while ((str = br.readLine()) != null)
                license.append(str + "\n");
        } catch (FileNotFoundException fnfe) {
            license.append(fnfe.getMessage());
        } catch (IOException ioe) {
            license.append(ioe.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception ignored) {
                    license.append(ignored.getMessage());
                }
            }
        }

        licensePane.setText(license.toString());
    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        //JFrame frame = new JFrame();
        //frame.addActionListener(new WaveletDecompAbout(null));
    }

}
