/*
###########################################################################
# qiProjectManager - Manage a project's data files and their location.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiProjectManager;

import java.awt.Component;

import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bhpb.qiProjectManager.QiProjectManagerConstants;
import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ComponentStateUtils;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.compAPI.QiSpaceDescUtils;
import com.bhpb.qiworkbench.compAPI.XmlUtils;
import com.bhpb.qiworkbench.workbench.QiComponentBase;

/**
 * qiProjectManager plugin, a core qiComponent of qiWorkbench. It is
 * started as a thread by the Messenger Dispatcher that executes the
 * "Activate plugin" command.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class QiProjectManagerPlugin extends QiComponentBase implements IqiWorkbenchComponent, Runnable {
    private static Logger logger = Logger.getLogger(QiProjectManagerPlugin.class.getName());

    // Messaging Manager for this qiComponent
    private MessagingManager messagingMgr;

    //  GUI component of this qiComponent
    private QiProjectManagerUI gui;

    public QiProjectManagerUI getGUI() {
        return gui;
    }

    // my thread
    private static Thread pluginThread;

    /** CID for component instance. Generated before the thread is started and carried as the thread's name. */
    private static String myCID = "";

    // Flag to indicate to quit the qiComponent
    private boolean stop = false;

    private static int saveAsCount = 0;

    /** Instance of project metadata editor that is using a file selector */
    private EditProjectDirStructureDialog projEditor = null;

    /**
     * Get Messaging Manager of this component.
     * @return  MessagingManager
     */
    public MessagingManager getMessagingMgr() {
        return messagingMgr;
    }

    /**
     * Project associated with this Project Manager instance. Project metadata
     * initialized to default values when instance created. */
    private QiProjectDescriptor qiProjectDesc = new QiProjectDescriptor();

    protected static String getCID() {
        return myCID;
    }

    /**
     * Get the descriptor of the project associated with this qiComponent.
     */
    public QiProjectDescriptor getProjectDescriptor() {
        return qiProjectDesc;
    }

    /**
     * Set the descriptor of the project associated with this qiComponent.
     * Mainly called when importing a copied project containing the exported state of a PM.
     * @param projDesc Project descriptor
     */
    public void setProjectDescriptor(QiProjectDescriptor projDesc) {
        this.qiProjectDesc = projDesc;
    }

    /**
     * Get the component descriptor for this qiComponent
     */
    public IComponentDescriptor getComponentDescriptor() {
        return messagingMgr.getMyComponentDesc();
    }

    /**
     * Get ID of project associated with this PM.
     * Typically, the PID is the full path of the project within a qiSpace.
     * @return PID of associated project; empty string if none associated yet.
     */
    public String getPid() {
        return qiProjectDesc.getPid();
    }

    /**
     * Initialize the plugin component:
     * <ul>
     * <li>Create its messaging manager</li>
     * <li>Register with the Message Dispatcher</li>
     * </ul>
     */
    public void init() {
        try {
            QIWConstants.SYNC_LOCK.lock();
            messagingMgr = new MessagingManager();
            
            myCID = Thread.currentThread().getName();
            
            // register self with the Message Dispatcher
            messagingMgr.registerComponent(QIWConstants.PLUGIN_PROJMGR_COMP, QiProjectManagerConstants.QI_PROJECT_MANAGER_PLUGIN_NAME, myCID);
            
            //notify the startup method initialization finished
            QIWConstants.SYNC_LOCK.unlock();
            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in QiProjectManagerPlugin.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

    /**
     * Generate state information into XML string format
     * @return  String
     */
    public String genState() {
        StringBuffer content = new StringBuffer();
        IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
        //TODO WorkbenchStateManager will need to be changed to conform to the package change of QiProjectManagerPlugin in ServletDispatcher
        content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc) + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + CompDescUtils.getDescPreferredDisplayName(desc) + "\">\n");
        content.append(gui.saveState());
        content.append("</component>\n");
        return content.toString();
    }

    /**
     * Generate state information into XML string format in repsonse to save as command
     * @return  String
     */
    public String genStateAsClone() {
        StringBuffer content = new StringBuffer();
        IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
        //TODO WorkbenchStateManager will need to be changed to conform to the package change of AmpExtPlugin in ServletDispatcher
        String displayName = "";
        saveAsCount++;
        if (saveAsCount == 1)
            displayName = "CopyOf" + CompDescUtils.getDescPreferredDisplayName(desc);
        else
            displayName = "Copy(" + saveAsCount + ")Of" + CompDescUtils.getDescPreferredDisplayName(desc);
        content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc) + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + displayName + "\">\n");
        content.append(gui.saveState());
        content.append("</component>\n");

        return content.toString();
    }

    /**
     * Restore plugin and it's GUI to previous condition
     *
     * @param node XML containing saved state variables
     */
    void restoreState(Node node) {
        String preferredDisplayName = ((Element)node).getAttribute("preferredDisplayName");
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals(QiProjectManagerUI.class.getName())) {
                    //get project descriptor from state since GUI needs it
                    qiProjectDesc = getProjectDesc(child);
                    gui = new QiProjectManagerUI(this);
                    gui.restoreState(child);
                    if (preferredDisplayName != null && preferredDisplayName.trim().length() > 0) {
                        CompDescUtils.setDescPreferredDisplayName(messagingMgr.getMyComponentDesc(), preferredDisplayName);
                        gui.renamePlugin(preferredDisplayName);
                    }

                    //set title of window
                    String projectName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
                    gui.resetTitle(projectName);
                }
            }
        }
    }

    /** XML containing saved state variables. Implicit input parameter to importState() */
    Node compNode = null;

    /**
     * Import the state of a PM using the XML tree saved when the state file was selected.
     */
    public void importState() {
        if (compNode != null) {
            //import state
            importState(compNode);  //extracts project descriptor
        }
    }

    /**
     * Import state saved in a XML file. Use existing GUI. User not asked to save current
     * state first, because after import, saving state will save imported state. The preferred
     * display name is not changed. Therefore, the name in the GUI title and component tree node
     * don't have to change.
     *
     * @param node XML containing saved state variables
     */
    public void importState(Node node) {
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals(QiProjectManagerUI.class.getName())) {
                    //get project descriptor from state since GUI needs it
                    qiProjectDesc = getProjectDesc(child);
                    gui.restoreState(child);

                    //set title of window
                    String projectName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
                    gui.resetTitle(projectName);
                }
            }
        }
    }

    /**
     * Extract the project descriptor from the imported state
     * Implicit input is the node of the imported state
     * @return Imported project descriptor; null if can't extracgt
     */
    public QiProjectDescriptor getImportedProjDesc() {
        NodeList children = compNode.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals(QiProjectManagerUI.class.getName())) {
                    return getProjectDesc(child);
                }
            }
        }

        return null;
    }

    /**
     * Extract the project descriptor from the saved state. If the state
     * does not contain a project descriptor, form one from the implicit
     * project so backward compatible.
     *
     * @return Project descriptor for project associated with the PM.
     */
    private QiProjectDescriptor getProjectDesc(Node node) {
        QiProjectDescriptor projDesc = null;

        NodeList children = node.getChildNodes();
        String nodeName;
        Node child;
        for (int i = 0; i < children.getLength(); i++) {
          child = children.item(i);
          if (child.getNodeType() == Node.ELEMENT_NODE) {
            nodeName = child.getNodeName();
            if (nodeName.equals("QiProjectDescriptor")) {
                projDesc = (QiProjectDescriptor)XmlUtils.XmlToObject(child);
                break;
            }
          }
        }

        //Check if saved state has a project descriptor. If not, create
        //one using the implicit project so backward compatible.
        if (projDesc == null) {
            projDesc = new QiProjectDescriptor();
            //get path of implicit project from Message Dispatcher
            String projPath = messagingMgr.getProject();
            QiProjectDescUtils.setQiSpace(projDesc, projPath);
            //leave relative location of project as "/"
            int idx = projPath.lastIndexOf("\\");
            if (idx == -1) idx = projPath.lastIndexOf("/");
            String projectName = projPath.substring(idx+1);
            QiProjectDescUtils.setQiProjectName(projDesc, projectName);
        }

        return projDesc;
    }

    /**
     * Initialize the plugin, then start processing messages its receives.
     * The pleaseStop flag is set when user has terminated the GUI, and it is time to quit
     * If a message has the skip flag set, it is to be handled by the GUI, not this class
     */
    public void run() {
        // initialize the Wavelet Decomposition plugin
        init();
        // process any messages received from other components
        while (stop == false) {
            IQiWorkbenchMsg msg = messagingMgr.peekNextMsg();
            if (msg != null && !(msg.getMsgKind().equals(QIWConstants.DATA_MSG) && msg.skip())) {
                msg = messagingMgr.getNextMsgWait();
                processMsg(msg);
            }
        }
    }

    private String jobID;

    /**
     * Find the request matching the response and process the response based on the request.
     */
    public void processMsg(IQiWorkbenchMsg msg) {
        /** Request that matches the response */
        IQiWorkbenchMsg request = null;

        // log message traffic
        logger.fine("msg="+msg.toString());

        // Check if a response. If so, process and consume response
        if (messagingMgr.isResponseMsg(msg)) {
            request = messagingMgr.checkForMatchingRequest(msg);
            String cmd = MsgUtils.getMsgCommand(request);
            // check if from the message dispatcher
            if (messagingMgr.isResponseFromMsgDispatcher(msg)) {
                if (cmd.equals(QIWConstants.NULL_CMD))
                    return;
            }
            else if (cmd.equals(QIWConstants.GET_JOB_OUTPUT_CMD)) {
                ArrayList<String> stdOut = (ArrayList<String>)msg.getContent();
                String jobOutput = "";
                for(String s : stdOut) {
                    jobOutput += s + "\n";
                }
                //gui.setStdOutTextArea(jobOutput);

            }
            else if (cmd.equals(QIWConstants.SUBMIT_JOB_CMD)) {
                jobID = (String)msg.getContent();
            }
            // process the selected project (Browse in the metadata editor)
            else if (cmd.equals(QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD)) {
                ArrayList list = (ArrayList)msg.getContent();

                if (((Integer)list.get(0)).intValue() == JFileChooser.APPROVE_OPTION) {
                    final String selectionPath = (String)list.get(1);
                    String filesep = messagingMgr.getServerOSFileSeparator();

                    String action = (String)list.get(3);
                    if (action.equals(QiProjectManagerConstants.PM_SELECT_QIPROJ)) {
                        //Selection is the path of a project
                        String qispacePath = QiSpaceDescUtils.getQiSpacePath(messagingMgr.getQispaceDesc());
                        //Check if the project is contained w/i the qiSpace or is the qiSpace
                        if (selectionPath.startsWith(qispacePath)) {
                            String projName = "";

                            //Extract the name of the project which is the name of its directory
                            int idx = selectionPath.lastIndexOf(filesep);
                            if (idx != -1) {
                                projName = selectionPath.substring(idx+1);
                                QiProjectDescUtils.setQiProjectName(qiProjectDesc, projName);
                                projEditor.setQiProjectNameTextField(projName);
                            }

                            //Set the relative path of the project within the qiSpace
                            if (!selectionPath.equals(qispacePath)) {
                                //skip leading file separator
                                idx = qispacePath.length()+1;
                                String projReloc = selectionPath.substring(idx);
                                QiProjectDescUtils.setQiProjectReloc(qiProjectDesc, projReloc);

                                projEditor.setQiProjectRelocTextField(projReloc);
                            }

                            //Enable exporting of project information
                            gui.enableExporting();

                            //Set project ID
                            qiProjectDesc.setPid(selectionPath);

                            //Reset PM's title
                            gui.resetTitle(projName);
                        } else {
                            JOptionPane.showMessageDialog(gui,
                                            "qiProject is not contained within qiSpace. Browse again",
                                            "Browse Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    else if (action.equals(QiProjectManagerConstants.PM_SELECT_COPIED_QIPROJ)) {
                        //Selection is the full path of a copied project
                        gui.setNewProject(selectionPath);
                    }
                    else if (action.equals(QiProjectManagerConstants.PM_SELECT_DATASET_DIR)) {
                        //Selection is the path of datasets directory

                        //don't have to make sure selection is within the project directory beacause file chooser
                        //prevents navigating above the project level.
                        String projPath = QiProjectDescUtils.getProjectPath(qiProjectDesc);
                        int idx = projPath.length();
                        String datasetReloc = selectionPath.substring(idx);
                        if (datasetReloc.startsWith(filesep)) datasetReloc = datasetReloc.substring(1);
                        QiProjectDescUtils.setDatasetsReloc(qiProjectDesc, datasetReloc);
                        projEditor.setDatasetsRelocTextField(datasetReloc);
                    }
                    else if (action.equals(QiProjectManagerConstants.PM_SELECT_SAVESET_DIR)) {
                        //Selection is the path of savesets directory

                        String projPath = QiProjectDescUtils.getProjectPath(qiProjectDesc);
                        int idx = projPath.length();
                        String savesetReloc = selectionPath.substring(idx);
                        if (savesetReloc.startsWith(filesep)) savesetReloc = savesetReloc.substring(1);
                        QiProjectDescUtils.setSavesetsReloc(qiProjectDesc, savesetReloc);
                        projEditor.setSavesetsRelocTextField(savesetReloc);
                    }
                    else if (action.equals(QiProjectManagerConstants.PM_SELECT_WORKBENCH_DIR)) {
                        //Selection is the path of workbenches directory

                        String projPath = QiProjectDescUtils.getProjectPath(qiProjectDesc);
                        int idx = projPath.length();
                        String workbenchReloc = selectionPath.substring(idx);
                        if (workbenchReloc.startsWith(filesep)) workbenchReloc = workbenchReloc.substring(1);
                        QiProjectDescUtils.setWorkbenchesReloc(qiProjectDesc, workbenchReloc);
                        projEditor.setWorkbenchesRelocTextField(workbenchReloc);
                    }
                    else if (action.equals(QiProjectManagerConstants.PM_SELECT_HORIZON_DIR)) {
                        //Selection is the path of horizons directory

                        String projPath = QiProjectDescUtils.getProjectPath(qiProjectDesc);
                        int idx = projPath.length();
                        String horizonReloc = selectionPath.substring(idx);
                        if (horizonReloc.startsWith(filesep)) horizonReloc = horizonReloc.substring(1);
                        QiProjectDescUtils.setHorizonsReloc(qiProjectDesc, horizonReloc);
                        projEditor.setHorizonsRelocTextField(horizonReloc);
                    }
                    else if (action.equals(QiProjectManagerConstants.PM_SELECT_SCRIPT_DIR)) {
                        //Selection is the path of scripts directory

                        String projPath = QiProjectDescUtils.getProjectPath(qiProjectDesc);
                        int idx = projPath.length();
                        String scriptsReloc = selectionPath.substring(idx);
                        if (scriptsReloc.startsWith(filesep)) scriptsReloc = scriptsReloc.substring(1);
                        QiProjectDescUtils.setScriptsReloc(qiProjectDesc, scriptsReloc);
                        projEditor.setScriptsRelocTextField(scriptsReloc);
                    }
                    else if (action.equals(QiProjectManagerConstants.PM_SELECT_WELL_LOG_DIR)) {
                        //Selection is the path of well logs directory

                        String projPath = QiProjectDescUtils.getProjectPath(qiProjectDesc);
                        int idx = projPath.length();
                        String wellsReloc = selectionPath.substring(idx);
                        if (wellsReloc.startsWith(filesep)) wellsReloc = wellsReloc.substring(1);
                        QiProjectDescUtils.setWellsReloc(qiProjectDesc, wellsReloc);
                        projEditor.setWellsRelocTextField(wellsReloc);
                    }
                    else if (action.equals(QiProjectManagerConstants.PM_SELECT_TEMP_DIR)) {
                        //Selection is the path of temp directory

                        String projPath = QiProjectDescUtils.getProjectPath(qiProjectDesc);
                        int idx = projPath.length();
                        String tempReloc = selectionPath.substring(idx);
                        if (tempReloc.startsWith(filesep)) tempReloc = tempReloc.substring(1);
                        QiProjectDescUtils.setTempReloc(qiProjectDesc, tempReloc);
                        projEditor.setTempDirTextField(tempReloc);
                    }
                    else if (action.equals(QiProjectManagerConstants.PM_SELECT_SEGY_DIR)) {
                        //Selection is the path of temp directory

                        String projPath = QiProjectDescUtils.getProjectPath(qiProjectDesc);
                        int idx = projPath.length();
                        String segyReloc = selectionPath.substring(idx);
                        if (segyReloc.startsWith(filesep)) segyReloc = segyReloc.substring(1);
                        QiProjectDescUtils.setSegyReloc(qiProjectDesc, segyReloc);
                        projEditor.setSegyDirTextField(segyReloc);
                    }
                    else if (action.equals(QiProjectManagerConstants.PM_SELECT_XML_DIR)) {
                        //Selection is the path of XML directory

                        String projPath = QiProjectDescUtils.getProjectPath(qiProjectDesc);
                        int idx = projPath.length();
                        String xmlReloc = selectionPath.substring(idx);
                        if (xmlReloc.startsWith(filesep)) xmlReloc = xmlReloc.substring(1);
                        QiProjectDescUtils.setXmlReloc(qiProjectDesc, xmlReloc);
                        projEditor.setXmlDirTextField(xmlReloc);
                    }
                    else if (action.equals(QIWConstants.EXPORT_COMP_STATE)) {
                        System.out.println("PM: export state to "+selectionPath);
                        //Write XML state to the specified file and location
                        String pmState = genState();
                        String xmlHeader = "<?xml version=\"1.0\" ?>\n";
//TODO: handle remote case - use an IO service
                        try {
                            ComponentStateUtils.writeState(xmlHeader+pmState, selectionPath, messagingMgr);
                        } catch (QiwIOException qioe) {
                            //TODO notify user cannot write state file
                            logger.finest(qioe.getMessage());
                        }
                    }
                    else if (action.equals(QIWConstants.IMPORT_COMP_STATE)) {
//TODO: handle remote case - use an IO service
                        //check if file exists
                        if (ComponentStateUtils.stateFileExists(selectionPath, messagingMgr)) {
                            try {
                                String pmState = ComponentStateUtils.readState(selectionPath, messagingMgr);
                                //save XML node; used later to import the state
                                compNode = ComponentStateUtils.getComponentNode(pmState);
                                if (compNode == null) {
                                    JOptionPane.showMessageDialog(gui, "Can't convert XML state to XML object", "Internal Processing Error", JOptionPane.ERROR_MESSAGE);
                                } else {
                                    //check for matching component type
                                    String compType = ((Element)compNode).getAttribute("componentType");
                                    String expectedCompType = messagingMgr.getRegisteredComponentDisplayNameByDescriptor(messagingMgr.getMyComponentDesc());
                                    if (!compType.equals(expectedCompType)) {
                                        JOptionPane.showMessageDialog(gui, "State file not for this qiComponent type. Select again", "Selection Error", JOptionPane.ERROR_MESSAGE);
                                    } else {
                                        //Note: don't ask if want to save state before importing because
                                        //      for future saves, the imported state will be saved.
                                        gui.setStatePath(selectionPath);

                                        //Note: The Import dialog will 1) ask the agent to import the state
                                        //      and 2) notify the associated qiComponents of the new project
                                        //      descriptor when OK is selected.
                                    }
                                }
                            } catch (QiwIOException qioe) {
                                JOptionPane.showMessageDialog(gui, "Can't read state file. Select again.", "Processing Error", JOptionPane.ERROR_MESSAGE);
                                logger.finest(qioe.getMessage());
                            }
                        } else {
                            JOptionPane.showMessageDialog(gui, "State file doesn't exist or is not a file. Select again.", "Selection Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            }
            // TODO other possible responses...
            else
                logger.warning("QiProjectManagerPlugin: Response to " + cmd + " command not processed " + msg.toString());
            return;
        }
        // Check if a request. If so, process and send back a response
        else if (messagingMgr.isRequestMsg(msg)) {
            String cmd = msg.getCommand();
            // deactivate plugin came from user, tell gui to quit and stop run method
            if (cmd.equals(QIWConstants.DEACTIVATE_PLUGIN_CMD) || cmd.equals(QIWConstants.REMOVE_COMPONENT_CMD) || cmd.equals(QIWConstants.DEACTIVATE_COMPONENT_CMD) || cmd.equals(QIWConstants.REMOVE_PLUGIN_CMD)) {
                if (QiProjectManagerConstants.DEBUG_PRINT > 0)
                    System.out.println(CompDescUtils.getDescPreferredDisplayName(messagingMgr.getMyComponentDesc()) + " quitting");
                try {
                    if (gui != null && gui.isVisible())
                        gui.closeGUI();
                    // unregister plugin
                    messagingMgr.unregisterComponent(messagingMgr.getComponentDesc(myCID));
                } catch (Exception e) {
                    IQiWorkbenchMsg res = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Unknown exception occurred with " + myCID);
                    messagingMgr.routeMsg(res);
                }
                stop = true;
                messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE,myCID + " is successfully deactivated.");
            }
            else if (cmd.equals(QIWConstants.OPEN_COMPONENT_GUI_CMD)) {
                if (gui != null)
                    gui.setVisible(true);
            }
            else if (cmd.equals(QIWConstants.CLOSE_COMPONENT_GUI_CMD)) {
                if (QiProjectManagerConstants.DEBUG_PRINT > 0)
                    System.out.println(myCID + " quitting");
                if (gui != null){
                	messagingMgr.sendResponse(msg,Component.class.getName(),(Component)gui);
                    gui.closeGUI();
                }
            }
            else if (cmd.equals(QIWConstants.SET_PREFERRED_DISPLAY_NAME_CMD)) {
                            String preferredDisplayName = (String)msg.getContent();
                if (preferredDisplayName != null && preferredDisplayName.trim().length() > 0) {
                    CompDescUtils.setDescPreferredDisplayName(messagingMgr.getMyComponentDesc(), preferredDisplayName);
                    gui.renamePlugin(preferredDisplayName);
                }
                messagingMgr.sendResponse(msg,QIWConstants.COMP_DESC_TYPE,messagingMgr.getMyComponentDesc());
            }
            // save state
            else if (cmd.equals(QIWConstants.SAVE_COMP_CMD))
                messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, genState());
            // save component state as clone
            else if (cmd.equals(QIWConstants.SAVE_COMP_AS_CLONE_CMD)) {
                messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, genStateAsClone());
            // restore state
            } else if (cmd.equals(QIWConstants.RESTORE_COMP_CMD)) {
                Node node = (Node)msg.getContent();
                restoreState(node);
                messagingMgr.sendResponse(msg, QiProjectManagerUI.class.getName(), gui);
            }
            // Note: invoke is done after activate
            else if (cmd.equals(QIWConstants.INVOKE_SELF_CMD)) {
                // Do nothing if don't have a GUI.

                // Create the plugin's GUI, but don't make it visible. That is
                // up to the Workbench Manager who requested the plugin be
                // activated. Pass GUI the CID of its parent.

                // Check to see if content incudes XML information which means
                // invoke self will do the restoration
                ArrayList msgList = (ArrayList)msg.getContent();
                String invokeType = (String)msgList.get(0);
                if (invokeType.equals(QIWConstants.COMPONENT_INVOKE_RESTORED)) {
                    Node node = ((Node)(msgList).get(2));
                    restoreState(node);
                } else if (invokeType.equals(QIWConstants.COMPONENT_INVOKE_NEW)) {
                    //Initialize PM's project descriptor to qiSpace
                    qiProjectDesc.setQiSpace(messagingMgr.getQispaceDesc().getPath());
                    qiProjectDesc.setQiProjectReloc(messagingMgr.getServerOSFileSeparator());
                    qiProjectDesc.setQiProjectName("");
                    //Initialize ID of project PM is managing
                    qiProjectDesc.setPid(QiProjectDescUtils.getProjectPath(qiProjectDesc));
                    gui = new QiProjectManagerUI(this);
                    java.awt.Point p = (java.awt.Point)msgList.get(1);
                    if(p != null)
                    	gui.setLocation(p);
                }

                //Send a normal response back with the plugin's JInternalFrame and
                //let the Workbench Manager add it to the desktop and make
                //it visible.
                messagingMgr.sendResponse(msg, QiProjectManagerUI.class.getName(), gui);
            }
            // user selected rename plugin menu item
            else if (cmd.equals(QIWConstants.RENAME_COMPONENT)) {
                ArrayList<String> names = new ArrayList<String>(2);
                names = (ArrayList)msg.getContent();
                if (QiProjectManagerConstants.DEBUG_PRINT > 0)
                    System.out.println("Rename myself from " + names.get(0) + " to " + names.get(1));
                gui.renamePlugin(names.get(1));
            }
            //get info about the associated project
            else if (cmd.equals(QIWConstants.GET_PROJ_INFO_CMD)) {
                ArrayList projInfo = new ArrayList();
                //add the project's ID
                projInfo.add(QiProjectDescUtils.getPid(qiProjectDesc));
                //add the project's metadata
                projInfo.add(qiProjectDesc);
                messagingMgr.sendResponse(msg, QIWConstants.ARRAYLIST_TYPE, projInfo);
            }
            //respond if PIDs match
            else if (cmd.equals(QIWConstants.GET_ASSOC_PROJMGR_CMD)) {
                //get the PID of the component
                String compPid = (String)msg.getContent();
                String pmPid = QiProjectDescUtils.getPid(qiProjectDesc);
                if (compPid.equals(pmPid)) {
                    ArrayList pmInfo = new ArrayList();
                    pmInfo.add(getComponentDescriptor());
                    pmInfo.add(qiProjectDesc);
                    messagingMgr.sendResponse(msg, QIWConstants.ARRAYLIST_TYPE, pmInfo);
                }
            }
            else
                logger.warning("QiProjectManagerPlugin: Request not processed, requeued: " + msg.toString());
            return;
        }
    }

    /**
     * Send a message to the Workbench Manager to remove this instance of
     * qiProjectManager from the component tree and send a message to it
     * to deactivate itself.
     */
    public void deactivateSelf() {
        //ask Workbench Manager to remove qiProjectManager from workbench GUI and
        //then send it back to self to deactivate self
        IComponentDescriptor wbMgr = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_MGR_NAME);
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.QUIT_COMPONENT_CMD, wbMgr, QIWConstants.STRING_TYPE, messagingMgr.getMyComponentDesc());
    }

//    private ImportDialog importDialog = null;

    /**
     * Browse for a copied project within the qiSpace using a file chooser.
     * @param parent Import Dialog
     */
    public void browseForCopiedProject(Component parent, String startDir, String title, FileFilter filter, String chooserType, String command, String serverUrl) {
//      importDialog = (ImportDialog)parent;

      ArrayList list = new ArrayList();
      //1st element the parent GUI object
      list.add(parent);
      //2nd element is the dialog title
      list.add(title);
      //3rd element is a list that contains current directory to start with
      //and a flag (yes or no) indicating if a already remembered directory
      //should be used instead
      ArrayList lst = new ArrayList();
      lst.add(startDir);
      lst.add("no");
      list.add(lst);
      //4th element is the file filter
      list.add(filter);
      //5th element is the navigation flag
      list.add(true);
      //6th element is the producer component descriptor
      list.add(messagingMgr.getMyComponentDesc());
      //7th element is the type of file chooser either Open or Save
      list.add(chooserType);
      //8th element is the original qiworkbench message command or action that request for this service
      list.add(command);
      //9th element is the default pretext for the file name if user fails to pretext it. If no pretext is required then use empty String
      list.add("");
      //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
      list.add("");
      //11th element is the target tomcat url where the file chooser chooses
      list.add(serverUrl);
      doFileChooser(list);
    }

    /**
     * Browse for a project within the qiSpace using a file chooser.
     * @param parent Project metadata editor
     */
    public void browseForQiProject(Component parent, String title, FileFilter filter, String chooserType, String command, String serverUrl) {
      projEditor = (EditProjectDirStructureDialog)parent;

      ArrayList list = new ArrayList();
      //1st element the parent GUI object
      list.add(parent);
      //2nd element is the dialog title
      list.add(title);
      //3rd element is a list that contains current directory to start with
      //and a flag (yes or no) indicating if a already remembered directory
      //should be used instead
      ArrayList lst = new ArrayList();
      lst.add(QiSpaceDescUtils.getQiSpacePath(messagingMgr.getQispaceDesc()));
      lst.add("no");
      list.add(lst);
      //4th element is the file filter
      list.add(filter);
      //5th element is the navigation flag
      list.add(true);
      //6th element is the producer component descriptor
      list.add(messagingMgr.getMyComponentDesc());
      //7th element is the type of file chooser either Open or Save
      list.add(chooserType);
      //8th element is the original qiworkbench message command or action that request for this service
      list.add(command);
      //9th element is the default pretext for the file name if user fails to pretext it. If no pretext is required then use empty String
      list.add("");
      //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
      list.add("");
      //11th element is the target tomcat url where the file chooser chooses
      list.add(serverUrl);
      doFileChooser(list);
    }

    /**
     * Browse for a project directory within a project using a file chooser.
     * @param parent Project metadata editor
     */
    public void browseForProjectDir(Component parent, String title, FileFilter filter, String chooserType, String command, String serverUrl) {
      projEditor = (EditProjectDirStructureDialog)parent;

      ArrayList list = new ArrayList();
      //1st element the parent GUI object
      list.add(parent);
      //2nd element is the dialog title
      list.add(title);
      //3rd element is a list that contains current directory to start with
      //and a flag (yes or no) indicating if a already remembered directory
      //should be used instead
      ArrayList lst = new ArrayList();
      String startDir = QiProjectDescUtils.getProjectPath(qiProjectDesc);
      if (command.equals(QiProjectManagerConstants.PM_SELECT_QIPROJ)) startDir = QiProjectDescUtils.getQiSpace(qiProjectDesc);
      lst.add(startDir);
      lst.add("no");
      list.add(lst);
      //4th element is the file filter
      list.add(filter);
      //5th element is the navigation flag (can't go above starting directory)
      list.add(false);
      //6th element is the producer component descriptor
      list.add(messagingMgr.getMyComponentDesc());
      //7th element is the type of file chooser either Open or Save
      list.add(chooserType);
      //8th element is the original qiworkbench message command or action that request for this service
      list.add(command);
      //9th element is the default pretext for the file name if user fails to pretext it. If no pretext is required then use empty String
      list.add("");
      //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
      list.add("");
      //11th element is the target tomcat url where the file chooser chooses
      list.add(serverUrl);
      doFileChooser(list);
    }

    /**
     * Invoke File Chooser Service
     * @param list Passed through to fileChooser from caller
     *
     */
    private void doFileChooser(ArrayList list) {
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD, true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, QiProjectManagerConstants.MINUTE_WAIT_TIME);
        if (response == null) {
            logger.warning("Respone to get file chooser service returning null due to timed out");
            showInternalErrorDialog(Thread.currentThread().getStackTrace(), "Failed to start file chooser service");
            return;
        }

        if (response.isAbnormalStatus()) {
            logger.warning("Internal error occurring in getting file chooser service. Cause: " + response.getContent());
            showInternalErrorDialog(Thread.currentThread().getStackTrace(), "Failed to start file chooser service");
            return;
        } else {
            ComponentDescriptor cd = (ComponentDescriptor)response.getContent();

            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_FILE_CHOOSER_CMD, cd, QIWConstants.ARRAYLIST_TYPE,list);
            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD, cd);
            return;
        }
    }

    /**
     * Display an error dialog.
     * @param type is QIWConstants.ERROR_DIALOG or QIWConstants.WARNING_DIALOG
     * @param message is one-line message
     * @param stack is StackTrace
     * @param causes is list of possible causes
     * @param suggestions is list of suggested remedies
     */
    public void showErrorDialog(String type, StackTraceElement[] stack, String message,
            String[] causes, String[] suggestions) {
        ArrayList list = new ArrayList(7);
        // component
        list.add(gui);
        // message type
        list.add(type);
        // caller's display name
        list.add(getComponentDescriptor().getDisplayName());
        // stack trace
        list.add(stack);
        // message
        list.add(message);
        // possible causes
        list.add(causes);
        // suggested remedies
        list.add(suggestions);
        // send message to start error service
        getErrorService(list);
    }

    /**
     * Show an error dialog for an internal error, for example, unexpected NULL etc
     * @param stack stackTrace
     * @param message one-line error message
     */
    public void showInternalErrorDialog(StackTraceElement[] stack, String message) {
        showErrorDialog(QIWConstants.ERROR_DIALOG,stack,message,
                new String[] {"Internal plugin error"},
                new String[] {"Contact workbench support"});
    }

    /**
     * Get an error service.
     * @param list ArrayList of arguments
     */
    private void getErrorService(ArrayList list) {
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_ERROR_DIALOG_SERVICE_CMD,true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, QiProjectManagerConstants.MINUTE_WAIT_TIME);
        if (response == null || response.isAbnormalStatus()) {
            JOptionPane.showMessageDialog(null,"Error getting Error Service");
            return;
        }
        IComponentDescriptor errorServiceDesc = (ComponentDescriptor)response.getContent();
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.INVOKE_ERROR_DIALOG_SERVICE_CMD,
                errorServiceDesc,QIWConstants.ARRAYLIST_TYPE,list,true);
        response = messagingMgr.getMatchingResponseWait(msgID, QiProjectManagerConstants.MINUTE_WAIT_TIME);
    }

    /**
     * Have the State Manager save the state of this qiComponent.
     */
    public void saveState() {
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_CMD,stMgrDesc,
                QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
    }

    /**
     * Have the State Manager save the state of this qiComponent.
     */
    public void saveStateAsClone() {
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_AS_CLONE_CMD,stMgrDesc,
                QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
    }    
    /**
     * Have the State Manager save the state of this qiComponent and then quit the component.
     */
    public void saveStateThenQuit() {
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_THEN_QUIT_CMD,stMgrDesc,
                QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
    }

    /** Launch the qiProjectManager plugin:
     *  <ul>
     *  <li>Start up the plugin thread which will initialize the plugin.</li>
     *  </ul>
     * <p>
     * NOTE: Each thread's init() must finish before the next thread is
     * started. This is accomplished by monitoring the SYNC_LOCK object.
     */
    public static void main(String[] args) {
        QiProjectManagerPlugin pluginInstance = new QiProjectManagerPlugin();
        //CID has to be passed in since no way to get it back out
        String cid = args[0];
        // use the CID as the name of the thread
        pluginThread = new Thread(pluginInstance, cid);
        pluginThread.start();
        long threadId = pluginThread.getId();
        logger.info("qiProjectManager Thread-"+Long.toString(threadId)+" started");
        // When the plugin's init() is finished, it will release the lock

        //  wait until the plugin's init() has finished
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();
        logger.info("qiProjectManager main finished");
    }
}
