/*
###########################################################################
# qiProjectManager - Manage a project's data files and their location.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiProjectManager;

import java.awt.Component;
import java.awt.Point;
import java.awt.dnd.DnDConstants;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.Set;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.bhpb.qiProjectManager.QiProjectManagerConstants;
import com.bhpb.qiProjectManager.DataExplorerModel.DataGroup;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ComponentStateUtils;
import com.bhpb.qiworkbench.compAPI.ComponentUtils;
import com.bhpb.qiworkbench.compAPI.DataDataDescriptor;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.compAPI.QiSpaceDescUtils;
import com.bhpb.qiworkbench.compAPI.XmlUtils;

/**
 * qiProjectManager GUI, a core qiComponent of qiWorkbench.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class QiProjectManagerUI extends javax.swing.JInternalFrame {
    private static Logger logger = Logger.getLogger(QiProjectManagerUI.class.getName());

    Component gui;

    String rootName = "Project Data";

    /** Right-click content menu for Project tree node */
    JPopupMenu projectPopupMenu = new JPopupMenu();
    JMenuItem projectScanPopupMenuItem = new JMenuItem(SCAN_FILTER);

    /** Right-click content menu for file data tree nodes */
    JPopupMenu filePopupMenu = new JPopupMenu();
    JMenuItem fileRemovePopupMenuItem = new JMenuItem("Remove");
    JMenuItem filePreviewPopupMenuItem = new JMenuItem("Preview");
    JMenuItem fileRenamePopupMenuItem = new JMenuItem("Rename");

    /** true if user right clicked; otherwise, false */
    boolean popupTrigger = false;

    /** Last data node selected */
    DefaultMutableTreeNode selectedNode = null;
    /** Name of last data node selected */
    String selectedNodeName = "";
    /** Parent node of selected node */
    DefaultMutableTreeNode selectedNodeParent = null;
    /** Name of parent node of selected node */
    String selectedNodeParentName = "";
    /** true if selected node is the root node; otherwise, false */
    boolean isSelectedNodeRoot = false;
    /** true if parent of selected node is the root node; otherwise, false */
    boolean isSelectedNodeParentRoot = false;

    private QiProjectManagerPlugin agent;

    private ImportDialog importDialog;

    /**
     * Action command for scanned directories. Saved as part of state so data tree can be restored.
     * key = action command; value = action command. Note: Action commands are unique.
     */
    HashMap<String, String> scannedDirs = new HashMap<String, String>();

    /**
     * Tree nodes for each data group. key=group name; value=tree node
     */
    HashMap<String, DefaultMutableTreeNode> groupRoots = new HashMap<String, DefaultMutableTreeNode>();

    /** Model of the data explorer tree and Input menu */
    DataExplorerModel projectDataModel = DataExplorerModel.getInstance();

    //temporary fix for the anonymous inner class worker thread that
    //causes the JTree to only be editable via the rename function
    private static final int QI_PM_UI_WAIT = 500;

    String serverFileSeparator = "/";

    MessagingManager messagingMgr = null;

    /** Creates new Project Manager GUI */
    public QiProjectManagerUI(QiProjectManagerPlugin agent) {
        this.agent = agent;
        // Get my Component for use in locating dialogs
        gui = (Component)this;

        messagingMgr = agent.getMessagingMgr();
        serverFileSeparator = messagingMgr.getServerOSFileSeparator();

        // Parse the definition of a project. Used to populate the data explorer tree.
        // NOTE: Parsing must be done BEFORE calling initComponents()
        // ProjectDataModel is a singleton.  As such, avoid reading the project-defn.xml if
        // one or more additional project managers are opened after the first
        if (projectDataModel.groupIterator().hasNext() == false)
            projectDataModel.parseProjectDefn("/project-defn.xml");

        initComponents();

        // CREATE THE CONTENT POPUP MENUS
        ActionListener listener = new PopupMenuItemActionListener();

        projectPopupMenu.add(projectScanPopupMenuItem);
        projectScanPopupMenuItem.setActionCommand(PROJECT_GROUP+"_"+SCAN_FILTER);
        projectScanPopupMenuItem.addActionListener(listener);

        final HashMap<String, JPopupMenu> popupMenus = new HashMap<String, JPopupMenu>();
        Iterator<DataGroup> groupIterator = projectDataModel.groupIterator();
        while (groupIterator.hasNext()) {
            // create group menu item
            String groupName = groupIterator.next().getGroupName();
            if (groupName.equals(PROJECT_GROUP)) continue;
            JPopupMenu popupMenu = new JPopupMenu();

            // create filter menu items for this group
            JMenuItem menuItem = new JMenuItem();
            String filterName = SCAN_FILTER;
            menuItem.setText(filterName);
            menuItem.setActionCommand(groupName+"_"+filterName);
            menuItem.addActionListener(listener);
            popupMenu.add(menuItem);

            Iterator<String> filterIterator = projectDataModel.filterIterator(groupName);
            while (filterIterator.hasNext()) {
                menuItem = new JMenuItem();
                filterName = filterIterator.next();
                menuItem.setText(filterName);
                menuItem.setActionCommand(groupName+"_"+filterName);
                menuItem.addActionListener(listener);
                popupMenu.add(menuItem);
            }

            popupMenus.put(groupName, popupMenu);
        }

        filePopupMenu.add(fileRemovePopupMenuItem);
        filePopupMenu.add(filePreviewPopupMenuItem);
        filePopupMenu.add(fileRenamePopupMenuItem);
        fileRemovePopupMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileRemovePopupMenuItemActionPerformed(evt);
            }
        });
        filePreviewPopupMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filePreviewPopupMenuItemActionPerformed(evt);
            }
        });
        fileRenamePopupMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileRenamePopupMenuItemActionPerformed(evt);
            }
        });

        // Create the data tree for a project
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(new DataDataDescriptor(rootName));
        createProjectNodes(root);
        projectDataTree = new DragTree(root, dataPreviewerDesktopPane, DnDConstants.ACTION_COPY);
        MouseListener ml = new MouseAdapter() {
            public void mousePressed(MouseEvent evt) {
                int selRow = projectDataTree.getRowForLocation(evt.getX(), evt.getY());
                TreePath selPath = projectDataTree.getPathForLocation(evt.getX(), evt.getY());
                if(selRow != -1) {
                    if(evt.getClickCount() == 1) {

                    }
                    else if(evt.getClickCount() == 2) {
                        QiProjectManagerUI.this.createFrame();
                    }
                }
            }
        };
        projectDataTree.addMouseListener(ml);
        dataExplorerScrollPane.setViewportView(projectDataTree);

        //Set up listener for when a tree node is selected
        projectDataTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        projectDataTree.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                projectDataTreeValueChanged(evt);
            }
        });

        projectDataTree.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                if (me.isPopupTrigger()) {
                    popupTrigger = true;
                }
            }
            public void mouseReleased(MouseEvent me) {
//                System.out.println("isPopupTrigger="+me.isPopupTrigger());
                if (popupTrigger || me.isPopupTrigger()) {
                    popupTrigger = true;
                    int selRow = projectDataTree.getRowForLocation(me.getX(), me.getY());
//                    System.out.println("right clicked tree row="+selRow);
                    if (selRow != -1) {
                        // make sure node is selected
                        projectDataTree.setSelectionRow(selRow);
                        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode)projectDataTree.getLastSelectedPathComponent();
                        if (selectedNode == null) {
                            return;
                        }

                        Object nodeInfo = selectedNode.getUserObject();
                        String nodeName = ((DataDataDescriptor)nodeInfo).getName();

                        DefaultMutableTreeNode parent = (DefaultMutableTreeNode)selectedNode.getParent();
                        if (parent != null && parent.isRoot()) {
                            JPopupMenu popupMenu = popupMenus.get(nodeName);
                            popupMenu.show(projectDataTree, me.getX(), me.getY());
                        } else

                        if (selectedNode.isLeaf()) {
                            filePopupMenu.show(projectDataTree, me.getX(), me.getY());
                        } else

                        if (selectedNode.isRoot()) {
                            projectPopupMenu.show(projectDataTree, me.getX(), me.getY());
                        }
                    }
                }
            }
        });

        //Can't export project info until associate with a project
//        exportMenuItem.setEnabled(false);

        //make frame iconable, maximizable.and resizeable
        this.setIconifiable(true);
        this.setMaximizable(true);
        this.setResizable(true);
    }

    /**
     * Create project data tree with predefined data types. The data types match
     * those found in the project metadata
     **/
    private void createProjectNodes(DefaultMutableTreeNode root) {
        DefaultMutableTreeNode groupRoot = null;

        Iterator<DataGroup> groupIterator = projectDataModel.groupIterator();
        while (groupIterator.hasNext()) {
            String groupName = groupIterator.next().getGroupName();
            if (groupName.equals(PROJECT_GROUP)) continue;
            groupRoot = new DefaultMutableTreeNode(new DataDataDescriptor(groupName));
            root.add(groupRoot);
            groupRoots.put(groupName, groupRoot);
        }
    }

    /**
     * Find the file node specified by its name starting at a root node.
     * @param root Root start to start search
     * @param nodeName Name of the node searching for
     * @return Tree node if found; otherwise, null
     */
    private DefaultMutableTreeNode findNodeByName(DefaultMutableTreeNode root, String nodeName) {
        for (Enumeration e=root.children(); e.hasMoreElements();) {
            DefaultMutableTreeNode child = (DefaultMutableTreeNode)e.nextElement();
            if ((((DataDataDescriptor)child.getUserObject()).getName()).equals(nodeName)) return child;
        }
        return null;
    }

    /**
     * Add file nodes to a data group node. Only add nodes if they don't exist in
     * the group node.
     * @param groupName The name of the data group.
     * @param file List of sorted file names.
     */
    private void addFileNodes(String groupName, ArrayList<DataDataDescriptor> files) {
        DefaultMutableTreeNode dataFile = null;
        DefaultMutableTreeNode groupRoot = groupRoots.get(groupName);
        DefaultTreeModel model = (DefaultTreeModel)projectDataTree.getModel();
        // add all the file nodes if the group node is empty
        if (groupRoot.getChildCount() == 0) {
            for (int i=0; i<files.size(); i++) {
                dataFile = new DefaultMutableTreeNode(files.get(i));
                dataFile.setAllowsChildren(false);
//              groupRoot.add(dataFile);
                //Insert file node as last child of group node
                model.insertNodeInto(dataFile, groupRoot, groupRoot.getChildCount());
            }
        } else {
            // add node only if it is not already in the group
            for (int i=0; i<files.size(); i++) {
                String nodeName = files.get(i).getName();
                if (findNodeByName(groupRoot, nodeName) == null) {
                    dataFile = new DefaultMutableTreeNode(files.get(i));
                    dataFile.setAllowsChildren(false);
                    //Insert file node as last child of group node
//TODO: insert in sorted order
                    model.insertNodeInto(dataFile, groupRoot, groupRoot.getChildCount());
                }
            }
        }
    }

    private static final String PROJECT_GROUP = "Project";
    private static final String SCAN_FILTER = "Scan";

    /**
     * Populate data group node(s) with filtered files.
     * @param actionCmd The group name and filter name concatenated with '_'.
     */
    private void populateFileNodes(String actionCmd) {
        int idx = actionCmd.indexOf('_');
        //internal error if action command of the wrong form
        if (idx == -1) return;
        String groupName = actionCmd.substring(0, idx);
        String filterName = actionCmd.substring(idx+1);

        ArrayList<DataDataDescriptor> files = null;
        HashMap<String, ArrayList<DataDataDescriptor>> filesByGroup = null;

        if (groupName.equals(PROJECT_GROUP)) {
            if (filterName.equals(SCAN_FILTER)) {
                filesByGroup = scanAllGroups();
                Set<String> keys = filesByGroup.keySet();
                Iterator<String> iter = keys.iterator();
                while (iter.hasNext()) {
                    groupName = iter.next();
                    files = filesByGroup.get(groupName);
                    addFileNodes(groupName, files);
                }
            }
        } else
        if (filterName.equals(SCAN_FILTER)) {
            files = scanGroup(groupName);
            addFileNodes(groupName, files);
        } else {
            files = scanFiles(groupName, filterName);
            addFileNodes(groupName, files);
        }
    }

    /**
     * Action listener for all Input menu items. A menu item is distinguished by its action command. An action command is of the form 'group_filter'
     */
    private class InputMenuItemActionListener implements ActionListener {
        public void actionPerformed(ActionEvent ae) {
            String actionCmd = ae.getActionCommand();
            System.out.println("Input actionCmd = "+actionCmd);

            //Save action command
            scannedDirs.put(actionCmd, actionCmd);

            populateFileNodes(actionCmd);
        }
    }

    /**
     * Action listener for all popup menu items. A menu item is distinguished by its action command. An action command is of the form 'group_filter'
     */
    private class PopupMenuItemActionListener implements ActionListener {
        public void actionPerformed(ActionEvent ae) {
            popupTrigger = false;
            String actionCmd = ae.getActionCommand();
            System.out.println("Popup actionCmd = "+actionCmd);

            //Save action command
            scannedDirs.put(actionCmd, actionCmd);

            populateFileNodes(actionCmd);
        }
    }

    // This is where the semantics are built in.
    private static final String DATASETS_GROUP = "Datasets";
    private static final String HORIZONS_GROUP = "Horizons";
    private static final String SCRIPTS_GROUP = "Scripts";
    private static final String TEMP_GROUP = "Temp";
    private static final String WELLS_GROUP = "Wells";
    private static final String SAVESETS_GROUP = "Sessions";
    private static final String WORKBENCHES_GROUP = "Workbenches";
    private static final String XML_GROUP = "XML";
    private static final String SEGY_GROUP = "SEGY";
    private static final String SEISMIC_GROUP = "Seismic";

    /**
     * Get the path of a data group's directory.
     * @param groupName The name of the data group.
     * @return The path of the group's directory.
     */
    private String getGroupPath(String groupName) {
        String groupPath = "";
        QiProjectDescriptor qpDesc = agent.getProjectDescriptor();
        if (groupName.equals(DATASETS_GROUP))
            groupPath = QiProjectDescUtils.getDatasetsPath(qpDesc);
        else if (groupName.equals(HORIZONS_GROUP))
            groupPath = QiProjectDescUtils.getHorizonsPath(qpDesc);
        else if (groupName.equals(SCRIPTS_GROUP))
            groupPath = QiProjectDescUtils.getScriptsPath(qpDesc);
        else if (groupName.equals(TEMP_GROUP))
            groupPath = QiProjectDescUtils.getTempPath(qpDesc);
        else if (groupName.equals(WELLS_GROUP))
            groupPath = QiProjectDescUtils.getWellsPath(qpDesc);
        else if (groupName.equals(SAVESETS_GROUP))
            groupPath = QiProjectDescUtils.getSavesetsPath(qpDesc);
        else if (groupName.equals(WORKBENCHES_GROUP))
            groupPath = QiProjectDescUtils.getWorkbenchesPath(qpDesc);
        else if (groupName.equals(XML_GROUP))
            groupPath = QiProjectDescUtils.getXmlPath(qpDesc);
        else if (groupName.equals(SEGY_GROUP))
            groupPath = QiProjectDescUtils.getSegyPath(qpDesc);
        else if (groupName.equals(SEISMIC_GROUP)) {
            ArrayList<String> seismicPaths = QiProjectDescUtils.getSeismicPaths(qpDesc);
            groupPath = seismicPaths.get(0);
            //make a semicolon separated list of the seismic directories
            //Note: semicolon is not a valid character in a directory name for all OSes.
            for (int i=1; i<seismicPaths.size(); i++) {
                groupPath += ";"+seismicPaths.get(i);
            }
        }

        return groupPath;
    }

    /**
     * Recursively scan a subdirectory on a local server for files based on suffix filter(s).
     * @param dirPath Path of the directory
     * @param relPath Relative path of the directory in the project
     * @param suffixes Suffix filters
     * @param prefix Prefix to file path; not empty string when data group has multiple directories
     * @return A list of filtered files in the subdirectory
     */
    private ArrayList<DataDataDescriptor> scanDir(String dirPath, String relPath, String[] sufs, String prefix) {
        ArrayList<DataDataDescriptor> files = new ArrayList<DataDataDescriptor>();
        File dir = new File(dirPath);

        File[] fileDirs = dir.listFiles();
        for (int i=0; i<fileDirs.length; i++) {
            String name = fileDirs[i].getName();
            String relPath2 = relPath + serverFileSeparator + name;
            if (fileDirs[i].isDirectory()) {
                String path = dirPath + serverFileSeparator + name;
                files.addAll(scanDir(path, relPath2, sufs, prefix));
            } else {
                //filter out files
                for (int j=0; j<sufs.length; j++) {
                    if (name.endsWith(sufs[j])) files.add(new DataDataDescriptor(prefix + relPath2));
                }
            }
        }

        return files;
    }

    /**
     * Recursively scan a subdirectory on a remote server for files based on suffix filter(s).
     * @param dirPath Path of the directory
     * @param relPath Relative path of the directory in the project
     * @param suffixes Suffix filters
     * @param prefix Prefix to file path; not empty string when data group has multiple directories
     * @return A list of filtered files in the subdirectory
     */
    private ArrayList<DataDataDescriptor> scanRemoteDir(String dirPath, String relPath, String[] sufs, String prefix) {
        ArrayList<DataDataDescriptor> files = new ArrayList<DataDataDescriptor>();

        ArrayList<String> list = new ArrayList<String>();
        list.add(QIWConstants.REMOTE_SERVICE_PREF);
        list.add(dirPath);
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_DIR_FILE_LIST_CMD,QIWConstants.ARRAYLIST_TYPE, list, true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId, QiProjectManagerConstants.MINUTE_WAIT_TIME);

        ArrayList<ArrayList> dirFileList = null;

        if (response == null) {
            logger.warning("Returning null response. Check to see if this is a race condition");
            return files;
        }

        if (response.isAbnormalStatus()) {
            logger.warning("Error in running GET_DIR_FILE_LIST_CMD command. caused by " + (String)response.getContent());
            JOptionPane.showMessageDialog(null, "Error in getting dirctory and file list from " + dirPath + ". Cause: " + (String)response.getContent(), "Project Manager",
                        JOptionPane.WARNING_MESSAGE);

            return files;
        }
        dirFileList = (ArrayList<ArrayList>)response.getContent();
        ArrayList<String> dirList = dirFileList.get(0);  //the first element is directory list
        ArrayList<String> fileList = dirFileList.get(1);  //the second element is file list

        //filter out files
        for (int i=0; i<fileList.size(); i++) {
            String name = fileList.get(i);
            String relPath2 = relPath + serverFileSeparator + name;
            for (int j=0; j<sufs.length; j++) {
                if (name.endsWith(sufs[j])) files.add(new DataDataDescriptor(prefix + relPath2));
            }
        }

        //Recursively get files in directories
        for (int i=0; i<dirList.size(); i++) {
            String name = dirList.get(i);
            String relPath2 = relPath + serverFileSeparator + name;
            String path = dirPath + serverFileSeparator + name;
            files.addAll(scanRemoteDir(path, relPath2, sufs, prefix));
        }

        return files;
    }

    /**
     * Scan a data group for files based on suffix filter(s).
     * @param groupName The name of the group node.
     * @param filterName A comma separated list of suffixes
     * @return A sorted list of filtered files in that group.
     */
    private ArrayList<DataDataDescriptor> scanFiles(String groupName, String filterName) {
        QiProjectDescriptor qpDesc = agent.getProjectDescriptor();
        String projectPath = QiProjectDescUtils.getProjectPath(qpDesc);
        int projPathLen = projectPath.length()+1;

        ArrayList<DataDataDescriptor> files = new ArrayList<DataDataDescriptor>();

        //Get full path of group directory
        String groupPath = getGroupPath(groupName);

        //Check if a list of paths separated by a semicolon
        String paths[];
        if (groupPath.contains(";")) {
            paths = groupPath.split(";");
        } else {
            paths = new String[1];
            paths[0] = groupPath;
        }

        for (int k=0; k<paths.length; k++) {
            String prefix = "";
            groupPath = paths[k];
            if (k > 0) {
                //Note: group path starts with the project path
                prefix = groupPath.substring(projPathLen) + serverFileSeparator;
            }

            File groupDir = new File(groupPath);

            String suffixes = projectDataModel.getSuffixes(groupName, filterName);
            String[] sufs = suffixes.split(",");

            boolean isLocalServer = messagingMgr.getLocationPref().equals(QIWConstants.LOCAL_SERVICE_PREF);
            if (isLocalServer) {
                if (!groupDir.exists()) return files;

                File[] fileDirs = groupDir.listFiles();
                for (int i=0; i<fileDirs.length; i++) {
                    String name = fileDirs[i].getName();
                    if (fileDirs[i].isDirectory()) {
                        String dirPath = groupPath + serverFileSeparator + name;
                        files.addAll(scanDir(dirPath, name, sufs, prefix));
                    } else {
                        //filter out files
                        for (int j=0; j<sufs.length; j++) {
                            if (name.endsWith(sufs[j])) files.add(new DataDataDescriptor(prefix+name, groupName, groupPath));
                        }
                    }
                }
            } else { //remote runtime Tomcat server
                ArrayList<String> list = new ArrayList<String>();
                list.add(QIWConstants.REMOTE_SERVICE_PREF);
                list.add(groupPath);
                String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_DIR_FILE_LIST_CMD,QIWConstants.ARRAYLIST_TYPE, list, true);
                IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId, QiProjectManagerConstants.MINUTE_WAIT_TIME);

                ArrayList<ArrayList> dirFileList = null;

                if (response == null) {
                    logger.warning("Returning null response. Check to see if this is a race condition");
                    return files;
                }

                if (response.isAbnormalStatus()) {
                    logger.warning("Error in running GET_DIR_FILE_LIST_CMD command. caused by " + (String)response.getContent());
                    JOptionPane.showMessageDialog(null, "Error in getting dirctory and file list from " + groupPath + ". Cause: " + (String)response.getContent(), "Project Manager",
                                JOptionPane.WARNING_MESSAGE);

                    return files;
                }
                dirFileList = (ArrayList<ArrayList>)response.getContent();
                ArrayList<String> dirList = dirFileList.get(0);  //the first element is directory list
                ArrayList<String> fileList = dirFileList.get(1);  //the second element is file list

                //filter out files
                for (int i=0; i<fileList.size(); i++) {
                    String name = fileList.get(i);
                    for (int j=0; j<sufs.length; j++) {
                        if (name.endsWith(sufs[j])) files.add(new DataDataDescriptor(prefix+name, groupName, groupPath));
                    }
                }

                //Recursively get files in directories
                for (int i=0; i<dirList.size(); i++) {
                    String name = dirList.get(i);
                    String dirPath = groupPath + serverFileSeparator + name;
                    files.addAll(scanRemoteDir(dirPath, name, sufs, prefix));
                }
            }
        }

        //sort files
        Object[] fileList = files.toArray();
        java.util.Arrays.sort(fileList);
        files.clear();
        for (int i = 0; i < fileList.length; i++)
            files.add((DataDataDescriptor)fileList[i]);

        return files;
    }

    /**
     * Scan a data group for files based on all filters.
     * @param groupName The name of the group node.
     * @return A sorted list of files in that group.
     */
    private ArrayList<DataDataDescriptor> scanGroup(String groupName) {
        ArrayList<DataDataDescriptor> files = new ArrayList<DataDataDescriptor>();

        Iterator<String> filterIterator = projectDataModel.filterIterator(groupName);
        while (filterIterator.hasNext()) {
            String filterName = filterIterator.next();
            ArrayList<DataDataDescriptor> fileList = scanFiles(groupName, filterName);
            files.addAll(fileList);
        }

        //sort files
        Object[] fileList = files.toArray();
        java.util.Arrays.sort(fileList);
        files.clear();
        for (int i = 0; i < fileList.length; i++)
            files.add((DataDataDescriptor)fileList[i]);

        return files;
    }

    /**
     * Scan all data groups for files based on all filters.
     * @return A sorted list of files in that group.
     */
    public HashMap<String, ArrayList<DataDataDescriptor>> scanAllGroups() {
        HashMap<String, ArrayList<DataDataDescriptor>> filesByGroup = new HashMap<String, ArrayList<DataDataDescriptor>>();

        Iterator<DataGroup> groupIterator = projectDataModel.groupIterator();
        while (groupIterator.hasNext()) {
            String groupName = groupIterator.next().getGroupName();
            if (groupName.equals(PROJECT_GROUP)) continue;
            ArrayList<DataDataDescriptor> files = scanGroup(groupName);
            filesByGroup.put(groupName, files);
        }

        return filesByGroup;
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        new Thread() {
            public void run() {
                while(projectDataTree==null) {
                }
                while(true) {
                    if(projectDataTree.isEditing()) {
                        flag = true;
                    } else if(flag) {
                        //System.out.println("Testing flag Test.");
                        projectDataTree.setEditable(false);
                        flag = false;
                        System.out.println(QiProjectManagerUI.this.selectedNodeName);
                        if(isFrameOpen(QiProjectManagerUI.this.selectedNodeName)) {

                        }
                    }
        try {
            Thread.sleep(QI_PM_UI_WAIT);
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        }
                }
            }
        }.start();
        dataSplitPane = new javax.swing.JSplitPane();
        dataExplorerScrollPane = new javax.swing.JScrollPane();
        projectDataTree = new javax.swing.JTree();
        //dataPreviewerScrollPane = new javax.swing.JScrollPane();
        //dataPreviewerDesktopPane = new javax.swing.JDesktopPane();
        dataPreviewerDesktopPane = new DropDesktopPane(DnDConstants.ACTION_COPY, this);
        dataPreviewerDesktopPane.setBackground(new java.awt.Color(204, 204, 204));
        projectManagerMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        saveMenuItem = new javax.swing.JMenuItem();
        saveAsMenuItem = new javax.swing.JMenuItem();
        saveQuitMenuItem = new javax.swing.JMenuItem();
        quitMenuItem = new javax.swing.JMenuItem();
        projectMenu = new javax.swing.JMenu();
        importMenuItem = new javax.swing.JMenuItem();
        exportMenuItem = new javax.swing.JMenuItem();
        projectMenuSeparator = new javax.swing.JSeparator();
        defineMenuItem = new javax.swing.JMenuItem();
        projectScanMenuItem = new javax.swing.JMenuItem();
        inputMenu = new javax.swing.JMenu();
        helpMenu = new javax.swing.JMenu();
        topicsMenuItem = new javax.swing.JMenuItem();
        faqsMenuItem = new javax.swing.JMenuItem();
        helpMenuSeparator = new javax.swing.JSeparator();
        aboutMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setName("projectFrame");
        dataSplitPane.setLastDividerLocation(-1);
        dataSplitPane.setOneTouchExpandable(true);
        dataSplitPane.setRequestFocusEnabled(false);
        dataExplorerScrollPane.setMinimumSize(new java.awt.Dimension(100, 100));
        dataExplorerScrollPane.setPreferredSize(new java.awt.Dimension(164, 191));
        projectDataTree.setName("ProjectData");
        projectDataTree.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                projectDataTreeValueChanged(evt);
            }
        });

        dataExplorerScrollPane.setViewportView(projectDataTree);

        dataSplitPane.setLeftComponent(dataExplorerScrollPane);

        dataSplitPane.setRightComponent(dataPreviewerDesktopPane);

        // CREATE FILE MENU
        fileMenu.setMnemonic('F');
        fileMenu.setText("File");
        fileMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileMenuActionPerformed(evt);
            }
        });


        importMenuItem.setMnemonic('I');
        importMenuItem.setText("Import");
        importMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importMenuItemActionPerformed(evt);
            }
        });

        fileMenu.add(importMenuItem);

        exportMenuItem.setMnemonic('E');
        exportMenuItem.setText("Export");
        exportMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exportMenuItem);

        fileMenu.add(projectMenuSeparator);

        quitMenuItem.setMnemonic('Q');
        quitMenuItem.setText("Quit");
        quitMenuItem.setToolTipText("Select to Quit");
        quitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitMenuitemActionPerformed(evt);
            }
        });

        fileMenu.add(quitMenuItem);


        saveMenuItem.setMnemonic('S');
        saveMenuItem.setText("Save");
        saveMenuItem.setToolTipText("Select to Save state");
        saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuItemActionPerformed(evt);
            }
        });

        fileMenu.add(saveMenuItem);

        saveAsMenuItem.setMnemonic('A');
        saveAsMenuItem.setText("Save As");
        saveAsMenuItem.setToolTipText("Select to Save state as a clone");
        saveAsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsMenuItemActionPerformed(evt);
            }
        });

        fileMenu.add(saveAsMenuItem);
        
        saveQuitMenuItem.setMnemonic('U');
        saveQuitMenuItem.setText("Save, Quit");
        saveQuitMenuItem.setToolTipText("Select to Save state then Quit");
        saveQuitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveQuitMenuItemActionPerformed(evt);
            }
        });

        fileMenu.add(saveQuitMenuItem);


        projectManagerMenuBar.add(fileMenu);

        // CREATE PROJECT MENU
        projectMenu.setMnemonic('P');
        projectMenu.setText("Project");
        projectMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                projectMenuActionPerformed(evt);
            }
        });



        defineMenuItem.setMnemonic('D');
        defineMenuItem.setText("Define");
        defineMenuItem.setToolTipText("Select to define project metadata");
        defineMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                defineMenuItemActionPerformed(evt);
            }
        });
        projectMenu.add(defineMenuItem);

        projectManagerMenuBar.add(projectMenu);

        inputMenu.setMnemonic('I');
        inputMenu.setText("Input");
        inputMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputMenuActionPerformed(evt);
            }
        });

        // CREATE INPUT MENU
        // TODO: set Mnemonics - setMnemonic('X')
        ActionListener listener = new InputMenuItemActionListener();

        Iterator<DataGroup> groupIterator = projectDataModel.groupIterator();
        while (groupIterator.hasNext()) {
            // create group menu item
            String groupName = groupIterator.next().getGroupName();
            JMenu menu = new JMenu();
            menu.setText(groupName);

            // create filter menu items for this group
            JMenuItem menuItem = new JMenuItem();
            String filterName = SCAN_FILTER;
            menuItem.setText(filterName);
            menuItem.setActionCommand(groupName+"_"+filterName);
            menuItem.addActionListener(listener);
            menu.add(menuItem);

            Iterator<String> filterIterator = projectDataModel.filterIterator(groupName);
            while (filterIterator.hasNext()) {
                menuItem = new JMenuItem();
                filterName = filterIterator.next();
                menuItem.setText(filterName);
                menuItem.setActionCommand(groupName+"_"+filterName);
                menuItem.addActionListener(listener);
                menu.add(menuItem);
            }

            inputMenu.add(menu);

            if (groupName.equals(PROJECT_GROUP)) {
                inputMenu.add(new JSeparator());
            }
        }

        projectManagerMenuBar.add(inputMenu);

        // CREATE HELP MENU
        helpMenu.setMnemonic('H');
        helpMenu.setText("Help");
        helpMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                helpMenuActionPerformed(evt);
            }
        });

        topicsMenuItem.setMnemonic('T');
        topicsMenuItem.setText("Topics");
        topicsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                topicsMenuItemActionPerformed(evt);
            }
        });

        helpMenu.add(topicsMenuItem);

        faqsMenuItem.setMnemonic('F');
        faqsMenuItem.setText("FAQs");
        faqsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                faqsMenuItemActionPerformed(evt);
            }
        });

        helpMenu.add(faqsMenuItem);

        helpMenu.add(helpMenuSeparator);

        aboutMenuItem.setMnemonic('A');
        aboutMenuItem.setText("About...");
        aboutMenuItem.setToolTipText("View version and license information");
        final Component comp = this;
        aboutMenuItem.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(props == null){
                    String compName = agent.getMessagingMgr().getRegisteredComponentDisplayNameByDescriptor(agent.getComponentDescriptor());
                    props = ComponentUtils.getComponentVersionProperies(compName);
                }
                new AboutListener(comp,props);
            }
        });

        helpMenu.add(aboutMenuItem);

        projectManagerMenuBar.add(helpMenu);

        setJMenuBar(projectManagerMenuBar);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(dataSplitPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 931, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(dataSplitPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 687, Short.MAX_VALUE)
        );

        // reset window title
        QiProjectDescriptor qpDesc = agent.getProjectDescriptor();
        String projectName = QiProjectDescUtils.getQiProjectName(qpDesc);
        resetTitle(projectName);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_aboutMenuItemActionPerformed

    private void faqsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_faqsMenuItemActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_faqsMenuItemActionPerformed

    private void topicsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_topicsMenuItemActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_topicsMenuItemActionPerformed

    private void projectScanMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_projectScanMenuItemActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_projectScanMenuItemActionPerformed

    private void defineMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_defineMenuItemActionPerformed
        QiProjectDescriptor oldProjDesc = agent.getProjectDescriptor();
        QiProjectDescriptor cloneDesc = (QiProjectDescriptor)oldProjDesc.clone();
        int status = showQiProjectDescriptorEditor(gui, true, oldProjDesc, agent);
        //nothing to do if edit cancelled
        if (status == JOptionPane.CANCEL_OPTION) return;

        //get updated project descriptor
        QiProjectDescriptor newProjDesc = agent.getProjectDescriptor();
        //compare old with new. If nothing changed, return
        if (cloneDesc.compareTo(newProjDesc) == 0) return;

        String invalidList = validateProjInfo(cloneDesc, newProjDesc);
        if (!invalidList.equals("")) {
            JOptionPane.showMessageDialog(gui,
                            invalidList+" not within qiSpace. Create and/or edit again",
                            "Edit Warning", JOptionPane.WARNING_MESSAGE);
            //Let changes propagate to qiComponents. If the change issues are corrected
            //they will be propagated. When scripts are generated, directories that don't
            //exist will be created.
//            return;
        }

        //update title of PM window
        String projectName = QiProjectDescUtils.getQiProjectName(newProjDesc);
        resetTitle(projectName);

        //Send message to the MessageDispatcher to notify all non-PM qiComponents
        //a project's metadata has been changed.
        ArrayList args = new ArrayList();
        args.add(agent.getComponentDescriptor());
        args.add(newProjDesc);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.NOTIFY_PROJ_INFO_CHANGED_CMD, QIWConstants.ARRAYLIST_TYPE, args);
    }//GEN-LAST:event_defineMenuItemActionPerformed

    private void exportMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportMenuItemActionPerformed
        //Use a file chooser to specify the path of the file where the qiComponent's state will be saved.
        QiProjectDescriptor projDesc = agent.getProjectDescriptor();
        ComponentStateUtils.callFileChooser(gui, QiProjectDescUtils.getXmlPath(projDesc), QIWConstants.EXPORT_COMP_STATE, agent.getComponentDescriptor(), messagingMgr);

        //NOTE PM agent will invoke a file chooser service. When it processes the GET_RESULT_FROM_FILE_CHOOSER_CMD,
        //     it will capture the state of the PM in XML by invoking its genState() method. Then it will
        //     invoke an IO service to write the state to a .xml file.
    }//GEN-LAST:event_exportMenuItemActionPerformed

    private void importMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importMenuItemActionPerformed
        //Create the Import dialog
        javax.swing.JDialog.setDefaultLookAndFeelDecorated(true);
        importDialog = new ImportDialog(agent);
        importDialog.setSize(740, 330);
        importDialog.setDefaultCloseOperation(javax.swing.JDialog.DISPOSE_ON_CLOSE);
        importDialog.setLocation(200, 100);
        importDialog.setVisible(true);
    }//GEN-LAST:event_importMenuItemActionPerformed

    private void quitMenuitemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitMenuitemActionPerformed
        agent.deactivateSelf();
    }//GEN-LAST:event_quitMenuitemActionPerformed

    private void saveQuitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveQuitMenuItemActionPerformed
        agent.saveStateThenQuit();
    }//GEN-LAST:event_saveQuitMenuItemActionPerformed

    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuItemActionPerformed
        agent.saveState();
    }//GEN-LAST:event_saveMenuItemActionPerformed

    private void saveAsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsMenuItemActionPerformed
        agent.saveStateAsClone();
    }//GEN-LAST:event_saveAsMenuItemActionPerformed
    
    private void projectDataTreeValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_projectDataTreeValueChanged
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode)projectDataTree.getLastSelectedPathComponent();
        if (selectedNode == null) return;

        Object nodeInfo = selectedNode.getUserObject();
        String nodeName = ((DataDataDescriptor)nodeInfo).getName();
        DefaultMutableTreeNode parent = (DefaultMutableTreeNode)selectedNode.getParent();

        // gather info about the selected node and its parent
        this.selectedNode = selectedNode;
        this.selectedNodeName = nodeName;
        this.selectedNodeParent = parent;
        this.isSelectedNodeRoot = selectedNode.isRoot();
        //this.selectedNodeParentName = this.isSelectedNodeRoot ? "" : parent.getUserObject().toString();
        this.selectedNodeParentName = this.isSelectedNodeRoot ? "" : ((DataDataDescriptor)(parent.getUserObject())).getName();
        this.isSelectedNodeParentRoot = selectedNode.isRoot() ? false : parent.isRoot();

        //check if user right-clicked on node
        if (popupTrigger) {
            popupTrigger = false;

            if (selectedNode.isRoot()) {
                System.out.println("Right selected root node="+nodeName);
            } else

            if (parent.isRoot()) {
                System.out.println("Right selected data type node="+nodeName);
            } else

            if (selectedNode.isLeaf()) {
                System.out.println("Right selected leaf node="+nodeName);
            }
        } else {
            if (selectedNode.isRoot()) {
                System.out.println("Selected root node="+nodeName);
            } else

            if (parent.isRoot()) {
                System.out.println("Selected data type node="+nodeName);
            } else

            if (selectedNode.isLeaf()) {
                System.out.println("Selected leaf node="+nodeName);
            }
        }
    }//GEN-LAST:event_projectDataTreeValueChanged

    private void helpMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_helpMenuActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_helpMenuActionPerformed

    private void inputMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputMenuActionPerformed
        System.out.println("Input menu selected");
    }//GEN-LAST:event_inputMenuActionPerformed

    private void projectMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_projectMenuActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_projectMenuActionPerformed

    private void fileMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileMenuActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_fileMenuActionPerformed

    private void fileRemovePopupMenuItemActionPerformed(java.awt.event.ActionEvent evt){
        this.popupTrigger = false;

        DefaultTreeModel model = (DefaultTreeModel)projectDataTree.getModel();

        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode)projectDataTree.getLastSelectedPathComponent();
        if (selectedNode == null) return;

        System.out.println("Remove file: "+this.selectedNodeName+" from "+this.selectedNodeParentName);

        model.removeNodeFromParent(selectedNode);
    }

    private void filePreviewPopupMenuItemActionPerformed(java.awt.event.ActionEvent evt){
        this.popupTrigger = false;
        // take preview action based on parent of node
        System.out.println("Preview file: "+this.selectedNodeName+" in "+this.selectedNodeParentName);
        createFrame();
    }

    protected void createFrame() {
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode)projectDataTree.getLastSelectedPathComponent();
        if (selectedNode == null) {
            return;
        }
;
        Object nodeInfo = selectedNode.getUserObject();
        String nodeName = ((DataDataDescriptor)nodeInfo).getName();
        String nodePath = ((DataDataDescriptor)nodeInfo).getPath();
        //System.out.println("nodeName: " + nodeName + " nodePath: " + nodePath + " getAllowsChildren: " + selectedNode.getAllowsChildren());
        if(selectedNode.getAllowsChildren()) {
            return;
        } else if(isFrameOpen(nodeName)) {
            return;
        }

        PreviewWindow frame = new PreviewWindow(nodeName, nodePath);
        frame.setVisible(true);
        dataPreviewerDesktopPane.add(frame);
        //dataPreviewerScrollPane.validate();
        try {
            frame.setSelected(true);
        } catch (java.beans.PropertyVetoException e) {}
    }

    protected void createFrame(Point point) {
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode)projectDataTree.getLastSelectedPathComponent();
        if (selectedNode == null) {
            return;
        }
;
        Object nodeInfo = selectedNode.getUserObject();
        String nodeName = ((DataDataDescriptor)nodeInfo).getName();
        String nodePath = ((DataDataDescriptor)nodeInfo).getPath();
        //System.out.println("nodeName: " + nodeName + " nodePath: " + nodePath + " getAllowsChildren: " + selectedNode.getAllowsChildren());
        if(selectedNode.getAllowsChildren()) {
            return;
        } else if(isFrameOpen(nodeName)) {
            return;
        }

        PreviewWindow frame = new PreviewWindow(nodeName, nodePath, point);
        frame.setVisible(true);
        dataPreviewerDesktopPane.add(frame);
        //dataPreviewerScrollPane.validate();
        try {
            frame.setSelected(true);
        } catch (java.beans.PropertyVetoException e) {}
    }

    protected void createFrame(DataDataDescriptor DDD, Point point) {
        String nodeName = DDD.getName();
        String nodePath = DDD.getPath();
        JInternalFrame[] internalFrames = dataPreviewerDesktopPane.getAllFrames();

        if(isFrameOpen(nodeName)) {
            return;
        }

        //System.out.println("nodeName: " + nodeName + " nodePath: " + nodePath);
        PreviewWindow frame = new PreviewWindow(nodeName, nodePath, point);
        frame.setVisible(true);
        dataPreviewerDesktopPane.add(frame);
        //dataPreviewerScrollPane.validate();
        try {
            frame.setSelected(true);
        } catch (java.beans.PropertyVetoException e) {}
    }

    protected boolean isFrameOpen(String nodeName) {
        JInternalFrame[] internalFrames = dataPreviewerDesktopPane.getAllFrames();
        for(int i=0; i<internalFrames.length; i++) {
            if(nodeName.equals(internalFrames[i].getTitle())) {
                //internalFrames[i].toFront();
                try{
                    internalFrames[i].setSelected(true);
                } catch(java.beans.PropertyVetoException pve) {}
                return true;
            }
        }
        return false;
    }

    private void fileRenamePopupMenuItemActionPerformed(java.awt.event.ActionEvent evt){
        this.popupTrigger = false;
        // take preview action based on parent of node
        projectDataTree.setEditable(true);
        System.out.println("Rename file: "+this.selectedNodeName+" in "+this.selectedNodeParentName);
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode)projectDataTree.getLastSelectedPathComponent();
        if (selectedNode == null) {
            return;
        }
        projectDataTree.startEditingAtPath(projectDataTree.getSelectionPath());
    }

    /**
     * Show a dialog editor to edit the QiProjectDescriptor.
     * Note: No check is made to determine if the seismic directory is already on the list.
     * @param comp: parent GUI component which requests this dialog
     * @param modal: indicates if this dialog is modal
     * @param desc: a instance of the QiProjectDescriptor to be modified.
     * @return Exit status of dialog, i.e., whether it was cancelled or not.
     */
    private static int showQiProjectDescriptorEditor(Component comp, boolean modal, QiProjectDescriptor desc, QiProjectManagerPlugin agent) {
        EditProjectDirStructureDialog dialog = new EditProjectDirStructureDialog(comp, modal, desc, agent);
        dialog.setLocationRelativeTo(comp);
        dialog.setVisible(true);
        return dialog.getReturnStatus();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new QiProjectManagerUI(null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JScrollPane dataExplorerScrollPane;
    //private javax.swing.JScrollPane dataPreviewerScrollPane;
    private javax.swing.JDesktopPane dataPreviewerDesktopPane;
    private javax.swing.JSplitPane dataSplitPane;
    private javax.swing.JMenuItem defineMenuItem;
    private javax.swing.JMenuItem exportMenuItem;
    private javax.swing.JMenuItem faqsMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JSeparator helpMenuSeparator;
    private javax.swing.JMenuItem importMenuItem;
    private javax.swing.JMenu inputMenu;
    private javax.swing.JTree projectDataTree;
    private javax.swing.JMenuBar projectManagerMenuBar;
    private javax.swing.JMenu projectMenu;
    private javax.swing.JSeparator projectMenuSeparator;
    private javax.swing.JMenuItem projectScanMenuItem;
    private javax.swing.JMenuItem quitMenuItem;
    private javax.swing.JMenuItem saveMenuItem;
    private javax.swing.JMenuItem saveAsMenuItem;
    private javax.swing.JMenuItem saveQuitMenuItem;
    private javax.swing.JMenuItem topicsMenuItem;
    private Properties props;
    // End of variables declaration//GEN-END:variables

    /**
     * Convert scanned actions to a comma delimited list.
     * @return Comma delimited list of scan actions.
     */
    private String getScanActionList() {
        String list = "";

        Set<String> keys = scannedDirs.keySet();
        Iterator<String> iter = keys.iterator();
        int n = scannedDirs.size(), i = 0;
        while (iter.hasNext()) {
            String actionCmd = iter.next();
            list += i==0 ? actionCmd : ","+actionCmd;
            i++;
        }

        return list;
    }

    /**
     * Save the state information for the qiProjectManager GUI.
     *
     * @return Saved state as a XML string.
     */
    public String saveState() {
        StringBuffer content = new StringBuffer();
        content.append("<" + this.getClass().getName() + " ");

        //save position in workbench canvas
        content.append("height=\"" + this.getHeight() + "\" ");
        content.append("width=\"" + this.getWidth() + "\" ");
        content.append("x=\"" + this.getX() + "\" ");
        content.append("y=\"" + this.getY() + "\" ");

        //save the scanned directories
        content.append("scanActions=\"" + getScanActionList() + "\" >\n");

        //save the project descriptor
        String pdXml = XmlUtils.objectToXml(agent.getProjectDescriptor());
        content.append(pdXml);

        content.append("</" + this.getClass().getName() + ">\n");

        return content.toString();
    }

    /**
     * Get scan actions and restore scanned directories in data tree..
     * @param Comma delimited list of scan actions
     */
    private void restoreScanActions(String actionList) {
        scannedDirs.clear();
        if (actionList.equals("")) return;

        String[] actions = actionList.split(",");
        for (int i=0; i<actions.length; i++) {
            String action = actions[i];
            scannedDirs.put(action, action);
            populateFileNodes(action);
        }
    }

    /**
     * Restore the state of the qiProjectManager GUI.
     * <p>
     * NOTE: The project descriptor is restored separately (by the qiProjectManager agent)
     * before restoring the rest of the state, because the GUI builder needs it.
     *
     * @param node XML tree node denoting the GUI element.
     */
    public void restoreState(Node node) {
        Element el = (Element)node;

        //restore position of window in workbench canbas
        String hStr = el.getAttribute("height");
        if (hStr.trim().length() == 0)
            hStr = "298";  //default
        int height = Integer.valueOf(hStr).intValue();
        String wStr = el.getAttribute("width");
        if (wStr.trim().length() == 0)
            wStr = "761";  //default
        int width = Integer.valueOf(wStr).intValue();
        String X = el.getAttribute("x");
        if (X.trim().length() == 0)
            X = "0";    //default
        int x = Integer.valueOf(X).intValue();
        String Y = el.getAttribute("y");
        if (Y.trim().length() == 0)
            Y = "0";  //default
        int y = Integer.valueOf(Y).intValue();
        this.setSize(width,height);
        this.setLocation(x,y);

        //restore the data tree, i.e., the scanned directories
        String actionCmds = el.getAttribute("scanActions");
        restoreScanActions(actionCmds);
    }

    /**
     * Rename the qiComponent.
     *
     * @param name User specified name of qiComponent.
     */
    public void renamePlugin(String name) {
//        this.setTitle(name + "  Project: " + "");
        QiProjectDescriptor qpDesc = agent.getProjectDescriptor();
        String project = QiProjectDescUtils.getQiProjectName(qpDesc);
        resetTitle(project);
    }

    /**
     * Reset the PM's title
     *
     * @param name Name of the associated project.
     */
    public void resetTitle(String projName) {
        String pmName = "";
        IComponentDescriptor agentDesc = messagingMgr.getMyComponentDesc();
        String pdn = CompDescUtils.getDescPreferredDisplayName(agentDesc);
        String screenName = CompDescUtils.getDescDisplayName(agentDesc);
        if (pdn.trim().length() > 0)
            pmName = pdn.trim();
        else
            pmName = screenName;

        //get the type of qiComponent
        int idx = screenName.indexOf("#");
        if (idx != -1) screenName = screenName.substring(0, idx);
        this.setTitle(screenName+": " + pmName + "  Project: " + projName);
    }

    /**
     * Close GUI and dispose, called by plug-in when user has terminated GUI.
     */
    public void closeGUI() {
        setVisible(false);
        dispose();
    }

    /**
     * Disable importing of a project.
     */
    public void disableImporting() {
        importMenuItem.setEnabled(false);
    }

    /**
     * Enable exporting of project information.
     */
    public void enableExporting() {
        exportMenuItem.setEnabled(true);
    }

    /**
     * Validate project metadata that was changed. Verify each relative directory is
     * within the qiSpace. It is assumed the metadata changed.
     * Note: If a relative directory was obtain by a file selector, it is within the qiSpace by definition.
     * If a relative directory was entered, it may not be within the qiSpace.
     * @param oldDesc Original project metadata before it was changed.
     * @param projDesc Project metadata
     * @return empty string if passed validation; otherwise, comma separate list of invalid directories
     */
    private String validateProjInfo(QiProjectDescriptor oldProjDesc, QiProjectDescriptor newProjDesc) {
        boolean isLocalServer = messagingMgr.getLocationPref().equals(QIWConstants.LOCAL_SERVICE_PREF);
        if (isLocalServer) {
            return oldProjDesc.diffTo(newProjDesc);
        } else {
            return remoteDiffTo(oldProjDesc, newProjDesc);
        }
    }

    /**
     * Extract the relative location of the project within the qiSpace.
     * @param projDesc Project metadata
     * @return Project root within the qiSpace.
     */
    private String getProjRoot(QiProjectDescriptor projDesc) {
        String projName = "";

        String qispacePath = QiSpaceDescUtils.getQiSpacePath(messagingMgr.getQispaceDesc());
        String projPath = QiProjectDescUtils.getProjectPath(projDesc);

        //Extract the name of the project which is the name of its directory
        int idx = projPath.lastIndexOf(serverFileSeparator);
        if (idx != -1) {
            projName = projPath.substring(idx+1);
            QiProjectDescUtils.setQiProjectName(projDesc, projName);
        }

        //Set the relative path of the project within the qiSpace
        if (!projPath.equals(qispacePath)) {
            QiProjectDescUtils.setQiProjectReloc(projDesc, projName);
        }

        return projName;
    }

    /**
     * Set the full path of the XML state file. Path obtained by Select in the Import dialog.
     * @param statePath Full path of the XML state file
     */
    public void setStatePath(String statePath) {
        importDialog.setStatePath(statePath);
    }

    /**
     * Set the qiSpace and relative location of the copied project.
     * @param copieProjPath Full path of the copied project obtained by Browse in
     * the Import dialog..
     */
    public void setNewProject(String copiedProjPath) {
        importDialog.setNewProject(copiedProjPath);
    }

    private boolean flag = false;

    /**
     * Diff two project descriptors for valid directories. Is is assumed the qiSpaces are
     * identical because it cannot be changed. Only directories whose relative path has
     * changed are checked to exist and be a directory. The directories must be on a
     * remote runtime Tomcat server.
     * @param oldPd old project desscriptor
     * @param newPd new project descriptor to diff against
     * @return comma separated list of values (in the input argument) that are different
     */
    private String remoteDiffTo(QiProjectDescriptor oldPd, QiProjectDescriptor newPd) {
        String diffList = "";
        boolean empty = true;
        QiProjectDescriptor qpDesc = agent.getProjectDescriptor();
        String qiSpace = QiProjectDescUtils.getQiSpace(qpDesc);
        String dirPath = qiSpace + serverFileSeparator;
        String isDir = "";

        String newVal = newPd.getQiProjectReloc();
        String oldVal = oldPd.getQiProjectReloc();
        if (!oldVal.equals(newVal)) {
            String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.IS_REMOTE_DIRECTORY_CMD, QIWConstants.STRING_TYPE, dirPath+newVal, true);
            IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId, QiProjectManagerConstants.MINUTE_WAIT_TIME);

            if (response != null && !response.isAbnormalStatus()) {
                isDir = (String)response.getContent();
                if (isDir.equals("no")) {
                    diffList += empty ? newVal : ","+newVal;
                    empty = false;
                }
            } else {
                diffList += empty ? newVal+"(unsure)" : ","+newVal+"(unsure)";
                empty = false;
            }
        }

        newVal = newPd.getDatasetsReloc();
        oldVal = oldPd.getDatasetsReloc();
        if (!oldVal.equals(newVal)) {
            String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.IS_REMOTE_DIRECTORY_CMD, QIWConstants.STRING_TYPE, dirPath+newVal, true);
            IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId, QiProjectManagerConstants.MINUTE_WAIT_TIME);

            if (response != null && !response.isAbnormalStatus()) {
                isDir = (String)response.getContent();
                if (isDir.equals("no")) {
                    diffList += empty ? newVal : ","+newVal;
                    empty = false;
                }
            } else {
                diffList += empty ? newVal+"(unsure)" : ","+newVal+"(unsure)";
                empty = false;
            }
        }

        newVal = newPd.getSavesetsReloc();
        oldVal = oldPd.getSavesetsReloc();
        if (!oldVal.equals(newVal)) {
            String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.IS_REMOTE_DIRECTORY_CMD, QIWConstants.STRING_TYPE, dirPath+newVal, true);
            IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId, QiProjectManagerConstants.MINUTE_WAIT_TIME);

            if (response != null && !response.isAbnormalStatus()) {
                isDir = (String)response.getContent();
                if (isDir.equals("no")) {
                    diffList += empty ? newVal : ","+newVal;
                    empty = false;
                }
            } else {
                diffList += empty ? newVal+"(unsure)" : ","+newVal+"(unsure)";
                empty = false;
            }
        }

        newVal = newPd.getHorizonsReloc();
        oldVal = oldPd.getHorizonsReloc();
        if (!oldVal.equals(newVal)) {
            String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.IS_REMOTE_DIRECTORY_CMD, QIWConstants.STRING_TYPE, dirPath+newVal, true);
            IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId, QiProjectManagerConstants.MINUTE_WAIT_TIME);

            if (response != null && !response.isAbnormalStatus()) {
                isDir = (String)response.getContent();
                if (isDir.equals("no")) {
                    diffList += empty ? newVal : ","+newVal;
                    empty = false;
                }
            } else {
                diffList += empty ? newVal+"(unsure)" : ","+newVal+"(unsure)";
                empty = false;
            }
        }

        newVal = newPd.getScriptsReloc();
        oldVal = oldPd.getScriptsReloc();
        if (!oldVal.equals(newVal)) {
            String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.IS_REMOTE_DIRECTORY_CMD, QIWConstants.STRING_TYPE, dirPath+newVal, true);
            IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId, QiProjectManagerConstants.MINUTE_WAIT_TIME);

            if (response != null && !response.isAbnormalStatus()) {
                isDir = (String)response.getContent();
                if (isDir.equals("no")) {
                    diffList += empty ? newVal : ","+newVal;
                    empty = false;
                }
            } else {
                diffList += empty ? newVal+"(unsure)" : ","+newVal+"(unsure)";
                empty = false;
            }
        }

        newVal = newPd.getWellsReloc();
        oldVal = oldPd.getWellsReloc();
        if (!oldVal.equals(newVal)) {
            String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.IS_REMOTE_DIRECTORY_CMD, QIWConstants.STRING_TYPE, dirPath+newVal, true);
            IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId, QiProjectManagerConstants.MINUTE_WAIT_TIME);

            if (response != null && !response.isAbnormalStatus()) {
                isDir = (String)response.getContent();
                if (isDir.equals("no")) {
                    diffList += empty ? newVal : ","+newVal;
                    empty = false;
                }
            } else {
                diffList += empty ? newVal+"(unsure)" : ","+newVal+"(unsure)";
                empty = false;
            }
        }

        newVal = newPd.getWorkbenchesReloc();
        oldVal = oldPd.getWorkbenchesReloc();
        if (!oldVal.equals(newVal)) {
            String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.IS_REMOTE_DIRECTORY_CMD, QIWConstants.STRING_TYPE, dirPath+newVal, true);
            IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId, QiProjectManagerConstants.MINUTE_WAIT_TIME);

            if (response != null && !response.isAbnormalStatus()) {
                isDir = (String)response.getContent();
                if (isDir.equals("no")) {
                    diffList += empty ? newVal : ","+newVal;
                    empty = false;
                }
            } else {
                diffList += empty ? newVal+"(unsure)" : ","+newVal+"(unsure)";
                empty = false;
            }
        }

        newVal = newPd.getTempReloc();
        oldVal = oldPd.getTempReloc();
        if (!oldVal.equals(newVal)) {
            String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.IS_REMOTE_DIRECTORY_CMD, QIWConstants.STRING_TYPE, dirPath+newVal, true);
            IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId, QiProjectManagerConstants.MINUTE_WAIT_TIME);

            if (response != null && !response.isAbnormalStatus()) {
                isDir = (String)response.getContent();
                if (isDir.equals("no")) {
                    diffList += empty ? newVal : ","+newVal;
                    empty = false;
                }
            } else {
                diffList += empty ? newVal+"(unsure)" : ","+newVal+"(unsure)";
                empty = false;
            }
        }

        newVal = newPd.getSegyReloc();
        oldVal = oldPd.getSegyReloc();
        if (!oldVal.equals(newVal)) {
            String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.IS_REMOTE_DIRECTORY_CMD, QIWConstants.STRING_TYPE, dirPath+newVal, true);
            IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId, QiProjectManagerConstants.MINUTE_WAIT_TIME);

            if (response != null && !response.isAbnormalStatus()) {
                isDir = (String)response.getContent();
                if (isDir.equals("no")) {
                    diffList += empty ? newVal : ","+newVal;
                    empty = false;
                }
            } else {
                diffList += empty ? newVal+"(unsure)" : ","+newVal+"(unsure)";
                empty = false;
            }
        }

        newVal = newPd.getXmlReloc();
        oldVal = oldPd.getXmlReloc();
        if (!oldVal.equals(newVal)) {
            String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.IS_REMOTE_DIRECTORY_CMD, QIWConstants.STRING_TYPE, dirPath+newVal, true);
            IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId, QiProjectManagerConstants.MINUTE_WAIT_TIME);

            if (response != null && !response.isAbnormalStatus()) {
                isDir = (String)response.getContent();
                if (isDir.equals("no")) {
                    diffList += empty ? newVal : ","+newVal;
                    empty = false;
                }
            } else {
                diffList += empty ? newVal+"(unsure)" : ","+newVal+"(unsure)";
                empty = false;
            }
        }

        ArrayList<String> newSdirs = newPd.getSeismicDirs();
        ArrayList<String> oldSdirs = oldPd.getSeismicDirs();
        int oldSize = oldSdirs.size();

        for (int i=0; i<newSdirs.size(); i++) {
            newVal = newSdirs.get(i);
            //if the new project has more seismic directories, just check if the
            //exist and are a directory
            if (i >= oldSize) {
                String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.IS_REMOTE_DIRECTORY_CMD, QIWConstants.STRING_TYPE, dirPath+newVal, true);
                IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId, QiProjectManagerConstants.MINUTE_WAIT_TIME);

                if (response != null && !response.isAbnormalStatus()) {
                    isDir = (String)response.getContent();
                    if (isDir.equals("no")) {
                        diffList += empty ? newVal : ","+newVal;
                        empty = false;
                    }
                } else {
                    diffList += empty ? newVal+"(unsure)" : ","+newVal+"(unsure)";
                    empty = false;
            }
            } else {
                if (!oldSdirs.get(i).equals(newVal)) {
                    String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.IS_REMOTE_DIRECTORY_CMD, QIWConstants.STRING_TYPE, dirPath+newVal, true);
                    IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId, QiProjectManagerConstants.MINUTE_WAIT_TIME);

                    if (response != null && !response.isAbnormalStatus()) {
                        isDir = (String)response.getContent();
                        if (isDir.equals("no")) {
                            diffList += empty ? newVal : ","+newVal;
                            empty = false;
                        }
                    } else {
                        diffList += empty ? newVal+"(unsure)" : ","+newVal+"(unsure)";
                        empty = false;
                    }
                }
            }
        }

        return diffList;
    }
}