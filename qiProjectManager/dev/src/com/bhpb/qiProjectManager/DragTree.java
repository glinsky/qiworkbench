package com.bhpb.qiProjectManager;

import java.awt.Component;
import java.awt.Rectangle;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceContext;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.InvalidDnDOperationException;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.EventObject;
import java.util.logging.Logger;

import javax.swing.DefaultCellEditor;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.CellEditorListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellEditor;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeCellRenderer; 

import com.bhpb.qiworkbench.compAPI.DataDataDescriptor;
import com.bhpb.qiworkbench.compAPI.DataDataDescriptorTransferable;
/*
 * DragTree.java
 *
 * Created on July 11, 2007, 1:24 PM
 */

/**
 * Extention of JTree that allows Drag and Drop
 * @author Marcus Vaal
 */
public class DragTree extends JTree {

	private static Logger logger = Logger.getLogger(DragTree.class.getName());
	
    /**
     * DragTree Constructor
     */
    public DragTree() {
    	super();
    	this.setCellEditor(new MyDefaultTreeCellEditor(this));
        this.dragSource = DragSource.getDefaultDragSource();
        this.dgListener = new DGListener();
        this.dsListener = new DSListener();
        this.dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_COPY_OR_MOVE, this.dgListener);
    }
    
    /**
     * DragTree Constructor
     * @param action Type of action allowed by the tree
     */
    public DragTree(int action) {
		this.setCellEditor(new MyDefaultTreeCellEditor(this));
		if (action != DnDConstants.ACTION_NONE &&
			action != DnDConstants.ACTION_COPY &&
			action != DnDConstants.ACTION_MOVE &&
			action != DnDConstants.ACTION_COPY_OR_MOVE &&      
			action != DnDConstants.ACTION_LINK
		) throw new IllegalArgumentException("action" + action);
		
		this.dragAction = action;
		this.setOpaque(true);    
		this.dragSource = DragSource.getDefaultDragSource();
		this.dgListener = new DGListener();
		this.dsListener = new DSListener();
		
		this.dragSource.createDefaultDragGestureRecognizer(this, this.dragAction, this.dgListener); 
	}
    
    /**
     * DragTree Constuctor
     * @param root Tree root node
     * @param action Type of action allowed bt the tree
     */
    public DragTree(DefaultMutableTreeNode root, int action) {
		super(root);
		this.setCellEditor(new MyDefaultTreeCellEditor(this));
		if (action != DnDConstants.ACTION_NONE &&
			action != DnDConstants.ACTION_COPY &&
			action != DnDConstants.ACTION_MOVE &&
			action != DnDConstants.ACTION_COPY_OR_MOVE &&      
			action != DnDConstants.ACTION_LINK
		) throw new IllegalArgumentException("action" + action);
		
		this.dragAction = action;
		this.setOpaque(true);    
		this.dragSource = DragSource.getDefaultDragSource();
		this.dgListener = new DGListener();
		this.dsListener = new DSListener();
		
		this.dragSource.createDefaultDragGestureRecognizer(this, this.dragAction, this.dgListener); 
	}
    
    /**
     * DragTree Constuctor
     * @param root Tree root node
     * @param action Type of action allowed bt the tree
     */
    public DragTree(DefaultMutableTreeNode root, JDesktopPane desktopPane, int action) {
		super(root);
		this.desktopPane = desktopPane;
		this.setCellEditor(new MyDefaultTreeCellEditor(this));
		if (action != DnDConstants.ACTION_NONE &&
			action != DnDConstants.ACTION_COPY &&
			action != DnDConstants.ACTION_MOVE &&
			action != DnDConstants.ACTION_COPY_OR_MOVE &&      
			action != DnDConstants.ACTION_LINK
		) throw new IllegalArgumentException("action" + action);
		
		this.dragAction = action;
		this.setOpaque(true);    
		this.dragSource = DragSource.getDefaultDragSource();
		this.dgListener = new DGListener();
		this.dsListener = new DSListener();
		
		this.dragSource.createDefaultDragGestureRecognizer(this, this.dragAction, this.dgListener); 
	}
    
    /**
     * Overrides converValueToText
     */
	public String convertValueToText(Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		if(value != null) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
			Object nodeInfo = node.getUserObject();
			    	
			String sValue = ((DataDataDescriptor)nodeInfo).getName();
			if (sValue != null) {
				return sValue;
			}
		}
		return "";
	}
    
	/**
	 * Extention to DragGestureListener for use by DragTree
	 * @author Marcus Vaal
	 */
	class DGListener implements DragGestureListener {
		
		/**
		 * Recognizes a drag event
		 * @param evt DragGestEvent occurance
		 */
		public void dragGestureRecognized(DragGestureEvent evt) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)DragTree.this.getLastSelectedPathComponent();
			
			if (node != null) {
				Object nodeInfo = node.getUserObject();
			} else {
				node = new DefaultMutableTreeNode();
			}
			
			//Creates a transferable objectfor the drag gesture
			Transferable transferable = new DataDataDescriptorTransferable( (DataDataDescriptor)(node.getUserObject()) );
			
			try {
				evt.startDrag(DragSource.DefaultCopyNoDrop, transferable, DragTree.this.dsListener);
			}catch( InvalidDnDOperationException idoe ) {
				logger.finest( idoe.toString() );
			}
		}
	}

	/**
	 * Extention to DragSourceListener
	 * @author Marcus Vaal 
	 */
	class DSListener implements DragSourceListener {

		/**
		 * Invoked to signify that the Drag and Drop operation is complete.
		 */
		public void dragDropEnd(DragSourceDropEvent evt) {
			if(evt.getDropSuccess() == false) {
				return;
			}
		}
		
		/**
		 * Invoked to signify a Drag and Drop item has entered a drop site
		 */
		public void dragEnter(DragSourceDragEvent evt) {
			DragSourceContext context = evt.getDragSourceContext();
			int myaction = evt.getDropAction();
			if( (myaction & DragTree.this.dragAction) != 0) {
				context.setCursor(DragSource.DefaultCopyDrop);
			} else {
				context.setCursor(DragSource.DefaultCopyNoDrop);
			}
		}
		
		/**
		 * Invoked to signify a Drag and Drop item is being dragged over a drop site
		 */
		public void dragOver(DragSourceDragEvent evt) {
		}
		
		/**
		 * Invoked to signify a Drag and Drop item has exited a drop site
		 */
		public void dragExit(DragSourceEvent evt) {
			DragSourceContext context = evt.getDragSourceContext();
			context.setCursor(DragSource.DefaultCopyNoDrop);
		}
		
		/**
		 * Invoked to signify a Drag and Drop item has ben dropped into a drop site
		 */
		public void dropActionChanged(DragSourceDragEvent evt) {
			DragSourceContext context = evt.getDragSourceContext();
			context.setCursor(DragSource.DefaultCopyNoDrop);
		}
	}
  
	/**
	 * Extention of the DefaultTreeCellEditor
	 * @author Marcus Vaal
	 */
	class MyDefaultTreeCellEditor extends DefaultTreeCellEditor {
		
		/**
		 * MyDefaultTreeCellEditor Constructor
		 * @param tree Drag and Drop JTree
		 */
		public MyDefaultTreeCellEditor(JTree tree) {
			super(tree, new DefaultTreeCellRenderer(), new MyCellEditor());
		}
	}

	/**
	 * Extention of the Default CellEditor used in MyDefaultTreeCellEditor
	 * @author Marcus Vaal
	 */
	class MyCellEditor extends DefaultCellEditor {
		
		/**
		 * MyCellEditor Constructor
		 */
		public MyCellEditor() {
			super(new JTextField());
		}
		
		/**
		 * Overrides getCellEditorValue
		 */
		public Object getCellEditorValue() {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)DragTree.this.getLastSelectedPathComponent();
			Object nodeInfo = node.getUserObject();
			DataDataDescriptor DDD = (DataDataDescriptor)nodeInfo;
			
			String subString = DDD.getName().substring(DDD.getName().lastIndexOf("."));
			
			//Checks to make sure the rename is not the same as the previous name or the name of another file
			for(int i=0; i<node.getParent().getChildCount(); i++) {
				if(((DataDataDescriptor)((DefaultMutableTreeNode)(node.getParent().getChildAt(i))).getUserObject()).getName().equals(((JTextField)super.editorComponent).getText()) || 
						((DataDataDescriptor)((DefaultMutableTreeNode)(node.getParent().getChildAt(i))).getUserObject()).getName().equals(((JTextField)super.editorComponent).getText()+subString)) {
					return DDD;
				}
			}
			
			//Checks if the rename is empty
			if((((JTextField)super.editorComponent).getText()).equals("")) {
				return DDD;
			} else {
				DataDataDescriptor newDDD = null;
				//Check for ending (make sure ending is the same when renaming a file, will put it on the end for you if it is not
				if(!((JTextField)super.editorComponent).getText().endsWith(subString)) {
					newDDD = new DataDataDescriptor(((JTextField)super.editorComponent).getText()+subString, DDD.getGroupName(), DDD.getDirPath());
				} else {
					newDDD = new DataDataDescriptor(((JTextField)super.editorComponent).getText(), DDD.getGroupName(), DDD.getDirPath());
				}
				
				File file = new File(DDD.getPath());
				File file2 = new File(newDDD.getPath());
                
                // Rename file (or directory)
                boolean success = file.renameTo(file2);
                if (!success) {
                    System.out.println("File was not successfully renamed");
                } else {
                	System.out.println("File was successfully renamed " + newDDD.getPath());
                }
                
                JInternalFrame[] internalFrames = desktopPane.getAllFrames();
            	for(int i=0; i<internalFrames.length; i++) {
            		if(DDD.getName().equals(internalFrames[i].getTitle())) {
            			internalFrames[i].setTitle(newDDD.getName());
            			try{
            				internalFrames[i].setSelected(true);
            			} catch(java.beans.PropertyVetoException pve) {}
            			break;
            		}
            	}
                return newDDD;
			}
		}
	}
  
	private DragSource dragSource;
	private DragGestureListener dgListener;
	private DragSourceListener dsListener;
	private int dragAction = DnDConstants.ACTION_COPY;
	private JDesktopPane desktopPane = new JDesktopPane();
}
