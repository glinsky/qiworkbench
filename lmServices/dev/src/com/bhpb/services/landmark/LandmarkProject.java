package com.bhpb.services.landmark;

import java.io.IOException;
import java.nio.channels.Pipe;
import java.util.concurrent.SynchronousQueue;
import java.util.logging.Logger;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Iterator;
import java.util.ArrayList;
import com.bhpb.services.landmark.jni.LandmarkProjectHandler;


public class LandmarkProject {
    private static Logger logger = Logger.getLogger(LandmarkProject.class.getName());
	/**
	 * @param args
	 */


    public static void main(String args[]) {
        //float ar[];
        //String[] horizons = {"Water_Bottom_test_interpolate","Top_Salt_test_interpolate"};
        //String[] horizons = {"Water_Bottom_test_interpolate1"};
        //int i = 0;
        LandmarkProjectHandler lh= LandmarkProjectHandler.newInstance();
        String[] sar = lh.getWellListBySeisWorkProjectName("ggt_tst",3);
        for (int i=0; i < sar.length; i++) {
		     System.out.println("sar[" + i + "]=" + sar[i]);
        }
        //ar = lh.getShotpointRangeInfoBy2DLine("colombia", "SAC-99-F17a");

        //String [] sar = lh.getAll2v2Files("test_2d");
        //String [] sar = lh.getProjects(2);

        //ar = lh.getAllActive2v2Files("colombia");

        //ar = lh.getHorizonList("colombia",2);
        //ar = lh.getShotpointRangeInfoBy2DLine("colombia","SAC-99-F17a");
        //System.out.println("2d projects");
        //for (int i=0; i < sar.length; i++) {
        //	System.out.println("sar[" + i + "]=" + sar[i]);
        //}
        //System.out.println("3d projects");
        //sar = lh.getProjects(3);
        //for (int i=0; i < sar.length; i++) {
        //	System.out.println("sar[" + i + "]=" + sar[i]);
        //}

        //double f1 = 3059.33;
        //double f2 = 3058;
        //double f3 = -0.66;
        //ar[1]= (float)f1;
        //ar[2]= (float)f2;
        //ar[3]= (float)f3;
        //ar = lh.convertShotpointsTo2DTraces("colombia","SAC-99-F17a",ar[1],ar[2],ar[3]);
        //for (int i=0; i < ar.length; i++) {
        //	System.out.println("ar[" + i + "]=" + (int)ar[i]);
        //}
        //System.out.println(lh.get2DLineNumberByName("colombia","SAC-99-F17a"));
        //int ar[][] = new int[0][0];
        //ar = lh.getLineTraceInfo("ggt_tst", 3, "POSTM_16flt_line1970_8bitI.bri");
        //for (int i=0; i < 2; i++) {
        //	for (int j=0; j < 3; j++) {
        //			System.out.println("ar[" + i + "][" + j + "]=" + ar[i][j]);
        //	}

        //}
        //int ar1[][] = new int[4][3];
        //ar1 = lh.getLineTraceTimeInfo("ggt_tst", 3, "POSTM_16flt.bri");
        //for (int i=0; i < 4; i++) {
        //	for (int j=0; j < 3; j++) {
        //			System.out.println("ar1[" + i + "][" + j + "]=" + ar1[i][j]);
        //	}

        //}
        //System.out.println("test_3d");
        //String[] datasets = lh.getDatasetList("test_3d", 3, "*.bri");

        //for(String s : datasets){
        //	System.out.println(s);
        //}
        //System.out.println("test_3d");
        /*
        ar = lh.get2DHorizonListByLine("lab_test", "LAB-04-101");
        System.out.println("LAB-04-101");
        for (int i=0; i < ar.length; i++) {
        	System.out.println("ar[" + i + "]=" + ar[i]);
        }
        ar = lh.get2DHorizonListByLine("lab_test", "LAB-04-102");
        System.out.println("LAB-04-102");
        for (int i=0; i < ar.length; i++) {
        	System.out.println("ar[" + i + "]=" + ar[i]);
        }
        ar = lh.get2DHorizonListByLine("lab_test", "LAB-04-103");
        System.out.println("LAB-04-103");
        for (int i=0; i < ar.length; i++) {
        	System.out.println("ar[" + i + "]=" + ar[i]);
        }
        ar = lh.get2DHorizonListByLine("lab_test", "LAB-04-104");
        System.out.println("LAB-04-104");
        for (int i=0; i < ar.length; i++) {
        	System.out.println("ar[" + i + "]=" + ar[i]);
        }
        ar = lh.get2DHorizonListByLine("lab_test", "LAB-04-105");
        System.out.println("LAB-04-105");
        for (int i=0; i < ar.length; i++) {
        	System.out.println("ar[" + i + "]=" + ar[i]);
        }
        */
        //String [] ars = lh.get2DNonRelatedLinesByHorizonArray("lab_test",
        //		new String[]{"LAB-04-101","LAB-04-102","LAB-04-103"},
        //		new String[]{"cjs_basalts","cjs_tert1"});
        //System.out.println("get2DNonRelatedLinesByHorizonArray(\"falk_2d\"..");
        /*String [] ars = lh.get2DNonRelatedLinesByHorizonArray("falk_2d",
        		//new String[]{"FIS-4541","FIS-4553","FIS-4124","6S-205P1-068","FIS-4547","6S-215","FIS-4118","FIS-4125","6S-117","FIS-4549","FIS-4043","6S-116","6S-124","FIS-4116","6S-125","6S-211","FIS-4555","FIS-4028","FIS-4539","6S-118","FIS-4545","6S-126","FIS-4041A","FIS-4039","6S-212","FIS-4055","6S-213","FIS-4053","6S-129","FIS-4045","FIS-4512","6S-119","FIS-4110R","FIS-4057","FIS-4115","6S-127","FIS-4051","FIS-4119A","6S-115","FIS-4049","FIS-4514","FIS-4126C","6S-122","FIS-4117","FIS-4120","6S-214","FIS-4551","FIS-4047","6S-128","FIS-4543"},
        		//new String[]{"6S-122"},
        		new String[]{"6S-115","6S-116","6S-117","6S-118","6S-119","6S-122","6S-124","6S-125","6S-126","6S-127","6S-128","6S-129","6S-205P1-068","6S-211","6S-212","6S-213","6S-214","6S-215","FIS-4028","FIS-4039","FIS-4041A","FIS-4043","FIS-4045","FIS-4047","FIS-4049","FIS-4051","FIS-4053","FIS-4055","FIS-4057","FIS-4110R","FIS-4115","FIS-4116","FIS-4117","FIS-4118","FIS-4119A","FIS-4120","FIS-4124","FIS-4125","FIS-4126C","FIS-4512","FIS-4514","FIS-4539","FIS-4541","FIS-4543","FIS-4545","FIS-4547","FIS-4549","FIS-4551","FIS-4553","FIS-4555"},
        		new String[]{"TB_TOROA_A_POS_RS","TB_TOROA_B_POS_RS"});
        		//new String[]{"TB_TOROA_B_POS_RS"});

   		 */
        //System.out.println("get2DNonRelatedLinesByHorizonArray(\"falk_2d\"..");
        //String [] ars = lh.get2DRelatedLinesByHorizonName("falk_2d", "TB_TOROA_A_POS_RS_LT");
        		//new String[]{"FIS-4541","FIS-4553","FIS-4124","6S-205P1-068","FIS-4547","6S-215","FIS-4118","FIS-4125","6S-117","FIS-4549","FIS-4043","6S-116","6S-124","FIS-4116","6S-125","6S-211","FIS-4555","FIS-4028","FIS-4539","6S-118","FIS-4545","6S-126","FIS-4041A","FIS-4039","6S-212","FIS-4055","6S-213","FIS-4053","6S-129","FIS-4045","FIS-4512","6S-119","FIS-4110R","FIS-4057","FIS-4115","6S-127","FIS-4051","FIS-4119A","6S-115","FIS-4049","FIS-4514","FIS-4126C","6S-122","FIS-4117","FIS-4120","6S-214","FIS-4551","FIS-4047","6S-128","FIS-4543"},
        		//new String[]{"6S-122"},
        		//new String[]{"6S-115","6S-116","6S-117","6S-118","6S-119","6S-122","6S-124","6S-125","6S-126","6S-127","6S-128","6S-129","6S-205P1-068","6S-211","6S-212","6S-213","6S-214","6S-215","FIS-4028","FIS-4039","FIS-4041A","FIS-4043","FIS-4045","FIS-4047","FIS-4049","FIS-4051","FIS-4053","FIS-4055","FIS-4057","FIS-4110R","FIS-4115","FIS-4116","FIS-4117","FIS-4118","FIS-4119A","FIS-4120","FIS-4124","FIS-4125","FIS-4126C","FIS-4512","FIS-4514","FIS-4539","FIS-4541","FIS-4543","FIS-4545","FIS-4547","FIS-4549","FIS-4551","FIS-4553","FIS-4555"},
        		//new String[]{"TB_TOROA_A_POS_RS_LTL"});
        		//new String[]{"TB_TOROA_B_POS_RS"});

        //System.out.println("final");
        //for (int i=0; i < ars.length; i++) {
        //	System.out.println("ars[" + i + "]=" + ars[i]);
        //	if(i >= 5)
        //		break;
        //}

        //System.out.println("getHorizonList(\"nept_2\",3)");
        //String [] ars = lh.getHorizonList("nept_2",3);
        //System.out.println("getHorizonList(\"falk_2d\",2)");
        //System.out.println("2D horizon list");
        //for (int i=0; i < ars.length; i++) {
        //	System.out.println("ars[" + i + "]=" + ars[i]);
        //	if(ars[i].equals("wbl_RVL_Jan09_WWC_M10_TopTrough_Canary_IntSm5")){
		//		System.out.println("ars[" + i + "]=" + ars[i]);
		//	}
        //		break;
        //}

        //System.out.println("getAllActive2v2Files(\"tstflk2d\")");
        //String[] v2 = lh.getAllActive2v2Files("tstflk2d");
        //String[] v2 = lh.getAll2v2Files("tstflk2d");
        //for(String s : v2){
        //	System.out.println(s);
        //}
        //System.out.println("getAllActive2v2Files(\"tstflk2d\")");
        /*
        String [] lines = new String[2];
        lines[0] = "LAB-04-101";
        lines[1] = "LAB-04-101";
        //lines[2] = "LAB-04-103";
        //lines[3] = "LAB-04-104";
        //lines[4] = "LAB-04-105";
        ar = lh.get2DHorizonListByLines("lab_test", lines);
        for (int i=0; i < ar.length; i++) {
        	System.out.println("ar[" + i + "]=" + ar[i]);
        }
        */
/*
        Map map = new HashMap<String,List<String>>();
        for (int i=0; i < ar.length; i++) {
			String key = ar[i].substring(30,39);
			String line = ar[i].substring(0,30);
			if(!map.containsKey(key)){
				List<String> list = new ArrayList<String>();
				list.add(line);
				map.put(key,list);
			}else{
				List<String> list = (List<String>)map.get(key);
				if(!list.contains(line)){
					list.add(line);
					map.put(key,list);
				}
			}
        }
        */
        /*
		Set set= map.keySet () ;
		Iterator iter = set.iterator () ;

		while ( iter.hasNext())  {
		   Object Obj = iter.next ();
		   System.out.println ( "Key: "+ Obj);

		   List<String>  list = (List<String>) map.get(Obj);
		   System.out.println ( "size: "+ list.size());
		   for(int i = 0; i < list.size(); i++){
			   String s = list.get(i);
		   		System.out.println(s) ;
	   	   }
		}
		*/
    }
}