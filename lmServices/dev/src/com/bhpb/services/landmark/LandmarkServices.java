/**
 ###########################################################################
 # LandmarkServices - Provide Landmark SeisWorks project related information
 # through Java Native Interface. A singleton class to ensure one instance of 
 # service available within the appliation scope.
 # Copyright (C) 2006  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
*/

package com.bhpb.services.landmark;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.bhpb.services.landmark.jni.LandmarkProjectHandler;


public class LandmarkServices {
	private LandmarkProjectHandler projectHandler;
	private static LandmarkServices singleton;
	private List<ProjectInfo> landmarkAllProjects = null;
	
	private LandmarkServices(){
		initLandmarkProjectHandler();
	}
	
	public static LandmarkServices getInstance() throws UnsatisfiedLinkError, NoClassDefFoundError{
        if (singleton == null) {
            singleton = new LandmarkServices();
        }
        return singleton;
    }

	/*
	 * Get a HashMap of 2v2 Line keyed by process level and version
	 */	
	public synchronized Map<String,List<String>> get2v2FileLinesMap(String project){
        Map map = new HashMap<String,List<String>>();
        if(project == null || project.trim().length() == 0){
            return map;
            //throw new IllegalArgumentException("Landmark Project name can not be null or empty.");
        }
		String ar[];
        ar = projectHandler.getAll2v2Files(project);
		//Map map = new HashMap<String,List<String>>();
        for (int i=0; i < ar.length; i++) {
			String key = ar[i].substring(30,39);
			String line = ar[i].substring(0,30);
			int ind = line.indexOf("__");
			if(ind != -1)
				line = line.substring(0, ind);
			if(!map.containsKey(key)){
				List<String> list = new ArrayList<String>();
				list.add(line);
				map.put(key,list);
			}else{
				List<String> list = (List<String>)map.get(key);
				if(!list.contains(line)){
					list.add(line);
					map.put(key,list);
				}
			}
        }
        return map;
	}
	/*
	 * Get line/trace range information {{min_line,max_line,increment_line},{min_trace,max_trace,increment_trace}}
	 * given project name, project type, and volume name 
	 */
	public int[][] getLandmarkLineTraceInfo(String project, int type, String volume)  throws UnsatisfiedLinkError, NoClassDefFoundError {
		int[][] out = new int[0][0];
		out = projectHandler.getLineTraceInfo(project, type, volume);
		return out;
	}
	
	public int get2DLineNumberByName(String projectName,String lineName){
		return projectHandler.get2DLineNumberByName(projectName,lineName);
	}
	/*
	 * Get time range information {min_time,max_time,sample_rate} given project name, project type and volume name
	 */
	public float [] getLandmarkTimeRangeInfo(String project, int type, String volume)  throws UnsatisfiedLinkError, NoClassDefFoundError {
		float[] out = new float[0];
		out = projectHandler.getTimeRangeInfo(project, type, volume);
		return out;
	}

	/*
	 * Convert given shotpoint parameters (min, max, inc) to an array of traces {min, max, inc}
	 */
	public float [] convertShotpointsTo2DTraces(String project,String lineName, float minShotpoint, float maxShotpoint, float incShotpoint)  throws UnsatisfiedLinkError, NoClassDefFoundError {
		float[] out = new float[0];
		out = projectHandler.convertShotpointsTo2DTraces(project,lineName, minShotpoint,maxShotpoint,incShotpoint);
		return out;
	}	
	
	/*
	 * Get an array of project names given a project type
	 */
	public String[] getLandmarkProjects(int type){
		String[] out = new String[0];
		out = projectHandler.getProjects(type);
		return out;
	}

	/*
	 * Get the list of 3D volumes based on project name and volume's extension (.bri, .3dv, .cmp)
	 */
	public String[] getLandmarkVolumeList(String project, int type, String ext) throws UnsatisfiedLinkError, NoClassDefFoundError {
		String [] volumes = new String[0];
		if(project == null || project.trim().length() == 0)
			return volumes;

		if(!(type >= 2 && type <= 3)){
			int itype = getLandmarkProjectType(project);
			if(itype != -1)
				volumes = projectHandler.getDatasetList(project, itype, ext);
		}else
			volumes = projectHandler.getDatasetList(project, type, ext);
		return volumes;
	}
	
	/*
	 * Get project type based on the given project name
	 */
	public int getLandmarkProjectType(String project)  throws UnsatisfiedLinkError, NoClassDefFoundError{
		if(project == null || project.trim().length() == 0)
			return -1;
		int itype = -1;
		if(landmarkAllProjects == null)
			populateLandmarkProjects();
		for(int i = 0; i < landmarkAllProjects.size(); i++){
			ProjectInfo pi =landmarkAllProjects.get(i); 
			if(pi.getName().equals(project)){
				itype = pi.getType(); 
				break;
			}
		}
		return itype;
	}
	
	public LandmarkProjectHandler getLandmarkProjectHandler(){
		return projectHandler;
	}
	
	public boolean isValidLandmarkProject(String project) throws UnsatisfiedLinkError, NoClassDefFoundError{
		if(landmarkAllProjects == null){
			populateLandmarkProjects();
		}
		
		for(ProjectInfo pi : landmarkAllProjects){
			if(pi.getName().equals(project))
				return true;
		}
		return false;
	}
	
	private void populateLandmarkProjects() throws UnsatisfiedLinkError, NoClassDefFoundError{
		landmarkAllProjects = new ArrayList<ProjectInfo>();
		String [] prjs = projectHandler.getProjects(3);
		for(int i = 0; i < prjs.length; i++){
			ProjectInfo p = new ProjectInfo(prjs[i],3);
			landmarkAllProjects.add(p);
		}
		
		prjs = projectHandler.getProjects(2);
		for(int i = 0; i < prjs.length; i++){
			ProjectInfo p = new ProjectInfo(prjs[i],2);
			landmarkAllProjects.add(p);
		}
		return;
	}
	
	private void initLandmarkProjectHandler() throws UnsatisfiedLinkError, NoClassDefFoundError{
		if(projectHandler == null){
			projectHandler = LandmarkProjectHandler.newInstance();
		}
		return;
	}
    /*
     * Simple project properties containing project name and type (2D or 3D)
     */
	class ProjectInfo{
		String name;
		int type;
		public ProjectInfo(String name, int type){
			this.name = name;
			this.type = type;
		}
		public String getName(){
			return name;
		}
		public int getType(){
			return type;
		}
	}
}
