/**
 ###########################################################################
 # LandmarkProjectHandler - A JNI class providing interfaces between java app
 # and C/C++ modules
 # Copyright (C) 2006  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
*/
package com.bhpb.services.landmark.jni;
import java.util.concurrent.SynchronousQueue;

public class LandmarkProjectHandler {

	//Boolean lock=new Boolean(true);
    Format forInf1 = new Format("%14d");
    Format forInf2 = new Format("%15d");
    Format forFlf = new Format("%31.4f");
    private boolean stop = false;
    private String currentHorizon;
    private String message = "";
    public static double LANDMARK_NULL_VALUE = 9999999933815812510711506376257961984.00;
	public static double BHPSU_NULL_VALUE = -999.25;
	public static final String LANDMARK_JNI_LIB_NAME = "landmarkprojects";
	/** empty queue lock */
    //private static Semaphore available = new Semaphore(0);
    private SynchronousQueue<String> queue;
    private SynchronousQueue<String> horizonQueue;
    private LandmarkProjectHandler(){
	}
    private void printClassLoaderName(ClassLoader loader){
		if(loader != null){
			System.out.println("loader name " + loader.getClass().getName());
			printClassLoaderName(loader.getParent());
		}
		return;
	}
    public static LandmarkProjectHandler newInstance(){
    	return new LandmarkProjectHandler();
    }
    public static LandmarkProjectHandler newInstance(SynchronousQueue<String> queue){
    	return new LandmarkProjectHandler(queue);
    }
	private LandmarkProjectHandler(SynchronousQueue<String> queue){
		this.queue = queue;
	}
	public void setQueue(SynchronousQueue<String> queue){
		this.queue = queue;
	}
	public String getCurrentHorizon(){
		return currentHorizon;
	}
	public boolean getStop(){
		return stop;
	}

	public void setStop(boolean flag){
		stop = flag;
	}

	/**
     * Return an array of current Landmark project names given the project type
     * @param type: project type either 3(3D), 2(2D), 1(All)
     * @return String[]
     */
    public native synchronized String[] getProjects(int type);

    /**
     * Return an array of Landmark 2D seismic line names given a project
     * @param projectName: the name of a Landmark project
     * @param type: the project type  2 (2D)
     * @return String[]
     */
    public native synchronized String[] getSeismicLineList(String projectName, int type);

   /**
     * Return an array of all Landmark 2D seismic 2v2_glb file names given a project
     * @param projectName: the name of a Landmark project
     * @param type: the project type  2 (2D)
     * @return String[]
     */
    public native synchronized String[] getAll2v2Files(String projectName);

    /**
     * THIS API IS NOT CURRENTLY USED AND ITS IMPLEMENTATION IS SUBJECT TO FURTHER MODIFICATION
     * Return an array of all Landmark 2D seismic 2v2_glb file names that are associated with
     * active lines given a project
     * @param projectName: the name of a Landmark project
     * @param type: the project type  2 (2D)
     * @return String[]
     */
    public native synchronized String[] getAllActive2v2Files(String projectName);

    /**
     * Return an intersection of Landmark 2D horizon names given a project and a list of line names
     * Each line has its own horizon set. Take an intersection of horizon sets from each line.
     * @param projectName: the name of a Landmark project
     * @param lines: a list of 2D Line name
     * @return String[]
     */
    public native synchronized String[] get2DHorizonListByLines(String projectName, String[] lines);
    /**
     * Return an array of Landmark 2D horizon names given a project and a line name
     * @param projectName: the name of a Landmark project
     * @param lineName: a 2D Line name
     * @return String[]
     */
    public native synchronized String[] get2DHorizonListByLine(String projectName, String lineName);

    /**
     * Return an array of Landmark 2D line names which do not relate any of the given horizon names
     * given project and a list of line names and a list of horizon names
     * @param projectName: the name of a Landmark project
     * @param lines: a list of 2D Line names
     * @param horizons: a list of 2D Horizon names
     * @return String[] a list of qualified line names
     */
    public native synchronized String[] get2DNonRelatedLinesByHorizonArray(String projectName, String[] lines, String[] horizons);

    /**
     * Return an array of Landmark 2D line names which relate to the given horizon names
     * given the project name and an array of line names and an array of horizon names
     * @param projectName: the name of a Landmark project
     * @param horizonName: the horizon name
     * @return String[]: a list of qualified line names
     */
    public native synchronized String[] get2DRelatedLinesByHorizonName(String projectName, String horizonName);

    /**
     * Return an array of Landmark 2D seismic file names given a project and a line name
     * @param projectName: the name of a Landmark project
     * @param type: the project type  2 (2D)
     * @param lineName: a 2D Line name
     * @return String[]  an array of seismic file names
     */
    public native synchronized String[] getSeismicFilesByLine(String projectName, int type, String line);

    /**
     * Return an array of Landmark volume names given a project and its volume extension
     * @param projectName: the name of a Landmark project
     * @param type: the project type either 3 (3D), 2 (2D)
     * @param extension: volume extension (such as *.bri, *.3dv)
     * @return String[]
     */
    public native synchronized String[] getDatasetList(String projectName, int type, String extension);

    /**
     * Return an 2X3 array of Landmark Line & Trace information given a project and the volume name
     * @param projectName: the name of a Landmark project
     * @param type: the project type either 3 (3D), 2 (2D)
     * @param volumeName: the name of a volume
     * @return int[][] : 2X3 array of integers denote
     *    {{min_line,max_line,increment_line},{min_trace,max_trace,increment_trace}}
     */
    public native synchronized int[][] getLineTraceInfo(String projectName, int type, String volumeName);

    /**
     * Return an 4X3 array of Landmark Line & Trace information given a project and the volume name
     * @param projectName: the name of a Landmark project
     * @param type: the project type either 3 (3D), 2 (2D)
     * @param volumeName: the name of a volume
     * @return int[][] : 4X3 array of integers denote
     *    {{min_line,max_line,increment_line},{min_trace,max_trace,increment_trace},
     *    {min_time,max_time,increment_time},{data_type,dummy,dummy}}
     */
    public native synchronized int[][] getLineTraceTimeInfo(String projectName, int type, String volumeName);

    /**
     * Return an array of Landmark 2D default shotpoint range and increment information given a project,
     * the line name
     * @param projectName: the name of a Landmark project
     * @param type: the project type 2
     * @param lineName: line name
     * @return float[] : a array of floating number denoted by
     *    {line_number,min_shotpoint,max_shotpoint,increment}
     */
    public native synchronized float[] getShotpointRangeInfoBy2DLine(String projectName,String lineName);

    /**
     * Return the line number given the 2D project and the line name
     * @param projectName: the name of a Landmark project
     * @param lineName: line name
     * @return int : line number
     */
    public native synchronized int get2DLineNumberByName(String projectName,String lineName);

    /**
     * Return an array of converted Landmark 2D traces and increment information given a project,
     * the line name, shotpoint 1, shotpoint 2, increment
     * @param projectName: the name of a Landmark project
     * @param lineName: line name
     * @return float[] : a array of floating number denoted by
     *    {trace1,trace2,increment}
     */
    public native synchronized float[] convertShotpointsTo2DTraces(String projectName,String lineName, float minShotpoint, float maxShotpoint, float incShotpoint);

    /**
     * Return an array of Landmark time range and sample rate information given a project and the volume name
     * @param projectName: the name of a Landmark project
     * @param type: the project type either 3 (3D), 2 (2D)
     * @param volumeName: the name of a volume
     * @return float[] : a array of floating number denote
     *    {min_time,max_time,sample_rate}
     */
    public native synchronized float[] getTimeRangeInfo(String projectName, int type, String volumeName);

    /**
     * Return an array of Landmark horizon names given a project
     * @param projectName: the name of a Landmark project
     * @param type: the project type either 3 (3D), 2 (2D)
     * @return String[]
     */
    public native synchronized String[] getHorizonList(String projectName, int type);

    /**
     * Return an array of Landmark Well List names given a SeisWorks project
     * @param projectName: the name of a Landmark seiswork project
     * @param type: the project type either 3 (3D), 2 (2D)
     * @return String[]
     */
    public native synchronized String[] getWellListBySeisWorkProjectName(String projectName, int type);

    static{
        System.loadLibrary(LANDMARK_JNI_LIB_NAME);
    }

    public native synchronized boolean openProject(String project, int type, Boolean lock);

    //public native boolean getProjectHorizonInfo1(String projectName, int type, boolean projectOpen, String horizon, boolean horizonOpen, int minline, int maxline, int mintrace,  int maxtrace, Boolean lock);
    public native synchronized boolean getProjectHorizonInfo1(String projectName, int type, String horizon, int minline, int maxline, int mintrace,  int maxtrace, Boolean lock);
    //public native boolean closeProjectHorizon();
    public native synchronized void closeProject(int type);
    public native synchronized void getProjectHorizonInfo(String projectName, int type, String[] horizons, int minline, int maxline, int mintrace,  int maxtrace, Boolean lock);

    public void setCurrentHorizon(String horizon){
    	currentHorizon = horizon;
    	//try{
    	//	horizonQueue.put(horizon);
    	//}catch(InterruptedException ie){
    	//	ie.printStackTrace();
    	//}
    }

    public void setMessage(String msg){
    	message = msg;
    }

    public String getMessage(){
    	return message;
    }
    public SynchronousQueue<String> getHorizonQueue(){
    	return horizonQueue;
    }

    public void printLineTraceTime(float[] ltt){
    	try {
    	if(ltt[2] == LANDMARK_NULL_VALUE)
    		ltt[2] = (float)BHPSU_NULL_VALUE;
	    queue.put(forInf1.format((int)ltt[0]) + "." + forInf2.format((int)ltt[1]) + "." + forFlf.format((double)ltt[2]));
	    //	queue.put(ltt[0] + "\t" + ltt[1] + "\t" + ltt[2]);
    	} catch (InterruptedException ie) {
            // should never happen
        }
        //available.release();
	System.out.println(forInf1.format((int)ltt[0]) + "." + forInf2.format((int)ltt[1]) + "." + forFlf.format((double)ltt[2]));
	//System.out.println(ltt[0] + "\t" + ltt[1] + "\t" + ltt[2]);
    }

    public void done(){
    	stop = true;
    }

    public static void main(String args[]) {
        String ar[];
        /*
        try{
        	Pipe pipe = Pipe.open();
        	LandmarkHorizonProducer prod = new LandmarkHorizonProducer(pipe.sink());
			String horizonOut = "/home/lilt9/qiProjects/neptune/horizons/horizonCopy2.xyz";
            Consumer cons = new Consumer(pipe.source(), horizonOut);

		} catch (IOException ioe) {
            System.out.println("LandmarkProjectHandler: "+ioe.getMessage());
        }

         LandmarkProjectHandler lh= new  LandmarkProjectHandler(prod,cons);
         */
        //ar = lh.getProjects(3);
        //for (int i=0; i < ar.length; i++) {
        //   System.out.println("array element"+i+
        //                         "=" + ar[i]);
        //}
        /*
        ar = lh.getDatasetList("neptune", 3, "*.3dv");
        for (int i=0; i < ar.length; i++) {
            System.out.println("array element2"+i+
                                  "=" + ar[i]);
         }
        int[][] iar;
        //iar = lh.getLineTraceInfo("ggt_tst", 3, "POSTM_16flt.bri");
        iar = lh.getLineTraceInfo("wc76", 3, "temp.bri");

        for (int i=0; i < iar.length; i++) {
        	int jar [] = iar[i];
        	for(int j = 0; j < jar.length; j++){
        		System.out.println("iar[" + i + "][" + j + "]= " + iar[i][j]);
        	}
        }

        String [] horizons = lh.getHorizonList("wc76", 3);
        for (int i=0; i < horizons.length; i++) {
       		System.out.println("horizons[" + i + "]= " + horizons[i]);
		if(i > 5)
		    break;
        }

        float[] far;
        far = lh.getTimeRangeInfo("neptune", 3, "Repsol_Ph02_GSP_Salt_Flood_1_Vel.bri");

        for (int i=0; i < far.length; i++) {
        		System.out.println("far[" + i + "]" + far[i]);
        }
        */
        /*
//       generate a horizon
         prod.start();
         //save generate horizon to a file
         cons.start();
     	System.out.println("line\ttrace\ttime");
         lh.getProjectHorizonInfo("ggt_tst", 3, "Water_Bottom_test_interpolate", 2065, 2068, 8294, 8296, lh.lock);
         */
    }


    /**
     * A Producer thread which generates horizon data line-by-line. Instead of
     * extracting the data from a Landmark horizon file using the SeisWorks API,
     * the data is read from a file. Each line consists of inline (ep), xline
     * (cdp) and a value (time).
     *
     */
    /*
     private static class Producer extends Thread {
         //The write end of a pipe
         WritableByteChannel wchannel;
         //Path of the output file
         String fileIn = "";
         String sline;
         boolean stop = false;
         Producer (WritableByteChannel wchannel, String fileIn) {
             this.wchannel = wchannel;
             this.fileIn = fileIn;
         }

         Producer (WritableByteChannel wchannel){
        	 this.wchannel = wchannel;
         }

         public void setStop(boolean flag){
        	 stop = flag;
         }
         public void setLine(String line){
        	 sline = line;
         }
         public void run() {
             ByteBuffer buf = ByteBuffer.allocateDirect(BUF_SIZE);
             File fin = null;
             if(fileIn.trim().length() != 0)
            	 fin = new File(fileIn);

             int bufCapacity = BUF_SIZE;
             int bufFilled = 0;

             //TODO: determine the line-termination characters for the underlying OS
             String lineTermChars = "\r\n";
             byte[] lineTermBytes = lineTermChars.getBytes();
             int lineTermSize = lineTermBytes.length;

             Selector sel = null;
             try {
                 //create a readiness selector for the writable channel
                 sel = Selector.open();

                 //set nonblocking mode for the writable channel
                 ((Pipe.SinkChannel)wchannel).configureBlocking(false);

                 //register the writable channel with the selector
                 ((Pipe.SinkChannel)wchannel).register(sel, SelectionKey.OP_WRITE);
             } catch (ClosedChannelException cce) {
                 System.out.println("Producer::selector2: "+cce.getMessage());
             } catch (IOException ioe) {
                 System.out.println("Producer::selector1: "+ioe.getMessage());
             }

             BufferedReader br = null;
             try {
            	 if(fin != null){
                 FileInputStream fis = new FileInputStream(fin);
                 br = new BufferedReader(new InputStreamReader(fis));
            	 }
                 buf.clear();

                 //generate horizon data line-by-line
                 String line = null;
                 if(br != null)
                	 line = br.readLine();
                 else{
                	 //line = sline;

                 }

                 while (!stop) {
                	 try {
                		 line = queue.take();
                     } catch (InterruptedException ie) {
                    	 ie.printStackTrace();
                     }
                     byte[] lineBytes = line.getBytes();
                     int lineSize = lineBytes.length;
                     //check if room in the buffer
                     int newAmtFilled = bufFilled + lineSize + lineTermSize;
                     if (newAmtFilled <= bufCapacity) {
                         buf.put(lineBytes);
                         buf.put(lineTermBytes);
                         bufFilled = newAmtFilled;
                     } else {
                         //buffer full, write it out over the channel
                         //prepare to be drained
                         buf.flip();

                         //block until channel is ready for writing
                         //System.out.println("Producer: block for writing");
                         int n = sel.select();

                         if (n == 0) {
                             System.out.println("Producer::System error: no ready keys");
                         } else {
                             //There is only one entry in the set of selected
                             //keys so there is nothing to iterate over.

                             //The channel associate with the key must be
                             //writable for this is the only op we are interested in.

                             //Remove key from selected set indicating it has
                             //been handled.
                             sel.selectedKeys().clear();

                             //channel may not take it all at once
                             while (wchannel.write(buf) > 0) {
                                 //empty
                                 //System.out.println("Producer: write buf");
                             }

                             //prepare buffer for next set of lines
                             buf.clear();
                             bufFilled = 0;
                             //add line to buffer
                             buf.put(lineBytes);
                             buf.put(lineTermBytes);
                             bufFilled += lineSize + lineTermSize;
                         }
                     }
                     if(br != null)
                    	 line = br.readLine();
                     else{
                    //	 try {
                    //         available.acquire();
                    //     } catch (InterruptedException ie) {
                    //     }

                    //     line = queue.poll();
                    	 //line = sline;
                     }

                 }
                 //if anything left in the buffer, drain it
                 buf.flip();
                 while (wchannel.write(buf) > 0) {
                     //empty
                 }

                 //Send EOF
                 byte[] eof = "EOF".getBytes();
                 buf.clear();
                 buf.put(eof);
                 buf.flip();
                 while (wchannel.write(buf) > 0) { }

                 this.wchannel.close();
             } catch (Exception e) {
                 System.out.println("Producer: "+e.getMessage());
             } finally {
                 try {
                     if (br != null) br.close();
                 } catch (IOException e) {}
             }
         }
     }
*/
     /**
      * A Consumer thread which writes the horizon generated by the Producer
      * thread to a file.
      *
      */
    /*
     private static class Consumer extends Thread {
         //The read end of a pipe
         ReadableByteChannel rchannel;
         //Path of the output file
         String fileOut = "";

         Consumer (ReadableByteChannel rchannel, String fileOut) {
             this.rchannel = rchannel;
             this.fileOut = fileOut;
         }

         public void run() {
             ByteBuffer buf = ByteBuffer.allocateDirect(BUF_SIZE);

             File fout = new File(fileOut);

             Selector sel = null;
             try {
                 //create a readiness selector for the readable channel
                sel = Selector.open();

                 //set nonblocking mode for the readable channel
                 ((Pipe.SourceChannel)rchannel).configureBlocking(false);

                 //register the readable channel with the selector
                 ((Pipe.SourceChannel)rchannel).register(sel, SelectionKey.OP_READ);
             } catch (ClosedChannelException cce) {
                 System.out.println("Consumer::selector2: "+cce.getMessage());
             } catch (IOException ioe) {
                 System.out.println("Consumer::selector1: "+ioe.getMessage());
             }

             try {
                 WritableByteChannel wchannel = new FileOutputStream(fout).getChannel();

                 while (true) {
                     //block until channel is ready for reading
                     //System.out.println("Consumer: block for read");
                     int n = sel.select();

                     if (n == 0) {
                         System.out.println("Consumer::System error: no ready keys");
                     } else {
                         //There is only one entry in the set of selected
                         //keys so there is nothing to iterate over.

                         //The channel associate with the key must be
                         //readable for this is the only op we are interested in.

                         //Remove key from selected set indicating it has
                         //been handled.
                         sel.selectedKeys().clear();

                         //Read bytes into the buffer
                         int bytesRead = rchannel.read(buf);
                         //System.out.println("consumer: bytes read="+bytesRead);

                         //Check if buffer ends in EOF marker
                         int position = buf.position();
                         byte[] bytes = new byte[3];
                         bytes[0] = buf.get(position-3);
                         bytes[1] = buf.get(position-2);
                         bytes[2] = buf.get(position-1);
                         String eof = new String(bytes);
                         boolean isEOF = eof.equals("EOF");
                         if (isEOF) {
                             //strip off EOF
                             buf.position(position-3);
                         }

                         //prepare the buffer to be drained
                         buf.flip();

                         //Make sure the buffer is fully drained
                         while (buf.hasRemaining()) {
                             wchannel.write(buf);
                         }

                         //Make the buffer empty and ready for filling
                         buf.clear();

                         if (isEOF) break;
                     }
                 }

                 //close the file
                 wchannel.close();
                 //close the read end of the pipe
                 rchannel.close();
             } catch (FileNotFoundException fnfe) {
                 System.out.println("Consumer2: "+fnfe.getMessage());
             } catch (IOException ioe) {
                 System.out.println("Consumer1: "+ioe.getMessage());
             }
         }
     }
     */

/*
    String ar[];
    try{
    	Pipe pipe = Pipe.open();
    	SynchronousQueue<String> queue = new SynchronousQueue<String>();
    	LandmarkHorizonProducer prod = new LandmarkHorizonProducer(pipe.sink(),queue);
		String horizonOut = "/home/lilt9/qiProjects/neptune/horizons/horizonCopy2.xyz";
        Consumer cons = new Consumer(pipe.source(), horizonOut);


     LandmarkProjectHandler lh= new LandmarkProjectHandler(queue);


//   generate a horizon
     prod.start();
     //save generate horizon to a file
     cons.start();
 	System.out.println("line\ttrace\ttime");

     lh.getProjectHorizonInfo1("ggt_tst", 3, "Water_Bottom_test_interpolate", 1963, 1968, 8194, 8199, new Boolean(true));
     if(lh.getStop() == true){
    	 prod.setStop(true);
     }
	} catch (IOException ioe) {
        System.out.println("LandmarkProjectHandler: "+ioe.getMessage());
    }
*/

  }
