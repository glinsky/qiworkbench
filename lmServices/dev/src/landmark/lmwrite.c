/*   Name        lmwrite.c
 **
 **   Function  of writing trace data from SU to Landmark 3D.
 **
 */

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <iostream.h>
#include <fstream.h>
#include "sdlprototypes.h"
#include <string>
#include "su.h"
#include "segy.h"
//#include "Util.h"

using namespace std;

void build_fcrec(dm3dfc_record* fcrec, int mininx, int maxinx, int mininy, int maxiny, int inputType, segy *trace, int fileOrg, int dataType);
void create_tracefile(dm3dfc_record*, int*, int, const char*);
void write_tracedata(int ipdflg, dm3dfc_record* fcrec, int* ndxbuf, segy *trc, FILE *f);
void TrcDataShow(int ctr, int nsmp, float *rtrace);

void write_trace(int ipdflg, dm3dfc_record* fcrec, int* ndxbuf, segy *trc, void *source, void *target);
void close_proj(int type);

#define NO_OF_BUF_IDX 32767

const char* suffix[] = {"01.2v2", "01.3dv", ".bri", ".cmp"};

enum FileTypes {
    STD_2D, STD_3D, BRICK, COMPRESS
};

segy tr;
/*********************** self documentation **********************/
char *sdoc[] = {
    "									",
    " LMWRITE - write an Landmark output file (currently only support brick format)	",
    "									",
    " lmwrite <stdin lm_project= lm_ptype= lm_brickname=",
    "									",
    " Required parameter:",
    " lm_project=Landmark project name",
    " lm_ptype=Landmark project type 3 (3D) currently only support 3D",
    " lm_brickname=Landmark output brick file name (.bri extension is required) ",
    "									",
    " Optional parameters:  	",
    " file_type  4 = 32bit float bricked. (default)	",
    "            5 = 16bit float bricked.           ",
    "            6 =  8bit float bricked.           ",
    "            7 = 16bit int. Values are stored as 16 bit integers but returned as floats.",
    "            8 =  8bit int. Values are stored as 8 bit integers but returned as floats.",
    " data_type  1 = data sampled in time. (default) ",
    "            2 = data sampled in depth.  ",
    NULL
};

/**************** end self doc ***********************************/
int main(int argc, char **argv) {
    char *prjname;
    char *fileNam;
    int mininx, mininy, maxinx, maxiny;
    int ier, ipdflg;
    int inputType = -1;
    ;
    int ndxbuf[NO_OF_BUF_IDX];
    int file_type = 0; //32bit float bricked default
    int data_type = 1; //data sampled in time default
    struct dm3dfc_record fcrec;

    FILE *f = NULL;
    /* Initialize */
    initargs(argc, argv);
    requestdoc(1);
    /* Get parameters */
    MUSTGETPARSTRING("lm_project", &prjname);
    MUSTGETPARINT("lm_ptype", &ipdflg);
    MUSTGETPARSTRING("lm_brickname", &fileNam);
    if (!getparint("file_type", &file_type)) file_type = 4;
    if (!(file_type >= 4 && file_type <= 8))
        err("file_type should be any integer between 4 and 8 inclusive.\n");
    if (!getparint("data_type", &data_type)) data_type = 1;
    string projectName(prjname);
    int projectType = 3;
    ipdflg = projectType;
    prjin2_(&ier, &ipdflg, (char*) projectName.c_str(), projectName.length());

    if (ier != 0) {
        err("prjin2 error %d\n", ier);
        if (ier == 1) {
            fprintf(stderr, "Error: Project Definition file not found (.pds or .ps2)\n");
        } else if (ier == 2) {
            fprintf(stderr, "Error: Unable to log project on all drives\n");
        } else if (ier == 4) {
            fprintf(stderr, "Error: Could not obtain an OpenWorks license\n");
        } else {
            fprintf(stderr, "Unknown error in opening project. \n");
        }

        close_proj(ipdflg);
        return ier;
    }

    /*
     ** Get the master grid extent of this project
     */

    srvbnd_(&ipdflg, &mininx, &mininy, &maxinx, &maxiny, &ier);
    if (ier) {
        fprintf(stderr, " srvbnd error: %d\n", ier);
        close_proj(ipdflg);
        return ier;
    }

    printf(" project extents:\n");
    printf("     minimum ix = %d\n", mininx);
    printf("     minimum iy = %d\n", mininy);
    printf("     maximum ix = %d\n", maxinx);
    printf("     maximum iy = %d\n", maxiny);

    /*
     ** Select seismic file type and name
     */

    string outFileName(fileNam);

    int ind = outFileName.find(".bri", 0);
    if (ind != -1) {
        inputType = BRICK; // brick format

    }
    ind = outFileName.find(".3dv", 0);
    if (ind != -1) {
        inputType = STD_3D;
    }

    ind = outFileName.find(".cmp", 0);
    if (ind != -1) {
        inputType = COMPRESS;
    }

    if (inputType != 2)
        err("Currently only brick file format is supported.\n");

    segy trace1;
    if (!gettr(&trace1)) {
        err("Can not get first trace.");
    }

    /*
     ** Build the seismic file control recored
     */
    build_fcrec(&fcrec, mininx, maxinx, mininy, maxiny, inputType, &trace1, file_type, data_type);

    /*
     ** Create and open the seismic file based on the control record
     */

    create_tracefile(&fcrec, ndxbuf, ipdflg, outFileName.c_str());

    /*
     ** Write out trace data into the just created file
     */

    write_tracedata(ipdflg, &fcrec, ndxbuf, &trace1, f);


    /*  close file  */
    cout << "\n\nClose seismic file." << endl;

    dm3dcl_((void*) & fcrec, &ier, ndxbuf);
    if (f)
        fclose(f);

    if (ier) {
        printf(" dm3dcl ier: %d", ier);
        return ier;
    } else
        cout << "SUCCESSFUL JOB COMPLETION!" << endl;

    close_proj(ipdflg);
    return 0;

}

void close_proj(int type) {
    int ier;
    prjend_(&type, &ier);
    if (ier)
        printf(" prjend ier: %d\n", ier);
    return;
}

/*
 ** This function will assign the required values to the seismic control record
 ** This must be done before a new file is created.
 ** If the file already exists these values will be over written with
 ** those of the existing file
 */
void build_fcrec(dm3dfc_record* fcrec, int mininx, int maxinx, int mininy, int maxiny, int inputType, segy *trace, int file_type, int data_type) {

    // -------- Initialize File Control Record (all zero).

    // this must be max survey size for compressed and bricked volumes
    // anything else will be reset internally to that max area
    fcrec->lfmnpx = mininx;
    fcrec->lfmxpx = maxinx;
    fcrec->lfmnpy = mininy;
    fcrec->lfmxpy = maxiny;

    int nsmp = trace->ns;
    int samplerate = trace->dt * 0.001; //millisec


    fcrec->rfmnpt = trace->delrt;
    fcrec->rfmxpt = trace->delrt + samplerate * (nsmp - 1);
    int minIndex = trace->delrt / samplerate;
    fcrec->lfmnpt = minIndex + 1;
    fcrec->lfmxpt = minIndex + trace->ns;
    cout << "lmwrite the parameters from the incoming traces." << endl;
    cout << "lmwrite sample rate = " << samplerate << endl;
    cout << "lmwrite minmum sample value = " << fcrec->rfmnpt << endl;
    cout << "lmwrite maxmum sample value = " << fcrec->rfmxpt << endl;
    cout << "lmwrite minmum sample index = " << fcrec->lfmnpt << endl;
    cout << "lmwrite maxmum sample index = " << fcrec->lfmxpt << endl;
    cout << "lmwrite number of samples = " << nsmp << endl;
    cout << endl;


    fcrec->rfsamp = samplerate; //millisec
    fcrec->lftmdp = data_type;
    if (fcrec->lftmdp == 1)
        cout << "lmwrite data sampled in time" << endl;
    else if (fcrec->lftmdp == 2)
        cout << "lmwrite data sampled in depth" << endl;
    fcrec->lfhdfm = 0; //no header
    fcrec->lfhdln = 0; //header length
    fcrec->lfnxrg = 1; //x major y minor
    fcrec->lfusln = 0;
    fcrec->lfnxmd = 1;

    /*
     ** set values for each file format
     */

    if (inputType == 1) //3dv
    {
        fcrec->lforgn = 1;

        // set to 8 bit data

        fcrec->lffrmt = 8;
    }

    if (inputType == 2) //bri
    {
        fcrec->lforgn = file_type; //32bit float  bricked
        if (fcrec->lforgn == 4)
            cout << "lmwrite 32bit float bricked." << endl;
        else if (fcrec->lforgn == 5)
            cout << "lmwrite 16bit float bricked." << endl;
        else if (fcrec->lforgn == 6)
            cout << "lmwrite 8bit float bricked." << endl;
        else if (fcrec->lforgn == 7)
            cout << "lmwrite 16bit int. Values are stored as 16 bit integers but returned as floats." << endl;
        else if (fcrec->lforgn == 8)
            cout << "lmwrite 8bit int. Values are stored as 8 bit integers but returned as floats." << endl;

        fcrec->lffrmt = 0;
        fcrec->lbrxsz = 8;
        fcrec->lbrysz = 8;
        fcrec->lbrzsz = 16;
    }

    if (inputType == 3) //cmp
    {
        fcrec->lforgn = 3;

        // set fidelity for compress volume

        fcrec->rdstor = .10;
    }
}

/*
 **
 ** This function create the trace file, and if necessary  open for writing
 **
 */

void create_tracefile(dm3dfc_record* fcrec, int* ndxbuf, int ipdflg, const char* fileName) {

    int ier, mode;

    /*  try to open the input file to see if it exists  */
    mode = -2;


    dm3dop_(fileName, (void*) fcrec, &mode, &ier);

    cout << "lmwrite Initial dm3dop ier = " << ier << endl;
    dm3dcl_((void*) fcrec, &ier, ndxbuf);
    cout << "lmwrite Initial dm3dcl ier = " << ier << endl;

    // Open for writing
    mode = 3;
    dm3dop_(fileName, (void*) fcrec, &mode, &ier);
    cout << "lmwrite Main dm3dop ier = " << ier << endl;

    // Open memory cache for brick or compressed format

    int lfnxrg = fcrec->lfnxrg;
    dm3dco_(&lfnxrg, (void*) fcrec, &ier);
    // Complete the open ( make a working copy of the trace index area

    dm3dou_((void*) fcrec, &ier);

    cout << "lmwrite dm3dou ier = " << ier << endl;
    // validate pds against the seismic trace file
    dm3dpf_((void*) fcrec, &ipdflg, &ier);
    cout << "lmwrite dm3dpf ier = " << ier << endl;
}

/*
 **
 ** This function will create a simulated trace and write it to the trace file
 **
 */

void write_tracedata(int ipdflg, dm3dfc_record* fcrec, int* ndxbuf, segy *trc, FILE *f) {
    int ier;
    int ival1 = 1;
    short *strace = NULL;
    long *itrace = NULL;
    char *ctrace = NULL;
    float *rtrace = NULL;
    int mode = 1;
    int jfmnpy = fcrec->lfmnpy;
    int jfmxpy = fcrec->lfmxpy;


    // Setup buffer for index area
    dm3dnx_((void*) fcrec, &mode, &jfmnpy, &jfmxpy, &ival1, ndxbuf, &ier);
    cout << "lmwrite  dm3dnx ier = " << ier << endl;

    cout << "lmwrite the parameters actually applied to the output volume." << endl;
    cout << "lmwrite minmum sample value = " << fcrec->rfmnpt << endl;
    cout << "lmwrite maxmum sample value = " << fcrec->rfmxpt << endl;
    cout << "lmwrite minmum sample index = " << fcrec->lfmnpt << endl;
    cout << "lmwrite maxmum sample index = " << fcrec->lfmxpt << endl;


    int minSampleIndex = fcrec->lfmnpt;
    int maxSampleIndex = fcrec->lfmxpt;
    int numsample = maxSampleIndex - minSampleIndex + 1;
    cout << "lmwrite number of samples = " << numsample << endl;
    if (fcrec->lftmdp == 1)
        cout << "lmwrite data sampled in time" << endl;
    else if (fcrec->lftmdp == 2)
        cout << "lmwrite data sampled in depth" << endl;

    //isamp = 0;
    if (fcrec->lffrmt == 0)
        fprintf(stderr, "lmwrite sample format: floating point with fcrec->lffrmt = %ld\n", fcrec->lffrmt);
    if (fcrec->lffrmt == 8)
        ctrace = new char[numsample];
    else if (fcrec->lffrmt == 16)
        strace = new short[numsample];
    else if (fcrec->lffrmt == 32)
        itrace = new long[numsample];
    else if (fcrec->lffrmt == 0)
        rtrace = new float[numsample];
    float *sourceTr = new float[numsample];

    sourceTr = trc->data;
    if (fcrec->lffrmt == 8)
        write_trace(ipdflg, fcrec, ndxbuf, trc, sourceTr, ctrace);
    else if (fcrec->lffrmt == 16)
        write_trace(ipdflg, fcrec, ndxbuf, trc, sourceTr, strace);
    else if (fcrec->lffrmt == 32)
        write_trace(ipdflg, fcrec, ndxbuf, trc, sourceTr, itrace);
    else if (fcrec->lffrmt == 0);
    write_trace(ipdflg, fcrec, ndxbuf, trc, sourceTr, rtrace);

    // loop through through all the traces in the coming volume
    while (gettr(&tr)) {
        /*  generate trace buffer so that each trace in file is unique  */
        sourceTr = tr.data;
        if (fcrec->lffrmt == 8)
            write_trace(ipdflg, fcrec, ndxbuf, &tr, sourceTr, ctrace);
        else if (fcrec->lffrmt == 16)
            write_trace(ipdflg, fcrec, ndxbuf, &tr, sourceTr, strace);
        else if (fcrec->lffrmt == 32)
            write_trace(ipdflg, fcrec, ndxbuf, &tr, sourceTr, itrace);
        else if (fcrec->lffrmt == 0);
        write_trace(ipdflg, fcrec, ndxbuf, &tr, sourceTr, rtrace);
    }

    if (ctrace != NULL)
        delete ctrace;
    if (itrace != NULL)
        delete itrace;
    if (strace != NULL)
        delete strace;
    if (rtrace != NULL)
        delete rtrace;
}

void write_trace(int ipdflg, dm3dfc_record* fcrec, int* ndxbuf, segy *trc, void *source, void *target) {
    int ix, iy, ier;
    float filine, fxline;
    int ival0 = 0, ival1 = 1, ival8 = 8, ival16 = 16;



    int minSampleIndex = fcrec->lfmnpt;
    int maxSampleIndex = fcrec->lfmxpt;
    int numsample = maxSampleIndex - minSampleIndex + 1; //trc->ns;

    float dummy;
    filine = trc->ep;
    fxline = trc->cdp;
    cs2int_(&ipdflg, &filine, &fxline, &ix, &iy);
    if (!((fcrec->lfmnpx <= ix) && (ix <= fcrec->lfmxpx)) || !((fcrec->lfmnpy <= iy) && (iy <= fcrec->lfmxpy))) {
        fprintf(stderr, " lmwrite fcrec->lfmnpx = %ld\n", fcrec->lfmnpx);
        fprintf(stderr, " lmwrite fcrec->lfmxpx = %ld\n", fcrec->lfmxpx);
        fprintf(stderr, " lmwrite fcrec->lfmnpy = %ld\n", fcrec->lfmnpy);
        fprintf(stderr, " lmwrite fcrec->lfmxpy = %ld\n", fcrec->lfmxpy);
        err("lmwrite out of range or project not compatible\n");
        return;
    }
    int iread = 0;

    ival0 = 0;

    if (fcrec->lffrmt == 8) {
        dm3dft_((int*) source, &ival0, &ival1, &numsample, (int*) target, &ival8);
        dm3dtr_((void*) fcrec, &ix, &iy, &minSampleIndex, &maxSampleIndex, &iread, &ival0, (void*) target, (void*) & dummy, &ier, ndxbuf);
    }

    if (fcrec->lffrmt == 16) {
        // write 16 bit data
        dm3dft_((int*) source, &ival0, &ival1, &numsample, (int*) target, &ival16);

        dm3dtr_((void*) fcrec, &ix, &iy, &minSampleIndex, &maxSampleIndex, &iread, &ival0, (void*) target, (void*) & dummy, &ier, ndxbuf);

    }

    if (fcrec->lffrmt == 0) {
        dm3dft_((int*) source, &ival0, &ival1, &numsample, (int*) target, &ival0);
        dm3dtr_((void*) fcrec, &ix, &iy, &minSampleIndex, &maxSampleIndex, &iread, &ival0, (void*) target, (void*) & dummy, &ier, ndxbuf);

    }
}

void TrcDataShow(int ctr, int nsmp, float *rtrace) {
    int i, n;


    printf("trace # %d\n", ctr);

    for (i = n = 0; n < nsmp; n++) {
        printf("%11g ", rtrace[n]);
        i++;
        if (i == 6) {
            printf("\n");
            i = 0;
        }
    }
    printf("\n");
}
