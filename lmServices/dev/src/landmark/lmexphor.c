/*   Name        lmexphor.c
 **
 **   Function   export Landmark horizon files to the SU horizon format.
 **
 **
 */

#include <memory.h>
#include <math.h>
#include <iostream.h>
#include <iomanip>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <exception>
#include <fstream.h>
#include "sdlprototypes.h"
#include <string>
#include <vector>
#include <sstream>
#include <list>
#include "su.h"

using namespace std;
void close_proj(int type);
void exportHorizons(string projectName, int type, vector<string> horizons, int minline, int maxline, int mintrace, int maxtrace, string targetDir, int nullOk);
vector<string> tokenize(const string& str, const string& delimiters);
int search_hrz(const char *horizon, int max_hrz, int i_prj_type);
int exist(string s, vector<string> *v);
void close_hrz_file(int ipdflg);
void get_default_inc(int ipdflg, int minx, int miny, int major, int *outlinc, int *outtrinc);
void adjustValue(int val_type, float from_base, float to_base, float *val, float defInc, float inc);
//#define LANDMARK_NULL_VALUE 9999999933815812510711506376257961984.00
#define LANDMARK_NULL_VALUE1 1.0e+30
#define LANDMARK_NULL_VALUE2 -1.0e+30
#define BHPSU_NULL_VALUE -999.25
#define REMARK_LEN    61
#define PROJ_NAME_LEN 17
#define LINE_NAME_LEN 31
#define NO_OF_COLOR   3
#define NO_OF_BUF_IDX 32767
#define T(x,y,z)  z >= 0 ? x <= y : x >= y
extern "C" {
    void getp23_(int *prjtyp, int *mdflag);
    void fstbio_cache_init();
    char* fstbio_find_suf_all(char* suffix, int init);
    char* fstbio_find_pre_suf_all(char *suffix, char *prefix, int init);
}

const int MAX_FILE_NAME = 36;

enum FileTypes {
    STD_2D, STD_3D, BRICK, COMPRESS
};

char *sdoc[] = {
    "",
    " LMEXPHOR - export LANDMARK horizon(s)",
    "",
    "   lmexphor lm_project= lm_ptype= lm_horizons=",
    "",
    " Required parameter:",
    " lm_project=Landmark project name",
    " lm_ptype=Landmark project type (either 2 (2D) or 3 (3D))",
    " lm_horizons=Landmark horizon names delimited by a comma",
    "                                   ",
    " Optional parameters:  (default is based on the project's actual extents)	",
    " minline=min inline 	",
    " maxline=max inline	",
    " mintrace=min trace	",
    " maxtrace=max trace	",
    " output_nulls=0 (no) or 1 (yes) (0 by default)",
    " dir=target directory to export to	(default is the current directory)",
    "									",
    "",
    NULL
};

int main(int argc, char **argv) {
    char *prjname;
    char *targetDir;
    char *horizons;
    int ier, ipdflg;
    int minline, maxline, mintrace, maxtrace;

    initargs(argc, argv);
    requestdoc(1); /* stdin not used */


    /* Set filenames */
    MUSTGETPARSTRING("lm_project", &prjname);
    MUSTGETPARINT("lm_ptype", &ipdflg);
    MUSTGETPARSTRING("lm_horizons", &horizons);
    if (ipdflg == 2) {
        MUSTGETPARINT("minline", &minline);
    } else if (ipdflg == 3) {
        if (!getparint("minline", &minline)) minline = -1;
        if (!getparint("maxline", &maxline)) maxline = -1;
        if (!getparint("mintrace", &mintrace)) mintrace = -1;
        if (!getparint("maxtrace", &maxtrace)) maxtrace = -1;
    }
    vector<string> hlist = tokenize(horizons, ",");
    if (hlist.size() < 1) {
        cout << "invalid lm_horizons values " << endl;
        return 1;
    }
    int output_nulls;
    if (!getparint("output_nulls", &output_nulls)) output_nulls = 0;
    if (!getparstring("dir", &targetDir)) targetDir = "";

    if (ipdflg == 2) {
        if (minline != -1 && maxline != -1) {
            if (minline != maxline)
                cout << "For 2D project minline and maxline should be the same" << endl;
        }
    }
    string loc(targetDir);
    string inHorizons(horizons);
    fprintf(stderr, "inHorizons %s\n", inHorizons.c_str());
    string projectName(prjname);
    fprintf(stderr, "projectName %s\n", projectName.c_str());

    prjin2_(&ier, &ipdflg, (char*) projectName.c_str(), projectName.size());

    if (ier != 0) {
        fprintf(stderr, "prjin2 error %d\n", ier);
        if (ier == 1) {
            fprintf(stderr, "Error: Project Definition file not found (.pds or .ps2)\n");
        } else if (ier == 2) {
            fprintf(stderr, "Error: Unable to log project on all drives\n");
        } else if (ier == 4) {
            fprintf(stderr, "Error: Could not obtain an OpenWorks license\n");
        } else {
            fprintf(stderr, "Unknown error in opening project. \n");
        }
        close_proj(ipdflg);
        return ier;
    }
    cout << "projectName " << projectName.c_str() << endl;
    cout << "ipdflg " << ipdflg << endl;
    cout << "param minline " << minline << endl;
    cout << "param maxline " << maxline << endl;
    cout << "param mintrace " << mintrace << endl;
    cout << "param maxtrace " << maxtrace << endl;
    cout << "loc " << loc << endl;
    exportHorizons(projectName, ipdflg, hlist, minline, maxline, mintrace, maxtrace, loc, output_nulls);

    /*  close the project  */
    close_proj(ipdflg);
    return 0;
}

void exportHorizons(string projectName, int type, vector<string> horizons, int minline, int maxline, int mintrace, int maxtrace, string targetDir, int nullOk) {
    int ier, ipdflg = type, mdflag;
    float *data;
    char lname[LINE_NAME_LEN];
    int mininx, mininy, maxinx, maxiny, ixmin, ixmax, iymin, iymax, ix, iy;
    int istat, maxhrz, index;
    int iacthz, ix1, ix2, iy1, iy2, idum;
    unsigned int ii;
    int iop, line, iskip, ival1 = 1;
    float rnul, rline, rtrace, dummy, spn, rdumx, rdumy;
    double rx, ry;
    vector<string> alllines;
    float fminline, fmaxline, fmintrace, fmaxtrace;
    /*  report on project extents  */
    if (ipdflg == 3) {
        srvbnd_(&ipdflg, &mininx, &mininy, &maxinx, &maxiny, &ier);
        if (ier) {
            printf(" srvbnd error: %d\n", ier);
            return;
        }
        float pfminline, pfmintrace, pfmaxline, pfmaxtrace;
        cs2ano_(&ipdflg, &mininx, &mininy, &pfminline, &pfmintrace);
        cs2ano_(&ipdflg, &maxinx, &maxiny, &pfmaxline, &pfmaxtrace);
        cout << "project " << projectName << " extents: " << "(mininline,maxinline) = (" << pfminline << "," << pfmaxline
                << ") (minxline,maxxline) = (" << pfmintrace << "," << pfmaxtrace << ")" << endl;
        /*  open the horizon file  */
        rnul = 1.0e+37;
        hrzopn_(&istat, projectName.c_str(), &ixmin, &ixmax, &iymin, &iymax, &rnul, &maxhrz);
        float hfminline, hfmintrace, hfmaxline, hfmaxtrace;
        cs2ano_(&ipdflg, &ixmin, &iymin, &hfminline, &hfmintrace);
        cs2ano_(&ipdflg, &ixmax, &iymax, &hfmaxline, &hfmaxtrace);
        cout << "horizon subsystem extents: " << " extents: " << "(mininline,maxinline) = (" << hfminline << "," << hfmaxline
                << ") (minxline,maxxline) = (" << hfmintrace << "," << hfmaxtrace << ")" << endl;
        if (istat == 0) {
            printf(" horizon file opened \n");
            printf(" current number of horizons: %d\n", maxhrz);
        } else if (istat == 2) {
            printf(" horizon file was created. I am going to quit. \n");
            close_hrz_file(ipdflg);
            return;
        } else {
            printf(" hrzopn error: %d\n", istat);
            close_hrz_file(ipdflg);
            return;
        }

        /*  check that survey bounds match horizon file bounds  */
        if ((maxinx - mininx != ixmax - ixmin) || (maxiny - mininy != iymax - iymin)) {
            printf(" survey mismatch with horizon file \n");
            close_hrz_file(ipdflg);
            return;

        }


        getp23_(&ipdflg, &mdflag);

        cout << "mdflag (major) = " << mdflag << endl;
        cout << "Before adjustment " << endl;
        cout << "minline = " << minline << " maxline = " << maxline << endl;
        cout << "mintrace = " << mintrace << " maxtrace = " << maxtrace << endl;
        cout << "ixmin = " << ixmin << " iymin = " << iymin << endl;
        cout << "ixmax = " << ixmax << " iymax = " << iymax << endl;

        int def_line_inc, def_xline_inc;
        get_default_inc(ipdflg, mininx, mininy, mdflag, &def_line_inc, &def_xline_inc);
        cout << "def_line_inc = " << def_line_inc << endl;
        cout << "def_xline_inc = " << def_xline_inc << endl;
        if (minline != -1 && maxline != -1) {
            fminline = minline;
            adjustValue(0, pfminline, pfmaxline, &fminline, def_line_inc, def_line_inc);
            fmaxline = maxline;
            adjustValue(1, pfminline, pfmaxline, &fmaxline, def_line_inc, def_line_inc);
            cout << "adjusted inline" << endl;
            cout << "minline = " << fminline << " maxline = " << fmaxline << endl;
        } else if (minline == -1 && maxline != -1) {
            fmaxline = maxline;
            adjustValue(1, pfminline, pfmaxline, &fmaxline, def_line_inc, def_line_inc);
            fminline = pfminline;

        } else if (minline != -1 && maxline == -1) {
            fminline = minline;
            adjustValue(0, pfminline, pfmaxline, &fminline, def_line_inc, def_line_inc);
            fmaxline = pfmaxline;
        } else if (minline == -1 && maxline == -1) {
            fminline = pfminline;
            fmaxline = pfmaxline;
        }

        if (mintrace != -1 && maxtrace != -1) {
            fmintrace = mintrace;
            adjustValue(0, pfmintrace, pfmaxtrace, &fmintrace, def_xline_inc, def_xline_inc);
            fmaxtrace = maxtrace;
            adjustValue(1, pfmintrace, pfmaxtrace, &fmaxtrace, def_xline_inc, def_xline_inc);
            cout << "adjusted cross line" << endl;
            cout << "mintrace = " << fmintrace << " maxtrace = " << fmaxtrace << endl;
        } else if (mintrace == -1 && maxtrace != -1) {
            fmaxtrace = maxtrace;
            adjustValue(1, pfmintrace, pfmaxtrace, &fmaxtrace, def_xline_inc, def_xline_inc);
            fmintrace = pfmintrace;
        } else if (mintrace != -1 && maxtrace == -1) {
            fmintrace = mintrace;
            adjustValue(0, pfmintrace, pfmaxtrace, &fmintrace, def_xline_inc, def_xline_inc);
            fmaxtrace = pfmaxtrace;
        } else if (mintrace == -1 && maxtrace == -1) {
            fmintrace = pfmintrace;
            fmaxtrace = pfmaxtrace;

        }
        cout << "After adjustment" << endl;
        cout << "minline = " << fminline << endl;
        cout << "maxline = " << fmaxline << endl;
        cout << "mintrace = " << fmintrace << endl;
        cout << "maxtrace = " << fmaxtrace << endl;
        cs2int_(&ipdflg, &fminline, &fmintrace, &ix1, &iy1);
        cs2int_(&ipdflg, &fmaxline, &fmaxtrace, &ix2, &iy2);
        cout << "ix1 = " << ix1 << " ix2 = " << ix2 << endl;
        cout << "iy1 = " << iy1 << " iy2 = " << iy2 << endl;
        cout << "ix1 = " << ix1 << " iy1 = " << iy1 << endl;
        cout << "ix2 = " << ix2 << " iy2 = " << iy2 << endl;

        if (!((ix1 >= ixmin && ix1 <= ixmax) || (ix2 >= ixmin && ix2 <= ixmax) || (iy1 >= iymin && iy1 <= iymax) || (iy2 >= iymin && iy2 <= iymax))) {
            cout << "Out of range in Lines or Traces fields." << endl;
            close_hrz_file(ipdflg);
            return;
        }
    } else {
        /*
         **  pjhzsz is to get the number of all/active-only
         **  horizon from 2d <project>.hrz_cat.
         **  1st argument is total horizon in the project
         **  2nd argument is active horizon
         */
        pjhzsz_(&maxhrz, &iacthz, &ier);
        if (ier) {
            printf("error in opening horizon catalog.\n");
            close_hrz_file(ipdflg);
            //close_proj(ipdflg);
            return;
        }
        /* get number of lines in the project */
        pjlnsz_(&idum, &ixmax, &ier);
        if (ier) {
            printf(" error in reading line catalog.\n");
            close_hrz_file(ipdflg);
            //close_proj(ipdflg);
            return;
        }
        ixmin = 1;
        if (minline < ixmin) {
            printf("Bad range, minline must be equal or greater than %d\n", ixmin);
            close_hrz_file(ipdflg);
            return;
        }
        ix1 = minline;
        ix2 = ix1;
        lnolnm_(&ix1, lname, &ier);

        lny1y2_(&ix1, &mininy, &maxiny, &ier);
        printf("2D iymin = %d\n", mininy);
        printf("2D iymax = %d\n", maxiny);
        if (mintrace == -1)
            iy1 = mininy;
        else
            iy1 = mintrace;
        if (maxtrace == -1)
            iy2 = maxiny;
        else
            iy2 = maxtrace;
        if (!(iy1 >= mininy && iy2 <= maxiny)) {
            cout << "Trace range mismatch with the 2D horizon." << endl;
            close_hrz_file(ipdflg);
            return;
        }
    }

    for (ii = 0; ii < horizons.size(); ii++) {
        int hrz_id = search_hrz(horizons[ii].c_str(), maxhrz, ipdflg);
        cout << "hrz_id = " << hrz_id << endl;
        if (hrz_id == -1) {
            cout << "Can not find or run into issues with finding horizon " << horizons[ii].c_str() << endl;
            continue;
        }

        string fileName("");
        if (ipdflg == 2) {
            fileName += lname;
        }
        if (targetDir.size() == 0) {
            //fileName = get_current_dir_name();
            fileName = "./" + fileName + horizons[ii];
        } else
            fileName = targetDir + "/" + fileName + horizons[ii];
        fileName = fileName + ".xyz";
        cout << " About to create file " << fileName << endl;
        ofstream oFile(fileName.c_str(), ios::out);

        if (!oFile || !oFile.is_open()) {
            cout << "Error opening output file " << fileName << endl;
            return;
        }

        /*  read horizon data  */
        if (ipdflg == 3)
            data = new float[(ix2 - ix1 + 1)*(iy2 - iy1 + 1)];
        else
            data = new float[32767];

        if (ipdflg == 3) {
            iop = 1;

            iskip = iy2 - iy1 + 1;
            hrzrw_(&hrz_id, &iop, &istat, &ix1, &ix2, &iy1, &iy2, data, &iskip);
            if (istat) {
                if (istat == 3)
                    cout << "Read error! check to see if the range values are correct?" << endl;
                else if (istat == 4)
                    cout << "Region not entirely in bounds" << endl;
                printf(" hrzrw error: %d\n", istat);
                close_hrz_file(ipdflg);
                return;
            }
        }

        list<string> ml;
        if (hrz_id != -1) {
            index = 0;
            try
            {
                oFile.exceptions(fstream::failbit);
                for (ix = ix1; ix <= ix2; ix++) {
                    /*  read one line at a time  */
                    if (ipdflg == 2) {
                        iop = 1;
                        cout << "ix = " << ix << endl;
                        cout << "iy1 = " << iy1 << endl;
                        cout << "iy2 = " << iy2 << endl;
                        iskip = iy2 - iy1 + 1;
                        hrzrw_(&hrz_id, &iop, &istat, &ix, &ix, &iy1, &iy2, data, &iskip);
                        if (istat) {
                            printf(" hrzrw error: %d\n", istat);
                            close_hrz_file(ipdflg);
                            return;
                        }
                    }
                    for (iy = iy1; iy <= iy2; iy++)
                        /*
                         **  report horizon value being output in line, trace coordinates and
                         **  in real world coordinates
                         */ {
                        cs2ano_(&ipdflg, &ix, &iy, &rline, &rtrace);
                        if (ipdflg == 3) {
                            rdumx = ix;
                            rdumy = iy;
                            intorw_(&ival1, &rdumx, &rdumy, &rx, &ry, &dummy);
                        } else {
                            line = (int) rline;
                            if (iy == iymin) {
                                lnolnm_(&line, lname, &ier);
                                if (ier == 1) {
                                    printf("line %d not defined\n", line);
                                    continue;
                                } else if (ier) {
                                    printf("lnolnm ier: %d\n", ier);
                                    close_hrz_file(ipdflg);
                                    return;
                                }
                                printf(" line - %s\n", lname);
                            }
                            trtosp_(&line, &spn, &rtrace, &ier);
                            if (ier) {
                                printf("trtosp ier: %d\n", ier);
                                close_hrz_file(ipdflg);
                                return;
                            }
                            sptoxy_(&line, &spn, &rx, &ry, &ier);
                            if (ier) {
                                printf("sptoxy ier: %d\n", ier);
                                close_hrz_file(ipdflg);
                                return;

                            }
                        }
                        if (data[index] > LANDMARK_NULL_VALUE1 || data[index] < LANDMARK_NULL_VALUE2) {
                            data[index] = (float) BHPSU_NULL_VALUE;
                        }

                        if ((data[index] != (float) BHPSU_NULL_VALUE) || nullOk) {
                            if (mdflag == 1) {
                                oFile << setw(14) << (int) rline << "."
                                        << setw(15) << (int) rtrace << "."
                                        << setiosflags(ios::fixed) << setw(31) << setprecision(4) << data[index] << endl;
                            } else if (mdflag == 2) {
                                stringstream ss;
                                ss << setw(14) << (int) rline << "."
                                        << setw(15) << (int) rtrace << "."
                                        << setiosflags(ios::fixed) << setw(31) << setprecision(4) << data[index];
                                ml.push_front(ss.str());
                            }
                        }

                        index++;
                    }
                }
                if (mdflag == 2) {
                    ml.sort();

                    if (fminline <= fmaxline) {
                        cout << "min inline= " << fminline << " max inline= " << fmaxline << endl;
                        list<string>::iterator it;
                        for (it = ml.begin(); it != ml.end(); ++it) {
                            oFile << *it << endl;
                        }
                    } else {
                        cout << "min inline= " << fminline << " max inline= " << fmaxline << endl;
                        list<string>::reverse_iterator rit;
                        for (rit = ml.rbegin(); rit != ml.rend(); ++rit) {
                            oFile << *rit << endl;
                        }
                    }
                }
            }
catch(std::exception & e) {
                std::cout << "Exception caught: " << e.what() << endl;
                return;
            }
        }
        if (oFile.is_open())
            oFile.close();
    }
    close_hrz_file(ipdflg);
    return;
}

void close_proj(int type) {
    int ier;
    prjend_(&type, &ier);
    if (ier)
        err(" prjend ier: %d\n", ier);
    return;
}

int search_hrz(const char *horizon, int max_hrz, int i_prj_type) {
    int icolor[NO_OF_COLOR];
    char remark[REMARK_LEN];
    int istat, itype = 1, maxhrz = max_hrz;
    int i;
    int iop, id, ionset, nxtseg, lstend;
    int minus_one = -1, ier;
    float zmin, zmax;

    for (i = 1; i <= maxhrz; i++) {
        iop = 1;
        id = 0;
        istat = 0;
        hrzhdr_(&i, &iop, &istat, &id, remark, &zmin, &zmax, icolor,
                &ionset, &itype, &nxtseg, &lstend);

        if (id > 0 && istat != 1 && istat != 0) {
            printf(" hrzhdr error: %d\n", istat);
            return -1;
        }
        /*   close each hzd ( warning this is not in the manual)  */
        if (i_prj_type == 3) hzdcls_(&minus_one, &i, &ier);

        if (istat == 1) { //the given id does not exist
            continue;
        }

        /*  id==0 means deleted or not-active horizon  */
        if (id != 0) {
            string h_name(remark);

            if (h_name.compare(0, strlen(horizon), horizon) == 0) {
                printf("found a matching horizon: %s \n", horizon);
                return i;
            }
        }
    }
    return -1;
}

void close_hrz_file(int ipdflg) {
    int istat;
    if (ipdflg == 3) {
        hrzcls_(&istat);
        if (istat)
            printf(" hrzcls error: %d\n", istat);
    }
}

vector<string> tokenize(const string& str, const string& delimiters) {
    vector<string> tokens;

    string::size_type lastPos = 0, pos = 0;
    int count = 0;

    if (str.length() < 1) return tokens;

    // skip delimiters at beginning.
    lastPos = str.find_first_not_of(delimiters, 0);

    if ((str.substr(0, lastPos - pos).length()) > 0) {
        count = str.substr(0, lastPos - pos).length();

        for (int i = 0; i < count; i++)
            tokens.push_back("");

        if (string::npos == lastPos)
            tokens.push_back("");
    }

    // find first "non-delimiter".
    pos = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos) {
        // found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));

        // skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);

        if ((string::npos != pos) && (str.substr(pos, lastPos - pos).length() > 1)) {
            count = str.substr(pos, lastPos - pos).length();

            for (int i = 0; i < count; i++)
                tokens.push_back("");
        }

        pos = str.find_first_of(delimiters, lastPos);
    }

    return tokens;
}

int exist(string s, vector<string> *v) {
    for (int i = 0; i < v->size(); i++) {
        if (i == 0)
            cout << "hzd file = " << v->at(i) << endl;
        if (v->at(i).compare(0, s.length(), s) == 0)
            return 1;
    }
    return 0;
}

void get_default_inc(int ipdflg, int minx, int miny, int major, int *outlinc, int *outtrinc) {
    int ix = minx;
    int iy = miny;
    float rline_min, rtrace_min, rline_next, rtrace_next;

    cs2ano_(&ipdflg, &ix, &iy, &rline_min, &rtrace_min);

    if (major == 1) { //line major
        iy++;
        cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
        *outtrinc = rtrace_next - rtrace_min;
        ix++;
        iy--;
        cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
        *outlinc = rline_next - rline_min;
    } else if (major = 2) { //trace major
        iy++;
        cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
        *outlinc = rline_next - rline_min;
        ix++;
        iy--;
        cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
        *outtrinc = rtrace_next - rtrace_min;
    }

}

/** 
 * adjust fromVal:toVal:inc
 * param: val_type 0: "from" type, 1: "to" type               input
 * param: base on which the value is to be adjusted           input
 * param: val  the target value to be adjusted                input/output
 * param: defInc  the default increment value				  input
 * param: inc  the increment value supplied by the user       input
 */
void adjustValue(int val_type, float from_base, float to_base, float *val, float defInc, float inc) {
    if (val_type == 0) { //from
        if ((from_base >= *val && defInc > 0) || (from_base <= *val && defInc < 0)) {
            *val = from_base;
            return;
        }
    } else { //to
        if ((to_base <= *val && defInc > 0) || (to_base >= *val && defInc < 0)) {
            *val = to_base;
            return;
        }
    }
    if ((defInc > 0 && inc < 0) || (defInc < 0 && inc > 0))
        inc = defInc;
    if (defInc * inc > 0) {
        if ((defInc > 0 && defInc > inc) || (defInc < 0 && defInc < inc))
            inc = defInc;
    }
    int rem = (int) inc % (int) defInc;
    float adjusted_inc;
    if (rem != 0) {
        cout << "rem = " << rem << endl;
        ;
        adjusted_inc = inc - rem;
    } else
        adjusted_inc = inc;
    cout << "adjusted inc = " << adjusted_inc << endl;
    float diff = *val - from_base;
    rem = (int) diff % (int) adjusted_inc;
    if (rem != 0) {
        cout << "rem = " << rem << endl;
        *val = *val - rem;
    }
}