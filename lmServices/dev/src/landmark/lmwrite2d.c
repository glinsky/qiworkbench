/*   Name        seisWrite2d.C
 **
 **   Function    Writing 2D trace data.
 **
 **   Notes       This example shows the necessary sequences to
 **               read a trace file (.2v2 file).
 **               Information concerning the file header record
 **               can be found in dm3dfc.rec.
 **
 */


#include <math.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <iostream.h>
#include <fstream.h>
#include "sdlprototypes.h"
#include <string>
#include <vector>
#include "su.h"
#include "segy.h"

using namespace std;

void build_fcrec(dm3dfc_record*, int, int, int, int, segy*);
void create_tracefile(dm3dfc_record*, int*, int, char*);
void write_tracedata(int, dm3dfc_record*, int*, segy*);
void write_trace(int ipdflg, dm3dfc_record* fcrec, int* ndxbuf, segy *trc, void *source, void *target);
vector<string> tokenize(const string& str, const string& delimiters);

void close_proj(int type);


#define PROJ_NAME_LEN 17
#define LINE_NAME_LEN 31
#define NO_OF_BUF_IDX 32767
#define NO_OF_SAMPLES   251
#define FILE_NAME_LEN 256
#define PROVER_LEN    10

/*********************** self documentation **********************/
char *sdoc[] = {
    "									",
    " LMWRITE2D - write an Landmark 2D output file	",
    "									",
    " lmwrite2d <stdin lm_project= lm_ptype= lm_plevel= lm_lines",
    "									",
    " Required parameter:",
    " lm_project=Landmark project name",
    " lm_ptype=Landmark project type -- 2D",
    " lm_plevel=Landmark 2D process level and version",
    " lm_lines=List of 2D lines associated with the lm_plevel",
    "									",
    NULL
};

/**************** end self doc ***********************************/

segy tr;

main(int argc, char **argv) {
    char lname[LINE_NAME_LEN];
    char *prjname;
    char fname[FILE_NAME_LEN + 1];
    char *prover;
    char *lines;
    int mininx, mininy, maxinx, maxiny, ix;
    int ier, ipdflg, maxlin, idum;
    int ndxbuf[NO_OF_BUF_IDX];
    struct dm3dfc_record fcrec;

    printf(" lmwrite2d program \n");

    /* Initialize */
    initargs(argc, argv);
    requestdoc(1);

    /* Get parameters */
    MUSTGETPARSTRING("lm_project", &prjname);
    MUSTGETPARSTRING("lm_plevel", &prover);
    MUSTGETPARSTRING("lm_lines", &lines);

    string projectName(prjname);
    int projectType = 2;
    ipdflg = projectType;
    vector<string> llist = tokenize(lines, ",");
    if (llist.size() < 1) {
        cout << "invalid lm_lines values " << endl;
        return 1;
    }
    prjin2_(&ier, &ipdflg, (char*) projectName.c_str(), projectName.length());

    if (ier != 0) {
        err("prjin2 error %d\n", ier);
        if (ier == 1) {
            fprintf(stderr, "Error: Project Definition file not found (.pds or .ps2)\n");
        } else if (ier == 2) {
            fprintf(stderr, "Error: Unable to log project on all drives\n");
        } else if (ier == 4) {
            fprintf(stderr, "Error: Could not obtain an OpenWorks license\n");
        } else {
            fprintf(stderr, "Unknown error in opening project. \n");
        }

        close_proj(ipdflg);
        return ier;
    }

    /*  pjlnsz returns the total number of lines for a 2d project  */
    pjlnsz_(&idum, &maxlin, &ier);
    mininx = 1;
    maxinx = maxlin;
    segy trace1;
    if (!gettr(&trace1)) {
        err("Can not get first trace.");
    }
    // loop through each line
    for (int ii = 0; ii < llist.size(); ii++) {
        int linenum;
        //Return the unique 2D line identifier given a line name.
        int ival30 = 30;
        strcpy(lname, llist[ii].c_str());
        pumknm_(&ival30, lname, lname, &ier);
        lnotst_(&linenum, lname, &ier);
        if (ier) {
            cout << "No line found with given name: " << llist[ii].c_str() << endl;
            continue;
        }

        cout << "linename = " << lname << endl;
        cout << "linenum = " << linenum << endl;
        cout << "prover = " << prover << endl;
        ix = linenum;
        if (ix != mininx) dm3dcl_(&fcrec, &ier, ndxbuf);
        // build the seismic file name for this line
        lnmtfn_(lname, prover, fname, &ier, LINE_NAME_LEN, PROVER_LEN, FILE_NAME_LEN);
        cout << "fname = " << fname << endl;

        // get the trace range for the line
        lny1y2_(&ix, &mininy, &maxiny, &ier);
        cout << "ix = " << ix << endl;
        cout << "mininy = " << mininy << endl;
        cout << "maxiny = " << maxiny << endl;

        /*
         ** Build the seismic file control recored
         */
        build_fcrec(&fcrec, mininx, maxinx, mininy, maxiny, &trace1);
        fprintf(stderr, "lmwrite2d sample format fcrec.lffrmt = %ld\n", fcrec.lffrmt);
        /*
         ** Create and open the seismic file based on the control record
         */

        create_tracefile(&fcrec, ndxbuf, ipdflg, fname);

        /*
         ** Write out trace data into the just created file
         */
        fprintf(stderr, "lmwrite2d sample format fcrec.lffrmt = %ld\n", fcrec.lffrmt);
        write_tracedata(ipdflg, &fcrec, ndxbuf, &trace1);

        /*  close file  */
        cout << "\n\nClose seismic file." << endl;

        dm3dcl_((void*) & fcrec, &ier, ndxbuf);

        if (ier) printf(" dm3dcl ier: %d", ier);
        else cout << "SUCCESSFUL JOB COMPLETION!" << endl;

    }

    /*  close the project  */
    close_proj(ipdflg);
    return;
}

/*
 ** This function will assign the required values to the seismic control record
 ** This must be done before a new file is created.
 ** If the file already exists these values will be over written with
 ** those of the existing file
 */
void build_fcrec(dm3dfc_record* fcrec, int mininx, int maxinx, int mininy, int maxiny, segy *trace) {

    // -------- Initialize File Control Record (all zero).
    //  (void) memset( (void*)fcrec, 0, sizeof( fcrec* ) );

    fcrec->lfmnpx = mininx;
    fcrec->lfmxpx = maxinx;
    fcrec->lfmnpy = mininy;
    fcrec->lfmxpy = maxiny;
    cout << "lmwrite2d mininx = " << fcrec->lfmnpx << endl;
    cout << "lmwrite2d maxinx = " << fcrec->lfmxpx << endl;
    cout << "lmwrite2d mininy = " << fcrec->lfmnpy << endl;
    cout << "lmwrite2d maxiny = " << fcrec->lfmxpy << endl;

    int nsmp = trace->ns;
    int samplerate = trace->dt * 0.001; //millisec
    fcrec->rfmnpt = trace->delrt;
    fcrec->rfmxpt = trace->delrt + samplerate * (nsmp - 1);

    int minIndex = 0;
    if (samplerate != 0)
        minIndex = trace->delrt / samplerate;
    fcrec->lfmnpt = minIndex + 1;
    fcrec->lfmxpt = minIndex + trace->ns;
    cout << "lmwrite2d the parameters from the incoming traces." << endl;
    cout << "lmwrite2d sample rate = " << samplerate << endl;
    cout << "lmwrite2d minmum sample value = " << fcrec->rfmnpt << endl;
    cout << "lmwrite2d maxmum sample value = " << fcrec->rfmxpt << endl;
    cout << "lmwrite2d minmum sample index = " << fcrec->lfmnpt << endl;
    cout << "lmwrite2d maxmum sample index = " << fcrec->lfmxpt << endl;
    cout << "lmwrite2d number of samples = " << nsmp << endl;
    cout << endl;


    fcrec->rfsamp = samplerate; //millisec
    fcrec->lftmdp = 1;
    fcrec->lfhdfm = 0; //no header
    fcrec->lfhdln = 0; //header length
    fcrec->lfnxrg = 1; //x major y minor
    fcrec->lfusln = 0;
    fcrec->lfnxmd = 1;
    fcrec->lforgn = 1; //??
    fcrec->lffrmt = 0; //??
}

/*
 **
 ** This function create the trace file, and if necessary  open for writing
 **
 */

void create_tracefile(dm3dfc_record* fcrec, int* ndxbuf, int ipdflg, char* fileName) {

    int ier, mode;
    /*  try to open the input file to see if it exists  */
    mode = -2; //the Initialization Phase, the seismic file is not actually created
    dm3dop_(fileName, (void*) fcrec, &mode, &ier);
    cout << "lmwrite2d Initial dm3dop ier = " << ier << endl;
    //used to close a seismic file. It must be called when the seismic file has been opened for
    //writing, so that the File Control Record and Index Buffer will be correctly updated
    //in the seismic file.
    dm3dcl_((void*) fcrec, &ier, ndxbuf);
    cout << "lmwrite2d Initial dm3dcl ier = " << ier << endl;

    // Open for writing

    mode = 3;
    dm3dop_(fileName, (void*) fcrec, &mode, &ier);
    cout << "lmwrite2d Main dm3dop ier = " << ier << endl;

    // Complete the open ( make a working copy of the trace index area
    dm3dou_((void*) fcrec, &ier);
    cout << "lmwrite2d dm3dou ier = " << ier << endl;

}

void write_tracedata(int ipdflg, dm3dfc_record* fcrec, int* ndxbuf, segy *trc) {
    int ier;
    int ival1 = 1;
    int mode = 1;
    int jfmnpy = fcrec->lfmnpy;
    int jfmxpy = fcrec->lfmxpy;
    fprintf(stderr, "lmwrite2d sample format fcrec->lffrmt = %ld\n", fcrec->lffrmt);
    // Setup buffer for index area
    dm3dnx_((void*) fcrec, &mode, &jfmnpy, &jfmxpy, &ival1, ndxbuf, &ier);
    cout << "lmwrite2d  dm3dnx ier = " << ier << endl;

    int minSampleIndex = fcrec->lfmnpt;
    int maxSampleIndex = fcrec->lfmxpt;
    int numsample = maxSampleIndex - minSampleIndex + 1;
    cout << "lmwrite2d number of samples = " << numsample << endl;
    if (fcrec->lftmdp == 1)
        cout << "lmwrite2d data sampled in time" << endl;
    else if (fcrec->lftmdp == 2)
        cout << "lmwrite2d data sampled in depth" << endl;


    short *strace = NULL;
    long *itrace = NULL;
    char *ctrace = NULL;
    float *rtrace = NULL;
    if (fcrec->lffrmt == 8)
        ctrace = new char[numsample];
    else if (fcrec->lffrmt == 16)
        strace = new short[numsample];
    else if (fcrec->lffrmt == 32)
        itrace = new long[numsample];
    else if (fcrec->lffrmt == 0);
    rtrace = new float[numsample];

    float *sourceTr = new float[numsample];

    sourceTr = trc->data;
    if (fcrec->lffrmt == 8)
        write_trace(ipdflg, fcrec, ndxbuf, trc, sourceTr, ctrace);
    else if (fcrec->lffrmt == 16)
        write_trace(ipdflg, fcrec, ndxbuf, trc, sourceTr, strace);
    else if (fcrec->lffrmt == 32)
        write_trace(ipdflg, fcrec, ndxbuf, trc, sourceTr, itrace);
    else if (fcrec->lffrmt == 0);
    write_trace(ipdflg, fcrec, ndxbuf, trc, sourceTr, rtrace);

    while (gettr(&tr)) {
        /*  generate trace buffer so that each trace in file is unique  */
        sourceTr = tr.data;
        if (fcrec->lffrmt == 8)
            write_trace(ipdflg, fcrec, ndxbuf, &tr, sourceTr, ctrace);
        else if (fcrec->lffrmt == 16)
            write_trace(ipdflg, fcrec, ndxbuf, &tr, sourceTr, strace);
        else if (fcrec->lffrmt == 32)
            write_trace(ipdflg, fcrec, ndxbuf, &tr, sourceTr, itrace);
        else if (fcrec->lffrmt == 0);
        write_trace(ipdflg, fcrec, ndxbuf, &tr, sourceTr, rtrace);
    }
    if (ctrace != NULL)
        delete ctrace;
    if (itrace != NULL)
        delete itrace;
    if (strace != NULL)
        delete strace;
    if (rtrace != NULL)
        delete rtrace;

}

/*
 **
 ** This function will create a simulated trace and write it to the trace file
 **
 */
void write_trace(int ipdflg, dm3dfc_record* fcrec, int* ndxbuf, segy *trc, void *source, void *target) {
    int ier;
    int ix, iy;
    int iline = 1;
    float filine, fxline;
    int ival0 = 0, ival1 = 1, ival8 = 8, ival16 = 16;
    int iread;
    float dummy;


    int minSampleIndex = fcrec->lfmnpt;
    int maxSampleIndex = fcrec->lfmxpt;
    int numsample = maxSampleIndex - minSampleIndex + 1;
    filine = trc->ep;
    fxline = trc->cdp;

    cs2int_(&ipdflg, &filine, &fxline, &ix, &iy);
    if (!((fcrec->lfmnpx <= ix) && (ix <= fcrec->lfmxpx)) || !((fcrec->lfmnpy <= iy) && (iy <= fcrec->lfmxpy))) {
        fprintf(stderr, " lmwrite2d fcrec->lfmnpx = %ld\n", fcrec->lfmnpx);
        fprintf(stderr, " lmwrite2d fcrec->lfmxpx = %ld\n", fcrec->lfmxpx);
        fprintf(stderr, " lmwrite2d fcrec->lfmnpy = %ld\n", fcrec->lfmnpy);
        fprintf(stderr, " lmwrite2d fcrec->lfmxpy = %ld\n", fcrec->lfmxpy);
        err("lmwrite2d out of range or project not compatible\n");
        return;
    }


    // set sample index range  for this trace write


    iread = 0;
    ival0 = 0;

    if (fcrec->lffrmt == 8) {
        dm3dft_((int*) source, &ival0, &ival1, &numsample, (int*) target, &ival8);
        dm3dtr_((void*) fcrec, &iline, &iy, &minSampleIndex, &maxSampleIndex, &iread, &ival0, (void*) target, (void*) & dummy, &ier, ndxbuf);
    } else if (fcrec->lffrmt == 16) {
        // write 16 bit data
        dm3dft_((int*) source, &ival0, &ival1, &numsample, (int*) target, &ival16);
        dm3dtr_((void*) fcrec, &iline, &iy, &minSampleIndex, &maxSampleIndex, &iread, &ival0, (void*) target, (void*) & dummy, &ier, ndxbuf);
    } else if (fcrec->lffrmt == 0) {
        dm3dft_((int*) source, &ival0, &ival1, &numsample, (int*) target, &ival0);
        dm3dtr_((void*) fcrec, &iline, &iy, &minSampleIndex, &maxSampleIndex, &iread, &ival0, (void*) target, (void*) & dummy, &ier, ndxbuf);
    }

}

vector<string> tokenize(const string& str, const string& delimiters) {
    vector<string> tokens;

    string::size_type lastPos = 0, pos = 0;
    int count = 0;

    if (str.length() < 1) return tokens;

    // skip delimiters at beginning.
    lastPos = str.find_first_not_of(delimiters, 0);

    if ((str.substr(0, lastPos - pos).length()) > 0) {
        count = str.substr(0, lastPos - pos).length();

        for (int i = 0; i < count; i++)
            tokens.push_back("");

        if (string::npos == lastPos)
            tokens.push_back("");
    }

    // find first "non-delimiter".
    pos = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos) {
        // found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));

        // skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);

        if ((string::npos != pos) && (str.substr(pos, lastPos - pos).length() > 1)) {
            count = str.substr(pos, lastPos - pos).length();

            for (int i = 0; i < count; i++)
                tokens.push_back("");
        }

        pos = str.find_first_of(delimiters, lastPos);
    }
    return tokens;
}

void close_proj(int type) {
    int ier;
    prjend_(&type, &ier);
    if (ier)
        printf(" prjend ier: %d\n", ier);
    return;
}

/*  INPUT linename
 *  OUTPUT linenum
 *  Assuming 2D project is already initialized (prjin2_ or prjini_ successfully executed)
 */
/*
void get_line_num_by_name(char *linename, int *linenum){
       // ------ Get the number of lines in project (needed to allocate memory).
   int totalLines = 0;
   int activeLines = 0;
   int status,id;
   int lastLine,index;
   char	name[MAX_LINE_NAME+1];
   pjlnsz_( &totalLines, &activeLines, &status );
   if ( status == OKAY )
   {
       int mininx = 1;
               int maxinx = activeLines;
               // loop through each line
               // ------ Loop to get line number and name for each line.  Print
           // ------ each, adding min and max trace from above.
           lastLine = 0;
               index = 0;
               while ( ! lastLine  &&  index < maxinx )
               {
                       int first = ! index;
                       retlin_( &first, &id, name, &lastLine, &status, MAX_LINE_NAME );
                       if ( status == OKAY )
                       {
                               name[MAX_LINE_NAME] = '\0';
                               if(!strcmp(name,linename)){
 *linenum = id;
                                       return;
                               }
                       }
                       else
                       {
                           cout << "FAILURE: retlin_: Bad Project Definition File or "
                                << "Line Catalog is empty." << endl;
                           lastLine = 1;
                       }
                       index++;
           }
   }
 *linenum  = -1;
   return;
}
 */

