/*   Name        lmexp2dhor.c
 **
 **   Function   export Landmark horizon files to the SU horizon format.
 **
 **
 */

#include <memory.h>
#include <math.h>
#include <iostream.h>
#include <iomanip>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <exception>
#include <fstream.h>
#include "sdlprototypes.h"
#include <string>
#include <vector>
#include "su.h"

using namespace std;
void close_proj(int type);
void exportHorizons(string projectName, int type, vector<string> horizons, int minline, int maxline, int mintrace, int maxtrace, string targetDir, int nullOk);
vector<string> tokenize(const string& str, const string& delimiters);
int search_hrz(const char *horizon, int max_hrz, int i_prj_type);
int exist(string s, vector<string> *v);
int line_horizon_match(char *prjnam, char *line_name, char *horizon_name);
int line_horizon_match1(char *line_name, char *horizon_name);
void close_hrz_file(int ipdflg);
//#define LANDMARK_NULL_VALUE 9999999933815812510711506376257961984.00
#define LANDMARK_NULL_VALUE1 1.0e+30
#define LANDMARK_NULL_VALUE2 -1.0e+30
#define BHPSU_NULL_VALUE -999.25
#define REMARK_LEN    61
#define PROJ_NAME_LEN 17
#define LINE_NAME_LEN 31
#define NO_OF_COLOR   3
#define NO_OF_BUF_IDX 32767
#define T(x,y,z)  z >= 0 ? x <= y : x >= y
extern "C" {
    void getp23_(int *prjtyp, int *mdflag);

    void strtrm_(char*, int);
    void setprj_(int*, char*, int);
    void locdef_(int*, char*, int*, int*, int*, int);
    void locnxt_(int*, char*, int);
    void fstbio_cache_init();
    char* fstbio_find_suf_all(char* suffix, int init);
    char* fstbio_find_pre_suf_all(char *suffix, char *prefix, int init);
    void inglct_(char*, int*, int);
}

const int MAX_FILE_NAME = 36;

enum FileTypes {
    STD_2D, STD_3D, BRICK, COMPRESS
};

char *sdoc[] = {
    "",
    " LMEXP2DHOR - export LANDMARK 2D horizon(s)",
    "",
    "   lmexp2dhor lm_project= lm_linename= lm_horizons=",
    "",
    " Required parameter:",
    " lm_project=Landmark 2D project name",
    " lm_linename=Landmark 2D Line name",
    " lm_horizons=Landmark 2D horizon names delimited by a comma",
    "                                   ",
    " Optional parameters:  (default is based on the project's actual extents)	",
    " mintrace=min trace	",
    " maxtrace=max trace	",
    " output_nulls=0 (no) or 1 (yes) (0 by default)",
    " dir=target directory to export to	(default is the current directory)",
    "									",
    " The output horizon file name will be in the form of linename_horizonname1.xyz, linename_horizonname2.xyz, ...",
    NULL
};

int main(int argc, char **argv) {
    char *prjname;
    char *linename;
    char *targetDir;
    char *horizons;
    int ier, ipdflg = 2;
    int minline, maxline, mintrace, maxtrace;

    initargs(argc, argv);
    requestdoc(1); /* stdin not used */


    /* Set filenames */
    MUSTGETPARSTRING("lm_project", &prjname);
    MUSTGETPARSTRING("lm_linename", &linename);
    MUSTGETPARSTRING("lm_horizons", &horizons);

    if (!getparint("mintrace", &mintrace)) mintrace = -1;
    if (!getparint("maxtrace", &maxtrace)) maxtrace = -1;

    vector<string> hlist = tokenize(horizons, ",");
    if (hlist.size() < 1) {
        cout << "invalid lm_horizons values " << endl;
        return 1;
    }
    int output_nulls;
    if (!getparint("output_nulls", &output_nulls)) output_nulls = 0;
    if (!getparstring("dir", &targetDir)) targetDir = "";

    //if(ipdflg == 2){
    //	if(minline != -1 && maxline != -1){
    //		if(minline != maxline)
    //			cout << "For 2D project minline and maxline should be the same" << endl;
    //	}
    //}

    string loc(targetDir);
    string inHorizons(horizons);
    fprintf(stderr, "inHorizons %s\n", inHorizons.c_str());
    string projectName(prjname);
    fprintf(stderr, "projectName %s\n", projectName.c_str());
    // initial global cache - this can take some time
    fstbio_cache_init();
    prjin2_(&ier, &ipdflg, (char*) projectName.c_str(), projectName.size());

    if (ier != 0) {
        fprintf(stderr, "prjin2 error %d\n", ier);
        if (ier == 1) {
            fprintf(stderr, "Error: Project Definition file not found (.pds or .ps2)\n");
        } else if (ier == 2) {
            fprintf(stderr, "Error: Unable to log project on all drives\n");
        } else if (ier == 4) {
            fprintf(stderr, "Error: Could not obtain an OpenWorks license\n");
        } else {
            fprintf(stderr, "Unknown error in opening project. \n");
        }
        close_proj(ipdflg);
        return ier;
    }
    int ival30 = 30;
    pumknm_(&ival30, linename, linename, &ier);
    lnotst_(&minline, linename, &ier);
    if (ier != 0) {
        fprintf(stderr, "lmexp2dhor: No line found with given name %s\n", linename);
        close_proj(ipdflg);
        return ier;
    }
    maxline = minline;
    cout << "projectName " << projectName.c_str() << endl;
    cout << "ipdflg " << ipdflg << endl;
    cout << "lineName " << linename << endl;
    cout << "minline " << minline << endl;
    cout << "maxline " << maxline << endl;
    cout << "mintrace " << mintrace << endl;
    cout << "maxtrace " << maxtrace << endl;
    cout << "loc " << loc << endl;
    exportHorizons(projectName, ipdflg, hlist, minline, maxline, mintrace, maxtrace, loc, output_nulls);

    /*  close the project  */
    close_proj(ipdflg);
    return 0;
}

void exportHorizons(string projectName, int type, vector<string> horizons,
        int minline, int maxline, int mintrace, int maxtrace, string targetDir, int nullOk) {
    int ier, ipdflg = type;
    float *data;
    char lname[LINE_NAME_LEN];
    int mininy, maxiny, ixmin, ixmax, iymin, ix, iy;
    int istat, maxhrz, index;
    int iacthz, ix1, ix2, iy1, iy2, idum;
    unsigned int ii;
    int iop, line, iskip;
    float rline, rtrace, spn;
    double rx, ry;
    vector<string> alllines;
    /*
     **  pjhzsz is to get the number of all/active-only
     **  horizon from 2d <project>.hrz_cat.
     **  1st argument is total horizon in the project
     **  2nd argument is active horizon
     */
    pjhzsz_(&maxhrz, &iacthz, &ier);
    if (ier) {
        printf("error in opening horizon catalog.\n");
        close_hrz_file(ipdflg);
        //close_proj(ipdflg);
        return;
    }
    /* get number of lines in the project */
    pjlnsz_(&idum, &ixmax, &ier);
    if (ier) {
        printf(" error in reading line catalog.\n");
        close_hrz_file(ipdflg);
        //close_proj(ipdflg);
        return;
    }
    ixmin = 1;
    if (minline < ixmin) {
        printf("Bad range, minline must be equal or greater than %d\n", ixmin);
        close_hrz_file(ipdflg);
        return;
    }
    ix1 = minline;
    ix2 = ix1;
    lnolnm_(&ix1, lname, &ier);

    lny1y2_(&ix1, &mininy, &maxiny, &ier);
    printf("2D iymin = %d\n", mininy);
    printf("2D iymax = %d\n", maxiny);
    if (mintrace == -1)
        iy1 = mininy;
    else
        iy1 = mintrace;
    if (maxtrace == -1)
        iy2 = maxiny;
    else
        iy2 = maxtrace;
    if (!(iy1 >= mininy && iy2 <= maxiny)) {
        cout << "Trace range mismatch with the 2D horizon." << endl;
        close_hrz_file(ipdflg);
        return;
    }

    for (ii = 0; ii < horizons.size(); ii++) {
        if (line_horizon_match1(lname, horizons[ii].c_str()) != 0) {
            cout << horizons[ii] << " and " << lname << " do not match" << endl;
            continue;
        }
        int hrz_id = search_hrz(horizons[ii].c_str(), maxhrz, ipdflg);
        cout << "hrz_id = " << hrz_id << endl;
        if (hrz_id == -1) {
            cout << "Can not find horizon " << horizons[ii].c_str() << endl;
        }

        string fileName("");
        if (ipdflg == 2) {
            char* tail = strstr(lname, "__");
            if (tail != 0)
                *tail = '\0';
            fileName += lname;
        }
        if (targetDir.size() == 0) {
            fileName = "./" + fileName + "_" + horizons[ii];
        } else
            fileName = targetDir + "/" + fileName + "_" + horizons[ii];
        fileName = fileName + ".xyz";
        cout << " fileName = " << fileName << endl;
        ofstream oFile(fileName.c_str(), ios::out);

        if (!oFile || !oFile.is_open()) {
            cout << "Error opening output file" << endl;
            return;
        }

        data = new float[iy2 - iy1 + 1];


        if (hrz_id != -1) {
            index = 0;
            try
            {
                oFile.exceptions(fstream::failbit);
                for (ix = ix1; ix <= ix2; ix++) {
                    /*  read one line at a time  */
                    if (ipdflg == 2) {
                        iop = 1;
                        cout << "ix = " << ix << endl;
                        cout << "iy1 = " << iy1 << endl;
                        cout << "iy2 = " << iy2 << endl;
                        iskip = iy2 - iy1 + 1;
                        hrzrw_(&hrz_id, &iop, &istat, &ix, &ix, &iy1, &iy2, data, &iskip);
                        if (istat) {
                            printf(" hrzrw error: %d\n", istat);
                            close_hrz_file(ipdflg);
                            return;
                        }
                    }
                    for (iy = iy1; iy <= iy2; iy++)
                        /*
                         **  report horizon value being output in line, trace coordinates and
                         **  in real world coordinates
                         */ {
                        cs2ano_(&ipdflg, &ix, &iy, &rline, &rtrace);
                        line = (int) rline;
                        if (iy == iymin) {
                            lnolnm_(&line, lname, &ier);
                            if (ier == 1) {
                                printf("line %d not defined\n", line);
                                continue;
                            } else if (ier) {
                                printf("lnolnm ier: %d\n", ier);
                                close_hrz_file(ipdflg);
                                return;
                            }
                            printf(" line - %s\n", lname);
                        }
                        trtosp_(&line, &spn, &rtrace, &ier);
                        if (ier) {
                            printf("trtosp ier: %d\n", ier);
                            close_hrz_file(ipdflg);
                            return;
                        }
                        sptoxy_(&line, &spn, &rx, &ry, &ier);
                        if (ier) {
                            printf("sptoxy ier: %d\n", ier);
                            close_hrz_file(ipdflg);
                            return;

                        }

                        if (data[index] > LANDMARK_NULL_VALUE1 || data[index] < LANDMARK_NULL_VALUE2) {
                            data[index] = (float) BHPSU_NULL_VALUE;
                        }
                        if ((data[index] != (float) BHPSU_NULL_VALUE) || nullOk) {
                            oFile << setw(14) << (int) rline
                                    << setw(15) << (int) rtrace
                                    << setiosflags(ios::fixed) << setw(31) << setprecision(4) << data[index] << endl;
                        }
                        index++;
                    }
                }
            }
catch(std::exception & e) {
                std::cout << "Exception caught: " << e.what() << endl;
                return;
            }
        }
        if (oFile.is_open())
            oFile.close();
    }
    close_hrz_file(ipdflg);
    return;
}

void close_proj(int type) {
    int ier;
    prjend_(&type, &ier);
    if (ier)
        err(" prjend ier: %d\n", ier);
    return;
}

int search_hrz(const char *horizon, int max_hrz, int i_prj_type) {
    int icolor[NO_OF_COLOR];
    char remark[REMARK_LEN];
    int istat, itype, maxhrz = max_hrz;
    int i;
    int iop, id, ionset, nxtseg, lstend;
    float zmin, zmax;

    for (i = 1; i <= maxhrz; i++) {
        iop = 1;
        id = 0;
        hrzhdr_(&i, &iop, &istat, &id, remark, &zmin, &zmax, icolor,
                &ionset, &itype, &nxtseg, &lstend);
        if (id > 0 && istat != 1 && istat != 0) {
            printf(" hrzhdr error: %d\n", istat);
            return -1;
        }

        /*   close each hzd ( warning this is not in the manual)  */
        //if(ipdflg == 3) hzdcls_(&minus_one, &i, &ier);

        /*  id==0 means deleted or not-active horizon  */
        if (id != 0) {
            string h_name(remark);

            if (h_name.compare(0, strlen(horizon), horizon) == 0)
                return i;
        }
    }
    return -1;
}

//check to see if linename______horizonname____.hzd_glb exist

int line_horizon_match1(char *line_name, char *horizon_name) {
    char prefix[256];
    char lname[LINE_NAME_LEN];
    char suffix[80];
    int ier, ival30 = 30, ival60 = 60;
    char remark[REMARK_LEN];

    strcpy(remark, horizon_name);
    pumknm_(&ival60, remark, remark, &ier);
    strcpy(lname, line_name);
    pumknm_(&ival30, lname, lname, &ier);
    /* list all .hzd_glb files */
    //strcpy(extlin, ".hzd_glb");
    strcpy(prefix, lname);
    strcat(prefix, remark);
    strcpy(suffix, ".hzd_glb");
    int init = 1;
    char* fname;
    if ((fname = fstbio_find_pre_suf_all(suffix, prefix, init)) != 0) {
        free(fname);
        return 0;
    }
    free(fname);
    return 1;
}

//check to see if linename______horizonname____.hzd_glb exist

int line_horizon_match(char *prjnam, char *line_name, char *horizon_name) {
    char filnam[256];
    char glbnam[256];
    char extlin[256];
    int ier, totfil, imode, i_1 = 1, L256 = 256;
    /* get the global area for the project */
    inglct_(glbnam, &ier, L256);
    if (ier != 0) {
        printf("error from inglct ier = %d\n", ier);
        exit(1);
    }

    strtrm_(glbnam, L256);

    /* set the global area for listing */

    setprj_(&ier, glbnam, L256);
    /* list all 01.2v2 files */
    strcpy(extlin, "*.hzd_glb");


    locdef_(&ier, extlin, &imode, &totfil, &i_1, L256);
    if (ier != 0) {
        printf("error from locdef ier = %d\n", ier);
        exit(1);
    }

    for (int i = 0; i < totfil; i++) {

        /* get files that match file card one by one */

        locnxt_(&ier, filnam, L256);
        if (ier != 0) {
            printf("error from locnxt ier = %d\n", ier);
            exit(1);
        }

        if (strstr(filnam, line_name) != 0 && strstr(filnam, horizon_name) != 0) {
            /* reset to seismic project */
            setprj_(&ier, prjnam, L256);
            return 0;
        }
    }
    /* reset to seismic project */
    setprj_(&ier, prjnam, L256);
    return 1;
}

void close_hrz_file(int ipdflg) {
    int istat;
    if (ipdflg == 3) {
        hrzcls_(&istat);
        if (istat)
            printf(" hrzcls error: %d\n", istat);
    }
}

vector<string> tokenize(const string& str, const string& delimiters) {
    vector<string> tokens;

    string::size_type lastPos = 0, pos = 0;
    int count = 0;

    if (str.length() < 1) return tokens;

    // skip delimiters at beginning.
    lastPos = str.find_first_not_of(delimiters, 0);

    if ((str.substr(0, lastPos - pos).length()) > 0) {
        count = str.substr(0, lastPos - pos).length();

        for (int i = 0; i < count; i++)
            tokens.push_back("");

        if (string::npos == lastPos)
            tokens.push_back("");
    }

    // find first "non-delimiter".
    pos = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos) {
        // found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));

        // skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);

        if ((string::npos != pos) && (str.substr(pos, lastPos - pos).length() > 1)) {
            count = str.substr(pos, lastPos - pos).length();

            for (int i = 0; i < count; i++)
                tokens.push_back("");
        }

        pos = str.find_first_of(delimiters, lastPos);
    }

    return tokens;
}

int exist(string s, vector<string> *v) {
    for (int i = 0; i < v->size(); i++) {
        if (i == 0)
            cout << "hzd file = " << v->at(i) << endl;
        if (v->at(i).compare(0, s.length(), s) == 0)
            return 1;
    }
    return 0;
}
