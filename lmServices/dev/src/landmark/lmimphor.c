#include <iostream.h>
#include <exception>
#include <fstream.h>
#include <sstream>
#include <string>
#include <vector>
#include "su.h"
#include "sdlprototypes.h"
char *sdoc[] = {
    "",
    " LMIMPHOR - import ASCII horizons (.XYZ files) to LANDMARK application ",
    "",
    "   lmimphor lm_project= lm_ptype= horizons=",
    "",
    " Required parameter:",
    " lm_project=Landmark project name",
    " lm_ptype=Landmark project type (either 2 (2D) or 3 (3D)) currently only support 3D",
    " horizons=horizon names (.XYZ files) delimited by a comma",
    "                                   ",
    "									",
    "",
    NULL
};

/**************** end self doc ***********************************/


using namespace std;
vector<string> tokenize(const string& str, const string& delimiters);
void stripLeadingAndTrailingBlanks(string& StringToModify);
void close_hrz_file(int ipdflg);
void close_proj(int type);
void get_line_num_by_name(char *linename, int *linenum);

typedef struct {
    double ep;
    double cdp;
    double zval;
} horizonline;
#define NO_OF_COLOR   3
#define REMARK_LEN    61
#define LANDMARK_NULL_VALUE 9999999933815812510711506376257961984.00
#define BHPSU_NULL_VALUE -999.25
const int OKAY = 0;
const int MAX_LINE_NAME = 30;

int main(int argc, char **argv) {
    char *prjname;
    char *horizons;
    int ier, ipdflg, linenum;
    int minline, maxline, mintrace, maxtrace;
    int icolor[NO_OF_COLOR];
    int istat, nxtseg, iop, lstend, iskip;
    float *data, zdum;
    char remark[REMARK_LEN];
    /* Initialize */
    initargs(argc, argv);
    requestdoc(1);

    /* Set project name */
    MUSTGETPARSTRING("lm_project", &prjname);
    MUSTGETPARINT("lm_ptype", &ipdflg);
    MUSTGETPARSTRING("horizons", &horizons);
    vector<string> hlist = tokenize(horizons, ",");
    if (hlist.size() < 1) {
        cout << "invalid horizons values " << endl;
        return 1;
    }
    string projectName(prjname);

    prjin2_(&ier, &ipdflg, (char*) projectName.c_str(), projectName.size());

    if (ier != 0) {
        fprintf(stderr, "prjin2 error %d\n", ier);
        if (ier == 1) {
            fprintf(stderr, "Error: Project Definition file not found (.pds or .ps2)\n");
        } else if (ier == 2) {
            fprintf(stderr, "Error: Unable to log project on all drives\n");
        } else if (ier == 4) {
            fprintf(stderr, "Error: Could not obtain an OpenWorks license\n");
        } else {
            fprintf(stderr, "Unknown error in opening project. \n");
        }

        close_proj(ipdflg);
        return ier;
    }
    cout << "projectName " << projectName.c_str() << endl;
    cout << "ipdflg " << ipdflg << endl;

    if (!getparint("minline", &minline)) minline = -1;
    if (!getparint("maxline", &maxline)) maxline = -1;
    if (!getparint("mintrace", &mintrace)) mintrace = -1;
    if (!getparint("maxtrace", &maxtrace)) maxtrace = -1;

    /*  report on project extents  */
    int mininx, mininy, maxinx, maxiny;
    if (ipdflg == 3) {
        srvbnd_(&ipdflg, &mininx, &mininy, &maxinx, &maxiny, &ier);
        if (ier) {
            printf(" srvbnd error: %d\n", ier);
            close_proj(ipdflg);
            ;
            return ier;
        }
        cout << " 3d project extents: " << endl;
        cout << "mininx = " << mininx << " mininy = " << mininy << endl;
        cout << "maxinx = " << maxinx << " maxiny = " << maxiny << endl;
        float fminline, fmintrace, fmaxline, fmaxtrace;
        cs2ano_(&ipdflg, &mininx, &mininy, &fminline, &fmintrace);
        cs2ano_(&ipdflg, &maxinx, &maxiny, &fmaxline, &fmaxtrace);
        cout << "fminline = " << fminline << " fmintrace = " << fmintrace << endl;
        cout << "fmaxline = " << fmaxline << " fmaxtrace = " << fmaxtrace << endl;
    }
    for (int i = 0; i < hlist.size(); i++) {

        string filePath(hlist[i]);
        int lastpos = -1;
        int pos = filePath.find("/", 0);
        while (pos >= 0) {
            pos = filePath.find("/", ++pos);
            if (pos >= 0)
                lastpos = pos;
        }
        cout << "lastpos=" << lastpos << endl;
        int xyzpos = filePath.find(".xyz", 0);
        if (xyzpos == -1) {
            cout << "Unexpected horizon file format not ending with .xyz" << endl;
            close_proj(ipdflg);
            return 1;
        }
        string fileName = filePath.substr(lastpos + 1, xyzpos - lastpos - 1);
        cout << "fileName=" << fileName << endl;
        cout << " filePath = " << filePath << endl;
        if (ipdflg == 2) {
            string line_name = fileName.substr(0, MAX_LINE_NAME);
            cout << "line_name=" << line_name << endl;
            get_line_num_by_name(line_name.c_str(), &linenum);
            cout << "linenum = " << linenum << endl;
        }
        fstream iFile(filePath.c_str(), ios::in);

        if (!iFile || !iFile.is_open()) {
            cout << "Error opening input file" << endl;
            return 1;
        }
        int miniline = -1;
        int maxiline = -1;
        int minxline = -1;
        int maxxline = -1;
        double minzval = -1;
        double maxzval = -1;
        char str[100];


        vector<horizonline> horizonlist;
        while (!iFile.eof()) {
            iFile.getline(str, 100);

            //stripLeadingAndTrailingBlanks(line);
            istringstream totalSString(str);
            double d1, d2, d3;
            totalSString >> d1 >> d2 >> d3;
            horizonline h;
            h.ep = d1;
            h.cdp = d2;
            h.zval = d3;
            horizonlist.push_back(h);
            if (miniline == -1)
                miniline = d1;
            else if (miniline >= d1)
                miniline = d1;
            if (maxiline == -1)
                maxiline = d1;
            else if (maxiline <= d1)
                maxiline = d1;

            if (minxline == -1)
                minxline = d2;
            else if (minxline >= d2)
                minxline = d2;
            if (maxxline == -1)
                maxxline = d2;
            else if (maxxline <= d2)
                maxxline = d2;

            if (minzval == -1)
                minzval = d3;
            else if (minzval >= d3 && d3 != BHPSU_NULL_VALUE)
                minzval = d3;
            if (maxzval == -1)
                maxzval = d3;
            else if (maxzval <= d3)
                maxzval = d3;
        }

        int ixmax, ixmin, iymin, iymax;
        cout << "minline=" << miniline << ",maxiline=" << maxiline << ",minxline=" << minxline << ",maxxline=" << maxxline << ",minzval=" << minzval << ",maxzval=" << maxzval << endl;
        float fminiline = miniline;
        float fmaxiline = maxiline;
        float fminxline = minxline;
        float fmaxxline = maxxline;
        cs2int_(&ipdflg, &fminiline, &fminxline, &ixmin, &iymin);
        cs2int_(&ipdflg, &fmaxiline, &fmaxxline, &ixmax, &iymax);
        cout << "ixmin=" << ixmin << ",ixmax=" << ixmax << ",iymin=" << iymin << ",iymax=" << iymax << endl;
        float rnul = 1.0e+37;
        int maxhrz;
        hrzopn_(&istat, prjname, &ixmin, &ixmax, &iymin, &iymax, &rnul, &maxhrz);
        if (istat == 0) {
            printf(" horizon file opened \n");
            printf("   current number of horizons: %d\n", maxhrz);
        } else if (istat == 2)
            printf(" horizon file was created \n");
        else {
            printf(" hrzopn error: %d\n", istat);
            close_proj(ipdflg);
            return 1;
        }

        /*  check that survey bounds match horizon file bounds  */
        if ((maxinx - mininx != ixmax - ixmin) || (maxiny - mininy != iymax - iymin)) {
            printf(" survey mismatch with horizon file \n");
            close_hrz_file(ipdflg);
            close_proj(ipdflg);
            return 1;
        }

        float zmin = minzval;
        float zmax = maxzval;


        if (ipdflg == 3)
            data = new float[ixmax * iymax];
        else
            data = new float[32767];

        for (int i = 0; i < horizonlist.size(); i++) {
            int ix, iy;
            float f1 = (float) horizonlist[i].ep, f2 = (float) horizonlist[i].cdp;
            cs2int_(&ipdflg, &f1, &f2, &ix, &iy);
            int index = (ix - 1)*(iymax - iymin + 1) + iy - 1;
            if (horizonlist[i].zval == BHPSU_NULL_VALUE)
                data[index] = LANDMARK_NULL_VALUE;
            else
                data[index] = horizonlist[i].zval;
            if (data[index] < zmin)
                zmin = data[index];
            if (data[index] > zmax && data[index] != LANDMARK_NULL_VALUE)
                zmax = data[index];
        }

        /*
         **  Find next available location in horizon file to put a new
         **  horizon and initialize it.  (horizon number is returned in numhrz.)
         **  (itype = 1 for a raster horizon.)
         */
        int itype = 1;
        icolor[0] = 255;
        icolor[1] = 200;
        icolor[2] = 200;
        int ionset = 1;

        int numhrz;
        int ival60 = 60;
        int id;
        if (ipdflg == 2) {
            /*
             **  *** important: remark must in valid format. use pumknm()
             **                 to make a correct format.
             **                 remark must declare as 60+1
             */
            strcpy(remark, fileName.c_str());
            pumknm_(&ival60, remark, remark, &ier);
            hz2crt_(&numhrz, &istat, &itype, icolor, &ionset, &itype, remark);
            if (istat) {
                printf(" hz2crt error: %d\n", istat);
                close_hrz_file(ipdflg);
            }
        } else {
            hrzcrt_(&itype, &numhrz, &istat);
            strcpy(remark, fileName.c_str());
            //sprintf(remark, "This is horizon number - %3d", numhrz);
            //printf(" writing horizon - %s\n", remark);
            if (istat) {
                printf(" hrzcrt error: %d\n", istat);
                close_hrz_file(ipdflg);
            }
            /*
             ** write the horizon header with following values:
             **     id     = 1                - time data flag
             **     remark = this is horizon number xx.
             **     zmin   =                  - smallest value
             **     zmax   =                  - largest value
             **     icolor = 255,200,200      - redish gray
             **     ionset = 1                - min auto picking onset
             **     itype  = 1                - raster horizon
             **     nxtseg = 0                - (unused in raster horizons)
             **     lstend = 0                - (unused in raster horizons)
             */
            id = 1;
            int itype = 1;
            nxtseg = 0;
            lstend = 0;
            iop = 2;
            hrzhdr_(&numhrz, &iop, &istat, &id, remark, &zmin, &zmax, icolor,
                    &ionset, &itype, &nxtseg, &lstend);
            if (istat) {
                printf(" hrzhdr error: %d\n", istat);
                close_hrz_file(ipdflg);
            }
        }


        /*  output horizon  */
        if (ipdflg == 3) {
            iop = 2;
            iskip = iymax - iymin + 1;
            hrzrw_(&numhrz, &iop, &istat, &ixmin, &ixmax, &iymin, &iymax, data, &iskip);
            if (istat) {
                printf(" hrzrw error: %d\n", istat);
                close_hrz_file(ipdflg);
            }
        }

        /*  update header (read header; then write header with new z values)  */
        iop = 1;
        hrzhdr_(&numhrz, &iop, &istat, &id, remark, &zdum, &zdum, icolor, &ionset,
                &itype, &nxtseg, &lstend);
        iop = 2;
        hrzhdr_(&numhrz, &iop, &istat, &id, remark, &zmin, &zmax, icolor, &ionset,
                &itype, &nxtseg, &lstend);
        if (istat) {
            printf(" hrzhdr error: %d\n", istat);
            close_hrz_file(ipdflg);
        }

        iFile.close();
        if (data != NULL)
            delete data;
        cout << endl;
    }
    close_hrz_file(ipdflg);
    close_proj(ipdflg);
    return 0;
}

vector<string> tokenize(const string& str, const string& delimiters) {
    vector<string> tokens;

    string::size_type lastPos = 0, pos = 0;
    int count = 0;

    if (str.length() < 1) return tokens;

    // skip delimiters at beginning.
    lastPos = str.find_first_not_of(delimiters, 0);

    if ((str.substr(0, lastPos - pos).length()) > 0) {
        count = str.substr(0, lastPos - pos).length();

        for (int i = 0; i < count; i++)
            tokens.push_back("");

        if (string::npos == lastPos)
            tokens.push_back("");
    }

    // find first "non-delimiter".
    pos = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos) {
        // found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));

        // skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);

        if ((string::npos != pos) && (str.substr(pos, lastPos - pos).length() > 1)) {
            count = str.substr(pos, lastPos - pos).length();

            for (int i = 0; i < count; i++)
                tokens.push_back("");
        }

        pos = str.find_first_of(delimiters, lastPos);
    }

    return tokens;
}

void stripLeadingAndTrailingBlanks(string& StringToModify) {
    if (StringToModify.empty()) return;

    int startIndex = StringToModify.find_first_not_of(" ");
    int endIndex = StringToModify.find_last_not_of(" ");
    string tempString = StringToModify;
    StringToModify.erase();

    StringToModify = tempString.substr(startIndex, (endIndex - startIndex + 1));
}

void close_proj(int type) {
    int ier;
    prjend_(&type, &ier);
    if (ier)
        err(" prjend ier: %d\n", ier);
    return;
}

/*  close the horizon file  */
void close_hrz_file(int ipdflg) {
    int istat;
    if (ipdflg == 3) {
        hrzcls_(&istat);
        if (istat)
            printf(" hrzcls error: %d\n", istat);
    }
}

/*  INPUT linename
 *  OUTPUT linenum
 *  Assuming 2D project is already initialized (prjin2_ or prjini_ successfully executed)
 */
void get_line_num_by_name(char *linename, int *linenum) {
    // ------ Get the number of lines in project (needed to allocate memory).
    int totalLines = 0;
    int activeLines = 0;
    int status, id;
    int lastLine, index;
    char name[MAX_LINE_NAME + 1];
    pjlnsz_(&totalLines, &activeLines, &status);
    if (status == OKAY) {
        int mininx = 1;
        int maxinx = activeLines;
        // loop through each line
        // ------ Loop to get line number and name for each line.  Print
        // ------ each, adding min and max trace from above.
        lastLine = 0;
        index = 0;
        while (!lastLine && index < maxinx) {
            int first = !index;
            retlin_(&first, &id, name, &lastLine, &status, MAX_LINE_NAME);
            if (status == OKAY) {
                name[MAX_LINE_NAME] = '\0';
                if (!strcmp(name, linename)) {
                    *linenum = id;
                    return;
                }
            } else {
                cout << "FAILURE: retlin_: Bad Project Definition File or "
                        << "Line Catalog is empty." << endl;
                lastLine = 1;
            }
            index++;
        }
    }
    *linenum = -1;
    return;
}