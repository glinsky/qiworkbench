#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <malloc.h>

#include "pla.h"
#include "LandmarkProjectHandler.h"



#include <ow/OpenWorks.h>
#include <ow/prj.h>
#include <ow/gdincl/gdiWell.h>
#include <ow/gdincl/gdiLogCurve.h>
#include <ow/gdincl/gdiMeasSystem.h>
#include <ow/log.h>
#include <ow/lut.h>
#include <ow/owmisc.h>
#include <ow/ssm.h>
#include <idb/wellfile.h>
#include <sdlprototypes.h>
#include <ow/gdi.h>


#include <ow/owmisc.h>

#include <string>
#include <vector>
#include <algorithm>

#define NO_OF_BUF_IDX 32767
#define REMARK_LEN    61
#define PROJ_NAME_LEN 17
#define LINE_NAME_LEN 31
#define FILE_NAME_LEN 50
#define PROVER_LEN    10
#define NO_OF_COLOR   3
#define NO_OF_DATA    32767
#define OWINIT_FAILED 1
#define ERROR_GET_LICENSE 2
const int MAX_OWDB_NAME = 20;
using namespace std;

const int OKAY = 0;
const int MAX_LINE_NAME = 30;

extern "C" {
    void inglct_(char*, int*, int);
    void strtrm_(char*, int);
    void setprj_(int*, char*, int);
    void locdef_(int*, char*, int*, int*, int*, int);
    void locnxt_(int*, char*, int);
    void fstbio_cache_init();
    char* fstbio_find_suf_all(char* suffix, int init);
    char* fstbio_find_pre_suf_all(char *suffix, char *prefix, int init);
}
int search_hrz(const char *horizon, int max_hrz, int i_prj_type);
void close_proj(int ipdflg);
void close_hrz_file(int ipdflg, int istat);
void close_hrz_file(int ipdflg);
void setMessage(JNIEnv *env, jobject jobj, string message, jobject lock);
int getLineIdByLineName(char *linename);
int exist(string s, vector<string> *v);
int getProjectNames(int type, vector<string> *v);
static int InitOW();
static void CloseOW();

JNIEXPORT jboolean JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_openProject
(JNIEnv *env, jobject jobj, jstring project, jint type, jobject lock) {
    char prjnam[PROJ_NAME_LEN];
    int ipdflg = type;
    int ier;
    jsize s = (jint) (env)->GetStringUTFLength(project);
    (env)->GetStringUTFRegion(project, 0, s, prjnam);
    prjnam[s] = '\0';

    prjin2_(&ier, &ipdflg, prjnam, s);

    if (ier != 0) {
        printf("prjin2 error %d/n", ier);
        string msg("");
        if (ier == 1) {
            msg += "Error: Project Definition file not found (.pds or .ps2)";
        } else if (ier == 2) {
            msg += "Error: Unable to log project on all drives";
        } else if (ier == 4) {
            msg += "Error: Could not obtain an OpenWorks license";
        } else {
            msg += "Unknown error in opening project ";
        }
        setMessage(env, jobj, msg, lock);
        close_proj(ipdflg);
        return false;
    }
    return true;
}

JNIEXPORT jobjectArray JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_get2DHorizonListByLines
(JNIEnv *env, jobject jobj, jstring projectName, jobjectArray lines) {
    int i, ier, ipdflg = 2;
    char prjnam[PROJ_NAME_LEN];
    char lname[LINE_NAME_LEN];
    char suffix[80];
    char prefix[31];

    jobjectArray ret;
    ret = (jobjectArray) (env)->NewObjectArray(0, env->FindClass("java/lang/String"), env->NewStringUTF(""));

    jsize lsize = env->GetArrayLength(lines);
    if (lsize == 0)
        return ret;
    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    // initial global cache - this can take some time
    fstbio_cache_init();
    prjin2_(&ier, &ipdflg, prjnam, s);

    if (ier != 0) {
        printf("prjin2 error %d/n", ier);
        close_proj(ipdflg);
        return ret;
    }

    vector<vector<string> > v;

    for (i = 0; i < lsize; i++) {
        /* obtain the current object from the object array */
        jobject lObject = env->GetObjectArrayElement(lines, i);
        /* Convert the object just obtained into a String */
        const char *lstr = env->GetStringUTFChars((jstring) lObject, 0);
        printf("lines[%d] = %s\n", i, lstr);

        int ival30 = 30;
        strcpy(lname, lstr);
        pumknm_(&ival30, lname, lname, &ier);
        //strcpy(suffix,hname);
        strcpy(suffix, ".hzd_glb");
        strcpy(prefix, lname);

        int init = 1;

        /* Free up memory to prevent memory leaks */
        char* fname;
        vector<string> files;
        while ((fname = fstbio_find_pre_suf_all(suffix, prefix, init)) != 0) {
            init = 0;

            char* ptr = strstr(fname, lname);
            if (ptr != 0) {
                string s(fname);
                s = s.substr(30, s.length());
                char *fname1 = s.c_str();
                char* tail = strstr(fname1, "__");
                if (tail != 0)
                    *tail = '\0';
                s = fname1;
                if (exist(s, &files) == 0)
                    files.push_back(s);
            }
            free(fname);
        }
        v.push_back(files);
        files.clear();
        env->ReleaseStringUTFChars((jstring) lObject, lstr);
    }


    vector<string> setIntersection;
    if (v.size() <= 1)
        setIntersection = v.at(0);
    else {
        set_intersection(v.at(0).begin(), v.at(0).end(), v.at(1).begin(), v.at(1).end(), back_inserter(setIntersection));
        if (v.size() > 2) {
            vector<string> setIntersection2 = setIntersection;
            for (i = 2; i < v.size(); i++) {
                setIntersection.clear();
                set_intersection(setIntersection2.begin(), setIntersection2.end(), v.at(i).begin(), v.at(i).end(), back_inserter(setIntersection));
                setIntersection2 = setIntersection;
            }
        }
    }
    ret = (jobjectArray) (env)->NewObjectArray(setIntersection.size(), env->FindClass("java/lang/String"), env->NewStringUTF(""));
    for (int i = 0; i < setIntersection.size(); i++) {
        jstring arrayElement = (jstring) env->NewStringUTF(setIntersection[i].c_str());
        env->SetObjectArrayElement(ret, i, arrayElement);
    }
    close_proj(ipdflg);
    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_get2DNonRelatedLinesByHorizonArray
(JNIEnv *env, jobject jobj, jstring projectName, jobjectArray lines, jobjectArray horizons) {
    int i, j, ier, ipdflg = 2;
    char prjnam[PROJ_NAME_LEN];
    char hname[REMARK_LEN];
    char lname[LINE_NAME_LEN];
    char suffix[80];
    char prefix[31];

    jobjectArray ret;
    ret = (jobjectArray) (env)->NewObjectArray(0, env->FindClass("java/lang/String"), env->NewStringUTF(""));
    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    // initial global cache - this can take some time
    fstbio_cache_init();
    prjin2_(&ier, &ipdflg, prjnam, s);

    if (ier != 0) {
        printf("prjin2 error %d/n", ier);
        close_proj(ipdflg);
        return ret;
    }

    vector<string> v;
    jsize hsize = env->GetArrayLength(horizons);
    jsize lsize = env->GetArrayLength(lines);
    for (i = 0; i < hsize; i++) {
        /* obtain the current object from the object array */
        jobject hObject = env->GetObjectArrayElement(horizons, i);
        const char *hstr = env->GetStringUTFChars((jstring) hObject, 0);
        for (j = 0; j < lsize; j++) {
            jobject lObject = env->GetObjectArrayElement(lines, j);
            /* Convert the object just obtained into a String */
            const char *lstr = env->GetStringUTFChars((jstring) lObject, 0);

            int ival60 = 60;
            int ival30 = 30;
            strcpy(hname, hstr);
            pumknm_(&ival60, hname, hname, &ier);
            strcpy(lname, lstr);
            pumknm_(&ival30, lname, lname, &ier);
            strcpy(suffix, hname);
            strcat(suffix, ".hzd_glb");
            strcpy(prefix, lname);

            int init = 1;

            /* Free up memory to prevent memory leaks */

            int found = 0;
            char* fname;
            fname = fstbio_find_pre_suf_all(suffix, prefix, init);
            if (fname == 0) {
                string s(lstr);
                if (exist(s, &v) == 0)
                    v.push_back(s);
            }
            free(fname);
            env->ReleaseStringUTFChars((jstring) lObject, lstr);
        }
        env->ReleaseStringUTFChars((jstring) hObject, hstr);
    }
    ret = (jobjectArray) (env)->NewObjectArray(v.size(), env->FindClass("java/lang/String"), env->NewStringUTF(""));
    for (int i = 0; i < v.size(); i++) {
        jstring arrayElement = (jstring) env->NewStringUTF(v[i].c_str());
        env->SetObjectArrayElement(ret, i, arrayElement);
    }
    close_proj(ipdflg);
    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_get2DRelatedLinesByHorizonName
(JNIEnv *env, jobject jobj, jstring projectName, jstring horizonName) {
    int i, j, ier, init = 1, ipdflg = 2;
    char prjnam[PROJ_NAME_LEN];
    char hname[REMARK_LEN];
    char lname[LINE_NAME_LEN];
    char suffix[80];
    char prefix[31];

    jobjectArray ret;
    ret = (jobjectArray) (env)->NewObjectArray(0, env->FindClass("java/lang/String"), env->NewStringUTF(""));
    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';
    printf("project name=%s\n", prjnam);
    s = (jint) (env)->GetStringUTFLength(horizonName);
    (env)->GetStringUTFRegion(horizonName, 0, s, hname);
    hname[s] = '\0';
    printf("horizon name=%s\n", hname);
    // initial global cache - this can take some time
    fstbio_cache_init();
    prjin2_(&ier, &ipdflg, prjnam, s);

    if (ier != 0) {
        printf("prjin2 error %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    char* fname;
    vector<string> files;
    int ival60 = 60;
    int ival30 = 30;

    pumknm_(&ival60, hname, hname, &ier);
    strcpy(suffix, hname);
    strcat(suffix, ".hzd_glb");
    while ((fname = fstbio_find_suf_all(suffix, init)) != 0) {
        init = 0;
        char* ptr = strstr(fname, hname);

        if (ptr != 0) {
            string s(fname);
            s = s.substr(0, ival30);

            char *fname1 = s.c_str();
            char* tail = strstr(fname1, "__");
            if (tail != 0)
                *tail = '\0';
            s = fname1;

            if (exist(s, &files) == 0)
                files.push_back(s);
        }
        free(fname);
    }
    printf("Number of qualified lines=%d\n", files.size());
    ret = (jobjectArray) (env)->NewObjectArray(files.size(), env->FindClass("java/lang/String"), env->NewStringUTF(""));
    for (int i = 0; i < files.size(); i++) {
        jstring arrayElement = (jstring) env->NewStringUTF(files[i].c_str());
        env->SetObjectArrayElement(ret, i, arrayElement);
    }
    close_proj(ipdflg);
    return ret;
}

JNIEXPORT void JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_getProjectHorizonInfo
(JNIEnv *env, jobject jobj, jstring projectName, jint type, jobjectArray horizons, jint minline, jint maxline, jint mintrace, jint maxtrace, jobject lock) {
    int i, ier, ipdflg = type;
    jfloatArray args = env->NewFloatArray(3);
    float *data;
    //char   remark[REMARK_LEN];
    char lname[LINE_NAME_LEN];
    char prjnam[PROJ_NAME_LEN];
    int mininx, mininy, maxinx, maxiny, ixmin, ixmax, iymin, iymax, ix, iy;
    int istat, maxhrz, index;
    int ihrz, iacthz, ix1, ix2, iy1, iy2;
    int iop, line, iskip, ival1 = 1;
    float rnul, rline, rtrace, dummy, spn, rdumx, rdumy;
    double rx, ry;
    jfieldID fid; /* field ID required to update field in 'this' */
    jclass cls;
    jmethodID mid;

    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    prjin2_(&ier, &ipdflg, prjnam, s);

    if (ier != 0) {
        printf("prjin2 error %d\n", ier);
        close_proj(ipdflg);
        return;
    }

    /*  report on project extents  */
    if (ipdflg == 3) {
        srvbnd_(&ipdflg, &mininx, &mininy, &maxinx, &maxiny, &ier);
        if (ier) {
            printf(" srvbnd error: %d\n", ier);
            close_proj(ipdflg);
            ;
        }

        /*  open the horizon file  */
        rnul = 1.0e+37;
        hrzopn_(&istat, prjnam, &ixmin, &ixmax, &iymin, &iymax, &rnul, &maxhrz);
        if (istat == 0) {
            printf(" horizon file opened \n");
            printf(" current number of horizons: %d\n", maxhrz);
        } else if (istat == 2) {
            printf(" horizon file was created. I am going to quit. \n");
            close_hrz_file(ipdflg);

        } else
            printf(" hrzopn error: %d\n", istat);

        /*  check that survey bounds match horizon file bounds  */
        if ((maxinx - mininx != ixmax - ixmin) || (maxiny - mininy != iymax - iymin)) {
            printf(" survey mismatch with horizon file \n");
            close_hrz_file(ipdflg);

        }
        float fminline = minline, fmaxline = maxline, fmintrace = mintrace, fmaxtrace = maxtrace;
        cout << "before " << endl;
        cout << "fminline = " << fminline << " fmintrace = " << fmintrace << endl;
        cout << "ix1 = " << ix1 << " iy1 = " << iy1 << endl;
        cs2int_(&ipdflg, &fmaxline, &fmaxtrace, &ix2, &iy2);
        cout << "ix2 = " << ix2 << " iy2 = " << iy2 << endl;
        cout << "fmaxline = " << fmaxline << " fmaxtrace = " << fmaxtrace << endl;
        cout << "after " << endl;
        ;
        cs2int_(&ipdflg, &fminline, &fmintrace, &ix1, &iy1);
        cout << "fminline = " << fminline << " fmintrace = " << fmintrace << endl;
        cout << "ix1 = " << ix1 << " iy1 = " << iy1 << endl;
        cs2int_(&ipdflg, &fmaxline, &fmaxtrace, &ix2, &iy2);
        cout << "ix2 = " << ix2 << " iy2 = " << iy2 << endl;
        cout << "fmaxline = " << fmaxline << " fmaxtrace = " << fmaxtrace << endl;
        if (!((ix1 >= ixmin && ix1 <= ixmax) || (ix2 >= ixmin && ix2 <= ixmax) || (iy1 >= iymin && iy1 <= iymax) || (iy2 >= iymin && iy2 <= iymax))) {
            cout << "out of range." << endl;
            close_hrz_file(ipdflg);
            close_proj(ipdflg);
        }
    } else {
        /*
         **  pjhzsz is to get the number of all/active-only
         **  horizon from 2d <project>.hrz_cat.
         **  1st argument is total horizon in the project
         **  2nd argument is active horizon
         */
        pjhzsz_(&maxhrz, &iacthz, &ier);
        if (ier) {
            printf("error in opening horizon catalog.\n");
            close_hrz_file(ipdflg);
        }
    }
    cls = env->GetObjectClass(jobj);
    jsize argc = env->GetArrayLength(horizons);
    for (i = 0; i < argc; i++) {
        /* obtain the current object from the object array */
        jobject myObject = env->GetObjectArrayElement(horizons, i);

        /* Convert the object just obtained into a String */
        const char *str = env->GetStringUTFChars((jstring) myObject, 0);

        /* Build the argv array */
        string horizon(str);

        /* Free up memory to prevent memory leaks */
        env->ReleaseStringUTFChars((jstring) myObject, str);

        /* print the argv array to the screen */
        printf("horizons[%i] = %s\n", i, horizon.c_str());

        int hrz_id = search_hrz(horizon.c_str(), maxhrz, ipdflg);
        cout << "hrz_id = " << hrz_id << endl;



        jstring stringObject;
        if (!(stringObject = env->NewStringUTF(horizon.c_str()))) {
            /*  For nearly every function in the JNI, a null return value indicates
            that there was an error, and that an exception had been placed where it
            could be retrieved by 'ExceptionOccurred()'. In this case, the error
            would typically be fatal, but for purposes of this example, go ahead
            and catch the error, and continue.  */
            cout << "A null returned  env->NewStringUTF " << endl;
            env->ExceptionDescribe(); /* write exception data to the console */
            env->ExceptionClear(); /* clear the exception that was pending */
            return;
        }

        mid = env->GetMethodID(cls, "setCurrentHorizon", "(Ljava/lang/String;)V");

        if (mid == 0) {
            cout << "Can't find method printLineTraceTime";
            return;
        }

        env->ExceptionClear();
        env->MonitorEnter(lock);
        env->CallVoidMethod(jobj, mid, stringObject);
        env->MonitorExit(lock);
        if (env->ExceptionOccurred()) {
            cout << "error occured in callback setCurrentHorizon." << endl;
            env->ExceptionDescribe();
            env->ExceptionClear();
            close_hrz_file(ipdflg);
            close_proj(ipdflg);
            return;
        }

        /*  read horizon data  */
        if (ipdflg == 3)
            data = new float[(ix2 - ix1 + 1)*(iy2 - iy1 + 1)];
            //data = new float[ixmax*iymax];
        else
            data = new float[32767];

        if (ipdflg == 3) {
            iop = 1;
            iskip = iy2 - iy1 + 1;
            hrzrw_(&hrz_id, &iop, &istat, &ix1, &ix2, &iy1, &iy2, data, &iskip);
            if (istat) {
                printf(" hrzrw error: %d\n", istat);
                close_proj(ipdflg);
            }
        }

        if (hrz_id != -1) {
            index = 0;
            for (ix = ix1; ix <= ix2; ix++) {
                /*  read one line at a time  */
                if (ipdflg == 2) {
                    index = 0;
                    iop = 1;
                    hrzrw_(&ihrz, &iop, &istat, &ix, &ix, &iy1, &iy2, data, &iskip);
                    if (istat) {
                        printf(" hrzrw error: %d\n", istat);
                        close_proj(ipdflg);
                    }
                }
                for (iy = iy1; iy <= iy2; iy++)
                    /*
                     **  report horizon value being output in line, trace coordinates and
                     **  in real world coordinates
                     */ {
                    cs2ano_(&ipdflg, &ix, &iy, &rline, &rtrace);
                    if (ipdflg == 3) {
                        rdumx = ix;
                        rdumy = iy;
                        intorw_(&ival1, &rdumx, &rdumy, &rx, &ry, &dummy);

                        env->SetFloatArrayRegion(args, 0, 1, &rline);
                        env->SetFloatArrayRegion(args, 1, 1, &rtrace);
                        env->SetFloatArrayRegion(args, 2, 1, &data[index]);

                        mid = env->GetMethodID(cls, "printLineTraceTime", "([F)V");

                        if (mid == 0) {
                            cout << "Can't find method printLineTraceTime";
                            return;
                        }

                        env->ExceptionClear();
                        env->MonitorEnter(lock);
                        env->CallVoidMethod(jobj, mid, args);

                        env->MonitorExit(lock);
                        if (env->ExceptionOccurred()) {
                            cout << "error occured in callback printLineTraceTime." << endl;
                            env->ExceptionDescribe();
                            env->ExceptionClear();
                            close_hrz_file(ipdflg);
                            close_proj(ipdflg);
                            return;
                        }
                    } else {
                        line = (int) rline;
                        if (iy == iymin) {
                            lnolnm_(&line, lname, &ier);
                            if (ier == 1) {
                                printf("line %d not defined\n", line);
                                continue;
                                //goto next_line;
                            } else if (ier) {
                                printf("lnolnm ier: %d\n", ier);
                                close_hrz_file(ipdflg);
                            }
                            printf(" line - %s\n", lname);
                        }
                        trtosp_(&line, &spn, &rtrace, &ier);
                        if (ier) {
                            printf("trtosp ier: %d\n", ier);
                            close_hrz_file(ipdflg);
                        }
                        sptoxy_(&line, &spn, &rx, &ry, &ier);
                        if (ier) {
                            printf("sptoxy ier: %d\n", ier);
                            close_hrz_file(ipdflg);
                            close_proj(ipdflg);
                            return;
                        }
                    }
                    index++;
                }
            }
        }
    }
    mid = env->GetMethodID(cls, "done", "()V");

    if (mid == 0) {
        cout << "Can't find method done";
        return;
    }

    env->ExceptionClear();
    env->MonitorEnter(lock);
    env->CallVoidMethod(jobj, mid, args);

    env->MonitorExit(lock);
    if (env->ExceptionOccurred()) {
        cout << "error occured in callback function: done()" << endl;
        env->ExceptionDescribe();
        env->ExceptionClear();
        close_hrz_file(ipdflg);
        close_proj(ipdflg);
        return;
    }

    return;
}

JNIEXPORT jboolean JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_getProjectHorizonInfo1
(JNIEnv *env, jobject jobj, jstring projectName, jint type, jstring horizon, jint minline, jint maxline, jint mintrace, jint maxtrace, jobject lock) {
    int ier, ipdflg = type;
    jfloatArray args = env->NewFloatArray(3);
    float *data;
    char remark[REMARK_LEN];
    char lname[LINE_NAME_LEN];
    char prjnam[PROJ_NAME_LEN];
    int mininx, mininy, maxinx, maxiny, ixmin, ixmax, iymin, iymax, ix, iy;
    int istat, itype, maxhrz, index;
    int ihrz, iacthz, ix1, ix2, iy1, iy2;
    int iop, line, iskip, ival1 = 1;
    float rnul, rline, rtrace, dummy, spn, rdumx, rdumy;
    double rx, ry;
    jclass cls = env->GetObjectClass(jobj);
    jmethodID mid;

    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    s = (jint) (env)->GetStringUTFLength(horizon);
    (env)->GetStringUTFRegion(horizon, 0, s, remark);
    remark[s] = '\0';

    prjin2_(&ier, &ipdflg, prjnam, s);

    if (ier != 0) {
        printf("prjin2 error %d\n", ier);
        close_proj(ipdflg);
        return false;
    }

    /*  report on project extents  */
    if (ipdflg == 3) {
        srvbnd_(&ipdflg, &mininx, &mininy, &maxinx, &maxiny, &ier);
        if (ier) {
            printf(" srvbnd error: %d\n", ier);
            close_proj(ipdflg);
            ;
            return false;
        }

        /*  open the horizon file  */
        rnul = 1.0e+37;
        hrzopn_(&istat, prjnam, &ixmin, &ixmax, &iymin, &iymax, &rnul, &maxhrz);
        if (istat == 0) {
            printf(" horizon file opened \n");
            printf(" current number of horizons: %d\n", maxhrz);
        } else if (istat == 2) {
            printf(" horizon file was created. I am going to quit. \n");
            close_hrz_file(ipdflg);
            //close_proj(ipdflg);
            return false;
        } else {
            printf(" hrzopn error: %d\n", istat);
            close_hrz_file(ipdflg);
            //close_proj(ipdflg);
            return false;
        }

        /*  check that survey bounds match horizon file bounds  */
        if ((maxinx - mininx != ixmax - ixmin) || (maxiny - mininy != iymax - iymin)) {
            printf(" survey mismatch with horizon file \n");
            close_hrz_file(ipdflg);
            //close_proj(ipdflg);
            return false;

        }
        float fminline = minline, fmaxline = maxline, fmintrace = mintrace, fmaxtrace = maxtrace;
        cout << "before " << endl;
        cout << "fminline = " << fminline << " fmintrace = " << fmintrace << endl;
        cout << "ix1 = " << ix1 << " iy1 = " << iy1 << endl;
        cs2int_(&ipdflg, &fmaxline, &fmaxtrace, &ix2, &iy2);
        cout << "ix2 = " << ix2 << " iy2 = " << iy2 << endl;
        cout << "fmaxline = " << fmaxline << " fmaxtrace = " << fmaxtrace << endl;
        cout << "after " << endl;
        ;
        cs2int_(&ipdflg, &fminline, &fmintrace, &ix1, &iy1);
        cout << "fminline = " << fminline << " fmintrace = " << fmintrace << endl;
        cout << "ix1 = " << ix1 << " iy1 = " << iy1 << endl;
        cs2int_(&ipdflg, &fmaxline, &fmaxtrace, &ix2, &iy2);
        cout << "ix2 = " << ix2 << " iy2 = " << iy2 << endl;
        cout << "fmaxline = " << fmaxline << " fmaxtrace = " << fmaxtrace << endl;
        if (!((ix1 >= ixmin && ix1 <= ixmax) || (ix2 >= ixmin && ix2 <= ixmax) || (iy1 >= iymin && iy1 <= iymax) || (iy2 >= iymin && iy2 <= iymax))) {
            cout << "Out of range in Lines or Traces fields." << endl;
            close_hrz_file(ipdflg);
            string msg("Out of range in Lines or Traces fields.");
            msg = "Can not find horizon " + msg;
            setMessage(env, jobj, msg, lock);
            //close_proj(ipdflg);
            return false;
        }
    } else {
        /*
         **  pjhzsz is to get the number of all/active-only
         **  horizon from 2d <project>.hrz_cat.
         **  1st argument is total horizon in the project
         **  2nd argument is active horizon
         */
        pjhzsz_(&maxhrz, &iacthz, &ier);
        if (ier) {
            printf("error in opening horizon catalog.\n");
            close_hrz_file(ipdflg);
            //close_proj(ipdflg);
            return false;
        }
    }

    int hrz_id = search_hrz(remark, maxhrz, ipdflg);
    cout << "hrz_id = " << hrz_id << endl;
    if (hrz_id == -1) {

        string msg(remark);
        msg = "Can not find horizon " + msg;
        setMessage(env, jobj, msg, lock);
        close_hrz_file(ipdflg);
        return false;
    }


    /*  read horizon data  */
    if (ipdflg == 3)
        data = new float[(ix2 - ix1 + 1)*(iy2 - iy1 + 1)];
        //data = new float[ixmax*iymax];
    else
        data = new float[32767];

    if (ipdflg == 3) {
        iop = 1;
        iskip = iy2 - iy1 + 1;
        hrzrw_(&hrz_id, &iop, &istat, &ix1, &ix2, &iy1, &iy2, data, &iskip);
        if (istat) {

            printf(" hrzrw error: %d\n", istat);
            char status[3];
            string msg(" hrzrw error: ");
            sprintf(status, "%d", istat);
            msg = msg + status;
            setMessage(env, jobj, msg, lock);
            close_hrz_file(ipdflg);
            //close_proj(ipdflg);
            return false;
        }
    }

    if (hrz_id != -1) {
        index = 0;
        for (ix = ix1; ix <= ix2; ix++) {
            /*  read one line at a time  */
            if (ipdflg == 2) {
                index = 0;
                iop = 1;
                hrzrw_(&ihrz, &iop, &istat, &ix, &ix, &iy1, &iy2, data, &iskip);
                if (istat) {
                    char status[3];
                    printf(" hrzrw error: %d\n", istat);
                    sprintf(status, "%d", istat);
                    close_hrz_file(ipdflg);
                    string msg(" hrzrw error: ");
                    msg = msg + status;
                    setMessage(env, jobj, msg, lock);
                    //close_proj(ipdflg);
                    return false;
                }
            }
            for (iy = iy1; iy <= iy2; iy++)
                /*
                 **  report horizon value being output in line, trace coordinates and
                 **  in real world coordinates
                 */ {
                cs2ano_(&ipdflg, &ix, &iy, &rline, &rtrace);
                if (ipdflg == 3) {
                    rdumx = ix;
                    rdumy = iy;
                    intorw_(&ival1, &rdumx, &rdumy, &rx, &ry, &dummy);

                    env->SetFloatArrayRegion(args, 0, 1, &rline);
                    env->SetFloatArrayRegion(args, 1, 1, &rtrace);
                    env->SetFloatArrayRegion(args, 2, 1, &data[index]);

                    mid = env->GetMethodID(cls, "printLineTraceTime", "([F)V");

                    if (mid == 0) {
                        cout << "Can't find method printLineTraceTime";
                        string msg("Can't find method printLineTraceTime.");
                        setMessage(env, jobj, msg, lock);
                        close_hrz_file(ipdflg);
                        return false;
                    }

                    env->ExceptionClear();
                    env->MonitorEnter(lock);
                    env->CallVoidMethod(jobj, mid, args);

                    env->MonitorExit(lock);
                    if (env->ExceptionOccurred()) {
                        cout << "error occured in callback printLineTraceTime." << endl;
                        string msg("error occured in callback function printLineTraceTime.");
                        setMessage(env, jobj, msg, lock);
                        env->ExceptionDescribe();
                        env->ExceptionClear();
                        close_hrz_file(ipdflg);
                        //close_proj(ipdflg);
                        return false;

                    }
                } else {
                    line = (int) rline;
                    if (iy == iymin) {
                        lnolnm_(&line, lname, &ier);
                        if (ier == 1) {
                            printf("line %d not defined\n", line);
                            continue;
                            //goto next_line;
                        } else if (ier) {
                            printf("lnolnm ier: %d\n", ier);
                            close_hrz_file(ipdflg);
                            return false;
                        }
                        printf(" line - %s\n", lname);
                    }
                    trtosp_(&line, &spn, &rtrace, &ier);
                    if (ier) {
                        printf("trtosp ier: %d\n", ier);
                        close_hrz_file(ipdflg);
                        //close_proj(ipdflg);
                        return false;
                    }
                    sptoxy_(&line, &spn, &rx, &ry, &ier);
                    if (ier) {
                        printf("sptoxy ier: %d\n", ier);
                        close_hrz_file(ipdflg);
                        //close_proj(ipdflg);
                        return false;

                    }

                }
                index++;
            }
        }
    }
    close_hrz_file(ipdflg);
    mid = env->GetMethodID(cls, "done", "()V");

    if (mid == 0) {
        cout << "Can't find callback method done()";
        //message = env->NewStringUTF("Can't find callback method done()");
        string msg("Can't find callback function done().");
        setMessage(env, jobj, msg, lock);
        close_proj(ipdflg);
        return false;
    }

    env->ExceptionClear();
    env->MonitorEnter(lock);
    env->CallVoidMethod(jobj, mid, args);

    env->MonitorExit(lock);
    if (env->ExceptionOccurred()) {
        //message = env->NewStringUTF("error in callback function done()");
        cout << "error in callback function done()" << endl;
        string msg("error in callback function done().");
        setMessage(env, jobj, msg, lock);
        env->ExceptionDescribe();
        env->ExceptionClear();
        close_proj(ipdflg);
        return false;
    }


    //close_proj(ipdflg);
    return true;
}

JNIEXPORT void JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_closeProject
(JNIEnv *env, jobject jobj, jint type) {
    close_proj(type);
    return;
}

JNIEXPORT jobjectArray JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_getProjects
(JNIEnv *env, jobject, jint type) {
    char **lst1;
    int cnt1;
    int i, ier;

    jobjectArray ret;

    vector<string> v;
    int retcode = getProjectNames(type, &v);
    if (retcode == 0) {
        cnt1 = v.size();
        ret = (jobjectArray) (env)->NewObjectArray(cnt1, env->FindClass("java/lang/String"), env->NewStringUTF(""));
        for (i = 0; i < v.size(); i++) {
            jstring arrayElement = (jstring) env->NewStringUTF(v.at(i).c_str());
            env->SetObjectArrayElement(ret, i, arrayElement);
        }
    }
    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_getSeismicLineList
(JNIEnv *env, jobject jobj, jstring projectName, jint type) {
    char prjnam[PROJ_NAME_LEN];
    char name[MAX_LINE_NAME + 1];
    jobjectArray ret;
    int ipdflg = type, ier;
    vector<string> lines;
    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    (jobjectArray) (env)->NewObjectArray(0, env->FindClass("java/lang/String"), env->NewStringUTF(""));

    prjin2_(&ier, &ipdflg, prjnam, s);
    if (ier != 0) {
        printf("prjin2 error %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    // ------ Get the number of lines in project (needed to allocate memory).
    int totalLines = 0;
    int activeLines = 0;
    int status, id;
    int lastLine, index;
    pjlnsz_(&totalLines, &activeLines, &status);
    if (status == OKAY) {
        //int mininx = 1;
        int maxinx = activeLines;
        // loop through each line
        // ------ Loop to get line number and name for each line.  Print
        // ------ each, adding min and max trace from above.
        lastLine = 0;
        index = 0;
        while (!lastLine && index < maxinx) {
            int first = !index;
            retlin_(&first, &id, name, &lastLine, &status, MAX_LINE_NAME);
            if (status == OKAY) {
                name[MAX_LINE_NAME] = '\0';
                string s(name);
                lines.push_back(s);
            } else {
                cout << "FAILURE: retlin_: Bad Project Definition File or "
                        << "Line Catalog is empty." << endl;
                lastLine = 1;
            }
            index++;
        }
        ret = (jobjectArray) (env)->NewObjectArray(lines.size(), env->FindClass("java/lang/String"), env->NewStringUTF(""));
        int i;
        for (i = 0; i < lines.size(); i++) {
            cout << lines[i] << endl;
            jstring arrayElement = (jstring) env->NewStringUTF(lines[i].c_str());
            env->SetObjectArrayElement(ret, i, arrayElement);
        }
        /* reset to seismic project */
    }

    close_proj(ipdflg);
    return ret;
}

//this function is not currently used and subject to modification

JNIEXPORT jobjectArray JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_getAllActive2v2Files
(JNIEnv *env, jobject, jstring projectName) {
    char prjnam[PROJ_NAME_LEN];
    char lname[LINE_NAME_LEN];
    char extlin[FILE_NAME_LEN];
    char filnam[FILE_NAME_LEN];
    char glbnam[FILE_NAME_LEN];


    int ier, idum, maxlin;
    int ipdflg = 2;
    int L256 = 256;
    jobjectArray ret;
    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    ret = (jobjectArray) (env)->NewObjectArray(0, env->FindClass("java/lang/String"), env->NewStringUTF(""));
    cout << "prjnam = " << prjnam << endl;
    prjin2_(&ier, &ipdflg, prjnam, s);
    if (ier != 0) {
        printf("prjin2 error %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }
    inglct_(glbnam, &ier, L256);
    if (ier != 0) {
        printf("error from inglct ier = %d\n", ier);
        exit(1);
    }
    strtrm_(glbnam, L256);
    cout << "glbnam= " << glbnam << endl;
    /* set the global area for listing */
    setprj_(&ier, glbnam, L256);
    cout << "glbnam2 = " << glbnam << endl;
    pjlnsz_(&idum, &maxlin, &ier);
    //	int mininx = 1;
    //	int maxinx = maxlin;
    int imode = 1, i_1 = 1;
    int totfil;


    vector<string> allfiles;
    //for (int ix=mininx; ix<=maxinx; ix++){
    for (int ix = 840; ix <= 840; ix++) {
        lnolnm_(&ix, lname, &ier);
        lname[LINE_NAME_LEN - 1] = '\0';
        strcpy(extlin, "*01.2v2_glb");
        locdef_(&ier, extlin, &imode, &totfil, &i_1, L256);
        cout << "extlin = " << extlin << endl;
        printf("# of files matching %d\n", totfil);

        for (int i = 0; i < totfil; i++) {
            /* get files that match file card one by one */
            locnxt_(&ier, filnam, L256);
            if (ier != 0) {
                printf("error from locnxt ier = %d\n", ier);
                close_proj(ipdflg);
                return ret;
            }
            printf("%s\n", filnam);
            string s(filnam);
            if (!exist(s, &allfiles))
                allfiles.push_back(s);
        }
    }
    /* reset to seismic project */

    setprj_(&ier, prjnam, L256);
    ret = (jobjectArray) (env)->NewObjectArray(allfiles.size(), env->FindClass("java/lang/String"), env->NewStringUTF(""));
    for (int i = 0; i < allfiles.size(); i++) {
        jstring arrayElement = (jstring) env->NewStringUTF(allfiles[i].c_str());
        env->SetObjectArrayElement(ret, i, arrayElement);
    }
    close_proj(ipdflg);
    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_getAll2v2Files
(JNIEnv *env, jobject, jstring projectName) {

    char prjnam[PROJ_NAME_LEN];
    char suffix[16];
    char* fname;
    int ier, init = 1;
    int ipdflg = 2;
    jobjectArray ret;
    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    ret = (jobjectArray) (env)->NewObjectArray(0, env->FindClass("java/lang/String"), env->NewStringUTF(""));

    int initRet = InitOW();
    if (initRet == OWINIT_FAILED) {
        jstring arrayElement = (jstring) env->NewStringUTF("Error in initializing OpenWorks");
        env->SetObjectArrayElement(ret, 0, arrayElement);
        return ret;
    } else if (initRet == ERROR_GET_LICENSE) {
        jstring arrayElement = (jstring) env->NewStringUTF("Error in Getting OpenWorks License");
        env->SetObjectArrayElement(ret, 0, arrayElement);
        return ret;
    }

    printf("project name passed in %s\n", prjnam);
    if (s == 0) {
        printf("empty project name passed in\n");
        return ret;
    } else {
        vector<string> v;
        int retcode = getProjectNames(ipdflg, &v);
        if (retcode != 0) {
            return ret;
        }
        string sobj(prjnam);
        printf("check to see if project %s exists.\n", sobj.c_str());
        if (exist(sobj, &v) == 0) {
            printf("Can not find project %s\n", sobj.c_str());
            return ret;
        }
    }


    // initial global cache - this can take some time

    fstbio_cache_init();

    prjin2_(&ier, &ipdflg, prjnam, s);

    if (ier != 0) {
        printf("prjin2 error %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    strcpy(suffix, "01.2v2_glb");
    vector<string> allfiles;
    while ((fname = fstbio_find_suf_all(suffix, init)) != 0) {
        init = 0;
        string s(fname);
        allfiles.push_back(s);
        free(fname);
    }

    ret = (jobjectArray) (env)->NewObjectArray(allfiles.size(), env->FindClass("java/lang/String"), env->NewStringUTF(""));
    for (int i = 0; i < allfiles.size(); i++) {
        jstring arrayElement = (jstring) env->NewStringUTF(allfiles[i].c_str());
        env->SetObjectArrayElement(ret, i, arrayElement);
    }
    close_proj(ipdflg);
    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_getDatasetList
(JNIEnv *env, jobject, jstring projectName, jint projectType, jstring extension) {
    char extlin[256];
    char filnam[256];
    char glbnam[256];
    char prjnam[PROJ_NAME_LEN];
    int i, ier, totfil, i_1 = 1, imode, L256 = 256, ipdflg = projectType;
    jobjectArray ret;

    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    ret = (jobjectArray) (env)->NewObjectArray(0, env->FindClass("java/lang/String"), env->NewStringUTF(""));
    prjin2_(&ier, &ipdflg, prjnam, s);

    if (ier != 0) {
        printf("prjin2 error %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    s = (jint) (env)->GetStringUTFLength(extension);
    (env)->GetStringUTFRegion(extension, 0, s, extlin);
    extlin[s] = '\0';

    if (ipdflg == 2) {
        /* get the global area for the project */

        inglct_(glbnam, &ier, L256);
        if (ier != 0) {
            printf("error from inglct ier = %d\n", ier);
            close_proj(ipdflg);
            return ret;
        }
        strtrm_(glbnam, L256);
        /* set the global area for listing */
        setprj_(&ier, glbnam, L256);
    }

    locdef_(&ier, extlin, &imode, &totfil, &i_1, L256);
    if (ier != 0) {
        printf("error from locdef ier = %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    vector<string> SS;
    for (i = 0; i < totfil; i++) {
        /* get files that match file card one by one */
        locnxt_(&ier, filnam, L256);
        if (ier != 0) {
            printf("error from locnxt ier = %d\n", ier);
        }
        string temp(filnam);
        string ext(extlin);
        ext = ext.substr(1, ext.length());
        int ind1 = temp.find(ext, 0);
        temp = temp.substr(0, ind1 - 1);
        int ind2 = temp.find("0000", ind1 - 5);
        if (ind2 == -1) {
            SS.push_back(filnam);
        }
    }

    sort(SS.begin(), SS.end());
    ret = (jobjectArray) (env)->NewObjectArray(SS.size(), env->FindClass("java/lang/String"), env->NewStringUTF(""));

    for (i = 0; i < SS.size(); i++) {
        cout << SS[i] << endl;
        jstring arrayElement = (jstring) env->NewStringUTF(SS[i].c_str());
        env->SetObjectArrayElement(ret, i, arrayElement);
    }
    /* reset to seismic project */

    setprj_(&ier, prjnam, L256);

    close_proj(ipdflg);
    return ret;
}

JNIEXPORT jfloatArray JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_getShotpointRangeInfoBy2DLine
(JNIEnv *env, jobject, jstring projectName, jstring lineName) {
    char lname[LINE_NAME_LEN];

    char prjnam[PROJ_NAME_LEN];

    int ier, ipdflg = 2;
    int mininy, maxiny;


    jfloatArray ret;
    float array[4];

    ret = (jfloatArray) env->NewFloatArray(4);

    if (ipdflg != 2) {
        cout << "project type error! only accept 2D" << endl;
        return ret;
    }
    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    prjin2_(&ier, &ipdflg, prjnam, s);

    if (ier != 0) {
        printf("prjin2 error %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    s = (jint) (env)->GetStringUTFLength(lineName);
    (env)->GetStringUTFRegion(lineName, 0, s, lname);
    lname[s] = '\0';

    int lid;
    int ival30 = 30;
    cout << "lname1 = " << lname << endl;
    pumknm_(&ival30, lname, lname, &ier);
    cout << "lname2 = " << lname << endl;

    lnotst_(&lid, lname, &ier);
    if (ier != 0) {
        printf("No line found with given name %s\n", lname);
        close_proj(ipdflg);
        return ret;
    }
    cout << "lid = " << lid << endl;


    lny1y2_(&lid, &mininy, &maxiny, &ier);
    cout << "lny1y2_: ier = " << ier << endl;
    cout << "minTrace: mininy = " << mininy << endl;
    cout << "maxTrace: maxiny = " << maxiny << endl;

    float rline, rtrace;
    cs2ano_(&ipdflg, &lid, &mininy, &rline, &rtrace);
    cout << "minrline = " << rline << endl;
    cout << "minrtrace = " << rtrace << endl;
    int line = rline;
    float minspn;
    char string[50];
    trtosp_(&line, &minspn, &rtrace, &ier);
    if (ier) {
        printf(" trtosp ier: %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }
    cout << "min shotpoint=" << minspn << endl;
    array[0] = lid;
    sprintf(string, "%.2f", minspn);
    array[1] = atof(string);
    int y = mininy + 1;
    cs2ano_(&ipdflg, &lid, &y, &rline, &rtrace);
    cout << "next rline = " << rline << endl;
    cout << "next rtrace = " << rtrace << endl;
    line = rline;
    float spn;
    trtosp_(&line, &spn, &rtrace, &ier);
    if (ier) {
        printf(" trtosp ier: %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }
    cout << "next shotpoint=" << spn << endl;
    float increment = spn - minspn;
    sprintf(string, "%.2f", increment);
    array[3] = atof(string);
    cout << "increment=" << increment << endl;
    cs2ano_(&ipdflg, &lid, &maxiny, &rline, &rtrace);
    cout << "maxrline = " << rline << endl;
    cout << "maxrtrace = " << rtrace << endl;
    line = rline;
    float maxspn;
    trtosp_(&line, &maxspn, &rtrace, &ier);
    if (ier) {
        printf(" trtosp ier: %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }
    sprintf(string, "%.2f", maxspn);
    array[2] = atof(string);
    cout << "max shotpoint=" << maxspn << endl;
    env->SetFloatArrayRegion(ret, (jsize) 0, 4, (jfloat *) array);

    close_proj(ipdflg);
    return ret;
}

JNIEXPORT jint JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_get2DLineNumberByName
(JNIEnv *env, jobject, jstring projectName, jstring lineName) {
    char lname[LINE_NAME_LEN];
    char prjnam[PROJ_NAME_LEN];
    int ier, ipdflg = 2;

    if (ipdflg != 2) {
        cout << "project type error! only accept 2D" << endl;
        return -1;
    }
    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    prjin2_(&ier, &ipdflg, prjnam, s);

    if (ier != 0) {
        printf("prjin2 error %d\n", ier);
        close_proj(ipdflg);
        return -1;
    }

    s = (jint) (env)->GetStringUTFLength(lineName);
    (env)->GetStringUTFRegion(lineName, 0, s, lname);
    lname[s] = '\0';


    int lid = 0;
    int ival30 = 30;
    cout << "lname1 = " << lname << endl;
    pumknm_(&ival30, lname, lname, &ier);
    cout << "lname2 = " << lname << endl;
    lnotst_(&lid, lname, &ier);
    cout << "lid = " << lid << endl;
    if (ier != 0) {
        printf("No line found with given name %s\n", lname);
        close_proj(ipdflg);
        return -1;
    }
    close_proj(ipdflg);
    return lid;
}

JNIEXPORT jfloatArray JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_convertShotpointsTo2DTraces
(JNIEnv *env, jobject, jstring projectName, jstring lineName, jfloat minShotpoint, jfloat maxShotpoint, jfloat incShotpoint) {
    char lname[LINE_NAME_LEN];
    char prjnam[PROJ_NAME_LEN];

    int ier, ipdflg = 2;
    int mininy, maxiny;

    jfloatArray ret;
    float array[4];

    ret = (jfloatArray) env->NewFloatArray(3);

    if (ipdflg != 2) {
        cout << "project type error! only accept 2D" << endl;
        return ret;
    }
    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    prjin2_(&ier, &ipdflg, prjnam, s);

    if (ier != 0) {
        printf("prjin2 error %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    s = (jint) (env)->GetStringUTFLength(lineName);
    (env)->GetStringUTFRegion(lineName, 0, s, lname);
    lname[s] = '\0';


    int lid;
    int ival30 = 30;
    cout << "lname1 = " << lname << endl;
    pumknm_(&ival30, lname, lname, &ier);
    cout << "lname2 = " << lname << endl;
    lnotst_(&lid, lname, &ier);
    cout << "lid = " << lid << endl;
    if (ier != 0) {
        printf("No line found with given name %s\n", lname);
        close_proj(ipdflg);
        return ret;
    }
    float minTrace;
    sptotr_(&lid, &minShotpoint, &minTrace, &ier);
    if (ier) {
        cout << "Error: Memory allocation error or shotpoint/trace array is empty. status = " << ier << endl;
        close_proj(ipdflg);
        return ret;
    }
    array[0] = minTrace;

    float maxTrace;
    sptotr_(&lid, &maxShotpoint, &maxTrace, &ier);
    if (ier) {
        cout << "Error: Memory allocation error or shotpoint/trace array is empty. status = " << ier << endl;
        close_proj(ipdflg);
        return ret;
    }
    array[1] = maxTrace;
    lny1y2_(&lid, &mininy, &maxiny, &ier);
    cout << "minTrace: mininy = " << mininy << endl;
    cout << "maxTrace: maxiny = " << maxiny << endl;

    float rline, rtrace;
    cs2ano_(&ipdflg, &lid, &mininy, &rline, &rtrace);
    cout << "minrline = " << rline << endl;
    cout << "minrtrace = " << rtrace << endl;
    int line = rline;
    float minspn;
    char string[50];
    trtosp_(&line, &minspn, &rtrace, &ier);
    if (ier) {
        printf(" trtosp ier: %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }
    cout << "min shotpoint=" << minspn << endl;


    int y = mininy + 1;
    cs2ano_(&ipdflg, &lid, &y, &rline, &rtrace);
    cout << "next rline = " << rline << endl;
    cout << "next rtrace = " << rtrace << endl;
    line = rline;
    float spn;
    trtosp_(&line, &spn, &rtrace, &ier);
    if (ier) {
        printf(" trtosp ier: %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }
    cout << "next shotpoint=" << spn << endl;
    float increment = spn - minspn;
    cout << "increment=" << increment << endl;
    sprintf(string, "%.2f", increment);
    float inc = atof(string);
    inc = incShotpoint / inc;

    cout << "inc = " << inc << endl;
    array[2] = round(inc);
    env->SetFloatArrayRegion(ret, (jsize) 0, 3, (jfloat *) array);
    close_proj(ipdflg);
    return ret;
}

JNIEXPORT jfloatArray JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_getTimeRangeInfo
(JNIEnv *env, jobject, jstring projectName, jint projectType, jstring volumeName) {
    char volume[256];
    char prjnam[PROJ_NAME_LEN];
    //int    ndxbuf[NO_OF_BUF_IDX];
    int ier, ipdflg = projectType;
    int mininx, mininy, maxinx, maxiny;
    int mode;
    struct dm3dfc_record fcrec;
    jfloatArray ret;
    float array[3];

    ret = (jfloatArray) env->NewFloatArray(3);

    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    prjin2_(&ier, &ipdflg, prjnam, s);

    if (ier != 0) {
        printf("prjin2 error %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    s = (jint) (env)->GetStringUTFLength(volumeName);
    (env)->GetStringUTFRegion(volumeName, 0, s, volume);
    volume[s] = '\0';

    if (ipdflg == 3) {
        srvbnd_(&ipdflg, &mininx, &mininy, &maxinx, &maxiny, &ier);
        if (ier) {
            printf(" srvbnd error: %d\n", ier);
            close_proj(ipdflg);
            return ret;
        }
    }
    (void) memset((void*) & fcrec, 0, sizeof ( fcrec));

    mode = 1; //open for reading
    dm3dop_(volume, (void*) & fcrec, &mode, &ier);
    if (ier) {
        printf(" dm3dop error : %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    int lfnxrg = fcrec.lfnxrg;
    dm3dco_(&lfnxrg, (void*) & fcrec, &ier);
    if (ier) {
        printf(" dm3dco_ error (exec phase): %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    array[0] = fcrec.rfmnpt; //default start time
    array[1] = fcrec.rfmxpt; //default end time
    array[2] = fcrec.rfsamp; //sample rate
    env->SetFloatArrayRegion(ret, (jsize) 0, 3, (jfloat *) array);

    close_proj(ipdflg);
    return ret;

}

JNIEXPORT jobjectArray JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_getLineTraceInfo
(JNIEnv *env, jobject, jstring projectName, jint projectType, jstring volumeName) {
    char volume[256];
    char prjnam[PROJ_NAME_LEN];
    int ndxbuf[NO_OF_BUF_IDX];
    int ier, ival1 = 1, ipdflg = projectType;
    int mininx, mininy, maxinx, maxiny, ix, iy;
    int mode, jfmnpy, jfmxpy;
    float rline_min, rline_max, rtrace_min, rtrace_max, rline_next, rtrace_next;
    int iline_min, iline_max, itrace_min, itrace_max, iline_inc, itrace_inc;
    struct dm3dfc_record fcrec;
    jobjectArray ret;
    int array[2][3] = {
        {0, 0, 1},
        {0, 0, 1}};
    jintArray row = (jintArray) env->NewIntArray(3);
    ret = (jobjectArray) env->NewObjectArray(2, env->GetObjectClass(row), 0);
    env->SetIntArrayRegion((jintArray) row, (jsize) 0, 3, (jint *) array[0]);
    env->SetObjectArrayElement(ret, 0, row);
    row = (jintArray) env->NewIntArray(3);
    env->SetIntArrayRegion((jintArray) row, (jsize) 0, 3, (jint *) array[1]);
    env->SetObjectArrayElement(ret, 1, row);

    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    prjin2_(&ier, &ipdflg, prjnam, s);

    if (ier != 0) {
        printf("prjin2 error %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    s = (jint) (env)->GetStringUTFLength(volumeName);
    (env)->GetStringUTFRegion(volumeName, 0, s, volume);
    volume[s] = '\0';

    if (ipdflg == 3) {
        srvbnd_(&ipdflg, &mininx, &mininy, &maxinx, &maxiny, &ier);
        if (ier) {
            printf(" srvbnd error: %d\n", ier);
            //goto close_proj;
            close_proj(ipdflg);
            return ret;
        }

        printf(" project extents:\n");
        printf("     minimum ix = %d\n", mininx);
        printf("     minimum iy = %d\n", mininy);
        printf("     maximum ix = %d\n", maxinx);
        printf("     maximum iy = %d\n", maxiny);

    }
    (void) memset((void*) & fcrec, 0, sizeof ( fcrec));

    mode = 1; //open for reading
    dm3dop_(volume, (void*) & fcrec, &mode, &ier);
    if (ier) {
        printf(" dm3dop error in getLineTraceInfo : %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    int lfnxrg = fcrec.lfnxrg;
    dm3dco_(&lfnxrg, (void*) & fcrec, &ier);
    if (ier) {
        printf(" dm3dco_ error (exec phase): %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    /*  validate .pds or .ps2 against .3dv file  */
    dm3dpf_((void*) & fcrec, &ipdflg, &ier);
    if (ier) {
        printf(" dm3dpf error (exec phase): %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    mode = 1;
    jfmnpy = fcrec.lfmnpy;
    jfmxpy = fcrec.lfmxpy;

    dm3dnx_((void*) & fcrec, &mode, &jfmnpy, &jfmxpy, &ival1, ndxbuf, &ier);

    if (ier) {
        printf(" dm3dnx error: %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }
    mininx = fcrec.lfmnax;
    maxinx = fcrec.lfmxax;
    mininy = fcrec.lfmnay;
    maxiny = fcrec.lfmxay;

    ix = mininx;
    iy = mininy;
    cs2ano_(&ipdflg, &ix, &iy, &rline_min, &rtrace_min);
    iline_min = (int) rline_min;
    itrace_min = (int) rtrace_min;
    cout << " iline_min= " << iline_min << " itrace_min= " << itrace_min << endl;
    ix = maxinx;
    iy = maxiny;
    cs2ano_(&ipdflg, &ix, &iy, &rline_max, &rtrace_max);

    iline_max = (int) rline_max;
    itrace_max = (int) rtrace_max;
    cout << " iline_max= " << iline_max << " itrace_max= " << itrace_max << endl;
    ix = mininx;
    iy = mininy;
    ix++;
    cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
    if (rline_max > rline_min) {
        if (rline_next > rline_min)
            iline_inc = (int) (rline_next - rline_min);
        else {
            ix--;
            iy++;
            cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
            if (rline_next > rline_min)
                iline_inc = (int) (rline_next - rline_min);
            else
                iline_inc = 666; //iline_inc = 1;
        }
    } else if (rline_max < rline_min) {
        if (rline_next < rline_min)
            iline_inc = (int) (rline_next - rline_min);
        else {
            ix--;
            iy++;
            cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
            if (rline_next < rline_min)
                iline_inc = (int) (rline_next - rline_min);
            else
                iline_inc = -666; //iline_inc = -1;
        }
    } else
        iline_inc = 1;

    ix = mininx;
    iy = mininy;
    iy++;
    cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
    if (rtrace_max > rtrace_min) {
        if (rtrace_next > rtrace_min)
            itrace_inc = (int) (rtrace_next - rtrace_min);
        else {
            ix++;
            iy--;
            cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
            if (rtrace_next > rtrace_min)
                itrace_inc = (int) (rtrace_next - rtrace_min);
            else
                itrace_inc = 888; //itrace_inc = 1
        }
    } else if (rtrace_max < rtrace_min) {
        if (rtrace_next < rtrace_min)
            itrace_inc = (int) (rtrace_next - rtrace_min);
        else {
            ix++;
            iy--;
            cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
            if (rtrace_next < rtrace_min)
                itrace_inc = (int) (rtrace_next - rtrace_min);
            else
                itrace_inc = -888; //itrace_inc = -1
        }
    } else
        itrace_inc = 1;


    array[0][0] = iline_min;
    array[0][1] = iline_max;
    array[0][2] = iline_inc;
    array[1][0] = itrace_min;
    array[1][1] = itrace_max;
    array[1][2] = itrace_inc;
    row = (jintArray) env->NewIntArray(3);
    env->SetIntArrayRegion((jintArray) row, (jsize) 0, 3, (jint *) array[0]);
    env->SetObjectArrayElement(ret, 0, row);
    row = (jintArray) env->NewIntArray(3);
    env->SetIntArrayRegion((jintArray) row, (jsize) 0, 3, (jint *) array[1]);
    env->SetObjectArrayElement(ret, 1, row);
    close_proj(ipdflg);
    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_getLineTraceTimeInfo
(JNIEnv *env, jobject, jstring projectName, jint projectType, jstring volumeName) {
    char volume[256];
    char prjnam[PROJ_NAME_LEN];
    int ndxbuf[NO_OF_BUF_IDX];
    int ier, ival1 = 1, ipdflg = projectType;
    int mininx, mininy, maxinx, maxiny, ix, iy;
    int mode, jfmnpy, jfmxpy;
    float rline_min, rline_max, rtrace_min, rtrace_max, rline_next, rtrace_next;
    int iline_min, iline_max, itrace_min, itrace_max, iline_inc, itrace_inc;
    struct dm3dfc_record fcrec;
    jobjectArray ret;
    int array[4][3] = {
        {0, 0, 1},
        {0, 0, 1},
        {0, 0, 1},
        {0, 0, 0}};
    jintArray row = (jintArray) env->NewIntArray(3);
    ret = (jobjectArray) env->NewObjectArray(4, env->GetObjectClass(row), 0);
    env->SetIntArrayRegion((jintArray) row, (jsize) 0, 3, (jint *) array[0]);
    env->SetObjectArrayElement(ret, 0, row);
    row = (jintArray) env->NewIntArray(3);
    env->SetIntArrayRegion((jintArray) row, (jsize) 0, 3, (jint *) array[1]);
    env->SetObjectArrayElement(ret, 1, row);
    row = (jintArray) env->NewIntArray(3);
    env->SetIntArrayRegion((jintArray) row, (jsize) 0, 3, (jint *) array[2]);
    env->SetObjectArrayElement(ret, 2, row);
    row = (jintArray) env->NewIntArray(3);
    env->SetIntArrayRegion((jintArray) row, (jsize) 0, 3, (jint *) array[3]);
    env->SetObjectArrayElement(ret, 3, row);



    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    prjin2_(&ier, &ipdflg, prjnam, s);

    if (ier != 0) {
        printf("prjin2 error %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    s = (jint) (env)->GetStringUTFLength(volumeName);
    (env)->GetStringUTFRegion(volumeName, 0, s, volume);
    volume[s] = '\0';

    if (ipdflg == 3) {
        srvbnd_(&ipdflg, &mininx, &mininy, &maxinx, &maxiny, &ier);
        if (ier) {
            printf(" srvbnd error: %d\n", ier);
            close_proj(ipdflg);
            return ret;
        }

        printf(" project extents:\n");
        printf("     minimum ix = %d\n", mininx);
        printf("     minimum iy = %d\n", mininy);
        printf("     maximum ix = %d\n", maxinx);
        printf("     maximum iy = %d\n", maxiny);

    }
    (void) memset((void*) & fcrec, 0, sizeof ( fcrec));

    mode = 1; //open for reading
    dm3dop_(volume, (void*) & fcrec, &mode, &ier);
    if (ier) {
        printf(" dm3dop error in getLineTraceInfo : %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    int lfnxrg = fcrec.lfnxrg;
    dm3dco_(&lfnxrg, (void*) & fcrec, &ier);
    if (ier) {
        printf(" dm3dco_ error (exec phase): %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    /*  validate .pds or .ps2 against .3dv file  */
    dm3dpf_((void*) & fcrec, &ipdflg, &ier);
    if (ier) {
        printf(" dm3dpf error (exec phase): %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    mode = 1;
    jfmnpy = fcrec.lfmnpy;
    jfmxpy = fcrec.lfmxpy;

    dm3dnx_((void*) & fcrec, &mode, &jfmnpy, &jfmxpy, &ival1, ndxbuf, &ier);

    if (ier) {
        printf(" dm3dnx error: %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }
    mininx = fcrec.lfmnax;
    maxinx = fcrec.lfmxax;
    mininy = fcrec.lfmnay;
    maxiny = fcrec.lfmxay;

    ix = mininx;
    iy = mininy;
    cs2ano_(&ipdflg, &ix, &iy, &rline_min, &rtrace_min);
    iline_min = (int) rline_min;
    itrace_min = (int) rtrace_min;
    cout << " iline_min= " << iline_min << " itrace_min= " << itrace_min << endl;
    ix = maxinx;
    iy = maxiny;
    cs2ano_(&ipdflg, &ix, &iy, &rline_max, &rtrace_max);

    iline_max = (int) rline_max;
    itrace_max = (int) rtrace_max;
    cout << " iline_max= " << iline_max << " itrace_max= " << itrace_max << endl;
    ix = mininx;
    iy = mininy;
    ix++;
    cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
    if (rline_max > rline_min) {
        if (rline_next > rline_min) {
            iline_inc = (int) (rline_next - rline_min);
        } else {
            ix--;
            iy++;
            cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
            if (rline_next > rline_min) {
                iline_inc = (int) (rline_next - rline_min);
            } else {
                iline_inc = 666; //iline_inc = 1;
            }
        }
    } else if (rline_max < rline_min) {
        if (rline_next < rline_min)
            iline_inc = (int) (rline_next - rline_min);
        else {
            ix--;
            iy++;
            cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
            if (rline_next < rline_min)
                iline_inc = (int) (rline_next - rline_min);
            else
                iline_inc = -666; //iline_inc = -1;
        }
    } else {
        cout << " iline_inc = 1 " << endl;
        iline_inc = 1;
    }
    ix = mininx;
    iy = mininy;
    iy++;
    cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
    if (rtrace_max > rtrace_min) {
        if (rtrace_next > rtrace_min)
            itrace_inc = (int) (rtrace_next - rtrace_min);
        else {
            ix++;
            iy--;
            cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
            if (rtrace_next > rtrace_min)
                itrace_inc = (int) (rtrace_next - rtrace_min);
            else
                itrace_inc = 888; //itrace_inc = 1
        }
    } else if (rtrace_max < rtrace_min) {
        if (rtrace_next < rtrace_min)
            itrace_inc = (int) (rtrace_next - rtrace_min);
        else {
            ix++;
            iy--;
            cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
            if (rtrace_next < rtrace_min)
                itrace_inc = (int) (rtrace_next - rtrace_min);
            else
                itrace_inc = -888; //itrace_inc = -1
        }
    } else
        itrace_inc = 1;


    array[0][0] = iline_min;
    array[0][1] = iline_max;
    array[0][2] = iline_inc;
    array[1][0] = itrace_min;
    array[1][1] = itrace_max;
    array[1][2] = itrace_inc;
    array[2][0] = fcrec.rfmnpt;
    array[2][1] = fcrec.rfmxpt;
    array[2][2] = fcrec.rfsamp;
    array[3][0] = fcrec.lftmdp;
    cout << "fcrec.rfmnpt = " << fcrec.rfmnpt << endl;
    cout << "fcrec.rfmxpt = " << fcrec.rfmxpt << endl;
    cout << "fcrec.lfmnpt = " << fcrec.lfmnpt << endl;
    cout << "fcrec.lfmxpt = " << fcrec.lfmxpt << endl;
    cout << "fcrec.lfmnat = " << fcrec.lfmnat << endl;
    cout << "fcrec.lfmxat = " << fcrec.lfmxat << endl;
    cout << "fcrec.lftmdp (time/depth flag) = " << fcrec.lftmdp << endl;
    if (fcrec.lftmdp == 1)
        cout << volume << " has time data" << endl;
    else if (fcrec.lftmdp == 2)
        cout << volume << " has depth data" << endl;
    else
        cout << volume << " has unknown data domain" << endl;
    row = (jintArray) env->NewIntArray(3);
    env->SetIntArrayRegion((jintArray) row, (jsize) 0, 3, (jint *) array[0]);
    env->SetObjectArrayElement(ret, 0, row);
    row = (jintArray) env->NewIntArray(3);
    env->SetIntArrayRegion((jintArray) row, (jsize) 0, 3, (jint *) array[1]);
    env->SetObjectArrayElement(ret, 1, row);
    row = (jintArray) env->NewIntArray(3);
    env->SetIntArrayRegion((jintArray) row, (jsize) 0, 3, (jint *) array[2]);
    env->SetObjectArrayElement(ret, 2, row);
    row = (jintArray) env->NewIntArray(3);
    env->SetIntArrayRegion((jintArray) row, (jsize) 0, 3, (jint *) array[3]);
    env->SetObjectArrayElement(ret, 3, row);

    close_proj(ipdflg);

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_getWellListBySeisWorkProjectName
(JNIEnv *env, jobject, jstring projectName, jint projectType) {

    char prjnam[PROJ_NAME_LEN];
    int ier, ipdflg = projectType, itype;
    int i;


    jobjectArray ret;

    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    ret = (jobjectArray) (env)->NewObjectArray(0, env->FindClass("java/lang/String"), env->NewStringUTF(""));
    int initRet = InitOW();
    if (initRet == OWINIT_FAILED) {
        jstring arrayElement = (jstring) env->NewStringUTF("Error in initializing OpenWorks");
        ret = (jobjectArray) (env)->NewObjectArray(1, env->FindClass("java/lang/String"), env->NewStringUTF(""));
        env->SetObjectArrayElement(ret, 0, arrayElement);
        return ret;
    } else if (initRet == ERROR_GET_LICENSE) {
        jstring arrayElement = (jstring) env->NewStringUTF("Error in Getting OpenWorks License");
        ret = (jobjectArray) (env)->NewObjectArray(1, env->FindClass("java/lang/String"), env->NewStringUTF(""));
        env->SetObjectArrayElement(ret, 0, arrayElement);
        return ret;
    }


    char message[80];
    prjin2_(&ier, &ipdflg, prjnam, s);

    if (ier != OKAY) {
        printf("prjin2 error %d\n", ier);
        return ret;
        if (ier == 1) {
            cout << "Error: Project Definition file not found (.pds or .ps2) for " << prjnam;
            sprintf(message, "Error: Project Definition file not found (.pds or .ps2) for %s", prjnam);
        } else if (ier == 2) {
            cout << "Error: Unable to log project on all drives for " << prjnam;
            sprintf(message, "Error: Unable to log project on all drives for %s", prjnam);
        } else if (ier == 4) {
            cout << "Error: Could not obtain an OpenWorks license for " << prjnam;
            sprintf(message, "Error: Could not obtain an OpenWorks license for %s", prjnam);
        } else {
            cout << "Error: Unknown error in calling prjin2_";
            sprintf(message, "Error: Unknown error in calling prjin2_ %s", prjnam);
        }
        ret = (jobjectArray) (env)->NewObjectArray(1, env->FindClass("java/lang/String"), env->NewStringUTF(""));
        jstring arrayElement = (jstring) env->NewStringUTF(message);
        env->SetObjectArrayElement(ret, 0, arrayElement);
        close_proj(ipdflg);
        return ret;
    }
    // ------- Obtain the OpenWorks project for this seismic project.
    char OWproject[MAX_OWDB_NAME + 1];

    ier = getOWProjOfSeisProj(prjnam, OWproject);
    printf("ier = %d\n", ier);
    if (ier != OKAY) {
        cout << "FAILURE: getOwProjOfSeisProj: "
                << "Could not get OW project for seismic project " << prjnam;
        sprintf(message, "Error: Could not get OW project for seismic project %s", prjnam);
        jstring arrayElement = (jstring) env->NewStringUTF(message);
        env->SetObjectArrayElement(ret, 0, arrayElement);
        close_proj(ipdflg);
        return ret;
    }
    printf("OWproject = %s\n", OWproject);
    prjOpen(OWproject, PRJ_SHARED, &ier);
    if (ier != SUCCESSFUL) {
        printf("Open: prjOpen failure\n");
        sprintf(message, "Error: prjOpen failure for OpenWorks project %s", OWproject);
        jstring arrayElement = (jstring) env->NewStringUTF(message);
        env->SetObjectArrayElement(ret, 0, arrayElement);
        close_proj(ipdflg);
        return ret;
    }

    ier = gdiInit();
    if (ier != SUCCESSFUL) {
        OHNOOO(&ier, "InitOW: gdiInit() failed");
        sprintf(message, "Error: gdiInit() failed for OpenWorks project %s", OWproject);
        jstring arrayElement = (jstring) env->NewStringUTF(message);
        env->SetObjectArrayElement(ret, 0, arrayElement);
        close_proj(ipdflg);
        return ret;
    }

    /* get all  the well lists in the database*/
    int count;
    gdiWellList_t *well_lists;
    ier = gdiReadSet(WELL_LIST, &count, (void**) & well_lists);
    if (ier != SUCCESSFUL) {
        OHNOOO(&ier, "ReadWells: error reading well lists.");
        sprintf(message, "Error: error reading well lists for OpenWorks project %s", OWproject);
        jstring arrayElement = (jstring) env->NewStringUTF(message);
        env->SetObjectArrayElement(ret, 0, arrayElement);
        close_proj(ipdflg);
        return ret;
    }

    ret = (jobjectArray) (env)->NewObjectArray(count, env->FindClass("java/lang/String"), env->NewStringUTF(""));
    for (i = 0; i < count; i++) {
        jstring arrayElement = (jstring) env->NewStringUTF(well_lists[i].name);
        env->SetObjectArrayElement(ret, i, arrayElement);
    }
    /* clean up the gdi memory allocated by the gdiReadSet call */
    gdiFree((char *) well_lists);
    CloseOW();
    close_proj(ipdflg);
    return ret;

}

JNIEXPORT jobjectArray JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_getHorizonList
(JNIEnv *env, jobject, jstring projectName, jint projectType) {
    int icolor[NO_OF_COLOR];
    char remark[REMARK_LEN];
    char id_remark[REMARK_LEN + 6];
    char prjnam[PROJ_NAME_LEN];
    int mininx, mininy, maxinx, maxiny, ixmin, ixmax, iymin, iymax;
    int ier, ipdflg = projectType, istat, itype, maxhrz;
    int i, j, iacthz;
    int iop, id, ionset, nxtseg, lstend, minus_one = -1;
    float rnul, zmin, zmax;
    jobjectArray ret;

    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    prjin2_(&ier, &ipdflg, prjnam, s);

    if (ier != 0) {
        printf("prjin2 error %d\n", ier);
        close_proj(ipdflg);
    }

    if (ipdflg == 3) {
        srvbnd_(&ipdflg, &mininx, &mininy, &maxinx, &maxiny, &ier);
        if (ier) {
            printf(" srvbnd error: %d\n", ier);
            close_proj(ipdflg);
        }

        /*  open the horizon file  */
        rnul = 1.0e+37;
        hrzopn_(&istat, prjnam, &ixmin, &ixmax, &iymin, &iymax, &rnul, &maxhrz);
        if (istat == 0) {
            printf(" horizon file opened \n");
            printf(" current number of horizons: %d\n", maxhrz);
        } else if (istat == 2) {
            printf(" horizon file was created. I am going to quit. \n");
            close_hrz_file(ipdflg, istat);
        } else
            printf(" hrzopn error: %d\n", istat);

        /*  check that survey bounds match horizon file bounds  */
        if ((maxinx - mininx != ixmax - ixmin) || (maxiny - mininy != iymax - iymin)) {
            printf(" survey mismatch with horizon file \n");
            close_hrz_file(ipdflg, istat);
        }
    } else {
        /*
         **  pjhzsz is to get the number of all/active-only
         **  horizon from 2d <project>.hrz_cat.
         **  1st argument is total horizon in the project
         **  2nd argument is active horizon
         */
        pjhzsz_(&maxhrz, &iacthz, &ier);
        if (ier) {
            printf("error in opening horizon catalog.\n");
            close_hrz_file(ipdflg, istat);
        }
    }
    vector<string> horizons;
    /*  list all the horizons here by title and let the user choose  */
    j = 0;
    for (i = 1; i <= maxhrz; i++) {
        iop = 1;
        id = 0;
        hrzhdr_(&i, &iop, &istat, &id, remark, &zmin, &zmax, icolor,
                &ionset, &itype, &nxtseg, &lstend);
        if (id > 0 && istat != 1 && istat != 0) {
            printf(" hrzhdr error: %d\n", istat);
            close_proj(ipdflg);
        }

        /*   close each hzd ( warning this is not in the manual)  */
        if (ipdflg == 3) hzdcls_(&minus_one, &i, &ier);

        /*  id==0 means deleted or not-active horizon  */
        if (id != 0) {
            sprintf(id_remark, "%d %s\n", i, remark);

            if (ipdflg == 2) {
                char *tail = strstr(remark, "__");
                if (tail != 0) {
                    *tail = '\0';
                } else {
                    cout << "not found" << endl;
                }
            }
            horizons.push_back(remark);
        }
    }

    sort(horizons.begin(), horizons.end());
    ret = (jobjectArray) (env)->NewObjectArray(horizons.size(), env->FindClass("java/lang/String"), env->NewStringUTF(""));
    for (i = 0; i < horizons.size(); i++) {
        jstring arrayElement = (jstring) env->NewStringUTF(horizons[i].c_str());
        env->SetObjectArrayElement(ret, j++, arrayElement);

    }
    close_proj(ipdflg);
    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_com_bhpb_services_landmark_jni_LandmarkProjectHandler_get2DHorizonListByLine
(JNIEnv *env, jobject, jstring projectName, jstring lineName) {

    char prjnam[PROJ_NAME_LEN];
    char lname[LINE_NAME_LEN];
    char suffix[16];
    char prefix[31];

    char* fname;
    int ier;
    int ipdflg = 2;

    jobjectArray ret;
    jsize s = (jint) (env)->GetStringUTFLength(projectName);
    (env)->GetStringUTFRegion(projectName, 0, s, prjnam);
    prjnam[s] = '\0';

    ret = (jobjectArray) (env)->NewObjectArray(0, env->FindClass("java/lang/String"), env->NewStringUTF(""));
    cout << "prjnam = " << prjnam << endl;
    // initial global cache - this can take some time
    fstbio_cache_init();

    prjin2_(&ier, &ipdflg, prjnam, s);
    if (ier != 0) {
        printf("prjin2 error %d\n", ier);
        close_proj(ipdflg);
        return ret;
    }

    s = (jint) (env)->GetStringUTFLength(lineName);
    (env)->GetStringUTFRegion(lineName, 0, s, lname);
    lname[s] = '\0';
    int ival30 = 30;
    pumknm_(&ival30, lname, lname, &ier);
    strcpy(suffix, ".hzd_glb");
    strcpy(prefix, lname);
    int init = 1;
    vector<string> files;
    while ((fname = fstbio_find_pre_suf_all(suffix, prefix, init)) != 0) {
        init = 0;

        char* ptr = strstr(fname, lname);
        if (ptr != 0) {
            string s(fname);
            s = s.substr(30, s.length());
            char *fname1 = s.c_str();
            char* tail = strstr(fname1, "__");
            if (tail != 0)
                *tail = '\0';
            s = fname1;
            if (exist(s, &files) == 0)
                files.push_back(s);
        }
        free(fname);
    }
    sort(files.begin(), files.end());
    ret = (jobjectArray) (env)->NewObjectArray(files.size(), env->FindClass("java/lang/String"), env->NewStringUTF(""));
    for (int i = 0; i < files.size(); i++) {
        jstring arrayElement = (jstring) env->NewStringUTF(files[i].c_str());
        env->SetObjectArrayElement(ret, i, arrayElement);
    }

    close_proj(ipdflg);
    return ret;

}

void close_proj(int ipdflg) {
    int ier;

    prjend_(&ipdflg, &ier);
    if (ier)
        printf(" prjend ier: %d\n", ier);
}

void close_hrz_file(int ipdflg, int istat) {

    if (ipdflg == 3) {
        hrzcls_(&istat);
        if (istat)
            printf(" hrzcls error: %d\n", istat);
    }
}

void close_hrz_file(int ipdflg) {
    int istat;
    if (ipdflg == 3) {
        hrzcls_(&istat);
        if (istat)
            printf(" hrzcls error: %d\n", istat);
    }
}

int search_hrz(const char *horizon, int max_hrz, int i_prj_type) {
    int icolor[NO_OF_COLOR];
    char remark[REMARK_LEN];
    int istat, itype, maxhrz = max_hrz;
    int i;
    int iop, id, ionset, nxtseg, lstend;
    float zmin, zmax;

    for (i = 1; i <= maxhrz; i++) {
        iop = 1;
        id = 0;
        hrzhdr_(&i, &iop, &istat, &id, remark, &zmin, &zmax, icolor,
                &ionset, &itype, &nxtseg, &lstend);
        if (id > 0 && istat != 1 && istat != 0) {
            printf(" hrzhdr error: %d\n", istat);
            return -1;
        }

        /*   close each hzd ( warning this is not in the manual)  */
        //if(ipdflg == 3) hzdcls_(&minus_one, &i, &ier);

        /*  id==0 means deleted or not-active horizon  */
        if (id != 0) {
            printf("%d %s\n", i, remark);
            string h_name(remark);

            if (h_name.compare(0, h_name.length(), horizon) == 0)
                return i;
        }
    }
    return -1;
}

int getLineIdByLineName(char *linename) {
    int ix = 0, idum, maxlin, ier;
    char lname[LINE_NAME_LEN];

    pjlnsz_(&idum, &maxlin, &ier);
    cout << "total number of lines = " << idum << endl;
    cout << "active lines = " << maxlin << endl;
    int mininx = 1;
    int maxinx = maxlin;
    for (int ix = mininx; ix <= maxinx; ix++) {
        lnolnm_(&ix, lname, &ier);
        string name(lname);
        if (name.compare(0, strlen(linename), linename) == 0)
            return ix;
    }
    return ix;
}

int exist(string s, vector<string> *v) {
    for (int i = 0; i < v->size(); i++) {
        if (v->at(i).compare(0, v->at(i).length(), s) == 0)
            return 1;
    }
    return 0;
}

int getProjectNames(int type, vector<string> *v) {
    int i, cnt1, ier;
    char **lst1;
    if (v == NULL)
        return 1;
    printf("try to open projects\n");
    if (type == 1) { //get all project names including 2D and 3D
        ier = plaGetAllProjects(&lst1, &cnt1);

        if (ier != 0) {
            printf("error getting all projects: %d\n", ier);
            return ier;
        }

        printf("Number of all projects: %d\n", cnt1);
    } else if (type == 2) { //get only 2D project names
        ier = plaGet2dProjects(&lst1, &cnt1);

        if (ier != 0) {
            printf("error getting 2d projects: %d\n", ier);
            return ier;
        }
    } else if (type == 3) {//get only 3D project names
        ier = plaGet3dProjects(&lst1, &cnt1);

        if (ier != 0) {
            printf("error getting 3d projects: %d\n", ier);
            return ier;
        }
    }


    for (i = 0; i < cnt1; i++) {
        string s(lst1[i]);
        v->push_back(s);
    }
    ier = plaFreeProjectList(&lst1, &cnt1);
    if (ier != 0) {
        printf("Error releasing projects\n");
        return ier;
    }
    printf("all projects released\n");
    //sort(v->.begin(),v->end());
    return 0;
}

void setMessage(JNIEnv *env, jobject jobj, string message, jobject lock) {
    jstring stringObject;
    jmethodID mid;
    jclass cls = env->GetObjectClass(jobj);
    if (!(stringObject = env->NewStringUTF(message.c_str()))) {
        cout << "A null returned  env->NewStringUTF " << endl;
        env->ExceptionDescribe();
        env->ExceptionClear();
        return;
    }

    mid = env->GetMethodID(cls, "setMessage", "(Ljava/lang/String;)V");

    if (mid == 0) {
        cout << "Can't find method setMessage";
        return;
    }

    env->ExceptionClear();
    env->MonitorEnter(lock);
    env->CallVoidMethod(jobj, mid, stringObject);
    env->MonitorExit(lock);

    if (env->ExceptionOccurred()) {
        cout << "error occured in call back setMessage." << endl;
        env->ExceptionDescribe(); /* write exception data to the console */
        env->ExceptionClear(); /* clear the exception that was pending */
        return;
    }
}

static void CloseOW() {
    long ier = SUCCESSFUL;

    /* close the OpenWorks Project Connection */
    prjClose(&ier);
    if (ier != SUCCESSFUL) {
        OHNOOO(&ier, "CloseOW: Problem closing the database");
        return;
    }
}

static int InitOW() {
    long ier = SUCCESSFUL;
    char err_buff[256];

    /* Initialize OpenWorks */
    ier = (long) owinit();
    if (ier != SUCCESSFUL) {
        printf("Open: owinit() failed\n");
        return OWINIT_FAILED;
    }

    owiCheckoutRuntime(FALSE, (Widget) NULL, (char *) NULL,
            err_buff, 256, &ier);
    if (ier != SUCCESSFUL) {
        printf("err_buff|%s|\n", err_buff);
        return ERROR_GET_LICENSE;
    }
    return 0;
}
