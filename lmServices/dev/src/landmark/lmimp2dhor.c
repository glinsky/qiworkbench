#include <iostream.h>
#include <exception>
#include <fstream.h>
#include <sstream>
#include <string>
#include <vector>
#include "su.h"
#include "sdlprototypes.h"
char *sdoc[] = {
    "",
    " LMIMP2DHOR - import ASCII horizons (.XYZ files) to LANDMARK 2D application ",
    "",
    "   lmimp2dhor lm_project=  hname=  horizons=",
    "",
    " Required parameter:",
    " lm_project=Landmark 2D project name",
    " hname=additional horizon attribute or base name",
    " horizons=horizon names (.XYZ files with absolute path) delimited by a comma",
    "                                   ",
    "									",
    " The input horizon file (*.xyz) is assumed to be the one exported by lmexp2dhor.",
    " This module will extract line name out of the input file name (*.xyz) and perform import",
    " horizon data to that line",
    NULL
};
/**************** end self doc ***********************************/
using namespace std;

typedef struct {
    double ep;
    double cdp;
    double zval;
} horizonline;

vector<string> tokenize(const string& str, const string& delimiters);
void stripLeadingAndTrailingBlanks(string& StringToModify);
void close_hrz_file(int ipdflg);
void close_proj(int type);
void get_line_num_by_name(char *linename, int *linenum);
void init_2d(char *prjnam, int type, int* ixmax, int* maxhrz, int *status);
void get_horizon_datalist(char *filepath, int linenum, vector<horizonline> *datalist, int *o_minxline, int *o_maxxline, int *o_minzval, int *o_maxzval, int* status);
void hrz_create(int* numhrz, char* horizon_name, int* status);
void hrz_load_buffer(int type, vector<horizonline> *datalist, int minxline, int maxxline, float *data, float* zmin, float* zmax);
void hrz_write_buffer(int numhrz, int ix, int iymin, int iymax, float* data, int *status);
void hrz_update_hdr(int numhrz, float zmin, float zmax);

#define NO_OF_COLOR   3
#define REMARK_LEN    61
#define LANDMARK_NULL_VALUE 9999999933815812510711506376257961984.00
#define BHPSU_NULL_VALUE -999.25
const int OKAY = 0;
const int MAX_LINE_NAME = 30;

int main(int argc, char **argv) {
    char *prjname;

    char *horizons;
    char *added_hname;
    int ier, ipdflg = 2, linenum;

    int maxhrz, maxlin;
    float *data;
    int id;
    /* Initialize */
    initargs(argc, argv);
    requestdoc(1);

    /* Set project name */
    MUSTGETPARSTRING("lm_project", &prjname);
    MUSTGETPARSTRING("hname", &added_hname);
    MUSTGETPARSTRING("horizons", &horizons);
    vector<string> hlist = tokenize(horizons, ",");
    if (hlist.size() < 1) {
        cout << "invalid horizons values " << endl;
        return 1;
    }

    string projectName(prjname);
    cout << "projectName " << projectName.c_str() << endl;
    cout << "ipdflg " << ipdflg << endl;
    init_2d(prjname, ipdflg, &maxlin, &maxhrz, &ier);
    if (ier) {
        cout << " Problem in initializing the project and horizon subsystem. " << endl;
        return ier;
    }

    cout << "maxlin = " << maxlin << endl;
    cout << "maxhrz = " << maxhrz << endl;

    /*  report on project extents  */

    for (int i = 0; i < hlist.size(); i++) {
        string filePath(hlist[i]);
        int lastpos = -1;
        int pos = filePath.find("/", 0);
        while (pos >= 0) {
            pos = filePath.find("/", ++pos);
            if (pos >= 0)
                lastpos = pos;
        }
        cout << "lastpos=" << lastpos << endl;
        int xyzpos = filePath.find(".xyz", 0);
        if (xyzpos == -1) {
            cout << "Unexpected horizon file format not ending with .xyz" << endl;
            close_proj(ipdflg);
            return 1;
        }
        string fileName = filePath.substr(lastpos + 1, xyzpos - lastpos - 1);
        cout << "fileName=" << fileName << endl;
        cout << " filePath = " << filePath << endl;

        string line_name = fileName.substr(0, MAX_LINE_NAME);
        string horizon_name = fileName.substr(MAX_LINE_NAME, fileName.size());
        horizon_name += "_";
        horizon_name += added_hname;
        cout << "line_name=" << line_name << endl;
        cout << "horizon_name=" << horizon_name << endl;
        hrz_create(&id, horizon_name.c_str(), &ier);
        if (ier) {
            continue;
        }
        cout << "horizon id = " << id << endl;
        char lname[MAX_LINE_NAME];
        int ival30 = 30;
        strcpy(lname, line_name.c_str());
        pumknm_(&ival30, lname, lname, &ier);
        lnotst_(&linenum, lname, &ier);
        if (ier != 0) {
            fprintf(stderr, "No line found with given name %s\n", line_name.c_str());
            return 1;
        }

        cout << "linenum = " << linenum << endl;

        int miniline, maxiline, maxxline, minxline, minzval, maxzval;
        vector<horizonline> hdatalist;
        get_horizon_datalist(filePath.c_str(), linenum, &hdatalist, &minxline, &maxxline, &minzval, &maxzval, &ier);
        if (ier) {
            cout << "Error in reading horizon input data. (.xyz)" << endl;
            return 1;
        }

        int iymin, iymax;
        cout << "minxline=" << minxline << " maxxline=" << maxxline << endl;

        /* get trace range for line */
        lny1y2_(&linenum, &iymin, &iymax, &ier);
        cout << "iymin=" << iymin << " iymax=" << iymax << endl;
        if (!((minxline >= iymin && minxline <= iymax) && (maxxline >= iymin && maxxline <= iymax))) {
            cout << "The trace range of the horizon input file " << filePath << " does not match the trace extents of line " << line_name << endl;
            continue;
        }

        data = new float[iymax - iymin + 1];
        for (int i = 0; i < iymax - iymin + 1; i++) {
            data[i] = LANDMARK_NULL_VALUE;
        }
        float zmin = 1.0e37, zmax = -1.0e37;
        hrz_load_buffer(ipdflg, &hdatalist, iymin, iymax, data, &zmin, &zmax);
        cout << "zmin=" << zmin << " zmax=" << zmax << endl;
        cout << "minline=" << miniline << ",maxiline=" << maxiline << ",minxline=" << iymin << ",maxxline=" << iymax << ",minzval=" << minzval << ",maxzval=" << maxzval << endl;
        /* write the data to the horizon */
        hrz_write_buffer(id, linenum, iymin, iymax, data, &ier);
        if (ier) {
            if (data != NULL)
                delete data;
            continue;
        }
        if (data != NULL)
            delete data;
        hrz_update_hdr(id, zmin, zmax);
        close_hrz_file(ipdflg);
        /*
         **  Find next available location in horizon file to put a new
         **  horizon and initialize it.  (horizon number is returned in numhrz.)
         **  (itype = 1 for a raster horizon.)
         */
        /*  output horizon  */
    }
    close_hrz_file(ipdflg);
    close_proj(ipdflg);
    return 0;
}

vector<string> tokenize(const string& str, const string& delimiters) {
    vector<string> tokens;

    string::size_type lastPos = 0, pos = 0;
    int count = 0;

    if (str.length() < 1) return tokens;

    // skip delimiters at beginning.
    lastPos = str.find_first_not_of(delimiters, 0);

    if ((str.substr(0, lastPos - pos).length()) > 0) {
        count = str.substr(0, lastPos - pos).length();

        for (int i = 0; i < count; i++)
            tokens.push_back("");

        if (string::npos == lastPos)
            tokens.push_back("");
    }

    // find first "non-delimiter".
    pos = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos) {
        // found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));

        // skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);

        if ((string::npos != pos) && (str.substr(pos, lastPos - pos).length() > 1)) {
            count = str.substr(pos, lastPos - pos).length();

            for (int i = 0; i < count; i++)
                tokens.push_back("");
        }

        pos = str.find_first_of(delimiters, lastPos);
    }

    return tokens;
}

void stripLeadingAndTrailingBlanks(string& StringToModify) {
    if (StringToModify.empty()) return;

    int startIndex = StringToModify.find_first_not_of(" ");
    int endIndex = StringToModify.find_last_not_of(" ");
    string tempString = StringToModify;
    StringToModify.erase();

    StringToModify = tempString.substr(startIndex, (endIndex - startIndex + 1));
}

void close_proj(int type) {
    int ier;
    prjend_(&type, &ier);
    if (ier)
        err(" prjend ier: %d\n", ier);
    return;
}

/*  close the horizon file  */
void close_hrz_file(int ipdflg) {
    int istat;
    if (ipdflg == 3) {
        hrzcls_(&istat);
        if (istat)
            printf(" hrzcls error: %d\n", istat);
    }
}

/* open horizon subsystem and project*/
void init_2d(char *prjnam, int type, int* ixmax, int* maxhrz, int *status) {
    int ier, ipdflg = type;
    int ixmin, iymin, iymax, idum, iacthz;
    float rnul;
    int istat;

    /* initialize seiswork project */
    prjin2_(&ier, &ipdflg, prjnam, strlen(prjnam));

    if (ier != 0) {
        fprintf(stderr, "prjin2 error %d\n", ier);
        if (ier == 1) {
            fprintf(stderr, "Error: Project Definition file not found (.pds or .ps2)\n");
        } else if (ier == 2) {
            fprintf(stderr, "Error: Unable to log project on all drives\n");
        } else if (ier == 4) {
            fprintf(stderr, "Error: Could not obtain an OpenWorks license\n");
        } else {
            fprintf(stderr, "Unknown error in opening project. \n");
        }
        close_proj(ipdflg);
        *status = 1;
        return;
    }

    if (ipdflg == 3) {
        printf("The program is only supported for 2d seismic projects\n");
        *status = 1;
        return;
    }

    /* open horizon subsystem*/
    hrzopn_(&istat, prjnam, &ixmin, ixmax, &iymin, &iymax, &rnul, maxhrz);
    if (istat == 0) {
        printf(" horizon file opened \n");
        printf("   current number of horizons: %d\n", *maxhrz);
    } else if (istat == 2)
        printf(" horizon file was created \n");
    else {
        printf(" hrzopn error: %d\n", istat);
        *status = 1;
        return;
    }

    /* get number of lines in the project */

    pjlnsz_(&idum, ixmax, &ier);
    if (ier) {
        printf(" error in reading line catalog.\n");
        *status = 1;
        return;
    }

    /* get the number of horizons */

    pjhzsz_(maxhrz, &iacthz, &ier);
    if (ier) {
        printf("error in opening horizon catalog.\n");
        *status = 1;
        return;
    }
    *status = 0;
    printf("init_2d completed successfully\n");
    return;
}

/* create a new horizon and return its value */

void hrz_create(int* numhrz, char* horizon_name, int* status) {

    int icolor[NO_OF_COLOR];
    char remark[REMARK_LEN];
    int itype, ionset, istat;
    int ival60 = 60, ier;

    /*
     **  Find next available location in horizon file to put a new
     **  horizon and initialize it.  (horizon number is returned in numhrz.)
     **  (itype = 1 for a raster horizon.)
     */
    itype = 1;
    icolor[0] = 255;
    icolor[1] = 200;
    icolor[2] = 200;
    ionset = 1;

    strcpy(remark, horizon_name);

    /* format horizon name to 60 characters */
    pumknm_(&ival60, remark, remark, &ier);
    hz2crt_(numhrz, &istat, &itype, icolor, &ionset, &itype, remark);
    if (istat) {
        printf(" hz2crt error: %d\n", istat);
        *status = 1;
    } else {
        printf("horizon creation successfully\n");
    }
    *status = 0;
    return;
}

void get_horizon_datalist(char *filepath, int linenum, vector<horizonline> *datalist, int *o_minxline, int *o_maxxline, int *o_minzval, int *o_maxzval, int* status) {
    fstream iFile(filepath, ios::in);

    if (!iFile || !iFile.is_open()) {
        cout << "Error opening input file" << endl;
        *status = 1;
        return;
    }
    int miniline = -1;
    int maxiline = -1;
    int minxline = -1;
    int maxxline = -1;
    double minzval = -1;
    double maxzval = -1;
    char str[100];

    while (!iFile.eof()) {
        iFile.getline(str, 100);

        //stripLeadingAndTrailingBlanks(line);
        istringstream totalSString(str);
        double d1, d2, d3;
        totalSString >> d1 >> d2 >> d3;
        horizonline h;
        h.ep = d1;
        if ((int) d1 != linenum) {
            cout << "The horizon file " << filepath << " contains data does not match with the given line number " << linenum << endl;
            *status = 1;
            iFile.close();
            return;
        }
        h.cdp = d2;
        h.zval = d3;
        datalist->push_back(h);
        if (miniline == -1)
            miniline = d1;
        else if (miniline >= d1)
            miniline = d1;
        if (maxiline == -1)
            maxiline = d1;
        else if (maxiline <= d1)
            maxiline = d1;

        if (minxline == -1)
            minxline = d2;
        else if (minxline >= d2)
            minxline = d2;
        if (maxxline == -1)
            maxxline = d2;
        else if (maxxline <= d2)
            maxxline = d2;

        if (minzval == -1)
            minzval = d3;
        else if (minzval >= d3 && d3 != BHPSU_NULL_VALUE)
            minzval = d3;
        if (maxzval == -1)
            maxzval = d3;
        else if (maxzval <= d3)
            maxzval = d3;
    }
    cout << "miniline=" << miniline << " maxiline=" << maxiline << " minxline=" << minxline << " maxxline=" << maxxline << " minzval=" << minzval << " maxzval=" << maxzval << endl;
    *o_minxline = minxline;
    *o_maxxline = maxxline;
    *o_minzval = minzval;
    *o_maxzval = maxzval;
    *status = 0;
    iFile.close();
    return;
}

/* fill a buffer with data */
void hrz_load_buffer(int type, vector<horizonline> *datalist, int minxline, int maxxline, float *data, float* zmin, float* zmax) {
    int index = 0;
    int ipdflg = type;
    for (int i = 0; i < datalist->size() - 1; i++) {
        if (i > datalist->size() - 5)
            cout << "ep= " << datalist->at(i).ep << ",cdp= " << datalist->at(i).cdp << ",zval=" << datalist->at(i).zval << endl;
        int ix, iy;
        float f1 = (float) datalist->at(i).ep, f2 = (float) datalist->at(i).cdp;
        cs2int_(&ipdflg, &f1, &f2, &ix, &iy);
        index = iy - minxline;
        if (datalist->at(i).zval == BHPSU_NULL_VALUE)
            data[index] = LANDMARK_NULL_VALUE;
        else
            data[index] = (float) (datalist->at(i).zval);
        if (i > datalist->size() - 5)
            cout << "ix= " << ix << ",iy= " << iy << ",data[" << index << "]= " << data[index] << endl;

        if (data[index] < *zmin)
            *zmin = data[index];
        if (data[index] > *zmax && data[index] != LANDMARK_NULL_VALUE)
            *zmax = data[index];
    }
}

/* write the data to the horizon */
void hrz_write_buffer(int numhrz, int ix, int iymin, int iymax, float* data, int *status) {

    int iop, iskip, istat;

    iop = 2;
    iskip = iymax - iymin + 1;

    /* write out the horizon */

    hrzrw_(&numhrz, &iop, &istat, &ix, &ix, &iymin, &iymax, data, &iskip);
    if (istat) {
        printf("hrzrw error: %d ix: %d\n", istat, ix);
        *status = istat;
    } else {
        printf("hrzrw write completed successfully\n");
        *status = 0;
    }


}

/* update the horizon header */

void hrz_update_hdr(int numhrz, float zmin, float zmax) {


    int icolor[NO_OF_COLOR];
    char remark[REMARK_LEN];
    int itype, ionset, nxtseg, id, lstend, iop, istat;
    float zdum;

    /*  update header (read header; then write header with new z values)  */
    /*
     ** write the horizon header with following values:
     **     id     = 1                - time data flag
     **     remark = this is horizon number xx.
     **     zmin   =                  - smallest value
     **     zmax   =                  - largest value
     **     icolor = 255,200,200      - redish gray
     **     ionset = 1                - min auto picking onset
     **     itype  = 1                - raster horizon
     **     nxtseg = 0                - (unused in raster horizons)
     **     lstend = 0                - (unused in raster horizons)
     */

    iop = 1;
    hrzhdr_(&numhrz, &iop, &istat, &id, remark, &zdum, &zdum, icolor, &ionset,
            &itype, &nxtseg, &lstend);
    if (istat) {
        printf(" hrzhdr error: %d\n", istat);

    }
    iop = 2;
    hrzhdr_(&numhrz, &iop, &istat, &id, remark, &zmin, &zmax, icolor, &ionset,
            &itype, &nxtseg, &lstend);
    if (istat) {
        printf(" hrzhdr error: %d\n", istat);

    } else {
        printf("hrz_update_hdr successful\n");
    }
}

/*  INPUT linename
 *  OUTPUT linenum
 *  Assuming 2D project is already initialized (prjin2_ or prjini_ successfully executed)
 */
void get_line_num_by_name(char *linename, int *linenum) {
    // ------ Get the number of lines in project (needed to allocate memory).
    int totalLines = 0;
    int activeLines = 0;
    int status, id;
    int lastLine, index;
    char name[MAX_LINE_NAME + 1];
    pjlnsz_(&totalLines, &activeLines, &status);
    if (status == OKAY) {
        //    	int mininx = 1;
        int maxinx = activeLines;
        // loop through each line
        // ------ Loop to get line number and name for each line.  Print
        // ------ each, adding min and max trace from above.
        lastLine = 0;
        index = 0;
        while (!lastLine && index < maxinx) {
            int first = !index;
            retlin_(&first, &id, name, &lastLine, &status, MAX_LINE_NAME);
            if (status == OKAY) {
                name[MAX_LINE_NAME] = '\0';
                if (!strcmp(name, linename)) {
                    *linenum = id;
                    return;
                }
            } else {
                cout << "FAILURE: retlin_: Bad Project Definition File or "
                        << "Line Catalog is empty." << endl;
                lastLine = 1;
            }
            index++;
        }
    }
    *linenum = -1;
    return;
}

