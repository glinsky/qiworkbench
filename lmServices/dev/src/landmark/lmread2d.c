/*   Name        lmread2d.C
 **
 **   Function    reading Landmark 2D line and export trace data (in SU format) to stdout.
 **
 **
 */

#include <memory.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <iostream.h>
#include <fstream.h>
#include "sdlprototypes.h"
#include <string>
//#include "Util.h"
#include "su.h"
#include "segy.h"

using namespace std;
void open_read_file(dm3dfc_record*, int*, int, char*);
void read_tracedata(int ipdflg, dm3dfc_record* read_fcrec, int* read_ndxbuf, FILE *outf, string *inline_pars, string *crossline_pars);
void read_2Dtracedata(dm3dfc_record* read_fcrec, int* read_ndxbuf, int iline, int iymin, int iymax, int y_inc, int tmin, int tmax);
void put_trace(int ipdflg, dm3dfc_record* read_fcrec, int n, int iline, int itrace, void *trace, int* read_ndxbuf, int linenum, int minSampleIndex, int maxSampleIndex);
void close_proj(int type);
void get_default_inc(int ipdflg, int minx, int miny, int major, int *outlinc, int *outtrinc);
int get_default_shotpoint_inc(int lineid, int y, float *outsninc);
void printTrace(segy *tr);
void printTraceSamples(int n, segy *tr);
void printTraceHeader(int _ctr, segy *tr);
#define PROJ_NAME_LEN 17
#define LINE_NAME_LEN 31
#define NO_OF_BUF_IDX 32767
#define FILE_NAME_LEN 256
#define PROVER_LEN    10
#define NO_OF_BUF_IDX 32767
#define T(x,y,z)  z >= 0 ? x <= y : x >= y
extern "C" {
    void getp23_(int *prjtyp, int *mdflag);
}

const int MAX_FILE_NAME = 36;
const char* suffix[] = {"01.2v2", "01.3dv", ".bri", ".cmp"};

enum FileTypes {
    STD_2D, STD_3D, BRICK, COMPRESS
};
segy tr;
segy tr1;
char *sdoc[] = {
    "",
    " LMREAD2D - read a LANDMARK 2D line (a 2v2_glb file)",
    "",
    "   lmread2d > stdout lm_project= process_level= line_name=",
    "",
    " Required parameter:",
    " lm_project=Landmark 2D project name",
    " process_level=Landmark input process level and version string",
    " line_name=line name",
    "                                   ",
    " Optional parameters:  (default is based on project actual extent)	",
    " shotpoints=min_shotpoint:max_shotpoint:shotpoint_inc 		",
    " tmin=min_time 		",
    " tmax=max_time 		",
    "									",
    "",
    NULL
};

int main(int argc, char **argv) {
    char *prjname;
    char *readfileNam;
    char *lname;
    //char   lname[LINE_NAME_LEN];
    char readfileName[FILE_NAME_LEN + 1];
    char read_prover[PROVER_LEN];
    int mininy, maxiny, y_inc = 1;
    char *iline_pars;
    char *xline_pars;
    int ier, ipdflg = 2;
    int inputType;
    int read_ndxbuf[NO_OF_BUF_IDX];
    struct dm3dfc_record read_fcrec;
    initargs(argc, argv);
    requestdoc(0); /* stdin not used */
    /* Make sure stdout is a file or pipe */
    switch (filestat(STDOUT)) {
        case TTY:
            err("stdout can't be tty");
            break;
        case DIRECTORY:
            err("stdout must be a file, not a directory");
            break;
        case BADFILETYPE:
            err("stdout is illegal filetype");
            break;
        default: /* Others OK */
            break;
    }
    /* Set filenames */
    MUSTGETPARSTRING("lm_project", &prjname);
    MUSTGETPARSTRING("process_level", &readfileNam);
    MUSTGETPARSTRING("line_name", &lname);
    if (!getparstring("shotpoints", &xline_pars)) xline_pars = "unknown";
    int tmin = -1, tmax = -1;
    if (!getparint("tmin", &tmin)) tmin = -1;
    if (!getparint("tmax", &tmax)) tmax = -1;
    string crossline_pars(xline_pars);
    string inline_pars(iline_pars);
    string inFileName(readfileNam);


    string projectName(prjname);
    fprintf(stderr, "lm_project = %s\n", projectName.c_str());
    fprintf(stderr, "process_level %s\n", inFileName.c_str());
    fprintf(stderr, "line_name = %s\n", lname);
    fprintf(stderr, "shotpoints = %s\n", crossline_pars.c_str());
    fprintf(stderr, "tmin = %d\n", tmin);
    fprintf(stderr, "tmax = %d\n", tmax);

    /*
     **  Initialize the project, user will be prompted for the project name
     **  and all drives for the project will be logged. The user will be
     **  prompted if it is a 2-d or 3-d project. The project information
     **  from the .pds or .ps2 file will be loaded. The project name will
     **  be returned.
     */
    prjin2_(&ier, &ipdflg, (char*) projectName.c_str(), projectName.length());

    if (ier != 0) {
        fprintf(stderr, "prjin2 error %d\n", ier);
        if (ier == 1) {
            fprintf(stderr, "Error: Project Definition file not found (.pds or .ps2)\n");
        } else if (ier == 2) {
            fprintf(stderr, "Error: Unable to log project on all drives\n");
        } else if (ier == 4) {
            fprintf(stderr, "Error: Could not obtain an OpenWorks license\n");
        } else {
            fprintf(stderr, "Unknown error in opening project. \n");
        }

        close_proj(ipdflg);
        return ier;
    }


    FILE* outf = NULL;
    int ival30 = 30;
    int lineId = 1;
    if (ipdflg == 2) {
        pumknm_(&ival30, lname, lname, &ier);
        lnotst_(&lineId, lname, &ier);
        if (ier) {
            err("No line found with given name: %s\n", lname);
        }

        fprintf(stderr, "lineId = %d\n", lineId);
        int idum, maxlin;
        pjlnsz_(&idum, &maxlin, &ier);
        fprintf(stderr, "total number of lines: idum = %d\n", idum);
        fprintf(stderr, "active lines: maxlin = %d\n", maxlin);
        strcpy(read_prover, readfileNam);
        // -------- Initialize File Control Record (all zero).
        //(void) memset( (void*)&read_fcrec, 0, sizeof( read_fcrec ) );

        // build the seismic file name for this line
        lnmtfn_(lname, read_prover, readfileName, &ier, LINE_NAME_LEN, PROVER_LEN, FILE_NAME_LEN);
        fprintf(stderr, "2D global seismic file name=%s\n", readfileName);

        inFileName = readfileName;
        // get the trace range for the line
        lny1y2_(&lineId, &mininy, &maxiny, &ier);


        if (strcmp(xline_pars, "unknown")) {
            string strmin;
            string strmax;
            string strinc;
            int pos = crossline_pars.find(":", 0);
            if (pos != -1) {
                strmin = crossline_pars.substr(0, pos);
                int pos2 = crossline_pars.find(":", pos + 1);
                if (pos2 != -1) {
                    strmax = crossline_pars.substr(pos + 1, pos2 - pos - 1);
                    strinc = crossline_pars.substr(pos2 + 1, crossline_pars.length() - pos2 - 1);
                }
            }
            float spnt_min = atof(strmin.c_str());
            float spnt_max = atof(strmax.c_str());
            float spnt_inc = atof(strinc.c_str());
            float default_sn_inc;
            if (!get_default_shotpoint_inc(lineId, mininy, &default_sn_inc))
                err("Error in getting default shotpoint increase\n");
            fprintf(stderr, "default_shotpoint_increase=%f\n", default_sn_inc);
            fprintf(stderr, "spnt_min=%f,spnt_max=%f,spnt_inc=%f\n", spnt_min, spnt_max, spnt_inc);
            /*  try to open the input file to see if it exists  */
            /*int mode = 1;
            dm3dop_(readfileName, (void*)&read_fcrec, &mode, &ier );
            if (ier)
            {
            printf(" dm3dop error : %d\n", ier);
                close_proj(ipdflg);
            }
             */
            //int lfnxrg = read_fcrec.lfnxrg;
            //dm3dco_( &lfnxrg, (void*)&read_fcrec, &ier );
            float mintrace;
            float mintrace_next;
            float maxtrace;
            int status;
            sptotr_(&lineId, &spnt_min, &mintrace, &status);
            if (status)
                err("Error: Memory allocation error or shotpoint/trace array is empty");
            spnt_min += spnt_inc;
            sptotr_(&lineId, &spnt_min, &mintrace_next, &status);
            if (status)
                err("Error: Memory allocation error or shotpoint/trace array is empty");

            sptotr_(&lineId, &spnt_max, &maxtrace, &status);
            if (status)
                err("Error: Memory allocation error or shotpoint/trace array is empty");

            fprintf(stderr, "mintrace=%f,mintrace_next=%f,maxtrace=%f\n", mintrace, mintrace_next, maxtrace);
            float rline = lineId;
            int minix, miniy, miniy_next, maxix, maxiy;
            mintrace = round(mintrace);
            cs2int_(&ipdflg, &rline, &mintrace, &minix, &miniy);
            fprintf(stderr, "minix=%d,miniy=%d\n", minix, miniy);
            mintrace_next = round(mintrace_next);
            cs2int_(&ipdflg, &rline, &mintrace_next, &minix, &miniy_next);
            fprintf(stderr, "minix=%d,miniy_next=%d\n", minix, miniy_next);

            maxtrace = round(maxtrace);
            cs2int_(&ipdflg, &rline, &maxtrace, &maxix, &maxiy);
            fprintf(stderr, "maxix=%d,maxiy=%d\n", maxix, maxiy);

            if (!(miniy >= mininy && maxiy <= maxiny))
                err("xline parameter is out of project extents (%d,%d)\n", mininy, maxiny);
            else {
                mininy = miniy;
                maxiny = maxiy;
            }
            y_inc = miniy_next - miniy;
        }
        fprintf(stderr, "mininy=%d, maxiny=%d, y_inc=%d\n", mininy, maxiny, y_inc);

    }
    /*
     *   Open Seismic file
     */
    open_read_file(&read_fcrec, read_ndxbuf, ipdflg, inFileName.c_str());
    fprintf(stderr, "read_fcrec.lfmnpx=%d, read_fcrec.lfmxpx=%d, read_fcrec.lfmnpy=%d, read_fcrec.lfmxpy=%d\n", read_fcrec.lfmnpx, read_fcrec.lfmxpx, read_fcrec.lfmnpy, read_fcrec.lfmxpy);
    fprintf(stderr, "read_fcrec.lfmnax=%d, read_fcrec.lfmxax=%d, read_fcrec.lfmnay=%d, read_fcrec.lfmxay=%d\n", read_fcrec.lfmnax, read_fcrec.lfmxax, read_fcrec.lfmnay, read_fcrec.lfmxay);

    /*
     ** Write out trace data into the just created file
     */

    read_2Dtracedata(&read_fcrec, read_ndxbuf, lineId, mininy, maxiny, y_inc, tmin, tmax);

    /*  close file  */

    dm3dcl_((void*) & read_fcrec, &ier, read_ndxbuf);
    if (ier) fprintf(stderr, " lmread dm3dcl ier: %d", ier);

    if (outf)
        fclose(outf);

    /*  close the project  */
    close_proj(ipdflg);
    return 0;
}

/*
 **
 ** This function will read data from Lammark 2D line, create SU traces and call SU function puttr(&tr)
 **
 */
void read_2Dtracedata(dm3dfc_record* read_fcrec, int* read_ndxbuf, int iline, int iymin, int iymax, int y_inc, int tmin, int tmax) {

    int ipdflg = 2;
    int iy;
    int ier;
    short *strace = NULL;
    long *itrace = NULL;
    char *ctrace = NULL;
    float *rtrace = NULL;
    int ival0 = 0;
    int iread = 1;
    int skip = 1;
    float dummy;

    // for 2d ix is alway equal to 1
    int ix = 1;
    int n = 0;
    // set sample index range  for this trace write
    iread = 1;
    int minSampleIndex, maxSampleIndex;
    if (tmin == -1)
        minSampleIndex = read_fcrec->lfmnpt;
    else {
        if (tmin < read_fcrec->rfmnpt)
            tmin = read_fcrec->rfmnpt;
        int sample_rate = read_fcrec->rfsamp;
        int rem = tmin % sample_rate;
        minSampleIndex = ((tmin - rem) / sample_rate) + 1;
    }
    if (tmax == -1)
        maxSampleIndex = read_fcrec->lfmxpt;
    else {
        if (tmax > read_fcrec->rfmxpt)
            tmax = read_fcrec->rfmxpt;
        int sample_rate = read_fcrec->rfsamp;
        int rem = tmax % sample_rate;
        maxSampleIndex = ((tmax - rem) / sample_rate) + 1;
    }
    fprintf(stderr, "lmread2d sample rate %f\n", read_fcrec->rfsamp);
    fprintf(stderr, "lmread2d min time value %f\n", read_fcrec->rfmnpt);
    fprintf(stderr, "lmread2d max time value %f\n", read_fcrec->rfmxpt);
    fprintf(stderr, "lmread2d min sample index %ld\n", read_fcrec->lfmnpt);
    fprintf(stderr, "lmread2d max sample index %ld\n", read_fcrec->lfmxpt);
    fprintf(stderr, "lmread2d adjusted sample index:\n");
    fprintf(stderr, "lmread2d minSampleIndex = %d\n", minSampleIndex);
    fprintf(stderr, "lmread2d maxSampleIndex = %d\n", maxSampleIndex);
    fprintf(stderr, "lmread2d read_fcrec->lffrmt = %d\n", read_fcrec->lffrmt);

    // allocate memory for one trace

    if (read_fcrec->lffrmt == 8)
        ctrace = new char[maxSampleIndex - minSampleIndex + 1];
    else if (read_fcrec->lffrmt == 16)
        strace = new short[maxSampleIndex - minSampleIndex + 1];
    else if (read_fcrec->lffrmt == 32)
        itrace = new long[maxSampleIndex - minSampleIndex + 1];
    else
        rtrace = new float[maxSampleIndex - minSampleIndex + 1];


    int nsamples = maxSampleIndex - minSampleIndex + 1;
    int targetFormat = 0;
    float *targetTr = new float[nsamples];

    // loop through through all the traces in the master grid
    fprintf(stderr, "lmread2d iymin = %d iymax = %d \n", iymin, iymax);
    for (iy = iymin; T(iy, iymax, y_inc); iy += y_inc) {
        if (n < 4) {
            fprintf(stderr, "lmread2d ix = %d, iy = %d\n", ix, iy);
        }
        if (read_fcrec->lffrmt == 8) {
            dm3dtr_((void*) read_fcrec, &ix, &iy, &minSampleIndex, &maxSampleIndex, &iread, &ival0, (void*) ctrace, (void*) & dummy, &ier, read_ndxbuf);
            if (ier > 2)
                err("dm3dtr_ Error at ix = %d iy = %d status = %d\n", ix, iy, ier);
            dm3dft_((void*) ctrace, &(read_fcrec->lffrmt), &skip, &nsamples, (void*) targetTr, &targetFormat);

            put_trace(ipdflg, read_fcrec, ++n, ix, iy, (void*) targetTr, read_ndxbuf, iline, minSampleIndex, maxSampleIndex);
        } else if (read_fcrec->lffrmt == 16) {
            dm3dtr_((void*) read_fcrec, &ix, &iy, &minSampleIndex, &maxSampleIndex, &iread, &ival0, (void*) strace, (void*) & dummy, &ier, read_ndxbuf);
            if (ier > 2)
                err("dm3dtr_ Error at ix = %d iy = %d status = %d\n", ix, iy, ier);
            dm3dft_((void*) strace, &(read_fcrec->lffrmt), &skip, &nsamples, (void*) targetTr, &targetFormat);

            put_trace(ipdflg, read_fcrec, ++n, ix, iy, (void*) targetTr, read_ndxbuf, iline, minSampleIndex, maxSampleIndex);
        } else if (read_fcrec->lffrmt == 32) {
            dm3dtr_((void*) read_fcrec, &ix, &iy, &minSampleIndex, &maxSampleIndex, &iread, &ival0, (void*) itrace, (void*) & dummy, &ier, read_ndxbuf);
            if (ier > 2)
                err("dm3dtr_ Error at ix = %d iy = %d status = %d\n", ix, iy, ier);
            dm3dft_((void*) itrace, &(read_fcrec->lffrmt), &skip, &nsamples, (void*) targetTr, &targetFormat);

            put_trace(ipdflg, read_fcrec, ++n, ix, iy, (void*) targetTr, read_ndxbuf, iline, minSampleIndex, maxSampleIndex);
        } else {
            dm3dtr_((void*) read_fcrec, &ix, &iy, &minSampleIndex, &maxSampleIndex, &iread, &ival0, (void*) rtrace, (void*) & dummy, &ier, read_ndxbuf);
            if (ier > 2)
                err("dm3dtr_ Error at ix = %d iy = %d status = %d\n", ix, iy, ier);
            put_trace(ipdflg, read_fcrec, ++n, ix, iy, (void*) rtrace, read_ndxbuf, iline, minSampleIndex, maxSampleIndex);
        }
    }
    if (rtrace != NULL)
        delete rtrace;
    if (itrace != NULL)
        delete itrace;
    if (strace != NULL)
        delete strace;
    if (ctrace != NULL)
        delete ctrace;
    if (targetTr != NULL)
        delete targetTr;

}

void put_trace(int ipdflg, dm3dfc_record* read_fcrec, int n, int iline, int itrace, void *trace, int* read_ndxbuf, int linenum, int minSampleIndex, int maxSampleIndex) {

    int numPoints = 1;

    double dwx, dwy;
    int iwx, iwy, ix, iy, ier;
    float rdumx, rdumy, dummy, rrline, rrtrace;
    if (ipdflg == 3) {
        rrline = iline;
        rrtrace = itrace;
        cs2int_(&ipdflg, &rrline, &rrtrace, &ix, &iy);
    } else if (ipdflg == 2) {
        ix = iline;
        iy = itrace;
    }

    if (ipdflg == 3) {
        rdumx = ix;
        rdumy = iy;

        intorw_(&numPoints, &rdumx, &rdumy, &dwx, &dwy, &dummy);
        iwx = (int) dwx;
        iwy = (int) dwy;
    }


    unsigned short nsamples = maxSampleIndex - minSampleIndex + 1;
    if (read_fcrec->lffrmt == 8)
        memset((void*) & tr, (int) '\0', nsamples * sizeof (char) + 240);
    else if (read_fcrec->lffrmt == 16)
        memset((void*) & tr, (int) '\0', nsamples * sizeof (short) + 240);
    else if (read_fcrec->lffrmt == 32)
        memset((void*) & tr, (int) '\0', nsamples * sizeof (long) + 240);
    else
        memset((void*) & tr, (int) '\0', nsamples * FSIZE + 240);

    memset((void*) & tr1, (int) '\0', nsamples * FSIZE + 240);
    tr.tracl = n;
    tr1.tracl = n;
    float fline, ftrace;
    if (ipdflg == 2) {

        cs2ano_(&ipdflg, &iline, &itrace, &fline, &ftrace);

        float spn;
        trtosp_(&linenum, &spn, &ftrace, &ier);
        if (ier) {
            err(" trtosp ier: %d\n", ier);
        }
        tr.d1 = spn;
        tr1.d1 = spn;
        double rx, ry;
        sptoxy_(&linenum, &spn, &rx, &ry, &ier);
        iwx = rx;
        iwy = ry;
        if (ier) {
            err(" sptoxy ier: %d\n", ier);
        }
        tr.fldr = linenum;
        tr.tracf = itrace;
        tr.ep = linenum;
        tr.cdp = itrace;
        tr1.fldr = linenum;
        tr1.tracf = itrace;
        tr1.ep = linenum;
        tr1.cdp = itrace;
    } else if (ipdflg == 3) {
        tr.fldr = iline;
        tr.tracf = itrace;
        tr.ep = iline;
        tr.cdp = itrace;
        tr1.fldr = iline;
        tr1.tracf = itrace;
        tr1.ep = iline;
        tr1.cdp = itrace;
    }
    tr.sx = iwx;
    tr.sy = iwy;
    //tr.gx = iwx;
    //tr.gy = iwy;
    tr1.sx = iwx;
    tr1.sy = iwy;

    float sample_rate = read_fcrec->rfsamp;
    unsigned short si = sample_rate * 1000;

    float fdelrt;
    short idelrt;
    dm3dst_(&minSampleIndex, &sample_rate, &fdelrt);
    idelrt = fdelrt;
    tr.ns = nsamples;
    tr.dt = si;
    tr.delrt = idelrt;
    tr1.ns = nsamples;
    tr1.dt = si;
    tr1.delrt = idelrt;
    memcpy((void *) tr1.data, (const void *) trace, nsamples * FSIZE);
    tr1.offset = abs(tr1.offset);
    puttr(&tr1);
}

int get_default_shotpoint_inc(int lineid, int y, float *outsninc) {
    float rline, rtrace;
    int ipdflg = 2, ier;
    cs2ano_(&ipdflg, &lineid, &y, &rline, &rtrace);
    int line = rline;
    float minspn;
    trtosp_(&line, &minspn, &rtrace, &ier);
    if (ier) {
        fprintf(stderr, " trtosp ier: %d\n", ier);
        return FALSE;
    }
    y = y + 1;
    cs2ano_(&ipdflg, &lineid, &y, &rline, &rtrace);
    line = rline;
    float spn;
    trtosp_(&line, &spn, &rtrace, &ier);
    if (ier) {
        fprintf(stderr, " trtosp ier: %d\n", ier);
        return FALSE;
    }
    *outsninc = spn - minspn;
    return TRUE;
}

void get_default_inc(int ipdflg, int minx, int miny, int major, int *outlinc, int *outtrinc) {
    int ix = minx;
    int iy = miny;
    float rline_min, rtrace_min, rline_next, rtrace_next;

    cs2ano_(&ipdflg, &ix, &iy, &rline_min, &rtrace_min);

    if (major == 1) { //line major
        iy++;
        cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
        *outtrinc = rtrace_next - rtrace_min;
        ix++;
        iy--;
        cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
        *outlinc = rline_next - rline_min;
    } else if (major = 2) { //trace major
        iy++;
        cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
        *outlinc = rline_next - rline_min;
        ix++;
        iy--;
        cs2ano_(&ipdflg, &ix, &iy, &rline_next, &rtrace_next);
        *outtrinc = rtrace_next - rtrace_min;
    }

}

void printTraceSamples(int n, segy *tr) {
    int i = 0;
    fprintf(stderr, "\ntrace %d samples\n", n);
    for (i = 0; i < tr->ns; i++) {
        fprintf(stderr, "%5.2f  ", tr->data[i]);
        if ((i + 1) % 10 == 0)
            fprintf(stderr, "\n");
    }
    fprintf(stderr, "\n");
}

void printTraceHeader(int _ctr, segy *tr) {
    //if(_f==NULL) return;

    fprintf(stderr, "trace # %d\n", _ctr);

    fprintf(stderr, "* Trace sequence number withing line:........%10d\n", tr->tracl);
    fprintf(stderr, "  Trace sequence number withing reel:........%10d\n", tr->tracr);
    fprintf(stderr, "* Original field record number :.............%10d\n", tr->fldr);
    fprintf(stderr, "* Trace number withing field record:.........%10d\n", tr->tracf);
    fprintf(stderr, "  Energy source point number:................%10d\n", tr->ep);
    fprintf(stderr, "  CDP ensemble  number :.....................%10d\n", tr->cdp);
    fprintf(stderr, "  Trace  number:.............................%10d\n", tr->cdpt);
    fprintf(stderr, "* Trace identification code:.................%10d\n", tr->trid);
    fprintf(stderr, "  Number of vertically summed traces:........%10d\n", tr->nvs);
    fprintf(stderr, "  Number of horizontally stacked traces:.....%10d\n", tr->nhs);
    fprintf(stderr, "  Data use ( 1-production, 2-test ):.........%10d\n", tr->duse);
    fprintf(stderr, "  Distance from source point to receiv. grp.:%10d\n", tr->offset);
    fprintf(stderr, "  Receiver group elevation:..................%10d\n", tr->gelev);
    fprintf(stderr, "  Surface elevation at source:...............%10d\n", tr->selev);
    fprintf(stderr, "  Surface depth below source:................%10d\n", tr->sdepth);
    fprintf(stderr, "  Datum elevation at reciever group:.........%10d\n", tr->gdel);
    fprintf(stderr, "  Datum elevation at source:.................%10d\n", tr->sdel);
    fprintf(stderr, "  Water depth at source:.....................%10d\n", tr->swdep);
    fprintf(stderr, "  Water depth at group:......................%10d\n", tr->gwdep);
    fprintf(stderr, "  Scaler to all elevations & depths:.........%10d\n", tr->scalel);
    fprintf(stderr, "  Scaler to all coordinates:.................%10d\n", tr->scalco);
    fprintf(stderr, "  Source X coordinate:.......................%10d\n", tr->sx);
    fprintf(stderr, "  Source Y coordinate:.......................%10d\n", tr->sy);
    fprintf(stderr, "  Group  X coordinate:.......................%10d\n", tr->gx);
    fprintf(stderr, "  Group  Y coordinate:.......................%10d\n", tr->gy);
    fprintf(stderr, "  Coordinate units (1-len.m/ft 2-sec.arc):...%10d\n", tr->counit);
    fprintf(stderr, "  Weathering velocity:.......................%10d\n", tr->wevel);
    fprintf(stderr, "  Subweathering velocity:....................%10d\n", tr->swevel);
    fprintf(stderr, "  Uphole time at source:.....................%10d\n", tr->sut);
    fprintf(stderr, "  Uphole time at group:......................%10d\n", tr->gut);
    fprintf(stderr, "  Source static correction:..................%10d\n", tr->sstat);
    fprintf(stderr, "  Group  static correction:..................%10d\n", tr->gstat);
    fprintf(stderr, "  Total static applied:......................%10d\n", tr->tstat);
    fprintf(stderr, "  Lag time A:................................%10d\n", tr->laga);
    fprintf(stderr, "  Lag time B:................................%10d\n", tr->lagb);
    fprintf(stderr, "  Delay Recording time:......................%10d\n", tr->delrt);
    fprintf(stderr, "  Mute time start:...........................%10d\n", tr->muts);
    fprintf(stderr, "  Mute time end:.............................%10d\n", tr->mute);
    fprintf(stderr, "* Number of samples in this trace:...........%10d\n", tr->ns);
    fprintf(stderr, "* Sample interval in ms for this trace:......%10d\n", tr->dt);
    fprintf(stderr, "  Gain type of field instruments:............%10d\n", tr->gain);
    fprintf(stderr, "  Insrument gain:............................%10d\n", tr->igc);
    fprintf(stderr, "  Insrument gain constant:...................%10d\n", tr->igi);
    fprintf(stderr, "  Corellated (1-yes / 2-no):.................%10d\n", tr->corr);
    fprintf(stderr, "  Sweep frequency at start:..................%10d\n", tr->sfs);
    fprintf(stderr, "  Sweep frequency at end:....................%10d\n", tr->sfe);
    fprintf(stderr, "  Sweep lenth in ms:.........................%10d\n", tr->slen);
    fprintf(stderr, "  Sweep type 1-lin,2-parabol,2-exp,4-ohter:..%10d\n", tr->styp);
    fprintf(stderr, "  Sweep trace taper length at start in ms:...%10d\n", tr->stas);
    fprintf(stderr, "  Sweep trace taper length at end   in ms:...%10d\n", tr->stae);
    fprintf(stderr, "  Taper type 1-lin,2-cos2,3-other:...........%10d\n", tr->tatyp);
    fprintf(stderr, "  Alias filter frequency, if used:...........%10d\n", tr->afilf);
    fprintf(stderr, "  Alias filter slope:........................%10d\n", tr->afils);
    fprintf(stderr, "  Low cut frequency,  if used :..............%10d\n", tr->lcf);
    fprintf(stderr, "  High cut frequency, if used: ..............%10d\n", tr->hcf);
    fprintf(stderr, "  Low cut slope:.............................%10d\n", tr->lcs);
    fprintf(stderr, "  High cut slope:............................%10d\n", tr->hcs);
    fprintf(stderr, "  Year data recorded:........................%10d\n", tr->year);
    fprintf(stderr, "  Day of year:...............................%10d\n", tr->day);
    fprintf(stderr, "  Hour of day:...............................%10d\n", tr->hour);
    fprintf(stderr, "  Minute of hour:............................%10d\n", tr->minute);
    fprintf(stderr, "  Second of minute:..........................%10d\n", tr->sec);
    fprintf(stderr, "  Time basis code 1-local,2-GMT,3-other:.....%10d\n", tr->timbas);
    fprintf(stderr, "  Trace weighting factor:....................%10d\n", tr->trwf);
    fprintf(stderr, "  Geophone group number of roll sw. pos. 1...%10d\n", tr->grnors);
    fprintf(stderr, "  Geophone group number of trace # 1:........%10d\n", tr->grnofr);
    fprintf(stderr, "  Geophine group number of last trace:.......%10d\n", tr->grnlof);
    fprintf(stderr, "  Gap size (total # of groups dropped):......%10d\n", tr->gaps);
    fprintf(stderr, "  Overtravel assoc w taper of beg/end line:..%10d\n", tr->otrav);
    fprintf(stderr, "\n");
}

void printTrace(segy *tr) {
    fprintf(stderr, "\n");
    fprintf(stderr, " tr->tracl = %d\n", tr->tracl);
    fprintf(stderr, " tr->tracl = %d\n", tr->tracl);
    fprintf(stderr, " tr->fldr = %d\n", tr->fldr);
    fprintf(stderr, " tr->tracf = %d\n", tr->tracf);
    fprintf(stderr, " tr->ep = %d\n", tr->ep);
    fprintf(stderr, " tr->cdp = %d\n", tr->cdp);
    fprintf(stderr, " tr->sx = %d\n", tr->sx);
    fprintf(stderr, " tr->sy = %d\n", tr->sy);
    fprintf(stderr, " tr->gx = %d\n", tr->gx);
    fprintf(stderr, " tr->gy = %d\n", tr->gy);
    fprintf(stderr, " tr->ns = %d\n", tr->ns);
    fprintf(stderr, " tr->dt = %d\n", tr->dt);
    fprintf(stderr, " tr->delrt = %d\n", tr->delrt);
}

void open_read_file(dm3dfc_record* fcrec, int* ndxbuf, int ipdflg, char* readfileName) {

    int mode, ier;

    /*  try to open the input file to see if it exists  */
    mode = 1;
    dm3dop_(readfileName, (void*) fcrec, &mode, &ier);
    fprintf(stderr, "readFileName=%s\n", readfileName);
    if (ier) {
        err(" dm3dop error : %d\n", ier);
    }

    if (ipdflg == 2)
        return;
    int lfnxrg = fcrec->lfnxrg;
    dm3dco_(&lfnxrg, (void*) fcrec, &ier);
    /*  validate .pds or .ps2 against .3dv file  */
    dm3dpf_((void*) fcrec, &ipdflg, &ier);
    if (ier) {
        err(" dm3dpf error (exec phase): %d\n", ier);
    }
}

void close_proj(int type) {
    int ier;
    prjend_(&type, &ier);
    if (ier)
        err(" prjend ier: %d\n", ier);
    return;
}


