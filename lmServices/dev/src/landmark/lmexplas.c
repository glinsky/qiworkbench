/* ***************************************************************************
        OpenWorks Log Curve Data Export Program
 * This program takes parameters and export a LAS file for every well.
 *************************************************************************** */
#include <iostream.h>
#include <fstream.h>
#include <unistd.h>
#include <X11/Intrinsic.h>
#include <Xm/Xm.h>
#include <Xm/MainW.h>
#include <Xm/Form.h>
#include <Xm/RowColumn.h>
#include <Xm/Label.h>
#include <Xm/Text.h>
#include <Xm/List.h>
#include <Xm/Separator.h>

#include <ow/OpenWorks.h>
#include <ow/prj.h>
#include <ow/gdincl/gdiWell.h>
#include <ow/gdincl/gdiLogCurve.h>
#include <ow/gdincl/gdiMeasSystem.h>
#include <ow/log.h>
#include <ow/lut.h>
#include <ow/owmisc.h>
#include <ow/ssm.h>
#include <idb/wellfile.h>
#include <sdlprototypes.h>
#include <ow/gdi.h>
#include <string>
#include <vector>
#include <su.h>
/* text block size for text widget space allocation */
#define TEXT_BLOCK 18384
const int MAX_OWDB_NAME = 20;
const int OKAY = 0;
using namespace std;
/* functions defined in this file */

static long InitOW(Display *dpy, int argc, char **argv, int type, char *seisProject, char *openWorksProject);
static void ExportLogCurves(char *openWorksProject, vector<string> *well_list, vector<string> *curve_list, vector<string> *depth_option, char *outDir);

static void CloseOW();
vector<string> tokenize(const string& str, const string& delimiters);
int exist(string s, vector<string> *v);
char *replace_str(char *str, char *orig, char *rep);
void genVersionInfo(char *messageString);
void genWellInfo(char *messageString, gdiWell_t *w, char *unitName, float minStartDepth,
        float maxStopDepth, float increment, float nullValue, char *external_wellname, char *company, char *log_date);
void genComments(char *messageString, char *openWorksProject,char *login);

char *sdoc[] = {
		"",
		" LMEXPLAS - export LANDMARK Log Curve data into LAS (version 2.0) format",
		"",
		"   lmexplas lm_project= lm_ptype= lm_wells= lm_curves=",
		"",
		" Required parameter:",
		" lm_project=Landmark SeisWorks project name",
		" lm_ptype=Landmark SeisWorks project type (either 2 (2D) or 3 (3D))",
		" lm_wells=List of landmark well UWIs delimited by a comma",
                " lm_curves=List of landmark well log curve names delimited by a comma",
		"                                   ",
		" Optional parameters:  ",
		" out_dir=target directory to export to	(default is the current directory)",
		"									",
		"",
		NULL};

int main(int argc, char **argv) {
    char err_buff[256];
    long ier = SUCCESSFUL;
    int ipdflg;
    char *seisProject;
    char *targetDir;
    char   *wells,*curves;
    initargs(argc, argv);
    requestdoc(1); /* stdin not used */
    MUSTGETPARSTRING("lm_project",  &seisProject);
    MUSTGETPARINT("lm_ptype",&ipdflg);
    MUSTGETPARSTRING("lm_wells",  &wells);
    MUSTGETPARSTRING("lm_curves",  &curves);
    if (!getparstring("out_dir", &targetDir))	targetDir = "";

    owiCheckoutRuntime(FALSE, (Widget) NULL, (char *) NULL,
            err_buff, 256, &ier);
    if (ier != SUCCESSFUL) {
        OHNOOO(&ier, "owiCheckoutRuntime: Problem checking out license");
        return (ier);
    }

    //char *seisProject = "ggt_tst";

    char openWorksProject[MAX_OWDB_NAME + 1];
    int type = 3;
    /* initialize OpenWorks and connect to the project*/
    if (InitOW(NULL /*XtDisplay(toplevel)*/, argc, argv, type, seisProject, openWorksProject)) {
        printf("Big Trouble\n");
        exit(0);
    }

    /* post the list of wells in the list widget */
    //char *curves = "DT,GR,HE,HED,HES,LM,NPHI,RES,SP,CONDWS,DRESWS,GRWS,ROPWS";
    //char *curves = "";
    //char *wells = "60818400130000,60817400030000";
    //char *outdir = "/data/qi/lilt9/qiWork/";
    //char *depthType = "3,8";
    char *depthType = "3";
    vector<string> wellList = tokenize(wells, ",");
    vector<string> curveList = tokenize(curves, ",");
    vector<string> depthOptionList = tokenize(depthType, ",");
    ExportLogCurves(openWorksProject, &wellList, &curveList, &depthOptionList, targetDir);
    int dummy;
    prjend_(&type, &dummy);
    CloseOW();

    return (0);
}

static long InitOW(Display *dpy, int argc, char **argv, int type, char *seisProject, char *OWproject) {
    long ier = SUCCESSFUL;

    /* initialize OpenWorks */
    ier = owinit();
    if (ier != SUCCESSFUL) {
        OHNOOO(&ier, "InitOW: owinit() failed");
        return (ier);
    }

    /* Open Project */

    // ------- Open the seismic project.
    int status;
    prjin2_(&status, &type, seisProject, strlen(seisProject));
    printf("status = %d\n", status);
    if (ier != OKAY) {
        if (status == 1)
            cout << "Error: Project Definition file not found (.pds or .ps2) for " << seisProject;
        else if (status == 2)
            cout << "Error: Unable to log project on all drives for " << seisProject;
        else if (status == 4)
            cout << "Error: Could not obtain an OpenWorks license for " << seisProject;
        else
            cout << "Error: Unknown error in calling prjin2_";
        int dummy;
        prjend_(&type, &dummy);
        return status;
    }
    // ------- Obtain the OpenWorks project for this seismic project.

    printf("seisProject = %s\n", seisProject);
    ier = getOWProjOfSeisProj(seisProject, OWproject);
    printf("ier = %d\n", ier);
    if (ier != OKAY) {
        cout << "FAILURE: getOwProjOfSeisProj: "
                << "Could not get OW project for seismic project " << seisProject;
        int dummy;
        prjend_(&type, &dummy);
        return ier;
    }
    printf("OWproject = %s\n", OWproject);
    //char *project = "MISSCYN";
    prjOpen(OWproject, PRJ_SHARED, &ier);
    if (ier != SUCCESSFUL) {
        printf("Open: prjOpen failure\n");
        return ier;
    }

    ier = gdiInit();
    if (ier != SUCCESSFUL) {
        OHNOOO(&ier, "InitOW: gdiInit() failed");
        return (ier);
    }

    /* Retrieve the current measurement system.  If one is not set,
           display the list of available measurement systems.  */
    gdiMeasurementSystem_t ms;
    gdiMeasurementSystem_t ms1;
    gdiGetProjectMeasSystem(&ms);
    gdiGetWorkingMeasSystem(&ms1);
    int cnt;
    gdiMeasurementSystem_t *set = NULL;
    gdiMeasurementSystem_t ms2;
    strcpy(ms2.name, "SPE Preferred Metric");
    ms2.id = 2;
    //gdiInitiateObject(MEASUREMENT_SYSTEM, 1, (void*) &ms2);
    //gdiReadSubset (MEASUREMENT_SYSTEM, (void*) &ms2, MEASUREMENT_SYSTEM, &cnt,(void**) &set);
    ier = gdiReadSet(MEASUREMENT_SYSTEM, &cnt, (void**) & set);
    if (ier != SUCCESSFUL) {
        OHNOOO(&ier, "ReadWells: error reading wells.");
        return 1;
    }

    char *unitList = untGetUnitList(UNT_FEET, &ier);
    printf(" unitlist %s \n", unitList);
    //char *typelist = untGetTypeList(&ier);
    //while (typelist){
    //printf(" typelist %s \n", typelist);
    //printf(" MEASUREMENT_SYSTEM name %s \n", set[i].name);
    //}
    //char *unitname = untGetUnitName(UNT_FEET,&ier);
    //printf(" unitname %s \n", unitname);
    //printf("cnt=%d\n",cnt);
    //for (int i=0; i<cnt; i++){
    //printf(" MEASUREMENT_SYSTEM id %d \n", set[i].id);
    //printf(" MEASUREMENT_SYSTEM name %s \n", set[i].name);
    //}
    printf("ms2.id=%d\n", ms2.id);
    printf("ms2.name=%s\n", ms2.name);
    //gdiSetWorkingMeasSystem(&ms2);
    gdiGetWorkingMeasSystem(&ms1);
    printf("ms1.id=%d\n", ms1.id);
    printf("ms1.name=%s\n", ms1.name);

    ///SetStatusMessage((char*) "Initializing Complete...");
    return (SUCCESSFUL);
}

static void CloseOW() {
    long ier = SUCCESSFUL;

    /* close the OpenWorks Project Connection */
    prjClose(&ier);
    if (ier != SUCCESSFUL) {
        OHNOOO(&ier, "CloseOW: Problem closing the database");
        return;
    }

    /* close the pointing dispatcher connection */
    xpdClose(&ier);
    if (ier != SUCCESSFUL) {
        OHNOOO(&ier, "CloseOW: Problem closing PD");
        return;
    }

}

static void ExportLogCurves(char *openWorksProject, vector<string> *well_list, vector<string> *curveList, vector<string> *depthOptionList, char *outDir) {
    long ier = SUCCESSFUL;
    int well_count = 0;

    gdiWell_t *well = NULL;

    /* get ALL the wells in the database*/
    ier = gdiReadSet(WELL, &well_count, (void**) & well);
    if (ier != SUCCESSFUL) {
        OHNOOO(&ier, "ReadWells: error reading wells.");
        return;
    }

    /* make enough space for an array of wellids */
    //if (wellid == NULL)
    //    wellid = (wellID *) calloc(well_count, sizeof (wellID));
    //else
    //    wellid = (wellID *) realloc((char *) wellid, sizeof (wellID) * well_count);
    char buf[2];
    Bool tvd = False;
    Bool tvdss = False;
    char *login = getlogin();
    sprintf(buf, "%d", GDI_TVD);
    if (exist(buf, depthOptionList)) {
        tvd = True;
    }
    sprintf(buf, "%d", GDI_TVDSS);
    if (exist(buf, depthOptionList)) {
        tvdss = True;
    }

    for (int i = 0; i < well_count; i++) {
        //strcpy(wellid[i], well[i].wellid);
        for (int jj = 0; jj < well_list->size(); jj++) {
            /* make a curve to start with */
            if (!strcmp(well[i].uwi, well_list->at(jj).c_str())) {
                gdiWellEntire_t wellEntire;
                gdiInitiateObject(WELL_ENTIRE, 1, (void*) & wellEntire);
                strcpy(wellEntire.wellid, well[i].wellid);
                ier = gdiRead(WELL_ENTIRE, (void*) & wellEntire);
                if (ier != SUCCESSFUL) {
                    OHNOOO(&ier, "ListSelectCB: error reading well_entire instance.");
                    return;
                }
                printf("wellid %s\n", wellEntire.wellid);
                printf("common_well_name %s\n", wellEntire.common_well_name);
                printf("well location id %d\n", wellEntire.well_location_id);

                gdiWell_t w1;
                gdiWell_t *w;
                gdiInitiateObject(WELL, 1, (void*) & w1);
                strcpy(w1.wellid, well[i].wellid);
                /* get the data for the selected well */
                ier = gdiRead(WELL, (void*) & w1);
                w = &w1;
                if (ier != SUCCESSFUL) {
                    OHNOOO(&ier, "ListSelectCB: error reading well instance.");
                    return;
                }

                gdiLoggingJob_t *jobs;
                int loggingJobCount = 0;
                Bool logError = False;
                char sql1[80] = "well_id = ";
                strcat(sql1, w1.wellid);
                strcat(sql1, " ORDER BY logging_job_no");
                /* get the curves for the wells */
                ier = gdiReadSetWithClause(LOGGING_JOB, sql1, &loggingJobCount,
                        (void **) & jobs);
                if (ier != SUCCESSFUL) {
                    OHNOOO(&ier, "ListSelectCB: error reading subset.");
                    logError = True;
                }
                printf("logging job count = %d\n", loggingJobCount);
                char *company = "";
                char *log_date = "";
                for (int l = 0; !logError && l < loggingJobCount; l++) {
                    printf("job number = %d\n", jobs[l].logging_job_no);
                    printf("logging company %s\n", jobs[l].company);
                    company = jobs[l].company;
                    printf("job date %s\n", jobs[l].job_date);
                    log_date = jobs[l].job_date;
                    break;
                }

                if (strlen(company) == 0) {
                    company = "UNKNOWN";
                }

                if (strlen(log_date) == 0) {
                    log_date = "UNKNOWN";
                }


                Bool curveError = False;

                LogCrv_t *curves = logCreateCurve(1, &ier);
                if (ier != SUCCESSFUL) {
                    OHNOOO(&ier, "WellListSelectCB: logCreateCurve() failed");
                    printf("return code=%d WellListSelectCB: logCreateCurve() failed", ier);
                    curveError = True;
                    return;
                }

                // get the well name from the list
                strcpy(curves->wellid, well[i].wellid);

                // get the log headers for the well
                int curve_count = logReadHeader(&curves, &ier);
                if (ier != SUCCESSFUL && ier != GDINOINSTANCE) {
                    OHNOOO(&ier, "WellListSelectCB: logReadHeader() failed");
                    printf("return code=%d WellListSelectCB: logReadHeader() failed", ier);
                    curveError = True;
                    return;
                }

                vector<LogCrv_t> logCrvList;
                for (int w = 0; w < curve_count; w++) {
                    string name(curves[w].log_crv_name);
                    if (exist(name, curveList))
                        logCrvList.push_back(curves[w]);
                }

                // convert the curvenames to XmStrings
                char namestring[100];
                char tmpstr[500];
                float minStartDepth, maxStopDepth, increment;
                long maxNumberSamples;
                float nullValue;
                char *external_wellname;
                if (logCrvList.size() == 0) {
                    strcpy(namestring, "No Curves");
                    printf("%s for well %s with UWI=%s,\n", namestring, well[i].common_well_name, well[i].uwi);
                    break;

                } else {
                    minStartDepth = logCrvList.at(0).top_depth;
                    maxStopDepth = logCrvList.at(0).base_depth;
                    increment = logCrvList.at(0).depth_increment;
                    nullValue = logCrvList.at(0).null_value;
                    maxNumberSamples = logCrvList.at(0).total_samples;
                    external_wellname = logCrvList.at(0).external_well_name;
                    for (int p = 1; !curveError && p < logCrvList.size(); p++) {
                        if (logCrvList.at(p).top_depth < minStartDepth)
                            minStartDepth = logCrvList.at(p).top_depth;
                        if (logCrvList.at(p).base_depth > maxStopDepth)
                            maxStopDepth = logCrvList.at(p).base_depth;
                        if (logCrvList.at(p).total_samples > maxNumberSamples)
                            maxNumberSamples = logCrvList.at(p).total_samples;
                        if (logCrvList.at(p).depth_increment < increment)
                            increment = logCrvList.at(p).depth_increment;
                    }
                }

                string fileName(outDir);
                if(fileName.size() == 0)
                    fileName =  "./";
		else
                    fileName.operator +=("/");
                char *well_name = replace_str(well[i].common_well_name, " ", "_");
                printf("well_name=%s\n", well_name);
                fileName.operator +=(well_name);
                fileName.operator +=(".las");
                ofstream oFile(fileName.c_str(), ios::out);

                char *messageString = (char *) calloc(1, TEXT_BLOCK);
                char unitName[4] = "FT";
                genComments(messageString, openWorksProject,login);
                oFile << messageString;
                genVersionInfo(messageString);
                oFile << messageString;

                genWellInfo(messageString, w, unitName, minStartDepth,
                    maxStopDepth, increment, nullValue, external_wellname, company, log_date);
                oFile << messageString;
                
                strcpy(messageString, "~CURVE INFORMATION\n");
                strcat(messageString, "#MNEMONIC.UNIT        API CODE       : CURVE DESCRIPTION\n");
                strcat(messageString, "#------------------------------------------------------------------------------\n");
                strcat(messageString, "DEPTH    .FT                         : Measured Depth\n");
                if (tvd)
                    strcat(messageString, "TVD      .FT                         : True Vertical Depth (TVD)\n");
                if (tvdss)
                    strcat(messageString, "TVDSS    .FT                         : True Vertical Depth (TVDSS)\n");
                printf("%s", messageString);
                oFile << messageString;

                /* post the curve header data in the text widget */
                char namestring1[100];

                for (int o = 0; !curveError && o < logCrvList.size(); o++) {
                    sprintf(namestring1, "%s:%s:r%d:%s:v%d",
                            logCrvList.at(o).log_crv_name,
                            logCrvList.at(o).log_name,
                            logCrvList.at(o).log_run_no,
                            logCrvList.at(o).log_pass_id,
                            logCrvList.at(o).log_crv_version
                            );
                    sprintf(tmpstr, "%-9s.%-12s%-15s: %s\n", logCrvList.at(o).log_crv_name, logCrvList.at(o).value_working_uom, logCrvList.at(o).api_code, namestring1);
                    strcpy(messageString, tmpstr);
                    printf("%s", messageString);
                    oFile << messageString;
                }

                //printf(messageString);
                strcpy(messageString, "~A     DEPTH");

                if (tvd) {
                    sprintf(tmpstr, "%12s", "TVD");
                    strcat(messageString, tmpstr);
                }
                if (tvdss) {
                    sprintf(tmpstr, "%12s", "TVDSS");
                    strcat(messageString, tmpstr);
                }
                for (int m = 0; m < logCrvList.size(); m++) {
                    //LogCrv_t curve = logCrvList.at(m);
                    sprintf(tmpstr, "%13s", logCrvList.at(m).log_crv_name);
                    strcat(messageString, tmpstr);
                }
                strcat(messageString, "\n");
                printf("%s", messageString);
                oFile << messageString;
                for (int k = 0; k < logCrvList.size(); k++) {
                    /* post the curve header data in the text widget */
                    //LogCrv_t curve = curves[k];
                    /* read the curve data values */
                    logReadValues(&logCrvList.at(k), &ier);
                    //logReadValues(&curve, &ier);
                    if (ier != SUCCESSFUL) {
                        OHNOOO(&ier, "CurveListSelectCB: logReadValues() failed.");
                        curveError = True;
                        return;
                    }
                }
                if (!curveError) {
                    char tmpstr[80];

                    /* post the curve data in the text widget */
                    gdiMeasurementSystem_t ms;

                    gdiGetWorkingMeasSystem(&ms);
                    int unit = UNT_METERS; //default SPE Preferred Metric
                    if (ms.id == 1) //US Oil Field
                        unit = UNT_FEET;
                    //for (int j = 0; j < MIN(curve.total_samples, 100); j++) {
                    float mdepth;
                    float fval;
                    int* ii = (int *) malloc(logCrvList.size() * sizeof (int));
                    for (int iii = 0; iii < logCrvList.size(); iii++) {
                        ii[iii] = 0;
                    }

                    for (int j = 0; j < maxNumberSamples; j++) {
                        mdepth = minStartDepth + j*increment;
                        sprintf(tmpstr, "%12.4f", mdepth);
                        strcpy(messageString, tmpstr);
                        if (tvd) {
                            double depthOutTVD;
                            ier = gdiDepthConversion(curves->wellid, unit,
                                    unit, GDI_WBD, GDI_TVD, mdepth,
                                    &depthOutTVD);
                            if (ier != SUCCESSFUL) {
                                OHNOOO(&ier, "ExportLogCurves: gdiDepthConversion() failed");
                                return;
                            }
                            sprintf(tmpstr, "%12.4f", depthOutTVD);
                            strcat(messageString, tmpstr);
                        }
                        if (tvdss) {
                            double depthOutTVDSS;
                            ier = gdiDepthConversion(curves->wellid, unit,
                                    unit, GDI_WBD, GDI_TVDSS, mdepth,
                                    &depthOutTVDSS);
                            if (ier != SUCCESSFUL) {
                                OHNOOO(&ier, "ExportLogCurves: gdiDepthConversion() failed");
                                return;
                            }
                            sprintf(tmpstr, "%12.4f", depthOutTVDSS);
                            strcat(messageString, tmpstr);
                        }

                        for (int p = 0; p < logCrvList.size(); p++) {
                            if (mdepth >= logCrvList.at(p).top_depth && ii[p] < logCrvList.at(p).total_samples)
                                fval = logCrvList.at(p).values[ii[p]++];
                            else
                                fval = nullValue;
                            sprintf(tmpstr, "%13.4f", fval);
                            strcat(messageString, tmpstr);
                        }
                        strcat(messageString, "\n");
                        oFile << messageString;
                        //printf("%s", messageString);
                    }
                    if (ii)
                        free(ii);

                    /* post the sample count in the status widget */
                    //printf("%ld Samples Read\n", curve.total_samples);
                }

                if (messageString)
                    free(messageString);


                /* free the curve data space */
                logFreeCurve(curves, &ier);
                if (ier != SUCCESSFUL) {
                    OHNOOO(&ier, "WellListSelectCB: logFreeCurve() failed");
                    return;

                }
                printf("%s has been created.\n",fileName.c_str());
                if (oFile.is_open())
                    oFile.close();
                break;
            }

        }
    }

    /* post the well count in the label widget */
    //sprintf(message, "%d Wells Read", i);

    /* clean up the gdi memory allocated by the gdiReadSet call */
    if (well) free((char *) well);
}


void genVersionInfo(char *messageString){
    strcpy(messageString, "#------------------------------------------------------------------------------\n");
    strcat(messageString, "~VERSION INFORMATION\n");
    strcat(messageString, "VERS. 2.0 : CWLS Log ASCII Standard - version 2.0\n");
    strcat(messageString, "WRAP.  NO : One line per depth step\n");
}

void genWellInfo(char *messageString, gdiWell_t *w, char *unitName, float minStartDepth,
        float maxStopDepth, float increment, float nullValue, char *external_wellname, char *company, char *log_date){
    strcpy(messageString, "~WELL INFORMATION\n");
    strcat(messageString, "#MNEMONIC   .UNIT                                           VALUE :DESCRIPTION\n");
    strcat(messageString, "#------------------------------------------------------------------------------\n");
    strcat(messageString, "STRT        .");
    char tmpstr[120];
    sprintf(tmpstr, "%-4s%49.4f", unitName, minStartDepth);
    strcat(messageString, tmpstr);
    strcat(messageString, ":START DEPTH\n");

    strcat(messageString, "STOP        .");
    sprintf(tmpstr, "%-4s%49.4f", unitName, maxStopDepth);
    strcat(messageString, tmpstr);
    strcat(messageString, ":STOP DEPTH\n");

    strcat(messageString, "STEP        .");
    sprintf(tmpstr, "%-4s%49.4f", unitName, increment);
    strcat(messageString, tmpstr);
    strcat(messageString, ":STEP\n");

    strcat(messageString, "NULL        .");
    sprintf(tmpstr, "%53.4f", nullValue);
    strcat(messageString, tmpstr);
    strcat(messageString, ":NULL VALUE\n");

    strcat(messageString, "COMP        .");
    sprintf(tmpstr, "%53s", w->well_operator);
    strcat(messageString, tmpstr);
    strcat(messageString, ":COMPANY\n");

    //double check
    strcat(messageString, "WELL        .");
    sprintf(tmpstr, "%53s", external_wellname);
    strcat(messageString, tmpstr);
    strcat(messageString, ":WELL\n");

    strcat(messageString, "FLD         .");
    sprintf(tmpstr, "%53s", w->field);
    strcat(messageString, tmpstr);
    strcat(messageString, ":FIELD\n");


    //double check
    strcat(messageString, "LOC         .");
    sprintf(tmpstr, "%53s", w->current_location);
    strcat(messageString, tmpstr);
    strcat(messageString, ":LOCATION\n");

    strcat(messageString, "CNTY        .");
    sprintf(tmpstr, "%53s", w->county);
    strcat(messageString, tmpstr);
    strcat(messageString, ":COUNTY\n");

    strcat(messageString, "STAT        .");
    sprintf(tmpstr, "%53s", w->state);
    strcat(messageString, tmpstr);
    strcat(messageString, ":STATE\n");

    strcat(messageString, "CTRY        .");
    sprintf(tmpstr, "%53s", w->country);
    strcat(messageString, tmpstr);
    strcat(messageString, ":COUNTRY\n");

    //???
    strcat(messageString, "SRVC        .");
    sprintf(tmpstr, "%53s", company);
    strcat(messageString, tmpstr);
    strcat(messageString, ":SERVICE COMPANY\n");

    //????
    strcat(messageString, "DATE        .");
    sprintf(tmpstr, "%53s", log_date);
    strcat(messageString, tmpstr);
    strcat(messageString, ":LOG DATE\n");

    strcat(messageString, "UWI         .");
    sprintf(tmpstr, "%53s", w->uwi);
    strcat(messageString, tmpstr);
    strcat(messageString, ":UNIQUE WELL ID\n");

    strcat(messageString, "XCOORD      .");
    sprintf(tmpstr, "%53.6f", w->x_coordinate);
    strcat(messageString, tmpstr);
    strcat(messageString, ":SURFACE X\n");

    strcat(messageString, "YCOORD      .");
    sprintf(tmpstr, "%53.6f", w->y_coordinate);
    strcat(messageString, tmpstr);
    strcat(messageString, ":SURFACE Y\n");

    strcat(messageString, "LAT         .");
    sprintf(tmpstr, "%53.6f", w->latitude);
    strcat(messageString, tmpstr);
    strcat(messageString, ":LATITUDE\n");

    strcat(messageString, "LON         .");
    sprintf(tmpstr, "%53.6f", w->longitude);
    strcat(messageString, tmpstr);
    strcat(messageString, ":LONGITUDE\n");

    strcat(messageString, "ELEV        .");
    sprintf(tmpstr, "%-4s%49.4f", unitName, w->elevation);
    strcat(messageString, tmpstr);
    strcat(messageString, ":SURFACE ELEV\n");

    strcat(messageString, "ELEV_TYPE   .");
    sprintf(tmpstr, "%53s", w->elev_type);
    strcat(messageString, tmpstr);
    strcat(messageString, ":ELEV TYPE\n");

}

void genComments(char *messageString, char *openWorksProject,char *login){
    strcpy(messageString, "#------------------------------------------------------------------------------\n");
    strcat(messageString, "#\n");
    strcat(messageString, "#   Created by  : lmexplas\n");
    struct tm *timeinfo;


    struct tm tm_gmt;
    struct tm tm_loc;
    time_t timestamp = time(NULL);
    gmtime_r(&timestamp, &tm_gmt);

    localtime_r((const time_t *) & timestamp, &tm_loc);
    //sprintf(tmpstr, "#   Created on  : %4d-%02d-%02d %2d:%2d:%2d\n", tm_loc.tm_year+1900, tm_loc.tm_mon+1,
    //                    tm_loc.tm_mday,tm_loc.tm_hour,tm_loc.tm_min,tm_loc.tm_sec);
    //timeinfo = localtime ( &rawtime );
    //timeinfo = getdate("%Y-%m-%d %H:%M:%S");
    char tmpstr[80];
    sprintf(tmpstr, "#   Created on  : %s", ctime(&timestamp));
    //free(timeinfo)        ;
    strcat(messageString, tmpstr);
    //strcat(messageString,"#   Created on  : 2009-02-26 11:38:12\n");
    strcat(messageString, "#   Project     : ");
    strcat(messageString, openWorksProject);
    strcat(messageString, "\n");

    sprintf(tmpstr, "#   User        : %s\n", login);
    strcat(messageString, tmpstr);
    strcat(messageString, "#\n");
}

vector<string> tokenize(const string& str, const string& delimiters) {
    vector<string> tokens;

    string::size_type lastPos = 0, pos = 0;
    int count = 0;

    if (str.length() < 1) return tokens;

    // skip delimiters at beginning.
    lastPos = str.find_first_not_of(delimiters, 0);

    if ((str.substr(0, lastPos - pos).length()) > 0) {
        count = str.substr(0, lastPos - pos).length();

        for (int i = 0; i < count; i++)
            tokens.push_back("");

        if (string::npos == lastPos)
            tokens.push_back("");
    }

    // find first "non-delimiter".
    pos = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos) {
        // found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));

        // skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);

        if ((string::npos != pos) && (str.substr(pos, lastPos - pos).length() > 1)) {
            count = str.substr(pos, lastPos - pos).length();

            for (int i = 0; i < count; i++)
                tokens.push_back("");
        }

        pos = str.find_first_of(delimiters, lastPos);
    }

    return tokens;
}

int exist(string s, vector<string> *v) {
    for (int i = 0; i < v->size(); i++) {
        if (v->at(i).compare(0, s.length(), s) == 0)
            return 1;
    }
    return 0;
}

char *replace_str(char *str, char *orig, char *rep) {
    static char buffer[4096];
    char *p;

    if (!(p = strstr(str, orig))) // Is 'orig' even in 'str'?
        return str;

    strncpy(buffer, str, p - str); // Copy characters from 'str' start to 'orig' st$
    buffer[p - str] = '\0';

    sprintf(buffer + (p - str), "%s%s", rep, p + strlen(orig));

    return buffer;
}