#!/bin/bash
#set -x
source ~/.bashrc
alias rm="rm -f"
alias cp=cp

# Parameters specified in the qiProject
# qiSpace containing one or more projects
export QISPACE="<QI_SPACE>"
# location of project relative to $QISPACE
export QIPROJECT="<QI_PROJECT_LOCATION>"
# location of datasets relative to $QISPACE/$QIPROJECT
export QIDATASETS="<QI_DATASETS>"
# location of horizons relative to $QISPACE/$QIPROJECT
export QIHORIZONS="<QI_HORIZONS>"
# list of seismic directories separated by a line separator (\n)
#export QISEISMICS=$QISPACE/$QIPROJECT/seismic
export QISEISMICS="<QI_SEISMICS>"

# Parameters specified in the WaveletDecomp GUI
# path of input dataset file relative to $QIDATASETS; file name has no suffix for .dat is assumed
export IN_DATASET="<IN_DATASET>"
# path of output dataset file relative to $QIDATASETS; file name has no suffix for .dat is assumed
export OUT_DATASET="<OUT_DATASET>"
OUT_DATASET_DIR="<OUT_DATASET_DIR>"

#parse in Java of OUT_DATASET (remove the path)
export OUT_DATASET_FILENAME="<OUT_DATASET_FILENAME>"
export IN_DATASET_FILENAME="<IN_DATASET_FILENAME>"

project=$QISPACE/$QIPROJECT

input_type=<INPUT_TYPE>
horizon_type=<HORIZON_TYPE>
output_type=<OUTPUT_TYPE>

lm_in_project=<LANDMARK_IN_PROJECT>
lm_out_project=<LANDMARK_OUT_PROJECT>
lm_in_project_type=<LANDMARK_IN_PROJECT_TYPE>
lm_out_project_type=<LANDMARK_OUT_PROJECT_TYPE>
lm_in_volume=<LANDMARK_IN_VOLUME>
lm_in_volume_base=<LANDMARK_IN_VOLUME_BASE>
lm_2d_in_process_level=<LANDMARK_2D_IN_PROCESS_LEVEL>
lm_2d_out_process_level=<LANDMARK_2D_OUT_PROCESS_LEVEL>
#lm_out_volume=<LANDMARK_OUT_VOLUME>.bri
lm_min_line=<LANDMARK_MIN_LINE>
lm_max_line=<LANDMARK_MAX_LINE>
lm_inc_line=<LANDMARK_INC_LINE>
lm_min_trace=<LANDMARK_MIN_TRACE>
lm_max_trace=<LANDMARK_MAX_TRACE>
lm_inc_trace=<LANDMARK_INC_TRACE>
lm_min_time=<LANDMARK_MIN_TIME>
lm_max_time=<LANDMARK_MAX_TIME>
out_scale_list=<OUTPUT_SCALE_LIST>
mincdp=<MIN_CDP>
maxcdp=<MAX_CDP>
inccdp=<INC_CDP>
minep=<MIN_EP>
maxep=<MAX_EP>
incep=<INC_EP>

smallest_scale=<SMALLEST_SCALE>
largest_scale=<LARGEST_SCALE>
number_scales=<NUMBER_OF_SCALES>


horizon_files=<HORIZON_FILE_LIST>
lm_horizon_files=<LANDMARK_HORIZON_LIST>
#line_shotpoint_list=l_name1:l_num1:min_spn1:max_spn1:inc_spn1,l_name2:l_num2:min_spn2:max_spn2:inc_spn2,...
line_shotpoint_list=<LANDMARK_LINE_SHOTPOINT_LIST>
#line_trace_list=l_name1:l_num1:min_tr1:max_tr1:inc_tr1,l_name2:l_num2:min_tr2:max_tr2:inc_tr2,....
line_trace_list=<LANDMARK_LINE_TRACE_LIST>

tmin=<START_TIME>
horizon_times=<HORIZON_TIMES_LIST>
tmax=<END_TIME>
dt=<TIME_INCREMENT>
velocity=<AVERGE_VELOCITY>
gamma=<GAMMA_VALUE>
smoothing_length=<SMOOTHING_LENGTH>
number_pass=<NUMBER_OF_PASS>
run_normalization=<RUN_NORMALIZATION_FLAG>
run_slice=<RUN_SLICE_FLAG>
nx=${number_scales}
process_id=$$

if [ ${input_type} == "landmark2D" ]
then
    ii=0
    IFS=,
    for i in ${line_shotpoint_list}
    do
       line_shotpoint_array[${ii}]=$i
       ii=`expr ${ii} + 1`
    done

    kk=0
    for ((  jj = 0 ;  jj < ${#line_shotpoint_array[*]};  jj++  ))
    do
      IFS=:
      for k in ${line_shotpoint_array[${jj}]}
      do
        line_shotpoint_detail[${kk}]=$k
        kk=`expr ${kk} + 1`
      done
    done

    ii=0
    IFS=,
    for i in ${line_trace_list}
    do
       line_trace_array[${ii}]=$i
       ii=`expr ${ii} + 1`
    done

    kk=0
    for ((  jj = 0 ;  jj < ${#line_trace_array[*]};  jj++  ))
    do
      IFS=:
      for k in ${line_trace_array[${jj}]}
      do
        line_trace_detail[${kk}]=$k
        kk=`expr ${kk} + 1`
      done
    done

    jjj=0
    nnn=0
    for ((  jj = 0 ;  jj < ${#line_trace_detail[*]};  jj++  ))
    do
       jjj=`expr ${jj} % 5`
       if [ ${jjj} = 0 ]
       then
          linenum=`expr ${jj} + 1`
          mins=`expr ${jj} + 2`
          maxs=`expr ${jj} + 3`
          inc=`expr ${jj} + 4`
          line_name_array[${nnn}]=${line_shotpoint_detail[${jj}]}
          nnn=`expr ${nnn} + 1`
          echo "line name=${line_shotpoint_detail[${jj}]} linenum=${line_shotpoint_detail[${linenum}]} min_shotpoint=${line_shotpoint_detail[${mins}]}\
          max_shotpoint=${line_shotpoint_detail[${maxs}]} increment=${line_shotpoint_detail[${inc}]}"
          echo "line name=${line_trace_detail[${jj}]} linenum=${line_trace_detail[${linenum}]} min_trace=${line_trace_detail[${mins}]}\
          max_trace=${line_trace_detail[${maxs}]} increment=${line_trace_detail[${inc}]}"
       fi
    done
fi

if [ ${input_type} == "landmark3D" ]
then
   if [ ${lm_min_line} -gt ${lm_max_line} ]
   then
      minep=${lm_max_line}
      maxep=${lm_min_line}
      if [ ${lm_inc_line} -lt 0 ]
      then
         incep=`expr ${lm_inc_line} \* -1`
      fi
   else
      maxep=${lm_max_line}
      minep=${lm_min_line}
      incep=${lm_inc_line}
   fi

   if [ ${lm_min_trace} -gt ${lm_max_trace} ]
   then
      mincdp=${lm_max_trace}
      maxcdp=${lm_min_trace}
      if [ ${lm_inc_trace} -lt 0 ]
      then
         inccdp=`expr ${lm_inc_trace} \* -1`
      fi
   else
      maxcdp=${lm_max_trace}
      mincdp=${lm_min_trace}
      inccdp=${lm_inc_trace}
   fi
fi

if [ ${input_type} == "landmark3D" ] || [ ${input_type} == "bhpsu" ]
then
    ncdp=`expr ${maxcdp} - ${mincdp}`
    ncdp=`expr ${ncdp} / ${inccdp}`
    ncdp=`expr ${ncdp} + 1`

    nep=`expr ${maxep} - ${minep}`
    nep=`expr ${nep} / ${incep}`
    nep=`expr ${nep} + 1`
fi
increase=`echo ${largest_scale} - ${smallest_scale} | bc`
increase=`echo "scale=2; ${increase} / ${number_scales}" | bc`

sec=`echo "scale=1; ${tmin} / 1000" | bc`

mincdpevent=${mincdp}
maxcdpevent=${maxcdp}
minepevent=${minep}
maxepevent=${maxep}

echo "project=" ${project}
echo "lm_in_project=" ${lm_in_project}
echo "lm_out_project=" ${lm_out_projec}
echo "lm_in_project_type=" ${lm_in_project_type}
echo "lm_out_project_type=" ${lm_out_project_type}
echo "lm_in_volume=" ${lm_in_volume}
#echo "lm_out_volume=" ${lm_out_volume}
echo "lm_min_line=" ${lm_min_line}
echo "lm_max_line=" ${lm_max_line}
echo "lm_inc_line=" ${lm_inc_line}
echo "lm_min_trace=" ${lm_min_trace}
echo "lm_max_trace=" ${lm_max_trace}
echo "lm_inc_trace=" ${lm_inc_trace}
echo "lm_min_shotpoint=" ${lm_min_shotpoint}
echo "lm_max_shotpoint=" ${lm_max_shotpoint}
echo "lm_inc_shotpoint=" ${lm_inc_shotpoint}
echo "mincdp=" ${mincdp}
echo "maxcdp=" ${maxcdp}
echo "inccdp=" ${inccdp}
echo "minep=" ${minep}
echo "maxep=" ${maxep}
echo "incep=" ${incep}
echo "smallest_scale=" ${smallest_scale}
echo "largest_scale=" ${largest_scale}
echo "number_scales=" ${number_scales}
echo "out_scale_list=" ${out_scale_list}
echo "tmin=" ${tmin}
echo "horizon_files=" ${horizon_files}
echo "lm_horizon_files=" ${lm_horizon_files}
echo "horizon_times=" ${horizon_times}
echo "tmax=" ${tmax}
echo "dt=" ${dt}
echo "velocity=" ${velocity}
echo "smoothing_length=" ${smoothing_length}
echo "nx=" ${nx}
echo "run_normalization=" ${run_normalization}
echo "ncdp=" ${ncdp}
echo "nep=" ${nep}
echo "increase=" ${increase}
echo "sec=" ${sec}
echo "gamma=" ${gamma}
echo "horizon_type=" ${horizon_type}
echo "line_shotpoint_list=" ${line_shotpoint_list}
echo "line_trace_list=" ${line_trace_list}
echo "lm_2d_in_process_level=" ${lm_2d_in_process_level}
echo "lm_2d_out_process_level=" ${lm_2d_out_process_level}

dirname=`pwd`
echo "$0 running from ${dirname}"
cd $QISPACE/$QIPROJECT
echo "pwd=" `pwd`

if [ ! -d ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET_DIR} ]
   then
    mkdir -p ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET_DIR}
fi
if [ $? -ne 0 ]
then
echo "Error in creating ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET_DIR}"
echo "exit_status=$?"
exit $?
fi

#
# spec_decomp.sh
#
if [ ${input_type} == "landmark3D" ] || [ ${input_type} == "bhpsu" ]
then
    OUT_DATASET_FILENAME=${OUT_DATASET_FILENAME}_cwt
    OUT_DATASET=${OUT_DATASET}_cwt
    echo "------"
    echo "About to run spec_decomp.sh"
    echo "IN_DATASET =" ${IN_DATASET}
    echo "IN_DATASET_FILENAME =" ${IN_DATASET_FILENAME}
    echo "OUT_DATASET=" ${OUT_DATASET}
    echo "OUT_DATASET_FILENAME=" ${OUT_DATASET_FILENAME}
    echo "------"
fi

echo "$QISEISMICS" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat
bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}

#put this in temp directory

if [ ! -d ${QISPACE}/${QIPROJECT}/tmp ]
   then
    mkdir -p ${QISPACE}/${QIPROJECT}/tmp
fi
if [ $? -ne 0 ]
then
echo "Error in creating ${QISPACE}/${QIPROJECT}/tmp"
echo "exit_status=$?"
exit $?
fi

bhpwavelet > tmp/wavelet.su

#if [ ${input_type} == "landmark" ] && [ ${output_type} == "bhpsu" ]
if [ ${input_type} == "landmark3D" ]
then
    echo "$QISEISMICS" > $QISPACE/$QIPROJECT/$QIDATASETS/${lm_in_volume_base}.dat
    bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${lm_in_volume_base}.dat filename=${lm_in_volume_base}
    lmread lm_project=${lm_in_project} lm_ptype=${lm_in_project_type} lm_brickname=${lm_in_volume} \
    inline=${lm_min_line}:${lm_max_line}:${lm_inc_line} \
    xline=${lm_min_trace}:${lm_max_trace}:${lm_inc_trace} tmin=${lm_min_time} tmax=${lm_max_time} | \
    bhpwritecube init=yes key1=cdp,${mincdp},${inccdp},${ncdp} key2=ep,${minep},${incep},${nep}\
    pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${lm_in_volume_base}.dat filename=${lm_in_volume_base}

    lmread lm_project=${lm_in_project} lm_ptype=${lm_in_project_type} lm_brickname=${lm_in_volume} \
    inline=${lm_min_line}:${lm_max_line}:${lm_inc_line} \
    xline=${lm_min_trace}:${lm_max_trace}:${lm_inc_trace} tmin=${lm_min_time} tmax=${lm_max_time} \
    | bhpcwt velocity=${velocity} first=${smallest_scale} inc=${increase} last=${largest_scale} \
    wavelet=tmp/wavelet.su |
    bhpsmooth npass=${number_pass} length=${smoothing_length} |
    bhpwritecube init=yes key3=cdp,${mincdp},${inccdp},${ncdp} key1=ep,${minep},${incep},${nep} key2=cdpt,1,1,${number_scales} \
     pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}
elif [ ${input_type} == "landmark2D" ]
then
    jjj=0
    ind=0
    for ((  jj = 0 ;  jj < ${#line_shotpoint_detail[*]};  jj++  ))
    do
       jjj=`expr ${jj} % 5`
       if [ ${jjj} = 0 ]
       then
          linenum=`expr ${jj} + 1`
          mins=`expr ${jj} + 2`
          maxs=`expr ${jj} + 3`
          inc=`expr ${jj} + 4`

          if [ ${line_trace_detail[${mins}]} -gt ${line_trace_detail[${maxs}]} ]
          then
              mincdp=${line_trace_detail[${maxs}]}
              maxcdp=${line_trace_detail[${mins}]}
              if [ ${line_trace_detail[${inc}]} -lt 0 ]
              then
                  inccdp=`expr ${line_trace_detail[${inc}]} \* -1`
              fi
          else
              maxcdp=${line_trace_detail[${maxs}]}
              mincdp=${line_trace_detail[${mins}]}
              inccdp=${line_trace_detail[${inc}]}
          fi
          ncdp=`expr ${maxcdp} - ${mincdp}`
          ncdp=`expr ${ncdp} / ${inccdp}`
          ncdp=`expr ${ncdp} + 1`
          minep=${line_trace_detail[${linenum}]}
          incep=1
          nep=1

          out_dataset_filename=${OUT_DATASET_FILENAME}_${line_shotpoint_detail[${jj}]}_cwt
          out_dataset=${OUT_DATASET}_${line_shotpoint_detail[${jj}]}_cwt
          out_dataset_filename_array[${ind}]=${out_dataset_filename}
          out_dataset_array[${ind}]=${out_dataset}
          ind=`expr ${ind} + 1`
          echo "------"
          echo "About to run spec_decomp.sh"
          echo "IN_DATASET =" ${IN_DATASET}
          echo "IN_DATASET_FILENAME =" ${IN_DATASET_FILENAME}
          echo "OUT_DATASET=" ${out_dataset}
          echo "OUT_DATASET_FILENAME=" ${out_dataset_filename}
          echo "------"

          echo "$QISEISMICS" > $QISPACE/$QIPROJECT/$QIDATASETS/${out_dataset}.dat
          bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${out_dataset}.dat filename=${out_dataset_filename}

          lmread2d lm_project=${lm_in_project} process_level=${lm_2d_in_process_level}\
            line_name=${line_shotpoint_detail[${jj}]}\
            shotpoints=${line_shotpoint_detail[${mins}]}:${line_shotpoint_detail[${maxs}]}:${line_shotpoint_detail[${inc}]} |\
          bhpcwt velocity=${velocity} first=${smallest_scale} inc=${increase} last=${largest_scale} \
          wavelet=tmp/wavelet.su |
          bhpsmooth npass=${number_pass} length=${smoothing_length} |
          bhpwritecube init=yes key3=cdp,${mincdp},${inccdp},${ncdp} key1=ep,${minep},${incep},${nep} key2=cdpt,1,1,${number_scales} \
            filename=${output} pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${out_dataset}.dat filename=${out_dataset_filename}
       fi
    done
else
bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${IN_DATASET}.dat filename=${IN_DATASET_FILENAME} keys=ep,cdp keylist=${minep}-${maxep}[${incep}]:${mincdp}-${maxcdp}[${inccdp}] |
bhpcwt velocity=${velocity} first=${smallest_scale} inc=${increase} last=${largest_scale} wavelet=tmp/wavelet.su |
bhpsmooth npass=${number_pass} length=${smoothing_length} |
bhpwritecube init=yes key3=cdp,${mincdp},${inccdp},${ncdp} key1=ep,${minep},${incep},${nep} key2=cdpt,1,1,${number_scales} \
    pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}
if [ $? -ne 0 ]
then
echo "Error in running bhpread | bhpcwt | bhpsmooth | bhpwritecube"
echo "$0 job ends abnormally"
echo "exit_status=$?"
exit $?
fi
fi

rm tmp/wavelet.su

echo "end of spec_decomp.sh"


if [ ${run_normalization} = "yes" ]
then
echo "nx=" ${nx}
#
# normavg.sh
#
    if [ ${input_type} == "landmark3D" ] || [ ${input_type} == "bhpsu" ]
    then
        IN_DATASET_FILENAME=${OUT_DATASET_FILENAME}
        IN_DATASET=${OUT_DATASET}
        OUT_DATASET_FILENAME=${OUT_DATASET_FILENAME}_norm
        OUT_DATASET=${OUT_DATASET}_norm
        echo "------"
        echo "About to run normavg.sh"
        echo "IN_DATASET =" ${IN_DATASET}
        echo "IN_DATASET_FILENAME =" ${IN_DATASET_FILENAME}
        echo "OUT_DATASET=" ${OUT_DATASET}
        echo "OUT_DATASET_FILENAME=" ${OUT_DATASET_FILENAME}
        echo "------"

        echo "$QISEISMICS" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat
        bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}

        bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${IN_DATASET}.dat filename=${IN_DATASET_FILENAME} keys=ep,cdp,cdpt |
        #write this to a temporaary directory
        bhpavg nx=${nx} > tmp/avg_${IN_DATASET_FILENAME}_merge.su

        bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${IN_DATASET}.dat filename=${IN_DATASET_FILENAME} keys=ep,cdp,cdpt |
        bhpnorm gamma=${gamma} nx=${nx} avg=tmp/avg_${IN_DATASET_FILENAME}_merge.su |
        bhpwritecube init=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME} \
            key1=ep,${minep},${incep},${nep} key3=cdp,${mincdp},${inccdp},${ncdp} key2=cdpt,1,1,${number_scales}

        if [ $? -ne 0 ]
        then
        echo "Error in running bhpread | bhpnorm | bhpwritecube"
        echo "$0 job ends abnormally"
        echo "exit_status=$?"
        exit $?
        fi

        rm tmp/avg_${IN_DATASET_FILENAME}_merge.su
    elif [ ${input_type} == "landmark2D" ]
    then
        for ((  jj = 0 ;  jj < ${#out_dataset_filename_array[*]};  jj++  ))
        do
            IN_DATASET_FILENAME=${out_dataset_filename_array[${jj}]}
            IN_DATASET=${out_dataset_array[${jj}]}
            out_dataset_filename=${out_dataset_filename_array[${jj}]}_norm
            out_dataset=${out_dataset_array[${jj}]}_norm
            out_dataset_filename_array[${jj}]=${out_dataset_filename}
            out_dataset_array[${jj}]=${out_dataset}
            echo "------"
            echo "About to run normavg.sh"
            echo "IN_DATASET =" ${IN_DATASET}
            echo "IN_DATASET_FILENAME =" ${IN_DATASET_FILENAME}
            echo "OUT_DATASET=" ${out_dataset}
            echo "OUT_DATASET_FILENAME=" ${out_dataset_filename}
            echo "------"

            echo "$QISEISMICS" > $QISPACE/$QIPROJECT/$QIDATASETS/${out_dataset}.dat
            bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${out_dataset}.dat filename=${out_dataset_filename}

            bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${IN_DATASET}.dat filename=${IN_DATASET_FILENAME} keys=ep,cdp,cdpt |
            #write this to a temporary directory
            bhpavg nx=${nx} > tmp/avg_${IN_DATASET_FILENAME}_merge.su

            bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${IN_DATASET}.dat filename=${IN_DATASET_FILENAME} keys=ep,cdp,cdpt |
            bhpnorm gamma=${gamma} nx=${nx} avg=tmp/avg_${IN_DATASET_FILENAME}_merge.su |
            bhpwritecube init=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${out_dataset}.dat filename=${out_dataset_filename} \
                key1=ep,${minep},${incep},${nep} key3=cdp,${mincdp},${inccdp},${ncdp} key2=cdpt,1,1,${number_scales}

            rm tmp/avg_${IN_DATASET_FILENAME}_merge.su
        done
    fi
echo "end of normavg.sh"

fi

#
# slice_all.sh
#
if [ ${run_slice} = "yes" ]
then
    if [ ${horizon_type} == "landmark" ]
    then
       if [ ! -d ${QISPACE}/${QIPROJECT}/${QIHORIZONS} ]
       then
        mkdir ${QISPACE}/${QIPROJECT}/${QIHORIZONS}
       fi
       lmexphor lm_project=${lm_in_project} lm_ptype=${lm_in_project_type} \
           lm_horizons=${lm_horizon_files} minline=${lm_min_line} maxline=${lm_max_line} \
           mintrace=${lm_min_trace} maxtrace=${lm_max_trace} dir=${QISPACE}/${QIPROJECT}/${QIHORIZONS}
    fi

    if [ ${horizon_type} == "bhpsu" ]
    then
        ii=0
        IFS=,
        for i in ${horizon_files}
        do
            horizon_array[${ii}]=$i
            ii=`expr ${ii} + 1`
        done


        horizon_files="";
        horizon_count_1=`expr ${#horizon_array[*]} - 1`
        for ((  jj = 0 ;  jj < ${#horizon_array[*]};  jj++  ))
        do
            IFS=:
            kk=0
            for k in ${horizon_array[$jj]}
            do
                if [ ${kk} = 0 ]
                then
                    dataset=$k
                    echo "dataset=${dataset}"
                fi
                if [ ${kk} = 1 ]
                then
                    horizon_name=$k
                    echo "horizon_name=${horizon_name}"
                fi
                kk=`expr ${kk} + 1`
            done
            bhpread keys=ep,cdp filename=${dataset} pathlist=${QISPACE}/${QIPROJECT}/${QIDATASETS}/${dataset}.dat \
            horizons=${horizon_name} |\
            bhpstorehdr keys=ep,cdp horizons=yes file=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${horizon_name}.xyz | \
            surange

            exit_status=$?
            echo "exit_status=$exit_status"
            if [ ${exit_status} != 0 ]
            then
                echo "Error in running bhpread | bhpstorehdr | surange"
                echo "$0 job ends abnormally"
                exit 1
            fi
            horizon_files=${horizon_files}${horizon_name}.xyz
            if [ ${jj} -lt ${horizon_count_1} ]
            then
                horizon_files=${horizon_files},
            fi
        done
    fi

    IN_DATASET_FILENAME=${OUT_DATASET_FILENAME}
    IN_DATASET=${OUT_DATASET}
    OUT_DATASET_FILENAME=${OUT_DATASET_FILENAME}_slice
    OUT_DATASET=${OUT_DATASET}_slice
    echo "------"
    echo "About to run slice_all.sh"
    echo "IN_DATASET =" ${IN_DATASET}
    echo "IN_DATASET_FILENAME =" ${IN_DATASET_FILENAME}
    echo "OUT_DATASET=" ${OUT_DATASET}
    echo "OUT_DATASET_FILENAME=" ${OUT_DATASET_FILENAME}
    echo "------"


    echo "$QISEISMICS" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat
    bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}


    ii=0;
    IFS=,
    for i in ${horizon_files}
    do
    #echo $i
    file_array[${ii}]=$i
    ii=`expr ${ii} + 1`
    done

    for ((  jj = 0 ;  jj < ${#file_array[*]};  jj++  ))
    do
      echo "file_array ${file_array[$jj]}"
    done

    array=( f1 f2 d1 d2 ungpow unscale )
    for ((  j = 0 ;  j < ${#array[*]};  j++  ))
    do
      echo "float header ${array[$j]}"
    done

    if [ ${#array[*]} -ge ${#file_array[*]} ]
    then
      header_number=${#file_array[*]}
    else
      header_number=${#array[*]}
    fi

    echo "header_number = " ${header_number}
    header_number_minus1=`expr ${header_number} - 1`

    command="bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${IN_DATASET}.dat filename=${IN_DATASET_FILENAME} keys=ep,cdpt,cdp keylist=${minepevent}-${maxepevent}:*:${mincdpevent}-${maxcdpevent}"

    slice_cmd="| bhpslice hlist="
    key_cmd_base="| sushw key="
    key_cmd=""
    for ((  k = 0 ;  k < ${header_number};  k++  ))
    do
      if [ $k -eq 0 ]
      then
        key_cmd="${key_cmd}${key_cmd_base}${array[$k]} a=${sec} "
      else
        key_cmd="${key_cmd}${key_cmd_base}${array[$k]} a=0 "
      fi
    done

    for ((  k = 0 ;  k < ${header_number};  k++  ))
    do
      command=${command}" | bhploadhdr key=ep,cdp,${array[$k]} infile=${QIHORIZONS}/${file_array[$k]} gridtype=R extrap=no "
      slice_cmd=${slice_cmd}${array[$k]}
      if [ $k -lt ${header_number_minus1} ]
      then
        slice_cmd=${slice_cmd},
      fi
    done

    slice_cmd="${slice_cmd} times=${horizon_times} tmin=${tmin} tmax=${tmax} dt=${dt} "

    command="${command}${slice_cmd}${key_cmd} | bhpwritecube init=yes key3=cdp,${mincdp},${inccdp},${ncdp} key2=cdpt,1,1,${number_scales} key1=ep,${minep},${incep},${nep} \
            pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}"


    temp_script="`basename $0`.tmp"
    echo "generating temporary script: ${temp_script}"
    echo "#!/bin/sh" > ${dirname}/${temp_script}
    echo "cd `pwd`" >> ${dirname}/${temp_script}
    echo "${command}" >> ${dirname}/${temp_script}
    echo "${command}"
    chmod u+x ${dirname}/${temp_script}
    sh -c ${dirname}/${temp_script}
    if [ $? -eq 0 ]
    then
        echo "removing temporary script: ${temp_script}"
        rm ${dirname}/${temp_script}
    else
        echo "$0 job ends abnormally"
        exit 1
    fi
#${command}

    IN_DATASET_FILENAME=${OUT_DATASET_FILENAME}
    IN_DATASET=${OUT_DATASET}
    OUT_DATASET_FILENAME=${OUT_DATASET_FILENAME}_transpose_ep
    OUT_DATASET=${OUT_DATASET}_transpose_ep

    echo "------"
    echo "About to run bhptranspose"
    echo "IN_DATASET =" ${IN_DATASET}
    echo "IN_DATASET_FILENAME =" ${IN_DATASET_FILENAME}
    echo "OUT_DATASET=" ${OUT_DATASET}
    echo "OUT_DATASET_FILENAME=" ${OUT_DATASET_FILENAME}
    echo "------"

    #echo -e "$QISEISMICS" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}_transpose_cdp.dat
    echo "$QISEISMICS" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat
    #bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}_transpose_cdp.dat filename=${OUT_DATASET_FILENAME}_transpose_cdp
    bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}

    #bhptranspose pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}_slice_transpose_cdp.dat  filename=${OUT_DATASET_FILENAME}
    bhptranspose pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat  filename=${IN_DATASET_FILENAME}
    if [ $? -ne 0 ]
    then
    echo "Error in running bhptranspose"
    echo "$0 job ends abnormally"
    echo "exit_status=$?"
    exit $?
    fi
    echo "end of slice_all.sh for the spec decomp"

fi

if [ ${output_type} == "landmark3D" ]
then
    ii=0;
    IFS=,
    for i in ${out_scale_list}
    do
    scale_array[${ii}]=$i
    ii=`expr ${ii} + 1`
    done

    for ((  jj = 0 ;  jj < ${#scale_array[*]};  jj++  ))
    do
      echo "scale_array ${scale_array[${jj}]}"
      old_ow_pmpath=$OW_PMPATH
      export OW_PMPATH=$OW_PMPATH_DM
      echo "Before Landmark access OW_PMPATH=$OW_PMPATH"
      bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME} keys=ep,cdpt,cdp keylist=*:${scale_array[$jj]}:* |
      lmwrite lm_project=${lm_out_project} lm_ptype=${lm_out_project_type} lm_brickname=${OUT_DATASET_FILENAME}_${scale_array[$jj]}.bri
      export OW_PMPATH=$old_ow_pmpath
      echo "After Landmark access OW_PMPATH=$OW_PMPATH"
    done
elif [ ${output_type} == "landmark2D" ]
then

    ii=0;
    IFS=,
    for i in ${out_scale_list}
    do
    scale_array[${ii}]=$i
    ii=`expr ${ii} + 1`
    done

    linename_list="";
    line_name_array_size_minus_1=`expr ${#line_name_array[*]} - 1`
    echo "line_name_array_size_minus_1 = ${line_name_array_size_minus_1}"
    for ((  jj = 0 ;  jj < ${#line_name_array[*]};  jj++  ))
    do
        echo "line name=${line_name_array[${jj}]} "
        linename_list=${linename_list}${line_name_array[${jj}]}
        if [ ${jj} -lt ${line_name_array_size_minus_1} ]
        then
        linename_list=${linename_list},
        fi


    done
    echo "linename_list=${linename_list}"
    for ((  jj = 0 ;  jj < ${#scale_array[*]};  jj++  ))
    do
      echo "scale_array ${scale_array[$jj]}"
      for ((  kk = 0 ;  kk < ${#out_dataset_filename_array[*]};  kk++  ))
      do
          echo "out_dataset_filename_array[${kk}] = ${out_dataset_filename_array[${kk}]}"
          echo "scale_array[$jj] = ${scale_array[$jj]}"
          old_ow_pmpath=$OW_PMPATH
          export OW_PMPATH=$OW_PMPATH_DM
          echo "Before Landmark access OW_PMPATH=$OW_PMPATH"
          bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${out_dataset_array[${kk}]}.dat filename=${out_dataset_filename_array[${kk}]} keys=ep,cdpt,cdp keylist=*:${scale_array[$jj]}:* \
          | lmwrite2d lm_project=${lm_out_project} lm_ptype=${lm_out_project_type} lm_plevel=${lm_2d_out_process_level}_${scale_array[$jj]} lm_lines=${linename_list}
          export OW_PMPATH=$old_ow_pmpath
          echo "After Landmark access OW_PMPATH=$OW_PMPATH"
      done
    done
fi

exit_status=$?
echo "exit_status=$exit_status"
if [ ${exit_status} != 0 ]
then
    echo "$0 job ends abnormally"
    exit 1
fi
echo "$0 job ends normally"

