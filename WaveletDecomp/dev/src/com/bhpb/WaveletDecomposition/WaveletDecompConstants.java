/*
 ###########################################################################
 # WaveletDecomp - A continuous time-frequency analysis technique that  
 # computes frequency spectrum for each time sample of a seismic trace.
 # Copyright (C) 2006  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
*/


package com.bhpb.WaveletDecomposition;

import com.bhpb.qiworkbench.compAPI.QIWConstants;


/**
 * Constants used within Wavelet Decomposition
 *
 * @author Gil Hansen
 * @version 1.0
 */
public final class WaveletDecompConstants {
    /**
     * Prevent object construction outside of this class.
     */
    private WaveletDecompConstants() {}

    /** plugin's display name */
    public static final String SPEC_DECOMP_PLUGIN_NAME = "waveletDecomp";

    /** plugin GUI's display name */
    public static final String SPEC_DECOMP_GUI_NAME = "Wavelet Decomposition GUI";
    public static final String JNI_LIBS_LOCATION = System.getProperty("user.home") + "/" + QIWConstants.QIWB_WORKING_DIR + "/qiLib";
    // debug print: 0=none, 1=moderate, 2=verbose
    public static final int DEBUG_PRINT = 2;

    // integer status
    public static final int ERROR_STATUS = 1;
    public static final int OK_STATUS = 0;
    public static final int JOB_RUNNING_STATUS = 1;
    public static final int JOB_ENDED_STATUS = 2;
    public static final int JOB_UNKNOWN_STATUS = 3;
    public static final int JOB_UNSUBMITTED_STATUS = 0;
    // messages used for file chooser service
    public static final String SPEC_DECOMP_SELECT_INPUT = "Select Input";
    public static final String SPEC_DECOMP_SELECT_OUTPUT = "Select Output";
    public static final String SPEC_DECOMP_SELECT_TOP_HORIZON = "Select Top Horizon";
    public static final String SPEC_DECOMP_SELECT_BOTTOM_HORIZON = "Select Bottom Horizon";
    public static final String SPEC_DECOMP_SELECT_HORIZON = "Select Horizon";
    public static final String SPEC_DECOMP_SAVE_SCRIPT = "Save Script";
    public static final String SPEC_DECOMP_LOAD_SCRIPT = "Load Script";
    public static final String SPEC_DECOMP_EXECUTE_SCRIPT = "Execute Script";
    public static final String FILE_CHOOSER_CMD_GET_SEISMIC_DATASET = "FileChooserCmdGetSeismicDataset";
    public static final String FILE_CHOOSER_CMD_GET_HORIZON_DATA = "FileChooserCmdGetHorizonData";
    public static final String LANDMARK_JNI_LIB_NAME = "landmarkprojects";
    public static final String GET_HORIZON_BHPSU_DATASET_CMD = "getHorizonBHPSUDataset";

}
