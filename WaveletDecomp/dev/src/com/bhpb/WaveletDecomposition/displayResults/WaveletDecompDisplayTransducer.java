/*
 ###########################################################################
 # WaveletDecomp - A continuous time-frequency analysis technique that
 # computes frequency spectrum for each time sample of a seismic trace.
 # Copyright (C) 2008  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */

package com.bhpb.WaveletDecomposition.displayResults;

import java.lang.NumberFormatException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.ControlConstants;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

import com.bhpb.WaveletDecomposition.WaveletDecompPlugin;
import com.bhpb.WaveletDecomposition.WaveletDecompGUI;
import com.bhpb.WaveletDecomposition.displayResults.WaveletDecompDisplayFiles;
import com.bhpb.WaveletDecomposition.displayResults.WaveletDecompControlConstants;

/**
 * Finite state machine (FSM) for displaying the results of WaveletDecomp.
 */
public class WaveletDecompDisplayTransducer {
    /** Display step */
    private int stepState = ControlConstants.SESSION_STEP;
    /** Step action */
    private int actionState = ControlConstants.START_SESSION;
    /** Agent's messaging manager. */
    MessagingManager messagingMgr;
    // GUI component
    private WaveletDecompGUI gui;
    /** Component descriptor of the qiViewer being controlled */
    ComponentDescriptor qiViewerDesc;
    WaveletDecompPlugin agent;
    
    /** Indicator that session was terminated either due to an error in the transduced or the user cancelled displaying the results */
    boolean sessionTerminated = false;
	boolean ignoreResponse = false;

    String sessionID = "", xsecWID = "";
    String geofileID = "";
    //command action
    ArrayList actions = new ArrayList();
	
    //parameters for multi-action commands
    ArrayList<String> actionParams1 = new ArrayList<String>();
    ArrayList<String> actionParams2 = new ArrayList<String>();
    ArrayList<String> actionParams3 = new ArrayList<String>();
    ArrayList<String> actionParams4 = new ArrayList<String>();
    ArrayList<String> actionParams5 = new ArrayList<String>();
    ArrayList<String> actionParams6 = new ArrayList<String>();
	ArrayList<String> actionParams7 = new ArrayList<String>();
	ArrayList<String> actionParams8 = new ArrayList<String>();
	ArrayList<String> actionParams9 = new ArrayList<String>();
	ArrayList<String> actionParams10 = new ArrayList<String>();
	ArrayList<String> actionParams11 = new ArrayList<String>();
    ArrayList<String> actionParams12 = new ArrayList<String>();
    ArrayList<String> actionParams13 = new ArrayList<String>();

    //Window characteristics: Window ID, size (width, height), position (x, y)
    String xsecWID1 = "", xsecWID2 = "", xsecWID3 = "", xsecWID4 = "",
		   xsecWID5 = "", xsecWID6 = "", mapWID1 = "";
    int xsecWidth1 = 0, xsecHeight1 = 0, xsecWidth2 = 0, xsecHeight2 = 0,
        xsecWidth3 = 0, xsecHeight3 = 0, xsecWidth4 = 0, xsecHeight4 = 0,
		xsecWidth5 = 0, xsecHeight5 = 0, xsecWidth6 = 0, xsecHeight6 = 0,
		mapWidth1 = 0, mapHeight1 = 0;
    int xsecX1 = 0, xsecY1 = 0, xsecX2 = 0, xsecY2 = 0, xsecX3 = 0, xsecY3 = 0,
		xsecX4 = 0, xsecY4 = 0, xsecX5 = 0, xsecY5 = 0, xsecX6 = 0, xsecY6 = 0,
		mapX1 = 0, mapY1 = 0;
	int viewerWidth = 50, viewerHeight = 50;    //leave room around the edges

    //Note: layer IDs are unique across all windows
    //Window #1 layers; leave extra room. If the layerID is -1, this means the 
    //file for that layer does not exist.
    String[] layerIDs1 = new String[20]; int layer1Size = 0;
    String[] layerPropsID1 = new String[20]; int layerProps1Size = 0;
    //Window #2 layers; leave extra room
    String[] layerIDs2 = new String[10]; int layer2Size = 0;
    String[] layerPropsID2 = new String[10]; int layerProps2Size = 0;
    //Window #3 layers
    String[] layerIDs3 = new String[10]; int layer3Size = 0;
    String[] layerPropsID3 = new String[10]; int layerProps3Size = 0;
    //Window #4 layers
    String[] layerIDs4 = new String[2]; int layer4Size = 0;
    String[] layerPropsID4 = new String[2]; int layerProps4Size = 0;
    //Window #5 layers
    String[] layerIDs5 = new String[10]; int layer5Size = 0;
    String[] layerPropsID5 = new String[10]; int layerProps5Size = 0;
    //Window #6 layers
    String[] layerIDs6 = new String[10]; int layer6Size = 0;
    String[] layerPropsID6 = new String[10]; int layerProps6Size = 0;
    //Window #7 layers
    String[] layerIDs7 = new String[10]; int layer7Size = 0;
    String[] layerPropsID7 = new String[10]; int layerProps7Size = 0;
    
    //intermediate holder for a layer property ID
    String layerPropsID = "";
    
    //layer ID for the input layer
    String inputLayerID = "";
	
	//Window and layer IDs returned when adding a layer
	ArrayList ids;
	
	//Min and Max trace values
	ArrayList<String> minMaxTraceVals;

    //Info about all the datasets used to display WaveletDecomp results
    WaveletDecompDisplayFiles datasets;
    
    //Indicator if layer file exists ('yes' or 'no)
    String fileExists = "yes";
    //Indicator for processing a layer
    boolean skipLayer = false;

	int horizonLayerIdx = 0;
    /** Path of selected strategraphic horizon datasets */
	ArrayList<String> horizonLayers = new ArrayList<String>(10);
    /** Selected horizon within the horizon dataset */
    ArrayList<String> selectedHorizons = new ArrayList<String>(10);
    
    String horizLayerID = "";
           
    String annotatedLayer1 = "", annotatedLayer2 = "", annotatedLayer3 = "",
		   annotatedLayer4 = "", annotatedLayer5 = "", annotatedLayer6 = "";
           
    /** Number of BHP-SU horizons selected for strategraphic flattening */
    int numBhpsuHorizons = 0;

    boolean skipHorizons = true;
		   
	Double traclChosenRange;
    
    //Full pathname of dataset to be displayed
    String pathname = "";
    
    //Full cdp range for the input file
    ArrayList<Double> cdpFullRange;
    
    //Full ep range for the input file
    ArrayList<Double> epFullRange;
    
    //New incr if it needs to be changes
    double newIncr = 1.0;

    /**
     * Constructor. Initialize the finite state machine.
     * @param gui WaveletDecomp GUI.
     * @param messagingMgr WaveletDecomp Agent's messaging manager.
     * @param stepState The state (display step) in which to start.
     * @param actionState The action within a step in which to start.
     */
    public WaveletDecompDisplayTransducer(WaveletDecompGUI gui, MessagingManager messagingMgr, int stepState, int actionState) {
        this.stepState = stepState;
        this.actionState = actionState;
        this.messagingMgr = messagingMgr;
        this.gui = gui;
        this.agent = gui.getAgent();
    }

    /**
     * Process the response message from a CONTROL_QIVIEWER command.
     * The actions in the request are specified by the actionState
     * which is relative to the step (stepState) in the display process.
     * <p>
     * Step state 0, action 0 is start_control_session.
     * <br> Step state 0, action 1 is end_control_session.
     * @param resp The response message.
     */
    public void processResp(IQiWorkbenchMsg resp) {
System.out.println("WaveletDecomp::processResp: response="+resp);
        //Check if the user terminated the control session
        if (resp.isAbnormalStatus()) {
            String errmsg = (String)resp.getContent();
            if (errmsg.contains("CANCELLED")) {
                endControlSession(errmsg);
            }
        }
        
        switch (stepState) {
            case ControlConstants.SESSION_STEP:
                switch (actionState) {
                    case ControlConstants.START_SESSION:
                        //Check if session terminated and reinitialized for 
                        //reuse. NOTE: This consumes the response from the
                        //request to end the session.
                        if (ignoreResponse) {
                            //prepare to start a new session
                            //NOTE: The response from a CONTROL_QIVIEWER_CMD
                            //triggers the start of a new session.
                            ignoreResponse = false;
							sessionTerminated = false;
                            break;
                        }
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot start a display session");
                        //} //else
                        //Can only display results if input is BHP-SU
                        //if (!gui.isBhpsuInput() || !gui.isBhpsuOutput()) {
                        //    endControlSession("Cannot display results: Input not BHP-SU");
                        } else {
                            //Create pathnames of all datasets used to display 
                            //WaveletDecomp results. NOTE: This cannot be performed
                            //when the Transducer instance is created because
                            //the GUI is not yet populated.
                            datasets = new WaveletDecompDisplayFiles(gui);
                            
                            ArrayList outs = (ArrayList)resp.getContent();
                            sessionID = (String)outs.get(0);
                            qiViewerDesc = (ComponentDescriptor)outs.get(1);

                            stepState = WaveletDecompControlConstants.GET_LAYER_PROPS_WIN1;
							
							actionState = WaveletDecompControlConstants.INIT_PROGRESS_DIALOG;
							
							actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.INIT_PROGRESS_DIALOG_ACTION);
							actionParams1.add("title=Display WaveletDecomp Results");
                            int numWins = numWindows(datasets);
							actionParams1.add("windows="+numWins);
							int numHorizons = gui.isBhpsuHorizonDataset() ? gui.getNumOfHorizons() : 0;
                            int[] layerCnts = numLayers(datasets, numWins, numHorizons);

                            String layers = "";
                            for (int i=0; i<numWins; i++) {
                                layers += Integer.toString(layerCnts[i]);
                                if (i != numWins-1) layers += ",";
                            }
							actionParams1.add("layers="+layers);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case ControlConstants.END_SESSION:
                        //Prepare for next control session (in case use the same transducer)
                        initState();
                        break;

                    default:
                }
                break;

			//Map Window #1
            case WaveletDecompControlConstants.GET_LAYER_PROPS_WIN1:
                switch (actionState) {
					case WaveletDecompControlConstants.INIT_PROGRESS_DIALOG:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot initialize the progress dialog\n"+resp.getContent());
                        } else {
                            if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM_SLICE_TRANSPOSE_EP))
                                pathname = datasets.getPathname(WaveletDecompDisplayFiles.CWT_NORM_SLICE_TRANSPOSE_EP);
                            else if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_SLICE_TRANSPOSE_EP))
                                pathname = datasets.getPathname(WaveletDecompDisplayFiles.CWT_SLICE_TRANSPOSE_EP);
                            else pathname  = "";
                            
                            if (!pathname.equals("")) {
                                actionState = WaveletDecompControlConstants.GET_LAYER_PROPS;
    
                                actions.clear(); actionParams1.clear();
                                actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                                actionParams1.add("format=BHP-SU");
                                actionParams1.add("file="+pathname);
                            } else {
                                actionState = WaveletDecompControlConstants.SKIP_WINDOW1;
                                
                                actions.clear(); actionParams1.clear();
                                actions.add("action="+ControlConstants.NOP_ACTION);
                            }
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                    
                    //NOTE: Must set dataset properties before any other layer properties
                    case WaveletDecompControlConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for layer in Map window #1.\n"+resp.getContent());
                        } else {
                            layerPropsID1[layerProps1Size++] = layerPropsID =  (String)resp.getContent();
                            
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROP;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.tracl");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROP:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's tracl full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = traclChosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.tracl");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
							
                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=Synchronize.tracl");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set  cube's tracl chosen range.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROP1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdpt");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROP1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's cdpt full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdpt");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
							
                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=Synchronize.cdpt");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);
                            
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams3.clear();
							actionParams3.add("layerPropsID="+layerPropsID);
							actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's cdpt chosen range.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS2;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Scale.HorizontalScaleMultiplier");
                            actionParams1.add("value=2");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Scale.VerticalScaleMultiplier");
                            actionParams2.add("value=2");
                            actions.add(actionParams2);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=DisplayAttribute.Transpose");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("colormap=rainbow");
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set seismic layer display properties.\n"+resp.getContent());
                        } else {							
                           actionState = WaveletDecompControlConstants.SET_LAYER_PROPS3;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
                    case WaveletDecompControlConstants.SET_LAYER_PROPS3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set output cube's sync parameters.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.CREATE_WINDOW;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(WaveletDecompDisplayFiles.CWT_NORM_SLICE_TRANSPOSE_EP));
							actionParams1.add("ltype="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        } 
						break;
                        
                    case WaveletDecompControlConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot open XSection window #1.\n"+resp.getContent());
							break;
                        }
						ids = (ArrayList)resp.getContent();
						mapWID1 = (String)ids.get(0);
						layer1Size = 0;
						layerIDs1[layer1Size++] = (String)ids.get(1);
                        
                        //turn off broadcast so Window Scale properties can be set in insolation
                        actionState =  WaveletDecompControlConstants.SET_SYNC_PROPS1;
                        
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                        actionParams1.add("winID="+mapWID1);
                        actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                        actionParams1.add("field=Broadcast");
                        actionParams1.add("value=off");
                        actions.add(actionParams1);
                        
                        actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                        actionParams2.clear();
                        actionParams2.add("winID="+mapWID1);
                        actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                        actions.add(actionParams2);
                        
                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
                            
                    case WaveletDecompControlConstants.SET_SYNC_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot turn off broadcast\n"+resp.getContent());
                            break;
                        }
						actionState = WaveletDecompControlConstants.SET_ANNO_PROPS;

						actions.clear(); actionParams1.clear();
						actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
						actionParams1.add("winID="+mapWID1);
						actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
						actionParams1.add("field=Misc.Title.Location");
						actionParams1.add("value=TOP");
						actions.add(actionParams1);
						
						actionParams2.clear();
						actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
						actionParams2.add("winID="+mapWID1);
						actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
						actionParams2.add("field=Misc.Title.Text");
						actionParams2.add("value=Time/Depth: "+traclChosenRange);
						actions.add(actionParams2);

						actionParams3.clear();
						actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
						actionParams3.add("winID="+mapWID1);
						actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
						actionParams3.add("field=Misc.Labels.Left");
						actionParams3.add("value=cdp");
						actions.add(actionParams3);
						
						actionParams4.clear();
						actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
						actionParams4.add("winID="+mapWID1);
						actionParams4.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
						actionParams4.add("field=Misc.Labels.Top");
						actionParams4.add("value=ep");
						actions.add(actionParams4);

						actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
						actionParams5.clear();
						actionParams5.add("winID="+mapWID1);
						actionParams5.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
						actions.add(actionParams5);

						messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
					
                    case WaveletDecompControlConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set window Annotation properties.\n"+resp.getContent());
                        } else {
							actionState =  WaveletDecompControlConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+mapWID1);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToHorizontalScale");
                            actionParams2.add("value=off");
                            actions.add(actionParams2);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+mapWID1);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToVerticalScale");
                            actionParams3.add("value=off");
                            actions.add(actionParams3);
							
                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+mapWID1);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=ListenToCursorPosition");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);

							actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+mapWID1);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams5.add("field=ListenToScrollPosition");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams6.clear();
                            actionParams6.add("winID="+mapWID1);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams6);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set window Sync properties.\n"+resp.getContent());
                        } else {
							actionState =  WaveletDecompControlConstants.SET_SCALE_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+mapWID1);
                            actionParams1.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actionParams1.add("field=LockAspectRatio");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);
                            
                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+mapWID1);
                            actionParams2.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actions.add(actionParams2);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_SCALE_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set window scale properties.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.RESIZE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            //determine height and width
                            mapHeight1 = 600; mapWidth1 = 600;

                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+mapWID1);
                            actionParams1.add("width="+mapWidth1);
                            actionParams1.add("height="+mapHeight1);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+mapWID1);
                            actionParams2.add("x="+mapX1);
                            actionParams2.add("y="+mapY1);
                            actions.add(actionParams2);
                            
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+mapWID1);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot resize XSec window #1.\n"+resp.getContent());
                        } else {
                            //It is now safe to turn broadcast on
                            actionState =  WaveletDecompControlConstants.SET_SYNC_PROPS2;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+mapWID1);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);
                            
                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+mapWID1);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams2);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                            
                    case WaveletDecompControlConstants.SET_SYNC_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot turn on broadcast\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.WIDEN_SPLIT_PANE;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_DIVIDER_ACTION);
                            actionParams1.add("winID="+mapWID1);
                            actionParams1.add("splitPane="+ControlConstants.LAYER_EXPLORER);
                            actionParams1.add("dividerLoc=100");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case WaveletDecompControlConstants.SKIP_WINDOW1:
                    case WaveletDecompControlConstants.WIDEN_SPLIT_PANE:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set divider in window #1.\n"+resp.getContent());
                        } else {
                            if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM_SLICE))
                                pathname = datasets.getPathname(WaveletDecompDisplayFiles.CWT_NORM_SLICE);
                            else if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_SLICE))
                                pathname = datasets.getPathname(WaveletDecompDisplayFiles.CWT_SLICE);
                            else pathname  = "";
                            
                            stepState = WaveletDecompControlConstants.GET_LAYER_PROPS_WIN2;
                            
                            if (!pathname.equals("")) {
                                actionState = WaveletDecompControlConstants.GET_LAYER_PROPS;
    
                                actions.clear(); actionParams1.clear();
                                actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                                actionParams1.add("format=BHP-SU");
                                actionParams1.add("file="+pathname);
                            } else {
                                actionState = WaveletDecompControlConstants.SKIP_WINDOW2;
                                
                                actions.clear(); actionParams1.clear();
                                actions.add("action="+ControlConstants.NOP_ACTION);
                            }
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;									

			//XSec Window #1
            case WaveletDecompControlConstants.GET_LAYER_PROPS_WIN2:
                switch (actionState) {
                    case WaveletDecompControlConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for layer in XSection window #1.\n"+resp.getContent());
                        } else {
                            layerPropsID2[layerProps2Size++] = layerPropsID =  (String)resp.getContent();
							
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROP;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.ep");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROP:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's tracl full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.ep");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=Synchronize.ep");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                    
                    case WaveletDecompControlConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set  cube's tracl chosen range.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROP1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdp");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROP1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's cdpt full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdp");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
                            
                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=Synchronize.cdp");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);
                            
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams3.clear();
							actionParams3.add("layerPropsID="+layerPropsID);
							actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's cdpt chosen range.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS2;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=1");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value=0");
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value=5");
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Interpolated_Density");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
							
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("colormap=rainbow");
                            actions.add(actionParams9);
                            
                            actionParams10.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams10.add("field=Scale.HorizontalScaleMultiplier");
                            actionParams10.add("value=1");
                            actions.add(actionParams10);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Scale.VerticalScaleMultiplier");
                            actionParams11.add("value=1");
                            actions.add(actionParams11);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams12.clear();
                            actionParams12.add("layerPropsID="+layerPropsID);
                            actionParams12.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams12.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams12);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set seismic layer properties.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS3;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
                    case WaveletDecompControlConstants.SET_LAYER_PROPS3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's sync parameters.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.CREATE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(WaveletDecompDisplayFiles.CWT_NORM_SLICE));
                            actionParams1.add("ltype="+ControlConstants.SEISMIC_LAYER);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        } 
						break;					
					
                    case WaveletDecompControlConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot open XSection window #1.\n"+resp.getContent());
							break;
                        }
						
						ids = (ArrayList)resp.getContent();
						xsecWID1 = (String)ids.get(0);
						layer2Size = 0;
						layerIDs2[layer2Size++] = annotatedLayer1 = (String)ids.get(1);
                            
                        //turn off broadcast so Window Scale properties can be set in insolation
                        actionState =  WaveletDecompControlConstants.SET_SYNC_PROPS1;
                        
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                        actionParams1.add("winID="+xsecWID1);
                        actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                        actionParams1.add("field=Broadcast");
                        actionParams1.add("value=off");
                        actions.add(actionParams1);
                        
                        actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                        actionParams2.clear();
                        actionParams2.add("winID="+xsecWID1);
                        actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                        actions.add(actionParams2);
                        
                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
                            
                    case WaveletDecompControlConstants.SET_SYNC_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot turn off broadcast\n"+resp.getContent());
                            break;
                        }
						actionState =  WaveletDecompControlConstants.SET_ANNO_PROPS;
                        
                        //Must set the annotation layer first
						actions.clear(); actionParams4.clear();
						actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
						actionParams4.add("winID="+xsecWID1);
						actionParams4.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
						actionParams4.add("field=AnnotatedLayer");
						actionParams4.add("value="+annotatedLayer1);
						actions.add(actionParams4);
                        
                        //Clear the list of horizontal selected keys
                        actionParams6.clear();
                        actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                        actionParams6.add("winID="+xsecWID1);
                        actionParams6.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                        actionParams6.add("field=Horizontal.SelectedKeys");
                        actionParams6.add("value=");
                        actions.add(actionParams6);
                            
						actionParams1.clear();
						actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
						actionParams1.add("winID="+xsecWID1);
						actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
						actionParams1.add("field=Horizontal.SelectedKeys");
						actionParams1.add("value=cdp,ep,cdpt");
						actions.add(actionParams1);
						
						actionParams2.clear();
						actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
						actionParams2.add("winID="+xsecWID1);
						actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
						actionParams2.add("field=Horizontal.Location");
						actionParams2.add("value=TOP");
						actions.add(actionParams2);

						actionParams3.clear();
						actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
						actionParams3.add("winID="+xsecWID1);
						actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
						actionParams3.add("field=Horizontal.SynchronizationKey");
						actionParams3.add("value=cdpt");
						actions.add(actionParams3);

						actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
						actionParams5.clear();
						actionParams5.add("winID="+xsecWID1);
						actionParams5.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
						actions.add(actionParams5);

						messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
						
                    case WaveletDecompControlConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Annotation properties.\n"+resp.getContent());
                        } else {
							actionState =  WaveletDecompControlConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID1);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToHorizontalScale");
                            actionParams2.add("value=off");
                            actions.add(actionParams2);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID1);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToVerticalScale");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);
							
                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID1);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=ListenToCursorPosition");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);

							actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+xsecWID1);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams5.add("field=ListenToScrollPosition");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams6.clear();
                            actionParams6.add("winID="+xsecWID1);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams6);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set sync properties.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.RESIZE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            //determine height and width
                            xsecHeight1 = 600; xsecWidth1 = 300;
                            //deterimine positon of window next to XSection window #1
                            xsecX1 = mapWidth1+1; xsecY1 = 0;

                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("width="+xsecWidth1);
                            actionParams1.add("height="+xsecHeight1);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID1);
                            actionParams2.add("x="+xsecX1);
                            actionParams2.add("y="+xsecY1);
                            actions.add(actionParams2);
                            
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID1);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot resize XSec window #2.\n"+resp.getContent());
                        } else {
                            //It is now safe to turn broadcast on
                            actionState =  WaveletDecompControlConstants.SET_SYNC_PROPS2;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);
                            
                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID1);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams2);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                            
                    case WaveletDecompControlConstants.SET_SYNC_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot turn on broadcast\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.WIDEN_SPLIT_PANE;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_DIVIDER_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("splitPane="+ControlConstants.LAYER_EXPLORER);
                            actionParams1.add("dividerLoc=100");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SKIP_WINDOW2:
                    case WaveletDecompControlConstants.WIDEN_SPLIT_PANE:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set divider in XSec window #1.\n"+resp.getContent());
                        } else {
                            if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM_SLICE))
                                pathname = datasets.getPathname(WaveletDecompDisplayFiles.CWT_NORM_SLICE);
                            else if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_SLICE))
                                pathname = datasets.getPathname(WaveletDecompDisplayFiles.CWT_SLICE);
                            else pathname  = "";
                            
                            stepState = WaveletDecompControlConstants.GET_LAYER_PROPS_WIN3;
                            
                            if (!pathname.equals("")) {
                                actionState = WaveletDecompControlConstants.GET_LAYER_PROPS;
                                actionState =  WaveletDecompControlConstants.GET_LAYER_PROPS;
                                
                                //NOTE: The near_wavelet and far_wavelet always exist
                                actions.clear(); actionParams1.clear();
                                actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                                actionParams1.add("format=BHP-SU");
                                actionParams1.add("file="+ pathname);
                            } else {
                                actionState = WaveletDecompControlConstants.SKIP_WINDOW3;
                                
                                actions.clear(); actionParams1.clear();
                                actions.add("action="+ControlConstants.NOP_ACTION);
                            }
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

			//XSec window #2
            case WaveletDecompControlConstants.GET_LAYER_PROPS_WIN3:
                switch (actionState) {
					case WaveletDecompControlConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for layer in XSection window #2.\n"+resp.getContent());
                        } else {
                            layerPropsID3[layerProps3Size++] = layerPropsID =  (String)resp.getContent();
							
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROP;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.ep");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROP:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's tracl full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.ep");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=Synchronize.ep");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set  cube's tracl chosen range.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROP1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdpt");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROP1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's cdpt full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdpt");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.cdp");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.cdpt");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);
                            
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams4.clear();
							actionParams4.add("layerPropsID="+layerPropsID);
							actions.add(actionParams4);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                            
                    case WaveletDecompControlConstants.SET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's cdpt chosen range.\n"+resp.getContent());
                        } else {
							actionState =  WaveletDecompControlConstants.SET_LAYER_PROPS2;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=1");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value=0");
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value=5");
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Interpolated_Density");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
							
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("colormap=rainbow");
                            actions.add(actionParams9);
							
                            actionParams10.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams10.add("field=Scale.HorizontalScaleMultiplier");
                            actionParams10.add("value=1");
                            actions.add(actionParams10);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Scale.VerticalScaleMultiplier");
                            actionParams11.add("value=1");
                            actions.add(actionParams11);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams12.clear();
                            actionParams12.add("layerPropsID="+layerPropsID);
                            actionParams12.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams12.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams12);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set 1st layer's properties.\n"+resp.getContent());
                        } else {
                           actionState = WaveletDecompControlConstants.SET_LAYER_PROPS3;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
                    case WaveletDecompControlConstants.SET_LAYER_PROPS3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's sync parameters.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.CREATE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+ datasets.getPathname(WaveletDecompDisplayFiles.CWT_NORM_SLICE));
							actionParams1.add("ltype="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
						}
						break;
					
                    case WaveletDecompControlConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot open XSection window #2.\n"+resp.getContent());
                        } else {
                            ids = (ArrayList)resp.getContent();
                            xsecWID2 = (String)ids.get(0);
                            layer3Size = 0;
                            layerIDs3[layer3Size++] = annotatedLayer2 = (String)ids.get(1);
                            
                            actionState = WaveletDecompControlConstants.RESIZE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            //determine height and width
                            xsecHeight2 = 600; xsecWidth2 = 1200;
                            //deterimine positon of window under XSection window #2
                            xsecX2 = xsecX1 + xsecWidth1 + 1; xsecY2 = 0;

                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("width="+xsecWidth2);
                            actionParams1.add("height="+xsecHeight2);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID2);
                            actionParams2.add("x="+xsecX2);
                            actionParams2.add("y="+xsecY2);
                            actions.add(actionParams2);
                            
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID2);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot resize XSection Window #2.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=ListenToHorizontalScale");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID2);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToVerticalScale");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID2);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToScrollPosition");
                            actionParams3.add("value=off");
                            actions.add(actionParams3);

                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID2);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=ListenToCursorPosition");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);

                            actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+xsecWID2);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams5.add("field=Broadcast");
                            actionParams5.add("value=on");
                            actions.add(actionParams5);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams6.clear();
                            actionParams6.add("winID="+xsecWID2);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams6);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                   case WaveletDecompControlConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Synchronization properties for XSection window #2.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_ANNO_PROPS ;

                            //Must set the annotation layer first
                            actions.clear(); actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID2);
                            actionParams4.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams4.add("field=AnnotatedLayer");
                            actionParams4.add("value="+annotatedLayer2);
                            actions.add(actionParams4);
                            
                            //Clear the list of horizontal selected keys
                            actionParams6.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams6.add("winID="+xsecWID2);
                            actionParams6.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams6.add("field=Horizontal.SelectedKeys");
                            actionParams6.add("value=");
                            actions.add(actionParams6);
                            
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams1.add("field=Horizontal.SelectedKeys");
                            actionParams1.add("value=cdp,ep,cdpt");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID2);
                            actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams2.add("field=Horizontal.SynchronizationKey");
                            actionParams2.add("value=cdp");
                            actions.add(actionParams2);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID2);
                            actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams3.add("field=Horizontal.Location");
                            actionParams3.add("value=TOP");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("winID="+xsecWID2);
                            actionParams5.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Annotation properties.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.WIDEN_SPLIT_PANE;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_DIVIDER_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("splitPane="+ControlConstants.LAYER_EXPLORER);
                            actionParams1.add("dividerLoc=100");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.SKIP_WINDOW3:
                    case WaveletDecompControlConstants.WIDEN_SPLIT_PANE:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set the divider in XSection window #2.\n"+resp.getContent());
                        } else {
                            if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM_SLICE))
                                pathname = datasets.getPathname(WaveletDecompDisplayFiles.CWT_NORM_SLICE);
                            else if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_SLICE))
                                pathname = datasets.getPathname(WaveletDecompDisplayFiles.CWT_SLICE);
                            else pathname  = "";
                            
                            stepState = WaveletDecompControlConstants.GET_LAYER_PROPS_WIN4;
                            
                            if (!pathname.equals("")) {
                                actionState =  WaveletDecompControlConstants.GET_LAYER_PROPS;
                                
                                actions.clear(); actionParams1.clear();
                                actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                                actionParams1.add("format=BHP-SU");
                                actionParams1.add("file="+ pathname);
                            } else {
                                actionState = WaveletDecompControlConstants.SKIP_WINDOW4;
                                
                                actions.clear(); actionParams1.clear();
                                actions.add("action="+ControlConstants.NOP_ACTION);
                            }
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

			//XSec window #3
            case WaveletDecompControlConstants.GET_LAYER_PROPS_WIN4:
                switch (actionState) {
					case WaveletDecompControlConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for layer in XSection window #3.\n"+resp.getContent());
                        } else {
                            layerPropsID4[layerProps4Size++] = layerPropsID =  (String)resp.getContent();
							
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROP;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdpt");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROP:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's tracl full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdpt");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=Synchronize.cdpt");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set  cube's tracl chosen range.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROP1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdp");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROP1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's cdpt full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdp");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.ep");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);
                            
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.cdp");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);
                            
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams4.clear();
							actionParams4.add("layerPropsID="+layerPropsID);
							actions.add(actionParams4);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's cdpt chosen range.\n"+resp.getContent());
                        } else {
							actionState =  WaveletDecompControlConstants.SET_LAYER_PROPS2;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=1");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value=0");
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value=5");
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Interpolated_Density");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
							
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("colormap=rainbow");
                            actions.add(actionParams9);
                            
                            actionParams10.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams10.add("field=Scale.HorizontalScaleMultiplier");
                            actionParams10.add("value=2");
                            actions.add(actionParams10);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Scale.VerticalScaleMultiplier");
                            actionParams11.add("value=1");
                            actions.add(actionParams11);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams12.clear();
                            actionParams12.add("layerPropsID="+layerPropsID);
                            actionParams12.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams12.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams12);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set layer's properties.\n"+resp.getContent());
                        } else {
                           actionState = WaveletDecompControlConstants.SET_LAYER_PROPS3;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
                    case WaveletDecompControlConstants.SET_LAYER_PROPS3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's sync parameters.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.CREATE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+ datasets.getPathname(WaveletDecompDisplayFiles.CWT_NORM_SLICE));
							actionParams1.add("ltype="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
						}
						break;
					
                    case WaveletDecompControlConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot open XSection window #3.\n"+resp.getContent());
                        } else {
                            ids = (ArrayList)resp.getContent();
                            xsecWID3 = (String)ids.get(0);
                            layer4Size = 0;
                            layerIDs4[layer4Size++] = annotatedLayer3 = (String)ids.get(1);
                            
                            actionState = WaveletDecompControlConstants.RESIZE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            //determine height and width
                            xsecHeight3 = 600; xsecWidth3 = 1200;
                            //deterimine positon of window next to XSection window #3
                            xsecX3 = xsecX2 + xsecWidth2 + 1; xsecY3 = 0;

                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("width="+xsecWidth3);
                            actionParams1.add("height="+xsecHeight3);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID3);
                            actionParams2.add("x="+xsecX3);
                            actionParams2.add("y="+xsecY3);
                            actions.add(actionParams2);
                            
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID3);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot resize XSection Window #3.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=ListenToHorizontalScale");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID3);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToVerticalScale");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID3);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToScrollPosition");
                            actionParams3.add("value=off");
                            actions.add(actionParams3);

                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID3);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=ListenToCursorPosition");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);

                            actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+xsecWID3);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams5.add("field=Broadcast");
                            actionParams5.add("value=on");
                            actions.add(actionParams5);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams6.clear();
                            actionParams6.add("winID="+xsecWID3);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams6);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                   case WaveletDecompControlConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Synchronization properties for XSection window #3.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_ANNO_PROPS ;

                            //Must set the annotation layer first
                            actions.clear(); actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID3);
                            actionParams4.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams4.add("field=AnnotatedLayer");
                            actionParams4.add("value="+annotatedLayer3);
                            actions.add(actionParams4);
                            
                            //Clear the list of horizontal selected keys
                            actionParams6.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams6.add("winID="+xsecWID3);
                            actionParams6.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams6.add("field=Horizontal.SelectedKeys");
                            actionParams6.add("value=");
                            actions.add(actionParams6);
                            
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams1.add("field=Horizontal.SelectedKeys");
                            actionParams1.add("value=cdp,ep,cdpt");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID3);
                            actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams2.add("field=Horizontal.SynchronizationKey");
                            actionParams2.add("value=ep");
                            actions.add(actionParams2);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID3);
                            actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams3.add("field=Horizontal.Location");
                            actionParams3.add("value=TOP");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("winID="+xsecWID3);
                            actionParams5.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Annotation properties.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.WIDEN_SPLIT_PANE;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_DIVIDER_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("splitPane="+ControlConstants.LAYER_EXPLORER);
                            actionParams1.add("dividerLoc=100");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.SKIP_WINDOW4:
                    case WaveletDecompControlConstants.WIDEN_SPLIT_PANE:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set the divider in XSection window #3.\n"+resp.getContent());
                        } else {
                            if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM))
                                pathname = datasets.getPathname(WaveletDecompDisplayFiles.CWT_NORM);
                            else if (datasets.isFilename(WaveletDecompDisplayFiles.CWT))
                                pathname = datasets.getPathname(WaveletDecompDisplayFiles.CWT);
                            else pathname  = "";
                            
                            stepState = WaveletDecompControlConstants.GET_LAYER_PROPS_WIN5;
                            
                            if (!pathname.equals("")) {
                                actionState =  WaveletDecompControlConstants.GET_LAYER_PROPS;
                                
                                actions.clear(); actionParams1.clear();
                                actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                                actionParams1.add("format=BHP-SU");
                                actionParams1.add("file="+ pathname);
                            } else {
                                actionState = WaveletDecompControlConstants.SKIP_WINDOW5;
                                
                                actions.clear(); actionParams1.clear();
                                actions.add("action="+ControlConstants.NOP_ACTION);
                            }
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

			//XSec window #4
            case WaveletDecompControlConstants.GET_LAYER_PROPS_WIN5:
                switch (actionState) {
					case WaveletDecompControlConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for layer in XSection window #4.\n"+resp.getContent());
                        } else {
                            layerPropsID5[layerProps5Size++] = layerPropsID =  (String)resp.getContent();
							
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROP;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.ep");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROP:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's tracl full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.ep");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=Synchronize.ep");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set  cube's tracl chosen range.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROP1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdp");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROP1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's cdpt full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdp");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.cdpt");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);
                            
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.cdp");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);
                            
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams4.clear();
							actionParams4.add("layerPropsID="+layerPropsID);
							actions.add(actionParams4);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's cdpt chosen range.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS2;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=1");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value=0");
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value=5");
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Interpolated_Density");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
							
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("colormap=rainbow");
                            actions.add(actionParams9);
                            
                            actionParams10.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams10.add("field=Scale.HorizontalScaleMultiplier");
                            actionParams10.add("value=1");
                            actions.add(actionParams10);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Scale.VerticalScaleMultiplier");
                            actionParams11.add("value=1");
                            actions.add(actionParams11);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams12.clear();
                            actionParams12.add("layerPropsID="+layerPropsID);
                            actionParams12.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams12.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams12);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set layer's properties.\n"+resp.getContent());
                        } else {
                           actionState = WaveletDecompControlConstants.SET_LAYER_PROPS3;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
                    case WaveletDecompControlConstants.SET_LAYER_PROPS3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's sync parameters.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.CREATE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+ datasets.getPathname(WaveletDecompDisplayFiles.CWT_NORM));
							actionParams1.add("ltype="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
						}
						break;
					
                    case WaveletDecompControlConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot open XSection window #4.\n"+resp.getContent());
                        } else {
                            ids = (ArrayList)resp.getContent();
                            xsecWID4 = (String)ids.get(0);
                            layer5Size = 0;
                            layerIDs5[layer5Size++] = annotatedLayer4 = (String)ids.get(1);
                            
                            actionState = WaveletDecompControlConstants.RESIZE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            //determine height and width
                            xsecHeight4 = 600; xsecWidth4 = 300;
                            //deterimine positon of window under XSection window #1, but half way down
                            xsecX4 = mapX1; xsecY4 = mapY1 + mapHeight1/2 + 1;

                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+xsecWID4);
                            actionParams1.add("width="+xsecWidth4);
                            actionParams1.add("height="+xsecHeight4);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID4);
                            actionParams2.add("x="+xsecX4);
                            actionParams2.add("y="+xsecY4);
                            actions.add(actionParams2);

                            /*
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID4);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);
                            */
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot resize XSection Window #4.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID4);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=ListenToHorizontalScale");
                            actionParams1.add("value=off");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID4);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToVerticalScale");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID4);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToScrollPosition");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);
                            
                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID4);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=SynchronizeVerticalScrolling");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);

                            actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+xsecWID4);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams5.add("field=ListenToCursorPosition");
                            actionParams5.add("value=on");
                            actions.add(actionParams5);

                            actionParams6.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams6.add("winID="+xsecWID4);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams6.add("field=Broadcast");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams7.clear();
                            actionParams7.add("winID="+xsecWID4);
                            actionParams7.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams7);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                   case WaveletDecompControlConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Synchronization properties for XSection window #4.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_ANNO_PROPS;

                            //Must set the annotation layer first
                            actions.clear(); actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID4);
                            actionParams4.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams4.add("field=AnnotatedLayer");
                            actionParams4.add("value="+annotatedLayer4);
                            actions.add(actionParams4);
                            
                            //Clear the list of horizontal selected keys
                            actionParams6.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams6.add("winID="+xsecWID4);
                            actionParams6.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams6.add("field=Horizontal.SelectedKeys");
                            actionParams6.add("value=");
                            actions.add(actionParams6);
                            
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID4);
                            actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams1.add("field=Horizontal.SelectedKeys");
                            actionParams1.add("value=cdp,ep,cdpt,f2");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID4);
                            actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams2.add("field=Horizontal.SynchronizationKey");
                            actionParams2.add("value=cdpt");
                            actions.add(actionParams2);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID4);
                            actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams3.add("field=Horizontal.Location");
                            actionParams3.add("value=TOP");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("winID="+xsecWID4);
                            actionParams5.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Annotation properties.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.WIDEN_SPLIT_PANE;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_DIVIDER_ACTION);
                            actionParams1.add("winID="+xsecWID4);
                            actionParams1.add("splitPane="+ControlConstants.LAYER_EXPLORER);
                            actionParams1.add("dividerLoc=100");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.SKIP_WINDOW5:
                    case WaveletDecompControlConstants.WIDEN_SPLIT_PANE:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set the divider in XSection window #4.\n"+resp.getContent());
                        } else {
                            stepState = WaveletDecompControlConstants.GET_LAYER_PROPS_WIN6;
							actionState =  WaveletDecompControlConstants.GET_LAYER_PROPS;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+ datasets.getPathname(WaveletDecompDisplayFiles.INPUT));
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

			//Xsec window #5
            case WaveletDecompControlConstants.GET_LAYER_PROPS_WIN6:
                switch (actionState) {
                    case WaveletDecompControlConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for input layer in XSection window #5.\n"+resp.getContent());
                        } else {	
                            layerPropsID6[layerProps6Size++] = layerPropsID = (String)resp.getContent();
							
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROPS3;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.ep");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROPS3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's ep full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS4;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.ep");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.cdp");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.ep");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);
                            
                            actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams4.clear();
							actionParams4.add("layerPropsID="+layerPropsID);
							actions.add(actionParams4);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS4:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's dataset properties.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROPS11;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdp");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROPS11:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's cdp full range.\n"+resp.getContent());
                        } else {
                            //Save the cdp full range for the input in case the incr needs to be changed
                            cdpFullRange = getFullRange((String)resp.getContent());
                            
                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS5;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=0.2");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=RMS");
                            actions.add(actionParams2);
/*Not needed for RMS
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value=0");
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value=5");
                            actions.add(actionParams4);
*/
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Wiggle_Trace");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams13.clear();
                            actionParams13.add("layerPropsID="+layerPropsID);
                            actionParams13.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams13.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams13.add("field=RasterizingType.Positive_Fill");
                            actionParams13.add("value=on");
                            actions.add(actionParams13);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
							
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("colormap=rainbow");
                            actions.add(actionParams9);
                            
                            actionParams10.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams10.add("field=Scale.HorizontalScaleMultiplier");
                            actionParams10.add("value=1");
                            actions.add(actionParams10);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Scale.VerticalScaleMultiplier");
                            actionParams11.add("value=1");
                            actions.add(actionParams11);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams12.clear();
                            actionParams12.add("layerPropsID="+layerPropsID);
                            actionParams12.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams12.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams12);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS5:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set seismic layer properties.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS6;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
                    case WaveletDecompControlConstants.SET_LAYER_PROPS6:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's sync parameters.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.CREATE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+ datasets.getPathname(WaveletDecompDisplayFiles.INPUT));
							actionParams1.add("ltype="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
						}
						break;
					
                    case WaveletDecompControlConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot open XSection window #5.\n"+resp.getContent());
                        } else {
                            ids = (ArrayList)resp.getContent();
                            xsecWID5 = (String)ids.get(0);
                            layer6Size = 0;
                            //Note: The annotation layer if there are no more layers to be added
                            layerIDs6[layer6Size++] = annotatedLayer5 = inputLayerID = (String)ids.get(1);
                            
                            actions.clear();
                            if (!datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM) &&
                            !datasets.isFilename(WaveletDecompDisplayFiles.CWT)) {
                                actionState = WaveletDecompControlConstants.SKIP_LAYER2;
                                
                                actions.add("action="+ControlConstants.NOP_ACTION);
                            } else
                            if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM)) {
                                pathname = datasets.getPathname(WaveletDecompDisplayFiles.CWT_NORM);
                            
                                actionState = WaveletDecompControlConstants.GET_LAYER_PROPS2;
                                
                                actionParams1.clear();
                                actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                                actionParams1.add("format=BHP-SU");
                                actionParams1.add("file="+pathname);
                            } else {
                                //handle the raw file, if any
                                actionState = WaveletDecompControlConstants.SKIP_LAYER3;
                                
                                actions.add("action="+ControlConstants.NOP_ACTION);
                            }
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                    
                    //Load norm layer
					case WaveletDecompControlConstants.GET_LAYER_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for 2nd layer in XSection window #5.\n"+resp.getContent());
                        } else {
                            layerPropsID6[layerProps6Size++] = layerPropsID =  (String)resp.getContent();
							
							actionState =  WaveletDecompControlConstants.GET_LAYER_PROP;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.ep");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROP:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's ep full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS3;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.ep");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.cdp");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.ep");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set  cube's ep chosen range.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROPS4;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdpt");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROPS4:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's cdpt full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdpt");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
                            
                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=Synchronize.cdpt");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);
                            
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams3.clear();
							actionParams3.add("layerPropsID="+layerPropsID);
							actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set  cube's cdpt chosen range.\n"+resp.getContent());
                        } else {
							actionState =  WaveletDecompControlConstants.GET_TRACE_RANGE;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_TRACE_RANGE_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
                    case WaveletDecompControlConstants.GET_TRACE_RANGE:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get trace range values\n"+resp.getContent());
                        } else {
							minMaxTraceVals = (ArrayList<String>)resp.getContent();
                            
							actionState =  WaveletDecompControlConstants.SET_LAYER_PROPS1;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=1.0");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value=0");
                            actions.add(actionParams3);
							
                            int max = ceiling(minMaxTraceVals.get(1));
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+max);
                            actions.add(actionParams4);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Interpolated_Density");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
/*
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams13.clear();
                            actionParams13.add("layerPropsID="+layerPropsID);
                            actionParams13.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams13.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams13.add("field=RasterizingType.Positive_Fill");
                            actionParams13.add("value=on");
                            actions.add(actionParams13);
*/
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=15");
                            actions.add(actionParams8);
							
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("colormap=rainbow");
                            actions.add(actionParams9);
                            
                            actionParams10.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams10.add("field=Scale.HorizontalScaleMultiplier");
                            actionParams10.add("value=1");
                            actions.add(actionParams10);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Scale.VerticalScaleMultiplier");
                            actionParams11.add("value=1");
                            actions.add(actionParams11);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams12.clear();
                            actionParams12.add("layerPropsID="+layerPropsID);
                            actionParams12.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams12.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams12);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set layer's properties.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS2;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
                    case WaveletDecompControlConstants.SET_LAYER_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's sync parameters.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.ADD_LAYER;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID5);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+pathname);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    //Add layer
                    case WaveletDecompControlConstants.ADD_LAYER:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add norm layer to XSection window #5.\n"+resp.getContent());
                            break;
                        }
                        ids = (ArrayList)resp.getContent();
//already set                        xsecWID5 = (String)ids.get(0);
						layerIDs6[layer6Size++] = annotatedLayer5 = (String)ids.get(1);
/*
                        actionState = WaveletDecompControlConstants.MOVE_LAYER1;
                        
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.MOVE_LAYER_ACTION);
                        actionParams1.add("winID="+xsecWID5);
                        actionParams1.add("layerID="+annotatedLayer5);
                        actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                        actionParams1.add("direction=Down");
                        actionParams1.add("value=Bottom");
                        actions.add(actionParams1);
                        
                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
                            
                    case WaveletDecompControlConstants.MOVE_LAYER1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot reposition seismic layer.\n"+resp.getContent());
                            break;
                        }
*/
                        actions.clear(); actionParams1.clear();
                        if (!datasets.isFilename(WaveletDecompDisplayFiles.CWT)) {
                            actionState = WaveletDecompControlConstants.SKIP_LAYER2;
                            
                            actions.add("action="+ControlConstants.NOP_ACTION);
                        } else {
                            pathname = datasets.getPathname(WaveletDecompDisplayFiles.CWT);
                        
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROPS5;
                            
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+pathname);
                        }
                        actions.add(actionParams1);

                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);

                        break;
                    
                    //Load raw layer
					case WaveletDecompControlConstants.GET_LAYER_PROPS5:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for raw layer in XSection window #5.\n"+resp.getContent());
                        } else {
                            layerPropsID6[layerProps6Size++] = layerPropsID =  (String)resp.getContent();
							
							actionState =  WaveletDecompControlConstants.GET_LAYER_PROP1;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.ep");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROP1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's ep full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS7;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.ep");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.cdp");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.ep");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS7:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set  cube's ep chosen range.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROPS8;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdpt");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROPS8:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's cdpt full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS9;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdpt");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
                            
                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=Synchronize.cdpt");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);
                            
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams3.clear();
							actionParams3.add("layerPropsID="+layerPropsID);
							actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS9:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set  cube's cdpt chosen range.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROPS12;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdp");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROPS12:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's cdp full range.\n"+resp.getContent());
                        } else {
                            ArrayList<Double> cdpRange = getFullRange((String)resp.getContent());
                            
                            actions.clear(); actionParams1.clear();
                            
                            //Check if cdp's incr of this layer differs from the incr of the input layer. If so, set the selected range for cdp with the new incr
                            if (cdpRange.get(2).doubleValue() == cdpFullRange.get(2).doubleValue()) {
                                actionState =  WaveletDecompControlConstants.SKIP_OP;
                                
                                actions.add("action="+ControlConstants.NOP_ACTION);
                                actions.add(actionParams1);
							} else {
                                actionState = WaveletDecompControlConstants.SET_SELECTED_RANGE;
                                
                                actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                                actionParams1.add("layerID="+inputLayerID);
                                actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                                actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                                actionParams1.add("field=ChosenRange.cdp");
                                newIncr = cdpRange.get(2).doubleValue();
                                String chosenRange = getSelectedRange(cdpFullRange.get(0).doubleValue(), cdpFullRange.get(1).doubleValue(), newIncr);
                                actionParams1.add("value="+chosenRange);
                                actions.add(actionParams1);
                                
                                actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                                actionParams2.clear();
                                actionParams2.add("layerID="+inputLayerID);
                                actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                                actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                                actions.add(actionParams2);
                            }
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_SELECTED_RANGE:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cdp's selected range for input layer\n"+resp.getContent());
                            break;
                        }
                    case WaveletDecompControlConstants.SKIP_OP:
                        actionState =  WaveletDecompControlConstants.GET_TRACE_RANGE1;
                        
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.GET_TRACE_RANGE_ACTION);
                        actionParams1.add("layerPropsID="+layerPropsID);
                        actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                        actions.add(actionParams1);
                        
                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
							
                    case WaveletDecompControlConstants.GET_TRACE_RANGE1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get trace range values\n"+resp.getContent());
                        } else {
							minMaxTraceVals = (ArrayList<String>)resp.getContent();
                            
							actionState =  WaveletDecompControlConstants.SET_LAYER_PROPS10;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=1.0");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value=0");
                            actions.add(actionParams3);
							
                            int max = ceiling(minMaxTraceVals.get(1));
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+max);
                            actions.add(actionParams4);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Interpolated_Density");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
/*
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams13.clear();
                            actionParams13.add("layerPropsID="+layerPropsID);
                            actionParams13.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams13.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams13.add("field=RasterizingType.Positive_Fill");
                            actionParams13.add("value=on");
                            actions.add(actionParams13);
*/
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=15");
                            actions.add(actionParams8);
							
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("colormap=rainbow");
                            actions.add(actionParams9);
                            
                            actionParams10.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams10.add("field=Scale.HorizontalScaleMultiplier");
                            actionParams10.add("value=1");
                            actions.add(actionParams10);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Scale.VerticalScaleMultiplier");
                            actionParams11.add("value=1");
                            actions.add(actionParams11);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams12.clear();
                            actionParams12.add("layerPropsID="+layerPropsID);
                            actionParams12.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams12.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams12);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS10:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set layer's properties.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS11;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
                    case WaveletDecompControlConstants.SET_LAYER_PROPS11:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's sync parameters.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.ADD_LAYER1;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID5);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+pathname);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    //Add layer
                    case WaveletDecompControlConstants.ADD_LAYER1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add 2nd layer to XSection window #5.\n"+resp.getContent());
                            break;
                        }
                        ids = (ArrayList)resp.getContent();
//already set                        xsecWID5 = (String)ids.get(0);
						layerIDs6[layer6Size++] = (String)ids.get(1);
                        //Make the raw layer the annotated layer if there is no
                        //norm layer
                        if (!datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM))
                            annotatedLayer5 = (String)ids.get(1);
/*
                        actionState = WaveletDecompControlConstants.MOVE_LAYER2;
                        
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.MOVE_LAYER_ACTION);
                        actionParams1.add("winID="+xsecWID5);
                        actionParams1.add("layerID="+(String)ids.get(1));
                        actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                        actionParams1.add("direction=Down");
                        actionParams1.add("value=Bottom");
                        actions.add(actionParams1);
                        
                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
                            
                    case WaveletDecompControlConstants.MOVE_LAYER2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot reposition seismic layer.\n"+resp.getContent());
                            break;
                        }
*/
                    case WaveletDecompControlConstants.SKIP_LAYER2:
                        actionState = WaveletDecompControlConstants.MOVE_LAYER1;
                        
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.MOVE_LAYER_ACTION);
                        actionParams1.add("winID="+xsecWID5);
                        actionParams1.add("layerID="+inputLayerID);
                        actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                        actionParams1.add("direction=Up");
                        actionParams1.add("value=Top");
                        actions.add(actionParams1);
                        
                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
                            
                    case WaveletDecompControlConstants.MOVE_LAYER1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot reposition input layer.\n"+resp.getContent());
                            break;
                        }
                        //SETUP FOR ADD HORIZONS LOOP
                        //Load strategraphic BHP-SU horizons, if any
                        numBhpsuHorizons = gui.getNumberOfBhpsuHorizons();
                        //Note: For Landmark 2D there are no horizons. We cannot
                        //display Landmark 3D horizons.
                        skipHorizons = !gui.isBhpsuHorizonDataset() || numBhpsuHorizons == 0;
                        horizonLayerIdx = 0;
                        horizonLayers.clear(); selectedHorizons.clear();
						if (!skipHorizons) {
                            //Get the paths and names of the BHP-SU horizen(s) used in stratigraphic flattening
                            gui.getBhpsuHorizons(horizonLayers, selectedHorizons);
                        }
                        
                    //Add horizons
                    case WaveletDecompControlConstants.DO_LAYER:
                        actionState = skipHorizons ? WaveletDecompControlConstants.SKIP_LAYER4 : WaveletDecompControlConstants.ADD_LAYER2;
                        
						actions.clear(); actionParams1.clear();
						if (!skipHorizons) {
							actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("winID="+xsecWID5);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+horizonLayers.get(horizonLayerIdx));
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon="+selectedHorizons.get(horizonLayerIdx++));
						} else {
							actions.add("action="+ControlConstants.NOP_ACTION);
                        }
						actions.add(actionParams1);

   						messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
                        
                    //Add horizon layer
                    case WaveletDecompControlConstants.ADD_LAYER2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add horizon layer\n\n"+resp.getContent()+resp.getContent());
                            break;
                        }
                        ids = (ArrayList)resp.getContent();
//already set                        xsecWID5 = (String)ids.get(0);
						layerIDs6[layer6Size++] = horizLayerID = (String)ids.get(1);
                        
                        actionState =  WaveletDecompControlConstants.GET_LAYER_PROP2;
                        
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                        actionParams1.add("layerID="+horizLayerID);
                        actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                        actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                        actionParams1.add("field=FullRange.ep");
                        actions.add(actionParams1);

                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROP2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get horizon's ep full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS12;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerID="+horizLayerID);
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.ep");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerID="+horizLayerID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.cdp");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.add("layerID="+horizLayerID);
                            actionParams3.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.ep");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);
                            
                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams4.add("layerID="+horizLayerID);
                            actionParams4.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams4.add("field=HorizonSetting");
                            actionParams4.add("value="+selectedHorizons.get(horizonLayerIdx-1));
                            actions.add(actionParams4);
                            
                            actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams5.clear();
							actionParams5.add("layerPropsID="+layerPropsID);
							actions.add(actionParams5);
                            
                            actionParams6.clear();
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams6.add("layerID="+horizLayerID);
                            actionParams6.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams6);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

					case WaveletDecompControlConstants.SET_LAYER_PROPS12:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set  horizon's ep chosen range.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROPS13;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerID="+horizLayerID);
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdp");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROPS13:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get horizon's cdp full range.\n"+resp.getContent());
                        } else {
                            ArrayList<Double> cdpRange = getFullRange((String)resp.getContent());
                            
                            actions.clear(); actionParams1.clear();
                            
                            //Check if cdp's incr of this layer differs from the new incr. If so, set the selected range for cdp with the new incr
                            if (cdpRange.get(2).doubleValue() == newIncr) {
                                actionState =  WaveletDecompControlConstants.SKIP_OP1;
                                
                                actions.add("action="+ControlConstants.NOP_ACTION);
                                actions.add(actionParams1);
							} else {
                                actionState = WaveletDecompControlConstants.SET_SELECTED_RANGE1;
                                
                                actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                                actionParams1.add("layerID="+horizLayerID);
                                actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                                actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                                actionParams1.add("field=ChosenRange.cdp");
                                String chosenRange = getSelectedRange(cdpRange.get(0).doubleValue(), cdpRange.get(1).doubleValue(), newIncr);
                                actionParams1.add("value="+chosenRange);
                                actions.add(actionParams1);
                                
                                actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                                actionParams2.clear();
                                actionParams2.add("layerID="+horizLayerID);
                                actionParams2.add("type="+ControlConstants.HORIZON_LAYER);
                                actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                                actions.add(actionParams2);
                            }
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_SELECTED_RANGE1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cdp's selected range for horizon layer\n"+resp.getContent());
                            break;
                        }
                    case WaveletDecompControlConstants.SKIP_OP1:
                        actionState = WaveletDecompControlConstants.SET_LAYER_PROPS13;
							
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                        actionParams1.add("layerID="+horizLayerID);
                        actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                        actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                        actionParams1.add("field=Layer.LayerName");
                        actionParams1.add("value="+selectedHorizons.get(horizonLayerIdx-1));
                        actions.add(actionParams1);
                        
                        actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                        actionParams2.clear();
                        actionParams2.add("layerID="+horizLayerID);
                        actionParams2.add("type="+ControlConstants.HORIZON_LAYER);
                        actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                        actionParams2.add("field=LineSettings.LineWidth");
                        actionParams2.add("value=3");
                        actions.add(actionParams2);
                        
                        actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                        actionParams3.clear();
                        actionParams3.add("layerID="+horizLayerID);
                        actionParams3.add("type="+ControlConstants.HORIZON_LAYER);
                        actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                        actionParams3.add("field=LineSettings.LineColor");
                        actionParams3.add("value=Gray");
                        actions.add(actionParams3);
                
                        actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                        actionParams4.clear();
                        actionParams4.add("layerID="+horizLayerID);
                        actionParams4.add("type="+ControlConstants.HORIZON_LAYER);
                        actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                        actions.add(actionParams4);

                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS13:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set horizon layer's display properties in XSec window #5.\n"+resp.getContent());
                            break;
                        }
                        
                        actionState = WaveletDecompControlConstants.SET_LAYER_PROPS14;

                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                        actionParams1.add("layerID="+horizLayerID);
                        actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                        actionParams1.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                        actionParams1.add("field=Broadcast");
                        actionParams1.add("value=off");
                        actions.add(actionParams1);

                        actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                        actionParams2.clear();
                        actionParams2.add("layerID="+horizLayerID);
                        actionParams2.add("type="+ControlConstants.HORIZON_LAYER);
                        actionParams2.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                        actions.add(actionParams2);

                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
							
                    case WaveletDecompControlConstants.SET_LAYER_PROPS14:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set horizon's sync parameters.\n"+resp.getContent());
                            break;
                        }
/*Horizon is not suppose to be hidden
                        actionState =  WaveletDecompControlConstants.HIDE_LAYER;
                        
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.HIDE_LAYER_ACTION);
                        actionParams1.add("winID="+xsecWID5);
                        actionParams1.add("layerID="+horizLayerID);
                        actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                        actions.add(actionParams1);
                        
                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
						
					//Hide selected horizon layer
                    case WaveletDecompControlConstants.HIDE_LAYER:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot hide horizon layer.\n"+resp.getContent());
                            break;
                        }
*/
                        boolean finished = horizonLayerIdx == selectedHorizons.size();
                        
                        actionState = finished ? WaveletDecompControlConstants.END_LAYER : WaveletDecompControlConstants.DO_LAYER;
                        
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.NOP_ACTION);
                        actions.add(actionParams1);
                        
   						messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
                        
                    case WaveletDecompControlConstants.END_LAYER:
                    case WaveletDecompControlConstants.SKIP_LAYER4:
						actionState = WaveletDecompControlConstants.RESIZE_WINDOW;

						actions.clear(); actionParams1.clear();
						//determine height and width
						xsecHeight5 = 600; xsecWidth5 = 1200;
						//deterimine positon of window next to XSection window #4
						xsecX5 = xsecX4 + xsecWidth4 + 1; xsecY5 = xsecY4;

						actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
						actionParams1.add("winID="+xsecWID5);
						actionParams1.add("width="+xsecWidth5);
						actionParams1.add("height="+xsecHeight5);
						actions.add(actionParams1);

						actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
						actionParams2.clear();
						actionParams2.add("winID="+xsecWID5);
						actionParams2.add("x="+xsecX5);
						actionParams2.add("y="+xsecY5);
						actions.add(actionParams2);

                        /*
						actions.add("action="+ControlConstants.ZOOM_ACTION);
						actionParams3.clear();
						actionParams3.add("winID="+xsecWID5);
						actionParams3.add("direction=All");
						actions.add(actionParams3);
                        */
						messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);

                        break;

                    case WaveletDecompControlConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot resize XSection Window #6.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID5);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=ListenToHorizontalScale");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID5);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToVerticalScale");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID5);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToCursorPosition");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID5);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=ListenToScrollPosition");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);
							
                            actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+xsecWID5);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams5.add("field=SynchronizeVertialScrolling");
                            actionParams5.add("value=on");
                            actions.add(actionParams5);

                            actionParams6.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams6.add("winID="+xsecWID5);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams6.add("field=Broadcast");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams7.clear();
                            actionParams7.add("winID="+xsecWID5);
                            actionParams7.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams7);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                   case WaveletDecompControlConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Synchronization properties for XSection window #5.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_ANNO_PROPS ;
							
							//Must set the annotation layer first.
                            actions.clear(); actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID5);
                            actionParams4.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams4.add("field=AnnotatedLayer");
                            actionParams4.add("value="+annotatedLayer5);
                            actions.add(actionParams4);
                            
                            //Clear the list of horizontal selected keys
                            actionParams6.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams6.add("winID="+xsecWID5);
                            actionParams6.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams6.add("field=Horizontal.SelectedKeys");
                            actionParams6.add("value=");
                            actions.add(actionParams6);

                            actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID5);
                            actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams1.add("field=Horizontal.SelectedKeys");
                            actionParams1.add("value=cdp,ep,cdpt,f2");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID5);
                            actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams2.add("field=Horizontal.SynchronizationKey");
                            actionParams2.add("value=cdp");
                            actions.add(actionParams2);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID5);
                            actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams3.add("field=Horizontal.Location");
                            actionParams3.add("value=TOP");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("winID="+xsecWID5);
                            actionParams5.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Annotation properties.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.WIDEN_SPLIT_PANE;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_DIVIDER_ACTION);
                            actionParams1.add("winID="+xsecWID5);
                            actionParams1.add("splitPane="+ControlConstants.LAYER_EXPLORER);
                            actionParams1.add("dividerLoc=100");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.WIDEN_SPLIT_PANE:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set the divider in XSection window #5.\n"+resp.getContent());
                        } else {
                            stepState = WaveletDecompControlConstants.GET_LAYER_PROPS_WIN7;
							actionState =  WaveletDecompControlConstants.GET_LAYER_PROPS;
							
							//NOTE: The near_wavelet and far_wavelet always exist
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+ datasets.getPathname(WaveletDecompDisplayFiles.INPUT));
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

			//XSec window #6
            case WaveletDecompControlConstants.GET_LAYER_PROPS_WIN7:
                switch (actionState) {
                    case WaveletDecompControlConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for layer in XSection window #6.\n"+resp.getContent());
                        } else {	
                            layerPropsID7[layerProps7Size++] = layerPropsID = (String)resp.getContent();
							
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROPS3;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdp");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROPS3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's cdp full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS4;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdp");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.ep");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.cdp");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);
                            
                            actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams4.clear();
							actionParams4.add("layerPropsID="+layerPropsID);
							actions.add(actionParams4);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS4:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's cdp chosen range.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROPS11;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.ep");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROPS11:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's ep full range.\n"+resp.getContent());
                        } else {
                            //Save the ep full range for the input in case the incr needs to be changed
                            epFullRange = getFullRange((String)resp.getContent());
							
                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS5;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=0.2");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=RMS");
                            actions.add(actionParams2);
/*Not needed for RMS
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value=0");
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value=5");
                            actions.add(actionParams4);
*/
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Wiggle_Trace");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams13.clear();
                            actionParams13.add("layerPropsID="+layerPropsID);
                            actionParams13.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams13.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams13.add("field=RasterizingType.Positive_Fill");
                            actionParams13.add("value=on");
                            actions.add(actionParams13);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
							
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("colormap=rainbow");
                            actions.add(actionParams9);
                            
                            actionParams10.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams10.add("field=Scale.HorizontalScaleMultiplier");
                            actionParams10.add("value=1");
                            actions.add(actionParams10);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Scale.VerticalScaleMultiplier");
                            actionParams11.add("value=1");
                            actions.add(actionParams11);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams12.clear();
                            actionParams12.add("layerPropsID="+layerPropsID);
                            actionParams12.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams12.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams12);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS5:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set seismic layer properties.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS6;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
                    case WaveletDecompControlConstants.SET_LAYER_PROPS6:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's sync parameters.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.CREATE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+ datasets.getPathname(WaveletDecompDisplayFiles.INPUT));
							actionParams1.add("ltype="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
						}
						break;
					
                    case WaveletDecompControlConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot open XSection window #5.\n"+resp.getContent());
                        } else {
                            ids = (ArrayList)resp.getContent();
                            xsecWID6 = (String)ids.get(0);
                            layer7Size = 0;
                            //NOTE: Only the annotation layer if the next added layer does not exist
                            layerIDs7[layer7Size++] = annotatedLayer6 = inputLayerID = (String)ids.get(1);
                            
                            actions.clear(); actionParams1.clear();
                            if (!datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM) &&
                            !datasets.isFilename(WaveletDecompDisplayFiles.CWT)) {
                                actionState = WaveletDecompControlConstants.SKIP_LAYER2;
                                
                                actions.add("action="+ControlConstants.NOP_ACTION);
                            } else
                            if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM)) {
                                pathname = datasets.getPathname(WaveletDecompDisplayFiles.CWT_NORM);
                            
                                actionState = WaveletDecompControlConstants.GET_LAYER_PROPS2;
                                
                                actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                                actionParams1.add("format=BHP-SU");
                                actionParams1.add("file="+pathname);
                            } else {
                                //handle the raw file, if any
                                actionState = WaveletDecompControlConstants.SKIP_LAYER3;
                                
                                actions.add("action="+ControlConstants.NOP_ACTION);
                            }
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                    
                    //Load norm layer
					case WaveletDecompControlConstants.GET_LAYER_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for 2nd layer in XSection window #6.\n"+resp.getContent());
                        } else {
                            layerPropsID7[layerProps7Size++] = layerPropsID =  (String)resp.getContent();
							
							actionState =  WaveletDecompControlConstants.GET_LAYER_PROP;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdpt");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROP:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's cdpt full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS3;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdpt");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.ep");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.cdpt");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set  cube's cdpt chosen range.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROPS4;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdp");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROPS4:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's cdp full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdp");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
                            
                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=Synchronize.cdp");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);
                            
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams3.clear();
							actionParams3.add("layerPropsID="+layerPropsID);
							actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                   case WaveletDecompControlConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set  cube's cdp chosen range.\n"+resp.getContent());
                        } else {
							actionState =  WaveletDecompControlConstants.GET_TRACE_RANGE;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_TRACE_RANGE_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
                    case WaveletDecompControlConstants.GET_TRACE_RANGE:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cdp range values\n"+resp.getContent());
                        } else {
							minMaxTraceVals = (ArrayList<String>)resp.getContent();
                            
							actionState =  WaveletDecompControlConstants.SET_LAYER_PROPS1;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=1.0");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value=0");
                            actions.add(actionParams3);
							
                            int max = ceiling(minMaxTraceVals.get(1));
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+max);
                            actions.add(actionParams4);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Interpolated_Density");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
/*
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams13.clear();
                            actionParams13.add("layerPropsID="+layerPropsID);
                            actionParams13.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams13.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams13.add("field=RasterizingType.Positive_Fill");
                            actionParams13.add("value=on");
                            actions.add(actionParams13);
*/
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=15");
                            actions.add(actionParams8);
							
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("colormap=rainbow");
                            actions.add(actionParams9);
                            
                            actionParams10.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams10.add("field=Scale.HorizontalScaleMultiplier");
                            actionParams10.add("value=1");
                            actions.add(actionParams10);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Scale.VerticalScaleMultiplier");
                            actionParams11.add("value=1");
                            actions.add(actionParams11);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams12.clear();
                            actionParams12.add("layerPropsID="+layerPropsID);
                            actionParams12.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams12.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams12);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set layer's properties.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS2;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
                    case WaveletDecompControlConstants.SET_LAYER_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's sync parameters.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.ADD_LAYER;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID6);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+pathname);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    //Add layer
                    case WaveletDecompControlConstants.ADD_LAYER:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add norm layer to XSection window #6.\n"+resp.getContent());
                            break;
                        }
                        ids = (ArrayList)resp.getContent();
//already set                        xsecWID6 = (String)ids.get(0);
						layerIDs7[layer7Size++] = annotatedLayer6 = (String)ids.get(1);
/*
                        actionState = WaveletDecompControlConstants.MOVE_LAYER1;
                        
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.MOVE_LAYER_ACTION);
                        actionParams1.add("winID="+xsecWID6);
                        actionParams1.add("layerID="+annotatedLayer6);
                        actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                        actionParams1.add("direction=Down");
                        actionParams1.add("value=Bottom");
                        actions.add(actionParams1);
                        
                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
                            
                    case WaveletDecompControlConstants.MOVE_LAYER1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot reposition seismic layer.\n"+resp.getContent());
                            break;
                        }
*/
                    //load raw layer, if any
                    case WaveletDecompControlConstants.SKIP_LAYER3:
                        actions.clear(); actionParams1.clear();
                        if (!datasets.isFilename(WaveletDecompDisplayFiles.CWT)) {
                            actionState = WaveletDecompControlConstants.SKIP_LAYER2;
                            
                            actions.add("action="+ControlConstants.NOP_ACTION);
                        } else {
                            pathname = datasets.getPathname(WaveletDecompDisplayFiles.CWT);
                        
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROPS5;
                            
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+pathname);
                        }
                        actions.add(actionParams1);

                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);

                        break;
                    
                    //Load raw layer
					case WaveletDecompControlConstants.GET_LAYER_PROPS5:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for raw layer in XSection window #6.\n"+resp.getContent());
                        } else {
                            layerPropsID6[layerProps6Size++] = layerPropsID =  (String)resp.getContent();
							
							actionState =  WaveletDecompControlConstants.GET_LAYER_PROP1;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdp");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROP1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's cdp full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS7;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdp");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.ep");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.cdp");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS7:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set  cube's cdp chosen range.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROPS8;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdpt");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROPS8:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's cdpt full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS9;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdpt");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
                            
                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=Synchronize.cdpt");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);
                            
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams3.clear();
							actionParams3.add("layerPropsID="+layerPropsID);
							actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS9:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set  cube's cdpt chosen range.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROPS12;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.ep");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROPS12:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get cube's ep full range.\n"+resp.getContent());
                        } else {
                            ArrayList<Double> epRange = getFullRange((String)resp.getContent());
                            
                            actions.clear(); actionParams1.clear();
                            
                            //Check if ep's incr of this layer differs from the incr of the input layer. If so, set the selected range for ep with the new incr
                            if (epRange.get(2).doubleValue() == epFullRange.get(2).doubleValue()) {
                                actionState =  WaveletDecompControlConstants.SKIP_OP;
                                
                                actions.add("action="+ControlConstants.NOP_ACTION);
                                actions.add(actionParams1);
							} else {
                                actionState = WaveletDecompControlConstants.SET_SELECTED_RANGE;
                                
                                actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                                actionParams1.add("layerID="+inputLayerID);
                                actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                                actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                                actionParams1.add("field=ChosenRange.ep");
                                newIncr = epRange.get(2).doubleValue();
                                String chosenRange = getSelectedRange(epFullRange.get(0).doubleValue(), epFullRange.get(1).doubleValue(), newIncr);
                                actionParams1.add("value="+chosenRange);
                                actions.add(actionParams1);
                                
                                actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                                actionParams2.clear();
                                actionParams2.add("layerID="+inputLayerID);
                                actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                                actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                                actions.add(actionParams2);
                            }
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_SELECTED_RANGE:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set ep's selected range for input layer\n"+resp.getContent());
                            break;
                        }
                    case WaveletDecompControlConstants.SKIP_OP:
						actionState =  WaveletDecompControlConstants.GET_TRACE_RANGE1;
						
						actions.clear(); actionParams1.clear();
						actions.add("action="+ControlConstants.GET_TRACE_RANGE_ACTION);
						actionParams1.add("layerPropsID="+layerPropsID);
						actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
						actions.add(actionParams1);
						
						messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
							
                    case WaveletDecompControlConstants.GET_TRACE_RANGE1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get trace range values\n"+resp.getContent());
                        } else {
							minMaxTraceVals = (ArrayList<String>)resp.getContent();
                            
							actionState =  WaveletDecompControlConstants.SET_LAYER_PROPS10;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=1.0");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value=0");
                            actions.add(actionParams3);
							
                            int max = ceiling(minMaxTraceVals.get(1));
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+max);
                            actions.add(actionParams4);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Interpolated_Density");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
/*
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams13.clear();
                            actionParams13.add("layerPropsID="+layerPropsID);
                            actionParams13.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams13.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams13.add("field=RasterizingType.Positive_Fill");
                            actionParams13.add("value=on");
                            actions.add(actionParams13);
*/
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=15");
                            actions.add(actionParams8);
							
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("colormap=rainbow");
                            actions.add(actionParams9);
                            
                            actionParams10.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams10.add("field=Scale.HorizontalScaleMultiplier");
                            actionParams10.add("value=1");
                            actions.add(actionParams10);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Scale.VerticalScaleMultiplier");
                            actionParams11.add("value=1");
                            actions.add(actionParams11);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams12.clear();
                            actionParams12.add("layerPropsID="+layerPropsID);
                            actionParams12.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams12.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams12);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS10:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set layer's properties.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS11;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
                    case WaveletDecompControlConstants.SET_LAYER_PROPS11:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set cube's sync parameters.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.ADD_LAYER1;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID5);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+pathname);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    //Add layer
                    case WaveletDecompControlConstants.ADD_LAYER1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add raw layer to XSection window #6.\n"+resp.getContent());
                            break;
                        }
                        ids = (ArrayList)resp.getContent();
//already set                        xsecWID6 = (String)ids.get(0);
						layerIDs7[layer7Size++] = (String)ids.get(1);
                        //Make the raw layer the annotated layer if there is no
                        //norm layer
                        if (!datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM))
                            annotatedLayer6 = (String)ids.get(1);
/*
                        actionState = WaveletDecompControlConstants.MOVE_LAYER2;
                        
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.MOVE_LAYER_ACTION);
                        actionParams1.add("winID="+xsecWID6);
                        actionParams1.add("layerID="+(String)ids.get(1));
                        actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                        actionParams1.add("direction=Down");
                        actionParams1.add("value=Bottom");
                        actions.add(actionParams1);
                        
                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
                            
                    case WaveletDecompControlConstants.MOVE_LAYER2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot reposition seismic layer.\n"+resp.getContent());
                            break;
                        }
*/
                    case WaveletDecompControlConstants.SKIP_LAYER2:
                        actionState = WaveletDecompControlConstants.MOVE_LAYER1;
                        
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.MOVE_LAYER_ACTION);
                        actionParams1.add("winID="+xsecWID6);
                        actionParams1.add("layerID="+inputLayerID);
                        actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                        actionParams1.add("direction=Up");
                        actionParams1.add("value=Top");
                        actions.add(actionParams1);
                        
                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
                            
                    case WaveletDecompControlConstants.MOVE_LAYER1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot reposition input layer.\n"+resp.getContent());
                            break;
                        }
                        //SETUP FOR ADD HORIZONS LOOP
                        //Load strategraphic BHP-SU horizons, if any
                        numBhpsuHorizons = gui.getNumberOfBhpsuHorizons();
                        //Note: For Landmark 2D there are no horizons. We cannot
                        //display Landmark 3D horizons.
                        skipHorizons = !gui.isBhpsuHorizonDataset() || numBhpsuHorizons == 0;
                        horizonLayerIdx = 0;
                        horizonLayers.clear(); selectedHorizons.clear();
						if (!skipHorizons) {
                            //Get the paths and names of the BHP-SU horizon(s) used in stratigraphic flattening
                            gui.getBhpsuHorizons(horizonLayers, selectedHorizons);
                        }
                        
                    //Add horizons
                    case WaveletDecompControlConstants.DO_LAYER:
                        actionState = skipHorizons ? WaveletDecompControlConstants.SKIP_LAYER4 : WaveletDecompControlConstants.ADD_LAYER2;
                        
						actions.clear(); actionParams1.clear();
						if (!skipHorizons) {
							actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("winID="+xsecWID6);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+horizonLayers.get(horizonLayerIdx));
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon="+selectedHorizons.get(horizonLayerIdx++));
						} else {
							actions.add("action="+ControlConstants.NOP_ACTION);
                        }
						actions.add(actionParams1);

   						messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
                        
                    //Add horizon layer
                    case WaveletDecompControlConstants.ADD_LAYER2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add horizon layer\n\n"+resp.getContent()+resp.getContent());
                            break;
                        }
                        ids = (ArrayList)resp.getContent();
//already set                        xsecWID6 = (String)ids.get(0);
						layerIDs7[layer7Size++] = horizLayerID = (String)ids.get(1);
                        
                        actionState =  WaveletDecompControlConstants.GET_LAYER_PROP2;
                        
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                        actionParams1.add("layerID="+horizLayerID);
                        actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                        actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                        actionParams1.add("field=FullRange.cdp");
                        actions.add(actionParams1);

                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROP2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get horizon's cdp full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = WaveletDecompControlConstants.SET_LAYER_PROPS12;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerID="+horizLayerID);
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdp");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerID="+horizLayerID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.ep");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.add("layerID="+horizLayerID);
                            actionParams3.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.cdp");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);
                            
                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams4.add("layerID="+horizLayerID);
                            actionParams4.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams4.add("field=HorizonSetting");
                            actionParams4.add("value="+selectedHorizons.get(horizonLayerIdx-1));
                            actions.add(actionParams4);
                            
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams5.clear();
							actionParams5.add("layerPropsID="+layerPropsID);
							actions.add(actionParams5);
                            
                            actionParams6.clear();
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams6.add("layerID="+horizLayerID);
                            actionParams6.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams6);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

					case WaveletDecompControlConstants.SET_LAYER_PROPS12:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set  horizon's cdp chosen range.\n"+resp.getContent());
                            break;
                        } else {
                            actionState = WaveletDecompControlConstants.GET_LAYER_PROPS13;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerID="+horizLayerID);
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.ep");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.GET_LAYER_PROPS13:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get horizon's ep full range.\n"+resp.getContent());
                        } else {
                            ArrayList<Double> epRange = getFullRange((String)resp.getContent());
                            
                            actions.clear(); actionParams1.clear();
                            
                            //Check if ep's incr of this layer differs from the new incr. If so, set the selected range for cdp with the new incr
                            if (epRange.get(2).doubleValue() == newIncr) {
                                actionState =  WaveletDecompControlConstants.SKIP_OP1;
                                
                                actions.add("action="+ControlConstants.NOP_ACTION);
                                actions.add(actionParams1);
							} else {
                                actionState = WaveletDecompControlConstants.SET_SELECTED_RANGE1;
                                
                                actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                                actionParams1.add("layerID="+horizLayerID);
                                actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                                actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                                actionParams1.add("field=ChosenRange.ep");
                                String chosenRange = getSelectedRange(epRange.get(0).doubleValue(), epRange.get(1).doubleValue(), newIncr);
                                actionParams1.add("value="+chosenRange);
                                actions.add(actionParams1);
                                
                                actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                                actionParams2.clear();
                                actionParams2.add("layerID="+horizLayerID);
                                actionParams2.add("type="+ControlConstants.HORIZON_LAYER);
                                actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                                actions.add(actionParams2);
                            }
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case WaveletDecompControlConstants.SET_SELECTED_RANGE1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set ep's selected range for horizon layer\n"+resp.getContent());
                            break;
                        }
                    case WaveletDecompControlConstants.SKIP_OP1:
                        actionState = WaveletDecompControlConstants.SET_LAYER_PROPS13;
							
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                        actionParams1.add("layerID="+horizLayerID);
                        actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                        actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                        actionParams1.add("field=Layer.LayerName");
                        actionParams1.add("value="+selectedHorizons.get(horizonLayerIdx-1));
                        actions.add(actionParams1);
                        
                        actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                        actionParams2.clear();
                        actionParams2.add("layerID="+horizLayerID);
                        actionParams2.add("type="+ControlConstants.HORIZON_LAYER);
                        actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                        actionParams2.add("field=LineSettings.LineWidth");
                        actionParams2.add("value=3");
                        actions.add(actionParams2);
                        
                        actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                        actionParams3.clear();
                        actionParams3.add("layerID="+horizLayerID);
                        actionParams3.add("type="+ControlConstants.HORIZON_LAYER);
                        actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                        actionParams3.add("field=LineSettings.LineColor");
                        actionParams3.add("value=Gray");
                        actions.add(actionParams3);
                
                        actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                        actionParams4.clear();
                        actionParams4.add("layerID="+horizLayerID);
                        actionParams4.add("type="+ControlConstants.HORIZON_LAYER);
                        actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                        actions.add(actionParams4);

                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
                        
                    case WaveletDecompControlConstants.SET_LAYER_PROPS13:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set horizon layer's display properties in XSec window #6.\n"+resp.getContent());
                            break;
                        }
                        
                        actionState = WaveletDecompControlConstants.SET_LAYER_PROPS14;

                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                        actionParams1.add("layerID="+horizLayerID);
                        actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                        actionParams1.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                        actionParams1.add("field=Broadcast");
                        actionParams1.add("value=off");
                        actions.add(actionParams1);

                        actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                        actionParams2.clear();
                        actionParams2.add("layerID="+horizLayerID);
                        actionParams2.add("type="+ControlConstants.HORIZON_LAYER);
                        actionParams2.add("props="+ControlConstants.LAYER_SYNC_PROPERTIES);
                        actions.add(actionParams2);

                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
							
                    case WaveletDecompControlConstants.SET_LAYER_PROPS14:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set horizon's sync parameters.\n"+resp.getContent());
                            break;
                        }
/* Horizon is not suppose to be hidden
                        actionState =  WaveletDecompControlConstants.HIDE_LAYER;
                        
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.HIDE_LAYER_ACTION);
                        actionParams1.add("winID="+xsecWID6);
                        actionParams1.add("layerID="+horizLayerID);
                        actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                        actions.add(actionParams1);
                        
                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
						
					//Hide selected horizon layer
                    case WaveletDecompControlConstants.HIDE_LAYER:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot hide horizon layer.\n"+resp.getContent());
                            break;
                        }
*/
                        boolean finished = horizonLayerIdx == selectedHorizons.size();
                        
                        actionState = finished ? WaveletDecompControlConstants.END_LAYER : WaveletDecompControlConstants.DO_LAYER;
                        
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.NOP_ACTION);
                        actions.add(actionParams1);
                        
   						messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
                        
                    case WaveletDecompControlConstants.END_LAYER:
                    case WaveletDecompControlConstants.SKIP_LAYER4:						
						actionState = WaveletDecompControlConstants.RESIZE_WINDOW;

						actions.clear(); actionParams1.clear();
						//determine height and width
						xsecHeight6 = 600; xsecWidth6 = 1200;
						//deterimine positon of window next to XSection window #5
						xsecX6 = xsecX5 + xsecWidth5 + 1; xsecY6 = xsecY5;

						actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
						actionParams1.add("winID="+xsecWID6);
						actionParams1.add("width="+xsecWidth6);
						actionParams1.add("height="+xsecHeight6);
						actions.add(actionParams1);

						actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
						actionParams2.clear();
						actionParams2.add("winID="+xsecWID6);
						actionParams2.add("x="+xsecX6);
						actionParams2.add("y="+xsecY6);
						actions.add(actionParams2);

                        /*
						actions.add("action="+ControlConstants.ZOOM_ACTION);
						actionParams3.clear();
						actionParams3.add("winID="+xsecWID6);
						actionParams3.add("direction=All");
						actions.add(actionParams3);
                        */
                        actions.add("action="+ControlConstants.RESIZE_VIEWER_ACTION);
                        actionParams4.clear();

                        int w1 = mapWidth1 + xsecWidth1 + xsecWidth2 + xsecWidth3;
                        int w2 = xsecWidth4 + xsecWidth5 + xsecWidth6;
                        viewerWidth += w1>w2 ? w1 : w2;

                        int h1 = xsecY4 + xsecHeight4;
                        int h2 = xsecY5 + xsecHeight5;
                        int h3 = xsecY6 + xsecHeight6;
                        int maxh = h1>h2 ? h1 : h2;
                        viewerHeight += maxh>h3 ? maxh : h3;

                        actionParams4.add("width="+viewerWidth);
                        actionParams4.add("height="+viewerHeight);
                        actions.add(actionParams4);

						messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);

                        break;

                    case WaveletDecompControlConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot resize XSection Window #6.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID6);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=ListenToHorizontalScale");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID6);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToVerticalScale");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID6);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToCursorPosition");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID6);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=ListenToScrollPosition");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);
							
                            actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+xsecWID6);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams5.add("field=SynchronizeVertialScrolling");
                            actionParams5.add("value=on");
                            actions.add(actionParams5);

                            actionParams6.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams6.add("winID="+xsecWID6);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams6.add("field=Broadcast");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams7.clear();
                            actionParams7.add("winID="+xsecWID6);
                            actionParams7.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams7);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                   case WaveletDecompControlConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Synchronization properties for XSection window #5.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.SET_ANNO_PROPS ;

							//Must set the annotation layer first.
                            actions.clear(); actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID6);
                            actionParams4.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams4.add("field=AnnotatedLayer");
                            actionParams4.add("value="+annotatedLayer6);
                            actions.add(actionParams4);
                            
                            //Clear the list of horizontal selected keys
                            actionParams6.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams6.add("winID="+xsecWID6);
                            actionParams6.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams6.add("field=Horizontal.SelectedKeys");
                            actionParams6.add("value=");
                            actions.add(actionParams6);
							
							actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID6);
                            actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams1.add("field=Horizontal.SelectedKeys");
                            actionParams1.add("value=cdp,ep,cdpt,f2");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID6);
                            actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams2.add("field=Horizontal.SynchronizationKey");
                            actionParams2.add("value=ep");
                            actions.add(actionParams2);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID6);
                            actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams3.add("field=Horizontal.Location");
                            actionParams3.add("value=TOP");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("winID="+xsecWID6);
                            actionParams5.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Annotation properties.\n"+resp.getContent());
                        } else {
                            actionState = WaveletDecompControlConstants.WIDEN_SPLIT_PANE;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_DIVIDER_ACTION);
                            actionParams1.add("winID="+xsecWID6);
                            actionParams1.add("splitPane="+ControlConstants.LAYER_EXPLORER);
                            actionParams1.add("dividerLoc=100");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case WaveletDecompControlConstants.WIDEN_SPLIT_PANE:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set the divider in XSection window #5.\n"+resp.getContent());
                        } else {
							actionState = WaveletDecompControlConstants.END_PROGRESS_DIALOG;
							
							actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.END_PROGRESS_DIALOG_ACTION);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case WaveletDecompControlConstants.END_PROGRESS_DIALOG:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot end process dialog\n"+resp.getContent());
                        } else {
                            stepState = ControlConstants.SESSION_STEP;
                            actionState = ControlConstants.END_SESSION;

                            actions.clear(); actionParams1.clear();
                            //End the control session
                            actions.add("action="+ControlConstants.END_CONTROL_SESSION_ACTION);
                            actionParams1.add("sessionID="+sessionID);
                            actions.add(actionParams1);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

            default:
                //Internal error: No legal step transition state
                //End the control session
                endControlSession("Internal Error: No legal step transition for state "+actionState);
        }
    }
	
	/**
	 * End the control session because of an error. Close the progress dialog.
	 * @param errorMsg The reason why the control session is being terminated.
	 */
	private void endControlSession(String errorMsg) {
		sessionTerminated = true;
		
		//Display reason ending the control session
		JOptionPane.showMessageDialog(gui, errorMsg+"\nCannot continue displaying the results.", "Display Error", JOptionPane.ERROR_MESSAGE);
		
		//End the control session
        stepState = ControlConstants.SESSION_STEP;
        actionState = ControlConstants.END_SESSION;
		
		actions.clear(); actionParams1.clear();
		actions.add("action="+ControlConstants.END_PROGRESS_DIALOG_ACTION);
		actions.add(actionParams1);
		actions.add("action="+ControlConstants.END_CONTROL_SESSION_ACTION);
		actionParams1.add("sessionID="+sessionID);
		actions.add(actionParams1);
		
		messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
	}
    
    /**
     * Initialize the transducer's state variables. Invoked when displaying
     * results is ended so the transducer can be called again.
     */
    private void initState() {
        xsecWID1 = ""; xsecWID2 = ""; xsecWID3 = ""; xsecWID4 = "";
        xsecWID5 = ""; xsecWID6 = ""; mapWID1 = "";
        xsecWidth1 = 0; xsecHeight1 = 0; xsecWidth2 = 0; xsecHeight2 = 0;
        xsecWidth3 = 0; xsecHeight3 = 0; xsecWidth4 = 0; xsecHeight4 = 0;
        xsecWidth5 = 0; xsecHeight5 = 0; xsecWidth6 = 0; xsecHeight6 = 0;
        mapWidth1 = 0; mapHeight1 = 0;
        xsecX1 = 0; xsecY1 = 0; xsecX2 = 0; xsecY2 = 0; xsecX3 = 0; xsecY3 = 0;
        xsecX4 = 0; xsecY4 = 0; xsecX5 = 0; xsecY5 = 0; xsecX6 = 0; xsecY6 = 0;
        mapX1 = 0; mapY1 = 0;
        viewerWidth = 50; viewerHeight = 50;
        layer1Size = 0;
        layerProps1Size = 0;
        layer2Size = 0;
        layerProps2Size = 0;
        layer3Size = 0;
        layerProps3Size = 0;
        layer4Size = 0;
        layerProps4Size = 0;
        layer5Size = 0;
        layerProps5Size = 0;
        layer6Size = 0;
        layerProps6Size = 0;
        layer7Size = 0;
        layerProps7Size = 0;
        fileExists = "yes";
        skipLayer = false;
        horizonLayerIdx = 0;
        horizLayerID = "";
        annotatedLayer1 = ""; annotatedLayer2 = ""; annotatedLayer3 = "";
        annotatedLayer4 = ""; annotatedLayer5 = ""; annotatedLayer6 = "";
        numBhpsuHorizons = 0;
        skipHorizons = true;
            
        stepState = ControlConstants.SESSION_STEP;
        actionState = ControlConstants.START_SESSION;
        
		//Check is forced control session to terminate (due to an error or the
		//user cancelled). If so, the response needs to be ignored.
        ignoreResponse = sessionTerminated ? true : false;
     }
	
	/**
	 * Round up the trace value to the nearest 100
	 * @param tval Trace value as a decimal number
	 * @return Integer trace value rounded up to the nearest 100
	 */
	private int ceiling(String tval) {
		int val = 0;
		try {
			val = Float.valueOf(Float.parseFloat(tval)).intValue();
		} catch (NumberFormatException nef) {
		}
		
		int upper = (val / 100) * 100;
		int rem = val % 100;
		upper += (rem != 0 && val > 0) ? 100 : 0;
		return upper;
	}
	 
	/**
	 * Round down the trace value to the nearest 100
	 * @param tval Trace value as a decimal number
	 * @return Integer trace value rounded down to the nearest 100
	 */
	private int floor(String tval) {
		int val = 0;
		try {
			val = Float.valueOf(Float.parseFloat(tval)).intValue();
		} catch (NumberFormatException nef) {
		}
		
		int lower = (val / 100) * 100;
		int rem = val % 100;
		lower += (rem != 0 && val < 0) ? -100 : 0;
		return lower;
	}
	
	/**
	 * Get the abs max of two integer value either of which can be negative.
	 */
	 private int absMax(int n, int m) {
		 int i1 = n < 0 ? -n : n;
		 int i2 = m < 0 ? -m : m;
		 return i1 < i2 ? i2 : i1;
	 }
	 
	/**
	 * Get the midpoint of a key's full range. The full range is a single
	 * value when input is Landmark 2D, i.e., the ep for a single line is a
	 * single value; otherwise, it is of the form min-max[incr]
	 * @param fullRange Key's full range
	 * @return Midpoint of the full range.
	 */
	Double getMidpoint(String fullRange) {
		double midpoint = 0, min = 0, max = 0, incr = 1;
		
		//Parse the full range
		try {
			//Check if full range a single value
			if (fullRange.indexOf("-") == -1) {
				return Double.parseDouble(fullRange);
			}
				
			//full range: min-max[incr]
			String[] fr1 = fullRange.split("-");
			String[] fr2 = fr1[1].split("\\[");
			String[] fr3 = fr2[1].split("\\]");

			min = Double.parseDouble(fr1[0]);
			max = Double.parseDouble(fr2[0]);
			incr = Double.parseDouble(fr3[0]);
			
			midpoint = (max - min)/2;
		} catch (NumberFormatException nfe) {
		}
		
		//Adjust midpoint by taking incr into account
		return min + (midpoint - (midpoint % incr));
	}
    
    /**
     * Get a key's full range. The full range is a single
	 * value when input is Landmark 2D, i.e., the ep for a single line is a
	 * single value; otherwise, it is of the form min-max[incr]
	 * @param fullRange Key's full range as a string
     * @return Full range as a list of doubles in the order: min, max, inrc
     */
    private ArrayList<Double> getFullRange(String fullRange) {
		double min = 0, max = 0, incr = 1;
		
		//Parse the full range
		try {
			//Check if full range a single value
			if (fullRange.indexOf("-") == -1) {
				min = max = Double.parseDouble(fullRange);
			}
				
			//full range: min-max[incr]
			String[] fr1 = fullRange.split("-");
			String[] fr2 = fr1[1].split("\\[");
			String[] fr3 = fr2[1].split("\\]");

			min = Double.parseDouble(fr1[0]);
			max = Double.parseDouble(fr2[0]);
			incr = Double.parseDouble(fr3[0]);
		} catch (NumberFormatException nfe) {
		}
        
        //form the list
        ArrayList<Double> range = new ArrayList<Double>(3);
        range.add(Double.valueOf(min));
        range.add(Double.valueOf(max));
        range.add(Double.valueOf(incr));
        
        return range;
    }
    
    /**
     * Get key's selected range
     * @param min Minimum value
     * @param max Maximum value
     * @param incr Increment value
     * @return String representation of the range: min-max[incr]
     */
    private String getSelectedRange(Double min, Double max, Double incr) {
        return Double.toString(min) + "-" + Double.toString(max) + "[" + Double.toString(incr) + "]";
    }
     
    /**
     * Determine the number of windows that will be created.
     * @param datasets Datasets in the results.
     */
    private int numWindows(WaveletDecompDisplayFiles datasets) {
        int numWins = 0;
        //Map window
        if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM_SLICE_TRANSPOSE_EP))
            numWins++;
        else if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_SLICE_TRANSPOSE_EP))
            numWins++;
        //XSec #1
        if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM_SLICE))
            numWins++;
        else if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_SLICE))
            numWins++;
        //XSec #2
        if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM_SLICE))
            numWins++;
        else if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_SLICE))
            numWins++;
        //XSec #3
        if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM_SLICE))
            numWins++;
        else if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_SLICE))
            numWins++;
        //XSec #4
        if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM))
            numWins++;
        else if (datasets.isFilename(WaveletDecompDisplayFiles.CWT))
            numWins++;
        //XSec #5
        numWins++;
        //XSec #6
        numWins++; 
        
        return numWins;
    }
    
    /**
     * Determine the number of layers that will be created for each window that
     * will be created.
     * @param datasets Datasets in the results.
     * @param numWins Number of windows that will be created.
     * @param numHorizons Number of horizons to be displayed
     * @return vector of layer counts whose size is the number of windows that
     * will be created.
     */
     private int[] numLayers(WaveletDecompDisplayFiles datasets, int numWins, int numHorizons) {
         int[] layerCnts = new int[numWins];
         int idx = -1;
         
        //Map window
        if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM_SLICE_TRANSPOSE_EP) || 
        datasets.isFilename(WaveletDecompDisplayFiles.CWT_SLICE_TRANSPOSE_EP)) {
            idx++;
            layerCnts[idx]++;
        }
        //XSec #1
        if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM_SLICE) || 
        datasets.isFilename(WaveletDecompDisplayFiles.CWT_SLICE)) {
            idx++;
            layerCnts[idx]++;
        }
        //XSec #2
        if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM_SLICE) || 
            datasets.isFilename(WaveletDecompDisplayFiles.CWT_SLICE)) {
            idx++;
            layerCnts[idx]++;
        }
        //XSec #3
        if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM_SLICE) || 
            datasets.isFilename(WaveletDecompDisplayFiles.CWT_SLICE)) {
            idx++;
            layerCnts[idx]++;
        }
        //XSec #4
        if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM) || 
            datasets.isFilename(WaveletDecompDisplayFiles.CWT)) {
            idx++;
            layerCnts[idx]++;
        }
        //XSec #5
        idx++;  //Input is always added
        layerCnts[idx]++;
        if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM))
            layerCnts[idx]++;
        if (datasets.isFilename(WaveletDecompDisplayFiles.CWT))
            layerCnts[idx]++;
        layerCnts[idx] += numHorizons;
        //XSec #6
        idx++;  //Input is always added
        layerCnts[idx]++;
        if (datasets.isFilename(WaveletDecompDisplayFiles.CWT_NORM))
            layerCnts[idx]++;
        if (datasets.isFilename(WaveletDecompDisplayFiles.CWT))
            layerCnts[idx]++;
        layerCnts[idx] += numHorizons;
         
         return layerCnts;
     }
}
