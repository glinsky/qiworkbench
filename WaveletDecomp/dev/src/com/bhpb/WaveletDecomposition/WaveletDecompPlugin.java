/*
 ###########################################################################
 # WaveletDecomp - A continuous time-frequency analysis technique that
 # computes frequency spectrum for each time sample of a seismic trace.
 # Copyright (C) 2006  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */


package com.bhpb.WaveletDecomposition;

import java.awt.Component;
import java.io.IOException;
import java.nio.channels.Pipe;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.SynchronousQueue;
import java.util.logging.Logger;

import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.util.QiFileChooserDescriptor;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ComponentStateUtils;
import com.bhpb.qiworkbench.compAPI.ControlConstants;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.ProgressUtil;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.compAPI.XmlUtils;
import com.bhpb.services.landmark.LandmarkServices;
import com.bhpb.services.landmark.jni.LandmarkProjectHandler;
import com.bhpb.services.landmark.jni.LocalWriteHorizon;
import com.bhpb.qiworkbench.util.ExceptionMessageFormatter;
import com.bhpb.qiworkbench.workbench.QiComponentBase;
import com.bhpb.WaveletDecomposition.displayResults.WaveletDecompDisplayTransducer;

/**
 * Wavelet Decomposition plugin, a BHP propritary qiComponent. It is
 * started as a thread by the Messenger Dispatcher that executes the
 * "Activate plugin" command.
 */
public class WaveletDecompPlugin extends QiComponentBase implements IqiWorkbenchComponent, Runnable {
    private static Logger logger = Logger.getLogger(WaveletDecompPlugin.class.getName());

    // messaging mgr for this class only
    private MessagingManager messagingMgr;

    // gui component
    private WaveletDecompGUI gui;

    // my thread
    private static Thread pluginThread;

    /** CID for component instance. Generated before the thread is started and carried as the thread's name. */
    private String myCID = "";

	// boolean used to stop plugin thread
	private boolean pleaseStop = false;

	private static int saveAsCount = 0;
	//private LandmarkProjectHandler projectHandler;
	private LandmarkServices landmarkAgent;
	private SimpleDateFormat formatter = new SimpleDateFormat(
			"yyyy_MM_dd_HH_mm_ss_SSS");
	protected boolean horizonExportDone = false;

	protected String getCID() {
		return myCID;
	}


    public IComponentDescriptor getComponentDescriptor(){
        return messagingMgr.getMyComponentDesc();
    }

    /** Component descriptor of associated qiProject Manager. null means no association with a PM has been made. */
    private ComponentDescriptor projMgrDesc = null;

    /** Metadata of project associated with */
    private QiProjectDescriptor qiProjectDesc = new QiProjectDescriptor();

    public QiProjectDescriptor getQiProjectDescriptor() {
        return qiProjectDesc;
    }
	
    /** Display step */
    private int stepState = ControlConstants.SESSION_STEP;
    /** Step action */
    private int actionState = ControlConstants.START_SESSION;
    /** Finite state machine for displaying the results of a generated script */
    private WaveletDecompDisplayTransducer displayTransducer ;

    /**
     * Initialize the plugin component:
     * <ul>
     * <li>Create its messaging manager</li>
     * <li>Register with the Message Dispatcher</li>
     * </ul>
     */
    public void init() {
        try {
            QIWConstants.SYNC_LOCK.lock();
            messagingMgr = new MessagingManager();

            myCID = Thread.currentThread().getName();

            // register self with the Message Dispatcher
            messagingMgr.registerComponent(QIWConstants.PLUGIN_AGENT_COMP, WaveletDecompConstants.SPEC_DECOMP_PLUGIN_NAME, myCID);
            QIWConstants.SYNC_LOCK.unlock();
            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in WaveletDecompPlugin.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

    public List<String> getUnmadeQiProjDescDirs(){
        List params = new ArrayList();
        params.add(messagingMgr.getLocationPref());
        params.add(qiProjectDesc);
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_UNMADE_QIPROJ_DESC_DIRS_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);

        if (resp == null){
            logger.info("Response returning null from command GET_UNMADE_QIPROJ_DESC_DIRS_CMD. Consult qiWorkbench technical support.");
            return null;
        }else if(resp.isAbnormalStatus()){
            logger.info("Abnormal response from GET_UNMADE_QIPROJ_DESC_DIRS_CMD command:  in getting unmade directories that do not yet exists to meet QiProject's meta data specification. " + (String)resp.getContent());
            JOptionPane.showMessageDialog(gui, "Error in getting directories that do not yet exist to meet QiProject's meta data specification." + " cause: " +  (String)resp.getContent(), "QI Workbench",
                JOptionPane.WARNING_MESSAGE);
            return null;
        } else{
            return (List<String>)(resp.getContent());
        }
    }

    public boolean makeQiProjDescDirs(List<String> list){
        if(list == null || list.size() == 0)
            return true;

        List params = new ArrayList();
        params.add(messagingMgr.getLocationPref());
        params.add(list);
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.CREATE_DIRECTORIES_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);

        if (resp == null){
            logger.info("Response returning null from command CREATE_DIRECTORIES_CMD. Consult qiWorkbench technical support.");
            return false;
        }else if(resp.isAbnormalStatus()){
            logger.info("Abnormal response from CREATE_DIRECTORIES_CMD command:  in getting unmade directories that do not yet exists to meet QiProject's meta data specification. " + (String)resp.getContent());
            JOptionPane.showMessageDialog(gui, "Error in getting directories that do not yet exist to meet QiProject's meta data specification." + " cause: " +  (String)resp.getContent(), "QI Workbench",
                JOptionPane.WARNING_MESSAGE);
            return false;
        } else{
            logger.info("Normal response from CREATE_DIRECTORIES_CMD command:  " + (String)resp.getContent());
            String result = (String)resp.getContent();
            if(result.equals("success"))
                    return true;
            return false;
        }
    }

    /**
     * Generate state information into xml string format
     * @return  String
     */
    public String genState(){
        StringBuffer content = new StringBuffer();
        IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
        //TODO WorkbenchStateManager will need to be changed to conform to the package change of WaveletDecompPlugin in ServletDispatcher
        content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc) + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + CompDescUtils.getDescPreferredDisplayName(desc) + "\">\n");
        content.append(gui.genState());
        content.append("</component>\n");
        return content.toString();
    }

    /**
     * Generate state information into xml string format in repsonse to save as command
     * @return  String
     */
    public String genStateAsClone(){
        StringBuffer content = new StringBuffer();
        IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
        //TODO WorkbenchStateManager will need to be changed to conform to the package change of the plugin in ServletDispatcher
        String displayName = "";
        saveAsCount++;
        if(saveAsCount == 1)
            displayName = "CopyOf" + CompDescUtils.getDescPreferredDisplayName(desc);
        else
            displayName = "Copy(" + saveAsCount + ")Of" + CompDescUtils.getDescPreferredDisplayName(desc);
        content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc) + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + displayName + "\">\n");
        content.append(gui.genState());
        content.append("</component>\n");

        return content.toString();
    }

	///public List<ProjectInfo> getLandmarkAllProjects(){
	//	return landmarkAllProjects;
	//}
/*
	private List<ProjectInfo> landmarkAllProjects;
	public boolean populateLandmarkProjects(){
		if(initLandmarkProjectHandler()){
			try{
			landmarkAllProjects = new ArrayList<ProjectInfo>();
			String [] prjs = landmarkAgent.getLandmarkProjectHandler().getProjects(3);
			for(int i = 0; i < prjs.length; i++){
				ProjectInfo p = new ProjectInfo(prjs[i],3);
				landmarkAllProjects.add(p);
			}

			prjs = landmarkAgent.getLandmarkProjectHandler().getProjects(2);
			for(int i = 0; i < prjs.length; i++){
				ProjectInfo p = new ProjectInfo(prjs[i],2);
				landmarkAllProjects.add(p);
			}
			return true;
			}catch(UnsatisfiedLinkError e){
				e.printStackTrace();
				JOptionPane.showMessageDialog(gui,e.getMessage() + ". Contact workbench support.",
						"Linking Error",JOptionPane.WARNING_MESSAGE);
				return false;
			}catch(Exception e){
				e.printStackTrace();
				JOptionPane.showMessageDialog(gui,e.getMessage() + ". Contact workbench support.",
						"Linking Error",JOptionPane.WARNING_MESSAGE);
				return false;
			}
		}else
			return false;
	}

	*/
	/*public String[] getLandmarkProjects(int type){
		String[] out = new String[0];
		if(initLandmarkProjectHandler()){
			try{
			out = landmarkAgent.getLandmarkProjectHandler().getProjects(type);
			}catch(UnsatisfiedLinkError e){
				e.printStackTrace();
				JOptionPane.showMessageDialog(gui,e.getMessage() + ". Contact workbench support.",
						"Linking Error",JOptionPane.WARNING_MESSAGE);
			}catch(Exception e){
				e.printStackTrace();
				JOptionPane.showMessageDialog(gui,e.getMessage() + ". Contact workbench support.",
						"Linking Error",JOptionPane.WARNING_MESSAGE);
			}
		}
		return out;
	}
*/
	private void printClassLoaderName(String context,ClassLoader loader){
		if(loader != null){
			System.out.println("context " + context + " loader name " + loader.getClass().getName());
			printClassLoaderName(context,loader.getParent());
		}
		return;
	}
	/*
	public boolean initLandmarkProjectHandler(){
		if(landmarkAgent == null){
			try{
				//AHelpers.loadLandmarkJNILibraries();

				landmarkAgent = LandmarkServices.getInstance();
			}catch(UnsatisfiedLinkError e){
				e.printStackTrace();
				JOptionPane.showMessageDialog(gui,e.getMessage() + ". Contact workbench support.",
						"Linking Error",JOptionPane.WARNING_MESSAGE);
				return false;
			}catch(NoClassDefFoundError e){
				e.printStackTrace();
				JOptionPane.showMessageDialog(gui,"Problem in getting the library for handling Landmark projects. Contact workbench support.",
						"Linking Error",JOptionPane.WARNING_MESSAGE);
				return false;
			}catch(Exception e){
				e.printStackTrace();
				JOptionPane.showMessageDialog(gui,"Problem in getting the library for handling Landmark projects. Contact workbench support.",
						"Linking Error",JOptionPane.WARNING_MESSAGE);
				return false;
			}
		}
		return true;
	}
*/
	/*
	public int[][] getLandmarkLineTraceInfo(String project, int type, String volume){
		int[][] out = new int[0][0];
		if(initLandmarkProjectHandler())
			out = landmarkAgent.getLandmarkProjectHandler().getLineTraceInfo(project, type, volume);
		return out;
	}

	public float [] getLandmarkTimeRangeInfo(String project, int type, String volume){
		float[] out = new float[0];
		if(initLandmarkProjectHandler())
			out = landmarkAgent.getLandmarkProjectHandler().getTimeRangeInfo(project, type, volume);
		return out;
	}
	*/
	/*
	public List<String> getLandmark3DProjects(){
		List<String> projects = null;
		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_LANDMARK_3D_PROJECTS_CMD,true);
		QiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null){
			logger.warning("Respone to get file chooser service returning null due to timed out");
		}
		else if(MsgUtils.isResponseAbnormal(response)) {
			logger.warning("Internal error occurring in getting file chooser service. Cause: " + MsgUtils.getMsgContent(response));
		}
		else
			projects = (List<String>)MsgUtils.getMsgContent(response);
		return projects;
	}
*/
    public QiProjectDescriptor getProjectDescriptor(){
        return qiProjectDesc;
    }


    /** restore plugin and it's gui to previous condition
     *
     * @param node xml containing saved state variables
     */
    private void restoreProjectEnv(Node node) {
        //QiProjectDescriptor desc = new QiProjectDescriptor();
        NodeList children = node.getChildNodes();
        for(int i = 0; i < children.getLength(); i++){
            Node child = children.item(i);
            if(child.getNodeType() == Node.ELEMENT_NODE){
                if(child.getNodeName().equals(WaveletDecompGUI.class.getName())){
                    String project = ((Element)child).getAttribute("project_name");
                    if(project != null && project.trim().length() > 0)
                        QiProjectDescUtils.setQiProjectName(qiProjectDesc, project);
                    String fileSystem = ((Element)child).getAttribute("qiSpace");
                        if(fileSystem != null && fileSystem.trim().length() > 0)
                        QiProjectDescUtils.setQiSpace(qiProjectDesc, fileSystem);

                    String qiProjReloc = ((Element)child).getAttribute("project_location");
                        if(qiProjReloc != null && qiProjReloc.trim().length() > 0)
                        QiProjectDescUtils.setQiProjectReloc(qiProjectDesc, qiProjReloc);

                    String datasets = ((Element)child).getAttribute("datasets_dir");
                    if(datasets != null && datasets.trim().length() > 0)
                        QiProjectDescUtils.setDatasetsReloc(qiProjectDesc, datasets);

                    String horizons = ((Element)child).getAttribute("horizons_dir");
                    if(horizons != null && horizons.trim().length() > 0)
                        QiProjectDescUtils.setHorizonsReloc(qiProjectDesc, horizons);

                    String savesets = ((Element)child).getAttribute("savesets_dir");
                    if(savesets != null && savesets.trim().length() > 0)
                    QiProjectDescUtils.setSavesetsReloc(qiProjectDesc, savesets);

                    String welllog = ((Element)child).getAttribute("welllog_dir");
                    if(welllog != null && welllog.trim().length() > 0)
                        QiProjectDescUtils.setWellsReloc(qiProjectDesc, welllog);

                    String scripts = ((Element)child).getAttribute("scripts_dir");
                    if(scripts != null && scripts.trim().length() > 0)
                        QiProjectDescUtils.setScriptsReloc(qiProjectDesc, scripts);

                    String seismics = ((Element)child).getAttribute("seismic_dirs");
                    if(seismics != null && seismics.trim().length() > 0){
                        String [] s = seismics.split(",");
                        ArrayList<String> dirList = new ArrayList<String>();
                        for(int j = 0; s != null && j < s.length; j++){
                            dirList.add(s[j]);
                        }
                        QiProjectDescUtils.setSeismicDirs(qiProjectDesc, dirList);
                    }
                }
            }
        }
        return;
    }

    /** restore plugin and it's gui to previous condition
     *
     * @param node xml containing saved state variables
     */
    void restoreState(Node node){
        String preferredDisplayName = ((Element)node).getAttribute("preferredDisplayName");
        NodeList children = node.getChildNodes();
        for(int i = 0; i < children.getLength(); i++){
            Node child = children.item(i);
            if(child.getNodeType() == Node.ELEMENT_NODE){
                if(child.getNodeName().equals(WaveletDecompGUI.class.getName())){
                    //String project = ((Element)child).getAttribute("project_name");
                    //String fileSystem = ((Element)child).getAttribute("project_root");
                    //QiProjectDescUtils.setQiSpace(qiProjectDesc, fileSystem);
                    //QiProjectDescUtils.setQiProjectName(qiProjectDesc, project);
                    //QiProjectDescUtils.setQiProjectReloc(qiProjectDesc, messagingMgr.getServerOSFileSeparator());
                    //restoreProjectEnv(child);
                    qiProjectDesc = getProjectDesc(child);
                    gui = new WaveletDecompGUI(this);
                    gui.restoreState(child);

                    if(preferredDisplayName != null && preferredDisplayName.trim().length() > 0){
                        CompDescUtils.setDescPreferredDisplayName(messagingMgr.getMyComponentDesc(), preferredDisplayName);
                        gui.renamePlugin(preferredDisplayName);
                    }
                }
            }
        }
    }

    /**
     * Import state saved in a XML file. Use existing GUI. User not asked to save current
     * state first, because after import, saving state will save imported state. The preferred
     * display name is not changed. Therefore, the name in the GUI title and component tree node
     * don't have to change.
     *
     * @param node XML containing saved state variables
     */
    void importState(Node node) {
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals(WaveletDecompGUI.class.getName())) {
                    //get project descriptor from state since GUI needs it
                    //keep the associated project
//                    qiProjectDesc = getProjectDesc(child);
                    gui.restoreState(child);

                    //set title of window
                    //Not necessary to reset the title of the window, for the project didn't change
//                    String projectName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
//                    gui.resetTitle(projectName);
                }
            }
        }
    }

    /** Initialize the plugin, then start processing messages its receives.
     * The pleaseStop flag is set when user has terminated the GUI, and it is time to quit
     * If a message has the skip flag set, it is to be handled by the GUI, not this class
     */
    public void run() {
        // initialize the Wavelet Decomposition plugin
        init();

        //process any messages received from other components
        while(pleaseStop == false) {
            //Check if associated with a PM. If not (because was restored), discover the PM
            //with a matching PID. There will be one once its GUI comes up.
            String myPid = QiProjectDescUtils.getPid(qiProjectDesc);
            while (projMgrDesc == null && !myPid.equals("")) {
                //NOTE: Request will stay forwever on outstanding request list if no PM with matching PID.
                //      This can occur when restoring the workbench and the PM hasn't been restored
                //      or it is being restored but its GUI is not yet up.
                //Message Dispatcher will broadcast command to all PMs. Only PM with matching PID
                //will send back a response.
                messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_ASSOC_PROJMGR_CMD, QIWConstants.STRING_TYPE, myPid);
                try {
                    //wait and then try again
                    Thread.currentThread().sleep(1000);
                    //if a message arrived, process it. It probably is the response from a PM.
                    if (!messagingMgr.isMsgQueueEmpty()) break;
                } catch (InterruptedException ie) {
                    continue;
                }
            }

            IQiWorkbenchMsg msg = messagingMgr.peekNextMsg();
            if(msg != null){
            	if(msg.getMsgKind().equals(QIWConstants.DATA_MSG) && msg.skip()){
            		if (messagingMgr.getMatchingOutstandingRequest(msg) == null) {
                        msg = messagingMgr.getNextMsg();
                        logger.warning("Consuming response message which lacks a matching outstanding request. Response msg: " + msg);
                    }
            	}else{
            		msg = messagingMgr.getNextMsgWait();
            		try{
            			processMsg(msg);
            		}catch(Exception e){
            			e.printStackTrace();
                    	logger.warning(ExceptionMessageFormatter.getFormattedMessage(e));
            		}
            	}
            }
        }
    }


    /**
     * Extract the project descriptor from the saved state. If the state
     * does not contain a project descriptor, form one from the implicit
     * project so backward compatible.
     *
     * @return Project descriptor for project associated with the bhpViewer.
     */
    private QiProjectDescriptor getProjectDesc(Node node) {
        QiProjectDescriptor projDesc = null;
/*
        if (!node.getNodeName().equals(BhpViewerBase.class.getName())) {
            ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(), "restoreState error: State not bhpViewer's but of " + node.getNodeName());
            return;
        }
*/
        NodeList children = node.getChildNodes();
        String nodeName;
        Node child;
        for (int i = 0; i < children.getLength(); i++) {
          child = children.item(i);
          if (child.getNodeType() == Node.ELEMENT_NODE) {
            nodeName = child.getNodeName();
            if (nodeName.equals("QiProjectDescriptor")) {
                projDesc = (QiProjectDescriptor)XmlUtils.XmlToObject(child);
                break;
            }
          }
        }

        //Check if saved state has a project descriptor. If not, create
        //one using the implicit project so backward compatible.
        if (projDesc == null) {
            projDesc = new QiProjectDescriptor();
            //get path of implicit project from Message Dispatcher
            String projPath = messagingMgr.getProject();
            QiProjectDescUtils.setQiSpace(projDesc, projPath);
            //leave relative location of project as "/"
            int idx = projPath.lastIndexOf("\\");
            if (idx == -1) idx = projPath.lastIndexOf("/");
            String projectName = projPath.substring(idx+1);
            QiProjectDescUtils.setQiProjectName(projDesc, projectName);
        }

        return projDesc;
    }

    /**
     * Find the request matching the response and process the response based on the request.
     */
    public void processMsg(IQiWorkbenchMsg msg) {
        /** Request that matches the response */
        IQiWorkbenchMsg request = null;

        //log message traffic
        logger.fine("msg="+msg.toString());

        // Check if a response. If so, process and consume response
        if(messagingMgr.isResponseMsg(msg)) {
            request = messagingMgr.checkForMatchingRequest(msg);
            String cmd = MsgUtils.getMsgCommand(request);
            // check if from the message dispatcher
            if(messagingMgr.isResponseFromMsgDispatcher(msg)) {
                if(cmd.equals(QIWConstants.NULL_CMD))
                    return;
            }
            else if(cmd.equals(QIWConstants.GET_JOB_OUTPUT_CMD)) {
                ArrayList<String> stdOut = (ArrayList<String>)msg.getContent();
                StringBuffer jobOutput = new StringBuffer();
                for(String s : stdOut){
                    jobOutput.append(s + "\n");
                }
                gui.setStdOutTextArea(jobOutput.toString());

            }
            else if(cmd.equals(QIWConstants.SUBMIT_JOB_CMD)) {
                jobID = (String)msg.getContent();
                /*if(jobID != null){
                  ArrayList params = new ArrayList();
                  params.add(messagingMgr.getLocationPref());
                  params.add(jobID);
                  String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                                                                    QIWConstants.GET_JOB_OUTPUT_CMD,
                                                                    QIWConstants.ARRAYLIST_TYPE,params);
                 */
                /*QiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
                  ArrayList stdOut = new ArrayList();
                  if(response == null || response.isAbnormalStatus()) {
                    if(response != null) {
                      int errorStatus = response.getStatusCode();
                      String errorContent = (String)response.getContent();
                      logger.info("GET_JOB_OUTPUT_CMD errorContent= " + errorContent);
                    }
                  }
                  else{
                    stdOut = (ArrayList)response.getContent();
                    logger.info("stdOut " + stdOut);
                  }*/
                //}
                //  return;
            }
            else if(cmd.equals(QIWConstants.GET_RESULT_FROM_QI_FILE_CHOOSER_CMD)) {
				QiFileChooserDescriptor desc = (QiFileChooserDescriptor)msg.getContent();
				if(desc.getReturnCode()  == JFileChooser.APPROVE_OPTION){
					List<String> list = desc.getSelectedFileNameList();
					if(!desc.isMultiSelectionEnabled()){ //single file selection mode
						String filesep = messagingMgr.getServerOSFileSeparator();
						String filePath = desc.getSelectedFilePathBase() + filesep + list.get(0);
						String command = desc.getMessageCommand();
						Component comp = desc.getParentGUI();
						if(command.equals(WaveletDecompConstants.GET_HORIZON_BHPSU_DATASET_CMD)){
							Map map = desc.getAdditionalProperties();
							if(map != null){
								javax.swing.JTextField targetField = (javax.swing.JTextField)map.get("target");
								if(targetField != null)
									runBHPIOTask(filePath,targetField, true);
							}
						}else if(command.equals(WaveletDecompConstants.FILE_CHOOSER_CMD_GET_HORIZON_DATA)){
							String horizonDir = QiProjectDescUtils.getHorizonsPath(qiProjectDesc);
                            String horizon = filePath.substring(horizonDir.length()+1);
                            gui.setSUHorizonDataPath(horizon);
						}else if(command.equals(WaveletDecompConstants.FILE_CHOOSER_CMD_GET_SEISMIC_DATASET)){
							Map map = desc.getAdditionalProperties();
							if(map != null){
								javax.swing.JTextField targetField = (javax.swing.JTextField)map.get("target");
								if(targetField != null)
									runBHPIOTask(filePath,targetField, false);
							}
						}
					}
				}
			}
            // route the result from file chooser service back to its caller
            else if(cmd.equals(QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD)) {
                ArrayList list = (ArrayList)msg.getContent();
                final String selectionPath = (String)list.get(1);

                if (((Integer)list.get(0)).intValue() == JFileChooser.APPROVE_OPTION) {
                    final String filePath = (String)list.get(1);
                    String action = (String)list.get(3);
                    if(action.equals(WaveletDecompConstants.FILE_CHOOSER_CMD_GET_SEISMIC_DATASET)){
                        selectedSeismicDatasetPath = filePath;


                        doTask(selectedSeismicDatasetPath,gui);
                        /*
                  int status = getSeismicDatasetInfo(filePath);
                  logger.info("job submitting status = " + status);
                  if(status == 0){
                      Runnable updateAComponent = new Runnable() {
                          public void run() {
                              gui.setDatasetTextField(filePath);
                              gui.setEntireDatasetButtonEnabled(true);
                              gui.setDatasetDefaultValue(getSeismicDataset());
                          }
                      };
                      SwingUtilities.invokeLater(updateAComponent);
                  }else{
                    JOptionPane.showMessageDialog(gui,"Error in running SU script.",
                            "Problem in submitting job script",JOptionPane.WARNING_MESSAGE);
                  }
                         */
                    }else if(action.equals(WaveletDecompConstants.FILE_CHOOSER_CMD_GET_HORIZON_DATA)){
                        Runnable updateAComponent = new Runnable() {
                            public void run() {

                                String horizonDir = QiProjectDescUtils.getHorizonsPath(qiProjectDesc);
                                String horizon = filePath.substring(horizonDir.length()+1);
                                gui.setSUHorizonDataPath(horizon);
                            }
                        };
                        SwingUtilities.invokeLater(updateAComponent);

                    }
                    else if (action.equals(QIWConstants.EXPORT_COMP_STATE)) {
                        //Write XML state to the specified file and location
                        String pmState = genState();
                        String xmlHeader = "<?xml version=\"1.0\" ?>\n";
    //TODO: handle remote case - use an IO service
                        try {
                            ComponentStateUtils.writeState(xmlHeader+pmState, selectionPath,messagingMgr);
                        } catch (QiwIOException qioe) {
                            //TODO notify user cannot write state file
                            logger.finest(qioe.getMessage());
                        }
                    }
                    else if (action.equals(QIWConstants.IMPORT_COMP_STATE)) {
    //TODO: handle remote case - use an IO service
                        //check if file exists
                        if (ComponentStateUtils.stateFileExists(selectionPath,messagingMgr)) {
                            try {
                                String pmState = ComponentStateUtils.readState(selectionPath,messagingMgr);
                                Node compNode = ComponentStateUtils.getComponentNode(pmState);
                                if (compNode == null) {
                                    JOptionPane.showMessageDialog(gui, "Can't convert XML state to XML object", "Internal Processing Error", JOptionPane.ERROR_MESSAGE);
                                } else {
                                    //check for matching component type
                                    String compType = ((Element)compNode).getAttribute("componentType");
                                    String expectedCompType = messagingMgr.getRegisteredComponentDisplayNameByDescriptor(messagingMgr.getMyComponentDesc());
                                    if (!compType.equals(expectedCompType)) {
                                        JOptionPane.showMessageDialog(gui, "State file not for this qiComponent type. Select again", "Selection Error", JOptionPane.ERROR_MESSAGE);
                                    } else {
                                        //Note: don't ask if want to save state before importing because
                                        //      for future saves, the imported state will be saved.

                                        //import state
                                        importState(compNode);
                                    }
                                }
                            } catch (QiwIOException qioe) {
                                JOptionPane.showMessageDialog(gui, "Can't read state file. Select again.", "Processing Error", JOptionPane.ERROR_MESSAGE);
                                logger.finest(qioe.getMessage());
                            }
                        } else {
                            JOptionPane.showMessageDialog(gui, "State file doesn't exist or is not a file. Select again.", "Selection Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            }
            else if (cmd.equals(QIWConstants.GET_PROJ_INFO_CMD)) {
                ArrayList projInfo = (ArrayList)msg.getContent();
                qiProjectDesc = (QiProjectDescriptor)projInfo.get(1);

                //reset window title (if GUI up)
                if (gui != null) {
                    String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
                    gui.resetTitle(projName);
                }
            }
            else if (cmd.equals(QIWConstants.GET_ASSOC_PROJMGR_CMD)) {
                ArrayList projInfo = (ArrayList)msg.getContent();
                projMgrDesc = (ComponentDescriptor)projInfo.get(0);
                qiProjectDesc = (QiProjectDescriptor)projInfo.get(1);
                String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
                gui.resetTitle(projName);
            }
            else if (cmd.equals(QIWConstants.CONTROL_QIVIEWER_CMD)) {
                displayTransducer.processResp(msg);
            }
            else
                logger.warning("WaveletDecompPlugin: Response to " + cmd + " command not processed " + msg.toString());
            return;
        }
        //Check if a request. If so, process and send back a response
        else if(messagingMgr.isRequestMsg(msg)) {
            String cmd = msg.getCommand();
            // deactivate plugin came from user, tell gui to quit and stop run method
            if(cmd.equals(QIWConstants.DEACTIVATE_PLUGIN_CMD) || cmd.equals(QIWConstants.REMOVE_COMPONENT_CMD) || cmd.equals(QIWConstants.DEACTIVATE_COMPONENT_CMD) || cmd.equals(QIWConstants.REMOVE_PLUGIN_CMD)) {
                if(WaveletDecompConstants.DEBUG_PRINT > 0)
                    System.out.println(CompDescUtils.getDescPreferredDisplayName(messagingMgr.getMyComponentDesc()) + " quitting");
                try{
                    if(gui != null && gui.isVisible())
                        gui.closeGUI();
                    // unregister plugin
                    messagingMgr.unregisterComponent(messagingMgr.getComponentDesc(myCID));
                }
                catch (Exception e) {
                    IQiWorkbenchMsg res = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Unknown exception occurred with " + myCID);
                    messagingMgr.routeMsg(res);
                }
                pleaseStop = true;
                messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,myCID + " is successfully deactivated.");
            } else
            if(cmd.equals(QIWConstants.OPEN_COMPONENT_GUI_CMD)) {
                if(gui != null)
                    gui.setVisible(true);
            } else
            if(cmd.equals(QIWConstants.CLOSE_COMPONENT_GUI_CMD)) {
                if(WaveletDecompConstants.DEBUG_PRINT > 0)
                    System.out.println(myCID + " quitting");
                if(gui != null){
  				  	messagingMgr.sendResponse(msg,Component.class.getName(),(Component)gui);
                    gui.closeGUI();
                }
            } else
            if(cmd.equals(QIWConstants.SET_PREFERRED_DISPLAY_NAME_CMD)) {
                String preferredDisplayName = (String)msg.getContent();
                if(preferredDisplayName != null && preferredDisplayName.trim().length() > 0){
                    CompDescUtils.setDescPreferredDisplayName(messagingMgr.getMyComponentDesc(), preferredDisplayName);
                    gui.renamePlugin(preferredDisplayName);
                }
                messagingMgr.sendResponse(msg,QIWConstants.COMP_DESC_TYPE,messagingMgr.getMyComponentDesc());
            }
// save state
            else if(cmd.equals(QIWConstants.SAVE_COMP_CMD))
                messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,genState());
//save component state as clone
            else if(cmd.equals(QIWConstants.SAVE_COMP_AS_CLONE_CMD)) {
                messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,genStateAsClone());
                // restore state
            } else if(cmd.equals(QIWConstants.RESTORE_COMP_CMD)) {
                Node node = (Node)msg.getContent();
                restoreState(node);
				
				// Create the finite state machine that will drive the qiViewer when
				// displaying the results of the generated WaveletDecomp script.
				displayTransducer = new WaveletDecompDisplayTransducer(gui, messagingMgr, stepState, actionState);
				
                messagingMgr.sendResponse(msg,WaveletDecompGUI.class.getName(),gui);
            }
// invoke is done after activate
            else if(cmd.equals(QIWConstants.INVOKE_SELF_CMD)) {
                //Do nothing if don't have a GUI
                // Create the plugin's GUI, but don't make it visible. That is
                // up to the Workbench Manager who requested the plugin be
                // activated. Pass GUI the CID of its parent.
                //check to see if content incudes xml information which means invoke self will
                // do the restoration
                ArrayList msgList = (ArrayList)msg.getContent();
                String invokeType = (String)msgList.get(0);
                if(invokeType.equals(QIWConstants.COMPONENT_INVOKE_RESTORED)){
                    Node node = ((Node)((ArrayList)msg.getContent()).get(2));
                    restoreState(node);
                } else if(invokeType.equals(QIWConstants.COMPONENT_INVOKE_NEW)){
//                              qiProjectDesc = ((QiProjectDescriptor)((ArrayList)msg.getContent()).get(2));
                    gui = new WaveletDecompGUI(this);
                    java.awt.Point p = (java.awt.Point)msgList.get(1);
                    if(p != null)
                    	gui.setLocation(p);
                }
				
				// Create the finite state machine that will drive the qiViewer when
				// displaying the results of the generated WaveletDecomp script.
				displayTransducer = new WaveletDecompDisplayTransducer(gui, messagingMgr, stepState, actionState);

                //Send a normal response back with the plugin's JInternalFrame and
                //let the Workbench Manager add it to the desktop and make
                //it visible.
                messagingMgr.sendResponse(msg,WaveletDecompGUI.class.getName(),gui);
            }
// user selected rename plugin menu item
            else if(cmd.equals(QIWConstants.RENAME_COMPONENT)) {
                ArrayList<String> names = new ArrayList<String>(2);
                names = (ArrayList)msg.getContent();
                if(WaveletDecompConstants.DEBUG_PRINT > 0)
                    System.out.println("Rename myself from " + names.get(0) + " to " + names.get(1));
                gui.renamePlugin(names.get(1));
            }
            else if (cmd.equals(QIWConstants.NOTIFY_ASSOC_PROJMGR_CMD)) {
              //Just a notification. No response required or expected.
              projMgrDesc = (ComponentDescriptor)msg.getContent();
              //Note: Cannot get info about PM's project because GUI may not be up yet.
              if (projMgrDesc != null)
                  messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_PROJ_INFO_CMD, projMgrDesc);
            } else if (cmd.equals(QIWConstants.NOTIFY_PROJ_INFO_CHANGED_CMD)) {
              ArrayList info = (ArrayList)msg.getContent();
              ComponentDescriptor pmDesc = (ComponentDescriptor)info.get(0);
              QiProjectDescriptor projDesc = (QiProjectDescriptor)info.get(1);
              //ignore message if not from associated PM
              if (projMgrDesc != null && pmDesc.getCID().equals(projMgrDesc.getCID())) {
                  qiProjectDesc = projDesc;
//TODO: update PID [get from projDesc?]
                  //update window title
                  String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
                  gui.resetTitle(projName);
              }
			}
            else
                logger.warning("WaveletDecompPlugin: Request not processed, requeued: " + msg.toString());
            return;
        }
    }




    private Map<String,DataSetHeaderKeyInfo> parse(List<String> list){
        if(list == null || list.size() == 0)
            return null;

        List<String> list2 = new ArrayList<String>();
        boolean found = false;
        for(String s : list){
            if(found)
                list2.add(s);
            if(s.startsWith("Number of Header Keys:")){
                found = true;
            }
        }
        Map<String,DataSetHeaderKeyInfo> map = new HashMap<String,DataSetHeaderKeyInfo>();
        for(String s : list2){
            String [] s2 = s.split(",");
            //String tag = "Name: ";
            String name = s2[0].substring(s2[0].indexOf(":")+1).trim();
            // tag = "Min: ";
            String min = s2[1].substring(s2[1].indexOf(":")+1).trim();
            //tag = "Max: ";
            String max = s2[2].substring(s2[2].indexOf(":")+1).trim();
            //tag = "Incr: ";
            String incr = s2[3].substring(s2[3].indexOf(":")+1).trim();
            DataSetHeaderKeyInfo ds = new DataSetHeaderKeyInfo(name,Double.valueOf(min).doubleValue(),Double.valueOf(max).doubleValue(),Double.valueOf(incr).doubleValue());
            map.put(name,ds);
        }
        return map;
    }

    /** Send a message to the Workbench Manager to remove this instance of WaveletDecomp from the component tree and send a message to it to deactivate itself.
     */
    public void deactivateSelf(){
        //ask Workbench Manager to remove WaveletDecomp from workbench GUI and then send it back to self to deactivate self
        IComponentDescriptor wbMgr = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_MGR_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.QUIT_COMPONENT_CMD,wbMgr,QIWConstants.STRING_TYPE,
                messagingMgr.getMyComponentDesc());
    }
    /** Get Messaging Manager of WaveletDecomp component.
     *  @return  MessagingManager
     */
    public MessagingManager getMessagingMgr(){
        return messagingMgr;
    }


	/** Invoke QI File Chooser Service
	 * @param desc qiFileChooserDescriptor passed through to fileChooser from caller
	 *
	 */
	public void callFileChooser(QiFileChooserDescriptor desc){
		if(desc == null)
			return;
		IComponentDescriptor fileChooser = null;
		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD,true);
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null){
			logger.warning("Respone to get file chooser service returning null due to timed out");
			return;
		}
		else if(MsgUtils.isResponseAbnormal(response)) {
			logger.warning("Internal error occurring in getting file chooser service. Cause: " + MsgUtils.getMsgContent(response));
			return;
		}
		else
			fileChooser = (ComponentDescriptor)MsgUtils.getMsgContent(response);

		messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_QI_FILE_CHOOSER_CMD,fileChooser,QiFileChooserDescriptor.class.getName(),desc);
		messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_RESULT_FROM_QI_FILE_CHOOSER_CMD,fileChooser);
		return;
	}
    /** Invoke File Chooser Service
     * @param list Passed through to fileChooser from caller
     *
     */
    public void callFileChooser(ArrayList list){
        if(list == null)
            return;
        ComponentDescriptor fileChooser = null;
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD,true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
        if(response == null){
            logger.warning("Respone to get file chooser service returning null due to timed out");
            return;
        }
        else if(MsgUtils.isResponseAbnormal(response)) {
            logger.warning("Internal error occurring in getting file chooser service. Cause: " + MsgUtils.getMsgContent(response));
            return;
        }
        else
            fileChooser = (ComponentDescriptor)MsgUtils.getMsgContent(response);

        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_FILE_CHOOSER_CMD,fileChooser,QIWConstants.ARRAYLIST_TYPE,list);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD,fileChooser);
        return;
    }

    /**
     * Submit a job and get standard error and standard out
     * @param params parameters needed for SUBMIT_JOB_CMD
     * @return zero if successful, else -1
     */
    private Map<String,DataSetHeaderKeyInfo> seismicDataset;
    private String selectedSeismicDatasetPath = "";

    public Map<String,DataSetHeaderKeyInfo> getSeismicDataset(){
        return seismicDataset;
    }

    public void setSeismicDataset(Map<String,DataSetHeaderKeyInfo> map){
        seismicDataset = map;
    }

    public void emptySeismicDataset(){
        if(seismicDataset != null)
            seismicDataset.clear();
    }

    public String getSelectedSeismicDatasetPath(){
        return selectedSeismicDatasetPath;
    }
    private String jobID;

    /**
     * Get the job output by using JobAdaptor
     * @return
     */
    public void getJobOutput(){
        if(jobID != null){
            ArrayList params = new ArrayList();
            params.add(messagingMgr.getLocationPref());
            params.add(jobID);
            messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                    QIWConstants.GET_JOB_OUTPUT_CMD,
                    QIWConstants.ARRAYLIST_TYPE,params);
        }else
            logger.info("jobID is not yet available for browsing the output.");
        /*QiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
            / ArrayList<String> stdOut = new ArrayList<String>();
             if(response == null || MsgUtils.isResponseAbnormal(response)) {
                if(response != null) {
                  int errorStatus = response.getStatusCode();
                  String errorContent = (String)response.getContent();
                  logger.info("GET_JOB_OUTPUT_CMD errorContent= " + errorContent);
                  return errorContent;
                }
                return "Error or empty";
              }
              else{
                stdOut = (ArrayList)response.getContent();
                String jobOutput = "";
                for(String s : stdOut){
                    jobOutput += s + "\n";
                }
                return jobOutput;
              }
        }else
            return "job id not yet available";
         */
    }

    /**
     * Get the job output by retrieving the given output path
     * @param stdOutPath where the job output is stored
     * @return output string
     */
    public String getJobOutput(String stdOutPath){

        //String result = checkDirExist(stdOut);
        //if(result != null && !result.equals("yes")){
        //JOptionPane.showMessageDialog(gui,"Job ouput is not yet available.",
        //      "Job Output",JOptionPane.WARNING_MESSAGE);
        //  return "";
        //}

        ArrayList params = new ArrayList();
        params.add(messagingMgr.getLocationPref());
        params.add(stdOutPath);
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.FILE_READ_CMD,
                QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgID,10000);
        if(resp == null){
            logger.info("Null Response returned due to timed out.");
            JOptionPane.showMessageDialog(gui,"Error in running FILE_READ_CMD due to timed out problem in retrieving message.",
                    "IO Error",JOptionPane.WARNING_MESSAGE);
            return "";
        }else if(resp.isAbnormalStatus()){
            String message = "Abnormal response returned due to " + resp.getContent();
            logger.info(message);
            JOptionPane.showMessageDialog(gui,"Error in running FILE_READ_CMD due to " + resp.getContent(),
                    "IO Error",JOptionPane.WARNING_MESSAGE);
            if(stdOutPath.endsWith(".err"))
                gui.setStdErrTextArea((String)resp.getContent());
            else if(stdOutPath.endsWith(".out"))
                gui.setStdOutTextArea((String)resp.getContent());
            return "error";
        }else{
            ArrayList<String> list = (ArrayList<String>)resp.getContent();
            StringBuffer jobOutput = new StringBuffer();
            for(String s : list){
                jobOutput.append(s + "\n");
            }
            final String f_stdOut = stdOutPath;
            final String f_jobOutput = jobOutput.toString();
            Runnable updateAComponent = new Runnable() {
                public void run() {
                    if(f_stdOut.endsWith(".err")){
                        String text = "Standard Error Output\n\n";
                        text += f_jobOutput;
                        gui.setStdErrTextArea(text);
                    }else if(f_stdOut.endsWith(".out")){
                        String text = "Standard Output\n\n";
                        text += f_jobOutput;
                        gui.setStdOutTextArea(text);
                    }
                }
            };
            SwingUtilities.invokeLater(updateAComponent);

            return jobOutput.toString();
        }
        //}else{
        //  logger.info("jobID is not yet available for browsing the output.");
        //  return "";
        //}
    }

    /**
     * Write script content to a given directory
     * @param dir parameters in which the scripts is stored
     * @param scripts contents
     * @return
     */
    public void writeScript(String dir, String scripts){
        String result = checkDirExist(dir);
        logger.info("check if the " + dir + " exists: " + result);
        boolean ok = false;

        if (result.equals("no")) {
            int status = JOptionPane
            .showConfirmDialog(
                    gui,
                    "The script will be written into "
                    + dir
                    + " but the directory does not exist. Would you like the application to create it for you?",
                    "Confirm action", JOptionPane.YES_NO_OPTION);
            if (status == JOptionPane.YES_OPTION) {
                result = mkDir(dir);
                if (result.equals("success"))
                    ok = true;
                logger.info("Create directory " + dir + " returning " + result);
            } else
                return;
        } else if (result.equals("yes"))
            ok = true;

        // String fileName = String.valueOf(System.currentTimeMillis());

        if (ok){
            Timestamp ts = new Timestamp(System.currentTimeMillis());
            String fileNamePrefix = "wd_" + formatter.format(ts);
            String fileName = fileNamePrefix + ".sh";
            writeScripts(dir + messagingMgr.getServerOSFileSeparator()
                    + fileName, scripts);
        }
    }

    public void submitJob(List<String> params) {
        // submit job for execution
        int errorStatus = 0;
        jobID = null;
        String errorContent = "";
        params.add(0,messagingMgr.getLocationPref());
        params.add(1,"sh");
        params.add(2,"-c");
        //params.add(3,"ltlscript.sh ");
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.SUBMIT_JOB_CMD,
                QIWConstants.ARRAYLIST_TYPE,params);
        /*
        QiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
        if(response == null || response.isAbnormalStatus()) {
            if(response != null) {
                errorStatus = MsgUtils.getMsgStatusCode(response);
                errorContent = (String)MsgUtils.getMsgContent(response);
                logger.info("SUBMIT_JOB_CMD errorContent = " + errorContent);
            }
            if(errorStatus == -1)
                return jobID;
        }else
        jobID = (String)response.getContent();
        return jobID;
         */
    }

    /**
     * Cancel a job by using JobAdaptor
     * @return
     */
    public void cancelJob(){
        if(jobID == null || jobID.trim().length() == 0){
            logger.info("no job active to cancel or job id not yet available for cancelation");
            return;
        }
        ArrayList params = new ArrayList();
        params.add(messagingMgr.getLocationPref());
        params.add(jobID);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.KILL_JOB_CMD,QIWConstants.ARRAYLIST_TYPE,params);
    }


    //public int submitJob(String script,String paramString) {
    public void submitJob(String script,String paramString) {
        // submit job for execution
        int status = 0;
        int errorStatus = 0;
        String errorContent = "";
        ArrayList params = new ArrayList();
        params.add(0,messagingMgr.getLocationPref());
        params.add(1,"sh");
        params.add(2,"-c");
        params.add(3,"cd " + messagingMgr.getProject() + "/scripts; " + script + " " + paramString);
        logger.info("params = " + params);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.SUBMIT_JOB_CMD,
                QIWConstants.ARRAYLIST_TYPE,params);
        /*
        QiWorkbenchMsg response = messagingMgr.getNextMsgWait();
        if(response == null || response.isAbnormalStatus()) {
            if(response != null) {
                errorStatus = response.getStatusCode();
                errorContent = (String)response.getContent();
                logger.info("SUBMIT_JOB_CMD errorContent = " + errorContent);
            }
            return status = -1;
        }
        String jobID = (String)response.getContent();
        logger.info("SUBMIT_JOB_CMD jobID = " + jobID);

        //get stdout
        params.clear();
        //params.add(QIWConstants.LOCAL_SERVICE_PREF);
        params.add(messagingMgr.getLocationPref());
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                                                          QIWConstants.GET_JOB_OUTPUT_CMD,
                                                          QIWConstants.ARRAYLIST_TYPE,params);
        response = messagingMgr.getMatchingResponseWait(msgID,10000);
        ArrayList stdOut = new ArrayList();
        if(response == null || response.isAbnormalStatus()) {
          if(response != null) {
            errorStatus = response.getStatusCode();
            errorContent = (String)response.getContent();
            logger.info("GET_JOB_OUTPUT_CMD errorContent= " + errorContent);
          }
          return status = -1;
        }
        else
          stdOut = (ArrayList)MsgUtils.getMsgContent(response);

        seismicDataset = parse(stdOut);
        for(int i=0; i<stdOut.size(); i++)
          System.out.println(stdOut.get(i));

        //get stderr
        params.clear();
        //params.add(QIWConstants.LOCAL_SERVICE_PREF);
        params.add(messagingMgr.getLocationPref());
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                                                          QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
                                                          QIWConstants.ARRAYLIST_TYPE,params);
        response = messagingMgr.getMatchingResponseWait(msgID,10000);
        ArrayList stdErr = new ArrayList();
        if(response == null || MsgUtils.isResponseAbnormal(response)) {
          if(response != null) {
            errorStatus = MsgUtils.getMsgStatusCode(response);
            errorContent = (String)MsgUtils.getMsgContent(response);
            logger.info("GET_JOB_ERROR_OUTPUT_CMD errorContent = " + errorContent);
          }
          return status = -1;
        }
        else
          stdErr = (ArrayList)MsgUtils.getMsgContent(response);
        for(int i=0; i<stdErr.size(); i++)
            logger.info("GET_JOB_ERROR_OUTPUT_CMD " + stdErr.get(i));

        //Wait until the command has finished
        params.clear();
        //params.add(QIWConstants.LOCAL_SERVICE_PREF);
        params.add(messagingMgr.getLocationPref());
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                                                          QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
                                                          QIWConstants.ARRAYLIST_TYPE,params);
        response = messagingMgr.getMatchingResponseWait(msgID,10000);
        if(response == null || MsgUtils.isResponseAbnormal(response)) {
          if(response != null) {
            errorStatus = MsgUtils.getMsgStatusCode(response);
            errorContent = (String)MsgUtils.getMsgContent(response);
            logger.info("WAIT_FOR_JOB_EXIT_CMD errorContent = " + errorContent);
          }
          return status = -1;
        }
        else
          status = (Integer)MsgUtils.getMsgContent(response);
        logger.info("WAIT_FOR_JOB_EXIT_CMD status = " + status);
        // get exitValue
        params.clear();
        //params.add(QIWConstants.LOCAL_SERVICE_PREF);
        params.add(messagingMgr.getLocationPref());
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                                                          QIWConstants.GET_JOB_STATUS_CMD,
                                                          QIWConstants.ARRAYLIST_TYPE,params);
        response = messagingMgr.getMatchingResponseWait(msgID,10000);
        if(response == null)
          return -1;
        else if(status != 0){
          logger.info("abnormal job status " + (Integer)MsgUtils.getMsgContent(reponse));
          return status = (Integer)MsgUtils.getMsgContent(response);
        }
        //release the job
        params.clear();
        //params.add(QIWConstants.LOCAL_SERVICE_PREF);
        params.add(messagingMgr.getLocationPref());
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.RELEASE_JOB_CMD,
                                                          QIWConstants.ARRAYLIST_TYPE,params);
        response = messagingMgr.getMatchingResponseWait(msgID,10000);
        if(response == null || MsgUtils.isResponseAbnormal(response)) {
          if(response != null) {
            errorStatus = MsgUtils.getMsgStatusCode(response);
            errorContent = (String)MsgUtils.getMsgContent(response);
            logger.info("RELEASE_JOB_CMD errorContent = " + errorContent);
          }
          return status = -1;
        }
        else
          return status = 0;

         */
    }

    /**
     * Create a directory of a given path
     * @param filePath as directory name
     * @return yes, no or error
     */
    public String mkDir(String filePath){
        ArrayList params = new ArrayList();
        params.add(messagingMgr.getLocationPref());
        params.add(filePath);
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.CREATE_DIRECTORY_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId,1000);
        if(response != null && !MsgUtils.isResponseAbnormal(response))
            return (String)MsgUtils.getMsgContent(response);
        else if(response == null){
            JOptionPane.showMessageDialog(gui,"Timed out problem in running CREATE_DIRECTORY_CMD.",
                    "IO Error",JOptionPane.WARNING_MESSAGE);
            return "error";
        }else{
            JOptionPane.showMessageDialog(gui,"Error in running CREATE_DIRECTORY_CMD. " + (String)MsgUtils.getMsgContent(response),
                    "IO Error",JOptionPane.WARNING_MESSAGE);
            return "error";
        }
    }

    /**
     * Write script content to a given path
     * @param path where the scripts is stored
     * @param content scripts contents
     * @return true or false
     */
    public boolean writeScripts(String path, String content){
        ArrayList<String> params = new ArrayList<String>();
        //the first element is the loation preference (local or remote)
        params.add(messagingMgr.getLocationPref());
        //the second element is to add file path and name user just selected
        params.add(path);
        //the 3rd element is to add xml string returned from the components
        params.add(content);

        //send a message to message dispatcher for persist the state information into selected file path within preferred environment (local or remote)
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.FILE_WRITE_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);

        if (resp == null){
            logger.info("Response returning null from command FILE_WRITE_CMD in writeScripts path=" + path);
            return false;
        }else if(resp.isAbnormalStatus()){
            logger.info("Abnormal response to write scripts FILE_WRITE_CMD:  in writeScripts " + (String)resp.getContent());
            JOptionPane.showMessageDialog(gui, "Error in writing script " + path + " cause: " +  (String)resp.getContent(), "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            return false;
        } else{
            logger.info("normal response to write scripts FILE_WRITE_CMD: " + (String)resp.getContent());
        }
        return true;
    }

    /**
     * Write script content to a given path
     * @param path where the scripts is stored
     * @param content scripts contents
     * @return true or false
     */
    public boolean writeScripts(String path, List<String> scripts){
        if(scripts == null)
            return false;
        //Add the path of the file want to write to to the head of the list
        //the second element is to add file path and name user just selected
        scripts.add(0, path);
        //Add the location of the IO to the head of the list
        //the first element is the loation preference (local or remote)
        scripts.add(0,messagingMgr.getLocationPref());

        //send a message to message dispatcher for persist the state information into selected file path within preferred environment (local or remote)
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.FILE_WRITE_CMD,QIWConstants.ARRAYLIST_TYPE,scripts,true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);

        if (resp == null){
            logger.info("Response returning null from command FILE_WRITE_CMD in writeScripts path=" + path);
            return false;
        }else if(resp.isAbnormalStatus()){
            logger.info("Abnormal response to write scripts FILE_WRITE_CMD:  in writeScripts " + (String)resp.getContent());
            JOptionPane.showMessageDialog(gui, "Error in writing script " + path + " cause: " +  (String)resp.getContent(), "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            return false;
        } else{
            logger.info("normal response to write scripts FILE_WRITE_CMD: " + (String)resp.getContent());
        }
        return true;
    }


    /**
     * Check to see if a given directory exists in the file system
     * @param filePath file path to be checked
     * @return yes, no, or error
     */
    public String checkDirExist(String filePath){
        ArrayList params = new ArrayList();
        params.add(messagingMgr.getLocationPref());
        params.add(filePath);
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.CHECK_FILE_EXIST_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);
        String temp = "";
        if(resp != null && !MsgUtils.isResponseAbnormal(resp)){
            temp = (String)resp.getContent();
        }else if(resp == null){
            JOptionPane.showMessageDialog(gui,"Timed out problem in running CHECK_FILE_EXIST_CMD.",
                    "IO Error",JOptionPane.WARNING_MESSAGE);
            temp = "error";
        }else{
            JOptionPane.showMessageDialog(gui,"Error in running CHECK_FILE_EXIST_CMD. " + (String)MsgUtils.getMsgContent(resp),
                    "IO Error",JOptionPane.WARNING_MESSAGE);
            temp = "error";
        }
        return temp;
    }

    /**
     * Get the seismic dataset information by calling SU script bhpio given the data input, then retrieve the output,
     * parse the output and form the data structure
     * @param filePath file path of the seismic dataset input
     * @return status indicating normal 0 or abnormal non 0
     */
 /*   public int getSeismicDatasetInfo(String filePath) {
		gui.setEntireDatasetButtonEnabled(false);
		if(filePath == null || filePath.trim().length() == 0)
			return 1;
		ArrayList<String> params = new ArrayList<String>();
		// make sure script is executable
		params.add(messagingMgr.getLocationPref());
		params.add("bash");
		params.add("-c");
		//String fileDir = filePath.substring(0,filePath.lastIndexOf(File.separator));
		String filesep = messagingMgr.getServerOSFileSeparator();
		String fileDir = filePath.substring(0,filePath.lastIndexOf(filesep));
		//String fileName = filePath.substring(filePath.lastIndexOf(File.separator)+1);
		String fileName = filePath.substring(filePath.lastIndexOf(filesep)+1);
		fileName = fileName.substring(0,fileName.lastIndexOf(".dat"));
		params.add(" source ~/.bashrc; cd " + fileDir + "; bhpio filename=" +fileName);
		// submit job for execution
		int status = 0;
		int errorStatus = 0;
		String errorContent = "";

		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.SUBMIT_JOB_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		//QiWorkbenchMsg response = messagingMgr.getNextMsgWait();
		QiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("SUBMIT_JOB_CMD errorContent = " + errorContent);
				JOptionPane.showMessageDialog(gui,"Error in running SU script.",
						"Problem in running SU script: " + errorContent, JOptionPane.WARNING_MESSAGE);
			}
			//return status = -1;
			return errorStatus;
		}
		String jobID = (String)MsgUtils.getMsgContent(response);
		status = MsgUtils.getMsgStatusCode(response);
		logger.info("SUBMIT_JOB_CMD jobID = " + jobID);
		logger.info("status of SUBMIT_JOB_CMD normal response " + status);
		//get stdout
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);

		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_OUTPUT_CMD errorContent= " + errorContent);
			}
			//return status = -1;
			return errorStatus;
		}
		else{
			ArrayList stdOut = new ArrayList();
			stdOut = (ArrayList)MsgUtils.getMsgContent(response);
			status = MsgUtils.getMsgStatusCode(response);
			logger.info("status of GET_JOB_OUTPUT_CMD normal response " + status);
			System.out.println("stdOut.size() of GET_JOB_OUTPUT_CMD =" + stdOut.size());
			for(int i=0; i<stdOut.size(); i++)
				System.out.println(stdOut.get(i));
			seismicDataset = parse(stdOut);
		}


		//get stderr
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		ArrayList stdErr = new ArrayList();
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_ERROR_OUTPUT_CMD errorContent = " + errorContent);
				JOptionPane.showMessageDialog(gui,"Error in running SU script.",
						"Problem in running SU script: " + errorContent, JOptionPane.WARNING_MESSAGE);
				return status;
			}
			//return status = -1;
			return status;
		}
		else{
			stdErr = (ArrayList)MsgUtils.getMsgContent(response);

			for(int i=0; i<stdErr.size(); i++)
				logger.info("GET_JOB_ERROR_OUTPUT_CMD " + stdErr.get(i));

			status = MsgUtils.getMsgStatusCode(response);
			logger.info("status of GET_JOB_ERROR_OUTPUT_CMD normal response " + status);
			if(stdErr.size() > 0){
				JOptionPane.showMessageDialog(gui,"Error in running SU script:" + stdErr,
					"Problem in running SU script", JOptionPane.WARNING_MESSAGE);
			}
		}

		//Wait until the command has finished
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("WAIT_FOR_JOB_EXIT_CMD errorContent = " + errorContent);
			}
			//return status = -1;
			return status;
		}
		else{
			status = (Integer)MsgUtils.getMsgContent(response);
			logger.info("status of WAIT_FOR_JOB_EXIT_CMD normal response " + status);
		}
		// get exitValue
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_STATUS_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null){
			return -1;
		}else{
			//return status = (Integer)MsgUtils.getMsgContent(response);
			status = (Integer)MsgUtils.getMsgContent(response);
			logger.info("status of GET_JOB_STATUS_CMD normal job status " + status);
			return status;

		}*/
		/*
		//release the job
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.RELEASE_JOB_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("RELEASE_JOB_CMD errorContent = " + errorContent);
			}
			logger.info("abnormal errorStatus after RELEASE_JOB_CMD = " + errorStatus);
			return status = -1;
		}
		else{
			//return status = 0;
			status = MsgUtils.getMsgStatusCode(response);
			logger.info("normal status after RELEASE_JOB_CMD = " + status);
			return status;
		}
		*/
	//}


    private List<Object> datasetInfoList;
	/**
	 * Get the seismic dataset information by calling SU script bhpio given the data input, then retrieve the output,
	 * parse the output and form the data structure
	 * @param filePath file path of the seismic dataset input
	 * @return status indicating normal 0 or abnormal non 0
	 */
	private int getSeismicDatasetInfo(String filePath) {
		if(filePath == null || filePath.trim().length() == 0)
			return 1;
		ArrayList<String> params = new ArrayList<String>();
		// make sure script is executable
		params.add(messagingMgr.getLocationPref());
		params.add("bash");
		params.add("-c");
		//String fileDir = filePath.substring(0,filePath.lastIndexOf(File.separator));
		String filesep = messagingMgr.getServerOSFileSeparator();
		String fileDir = filePath.substring(0,filePath.lastIndexOf(filesep));
		//String fileName = filePath.substring(filePath.lastIndexOf(File.separator)+1);
		String fileName = filePath.substring(filePath.lastIndexOf(filesep)+1);
		fileName = fileName.substring(0,fileName.lastIndexOf(".dat"));
		params.add(" source ~/.bashrc; cd " + fileDir + "; bhpio filename=" +fileName);
		params.add("true");
		// submit job for execution
		int status = 0;
		int errorStatus = 0;
		String errorContent = "";

		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.SUBMIT_JOB_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		//QiWorkbenchMsg response = messagingMgr.getNextMsgWait();
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("SUBMIT_JOB_CMD errorContent = " + errorContent);
				JOptionPane.showMessageDialog(gui,"Error in running SU script.",
						"Problem in running SU script: " + errorContent, JOptionPane.WARNING_MESSAGE);
			}
			//return status = -1;
			return errorStatus;
		}
		String jobID = (String)MsgUtils.getMsgContent(response);
		status = MsgUtils.getMsgStatusCode(response);
		logger.info("SUBMIT_JOB_CMD jobID = " + jobID);
		logger.info("status of SUBMIT_JOB_CMD normal response " + status);
		//get stdout
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);

		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_OUTPUT_CMD errorContent= " + errorContent);
			}
			//return status = -1;
			return errorStatus;
		}
		else{
			ArrayList stdOut = (ArrayList)MsgUtils.getMsgContent(response);
			status = MsgUtils.getMsgStatusCode(response);
			logger.info("status of GET_JOB_OUTPUT_CMD normal response " + status);
			System.out.println("stdOut.size() of GET_JOB_OUTPUT_CMD =" + stdOut.size());
			for(int i=0; i<stdOut.size(); i++)
				System.out.println(stdOut.get(i));
			datasetInfoList = parseBHPIOOutput(stdOut);
			logger.info("eismicDataset header info = " + datasetInfoList.get(0));
			logger.info("shorizon info = " + datasetInfoList.get(1));
		}

		//get stderr
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);

		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_ERROR_OUTPUT_CMD errorContent = " + errorContent);
				JOptionPane.showMessageDialog(gui,"Error in running SU script.",
						"Problem in running SU script: " + errorContent, JOptionPane.WARNING_MESSAGE);
				return status;
			}
			//return status = -1;
			return status;
		}
		else{
			ArrayList stdErr = (ArrayList)MsgUtils.getMsgContent(response);

			for(int i=0; i<stdErr.size(); i++)
				logger.info("GET_JOB_ERROR_OUTPUT_CMD " + stdErr.get(i));

			status = MsgUtils.getMsgStatusCode(response);
			logger.info("status of GET_JOB_ERROR_OUTPUT_CMD normal response " + status);
			if(stdErr.size() > 0){
				JOptionPane.showMessageDialog(gui,"Error in running SU script:" + stdErr,
						"Problem in running SU script", JOptionPane.WARNING_MESSAGE);
			}
		}

		//Wait until the command has finished
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("WAIT_FOR_JOB_EXIT_CMD errorContent = " + errorContent);
			}
			//return status = -1;
			return status;
		}
		else{
			status = (Integer)MsgUtils.getMsgContent(response);
			logger.info("status of WAIT_FOR_JOB_EXIT_CMD normal response " + status);
		}
		// get exitValue
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_STATUS_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null){
			return -1;
		}else{
			//return status = (Integer)MsgUtils.getMsgContent(response);
			status = (Integer)MsgUtils.getMsgContent(response);
			logger.info("status of GET_JOB_STATUS_CMD normal job status " + status);
			return status;

		}
	}

	private List<Object> parseBHPIOOutput(List<String> list){
		if(list == null || list.size() == 0)
			return null;

		boolean foundHorizon = false;
		List<String> list2 = new ArrayList<String>();
		boolean foundHeaderKey = false;
		String horizonLine = null;
		String datasetName = "";
		for(String s : list){
			if(foundHeaderKey && s.trim().startsWith("Name:"))
				list2.add(s);
			if(foundHorizon && s.trim().startsWith("HORIZONS:"))
				horizonLine = s;
			if(s.startsWith("Number of Header Keys:")){
				foundHeaderKey = true;
			}
			if(s.contains("is HORIZON data")){
				int ind = s.indexOf(" ");
				if(ind != -1)
					datasetName = s.substring(0, ind);
				foundHorizon = true;
			}
		}
		if(datasetName.length() > 0)
			datasetName += ":";
		Map<String,DataSetHeaderKeyInfo> map = new HashMap<String,DataSetHeaderKeyInfo>();
		for(String s : list2){
			String [] s2 = s.split(",");
			//String tag = "Name: ";
			String name = s2[0].substring(s2[0].indexOf(":")+1).trim();
			// tag = "Min: ";
			String min = s2[1].substring(s2[1].indexOf(":")+1).trim();
			//tag = "Max: ";
			String max = s2[2].substring(s2[2].indexOf(":")+1).trim();
			//tag = "Incr: ";
			String incr = s2[3].substring(s2[3].indexOf(":")+1).trim();
			DataSetHeaderKeyInfo ds = new DataSetHeaderKeyInfo(name,Double.valueOf(min).doubleValue(),Double.valueOf(max).doubleValue(),Double.valueOf(incr).doubleValue());
			map.put(name,ds);
		}

		List<String> horizonList = new ArrayList<String>();
		if(horizonLine != null){
			int ind = horizonLine.indexOf("HORIZONS:");
			String [] horizons = null;
			if(ind != -1){
				horizonLine = horizonLine.substring("HORIZONS:".length());
				horizons = horizonLine.trim().split(" +");
			}
			if(horizons != null){
				for(String s : horizons)
					horizonList.add(datasetName+s);
			}
		}
		List<Object> infoList = new ArrayList<Object>();
		infoList.add(map);
		infoList.add(horizonList);
		return infoList;
	}

    private void runBHPIOTask(String file, Component cmp, final boolean askForHorizon){
		final Component comp = cmp;
		final String filePath = file;
		Runnable heavyRunnable = new Runnable(){
			public void run(){
				String title = "Gathering input dataset information";
				String text = "Please wait while system is gathering input dataset information....";

				Icon icon = null; //new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));

				com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(comp,100,true,500,title,text,icon);
				//monitor.start("");
				ProgressUtil.start(monitor,"");
				int status = getSeismicDatasetInfo(filePath);

				if(ProgressUtil.isCancel(monitor)){
					return;
				}
				logger.info("runBHPIOTask status = " + status);
				//if(status == 0 || status == 1 || status == -1){
				if(status == 0){
					ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
					List<String> horizonList = (List<String>)datasetInfoList.get(1);
					if(askForHorizon){
						if(horizonList != null && horizonList.size() == 0){
							JOptionPane.showMessageDialog(gui,"The selected dataset does not appear to be a HORIZON set. Please choose another dataset and try again.",
									"Wrong Dataset", JOptionPane.WARNING_MESSAGE);
							return;
						}
					}
					Object[] horizons = null;
					String[] horizonArray= new String[0];
					if(horizonList != null){
						horizons = horizonList.toArray();
					//String filesep = messagingMgr.getServerOSFileSeparator();
					//int ind1 = filePath.lastIndexOf(filesep);
					//int ind2 = filePath.lastIndexOf(".");
					//String dataset = "";
					//if(ind2 != -1)
					//	dataset = filePath.substring(ind1+1, ind2);
					//if(dataset.length() > 0)
					//	dataset += ":";
						horizonArray= new String[horizons.length];
						for(int i = 0; i < horizons.length; i++){
							String hs = (String)horizons[i];
							//int index = hs.toLowerCase().lastIndexOf(".xyz");
							//if(index != -1){
							//	hs = hs.substring(0,index);
							//}
							horizonArray[i] = hs;
						}
					}
					if(comp instanceof javax.swing.JTextField){
						javax.swing.JTextField field = (javax.swing.JTextField) comp;
						if(askForHorizon){
							new HorizonSelectDialog(gui,field,true,horizonArray).setVisible(true);
						}else{
							String datasetsDir = QiProjectDescUtils.getDatasetsPath(qiProjectDesc);
							String dataset = filePath.substring(datasetsDir.length()+1);
							//gui.setDatasetTextField(dataset);
							field.setText(dataset);
							gui.setEntireDatasetButtonEnabled(true);
							gui.setSUDatasetDefaultValue((Map<String,DataSetHeaderKeyInfo>)datasetInfoList.get(0));
						}
					}
				}else
					ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
			}
		};
		new Thread(heavyRunnable).start();

	}

    public void doTask(String file, Component cmp){
		final Component comp = cmp;
		final String filePath = file;
		Runnable heavyRunnable = new Runnable(){
			public void run(){
				String title = "Gathering input dataset information";
				String text = "Please wait while system is gathering input dataset information....";

				Icon icon = null; //new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));

				com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(comp,100,true,500,title,text,icon);
				//monitor.start("");
				ProgressUtil.start(monitor,"");
				int status = getSeismicDatasetInfo(filePath);

				if(ProgressUtil.isCancel(monitor)){
					//if(monitor.isCancel()){
					gui.setEntireDatasetButtonEnabled(false);
					return;
				}
				logger.info("status = " + status);
				//if(status == 0 || status == 1 || status == -1){
				if(status == 0){
					ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
					//monitor.setCurrent(null,monitor.getTotal());
					//if(status == 0){
						Runnable updateAComponent = new Runnable() {
							public void run() {
								String datasetsDir = QiProjectDescUtils.getDatasetsPath(qiProjectDesc);
								String dataset = filePath.substring(datasetsDir.length()+1);
								gui.setDatasetTextField(dataset);
								gui.setEntireDatasetButtonEnabled(true);
								gui.setSUDatasetDefaultValue((Map<String,DataSetHeaderKeyInfo>)datasetInfoList.get(0));
							}
						};
						SwingUtilities.invokeLater(updateAComponent);
					//}else{
					//	JOptionPane.showMessageDialog(gui,"Error in running SU script.",
					//			"Problem in running SU script",JOptionPane.WARNING_MESSAGE);
					//}
				}else
					ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
			}
		};
		new Thread(heavyRunnable).start();

    }

/*
    public int getLandmarkProjectType(String project){
		if(project == null || project.trim().length() == 0)
			return -1;
		int itype = -1;
		if(!initLandmarkProjectHandler())
			return -1;
		if(landmarkAllProjects != null){
			for(int i = 0; i < landmarkAllProjects.size(); i++){
				ProjectInfo pi =landmarkAllProjects.get(i);
				if(pi.getName().equals(project))
					return pi.getType();
			}
		}
		String [] projs2D = landmarkAgent.getLandmarkProjectHandler().getProjects(2);
		for(int i = 0; projs2D != null && i < projs2D.length; i++){
			if(projs2D[i].equals(project)){
				itype = 2;
				break;
			}
		}
		if(itype == -1){
			String [] projs3D = landmarkAgent.getLandmarkProjectHandler().getProjects(3);
			for(int i = 0; projs3D != null && i < projs3D.length; i++){
				if(projs3D[i].equals(project)){
					itype = 3;
					break;
				}
			}
		}
		return itype;

    }
*/
/*    public String[] getLandmarkVolumeList(String project, int type, String ext){
		String [] volumes = new String[0];
		if(project == null || project.trim().length() == 0)
			return volumes;

		if(!initLandmarkProjectHandler())
			return volumes;
		if(!(type >= 2 && type <= 3)){
			int itype = getLandmarkProjectType(project);
			if(itype != -1)
				volumes = landmarkAgent.getLandmarkProjectHandler().getDatasetList(project, itype, ext);
		}else
			volumes = landmarkAgent.getLandmarkProjectHandler().getDatasetList(project, type, ext);
		return volumes;
	}
*/
    public void openLandmarkHorizonDialog(String project, int projectType, WaveletDecompGUI cmp, final JTextField target){
		if(project == null || project.trim().length() == 0 || !(projectType >= 2 && projectType <= 3))
			return;
//		if(!initLandmarkProjectHandler())
//			return;
		if(landmarkAgent == null){
			landmarkAgent = LandmarkServices.getInstance();
		}
		final WaveletDecompGUI comp = cmp;
		final String prj = project;
		final int prjType = projectType;
		Runnable heavyRunnable = new Runnable(){
			public void run(){
				String title = "Gathering Horizon Information";
				String text = "Please wait while system is gathering horizon information....";

				Icon icon = null; //new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));

				com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(comp,100,true,500,title,text,icon);
				//monitor.start("");
				ProgressUtil.start(monitor,"");
				final String[] out = landmarkAgent.getLandmarkProjectHandler().getHorizonList(prj, prjType);

				if(ProgressUtil.isCancel(monitor)){
					return;
				}

				if(out != null){
					ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
					//monitor.setCurrent(null,monitor.getTotal());
					Runnable updateAComponent = new Runnable() {
						public void run() {
							new HorizonSelectDialog(comp,target, true,out).setVisible(true);
						}
					};
					SwingUtilities.invokeLater(updateAComponent);
				}
			}
		};
		new Thread(heavyRunnable).start();
    }

    public boolean openLandmarkProject(String project){
		int type = landmarkAgent.getLandmarkProjectType(project);
		if(type == -1){
			logger.warning("Error in getting project type.");
			return false;
		}

		if(!landmarkAgent.getLandmarkProjectHandler().openProject(project, type, true)){
			String msg = landmarkAgent.getLandmarkProjectHandler().getMessage();
			JOptionPane.showMessageDialog(gui,msg,
					"Error in opening Landmark project " + project,JOptionPane.WARNING_MESSAGE);
			return false;
		}
		return true;
    }

	public void exportLandmarkHorizons(String project,List<JTextField> horizonList, int minline, int maxline, int mintrace,  int maxtrace){
		if(horizonList == null || horizonList.size() == 0)
			return;
		final String fproject = project;
		final int fminline = minline;
		final int fmaxline = maxline;
		final int fmintrace = mintrace;
		final int fmaxtrace = maxtrace;
		int type = landmarkAgent.getLandmarkProjectType(project);
		final int ftype = type;
		final List<JTextField> fhorizonList = horizonList;
		Runnable heavyRunnable = new Runnable(){
			public void run(){
				int i = 0;
				while(i < fhorizonList.size()){
					if(!exportLandmarkHorizon(fproject, ftype, fhorizonList.get(i).getText().trim(), fminline, fmaxline, fmintrace, fmaxtrace)){
						return;
					}
					String text = fhorizonList.size() == 1 ?  " horizon" : " horizons";
					text = (i+1) + " of " + fhorizonList.size() + text + ": " + fhorizonList.get(i).getText().trim() + " exported.";
					gui.addToStdOutTextArea(text);
					i++;
				}
				horizonExportDone = true;
			}
		};
		new Thread(heavyRunnable).start();
	}

	private boolean exportLandmarkHorizon(String project, int type, String horizon,int minline,int maxline,int mintrace,int maxtrace){
		if(project == null || project.length() == 0)
			return false;
		if(horizon == null || horizon.length() == 0)
			return false;
		try{
			Pipe pipe = Pipe.open();
			SynchronousQueue<String> queue = new SynchronousQueue<String>();
			LocalReadLMHorizon prod = new LocalReadLMHorizon(pipe.sink(),queue);
			String userProject = messagingMgr.getProject();
			String horizonOut = userProject + "/horizons/" + horizon.replaceAll(" ", "_");
			horizonOut += ".xyz";
			LocalWriteHorizon cons = new LocalWriteHorizon(pipe.source(), horizonOut);
			//LandmarkProjectHandler lh= new LandmarkProjectHandler(queue);
			LandmarkProjectHandler lh=  landmarkAgent.getLandmarkProjectHandler();
			//       generate a horizon
			prod.start();
			//save generate horizon to a file
			cons.start();
			if(!lh.getProjectHorizonInfo1(project, type, horizon, minline, maxline, mintrace, maxtrace, true)){
				lh.closeProject(type);
				String msg = lh.getMessage();
				logger.info("msg from LandmarkProjectHandler getProjectHorizonInfo1 " + msg);
				gui.setStdErrTextArea(msg);
				prod.setEOF();
				prod.setStop(true);
				JOptionPane.showMessageDialog(gui,msg,
						"Error in exporting Landmark horizon information.",JOptionPane.WARNING_MESSAGE);

				return false;
			}
			if(lh.getStop() == true){
				prod.setStop(true);
				lh.closeProject(type);
			}

		} catch (IOException ioe) {
			System.out.println("LandmarkProjectHandler: "+ioe.getMessage());
			return false;
		} catch (Exception ioe) {
			System.out.println("LandmarkProjectHandler: "+ioe.getMessage());
			return false;
		}
		return true;
		//};
		//new Thread(heavyRunnable).start();
	}
    /**
     * showErrorDialog is called when ErrorDialogService is needed
     * @param type is QIWConstants.ERROR_DIALOG or QIWConstants.WARNING_DIALOG
     * @param message is one-line message
     * @param stack is StackTrace
     * @param causes is list of possible causes
     * @param suggestions is list of suggested remedies
     */
    public void showErrorDialog(String type, StackTraceElement[] stack, String message,
            String[] causes, String[] suggestions) {
        ArrayList list = new ArrayList(7);
        // component
        list.add(gui);
        // message type
        list.add(type);
        // caller's display name
        list.add(getComponentDescriptor().getDisplayName());
        // stack trace
        list.add(stack);
        // message
        list.add(message);
        // possible causes
        list.add(causes);
        // suggested remedies
        list.add(suggestions);
        // send message to start error service
        getErrorService(list);
    }
    /**
     * showInternalErrorDialog starts error service for showing internal errors, for example, unexpected NULL etc
     * @param stack stackTrace
     * @param message one-line error message
     */
    public void showInternalErrorDialog(StackTraceElement[] stack, String message) {
        showErrorDialog(QIWConstants.ERROR_DIALOG,stack,message,
                new String[] {"Internal plugin error"},
                new String[] {"Contact workbench support"});
    }

    /**
     * getErrorService starts ErrorDialogService
     * @param list ArrayList of arguments
     */
    private void getErrorService(ArrayList list) {
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_ERROR_DIALOG_SERVICE_CMD,true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
        if(response == null || response.isAbnormalStatus()) {
            JOptionPane.showMessageDialog(null,"Error getting Error Service");
            return;
        }
        ComponentDescriptor errorServiceDesc = (ComponentDescriptor)response.getContent();
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.INVOKE_ERROR_DIALOG_SERVICE_CMD,
                errorServiceDesc,QIWConstants.ARRAYLIST_TYPE,list,true);
        //response = messagingMgr.getMatchingResponseWait(msgID,10000);
    }
    /**
     * Cancel a given shell script name by calling the linux command kill and ps
     * @param scriptName
     * @return
     */
    public boolean cancelJob(String scriptName){
        ArrayList params = new ArrayList();
        String cmd = "kill -9 `ps ux | awk '/" + scriptName +"/ && !/awk/ {print $2}'`";
        logger.info("cmd " + cmd);
        params.add(0,messagingMgr.getLocationPref());
        params.add(1,"sh");
        params.add(2,"-c");
        params.add(cmd);
        params.add("true");
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.SUBMIT_JOB_CMD,
                QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,5000);
        if(response == null){
            logger.info("response to SUBMIT_JOB_CMD for Cancel Job is null due to timed out problem.");
            return false;
        }else if(response.isAbnormalStatus()){
            logger.info("response to SUBMIT_JOB_CMD for Cancel Job with abnormal status: " + (String)response.getContent());
            return false;
        }else
            return true;

    }

    /**
     * Get process id of a given shell script name by calling the linux command
     * @param scriptName
     * @return process id
     */
    public String getProcessId(String scriptPath) {
        scriptPath = scriptPath.replace("/", "\\/");
        ArrayList params = new ArrayList();
        String cmd = "ps ux | awk '/" + scriptPath +"/ && !/awk/ {print $2}'";
        params.add(0,messagingMgr.getLocationPref());
        params.add(1,"sh");
        params.add(2,"-c");
        params.add(cmd);
        params.add("true");
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.SUBMIT_JOB_CMD,
                QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,5000);
        String scriptName = scriptPath;
        int ind = scriptName.lastIndexOf("/");
        if(ind != -1){
            scriptName = scriptName.substring(ind+1, scriptName.length());
        }
        if(response == null){
            logger.info("Response to SUBMIT_JOB_CMD getProcessId for " + scriptName + " is null due to timed out problem.");
            return null;
        }else if(response.isAbnormalStatus()){
            logger.info("Response to SUBMIT_JOB_CMD getProcessId for " + scriptName + " with abnormal status: " + (String)response.getContent());
            return null;
        }else{
            String jobID = (String)MsgUtils.getMsgContent(response);
            logger.info("SUBMIT_JOB_CMD jobID = " + jobID);
			//get stdout
			params.clear();
			//params.add(QIWConstants.LOCAL_SERVICE_PREF);
			params.add(messagingMgr.getLocationPref());
			params.add(jobID);
			msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
					QIWConstants.GET_JOB_OUTPUT_CMD,
					QIWConstants.ARRAYLIST_TYPE,params,true);
			response = messagingMgr.getMatchingResponseWait(msgID,5000);

			if(response == null){
				logger.info("Response to GET_JOB_OUTPUT_CMD getProcessId for " + scriptName + " is null due to timed out problem.");
				return null;
			}else if(response.isAbnormalStatus()) {
				logger.info("Response to GET_JOB_OUTPUT_CMD getProcessId for " + scriptName + " with abnormal status: " + (String)response.getContent());
				return null;
			}else{
				ArrayList stdOut = (ArrayList)response.getContent();
				logger.info("stdOut getProcessId for " + scriptName + " = " + stdOut);
				if(stdOut != null && stdOut.size() > 0)
					return (String)stdOut.get(0);
				else
					return "";
			}
		}
	}

    public String getJobStatus(){
        if(jobID != null){
            ArrayList params = new ArrayList();
            params.add(messagingMgr.getLocationPref());
            params.add(jobID);
            String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                    QIWConstants.GET_JOB_STATUS_CMD,
                    QIWConstants.ARRAYLIST_TYPE,params,true);
            IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
            if(response == null){
                logger.info("Response to GET_JOB_STATUS_CMD returning null due to timed out problem.");
                return "Messaging Timed Out";
            }else if(MsgUtils.isResponseAbnormal(response)){
                logger.info("Abnormal response to GET_JOB_STATUS_CMD due to " + MsgUtils.getMsgContent(response));
                return "Abnormal Messaging Response";
            }else{
                int status = ((Integer)response.getContent()).intValue();
                if(status == -1)
                    return "Job Running";
                else
                    return "Job ends with status of " + status;
            }
        }else{
            logger.info("The job id is not yet available for browsing job status information.");
            return "No Running Job Found";
        }

    }

    /**
     * call the state manager to store the state
     */
    public void saveState(){
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_CMD,stMgrDesc,
                QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
    }

    /**
     * call the state manager to store the state
     */
    public void saveStateAsClone(){
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_AS_CLONE_CMD,stMgrDesc,
                QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
    }

    /**
     * call the state manager to store the state then quit the component
     */
    public void saveStateThenQuit(){
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_THEN_QUIT_CMD,stMgrDesc,
                QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
    }

    private int getDTvalue(){
        return (int)seismicDataset.get("tracl").getIncr();
    }

    /** Launch the Wavelet Decomposition plugin:
     *  <ul>
     *  <li>Start up the plugin thread which will initialize the plugin.</li>
     *  </ul>
     * <p>
     * NOTE: Each thread's init() must finish before the next thread is
     * started. This is accomplished by monitoring the SYNC_LOCK object.
     */
    public static void main(String[] args) {
        WaveletDecompPlugin sdPluginInstance = new WaveletDecompPlugin();
        //CID has to be passed in since no way to get it back out
        String cid = args[0];
        // use the CID as the name of the thread
        pluginThread = new Thread(sdPluginInstance, cid);
        pluginThread.start();
        long threadId = pluginThread.getId();
        logger.info("Wavelet Decomposition Thread-"+Long.toString(threadId)+" started");
        // When the plugin's init() is finished, it will release the lock

        // wait until the plugin's init() has finished
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();
        logger.info("Wavelet Decomposition main finished");
    }
}
