/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bhpb.WaveletDecomposition;

import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 
 */
public class ScriptGenerator {
    private WaveletDecompGUI gui;
    public ScriptGenerator(WaveletDecompGUI gui){
        this.gui = gui;
    }


    /**
	 * Generate common header script code such as QISPACE, QIPROJECT .. etc.
	 */
	public static List<String> genHeadScripts(QiProjectDescriptor qiProjectDesc, String filesep){
		List<String> script = new ArrayList<String>();
		if(qiProjectDesc == null)
			return script;

		// make sure the binary of SU and BHP-SU on PATH
		String command = "source ~/.bashrc";
		script.add(command);

		// alias rm
		// silently remove files so script can run standalone
		command = "alias rm=\"rm -f\"";
		script.add(command);

		// alias cp
		command = "alias cp=cp";
		script.add(command);
		//in case user want ot debug the script
        script.add("#set -x");
		script.add("");
		script.add("# Parameters specified in the qiProject");

		// export QISPACE
		String qiSpace = QiProjectDescUtils.getQiSpace(qiProjectDesc);
		script.add("# qiSpace containing one or more projects");
		command = "export QISPACE=\"" + qiSpace + "\"";
		script.add(command);

		// export QIPROJECT
		String qiProject = QiProjectDescUtils.getQiProjectReloc(qiProjectDesc);
		script.add("# location of qiProject relative to $QISPACE");
		command = "export QIPROJECT=\"" + qiProject + "\"";
		script.add(command);

		// export QIDATASETS
		String qiDatasets = QiProjectDescUtils.getDatasetsReloc(qiProjectDesc);
		script.add("# location of datasets relative to $QISPACE/$QIPROJECT");
		command = "export QIDATASETS=\"" + qiDatasets + "\"";
		script.add(command);

		// export QIHORIZONS
		String qiHorizons = QiProjectDescUtils.getHorizonsReloc(qiProjectDesc);
		script.add("# location of horizons relative to $QISPACE/$QIPROJECT");
		command = "export QIHORIZONS=\"" + qiHorizons + "\"";
		script.add(command);

		ArrayList<String> seismicDirs = QiProjectDescUtils.getSeismicDirs(qiProjectDesc);
		StringBuffer qiSeismics = new StringBuffer();
		//Note: there is always at least 1 seismic directory
		String projectPath = QiProjectDescUtils.getProjectPath(qiProjectDesc);
		for(int i = 0; seismicDirs != null && i < seismicDirs.size(); i++){
			qiSeismics.append(projectPath + filesep + seismicDirs.get(i));
			if(i < seismicDirs.size() -1){
				qiSeismics.append("\n");
			}
		}
		script.add("# list of seismic directories separated by a line separator (\\n)");
		command = "export QISEISMICS=\"" +  qiSeismics.toString() + "\"";

		script.add(command);
		return script;
	}

}
