/**
 ###########################################################################
 # WaveletDecomp - A continuous time-frequency analysis technique that
 # computes frequency spectrum for each time sample of a seismic trace.
 # Copyright (C) 2006  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
*/

package com.bhpb.WaveletDecomposition;

import java.awt.Component;
import java.awt.ContainerOrderFocusTraversalPolicy;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.dnd.DnDConstants;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.filechooser.FileFilter;

import org.jdesktop.layout.GroupLayout;
import org.jdesktop.layout.GroupLayout.ParallelGroup;
import org.jdesktop.layout.GroupLayout.SequentialGroup;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.client.util.GenericFileFilter;
import com.bhpb.qiworkbench.client.util.QiFileChooserDescriptor;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ComponentStateUtils;
import com.bhpb.qiworkbench.compAPI.ComponentUtils;
import com.bhpb.qiworkbench.compAPI.ControlConstants;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.compAPI.XmlUtils;
import com.bhpb.services.landmark.LandmarkServices;
import java.text.NumberFormat;

/**
 * The GUI for the Wavelet Decomposition plugin. It is not considered a separate
 * component so it has no its own message queue. Instead it use its plugin agent
 * for handling message passing. It is not necessary to start the GUI as a
 * thread because by definition a Swing GUI is a separate thread.
 */
public class WaveletDecompGUI extends JInternalFrame {
    private static Logger logger = Logger.getLogger(WaveletDecompGUI.class
            .getName());

    // Variables declaration - do not modify//GEN-BEGIN:variables

    private javax.swing.JButton landmarkOutListButton;
    private javax.swing.JLabel landmarkOutProjectLabel;
    private javax.swing.JTextField landmarkOutProjectTextField;
    private javax.swing.JTextField landmark2DOutProjectTextField;
    private javax.swing.JTextField landmark3DOutProjectTextField;
    private javax.swing.JPanel landmarkProjectPanel;

    private javax.swing.JTextField averageVelocityTextField;

    private javax.swing.JLabel averageVelociyLabel;

    private javax.swing.JRadioButton bhpsuInRadioButton;
    private javax.swing.JRadioButton bhpsuOutRadioButton;

    private javax.swing.JButton cancelJobButton;
    private javax.swing.JButton writeScriptButton;
    private javax.swing.JButton runScriptButton;

    private javax.swing.JTextField bhpsuNameBaseTextField;

    private javax.swing.JTextField landmarkNameBaseTextField;
    private javax.swing.JTextField landmark2DNameBaseTextField;
    private javax.swing.JTextField landmark3DNameBaseTextField;

    private javax.swing.JTextField bhpsuNoiseFactorTextField;

    private javax.swing.JTextField landmarkNoiseFactorTextField;
    private javax.swing.JTextField landmark2DNoiseFactorTextField;
    private javax.swing.JTextField landmark3DNoiseFactorTextField;

    private javax.swing.JRadioButton bhpsuScaleRawRadioButton;

    //private javax.swing.JCheckBox landmarkScaleRawCheckBox;
    private javax.swing.JRadioButton landmarkScaleRawRadioButton;
    private javax.swing.JRadioButton landmark2DScaleRawRadioButton;
    private javax.swing.JRadioButton landmark3DScaleRawRadioButton;

    private javax.swing.JRadioButton bhpsuScaleNormalizedRadioButton;


    //private javax.swing.JCheckBox landmarkScaleNormalizedCheckBox;
    private javax.swing.JRadioButton landmarkScaleNormalizedRadioButton;
    private javax.swing.JRadioButton landmark2DScaleNormalizedRadioButton;
    private javax.swing.JRadioButton landmark3DScaleNormalizedRadioButton;

    private List<JButton> suChooseHorzButtonList;
    private List<JButton> landmarkChooseHorzButtonList;

    private javax.swing.JMenuItem deleteMenuItem;

    private javax.swing.JPanel detailsPanel;

    private javax.swing.JScrollPane stdOutScrollPane;
    private javax.swing.JScrollPane stdErrScrollPane;
    private javax.swing.JScrollPane summaryScrollPane;
    private javax.swing.JTextArea stdOutTextArea;
    private javax.swing.JTextArea stdErrTextArea;
    private javax.swing.JTextArea summaryTextArea;

    private javax.swing.JTextField suEndTimeTextField;
    private javax.swing.JTextField landmarkEndTimeTextField;

    private javax.swing.JTextField timeIncrementTextField;
    private javax.swing.JTextField sampleRateTextField;

    private javax.swing.JButton entireVolumeButton;

    private javax.swing.JMenu executeMenu;

    private javax.swing.JMenu fileMenu;

//    private javax.swing.JMenu editMenu;

    private javax.swing.JPanel flattenedPanel;

    private javax.swing.JPanel flatteningPanel;

    //private javax.swing.JCheckBox horizonNormalizedCheckBox;
    private javax.swing.JRadioButton horizonNormalizedRadioButton;
    //private javax.swing.JCheckBox horizonRawCheckBox;
    private javax.swing.JRadioButton horizonRawRadioButton;

    private javax.swing.JLabel horizonVolumesLabel;

    private javax.swing.JComboBox horizonsComboBox;
    private javax.swing.JComboBox numHorizonsComboBox;

    private javax.swing.JLabel horizonsLabel;

    private javax.swing.JPanel horizonsPanel;

    private List<JLabel> suHorzLabelList;
    private List<JLabel> landmarkHorzLabelList;
    private List<JLabel> landmarkOutputTimeLabelList;
    private List<JLabel> landmarkOutputScaleLabelList;
    private List<JLabel> landmark2DOutputScaleLabelList;
    private List<JLabel> landmark3DOutputScaleLabelList;

    private List<JTextField> suHorzTextFieldList;
    private List<JTextField> landmarkHorzTextFieldList;
    private List<JTextField> landmarkOutputTimeTextFieldList;
    private List<JTextField> landmarkOutputScaleTextFieldList;
    private List<JTextField> landmark2DOutputScaleTextFieldList;
    private List<JTextField> landmark3DOutputScaleTextFieldList;

    private List<javax.swing.JTextField> suHorzTimeTextFieldList;
    private List<javax.swing.JTextField> landmarkHorzTimeTextFieldList;

    private javax.swing.JPanel bhpsuInputDataPanel;
    //private javax.swing.JPanel bhpsuHorizonDataPanel;
    private javax.swing.JPanel horizonTopPanel;
    private javax.swing.JPanel bhpsuHorizonPanel;
    private javax.swing.JPanel landmarkHorizonDataPanel;
    private javax.swing.JPanel landmarkHorizonPanel;


    private javax.swing.JPanel landmarkInputDataPanel;

    private javax.swing.JLabel jLabel1;

    private javax.swing.JLabel jLabel2;

    private javax.swing.JLabel jLabel3;

    private javax.swing.JPanel landmarkInPanel;
    private javax.swing.JPanel landmarkIn2D3DDataPanel;

    private javax.swing.JRadioButton landmarkInRadioButton;
    private javax.swing.JRadioButton landmarkHorizonRadioButton;
    private javax.swing.JRadioButton bhpsuHorizonRadioButton;
    private javax.swing.JRadioButton bhpsuHorizonDatasetRadioButton;

    private javax.swing.JRadioButton landmarkOutRadioButton;

    private javax.swing.JPanel landmarkOutPanel;

    private javax.swing.JPanel detailScalePanel;
    private javax.swing.JPanel scalePanel;
    private javax.swing.JLabel landmarkNumScaleLabel;
    private javax.swing.JComboBox landmarkNumOfScalesComboBox;
    private javax.swing.JComboBox landmark2DNumOfScalesComboBox;
    private javax.swing.JComboBox landmark3DNumOfScalesComboBox;
    private javax.swing.JLabel largestScaleLabel;

    private javax.swing.JTextField largestScaleTextField;

    private javax.swing.JRadioButton landmark2DRadioButton;
    private javax.swing.JRadioButton landmark3DRadioButton;
    private javax.swing.JButton entireVolume2DButton;
    private javax.swing.JTextField landmarkIn2DProjectTextField;
    private javax.swing.JTextField linesInTextField;
    private javax.swing.JLabel linesLabel;
    private javax.swing.JLabel selectedLinesLabel;
    private javax.swing.JButton linesListButton;
    private javax.swing.JButton list2DProjectsButton;
    private javax.swing.JComboBox seismic2DFileComboBox;
    private javax.swing.JScrollPane lineListScrollPane;
    private javax.swing.JScrollPane selectedLineListScrollPane;
    private javax.swing.JList seismic2DLineList;
    private javax.swing.JList selectedSeismic2DLineList;
    private javax.swing.JPanel seismicLinesPanel;
    private javax.swing.JLabel seismicFileLabel;
    private javax.swing.JButton seismicFileListButton;
    private javax.swing.JTextField seismicFileTextField;
    private javax.swing.JButton select2DButton;
    private javax.swing.JTextField shotpointByTextField;
    private javax.swing.JLabel shotpointLabel;
    private javax.swing.JTextField shotpointTextField;
    private javax.swing.JTextField shotpointToTextField;
    private javax.swing.JButton leftArrowButton;
    private javax.swing.JButton rightArrowButton;

    private javax.swing.JLabel linesByLabel;

    private javax.swing.JLabel cdpByLabel;

    private javax.swing.JTextField linesByTextField;

    private javax.swing.JTextField cdpByTextField;

    private javax.swing.JTextField linesTextField;

    private javax.swing.JTextField cdpTextField;

    private javax.swing.JLabel linesToLabel;
    private javax.swing.JLabel timeToLabel;
    private javax.swing.JLabel unitLabel;

    private javax.swing.JLabel cdpToLabel;

    private javax.swing.JTextField linesToTextField;

    private javax.swing.JTextField cdpToTextField;

    private javax.swing.JButton listProjectsButton;

    private javax.swing.JRadioButton listRadioButton;

    private javax.swing.JButton listVolumesButton;

    private javax.swing.JLabel bhpsuNameBaseLabel;

    private javax.swing.JLabel landmarkNameBaseLabel;

    private javax.swing.JMenuItem newMenuItem;

    private javax.swing.JLabel bhpsuNoiseFactorLabel;

    private javax.swing.JLabel landmarkNoiseFactorLabel;

    private javax.swing.JLabel numberScalesLabel;

    private javax.swing.JTextField numberScalesTextField;

    private javax.swing.JMenuItem openMenuItem;

    private javax.swing.JPanel bhpsuOutputsPanel;

    private javax.swing.JPanel landmarkOutputsPanel;

    private javax.swing.JPanel parametersPanel;

    private javax.swing.JLabel placementLabel;

    private javax.swing.JLabel bhpsuProjectLabel;

    private javax.swing.JLabel landmarkProjectLabel;

    private javax.swing.JTextField landmarkInProjectTextField;

    private javax.swing.JMenuItem quitMenuItem;

    private javax.swing.JLabel bhpsuRangeLabel;

    private javax.swing.JLabel landmarkRangeLabel;

    private javax.swing.JLabel rangeLinesLabel;

    private javax.swing.JMenuItem renameMenuItem;

    private javax.swing.JMenuItem runMenuItem;
    private javax.swing.JMenuItem writeMenuItem;
    private javax.swing.JMenuItem cancelRunMenuItem;

    private javax.swing.JMenuItem saveAsMenuItem;

    private javax.swing.JMenuItem saveMenuItem;
//    private javax.swing.JMenuItem updateProjectEnvMenuItem;

    private javax.swing.JMenuItem saveQuitMenuItem;

    private javax.swing.JMenuItem preferenceMenuItem;

    private javax.swing.JLabel bhpsuScaleVolumesLabel;

    private javax.swing.JLabel landmarkScaleVolumesLabel;

    private javax.swing.JButton selectButton;

    private javax.swing.JPanel inputParametersSmoothingPanel;
    private javax.swing.JPanel inputParametersScalePanel;
    private javax.swing.JLabel numberOfPassLabel;
    private javax.swing.JTextField numberOfPassTextField;
    private javax.swing.JLabel averageIncreaseLabel;
    private javax.swing.JLabel smallestScaleLabel;

    private javax.swing.JTextField averageIncreaseTextField;
    private javax.swing.JTextField smallestScaleTextField;
    private javax.swing.JLabel smoothingLengthLabel;

    private javax.swing.JTextField smoothingLengthTextField;

    private javax.swing.JMenuBar specDecompMenuBar;

    private javax.swing.JTabbedPane specDecompTabbedPane;

    private javax.swing.JTextField landmarkStartTimeTextField;
    private javax.swing.JTextField suStartTimeTextField;


    private javax.swing.JLabel stateLabel;

    private javax.swing.JLabel statusLabel;

    private javax.swing.JMenuItem statusMenuItem;

    private javax.swing.JPanel statusPanel;

    private javax.swing.JComboBox timeComboBox;

    private javax.swing.JPanel timePanel;
    private javax.swing.JScrollPane timeScrollPane;
    private javax.swing.JScrollPane scaleScrollPane;
    private javax.swing.JLabel tracesByLabel;

    private javax.swing.JLabel epByLabel;

    private javax.swing.JTextField tracesByTextField;

    private javax.swing.JTextField epByTextField;

    private javax.swing.JLabel tracesLabel;
    private javax.swing.JLabel timeLabel;

    private javax.swing.JLabel epLabel;

    private javax.swing.JTextField tracesTextField;
    private javax.swing.JTextField timeTextField;
    private javax.swing.JTextField timeToTextField;

    private javax.swing.JTextField epTextField;

    private javax.swing.JLabel tracesToLabel;

    private javax.swing.JLabel epToLabel;

    private javax.swing.JTextField tracesToTextField;

    private javax.swing.JTextField epToTextField;

    private javax.swing.JButton updateButton;

    private javax.swing.JRadioButton viewerRadioButton;

    private javax.swing.JLabel volumeLabel;

    private javax.swing.JTextField volumeTextField;

    private javax.swing.JMenu viewMenu;

    private javax.swing.JMenu helpMenu;

    private javax.swing.JMenuItem helpAboutMenuItem;

    private javax.swing.JMenuItem viewer2DMenuItem;

    private javax.swing.JMenuItem viewer3DMenuItem;

    // Seismic Data Input widgets.
    private javax.swing.JPanel bhpsuInPanel;

    private javax.swing.JLabel datasetLabel;

    private javax.swing.JButton chooseDatasetButton;

    private javax.swing.JLabel rangeCdpLabel;

    private javax.swing.JLabel rangeEpLabel;

    private javax.swing.JButton entireDatasetButton;

    private javax.swing.JButton pickButton;

    private javax.swing.JLabel selectedProjectLabel;

    private javax.swing.JTextField datasetTextField;

    private javax.swing.JSeparator fileSeparator;
    private javax.swing.JMenuItem importMenuItem;
    private javax.swing.JMenuItem exportMenuItem;

    private int selectedHorizonIndex = -1;
    private int suHorizonCount = 0;
    private int landmarkHorizonCount = 0;
    private int bhpsuHorizonCount = 0; //number of horizons as ASCII set (.xyz)

    private int landmarkOutputTimeCount = 0;
    private int landmark2DOutputTimeCount = 0;
    private int landmark3DOutputTimeCount = 0;
    private int landmarkOutNumScalesCount = 0;
    private int landmark2DOutNumScalesCount = 0;
    private int landmark3DOutNumScalesCount = 0;
    // Outputs widgets
    private javax.swing.JPanel bhpsuOutPanel;
    private javax.swing.JMenuItem displayResultsMenuItem;
	private javax.swing.JButton displayResultsButton;

    // End of variables declaration//GEN-END:variables
	
    private JScrollPane scrollPane;
    // Tab indices
    private static final int INPUT_TAB_INDEX = 0;

    private static final int IN_PARAMS_TAB_INDEX = 1;

    private static final int FLATTENING_TAB_INDEX = 2;

    private static final int OUTPUTS_TAB_INDEX = 3;

    private static final int STATUS_TAB_INDEX = 4;

    // Seismic formats
    private static final int LANDMARK_FORMAT = 0;

    private static final int BHPSU_FORMAT = 1;

    int seismicFormatIn = BHPSU_FORMAT;

    int seismicFormatOut = BHPSU_FORMAT;

    private WaveletDecompPlugin agent;
    private LandmarkServices landmarkAgent;
    //private String landmarkProjectType;
    private String landmarkOutProjectType;
    // Name of project from Workbench Manager (not full path)
    String project_name = "";

    // Project's path
    /** File separator for server OS. */
    String filesep = "/";
    HelpBroker WaveletDecompHB;

    Component gui;

    // GUI State
    // Seismic Input tab
    String landmarkProject = "";

    String landmarkVolume = "";

    String landmarkLinesMin = "";

    String landmarkLinesMax = "";

    String landmarkLinesInc = "";

    String landmarkTracesMin = "";

    String landmarkTracesMax = "";

    String landmarkTracesInc = "";

    String bhpsuDataset = "";

    String bhpsuCdpMin = "";

    String bhpsuCdpMax = "";

    String bhpsuCdpInc = "";

    String bhpsuEpMin = "";

    String bhpsuEpMax = "";

    String bhpsuEpInc = "";

    // Input Parameters tab
    String smallestScale = "";

    String largestScale = "";

    String numberScales = "";

    String avVelocity = "";

    String smoothingLength = "";

    // Stratigraphic Flattening tab
    String numberHorizons = "";

    ArrayList horizons = new ArrayList();

    String startTime = "";

    String endTime = "";

    ArrayList times = new ArrayList();

    // Outputs tab
    String landmarkNameBase = "";

    String landmarkRawScale = "";

    String landmarkNormalizedScale = "";

    String landmarkNoiseFactor = "";

    String landmarkRawHorizon = "";

    String landmarkNormalizedHorizon = "";

    String horizonPlacement = "";

    ArrayList placementTimes = new ArrayList();

    String bhpsuNameBase = "";

    String bhpsuRawScale = "";

    String bhpsuNormalizedScale = "";

    String bhpsuNoiseFactor = "";

    // Job Status tab
    String jobStatus = "";

    String statusDetails = "";

    String lastUpdate = "";

    boolean jobCanceled = false;

    //4 by 3 dimension {{"min line", "max line", "line inc"},{"min trace", "max trace", "trace inc"},
    // {min_time,max_time,increment_time},{data_type,dummy,dummy}}}
    private int lineTraceArray[][] = {{0,0,0},{0,0,0},{0,0,0},{0,0,0}};

    private int defaultcdpBy;

    private int defaultepBy;

    private int defaultcdpMin;

    private int defaultcdpMax;

    private int defaultepMin;

    private int defaultepMax;
    private int selectedProcessLevelIndex = 0;
    private Vector selectedLines = new Vector();

    private static final int defaultSmallesetScale = 0;


    private static final double defaultLargestScale = 1.5;

    private static final int defaultNumberOfScales = 15;
    private static final int avergeVelocity = 2500;
    private static final int defaultSmoothLength = 10;
    private static final int defaultNumOfPass = 3;
    private static final double defaultAvgIncrease = 0.1;
    private static final int defaultTimeIncrement = 4;

    private static final int defaultGamma = 15;

    private String defaultPreferredScriptDir = "";

    //private LandmarkProjectHandler projectHandler;

    private SimpleDateFormat formatter = new SimpleDateFormat(
            "yyyy_MM_dd_HH_mm_ss_SSS");
    private static String scriptTemplate ="";
    private String selectedLandmarkProject;
    private String currentRunningScriptPrefix ="";
    private int job_status = WaveletDecompConstants.JOB_UNSUBMITTED_STATUS;
    private Properties props;
    //QiProjectDescriptor qiProjectDesc;
    private Map<String,LineInfo> selectedLineMap = new HashMap<String,LineInfo>();
    /*static{
        Class me = WaveletDecompGUI.class;
        InputStream in = me.getResourceAsStream("/script_template.txt");
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        try {
            String inputLine = "";
            while ((inputLine = reader.readLine()) != null) {
                scriptTemplate += inputLine + "\n";
            }
        } catch (IOException ioe) {
            logger.warning("IO exception reading script_template.txt file:"
                    + ioe.getMessage());
        } finally {
            try {
                if (in != null)
                    in.close();
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
            }
        }
    }
*/

	/** Number of horizons selected for strategraphic flattening */
	int numberOfHorz = 0;
	public int getNumOfHorizons() {
		return numberOfHorz;
	}
	
	public WaveletDecompPlugin getAgent() {
		return agent;
	}
	
    /**
     * Constructor
     *
     * @param agent
     *            WaveletDecompPlugin agent
     */
    public WaveletDecompGUI(WaveletDecompPlugin agent) {
        this.agent = agent;
        QiProjectDescriptor  qiProjectDesc = agent.getProjectDescriptor();
        this.project_name = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
        logger.info("project_name = " + project_name);

        filesep = agent.getMessagingMgr().getServerOSFileSeparator();
        //projectHandler = new LandmarkProjectHandler();
        // Get my Component for use in locating dialogs
        gui = (Component) this;
        // fileManager = new WaveletDecompFileManager(agent);
        startGUI();
         //make frame iconable and maximizable.
        this.setIconifiable(true);
        this.setMaximizable(true);

        // Disable ability to display the results until script execution finishes.
		displayResultsButton.setEnabled(false);
        displayResultsMenuItem.setEnabled(false);
    }

    /*
     * private constructor for test only
     */
    private WaveletDecompGUI() {
        startGUI();
        //make frame iconable and maximizable.
        this.setIconifiable(true);
        this.setMaximizable(true);

    }

    public void setEntireDatasetButtonEnabled(boolean enable) {
        if(enable == false){
            cdpTextField.setText("");
            cdpToTextField.setText("");
            cdpByTextField.setText("");
            epTextField.setText("");
            epToTextField.setText("");
            epByTextField.setText("");
            defaultcdpMin = -1;
            defaultcdpMax = -1;
            defaultcdpBy = -1;
            defaultepMin = -1;
            defaultepMax = -1;
            defaultepBy = -1;
            agent.emptySeismicDataset();
        }
        cdpTextField.setEnabled(enable);
        cdpToTextField.setEditable(enable);
        cdpByTextField.setEditable(enable);
        epTextField.setEnabled(enable);
        epToTextField.setEditable(enable);
        epByTextField.setEditable(enable);
        entireDatasetButton.setEnabled(enable);
    }

    private void startGUI() {
        // help
        try {
            ClassLoader cl = WaveletDecompGUI.class.getClassLoader();
            // TODO create the WaveletDecomp help pages
            String helpsetName = "com/bhpb/help/WaveletDecompHelp.hs";
            URL helpSetURL = HelpSet.findHelpSet(cl, helpsetName);
            HelpSet WaveletDecompHS = new HelpSet(cl, helpSetURL);
            WaveletDecompHB = WaveletDecompHS.createHelpBroker();
        } catch (Exception e) {
            agent.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
            "WaveletDecomp plugin couldn't find Java help");
        }

        initComponents();

        // Group the common radio buttons.
        ButtonGroup formatGroupIn = new ButtonGroup();
        formatGroupIn.add(landmarkInRadioButton);
        formatGroupIn.add(bhpsuInRadioButton);

        ButtonGroup formatGroupHorizon = new ButtonGroup();
        formatGroupHorizon.add(landmarkHorizonRadioButton);
        formatGroupHorizon.add(bhpsuHorizonRadioButton);
        formatGroupHorizon.add(bhpsuHorizonDatasetRadioButton);

        ButtonGroup formatGroupOut = new ButtonGroup();
        formatGroupOut.add(landmarkOutRadioButton);
        formatGroupOut.add(bhpsuOutRadioButton);

        ButtonGroup landmarkTypeGroup = new ButtonGroup();
        landmarkTypeGroup.add(landmark3DRadioButton);
        landmarkTypeGroup.add(landmark2DRadioButton);


        // get project from message dispatcher
        if (agent != null) {
            //String proj = agent.getMessagingMgr().getProject();
            QiProjectDescriptor desc = agent.getProjectDescriptor();
            //String proj = QiProjectDescUtils.getQiSpace(desc) + agent.getMessagingMgr().getProject();

            //int ind = proj.lastIndexOf(filesep);

            //if(ind != -1){
                project_name = QiProjectDescUtils.getQiProjectName(desc);
                //defaultPreferredScriptDir= QiProjectDescUtils.getProjectPath(desc) + filesep + "tmp";
                defaultPreferredScriptDir = QiProjectDescUtils.getTempPath(desc);
//                this.setTitle(CompDescUtils.getDescDisplayName(agent.getMessagingMgr().getMyComponentDesc())
//                    + "  Project: " + project_name);
                resetTitle(project_name);
            //}
        }

    }

    private boolean validateFlatteningInfo(List<JLabel> horiLabList,List<JTextField> horiTFList, JTextField startTimeTF, JTextField endTimeTF, List<JTextField> timeTFList ) {
        final Component comp = this;
        //if (horiTFList == null || horiTFList.size() == 0) {
        //  JOptionPane.showMessageDialog(comp, "No horizon dataset found.",
        //          "Missing data entry", JOptionPane.WARNING_MESSAGE);
        //  specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
        //  return false;
        //}

        if (horiTFList == null || horiTFList.size() == 0) {
            return true;
        }

        for (int i = 0; horiTFList != null
                && i < horiTFList.size(); i++) {
            String s = horiTFList.get(i).getText();
            if (s != null && s.trim().length() == 0) {
                JOptionPane.showMessageDialog(comp, "The "
                        + horiLabList.get(i).getText() + " field is empty.",
                        "Missing data entry", JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
                horiTFList.get(i).requestFocus();
                return false;
            }
            if(bhpsuHorizonDatasetRadioButton.isSelected()){
            	if (s != null) {
            		String [] arr = s.split(":");
            		if(arr == null || arr.length != 2){
            			JOptionPane.showMessageDialog(comp, "The "
                        + horiLabList.get(i).getText() + " field is not in right format (bhpsu_dataset:horizon_name).",
                        "Missing data entry", JOptionPane.WARNING_MESSAGE);
            			specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
            			horiTFList.get(i).requestFocus();
            			return false;
            		}
            	}
            }else
            if(bhpsuHorizonRadioButton.isSelected()){
            	if (s != null) {
            		if(s.indexOf(":") != -1){
            			JOptionPane.showMessageDialog(comp, "The "
                        + horiLabList.get(i).getText() + " field is not in right format.",
                        "Missing data entry", JOptionPane.WARNING_MESSAGE);
            			specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
            			horiTFList.get(i).requestFocus();
            			return false;
            		}
            	}
            }


        }

        String str = startTimeTF.getText();
        if (str != null && str.trim().length() == 0) {
            JOptionPane.showMessageDialog(comp,
                    "Please enter a numeric value into Start Time field.",
                    "Missing data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
            startTimeTF.requestFocus();
            return false;
        }
        int startTime = -1;
        try {
            startTime = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp,
                    "Start Time must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
            startTimeTF.requestFocus();
            return false;
        }

        str = endTimeTF.getText();
        if (str != null && str.trim().length() == 0) {
            JOptionPane.showMessageDialog(comp,
                    "Please enter a numeric value into End Time field.",
                    "Missing data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
            endTimeTF.requestFocus();
            return false;
        }
        int endTime = -1;
        try {
            endTime = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp,
                    "End Time must be a numeric value.", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
            endTimeTF.requestFocus();
            return false;
        }

        if (timeTFList != null) {
            int[] arrTime = new int[timeTFList.size() + 2];
            arrTime[0] = startTime;
            for (int i = 0; i < timeTFList.size(); i++) {
                String s = timeTFList.get(i).getText();
                if (s != null && s.trim().length() == 0) {
                    JOptionPane.showMessageDialog(comp, "The "
                            + horiLabList.get(i).getText()
                            + " time field is empty.", "Missing data entry",
                            JOptionPane.WARNING_MESSAGE);
                    specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
                    timeTFList.get(i).requestFocus();
                    return false;
                }
                try {
                    arrTime[i + 1] = Integer.parseInt(s);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(comp, "The "
                            + horiLabList.get(i).getText()
                            + " time field must be a numeric value.",
                            "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                    specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
                    timeTFList.get(i).requestFocus();
                    return false;
                }
            }
            arrTime[timeTFList.size() + 1] = endTime;

            int[] arrTimeBak = new int[arrTime.length];
            for (int i = 0; i < arrTime.length; i++) {
                arrTimeBak[i] = arrTime[i];
            }

            Arrays.sort(arrTimeBak);

            for (int i = 0; i < arrTimeBak.length; i++) {
                for (int j = 0; j < arrTimeBak.length; j++) {
                    if ((i != j) && (arrTimeBak[i] == arrTimeBak[j])) {
                        JOptionPane.showMessageDialog(comp,
                                "The time fields must be distinct values.",
                                "Invalid data entry",
                                JOptionPane.WARNING_MESSAGE);
                        specDecompTabbedPane
                                .setSelectedIndex(FLATTENING_TAB_INDEX);
                        return false;
                    }
                }
            }
            for (int i = 0; i < arrTime.length; i++) {
                if (arrTime[i] != arrTimeBak[i]) {
                    JOptionPane
                            .showMessageDialog(
                                    comp,
                                    "The time fields must be in ascending order (start to end).",
                                    "Invalid data entry",
                                    JOptionPane.WARNING_MESSAGE);
                    specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
                    return false;
                }
            }
        }
        return true;
    }

    /* not currently used
    private boolean validateFlatteningInfo(Component comp, List<JTextField> horiLabList, List<JTextField> horiTFList, JTextField startTimeTF, JTextField endTimeTF, List<JTextField> timeTFList) {
        if (horiTFList == null || horiTFList.size() == 0)
            return false;

        for (int i = 0; horiTFList != null
                && i < horiTFList.size(); i++) {
            String s = horiTFList.get(i).getText();
            if (s != null && s.trim().length() == 0) {
                JOptionPane.showMessageDialog(comp, "The "
                        + horiLabList.get(i).getText() + " field is empty.",
                        "Missing data entry", JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
                horiTFList.get(i).requestFocus();
                return false;
            }
        }

        String str = startTimeTF.getText();
        if (str != null && str.trim().length() == 0) {
            JOptionPane.showMessageDialog(comp,
                    "Please enter a numeric value into Start Time field.",
                    "Missing data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
            startTimeTF.requestFocus();
            return false;
        }
        int startTime = -1;
        try {
            startTime = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp,
                    "Start Time must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
            startTimeTF.requestFocus();
            return false;
        }

        str = endTimeTF.getText();
        if (str != null && str.trim().length() == 0) {
            JOptionPane.showMessageDialog(comp,
                    "Please enter a numeric value into End Time field.",
                    "Missing data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
            endTimeTF.requestFocus();
            return false;
        }
        int endTime = -1;
        try {
            endTime = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp,
                    "End Time must be a numeric value.", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
            endTimeTF.requestFocus();
            return false;
        }

        if (timeTFList != null) {
            int[] arrTime = new int[timeTFList.size() + 2];
            arrTime[0] = startTime;
            for (int i = 0; i < timeTFList.size(); i++) {
                String s = timeTFList.get(i).getText();
                if (s != null && s.trim().length() == 0) {
                    JOptionPane.showMessageDialog(comp, "The "
                            + horiLabList.get(i).getText()
                            + " time field is empty.", "Missing data entry",
                            JOptionPane.WARNING_MESSAGE);
                    timeTFList.get(i).requestFocus();
                    specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
                    return false;
                }
                try {
                    arrTime[i + 1] = Integer.parseInt(s);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(comp, "The "
                            + horiLabList.get(i).getText()
                            + " time field must be a numeric value.",
                            "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                    timeTFList.get(i).requestFocus();
                    specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
                    return false;
                }
            }
            arrTime[timeTFList.size() + 1] = endTime;

            int[] arrTimeBak = new int[arrTime.length];
            for (int i = 0; i < arrTime.length; i++) {
                arrTimeBak[i] = arrTime[i];
            }

            Arrays.sort(arrTimeBak);

            for (int i = 0; i < arrTimeBak.length; i++) {
                for (int j = 0; j < arrTimeBak.length; j++) {
                    if ((i != j) && (arrTimeBak[i] == arrTimeBak[j])) {
                        JOptionPane.showMessageDialog(comp,
                                "The time fields must be distinct values.",
                                "Invalid data entry",
                                JOptionPane.WARNING_MESSAGE);
                        specDecompTabbedPane
                                .setSelectedIndex(FLATTENING_TAB_INDEX);
                        return false;
                    }
                }
            }
            for (int i = 0; i < arrTime.length; i++) {
                if (arrTime[i] != arrTimeBak[i]) {
                    JOptionPane
                            .showMessageDialog(
                                    comp,
                                    "The time fields must be in ascending order (start to end).",
                                    "Invalid data entry",
                                    JOptionPane.WARNING_MESSAGE);
                    specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
                    return false;
                }
            }
        }
        return true;
    }
*/

    public boolean isBhpsuInput() {
        return bhpsuInRadioButton.isSelected();
    }
    
    public boolean isBhpsuOutput() {
        return bhpsuOutRadioButton.isSelected();
    }
    
    public boolean isBhpsuHorizonDataset() {
        return !isLandmarkSelected() && bhpsuHorizonDatasetRadioButton.isSelected();
    }
    
    public String getBhpsuOutputBaseName(){
    	return bhpsuNameBaseTextField.getText().trim();
    }
    
    public int getNumberOfBhpsuHorizons(){
    	int index = numHorizonsComboBox.getSelectedIndex();
    	if(index < 0)
    		index = 0;
    	return index;
    }

    /**
     *  Get a list of bhpsu horizon files (.xyz) with full path
     */
    public List<String> getBhpsuHorizons(){
    	String horizonDir = QiProjectDescUtils.getHorizonsPath(agent.getQiProjectDescriptor());
    	String filesep = agent.getMessagingMgr().getServerOSFileSeparator();
    	List<String> list = new ArrayList<String>();
    	for(javax.swing.JTextField tf : suHorzTextFieldList){
    		list.add(horizonDir + filesep + tf.getText().trim());
    	}
    	return list;
    }
    
    /**
     *  Get a list of bhpsu horizon datasets full path and its corresponding horizons 
     *  @param datasets  return a list of bhpsu horizon datasets
     *  @param horizons  return a list of bhpsu horizons
     */
    public void getBhpsuHorizons(List<String> datasets, List<String> horizons){
    	if(datasets == null)
    		datasets = new ArrayList<String>();
    	if(horizons == null)
    		horizons = new ArrayList<String>();
    	String datasetDir = QiProjectDescUtils.getDatasetsPath(agent.getQiProjectDescriptor());
    	String filesep = agent.getMessagingMgr().getServerOSFileSeparator();
    	for(javax.swing.JTextField tf : suHorzTextFieldList){
    		String s = tf.getText().trim();
            int index = s.indexOf(":");
            if(index >= 0){
            	String prior = s.substring(0, index);
            	String post = s.substring(index+1,s.length());
            	datasets.add(datasetDir + filesep + prior + ".dat");
            	horizons.add(post);
            }
    	}
    }    
    
    /**
     *  Get the bhpsu input dataset full path
     */
    public String getBhpsuInputDataset(){
    	String datasetDir = QiProjectDescUtils.getDatasetsPath(agent.getQiProjectDescriptor());
    	String filesep = agent.getMessagingMgr().getServerOSFileSeparator();
    	return datasetDir + filesep + datasetTextField.getText().trim();
    }

    /**
     * Check if the normalized output is requested.
     * @return boolean
     */
    public boolean isOutputNormalized(){
        if(bhpsuOutRadioButton.isSelected()){
            return bhpsuScaleNormalizedRadioButton.isSelected();
        }else if(landmarkOutRadioButton.isSelected()){
            if(landmarkInRadioButton.isSelected()){
                if(landmark3DRadioButton.isSelected()){
                    return landmark3DScaleNormalizedRadioButton.isSelected();
                }else if(landmark2DRadioButton.isSelected()){
                    return landmark2DScaleNormalizedRadioButton.isSelected();
                }
            }
        }
        return false;
    }

    /**
     *  Get the bhpsu input dataset without full path; only dataset base name
     */
    public String getBhpsuInputDatasetName(){
        if(landmarkInRadioButton.isSelected()){
            if(landmark3DRadioButton.isSelected()){
                String name = volumeTextField.getText().trim();
                int ind = name.lastIndexOf(".");
                if(ind != -1)
                    name = name.substring(0, ind);
                return name;
            }else if(landmark2DRadioButton.isSelected()){
                String name = getOutputBaseName();
                Object[] objs = selectedSeismic2DLineList.getSelectedValues();
                if(objs != null && objs.length > 0){
                    name += "_" + (String)objs[0];
                }
                return name;
            }
        }else if(bhpsuInRadioButton.isSelected()){
            String name = datasetTextField.getText().trim();
            int ind = name.lastIndexOf(".");
            if(ind != -1){
                name = name.substring(0, ind);
            }
            return name;
        }
        return "";
    }
    
    private void restoreBhpsu(Node node){
        Element el = (Element)node;

        String inputDataset = el.getAttribute("inputDataset");
        if(inputDataset == null){
            inputDataset = "";
            setEntireDatasetButtonEnabled(false);
        }else if(inputDataset.trim().length() == 0)
            setEntireDatasetButtonEnabled(false);
        else{
            datasetTextField.setText(inputDataset);

            String mincdp = el.getAttribute("mincdp");
            if(mincdp == null)
                mincdp = "";
            cdpTextField.setText(mincdp);

            String maxcdp = el.getAttribute("maxcdp");
            if(maxcdp == null)
                maxcdp = "";
            cdpToTextField.setText(maxcdp);

            String inccdp = el.getAttribute("inccdp");
            if(inccdp == null)
                inccdp = "";
            cdpByTextField.setText(inccdp);

            String minep = el.getAttribute("minep");
            if(minep == null)
                minep = "";
            epTextField.setText(minep);

            String maxep = el.getAttribute("maxep");
            if(maxep == null)
                maxep = "";
            epToTextField.setText(maxep);

            String incep = el.getAttribute("incep");
            if(incep == null)
                incep = "";
            epByTextField.setText(incep);

            String default_mincdp = el.getAttribute("default_mincdp");

            if(default_mincdp == null || default_mincdp.length() == 0)
                default_mincdp = "-1";
            defaultcdpMin = Integer.valueOf(default_mincdp).intValue();

            String default_maxcdp = el.getAttribute("default_maxcdp");
            if(default_maxcdp == null || default_maxcdp.length() == 0)
                default_maxcdp = "-1";
            defaultcdpMax = Integer.valueOf(default_maxcdp).intValue();

            String default_inccdp = el.getAttribute("default_inccdp");
            if(default_inccdp == null || default_inccdp.length() == 0)
                default_inccdp = "-1";
            defaultcdpBy = Integer.valueOf(default_inccdp).intValue();
            Map<String,DataSetHeaderKeyInfo> map = new HashMap<String,DataSetHeaderKeyInfo>();
            DataSetHeaderKeyInfo dataSetHeaderKeyInfo = new DataSetHeaderKeyInfo("cdp",defaultcdpMin,defaultcdpMax,defaultcdpBy);
            map.put("cdp",dataSetHeaderKeyInfo);
            String default_minep = el.getAttribute("default_minep");
            if(default_minep == null || default_minep.length() == 0)
                default_minep = "-1";
            defaultepMin = Integer.valueOf(default_minep).intValue();

            String default_maxep = el.getAttribute("default_maxep");
            if(default_maxep == null || default_maxep.length() == 0)
                default_maxep = "-1";
            defaultepMax = Integer.valueOf(default_maxep).intValue();

            String default_incep = el.getAttribute("default_incep");
            if(default_incep == null || default_incep.length() == 0)
                default_incep = "-1";
            defaultepBy = Integer.valueOf(default_incep).intValue();
            dataSetHeaderKeyInfo = new DataSetHeaderKeyInfo("ep",defaultepMin,defaultepMax,defaultepBy);
            map.put("ep",dataSetHeaderKeyInfo);
            agent.setSeismicDataset(map);
            if(defaultepBy == -1)
                setEntireDatasetButtonEnabled(false);
            else
                setEntireDatasetButtonEnabled(true);
        }

        String horizon_files = el.getAttribute("horizon_files");
        if(horizon_files != null && horizon_files.trim().length() > 0){
            String [] files = horizon_files.split(",");
            int num = files.length;
            if(num > 0){
                //formSUHorizonsPanel(num);
                numHorizonsComboBox.setSelectedIndex(num);
                bhpsuHorizonCount = num;
                if(suHorzTextFieldList == null)
                    suHorzTextFieldList = new ArrayList<JTextField>();
                for(int i = 0; i < files.length; i++){
                    suHorzTextFieldList.get(i).setText(files[i]);
                }
            }
        }

        String tmin = el.getAttribute("tmin");
        if(tmin == null)
            tmin = "";
        suStartTimeTextField.setText(tmin);

        String outputNameBase = el.getAttribute("outputNameBase");
        if(outputNameBase == null)
            outputNameBase = "";
        bhpsuNameBaseTextField.setText(outputNameBase);

        String run_normalization = el.getAttribute("run_normalization");
        if(run_normalization == null)
            run_normalization = "yes";
        if(run_normalization.equals("yes")){
            bhpsuScaleNormalizedRadioButton.setSelected(true);
            bhpsuScaleRawRadioButton.setSelected(false);
            String gamma = el.getAttribute("gamma");
            if(gamma == null)
                gamma = String.valueOf(defaultGamma);
            bhpsuNoiseFactorTextField.setText(gamma);
        }else{
            bhpsuScaleNormalizedRadioButton.setSelected(false);
            bhpsuScaleRawRadioButton.setSelected(true);
            bhpsuNoiseFactorTextField.setEnabled(false);
        }

        String horizon_times = el.getAttribute("horizon_times");
        if(horizon_times != null && horizon_times.trim().length() > 0){
            String [] times = horizon_times.split(",");
            int num = times.length;
            if(num > 0){
                if(suHorzTimeTextFieldList == null)
                    suHorzTimeTextFieldList = new ArrayList<JTextField>();
                for(int i = 0; i < times.length; i++)
                    suHorzTimeTextFieldList.get(i).setText(times[i]);
            }
        }
        String tmax = el.getAttribute("tmax");
        if(tmax == null)
            tmax = "";
        suEndTimeTextField.setText(tmax);
        String dt = el.getAttribute("dt");
        if(dt == null)
            dt = "4";
        timeIncrementTextField.setText(dt);

        String landmark_outputNameBase = el.getAttribute("landmark_outputNameBase");
        landmark_outputNameBase = (landmark_outputNameBase == null) ? "" : landmark_outputNameBase.trim();
        landmarkNameBaseTextField.setText(landmark_outputNameBase);

        String landmark_outputProject = el.getAttribute("landmark_outputProject");
        landmark_outputProject = (landmark_outputProject == null) ? "" : landmark_outputProject.trim();
        landmarkOutProjectTextField.setText(landmark_outputProject);

        String landmark_run_normalization = el.getAttribute("landmark_run_normalization");
        if(landmark_run_normalization == null)
            landmark_run_normalization = "yes";
        if(landmark_run_normalization.equals("yes")){
            landmarkScaleNormalizedRadioButton.setSelected(true);
            landmarkScaleRawRadioButton.setSelected(false);
            String landmark_gamma = el.getAttribute("landmark_gamma");
            if(landmark_gamma == null)
                landmark_gamma = String.valueOf(defaultGamma);
            landmarkNoiseFactorTextField.setText(landmark_gamma);
        }else{
            landmarkScaleNormalizedRadioButton.setSelected(false);
            landmarkScaleRawRadioButton.setSelected(true);
            landmarkNoiseFactorTextField.setEnabled(false);
        }

        String [] scales = null;
        int numScales = 0;
        String scaleText = el.getAttribute("landmark_scales");
        if(scaleText != null && scaleText.trim().length() > 0){
            scales = scaleText.split(",");
            numScales = scales.length;


            landmarkOutNumScalesCount = numScales;
            landmarkOutputScaleTextFieldList = new ArrayList<JTextField>();
            landmarkOutputScaleLabelList = new ArrayList<JLabel>();
            if(numScales > 0){
                for(int i = 0; i < numScales; i++){
                    landmarkOutputScaleLabelList.add(new JLabel("Scale " + (i + 1)));
                    landmarkOutputScaleTextFieldList.add(new JTextField(scales[i]));
                }
            }
            formLandmarkNumScaleOutputPanel(numScales,landmarkOutputScaleTextFieldList,landmarkOutputScaleLabelList);
        }

    }

    private String landmark3dDataType = "time";
    private void restoreLandmark3D(Node node){
        Element el = (Element)node;
        landmarkInProjectTextField.setText(el.getAttribute("landmark3DProject"));
        String landmarkInputVolume = el.getAttribute("landmark3DInputVolume");
        if(landmarkInputVolume == null || landmarkInputVolume.trim().length() == 0){
            landmarkInputVolume = "";
            setLineTraceFieldEnabled(false);
        }else{
            setLineTraceFieldEnabled(true);
            volumeTextField.setText(landmarkInputVolume);
        }
        String minlines = el.getAttribute("min_inline");
        if(minlines == null)
            minlines = "";
        linesTextField.setText(minlines);
        String maxlines = el.getAttribute("max_inline");
        if(maxlines == null)
            maxlines = "";
        linesToTextField.setText(maxlines);
        String inclines = el.getAttribute("inc_inline");
        if(inclines == null)
            inclines = "";
        linesByTextField.setText(inclines);

        String mintraces = el.getAttribute("min_trace");
        if(mintraces == null)
            mintraces = "";
        tracesTextField.setText(mintraces);
        String maxtraces = el.getAttribute("max_trace");
        if(maxtraces == null)
            maxtraces = "";
        tracesToTextField.setText(maxtraces);
        String inctraces = el.getAttribute("inc_trace");
        if(inctraces == null)
            inctraces = "";
        tracesByTextField.setText(inctraces);

        String mintime = el.getAttribute("min_time");
        if(mintime == null)
            mintime = "";
        timeTextField.setText(mintime);
        String maxtime = el.getAttribute("max_time");
        if(maxtime == null)
            maxtime = "";
        timeToTextField.setText(maxtime);

        String default_min_line = el.getAttribute("default_min_line");
        if(default_min_line == null)
            default_min_line = "";
        if(default_min_line.trim().length() > 0)
            lineTraceArray[0][0] = Integer.valueOf(default_min_line).intValue();
        String default_max_line = el.getAttribute("default_max_line");
        if(default_max_line == null)
            default_max_line = "";
        if(default_max_line.trim().length() > 0)
            lineTraceArray[0][1] = Integer.valueOf(default_max_line).intValue();
        String default_inc_line = el.getAttribute("default_inc_line");
        if(default_inc_line == null)
            default_inc_line = "";
        if(default_inc_line.trim().length() > 0)
            lineTraceArray[0][2] = Integer.valueOf(default_inc_line).intValue();

        String default_min_trace = el.getAttribute("default_min_trace");
        if(default_min_trace == null)
            default_min_trace = "";
        if(default_min_trace.trim().length() > 0)
            lineTraceArray[1][0] = Integer.valueOf(default_min_trace).intValue();
        String default_max_trace = el.getAttribute("default_max_trace");
        if(default_max_trace == null)
            default_max_trace = "";
        if(default_max_trace.trim().length() > 0)
            lineTraceArray[1][1] = Integer.valueOf(default_max_trace).intValue();
        String default_inc_trace = el.getAttribute("default_inc_trace");
        if(default_inc_trace == null)
            default_inc_trace = "";
        if(default_inc_trace.trim().length() > 0)
            lineTraceArray[1][2] = Integer.valueOf(default_inc_trace).intValue();

        String default_min_time = el.getAttribute("default_min_time");
        if(default_min_time == null)
            default_min_time = "";
        if(default_min_time.trim().length() > 0)
            lineTraceArray[2][0] = Integer.valueOf(default_min_time).intValue();
        String default_max_time = el.getAttribute("default_max_time");
        if(default_max_time == null)
            default_max_time = "";
        if(default_max_time.trim().length() > 0)
            lineTraceArray[2][1] = Integer.valueOf(default_max_time).intValue();
        String default_inc_time = el.getAttribute("default_inc_time");
        if(default_inc_time == null)
            default_inc_time = "";
        if(default_inc_time.trim().length() > 0)
            lineTraceArray[2][2] = Integer.valueOf(default_inc_time).intValue();

        String data_type = el.getAttribute("data_type");
        if(data_type != null){
            landmark3dDataType = data_type;
            if(landmark3dDataType.equals("time"))
                unitLabel.setText("(millisec)");
            else if(landmark3dDataType.equals("depth"))
                unitLabel.setText("(feet/meters)");
        }

        String landmark_horizon_files = el.getAttribute("landmark_horizon_files");
        if(landmark_horizon_files != null && landmark_horizon_files.trim().length() > 0){
            landmarkHorizonRadioButton.setSelected(true);
            String [] files = landmark_horizon_files.split(",");
            int num = files.length;

            numHorizonsComboBox.setSelectedIndex(num);
            //formLandmarkHorizonsPanel(num);
            //if(horizon_type.trim().equals(landmarkHorizonRadioButton.getText()))
            //  buildflatteningTab1(num);
            landmarkHorizonCount = num;
            if(num > 0){
                //horizonsComboBox.setSelectedIndex(num);
                for(int i = 0; i < num; i++)
                    landmarkHorzTextFieldList.get(i).setText(files[i]);
            }
        }
        String landmark_tmin = el.getAttribute("landmark_tmin");
        if(landmark_tmin == null)
            landmark_tmin = "";
        landmarkStartTimeTextField.setText(landmark_tmin);

        String landmark_horizon_times = el.getAttribute("landmark_horizon_times");
        if(landmark_horizon_times != null && landmark_horizon_times.trim().length() > 0){
            String [] times = landmark_horizon_times.split(",");
            int numTimes = times.length;
            if(numTimes > 0){
                for(int i = 0; i < numTimes; i++)
                    landmarkHorzTimeTextFieldList.get(i).setText(times[i]);
            }
        }

        String landmark_tmax = el.getAttribute("landmark_tmax");
        if(landmark_tmax == null)
            landmark_tmax = "";
        landmarkEndTimeTextField.setText(landmark_tmax);
        String sampleRate = el.getAttribute("sampleRate");
        if(sampleRate == null)
            sampleRate = "";
        sampleRateTextField.setText(sampleRate);

        String landmark3D_outputNameBase = el.getAttribute("landmark3D_outputNameBase");
        if(landmark3D_outputNameBase == null)
            landmark3D_outputNameBase = "";
        landmark3DNameBaseTextField.setText(landmark3D_outputNameBase);
        String landmark3D_out_project = el.getAttribute("landmark3D_out_project");
        if(landmark3D_out_project == null)
            landmark3D_out_project = "";
        landmark3DOutProjectTextField.setText(landmark3D_out_project);

        String landmark3D_run_normalization = el.getAttribute("landmark3D_run_normalization");
        if(landmark3D_run_normalization == null)
            landmark3D_run_normalization = "yes";
        if(landmark3D_run_normalization.equals("yes")){
            landmark3DScaleNormalizedRadioButton.setSelected(true);
            landmark3DScaleRawRadioButton.setSelected(false);
            String landmark3D_gamma = el.getAttribute("landmark3D_gamma");
            if(landmark3D_gamma == null)
                landmark3D_gamma = String.valueOf(defaultGamma);
            landmark3DNoiseFactorTextField.setText(landmark3D_gamma);
        }else{
            landmark3DScaleNormalizedRadioButton.setSelected(false);
            landmark3DScaleRawRadioButton.setSelected(true);
            landmark3DNoiseFactorTextField.setEnabled(false);
        }

        String outProject = el.getAttribute("landmark3D_outputProject");
        outProject = (outProject == null) ? "" : outProject.trim();
        landmark3DOutProjectTextField.setText(outProject);
        String [] scales = null;
        int numScales = 0;
        String scaleText = el.getAttribute("landmark3D_scales");
        if(scaleText != null && scaleText.trim().length() > 0){
            scales = scaleText.split(",");
            numScales = scales.length;


            landmark3DOutNumScalesCount = numScales;
            landmark3DOutputScaleTextFieldList = new ArrayList<JTextField>();
            landmark3DOutputScaleLabelList = new ArrayList<JLabel>();
            if(numScales > 0){
                for(int i = 0; i < numScales; i++){
                    landmark3DOutputScaleLabelList.add(new JLabel("Scale " + (i + 1)));
                    landmark3DOutputScaleTextFieldList.add(new JTextField(scales[i]));
                }
            }
            formLandmarkNumScaleOutputPanel(numScales,landmark3DOutputScaleTextFieldList,landmark3DOutputScaleLabelList);
        }

    }

    private void restoreLandmark2D(Node node){
        Element el = (Element)node;

        String project = el.getAttribute("landmark2DProject");
        project = (project == null) ? "" : project.trim();
        if(project.length() > 0)
            setLandmarkIn2DProjectTextField(project);
        
        String process_level = el.getAttribute("landmark2D_seismic_file_process_level");
        process_level = (process_level == null) ? "" : process_level.trim();

        javax.swing.ComboBoxModel model = seismic2DFileComboBox.getModel();
        int index = -1;
        for(int i = 0; i < model.getSize(); i++){
            if(model.getElementAt(i) != null && model.getElementAt(i).equals(process_level)){
                index = i;
                break;
            }
        }
        //seismic2DFileComboBox.setSelectedItem(process_level);
        //if(index > 0)
        //  index--;
        if(index != -1)
            seismic2DFileComboBox.setSelectedIndex(index);
        String lines = el.getAttribute("landmark2D_seismic_lines");
        lines = (lines == null) ? "" : lines.trim();
        String [] lineArrs = new String[0];
        String [] contents = null;
        String [] def_ranges = null;
        String [] modified_ranges = null;
        //lname,def_min_spn:def_max_spn:def_inc_spn,min_spn:max_spn:inc~lname,def_min_spn:def_max_spn:def_inc_spn,min_spn:max_spn:inc~...
        if(lines.length() > 0){
            lineArrs = lines.split("~");
            selectedLineMap = new HashMap<String,LineInfo>();
        }
        for(int i = 0; lineArrs != null && i < lineArrs.length; i++){
            contents = lineArrs[i].split(",");

            def_ranges = contents[1].split(":");
            int linenum = landmarkAgent.get2DLineNumberByName(project,contents[0]);
            LineInfo li = new LineInfo(project,contents[0],linenum,Float.valueOf(def_ranges[0]),
                    Float.valueOf(def_ranges[1]),Float.valueOf(def_ranges[2]));
            modified_ranges = contents[2].split(":");
            li.setMinShotPoint(Float.valueOf(modified_ranges[0]));
            li.setMaxShotPoint(Float.valueOf(modified_ranges[1]));
            li.setIncShotPoint(Float.valueOf(modified_ranges[2]));
            selectedLineMap.put(contents[0], li);
        }
        //String [] lineArr = lines.split(",");
        Vector<String> v = new Vector<String>();
        for(String s : selectedLineMap.keySet()){
            v.add(s);
        }
        Collections.sort(v);
        selectedSeismic2DLineList.setListData(v);
        selectedLines = v;
        String landmark2D_selected_line = el.getAttribute("landmark2D_selected_line");
        landmark2D_selected_line = (landmark2D_selected_line == null) ? "" : landmark2D_selected_line.trim();
        //int ind = 0;
        //for(int i = 0; i < selectedSeismic2DLineList.getModel().getSize(); i++){
        //  if(selectedSeismic2DLineList.getModel().getElementAt(i) != null && selectedSeismic2DLineList.getModel().getElementAt(i).equals(landmark2D_selected_line)){
        //      ind = i;
        //      break;
        //  }
        //}
        //selectedSeismic2DLineList.setSelectedIndex(ind);
        selectedLineName = landmark2D_selected_line;
        String landmark2D_outputNameBase = el.getAttribute("landmark2D_outputNameBase");
        landmark2D_outputNameBase = (landmark2D_outputNameBase == null) ? "" : landmark2D_outputNameBase.trim();
        landmark2DNameBaseTextField.setText(landmark2D_outputNameBase);

        String landmark2D_out_project = el.getAttribute("landmark2D_outputProject");
        if(landmark2D_out_project == null)
            landmark2D_out_project = "";
        landmark2DOutProjectTextField.setText(landmark2D_out_project);

        String landmark2D_run_normalization = el.getAttribute("landmark2D_run_normalization");
        if(landmark2D_run_normalization == null)
            landmark2D_run_normalization = "yes";
        if(landmark2D_run_normalization.equals("yes")){
            landmark2DScaleNormalizedRadioButton.setSelected(true);
            landmark2DScaleRawRadioButton.setSelected(false);
            String landmark2D_gamma = el.getAttribute("landmark2D_gamma");
            if(landmark2D_gamma == null)
                landmark2D_gamma = String.valueOf(defaultGamma);
            landmark2DNoiseFactorTextField.setText(landmark2D_gamma);
        }else{
            landmark2DScaleNormalizedRadioButton.setSelected(false);
            landmark2DScaleRawRadioButton.setSelected(true);
            landmark2DNoiseFactorTextField.setEnabled(false);
        }
        String [] scales = null;
        int numScales = 0;
        String scaleText = el.getAttribute("landmark2D_scales");
        if(scaleText != null && scaleText.trim().length() > 0){
            scales = scaleText.split(",");
            numScales = scales.length;

            formLandmarkNumScaleOutputPanel(numScales,landmark2DOutputScaleTextFieldList,landmark2DOutputScaleLabelList);
            landmark2DOutNumScalesCount = numScales;
            landmark2DOutputScaleTextFieldList = new ArrayList<JTextField>();
            landmark2DOutputScaleLabelList = new ArrayList<JLabel>();
            if(numScales > 0){
                for(int i = 0; i < numScales; i++){
                    landmark2DOutputScaleLabelList.add(new JLabel("Scale " + (i + 1)));
                    landmark2DOutputScaleTextFieldList.add(new JTextField(scales[i]));
                }
            }
        }

    }

    private void resetShotpointInfo(){
        selectedLineMap = new HashMap<String,LineInfo>();
        selectedLineName = "";
        shotpointTextField.setText("");
        shotpointToTextField.setText("");
        shotpointByTextField.setText("");
    }
    private String selectedLineName;
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code
    // ">//GEN-BEGIN:initComponents
    private void initComponents() {
        specDecompTabbedPane = new javax.swing.JTabbedPane();
        /*
         * specDecompTabbedPane.addFocusListener(new FocusAdapter(){ public void
         * focusLost(FocusEvent e){ validateFlatteningInfo(); } });
         */
        final Component comp = this;

        final WaveletDecompGUI parent = this;


        list2DProjectsButton = new javax.swing.JButton();
        list2DProjectsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //new ProjectSelectDialog((java.awt.Frame)SwingUtilities.getWindowAncestor(comp),true).setVisible(true);
                java.awt.Dialog dialog = new ProjectSelectDialog(landmarkAgent,parent,true,"landmark_in");
                dialog.setLocationRelativeTo(JOptionPane.getFrameForComponent(comp));
                dialog.setVisible(true);
            }
        });

        seismic2DFileComboBox = new javax.swing.JComboBox();
        seismic2DFileComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                javax.swing.JComboBox cb = (javax.swing.JComboBox) e.getSource();
                 int idx = cb.getSelectedIndex();
                if(idx ==  -1 || cb.getSelectedItem().equals("Select one")){
                    seismic2DLineList.setListData(new Vector());
                    selectedLines = new Vector();
                    selectedSeismic2DLineList.setListData(selectedLines);
                    resetShotpointInfo();
                    return;
                }
                if(idx == selectedProcessLevelIndex)
                    return;
                selectedProcessLevelIndex= idx;

                String item = (String)cb.getSelectedItem();
                List<String> list = landmark2DfileLinesMap.get(item);
                Vector<String> v;
                if(list == null){
                    v = new Vector<String>();

                }else
                    v = new Vector<String>(list);
                seismic2DLineList.setListData(v);
                selectedLines = new Vector();
                selectedSeismic2DLineList.setListData(selectedLines);
                resetShotpointInfo();
            }
        });
        lineListScrollPane = new javax.swing.JScrollPane();
        linesLabel = new javax.swing.JLabel();
        linesListButton = new javax.swing.JButton();
        seismicFileLabel = new javax.swing.JLabel();
        shotpointLabel = new javax.swing.JLabel();
        entireVolume2DButton = new javax.swing.JButton();
        entireVolume2DButton.addActionListener(new ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(selectedLineName != null && selectedLineName.trim().length() > 0){
                    LineInfo li = selectedLineMap.get(selectedLineName);
                    shotpointTextField.setText(String.valueOf(li.getMinShotPoint_def()));
                    shotpointToTextField.setText(String.valueOf(li.getMaxShotPoint_def()));
                    shotpointByTextField.setText(String.valueOf(li.getIncShotPoint_def()));
                    li.setMinShotPoint(li.getMinShotPoint_def());
                    li.setMaxShotPoint(li.getMaxShotPoint_def());
                    li.setIncShotPoint(li.getIncShotPoint_def());
                    selectedLineMap.put(selectedLineName, li);
                }
            }
        });
        select2DButton = new javax.swing.JButton();
        seismicFileListButton = new javax.swing.JButton();
        landmarkIn2DProjectTextField = new javax.swing.JTextField();
        linesInTextField = new javax.swing.JTextField();
        seismicFileTextField = new javax.swing.JTextField();
        shotpointByTextField = new javax.swing.JTextField();
        shotpointByTextField.addFocusListener(new FocusAdapter() {
              public void focusLost(FocusEvent e) {
                  float num;
                  try{
                      num = Float.valueOf(shotpointByTextField.getText());
                  }catch(Exception ex){
                      JOptionPane.showMessageDialog(comp, "The shotpoint increment "
                              + shotpointByTextField.getText() + " is not a valid number.",
                              "Number Format Exception", JOptionPane.WARNING_MESSAGE);
                      shotpointByTextField.requestFocus();
                      return;
                  }
                  //String s = (String)selectedSeismic2DLineList.getSelectedValue();
                  if(selectedLineName != null && selectedLineName.trim().length() > 0){
                      LineInfo li = selectedLineMap.get(selectedLineName);
                      li.setIncShotPoint(num);
                      selectedLineMap.put(selectedLineName, li);
                  }
              }
        });
        shotpointTextField = new javax.swing.JTextField();
        shotpointTextField.addFocusListener(new FocusAdapter() {
              public void focusLost(FocusEvent e) {
                  float num;
                  try{
                      num = Float.valueOf(shotpointTextField.getText());
                  }catch(Exception ex){
                      JOptionPane.showMessageDialog(comp, "The minimum shotpoint "
                              + shotpointTextField.getText() + " is not a valid number.",
                              "Number Format Exception", JOptionPane.WARNING_MESSAGE);
                      shotpointTextField.requestFocus();
                      return;
                  }
                  //String s = (String)selectedSeismic2DLineList.getSelectedValue();
                  logger.info("selected line=" + selectedLineName);
                  if(selectedLineName != null && selectedLineName.trim().length() > 0){
                      LineInfo li = selectedLineMap.get(selectedLineName);
                      li.setMinShotPoint(num);
                      selectedLineMap.put(selectedLineName, li);
                  }
              }
        });
        shotpointToTextField = new javax.swing.JTextField();
        shotpointToTextField.addFocusListener(new FocusAdapter() {
              public void focusLost(FocusEvent e) {
                  float num;
                  try{
                      num = Float.valueOf(shotpointToTextField.getText());
                  }catch(Exception ex){
                      JOptionPane.showMessageDialog(comp, "The maxmum shotpoint "
                              + shotpointToTextField.getText() + " is not a valid number.",
                              "Number Format Exception", JOptionPane.WARNING_MESSAGE);
                      shotpointToTextField.requestFocus();
                      return;
                  }
                  //String s = (String)selectedSeismic2DLineList.getSelectedValue();
                  if(selectedLineName != null && selectedLineName.trim().length() > 0){
                      LineInfo li = selectedLineMap.get(selectedLineName);
                      li.setMaxShotPoint(num);
                      selectedLineMap.put(selectedLineName, li);
                  }
              }
        });
        selectedLineListScrollPane = new javax.swing.JScrollPane();
        selectedSeismic2DLineList = new javax.swing.JList();
        seismic2DLineList = new javax.swing.JList();
        MouseListener mouseListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    if(e.getSource().equals(selectedSeismic2DLineList))
                        leftArrowButton.doClick();
                    else if(e.getSource().equals(seismic2DLineList))
                        rightArrowButton.doClick();
                }
                if (e.getClickCount() == 1) {
                    if(e.getSource().equals(selectedSeismic2DLineList)){
                        //float range[] = landmarkAgent.getLandmarkProjectHandler().getShotpointRangeInfoBy2DLine
                        //(landmarkIn2DProjectTextField.getText().trim(), (String)selectedSeismic2DLineList.getSelectedValue());
                        String s = (String)selectedSeismic2DLineList.getSelectedValue();
                        if(s != null){
                            selectedLineName = s;
                            LineInfo li = selectedLineMap.get((String)selectedSeismic2DLineList.getSelectedValue());
                            if(li != null){
                                shotpointTextField.setText(String.valueOf(li.getMinShotPoint()));
                                shotpointToTextField.setText(String.valueOf(li.getMaxShotPoint()));
                                shotpointByTextField.setText(String.valueOf(li.getIncShotPoint()));
                            }
                        }
                    }
                }
            }
        };
        selectedSeismic2DLineList.addMouseListener(mouseListener);
        seismic2DLineList.addMouseListener(mouseListener);
        selectedLinesLabel = new javax.swing.JLabel();
        rightArrowButton = new javax.swing.JButton();
        rightArrowButton.addActionListener(new ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Object[] objs = seismic2DLineList.getSelectedValues();
                if(objs != null){
                    Vector v = new Vector();
                    ListModel model = selectedSeismic2DLineList.getModel();
                    for(int i = 0; model != null && i < model.getSize(); i++){
                        v.add(model.getElementAt(i));
                    }

                    for(int i = 0; i < objs.length; i++){
                        if(!v.contains(objs[i])){
                            v.add(objs[i]);
                            float range[] = landmarkAgent.getLandmarkProjectHandler().getShotpointRangeInfoBy2DLine
                            (landmarkIn2DProjectTextField.getText().trim(), (String)objs[i]);
                            LineInfo li = new LineInfo(landmarkIn2DProjectTextField.getText().trim(),(String)(objs[i]),(int)range[0], range[1], range[2],range[3]);
                            li.setMinShotPoint(range[1]);
                            li.setMaxShotPoint(range[2]);
                            li.setIncShotPoint(range[3]);
                            selectedLineMap.put((String)objs[i],li);
                        }
                    }
                    Collections.sort(v);
                    selectedSeismic2DLineList.setListData(v);
                    selectedLines = v;
                }
            }
        });
        leftArrowButton = new javax.swing.JButton();
        leftArrowButton.addActionListener(new ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Object[] objs = selectedSeismic2DLineList.getSelectedValues();
                if(objs != null){
                    Vector v = new Vector();
                    ListModel model = selectedSeismic2DLineList.getModel();
                    for(int i = 0; model != null && i < model.getSize(); i++){
                        v.add(model.getElementAt(i));
                    }
                    for(int i = 0; i < objs.length; i++){
                        v.remove(objs[i]);
                        selectedLineMap.remove((String)objs[i]);
                    }
                    selectedSeismic2DLineList.setListData(v);
                }
            }
        });



        landmarkProjectLabel = new javax.swing.JLabel();
        volumeLabel = new javax.swing.JLabel();
        landmarkRangeLabel = new javax.swing.JLabel();
        rangeLinesLabel = new javax.swing.JLabel();
        linesToLabel = new javax.swing.JLabel();
        timeToLabel = new javax.swing.JLabel();
        unitLabel = new javax.swing.JLabel();
        linesByLabel = new javax.swing.JLabel();
        tracesLabel = new javax.swing.JLabel();
        timeLabel = new javax.swing.JLabel();
        tracesToLabel = new javax.swing.JLabel();
        tracesByLabel = new javax.swing.JLabel();
        volumeTextField = new javax.swing.JTextField();
        landmarkInProjectTextField = new javax.swing.JTextField();

        //entireVolumeButton = new javax.swing.JButton();
        selectButton = new javax.swing.JButton();

        landmarkProjectPanel = new javax.swing.JPanel();
        landmarkOutProjectLabel = new javax.swing.JLabel();
        landmarkOutProjectTextField = new javax.swing.JTextField();
        landmark2DOutProjectTextField = new javax.swing.JTextField();
        landmark3DOutProjectTextField = new javax.swing.JTextField();
        landmarkOutListButton = new javax.swing.JButton();
        landmarkProjectPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Landmark Project"));
        landmarkOutProjectLabel.setText("  Landmark Project:");
        landmarkOutListButton.setText("List...");
        landmarkOutListButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //new ProjectSelectDialog((java.awt.Frame)SwingUtilities.getWindowAncestor(comp),true).setVisible(true);
                if(landmarkAgent == null)
                    landmarkAgent = LandmarkServices.getInstance();
                java.awt.Dialog dialog = new ProjectSelectDialog(landmarkAgent,parent,true,"landmark_out");
                dialog.setLocationRelativeTo(JOptionPane.getFrameForComponent(comp));
                dialog.setVisible(true);
            }
        });
        bhpsuInputDataPanel = new javax.swing.JPanel();
        landmarkInputDataPanel = new javax.swing.JPanel();
        landmarkInRadioButton = new javax.swing.JRadioButton();
        landmarkHorizonRadioButton = new javax.swing.JRadioButton();
        landmarkOutRadioButton = new javax.swing.JRadioButton();
        bhpsuInRadioButton = new javax.swing.JRadioButton();
        bhpsuHorizonRadioButton = new javax.swing.JRadioButton();
        bhpsuHorizonDatasetRadioButton = new javax.swing.JRadioButton();
        bhpsuOutRadioButton = new javax.swing.JRadioButton();
        landmarkInPanel = new javax.swing.JPanel();
        bhpsuProjectLabel = new javax.swing.JLabel();

        landmarkInProjectTextField = new javax.swing.JTextField();
        landmarkInProjectTextField.addKeyListener(new KeyAdapter(){
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER){
                    String prj = landmarkInProjectTextField.getText();
                    if(prj == null || prj.trim().length() == 0)
                        return;
                    listVolumesButton.doClick();
                }
            }
        });
        listProjectsButton = new javax.swing.JButton();
        listProjectsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //new ProjectSelectDialog((java.awt.Frame)SwingUtilities.getWindowAncestor(comp),true).setVisible(true);
                java.awt.Dialog dialog = new ProjectSelectDialog(landmarkAgent,parent,true,"landmark_in");
                dialog.setLocationRelativeTo(JOptionPane.getFrameForComponent(comp));
                dialog.setVisible(true);
            }
        });

        volumeTextField = new javax.swing.JTextField();
        volumeTextField.addKeyListener(new KeyAdapter(){
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER){
                    String prj = landmarkInProjectTextField.getText();
                    if(prj == null || prj.trim().length() == 0)
                        return;
                    int type = landmarkAgent.getLandmarkProjectType(prj);
                    if(type < 2 || type > 3)
                        return;

                    String vol = volumeTextField.getText();
                    if(vol == null || vol.trim().length() == 0)
                        return;
                    int ltArray [][] = landmarkAgent.getLandmarkLineTraceInfo(prj, type, vol);
                    setLineTraceFieldEnabled(true);
                    setLineTraceDefaultValue(ltArray);

                }
            }
        });
        listVolumesButton = new javax.swing.JButton();
        listVolumesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //new ProjectSelectDialog((java.awt.Frame)SwingUtilities.getWindowAncestor(comp),true).setVisible(true);
                if(landmarkInProjectTextField.getText() == null || landmarkInProjectTextField.getText().trim().length() == 0){
                    JOptionPane.showMessageDialog(comp, "Please choose a SeisWorks project before retrieving volume information.",
                            "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                    listProjectsButton.requestFocus();
                    return;
                }

                //String [] vlumes = agent.getLandmarkVolumeList(landmarkInProjectTextField.getText(), landmarkProjectType, "*.bri");
                java.awt.Dialog dialog = new VolumeSelectDialog(landmarkAgent,parent,true);
                dialog.setLocationRelativeTo(JOptionPane.getFrameForComponent(comp));
                dialog.setVisible(true);
            }
        });
        bhpsuRangeLabel = new javax.swing.JLabel();


        linesTextField = new javax.swing.JTextField();
        cdpTextField = new javax.swing.JTextField();

        //cdpTextField.addFocusListener(new FocusAdapter() {
        //  public void focusLost(FocusEvent e) {
        //      if (validateCdpTextField() == false)
        //          cdpTextField.requestFocus();
        //  }
        //});

        cdpToLabel = new javax.swing.JLabel();
        linesToTextField = new javax.swing.JTextField();
        cdpToTextField = new javax.swing.JTextField();
        //cdpToTextField.addFocusListener(new FocusAdapter() {
        //  public void focusLost(FocusEvent e) {
        //      if (validateCdpToTextField() == false)
        //          cdpToTextField.requestFocus();
        //  }
        //});

        cdpByLabel = new javax.swing.JLabel();
        linesByTextField = new javax.swing.JTextField();
        cdpByTextField = new javax.swing.JTextField();


        //cdpByTextField.addFocusListener(new FocusAdapter() {
        //  public void focusLost(FocusEvent e) {
        //      if (validateCdpByTextField() == false)
        //          cdpByTextField.requestFocus();
        //  }
        //});

        epLabel = new javax.swing.JLabel();
        tracesTextField = new javax.swing.JTextField();
        timeTextField = new javax.swing.JTextField();
        timeToTextField = new javax.swing.JTextField();
        epTextField = new javax.swing.JTextField();
        //epTextField.addFocusListener(new FocusAdapter() {
        //  public void focusLost(FocusEvent e) {
        //      if (validateEpTextField() == false)
        //          epTextField.requestFocus();
        //  }
        //});

        epToLabel = new javax.swing.JLabel();
        tracesToTextField = new javax.swing.JTextField();
        epToTextField = new javax.swing.JTextField();
        //epToTextField.addFocusListener(new FocusAdapter() {
        //  public void focusLost(FocusEvent e) {
        //      if (validateEpToTextField() == false)
        //          epToTextField.requestFocus();
        //  }
        //});

        epByLabel = new javax.swing.JLabel();
        tracesByTextField = new javax.swing.JTextField();

        epByTextField = new javax.swing.JTextField();
        //epByTextField.addFocusListener(new FocusAdapter() {
        //  public void focusLost(FocusEvent e) {
        //      if (validateEpByTextField() == false)
        //          epByTextField.requestFocus();
        //  }
        //});
        entireVolumeButton = new javax.swing.JButton();
        entireVolumeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(lineTraceArray == null)
                    return;
                setLineTraceDefaultValue(lineTraceArray);
            }
        });
        selectButton = new javax.swing.JButton();
        parametersPanel = new javax.swing.JPanel();
        landmarkNumScaleLabel = new javax.swing.JLabel("Number of Scales");

        //smoothingLengthTextField.addFocusListener(new FocusAdapter() {
        //  public void focusLost(FocusEvent e) {
        //      if (validateSmoothingLengthTextField() == false)
        //          smoothingLengthTextField.requestFocus();
        //  }
        //});
        flatteningPanel = new javax.swing.JPanel();
        horizonsComboBox = new javax.swing.JComboBox();
        numHorizonsComboBox = new javax.swing.JComboBox();
        horizonsPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        /*landmarkHorzLabelList = new ArrayList<JLabel>();
        landmarkHorzTextFieldList = new ArrayList<JTextField>();
        landmarkHorzTimeTextFieldList = new ArrayList<JTextField>();
        landmarkChooseHorzButtonList = new ArrayList<JButton>();
        */
        suStartTimeTextField = new javax.swing.JTextField();
        landmarkStartTimeTextField = new javax.swing.JTextField();
        /*
         startTimeTextField.addFocusListener(new FocusAdapter(){
             public void  focusLost(FocusEvent e){
                 String str = startTimeTextField.getText();
                 int ival = -1;
                 try{
                    ival = Integer.parseInt(str);
                 }catch(NumberFormatException ex){
                     JOptionPane.showMessageDialog(comp,"Must be a numeric value.",
                     "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                     startTimeTextField.setText("");
                     startTimeTextField.requestFocus();
                     return;
                 }
                 str = endTimeTextField.getText();
                 if(str != null && str.length() > 0) {
                     if(ival > Integer.valueOf(str).intValue()){
                        JOptionPane.showMessageDialog(comp,"Must be less than or equals to
                            the End Time.", "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                        startTimeTextField.setText(""); startTimeTextField.requestFocus();
                        return;
                     }
                 }
             }
         });
         */
        jLabel2 = new javax.swing.JLabel();
        suEndTimeTextField = new javax.swing.JTextField();
        landmarkEndTimeTextField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        timeIncrementTextField = new javax.swing.JTextField();
        timeIncrementTextField.setText("4");
        sampleRateTextField = new javax.swing.JTextField();
        sampleRateTextField.setEditable(false);
        /*
         endTimeTextField.addFocusListener(new FocusAdapter(){
             public void    focusLost(FocusEvent e){
                 String str = endTimeTextField.getText();
                 int ival = -1;
                 try{
                    ival = Integer.parseInt(str);
                 }catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(comp,"Must be a numeric value.",
                    "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                    endTimeTextField.requestFocus();
                    return;
                 }
                 str = startTimeTextField.getText();
                 if(str != null && str.length() > 0){
                     if(ival < Integer.valueOf(str).intValue()){
                         JOptionPane.showMessageDialog(comp,"Must not be less than or equals
                         to the Start Time.", "Invalid data
                         entry",JOptionPane.WARNING_MESSAGE);
                         endTimeTextField.requestFocus();
                         return;
                     }
                 }
             }
         });
         */
        horizonsLabel = new javax.swing.JLabel();
        listRadioButton = new javax.swing.JRadioButton();
        viewerRadioButton = new javax.swing.JRadioButton();
        bhpsuOutputsPanel = new javax.swing.JPanel();
        landmarkOutputsPanel = new javax.swing.JPanel();
        // landmarkRadioButton = new javax.swing.JRadioButton();
        landmarkOutPanel = new javax.swing.JPanel();
        bhpsuNameBaseLabel = new javax.swing.JLabel();
        bhpsuNameBaseLabel.setText("Output Name Base:");
        landmarkNameBaseLabel = new javax.swing.JLabel();
        bhpsuNameBaseTextField = new javax.swing.JTextField();

        landmarkNameBaseTextField = new javax.swing.JTextField();
        landmark2DNameBaseTextField = new javax.swing.JTextField();
        landmark3DNameBaseTextField = new javax.swing.JTextField();
        bhpsuScaleVolumesLabel = new javax.swing.JLabel();
        landmarkScaleVolumesLabel = new javax.swing.JLabel();
        landmarkScaleVolumesLabel.setText("Scale Volumes");

        bhpsuScaleRawRadioButton = new JRadioButton();
        bhpsuScaleRawRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(bhpsuScaleRawRadioButton.isSelected())
                    bhpsuNoiseFactorTextField.setEnabled(false);
                else
                    bhpsuNoiseFactorTextField.setEnabled(true);
            }
        });
        // bhpsuScaleRawRadioButton.setSelected(true);

        bhpsuNoiseFactorTextField = new javax.swing.JTextField();
        bhpsuNoiseFactorTextField.setText(String.valueOf(defaultGamma));
        //bhpsuNoiseFactorTextField.setEnabled(false);
        landmarkNoiseFactorTextField = new javax.swing.JTextField();
        landmark2DNoiseFactorTextField = new javax.swing.JTextField();
        landmark3DNoiseFactorTextField = new javax.swing.JTextField();
        landmarkNoiseFactorTextField.setText(String.valueOf(defaultGamma));
        landmark2DNoiseFactorTextField.setText(String.valueOf(defaultGamma));
        landmark3DNoiseFactorTextField.setText(String.valueOf(defaultGamma));
        //landmarkNoiseFactorTextField.setEnabled(false);
        bhpsuNoiseFactorLabel = new javax.swing.JLabel();
        landmarkNoiseFactorLabel = new javax.swing.JLabel();
        landmarkNoiseFactorLabel.setText("(Noise Factor, db)");
        bhpsuScaleNormalizedRadioButton = new JRadioButton();
        bhpsuScaleNormalizedRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(bhpsuScaleNormalizedRadioButton.isSelected())
                    bhpsuNoiseFactorTextField.setEnabled(true);
                else
                    bhpsuNoiseFactorTextField.setEnabled(false);
            }
        });
        bhpsuScaleNormalizedRadioButton.setSelected(true);

        landmarkScaleRawRadioButton = new javax.swing.JRadioButton();
        landmarkScaleRawRadioButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(landmarkScaleRawRadioButton.isSelected()){
                    landmarkNoiseFactorTextField.setEnabled(false);
                }else{
                    landmarkNoiseFactorTextField.setEnabled(true);
                }
            }
        });

        landmark2DScaleRawRadioButton = new javax.swing.JRadioButton();
        landmark2DScaleRawRadioButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(landmark2DScaleRawRadioButton.isSelected()){
                    landmark2DNoiseFactorTextField.setEnabled(false);
                }else{
                    landmark2DNoiseFactorTextField.setEnabled(true);
                }
            }
        });

        landmark3DScaleRawRadioButton = new javax.swing.JRadioButton();
        landmark3DScaleRawRadioButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(landmark3DScaleRawRadioButton.isSelected()){
                    landmark3DNoiseFactorTextField.setEnabled(false);
                }else{
                    landmark3DNoiseFactorTextField.setEnabled(true);
                }
            }
        });


        landmarkScaleNormalizedRadioButton = new javax.swing.JRadioButton();
        landmarkScaleNormalizedRadioButton.setSelected(true);
        landmarkScaleNormalizedRadioButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(landmarkScaleNormalizedRadioButton.isSelected()){
                    landmarkNoiseFactorTextField.setEnabled(true);
                }else{
                    landmarkNoiseFactorTextField.setEnabled(false);
                }
            }
        });

        landmark2DScaleNormalizedRadioButton = new javax.swing.JRadioButton();
        landmark2DScaleNormalizedRadioButton.setSelected(true);
        landmark2DScaleNormalizedRadioButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(landmark2DScaleNormalizedRadioButton.isSelected()){
                    landmark2DNoiseFactorTextField.setEnabled(true);
                }else{
                    landmark2DNoiseFactorTextField.setEnabled(false);
                }
            }
        });

        landmark3DScaleNormalizedRadioButton = new javax.swing.JRadioButton();
        landmark3DScaleNormalizedRadioButton.setSelected(true);
        landmark3DScaleNormalizedRadioButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(landmark3DScaleNormalizedRadioButton.isSelected()){
                    landmark3DNoiseFactorTextField.setEnabled(true);
                }else{
                    landmark3DNoiseFactorTextField.setEnabled(false);
                }
            }
        });

        ButtonGroup outputLandmarkScale = new ButtonGroup();
        outputLandmarkScale.add(landmarkScaleRawRadioButton);
        outputLandmarkScale.add(landmarkScaleNormalizedRadioButton);

        ButtonGroup outputLandmark2DScale = new ButtonGroup();
        outputLandmark2DScale.add(landmark2DScaleRawRadioButton);
        outputLandmark2DScale.add(landmark2DScaleNormalizedRadioButton);

        ButtonGroup outputLandmark3DScale = new ButtonGroup();
        outputLandmark3DScale.add(landmark3DScaleRawRadioButton);
        outputLandmark3DScale.add(landmark3DScaleNormalizedRadioButton);

        flattenedPanel = new javax.swing.JPanel();
        horizonVolumesLabel = new javax.swing.JLabel();
        horizonVolumesLabel.setText("Horizon Volumes");
        placementLabel = new javax.swing.JLabel();

        horizonRawRadioButton = new javax.swing.JRadioButton();
        horizonNormalizedRadioButton = new javax.swing.JRadioButton();
        horizonNormalizedRadioButton.setSelected(true);
        ButtonGroup horizonRadioGroup = new ButtonGroup();
        horizonRadioGroup.add(horizonRawRadioButton);
        horizonRadioGroup.add(horizonNormalizedRadioButton);
        timeComboBox = new javax.swing.JComboBox();
        landmarkNumOfScalesComboBox = new javax.swing.JComboBox();
        landmark2DNumOfScalesComboBox = new javax.swing.JComboBox();
        landmark3DNumOfScalesComboBox = new javax.swing.JComboBox();
        timePanel = new javax.swing.JPanel();
        scalePanel = new javax.swing.JPanel();
        statusPanel = new javax.swing.JPanel();
        statusLabel = new javax.swing.JLabel();
        stateLabel = new javax.swing.JLabel();
        updateButton = new javax.swing.JButton();
        detailsPanel = new javax.swing.JPanel();
        stdOutScrollPane = new javax.swing.JScrollPane();
        stdOutTextArea = new javax.swing.JTextArea();
        stdErrScrollPane = new javax.swing.JScrollPane();
        summaryScrollPane = new javax.swing.JScrollPane();
        stdErrTextArea = new javax.swing.JTextArea();
        summaryTextArea = new javax.swing.JTextArea();
        cancelJobButton = new javax.swing.JButton();
        writeScriptButton = new javax.swing.JButton();
        runScriptButton = new javax.swing.JButton();
        specDecompMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
//        editMenu = new javax.swing.JMenu();
        deleteMenuItem = new javax.swing.JMenuItem();
        newMenuItem = new javax.swing.JMenuItem();
        openMenuItem = new javax.swing.JMenuItem();
        saveMenuItem = new javax.swing.JMenuItem();
//        updateProjectEnvMenuItem = new javax.swing.JMenuItem();
        saveAsMenuItem = new javax.swing.JMenuItem();
        saveQuitMenuItem = new javax.swing.JMenuItem();
        quitMenuItem = new javax.swing.JMenuItem();
        renameMenuItem = new javax.swing.JMenuItem();
        preferenceMenuItem = new javax.swing.JMenuItem();
        executeMenu = new javax.swing.JMenu();
        displayResultsMenuItem = new javax.swing.JMenuItem();
		displayResultsButton = new javax.swing.JButton();

        runMenuItem = new javax.swing.JMenuItem();
        runMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runScriptActionPerformed(evt);
            }
        });
		
        writeMenuItem = new javax.swing.JMenuItem();
        writeMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //if(!validateLandmarkOutput())
                //  return;
                writeScriptActionPerformed(evt);
            }
        });
		
        cancelRunMenuItem = new javax.swing.JMenuItem();
        cancelRunMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelJobButtonActionPerformed(evt);
            }
        });
		
        statusMenuItem = new javax.swing.JMenuItem();
        viewMenu = new javax.swing.JMenu();

        helpMenu = new javax.swing.JMenu();
        helpAboutMenuItem = new javax.swing.JMenuItem();

        viewer2DMenuItem = new javax.swing.JMenuItem();
        viewer3DMenuItem = new javax.swing.JMenuItem();

        fileSeparator = new javax.swing.JSeparator();
        importMenuItem = new javax.swing.JMenuItem();
        exportMenuItem = new javax.swing.JMenuItem();

        // init Seismic Data Input widgets
        bhpsuInPanel = new javax.swing.JPanel();
        datasetLabel = new javax.swing.JLabel();
        chooseDatasetButton = new javax.swing.JButton();
        chooseDatasetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseDatasetButtonActionPerformed(evt);
            }
        });
		
        rangeCdpLabel = new javax.swing.JLabel();
        rangeEpLabel = new javax.swing.JLabel();
        entireDatasetButton = new javax.swing.JButton();
        entireDatasetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                entireDatasetButtonActionPerformed(evt);
            }
        });

        pickButton = new javax.swing.JButton();
        // TODO pick from viewer is currently disabled
        pickButton.setEnabled(false);
        selectedProjectLabel = new javax.swing.JLabel();
        datasetTextField = new DropTextField(DnDConstants.ACTION_COPY, this, agent, "SeismicDataset");

        datasetTextField.addKeyListener(new DatasetTextFieldKeyListener(this));
        // init Outputs widgets
        bhpsuOutPanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        specDecompTabbedPane.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED,
            new java.awt.Color(153, 0, 0), new java.awt.Color(153,
                    0, 153), new java.awt.Color(153, 153, 255),
            new java.awt.Color(0, 0, 0)));

        // landmarkInRadioButton.setSelected(true);
        landmarkInRadioButton.setText("Landmark");
        landmarkInRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        landmarkInRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        landmarkInRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                landmarkInRadioButtonActionPerformed(evt);
            }
        });

        landmarkHorizonRadioButton.setText("Landmark");
        landmarkHorizonRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        landmarkHorizonRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        landmarkHorizonRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //buildflatteningTab1(landmarkHorizonCount);
                if(landmarkHorizonRadioButton.isSelected())
                    numHorizonsComboBox.setSelectedIndex(landmarkHorizonCount);
            }
        });


        // landmarkOutRadioButton.setSelected(true);
        landmarkOutRadioButton.setText("Landmark & BHPSU");
        landmarkOutRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        landmarkOutRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        landmarkOutRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                landmarkOutRadioButtonActionPerformed(evt);
            }
        });

        bhpsuInRadioButton.setSelected(true);
        bhpsuInRadioButton.setText("BHP SU");
        bhpsuInRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        bhpsuInRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        bhpsuInRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bhpsuInRadioButtonActionPerformed(evt);
            }
        });

        bhpsuHorizonRadioButton.setSelected(true);
        bhpsuHorizonRadioButton.setText("ASCII");
        bhpsuHorizonRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        bhpsuHorizonRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        bhpsuHorizonRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //buildflatteningTab1(bhpsuHorizonCount);
                if(bhpsuHorizonRadioButton.isSelected()){
                    numHorizonsComboBox.setSelectedIndex(bhpsuHorizonCount);
                }
            }
        });

        bhpsuHorizonDatasetRadioButton.setText("BHP SU");
        bhpsuHorizonDatasetRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        bhpsuHorizonDatasetRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        bhpsuHorizonDatasetRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(bhpsuHorizonDatasetRadioButton.isSelected()){
                    numHorizonsComboBox.setSelectedIndex(bhpsuHorizonCount);
                }
            }
        });

        bhpsuOutRadioButton.setSelected(true);
        bhpsuOutRadioButton.setText("BHP SU");
        bhpsuOutRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        bhpsuOutRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        bhpsuOutRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bhpsuOutRadioButtonActionPerformed(evt);
            }
        });


        landmark2DRadioButton = new javax.swing.JRadioButton();
        landmark2DRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                landmarkOutProjectTextField.setEnabled(true);
                landmarkOutProjectTextField.setText("");
                switch2LandmarkIn();
            }
        });
        landmark3DRadioButton = new javax.swing.JRadioButton();
        landmark3DRadioButton.setSelected(true);
        landmark3DRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                landmark2DRadioButton.setSelected(false);
                landmarkOutProjectTextField.setEnabled(true);
                landmarkOutProjectTextField.setText("");
                switch2LandmarkIn();
            }
        });

        displayResultsButton.setText("Display Results");
        displayResultsButton.setToolTipText("Display script results in qiViewer");
        displayResultsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                displayResultsButtonActionPerformed(evt);
            }
        });

        // create the Data Input tab with Landmark format pre-selected
        // formLandmarkInPanel();
        // formLandmarkInputDataPanel();

        specDecompTabbedPane.addTab("Seismic Data Input",
                landmarkInputDataPanel);

        smallestScaleLabel = new javax.swing.JLabel();
        smallestScaleTextField = new javax.swing.JTextField(String.valueOf(defaultSmallesetScale));
        largestScaleLabel = new javax.swing.JLabel();
        largestScaleTextField = new javax.swing.JTextField(String.valueOf(defaultLargestScale));
        averageVelociyLabel = new javax.swing.JLabel();
        averageVelocityTextField = new javax.swing.JTextField(String.valueOf(avergeVelocity));
        smoothingLengthLabel = new javax.swing.JLabel();
        smoothingLengthTextField = new javax.swing.JTextField(String.valueOf(defaultSmoothLength));
        numberScalesLabel = new javax.swing.JLabel();
        numberScalesTextField = new javax.swing.JTextField(String.valueOf(defaultNumberOfScales));
                //smallestScaleLabel.setText("Smallest Scale");

        //numberScalesLabel.setText("Number of Scales");

        //largestScaleLabel.setText("Largest Scale");

        //averageVelociyLabel.setText("Average Velocity");

        //smoothingLengthLabel.setText("Smoothing Length");
        inputParametersSmoothingPanel = new javax.swing.JPanel();
        //smoothingLengthLabel = new javax.swing.JLabel();
        numberOfPassLabel = new javax.swing.JLabel();
        //smoothingLengthTextField = new javax.swing.JTextField();
        numberOfPassTextField = new javax.swing.JTextField(String.valueOf(defaultNumOfPass));
        inputParametersScalePanel = new javax.swing.JPanel();
        //smallestScaleLabel = new javax.swing.JLabel();
        //smallestScaleTextField = new javax.swing.JTextField();
        //largestScaleLabel = new javax.swing.JLabel();
        //largestScaleTextField = new javax.swing.JTextField();
        //numberScalesLabel = new javax.swing.JLabel();
        //numberScalesTextField = new javax.swing.JTextField();
        averageIncreaseLabel = new javax.swing.JLabel();
        averageIncreaseTextField = new javax.swing.JTextField(String.valueOf(defaultAvgIncrease));

        averageIncreaseTextField.setEditable(false);
        //averageVelociyLabel = new javax.swing.JLabel();
        //averageVelocityTextField = new javax.swing.JTextField();

        inputParametersSmoothingPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        smoothingLengthLabel.setText("Smoothing Length");

        numberOfPassLabel.setText("Number of Passes");

        smallestScaleTextField.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e) {
                if (validateSmallestScaleTextField() == false)
                    smallestScaleTextField.requestFocus();

                averageIncreaseTextField.setText(getAverageIncrease(largestScaleTextField,smallestScaleTextField,numberScalesTextField));
            }
          });


          numberScalesTextField.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e) {
                if (validateNumberScalesTextField() == false)
                    numberScalesTextField.requestFocus();
                averageIncreaseTextField.setText(getAverageIncrease(largestScaleTextField,smallestScaleTextField,numberScalesTextField));
            }
          });

          largestScaleTextField.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e) {
                if (validateLargestScaleTextField() == false)
                    largestScaleTextField.requestFocus();
                averageIncreaseTextField.setText(getAverageIncrease(largestScaleTextField,smallestScaleTextField,numberScalesTextField));
            }
          });

        org.jdesktop.layout.GroupLayout inputParametersSmoothingPanelLayout = new org.jdesktop.layout.GroupLayout(inputParametersSmoothingPanel);
        inputParametersSmoothingPanel.setLayout(inputParametersSmoothingPanelLayout);
        inputParametersSmoothingPanelLayout.setHorizontalGroup(
            inputParametersSmoothingPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(inputParametersSmoothingPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(inputParametersSmoothingPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(smoothingLengthLabel)
                    .add(numberOfPassLabel))
                .add(28, 28, 28)
                .add(inputParametersSmoothingPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(smoothingLengthTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 87, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(numberOfPassTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 87, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(261, Short.MAX_VALUE))
        );
        inputParametersSmoothingPanelLayout.setVerticalGroup(
            inputParametersSmoothingPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(inputParametersSmoothingPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(inputParametersSmoothingPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(smoothingLengthTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(smoothingLengthLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(inputParametersSmoothingPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(numberOfPassTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(numberOfPassLabel))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        inputParametersScalePanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        smallestScaleLabel.setText("Smallest Scale");

        largestScaleLabel.setText("Largest Scale");

        numberScalesLabel.setText("Number of Scales");

        averageIncreaseLabel.setText("Average Increase");

        averageVelociyLabel.setText("Average Velocity");

        org.jdesktop.layout.GroupLayout inputParametersScalePanelLayout = new org.jdesktop.layout.GroupLayout(inputParametersScalePanel);
        inputParametersScalePanel.setLayout(inputParametersScalePanelLayout);
        inputParametersScalePanelLayout.setHorizontalGroup(
            inputParametersScalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(inputParametersScalePanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(inputParametersScalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(averageIncreaseLabel)
                    .add(averageVelociyLabel)
                    .add(smallestScaleLabel)
                    .add(largestScaleLabel)
                    .add(numberScalesLabel))
                .add(35, 35, 35)
                .add(inputParametersScalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(averageVelocityTextField)
                    .add(largestScaleTextField)
                    .add(smallestScaleTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                    .add(numberScalesTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 50, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(averageIncreaseTextField))
                .addContainerGap(238, Short.MAX_VALUE))
        );
        inputParametersScalePanelLayout.setVerticalGroup(
            inputParametersScalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(inputParametersScalePanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(inputParametersScalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(smallestScaleLabel)
                    .add(smallestScaleTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(inputParametersScalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(largestScaleLabel)
                    .add(largestScaleTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(10, 10, 10)
                .add(inputParametersScalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(numberScalesLabel)
                    .add(numberScalesTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(10, 10, 10)
                .add(inputParametersScalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(averageIncreaseLabel)
                    .add(averageIncreaseTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(10, 10, 10)
                .add(inputParametersScalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(averageVelociyLabel)
                    .add(averageVelocityTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout parametersPanelLayout = new org.jdesktop.layout.GroupLayout(parametersPanel);
        parametersPanel.setLayout(parametersPanelLayout);
        parametersPanelLayout.setHorizontalGroup(
                parametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, parametersPanelLayout.createSequentialGroup()
                .add(parametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, inputParametersSmoothingPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(inputParametersScalePanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        parametersPanelLayout.setVerticalGroup(
                parametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(parametersPanelLayout.createSequentialGroup()
                .add(inputParametersScalePanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(inputParametersSmoothingPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(383, Short.MAX_VALUE))
        );



        specDecompTabbedPane.addTab("Input Parameters", parametersPanel);

        horizonsComboBox.setModel(new javax.swing.DefaultComboBoxModel(
                new String[] { "None", "1 horizon", "2 horizons", "3 horizons",
                        "4 horizons", "5 horizons", "6 horizons"}));
        numHorizonsComboBox.setModel(new javax.swing.DefaultComboBoxModel(
                new String[] { "None", "1 horizon", "2 horizons", "3 horizons",
                        "4 horizons", "5 horizons", "6 horizons"}));
        numHorizonsComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox) e.getSource();
                numberOfHorz = cb.getSelectedIndex();
                logger.info("number Of Horzion selected " + numberOfHorz);
                //formBhpsuHorizonPanel(numberOfHorz);
                //formBhpsuHorizonDataPanel(numberOfHorz);
                /*
                if(bhpsuHorizonRadioButton.isSelected()){
                    bhpsuHorizonCount = numberOfHorz;
                    //switch2BhpsuHorizon(numberOfHorz);
                }else if(landmarkHorizonRadioButton.isSelected()){
                    landmarkHorizonCount = numberOfHorz;
                    //switch2LandmarkHorizon(numberOfHorz);
                }
                */
                buildflatteningTab1(numberOfHorz);
            }
        });
/*
        landmarkHorizonsComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox) e.getSource();
                int numberOfHorz = cb.getSelectedIndex();
                landmarkHorizonCount = numberOfHorz;
                switch2LandmarkHorizon(numberOfHorz);
            }
        });
*/
        /*
        horizonsComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox) e.getSource();
                int numberOfHorz = cb.getSelectedIndex();

                buildflatteningTab(numberOfHorz);
            }
        });
*/
        horizonsLabel.setText("Number of horizons:");

        listRadioButton.setText("Choose from List");
        listRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(
                0, 0, 0, 0));
        listRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        listRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listRadioButtonActionPerformed(evt);
            }
        });

        viewerRadioButton.setText("Choose from Viewer");
        viewerRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        viewerRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        viewerRadioButton
                .addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        viewerRadioButtonActionPerformed(evt);
                    }
                });

        buildflatteningTab1(0);
        // create an Outputs tab with empty content. Content will be formed
        // when Outputs tab selected
        specDecompTabbedPane.addTab("Outputs", landmarkOutputsPanel);

        statusLabel.setText("Status:");

        stateLabel.setText("");

        updateButton.setText("Check Status");
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });

        detailsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(
                null, "Status Details",
                javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                javax.swing.border.TitledBorder.DEFAULT_POSITION,
                new java.awt.Font("Tahoma", 1, 11)));
        stdOutTextArea.setColumns(20);
        stdOutTextArea.setEditable(false);
        stdOutTextArea.setFont(new java.awt.Font("Arial", 0, 12));
        stdOutTextArea.setRows(5);
        stdOutScrollPane.setViewportView(stdOutTextArea);

        stdErrTextArea.setColumns(20);
        stdErrTextArea.setEditable(false);
        stdErrTextArea.setFont(new java.awt.Font("Arial", 0, 12));
        stdErrTextArea.setRows(5);
        stdErrScrollPane.setViewportView(stdErrTextArea);

        summaryTextArea.setColumns(20);
        summaryTextArea.setEditable(false);
        summaryTextArea.setFont(new java.awt.Font("Arial", 0, 12));
        summaryTextArea.setRows(5);
        summaryScrollPane.setViewportView(summaryTextArea);

        JSplitPane splitPane = new JSplitPane(
                                       JSplitPane.VERTICAL_SPLIT,
                                       stdErrScrollPane, stdOutScrollPane);
        splitPane.setOneTouchExpandable(true);


        org.jdesktop.layout.GroupLayout detailsPanelLayout = new org.jdesktop.layout.GroupLayout(
                detailsPanel);
        detailsPanel.setLayout(detailsPanelLayout);

        detailsPanelLayout.setHorizontalGroup(
            detailsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(splitPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 600,Short.MAX_VALUE)
                .add(summaryScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 600,Short.MAX_VALUE)
        );
        detailsPanelLayout.setVerticalGroup(detailsPanelLayout
                .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(org.jdesktop.layout.GroupLayout.TRAILING, detailsPanelLayout.createSequentialGroup()
                   .addContainerGap()
                   .add(splitPane,org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 353,Short.MAX_VALUE)
                   .add(summaryScrollPane,org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        cancelJobButton.setText("Cancel Run");
        cancelJobButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                cancelJobButtonActionPerformed(e);
            }
        });

        writeScriptButton.setText("Write Script");
        writeScriptButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                writeScriptActionPerformed(e);
            }
        });

        runScriptButton.setText("Run Script");
        runScriptButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                runScriptActionPerformed(e);
            }
        });

        org.jdesktop.layout.GroupLayout statusPanelLayout = new org.jdesktop.layout.GroupLayout(
                statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout
            .setHorizontalGroup(statusPanelLayout
                .createParallelGroup(
                        org.jdesktop.layout.GroupLayout.LEADING)
                .add(
                    statusPanelLayout
                        .createSequentialGroup()
                        .addContainerGap()
                        .add(
                            statusPanelLayout
                                .createParallelGroup(
                                        org.jdesktop.layout.GroupLayout.LEADING)
                                .add(
                                    detailsPanel,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    Short.MAX_VALUE)
                                .add(
                                    statusPanelLayout
                                        .createSequentialGroup()
                                        .add(
                                                statusLabel)
                                        .addPreferredGap(
                                                org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(
                                                stateLabel)
                                        .addPreferredGap(
                                                org.jdesktop.layout.LayoutStyle.RELATED,
                                                268,
                                                Short.MAX_VALUE)
                                        .add(writeScriptButton)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(runScriptButton)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
										.add(displayResultsButton)
										.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(updateButton)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(cancelJobButton)
                                        ))
                    .addContainerGap()));
        statusPanelLayout
            .setVerticalGroup(statusPanelLayout
                .createParallelGroup(
                        org.jdesktop.layout.GroupLayout.LEADING)
                .add(
                    statusPanelLayout
                        .createSequentialGroup()
                        .addContainerGap()
                        .add(
                            statusPanelLayout
                                .createParallelGroup(
                                        org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(statusLabel).add(
                                        stateLabel)
                                .add(updateButton)
								.add(displayResultsButton)
                                .add(runScriptButton)
                                .add(writeScriptButton)
                                .add(cancelJobButton))
                        .addPreferredGap(
                                org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(
                            detailsPanel,
                            org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                            org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                            Short.MAX_VALUE)
                        .addContainerGap()));
        specDecompTabbedPane.addTab("Job Status", statusPanel);

        // handle switching tabs
        specDecompTabbedPane
                .addChangeListener(new javax.swing.event.ChangeListener() {
                    public void stateChanged(javax.swing.event.ChangeEvent evt) {
                        specDecompTabbedPaneStateChanged(evt);
                    }
                });

        fileMenu.setMnemonic('F');
        fileMenu.setText("File");
//        editMenu.setMnemonic('E');
//        editMenu.setText("Edit");
        deleteMenuItem.setMnemonic('D');
        deleteMenuItem.setText("Delete");
        deleteMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteMenuItemActionPerformed(evt);
            }
        });

        //fileMenu.add(deleteMenuItem);

        newMenuItem.setMnemonic('N');
        newMenuItem.setText("New");
        newMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newMenuItemActionPerformed(evt);
            }
        });

        //fileMenu.add(newMenuItem);

        openMenuItem.setMnemonic('O');
        openMenuItem.setText("Open");
        openMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openMenuItemActionPerformed(evt);
            }
        });

        //fileMenu.add(openMenuItem);
        preferenceMenuItem.setMnemonic('P');
        preferenceMenuItem.setText("Preferences...");
        preferenceMenuItem.setToolTipText("Click to change the preferred default script directory");
        preferenceMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                String dir = (String) JOptionPane
                        .showInputDialog(comp, "Preferred script directory:",
                                defaultPreferredScriptDir);
                if (dir != null && dir.trim().length() == 0) {
                    JOptionPane.showMessageDialog(comp,
                            "The prefered script directory can not be empty.",
                            "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                    return;
                }
                if(dir != null)
                    defaultPreferredScriptDir = dir;
            }
        });

        fileMenu.add(preferenceMenuItem);

        javax.swing.JSeparator preferenceSeparator = new javax.swing.JSeparator();
        fileMenu.add(preferenceSeparator);

        importMenuItem.setMnemonic('I');
        importMenuItem.setText("Import");
        importMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(importMenuItem);

        exportMenuItem.setMnemonic('E');
        exportMenuItem.setText("Export");
        exportMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exportMenuItem);

        fileMenu.add(fileSeparator);

        quitMenuItem.setMnemonic('Q');
        quitMenuItem.setText("Quit");
        quitMenuItem.setToolTipText("Click to Quit");
        fileMenu.add(quitMenuItem);
        quitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitMenuItemActionPerformed(evt);
            }
        });

        saveMenuItem.setMnemonic('S');
        saveMenuItem.setToolTipText("Click to Save state");
        saveMenuItem.setText("Save");
        saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveMenuItem);
/*
        updateProjectEnvMenuItem.setMnemonic('M');
        updateProjectEnvMenuItem.setToolTipText("Click to modify project environment.");
        updateProjectEnvMenuItem.setText("Modify Project Environment");
        updateProjectEnvMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateProjectEnvMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(updateProjectEnvMenuItem);
*/
        saveAsMenuItem.setMnemonic('A');
        saveAsMenuItem.setText("Save As");
        saveAsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsMenuItemActionPerformed(evt);
            }
        });

        fileMenu.add(saveAsMenuItem);


        saveQuitMenuItem.setMnemonic('U');
        saveQuitMenuItem.setText("Save, Quit");
        saveQuitMenuItem.setToolTipText("Click to Save state then quit");
        saveQuitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveQuitMenuItemActionPerformed(evt);
            }
        });

        fileMenu.add(saveQuitMenuItem);


        renameMenuItem.setMnemonic('R');
        renameMenuItem.setText("Rename");
        //fileMenu.add(renameMenuItem);

        specDecompMenuBar.add(fileMenu);
//        specDecompMenuBar.add(editMenu);

        executeMenu.setMnemonic('E');
        executeMenu.setText("Execute");

        writeMenuItem.setMnemonic('W');
        writeMenuItem.setToolTipText("Click to write the script file");
        writeMenuItem.setText("Write Script");
        executeMenu.add(writeMenuItem);

        runMenuItem.setMnemonic('R');
        runMenuItem.setToolTipText("Click to execute the script file");
        runMenuItem.setText("Run Script");
        executeMenu.add(runMenuItem);
		
        displayResultsMenuItem.setMnemonic('D');
        displayResultsMenuItem.setText("Display Results");
        displayResultsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                displayResultsButtonActionPerformed(evt);
            }
        });
        executeMenu.add(displayResultsMenuItem);

        statusMenuItem.setMnemonic('S');
        statusMenuItem.setToolTipText("Click to check the status of the running script");
        statusMenuItem.setText("Check Status");

        statusMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                specDecompTabbedPane.setSelectedIndex(STATUS_TAB_INDEX);
                logger.info("job_status " + job_status);
                if(job_status == WaveletDecompConstants.JOB_UNSUBMITTED_STATUS){
                    JOptionPane.showMessageDialog(comp, "Job Not Yet Submitted.","Job Unsubmitted", JOptionPane.WARNING_MESSAGE);
                    stateLabel.setText("Job Not Submitted");
                    runMenuItem.setEnabled(true);
                    runScriptButton.setEnabled(true);
                    return;
                }

                Runnable heavyRunnable = new Runnable(){
                    public void run(){
                        updateStatus();                 }
                };
                new Thread(heavyRunnable).start();


                //updateButton.doClick();
            }
        });
        executeMenu.add(statusMenuItem);

        cancelRunMenuItem.setMnemonic('C');
        cancelRunMenuItem.setToolTipText("Click to cancel the job");
        cancelRunMenuItem.setText("Cancel Run");
        executeMenu.add(cancelRunMenuItem);

        specDecompMenuBar.add(executeMenu);

        // TODO Menu items only active if BHP SU selected
        viewMenu.setMnemonic('V');
        viewMenu.setText("View");
        helpMenu.setMnemonic('H');
        helpMenu.setText("Help");
        helpMenu.add(helpAboutMenuItem);
        helpAboutMenuItem.setText("About ...");
        helpAboutMenuItem.setToolTipText("Click to view About WaveletDecomp");
        helpAboutMenuItem.setMnemonic('A');
        helpAboutMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if(props == null){
                    String compName = agent.getMessagingMgr().getRegisteredComponentDisplayNameByDescriptor(agent.getComponentDescriptor());
                    props = ComponentUtils.getComponentVersionProperies(compName);
                }
                new WaveletDecompAbout(comp,props);
            }
        });
        //{
        //  public void actionPerformed(java.awt.event.ActionEvent evt) {
        //      new WaveletDecompAbout(comp);
        //  }
        //});
        viewer2DMenuItem.setMnemonic('2');
        viewer2DMenuItem.setText("2D Viewer");
        viewer2DMenuItem.setToolTipText("Click to view 2D Viewer");
        viewMenu.add(viewer2DMenuItem);

        viewer3DMenuItem.setMnemonic('3');
        viewer3DMenuItem.setToolTipText("Click to view 3D Viewer");
        viewer3DMenuItem.setText("3D Viewer");
        viewer3DMenuItem.setEnabled(false);
        viewMenu.add(viewer3DMenuItem);

        specDecompMenuBar.add(viewMenu);
        specDecompMenuBar.add(helpMenu);

        setJMenuBar(specDecompMenuBar);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(
                getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(
                org.jdesktop.layout.GroupLayout.LEADING).add(
                specDecompTabbedPane,
                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 575,
                Short.MAX_VALUE));
        layout.setVerticalGroup(layout.createParallelGroup(
                org.jdesktop.layout.GroupLayout.LEADING).add(
                layout.createSequentialGroup().add(specDecompTabbedPane,
                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 483,
                        Short.MAX_VALUE)));

        //final Component component = this;
        //specDecompTabbedPane.addChangeListener(new ChangeListener() {
        //  public void stateChanged(ChangeEvent e) {
        //      if (e.getSource() instanceof JTabbedPane) {
        //          JTabbedPane tp = (JTabbedPane) e.getSource();
        //          if (tp.getSelectedIndex() != FLATTENING_TAB_INDEX) {
        //              validateFlatteningInfo(component);
        //          }
        //      }
        //  }
        //});

        ButtonGroup outputSUGroupOut = new ButtonGroup();
        outputSUGroupOut.add(bhpsuScaleRawRadioButton);
        outputSUGroupOut.add(bhpsuScaleNormalizedRadioButton);
        switch2BhpsuIn();
        setSize(800, 740);
        this.setIconifiable(true);
        this.setClosable(false);
        this.setMaximizable(true);
        this.setResizable(true);

        listRadioButton.setEnabled(false);
        viewerRadioButton.setEnabled(false);
        cdpTextField.setEnabled(false);
        cdpToTextField.setEditable(false);
        cdpByTextField.setEditable(false);
        epTextField.setEnabled(false);
        epToTextField.setEditable(false);
        epByTextField.setEditable(false);
        entireDatasetButton.setEnabled(false);
        setLineTraceFieldEnabled(false);

        //smallestScaleTextField.setText(String.valueOf(defaultSmallScale));
        //largestScaleTextField.setText(String.valueOf(defaultLargestScale));
        //numberScalesTextField.setText(String.valueOf(defaultNumberOfScales));
        //smoothingLengthTextField.setText(String.valueOf(defaultSmoothLength));
        // pack();


    }// </editor-fold>//GEN-END:initComponents

    private void formInputParameterPanel(){

        org.jdesktop.layout.GroupLayout parametersPanelLayout = new org.jdesktop.layout.GroupLayout(
                parametersPanel);
        parametersPanel.setLayout(parametersPanelLayout);
        parametersPanelLayout
            .setHorizontalGroup(parametersPanelLayout
                .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(parametersPanelLayout
                    .createSequentialGroup()
                    .addContainerGap()
                    .add(parametersPanelLayout
                        .createParallelGroup(
                            org.jdesktop.layout.GroupLayout.LEADING)
                        .add(parametersPanelLayout
                            .createSequentialGroup()
                            .add(smallestScaleLabel)
                            .add(30,30,30)
                            .add(smallestScaleTextField,
                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                89,
                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(parametersPanelLayout
                .createSequentialGroup()
                .add(parametersPanelLayout
                    .createParallelGroup(
                            org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING,
                            parametersPanelLayout
                                .createSequentialGroup()
                                .add(largestScaleLabel)
                                .add(33,33,33)
                                .add(largestScaleTextField,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                    89,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.LEADING,
                        parametersPanelLayout
                            .createSequentialGroup()
                            .add(parametersPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.LEADING)
                                .add(numberScalesLabel)
                                .add(averageVelociyLabel))
                            .add(14,14,14)
                            .add(parametersPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.LEADING)
                                .add(
                                    averageVelocityTextField,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    89,
                                    Short.MAX_VALUE)
                                .add(
                                    numberScalesTextField,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    89,
                                    Short.MAX_VALUE)))
                    .add(org.jdesktop.layout.GroupLayout.LEADING,
                        parametersPanelLayout
                            .createSequentialGroup()
                            .add(smoothingLengthLabel)
                            .add(11,11,11)
                .add(smoothingLengthTextField,org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,89,org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
            .add(340,340,340)))));
        parametersPanelLayout
                .setVerticalGroup(parametersPanelLayout
                        .createParallelGroup(
                                org.jdesktop.layout.GroupLayout.LEADING)
                        .add(parametersPanelLayout
                            .createSequentialGroup()
                            .addContainerGap()
                            .add(parametersPanelLayout
                                .createParallelGroup(
                                        org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(smallestScaleLabel)
                                .add(smallestScaleTextField,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(
                                    org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(parametersPanelLayout
                                .createParallelGroup(
                                        org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(largestScaleLabel)
                                .add(
                                    largestScaleTextField,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(parametersPanelLayout
                                .createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(numberScalesLabel)
                                .add(numberScalesTextField,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(parametersPanelLayout
                                .createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(averageVelociyLabel)
                                .add(averageVelocityTextField,org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(parametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(smoothingLengthLabel)
                                .add(smoothingLengthTextField,org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)).addContainerGap(304, Short.MAX_VALUE)));

    }

    public void setLineTraceFieldEnabled(boolean bool){
        entireVolumeButton.setEnabled(bool);
        linesTextField.setText("");
        linesTextField.setEditable(bool);
        linesToTextField.setText("");
        linesToTextField.setEditable(bool);
        linesByTextField.setText("");
        linesByTextField.setEditable(bool);
        tracesTextField.setText("");
        tracesTextField.setEditable(bool);
        tracesToTextField.setText("");
        timeTextField.setText("");
        timeTextField.setEditable(bool);
        tracesToTextField.setEditable(bool);
        timeToTextField.setText("");
        timeToTextField.setEditable(bool);
        tracesByTextField.setText("");
        tracesByTextField.setEditable(bool);
    }

    public void setHorizonDataPath(javax.swing.JTextField target, String path) {
    	target.setText(path);
    }

    private String getAverageIncrease(JTextField last, JTextField first, JTextField ns){
        double last_d = Double.valueOf(last.getText().trim());
        double first_d = Double.valueOf(first.getText().trim());
        int ns_i = Integer.valueOf(ns.getText().trim());
        if(ns_i != 0){
            double d = (last_d - first_d) / ns_i;
            String d_s = new Format("%0.2f").format(d);
            return d_s;
        }else
            return "NAV";
    }

    public void setSelectedLandmarkProject(String text){
        selectedLandmarkProject = text;
        landmarkInProjectTextField.setText(text);

    }

    /*
     * Check is the data input is Landmark type
     */
    public boolean isLandmarkSelected(){
        return landmarkInRadioButton.isSelected();
    }

    public void setLandmarkOutProjectName(String name){
        if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected())
            landmark2DOutProjectTextField.setText(name);
        else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected())
            landmark3DOutProjectTextField.setText(name);
        else
            landmarkOutProjectTextField.setText(name);
    }
    public String getSelectedLandmarkProject(){
        return selectedLandmarkProject;
    }
    public void setVolumeTextField(String text){
        volumeTextField.setText(text);
    }

    public String getVolumeTextField(){
        return volumeTextField.getText();
    }

    public void setLandmarkHorizonCount(int val){
        landmarkHorizonCount = val;
        horizonsComboBox.setSelectedIndex(val);
    }

    private Map<String,List<String>> landmark2DfileLinesMap;
    public void setLandmarkIn2DProjectTextField(String text){
        landmarkIn2DProjectTextField.setText(text);
        String [] files = new String[1];
        if(text.trim().length() > 0){
            if(landmarkAgent == null)
                landmarkAgent = LandmarkServices.getInstance();
            landmark2DfileLinesMap = landmarkAgent.get2v2FileLinesMap(text.trim());
            Set set= landmark2DfileLinesMap.keySet () ;
            files = new String[set.size()+1];
            Iterator iter = set.iterator () ;
            files[0] = "Select one";
            int i = 1;
            while ( iter.hasNext())  {
                Object obj = iter.next();
                files[i++]=(String)obj;
            }
        }
        Arrays.sort(files,1,files.length);
        seismic2DFileComboBox.setModel(new javax.swing.DefaultComboBoxModel(files));
        seismic2DLineList.setListData(new Vector());
        selectedLines = new Vector();
        selectedSeismic2DLineList.setListData(selectedLines);
        selectedProcessLevelIndex = 0;
        resetShotpointInfo();
        if(landmarkOutRadioButton.isSelected()){
            if(!landmark2DOutProjectTextField.isEnabled())
                landmark2DOutProjectTextField.setEnabled(true);
            //landmarkOutListButton.setEnabled(false);
            //landmark2DOutProjectTextField.setEditable(false);
        }


    }
    private boolean validateCdpToTextField() {
        final Component comp = this;
        String str = cdpToTextField.getText();
        int toVal = -1;
        try {
            toVal = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            cdpToTextField.requestFocus();
            cdpToTextField.setText(String.valueOf(defaultcdpMax));
            return false;
        }
        if (toVal > defaultcdpMax) {
            JOptionPane.showMessageDialog(comp,
                    "Must not be greater than the default value ("
                            + defaultcdpMax + ")", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            cdpToTextField.requestFocus();
            cdpToTextField.setText(String.valueOf(defaultcdpMax));
            return false;
        }

        int fromVal = Integer.valueOf(cdpTextField.getText()).intValue();
        if (!(toVal >= fromVal)) {
            JOptionPane.showMessageDialog(comp,
                    "Must be greater than or equal to the cdp value ("
                            + fromVal + ")", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            cdpToTextField.requestFocus();
            cdpToTextField.setText(String.valueOf(fromVal));
            return false;
        }

         int rem = (defaultcdpMax-toVal) % defaultcdpBy;
         if(rem != 0){
             JOptionPane.showMessageDialog(comp,"Must be multiple of the incr by to the maxmum value (" + defaultcdpMax + ")" ,
                     "Invalid data entry",JOptionPane.WARNING_MESSAGE);
             specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
             cdpToTextField.requestFocus();
             cdpToTextField.setText(String.valueOf(toVal - rem));
             return false;
        }

        return true;

    }

    private boolean validateLineTraceTextField(JTextField from, JTextField to, JTextField inc, int[] def) {
        final Component comp = this;
        String str = from.getText().trim();
        int fromVal = -1;
        try {
            fromVal = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be an integer value. Reset to default \"from\" value (" + def[0] + ").",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            from.requestFocus();
            from.setText(String.valueOf(def[0]));
            return false;
        }

        int toVal = -1;
        try {
            toVal = Integer.valueOf(to.getText().trim()).intValue();
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be an integer value. Reset to default \"to\" value (" + def[1] + ").",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            to.requestFocus();
            to.setText(String.valueOf(def[1]));
            return false;
        }

        int by = -1;
        try {
            by = Integer.parseInt(inc.getText().trim());
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be an integer value. Reset to default increment value (" + def[2] + ").",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            inc.requestFocus();
            inc.setText(String.valueOf(def[2]));
            return false;
        }
        if(by == 0){
            JOptionPane.showMessageDialog(comp, "Increment must not be 0. Reset to the default value (" + def[2] + ").",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            inc.requestFocus();
            inc.setText(String.valueOf(def[2]));
            return false;
        }

        if(def[2] > 0){
            if(by < def[2]){
                JOptionPane.showMessageDialog(comp, "The increment value must not less than the defaul value (" + def[2] + "). Reset to default.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                inc.requestFocus();
                inc.setText(String.valueOf(def[2]));
                return false;
            }
        } else if(def[2] < 0){
            if(by > def[2]){
                JOptionPane.showMessageDialog(comp, "The increment value must not greater than the defaul value (" + def[2] + "). Reset to default.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                inc.requestFocus();
                inc.setText(String.valueOf(def[2]));
                return false;
            }
        }

        if(def[2] != 0){
            int rem = by % def[2];
            if(rem != 0){
                JOptionPane.showMessageDialog(comp, "The increment value must be divisible by the default value (" + def[2] +  "). Reset to a valid one",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                by = by - rem;
                inc.requestFocus();
                inc.setText(String.valueOf(by));
                return false;
            }
        }
        if(def[2] > 0){
            if (!(fromVal >= def[0])) {
                JOptionPane.showMessageDialog(comp,
                        "The vaule of \"from\" field must not be less than the default \"from\" value ("
                         + def[0] + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                from.requestFocus();
                from.setText(String.valueOf(def[0]));
                return false;
            }

            int rem = (fromVal-def[0]) % def[2];
            if(rem != 0){

                JOptionPane.showMessageDialog(comp,
                      "The difference of " + fromVal + " (from) and " + def[0] + " (default \"from\" value) must be divisible by "
                           + def[2] + ". Reset to a valid one.", "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                fromVal = fromVal - rem;
                from.requestFocus();
                from.setText(String.valueOf(fromVal));
                return false;
            }
            if (toVal > def[1]) {
                JOptionPane.showMessageDialog(comp,
                        "The value of \"to\" field must not be greater than the default \"to\" value ("
                                + def[1] + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }
            if (!(toVal >= fromVal)) {
                JOptionPane.showMessageDialog(comp,
                        "The value of \"to\" field must be greater than the value of \"from\" field("
                                + fromVal + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }
            rem = (toVal - fromVal) % by;
            if(rem != 0){
                JOptionPane.showMessageDialog(comp,
                      "The difference of " + toVal + " (\"to\" field) and " + fromVal + " (\"from\" field) must be divisible by increment "
                           + by + ". Reset to a valid one.", "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                to.requestFocus();
                toVal = toVal - rem;
                to.setText(String.valueOf(toVal));
                return false;
            }
        } else {
            if (fromVal > def[0]) {
                JOptionPane.showMessageDialog(comp,
                        "The value of \"from\" field must not be greater than the default \"from\" value ("
                         + def[0] + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                from.requestFocus();
                from.setText(String.valueOf(def[0]));
                return false;
            }
            int rem = (fromVal - def[0]) % def[2];
            if(rem != 0){

                JOptionPane.showMessageDialog(comp,
                  "The difference of \"from\" field value(" + fromVal + ") and default \"from\" field value(" + def[0] + ") must be divisible by the default increment ("
                       + def[2] + "). Reset to a valid one.", "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                fromVal = fromVal - rem;
                from.requestFocus();
                from.setText(String.valueOf(fromVal));
                return false;
            }

            if (!(toVal <= fromVal)) {
                JOptionPane.showMessageDialog(comp,
                        "The value of \"to\" field " +  toVal + " must be less than or equal to the value of \"from\" field " + fromVal + ". Reset to default \"to\" field."
                         + fromVal, "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }

            rem = (toVal - fromVal) % by;
            if(rem != 0){

                JOptionPane.showMessageDialog(comp,
                      "The difference of " + toVal + " (\"to\" field) and " + fromVal + " (\"from\" field) must be divisible by the increment "
                           + by + ". Reset to a valid one.", "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                toVal = toVal - rem;
                to.requestFocus();
                to.setText(String.valueOf(toVal));
                return false;
            }

            if(!(toVal >= def[1])){
                JOptionPane.showMessageDialog(comp,
                        "The value of \"to\" field must be greater than or equals to the value of \"from\" field. Reset to default \"to\" value ("
                          + def[1] + ")", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }
        }

        if(def[2] != 0){
            int rem = (fromVal-def[0]) % def[2];
            if(rem != 0){

                JOptionPane.showMessageDialog(comp,
                      "The difference of " + fromVal + " and " + def[0] + " must be divisible by "
                           + def[2], "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                fromVal = fromVal - rem;
                from.requestFocus();
                from.setText(String.valueOf(fromVal));
                return false;
            }
        }

        if(by != 0){
            int rem = (toVal - fromVal) % by;
            if(rem != 0){
             JOptionPane.showMessageDialog(comp,"Must be multiple of the increment " + by + " from the value " + fromVal ,
                     "Invalid data entry",JOptionPane.WARNING_MESSAGE);
             specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
             to.requestFocus();
             if(rem > 0){
                 int next = toVal - rem;
                 if(next == fromVal)
                     next += by;
                 to.setText(String.valueOf(next));
             }else{
                 int next = toVal + rem;
                 if(next == fromVal)
                     next += by;
                 to.setText(String.valueOf(next));
             }
             return false;
            }
        }

        return true;
    }

    private boolean validateTimeTextField(JTextField from, JTextField to, int[] def) {
        final Component comp = this;
        String str = from.getText().trim();
        int fromVal = -1;
        try {
            fromVal = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "The \"from\" field value must be an integer value. Reset to default value " + def[0],
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            from.requestFocus();
            from.setText(String.valueOf(def[0]));
            return false;
        }
        if(fromVal < 0){
            JOptionPane.showMessageDialog(comp, "The \"from\" field value must not be a negative numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            from.requestFocus();
            from.setText(String.valueOf(def[0]));
            return false;
        }
        int toVal = -1;
        try {
            toVal = Integer.valueOf(to.getText().trim()).intValue();
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "The \"to\" field value must be an integer value. Reset to default value " + def[1],
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            to.requestFocus();
            to.setText(String.valueOf(def[1]));
            return false;
        }
        if(toVal < 0){
            JOptionPane.showMessageDialog(comp, "The \"to\" field value must not be a negative numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            to.requestFocus();
            to.setText(String.valueOf(def[1]));
            return false;
        }
        if (!(fromVal >= def[0])) {
            JOptionPane.showMessageDialog(comp,
                    "The \"from\" field value must not be less than the default minimum time value "
                            + def[0] + ". Reset to default value.", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            from.requestFocus();
            from.setText(String.valueOf(def[0]));
            return false;
        }
        if (toVal > def[1]) {
            JOptionPane.showMessageDialog(comp,
                    "The \"to\" time field value " + toVal + " must not be greater than the default maximum time value " + def[1] + ". Reset to default.", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            to.requestFocus();
            to.setText(String.valueOf(def[1]));
            return false;
        }
        if (!(toVal >= fromVal)) {
            JOptionPane.showMessageDialog(comp,
                    "The \"to\" field value (" + toVal + ") must be greater than or equal to the \"from\" field value ("
                            + fromVal + ").", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            to.requestFocus();
            to.setText(String.valueOf(def[1]));
            return false;
        }

        if(def[2] != 0 ){
            int rem = (fromVal-def[0]) % def[2];
            if(rem != 0){
                JOptionPane.showMessageDialog(comp,
                        "The difference of the \"from\" field value " + fromVal + " and the default \"from\" value " + def[0] + " must be divisible by the sample rate " + def[2] + ". Reset to a valid one."
                        , "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                fromVal = fromVal - rem;
                from.requestFocus();
                from.setText(String.valueOf(fromVal));
                return false;
            }

            rem = (toVal - fromVal) % def[2];
            if(rem != 0){
                JOptionPane.showMessageDialog(comp,"The difference of the \"to\" field value " + toVal + " and the \"from\" field value " + fromVal + " must be divisible by the sample rate " + def[2] + ". Reset to a valid one." ,
                 "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                to.requestFocus();
                int next = toVal - rem;
                to.setText(String.valueOf(next));
                return false;
            }
        }
        return true;
    }


    public void setLandmarkOutProjectType(String type){
        landmarkOutProjectType = type;
    }


    //public String getLandmarkProjectType(){
    //  if(landmarkInProjectTextField.getText() == null || landmarkInProjectTextField.getText().trim().length() == 0)
    //      return null;
    //
    //  return landmarkProjectType;
    //}

    public String getLandmarkProjectText(){
        return landmarkInProjectTextField.getText();
    }
    private boolean validateEpTextField() {
        final Component comp = this;
        String str = epTextField.getText();
        int ival = -1;
        try {
            ival = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            epTextField.requestFocus();
            epTextField.setText(String.valueOf(defaultepMin));
            return false;
        }
        if (ival < defaultepMin) {
            JOptionPane.showMessageDialog(comp,
                    "Must not be less than the default value (" + defaultepMin
                            + ")", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            epTextField.requestFocus();
            epTextField.setText(String.valueOf(defaultepMin));
            return false;
        }
        int rem = (ival - defaultepMin) % defaultepBy;
        if (rem != 0) {
            JOptionPane.showMessageDialog(comp,
                    "Must be multiple of the incr by plus the minmum value ("
                            + defaultepMin + ")", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            epTextField.requestFocus();
            epTextField.setText(String.valueOf(ival - rem));
            return true;
        }
        return true;
    }


    private boolean validateEpToTextField() {
        final Component comp = this;
        String str = epToTextField.getText();
        int toVal = -1;
        try {
            toVal = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            epToTextField.requestFocus();
            epToTextField.setText(String.valueOf(defaultepMax));
            return false;
        }
        if (toVal > defaultepMax) {
            JOptionPane.showMessageDialog(comp,
                    "Must not be greater than the default value ("
                            + defaultepMax + ")", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            epToTextField.requestFocus();
            epToTextField.setText(String.valueOf(defaultepMax));
            return false;
        }

        int fromVal = Integer.valueOf(epTextField.getText()).intValue();
        if (!(toVal >= fromVal)) {
            JOptionPane.showMessageDialog(comp,
                    "Must be greater than or equal to the minmum value ("
                            + defaultepMin + ")", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            epToTextField.requestFocus();
            epToTextField.setText(String.valueOf(fromVal));
            return false;
        }
        /*
         * int rem = (defaultepMax-toVal) % defaultepBy; if(rem != 0){
         * JOptionPane.showMessageDialog(comp,"Must be multiple of the incr by
         * to the maxmum value (" + defaultepMax + ")" , "Invalid data
         * entry",JOptionPane.WARNING_MESSAGE);
         * epToTextField.setText(String.valueOf(toVal - rem));
         * epToTextField.requestFocus(); return; }
         */
        return true;
    }

    private boolean validateEpByTextField() {
        final Component comp = this;
        String str = epByTextField.getText();
        int ival = -1;
        try {
            ival = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            epByTextField.requestFocus();
            epByTextField.setText(String.valueOf(defaultepBy));
            return false;
        }
        if (ival < defaultepBy) {
            JOptionPane.showMessageDialog(comp,
                    "Must not be less than the default value (" + defaultepBy
                            + ")", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            epByTextField.requestFocus();
            epByTextField.setText(String.valueOf(defaultepBy));
            return false;
        }
        int rem = ival % defaultepBy;

        if (rem != 0) {
            JOptionPane.showMessageDialog(comp,
                    "Must be multiple of the default value (" + defaultepBy
                            + ")", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            epByTextField.requestFocus();
            epByTextField.setText(String.valueOf(ival - rem));
            return false;
        }
        return true;
    }

    private boolean validateCdpByTextField() {
        final Component comp = this;
        String str = cdpByTextField.getText();
        int ival = -1;
        try {
            ival = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            cdpByTextField.requestFocus();
            cdpByTextField.setText(String.valueOf(defaultcdpBy));
            return false;
        }
        if (ival < defaultcdpBy) {
            JOptionPane.showMessageDialog(comp,
                    "Must not be less than the default value (" + defaultcdpBy
                            + ")", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            cdpByTextField.requestFocus();
            cdpByTextField.setText(String.valueOf(defaultcdpBy));
            return false;
        }
        int rem = ival % defaultcdpBy;
        if (rem != 0) {
            JOptionPane.showMessageDialog(comp,
                    "Must be multiple of the default value (" + defaultcdpBy
                            + ")", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            cdpByTextField.requestFocus();
            cdpByTextField.setText(String.valueOf(ival - rem));
            return false;
        }
        return true;
    }

    private boolean validateBhpsuNoiseFactorTextField() {
        String str = bhpsuNoiseFactorTextField.getText().trim();
        final Component comp = this;
        if (str.length() == 0) {
            JOptionPane.showMessageDialog(comp,
                    "The Noise Factor field is empty.", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
            bhpsuNoiseFactorTextField.requestFocus();
            return false;
        }else{
            int ival = -1;
            try {
                ival = Integer.parseInt(str);
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                bhpsuNoiseFactorTextField.requestFocus();
                return false;
            }
            if (ival < 0) {
                JOptionPane.showMessageDialog(comp,
                        "Must be a positive numeric value.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                bhpsuNoiseFactorTextField.requestFocus();
                return false;
            }
            return true;
        }
    }

    private boolean validateAverageVelocityTextField() {
        String str = averageVelocityTextField.getText().trim();
        final Component comp = this;
        if (str.length() == 0) {
            JOptionPane.showMessageDialog(comp,
                    "The Average Velocity field is empty.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            averageVelocityTextField.requestFocus();
            return false;
        }
        int ival = -1;
        try {
            ival = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            averageVelocityTextField.requestFocus();
            return false;
        }
        if (ival < 0) {
            JOptionPane.showMessageDialog(comp,
                    "Must be a positive numeric value.", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            averageVelocityTextField.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validateCdpTextField() {
        String str = cdpTextField.getText();
        final Component comp = this;
        int ival = -1;
        try {
            ival = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            cdpTextField.setText(String.valueOf(defaultcdpMin));
            return false;
        }
        if (ival < defaultcdpMin) {
            JOptionPane.showMessageDialog(comp,
                    "Must not be less than the default value (" + defaultcdpMin
                            + ")", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            cdpTextField.setText(String.valueOf(defaultcdpMin));
            return false;
        }
        int rem = (ival - defaultcdpMin) % defaultcdpBy;
        if (rem != 0) {
            JOptionPane.showMessageDialog(comp,
                    "Must be multiple of the incr by plus the minmum value ("
                            + defaultcdpMin + ")", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            cdpTextField.setText(String.valueOf(ival - rem));
            return false;
        }
        return true;
    }

    private boolean validateSmallestScaleTextField() {
        final Component comp = this;
        String str = smallestScaleTextField.getText();
        if (str == null || str.trim().length() == 0) {
            JOptionPane.showMessageDialog(comp,
                    smallestScaleLabel.getText() + " field is empty.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            smallestScaleTextField.requestFocus();
            return false;
        }
        double dval = 0;
        try {
            dval = Double.parseDouble(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            smallestScaleTextField.requestFocus();
            return false;
        }
        if (dval < -2 || dval > 2) {
            JOptionPane.showMessageDialog(comp,
                    "Must be a numeric value between -2 and 2.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            smallestScaleTextField.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validateNumberScalesTextField() {
        final Component comp = this;
        String str = numberScalesTextField.getText();
        if (str != null && str.trim().length() == 0) {
            JOptionPane.showMessageDialog(comp,
                    numberScalesLabel.getText() + " field is empty.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            numberScalesTextField.requestFocus();
            return false;
        }
        int ival = 0;
        try {
            ival = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            //numberScalesTextField.setText(String.valueOf(defaultNumberOfScales));
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            numberScalesTextField.requestFocus();
            return false;
        }
        if (ival < 1 || ival > 30) {
            JOptionPane.showMessageDialog(comp,
                    "Must be a numeric value between 1 and 30.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            //numberScalesTextField.setText(String.valueOf(defaultNumberOfScales));
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            numberScalesTextField.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validateLargestScaleTextField() {
        final Component comp = this;
        String str = largestScaleTextField.getText();
        if (str == null || str.trim().length() == 0) {
            JOptionPane.showMessageDialog(comp,
                    largestScaleLabel.getText() + " field is empty.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            largestScaleTextField.requestFocus();
            return false;
        }

        double dval = 0;
        try {
            dval = Double.parseDouble(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            //largestScaleTextField.setText(String.valueOf(defaultLargestScale));
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            largestScaleTextField.requestFocus();
            return false;
        }
        if (dval < -1 || dval > 4) {
            JOptionPane.showMessageDialog(comp,
                    "Must be a numeric value between -1 and 4.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            //largestScaleTextField.setText(String.valueOf(defaultLargestScale));
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            largestScaleTextField.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validateSmoothingLengthTextField() {
        final Component comp = this;
        String str = smoothingLengthTextField.getText();
        if (str != null && str.trim().length() == 0) {
            JOptionPane.showMessageDialog(comp,
                    smoothingLengthLabel.getText() + " field is empty.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            smoothingLengthTextField.requestFocus();
            return false;
        }

        int ival = 0;
        try {
            ival = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            //smoothingLengthTextField.setText(String.valueOf(defaultSmoothLength));
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            smoothingLengthTextField.requestFocus();
            return false;
        }
        if (ival <= 0) {
            JOptionPane.showMessageDialog(comp,
                    "Must be a positive numeric value.", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            //smoothingLengthTextField.setText(String.valueOf(defaultSmoothLength));
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            smoothingLengthTextField.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validateNumberOfPassTextField() {
        final Component comp = this;
        String str = numberOfPassTextField.getText();
        if (str != null && str.trim().length() == 0) {
            JOptionPane.showMessageDialog(comp,
                    numberOfPassLabel.getText() + " field is empty.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            numberOfPassTextField.requestFocus();
            return false;
        }

        int ival = 0;
        try {
            ival = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            //smoothingLengthTextField.setText(String.valueOf(defaultSmoothLength));
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            numberOfPassTextField.requestFocus();
            return false;
        }
        if (ival < 0) {
            JOptionPane.showMessageDialog(comp,
                    "Must be a non-negative numeric value.", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            //smoothingLengthTextField.setText(String.valueOf(defaultSmoothLength));
            specDecompTabbedPane.setSelectedIndex(IN_PARAMS_TAB_INDEX);
            numberOfPassTextField.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validateTimeIncrementTextField() {
        final Component comp = this;
        String str = timeIncrementTextField.getText();
        if (str != null && str.trim().length() == 0) {
            JOptionPane.showMessageDialog(comp,
                    jLabel3.getText() + " field is empty.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
            timeIncrementTextField.requestFocus();
            timeIncrementTextField.setText(String.valueOf(defaultTimeIncrement));
            return false;
        }
        long ival = 0;
        try {
            ival = Long.parseLong(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                    specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
            timeIncrementTextField.requestFocus();
            timeIncrementTextField.setText(String.valueOf(defaultTimeIncrement));
            return false;
        }
        if (ival < 0) {
            JOptionPane.showMessageDialog(comp,
                    "Must be a positive numeric value.", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
            timeIncrementTextField.requestFocus();
            timeIncrementTextField.setText(String.valueOf(defaultTimeIncrement));
            return false;
        }
        return true;
    }

    private void formSUHorizonsPanel(int number) {
        // horizonsPanel = new javax.swing.JPanel();
        horizonsPanel.setBorder(javax.swing.BorderFactory
                .createTitledBorder(""));
        horizonsPanel.setAutoscrolls(true);
        scrollPane = new JScrollPane(horizonsPanel,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        if (number < 1)
            return;

        if(suHorizonCount != number){
            suHorzLabelList = new ArrayList<JLabel>();
            suHorzTextFieldList = new ArrayList<JTextField>();
            suHorzTimeTextFieldList = new ArrayList<JTextField>();
            suChooseHorzButtonList = new ArrayList<JButton>();
            final WaveletDecompGUI comp = this;
            for (int i = 0; i < number; i++) {
                suHorzLabelList.add(new JLabel("Horz " + (i + 1)));
                JButton button = new JButton("Choose...");
                final int count = i;
                button.addActionListener(new ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        selectedHorizonIndex = count;
                        chooseHorizonButtonActionPerformed(evt);
                    }
                });
                suChooseHorzButtonList.add(button);
                JTextField tf = new JTextField();
                /*
                  final Component comp = this;
                  tf.addFocusListener(new FocusAdapter(){
                    public void focusLost(FocusEvent e){
                        String  endStr = endTimeTextField.getText();
                        String startStr = startTimeTextField.getText();
                        if(endStr == null || endStr.trim().length() == 0)
                            return;
                        if(startStr == null || startStr.trim().length() == 0)
                            return;
                        String str = horzTimeTextFieldList.get(count).getText();
                        int ival = -1;
                        try{
                            ival = Integer.parseInt(str);
                        }catch(NumberFormatException ex){
                            JOptionPane.showMessageDialog(comp,"Must be a numeric value.", "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                            horzTimeTextFieldList.get(count).requestFocus();
                            return;
                        }
                        int startTime = Integer.valueOf(startTimeTextField.getText()).intValue();
                        int endTime = Integer.valueOf(endTimeTextField.getText()).intValue();
                        if(ival < startTime || ival > endTime){
                            JOptionPane.showMessageDialog(comp,"Must be a numeric value between start time and end time.", "Invalid data entry",
                            JOptionPane.WARNING_MESSAGE);
                            horzTimeTextFieldList.get(count).requestFocus();
                            return;
                        }
                    }
                    public void focusGained(FocusEvent e){
                        String str = startTimeTextField.getText();
                        if(str.trim().length() == 0){
                            JOptionPane.showMessageDialog(comp,"Please fill in the Start Time field before entering this field.",
                            "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                            startTimeTextField.requestFocus();
                            return;
                        }
                        str = endTimeTextField.getText();
                        if(str.trim().length() == 0){
                            JOptionPane.showMessageDialog(comp,"Please fill in the End Time field before entering this field.",
                            "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                            endTimeTextField.requestFocus(); return;
                        }
                    }
                  });
                 */
                suHorzTextFieldList.add(new JTextField());
                suHorzTimeTextFieldList.add(tf);
            }
        }
        jLabel1.setText("Start Time:");
        jLabel2.setText("End Time:");
        jLabel3.setText("Time Increment (ms):");

        org.jdesktop.layout.GroupLayout horizonsPanelLayout = new org.jdesktop.layout.GroupLayout(
                horizonsPanel);
        horizonsPanel.setLayout(horizonsPanelLayout);
        horizonsPanelLayout.setAutocreateGaps(true);
        horizonsPanelLayout.setAutocreateContainerGaps(true);
        SequentialGroup hGroup = horizonsPanelLayout.createSequentialGroup();
        ParallelGroup p = horizonsPanelLayout.createParallelGroup(
                org.jdesktop.layout.GroupLayout.LEADING, false);

        for (int i = 0; i < suHorzLabelList.size(); i++) {
            p = p.add(suHorzLabelList.get(i));
        }
        hGroup = hGroup.add(p);
        p = horizonsPanelLayout.createParallelGroup();
        for (int i = 0; i < suHorzTextFieldList.size(); i++) {
            p = p.add(suHorzTextFieldList.get(i),
                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 217,
                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE);
        }
        hGroup = hGroup.add(p);

        p = horizonsPanelLayout
                .createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING);
        p = p.add(jLabel1);
        for (int i = 0; i < suChooseHorzButtonList.size(); i++) {
            p = p.add(suChooseHorzButtonList.get(i));
        }
        p = p.add(jLabel2);
        p = p.add(jLabel3);
        hGroup = hGroup.add(p);

        p = horizonsPanelLayout.createParallelGroup();
        p = p.add(suStartTimeTextField);
        for (int i = 0; i < suHorzTimeTextFieldList.size(); i++) {
            p = p.add(suHorzTimeTextFieldList.get(i),
                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 135,
                    Short.MAX_VALUE);
        }
        p = p.add(suEndTimeTextField);

        p = p.add(timeIncrementTextField);
        hGroup = hGroup.add(p);
        horizonsPanelLayout.setHorizontalGroup(hGroup);

        SequentialGroup vGroup = horizonsPanelLayout.createSequentialGroup();
        p = horizonsPanelLayout.createParallelGroup(GroupLayout.BASELINE);
        p.add(jLabel1).add(suStartTimeTextField);
        vGroup = vGroup.add(p);
        for (int i = 0; i < suHorzTextFieldList.size(); i++) {
            p = horizonsPanelLayout.createParallelGroup(GroupLayout.BASELINE);
            p = p.add(suHorzLabelList.get(i)).add(suHorzTextFieldList.get(i)).add(
                    suChooseHorzButtonList.get(i)).add(
                    suHorzTimeTextFieldList.get(i));
            vGroup = vGroup.add(p);
        }
        p = horizonsPanelLayout.createParallelGroup(GroupLayout.BASELINE);
        p.add(jLabel2).add(suEndTimeTextField);
        vGroup = vGroup.add(p);
        p = horizonsPanelLayout.createParallelGroup(GroupLayout.BASELINE);
        p.add(jLabel3).add(timeIncrementTextField);

        vGroup = vGroup.add(p);
        horizonsPanelLayout.setVerticalGroup(vGroup);

    }

    private void formLandmarkHorizonsPanel(int number) {
        // horizonsPanel = new javax.swing.JPanel();
        horizonsPanel.setBorder(javax.swing.BorderFactory
                .createTitledBorder(""));
        horizonsPanel.setAutoscrolls(true);
        scrollPane = new JScrollPane(horizonsPanel,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        if (number < 1)
            return;

        if(landmarkHorizonCount != number){
            landmarkHorzLabelList = new ArrayList<JLabel>();
            landmarkHorzTextFieldList = new ArrayList<JTextField>();
            landmarkHorzTimeTextFieldList = new ArrayList<JTextField>();
            landmarkChooseHorzButtonList = new ArrayList<JButton>();
            final WaveletDecompGUI comp = this;
            for (int i = 0; i < number; i++) {
                landmarkHorzLabelList.add(new JLabel("Horz " + (i + 1)));
                JButton button = new JButton("Choose...");
                final int count = i;
                button.addActionListener(new ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        selectedHorizonIndex = count;
                        int projectType = 0;
                        if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                            if(landmarkInProjectTextField.getText() == null  || landmarkInProjectTextField.getText().trim().length() == 0){
                                JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                    "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                                int tabIdx = specDecompTabbedPane.indexOfTab("Seismic Data Input");
                                specDecompTabbedPane.setSelectedIndex(tabIdx);
                                return;
                            }
                            projectType = landmarkAgent.getLandmarkProjectType(landmarkInProjectTextField.getText().trim());

                        }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                            if(landmarkIn2DProjectTextField.getText() == null  || landmarkIn2DProjectTextField.getText().trim().length() == 0){
                                JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                    "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                                int tabIdx = specDecompTabbedPane.indexOfTab("Seismic Data Input");
                                specDecompTabbedPane.setSelectedIndex(tabIdx);
                                return;
                            }
                            projectType = landmarkAgent.getLandmarkProjectType(landmarkIn2DProjectTextField.getText().trim());
                        }
                        if(projectType < 2 || projectType > 3){
                            JOptionPane.showMessageDialog(comp, "Invalid project type found.",
                                "Invalid Project Type", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = specDecompTabbedPane.indexOfTab("Seismic Data Input");
                            specDecompTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                        if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                            agent.openLandmarkHorizonDialog(landmarkInProjectTextField.getText().trim(), projectType, comp,landmarkHorzTextFieldList.get(count));
                        }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                            agent.openLandmarkHorizonDialog(landmarkIn2DProjectTextField.getText().trim(), projectType, comp,landmarkHorzTextFieldList.get(count));
                        }
                        agent.horizonExportDone = false;
                        //new HorizonSelectDialog(comp,true).setVisible(true);
                    }
            });
            landmarkChooseHorzButtonList.add(button);
            JTextField tf = new JTextField();
            JTextField tf1 = new JTextField();
            tf1.addKeyListener(new KeyAdapter(){
                public void keyPressed(KeyEvent e) {
                    agent.horizonExportDone = false;
                }
            });
                landmarkHorzTextFieldList.add(tf1);
                landmarkHorzTimeTextFieldList.add(tf);
            }

            jLabel1.setText("Start Time:");
            jLabel2.setText("End Time:");
            jLabel3.setText("Sample Rate:");
        }

        float [] timeRange =  null;
        if(landmarkInProjectTextField != null && landmarkInProjectTextField.getText().trim().length() > 0){
            String selectVolume = volumeTextField.getText();
            if(selectVolume != null && selectVolume.trim().length() > 0){
                if(landmarkAgent == null)
                    landmarkAgent = LandmarkServices.getInstance();
                int projectType = landmarkAgent.getLandmarkProjectType(landmarkInProjectTextField.getText().trim());
                if(!(projectType < 2 || projectType > 3) )
                timeRange = landmarkAgent.getLandmarkTimeRangeInfo(landmarkInProjectTextField.getText().trim(), projectType, selectVolume);
            }
        }
        if(timeRange != null && number > 0 && number != landmarkHorizonCount){
            landmarkStartTimeTextField.setText(String.valueOf((int)timeRange[0]));
            landmarkEndTimeTextField.setText(String.valueOf((int)timeRange[1]));
            sampleRateTextField.setText(String.valueOf((int)timeRange[2]));
            int interval = (int)(timeRange[1]-timeRange[0])/(number+1);
            for(int j = 0; j < number; j++){
                int temp = (int)timeRange[0] + (interval*(j+1));
                if((int)timeRange[2] != 0){
                    int rem = temp % (int)timeRange[2];
                    landmarkHorzTimeTextFieldList.get(j).setText(String.valueOf(temp-rem));
                }
            }
        }

        org.jdesktop.layout.GroupLayout horizonsPanelLayout = new org.jdesktop.layout.GroupLayout(
                horizonsPanel);
        horizonsPanel.setLayout(horizonsPanelLayout);
        horizonsPanelLayout.setAutocreateGaps(true);
        horizonsPanelLayout.setAutocreateContainerGaps(true);
        SequentialGroup hGroup = horizonsPanelLayout.createSequentialGroup();
        ParallelGroup p = horizonsPanelLayout.createParallelGroup(
                org.jdesktop.layout.GroupLayout.LEADING, false);

        for (int i = 0; landmarkHorzLabelList != null && i < landmarkHorzLabelList.size(); i++) {
            p = p.add(landmarkHorzLabelList.get(i));
        }

        hGroup = hGroup.add(p);
        p = horizonsPanelLayout.createParallelGroup();
        for (int i = 0; landmarkHorzTextFieldList != null && i < landmarkHorzTextFieldList.size(); i++) {
            p = p.add(landmarkHorzTextFieldList.get(i),
                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 217,
                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE);
        }
        JButton export = new JButton("Export Horizons");
        export.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportHorizonsActionPerformed(evt);
            }
        });
        p = p.add(export);

        hGroup = hGroup.add(p);

        p = horizonsPanelLayout
                .createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING);
        p = p.add(jLabel1);
        for (int i = 0; landmarkChooseHorzButtonList != null && i < landmarkChooseHorzButtonList.size(); i++) {
            p = p.add(landmarkChooseHorzButtonList.get(i));
        }
        p = p.add(jLabel2);
        p = p.add(jLabel3);
        hGroup = hGroup.add(p);

        p = horizonsPanelLayout.createParallelGroup();
        p = p.add(landmarkStartTimeTextField);
        for (int i = 0; landmarkHorzTimeTextFieldList != null && i < landmarkHorzTimeTextFieldList.size(); i++) {
            p = p.add(landmarkHorzTimeTextFieldList.get(i),
                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 135,
                    Short.MAX_VALUE);
        }
        p = p.add(landmarkEndTimeTextField);
        p = p.add(sampleRateTextField);
        hGroup = hGroup.add(p);
        horizonsPanelLayout.setHorizontalGroup(hGroup);

        SequentialGroup vGroup = horizonsPanelLayout.createSequentialGroup();
        p = horizonsPanelLayout.createParallelGroup(GroupLayout.BASELINE);
        p.add(jLabel1).add(landmarkStartTimeTextField);
        vGroup = vGroup.add(p);
        for (int i = 0; i < landmarkHorzTextFieldList.size(); i++) {
            p = horizonsPanelLayout.createParallelGroup(GroupLayout.BASELINE);
            p = p.add(landmarkHorzLabelList.get(i)).add(landmarkHorzTextFieldList.get(i)).add(
                    landmarkChooseHorzButtonList.get(i)).add(
                    landmarkHorzTimeTextFieldList.get(i));
            vGroup = vGroup.add(p);
        }
        p = horizonsPanelLayout.createParallelGroup(GroupLayout.BASELINE);
        p.add(jLabel2).add(landmarkEndTimeTextField);
        vGroup = vGroup.add(p);
        p = horizonsPanelLayout.createParallelGroup(GroupLayout.BASELINE);

        p = p.add(export);

        p.add(jLabel3).add(sampleRateTextField);

        vGroup = vGroup.add(p);
        horizonsPanelLayout.setVerticalGroup(vGroup);

    }

    public boolean horizonAlreadyChosen(String selected){
        if(selected == null || selected.trim().length() == 0)
            return false;
        for(int i = 0; landmarkHorzTextFieldList != null && i < landmarkHorzTextFieldList.size(); i++){
            if(landmarkHorzTextFieldList.get(i).getText().trim().equals(selected))
                return true;
        }
        return false;
    }

    //Inner class for traversal policy A
    class TravPolicyA  extends ContainerOrderFocusTraversalPolicy{
/*
      public Component getDefaultComponent(Container focusCycleRoot){
        return horizonsComboBox;
      }//end getDefaultComponent
      //---------------------------------------//

      public Component getFirstComponent(Container focusCycleRoot){
          if(horzTimeTextFieldList != null && horzTimeTextFieldList.size() > 0)
              return startTimeTextField;
          else
              return horizonsComboBox;
      }//end getFirstComponent
      //---------------------------------------//

      public Component getLastComponent(Container focusCycleRoot){
          if(horzTimeTextFieldList != null && horzTimeTextFieldList.size() > 0)
              return timeIncrementTextField;
          else
              return horizonsComboBox;
      }//end getLastComponent
      //---------------------------------------//

      public Component getComponentAfter(Container focusCycleRoot,Component aComponent){
          if(aComponent == startTimeTextField){
              return horzTimeTextFieldList.get(0);
          }
          for(int i = 0; horzTimeTextFieldList != null && i < horzTimeTextFieldList.size(); i++){
              if(i+1 < horzTimeTextFieldList.size()){
                  if(aComponent == horzTimeTextFieldList.get(i)){
                      return horzTimeTextFieldList.get(i+1);
                  }
                  if(i == horzTimeTextFieldList.size()-1){
                      if(aComponent == horzTimeTextFieldList.get(horzTimeTextFieldList.size()-1))
                          return endTimeTextField;
                  }
              }
          }

         if(aComponent == endTimeTextField)
             return timeIncrementTextField;
         return horizonsComboBox;
      }//end getComponentAfter
      //---------------------------------------//

      public Component getComponentBefore(Container focusCycleRoot, Component aComponent){
        if(aComponent == timeIncrementTextField){
          return endTimeTextField;
        }else if(aComponent == endTimeTextField)
          return horzTimeTextFieldList.get(horzTimeTextFieldList.size()-1);

        if(horzTimeTextFieldList != null && horzTimeTextFieldList.size() > 0){
            for(int i = (horzTimeTextFieldList.size()-1) ; i >=0; i--){
                if(aComponent == horzTimeTextFieldList.get(i)){
                    if(i > 0)
                        return horzTimeTextFieldList.get(i-1);
                    else
                        return startTimeTextField;
                }
            }
        }
        return horizonsComboBox;//make compiler happy
      }//end getComponentBefore
      */
      protected boolean accept(Component aComponent){
          for(int i = 0; suHorzTextFieldList != null && i < suHorzTextFieldList.size(); i++){
              if(aComponent == suHorzTextFieldList.get(i) || aComponent == suChooseHorzButtonList.get(i))
                  return false;
          }
          return true;
      }
    }//end TravPolicyA

    private void formLandmarkOutputTimePanel(int number) {
        timePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        //timePanel.setSize(400, 250);
        timePanel.setAutoscrolls(true);
        timeScrollPane = new JScrollPane(timePanel,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        if (number < 1)
            return;

        if(landmarkOutputTimeCount != number){
            landmarkOutputTimeTextFieldList = new ArrayList<JTextField>();
            landmarkOutputTimeLabelList = new ArrayList<JLabel>();

            for (int i = 0; i < number; i++) {
                landmarkOutputTimeLabelList.add(new JLabel("Time " + (i + 1)));
                landmarkOutputTimeTextFieldList.add(new JTextField());
            }
        }

        org.jdesktop.layout.GroupLayout timePanelLayout = new org.jdesktop.layout.GroupLayout(
                timePanel);
        timePanel.setLayout(timePanelLayout);
        timePanelLayout.setAutocreateGaps(true);
        timePanelLayout.setAutocreateContainerGaps(true);
        SequentialGroup hGroup = timePanelLayout.createSequentialGroup();
        ParallelGroup p = timePanelLayout.createParallelGroup(
                org.jdesktop.layout.GroupLayout.LEADING, false);

        for (int i = 0; landmarkOutputTimeLabelList != null && i < landmarkOutputTimeLabelList.size(); i++) {
            p = p.add(landmarkOutputTimeLabelList.get(i));
        }
        hGroup = hGroup.add(p);
        p = timePanelLayout.createParallelGroup();
        for (int i = 0; landmarkOutputTimeTextFieldList != null && i < landmarkOutputTimeTextFieldList.size(); i++) {
            p = p.add(landmarkOutputTimeTextFieldList.get(i),
                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 217,
                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE);
        }
        hGroup = hGroup.add(p);

        p = timePanelLayout
                .createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING);
        hGroup = hGroup.add(p);

        timePanelLayout.setHorizontalGroup(hGroup);

        SequentialGroup vGroup = timePanelLayout.createSequentialGroup();

        for (int i = 0; i < landmarkOutputTimeTextFieldList.size(); i++) {
            p = timePanelLayout.createParallelGroup(GroupLayout.BASELINE);
            p = p.add(landmarkOutputTimeLabelList.get(i)).add(landmarkOutputTimeTextFieldList.get(i));
            vGroup = vGroup.add(p);
        }

        timePanelLayout.setVerticalGroup(vGroup);

    }

    private void formLandmarkNumScaleOutputPanel(int number,
            List<JTextField> scaleTextFieldList,List<JLabel> scaleLabelList) {

        scalePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        //scalePanel.setAutoscrolls(true);
        scaleScrollPane = new JScrollPane(scalePanel,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        if (number < 1)
            return;
/*
        if(count != number){
            scaleTextFieldList = new ArrayList<JTextField>();
            scaleLabelList = new ArrayList<JLabel>();

            for (int i = 0; i < number; i++) {
                scaleLabelList.add(new JLabel("Scale " + (i + 1)));
                scaleTextFieldList.add(new JTextField());
            }
        }
*/
        org.jdesktop.layout.GroupLayout panelLayout = new org.jdesktop.layout.GroupLayout(
                scalePanel);
        scalePanel.setLayout(panelLayout);
        panelLayout.setAutocreateGaps(true);
        panelLayout.setAutocreateContainerGaps(true);
        SequentialGroup hGroup = panelLayout.createSequentialGroup();
        ParallelGroup p = panelLayout.createParallelGroup(
                org.jdesktop.layout.GroupLayout.LEADING, false);

        for (int i = 0; scaleLabelList != null && i < scaleLabelList.size(); i++) {
            p = p.add(scaleLabelList.get(i));
        }
        hGroup = hGroup.add(p);
        p = panelLayout.createParallelGroup();
        for (int i = 0; scaleTextFieldList != null && i < scaleTextFieldList.size(); i++) {
            p = p.add(scaleTextFieldList.get(i),
                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 80,
                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE);
        }
        hGroup = hGroup.add(p);

        p = panelLayout
                .createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING);
        hGroup = hGroup.add(p);

        panelLayout.setHorizontalGroup(hGroup);

        SequentialGroup vGroup = panelLayout.createSequentialGroup();

        for (int i = 0; scaleTextFieldList != null && i  < scaleTextFieldList.size(); i++) {
            p = panelLayout.createParallelGroup(GroupLayout.BASELINE);
            p = p.add(scaleLabelList.get(i)).add(scaleTextFieldList.get(i));
            vGroup = vGroup.add(p);
        }

        panelLayout.setVerticalGroup(vGroup);

    }

    void formLandmarkScaleOutputPanel(javax.swing.JTextField nameBaseTextField,javax.swing.JTextField projectTextField,
             javax.swing.JTextField noiseFactorTextField, javax.swing.JComboBox numScaleComboBox,
             javax.swing.JRadioButton normalizedRadioButton, javax.swing.JRadioButton rawRadioButton){

        landmarkOutPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        landmarkProjectPanel = new javax.swing.JPanel();
        landmarkProjectPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Landmark Project"));

        org.jdesktop.layout.GroupLayout landmarkProjectPanelLayout = new org.jdesktop.layout.GroupLayout(landmarkProjectPanel);
        landmarkProjectPanel.setLayout(landmarkProjectPanelLayout);
        landmarkProjectPanelLayout.setHorizontalGroup(
            landmarkProjectPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(landmarkProjectPanelLayout.createSequentialGroup()
                    .add(landmarkOutProjectLabel)
                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                    .add(projectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 186, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                    .add(landmarkOutListButton)
                    .addContainerGap(18, Short.MAX_VALUE))
            );
        landmarkProjectPanelLayout.setVerticalGroup(
            landmarkProjectPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(landmarkProjectPanelLayout.createSequentialGroup()
                    .addContainerGap()
                    .add(landmarkProjectPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkOutProjectLabel)
                    .add(projectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(landmarkOutListButton))
                    .addContainerGap(36, Short.MAX_VALUE))
            );

        scaleScrollPane = new JScrollPane(scalePanel,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);


        //landmarkNumOfScalesComboBox.setSelectedIndex(landmarkOutNumScalesCount);
        landmarkOutPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        org.jdesktop.layout.GroupLayout landmarkOutPanelLayout = new org.jdesktop.layout.GroupLayout(landmarkOutPanel);
        landmarkOutPanel.setLayout(landmarkOutPanelLayout);
        landmarkOutPanelLayout.setHorizontalGroup(
            landmarkOutPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkOutPanelLayout.createSequentialGroup()
                .add(landmarkOutPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(landmarkOutPanelLayout.createSequentialGroup()
                        .add(scaleScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 190, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkProjectPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))                    .add(landmarkOutPanelLayout.createSequentialGroup()
                        .add(landmarkOutPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(landmarkScaleVolumesLabel)
                            .add(landmarkNameBaseLabel)
                            .add(landmarkNumScaleLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkOutPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(numScaleComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(landmarkOutPanelLayout.createSequentialGroup()
                                .add(normalizedRadioButton)
                                .add(20, 20, 20)
                                .add(noiseFactorTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 55, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(landmarkNoiseFactorLabel))
                            .add(rawRadioButton)
                            .add(nameBaseTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 389, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                //.addContainerGap()
        ));
        landmarkOutPanelLayout.setVerticalGroup(
            landmarkOutPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkOutPanelLayout.createSequentialGroup()
                .add(landmarkOutPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(nameBaseTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(landmarkNameBaseLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkOutPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkScaleVolumesLabel)
                    .add(rawRadioButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkOutPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(normalizedRadioButton)
                    .add(noiseFactorTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(landmarkNoiseFactorLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(landmarkOutPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkNumScaleLabel)
                    .add(numScaleComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(12, 12, 12)

                .add(landmarkOutPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, landmarkProjectPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, scaleScrollPane,org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE))
                    .addContainerGap())
                );
    }


    void formOutputFlattenedPanel(){
        org.jdesktop.layout.GroupLayout flattenedPanelLayout = new org.jdesktop.layout.GroupLayout(flattenedPanel);
        flattenedPanel.setLayout(flattenedPanelLayout);
        flattenedPanelLayout.setHorizontalGroup(
            flattenedPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(flattenedPanelLayout.createSequentialGroup()
               .add(flattenedPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(flattenedPanelLayout.createSequentialGroup()
                        .add(flattenedPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(horizonVolumesLabel)
                            .add(placementLabel))
                        .add(12, 12, 12)
                        .add(flattenedPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(timeComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(horizonNormalizedRadioButton)
                            .add(horizonRawRadioButton)))
                    .add(timeScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 450))
               .addContainerGap())
        );
        flattenedPanelLayout.setVerticalGroup(
            flattenedPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(flattenedPanelLayout.createSequentialGroup()
                .add(flattenedPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(horizonVolumesLabel)
                    .add(horizonRawRadioButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(horizonNormalizedRadioButton)
                .add(7, 7, 7)
                .add(flattenedPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(placementLabel)
                    .add(timeComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(12, 12, 12)
                .add(timeScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 200)
                .addContainerGap()
        ));


    }

    private void formFlatteningPanel() {
        org.jdesktop.layout.GroupLayout flatteningPanelLayout = new org.jdesktop.layout.GroupLayout(flatteningPanel);
        flatteningPanel.setLayout(flatteningPanelLayout);
        flatteningPanelLayout.setHorizontalGroup(
                flatteningPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(flatteningPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(flatteningPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(scrollPane,
                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                        Short.MAX_VALUE)
                    .add(flatteningPanelLayout.createSequentialGroup()
                        .add(horizonsLabel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(horizonsComboBox,
                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                            91,
                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(71,71,71)
                        .add(flatteningPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(listRadioButton)
                            .add(viewerRadioButton))))
                .addContainerGap()));
        flatteningPanelLayout.setVerticalGroup(
                flatteningPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(flatteningPanelLayout.createSequentialGroup()
                    .add(flatteningPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(flatteningPanelLayout.createSequentialGroup()
                            .addContainerGap()
                            .add(flatteningPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(horizonsLabel)
                                .add(horizonsComboBox,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                            .add(flatteningPanelLayout.createSequentialGroup()
                                .add(listRadioButton,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                    19,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(viewerRadioButton)))
                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(scrollPane,
                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                    Short.MAX_VALUE)
                .addContainerGap()));

    }

    private void buildflatteningTab(int number) {
        /*
         * horizonsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
         * horizonsPanel.setAutoscrolls(true);
         */

        horizonsPanel = new javax.swing.JPanel();
        flatteningPanel = new javax.swing.JPanel();
        if(seismicFormatIn == LANDMARK_FORMAT){
            formLandmarkHorizonsPanel(number);
            if(number > 0)
                landmarkHorizonCount = number;
        }else{
            formSUHorizonsPanel(number);
            if(number > 0)
                suHorizonCount = number;

        }
        formFlatteningPanel();
        //final FocusTraversalPolicy policy = new TravPolicyA();
        //horizonsPanel.setFocusTraversalPolicy(policy);
        //flatteningPanel.setFocusTraversalPolicy(policy);
        int tabIdx = specDecompTabbedPane
                .indexOfTab("Stratigraphic Flattening");

        if (tabIdx == -1)
            specDecompTabbedPane.addTab("Stratigraphic Flattening",
                    flatteningPanel);
        else
            specDecompTabbedPane.setComponentAt(tabIdx, flatteningPanel);

    }

    private void buildflatteningTab1(int number) {
        /*
         * horizonsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
         * horizonsPanel.setAutoscrolls(true);
         */

        bhpsuHorizonPanel = new javax.swing.JPanel();
        landmarkHorizonPanel = new javax.swing.JPanel();
        horizonTopPanel = new javax.swing.JPanel();
        if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            logger.info("bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()");
            formBhpsuHorizonPanel(number);
            bhpsuHorizonCount = number;
        }else if(landmarkHorizonRadioButton.isSelected()){
            logger.info("landmarkHorizonRadioButton.isSelected()");
            formLandmarkHorizonPanel(number);
            landmarkHorizonCount = number;
        }
        //final FocusTraversalPolicy policy = new TravPolicyA();
        //horizonsPanel.setFocusTraversalPolicy(policy);
        //flatteningPanel.setFocusTraversalPolicy(policy);
        formHorizonTopPanel();
        int tabIdx = specDecompTabbedPane
                .indexOfTab("Stratigraphic Flattening");

        if (tabIdx == -1)
            specDecompTabbedPane.addTab("Stratigraphic Flattening",
                    horizonTopPanel);
        else
            specDecompTabbedPane.setComponentAt(tabIdx, horizonTopPanel);

    }

    private void landmarkOutRadioButtonActionPerformed(
            java.awt.event.ActionEvent evt) {

        if(landmarkOutRadioButton.isSelected()){
            if(validateLandmarkEnvironment() == false){
                bhpsuOutRadioButton.setSelected(true);
                return;
            }
            try{
                landmarkAgent = LandmarkServices.getInstance();
            }catch(UnsatisfiedLinkError e){
                e.printStackTrace();
                JOptionPane.showMessageDialog(gui,e.getMessage() + ". Contact workbench support.",
                        "Linking Error",JOptionPane.WARNING_MESSAGE);
                bhpsuOutRadioButton.setSelected(true);
                return;
            }catch(NoClassDefFoundError e){
                e.printStackTrace();
                JOptionPane.showMessageDialog(gui,"Problem in getting the associated library for handling Landmark projects. Contact workbench support.",
                        "Linking Error",JOptionPane.WARNING_MESSAGE);
                bhpsuOutRadioButton.setSelected(true);
                return;
            }catch(Exception e){
                e.printStackTrace();
                JOptionPane.showMessageDialog(gui,"Problem in getting the associated library for handling Landmark projects. Contact workbench support.",
                        "Linking Error",JOptionPane.WARNING_MESSAGE);
                bhpsuOutRadioButton.setSelected(true);
                return;
            }
        }
        seismicFormatOut = LANDMARK_FORMAT;

        int idx = specDecompTabbedPane.getSelectedIndex();
        // check if Seismic Data Input tab selected
        if (idx == INPUT_TAB_INDEX) {
            switch2LandmarkIn();
        }

        // check if Outputs tab selected
        if (idx == OUTPUTS_TAB_INDEX) {
            if(bhpsuInRadioButton.isSelected())
                switch2LandmarkOut(landmarkOutputTimeCount,landmarkOutNumScalesCount,1);
            else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected())
                switch2LandmarkOut(landmarkOutputTimeCount,landmark2DOutNumScalesCount,2);
            else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected())
                switch2LandmarkOut(landmarkOutputTimeCount,landmark3DOutNumScalesCount,3);
        }
        //timeComboBox.setSelectedIndex(landmarkOutputTimeCount);
    }

    private boolean validateLandmarkEnvironment(){
        String os = System.getProperty("os.name");
        if(os.startsWith("Windows")){
            JOptionPane.showMessageDialog(this, "Landmark services are not currently supported in Windows environment.",
                    "Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
            bhpsuInRadioButton.setSelected(true);
            return false;
        }
        Map envmap = System.getenv();

        if(!envmap.containsKey("OWHOME")){
            JOptionPane.showMessageDialog(this, "Environment variable OWHOME must be set prior to using Landmark services.",
                    "Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
            bhpsuInRadioButton.setSelected(true);
            return false;
        }
        if(!envmap.containsKey("ORACLE_HOME")){
            JOptionPane.showMessageDialog(this, "Environment variable ORACLE_HOME must be set prior to using Landmark services.",
                    "Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
            bhpsuInRadioButton.setSelected(true);
            return false;
        }
        if(!envmap.containsKey("OW_PMPATH")){
            JOptionPane.showMessageDialog(this, "Environment variable OW_PMPATH must be set prior to using Landmark services.",
                    "Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
            bhpsuInRadioButton.setSelected(true);
            return false;
        }
        if(!envmap.containsKey("LM_LICENSE_FILE")){
            JOptionPane.showMessageDialog(this, "Environment variable LM_LICENSE_FILE must be set prior to using Landmark services.",
                    "Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
            bhpsuInRadioButton.setSelected(true);
            return false;
        }
        if(!envmap.containsKey("LD_LIBRARY_PATH")){
            JOptionPane.showMessageDialog(this, "Environment variable LD_LIBRARY_PATH must be set prior to using Landmark services.",
                    "Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
            bhpsuInRadioButton.setSelected(true);
            return false;
        }/*else{
            String value = (String)envmap.get("LD_LIBRARY_PATH");
            String ldpath = WaveletDecompConstants.JNI_LIBS_LOCATION;
            if(!value.contains(ldpath)){
                JOptionPane.showMessageDialog(this,"Environment variable LD_LIBRARY_PATH must be set to include "
                        + ldpath + " prior to using Landmark services.",
                        "Environment Variable Not Properly Set", JOptionPane.WARNING_MESSAGE);
                bhpsuInRadioButton.setSelected(true);
                return false;
            }
        }
        File lib = new File(WaveletDecompConstants.JNI_LIBS_LOCATION + "/lib"
                + WaveletDecompConstants.LANDMARK_JNI_LIB_NAME + ".so");
        if(!lib.exists()){
            JOptionPane.showMessageDialog(this,"Can not find the library file: lib" + WaveletDecompConstants.LANDMARK_JNI_LIB_NAME + ".so from "
                    + WaveletDecompConstants.JNI_LIBS_LOCATION + " required before using Landmark services.",
                    "Required Library Not Found", JOptionPane.WARNING_MESSAGE);
            bhpsuInRadioButton.setSelected(true);
            return false;
        }
        */
        if(landmarkAgent == null){
           landmarkAgent = LandmarkServices.getInstance();
        }
        return true;
    }

    private void landmarkInRadioButtonActionPerformed(
            java.awt.event.ActionEvent evt) {

        if(validateLandmarkEnvironment() == false){
            bhpsuInRadioButton.setSelected(true);
            return;
        }

        if(landmarkInRadioButton.isSelected()){
            try{
                landmarkAgent = LandmarkServices.getInstance();
            }catch(UnsatisfiedLinkError e){
                e.printStackTrace();
                JOptionPane.showMessageDialog(gui,e.getMessage() + ". Contact workbench support.",
                        "Linking Error",JOptionPane.WARNING_MESSAGE);
                bhpsuInRadioButton.setSelected(true);
                return;
            }catch(NoClassDefFoundError e){
                e.printStackTrace();
                JOptionPane.showMessageDialog(gui,"Problem in getting the associated library for handling Landmark projects. Contact workbench support.",
                        "Linking Error",JOptionPane.WARNING_MESSAGE);
                bhpsuInRadioButton.setSelected(true);
                return;
            }catch(Exception e){
                e.printStackTrace();
                JOptionPane.showMessageDialog(gui,"Problem in getting the associated library for handling Landmark projects. Contact workbench support.",
                        "Linking Error",JOptionPane.WARNING_MESSAGE);
                bhpsuInRadioButton.setSelected(true);
                return;
            }
        }
        //if(agent.getLandmarkAllProjects() == null){
        //  if(!agent.populateLandmarkProjects()){
        //      bhpsuInRadioButton.setSelected(true);
        //      return;
        //  }
        //}

        seismicFormatIn = LANDMARK_FORMAT;
        int idx = specDecompTabbedPane.getSelectedIndex();
        // check if Seismic Data Input tab selected
        if (idx == INPUT_TAB_INDEX) {
            switch2LandmarkIn();
        }

        // check if Outputs tab selected
        //if (idx == OUTPUTS_TAB_INDEX) {
        //  switch2LandmarkOut(landmarkOutputTimeCount,landmarkOutNumScalesCount);
        //}
        horizonsComboBox.setSelectedIndex(landmarkHorizonCount);
        //formLandmarkHorizonsPanel(landmarkHorizonCount);

    }


    private void bhpsuInRadioButtonActionPerformed(
            java.awt.event.ActionEvent evt) {
        seismicFormatIn = BHPSU_FORMAT;
        int idx = specDecompTabbedPane.getSelectedIndex();
        // check if Seismic Data Input tab selected
        if (idx == INPUT_TAB_INDEX) {
            switch2BhpsuIn();
        }

        // check if Outputs tab selected
        if (idx == OUTPUTS_TAB_INDEX) {
            switch2BhpsuOut();
        }
        horizonsComboBox.setSelectedIndex(suHorizonCount);
        //formSUHorizonsPanel(suHorizonCount);
    }

    private void bhpsuOutRadioButtonActionPerformed(
            java.awt.event.ActionEvent evt) {
        seismicFormatOut = BHPSU_FORMAT;
        int idx = specDecompTabbedPane.getSelectedIndex();
        // check if Seismic Data Input tab selected
        if (idx == INPUT_TAB_INDEX) {
            switch2BhpsuIn();
        }

        // check if Outputs tab selected
        if (idx == OUTPUTS_TAB_INDEX) {
            switch2BhpsuOut();
        }
    }

    /**
     * Generate file filter for use by file chooser service
     *
     * @param filterList
     *            extensions to use
     * @return filter
     */
    private FileFilter getFileFilter(String[] filterList) {
        FileFilter filter = (FileFilter) new GenericFileFilter(filterList,
                "Data File");
        return filter;
    }


    private void runScriptActionPerformed(java.awt.event.ActionEvent evt) {

        String path = writeScript();
        if (path == null)
            return;
        //currentRunningScriptPrefix = path;
        String scriptFile = path + ".sh";

        String stdOutFile = path + ".out";
        String stdErrFile = path + ".err";
        ArrayList params = new ArrayList();
        params.add("cd " + defaultPreferredScriptDir + "; chmod u+x "
                + scriptFile + "; sh " + scriptFile + " 1>" + stdOutFile + " 2>" + stdErrFile);
        params.add("false");
        //+ scriptFile + "; " + scriptFile + " 1>" + stdOutFile + " 2>" + stdErrFile + "; echo exit_code=$? 1>>" + stdOutFile);
        //params.add("cd " + defaultPreferredScriptDir + "; chmod u+x "
        //      + scriptFile + "; (" + scriptFile + " | tee " + stdOutFile + ") 3>&1 1>&2 2>&3 | tee " + stdErrFile);
        //params.add("cd " + defaultPreferredScriptDir + "; chmod u+x "
        //      + scriptFile + "; (((" + scriptFile + " | tee " + stdOutFile + ") 3>&1 1>&2 2>&3 | tee " + stdErrFile + ") 3>&1 1>&2 2>&3) 1>" + stdOutFile + " 2>" + stdErrFile);
        stateLabel.setText("Running");
        summaryTextArea.setText("");
        stdErrTextArea.setText("");
        stdOutTextArea.setText("");

        agent.submitJob(params);
        job_status = WaveletDecompConstants.JOB_RUNNING_STATUS;
        runMenuItem.setEnabled(false);
        runScriptButton.setEnabled(false);
		
        displayResultsMenuItem.setEnabled(false);
		displayResultsButton.setEnabled(false);

//TODO will be used upon request based on menu input from user.
/*
        Runnable heavyRunnable = new Runnable(){
            public void run(){
                while(job_status != WaveletDecompConstants.JOB_ENDED_STATUS){
                    try{
                        Thread.sleep(5000*60);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                    updateStatus();
                }
                return;
            }
        };
        new Thread(heavyRunnable).start();
*/
    }

    private boolean pass = false;
    private String getScriptTemplate(){
        Class me = WaveletDecompGUI.class;
        InputStream in;
        StringBuffer scriptTemp = new StringBuffer();
        if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected())
            in = me.getResourceAsStream("/script_template_2d.txt");
        else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected())
            in = me.getResourceAsStream("/script_template_3d.txt");
        else
            in = me.getResourceAsStream("/script_template.txt");
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        try {
            String inputLine = "";
            while ((inputLine = reader.readLine()) != null) {
                scriptTemp.append(inputLine + "\n");
            }
        } catch (IOException ioe) {
            logger.warning("IO exception reading script_template.txt file:"
                + ioe.getMessage());
        } finally {
            try {
                if (in != null)
                    in.close();
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
            }
        }
        return scriptTemp.toString();

    }
    private String writeScript(){
        //String scriptTemp = getScriptTemplate();
        //String scripts = formSUScripts(scriptTemp);
        List<String> scripts = formDynamicSUScripts();
        if(scripts == null)
            return null;

        //logger.info(scripts);
        List<String> list = agent.getUnmadeQiProjDescDirs();
        String projectRoot = QiProjectDescUtils.getProjectPath(agent.getProjectDescriptor());

        if(list != null && list.size() > 0){
            int returnStatus = QiProjectDescUtils.showQiProjectMetaDataSetupDialog(
                    JOptionPane.getFrameForComponent(this), true, list, projectRoot);
            if(returnStatus == JOptionPane.CANCEL_OPTION)
                return null;
            if(list.size() > 0){
                final List<String> flist = list;
                Runnable heavyRunnable = new Runnable(){
                    public void run(){
                        if(agent.makeQiProjDescDirs(flist))
                            pass = true;
                        else
                            pass = false;
                    }
                };
                Thread thread = new Thread(heavyRunnable);
                thread.start();
                try{
                    thread.join();
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
                if(!pass){
                    logger.info("Creating QiProject directory structure not passed.");
                    return null;
                }
                logger.info("Creating QiProject directory structure succeeds.");
            }
        }

        /*
        String result = agent.checkDirExist(defaultPreferredScriptDir);
        logger.info("check if the " + defaultPreferredScriptDir + " exists: " + result);

        boolean ok = false;

        if (result.equals("yes"))
            ok = true;
        else if (result.equals("no")) {
            int status = JOptionPane
                    .showConfirmDialog(
                            this,
                            "The script will be written into "
                                    + defaultPreferredScriptDir
                                    + " but the directory does not exist. Would you like the application to create it for you?",
                            "Confirm action", JOptionPane.YES_NO_OPTION);
            if (status == JOptionPane.YES_OPTION) {
                result = agent.mkDir(defaultPreferredScriptDir);
                if (result.equals("success"))
                    ok = true;
                logger.info("Create directory " + defaultPreferredScriptDir + " returning " + result);
            } else
                return null;
        }

        // String fileName = String.valueOf(System.currentTimeMillis());
        logger.info("checkDirExist " + defaultPreferredScriptDir + " ok=" + ok);
        */
        ///if (ok){
        defaultPreferredScriptDir = QiProjectDescUtils.getTempPath(agent.getProjectDescriptor());
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        String baseName = "";
        if(landmarkOutRadioButton.isSelected()){
            if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected())
                baseName = landmark2DNameBaseTextField.getText().trim();
            else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected())
                baseName = landmark3DNameBaseTextField.getText().trim();
            else
                baseName = landmarkNameBaseTextField.getText().trim();
        }else if(bhpsuOutRadioButton.isSelected())
            baseName = bhpsuNameBaseTextField.getText().trim();
        int ind = baseName.lastIndexOf(filesep);
        if(ind != -1){
            baseName = baseName.substring(ind+1);
        }
        String fileNamePrefix = baseName + "_" + formatter.format(ts);
        currentRunningScriptPrefix = fileNamePrefix;
        String fileName = fileNamePrefix + ".sh";
        boolean success = agent.writeScripts(defaultPreferredScriptDir + filesep
                + fileName, scripts);
        if(success){
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    String text = currentRunningScriptPrefix + ".sh\n";
                    text += "has been created in " + defaultPreferredScriptDir;
                    summaryTextArea.setText(text);
                }
                }
            );
            return defaultPreferredScriptDir + filesep  + fileNamePrefix;
        }else
            return null;
        ///}else
        ///    return null;
    }

    private void cancelJobButtonActionPerformed(java.awt.event.ActionEvent evt) {
        //ArrayList params = new ArrayList();
        //params.add("kill -9 `ps ux | awk '/" + currentRunningScriptPrefix + ".sh/ && !/awk/ {print $2}'`");
        //agent.submitJob(params);

        //agent.cancelJob();
        //stateLabel.setText("Job canceled");
        if(job_status == WaveletDecompConstants.JOB_RUNNING_STATUS || job_status == WaveletDecompConstants.JOB_UNKNOWN_STATUS){
            String pid = agent.getProcessId(defaultPreferredScriptDir + filesep +currentRunningScriptPrefix + ".sh");
            if(pid == null || pid.length() == 0){
                JOptionPane.showMessageDialog(this, "No such a process running", "Error In Cancel Job",
                        JOptionPane.WARNING_MESSAGE);
                return;
            }

            if(agent.cancelJob(currentRunningScriptPrefix + ".sh")){
                Runnable heavyRunnable = new Runnable(){
                public void run(){
                    updateStatus();                 }
            };
            new Thread(heavyRunnable).start();
            }

        }else{
            JOptionPane.showMessageDialog(this, "No active job found to be canelled.",
                    "No Active Job Found", JOptionPane.WARNING_MESSAGE);
            //updateButton.doClick();
        }
    }
    private void writeScriptActionPerformed(java.awt.event.ActionEvent evt) {
        writeScript();
    }

    private boolean validateLandmarkOutput(){

        if(!landmarkOutRadioButton.isSelected())
            return true;
        if(bhpsuInRadioButton.isSelected()){
            return validateLandmarkOutput(landmarkOutProjectTextField,null,landmarkNameBaseTextField,
                landmarkScaleNormalizedRadioButton,landmarkNoiseFactorTextField,
                landmarkOutputScaleTextFieldList,landmarkNumOfScalesComboBox,
                landmarkOutputScaleLabelList);
        }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
            return validateLandmarkOutput(landmark2DOutProjectTextField,landmarkIn2DProjectTextField,
                    landmark2DNameBaseTextField,
                    landmark2DScaleNormalizedRadioButton,landmark2DNoiseFactorTextField,
                    landmark2DOutputScaleTextFieldList,landmark2DNumOfScalesComboBox,
                    landmark2DOutputScaleLabelList);
        }else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
            
            if(validateLandmarkOutput(landmark3DOutProjectTextField,landmarkInProjectTextField,
                    landmark3DNameBaseTextField,
                    landmark3DScaleNormalizedRadioButton,landmark3DNoiseFactorTextField,
                    landmark3DOutputScaleTextFieldList,landmark3DNumOfScalesComboBox,
                    landmark3DOutputScaleLabelList))
                return validateLandmark3DOutputFileNameSize();
            else
                return false;
        }
        return false;
    }

    public final int LANDMARK_3D_BRICK_FILENAME_SIZE_LIMIT = 34;
    private boolean validateLandmark3DOutputFileNameSize(){
        String baseName = landmark3DNameBaseTextField.getText().trim();
        baseName += "_cwt"; //output for running spectral decomposition
        if(landmark3DScaleNormalizedRadioButton.isSelected()){
            baseName += "_norm"; //running normalization
        }
        if(landmarkHorzTextFieldList.size() > 0){ //running slice
            baseName += "_slice_transpose_ep";
        }

        int[] arrScales = new int[landmark3DOutputScaleTextFieldList.size()];
        for (int i = 0; i < landmark3DOutputScaleTextFieldList.size(); i++) {
             String s = landmark3DOutputScaleTextFieldList.get(i).getText();
             int val = Integer.valueOf(s).intValue();
             arrScales[i] = val;
        }
        Arrays.sort(arrScales);
        baseName += "_" + arrScales[arrScales.length-1];
        if(baseName.length() > LANDMARK_3D_BRICK_FILENAME_SIZE_LIMIT){
            JOptionPane.showMessageDialog(this, "The length of the output file name is beyond the limit " 
                    + LANDMARK_3D_BRICK_FILENAME_SIZE_LIMIT + 
                    " set by Landmark 3D Brick format. Please try to reduce the length of the ouput basename.",
             "File Name Length Exceeds Limit", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
            landmark3DNameBaseTextField.requestFocus();;
            return false;
        }
        return true;

    }

    private boolean validateLandmarkOutput(JTextField projectOutTextField,JTextField projectInTextField,
            JTextField baseTextField, JRadioButton normalizedRadioButton, JTextField noiseFactorTextField,
            List<JTextField>scaleTextFieldList, JComboBox scalesComboBox, List<JLabel>scaleLabelList ){
        final Component comp = this;
        if(projectOutTextField == null || projectOutTextField.getText().trim().length() == 0){
            JOptionPane.showMessageDialog(comp, "Must specify a Landmark project for the output volume.", "Landmark project is missing",    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
            landmarkOutListButton.requestFocus();
            return false;
        }else{
            if(landmarkInRadioButton.isSelected()){
                if(!projectInTextField.getText().trim().equals(projectOutTextField.getText().trim())){
                    JOptionPane.showMessageDialog(comp, "Projects mismatch between the input and the output.", "Landmark project mismatches",   JOptionPane.WARNING_MESSAGE);
                    specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
                    landmarkOutListButton.requestFocus();
                    return false;
                }
            }


        }

        String text = baseTextField.getText().trim();
        if (text == null || text.trim().length() == 0) {
            JOptionPane.showMessageDialog(comp, "The " + landmarkNameBaseLabel.getText()
                    + " field is empty.", "Missing data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
            baseTextField.requestFocus();
            return false;
        }
        if(text.endsWith(filesep)){
            JOptionPane.showMessageDialog(comp, "The " + landmarkNameBaseLabel.getText()
                    + " field can not end with " + filesep, "Error in data entry",  JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
            baseTextField.requestFocus();
            return false;
        }
        if(landmark2DRadioButton.isSelected()){
            int index = -1;
            String temp = "";
            if((index = text.lastIndexOf(filesep)) != -1 )
                temp = text.substring(index+1, text.length());
            else
                temp = text;
            if(temp.length() < 6){
                int action = JOptionPane.showConfirmDialog(comp,"The size of the name base " + "\"" + temp + "\"" +
                        " is less than 6 characters. \nIt will be padded by 0 up to 6 characters to form Landmark 2D process level and version. \nDo you want to proceed?",
                        "Action Confirmation",JOptionPane.YES_NO_OPTION);
                if(action == JOptionPane.NO_OPTION ){
                    return false;
                }
            }
            if(temp.length() > 6){
                int action = JOptionPane.showConfirmDialog(comp,"The size of the basename " + "\"" + temp
                        + "\"" + " is greater than 6 characters. \nOnly the first 6 characters will be used for naming Landmark 2D process level and version. \nDo you want to proceed?",
                        "Action Confirmation",JOptionPane.YES_NO_OPTION);
                if(action == JOptionPane.NO_OPTION ){
                    return false;
                }
            }
        }
        if(normalizedRadioButton.isSelected()){
            String txt = noiseFactorTextField.getText().trim();
            if(txt.length() == 0){
                JOptionPane.showMessageDialog(comp,
                        "The Noise Factor field is empty.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
                        specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
                        noiseFactorTextField.requestFocus();
                return false;
            }else{
                try {
                    Integer.parseInt(txt);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(comp,
                        "The Noise Factor field must be a numeric value.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                    specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
                    noiseFactorTextField.requestFocus();
                    return false;
                }
            }
        }

        if(scaleTextFieldList == null || scaleTextFieldList.size() == 0){
            JOptionPane.showMessageDialog(comp, "Must select at least one scale to produce the Landmark output",
                                         "Missing data entry",JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
            scalesComboBox.requestFocus();
            return false;

        }
        if (scaleTextFieldList != null && scaleTextFieldList.size() > 0) {
            int[] arrScales = new int[scaleTextFieldList.size()];
            for (int i = 0; i < scaleTextFieldList.size(); i++) {
                String s = scaleTextFieldList.get(i).getText();
                if (s != null && s.trim().length() == 0) {
                    JOptionPane.showMessageDialog(comp, "The "
                            + scaleLabelList.get(i).getText()
                            + " field is empty.", "Missing data entry",
                            JOptionPane.WARNING_MESSAGE);
                    specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
                    scaleTextFieldList.get(i).requestFocus();
                    return false;
                }
                try {
                    int val = Integer.parseInt(s);
                    arrScales[i] = val;
                    int numScales = Integer.valueOf(numberScalesTextField.getText().trim()).intValue();
                    if(val < 1 ||  val > numScales){
                        JOptionPane.showMessageDialog(comp, "The "
                                + scaleLabelList.get(i).getText()
                                + " field must be in the range of 1 and " + numScales,
                                "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                        specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
                        scaleTextFieldList.get(i).requestFocus();
                        return false;
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(comp, "The "
                            + scaleLabelList.get(i).getText()
                            + " field must be a numeric value.",
                            "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                    specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
                    scaleLabelList.get(i).requestFocus();
                    return false;
                }
            }
            Arrays.sort(arrScales);

            for (int i = 0; i < arrScales.length; i++) {
                for (int j = 0; j < arrScales.length; j++) {
                    if ((i != j) && (arrScales[i] == arrScales[j])) {
                        JOptionPane.showMessageDialog(comp,
                                "The scale fields must be distinct values.",
                                "Invalid data entry",
                                JOptionPane.WARNING_MESSAGE);
                        specDecompTabbedPane
                                .setSelectedIndex(OUTPUTS_TAB_INDEX);
                        return false;
                    }
                }
            }
        }
        //removed from the requirements
        //if (landmarkOutputTimeTextFieldList != null && landmarkOutputTimeTextFieldList.size() > 0) {
        //    int [] arrTimePlacement = new int[landmarkOutputTimeTextFieldList.size()];
        //    for (int i = 0; i < landmarkOutputTimeTextFieldList.size(); i++) {
        //        String s = landmarkOutputTimeTextFieldList.get(i).getText();
        //        if (s != null && s.trim().length() == 0) {
        //            JOptionPane.showMessageDialog(comp, "The "
        //                  + landmarkOutputTimeLabelList.get(i).getText()
        //                  + " time field is empty.", "Missing data entry",
        //                  JOptionPane.WARNING_MESSAGE);
        //            specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
        //            landmarkOutputTimeTextFieldList.get(i).requestFocus();
        //            return false;
        //        }
        //        try {
        //            int val = Integer.parseInt(s);
        //            arrTimePlacement[i] = val;
        //            int startTime = Integer.valueOf(landmarkStartTimeTextField.getText().trim()).intValue();
        //            int endTime = Integer.valueOf(landmarkEndTimeTextField.getText().trim()).intValue();
        //            if(val < startTime ||  val > endTime){
        //                JOptionPane.showMessageDialog(comp, "The "
        //                        + landmarkOutputTimeLabelList.get(i).getText()
        //                        + " time field must be in the range of start time " + startTime + " and end time " + endTime,
        //                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
        //                specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
        //                landmarkOutputTimeTextFieldList.get(i).requestFocus();
        //                return false;
        //            }
        //        } catch (NumberFormatException ex) {
        //            JOptionPane.showMessageDialog(comp, "The "
        //                    + landmarkOutputTimeLabelList.get(i).getText()
        //                    + " time field must be a numeric value.",
        //                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
        //            specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
        //            landmarkOutputTimeTextFieldList.get(i).requestFocus();
        //            return false;
        //        }
        //    }
        //    Arrays.sort(arrTimePlacement);

        //    for (int i = 0; i < arrTimePlacement.length; i++) {
        //        for (int j = 0; j < arrTimePlacement.length; j++) {
        //            if ((i != j) && (arrTimePlacement[i] == arrTimePlacement[j])) {
        //                JOptionPane.showMessageDialog(comp,
        //                        "The time fields must be distinct values.",
        //                        "Invalid data entry",
        //                        JOptionPane.WARNING_MESSAGE);
        //                specDecompTabbedPane
        //                        .setSelectedIndex(OUTPUTS_TAB_INDEX);
        //                return false;
        //            }
        //        }
        //    }
        //}
        return true;
    }


    //private boolean isValidLandmarkProject(String project){
    //  List<ProjectInfo> projects = agent.getLandmarkAllProjects();
    //  for(int i = 0; projects != null && i < projects.size(); i++){
    //      if(projects.get(i).getName().equals(project))
    //          return true;
    //  }
    //  return false;
    //}

    private boolean validateLandmarkInput(){
        final Component comp = this;
        if(!landmarkInRadioButton.isSelected())
            return true;
        if(landmarkAgent == null)
           landmarkAgent = LandmarkServices.getInstance();
        if(landmark3DRadioButton.isSelected()){
            String text = landmarkInProjectTextField.getText();
            if (text == null || text.trim().length() == 0) {
                JOptionPane.showMessageDialog(comp, "The " + landmarkProjectLabel.getText()
                    + " field is empty.", "Missing data entry", JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                landmarkInProjectTextField.requestFocus();
                return false;
            }
            
            if (!landmarkAgent.isValidLandmarkProject(text)) {
                JOptionPane.showMessageDialog(comp, "Can not find project name " + landmarkInProjectTextField.getText()
                        + ". Check to see if it is valid Landmark project", "Project not found",    JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                landmarkInProjectTextField.requestFocus();
                return false;
            }


            text = volumeTextField.getText();
            if (text == null || text.trim().length() == 0) {
                JOptionPane.showMessageDialog(comp, "The " + volumeLabel.getText()
                    + " field is empty.", "Missing data entry",
                    JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                volumeTextField.requestFocus();
                return false;
            }

            if(!validateLineTraceTextField(linesTextField,linesToTextField,linesByTextField,lineTraceArray[0]))
                return false;
            if(!validateLineTraceTextField(tracesTextField,tracesToTextField,tracesByTextField,lineTraceArray[1]))
                return false;
            if(!validateTimeTextField(timeTextField,timeToTextField,lineTraceArray[2]))
                return false;
        }else if(landmark2DRadioButton.isSelected()){
            String text = landmarkIn2DProjectTextField.getText();
            if (text == null || text.trim().length() == 0) {
                JOptionPane.showMessageDialog(comp, "The " + landmarkProjectLabel.getText()
                    + " field is empty.", "Missing data entry", JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                landmarkIn2DProjectTextField.requestFocus();
                return false;
            }

            if (!landmarkAgent.isValidLandmarkProject(text)) {
                JOptionPane.showMessageDialog(comp, "Can not find project name " + landmarkIn2DProjectTextField.getText()
                        + ". Check to see if it is valid Landmark project", "Project not found",    JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                landmarkIn2DProjectTextField.requestFocus();
                return false;
            }

            if(seismic2DFileComboBox.getSelectedIndex() < 1){
                JOptionPane.showMessageDialog(comp, "Must select a process level. "
                        , "Process Level and Version not found",    JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                return false;
            }
            Object[]  keys = selectedLineMap.keySet().toArray();
            if(keys == null || keys.length == 0){
                JOptionPane.showMessageDialog(comp, "Must select at least a seismic 2D line. "
                        , "2D Lines not selected",  JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
                seismic2DLineList.requestFocus();
                return false;
            }
            for(int i = 0; keys!= null && i < keys.length; i++){
                if(!validateShotPointTextField((String)keys[i]))
                    return false;
            }
        }
        return true;
    }

    private boolean validateLandmark(){
        if(!validateLandmarkInput())
            return false;
        //if(!validateFlatteningInfo(landmarkHorzLabelList, landmarkHorzTextFieldList, landmarkStartTimeTextField, landmarkEndTimeTextField, landmarkHorzTimeTextFieldList))
        //  return false;

        return true;
    }

    private boolean validateShotPointTextField(String linename) {
        if(linename == null || linename.trim().length() == 0)
            return false;
        int ind = 0;
        for(int i = 0; i < selectedSeismic2DLineList.getModel().getSize(); i++){
            if(selectedSeismic2DLineList.getModel().getElementAt(i) != null
                    && selectedSeismic2DLineList.getModel().getElementAt(i).equals(linename)){
                ind = i;
                break;
            }
        }
        LineInfo li = selectedLineMap.get(linename);

        final Component comp = this;
        int tabIdx = specDecompTabbedPane.indexOfTab("Seismic Data Input");

        float fromVal = li.getMinShotPoint();

        float toVal = li.getMaxShotPoint();

        float by = li.getIncShotPoint();

        if(li.getIncShotPoint_def() > 0){
            if(by < li.getIncShotPoint_def()){
                JOptionPane.showMessageDialog(comp, "The increment value (" + by +
                        ") should not be less than the defaul increment value (" + li.getIncShotPoint_def() +
                        ").\n Set it bask to default (" + li.getIncShotPoint_def() + ").",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointByTextField.requestFocus();
                shotpointByTextField.setText(String.valueOf(li.getIncShotPoint_def()));
                return false;
            }
        } else if(li.getIncShotPoint_def() < 0){
            if(by > li.getIncShotPoint_def()){
                JOptionPane.showMessageDialog(comp, "The increment value (" + by + 
                        ") should not be greater than the default increment value (" + li.getIncShotPoint_def() +
                        ").\n Set it bask to default (" + li.getIncShotPoint_def() + ").",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointByTextField.requestFocus();
                shotpointByTextField.setText(String.valueOf(li.getIncShotPoint_def()));
                return false;
            }
        }

        if(li.getIncShotPoint_def() != 0){
            //int by_def = (int)(li.getIncShotPoint_def()*100);
            //int by1 = (int)(by*100);
            //int rem = by1 % by_def;
            //if(rem != 0){
            double rem = Math.IEEEremainder(by, li.getIncShotPoint_def());
            if(rem > Double.MIN_VALUE){
                float f = (float)(by - rem);
                JOptionPane.showMessageDialog(comp, "The increment value (" + by +
                        ") should be multiple of the default increment value (" + li.getIncShotPoint_def() +
                        ").\n Set it to the closest value (" + f + ").",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointByTextField.requestFocus();
                //int fac = by1 - rem;
                //float f = (float)(fac * 0.01);
                
                shotpointByTextField.setText(String.valueOf(f));
                return false;
            }
        }
        if(li.getIncShotPoint_def() > 0){
            if (!(fromVal >= li.getMinShotPoint_def())) {
                JOptionPane.showMessageDialog(comp,
                        "The \"from\" field value (" + fromVal +
                        ") should not be less than the default \"from\" field value ("
                                + li.getMinShotPoint_def() + 
                                ").\n Set it back to default (" + li.getMinShotPoint_def() + ").",
                                "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointTextField.requestFocus();
                shotpointTextField.setText(String.valueOf(li.getMinShotPoint_def()));
                return false;
            }
            if (toVal > li.getMaxShotPoint_def()) {
                JOptionPane.showMessageDialog(comp,
                        "The \"to\" field value should not be greater than the default \"to\" field value ("
                         + li.getMaxShotPoint_def() + 
                         ").\n Set it back to default (" + li.getMaxShotPoint_def() + ").",
                         "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointToTextField.requestFocus();
                shotpointToTextField.setText(String.valueOf(li.getMaxShotPoint_def()));
                return false;
            }
            if (!(toVal > fromVal)) {
                JOptionPane.showMessageDialog(comp,
                        "The \"to\" field value (" + toVal + ") should be greater than the \"from\" value ("
                                + fromVal + 
                                ").\n Set it to default \"to\" field value (" + li.getMaxShotPoint_def() + ").",
                                "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointToTextField.requestFocus();
                shotpointToTextField.setText(String.valueOf(li.getMaxShotPoint_def()));
                return false;
            }
        } else {
            if (fromVal > li.getMinShotPoint_def()) {
                JOptionPane.showMessageDialog(comp,
                        "The \"from\" field value should not be greater than the default \"from\" field value ("
                                + li.getMinShotPoint_def() + 
                                ").\n Set it to default \"from\" field value (" +li.getMinShotPoint_def() + ").",
                                "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointTextField.requestFocus();
                shotpointTextField.setText(String.valueOf(li.getMinShotPoint_def()));
                return false;
            }
            if (!(toVal < fromVal)) {
                JOptionPane.showMessageDialog(comp,
                        "The \"to\" field value (" + toVal + ") should be less than the \"from\" field value ("
                                + fromVal + 
                                 ").\n Set it to deault \"to\" field value (" + li.getMaxShotPoint_def() + ").",
                                "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointToTextField.requestFocus();
                shotpointToTextField.setText(String.valueOf(li.getMaxShotPoint_def()));
                return false;
            }


             
            if(!(toVal >= li.getMaxShotPoint_def())){
                JOptionPane.showMessageDialog(comp,
                        "The \"to\" filed value should be greater than or equals to the default \"to\" field value ("
                                + li.getMaxShotPoint_def() + 
                                ").\n Set it to default \"to\" field value (" + li.getMaxShotPoint_def() + ").",
                                "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointToTextField.requestFocus();
                shotpointToTextField.setText(String.valueOf(li.getMaxShotPoint_def()));
                return false;
            }
        }


        if(li.getIncShotPoint_def() != 0){
            double rem = Math.IEEEremainder(fromVal-li.getMinShotPoint_def(), li.getIncShotPoint_def());
            if(rem > Double.MIN_VALUE){
            //float rem = (fromVal-li.getMinShotPoint_def()) % li.getIncShotPoint_def();
            //if(100 * rem > 0.1){
                float f = (float)(fromVal - rem);
                JOptionPane.showMessageDialog(comp,
                    "The difference between the current \"from\" field value (" + fromVal + 
                    ") and the default \"from\" field value (" + li.getMinShotPoint_def() + 
                    ") should be divisible by the default increment (" + li.getIncShotPoint_def() +
                    ").\n Set it to the closest value (" + f + ").",
                            "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
                specDecompTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointTextField.requestFocus();
                //int fac = Math.round((fromVal-li.getMinShotPoint_def()) / li.getIncShotPoint_def());
                //float val = li.getMinShotPoint_def() + (fac * li.getIncShotPoint_def());
                //Format fmt = new Format("%.2f");

                //shotpointTextField.setText(fmt.format(val));
                shotpointTextField.setText(String.valueOf(f));
                return false;
            }
        }

        if(by != 0){
            double rem = Math.IEEEremainder(toVal - fromVal, by);
            if(rem > Double.MIN_VALUE){
            //int by1 = (int)(by*100);
            //int fromVal1 = (int)(fromVal*100);
            //int toVal1 = (int)(toVal*100);
            //int rem =  ((toVal1 - fromVal1) % by1);
            float f = (float)(toVal - rem);
            //if(rem != 0){
             JOptionPane.showMessageDialog(comp,
                    "The difference between the current \"to\" field value (" + toVal +
                    ") and the default \"from\" field value (" + fromVal +
                    ") should be divisible by the increment (" + by +
                    ").\n Set it to the closest value (" + f + ").",
                     "Invalid data entry",
                     JOptionPane.WARNING_MESSAGE);
             specDecompTabbedPane.setSelectedIndex(tabIdx);
             selectedSeismic2DLineList.setSelectedIndex(ind);
             shotpointToTextField.requestFocus();
             //if(rem > 0){
             //int to = toVal1 - rem;
             
             //float val = (float)(to * 0.01);
                 //float next = toVal - rem;
                 //if(next == fromVal)
                //   next += by;
                 //shotpointToTextField.setText(String.valueOf(next));
             shotpointToTextField.setText(String.valueOf(f));
             //}else{
            //   float next = toVal + rem;
            //   if(next == fromVal)
            //       next += by;
            //   shotpointToTextField.setText(String.valueOf(next));
             //}
             return false;
            }
        }

        return true;
    }
    private boolean validateBHPSUInput(){
        final Component comp = this;
        String text = datasetTextField.getText();
        if (text == null || text.trim().length() == 0) {
            JOptionPane.showMessageDialog(comp, "The " + datasetLabel.getText()
                    + " field is empty.", "Missing data entry",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(INPUT_TAB_INDEX);
            datasetTextField.requestFocus();
            return false;
        }
        if (validateCdpTextField() == false)
            return false;
        if (validateCdpToTextField() == false)
            return false;
        if (validateCdpByTextField() == false)
            return false;
        if (validateEpTextField() == false)
            return false;
        if (validateEpToTextField() == false)
            return false;
        if (validateEpByTextField() == false)
            return false;
        return true;
    }

    private boolean validateBHPSU(){
        if(!validateBHPSUInput())
            return false;
        //if (validateFlatteningInfo(suHorzLabelList,suHorzTextFieldList,suStartTimeTextField,suEndTimeTextField,suHorzTimeTextFieldList) == false)
        //  return false;
        return true;
    }

    private boolean validateBHPSUOutput(){
        final Component comp = this;
        String text = bhpsuNameBaseTextField.getText();
        if (text == null || text.trim().length() == 0) {
            JOptionPane.showMessageDialog(comp, "The "
                    + bhpsuNameBaseLabel.getText() + " field is empty.",
                    "Missing data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
            bhpsuNameBaseTextField.requestFocus();
            return false;
        }
        if(text.endsWith(filesep)){
            JOptionPane.showMessageDialog(comp, "The "
                    + bhpsuNameBaseLabel.getText() + " field can not end with " + filesep,
                    "Error in data entry", JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(OUTPUTS_TAB_INDEX);
            bhpsuNameBaseTextField.requestFocus();
            return false;
        }

        if (bhpsuScaleNormalizedRadioButton.isSelected()) {
            if(validateBhpsuNoiseFactorTextField() == false)
                return false;
        }
        return true;
    }

    private List<String> formDynamicSUScripts(){
        List<String> scripts = ScriptGenerator.genHeadScripts(agent.getQiProjectDescriptor(), filesep);
        scripts.add("");
        if(landmarkInRadioButton.isSelected()){
            if(!validateLandmark())
                return null;
            if(landmark2DRadioButton.isSelected()){
                scripts.add("input_type=" + "landmark2D");
                String text = landmarkIn2DProjectTextField.getText().trim();
                scripts.add("lm_in_project=" + text);
                scripts.add("lm_in_project_type=" + "2");

                String plevel = (String)seismic2DFileComboBox.getSelectedItem();
                scripts.add("lm_2d_in_process_level=" +  plevel);
                convertShotpointsToTraces();

                Object[]  keys = selectedLineMap.keySet().toArray();
                StringBuffer shotpoints = new StringBuffer();
                StringBuffer traces = new StringBuffer();
                for(int i = 0; keys!= null && i < keys.length; i++){
                    LineInfo li = selectedLineMap.get((String)keys[i]);
                    shotpoints.append(li.lineName + ":" + li.lineNumber + ":" + li.minShotPoint + ":" + li.maxShotPoint + ":" + li.incShotPoint);
                    traces.append(li.lineName + ":" + li.lineNumber + ":" + li.minTrace + ":" + li.maxTrace + ":" + li.incTrace);
                    if(i < (keys.length - 1)){
                        shotpoints.append(",");
                        traces.append(",");
                    }
                }

                scripts.add("");
                scripts.add("#line_shotpoint_list=l_name1:l_num1:min_spn1:max_spn1:inc_spn1,l_name2:l_num2:min_spn2:max_spn2:inc_spn2,...");
                scripts.add("line_shotpoint_list=" + shotpoints.toString());
                scripts.add("#line_trace_list=l_name1:l_num1:min_tr1:max_tr1:inc_tr1,l_name2:l_num2:min_tr2:max_tr2:inc_tr2,....");
                scripts.add("line_trace_list=" + traces.toString());
            }else if(landmark3DRadioButton.isSelected()){
                scripts.add("input_type=" + "landmark3D");
                String text = landmarkInProjectTextField.getText().trim();
                scripts.add("");
                scripts.add("lm_in_project=" + text);
                scripts.add("lm_in_project_type=" + "3");
                text = volumeTextField.getText();
                scripts.add("lm_in_volume=" + text);
                int id = text.indexOf(".bri");
                if(id != -1){
                    text = text.substring(0, id);
                }
                scripts.add("lm_in_volume_base=" + text);

                String line = linesTextField.getText().trim();
                String lineTo = linesToTextField.getText().trim();
                String lineInc = linesByTextField.getText().trim();
                scripts.add("lm_min_line=" + line);
                scripts.add("lm_max_line=" + lineTo);
                scripts.add("lm_inc_line=" + lineInc);

                String traces = tracesTextField.getText().trim();
                String tracesTo = tracesToTextField.getText().trim();
                String tracesInc = tracesByTextField.getText().trim();
                scripts.add("lm_min_trace=" + traces);
                scripts.add("lm_max_trace=" + tracesTo);
                scripts.add("lm_inc_trace=" + tracesInc);

                String time = timeTextField.getText().trim();
                String timeTo = timeToTextField.getText().trim();
                scripts.add("lm_min_time=" + time);
                scripts.add("lm_max_time=" + timeTo);

                int min_line = Integer.valueOf(line).intValue();
                int max_line = Integer.valueOf(lineTo).intValue();
                int line_by = Integer.valueOf(lineInc).intValue();
                int min_trace = Integer.valueOf(traces).intValue();
                int max_trace = Integer.valueOf(tracesTo).intValue();
                int trace_by = Integer.valueOf(tracesInc).intValue();
                int minep=0, maxep=0, incep=1, nep=1, mincdp=0, maxcdp = 0, inccdp=1, ncdp=1;
                if(line_by < 0){
                    minep = max_line;
                    incep = line_by * (-1);
                    nep = (min_line-max_line)/incep + 1;
                    maxep = min_line;
                }else if(line_by > 0){
                    minep = min_line;
                    incep = line_by;
                    nep = (max_line-min_line)/incep + 1;
                    maxep = max_line;
                }

                if(trace_by < 0){
                    mincdp = max_trace;
                    inccdp = trace_by * (-1);
                    ncdp = (min_trace-max_trace)/inccdp + 1;
                    maxcdp = min_trace;
                }else if(trace_by > 0){
                    mincdp = min_trace;
                    inccdp = trace_by;
                    ncdp = (max_trace-min_trace)/inccdp + 1;
                    maxcdp = max_trace;
                }
                scripts.add("minep=" + minep);
                scripts.add("maxep=" + maxep);
                scripts.add("incep=" + incep);
                scripts.add("nep=" + nep);
                scripts.add("mincdp=" + mincdp);
                scripts.add("maxcdp=" + maxcdp);
                scripts.add("inccdp=" + inccdp);
                scripts.add("ncdp=" + ncdp);

           }
        }else{
            if(!validateBHPSU())
                return null;
            scripts.add("input_type=" + "bhpsu");
            String text = datasetTextField.getText();
            if(text != null && text.trim().length() > 0){
                int ind = text.lastIndexOf(filesep);
                if( ind != -1){
                    String name = text.substring(text.lastIndexOf(filesep) + 1, text.lastIndexOf("."));
                    scripts.add("IN_DATASET_FILENAME=" + name);
                    String inDataSet = text.substring(ind,text.lastIndexOf("."));
                    scripts.add("IN_DATASET=" + inDataSet);
                }else{
                    scripts.add("IN_DATASET_FILENAME=" + text.substring(0,text.lastIndexOf(".")));
                    scripts.add("IN_DATASET=" + text.substring(0,text.lastIndexOf(".")));
                }
            }else{
                scripts.add("IN_DATASET_FILENAME=" + "");
                scripts.add("IN_DATASET=" + "");
            }
            String mincdp = cdpTextField.getText().trim();
            String maxcdp = cdpToTextField.getText().trim();
            String inccdp = cdpByTextField.getText().trim();
            String minep = epTextField.getText().trim();
            String maxep = epToTextField.getText().trim();
            String incep = epByTextField.getText().trim();
            int i_minep = Integer.valueOf(minep).intValue();
            int i_maxep = Integer.valueOf(maxep).intValue();
            int i_incep = Integer.valueOf(incep).intValue();
            int nep = (i_maxep-i_minep)/i_incep + 1;
            scripts.add("minep=" + minep);
            scripts.add("maxep=" + maxep);
            scripts.add("incep=" + incep);
            scripts.add("nep=" + nep);
            int i_mincdp = Integer.valueOf(mincdp).intValue();
            int i_maxcdp = Integer.valueOf(maxcdp).intValue();
            int i_inccdp = Integer.valueOf(inccdp).intValue();
            int ncdp = (i_maxcdp-i_mincdp)/i_inccdp + 1;
            scripts.add("mincdp=" + mincdp);
            scripts.add("maxcdp=" + maxcdp);
            scripts.add("inccdp=" + inccdp);
            scripts.add("ncdp=" + ncdp);
        }

        String output_type = "bhpsu";
        if( landmarkOutRadioButton.isSelected() ){
            if(!validateLandmarkOutput())
                return null;
            if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                String text = landmark2DNameBaseTextField.getText();
                if(text != null && text.trim().length() > 0){
                    int ind = text.lastIndexOf(filesep);
                    String temp = "";
                    if(ind == -1){
                        scripts.add("OUT_DATASET="+ text);
                        scripts.add("OUT_DATASET_DIR=" + "");
                        scripts.add("OUT_DATASET_FILENAME="+text);
                    }else{
                        String tmp = text.substring(0, ind);
                        scripts.add("OUT_DATASET=" + text);
                        scripts.add("OUT_DATASET_DIR=" + tmp);
                        scripts.add("OUT_DATASET_FILENAME=" + text.substring(ind+1));
                        text = text.substring(ind+1);
                    }
                    if(text.length() > 6)
                        temp = text.substring(0, 6);
                    else if(text.length() < 6){
                        temp = text;
                        StringBuffer tempB = new StringBuffer();
                        for(int i = 0; i < 6 - text.length(); i++){
                            tempB.append("0");
                        }
                        temp += tempB.toString();
                    }else
                        temp = text;
                    scripts.add("lm_2d_out_process_level=" + temp);
                }
                scripts.add("lm_out_project=" + landmark2DOutProjectTextField.getText().trim());
                scripts.add("lm_out_project_type=" + "2");
                output_type = "landmark2D";
                scripts.add("output_type=" + output_type);
                if (landmark2DScaleNormalizedRadioButton.isSelected()) {
                    scripts.add("run_normalization=" + "yes");
                    text = landmark2DNoiseFactorTextField.getText().trim();
                    scripts.add("gamma=" + text);
                } else {
                    scripts.add("run_normalization=" + "no");
                    scripts.add("gamma=" + String.valueOf(defaultGamma));
                }

                StringBuffer scales = new StringBuffer();
                for(int i = 0; landmark2DOutputScaleTextFieldList != null && i < landmark2DOutputScaleTextFieldList.size(); i++){
                    String scale = landmark2DOutputScaleTextFieldList.get(i).getText().trim();
                    int num = Integer.valueOf(scale).intValue();
                    if(num < 10)
                        scale = "0" + scale;
                    scales.append(scale);
                    if(i < (landmark2DOutputScaleTextFieldList.size()-1))
                        scales.append(",");
                }
                scripts.add("out_scale_list=" + scales.toString());

            }else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                String text = landmark3DNameBaseTextField.getText();
                if(text != null && text.trim().length() > 0){
                    int ind = text.lastIndexOf(filesep);
                    if(ind == -1){
                        scripts.add("OUT_DATASET=" + text);
                        scripts.add("OUT_DATASET_DIR=" + "");
                        scripts.add("OUT_DATASET_FILENAME=" + text);
                    }else{
                        String tmp = text.substring(0, ind);
                        scripts.add("OUT_DATASET=" + text);
                        scripts.add("OUT_DATASET_DIR=" + tmp);
                        scripts.add("OUT_DATASET_FILENAME=" + text.substring(ind+1));
                        text = text.substring(ind+1);
                    }
                }
                String []  vlist = landmarkAgent.getLandmarkVolumeList(landmark3DOutProjectTextField.getText().trim(),
                        landmarkAgent.getLandmarkProjectType(landmark3DOutProjectTextField.getText().trim())
                        , ".bri");
                boolean exist = false;
                for(int i = 0; vlist != null && i < vlist.length; i++){
                    if(vlist[i].equalsIgnoreCase(text + ".bri"))
                        exist = true;
                }
                if(exist == true && notAskAgainForOverwite == false){
                    String message = text + ".bri exists in the project. Do you want to overwrite it when you run the script?";
                    ConfirmOverwriteDialog confirm = new ConfirmOverwriteDialog(JOptionPane.getFrameForComponent(this), true, message);
                    confirm.setVisible(true);
                    int returnStatus = confirm.getReturnStatus();
                    notAskAgainForOverwite = confirm.getNotAskAgain();
                    if(returnStatus == ConfirmOverwriteDialog.RET_NO)
                        return null;
                }
                scripts.add("lm_out_project=" + landmark3DOutProjectTextField.getText().trim());
                scripts.add("lm_out_project_type=" + "3");
                output_type = "landmark3D";
                scripts.add("output_type=" + output_type);
                if (landmark3DScaleNormalizedRadioButton.isSelected()) {
                    scripts.add("run_normalization=" + "yes");
                    text = landmark3DNoiseFactorTextField.getText().trim();
                    scripts.add("gamma=" + text);
                } else {
                    scripts.add("run_normalization=" + "no");
                    scripts.add("gamma=" + String.valueOf(defaultGamma));
                }

                StringBuffer scales = new StringBuffer();
                for(int i = 0; landmark3DOutputScaleTextFieldList != null && i < landmark3DOutputScaleTextFieldList.size(); i++){
                    scales.append(landmark3DOutputScaleTextFieldList.get(i).getText().trim());
                    if(i < (landmark3DOutputScaleTextFieldList.size()-1))
                        scales.append(",");
                }
                scripts.add("out_scale_list=" + scales.toString());
            }else{ //bhpsu input and landmark output
                String text = landmarkNameBaseTextField.getText();
                if(text != null && text.trim().length() > 0){
                    int ind = text.lastIndexOf(filesep);
                    if(ind == -1){
                        scripts.add("OUT_DATASET=" + text);
                        scripts.add("OUT_DATASET_DIR=" + "");
                        scripts.add("OUT_DATASET_FILENAME=" +text);
                    }else{
                        String tmp = text.substring(0, ind);
                        scripts.add("OUT_DATASET=" + text);
                        scripts.add("OUT_DATASET_DIR=" + tmp);
                        scripts.add("OUT_DATASET_FILENAME="+ text.substring(ind+1));
                        text = text.substring(ind+1);
                    }
                }

                int ptype = landmarkAgent.getLandmarkProjectType(landmarkOutProjectTextField.getText().trim());
                if(ptype == 3){
                    String []  vlist = landmarkAgent.getLandmarkVolumeList(landmarkOutProjectTextField.getText().trim(),
                        landmarkAgent.getLandmarkProjectType(landmarkOutProjectTextField.getText().trim())
                        , ".bri");
                    boolean exist = false;
                    for(int i = 0; vlist != null && i < vlist.length; i++){
                        if(vlist[i].equalsIgnoreCase(text + ".bri"))
                            exist = true;
                    }
                    if(exist == true && notAskAgainForOverwite == false){
                        String message = text + ".bri exists in the project. Do you want to overwrite it when you run the script?";
                        ConfirmOverwriteDialog confirm = new ConfirmOverwriteDialog(JOptionPane.getFrameForComponent(this), true, message);
                        confirm.setVisible(true);
                        int returnStatus = confirm.getReturnStatus();
                        notAskAgainForOverwite = confirm.getNotAskAgain();
                        if(returnStatus == ConfirmOverwriteDialog.RET_NO)
                            return null;
                    }
                }
                scripts.add("lm_out_project=" + landmarkOutProjectTextField.getText().trim());
                if(ptype == 2){
                    scripts.add("lm_out_project_type=" + "2");
                    output_type = "landmark2D";
                    scripts.add("output_type=" + output_type);
                }else if(ptype == 3){
                    scripts.add("lm_out_project_type=" + "3");
                    output_type = "landmark3D";
                    scripts.add("output_type=" + output_type);
                }
                if (landmarkScaleNormalizedRadioButton.isSelected()) {
                    scripts.add("run_normalization=" + "yes");
                    text = landmarkNoiseFactorTextField.getText().trim();
                    scripts.add("gamma=" + text);
                } else {
                    scripts.add("run_normalization=" + "no");
                    scripts.add("gamma=" + String.valueOf(defaultGamma));
                }

                StringBuffer scales = new StringBuffer();
                for(int i = 0; landmarkOutputScaleTextFieldList != null && i < landmarkOutputScaleTextFieldList.size(); i++){
                    scales.append(landmarkOutputScaleTextFieldList.get(i).getText().trim());
                    if(i < (landmarkOutputScaleTextFieldList.size()-1))
                        scales.append(",");
                }
                scripts.add("out_scale_list=" + scales.toString());
            }
        }else{
            if(!validateBHPSUOutput())
                return null;

            String text = bhpsuNameBaseTextField.getText();

            if(text != null && text.trim().length() > 0){
                int ind = text.lastIndexOf(filesep);
                if(ind == -1){
                    scripts.add("OUT_DATASET=" + text);
                    scripts.add("OUT_DATASET_DIR=" + "");
                    scripts.add("OUT_DATASET_FILENAME=" + text);
                }else{
                    String tmp = text.substring(0, ind);
                    scripts.add("OUT_DATASET=" + text);
                    scripts.add("OUT_DATASET_DIR=" + tmp);
                    scripts.add("OUT_DATASET_FILENAME=" + text.substring(ind+1));
                }
            }

            if (bhpsuScaleNormalizedRadioButton.isSelected()) {
                scripts.add("run_normalization=" + "yes");
                text = bhpsuNoiseFactorTextField.getText().trim();
                scripts.add("gamma=" + text);
            } else {
                scripts.add("run_normalization=" + "no");
                //gamma default value
                scripts.add("gamma=" + String.valueOf(defaultGamma));
            }

            output_type = "bhpsu";
            scripts.add("output_type=" + output_type);
            scripts.add("out_scale_list=" + "");
        }


        if (validateSmallestScaleTextField() == false)
            return null;
        String smallest_scale = smallestScaleTextField.getText().trim();
        scripts.add("smallest_scale=" + smallest_scale);

        if (validateLargestScaleTextField() == false)
            return null;

        String largest_scale = largestScaleTextField.getText().trim();
        scripts.add("largest_scale=" + largest_scale);

        if (validateNumberScalesTextField() == false)
            return null;

        String number_scales = numberScalesTextField.getText().trim();
        scripts.add("number_scales=" + number_scales);
        scripts.add("nx=" + number_scales);

        float fLargestScale = Float.valueOf(largest_scale).floatValue();
        float fSmallestScale = Float.valueOf(smallest_scale).floatValue();
        int iNumberScale = Integer.valueOf(number_scales).intValue();
        float increase = 0;
        if(iNumberScale != 0){
            increase = (fLargestScale-fSmallestScale) / (float)iNumberScale;
        }
        String sIncrease = "0";
        if(increase != 0){
            NumberFormat format = NumberFormat.getInstance();
            format.setMaximumFractionDigits(2);
            format.setMinimumFractionDigits(2);
            sIncrease = format.format(increase);
        }
        scripts.add("increase=" + sIncrease);


        if (validateAverageVelocityTextField() == false)
            return null;

        String text = averageVelocityTextField.getText().trim();
        scripts.add("velocity=" + text);

        if (validateSmoothingLengthTextField() == false)
            return null;
        String length = smoothingLengthTextField.getText().trim();
        scripts.add("smoothing_length=" + length);

        if (validateNumberOfPassTextField() == false)
            return null;
        length = numberOfPassTextField.getText().trim();
        scripts.add("number_pass=" + length);
        String tmin = "0";
        String tmax = "0";
        if(landmark3DRadioButton.isSelected() || bhpsuInRadioButton.isSelected()){
            if(landmarkHorizonRadioButton.isSelected() == true){
                if(!validateFlatteningInfo(landmarkHorzLabelList, landmarkHorzTextFieldList, landmarkStartTimeTextField, landmarkEndTimeTextField, landmarkHorzTimeTextFieldList))
                    return null;
                scripts.add("horizon_type=" + "landmark");
                StringBuffer string = new StringBuffer();
                StringBuffer string1 = new StringBuffer();
                for (int i = 0; landmarkHorzTextFieldList != null && i < landmarkHorzTextFieldList.size(); i++) {
                    String temp = landmarkHorzTextFieldList.get(i).getText();
                    string1.append(temp);
                    string.append(temp + ".xyz");
                    if (i < landmarkHorzTextFieldList.size() - 1){
                        string.append(",");
                        string1.append(",");
                    }
                }
                scripts.add("horizon_files=" + string);
                scripts.add("lm_horizon_files=" + string1);
                if(string.length() == 0)
                    scripts.add("run_slice=" + "no");
                else
                    scripts.add("run_slice=" + "yes");
                tmin = landmarkStartTimeTextField.getText().trim();
                if(tmin == null || tmin.trim().length() == 0)
                	tmin = "0";
                scripts.add("tmin=" + tmin);
                tmax = landmarkEndTimeTextField.getText().trim();
                if(tmax == null || tmax.trim().length() == 0)
                	tmax = "0";
                scripts.add("tmax=" + tmax);

                string = new StringBuffer();
                for (int i = 0; landmarkHorzTimeTextFieldList != null && i < landmarkHorzTimeTextFieldList.size(); i++) {
                    string.append(landmarkHorzTimeTextFieldList.get(i).getText());
                    if (i < landmarkHorzTimeTextFieldList.size() - 1)
                        string.append(",");
                }
                scripts.add("horizon_times=" + string.toString());
                text = sampleRateTextField.getText().trim();
                scripts.add("dt=" + text);
            }else{
                if (validateFlatteningInfo(suHorzLabelList,suHorzTextFieldList,suStartTimeTextField,suEndTimeTextField,suHorzTimeTextFieldList) == false)
                    return null;

            	if(bhpsuHorizonRadioButton.isSelected())
            		scripts.add("horizon_type=" + "ASCII");
            	else if(bhpsuHorizonDatasetRadioButton.isSelected())
                    scripts.add("horizon_type=" + "bhpsu");


                StringBuffer string = new StringBuffer();
                for (int i = 0; suHorzTextFieldList != null && i < suHorzTextFieldList.size(); i++) {
                    String temp = suHorzTextFieldList.get(i).getText();
                    string.append(temp);
                    if (i < suHorzTextFieldList.size() - 1)
                        string.append(",");
                }
                if(string.length() == 0)
                    scripts.add("run_slice=" + "no");
                else
                    scripts.add("run_slice=" + "yes");

                scripts.add("horizon_files=" + string);


                tmin = suStartTimeTextField.getText().trim();
                if(tmin == null || tmin.trim().length() == 0)
                	tmin = "0";
                scripts.add("tmin=" + tmin);
                tmax = suEndTimeTextField.getText().trim();
                if(tmax == null || tmax.trim().length() == 0)
                	tmax = "0";
                scripts.add("tmax=" + tmax);

                string = new StringBuffer();
                for (int i = 0; suHorzTimeTextFieldList != null && i < suHorzTimeTextFieldList.size(); i++) {
                    string.append(suHorzTimeTextFieldList.get(i).getText());
                    if (i < suHorzTimeTextFieldList.size() - 1)
                        string.append(",");
                }
                scripts.add("horizon_times=" + string.toString());
                text = timeIncrementTextField.getText().trim();
                scripts.add("dt=" + text);
            }

            float fSec = Float.valueOf(tmin).floatValue() / 1000f;
            String sSec = "0";
            if(fSec != 0f){
                NumberFormat format = NumberFormat.getInstance();
                format.setMaximumFractionDigits(1);
                format.setMinimumFractionDigits(1);
                sSec = format.format(fSec);
            }
            scripts.add("sec=" + sSec);
            scripts.add("");
        }

        scripts.add("dirname=`pwd`");
        scripts.add("echo $0 running from ${dirname}");
        scripts.add("cd $QISPACE/$QIPROJECT");
        scripts.add("echo \"pwd=\" `pwd`");
        scripts.add("");
        scripts.add("if [ ! -d ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET_DIR} ]");
        scripts.add("then");
        scripts.add("  mkdir -p ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET_DIR}");
        scripts.add("fi");
        scripts.add("return_code=$?");
        scripts.add("if [ ${return_code} -ne 0 ]");
        scripts.add("then");
        scripts.add("  echo \"Error in creating ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET_DIR}\"");
        scripts.add("  echo \"$0 job ends abnormally\"");
        scripts.add("  echo \"exit_status=$return_code\"");
        scripts.add("  exit $return_code");
        scripts.add("fi");
        scripts.add("");
        scripts.add("#");
        scripts.add("# spec_decomp.sh");
        scripts.add("#");
        if(bhpsuInRadioButton.isSelected() || (landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected())){
            // not for Landmark 2D type of input
            scripts.add("OUT_DATASET_FILENAME=${OUT_DATASET_FILENAME}_cwt");
            scripts.add("OUT_DATASET=${OUT_DATASET}_cwt");
            scripts.add("echo \"------\"");
            scripts.add("echo \"About to run spectral decompsition\"");
            scripts.add("echo \"IN_DATASET =\" ${IN_DATASET}");
            scripts.add("echo \"IN_DATASET_FILENAME =\" ${IN_DATASET_FILENAME}");
            scripts.add("echo \"OUT_DATASET=\" ${OUT_DATASET}");
            scripts.add("echo \"OUT_DATASET_FILENAME=\" ${OUT_DATASET_FILENAME}");
            scripts.add("echo \"------\"");
            scripts.add("");
            scripts.add("echo \"$QISEISMICS\" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat");
            scripts.add("bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}");
            scripts.add("");
        }
        
        scripts.add("if [ ! -d ${QISPACE}/${QIPROJECT}/tmp ]");
        scripts.add("then");
        scripts.add("  mkdir -p ${QISPACE}/${QIPROJECT}/tmp");
        scripts.add("fi");
        scripts.add("");
        scripts.add("return_code=$?");
        scripts.add("if [ ${return_code} -ne 0 ]");
        scripts.add("then");
        scripts.add("  echo \"Error in creating ${QISPACE}/${QIPROJECT}/tmp\"");
        scripts.add("  echo \"$0 job ends abnormally\"");
        scripts.add("  echo \"exit_status=$return_code\"");
        scripts.add("  exit $return_code");
        scripts.add("fi");
        scripts.add("");
        scripts.add("#put this in temp directory");
        scripts.add("bhpwavelet > tmp/wavelet.su");
        scripts.add("");
        if(landmark3DRadioButton.isSelected() || bhpsuInRadioButton.isSelected()){
            if(bhpsuInRadioButton.isSelected()){
                scripts.add("bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${IN_DATASET}.dat filename=${IN_DATASET_FILENAME} keys=ep,cdp keylist=${minep}-${maxep}[${incep}]:${mincdp}-${maxcdp}[${inccdp}] |\\");
                scripts.add("bhpcwt velocity=${velocity} first=${smallest_scale} inc=${increase} last=${largest_scale} wavelet=tmp/wavelet.su |\\");
                scripts.add("bhpsmooth npass=${number_pass} length=${smoothing_length} |\\");
                scripts.add("bhpwritecube init=yes key3=cdp,${mincdp},${inccdp},${ncdp} key1=ep,${minep},${incep},${nep} key2=cdpt,1,1,${number_scales} \\");
                scripts.add("  pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}");
                scripts.add("");
                scripts.add("return_code=$?");
                scripts.add("if [ ${return_code} -ne 0 ]");
                scripts.add("then");
                scripts.add("  echo \"Error in running bhpread | bhpcwt | bhpsmooth | bhpwritecube\"");
                scripts.add("  echo \"$0 job ends abnormally\"");
                scripts.add("  echo \"exit_status=$return_code\"");
                scripts.add("  exit $return_code");
                scripts.add("fi");
                scripts.add("");
            }else{
                scripts.add("echo \"$QISEISMICS\" > $QISPACE/$QIPROJECT/$QIDATASETS/${lm_in_volume_base}.dat");
                scripts.add("bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${lm_in_volume_base}.dat filename=${lm_in_volume_base}");
                scripts.add("");
                scripts.add("lmread lm_project=${lm_in_project} lm_ptype=${lm_in_project_type} lm_brickname=${lm_in_volume} \\");
                scripts.add("  iline=${lm_min_line}:${lm_max_line}:${lm_inc_line} \\");
                scripts.add("  xline=${lm_min_trace}:${lm_max_trace}:${lm_inc_trace} tmin=${lm_min_time} tmax=${lm_max_time} | \\");
                scripts.add("bhpwritecube init=yes key1=cdp,${mincdp},${inccdp},${ncdp} key2=ep,${minep},${incep},${nep}\\");
                scripts.add("  pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${lm_in_volume_base}.dat filename=${lm_in_volume_base}");
                scripts.add("");
                scripts.add("lmread lm_project=${lm_in_project} lm_ptype=${lm_in_project_type} lm_brickname=${lm_in_volume} \\");
                scripts.add("  iline=${lm_min_line}:${lm_max_line}:${lm_inc_line} \\");
                scripts.add("  xline=${lm_min_trace}:${lm_max_trace}:${lm_inc_trace} tmin=${lm_min_time} tmax=${lm_max_time} |\\");
                scripts.add("bhpcwt velocity=${velocity} first=${smallest_scale} inc=${increase} last=${largest_scale} \\");
                scripts.add("  wavelet=tmp/wavelet.su |\\");
                scripts.add("bhpsmooth npass=${number_pass} length=${smoothing_length} |\\");
                scripts.add("bhpwritecube init=yes key3=cdp,${mincdp},${inccdp},${ncdp} key1=ep,${minep},${incep},${nep} key2=cdpt,1,1,${number_scales} \\");
                scripts.add("  pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}");
                scripts.add("");
                scripts.add("return_code=$?");
                scripts.add("if [ ${return_code} -ne 0 ]");
                scripts.add("then");
                scripts.add("  echo \"Error in running lmread | bhpcwt | bhpsmooth | bhpwritecube\"");
                scripts.add("  echo \"$0 job ends abnormally\"");
                scripts.add("  echo \"exit_status=$return_code\"");
                scripts.add("  exit $return_code");
                scripts.add("fi");
                scripts.add("");
            }

            if((landmarkOutRadioButton.isSelected() && landmark3DScaleNormalizedRadioButton.isSelected())
                    || (bhpsuOutRadioButton.isSelected() && bhpsuScaleNormalizedRadioButton.isSelected())){
                scripts.add("echo \"nx=\" ${nx}");
                scripts.add("#");
                scripts.add("# normavg.sh");
                scripts.add("#");
                scripts.add("IN_DATASET_FILENAME=${OUT_DATASET_FILENAME}");
                scripts.add("IN_DATASET=${OUT_DATASET}");
                scripts.add("OUT_DATASET_FILENAME=${OUT_DATASET_FILENAME}_norm");
                scripts.add("OUT_DATASET=${OUT_DATASET}_norm");
                scripts.add("echo \"------\"");
                scripts.add("echo \"About to run normavg.sh\"");
                scripts.add("echo \"IN_DATASET =\" ${IN_DATASET}");
                scripts.add("echo \"IN_DATASET_FILENAME =\" ${IN_DATASET_FILENAME}");
                scripts.add("echo \"OUT_DATASET=\" ${OUT_DATASET}");
                scripts.add("echo \"OUT_DATASET_FILENAME=\" ${OUT_DATASET_FILENAME}");
                scripts.add("echo \"------\"");
                scripts.add("");
                scripts.add("echo \"$QISEISMICS\" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat");
                scripts.add("bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}");
                scripts.add("");
                scripts.add("bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${IN_DATASET}.dat filename=${IN_DATASET_FILENAME} keys=ep,cdp,cdpt | ");
                scripts.add("#write this to a temporaary directory");
                scripts.add("bhpavg nx=${nx} > tmp/avg_${IN_DATASET_FILENAME}_merge.su");
                scripts.add("");
                scripts.add("bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${IN_DATASET}.dat filename=${IN_DATASET_FILENAME} keys=ep,cdp,cdpt |\\");
                scripts.add("bhpnorm gamma=${gamma} nx=${nx} avg=tmp/avg_${IN_DATASET_FILENAME}_merge.su |");
                scripts.add("bhpwritecube init=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME} \\");
                scripts.add("    key1=ep,${minep},${incep},${nep} key3=cdp,${mincdp},${inccdp},${ncdp} key2=cdpt,1,1,${number_scales}");
                scripts.add("");
                scripts.add("return_code=$?");
                scripts.add("if [ ${return_code} -ne 0 ]");
                scripts.add("then");
                scripts.add("  echo \"Error in running bhpread | bhpnorm | bhpwritecube\"");
                scripts.add("  echo \"$0 job ends abnormally\"");
                scripts.add("  echo \"exit_status=$return_code\"");
                scripts.add("  exit $return_code");
                scripts.add("fi");
                scripts.add("");
                scripts.add("rm tmp/avg_${IN_DATASET_FILENAME}_merge.su");
                scripts.add("echo \"end of normavg.sh\"");
                scripts.add("");
            }
        }else if (landmark2DRadioButton.isSelected()){
            scripts.add("ii=0");
            scripts.add("IFS=,");
            scripts.add("for i in ${line_shotpoint_list}");
            scripts.add("do");
            scripts.add("  line_shotpoint_array[${ii}]=$i");
            scripts.add("  ii=`expr ${ii} + 1`");
            scripts.add("done");
            scripts.add("");
            scripts.add("kk=0");
            scripts.add("for ((  jj = 0 ;  jj < ${#line_shotpoint_array[*]};  jj++  ))");
            scripts.add("do");
            scripts.add("  IFS=:");
            scripts.add("  for k in ${line_shotpoint_array[${jj}]}");
            scripts.add("  do");
            scripts.add("    line_shotpoint_detail[${kk}]=$k");
            scripts.add("    kk=`expr ${kk} + 1`");
            scripts.add("  done");
            scripts.add("done");
            scripts.add("");
            scripts.add("ii=0");
            scripts.add("IFS=,");
            scripts.add("for i in ${line_trace_list}");
            scripts.add("do");
            scripts.add("   line_trace_array[${ii}]=$i");
            scripts.add("   ii=`expr ${ii} + 1`");
            scripts.add("done");
            scripts.add("");
            scripts.add("kk=0");
            scripts.add("for ((  jj = 0 ;  jj < ${#line_trace_array[*]};  jj++  ))");
            scripts.add("do");
            scripts.add("  IFS=:");
            scripts.add("  for k in ${line_trace_array[${jj}]}");
            scripts.add("  do");
            scripts.add("    line_trace_detail[${kk}]=$k");
            scripts.add("    kk=`expr ${kk} + 1`");
            scripts.add("  done");
            scripts.add("done");
            scripts.add("");
            scripts.add("jjj=0");
            scripts.add("nnn=0");
            scripts.add("for ((  jj = 0 ;  jj < ${#line_trace_detail[*]};  jj++  ))");
            scripts.add("do");
            scripts.add("   jjj=`expr ${jj} % 5`");
            scripts.add("   if [ ${jjj} = 0 ]");
            scripts.add("   then");
            scripts.add("      linenum=`expr ${jj} + 1`");
            scripts.add("      mins=`expr ${jj} + 2`");
            scripts.add("      maxs=`expr ${jj} + 3`");
            scripts.add("      inc=`expr ${jj} + 4`");
            scripts.add("      line_name_array[${nnn}]=${line_shotpoint_detail[${jj}]}");
            scripts.add("      nnn=`expr ${nnn} + 1`");
            scripts.add("      echo \"line name=${line_shotpoint_detail[${jj}]} linenum=${line_shotpoint_detail[${linenum}]} min_shotpoint=${line_shotpoint_detail[${mins}]}\\");
            scripts.add("      max_shotpoint=${line_shotpoint_detail[${maxs}]} increment=${line_shotpoint_detail[${inc}]}\"");
            scripts.add("      echo \"line name=${line_trace_detail[${jj}]} linenum=${line_trace_detail[${linenum}]} min_trace=${line_trace_detail[${mins}]}\\");
            scripts.add("      max_trace=${line_trace_detail[${maxs}]} increment=${line_trace_detail[${inc}]}\"");
            scripts.add("   fi");
            scripts.add("done");
            scripts.add("");
            scripts.add("jjj=0");
            scripts.add("ind=0");
            scripts.add("base_out_dataset_filename=${OUT_DATASET_FILENAME}");
            scripts.add("base_out_dataset=${OUT_DATASET}");
            scripts.add("      OUT_DATASET=${OUT_DATASET}_${line_shotpoint_detail[${jj}]}");
            scripts.add("for ((  jj = 0 ;  jj < ${#line_shotpoint_detail[*]};  jj++  ))");
            scripts.add("do");
            scripts.add("   jjj=`expr ${jj} % 5`");
            scripts.add("   if [ ${jjj} = 0 ]");
            scripts.add("   then");
            scripts.add("      linenum=`expr ${jj} + 1`");
            scripts.add("      mins=`expr ${jj} + 2`");
            scripts.add("      maxs=`expr ${jj} + 3`");
            scripts.add("      inc=`expr ${jj} + 4`");
            scripts.add("");
            scripts.add("      if [ ${line_trace_detail[${mins}]} -gt ${line_trace_detail[${maxs}]} ]");
            scripts.add("      then");
            scripts.add("          mincdp=${line_trace_detail[${maxs}]}");
            scripts.add("          maxcdp=${line_trace_detail[${mins}]}");
            scripts.add("          if [ ${line_trace_detail[${inc}]} -lt 0 ]");
            scripts.add("          then");
            scripts.add("              inccdp=`expr ${line_trace_detail[${inc}]} \\* -1`");
            scripts.add("          fi");
            scripts.add("      else");
            scripts.add("          maxcdp=${line_trace_detail[${maxs}]}");
            scripts.add("          mincdp=${line_trace_detail[${mins}]}");
            scripts.add("          inccdp=${line_trace_detail[${inc}]}");
            scripts.add("      fi");
            scripts.add("      ncdp=`expr ${maxcdp} - ${mincdp}`");
            scripts.add("      ncdp=`expr ${ncdp} / ${inccdp}`");
            scripts.add("      ncdp=`expr ${ncdp} + 1`");
            scripts.add("      minep=${line_trace_detail[${linenum}]}");
            scripts.add("      incep=1");
            scripts.add("      nep=1");
            scripts.add("");
            scripts.add("      OUT_DATASET_FILENAME=${base_out_dataset_filename}_${line_shotpoint_detail[${jj}]}");
            scripts.add("      OUT_DATASET=${base_out_dataset}_${line_shotpoint_detail[${jj}]}");
            scripts.add("      echo \"------\"");
            scripts.add("      echo \"About to export 2D Line ${OUT_DATASET_FILENAME} to BHPSU format\"");
            scripts.add("      echo \"IN_DATASET =\" ${IN_DATASET}");
            scripts.add("      echo \"IN_DATASET_FILENAME =\" ${IN_DATASET_FILENAME}");
            scripts.add("      echo \"OUT_DATASET=\" ${OUT_DATASET}");
            scripts.add("      echo \"OUT_DATASET_FILENAME=\" ${OUT_DATASET_FILENAME}");
            scripts.add("      echo \"------\"");
            scripts.add("");
            scripts.add("      echo \"$QISEISMICS\" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat");
            scripts.add("      bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}");
            scripts.add("");
            scripts.add("      lmread2d lm_project=${lm_in_project} process_level=${lm_2d_in_process_level}\\");
            scripts.add("        line_name=${line_shotpoint_detail[${jj}]}\\");
            scripts.add("        shotpoints=${line_shotpoint_detail[${mins}]}:${line_shotpoint_detail[${maxs}]}:${line_shotpoint_detail[${inc}]} |\\");
            scripts.add("      bhpwritecube init=yes key2=cdp,${mincdp},${inccdp},${ncdp} key1=ep,${minep},${incep},${nep} \\");
            scripts.add("        pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}");
            scripts.add("");
            scripts.add("      OUT_DATASET_FILENAME=${OUT_DATASET_FILENAME}_cwt");
            scripts.add("      OUT_DATASET=${OUT_DATASET}_cwt");
            //scripts.add("      OUT_DATASET_FILENAME=${OUT_DATASET_FILENAME}_${line_shotpoint_detail[${jj}]}_cwt");
            //scripts.add("      OUT_DATASET=${OUT_DATASET}_${line_shotpoint_detail[${jj}]}_cwt");

            //scripts.add("      out_dataset_filename_array[${ind}]=${OUT_DATASET_FILENAME}");
            //scripts.add("      out_dataset_array[${ind}]=${OUT_DATASET}");
            //scripts.add("      ind=`expr ${ind} + 1`");
            scripts.add("      echo \"------\"");
            scripts.add("      echo \"About to run spectral decomposition\"");
            scripts.add("      echo \"IN_DATASET =\" ${IN_DATASET}");
            scripts.add("      echo \"IN_DATASET_FILENAME =\" ${IN_DATASET_FILENAME}");
            scripts.add("      echo \"OUT_DATASET=\" ${OUT_DATASET}");
            scripts.add("      echo \"OUT_DATASET_FILENAME=\" ${OUT_DATASET_FILENAME}");
            scripts.add("      echo \"------\"");
            scripts.add("");
            scripts.add("      echo \"$QISEISMICS\" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat");
            scripts.add("      bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}");
            scripts.add("");
            scripts.add("      lmread2d lm_project=${lm_in_project} process_level=${lm_2d_in_process_level}\\");
            scripts.add("        line_name=${line_shotpoint_detail[${jj}]}\\");
            scripts.add("        shotpoints=${line_shotpoint_detail[${mins}]}:${line_shotpoint_detail[${maxs}]}:${line_shotpoint_detail[${inc}]} |\\");
            scripts.add("      bhpcwt velocity=${velocity} first=${smallest_scale} inc=${increase} last=${largest_scale} \\");
            scripts.add("        wavelet=tmp/wavelet.su |");
            scripts.add("      bhpsmooth npass=${number_pass} length=${smoothing_length} |");
            scripts.add("      bhpwritecube init=yes key3=cdp,${mincdp},${inccdp},${ncdp} key1=ep,${minep},${incep},${nep} key2=cdpt,1,1,${number_scales} \\");
            scripts.add("        pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}");
            scripts.add("");
            scripts.add("      return_code=$?");
            scripts.add("      if [ ${return_code} -ne 0 ]");
            scripts.add("      then");
            scripts.add("        echo \"Error in running lmread2d | bhpcwt | bhpsmooth | bhpwritecube\"");
            scripts.add("        echo \"$0 job ends abnormally\"");
            scripts.add("        echo \"exit_status=$return_code\"");
            scripts.add("        exit $return_code");
            scripts.add("      fi");
            scripts.add("");

            if((bhpsuOutRadioButton.isSelected() && bhpsuScaleNormalizedRadioButton.isSelected()) ||
                    landmarkOutRadioButton.isSelected() && landmark2DScaleNormalizedRadioButton.isSelected()){
                scripts.add("      echo \"nx=\" ${nx}");
                scripts.add("      #");
                scripts.add("      # normavg.sh");
                scripts.add("      #");
                scripts.add("      IN_DATASET_FILENAME=${OUT_DATASET_FILENAME}");
                scripts.add("      IN_DATASET=${OUT_DATASET}");
                scripts.add("      OUT_DATASET_FILENAME=${OUT_DATASET_FILENAME}_norm");
                scripts.add("      OUT_DATASET=${OUT_DATASET}_norm");
                scripts.add("      echo \"------\"");
                scripts.add("      echo \"About to run normavg.sh\"");
                scripts.add("      echo \"IN_DATASET =\" ${IN_DATASET}");
                scripts.add("      echo \"IN_DATASET_FILENAME =\" ${IN_DATASET_FILENAME}");
                scripts.add("      echo \"OUT_DATASET=\" ${OUT_DATASET}");
                scripts.add("      echo \"OUT_DATASET_FILENAME=\" ${OUT_DATASET_FILENAME}");
                scripts.add("      echo \"------\"");
                scripts.add("");
                scripts.add("      echo \"$QISEISMICS\" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat");
                scripts.add("      bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}");
                scripts.add("");
                scripts.add("      bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${IN_DATASET}.dat filename=${IN_DATASET_FILENAME} keys=ep,cdp,cdpt | \\");
                scripts.add("      #write this to a temporaary directory");
                scripts.add("      bhpavg nx=${nx} > tmp/avg_${IN_DATASET_FILENAME}_merge.su");
                scripts.add("");
                scripts.add("      bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${IN_DATASET}.dat filename=${IN_DATASET_FILENAME} keys=ep,cdp,cdpt |\\");
                scripts.add("      bhpnorm gamma=${gamma} nx=${nx} avg=tmp/avg_${IN_DATASET_FILENAME}_merge.su |\\");
                scripts.add("      bhpwritecube init=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME} \\");
                scripts.add("          key1=ep,${minep},${incep},${nep} key3=cdp,${mincdp},${inccdp},${ncdp} key2=cdpt,1,1,${number_scales}");
                scripts.add("");
                scripts.add("      return_code=$?");
                scripts.add("      if [ ${return_code} -ne 0 ]");
                scripts.add("      then");
                scripts.add("        echo \"Error in running bhpread | bhpnorm | bhpwritecube\"");
                scripts.add("        echo \"$0 job ends abnormally\"");
                scripts.add("        echo \"exit_status=$return_code\"");
                scripts.add("        exit $return_code");
                scripts.add("      fi");
                scripts.add("");
                scripts.add("      rm tmp/avg_${IN_DATASET_FILENAME}_merge.su");
                scripts.add("      echo \"end of normavg.sh\"");
                scripts.add("");
            }

            if(landmarkOutRadioButton.isSelected()){
                scripts.add("      old_ow_pmpath=$OW_PMPATH");
                scripts.add("      export OW_PMPATH=$OW_PMPATH_DM");
                scripts.add("      echo \"Before Landmark access OW_PMPATH=$OW_PMPATH\"");
                for(int i = 0; landmark2DOutputScaleTextFieldList != null && i < landmark2DOutputScaleTextFieldList.size(); i++){
                    String scale = landmark2DOutputScaleTextFieldList.get(i).getText().trim();
                    int num = Integer.valueOf(scale).intValue();
                    if(num < 10)
                        scale = "0" + scale;
                    scripts.add("      bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}\\");
                    scripts.add("        keys=ep,cdpt,cdp keylist=*:" + scale + ":* |\\");
                    scripts.add("      lmwrite2d lm_project=${lm_out_project} lm_ptype=${lm_out_project_type} lm_plevel=${lm_2d_out_process_level}_" + scale + " lm_lines=${line_shotpoint_detail[${jj}]}");
                    scripts.add("      return_code=$?");
                    scripts.add("      if [ ${return_code} -ne 0 ]");
                    scripts.add("      then");
                    scripts.add("        echo \"Error in running bhpread | lmwrite2d\"");
                    scripts.add("        echo \"$0 job ends abnormally\"");
                    scripts.add("        echo \"exit_status=$return_code\"");
                    scripts.add("        exit $return_code");
                    scripts.add("      fi");
                    scripts.add("");
                }
                scripts.add("      export OW_PMPATH=$old_ow_pmpath");
                scripts.add("      echo \"After Landmark access OW_PMPATH=$OW_PMPATH\"");
            }
            scripts.add("   fi");
            scripts.add("done");
        }


        if(bhpsuInRadioButton.isSelected() || landmark3DRadioButton.isSelected()){
            List<String> horizonList = new ArrayList<String>();
            if(landmarkHorizonRadioButton.isSelected() == true){
                if(!validateFlatteningInfo(landmarkHorzLabelList, landmarkHorzTextFieldList, landmarkStartTimeTextField, landmarkEndTimeTextField, landmarkHorzTimeTextFieldList))
                    return null;
                for (int i = 0; landmarkHorzTextFieldList != null && i < landmarkHorzTextFieldList.size(); i++) {
                    horizonList.add(landmarkHorzTextFieldList.get(i).getText() + ".xyz");
                }
                scripts.add("if [ ! -d ${QISPACE}/${QIPROJECT}/${QIHORIZONS} ]");
                scripts.add("then");
                scripts.add("  mkdir ${QISPACE}/${QIPROJECT}/${QIHORIZONS}");
                scripts.add("fi");
                scripts.add("lmexphor lm_project=${lm_in_project} lm_ptype=${lm_in_project_type} \\");
                scripts.add("  lm_horizons=${lm_horizon_files} minline=${lm_min_line} maxline=${lm_max_line} \\");
                scripts.add("  mintrace=${lm_min_trace} maxtrace=${lm_max_trace} dir=${QISPACE}/${QIPROJECT}/${QIHORIZONS}");
                scripts.add("");
                scripts.add("return_code=$?");
                scripts.add("if [ ${return_code} -ne 0 ]");
                scripts.add("then");
                scripts.add("  echo \"Error in running lmexphor\"");
                scripts.add("  echo \"$0 job ends abnormally\"");
                scripts.add("  echo \"exit_status=$return_code\"");
                scripts.add("  exit $return_code");
                scripts.add("fi");
                scripts.add("");
            }else{
                if (validateFlatteningInfo(suHorzLabelList,suHorzTextFieldList,suStartTimeTextField,suEndTimeTextField,suHorzTimeTextFieldList) == false)
                    return null;
                if(bhpsuHorizonDatasetRadioButton.isSelected()){
                    scripts.add("");
                    scripts.add("echo \"Export horizon(s) from horizon dataset\"");

                    for (int i = 0; suHorzTextFieldList != null && i < suHorzTextFieldList.size(); i++) {
                        String temp = suHorzTextFieldList.get(i).getText();
                        String[] horizns = temp.split(":");
                        String dataset = horizns[0];
                        String horizon_name = horizns[1];
                        scripts.add("bhpread keys=ep,cdp filename=" + dataset + " pathlist=${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + dataset + ".dat \\");
                        scripts.add("horizons=" + horizon_name + " |\\");
                        scripts.add("bhpstorehdr keys=ep,cdp horizons=yes file=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/" + horizon_name + " | \\");
                        scripts.add("surange");
                        scripts.add("");
                        scripts.add("return_code=$?");
                        scripts.add("if [ ${return_code} -ne 0 ]");
                        scripts.add("then");
                        scripts.add("  echo \"Error in running bhpread | bhpstorehdr | surange\"");
                        scripts.add("  echo \"$0 job ends abnormally\"");
                        scripts.add("  echo \"exit_status=$return_code\"");
                        scripts.add("  exit $return_code");
                        scripts.add("fi");
                        scripts.add("");
                        horizonList.add(horizon_name);
                    }
                    scripts.add("echo \"Export horizon(s) from horizon dataset completed.\"");
                    scripts.add("");
                }else if(bhpsuHorizonRadioButton.isSelected()){
                    for (int i = 0; suHorzTextFieldList != null && i < suHorzTextFieldList.size(); i++) {
                        String temp = suHorzTextFieldList.get(i).getText();
                        horizonList.add(temp);
                    }
                } 
            }
            if(horizonList.size() > 0){
                scripts.add("");
                scripts.add("#");
                scripts.add("# slice_all.sh");
                scripts.add("#");
                scripts.add("IN_DATASET_FILENAME=${OUT_DATASET_FILENAME}");
                scripts.add("IN_DATASET=${OUT_DATASET}");
                scripts.add("OUT_DATASET_FILENAME=${OUT_DATASET_FILENAME}_slice");
                scripts.add("OUT_DATASET=${OUT_DATASET}_slice");
                scripts.add("echo \"------\"");
                scripts.add("echo \"About to run slice_all.sh\"");
                scripts.add("echo \"IN_DATASET =\" ${IN_DATASET}");
                scripts.add("echo \"IN_DATASET_FILENAME =\" ${IN_DATASET_FILENAME}");
                scripts.add("echo \"OUT_DATASET=\" ${OUT_DATASET}");
                scripts.add("echo \"OUT_DATASET_FILENAME=\" ${OUT_DATASET_FILENAME}");
                scripts.add("echo \"------\"");
                scripts.add("");
                scripts.add("");
                scripts.add("echo \"$QISEISMICS\" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat");
                scripts.add("bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}");
                String[] float_headers = {"f1","f2","d1","d2","ungpow","unscale"};
                int numberOfHeaders = 0;
                if(float_headers.length >= horizonList.size())
                    numberOfHeaders = horizonList.size();
                else
                    numberOfHeaders = float_headers.length;
                String command= "bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${IN_DATASET}.dat filename=${IN_DATASET_FILENAME} keys=ep,cdpt,cdp\\\n";
                command += "  keylist=${minep}-${maxep}:*:${mincdp}-${maxcdp}";
                StringBuffer commandBuf = new StringBuffer(command);
                String slice_cmd = "| \\\nbhpslice hlist=";
                StringBuffer slice_cmdBuf = new StringBuffer(slice_cmd);
                String key_cmd_base = "| \\\nsushw key=";
                StringBuffer key_cmd = new StringBuffer();
                for(int i =0; i < numberOfHeaders; i++){
                    if(i == 0)
                       key_cmd.append(key_cmd_base).append(float_headers[i]).append(" a=${sec} ");
                    else
                       key_cmd.append(key_cmd_base).append(float_headers[i]).append(" a=0 ");
                }

                for(int i =0; i < numberOfHeaders; i++){
                    commandBuf.append(" | \\\nbhploadhdr key=ep,cdp,").append(float_headers[i]).append(" infile=${QIHORIZONS}/").append(horizonList.get(i)).append(" gridtype=R extrap=no ");
                    slice_cmdBuf.append(float_headers[i]);
                    if(i < (numberOfHeaders-1))
                        slice_cmdBuf.append(",");
                }
                slice_cmdBuf.append(" times=${horizon_times} tmin=${tmin} tmax=${tmax} dt=${dt} ");
                commandBuf.append(slice_cmdBuf.toString());
                commandBuf.append(key_cmd.toString());
                commandBuf.append(" | \\\nbhpwritecube init=yes key3=cdp,${mincdp},${inccdp},${ncdp} key2=cdpt,1,1,${number_scales} key1=ep,${minep},${incep},${nep} \\\n");
                commandBuf.append("  pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}");
                scripts.add(commandBuf.toString());
                scripts.add("");
                scripts.add("return_code=$?");
                scripts.add("if [ ${return_code} -ne 0 ]");
                scripts.add("then");
                scripts.add("  echo \"Error in running bhpread | bhploadhdr | bhpslice | sushw | bhpwritecube\"");
                scripts.add("  echo \"$0 job ends abnormally\"");
                scripts.add("  echo \"exit_status=$return_code\"");
                scripts.add("  exit $return_code");
                scripts.add("fi");
                scripts.add("");
                scripts.add("IN_DATASET_FILENAME=${OUT_DATASET_FILENAME}");
                scripts.add("IN_DATASET=${OUT_DATASET}");
                scripts.add("OUT_DATASET_FILENAME=${OUT_DATASET_FILENAME}_transpose_ep");
                scripts.add("OUT_DATASET=${OUT_DATASET}_transpose_ep");
                scripts.add("");
                scripts.add("echo \"------\"");
                scripts.add("echo \"About to run bhptranspose\"");
                scripts.add("echo \"IN_DATASET =\" ${IN_DATASET}");
                scripts.add("echo \"IN_DATASET_FILENAME =\" ${IN_DATASET_FILENAME}");
                scripts.add("echo \"OUT_DATASET=\" ${OUT_DATASET}");
                scripts.add("echo \"OUT_DATASET_FILENAME=\" ${OUT_DATASET_FILENAME}");
                scripts.add("echo \"------\"");
                scripts.add("");
                scripts.add("echo \"$QISEISMICS\" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat");
                scripts.add("bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}");
                scripts.add("");
                scripts.add("bhptranspose pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat  filename=${IN_DATASET_FILENAME}");
                scripts.add("");
                scripts.add("return_code=$?");
                scripts.add("if [ ${return_code} -ne 0 ]");
                scripts.add("then");
                scripts.add("  echo \"Error in running bhptranspose\"");
                scripts.add("  echo \"$0 job ends abnormally\"");
                scripts.add("  echo \"exit_status=$return_code\"");
                scripts.add("  exit $return_code");
                scripts.add("fi");
                scripts.add("");
                scripts.add("echo \"end of slice_all.sh for the spec decomp\"");
            }
        }

        if(output_type.equals("landmark3D")){
            scripts.add("");
            scripts.add("#write output data to Landmark 3D");
            scripts.add("ii=0;");
            scripts.add("IFS=,");
            scripts.add("for i in ${out_scale_list}");
            scripts.add("do");
            scripts.add("scale_array[${ii}]=$i");
            scripts.add("ii=`expr ${ii} + 1`");
            scripts.add("done");
            scripts.add("");
            scripts.add("for ((  jj = 0 ;  jj < ${#scale_array[*]};  jj++  ))");
            scripts.add("do");
            scripts.add("  echo \"scale_array ${scale_array[${jj}]}\"");
            scripts.add("  old_ow_pmpath=$OW_PMPATH");
            scripts.add("  export OW_PMPATH=$OW_PMPATH_DM");
            scripts.add("  echo \"Before Landmark access OW_PMPATH=$OW_PMPATH\"");
            scripts.add("  bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME} keys=ep,cdpt,cdp keylist=*:${scale_array[$jj]}:* |");
            scripts.add("  lmwrite lm_project=${lm_out_project} lm_ptype=${lm_out_project_type} lm_brickname=${OUT_DATASET_FILENAME}_${scale_array[$jj]}.bri");

            scripts.add("");
            scripts.add("  return_code=$?");
            scripts.add("  if [ ${return_code} -ne 0 ]");
            scripts.add("  then");
            scripts.add("    echo \"Error in running bhpread | lmwrite\"");
            scripts.add("    echo \"$0 job ends abnormally\"");
            scripts.add("    echo \"exit_status=$return_code\"");
            scripts.add("    exit $return_code");
            scripts.add("  fi");
            scripts.add("");
            scripts.add("  export OW_PMPATH=$old_ow_pmpath");
            scripts.add("  echo \"After Landmark access OW_PMPATH=$OW_PMPATH\"");
            scripts.add("done");
        }

        //scripts.add("exit_status=$?");
        //scripts.add("echo \"exit_status=$exit_status\"");
        //scripts.add("if [ ${exit_status} != 0 ]");
        //scripts.add("then");
        //scripts.add("    echo \"$0 job ends abnormally\"");
        //scripts.add("    exit 1");
        //scripts.add("fi");
        scripts.add("echo \"$0 job ends normally\"");
        return scripts;
    }

    private boolean notAskAgainForOverwite = false;
    private String formSUScripts(String templ) {
        final Component comp = this;
        String template = templ;
        //String project = agent.getMessagingMgr().getProject();

        String qiSpace = QiProjectDescUtils.getQiSpace(agent.getProjectDescriptor());
        String qiProjectReloc = QiProjectDescUtils.getQiProjectReloc(agent.getProjectDescriptor());
        String project = QiProjectDescUtils.getProjectPath(agent.getProjectDescriptor());
        String datasets = QiProjectDescUtils.getDatasetsReloc(agent.getProjectDescriptor());
        template = template.replace("<QI_SPACE>", qiSpace);
        template = template.replace("<QI_PROJECT_LOCATION>", qiProjectReloc);
        template = template.replace("<QI_DATASETS>",datasets);
        template = template.replace("<QI_HORIZONS>", QiProjectDescUtils.getHorizonsReloc(agent.getProjectDescriptor()));
        List<String> list = QiProjectDescUtils.getSeismicDirs(agent.getProjectDescriptor());
        StringBuffer seismics = new StringBuffer();
        for(int i = 0; list != null && i < list.size(); i++){
            seismics.append(project + filesep + list.get(i));
            if(i < list.size() -1){
                seismics.append("\n");
            }
        }
        template = template.replace("<QI_SEISMICS>", seismics.toString());
        //template = template.replace("<OUT_DATASET_DIRECTORY>", QiProjectDescUtils.getDatasetsReloc(agent.getProjectDescriptor()));
        if(landmarkInRadioButton.isSelected()){
            if(!validateLandmark())
                return null;
            template = template.replace("<IN_DATASET>", "");
            template = template.replace("<IN_DATASET_FILENAME>", "");
            if(landmark2DRadioButton.isSelected()){
                template = template.replace("<INPUT_TYPE>", "landmark2D");
                String text = landmarkIn2DProjectTextField.getText().trim();
                template = template.replace("<LANDMARK_IN_PROJECT>", text);
                template = template.replace("<LANDMARK_IN_PROJECT_TYPE>", "2");
                template = template.replace("<LANDMARK_IN_VOLUME>", "");
                template = template.replace("<LANDMARK_IN_VOLUME_BASE>", "");
                //template = template.replace("<LANDMARK_OUT_VOLUME>","");
                template = template.replace("<LANDMARK_MIN_LINE>", "");
                template = template.replace("<LANDMARK_MAX_LINE>", "");
                template = template.replace("<LANDMARK_INC_LINE>", "");
                template = template.replace("<LANDMARK_MIN_TRACE>", "");
                template = template.replace("<LANDMARK_MAX_TRACE>", "");
                template = template.replace("<LANDMARK_INC_TRACE>", "");
                template = template.replace("<LANDMARK_MIN_TIME>", "");
                template = template.replace("<LANDMARK_MAX_TIME>", "");

                template = template.replace("<MIN_CDP>", "");
                template = template.replace("<MAX_CDP>", "");
                template = template.replace("<INC_CDP>", "");
                template = template.replace("<MIN_EP>", "");
                template = template.replace("<MAX_EP>", "");
                template = template.replace("<INC_EP>", "");

                String plevel = (String)seismic2DFileComboBox.getSelectedItem();
                template = template.replace("<LANDMARK_2D_IN_PROCESS_LEVEL>", plevel);
                convertShotpointsToTraces();

                Object[]  keys = selectedLineMap.keySet().toArray();
                StringBuffer shotpoints = new StringBuffer();
                StringBuffer traces = new StringBuffer();
                for(int i = 0; keys!= null && i < keys.length; i++){
                    LineInfo li = selectedLineMap.get((String)keys[i]);
                    shotpoints.append(li.lineName + ":" + li.lineNumber + ":" + li.minShotPoint + ":" + li.maxShotPoint + ":" + li.incShotPoint);
                    traces.append(li.lineName + ":" + li.lineNumber + ":" + li.minTrace + ":" + li.maxTrace + ":" + li.incTrace);
                    if(i < (keys.length - 1)){
                        shotpoints.append(",");
                        traces.append(",");
                    }
                }
                template = template.replace("<LANDMARK_LINE_SHOTPOINT_LIST>", shotpoints.toString());
                template = template.replace("<LANDMARK_LINE_TRACE_LIST>", traces.toString());
            }else if(landmark3DRadioButton.isSelected()){
                template = template.replace("<INPUT_TYPE>", "landmark3D");
                String text = landmarkInProjectTextField.getText().trim();
                template = template.replace("<LANDMARK_IN_PROJECT>", text);
                template = template.replace("<LANDMARK_IN_PROJECT_TYPE>", "3");
                text = volumeTextField.getText();
                template = template.replace("<LANDMARK_IN_VOLUME>", text);
                int id = text.indexOf(".bri");
                if(id != -1){
                    text = text.substring(0, id);
                    template = template.replace("<LANDMARK_IN_VOLUME_BASE>", text);
                }else{
                    template = template.replace("<LANDMARK_IN_VOLUME_BASE>", text);
                }

                //template = template.replace("<LANDMARK_OUT_VOLUME>","");

                String line = linesTextField.getText();
                String lineTo = linesToTextField.getText();
                String lineInc = linesByTextField.getText();
                template = template.replace("<LANDMARK_MIN_LINE>", line);
                template = template.replace("<LANDMARK_MAX_LINE>", lineTo);
                template = template.replace("<LANDMARK_INC_LINE>", lineInc);
                String traces = tracesTextField.getText();
                String tracesTo = tracesToTextField.getText();
                String tracesInc = tracesByTextField.getText();
                template = template.replace("<LANDMARK_MIN_TRACE>", traces);
                template = template.replace("<LANDMARK_MAX_TRACE>", tracesTo);
                template = template.replace("<LANDMARK_INC_TRACE>", tracesInc);
                String time = timeTextField.getText().trim();
                String timeTo = timeToTextField.getText().trim();

                template = template.replace("<LANDMARK_MIN_TIME>", time);
                template = template.replace("<LANDMARK_MAX_TIME>", timeTo);

                template = template.replace("<MIN_CDP>", "");
                template = template.replace("<MAX_CDP>", "");
                template = template.replace("<INC_CDP>", "");
                template = template.replace("<MIN_EP>", "");
                template = template.replace("<MAX_EP>", "");
                template = template.replace("<INC_EP>", "");
                template = template.replace("<LANDMARK_2D_IN_PROCESS_LEVEL>", "");
                template = template.replace("<LANDMARK_2D_OUT_PROCESS_LEVEL>", "");
                template = template.replace("<LANDMARK_LINE_SHOTPOINT_LIST>", "");
                template = template.replace("<LANDMARK_LINE_TRACE_LIST>", "");

            }
        }else{
            if(!validateBHPSU())
                return null;

            template = template.replace("<INPUT_TYPE>", "bhpsu");
            template = template.replace("<LANDMARK_IN_PROJECT>", "");
            template = template.replace("<LANDMARK_IN_PROJECT_TYPE>", "");
            template = template.replace("<LANDMARK_IN_VOLUME>", "");
            template = template.replace("<LANDMARK_IN_VOLUME_BASE>", "");
            template = template.replace("<LANDMARK_MIN_LINE>", "");
            template = template.replace("<LANDMARK_MAX_LINE>", "");
            template = template.replace("<LANDMARK_INC_LINE>", "");
            template = template.replace("<LANDMARK_MIN_TRACE>", "");
            template = template.replace("<LANDMARK_MAX_TRACE>", "");
            template = template.replace("<LANDMARK_INC_TRACE>", "");
            template = template.replace("<LANDMARK_MIN_TIME>", "");
            template = template.replace("<LANDMARK_MAX_TIME>", "");

            template = template.replace("<LANDMARK_2D_IN_PROCESS_LEVEL>", "");
            template = template.replace("<LANDMARK_2D_OUT_PROCESS_LEVEL>", "");
            template = template.replace("<LANDMARK_LINE_SHOTPOINT_LIST>", "");
            template = template.replace("<LANDMARK_LINE_TRACE_LIST>", "");

            String text = datasetTextField.getText();
            if(text != null && text.trim().length() > 0){
                int ind = text.lastIndexOf(filesep);
                if( ind != -1){
                    String name = text.substring(text.lastIndexOf(filesep) + 1, text.lastIndexOf("."));
                    template = template.replace("<IN_DATASET_FILENAME>", name);
                    String inDataSet = text.substring(ind,text.lastIndexOf("."));
                    template = template.replace("<IN_DATASET>", inDataSet);
                }else{
                    template = template.replace("<IN_DATASET_FILENAME>", text.substring(0,text.lastIndexOf(".")));
                    template = template.replace("<IN_DATASET>", text.substring(0,text.lastIndexOf(".")));
                }
            }else{
                template = template.replace("<IN_DATASET>", "");
                template = template.replace("<IN_DATASET_FILENAME>", "");
            }


            String mincdp = cdpTextField.getText().trim();
            template = template.replace("<MIN_CDP>", mincdp);

            String maxcdp = cdpToTextField.getText().trim();
            template = template.replace("<MAX_CDP>", maxcdp);

            String inccdp = cdpByTextField.getText().trim();
            template = template.replace("<INC_CDP>", inccdp);

            String minep = epTextField.getText().trim();
            template = template.replace("<MIN_EP>", minep);

            String maxep = epToTextField.getText().trim();
            template = template.replace("<MAX_EP>", maxep);

            String incep = epByTextField.getText().trim();
            template = template.replace("<INC_EP>", incep);

        }
        if(landmarkHorizonRadioButton.isSelected() == true){
            if(!validateFlatteningInfo(landmarkHorzLabelList, landmarkHorzTextFieldList, landmarkStartTimeTextField, landmarkEndTimeTextField, landmarkHorzTimeTextFieldList))
                return null;
        }else{
            if (validateFlatteningInfo(suHorzLabelList,suHorzTextFieldList,suStartTimeTextField,suEndTimeTextField,suHorzTimeTextFieldList) == false)
                return null;
        }

            if(landmarkHorizonRadioButton.isSelected()){
                template = template.replace("<HORIZON_TYPE>", "landmark");
                StringBuffer string = new StringBuffer();
                StringBuffer string1 = new StringBuffer();
                for (int i = 0; landmarkHorzTextFieldList != null && i < landmarkHorzTextFieldList.size(); i++) {
                    String temp = landmarkHorzTextFieldList.get(i).getText();
                    string1.append(temp);
                    //string.append(agent.getProjectDescriptor().getHorizonsReloc() + filesep + temp + ".xyz");
                    string.append(temp + ".xyz");
                    if (i < landmarkHorzTextFieldList.size() - 1){
                        string.append(",");
                        string1.append(",");
                    }
                }

                template = template.replace("<HORIZON_FILE_LIST>", string);
                template = template.replace("<LANDMARK_HORIZON_LIST>", string1);
                if(string.length() == 0)
                    template = template.replace("<RUN_SLICE_FLAG>", "no");
                else
                    template = template.replace("<RUN_SLICE_FLAG>", "yes");
                String tmin = landmarkStartTimeTextField.getText().trim();
                if(tmin == null || tmin.trim().length() == 0)
                	tmin = "0";
                template = template.replace("<START_TIME>", tmin);
                String tmax = landmarkEndTimeTextField.getText().trim();
                if(tmax == null || tmax.trim().length() == 0)
                	tmax = "0";
                template = template.replace("<END_TIME>", tmax);

                string = new StringBuffer();
                for (int i = 0; landmarkHorzTimeTextFieldList != null && i < landmarkHorzTimeTextFieldList.size(); i++) {
                    string.append(landmarkHorzTimeTextFieldList.get(i).getText());
                    if (i < landmarkHorzTimeTextFieldList.size() - 1)
                        string.append(",");
                }
                template = template.replace("<HORIZON_TIMES_LIST>", string.toString());

                String text = sampleRateTextField.getText().trim();

                template = template.replace("<TIME_INCREMENT>", text);
            }



            if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            	if(bhpsuHorizonRadioButton.isSelected())
            		template = template.replace("<HORIZON_TYPE>", "ASCII");
            	else if(bhpsuHorizonDatasetRadioButton.isSelected())
            		template = template.replace("<HORIZON_TYPE>", "bhpsu");
                template = template.replace("<LANDMARK_HORIZON_LIST>", "");
                StringBuffer string = new StringBuffer();
                for (int i = 0; suHorzTextFieldList != null && i < suHorzTextFieldList.size(); i++) {
                    String temp = suHorzTextFieldList.get(i).getText();
                    //if (!temp.startsWith(project)) {
                    //  JOptionPane.showMessageDialog(comp,
                    //          "The horizon data field should start with project directory "
                    //                  + project, "Incorrect data entry",
                    //          JOptionPane.WARNING_MESSAGE);
                    //  return null;
                    //}
                    //string += temp.substring(project.length() + 1);
                    //string.append(QiProjectDescUtils.getHorizonsReloc(agent.getProjectDescriptor()) + filesep + temp);
                    string.append(temp);
                    if (i < suHorzTextFieldList.size() - 1)
                        string.append(",");
                }
                if(string.length() == 0)
                    template = template.replace("<RUN_SLICE_FLAG>", "no");
                else
                    template = template.replace("<RUN_SLICE_FLAG>", "yes");

                template = template.replace("<HORIZON_FILE_LIST>", string.toString());


                String tmin = suStartTimeTextField.getText().trim();
                if(tmin == null || tmin.trim().length() == 0)
                	tmin = "0";
                template = template.replace("<START_TIME>", tmin);
                String tmax = suEndTimeTextField.getText().trim();
                if(tmax == null || tmax.trim().length() == 0)
                	tmax = "0";
                template = template.replace("<END_TIME>", tmax);

                string = new StringBuffer();
                for (int i = 0; suHorzTimeTextFieldList != null && i < suHorzTimeTextFieldList.size(); i++) {
                    string.append(suHorzTimeTextFieldList.get(i).getText());
                    if (i < suHorzTimeTextFieldList.size() - 1)
                        string.append(",");
                }
                template = template.replace("<HORIZON_TIMES_LIST>", string.toString());

                //if (validateTimeIncrementTextField() == false)
                //  return null;
                String text = timeIncrementTextField.getText().trim();
                template = template.replace("<TIME_INCREMENT>", text);
            }
        //}

        if( landmarkOutRadioButton.isSelected() ){
            if(!validateLandmarkOutput())
                return null;
            if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                String text = landmark2DNameBaseTextField.getText();
                if(text != null && text.trim().length() > 0){
                    int ind = text.lastIndexOf(filesep);
                    String temp = "";
                    if(ind == -1){
                        template = template.replace("<OUT_DATASET>", text);
                        template = template.replace("<OUT_DATASET_DIR>", "");
                        template = template.replace("<OUT_DATASET_FILENAME>",text);
                    }else{
                        String tmp = text.substring(0, ind);
                        template = template.replace("<OUT_DATASET>", text);
                        template = template.replace("<OUT_DATASET_DIR>", tmp);
                        template = template.replace("<OUT_DATASET_FILENAME>",text.substring(ind+1));
                        text = text.substring(ind+1);
                    }
                    if(text.length() > 6)
                        temp = text.substring(0, 6);
                    else if(text.length() < 6){
                        temp = text;
                        StringBuffer tempB = new StringBuffer();
                        for(int i = 0; i < 6 - text.length(); i++){
                            tempB.append("0");
                        }
                        temp += tempB.toString();
                    }else
                        temp = text;
                    template = template.replace("<LM_2D_OUT_PROCESS_LEVEL>",temp);
                }
                template = template.replace("<LANDMARK_OUT_PROJECT>",landmark2DOutProjectTextField.getText().trim());
                template = template.replace("<LANDMARK_OUT_PROJECT_TYPE>", "2");
                template = template.replace("<OUTPUT_TYPE>", "landmark2D");
                if (landmark2DScaleNormalizedRadioButton.isSelected()) {
                    template = template.replace("<RUN_NORMALIZATION_FLAG>", "yes");
                    text = landmark2DNoiseFactorTextField.getText().trim();
                    template = template.replace("<GAMMA_VALUE>", text);
                } else {
                    template = template.replace("<RUN_NORMALIZATION_FLAG>", "no");
                    template = template.replace("<GAMMA_VALUE>", String.valueOf(defaultGamma));
                }

                StringBuffer scales = new StringBuffer();
                for(int i = 0; landmark2DOutputScaleTextFieldList != null && i < landmark2DOutputScaleTextFieldList.size(); i++){
                    String scale = landmark2DOutputScaleTextFieldList.get(i).getText().trim();
                    int num = Integer.valueOf(scale).intValue();
                    if(num < 10)
                        scale = "0" + scale;
                    scales.append(scale);
                    if(i < (landmark2DOutputScaleTextFieldList.size()-1))
                        scales.append(",");
                }
                template = template.replace("<OUTPUT_SCALE_LIST>", scales.toString());

            }else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                String text = landmark3DNameBaseTextField.getText();
                if(text != null && text.trim().length() > 0){
                    int ind = text.lastIndexOf(filesep);
                    if(ind == -1){
                        template = template.replace("<OUT_DATASET>", text);
                        template = template.replace("<OUT_DATASET_DIR>", "");
                        template = template.replace("<OUT_DATASET_FILENAME>",text);
                    }else{
                        String tmp = text.substring(0, ind);
                        template = template.replace("<OUT_DATASET>", text);
                        template = template.replace("<OUT_DATASET_DIR>", tmp);
                        template = template.replace("<OUT_DATASET_FILENAME>",text.substring(ind+1));
                        text = text.substring(ind+1);
                    }
                }
                String []  vlist = landmarkAgent.getLandmarkVolumeList(landmark3DOutProjectTextField.getText().trim(),
                        landmarkAgent.getLandmarkProjectType(landmark3DOutProjectTextField.getText().trim())
                        , ".bri");
                boolean exist = false;
                for(int i = 0; vlist != null && i < vlist.length; i++){
                    if(vlist[i].equalsIgnoreCase(text + ".bri"))
                        exist = true;
                }
                if(exist == true && notAskAgainForOverwite == false){
                    String message = text + ".bri exists in the project. Do you want to overwrite it when you run the script?";
                    ConfirmOverwriteDialog confirm = new ConfirmOverwriteDialog(JOptionPane.getFrameForComponent(this), true, message);
                    confirm.setVisible(true);
                    int returnStatus = confirm.getReturnStatus();
                    notAskAgainForOverwite = confirm.getNotAskAgain();
                    if(returnStatus == ConfirmOverwriteDialog.RET_NO)
                        return null;
                }
                template = template.replace("<LANDMARK_OUT_PROJECT>",landmark3DOutProjectTextField.getText().trim());
                template = template.replace("<LANDMARK_OUT_PROJECT_TYPE>", "3");
                template = template.replace("<OUTPUT_TYPE>", "landmark3D");
                if (landmark3DScaleNormalizedRadioButton.isSelected()) {
                    template = template.replace("<RUN_NORMALIZATION_FLAG>", "yes");
                    text = landmark3DNoiseFactorTextField.getText().trim();
                    template = template.replace("<GAMMA_VALUE>", text);
                } else {
                    template = template.replace("<RUN_NORMALIZATION_FLAG>", "no");
                    template = template.replace("<GAMMA_VALUE>", String.valueOf(defaultGamma));
                }

                StringBuffer scales = new StringBuffer();
                for(int i = 0; landmark3DOutputScaleTextFieldList != null && i < landmark3DOutputScaleTextFieldList.size(); i++){
                    scales.append(landmark3DOutputScaleTextFieldList.get(i).getText().trim());
                    if(i < (landmark3DOutputScaleTextFieldList.size()-1))
                        scales.append(",");
                }
                template = template.replace("<OUTPUT_SCALE_LIST>", scales.toString());
            }else{ //bhpsu input landmark output
                String text = landmarkNameBaseTextField.getText();
                if(text != null && text.trim().length() > 0){
                    int ind = text.lastIndexOf(filesep);
                    if(ind == -1){
                        template = template.replace("<OUT_DATASET>", text);
                        template = template.replace("<OUT_DATASET_DIR>", "");
                        template = template.replace("<OUT_DATASET_FILENAME>",text);
                    }else{
                        String tmp = text.substring(0, ind);
                        template = template.replace("<OUT_DATASET>", text);
                        template = template.replace("<OUT_DATASET_DIR>", tmp);
                        template = template.replace("<OUT_DATASET_FILENAME>",text.substring(ind+1));
                        text = text.substring(ind+1);
                    }
                }

                int ptype = landmarkAgent.getLandmarkProjectType(landmarkOutProjectTextField.getText().trim());
                if(ptype == 3){
                    String []  vlist = landmarkAgent.getLandmarkVolumeList(landmarkOutProjectTextField.getText().trim(),
                        landmarkAgent.getLandmarkProjectType(landmarkOutProjectTextField.getText().trim())
                        , ".bri");
                    boolean exist = false;
                    for(int i = 0; vlist != null && i < vlist.length; i++){
                        if(vlist[i].equalsIgnoreCase(text + ".bri"))
                            exist = true;
                    }
                    if(exist == true && notAskAgainForOverwite == false){
                        String message = text + ".bri exists in the project. Do you want to overwrite it when you run the script?";
                        ConfirmOverwriteDialog confirm = new ConfirmOverwriteDialog(JOptionPane.getFrameForComponent(this), true, message);
                        confirm.setVisible(true);
                        int returnStatus = confirm.getReturnStatus();
                        notAskAgainForOverwite = confirm.getNotAskAgain();
                        if(returnStatus == ConfirmOverwriteDialog.RET_NO)
                            return null;
                    }
                }
                template = template.replace("<LANDMARK_OUT_PROJECT>",landmarkOutProjectTextField.getText().trim());
                if(ptype == 2){
                    template = template.replace("<LANDMARK_OUT_PROJECT_TYPE>", "2");
                    template = template.replace("<OUTPUT_TYPE>", "landmark2D");
                }else if(ptype == 3){
                    template = template.replace("<LANDMARK_OUT_PROJECT_TYPE>", "3");
                    template = template.replace("<OUTPUT_TYPE>", "landmark3D");
                }
                if (landmarkScaleNormalizedRadioButton.isSelected()) {
                    template = template.replace("<RUN_NORMALIZATION_FLAG>", "yes");
                    text = landmarkNoiseFactorTextField.getText().trim();
                    template = template.replace("<GAMMA_VALUE>", text);
                } else {
                    template = template.replace("<RUN_NORMALIZATION_FLAG>", "no");
                    template = template.replace("<GAMMA_VALUE>", String.valueOf(defaultGamma));
                }

                StringBuffer scales = new StringBuffer();
                for(int i = 0; landmarkOutputScaleTextFieldList != null && i < landmarkOutputScaleTextFieldList.size(); i++){
                    scales.append(landmarkOutputScaleTextFieldList.get(i).getText().trim());
                    if(i < (landmarkOutputScaleTextFieldList.size()-1))
                        scales.append(",");
                }
                template = template.replace("<OUTPUT_SCALE_LIST>", scales.toString());
            }
        }else{
            if(!validateBHPSUOutput())
                return null;

            String text = bhpsuNameBaseTextField.getText();
            template = template.replace("<LANDMARK_OUT_PROJECT>","");
            template = template.replace("<LANDMARK_OUT_PROJECT_TYPE>","");
            //template = template.replace("<LANDMARK_OUT_VOLUME>","");

            if(text != null && text.trim().length() > 0){
                int ind = text.lastIndexOf(filesep);
                if(ind == -1){
                    template = template.replace("<OUT_DATASET>", text);
                    template = template.replace("<OUT_DATASET_DIR>", "");
                    template = template.replace("<OUT_DATASET_FILENAME>",text);
                }else{
                    String tmp = text.substring(0, ind);
                    template = template.replace("<OUT_DATASET>", text);
                    template = template.replace("<OUT_DATASET_DIR>", tmp);
                    template = template.replace("<OUT_DATASET_FILENAME>",text.substring(ind+1));
                }
            }

            if (bhpsuScaleNormalizedRadioButton.isSelected()) {
                template = template.replace("<RUN_NORMALIZATION_FLAG>", "yes");
                text = bhpsuNoiseFactorTextField.getText().trim();
                template = template.replace("<GAMMA_VALUE>", text);
            } else {
                template = template.replace("<RUN_NORMALIZATION_FLAG>", "no");
                //gamma default value
                template = template.replace("<GAMMA_VALUE>", String.valueOf(defaultGamma));
            }
            template = template.replace("<OUTPUT_TYPE>", "bhpsu");
            template = template.replace("<OUTPUT_SCALE_LIST>", "");
        }


        if (validateSmallestScaleTextField() == false)
            return null;
        String smallest_scale = smallestScaleTextField.getText().trim();
        template = template.replace("<SMALLEST_SCALE>", smallest_scale);

        if (validateLargestScaleTextField() == false)
            return null;

        String largest_scale = largestScaleTextField.getText().trim();
        template = template.replace("<LARGEST_SCALE>", largest_scale);

        if (validateNumberScalesTextField() == false)
            return null;

        String number_scales = numberScalesTextField.getText().trim();
        template = template.replace("<NUMBER_OF_SCALES>", number_scales);

        if (validateAverageVelocityTextField() == false)
            return null;

        String text = averageVelocityTextField.getText().trim();
        template = template.replace("<AVERGE_VELOCITY>", text);

        if (validateSmoothingLengthTextField() == false)
            return null;
        String length = smoothingLengthTextField.getText().trim();
        template = template.replace("<SMOOTHING_LENGTH>", length);

        if (validateNumberOfPassTextField() == false)
            return null;
        length = numberOfPassTextField.getText().trim();
        template = template.replace("<NUMBER_OF_PASS>", length);
        return template;
    }

    private void convertShotpointsToTraces(){
        Object[]  keys = selectedLineMap.keySet().toArray();
        if(landmarkAgent == null)
            landmarkAgent = LandmarkServices.getInstance();
        for(int i = 0; keys!= null && i < keys.length; i++){
            LineInfo li = selectedLineMap.get((String)keys[i]);
            float [] arr = landmarkAgent.convertShotpointsTo2DTraces(li.getProjectName(),(String)keys[i],
                    li.getMinShotPoint(), li.getMaxShotPoint(), li.getIncShotPoint());
            li.setMinTrace((int)arr[0]);
            li.setMaxTrace((int)arr[1]);
            li.setIncTrace((int)arr[2]);
            selectedLineMap.put((String)keys[i], li);
        }

    }
    private void chooseDatasetButtonActionPerformed(
            java.awt.event.ActionEvent evt) {

        String dir = QiProjectDescUtils.getDatasetsPath(agent.getProjectDescriptor());
        String result = agent.checkDirExist(dir);
        if(result.equals("no")){
            JOptionPane.showMessageDialog(gui,
                    "The dataset directory " + dir + " can not be found. Please check to see if the project directory is set up properly.","Error in getting dataset.", JOptionPane.WARNING_MESSAGE);
            return;
        }
        QiFileChooserDescriptor desc = new QiFileChooserDescriptor();
    	desc.setParentGUI(gui);
    	desc.setTitle("Select A Seismic Dataset");
    	desc.setHomeDirectory(QiProjectDescUtils.getDatasetsPath(agent.getQiProjectDescriptor()));
		String [] filterList = { ".dat",".DAT" };
        GenericFileFilter filter =  new GenericFileFilter(filterList,"Data Files (*.dat)");
        desc.setFileFilter(filter);
        desc.setMessageCommand(WaveletDecompConstants.FILE_CHOOSER_CMD_GET_SEISMIC_DATASET);
        Map props = new HashMap();
        props.put("target", datasetTextField);
        desc.setAdditionalProperties(props);
        desc.setDirectoryRememberedEnabled(false);
        desc.setNavigationUpwardEnabled(false);
        desc.setMultiSelectionEnabled(false);
        desc.setetProducerComponentDescriptor(agent.getComponentDescriptor());
        desc.setDialogType(javax.swing.JFileChooser.OPEN_DIALOG);
        desc.setServerUrl(agent.getMessagingMgr().getTomcatURL());
        agent.callFileChooser(desc);
    }

    private void chooseDatasetButtonActionPerformedOld(
            java.awt.event.ActionEvent evt) {

        String dir = QiProjectDescUtils.getDatasetsPath(agent.getProjectDescriptor());
        String result = agent.checkDirExist(dir);
        if(result.equals("no")){
            JOptionPane.showMessageDialog(gui,
                    "The dataset directory " + dir + " can not be found. Please check to see if the project directory is set up properly.","Error in getting dataset.", JOptionPane.WARNING_MESSAGE);
            return;
        }

        ArrayList list = new ArrayList();
        // 1st element the parent GUI object
        list.add(0, gui);
        // 2nd element is the dialog title
        list.add(1, "Select A Seismic Dataset");
        //3rd element is a list that contains current directory to start with
        //and a flag (yes or no) indicating if a already remembered directory
        //should be used instead
        ArrayList lst = new ArrayList();

        lst.add(dir);
        lst.add("no");
        list.add(2,lst);

        // 4th element is the file filter
        list.add(3, getFileFilter(new String[] { ".dat", ".DAT" }));
        // 5th element is the navigation flag
        list.add(4, false);
        // 6th element is the producer component descriptor
        list.add(5, agent.getMessagingMgr().getMyComponentDesc());
        // 7th element is the type of file chooser either Open or Save
        list.add(6, QIWConstants.FILE_CHOOSER_TYPE_OPEN);
        // 8th element is the message command
        list.add(7, WaveletDecompConstants.FILE_CHOOSER_CMD_GET_SEISMIC_DATASET);
        // 9th element is the default pretext for the file name if user fails to
        // pretext it. If no pretext is required then use empty String
        list.add(8, "");
        // 10th element is the default extension for the file name if user fails
        // to append. If no extension is required then use empty String
        list.add(9, ".dat");
        // 11th element is the target tomcat url where the file chooser chooses
        list.add(10, agent.getMessagingMgr().getTomcatURL());
        agent.callFileChooser(list);
        // datasetTextField.setText(agent.getSelectedSeismicDatasetPath());

    }


    private void chooseHorizonButtonActionPerformed(
            java.awt.event.ActionEvent evt) {

    	QiFileChooserDescriptor desc = new QiFileChooserDescriptor();
    	desc.setParentGUI(gui);
    	if(bhpsuHorizonRadioButton.isSelected()){
    		desc.setTitle("Select A Horizon Dataset");
    		desc.setHomeDirectory(QiProjectDescUtils.getHorizonsPath(agent.getQiProjectDescriptor()));
    		String [] filterList = { ".xyz", ".XYZ" };
            GenericFileFilter filter =  new GenericFileFilter(filterList,"Horizon ASCII Files (*.xyz)");
            desc.setFileFilter(filter);
            desc.setMessageCommand(WaveletDecompConstants.FILE_CHOOSER_CMD_GET_HORIZON_DATA);
    	}else if(bhpsuHorizonDatasetRadioButton.isSelected()){
    		desc.setTitle("Select BHP-SU Dataset");
    		desc.setHomeDirectory(QiProjectDescUtils.getDatasetsPath(agent.getQiProjectDescriptor()));
    		String [] filterList = { ".dat",".DAT" };
            GenericFileFilter filter =  new GenericFileFilter(filterList,"Data Files (*.dat)");
            desc.setFileFilter(filter);
            desc.setMessageCommand(WaveletDecompConstants.GET_HORIZON_BHPSU_DATASET_CMD);
    	}
    	Map props = new HashMap();
        props.put("target", suHorzTextFieldList.get(selectedHorizonIndex));
        desc.setAdditionalProperties(props);
        desc.setDirectoryRememberedEnabled(false);
        desc.setNavigationUpwardEnabled(false);
        desc.setMultiSelectionEnabled(false);
        desc.setetProducerComponentDescriptor(agent.getComponentDescriptor());
        desc.setDialogType(javax.swing.JFileChooser.OPEN_DIALOG);

        desc.setServerUrl(agent.getMessagingMgr().getTomcatURL());

        agent.callFileChooser(desc);

    	/*
        ArrayList list = new ArrayList();
        // 1st element the parent GUI object
        list.add(0, gui);
        // 2nd element is the dialog title
        list.add(1, "Select A Horizon Dataset");
        // 3rd element is a list that contains current directory to start with
        //and a flag (yes or no) indicating if a already remembered directory
        //should be used instead
        ArrayList lst = new ArrayList();
        String dir = QiProjectDescUtils.getHorizonsPath(agent.getProjectDescriptor());
        lst.add(dir);
        lst.add("no");
        list.add(2,lst);
        // 4th element is the file filter
        list.add(3, getFileFilter(new String[] { ".xyz", ".XYZ" }));
        // 5th element is the navigation flag
        list.add(4, false);
        // 6th element is the producer component descriptor
        list.add(5, agent.getMessagingMgr().getMyComponentDesc());
        // 7th element is the type of file chooser either Open or Save
        list.add(6, QIWConstants.FILE_CHOOSER_TYPE_OPEN);
        // 8th element is the message command
        list.add(7, WaveletDecompConstants.FILE_CHOOSER_CMD_GET_HORIZON_DATA);
        // 9th element is the default pretext for the file name if user fails to
        // pretext it. If no pretext is required then use empty String
        list.add(8, "");
        // 10th element is the default extension for the file name if user fails
        // to append. If no extension is required then use empty String
        list.add(9, ".xyz");
        // 11th element is the target tomcat url where the file chooser chooses
        list.add(10, agent.getMessagingMgr().getTomcatURL());
        agent.callFileChooser(list);
        // datasetTextField.setText(agent.getSelectedSeismicDatasetPath());
         */

    }

    public void setSUHorizonDataPath(String path) {

        if (suHorzTextFieldList == null || selectedHorizonIndex == -1
                || suHorzTextFieldList.size() < selectedHorizonIndex) {
            return;
        }
        suHorzTextFieldList.get(selectedHorizonIndex).setText(path);
    }

    public void setLandmarkHorizonDataPath(String path) {
        if (landmarkHorzTextFieldList == null || selectedHorizonIndex == -1
                || landmarkHorzTextFieldList.size() < selectedHorizonIndex) {
            return;
        }
        landmarkHorzTextFieldList.get(selectedHorizonIndex).setText(path);
    }

    public void setDatasetTextField(String text) {
        datasetTextField.setText(text);
    }

    private void entireDatasetButtonActionPerformed(
            java.awt.event.ActionEvent evt) {
        Map<String, DataSetHeaderKeyInfo> map = agent.getSeismicDataset();
        if (map == null) {
            JOptionPane.showMessageDialog(this,
                    "The system can not identify dataset information.",
                    "Problem in getting dataset information",
                    JOptionPane.WARNING_MESSAGE);
            return;
        }
        setSUDatasetDefaultValue(map);
    }

    public void setSUDatasetDefaultValue(Map<String, DataSetHeaderKeyInfo> map) {
        if (map == null)
            return;
        Iterator i = map.entrySet().iterator();
        while (i.hasNext()) {
            Map.Entry e = (Map.Entry) i.next();
            String key = (String)e.getKey();
            Object value = e.getValue();
        //for (String s : map.keySet()) {
            if (key.equals(rangeCdpLabel.getText())) {
                //DataSetHeaderKeyInfo ds = map.get(s);
                DataSetHeaderKeyInfo ds = (DataSetHeaderKeyInfo)value;
                if (ds != null) {
                    cdpTextField.setText(String.valueOf((int) ds.getMin()));
                    cdpToTextField.setText(String.valueOf((int) ds.getMax()));
                    cdpByTextField.setText(String.valueOf((int) ds.getIncr()));
                    defaultcdpBy = (int) ds.getIncr();
                    defaultcdpMin = (int) ds.getMin();
                    defaultcdpMax = (int) ds.getMax();
                }
            }
            if (key.equals(rangeEpLabel.getText())) {
                DataSetHeaderKeyInfo ds = (DataSetHeaderKeyInfo)value;
                if (ds != null) {
                    epTextField.setText(String.valueOf((int) ds.getMin()));
                    epToTextField.setText(String.valueOf((int) ds.getMax()));
                    epByTextField.setText(String.valueOf((int) ds.getIncr()));
                    defaultepBy = (int) ds.getIncr();
                    defaultepMax = (int) ds.getMax();
                    defaultepMin = (int) ds.getMin();
                }
            }
        }
    }

    public void setLineTraceDefaultValue(int[][] infoArray) {
        if (infoArray == null)
            return;
        lineTraceArray = infoArray.clone();
        fillLineTraceFields(lineTraceArray);
    }

    private void fillLineTraceFields(int[][] infoArray){
        linesTextField.setText(String.valueOf(infoArray[0][0]));
        linesToTextField.setText(String.valueOf(infoArray[0][1]));
        linesByTextField.setText(String.valueOf(infoArray[0][2]));
        tracesTextField.setText(String.valueOf(infoArray[1][0]));
        tracesToTextField.setText(String.valueOf(infoArray[1][1]));
        tracesByTextField.setText(String.valueOf(infoArray[1][2]));
        timeTextField.setText(String.valueOf(infoArray[2][0]));
        timeToTextField.setText(String.valueOf(infoArray[2][1]));
        if(infoArray[3][0] == 1){
            landmark3dDataType = "time";
            unitLabel.setText("(millisec)");
        }else if(infoArray[3][0] == 2){
            landmark3dDataType = "depth";
            unitLabel.setText("(feet/meters)");
        }

    }
    /**
     * When a different tab is selected, check which one. If it one of the
     * multi-purpose tabs (Input or Outputs tabs), recreate the tab content. The
     * last format radio button selected (Landmark or BHP SU) will be displayed
     * as selected even when switching tabs.
     *
     * @param evt
     *            Change event.
     */
    private void specDecompTabbedPaneStateChanged(
            javax.swing.event.ChangeEvent evt) {
        int idx = specDecompTabbedPane.getSelectedIndex();
        // check if Seismic Data Input tab selected
        if (idx == INPUT_TAB_INDEX) {
            if (seismicFormatIn == LANDMARK_FORMAT) {
                switch2LandmarkIn();
            } else {
                switch2BhpsuIn();
            }
        }
        if (idx == OUTPUTS_TAB_INDEX) {
            //if (seismicFormatOut == LANDMARK_FORMAT) {
            if (landmarkOutRadioButton.isSelected()) {
                if(bhpsuInRadioButton.isSelected())
                    switch2LandmarkOut(landmarkOutputTimeCount,landmarkOutNumScalesCount,1);
                else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected())
                    switch2LandmarkOut(landmarkOutputTimeCount,landmark2DOutNumScalesCount,2);
                else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected())
                    switch2LandmarkOut(landmarkOutputTimeCount,landmark3DOutNumScalesCount,3);
            } else {
                switch2BhpsuOut();
            }
        }

        if(idx == FLATTENING_TAB_INDEX){
            /*if (seismicFormatIn == LANDMARK_FORMAT) {
                buildflatteningTab(landmarkHorizonCount);
            } else {
                buildflatteningTab(suHorizonCount);
            }*/
            if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
                //switch2BhpsuHorizon(bhpsuHorizonCount);
                numHorizonsComboBox.setSelectedIndex(bhpsuHorizonCount);
                //buildflatteningTab1(bhpsuHorizonCount);
            }else if(landmarkHorizonRadioButton.isSelected()){
                //switch2LandmarkHorizon(landmarkHorizonCount);
                numHorizonsComboBox.setSelectedIndex(landmarkHorizonCount);
                //buildflatteningTab1(landmarkHorizonCount);
            }

        }

    }

    private void timeComboBoxActionPerformed(java.awt.event.ActionEvent evt) {
        if(landmarkStartTimeTextField == null || landmarkStartTimeTextField.getText().trim().length() == 0){
            JOptionPane.showMessageDialog(this, "Please set up the Start Time and End Time in Stratigraphic Flattening tab first.",
                    "Action Not Permitted.",
                    JOptionPane.WARNING_MESSAGE);
            specDecompTabbedPane.setSelectedIndex(FLATTENING_TAB_INDEX);
            landmarkHorizonRadioButton.setSelected(true);
            return;
        }
        JComboBox cb = (JComboBox) evt.getSource();
        int numberOfTime = cb.getSelectedIndex();
        switch2LandmarkOut(numberOfTime,landmarkOutNumScalesCount,1);
        landmarkOutputTimeCount = numberOfTime;
        //formLandmarkOutputTimePanel(numberOfTime);
        //formOutputFlattenedPanel();
        //if(numberOfTime > 0)
        //  landmarkOutputTimeCount = numberOfTime;
    }


    private void exportHorizonsActionPerformed(java.awt.event.ActionEvent evt) {
        if(!validateFlatteningInfo(landmarkHorzLabelList, landmarkHorzTextFieldList, landmarkStartTimeTextField, landmarkEndTimeTextField, landmarkHorzTimeTextFieldList))
            return;
        int minline = Integer.valueOf(linesTextField.getText().trim()).intValue();
        int maxline = Integer.valueOf(linesToTextField.getText().trim()).intValue();
        int mintrace =  Integer.valueOf(tracesTextField.getText().trim()).intValue();
        int maxtrace =  Integer.valueOf(tracesToTextField.getText().trim()).intValue();
        agent.exportLandmarkHorizons(landmarkInProjectTextField.getText().trim(), landmarkHorzTextFieldList, minline, maxline, mintrace, maxtrace);
    }

    private void viewerRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_viewerRadioButtonActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_viewerRadioButtonActionPerformed

    private void listRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_listRadioButtonActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_listRadioButtonActionPerformed

    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_updateButtonActionPerformed
        // TODO add your handling code here:
        /*
        if(job_status == WaveletDecompConstants.JOB_UNSUBMITTED_STATUS){
            JOptionPane.showMessageDialog(this, "Job Not Yet Submitted.",
                    "Job Unsubmitted", JOptionPane.WARNING_MESSAGE);
            statusMenuItem.doClick();
            return;
        }
        String stdErr = agent.getJobOutput(defaultPreferredScriptDir + "/" + currentRunningScriptPrefix + ".err");
        String stdOut = agent.getJobOutput(defaultPreferredScriptDir + "/" + currentRunningScriptPrefix + ".out");
        */
        logger.info("job_status " + job_status);

        if(job_status == WaveletDecompConstants.JOB_UNSUBMITTED_STATUS){
            JOptionPane.showMessageDialog(this, "Job Not Yet Submitted.","Job Unsubmitted", JOptionPane.WARNING_MESSAGE);
            stateLabel.setText("Job Not Submitted");
            runMenuItem.setEnabled(true);
            runScriptButton.setEnabled(true);
            return;
        }else{
            Runnable heavyRunnable = new Runnable(){
                public void run(){
                    updateStatus();                 }
            };
            new Thread(heavyRunnable).start();
        }
        //agent.getJobOutput();
        // detailsTextArea.setText(output);

    }// GEN-LAST:event_updateButtonActionPerformed

    private void updateStatus(){

        String pid = agent.getProcessId(defaultPreferredScriptDir + filesep + currentRunningScriptPrefix + ".sh");
        logger.info("Job Process ID = " + pid);
        if(pid != null && pid.length() > 0){
            job_status = WaveletDecompConstants.JOB_RUNNING_STATUS;
            stateLabel.setText("Job Running");
            stateLabel.setText("Job Running");
            runMenuItem.setEnabled(false);
            runScriptButton.setEnabled(false);
        }else if(pid != null && pid.length() == 0){
            job_status = WaveletDecompConstants.JOB_ENDED_STATUS;
            runMenuItem.setEnabled(true);
            runScriptButton.setEnabled(true);
			displayResultsMenuItem.setEnabled(true);
			displayResultsButton.setEnabled(true);
        }else{
            stateLabel.setText("Unknown");
            runMenuItem.setEnabled(true);
            runScriptButton.setEnabled(true);
			displayResultsMenuItem.setEnabled(false);
			displayResultsButton.setEnabled(false);
        }

        JobSvcOutputWorker worker1 = new JobSvcOutputWorker(agent,defaultPreferredScriptDir + filesep + currentRunningScriptPrefix + ".out");
        JobSvcOutputWorker worker2 = new JobSvcOutputWorker(agent,defaultPreferredScriptDir + filesep + currentRunningScriptPrefix + ".err");
        Thread t1 = new Thread(worker1);
        Thread t2 = new Thread(worker2);
        t1.start();
        t2.start();
        try{
        	t1.join();
        	t2.join();
        }catch(InterruptedException e){
        	e.printStackTrace();
        }
        
        if(job_status == WaveletDecompConstants.JOB_ENDED_STATUS || job_status == WaveletDecompConstants.JOB_RUNNING_STATUS){ //job ended
            String temp = worker1.getOutput();
            if(temp.endsWith(defaultPreferredScriptDir + filesep + currentRunningScriptPrefix + ".sh job ends normally\n")){
                stateLabel.setText("Job Ended Normally");
                job_status = WaveletDecompConstants.JOB_ENDED_STATUS;
            }else{
                //ps id could be fake because user could view the script by using editor
                //where ps id is valid but the acutal job may already end.
                if(job_status == WaveletDecompConstants.JOB_RUNNING_STATUS){
                    if(temp.endsWith(defaultPreferredScriptDir + filesep + currentRunningScriptPrefix + ".sh job ends abnormally\n")){
                        stateLabel.setText("Job Ended Abnormally");
                        job_status = WaveletDecompConstants.JOB_ENDED_STATUS;
                    }
                }else{ //job ended
                    stateLabel.setText("Job Ended Abnormally");
                }
            }
            if(job_status == WaveletDecompConstants.JOB_ENDED_STATUS)
                processEndedJob();
        }
    }

    private void processEndedJob(){
        //String stdErr = agent.getJobOutput(defaultPreferredScriptDir + filesep + currentRunningScriptPrefix + ".err");
        //final String stdOut = agent.getJobOutput(defaultPreferredScriptDir + filesep + currentRunningScriptPrefix + ".out");
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                //if(stdOut.endsWith(defaultPreferredScriptDir + filesep + currentRunningScriptPrefix + ".sh job ends normally\n")){
                //  stateLabel.setText("Job Ended Normally");
                //}else{
                //  stateLabel.setText("Job Ended Abnormally");
                //}
                String text = currentRunningScriptPrefix + ".sh\n";
                text += currentRunningScriptPrefix + ".err\n";
                text += currentRunningScriptPrefix + ".out\n";
                text += "have been created in " + defaultPreferredScriptDir;
                summaryTextArea.setText(text);
                runMenuItem.setEnabled(true);
                runScriptButton.setEnabled(true);
				
				displayResultsMenuItem.setEnabled(true);
				displayResultsButton.setEnabled(true);
            }
        });
    }

    public void setStdOutTextArea(String text) {
        final String txt = text;
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                stdOutTextArea.setText(txt);
            }
        });

    }

    class JobSvcOutputWorker implements Runnable {
    	private volatile WaveletDecompPlugin agent;
    	private volatile String outputLines;
    	private volatile String path;

    	public JobSvcOutputWorker(WaveletDecompPlugin agent,String path) {
    		this.agent = agent;
    		this.path = path;
    	}

    	public void run() {
    		outputLines = agent.getJobOutput(path);
    	}

    	public String getOutput() {
    		return outputLines;
    	}
    }
    
    public void addToStdOutTextArea(String text) {
        String tx = stdOutTextArea.getText();
        final String txt = tx + "\n" + text;
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                stdOutTextArea.setText(txt);
            }
        });

    }

    public void setStdErrTextArea(String text) {
        final String txt = text;
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                stdErrTextArea.setText(txt);
            }
        });

    }
    private void saveAsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_saveAsMenuItemActionPerformed
        agent.saveStateAsClone();
    }// GEN-LAST:event_saveAsMenuItemActionPerformed
/*
    private void updateProjectEnvMenuItemActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_saveAsMenuItemActionPerformed
        // TODO add your handling code here:
        //JDialog dialog =new UpdateProjectEnvDialog(this,true,agent.getProjectDescriptor());
        //dialog.setLocationRelativeTo(this);
        //dialog.setVisible(true);
        QiProjectDescUtils.showQiProjectDescriptorEditor(this,true,agent.getProjectDescriptor());
    }// GEN-LAST:event_saveAsMenuItemActionPerformed
*/
    private void deleteMenuItemActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_deleteMenuItemActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_deleteMenuItemActionPerformed

    private void saveQuitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_saveQuitMenuItemActionPerformed
        agent.saveStateThenQuit();
    }// GEN-LAST:event_saveQuitMenuItemActionPerformed

    private void quitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_saveQuitMenuItemActionPerformed
        agent.deactivateSelf();
    }// GEN-LAST:event_saveQuitMenuItemActionPerformed

    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_saveMenuItemActionPerformed
        agent.saveState();
    }// GEN-LAST:event_saveMenuItemActionPerformed

    private void openMenuItemActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_openMenuItemActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_openMenuItemActionPerformed

    private void newMenuItemActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_newMenuItemActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_newMenuItemActionPerformed

    private void importMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importMenuItemActionPerformed
        //Use a file chooser to select the state file
        QiProjectDescriptor projDesc = agent.getQiProjectDescriptor();
        ComponentStateUtils.callFileChooser(this, QiProjectDescUtils.getXmlPath(projDesc), QIWConstants.IMPORT_COMP_STATE, agent.getComponentDescriptor(), agent.getMessagingMgr());

        //NOTE: agent will invoke a file chooser service. When it processes the GET_RESULT_FROM_FILE_CHOOSER_CMD,
        //     it will read the state of the PM in XML and restore it using its restoreState() method.
    }//GEN-LAST:event_importMenuItemActionPerformed

    private void exportMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportMenuItemActionPerformed
        //Use a file chooser to specify the path of the file where the qiComponent's state will be saved.
        QiProjectDescriptor projDesc = agent.getQiProjectDescriptor();
        ComponentStateUtils.callFileChooser(this, QiProjectDescUtils.getXmlPath(projDesc), QIWConstants.EXPORT_COMP_STATE, agent.getComponentDescriptor(), agent.getMessagingMgr());

        //NOTE:  agent will invoke a file chooser service. When it processes the GET_RESULT_FROM_FILE_CHOOSER_CMD,
        //     it will capture the state of the PM in XML by invoking its genState() method. Then it will
        //     invoke an IO service to write the state to a .xml file.
    }//GEN-LAST:event_exportMenuItemActionPerformed
	
    private void displayResultsButtonActionPerformed(java.awt.event.ActionEvent evt) {
//GEN-FIRST:event_displayResultsButtonActionPerformed
        // Launch a new 2D viewer (qiViewer) for displaying the WaveletDecomp results
        IComponentDescriptor qiViewerDesc = ComponentUtils.launchNewComponent(QIWConstants.QI_VIEWER_NAME, agent.getMessagingMgr());

        //CANNOT proceed if qiViewerDesc null
        if (qiViewerDesc == null) {
            JOptionPane.showMessageDialog(this, "Cannot open a new qiViewer for displaying results", "Launch Error", JOptionPane.ERROR_MESSAGE);
        } else {
            //Start a control session
            ArrayList actions = new ArrayList();
            actions.add("action="+ControlConstants.START_CONTROL_SESSION_ACTION);
            ArrayList actionParams = new ArrayList();
            actionParams.add(qiViewerDesc);
            actions.add(actionParams);
            agent.getMessagingMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
        }
    }//GEN-LAST:event_displayResultsButtonActionPerformed

    /**
     * Switch to Landmark Input. Invoked when the Landmark radio button in the
     * Seismic Data Input tab is selected.
     */
    private void switch2LandmarkIn() {
        if (seismicFormatIn == LANDMARK_FORMAT)
            landmarkInRadioButton.setSelected(true);
        else
            bhpsuInRadioButton.setSelected(true);
        int tabIdx = specDecompTabbedPane.indexOfTab("Seismic Data Input");
        // Note: radio buttons automatically set correctly since in a group

        landmarkInputDataPanel = new javax.swing.JPanel();
        landmarkInPanel = new javax.swing.JPanel();

        if(landmark3DRadioButton.isSelected()){
            formLandmark3DPanel();
        }else if (landmark2DRadioButton.isSelected()){
            formLandmark2DPanel();
        }
        formLandmarkInPanel();
        formLandmarkInputDataPanel();
        // TODO update selections and fields to last value entered

        specDecompTabbedPane.setComponentAt(tabIdx, landmarkInputDataPanel);
        int index = specDecompTabbedPane.indexOfTab("Stratigraphic Flattening");
        if(landmark2DRadioButton.isSelected()){
            specDecompTabbedPane.setEnabledAt(index, false);
        }else if (landmark3DRadioButton.isSelected()){
            specDecompTabbedPane.setEnabledAt(index, true);
        }
        // pack();
    }

    /**
     * Switch to BHP SU Horizons. Invoked when the BHP SU radio button in the
     * Stratigraphic Flattening tab is selected.
     */
    private void switch2BhpsuHorizon(int count) {
        //if (seismicFormatIn == LANDMARK_FORMAT)
        //  landmarkInRadioButton.setSelected(true);
        //else
        //  bhpsuInRadioButton.setSelected(true);
        int tabIdx = specDecompTabbedPane.indexOfTab("Stratigraphic Flattening");
        // Note: radio buttons automatically set correctly since in a group

        horizonTopPanel = new javax.swing.JPanel();
        bhpsuHorizonPanel = new javax.swing.JPanel();
        formBhpsuHorizonPanel(count);
        formHorizonTopPanel();
        // TODO update selections and fields to last value entered

        specDecompTabbedPane.setComponentAt(tabIdx, horizonTopPanel);
        // pack();
    }

    /**
     * Switch to Landmark horizons. Invoked when the Landmark radio button in the
     * Stratigraphic Flattening tab is selected.
     */
    private void switch2LandmarkHorizon(int count) {
        //if (seismicFormatIn == LANDMARK_FORMAT)
        //  landmarkInRadioButton.setSelected(true);
        //else
        //  bhpsuInRadioButton.setSelected(true);
        int tabIdx = specDecompTabbedPane.indexOfTab("Stratigraphic Flattening");
        // Note: radio buttons automatically set correctly since in a group

        landmarkHorizonDataPanel = new javax.swing.JPanel();
        landmarkHorizonPanel = new javax.swing.JPanel();
        formLandmarkHorizonPanel(count);
        formHorizonTopPanel();
        // TODO update selections and fields to last value entered

        specDecompTabbedPane.setComponentAt(tabIdx, landmarkHorizonDataPanel);
        // pack();
    }

    /**
     * Switch to BHP SU Input. Invoked when the BHP SU radio button in the
     * Seismic Data Input tab is selected.
     */
    private void switch2BhpsuIn() {
        if (seismicFormatIn == LANDMARK_FORMAT)
            landmarkInRadioButton.setSelected(true);
        else
            bhpsuInRadioButton.setSelected(true);
        int tabIdx = specDecompTabbedPane.indexOfTab("Seismic Data Input");
        // Note: radio buttons automatically set correctly since in a group

        bhpsuInputDataPanel = new javax.swing.JPanel();
        bhpsuInPanel = new javax.swing.JPanel();
        formBhpsuInPanel();
        formBhpsuInputDataPanel();
        // TODO update selections and fields to last value entered

        specDecompTabbedPane.setComponentAt(tabIdx, bhpsuInputDataPanel);
        // pack();
    }

    /**
     * Switch to Landmark Output. Invoked when the Landmark radio button in the
     * Outputs tab is selected.
     * @param inputType=1 BHPSU, inputType=2 Landmark 2D, inputType=3 Landmark 3D
     */

    private void switch2LandmarkOut(int number,int numScales, int inputType) {
        //if (seismicFormatOut == LANDMARK_FORMAT)
        //  landmarkOutRadioButton.setSelected(true);
        //else
        //  bhpsuOutRadioButton.setSelected(true);
        int tabIdx = specDecompTabbedPane.indexOfTab("Outputs");

        // NOTE: get a NullPointerException if don't reset the variables
        // involved in the Landmark Outputs tab, but don't know which one
        // is absolutely necessary.

        landmarkOutputsPanel = new javax.swing.JPanel();
        landmarkOutPanel = new javax.swing.JPanel();
        scalePanel = new javax.swing.JPanel();
        placementLabel = new javax.swing.JLabel();
        flattenedPanel = new javax.swing.JPanel();

        timeComboBox = new javax.swing.JComboBox();
        timePanel = new javax.swing.JPanel();


        if(inputType == 1)
            formLandmarkScaleOutputPanel0(numScales);
        else if(inputType == 2)
            formLandmark2DScaleOutputPanel0(numScales);
        else if(inputType == 3)
            formLandmark3DScaleOutputPanel0(numScales);
        //if(numScales > 0)
        //  landmarkOutNumScalesCount = numScales;
        ////formLandmarkFlattenedPanel(number);
        //if(number > 0)
        //  landmarkOutputTimeCount = number;


        formLandmarkOutputsPanel();
        // TODO update selections and fields to last value entered

        specDecompTabbedPane.setComponentAt(tabIdx, landmarkOutputsPanel);
        // pack();
    }


    /**
     * Switch to BHP SU Output. Invoked when the BHP SU radio button in the
     * Outputs tab is selected.
     */
    private void switch2BhpsuOut() {
        if (seismicFormatOut == LANDMARK_FORMAT)
            landmarkOutRadioButton.setSelected(true);
        else
            bhpsuOutRadioButton.setSelected(true);

        int tabIdx = specDecompTabbedPane.indexOfTab("Outputs");

        bhpsuOutputsPanel = new javax.swing.JPanel();
        bhpsuOutPanel = new javax.swing.JPanel();
        formBhpsuOutPanel();
        formBhpsuOutputsPanel();
        // TODO update selections and fields to last value entered

        specDecompTabbedPane.setComponentAt(tabIdx, bhpsuOutputsPanel);
        // pack();

    }

    private void formBhpsuHorizonPanel(int number){
        bhpsuHorizonPanel
        .setBorder(javax.swing.BorderFactory.createTitledBorder(""));


        bhpsuHorizonPanel.setAutoscrolls(true);
        scrollPane = new JScrollPane(bhpsuHorizonPanel,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        logger.info("number selected = " + number);
        logger.info("current bhpsuHorizonCount = " + bhpsuHorizonCount);

        if (number < 1){
            suHorzTextFieldList = new ArrayList<JTextField>();
            suHorzTimeTextFieldList = new ArrayList<JTextField>();
            return;
        }
        if(bhpsuHorizonCount != number){
            suHorzLabelList = new ArrayList<JLabel>();
            suHorzTextFieldList = new ArrayList<JTextField>();
            suHorzTimeTextFieldList = new ArrayList<JTextField>();
            suChooseHorzButtonList = new ArrayList<JButton>();
            final WaveletDecompGUI comp = this;
            for (int i = 0; i < number; i++) {
                suHorzLabelList.add(new JLabel("Horz " + (i + 1)));
                JButton button = new JButton("Choose...");
                final int count = i;
                button.addActionListener(new ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        selectedHorizonIndex = count;
                        //if(bhpsuHorizonRadioButton.isSelected())
                        	//chooseHorizonButtonActionPerformed(evt);
                    	QiFileChooserDescriptor desc = new QiFileChooserDescriptor();
                    	desc.setParentGUI(gui);
                    	if(bhpsuHorizonRadioButton.isSelected()){
                    		desc.setTitle("Select A Horizon Dataset");
                    		desc.setHomeDirectory(QiProjectDescUtils.getHorizonsPath(agent.getQiProjectDescriptor()));
                    		String [] filterList = { ".xyz", ".XYZ" };
                            GenericFileFilter filter =  new GenericFileFilter(filterList,"Horizon ASCII Files (*.xyz)");
                            desc.setFileFilter(filter);
                            desc.setMessageCommand(WaveletDecompConstants.FILE_CHOOSER_CMD_GET_HORIZON_DATA);
                    	}else if(bhpsuHorizonDatasetRadioButton.isSelected()){
                    		desc.setTitle("Select BHP-SU Dataset");
                    		desc.setHomeDirectory(QiProjectDescUtils.getDatasetsPath(agent.getQiProjectDescriptor()));
                    		String [] filterList = { ".dat",".DAT" };
                            GenericFileFilter filter =  new GenericFileFilter(filterList,"Data Files (*.dat)");
                            desc.setFileFilter(filter);
                            desc.setMessageCommand(WaveletDecompConstants.GET_HORIZON_BHPSU_DATASET_CMD);
                    	}
                    	Map props = new HashMap();
                        props.put("target", suHorzTextFieldList.get(count));
                        desc.setAdditionalProperties(props);
                        desc.setDirectoryRememberedEnabled(false);
                        desc.setNavigationUpwardEnabled(false);
                        desc.setMultiSelectionEnabled(false);
                        desc.setetProducerComponentDescriptor(agent.getComponentDescriptor());
                        desc.setDialogType(javax.swing.JFileChooser.OPEN_DIALOG);

                        desc.setServerUrl(agent.getMessagingMgr().getTomcatURL());

                        agent.callFileChooser(desc);
                    }
                });
                suChooseHorzButtonList.add(button);
                JTextField tf = new JTextField();
                suHorzTextFieldList.add(new JTextField());
                suHorzTimeTextFieldList.add(tf);
            }
        }
        jLabel1.setText("Start Time:");
        jLabel2.setText("End Time:");
        jLabel3.setText("Time Increment (ms):");
        org.jdesktop.layout.GroupLayout horizonsPanelLayout = new org.jdesktop.layout.GroupLayout(
                bhpsuHorizonPanel);
        bhpsuHorizonPanel.setLayout(horizonsPanelLayout);
        horizonsPanelLayout.setAutocreateGaps(true);
        horizonsPanelLayout.setAutocreateContainerGaps(true);
        SequentialGroup hGroup = horizonsPanelLayout.createSequentialGroup();
        ParallelGroup p = horizonsPanelLayout.createParallelGroup(
                org.jdesktop.layout.GroupLayout.LEADING, false);

        for (int i = 0; suHorzLabelList != null && i < suHorzLabelList.size(); i++) {
            p = p.add(suHorzLabelList.get(i));
        }
        hGroup = hGroup.add(p);
        p = horizonsPanelLayout.createParallelGroup();
        for (int i = 0; suHorzTextFieldList != null && i < suHorzTextFieldList.size(); i++) {
            p = p.add(suHorzTextFieldList.get(i),
                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 260,
                    Short.MAX_VALUE);
        }
        hGroup = hGroup.add(p);

        p = horizonsPanelLayout
                .createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING);
        p = p.add(jLabel1);
        for (int i = 0; suChooseHorzButtonList != null && i < suChooseHorzButtonList.size(); i++) {
            p = p.add(suChooseHorzButtonList.get(i));
        }
        p = p.add(jLabel2);
        p = p.add(jLabel3);
        hGroup = hGroup.add(p);

        p = horizonsPanelLayout.createParallelGroup();
        p = p.add(suStartTimeTextField);
        for (int i = 0; suHorzTimeTextFieldList != null && i < suHorzTimeTextFieldList.size(); i++) {
            p = p.add(suHorzTimeTextFieldList.get(i),
                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 80,
                    Short.MAX_VALUE);
        }
        p = p.add(suEndTimeTextField);

        p = p.add(timeIncrementTextField);
        hGroup = hGroup.add(p);
        horizonsPanelLayout.setHorizontalGroup(hGroup);

        SequentialGroup vGroup = horizonsPanelLayout.createSequentialGroup();
        p = horizonsPanelLayout.createParallelGroup(GroupLayout.BASELINE);
        p.add(jLabel1).add(suStartTimeTextField);
        vGroup = vGroup.add(p);
        for (int i = 0; suHorzTextFieldList != null && i < suHorzTextFieldList.size(); i++) {
            p = horizonsPanelLayout.createParallelGroup(GroupLayout.BASELINE);
            p = p.add(suHorzLabelList.get(i)).add(suHorzTextFieldList.get(i)).add(
                    suChooseHorzButtonList.get(i)).add(
                    suHorzTimeTextFieldList.get(i));
            vGroup = vGroup.add(p);
        }
        p = horizonsPanelLayout.createParallelGroup(GroupLayout.BASELINE);
        p.add(jLabel2).add(suEndTimeTextField);
        vGroup = vGroup.add(p);
        p = horizonsPanelLayout.createParallelGroup(GroupLayout.BASELINE);
        p.add(jLabel3).add(timeIncrementTextField);

        vGroup = vGroup.add(p);
        horizonsPanelLayout.setVerticalGroup(vGroup);

    }

    private void formLandmarkHorizonPanel(int number){
        landmarkHorizonPanel
        .setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        landmarkHorizonPanel.setAutoscrolls(true);
        scrollPane = new JScrollPane(landmarkHorizonPanel,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        logger.info("number selected = " + number);
        logger.info("current landmarkHorizonCount = " + landmarkHorizonCount);

        if (number < 1){
            landmarkHorzTextFieldList = new ArrayList<JTextField>();
            landmarkHorzTimeTextFieldList = new ArrayList<JTextField>();
            return;
        }
        jLabel3.setText("Sample Rate:");
        if(landmarkHorizonCount != number){
            landmarkHorzLabelList = new ArrayList<JLabel>();
            landmarkHorzTextFieldList = new ArrayList<JTextField>();
            landmarkHorzTimeTextFieldList = new ArrayList<JTextField>();
            landmarkChooseHorzButtonList = new ArrayList<JButton>();
            final WaveletDecompGUI comp = this;
            for (int i = 0; i < number; i++) {
                landmarkHorzLabelList.add(new JLabel("Horz " + (i + 1)));
                JButton button = new JButton("Choose...");
                final int count = i;
                button.addActionListener(new ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        selectedHorizonIndex = count;
                        int projectType = 0;
                        if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                            if(landmarkInProjectTextField.getText() == null  || landmarkInProjectTextField.getText().trim().length() == 0){
                                JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                    "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                                int tabIdx = specDecompTabbedPane.indexOfTab("Seismic Data Input");
                                specDecompTabbedPane.setSelectedIndex(tabIdx);
                                return;
                            }
                            projectType = landmarkAgent.getLandmarkProjectType(landmarkInProjectTextField.getText().trim());

                        }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                            if(landmarkIn2DProjectTextField.getText() == null  || landmarkIn2DProjectTextField.getText().trim().length() == 0){
                                JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                    "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                                int tabIdx = specDecompTabbedPane.indexOfTab("Seismic Data Input");
                                specDecompTabbedPane.setSelectedIndex(tabIdx);
                                return;
                            }
                            projectType = landmarkAgent.getLandmarkProjectType(landmarkIn2DProjectTextField.getText().trim());
                        }
                        if(projectType < 2 || projectType > 3){
                            JOptionPane.showMessageDialog(comp, "Invalid project type found.",
                                "Invalid Project Type", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = specDecompTabbedPane.indexOfTab("Seismic Data Input");
                            specDecompTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                        if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                            agent.openLandmarkHorizonDialog(landmarkInProjectTextField.getText().trim(), projectType, comp,landmarkHorzTextFieldList.get(count));
                        }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                            agent.openLandmarkHorizonDialog(landmarkIn2DProjectTextField.getText().trim(), projectType, comp,landmarkHorzTextFieldList.get(count));
                        }
                        //new HorizonSelectDialog(comp,true).setVisible(true);
                    }
            });
            landmarkChooseHorzButtonList.add(button);
            JTextField tf = new JTextField();

                landmarkHorzTextFieldList.add(new JTextField());
                landmarkHorzTimeTextFieldList.add(tf);
            }

            jLabel1.setText("Start Time:");
            jLabel2.setText("End Time:");

        }

        float [] timeRange =  null;
        if(landmarkAgent == null)
            landmarkAgent = LandmarkServices.getInstance();
        if(landmarkInProjectTextField != null && landmarkInProjectTextField.getText().trim().length() > 0){
            String selectVolume = volumeTextField.getText();
            if(selectVolume != null && selectVolume.trim().length() > 0){
                int projectType = landmarkAgent.getLandmarkProjectType(landmarkInProjectTextField.getText().trim());
                if(!(projectType < 2 || projectType > 3) )
                timeRange = landmarkAgent.getLandmarkTimeRangeInfo(landmarkInProjectTextField.getText().trim(), projectType, selectVolume);
            }
        }
        if(timeRange != null && number > 0 && number != landmarkHorizonCount){
            landmarkStartTimeTextField.setText(String.valueOf((int)timeRange[0]));
            landmarkEndTimeTextField.setText(String.valueOf((int)timeRange[1]));
            sampleRateTextField.setText(String.valueOf((int)timeRange[2]));
            int interval = (int)(timeRange[1]-timeRange[0])/(number+1);
            for(int j = 0; j < number; j++){
                int temp = (int)timeRange[0] + (interval*(j+1));
                if((int)timeRange[2] != 0){
                    int rem = temp % (int)timeRange[2];
                    landmarkHorzTimeTextFieldList.get(j).setText(String.valueOf(temp-rem));
                }
            }
        }

        org.jdesktop.layout.GroupLayout horizonsPanelLayout = new org.jdesktop.layout.GroupLayout(
                landmarkHorizonPanel);
        landmarkHorizonPanel.setLayout(horizonsPanelLayout);
        horizonsPanelLayout.setAutocreateGaps(true);
        horizonsPanelLayout.setAutocreateContainerGaps(true);
        SequentialGroup hGroup = horizonsPanelLayout.createSequentialGroup();
        ParallelGroup p = horizonsPanelLayout.createParallelGroup(
                org.jdesktop.layout.GroupLayout.LEADING, false);

        for (int i = 0; landmarkHorzLabelList != null && i < landmarkHorzLabelList.size(); i++) {
            p = p.add(landmarkHorzLabelList.get(i));
        }
        hGroup = hGroup.add(p);
        p = horizonsPanelLayout.createParallelGroup();
        for (int i = 0; landmarkHorzTextFieldList != null && i < landmarkHorzTextFieldList.size(); i++) {
            p = p.add(landmarkHorzTextFieldList.get(i),
                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 260,
                    Short.MAX_VALUE);
        }
        hGroup = hGroup.add(p);

        p = horizonsPanelLayout
                .createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING);
        p = p.add(jLabel1);
        for (int i = 0; landmarkChooseHorzButtonList != null && i < landmarkChooseHorzButtonList.size(); i++) {
            p = p.add(landmarkChooseHorzButtonList.get(i));
        }
        p = p.add(jLabel2);
        p = p.add(jLabel3);
        hGroup = hGroup.add(p);

        p = horizonsPanelLayout.createParallelGroup();
        p = p.add(landmarkStartTimeTextField);
        for (int i = 0; landmarkHorzTimeTextFieldList != null && i < landmarkHorzTimeTextFieldList.size(); i++) {
            p = p.add(landmarkHorzTimeTextFieldList.get(i),
                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 80,
                    Short.MAX_VALUE);
        }
        p = p.add(landmarkEndTimeTextField);
        p = p.add(sampleRateTextField);
        hGroup = hGroup.add(p);
        horizonsPanelLayout.setHorizontalGroup(hGroup);

        SequentialGroup vGroup = horizonsPanelLayout.createSequentialGroup();
        p = horizonsPanelLayout.createParallelGroup(GroupLayout.BASELINE);
        p.add(jLabel1).add(landmarkStartTimeTextField);
        vGroup = vGroup.add(p);
        for (int i = 0; i < landmarkHorzTextFieldList.size(); i++) {
            p = horizonsPanelLayout.createParallelGroup(GroupLayout.BASELINE);
            p = p.add(landmarkHorzLabelList.get(i)).add(landmarkHorzTextFieldList.get(i)).add(
                    landmarkChooseHorzButtonList.get(i)).add(
                    landmarkHorzTimeTextFieldList.get(i));
            vGroup = vGroup.add(p);
        }
        p = horizonsPanelLayout.createParallelGroup(GroupLayout.BASELINE);
        p.add(jLabel2).add(landmarkEndTimeTextField);
        vGroup = vGroup.add(p);
        p = horizonsPanelLayout.createParallelGroup(GroupLayout.BASELINE);
        p.add(jLabel3).add(sampleRateTextField);

        vGroup = vGroup.add(p);
        horizonsPanelLayout.setVerticalGroup(vGroup);
    }
    /**
     * Form the interior bhpsuInPanel used in the multi-purpose Seismic Data
     * Input tab.
     */
    private void formBhpsuInPanel() {
        bhpsuInPanel
                .setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        bhpsuProjectLabel.setText("Project Path:");

        datasetLabel.setText("Seismic Dataset:");

        chooseDatasetButton.setText("Choose...");

        bhpsuRangeLabel.setText("Range:");

        rangeCdpLabel.setText("cdp");

        // cdpTextField.setText("0");

        cdpToLabel.setText("to");

        // cdpToTextField.setText("0");

        cdpByLabel.setText("by");

        // cdpByTextField.setText("1");

        rangeEpLabel.setText("ep");

        // epTextField.setText("0");

        epToLabel.setText("to");

        // epToTextField.setText("0");

        epByLabel.setText("by");

        // epByTextField.setText("1");

        entireDatasetButton.setText("Entire Dataset");

        pickButton.setText("Pick from Viewer");
        //selectedProjectLabel.setText("<selected project>");

        QiProjectDescriptor desc = agent.getProjectDescriptor();
        String project = QiProjectDescUtils.getProjectPath(desc);
        selectedProjectLabel.setText(project);

        org.jdesktop.layout.GroupLayout bhpsuInPanelLayout = new org.jdesktop.layout.GroupLayout(
                bhpsuInPanel);
        bhpsuInPanel.setLayout(bhpsuInPanelLayout);
        bhpsuInPanelLayout
            .setHorizontalGroup(bhpsuInPanelLayout
                .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(bhpsuInPanelLayout
                    .createSequentialGroup()
                    .addContainerGap()
                    .add(bhpsuInPanelLayout
                        .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(bhpsuInPanelLayout.createSequentialGroup()
                            .add(datasetLabel)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(datasetTextField,org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,233,Short.MAX_VALUE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(chooseDatasetButton))
                        .add(bhpsuInPanelLayout
                            .createSequentialGroup()
                            .add(bhpsuRangeLabel)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(bhpsuInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING,false)
                               .add(bhpsuInPanelLayout.createSequentialGroup()
                                    .add(bhpsuInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(rangeCdpLabel)
                                        .add(rangeEpLabel))
                                    .add(8,8,8)
                                    .add(bhpsuInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(bhpsuInPanelLayout.createSequentialGroup()
                                            .add(cdpTextField,org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                51,Short.MAX_VALUE)
                                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                            .add(cdpToLabel)
                                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                            .add(cdpToTextField,org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                51,Short.MAX_VALUE)
                                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                            .add(cdpByLabel)
                                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                            .add(cdpByTextField,org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                39,Short.MAX_VALUE))
                                            .add(bhpsuInPanelLayout
                                                .createSequentialGroup()
                                                    .add(epTextField,org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        51,Short.MAX_VALUE)
                                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                                    .add(epToLabel)
                                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                                    .add(epToTextField,org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        51,Short.MAX_VALUE)
                                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                                    .add(epByLabel)
                                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                                    .add(epByTextField,org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                        39,Short.MAX_VALUE))))
                            .add(bhpsuInPanelLayout.createSequentialGroup()
                                .add(entireDatasetButton)
                                .addPreferredGap(
                                    org.jdesktop.layout.LayoutStyle.RELATED,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    Short.MAX_VALUE)
                                .add(pickButton))))
                    .add(bhpsuInPanelLayout
                        .createSequentialGroup()
                        .add(bhpsuProjectLabel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(selectedProjectLabel,
                            org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                            359,
                            Short.MAX_VALUE)))
                .addContainerGap()));

        bhpsuInPanelLayout
            .setVerticalGroup(bhpsuInPanelLayout
                .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(bhpsuInPanelLayout
                .createSequentialGroup()
                .add(bhpsuInPanelLayout
                    .createParallelGroup( org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(bhpsuProjectLabel)
                    .add(selectedProjectLabel,
                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                        14,
                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(
                    bhpsuInPanelLayout
                      .createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(datasetLabel)
                        .add(chooseDatasetButton)
                        .add(datasetTextField,
                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                            org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(bhpsuInPanelLayout
                    .createParallelGroup(
                        org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(bhpsuRangeLabel)
                    .add(rangeCdpLabel)
                    .add(cdpByTextField,
                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cdpByLabel)
                    .add(cdpToTextField,
                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cdpToLabel)
                    .add(cdpTextField,
                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(bhpsuInPanelLayout
                        .createParallelGroup(
                            org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(rangeEpLabel)
                        .add(epTextField,
                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                            19,
                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(epToLabel)
                        .add(epToTextField,
                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                            org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(epByLabel)
                        .add(epByTextField,
                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                            org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(
                    bhpsuInPanelLayout
                        .createParallelGroup(
                            org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(
                            entireDatasetButton)
                        .add(pickButton))
                .addContainerGap(193, Short.MAX_VALUE)));
    }

    private void formHorizonTopPanel() {
        //bhpsuHorizonsComboBox.setSelectedIndex(count);
        org.jdesktop.layout.GroupLayout bhpsuHorizonDataPanelLayout = new org.jdesktop.layout.GroupLayout(
                horizonTopPanel);
        horizonTopPanel.setLayout(bhpsuHorizonDataPanelLayout);
        bhpsuHorizonDataPanelLayout.setHorizontalGroup(bhpsuHorizonDataPanelLayout
            .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(bhpsuHorizonDataPanelLayout
                    .createSequentialGroup()
                        .addContainerGap()
                        .add(bhpsuHorizonDataPanelLayout
                            .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(
                                    //bhpsuHorizonPanel,
                                    //bhpsuScrollPane,
                                        scrollPane,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE)
                                .add(bhpsuHorizonDataPanelLayout
                                    .createSequentialGroup()
                                        .add(landmarkHorizonRadioButton)
                                        //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(20,20,20)
                                        .add(bhpsuHorizonRadioButton)
                                        //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(20,20,20)
                                        .add(bhpsuHorizonDatasetRadioButton)
                                        .add(48,48,48)
                                        .add(horizonsLabel)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(numHorizonsComboBox,org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(48,48,48)
                                        .add(bhpsuHorizonDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                                .add(listRadioButton)
                                                .add(viewerRadioButton))
                                )).addContainerGap()));
        bhpsuHorizonDataPanelLayout.setVerticalGroup(bhpsuHorizonDataPanelLayout
                .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(bhpsuHorizonDataPanelLayout
                    .createSequentialGroup()
                        .addContainerGap()
                        .add(bhpsuHorizonDataPanelLayout
                            .createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(landmarkHorizonRadioButton)
                                .add(bhpsuHorizonRadioButton)
                                .add(bhpsuHorizonDatasetRadioButton)
                                .add(horizonsLabel)
                                .add(numHorizonsComboBox,org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(bhpsuHorizonDataPanelLayout.createSequentialGroup()
                                    .add(listRadioButton)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(viewerRadioButton)))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        //.add(bhpsuScrollPane,
                        //.add(bhpsuHorizonPanel,
                        .add(scrollPane,
                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                420)
                        .addContainerGap()));
    }


    /**
     * Return the current state of the set of DataSetHeaderKeyInfo 
     * keyed by inline(ep), xline(cdp), and optionally zvalue
     * Only applied to BHPSU and Landmark 3D type of datasets
     */
    protected Map<String,DataSetHeaderKeyInfo> getInputDataRange(){
        //assuming fields are validated prior to calling to this method.
        //if(validateLandmarkInput())
        Map<String,DataSetHeaderKeyInfo> map = new HashMap<String,DataSetHeaderKeyInfo>();
        if(landmark3DRadioButton.isSelected()){
            double startInline = Double.valueOf(linesTextField.getText().trim());
            double endInline = Double.valueOf(linesToTextField.getText().trim());
            double incInline = Double.valueOf(linesByTextField.getText().trim());
            DataSetHeaderKeyInfo info1 = new DataSetHeaderKeyInfo("inline",startInline,endInline,incInline);
            map.put("inline", info1);
            double startXline = Double.valueOf(tracesTextField.getText().trim());
            double endXline = Double.valueOf(tracesToTextField.getText().trim());
            double incXline = Double.valueOf(tracesByTextField.getText().trim());
            DataSetHeaderKeyInfo info2 = new DataSetHeaderKeyInfo("xline",startXline,endXline,incXline);
            map.put("xline", info2);
            double startVline = Double.valueOf(timeTextField.getText().trim());
            double endVline = Double.valueOf(timeToTextField.getText().trim());
            double incVline = (double)lineTraceArray[2][2];
            DataSetHeaderKeyInfo info3 = new DataSetHeaderKeyInfo("vline",startVline,endVline,incVline);
            map.put("vline", info3);
        }else if(bhpsuInRadioButton.isSelected()){
            double startInline = Double.valueOf(epTextField.getText().trim());
            double endInline = Double.valueOf(epToTextField.getText().trim());
            double incInline = Double.valueOf(epByTextField.getText().trim());
            DataSetHeaderKeyInfo info1 = new DataSetHeaderKeyInfo("inline",startInline,endInline,incInline);
            map.put("inline", info1);
            double startXline = Double.valueOf(cdpTextField.getText().trim());
            double endXline = Double.valueOf(cdpToTextField.getText().trim());
            double incXline = Double.valueOf(cdpByTextField.getText().trim());
            DataSetHeaderKeyInfo info2 = new DataSetHeaderKeyInfo("xline",startXline,endXline,incXline);
            map.put("xline", info2);
        }
        return map;
    }

    /**
     * Check to see if Landmark 2D input is requested for processing
     */
    public boolean isLandmark2D (){
        return landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected();
    }

    /**
     * Check to see if Landmark 3D input is requested for processing
     */
    public boolean isLandmark3D(){
        return landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected();

    }
    private void formBhpsuInputDataPanel() {
        org.jdesktop.layout.GroupLayout bhpsuInputDataPanelLayout = new org.jdesktop.layout.GroupLayout(
                bhpsuInputDataPanel);
        bhpsuInputDataPanel.setLayout(bhpsuInputDataPanelLayout);
        bhpsuInputDataPanelLayout
            .setHorizontalGroup(bhpsuInputDataPanelLayout
                .createParallelGroup(
                    org.jdesktop.layout.GroupLayout.LEADING)
                .add(
                    bhpsuInputDataPanelLayout
                        .createSequentialGroup()
                        .addContainerGap()
                        .add(
                            bhpsuInputDataPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.LEADING)
                                .add(
                                    bhpsuInPanel,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    Short.MAX_VALUE)
                                .add(
                                    bhpsuInputDataPanelLayout
                                        .createSequentialGroup()
                                        .add(landmarkInRadioButton)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(bhpsuInRadioButton)))
                        .addContainerGap()));
        bhpsuInputDataPanelLayout
            .setVerticalGroup(bhpsuInputDataPanelLayout
                .createParallelGroup(
                    org.jdesktop.layout.GroupLayout.LEADING)
                .add(
                    bhpsuInputDataPanelLayout
                        .createSequentialGroup()
                        .addContainerGap()
                        .add(
                            bhpsuInputDataPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(landmarkInRadioButton)
                                .add(bhpsuInRadioButton))
                        .addPreferredGap(
                            org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(
                            bhpsuInPanel,
                            org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                            org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                            Short.MAX_VALUE)
                        .addContainerGap()));
    }


    private void formLandmark2DPanel(){
        landmarkIn2D3DDataPanel = new javax.swing.JPanel();
        landmarkIn2D3DDataPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        landmarkProjectLabel.setText("SeisWorks Project:");

        list2DProjectsButton.setText("List...");

        seismicFileLabel.setText("Seismic File:");

        shotpointLabel.setText("Shotpoint Range:");

        tracesToLabel.setText("to");

        tracesByLabel.setText("by");

        entireVolume2DButton.setText("Entire Line");


        select2DButton.setText("Select from map");
        select2DButton.setEnabled(false);
        seismic2DFileComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        linesLabel.setText("All Associated Seismic Lines:");
        String [] files = new String[0];
        if(landmark2DfileLinesMap != null){
            Set set= landmark2DfileLinesMap.keySet ();
            files = new String[set.size()+1];
            Iterator iter = set.iterator () ;
            files[0] = "Select one";
            int i = 1;
            while(iter.hasNext()){
                Object obj = iter.next();
                files[i++] = (String)obj;
            }

        }
        /*
        while ( iter.hasNext())  {
            files[]
           Object Obj = iter.next ();
           System.out.println ( "Key: "+ Obj);

           List<String>  list = (List<String>) map.get(Obj);
           System.out.println ( "size: "+ list.size());
           for(int i = 0; i < list.size(); i++){
               String s = list.get(i);
                System.out.println(s) ;
           }
        }
        */
        if(files.length > 1)
        Arrays.sort(files,1,files.length);
        seismic2DFileComboBox.setModel(new javax.swing.DefaultComboBoxModel(files));
        String [] items = {};
        if(seismic2DFileComboBox.getSelectedIndex() != -1 && landmark2DfileLinesMap.keySet().size() > 0){
            Set set= landmark2DfileLinesMap.keySet ();
            //selectedProcessLevelIndex = seismic2DFileComboBox.getSelectedIndex();
            seismic2DFileComboBox.setSelectedIndex(selectedProcessLevelIndex);
            if(selectedProcessLevelIndex > 0){
                String item = (String)seismic2DFileComboBox.getSelectedItem();
                List<String> list = landmark2DfileLinesMap.get(item);
                if(list != null){
                    items = new String[list.size()];
                    for(int i = 0; i < list.size(); i++){
                        items[i] = list.get(i);
                    }
                }
                seismic2DLineList.setListData(items);
            }
        }
        final String []  fitems = items;
        seismic2DLineList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = fitems;
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        seismic2DLineList.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(127, 157, 185)));
        lineListScrollPane.setViewportView(seismic2DLineList);


        String [] strs = new String[selectedLines.size()];
        for(int i = 0 ; i < selectedLines.size(); i++){
            strs[i] = (String)selectedLines.get(i);
        }

        final String[] fstrs = strs;
        selectedSeismic2DLineList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = fstrs;
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });

        if(selectedLineName != null && selectedLineName.trim().length() > 0){
            int ind = -1;
            for(int i = 0; i < selectedSeismic2DLineList.getModel().getSize(); i++){
                if(selectedSeismic2DLineList.getModel().getElementAt(i) != null && selectedSeismic2DLineList.getModel().getElementAt(i).equals(selectedLineName)){
                    ind = i;
                    break;
                }
            }
            if(ind != -1){
                selectedSeismic2DLineList.setSelectedIndex(ind);
                LineInfo li = selectedLineMap.get(selectedLineName);
                shotpointTextField.setText(String.valueOf(li.getMinShotPoint()));
                shotpointToTextField.setText(String.valueOf(li.getMaxShotPoint()));
                shotpointByTextField.setText(String.valueOf(li.getIncShotPoint()));
            }
        }

        selectedSeismic2DLineList.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(127, 157, 185)));
        selectedLineListScrollPane.setViewportView(selectedSeismic2DLineList);
        selectedLinesLabel.setText("Selected Lines:");


        rightArrowButton.setText(">>");

        leftArrowButton.setText("<<");

        seismicLinesPanel = new javax.swing.JPanel();
        org.jdesktop.layout.GroupLayout seismicLinesPanelLayout = new org.jdesktop.layout.GroupLayout(seismicLinesPanel);
        seismicLinesPanel.setLayout(seismicLinesPanelLayout);
        seismicLinesPanelLayout.setHorizontalGroup(
            seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, seismicLinesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(seismicLinesPanelLayout.createSequentialGroup()
                        .add(lineListScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 240, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(15, 15, 15)
                        .add(seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(rightArrowButton)
                            .add(leftArrowButton)))
                    .add(linesLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 27, Short.MAX_VALUE)
                .add(seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(selectedSeismic2DLineList, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 249, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(selectedLinesLabel))
                .addContainerGap())
        );
        seismicLinesPanelLayout.setVerticalGroup(
            seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(seismicLinesPanelLayout.createSequentialGroup()
                .add(seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(seismicLinesPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .add(seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(selectedLinesLabel)
                            .add(linesLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(seismicLinesPanelLayout.createSequentialGroup()
                                .add(lineListScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 319, Short.MAX_VALUE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED))
                            .add(selectedSeismic2DLineList, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 319, Short.MAX_VALUE)))
                    .add(seismicLinesPanelLayout.createSequentialGroup()
                        .add(108, 108, 108)
                        .add(rightArrowButton)
                        .add(16, 16, 16)
                        .add(leftArrowButton)))
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout landmarkIn2D3DDataPanelLayout = new org.jdesktop.layout.GroupLayout(landmarkIn2D3DDataPanel);
        landmarkIn2D3DDataPanel.setLayout(landmarkIn2D3DDataPanelLayout);
        landmarkIn2D3DDataPanelLayout.setHorizontalGroup(
            landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                        .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(seismicFileLabel)
                            .add(landmarkProjectLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(landmarkIn2DProjectTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 394, Short.MAX_VALUE))
                                    .add(seismic2DFileComboBox, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(list2DProjectsButton)
                                .add(74, 74, 74))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                                        .add(shotpointLabel)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(shotpointTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(12, 12, 12)
                                        .add(tracesToLabel)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED))
                                    .add(entireVolume2DButton))
                                .add(18, 18, 18)
                                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(select2DButton)
                                    .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(shotpointToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(13, 13, 13)
                                        .add(tracesByLabel)
                                        .add(12, 12, 12)
                                        .add(shotpointByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 50, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                                .add(37, 37, 37))))
                    .add(seismicLinesPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        landmarkIn2D3DDataPanelLayout.setVerticalGroup(
            landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkProjectLabel)
                    .add(landmarkIn2DProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(list2DProjectsButton))
                .add(14, 14, 14)
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(seismicFileLabel)
                    .add(seismic2DFileComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(21, 21, 21)
                .add(seismicLinesPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(14, 14, 14)
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(shotpointToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(shotpointByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tracesByLabel)
                    .add(tracesToLabel)
                    .add(shotpointLabel)
                    .add(shotpointTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 21, Short.MAX_VALUE)
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(select2DButton)
                    .add(entireVolume2DButton))
                .addContainerGap())
        );
    }

    private void formLandmark2DPanelOld(){
        landmarkIn2D3DDataPanel = new javax.swing.JPanel();


        landmarkIn2D3DDataPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        landmarkProjectLabel.setText("SeisWorks Project:");
        list2DProjectsButton.setText("List...");
        linesLabel.setText("Seismic Lines:");
        linesListButton.setText("List...");
        seismicFileLabel.setText("Seismic Files:");
        shotpointLabel.setText("Shotpoints");
        entireVolume2DButton.setText("Entire Volume");
        select2DButton.setText("Select from map");
        seismicFileListButton.setText("List...");

        org.jdesktop.layout.GroupLayout landmarkIn2DPanelLayout = new org.jdesktop.layout.GroupLayout(landmarkIn2D3DDataPanel);
        landmarkIn2D3DDataPanel.setLayout(landmarkIn2DPanelLayout);
        landmarkIn2DPanelLayout.setHorizontalGroup(
            landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkIn2DPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(landmarkProjectLabel)
                    .add(landmarkIn2DPanelLayout.createSequentialGroup()
                        .add(landmarkRangeLabel)
                        .add(27, 27, 27))
                    .add(seismicFileLabel)
                    .add(linesLabel))
                .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn2DPanelLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(landmarkIn2DProjectTextField,org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
                            .add(linesInTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
                            .add(seismicFileTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)))
                    .add(landmarkIn2DPanelLayout.createSequentialGroup()
                        .add(shotpointLabel)
                        .add(14, 14, 14)
                        .add(shotpointTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 53, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(15, 15, 15)
                        .add(tracesToLabel)
                        .add(12, 12, 12)
                        .add(shotpointToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 53, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(16, 16, 16)
                        .add(tracesByLabel)
                        .add(13, 13, 13)
                        .add(shotpointByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 40, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(landmarkIn2DPanelLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(entireVolume2DButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(select2DButton)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(list2DProjectsButton)
                    .add(linesListButton)
                    .add(seismicFileListButton))
                .addContainerGap()
                    )
        );
        landmarkIn2DPanelLayout.setVerticalGroup(
            landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkIn2DPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(list2DProjectsButton)
                    .add(landmarkProjectLabel)
                    .add(landmarkIn2DProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(linesListButton)
                    .add(linesInTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(linesLabel))
                .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn2DPanelLayout.createSequentialGroup()
                        .add(11, 11, 11)
                        .add(seismicFileLabel))
                    .add(landmarkIn2DPanelLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(seismicFileTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(seismicFileListButton))))
                .add(18, 18, 18)
                .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkRangeLabel)
                    .add(shotpointLabel)
                    .add(tracesToLabel)
                    .add(shotpointToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(shotpointTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tracesByLabel)
                    .add(shotpointByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(16, 16, 16)
                .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(select2DButton)
                    .add(entireVolume2DButton))
                .addContainerGap(183, Short.MAX_VALUE)
                    )
        );
    }


    private void formLandmarkInPanel() {
        landmark3DRadioButton.setText("3 D");
        landmark3DRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        landmark3DRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        landmark2DRadioButton.setText("2 D");
        landmark2DRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        landmark2DRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        landmarkInPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        org.jdesktop.layout.GroupLayout landmarkInPanelLayout = new org.jdesktop.layout.GroupLayout(landmarkInPanel);
        landmarkInPanel.setLayout(landmarkInPanelLayout);
        landmarkInPanelLayout.setHorizontalGroup(
            landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkInPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn2D3DDataPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(landmarkInPanelLayout.createSequentialGroup()
                        .add(landmark3DRadioButton)
                        .add(16, 16, 16)
                        .add(landmark2DRadioButton)))
                .addContainerGap())
        );
        landmarkInPanelLayout.setVerticalGroup(
            landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkInPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmark3DRadioButton)
                    .add(landmark2DRadioButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkIn2D3DDataPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }

    private void formLandmark3DPanel() {
        landmarkIn2D3DDataPanel = new javax.swing.JPanel();

        landmarkIn2D3DDataPanel.setBorder(javax.swing.BorderFactory
                .createTitledBorder(""));
        landmarkProjectLabel.setText("SeisWorks Project:");

        listProjectsButton.setText("List...");

        volumeLabel.setText("Volume:");

        listVolumesButton.setText("List...");

        landmarkRangeLabel.setText("Range:");

        rangeLinesLabel.setText("Lines");

        linesToLabel.setText("to");
        linesToLabel.setText("to");

        linesByLabel.setText("by");

        tracesLabel.setText("Traces");
        timeLabel.setText("Time/Depth");

        tracesToLabel.setText("to");
        timeToLabel.setText("to");
        unitLabel.setText("(millisec)");

        tracesByLabel.setText("by");

        entireVolumeButton.setText("Entire Volume");

        selectButton.setText("Select from map");
        selectButton.setEnabled(false);



        org.jdesktop.layout.GroupLayout landmarkIn3DPanelLayout = new org.jdesktop.layout.GroupLayout(landmarkIn2D3DDataPanel);
        landmarkIn2D3DDataPanel.setLayout(landmarkIn3DPanelLayout);
        landmarkIn3DPanelLayout.setHorizontalGroup(
            landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkIn3DPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn3DPanelLayout.createSequentialGroup()
                        .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(volumeLabel)
                            .add(landmarkProjectLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(landmarkInProjectTextField)
                            .add(volumeTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 437, Short.MAX_VALUE)))
                    .add(landmarkIn3DPanelLayout.createSequentialGroup()
                        .add(landmarkRangeLabel)
                        .add(17, 17, 17)
                        .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(landmarkIn3DPanelLayout.createSequentialGroup()
                                .add(entireVolumeButton)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(selectButton))
                            .add(landmarkIn3DPanelLayout.createSequentialGroup()
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(landmarkIn3DPanelLayout.createSequentialGroup()
                                        .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                            .add(rangeLinesLabel)
                                            .add(tracesLabel))
                                        .add(27, 27, 27))
                                    .add(landmarkIn3DPanelLayout.createSequentialGroup()
                                        .add(timeLabel)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)))
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, timeTextField)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, tracesTextField)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, linesTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE))
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(landmarkIn3DPanelLayout.createSequentialGroup()
                                        .add(12, 12, 12)
                                        .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                            .add(linesToLabel)
                                            .add(tracesToLabel)))
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, landmarkIn3DPanelLayout.createSequentialGroup()
                                        .add(12, 12, 12)
                                        .add(timeToLabel)))
                                .add(17, 17, 17)
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(timeToTextField)
                                    .add(tracesToTextField)
                                    .add(linesToTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE))
                                .add(18, 18, 18)
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(unitLabel)
                                    .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                        .add(landmarkIn3DPanelLayout.createSequentialGroup()
                                            .add(linesByLabel)
                                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                            .add(linesByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 52, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .add(landmarkIn3DPanelLayout.createSequentialGroup()
                                            .add(tracesByLabel)
                                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                            .add(tracesByTextField))))))))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 6, Short.MAX_VALUE)
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(listProjectsButton)
                    .add(listVolumesButton))
                .addContainerGap())
        );
        landmarkIn3DPanelLayout.setVerticalGroup(
            landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkIn3DPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(listProjectsButton)
                    .add(landmarkProjectLabel)
                    .add(landmarkInProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(volumeLabel)
                        .add(volumeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(listVolumesButton))
                .add(11, 11, 11)
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkRangeLabel)
                    .add(rangeLinesLabel)
                    .add(linesTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(linesToLabel)
                    .add(linesToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(linesByLabel)
                    .add(linesByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(tracesLabel)
                    .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(tracesToLabel)
                        .add(tracesTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(tracesToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(tracesByLabel)
                        .add(tracesByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(9, 9, 9)
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(timeLabel)
                    .add(timeToLabel)
                    .add(timeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(timeToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(unitLabel))
                .add(16, 16, 16)
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(entireVolumeButton)
                    .add(selectButton))
                .addContainerGap(165, Short.MAX_VALUE))
        );

    }

    private void formLandmark3DPanelOld() {
        landmarkIn2D3DDataPanel = new javax.swing.JPanel();

        landmarkIn2D3DDataPanel.setBorder(javax.swing.BorderFactory
                .createTitledBorder(""));
        landmarkProjectLabel.setText("SeisWorks Project:");

        listProjectsButton.setText("List...");

        volumeLabel.setText("Volume:");

        listVolumesButton.setText("List...");

        landmarkRangeLabel.setText("Range:");

        rangeLinesLabel.setText("Lines");

        linesToLabel.setText("to");

        linesByLabel.setText("by");

        tracesLabel.setText("Traces");

        tracesToLabel.setText("to");

        tracesByLabel.setText("by");

        entireVolumeButton.setText("Entire Volume");

        selectButton.setText("Select from map");
        selectButton.setEnabled(false);
        org.jdesktop.layout.GroupLayout landmarkIn3DPanelLayout = new org.jdesktop.layout.GroupLayout(landmarkIn2D3DDataPanel);
        landmarkIn2D3DDataPanel.setLayout(landmarkIn3DPanelLayout);
        landmarkIn3DPanelLayout.setHorizontalGroup(
            landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkIn3DPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn3DPanelLayout.createSequentialGroup()
                        .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(volumeLabel)
                            .add(landmarkProjectLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(landmarkInProjectTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
                            .add(volumeTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)))
                    .add(landmarkIn3DPanelLayout.createSequentialGroup()
                        .add(landmarkRangeLabel)
                        .add(17, 17, 17)
                        .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(landmarkIn3DPanelLayout.createSequentialGroup()
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(rangeLinesLabel)
                                    .add(tracesLabel))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(tracesTextField)
                                    .add(linesTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE))
                                .add(14, 14, 14)
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(linesToLabel)
                                    .add(tracesToLabel))
                                .add(13, 13, 13)
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(tracesToTextField)
                                    .add(linesToTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 49, Short.MAX_VALUE))
                                .add(16, 16, 16)
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(tracesByLabel)
                                    .add(linesByLabel))
                                .add(12, 12, 12)
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(tracesByTextField)
                                    .add(linesByTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)))
                            .add(landmarkIn3DPanelLayout.createSequentialGroup()
                                .add(entireVolumeButton)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(selectButton)))))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(listProjectsButton)
                    .add(listVolumesButton))
                .addContainerGap())
        );
        landmarkIn3DPanelLayout.setVerticalGroup(
            landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkIn3DPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(listProjectsButton)
                    .add(landmarkProjectLabel)
                    .add(landmarkInProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(volumeLabel)
                        .add(volumeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(listVolumesButton))
                .add(11, 11, 11)
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkRangeLabel)
                    .add(rangeLinesLabel)
                    .add(linesToLabel)
                    .add(linesToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(linesTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(linesByLabel)
                    .add(linesByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn3DPanelLayout.createSequentialGroup()
                        .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(tracesLabel)
                                .add(tracesTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(tracesToLabel)
                                .add(tracesToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(tracesByLabel)))
                        .add(15, 15, 15)
                        .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(entireVolumeButton)
                            .add(selectButton)))
                    .add(tracesByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(196, Short.MAX_VALUE))
        );

    }
    /**
     * Form the interior landmarkInPanel used in the multi-purpose Seismic Data
     * Input tab.
     */
    private void formLandmarkInPanelOld() {
        landmarkInPanel.setBorder(javax.swing.BorderFactory
                .createTitledBorder(""));
        landmarkProjectLabel.setText("SeisWorks Project:");

        listProjectsButton.setText("List...");

        volumeLabel.setText("Volume:");

        listVolumesButton.setText("List...");

        landmarkRangeLabel.setText("Range:");

        rangeLinesLabel.setText("Lines");

        //linesTextField.setText("0");

        linesToLabel.setText("to");

        //linesToTextField.setText("0");

        linesByLabel.setText("by");

        //linesByTextField.setText("1");

        tracesLabel.setText("Traces");

        //tracesTextField.setText("0");

        tracesToLabel.setText("to");

        //tracesToTextField.setText("0");

        tracesByLabel.setText("by");

        //tracesByTextField.setText("1");

        entireVolumeButton.setText("Entire Volume");

        selectButton.setText("Select from map");
        selectButton.setEnabled(false);
        org.jdesktop.layout.GroupLayout landmarkInPanelLayout = new org.jdesktop.layout.GroupLayout(
            landmarkInPanel);
        landmarkInPanel.setLayout(landmarkInPanelLayout);
        landmarkInPanelLayout
            .setHorizontalGroup(landmarkInPanelLayout
                .createParallelGroup(
                    org.jdesktop.layout.GroupLayout.LEADING)
                .add(landmarkInPanelLayout
                    .createSequentialGroup()
                    .addContainerGap()
                    .add(landmarkInPanelLayout
                        .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(landmarkInPanelLayout
                                .createSequentialGroup()
                                .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(volumeLabel)
                                    .add(landmarkProjectLabel))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(volumeTextField,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        361,
                                        Short.MAX_VALUE)
                                    .add(landmarkInProjectTextField,
                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                        361,
                                        Short.MAX_VALUE))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(landmarkInPanelLayout
                                    .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(listVolumesButton)
                                    .add(listProjectsButton))
                                .addContainerGap())
                            .add(landmarkInPanelLayout
                                .createSequentialGroup()
                                .add(landmarkRangeLabel)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(landmarkInPanelLayout
                                    .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING,false)
                                    .add(landmarkInPanelLayout
                                        .createSequentialGroup()
                                        .add(landmarkInPanelLayout
                                            .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                                .add(rangeLinesLabel)
                                                .add(tracesLabel))
                                        .add(8,8,8)
                                        .add(landmarkInPanelLayout
                                            .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                                .add(landmarkInPanelLayout
                                                    .createSequentialGroup()
                                                    .add(linesTextField,
                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                        49,
                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                                    .add(linesToLabel)
                                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                                    .add(linesToTextField,
                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                        49,
                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                                    .add(linesByLabel)
                                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                                    .add(
                                                        linesByTextField,
                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                        48,
                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                                .add(
                                                    landmarkInPanelLayout
                                                        .createSequentialGroup()
                                                        .add(tracesTextField,
                                                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                            49,
                                                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                                        .add(tracesToLabel)
                                                        .addPreferredGap(
                                                            org.jdesktop.layout.LayoutStyle.RELATED)
                                                        .add(
                                                            tracesToTextField,
                                                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                            49,
                                                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(
                                                            org.jdesktop.layout.LayoutStyle.RELATED)
                                                        .add(tracesByLabel)
                                                        .addPreferredGap(
                                                            org.jdesktop.layout.LayoutStyle.RELATED)
                                                        .add(
                                                            tracesByTextField,
                                                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                            48,
                                                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                                    .add(landmarkInPanelLayout
                                        .createSequentialGroup()
                                            .add(entireVolumeButton)
                                            .addPreferredGap(
                                                org.jdesktop.layout.LayoutStyle.RELATED,
                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                Short.MAX_VALUE)
                                            .add(selectButton)))))));
        landmarkInPanelLayout
            .setVerticalGroup(landmarkInPanelLayout
                .createParallelGroup(
                    org.jdesktop.layout.GroupLayout.LEADING)
                .add(
                    landmarkInPanelLayout
                        .createSequentialGroup()
                        .add(
                            landmarkInPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(
                                    landmarkProjectLabel)
                                .add(listProjectsButton)
                                .add(
                                    landmarkInProjectTextField,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(
                            org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(
                            landmarkInPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(volumeLabel)
                                .add(
                                    volumeTextField,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(listVolumesButton))
                        .addPreferredGap(
                            org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(
                            landmarkInPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(landmarkRangeLabel)
                                .add(rangeLinesLabel)
                                .add(
                                    linesByTextField,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(linesByLabel)
                                .add(
                                    linesToTextField,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(linesToLabel)
                                .add(
                                    linesTextField,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(
                            org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(
                            landmarkInPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(tracesLabel)
                                .add(
                                    tracesTextField,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                    19,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(tracesToLabel)
                                .add(
                                    tracesToTextField,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(tracesByLabel)
                                .add(
                                    tracesByTextField,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(
                            org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(
                            landmarkInPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(entireVolumeButton)
                                .add(selectButton))
                        .addContainerGap(244, Short.MAX_VALUE)));
    }

    private void formLandmarkInputDataPanel() {
        org.jdesktop.layout.GroupLayout landmarkInputDataPanelLayout = new org.jdesktop.layout.GroupLayout(
                landmarkInputDataPanel);
        landmarkInputDataPanel.setLayout(landmarkInputDataPanelLayout);
        landmarkInputDataPanelLayout
            .setHorizontalGroup(landmarkInputDataPanelLayout
                .createParallelGroup(
                    org.jdesktop.layout.GroupLayout.LEADING)
                .add(
                    landmarkInputDataPanelLayout
                        .createSequentialGroup()
                        .addContainerGap()
                        .add(
                            landmarkInputDataPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.LEADING)
                                .add(
                                    landmarkInPanel,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    Short.MAX_VALUE)
                                .add(
                                    landmarkInputDataPanelLayout
                                        .createSequentialGroup()
                                        .add(
                                            landmarkInRadioButton)
                                        .addPreferredGap(
                                            org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(
                                            bhpsuInRadioButton)))
                        .addContainerGap()));
        landmarkInputDataPanelLayout
            .setVerticalGroup(landmarkInputDataPanelLayout
                .createParallelGroup(
                    org.jdesktop.layout.GroupLayout.LEADING)
                .add(
                    landmarkInputDataPanelLayout
                        .createSequentialGroup()
                        .addContainerGap()
                        .add(
                            landmarkInputDataPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(
                                    landmarkInRadioButton)
                                .add(bhpsuInRadioButton))
                        .addPreferredGap(
                            org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(
                            landmarkInPanel,
                            org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                            org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                            Short.MAX_VALUE)
                        .addContainerGap()));
    }

    /**
     * Form the interior bhpsuOutPanel used in the multi-purpose Outputs tab.
     */
    private void formBhpsuOutPanel() {
        bhpsuOutPanel.setBorder(javax.swing.BorderFactory
                .createTitledBorder(""));
        bhpsuNameBaseLabel.setText("Output Name Base:");

        bhpsuScaleVolumesLabel.setText("Scale Volumes");

        bhpsuScaleRawRadioButton.setText("raw");
        bhpsuScaleRawRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        bhpsuScaleRawRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        bhpsuScaleNormalizedRadioButton.setText("normalized");
        bhpsuScaleNormalizedRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        bhpsuScaleNormalizedRadioButton.setMargin(new java.awt.Insets(0, 0, 0,
                0));

        //bhpsuNoiseFactorTextField.setText("0");

        bhpsuNoiseFactorLabel.setText("(Noise Factor, db)");

        org.jdesktop.layout.GroupLayout bhpsuOutPanelLayout = new org.jdesktop.layout.GroupLayout(
                bhpsuOutPanel);
        bhpsuOutPanel.setLayout(bhpsuOutPanelLayout);
        bhpsuOutPanelLayout
            .setHorizontalGroup(bhpsuOutPanelLayout
                .createParallelGroup(
                    org.jdesktop.layout.GroupLayout.LEADING)
                .add(
                    bhpsuOutPanelLayout
                        .createSequentialGroup()
                        .add(
                            bhpsuOutPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.LEADING)
                                .add(bhpsuNameBaseLabel)
                                .add(
                                    bhpsuScaleVolumesLabel))
                        .addPreferredGap(
                            org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(
                            bhpsuOutPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.LEADING)
                                .add(
                                    bhpsuScaleRawRadioButton)
                                .add(
                                    bhpsuNameBaseTextField,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                    389,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(
                                    bhpsuOutPanelLayout
                                        .createSequentialGroup()
                                        .add(
                                            bhpsuScaleNormalizedRadioButton)
                                        .add(
                                            20,
                                            20,
                                            20)
                                        .add(
                                            bhpsuNoiseFactorTextField,
                                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                            55,
                                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(
                                            org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(
                                            bhpsuNoiseFactorLabel)))
                        .add(14, 14, 14)));
        bhpsuOutPanelLayout
            .setVerticalGroup(bhpsuOutPanelLayout
                .createParallelGroup(
                    org.jdesktop.layout.GroupLayout.LEADING)
                .add(
                    bhpsuOutPanelLayout
                        .createSequentialGroup()
                        .add(
                            bhpsuOutPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.LEADING)
                                .add(
                                    bhpsuOutPanelLayout
                                        .createSequentialGroup()
                                        .add(
                                            25,
                                            25,
                                            25)
                                        .add(
                                            bhpsuScaleRawRadioButton)
                                        .addPreferredGap(
                                            org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(
                                            bhpsuOutPanelLayout
                                                .createParallelGroup(
                                                    org.jdesktop.layout.GroupLayout.BASELINE)
                                                .add(
                                                    bhpsuScaleNormalizedRadioButton)
                                                .add(
                                                    bhpsuNoiseFactorTextField,
                                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                .add(
                                                    bhpsuNoiseFactorLabel)))
                                .add(
                                    bhpsuOutPanelLayout
                                        .createSequentialGroup()
                                        .add(
                                            bhpsuOutPanelLayout
                                                .createParallelGroup(
                                                    org.jdesktop.layout.GroupLayout.BASELINE)
                                                .add(
                                                    bhpsuNameBaseLabel)
                                                .add(
                                                    bhpsuNameBaseTextField,
                                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(
                                            org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(
                                            bhpsuScaleVolumesLabel)))
                        .addContainerGap(5, Short.MAX_VALUE)));
    }

    private void formBhpsuOutputsPanel() {
        org.jdesktop.layout.GroupLayout bhpsuOutputsPanelLayout = new org.jdesktop.layout.GroupLayout(
                bhpsuOutputsPanel);
        bhpsuOutputsPanel.setLayout(bhpsuOutputsPanelLayout);
        bhpsuOutputsPanelLayout
            .setHorizontalGroup(bhpsuOutputsPanelLayout
                .createParallelGroup(
                    org.jdesktop.layout.GroupLayout.LEADING)
                .add(
                    bhpsuOutputsPanelLayout
                        .createSequentialGroup()
                        .addContainerGap()
                        .add(
                            bhpsuOutputsPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.LEADING)
                                .add(
                                    bhpsuOutPanel,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    Short.MAX_VALUE)
                                .add(
                                    bhpsuOutputsPanelLayout
                                        .createSequentialGroup()
                                        .add(
                                            landmarkOutRadioButton)
                                        .addPreferredGap(
                                            org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(
                                            bhpsuOutRadioButton)))
                        .addContainerGap()));
        bhpsuOutputsPanelLayout
            .setVerticalGroup(bhpsuOutputsPanelLayout
                .createParallelGroup(
                    org.jdesktop.layout.GroupLayout.LEADING)
                .add(
                    bhpsuOutputsPanelLayout
                        .createSequentialGroup()
                        .addContainerGap()
                        .add(
                            bhpsuOutputsPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(
                                    landmarkOutRadioButton)
                                .add(
                                    bhpsuOutRadioButton))
                        .addPreferredGap(
                            org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(
                            bhpsuOutPanel,
                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                            org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(242, Short.MAX_VALUE)));
    }


    /**
     * Form the interior landmarkOutPanel used in the multi-purpose Outputs tab.
     */
    private void formLandmarkOutPanel1() {
        landmarkOutPanel.setBorder(javax.swing.BorderFactory
                .createTitledBorder(""));
        landmarkNameBaseLabel.setText("Output Name Base:");

        //landmarkScaleVolumesLabel.setText("Scale Volumes");

        landmarkScaleRawRadioButton.setText("raw");
        landmarkScaleRawRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        landmarkScaleRawRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        landmarkScaleNormalizedRadioButton.setText("normalized");
        landmarkScaleNormalizedRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        landmarkScaleNormalizedRadioButton.setMargin(new java.awt.Insets(0, 0, 0,
                0));

        //landmarkNoiseFactorTextField.setText("0");

        //landmarkNoiseFactorLabel.setText("(Noise Factor, db)");

        org.jdesktop.layout.GroupLayout landmarkOutPanelLayout = new org.jdesktop.layout.GroupLayout(
                landmarkOutPanel);
        landmarkOutPanel.setLayout(landmarkOutPanelLayout);
        landmarkOutPanelLayout
            .setHorizontalGroup(landmarkOutPanelLayout
                .createParallelGroup(
                    org.jdesktop.layout.GroupLayout.LEADING)
                .add(
                    landmarkOutPanelLayout
                        .createSequentialGroup()
                        .add(
                            landmarkOutPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.LEADING)
                                .add(
                                    landmarkNameBaseLabel)
                                .add(
                                    landmarkScaleVolumesLabel))
                        .addPreferredGap(
                            org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(
                            landmarkOutPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.LEADING)
                                .add(
                                    landmarkScaleRawRadioButton)
                                .add(
                                    landmarkNameBaseTextField,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                    389,
                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(
                                    landmarkOutPanelLayout
                                        .createSequentialGroup()
                                        .add(
                                            landmarkScaleNormalizedRadioButton)
                                        .add(
                                            20,
                                            20,
                                            20)
                                        .add(
                                            landmarkNoiseFactorTextField,
                                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                            55,
                                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(
                                            org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(
                                            landmarkNoiseFactorLabel))
                        )
                        .add(14, 14, 14)));
        landmarkOutPanelLayout
            .setVerticalGroup(landmarkOutPanelLayout
                .createParallelGroup(
                    org.jdesktop.layout.GroupLayout.LEADING)
                .add(
                    landmarkOutPanelLayout
                        .createSequentialGroup()
                        .add(
                            landmarkOutPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.LEADING)
                                .add(
                                    landmarkOutPanelLayout
                                        .createSequentialGroup()
                                        .add(
                                            25,
                                            25,
                                            25)
                                        .add(
                                            landmarkScaleRawRadioButton)
                                        .addPreferredGap(
                                            org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(
                                            landmarkOutPanelLayout
                                                .createParallelGroup(
                                                    org.jdesktop.layout.GroupLayout.BASELINE)
                                                .add(
                                                    landmarkScaleNormalizedRadioButton)
                                                .add(
                                                    landmarkNoiseFactorTextField,
                                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                .add(
                                                    landmarkNoiseFactorLabel))
                                      .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                )
                                .add(
                                    landmarkOutPanelLayout
                                        .createSequentialGroup()
                                        .add(
                                            landmarkOutPanelLayout
                                                .createParallelGroup(
                                                    org.jdesktop.layout.GroupLayout.BASELINE)
                                                .add(
                                                    landmarkNameBaseLabel)
                                                .add(
                                                    landmarkNameBaseTextField,
                                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                    org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(
                                            org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(
                                            landmarkScaleVolumesLabel)))
                        .addContainerGap(5, Short.MAX_VALUE)));
    }

    private void formLandmarkScaleOutputPanel0(int number) {
        landmarkNameBaseLabel.setText("Output Name Base:");
        landmarkScaleRawRadioButton.setText("raw");
        landmarkScaleRawRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        landmarkScaleRawRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        landmarkScaleNormalizedRadioButton.setText("normalized");
        landmarkScaleNormalizedRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        landmarkScaleNormalizedRadioButton.setMargin(new java.awt.Insets(0, 0, 0,
                0));
        landmarkNumOfScalesComboBox = new javax.swing.JComboBox();
        if(!validateNumberScalesTextField())
            return;
        int num = Integer.valueOf(numberScalesTextField.getText().trim()).intValue();

        String [] scaleArray = new String[num+1];
        for(int i = 0; i < scaleArray.length; i++){
            if(i == 0)
                scaleArray[i] = "None";
            else
                scaleArray[i] = String.valueOf(i);
        }
        landmarkNumOfScalesComboBox.setModel(new javax.swing.DefaultComboBoxModel(
                scaleArray));
        if(num < landmarkOutNumScalesCount){
            landmarkNumOfScalesComboBox.setSelectedIndex(num);
            landmarkOutputScaleTextFieldList = new ArrayList<JTextField>();
            landmarkOutputScaleLabelList = new ArrayList<JLabel>();

            for (int i = 0; i < num; i++) {
                landmarkOutputScaleLabelList.add(new JLabel("Scale " + (i + 1)));
                landmarkOutputScaleTextFieldList.add(new JTextField());
            }
            formLandmarkNumScaleOutputPanel(num,landmarkOutputScaleTextFieldList,landmarkOutputScaleLabelList);
            formLandmarkScaleOutputPanel(landmarkNameBaseTextField,landmarkOutProjectTextField,landmarkNoiseFactorTextField,
                    landmarkNumOfScalesComboBox,landmarkScaleNormalizedRadioButton,landmarkScaleRawRadioButton);
            return;
        }else
            landmarkNumOfScalesComboBox.setSelectedIndex(number);
            landmarkNumOfScalesComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JComboBox cb = (JComboBox) evt.getSource();
                int numberOfScales = cb.getSelectedIndex();
                switch2LandmarkOut(landmarkOutputTimeCount,numberOfScales,1);
                landmarkOutNumScalesCount = numberOfScales;
            }
        });

        if(landmarkOutNumScalesCount != number){
            landmarkOutputScaleTextFieldList = new ArrayList<JTextField>();
            landmarkOutputScaleLabelList = new ArrayList<JLabel>();

            for (int i = 0; i < number; i++) {
                landmarkOutputScaleLabelList.add(new JLabel("Scale " + (i + 1)));
                landmarkOutputScaleTextFieldList.add(new JTextField());
            }
        }

        formLandmarkNumScaleOutputPanel(number,landmarkOutputScaleTextFieldList,landmarkOutputScaleLabelList);
        formLandmarkScaleOutputPanel(landmarkNameBaseTextField,landmarkOutProjectTextField,landmarkNoiseFactorTextField,
                landmarkNumOfScalesComboBox,landmarkScaleNormalizedRadioButton,landmarkScaleRawRadioButton);

    }


    private void formLandmark2DScaleOutputPanel0(int number) {
        landmarkNameBaseLabel.setText("Output Name Base:");
        landmark2DScaleRawRadioButton.setText("raw");
        landmark2DScaleRawRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        landmark2DScaleRawRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        landmark2DScaleNormalizedRadioButton.setText("normalized");
        landmark2DScaleNormalizedRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        landmark2DScaleNormalizedRadioButton.setMargin(new java.awt.Insets(0, 0, 0,
                0));
        landmark2DNumOfScalesComboBox = new javax.swing.JComboBox();
        if(!validateNumberScalesTextField())
            return;
        int num = Integer.valueOf(numberScalesTextField.getText().trim()).intValue();

        String [] scaleArray = new String[num+1];
        for(int i = 0; i < scaleArray.length; i++){
            if(i == 0)
                scaleArray[i] = "None";
            else
                scaleArray[i] = String.valueOf(i);
        }
        landmark2DNumOfScalesComboBox.setModel(new javax.swing.DefaultComboBoxModel(
                scaleArray));
        if(num < landmark2DOutNumScalesCount){
            landmark2DNumOfScalesComboBox.setSelectedIndex(num);
            landmark2DOutputScaleTextFieldList = new ArrayList<JTextField>();
            landmark2DOutputScaleLabelList = new ArrayList<JLabel>();

            for (int i = 0; i < num; i++) {
                landmark2DOutputScaleLabelList.add(new JLabel("Scale " + (i + 1)));
                landmark2DOutputScaleTextFieldList.add(new JTextField());
            }
            formLandmarkNumScaleOutputPanel(num,landmark2DOutputScaleTextFieldList,landmark2DOutputScaleLabelList);
            formLandmarkScaleOutputPanel(landmark2DNameBaseTextField,landmark2DOutProjectTextField,landmark2DNoiseFactorTextField,
                    landmark2DNumOfScalesComboBox,landmark2DScaleNormalizedRadioButton,landmark2DScaleRawRadioButton);
            return;
        }else
            landmark2DNumOfScalesComboBox.setSelectedIndex(number);
            landmark2DNumOfScalesComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JComboBox cb = (JComboBox) evt.getSource();
                int numberOfScales = cb.getSelectedIndex();
                switch2LandmarkOut(landmark2DOutputTimeCount,numberOfScales,2);
                landmark2DOutNumScalesCount = numberOfScales;
            }
        });

        if(landmark2DOutNumScalesCount != number){
            landmark2DOutputScaleTextFieldList = new ArrayList<JTextField>();
            landmark2DOutputScaleLabelList = new ArrayList<JLabel>();

            for (int i = 0; i < number; i++) {
                landmark2DOutputScaleLabelList.add(new JLabel("Scale " + (i + 1)));
                landmark2DOutputScaleTextFieldList.add(new JTextField());
            }
        }

        formLandmarkNumScaleOutputPanel(number,landmark2DOutputScaleTextFieldList,landmark2DOutputScaleLabelList);
        formLandmarkScaleOutputPanel(landmark2DNameBaseTextField,landmark2DOutProjectTextField,landmark2DNoiseFactorTextField,
                landmark2DNumOfScalesComboBox,landmark2DScaleNormalizedRadioButton,landmark2DScaleRawRadioButton);

    }

    private void formLandmark3DScaleOutputPanel0(int number) {
        landmarkNameBaseLabel.setText("Output Name Base:");
        landmark3DScaleRawRadioButton.setText("raw");
        landmark3DScaleRawRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        landmark3DScaleRawRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        landmark3DScaleNormalizedRadioButton.setText("normalized");
        landmark3DScaleNormalizedRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        landmark3DScaleNormalizedRadioButton.setMargin(new java.awt.Insets(0, 0, 0,
                0));
        landmark3DNumOfScalesComboBox = new javax.swing.JComboBox();
        if(!validateNumberScalesTextField())
            return;
        int num = Integer.valueOf(numberScalesTextField.getText().trim()).intValue();

        String [] scaleArray = new String[num+1];
        for(int i = 0; i < scaleArray.length; i++){
            if(i == 0)
                scaleArray[i] = "None";
            else
                scaleArray[i] = String.valueOf(i);
        }
        landmark3DNumOfScalesComboBox.setModel(new javax.swing.DefaultComboBoxModel(
                scaleArray));
        if(num < landmark3DOutNumScalesCount){
            landmark3DNumOfScalesComboBox.setSelectedIndex(num);
            landmark3DOutputScaleTextFieldList = new ArrayList<JTextField>();
            landmark3DOutputScaleLabelList = new ArrayList<JLabel>();

            for (int i = 0; i < num; i++) {
                landmark3DOutputScaleLabelList.add(new JLabel("Scale " + (i + 1)));
                landmark3DOutputScaleTextFieldList.add(new JTextField());
            }
            formLandmarkNumScaleOutputPanel(num,landmark3DOutputScaleTextFieldList,landmark3DOutputScaleLabelList);
            formLandmarkScaleOutputPanel(landmark3DNameBaseTextField,landmark3DOutProjectTextField,landmark3DNoiseFactorTextField,
                    landmark3DNumOfScalesComboBox,landmark3DScaleNormalizedRadioButton,landmark3DScaleRawRadioButton);
            return;
        }else
            landmark3DNumOfScalesComboBox.setSelectedIndex(number);
            landmark3DNumOfScalesComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JComboBox cb = (JComboBox) evt.getSource();
                int numberOfScales = cb.getSelectedIndex();
                switch2LandmarkOut(landmarkOutputTimeCount,numberOfScales,3);
                landmark3DOutNumScalesCount = numberOfScales;
            }
        });

        if(landmark3DOutNumScalesCount != number){
            landmark3DOutputScaleTextFieldList = new ArrayList<JTextField>();
            landmark3DOutputScaleLabelList = new ArrayList<JLabel>();

            for (int i = 0; i < number; i++) {
                landmark3DOutputScaleLabelList.add(new JLabel("Scale " + (i + 1)));
                landmark3DOutputScaleTextFieldList.add(new JTextField());
            }
        }

        formLandmarkNumScaleOutputPanel(number,landmark3DOutputScaleTextFieldList,landmark3DOutputScaleLabelList);
        formLandmarkScaleOutputPanel(landmark3DNameBaseTextField,landmark3DOutProjectTextField,landmark3DNoiseFactorTextField,
                landmark3DNumOfScalesComboBox,landmark3DScaleNormalizedRadioButton,landmark3DScaleRawRadioButton);

    }

    /**
     * Form the interior flattenedPanel used in the multi-purpose Outputs tab.
     */
    private void formLandmarkFlattenedPanel(int count) {
        flattenedPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(
                //null, "(flattened)",
                null, "",
                javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                javax.swing.border.TitledBorder.DEFAULT_POSITION,
                new java.awt.Font("Tahoma", 1, 11)));

        horizonRawRadioButton.setText("raw");
        horizonRawRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        horizonRawRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        placementLabel.setText("Horizon Placement");

        horizonNormalizedRadioButton.setText("normalized");
        horizonNormalizedRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        horizonNormalizedRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        timeComboBox.setModel(new javax.swing.DefaultComboBoxModel(
                new String[] { "None", "1 time", "2 times", "3 times",
                        "4 times", "5 times", "6 times", "7 times", "8 times",
                        "9 times", "10 times", "11 times", "12 times",
                        "13 times", "14 times", "15 times", "16 times",
                        "17 times", "18 times", "19 times", "20 times" }));

        timeComboBox.setSelectedIndex(count);
        timeComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                timeComboBoxActionPerformed(evt);
            }
        });

        formLandmarkOutputTimePanel(count);
        ////formOutputFlattenedPanel();
    }

    private void formLandmarkOutputsPanel() {
        landmarkOutputsPanel.setAutoscrolls(true);
        org.jdesktop.layout.GroupLayout landmarkOutputsPanelLayout = new org.jdesktop.layout.GroupLayout(
                landmarkOutputsPanel);
        landmarkOutputsPanel.setLayout(landmarkOutputsPanelLayout);
        landmarkOutputsPanelLayout
            .setHorizontalGroup(landmarkOutputsPanelLayout
                .createParallelGroup(
                    org.jdesktop.layout.GroupLayout.LEADING)
                .add(
                    org.jdesktop.layout.GroupLayout.TRAILING,
                    landmarkOutputsPanelLayout
                        .createSequentialGroup()
                        .addContainerGap()
                        .add(
                            landmarkOutputsPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.TRAILING)
                                .add(
                                    org.jdesktop.layout.GroupLayout.LEADING,
                                    flattenedPanel,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    Short.MAX_VALUE)
                                .add(
                                    org.jdesktop.layout.GroupLayout.LEADING,
                                    landmarkOutPanel,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                    Short.MAX_VALUE)
                                .add(
                                    org.jdesktop.layout.GroupLayout.LEADING,
                                    landmarkOutputsPanelLayout
                                        .createSequentialGroup()
                                        .add(
                                            landmarkOutRadioButton)
                                        .addPreferredGap(
                                            org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(
                                            bhpsuOutRadioButton)))
                        .addContainerGap()));
        landmarkOutputsPanelLayout
            .setVerticalGroup(landmarkOutputsPanelLayout
                .createParallelGroup(
                    org.jdesktop.layout.GroupLayout.LEADING)
                .add(
                    landmarkOutputsPanelLayout
                        .createSequentialGroup()
                        .addContainerGap()
                        .add(
                            landmarkOutputsPanelLayout
                                .createParallelGroup(
                                    org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(
                                    landmarkOutRadioButton)
                                .add(
                                    bhpsuOutRadioButton))
                        .addPreferredGap(
                            org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(
                            landmarkOutPanel,
                            org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
//                          180,
//                          250)
                            org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                            Short.MAX_VALUE)
                        .addPreferredGap(
                            org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(
                            flattenedPanel,
                            org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                            168,
                            250)
                        .addContainerGap()));
    }

    /**
     * Rename the plugin
     *
     * @param name
     *            from plugin message processor
     */
    public void renamePlugin(String name) {
//        this.setTitle(name + "  Project: " + project_name);
        QiProjectDescriptor qpDesc = agent.getQiProjectDescriptor();
        project_name = QiProjectDescUtils.getQiProjectName(qpDesc);
        resetTitle(project_name);
    }

    /**
     * Save the state information for the Wavelet Decomposition GUI.
     *
     * @return xml string.
     */
    // TODO: rewrite WaveletDecomp saveState()
    public String genState() {
        StringBuffer content = new StringBuffer();

        content.append("<" + this.getClass().getName() + " ");
        content.append("height=\"" + this.getHeight() + "\" ");
        content.append("width=\"" + this.getWidth() + "\" ");
        content.append("x=\"" + this.getX() + "\" ");
        content.append("y=\"" + this.getY() + "\" ");

        String temp = "";
        if(bhpsuInRadioButton.isSelected()){
            temp = bhpsuInRadioButton.getText().trim();
            content.append("seismicDataInputType=\""  + temp + "\" ");
        }else if(landmarkInRadioButton.isSelected()){
            temp = landmarkInRadioButton.getText().trim();
            content.append("seismicDataInputType=\""  + temp + "\" ");
            if(landmark3DRadioButton.isSelected())
                content.append("LandmarkInputType=\"" + "3D" + "\" ");
            else if(landmark2DRadioButton.isSelected())
                content.append("LandmarkInputType=\"" + "2D" + "\" ");
        }

        //Stratigraphic Flattening
        if(landmarkHorizonRadioButton.isSelected()){
            temp = landmarkHorizonRadioButton.getText();
            content.append("horizon_type=\"" + temp + "\" ");
        }else if(bhpsuHorizonRadioButton.isSelected()){
            temp = bhpsuHorizonRadioButton.getText();
            content.append("horizon_type=\"" + temp + "\" ");
        }else if(bhpsuHorizonDatasetRadioButton.isSelected()){
            temp = bhpsuHorizonDatasetRadioButton.getText();
            content.append("horizon_type=\"" + temp + "\" ");
        }
//      Output Tab
        if(bhpsuOutRadioButton.isSelected()){
            content.append("output_type=\"" + "BHP SU" + "\" ");
        }else if(landmarkOutRadioButton.isSelected()){
            content.append("output_type=\"" + "Landmark and BHP SU" + "\" ");
            //content.append("landmark_out_project=\"" + landmarkOutProjectTextField.getText() + "\" ");
        }


        if(defaultPreferredScriptDir.length() > 0)
            content.append("defaultScriptDir=\"" + defaultPreferredScriptDir  + "\" ");
        if(currentRunningScriptPrefix.length() > 0){
            String pid = agent.getProcessId(defaultPreferredScriptDir + filesep +currentRunningScriptPrefix + ".sh");
            if(pid != null){
                content.append("scriptNamePrefix=\"" + currentRunningScriptPrefix  + "\" ");
                if(pid.length() > 0){
                    content.append("jobStatus=\"" + "Running"  + "\" ");
                } else if(pid.length() == 0)
                    content.append("jobStatus=\"" + "Ended"  + "\" ");
            }
        }
//      Input Parameters
        temp = smallestScaleTextField.getText() == null ? "" : smallestScaleTextField.getText().trim();
        content.append("smallest_scale=\"" + temp + "\" ");
        temp = largestScaleTextField.getText() == null ? "" : largestScaleTextField.getText().trim();
        content.append("largest_scale=\"" + temp + "\" ");
        temp = numberScalesTextField.getText() == null ? "" : numberScalesTextField.getText().trim();
        content.append("number_scales=\"" + temp + "\" ");
        temp = averageVelocityTextField.getText() == null ? "" : averageVelocityTextField.getText().trim();
        content.append("velocity=\"" + temp + "\" ");
        temp = numberOfPassTextField.getText() == null ? "" : numberOfPassTextField.getText().trim();
        content.append("npass=\"" + temp + "\" ");
        temp = smoothingLengthTextField.getText() == null ? "" : smoothingLengthTextField.getText().trim();
        content.append("smoothing_length=\"" + temp + "\" >\n");
        content.append("<BHPSU ");
            //temp = selectedProjectLabel.getText() == null ? "" : selectedProjectLabel.getText().trim();
            //content.append("project_name=\""  + temp + "\" ");
        temp = datasetTextField.getText() == null ? "" : datasetTextField.getText().trim();

        content.append("inputDataset=\"" + temp + "\" ");
        //temp = bhpsuNameBaseTextField.getText() == null ? "" : bhpsuNameBaseTextField.getText().trim();
        //content.append("outputNameBase=\"" + temp + "\" ");
        temp = cdpTextField.getText() == null ? "" : cdpTextField.getText().trim();
        content.append("mincdp=\"" + temp + "\" ");
        temp = cdpToTextField.getText() == null ? "" : cdpToTextField.getText().trim();
        content.append("maxcdp=\"" + temp + "\" ");
        temp = cdpByTextField.getText() == null ? "" : cdpByTextField.getText().trim();
        content.append("inccdp=\"" + temp + "\" ");
        temp = epTextField.getText() == null ? "" : epTextField.getText().trim();
        content.append("minep=\"" + temp + "\" ");
        temp = epToTextField.getText() == null ? "" : epToTextField.getText().trim();
        content.append("maxep=\"" + temp + "\" ");
        temp = epByTextField.getText() == null ? "" : epByTextField.getText().trim();
        content.append("incep=\"" + temp + "\" ");
        if(defaultcdpMin != -1)
            content.append("default_mincdp=\"" + defaultcdpMin + "\" ");
        else
            content.append("default_mincdp=\"" + "" + "\" ");
        if(defaultcdpMax != -1)
            content.append("default_maxcdp=\"" + defaultcdpMax + "\" ");
        else
            content.append("default_maxcdp=\"" + "" + "\" ");

        if(defaultcdpBy != -1)
            content.append("default_inccdp=\"" + defaultcdpBy + "\" ");
        else
            content.append("default_inccdp=\"" + "" + "\" ");
        if(defaultepMin != -1)
            content.append("default_minep=\"" + defaultepMin + "\" ");
        else
            content.append("default_minep=\"" + "" + "\" ");
        if(defaultepMax != -1)
            content.append("default_maxep=\"" + defaultepMax + "\" ");
        else
            content.append("default_maxep=\"" + "" + "\" ");
        if(defaultepBy != -1)
            content.append("default_incep=\"" + defaultepBy + "\" ");
        else
            content.append("default_incep=\"" + "" + "\" ");

        StringBuffer tempB = new StringBuffer();
        for (int i = 0; suHorzTextFieldList != null && i < suHorzTextFieldList.size(); i++) {
            tempB.append(suHorzTextFieldList.get(i).getText().trim());
            if (i < suHorzTextFieldList.size() - 1)
                tempB.append(",");
        }
        content.append("horizon_files=\"" + tempB.toString() + "\" ");
        temp = "";
        temp = suStartTimeTextField.getText() == null ? "" : suStartTimeTextField.getText().trim();
        content.append("tmin=\"" + temp + "\" ");
        temp = timeIncrementTextField.getText() == null ? "" : timeIncrementTextField.getText().trim();
        content.append("dt=\"" + temp + "\" ");
        temp = suEndTimeTextField.getText() == null ? "" : suEndTimeTextField.getText().trim();
        content.append("tmax=\"" + temp + "\" ");

        temp = "";
        temp = bhpsuNameBaseTextField.getText() == null ? "" : bhpsuNameBaseTextField.getText().trim();
        content.append("outputNameBase=\"" + temp + "\" ");
        if(bhpsuScaleNormalizedRadioButton.isSelected()){
            temp = "yes";
            content.append("run_normalization=\"" + temp + "\" ");
            temp = bhpsuNoiseFactorTextField.getText() == null ? "" : bhpsuNoiseFactorTextField.getText().trim();
            content.append("gamma=\"" + temp + "\" ");
        }

        tempB = new StringBuffer();
        for (int i = 0; suHorzTimeTextFieldList != null && i < suHorzTimeTextFieldList.size(); i++) {
            tempB.append(suHorzTimeTextFieldList.get(i).getText());
            if (i < suHorzTimeTextFieldList.size() - 1)
                tempB.append(",");
        }
        content.append("horizon_times=\"" + tempB.toString() + "\" \n");

        temp = landmarkNameBaseTextField.getText().trim();
        content.append("landmark_outputNameBase=\"" + temp + "\" ");
        if(landmarkScaleNormalizedRadioButton.isSelected()){
            temp = "yes";
            content.append("landmark_run_normalization=\"" + temp + "\" ");
            temp = landmarkNoiseFactorTextField.getText() == null ? "" : landmarkNoiseFactorTextField.getText().trim();
            content.append("landmark_gamma=\"" + temp + "\" ");
        }

        //if(horizonNormalizedRadioButton.isSelected()){
        //    temp = "yes";
        //    content.append("run_normalized_hori_volumes=\"" + temp + "\" ");
        //}

        tempB = new StringBuffer();
        for (int i = 0; landmarkOutputScaleTextFieldList != null && i < landmarkOutputScaleTextFieldList.size(); i++) {
            tempB.append(landmarkOutputScaleTextFieldList.get(i).getText());
            if (i < landmarkOutputScaleTextFieldList.size() - 1)
                tempB.append(",");
        }
        temp = landmarkOutProjectTextField.getText() == null ? "" : landmarkOutProjectTextField.getText().trim();
        content.append("landmark_outputProject=\"" + temp + "\" ");
        content.append("landmark_scales=\"" + tempB.toString() + "\" />\n");


        //}else{
        content.append("<Landmark3D ");
        temp = landmarkInProjectTextField.getText() == null ? "" : landmarkInProjectTextField.getText().trim();
        content.append("landmark3DProject=\""  + temp + "\" ");
        temp = volumeTextField.getText() == null ? "" : volumeTextField.getText().trim();
        content.append("landmark3DInputVolume=\"" + temp + "\" ");
        temp = linesTextField.getText() == null ? "" : linesTextField.getText().trim();
        content.append("min_inline=\"" + temp + "\" ");
        temp = linesToTextField.getText() == null ? "" : linesToTextField.getText().trim();
        content.append("max_inline=\"" + temp + "\" ");
        temp = linesByTextField.getText() == null ? "" : linesByTextField.getText().trim();
        content.append("inc_inline=\"" + temp + "\" ");
        temp = tracesTextField.getText() == null ? "" : tracesTextField.getText().trim();
        content.append("min_trace=\"" + temp + "\" ");
        temp = tracesToTextField.getText() == null ? "" : tracesToTextField.getText().trim();
        content.append("max_trace=\"" + temp + "\" ");
        temp = tracesByTextField.getText() == null ? "" : tracesByTextField.getText().trim();
        content.append("inc_trace=\"" + temp + "\" ");
        temp = timeTextField.getText() == null ? "" : timeTextField.getText().trim();
        content.append("min_time=\"" + temp + "\" ");
        temp = timeToTextField.getText() == null ? "" : timeToTextField.getText().trim();
        content.append("max_time=\"" + temp + "\" ");

        if(lineTraceArray[0][0] != 0)
            content.append("default_min_line=\"" + lineTraceArray[0][0] + "\" ");
        else
            content.append("default_min_line=\"" + "" + "\" ");
        if(lineTraceArray[0][1] != 0)
            content.append("default_max_line=\"" + lineTraceArray[0][1] + "\" ");
        else
            content.append("default_max_line=\"" + "" + "\" ");

        if(lineTraceArray[0][2] != 0)
            content.append("default_inc_line=\"" + lineTraceArray[0][2] + "\" ");
        else
            content.append("default_inc_line=\"" + "" + "\" ");
        if(lineTraceArray[1][0] != 0)
            content.append("default_min_trace=\"" + lineTraceArray[1][0] + "\" ");
        else
            content.append("default_min_trace=\"" + "" + "\" ");
        if(lineTraceArray[1][1] != 0)
            content.append("default_max_trace=\"" + lineTraceArray[1][1] + "\" ");
        else
            content.append("default_max_trace=\"" + "" + "\" ");
        if(lineTraceArray[1][2] != 0)
            content.append("default_inc_trace=\"" + lineTraceArray[1][2] + "\" ");
        else
            content.append("default_inc_trace=\"" + "" + "\" ");
        if(lineTraceArray[2][0] != 0)
            content.append("default_min_time=\"" + lineTraceArray[2][0] + "\" ");
        else
            content.append("default_min_time=\"" + "" + "\" ");
        if(lineTraceArray[2][1] != 0)
            content.append("default_max_time=\"" + lineTraceArray[2][1] + "\" ");
        else
            content.append("default_max_time=\"" + "" + "\" ");
        if(lineTraceArray[2][2] != 0)
            content.append("default_inc_time=\"" + lineTraceArray[2][2] + "\" ");
        else
            content.append("default_inc_time=\"" + "" + "\" ");
        if(lineTraceArray[3][0] != 0){
            if(lineTraceArray[3][0] == 1)
                content.append("data_type=\"" + "time" + "\" ");
            else if(lineTraceArray[3][0] == 2)
                content.append("data_type=\"" + "depth" + "\" ");
        }else
            content.append("data_type=\"" + "" + "\" ");

        //}

        tempB = new StringBuffer();
        for (int i = 0; landmarkHorzTextFieldList != null && i < landmarkHorzTextFieldList.size(); i++) {
            tempB.append(landmarkHorzTextFieldList.get(i).getText());
            if (i < landmarkHorzTextFieldList.size() - 1)
                tempB.append(",");
        }
        content.append("landmark_horizon_files=\"" + tempB.toString() + "\" ");


        temp = landmarkStartTimeTextField.getText() == null ? "" : landmarkStartTimeTextField.getText().trim();
        content.append("landmark_tmin=\"" + temp + "\" ");
        tempB = new StringBuffer();
        for (int i = 0; landmarkHorzTimeTextFieldList != null && i < landmarkHorzTimeTextFieldList.size(); i++) {
            tempB.append(landmarkHorzTimeTextFieldList.get(i).getText());
            if (i < landmarkHorzTimeTextFieldList.size() - 1)
                tempB.append(",");
        }
        content.append("landmark_horizon_times=\"" + tempB.toString() + "\" ");

        temp = landmarkEndTimeTextField.getText() == null ? "" : landmarkEndTimeTextField.getText().trim();
        content.append("landmark_tmax=\"" + temp + "\" ");


        temp = sampleRateTextField.getText() == null ? "" : sampleRateTextField.getText().trim();
        content.append("sampleRate=\"" + temp + "\" ");


        //if(bhpsuOutRadioButton.isSelected()){

        //}else{
        temp = landmark3DNameBaseTextField.getText() == null ? "" : landmark3DNameBaseTextField.getText().trim();
        content.append("landmark3D_outputNameBase=\"" + temp + "\" ");
        if(landmark3DScaleNormalizedRadioButton.isSelected()){
            temp = "yes";
            content.append("landmark3D_run_normalization=\"" + temp + "\" ");
            temp = landmark3DNoiseFactorTextField.getText() == null ? "" : landmark3DNoiseFactorTextField.getText().trim();
            content.append("landmark3D_gamma=\"" + temp + "\" ");
        }

        temp = landmark3DOutProjectTextField.getText() == null ? "" : landmark3DOutProjectTextField.getText().trim();
        content.append("landmark3D_outputProject=\"" + temp + "\" ");

        //if(horizonNormalizedRadioButton.isSelected()){
        //    temp = "yes";
        //    content.append("run_normalized_hori_volumes=\"" + temp + "\" ");
        //}

        tempB = new StringBuffer();
        for (int i = 0; landmark3DOutputScaleTextFieldList != null && i < landmark3DOutputScaleTextFieldList.size(); i++) {
            tempB.append(landmark3DOutputScaleTextFieldList.get(i).getText());
            if (i < landmark3DOutputScaleTextFieldList.size() - 1)
                tempB.append(",");
        }

        content.append("landmark3D_scales=\"" + tempB.toString() + "\" />\n");
        content.append("<Landmark2D ");
        temp = landmarkIn2DProjectTextField.getText() == null ? "" : landmarkIn2DProjectTextField.getText().trim();
        content.append("landmark2DProject=\""  + temp + "\" ");
        int index = seismic2DFileComboBox.getSelectedIndex();
        if(index > 0){
            String item = (String)seismic2DFileComboBox.getSelectedItem();
            content.append("landmark2D_seismic_file_process_level=\""  + item + "\" ");
        }else
            content.append("landmark2D_seismic_file_process_level=\""  + "" + "\" ");
        //ListModel list = selectedSeismic2DLineList.getModel();
        StringBuffer linelist = new StringBuffer();
        //for(int i = 0; list != null && i < list.getSize(); i++){
        //  linelist += (String)list.getElementAt(i);
        //  if(i < (list.getSize() -1))
        //      linelist += ",";
        //}
        //landmark2D_seismic_lines layout --
        //line name,default shotpoint range(#:#:#),actual shotpoint range(#:#:#)
        Object[]  keys = selectedLineMap.keySet().toArray();
        for(int i = 0; keys!= null && i < keys.length; i++){
            linelist.append((String)keys[i] + ",");
            LineInfo li = selectedLineMap.get(keys[i]);
            linelist.append(li.getMinShotPoint_def() + ":" + li.getMaxShotPoint_def() + ":" + li.getIncShotPoint_def() + ",");
            linelist.append(li.getMinShotPoint() + ":" + li.getMaxShotPoint() + ":" + li.getIncShotPoint());
            if(i < (keys.length -1))
                linelist.append("~");
        }
        content.append("landmark2D_seismic_lines=\""  + linelist + "\" \n");
        String selectedLine = (String)selectedSeismic2DLineList.getSelectedValue();
        selectedLine = (selectedLine == null) ? "" : selectedLine.trim();
        content.append("landmark2D_selected_line=\""  + selectedLine + "\" ");
        temp = landmark2DNameBaseTextField.getText() == null ? "" : landmark2DNameBaseTextField.getText().trim();
        content.append("landmark2D_outputNameBase=\"" + temp + "\" ");
        if(landmark2DScaleNormalizedRadioButton.isSelected()){
            temp = "yes";
            content.append("landmark2D_run_normalization=\"" + temp + "\" ");
            temp = landmark2DNoiseFactorTextField.getText() == null ? "" : landmark2DNoiseFactorTextField.getText().trim();
            content.append("landmark2D_gamma=\"" + temp + "\" ");
        }

        //if(horizonNormalizedRadioButton.isSelected()){
        //    temp = "yes";
        //    content.append("run_normalized_hori_volumes=\"" + temp + "\" ");
        //}
        temp = landmark2DOutProjectTextField.getText() == null ? "" : landmark2DOutProjectTextField.getText().trim();
        content.append("landmark2D_outputProject=\"" + temp + "\" ");
        tempB = new StringBuffer();
        for (int i = 0; landmark2DOutputScaleTextFieldList != null && i < landmark2DOutputScaleTextFieldList.size(); i++) {
            tempB.append(landmark2DOutputScaleTextFieldList.get(i).getText());
            if (i < landmark2DOutputScaleTextFieldList.size() - 1)
                tempB.append(",");
        }

        content.append("landmark2D_scales=\"" + tempB.toString() + "\" />\n");

        /*
        temp = "";
        for (int i = 0; landmarkOutputTimeTextFieldList != null && i < landmarkOutputTimeTextFieldList.size(); i++) {
            temp += landmarkOutputTimeTextFieldList.get(i).getText();
            if (i < landmarkOutputTimeTextFieldList.size() - 1)
                temp += ",";
        }
        content.append("horizon_placement=\"" + temp + "\" ");
        */
    //}


        //content.append(">\n ");
//      save the project descriptor
        String pdXml = XmlUtils.objectToXml(agent.getProjectDescriptor());
        content.append(pdXml);
        content.append("</" + this.getClass().getName() + ">\n");

        return content.toString();
    }

    /**
     * Restore the state of the Wavelet Decomposition GUI.
     *
     * @param node
     *            XML tree node denoting the GUI element
     */
    // TODO: rewrite WaveletDecomp restoreState()
    public void restoreState(Node node) {
        Element el = (Element) node;

        String hStr = el.getAttribute("height");
        if(hStr == null || hStr.trim().length() == 0)
            hStr = String.valueOf(this.getSize().height);

        int height = Integer.valueOf(hStr).intValue();
        String wStr = el.getAttribute("width");
        if(wStr == null || wStr.trim().length() == 0)
            wStr = String.valueOf(this.getSize().width);
        int width = Integer.valueOf(wStr).intValue();

        String X = el.getAttribute("x");
        if(X == null || X.trim().length() == 0)
            X = String.valueOf(this.getLocation().x);

        int x = Integer.valueOf(X).intValue();
        String Y = el.getAttribute("y");
        if(Y == null || Y.trim().length() == 0)
            Y = String.valueOf(this.getLocation().y);

        int y = Integer.valueOf(Y).intValue();
        this.setSize(width,height);
        this.setLocation(x,y);

        NodeList children = node.getChildNodes();
        String nodeName;
        Node child;
        for (int i = 0; i < children.getLength(); i++) {
            child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                nodeName = child.getNodeName();
                if (nodeName.equals("BHPSU")) {
                    restoreBhpsu(child);
                }else if(nodeName.equals("Landmark3D")) {
                    restoreLandmark3D(child);
                }else if(nodeName.equals("Landmark2D")) {
                    restoreLandmark2D(child);
                }
            }
        }

        String inputType = el.getAttribute("seismicDataInputType");
        if(inputType != null){
            if(inputType.equals(bhpsuInRadioButton.getText().trim()))
                bhpsuInRadioButton.setSelected(true);
            else if(inputType.equals(landmarkInRadioButton.getText().trim())){
                if(validateLandmarkEnvironment() == false){
                    JOptionPane.showMessageDialog(this, "Landmark environment must be properly set up before the states can be restored.",
                            "Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
                    bhpsuInRadioButton.setSelected(true);
                    return;
                }else{
                    //agent.populateLandmarkProjects();
                }
                landmarkInRadioButton.setSelected(true);
                seismicFormatIn = LANDMARK_FORMAT;
                int idx = specDecompTabbedPane.getSelectedIndex();
                // check if Seismic Data Input tab selected

                String LandmarkInputType = el.getAttribute("LandmarkInputType");

                if(LandmarkInputType != null && LandmarkInputType.equals("2D"))
                    landmark2DRadioButton.setSelected(true);
                else
                    landmark3DRadioButton.setSelected(true);
                if (idx == INPUT_TAB_INDEX) {
                    switch2LandmarkIn();
                }
            }else
                bhpsuInRadioButton.setSelected(true); //default
        }
        String outputType = el.getAttribute("output_type");
        if(outputType != null && outputType.equals("Landmark and BHP SU")){
            landmarkOutRadioButton.setSelected(true);
            if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                //String outLmProject = el.getAttribute("landmark2DOutputProject");
                //outLmProject = (outLmProject == null) ? "" : outLmProject.trim();
                //landmark2DOutProjectTextField.setText(outLmProject);
                switch2LandmarkOut(landmarkOutputTimeCount, landmark2DOutNumScalesCount, 2);
            }else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){ //Input Landmark 3D
                //String outLmProject = el.getAttribute("landmark3DOutputProject");
                //outLmProject = (outLmProject == null) ? "" : outLmProject.trim();
                //landmark3DOutProjectTextField.setText(outLmProject);
                switch2LandmarkOut(landmarkOutputTimeCount, landmark3DOutNumScalesCount, 3);
            }else{ //input Bhpsu Output Landmark
                //String outLmProject = el.getAttribute("landmark_outputProject");
                //outLmProject = (outLmProject == null) ? "" : outLmProject.trim();
                //landmarkOutProjectTextField.setText(outLmProject);
                switch2LandmarkOut(landmarkOutputTimeCount, landmarkOutNumScalesCount, 1);
            }

        }else
            bhpsuOutRadioButton.setSelected(true);


        String horizonInputType = el.getAttribute("horizonInputType");
        if(horizonInputType != null && horizonInputType.equals("Landmark"))
            landmarkHorizonRadioButton.setSelected(true);
        else
            bhpsuHorizonRadioButton.setSelected(true);



        QiProjectDescriptor desc = agent.getProjectDescriptor();
        String project = QiProjectDescUtils.getProjectPath(desc);
            //String project = el.getAttribute("project");
            //if(project == null)
            //  project = "";
        selectedProjectLabel.setText(project);

        String smallest_scale = el.getAttribute("smallest_scale");
        if(smallest_scale == null)
            smallest_scale = "";
        smallestScaleTextField.setText(smallest_scale);

        String largest_scale = el.getAttribute("largest_scale");
        if(largest_scale == null)
            largest_scale = "";
        largestScaleTextField.setText(largest_scale);

        String number_scales = el.getAttribute("number_scales");
        if(number_scales == null)
            number_scales = "";
        numberScalesTextField.setText(number_scales);

        String velocity = el.getAttribute("velocity");
        if(velocity == null)
            velocity = "";
        averageVelocityTextField.setText(velocity);

        String smoothing_length = el.getAttribute("smoothing_length");
        if(smoothing_length == null)
            smoothing_length = "";
        smoothingLengthTextField.setText(smoothing_length);

        String npass = el.getAttribute("npass");
        if(npass == null)
            npass = "";
        numberOfPassTextField.setText(npass);

        String horizon_type = el.getAttribute("horizon_type");
        if(horizon_type != null){
            if(horizon_type.trim().equals(landmarkHorizonRadioButton.getText()))
                landmarkHorizonRadioButton.setSelected(true);
            else if(horizon_type.trim().equals(bhpsuHorizonRadioButton.getText()))
                bhpsuHorizonRadioButton.setSelected(true);
            else if(horizon_type.trim().equals(bhpsuHorizonDatasetRadioButton.getText()))
            	bhpsuHorizonDatasetRadioButton.setSelected(true);
        }

        String defaultScriptDir = el.getAttribute("defaultScriptDir");
        if(defaultScriptDir != null && defaultScriptDir.length() > 0){
            defaultPreferredScriptDir = defaultScriptDir;

        }

        String scriptNamePrefix = el.getAttribute("scriptNamePrefix");
        if(scriptNamePrefix == null){ //job was never submitted
            currentRunningScriptPrefix = "";
            job_status = WaveletDecompConstants.JOB_UNSUBMITTED_STATUS;
        }else
            currentRunningScriptPrefix = scriptNamePrefix;
        if(currentRunningScriptPrefix.length() > 0){ //job was submitted
            String status = el.getAttribute("jobStatus");
            if(status != null){
                if(status.equals("Running")){
                    runMenuItem.setEnabled(false);
                    runScriptButton.setEnabled(false);
                    stateLabel.setText("Job Was Running");
                    job_status = WaveletDecompConstants.JOB_RUNNING_STATUS;
                }else if(status.equals("Ended")){
                    runMenuItem.setEnabled(true);
                    runScriptButton.setEnabled(true);
                    stateLabel.setText("Job ended");
                    job_status = WaveletDecompConstants.JOB_ENDED_STATUS;
                }
            }
        }else{
            runMenuItem.setEnabled(true);
            runScriptButton.setEnabled(true);
            stateLabel.setText("Job Not Submitted");
            job_status = WaveletDecompConstants.JOB_UNSUBMITTED_STATUS;
        }

        Runnable runnable = new Runnable(){
            public void run(){
                if(checkOutputDatasetsExist()){
                    displayResultsButton.setEnabled(true);
                    displayResultsMenuItem.setEnabled(true);
                }
            }

        };
        Thread worker = new Thread(runnable);
        worker.start();
        //updateStatus();
        //entireDatasetButton.setEnabled(true);
    }

    // Check if the first required output dataset is available for display
    private boolean checkOutputDatasetsExist(){
        String baseName = "";
        boolean normalized =true;
        if(bhpsuOutRadioButton.isSelected()){
            baseName = bhpsuNameBaseTextField.getText().trim();
        }else if(landmarkOutRadioButton.isSelected()){
            if(landmark3DRadioButton.isSelected()){
                baseName = landmark3DNameBaseTextField.getText().trim();
                if(!landmark3DScaleNormalizedRadioButton.isSelected())
                    normalized = false;
            }else if(landmark2DRadioButton.isSelected()){
                baseName = landmark2DNameBaseTextField.getText().trim();
                if(!landmark2DScaleNormalizedRadioButton.isSelected())
                    normalized = false;
            }
        }

        String datasetPath = QiProjectDescUtils.getDatasetsPath(agent.getQiProjectDescriptor());
        String filesep = agent.getMessagingMgr().getServerOSFileSeparator();
        String filePath = "";
        boolean pass = false;
        if(landmark2DRadioButton.isSelected()){
            //for(Object line : selectedLines){
            if(selectedLines != null && selectedLines.size() > 0){
                filePath = datasetPath + filesep + baseName + "_" + selectedLines.get(0) + "_cwt.dat";
                if(agent.checkDirExist(filePath).equals("yes"))
                    pass = true;
            }
            //if(pass && normalized){
            //    for(Object line : selectedLines){
            //        filePath = datasetPath + filesep + baseName + "_" + line + "_cwt_norm.dat";
            //        if(agent.checkDirExist(filePath).equals("yes"))
            //            pass = true;
            //        else{
            //            pass = false;
            //            break;
            //        }
            //    }
            //}
        }else{
            filePath = datasetPath + filesep + baseName + "_cwt.dat";
            if(agent.checkDirExist(filePath).equals("yes"))
                pass = true;
            //if(normalized){
            //    filePath = datasetPath + filesep + baseName + "_cwt_norm.dat";
            //    if(agent.checkDirExist(filePath).equals("yes"))
            //        pass = true;
            //    else
            //        pass = false;
            //}
        }
        return pass;
    }

    /**
     * Close GUI and dispose, called by plugin when user has terminated GUI
     *
     */
    public void closeGUI() {
        setVisible(false);
        dispose();
    }

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    private static void createAndShowGUI() {
        JFrame frame = new JFrame();
        // Make sure we have nice window decorations.
        frame.setDefaultLookAndFeelDecorated(true);

        // Create and set up the window.
        JInternalFrame decom = new WaveletDecompGUI();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JDesktopPane desktop = new JDesktopPane();
        frame.setContentPane(desktop);
        desktop.add(decom);
        decom.setVisible(true);

        // Display the window.
        frame.setSize(decom.getWidth(), decom.getHeight());
        frame.setVisible(true);
    }

    private class DatasetTextFieldKeyListener extends KeyAdapter {
        final Component comp;
        public DatasetTextFieldKeyListener(Component parent){
            comp = parent;
        }
        public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER){

                    String filePath = datasetTextField.getText();
                    if(filePath == null || filePath.trim().length() == 0)
                        return;
                    if(filePath.indexOf(filesep) == -1 || filePath.indexOf(".dat") == -1){
                        JOptionPane.showMessageDialog(gui,"Incorrect file path format " + filePath,
                                "Problem in submitting job script",JOptionPane.WARNING_MESSAGE);
                            return;
                    }
                    agent.doTask(filePath,comp);
                    /*
                    int status = agent.getSeismicDatasetInfo(filePath);
                    logger.info("job submitting status = " + status);
                      if(status == 0){
                          Runnable updateAComponent = new Runnable() {
                              public void run() {
                                  setEntireDatasetButtonEnabled(true);
                                  setDatasetDefaultValue(agent.getSeismicDataset());
                              }
                          };
                          SwingUtilities.invokeLater(updateAComponent);
                      }else{
                          setEntireDatasetButtonEnabled(false);
                        JOptionPane.showMessageDialog(gui,"Error in running SU script.",
                                "Problem in submitting job script",JOptionPane.ERROR_MESSAGE);
                      }
                      return;
                      */
                }

            }//*/home/lilt9/qiProjects/neptune/int_datasets/nep_ampl_sedi_small.dat
    }

    /**
     * Reset the GUI's title
     *
     * @param name Name of the associated project.
     */
    public void resetTitle(String projName) {
        String compName = "";
        IComponentDescriptor agentDesc = agent.getComponentDescriptor();
        String pdn = CompDescUtils.getDescPreferredDisplayName(agentDesc);
        String screenName = CompDescUtils.getDescDisplayName(agentDesc);
        if (pdn.trim().length() > 0)
            compName = pdn.trim();
        else
            compName = screenName;

        //get the type of qiComponent
        int idx = screenName.indexOf("#");
        if (idx != -1) screenName = screenName.substring(0, idx);
        this.setTitle(screenName+": " + compName + "  Project: " + projName);
    }
	
	/**
	 * Get the output base name of the generated datasets
	 * @return base name
	 */
	 public String getOutputBaseName() {
        String baseName = "";
        if (landmarkOutRadioButton.isSelected()) {
            if (landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected())
                baseName = landmark2DNameBaseTextField.getText().trim();
            else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected())
                baseName = landmark3DNameBaseTextField.getText().trim();
            else
                baseName = landmarkNameBaseTextField.getText().trim();
        } else if(bhpsuOutRadioButton.isSelected())
            baseName = bhpsuNameBaseTextField.getText().trim();
        int ind = baseName.lastIndexOf(filesep);
        if (ind != -1) {
            baseName = baseName.substring(ind+1);
        }
		return baseName;
	 }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    static class LineInfo{
        private String projectName;
        private String lineName;
        private int lineNumber = 0;
        private float minShotPoint = 0;
        private float maxShotPoint = 0;
        private float incShotPoint = 0;
        private int minTrace = 0;
        private int maxTrace = 0;
        private int incTrace = 0;
        private float minShotPoint_def;
        private float maxShotPoint_def;
        private float incShotPoint_def;

        public LineInfo(String pname, String lname, int lineNum, float defMinSnt, float defMaxSnt, float defIncSnt){
            projectName = pname;
            lineName = lname;
            lineNumber = lineNum;
            minShotPoint_def = defMinSnt;
            maxShotPoint_def = defMaxSnt;
            incShotPoint_def = defIncSnt;
        }
        public String getProjectName(){
            return projectName;
        }
        public int getlineNumber(){
            return lineNumber;
        }
        public float getMinShotPoint(){
            return minShotPoint;
        }
        public float getMaxShotPoint(){
            return maxShotPoint;
        }
        public float getIncShotPoint(){
            return incShotPoint;
        }

        public int getMinTrace(){
            return minTrace;
        }
        public int getMaxTrace(){
            return maxTrace;
        }
        public int getIncTrace(){
            return incTrace;
        }

        public float getMinShotPoint_def(){
            return minShotPoint_def;
        }
        public float getMaxShotPoint_def(){
            return maxShotPoint_def;
        }
        public float getIncShotPoint_def(){
            return incShotPoint_def;
        }

        public void setProjectName(String name){
            projectName = name;
        }

        public void setLineNumber(int pnt){
            lineNumber = pnt;
        }
        public void setMinShotPoint(float pnt){
            minShotPoint = pnt;
        }
        public void setMaxShotPoint(float pnt){
            maxShotPoint = pnt;
        }
        public void setIncShotPoint(float pnt){
            incShotPoint = pnt;
        }

        public void setMinTrace(int tr){
            minTrace = tr;
        }
        public void setMaxTrace(int tr){
            maxTrace = tr;
        }
        public void setIncTrace(int tr){
            incTrace = tr;
        }

        public String toString(){
            return "Project Name=" + projectName + " Line Name=" + lineName + "Line Num= " + lineNumber + " Default Range= (" + minShotPoint_def + "," + maxShotPoint_def
            + "," +  incShotPoint_def + ") Actual Range= (" + minShotPoint + "," + maxShotPoint
            + "," +  incShotPoint + ") Actual Trace= (" + minTrace + "," + maxTrace + "," + incTrace + ")";
        }
    }
}
