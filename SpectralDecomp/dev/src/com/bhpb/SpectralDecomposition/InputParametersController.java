package com.bhpb.SpectralDecomposition;

import com.bhpb.SpectralDecomposition.InputParametersModel.INTERPOLATION_MODE;
import com.bhpb.SpectralDecomposition.InputParametersModel.WAVELET_TYPE;
import com.bhpb.qiworkbench.util.StringValidator;
import com.bhpb.qiworkbench.util.StringValidator.TYPE;
import com.bhpb.qiworkbench.util.ValidatorResult;
import java.util.Map;
import java.util.logging.Logger;

public class InputParametersController {
    private static Logger logger = Logger.getLogger(InputParametersController.class
            .getName());
	private InputParametersModel model;
	private InputParameterPanel view;
    private WaveletDecompGUI parentGUI;
	public InputParametersController(InputParametersModel model, InputParameterPanel view){
		this.model = model;
		this.view = view;
        updateView(this.model);
	}

    public void updateView(InputParametersModel model){
        view.setHighestFrequency(model.getHigestFrequency());
        view.setInterpolationMode(model.getInterpolationMode());
        view.setLowestFrequency(model.getLowestFrequency());
        view.setNumberFrequencies(model.getNumberFrequencies());
        view.setSmoothingLength(model.getSmoothingLength());
        view.setVelocityTuningThickness(model.getVelocityTuningThickness());
        view.setWaveletType(model.getWaveletType());
    }

    public void setParentGUI(WaveletDecompGUI parentGUI){
    	this.parentGUI = parentGUI;
    }
    
    public float getHighestFrequency(){
        return model.getHigestFrequency();
    }

    public void setInterpolationMode(INTERPOLATION_MODE mode){
        model.setInterpolationMode(mode);
    }

    public INTERPOLATION_MODE getInterpolationMode(){
        return model.getInterpolationMode();
    }

    public void setWaveletType(WAVELET_TYPE type){
        model.setWaveletType(type);
    }

    public WAVELET_TYPE getWaveletType(){
        return model.getWaveletType();
    }

    public float getSigmaValue(){
        return model.getSigmaValue();
    }

    public void setHighestFrequency(String freq){
        ValidatorResult validatorResult = StringValidator.validate(freq, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            float f = Float.valueOf(freq);
            //float dt = parentGUI.getDatasetSampleRate();
            //if(dt != -1){
            //    if((f >= model.getLowestFrequency()) && (f <= 1000/(4*dt))){
                    model.setHigestFrequency(f);
                    view.setHighestFrequency(f);
            //    }else{
            //        view.showValidationFailure("Lowest frequency must be between lowest frequency and 1000/(2*dt) inclusive");
            //    }
            //}else{
            //    logger.info("dt is not available for validating highest frequency field");
           // }
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public float getLowestFrequency(){
        return model.getLowestFrequency();
    }


    public void setLowestestFrequency(String freq) {
        ValidatorResult validatorResult = StringValidator.validate(freq, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            float f = Float.valueOf(freq);
            if(f >= 0.01 && f <= model.getHigestFrequency()/2){
                model.setLowestFrequency(f);
                view.setLowestFrequency(f);
            }else
                view.showValidationFailure("Lowest frequency must be between 0.01Hz and (Highest frequency)/2 inclusive");
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public int getNumberFrequencies(){
        return model.getNumberFrequencies();
    }


    public void setNumberFrequencies(String num) {
        ValidatorResult validatorResult = StringValidator.validate(num, TYPE.INTEGER);
        if (validatorResult.isValid()) {
            int n = Integer.valueOf(num);
            if(n >=2 && n <= 100){
                model.setNumberFrequencies(n);
                view.setNumberFrequencies(n);
            }else
                view.showValidationFailure("Number of frequencies must be between 2 and 100 inclusive");
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public float getVelocityTuningThickness(){
        return model.getVelocityTuningThickness();
    }


    public void setVelocityTuningThickness(String v) {
        ValidatorResult validatorResult = StringValidator.validate(v, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            float f = Float.valueOf(v);
            if(f >= 1 && f <= 10000){
                model.setVelocityTuningThickness(f);
                view.setVelocityTuningThickness(f);
            }else
                view.showValidationFailure("Velocity for tuning thickness must be between 1 and 10000 inclusive");
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public float getSmoothingLength(){
        return model.getSmoothingLength();
    }


    public void setSmoothingLength(String v) {
        ValidatorResult validatorResult = StringValidator.validate(v, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            float f = Float.valueOf(v);
            if(f >= 0){
                model.setSmoothingLength(f);
                view.setSmoothingLength(f);
            }else
                view.showValidationFailure("Smoothing Length must be greater than or equal to 0");
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }
}
