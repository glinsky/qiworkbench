/*
 ###########################################################################
 # SpectralDecomp - A continuous time-frequency analysis technique that
 # computes frequency spectrum for each time sample of a seismic trace.
 # Copyright (C) 2006  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
*/

package com.bhpb.SpectralDecomposition;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.*;
import java.util.Properties;

import javax.swing.*;

import com.bhpb.qiworkbench.compAPI.ComponentUtils;


/**
 * Title:        WaveletDecomp <br><br>
 * Description:  This action will pop up a dialog showing version and license information about this component
 *               <br><br>
 *
 * @author
 * @version 1.0
 */

public class WaveletDecompAbout extends JDialog {
    private ImageIcon _icon;
    private Component parent;
    private Properties props;
    public WaveletDecompAbout(Component parent, Properties props) {
        super(JOptionPane.getFrameForComponent(parent),"About WaveletDecomp");
        this.parent = parent;
        this.props = props;
        _icon = new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    public void createAndShowGUI(){
        JEditorPane aboutTextEditorPane = new JEditorPane();
        aboutTextEditorPane.setEditable(false);
        aboutTextEditorPane.setContentType("text/html");

        aboutTextEditorPane.setText(getAboutHtmlText());
        aboutTextEditorPane.setBorder(BorderFactory.createLineBorder(Color.black));
        //final JDialog dialog = new JDialog(JOptionPane.getRootFrame(), "About WaveletDecomp");
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBackground(Color.WHITE);

        panel.add(new JLabel(_icon), BorderLayout.CENTER);
        //dialog.getContentPane().add(new JLabel(_icon), BorderLayout.WEST);
        getContentPane().add(panel, BorderLayout.WEST);
        getContentPane().add(aboutTextEditorPane, BorderLayout.EAST);
        JButton okButton = new JButton("Close");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                dispose();
            }
        });
        JPanel bpanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        //bpanel.add(license);
        bpanel.add(okButton);
        getContentPane().add(bpanel, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(parent);
        //int x = parent.getBounds().x + (parent.getBounds().width/2) - (dialog.getBounds().width/2);
        //int y = parent.getBounds().y + (parent.getBounds().height/2) - (dialog.getBounds().height/2);
        //dialog.setLocation(x,y);
        setVisible(true);
    }

    private String getAboutHtmlText(){
        StringBuffer sb = new StringBuffer();
        sb.append("<font face=\"Arial\" size=3>");
        sb.append("<br><b>WaveletDecomp</b> - A continuous time-frequency analysis technique <br>");
        sb.append("that computes frequency spectrum for each time sample of a <br>");
        sb.append("seismic trace.");
        sb.append("<br><br>");

        String versionProp = "";
        String buildProp = "";
        if(props != null){
            versionProp = props.getProperty("project.version");;
            buildProp = props.getProperty("project.build");
        }

        if(versionProp == null) {
             versionProp = "";
        }
        if(buildProp == null) {
             buildProp = "";
        }

        sb.append("Version: " + versionProp + "<br>");
        sb.append("Build id: " + buildProp + "<br><br>");
        sb.append("Copyright (C) 2007-2009  BHP Billiton Petroleum; BHP Billiton Confidential<br>");
        sb.append("<br><br>");
        sb.append("</font>");
        return sb.toString();

    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        //JFrame frame = new JFrame();
        //frame.addActionListener(new WaveletDecompAbout(null));

    }

}
