/*
 ###########################################################################
 # SpectralDecomp - A continuous time-frequency analysis technique that
 # computes frequency spectrum for each time sample of a seismic trace.
 # Copyright (C) 2008  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */

package com.bhpb.SpectralDecomposition.displayResults;

/**
 * Constants used within the WaveletDecomp FSM to control state transition.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public final class WaveletDecompControlConstants {
    /**
     * Prevent object construction outside of this class.
     */
    private WaveletDecompControlConstants() {}

    // STEP STATES for display finite state machine
    public static final int GET_LAYER_PROPS_WIN1 = 1;
    public static final int GET_LAYER_PROPS_WIN2 = 2;
    public static final int GET_LAYER_PROPS_WIN3 = 3;
	public static final int GET_LAYER_PROPS_WIN4 = 4;
	public static final int GET_LAYER_PROPS_WIN5 = 5;
	public static final int GET_LAYER_PROPS_WIN6 = 6;
	public static final int GET_LAYER_PROPS_WIN7 = 7;

    // ACTION STATES for display transducer
    public static final int CREATE_WINDOW = 1;
    public static final int LOAD_DATA = 2;
    public static final int GET_LAYER_PROP = 3;
    public static final int SET_LAYER_PROPS = 4;
    public static final int RESIZE_WINDOW = 5;
    public static final int SET_LAYER_PROPS1 = 6;
    public static final int SET_LAYER_PROPS2 = 7;
    public static final int GET_SYNC_PROP = 8;
    public static final int SET_SYNC_PROPS = 9;
    public static final int GET_ANNO_PROP = 10;
    public static final int SET_ANNO_PROPS = 11;
    public static final int ADD_LAYER = 12;
    public static final int GET_LAYER_PROP1 = 13;
    public static final int DISPLAY_WINDOW = 14;
    public static final int SET_BROADCAST = 15;
    public static final int GET_SCALE_PROP = 16;
    public static final int SET_SCALE_PROPS = 17;
    public static final int SELECT_WIN_TOOL = 18;
    public static final int SET_LAYER_PROPS3 = 19;
    public static final int RESIZE_VIEWER = 20;
    public static final int ADD_LAYER1 = 21;
    public static final int ADD_LAYER2 = 22;
    public static final int ADD_LAYER3 = 23;
    public static final int ADD_LAYER4 = 24;
    public static final int ADD_LAYER5 = 25;
    public static final int ADD_LAYER6 = 26;
    public static final int ADD_LAYER7 = 27;
    public static final int ADD_LAYER8 = 28;
    public static final int ADD_LAYER9 = 29;
    public static final int ADD_LAYER10 = 30;
    public static final int ADD_LAYER11 = 31;
	public static final int SET_LAYER_PROPS4 = 32;
	public static final int SET_LAYER_PROPS5 = 33;
	public static final int SET_LAYER_PROPS6 = 34;
	public static final int SET_LAYER_PROPS7 = 35;
	public static final int SET_LAYER_PROPS8 = 36;
    public static final int SET_LAYER_PROPS9 = 37;
	public static final int SET_LAYER_PROPS10 = 38;
	public static final int GET_TRACE_RANGE = 39;
	public static final int GET_TRACE_RANGE1 = 40;
	public static final int GET_TRACE_RANGE2 = 41;
	public static final int GET_TRACE_RANGE3 = 42;
	public static final int GET_TRACE_RANGE4 = 43;
	public static final int GET_TRACE_RANGE5 = 44;
	public static final int GET_TRACE_RANGE6 = 45;
	public static final int GET_TRACE_RANGE7 = 46;
	public static final int GET_TRACE_RANGE8 = 47;
	public static final int HIDE_LAYER = 48;
	public static final int HIDE_LAYER1 = 49;
	public static final int HIDE_LAYER2 = 50;
	public static final int HIDE_LAYER3 = 51;
	public static final int SCROLL_VERTICAL = 52;
    public static final int WIDEN_SPLIT_PANE = 53;
    public static final int GET_LAYER_PROPS = 54;
    public static final int GET_LAYER_PROPS1 = 55;
    public static final int GET_LAYER_PROPS2 = 56;
    public static final int GET_LAYER_PROPS3 = 57;
    public static final int GET_LAYER_PROPS4 = 58;
    public static final int GET_LAYER_PROPS5 = 59;
    public static final int GET_LAYER_PROPS6 = 60;
    public static final int GET_LAYER_PROPS7 = 61;
    public static final int GET_LAYER_PROPS8 = 62;
    public static final int GET_LAYER_PROPS9 = 63;
    public static final int GET_LAYER_PROPS10 = 64;
    public static final int GET_TRACE_RANGE9 = 65;
    public static final int GET_TRACE_RANGE10 = 66;
	public static final int CREATE_WINDOW1 = 67;
    public static final int SKIP_LAYER1 = 68;
    public static final int SKIP_LAYER2 = 69;
    public static final int SKIP_LAYER3 = 70;
    public static final int SKIP_LAYER4 = 71;
    public static final int SKIP_LAYER5 = 72;
    public static final int SKIP_LAYER6 = 73;
    public static final int SKIP_LAYER7 = 74;
    public static final int SKIP_LAYER8 = 75;
    public static final int SKIP_LAYER9 = 76;
    public static final int SKIP_LAYER10 = 77;
    public static final int SKIP_LAYER11 = 78;
	public static final int INIT_PROGRESS_DIALOG = 79;
	public static final int END_PROGRESS_DIALOG = 80;
	public static final int REDRAW_WINDOW = 81;
    public static final int DO_LAYER = 82;
    public static final int END_LAYER = 83;
    public static final int SKIP_WINDOW1 = 84;
    public static final int SKIP_WINDOW2 = 85;
    public static final int SKIP_WINDOW3 = 86;
    public static final int SKIP_WINDOW4 = 87;
    public static final int SKIP_WINDOW5 = 88;
    public static final int SKIP_WINDOW6 = 89;
    public static final int SKIP_WINDOW7 = 90;
    public static final int MOVE_LAYER = 91;
    public static final int MOVE_LAYER1 = 92;
    public static final int MOVE_LAYER2 = 93;
    public static final int SET_LAYER_PROPS11 = 94;
    public static final int SET_LAYER_PROPS12 = 95;
    public static final int SET_LAYER_PROPS13 = 96;
    public static final int SET_LAYER_PROPS14 = 97;
    public static final int GET_LAYER_PROP2 = 98;
    public static final int SET_SYNC_PROPS1 = 99;
    public static final int SET_SYNC_PROPS2 = 100;
    public static final int GET_LAYER_PROPS11 = 101;
    public static final int GET_LAYER_PROPS12 = 102;
    public static final int GET_LAYER_PROPS13 = 103;
    public static final int GET_LAYER_PROPS14 = 104;
    public static final int SET_SELECTED_RANGE = 105;
    public static final int SET_SELECTED_RANGE1 = 106;
    public static final int SKIP_OP = 107;
    public static final int SKIP_OP1 = 108;
}
