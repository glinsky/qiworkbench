/*
 ###########################################################################
 # SpectralDecomp - A continuous time-frequency analysis technique that
 # computes frequency spectrum for each time sample of a seismic trace.
 # Copyright (C) 2008  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */

package com.bhpb.SpectralDecomposition.displayResults;

import com.bhpb.SpectralDecomposition.WaveletDecompGUI;
import java.util.HashMap;
import java.util.Map;

import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;

/**
 * Create pathnames of dataset files used to display WaveletDecomp results in 
 * the qiViewer.
 * <p>
 * Generated file names have the following structure:<br>
 * <pre>    <output_prefix>_[cwt_norm | cwt_norm_slice | cwt_norm_slice_transpose_ep</pre><br>
 *
 * @author Gil Hansen
 * @version 1.0
 */

public class WaveletDecompDisplayFiles {
	public static final String INPUT = "";
    public static final String CWT = "cwt";
    public static final String CWT_SLICE = "cwt_slice";
    public static final String CWT_SLICE_TRANSPOSE_EP = "cwt_slice_transpose_ep";
    public static final String CWT_NORM = "cwt_norm";
    public static final String CWT_NORM_SLICE = "cwt_norm_slice";
    public static final String CWT_NORM_SLICE_TRANSPOSE_EP = "cwt_norm_slice_transpose_ep";

    private WaveletDecompGUI parentUI;

    //Pathnames of the datasets are stored in a HashMap keyed by qualified filename
    private Map<String, String> pathnames = new HashMap<String, String>();

    public WaveletDecompDisplayFiles(WaveletDecompGUI ui){
        parentUI = ui;
        genPathnames();
    }
    
    /**
     * Check if the filename exists
     * @param filename Qualified name of the display file
     * @return true if filename exists; otherwise, false.
     */
    public boolean isFilename(String filename) {
        return pathnames.containsKey(filename);
    }

    /**
     * Get the pathname of a display file
     * @param filename Qualified name of the display file
     * @return full path of the display file
     */
    public String getPathname(String filename) {
        return pathnames.get(filename);
    }

    private void genPathnames() {
        //get the output prefix
        String inputDataset = parentUI.getBhpsuInputDatasetName();
        String outPrefix = parentUI.getOutputBaseName();
        String datasetDir = QiProjectDescUtils.getDatasetsPath(parentUI.getAgent().getQiProjectDescriptor());
        String filesep = parentUI.getAgent().getMessagingMgr().getServerOSFileSeparator();
        String baseName = datasetDir + filesep + outPrefix;
        if(parentUI.isLandmark2D()){
            baseName = datasetDir + filesep + inputDataset;
        }
        String input = datasetDir + filesep + inputDataset + ".dat";
        pathnames.put(INPUT, input);
		String name = baseName + "_" + CWT + ".dat";
		pathnames.put(CWT, name);
        if(parentUI.isOutputNormalized()){
            name = baseName + "_" + CWT_NORM + ".dat";
            pathnames.put(CWT_NORM, name);
            if(parentUI.getNumOfHorizons() > 0 && !parentUI.isLandmark2D()){
                name = baseName + "_" + CWT_NORM_SLICE + ".dat";
                pathnames.put(CWT_NORM_SLICE, name);
                name = baseName + "_" + CWT_NORM_SLICE_TRANSPOSE_EP + ".dat";
                pathnames.put(CWT_NORM_SLICE_TRANSPOSE_EP, name);
            }
        }else{
            name = baseName + "_" + CWT + ".dat";
            pathnames.put(CWT, name);
            if(parentUI.getNumOfHorizons() > 0  && !parentUI.isLandmark2D()){
                name = baseName + "_" + CWT_SLICE + ".dat";
                pathnames.put(CWT_SLICE, name);
                name = baseName + "_" + CWT_SLICE_TRANSPOSE_EP + ".dat";
                pathnames.put(CWT_SLICE_TRANSPOSE_EP, name);
            }
        }
    }
}
