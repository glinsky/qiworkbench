/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bhpb.SpectralDecomposition;

import com.bhpb.geoio.util.ElementAttributeReader;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.StringReader;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author 
 */
public class InputParametersModel {
    public enum INTERPOLATION_MODE {
        LOGARITHMIC,LINEAR
    };

    public enum WAVELET_TYPE {
        RICKER, MORLET_1, MORLET_2, MORLET_3, MORLET_4, MORLET_5
    };

    private INTERPOLATION_MODE interpolationMode = INTERPOLATION_MODE.LOGARITHMIC;
    private WAVELET_TYPE waveletType = WAVELET_TYPE.RICKER;

    private float higestFrequency = 80;
    private float lowestFrequency = 5;
    private int numberFrequencies = 15;
    private float velocityTuningThickness = 2500;
    private float smoothingLength = 0;
    public static String getXmlAlias() {
        return "inputParameters";
    }

/**
     * Deserializes a PickingSettings.
     * 
     * @param node w3c.dom.node created from the XML String serialization of InputParametersModel
     * 
     * @return the deserialized PickingSettings object
     */
    public static InputParametersModel restoreState(Node node) {
        XStream xStream = new XStream(new DomDriver());
        xStream.setClassLoader(InputParametersModel.class.getClassLoader());
        xStream.alias(InputParametersModel.getXmlAlias(), InputParametersModel.class);
        Node nd = findInputParameterNode(node);
        String xml = ElementAttributeReader.nodeToXMLString(nd);

        InputParametersModel model = (InputParametersModel) xStream.fromXML(new StringReader(xml));

        return model;
    }

    private static Node findInputParameterNode(Node node){
        if(node == null)
            return null;
        NodeList children = node.getChildNodes();
        for(int i = 0; i < children.getLength(); i++){
            Node child = children.item(i);
            if(child != null ){
                if(child.getNodeType() == Node.ELEMENT_NODE && child.getNodeName().equals(InputParametersModel.getXmlAlias()))
                    return child;
                else
                    return findInputParameterNode(child);
            }
        }
        return null;

    }

    /**
     * Serializes the state of this <code>InputParametersModel</code> as a {@link String}
     * representation of an XML element.
     */
    public String saveState() {
        XStream xStream = new XStream(new DomDriver());
        //xStream.setClassLoader(InputParametersModel.class.getClassLoader());
        xStream.alias(InputParametersModel.getXmlAlias(), InputParametersModel.class);
        return xStream.toXML(this);
    }

    public void setInterpolationMode(INTERPOLATION_MODE mode){
        interpolationMode = mode;
    }

    public INTERPOLATION_MODE getInterpolationMode(){
        return interpolationMode;
    }

    public void setWaveletType(WAVELET_TYPE type){
        waveletType = type;
    }

    public WAVELET_TYPE getWaveletType(){
        return waveletType;
    }

    public float getSigmaValue(){
        float sigma = 0.0f;
        switch (waveletType) {
        case RICKER:
            sigma = 0.0f;
            break;
        case MORLET_1:
            sigma = 2.5f;
            break;
        case MORLET_2:
            sigma = 3.5f;
            break;
        case MORLET_3:
            sigma = 5.0f;
            break;
        case MORLET_4:
            sigma = 7.1f;
            break;
        case MORLET_5:
            sigma = 10.0f;
            break;

        default: 
    	}
        return sigma;
    }

    public float getSmoothingLength(){
        return smoothingLength;
    }

    public void setSmoothingLength(float length){
        smoothingLength = length;
    }

    public int getNumberFrequencies(){
        return numberFrequencies;
    }

    public void setNumberFrequencies(int number){
        numberFrequencies = number;
    }

    public float getHigestFrequency(){
        return higestFrequency;
    }

    public void setHigestFrequency(float freq){
        higestFrequency = freq;
    }

    public float getVelocityTuningThickness(){
        return velocityTuningThickness;
    }

    public void setVelocityTuningThickness(float v){
        velocityTuningThickness = v;
    }

    public float getLowestFrequency(){
        return lowestFrequency;
    }

    public void setLowestFrequency(float freq){
        lowestFrequency = freq;
    }
}
