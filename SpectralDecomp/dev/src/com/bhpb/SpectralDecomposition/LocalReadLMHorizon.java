/*
 ###########################################################################
 # SpectralDecomp - A continuous time-frequency analysis technique that
 # computes frequency spectrum for each time sample of a seismic trace.
 # Copyright (C) 2006  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
*/
package com.bhpb.SpectralDecomposition;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.Pipe;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.WritableByteChannel;
import java.util.concurrent.SynchronousQueue;


/**
 * A Producer thread which generates horizon data line-by-line. Instead of
 * extracting the data from a Landmark horizon file using the SeisWorks API,
 * the data is read from a file. Each line consists of inline (ep), xline
 * (cdp) and a value (time).
 *
 */
public class LocalReadLMHorizon extends Thread {
	//The write end of a pipe
	WritableByteChannel wchannel;
	//Path of the output file
	String fileIn = "";
	
	boolean stop = false;
	private SynchronousQueue<String> queue;
	public static final int BUF_SIZE = 4*1014;
	public LocalReadLMHorizon (WritableByteChannel wchannel, String fileIn) {
		this.wchannel = wchannel;
		this.fileIn = fileIn;
	}

	public LocalReadLMHorizon (WritableByteChannel wchannel,SynchronousQueue<String> queue){
		this.wchannel = wchannel;
		this.queue = queue;
	}

	public void setStop(boolean flag){
		stop = flag;
	}
	public boolean getStop(){
		return stop;
	}
	
	public void setEOF(){
		try{
		queue.put("EOF");
		}catch(InterruptedException e){
			e.printStackTrace();
		}
	}
	
	public void run() {
		ByteBuffer buf = ByteBuffer.allocateDirect(BUF_SIZE);
		File fin = null;
		if(fileIn.trim().length() != 0)
			fin = new File(fileIn);

		int bufCapacity = BUF_SIZE;
		int bufFilled = 0;

		//TODO: determine the line-termination characters for the underlying OS
		String lineTermChars = "\r\n";
		byte[] lineTermBytes = lineTermChars.getBytes();
		int lineTermSize = lineTermBytes.length;

		Selector sel = null;
		try {
			//create a readiness selector for the writable channel
			sel = Selector.open();

			//set nonblocking mode for the writable channel
			((Pipe.SinkChannel)wchannel).configureBlocking(false);

			//register the writable channel with the selector
			((Pipe.SinkChannel)wchannel).register(sel, SelectionKey.OP_WRITE);
		} catch (ClosedChannelException cce) {
			System.out.println("Producer::selector2: "+cce.getMessage());
		} catch (IOException ioe) {
			System.out.println("Producer::selector1: "+ioe.getMessage());
		}

		BufferedReader br = null;
		try {
			if(fin != null){
				FileInputStream fis = new FileInputStream(fin);
				br = new BufferedReader(new InputStreamReader(fis));
			}
			buf.clear();

			//generate horizon data line-by-line
			String line = null;
			if(br != null)
				line = br.readLine();
			

			while (!stop) {
				try {
					line = queue.take();
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}
				byte[] lineBytes = line.getBytes();
				int lineSize = lineBytes.length;
				//check if room in the buffer
				int newAmtFilled = bufFilled + lineSize + lineTermSize;
				if (newAmtFilled <= bufCapacity) {
					buf.put(lineBytes);
					buf.put(lineTermBytes);
					bufFilled = newAmtFilled;
				} else {
					//buffer full, write it out over the channel
					//prepare to be drained
					buf.flip();

					//block until channel is ready for writing
					//System.out.println("Producer: block for writing");
					int n = sel.select();

					if (n == 0) {
						System.out.println("Producer::System error: no ready keys");
					} else {
						//There is only one entry in the set of selected
						//keys so there is nothing to iterate over.

						//The channel associate with the key must be
						//writable for this is the only op we are interested in.

						//Remove key from selected set indicating it has
						//been handled.
						sel.selectedKeys().clear();

						//channel may not take it all at once
						while (wchannel.write(buf) > 0) {
							//empty
							//System.out.println("Producer: write buf");
						}

						//prepare buffer for next set of lines
						buf.clear();
						bufFilled = 0;
						//add line to buffer
						buf.put(lineBytes);
						buf.put(lineTermBytes);
						bufFilled += lineSize + lineTermSize;
					}
				}
				if(br != null)
					line = br.readLine();

			}
			//if anything left in the buffer, drain it
			buf.flip();
			while (wchannel.write(buf) > 0) {
				//empty
			}

			//Send EOF
			byte[] eof = "EOF".getBytes();
			buf.clear();
			buf.put(eof);
			buf.flip();
			while (wchannel.write(buf) > 0) { }

			this.wchannel.close();
		} catch (IOException e) {
			System.out.println("Producer: "+e.getMessage());
		} finally {
			try {
				if (br != null) br.close();
			} catch (IOException e) {}
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
	}

}
