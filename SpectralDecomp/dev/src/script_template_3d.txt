#!/bin/bash
#set -x
source ~/.bashrc
alias rm="rm -f"
alias cp=cp

# Parameters specified in the qiProject
# qiSpace containing one or more projects
export QISPACE="<QI_SPACE>"
# location of project relative to $QISPACE
export QIPROJECT="<QI_PROJECT_LOCATION>"
# location of datasets relative to $QISPACE/$QIPROJECT
export QIDATASETS="<QI_DATASETS>"
# location of horizons relative to $QISPACE/$QIPROJECT
export QIHORIZONS="<QI_HORIZONS>"
# list of seismic directories separated by a line separator (\n)
#export QISEISMICS=$QISPACE/$QIPROJECT/seismic
export QISEISMICS="<QI_SEISMICS>"

# Parameters specified in the WaveletDecomp GUI
# path of input dataset file relative to $QIDATASETS; file name has no suffix for .dat is assumed
export IN_DATASET="<IN_DATASET>"
# path of output dataset file relative to $QIDATASETS; file name has no suffix for .dat is assumed
export OUT_DATASET="<OUT_DATASET>"
OUT_DATASET_DIR="<OUT_DATASET_DIR>"

#parse in Java of OUT_DATASET (remove the path)
export OUT_DATASET_FILENAME="<OUT_DATASET_FILENAME>"
export IN_DATASET_FILENAME="<IN_DATASET_FILENAME>"

project=$QISPACE/$QIPROJECT

input_type=<INPUT_TYPE>
horizon_type=<HORIZON_TYPE>
output_type=<OUTPUT_TYPE>

lm_in_project=<LANDMARK_IN_PROJECT>
lm_out_project=<LANDMARK_OUT_PROJECT>
lm_in_project_type=<LANDMARK_IN_PROJECT_TYPE>
lm_out_project_type=<LANDMARK_OUT_PROJECT_TYPE>
lm_in_volume=<LANDMARK_IN_VOLUME>
lm_in_volume_base=<LANDMARK_IN_VOLUME_BASE>
#lm_out_volume=<LANDMARK_OUT_VOLUME>.bri
lm_min_line=<LANDMARK_MIN_LINE>
lm_max_line=<LANDMARK_MAX_LINE>
lm_inc_line=<LANDMARK_INC_LINE>
lm_min_trace=<LANDMARK_MIN_TRACE>
lm_max_trace=<LANDMARK_MAX_TRACE>
lm_inc_trace=<LANDMARK_INC_TRACE>
lm_min_time=<LANDMARK_MIN_TIME>
lm_max_time=<LANDMARK_MAX_TIME>
out_scale_list=<OUTPUT_SCALE_LIST>
mincdp=<MIN_CDP>
maxcdp=<MAX_CDP>
inccdp=<INC_CDP>
minep=<MIN_EP>
maxep=<MAX_EP>
incep=<INC_EP>




horizon_files=<HORIZON_FILE_LIST>
lm_horizon_files=<LANDMARK_HORIZON_LIST>

tmin=<START_TIME>
horizon_times=<HORIZON_TIMES_LIST>
tmax=<END_TIME>
dt=<TIME_INCREMENT>
gamma=<GAMMA_VALUE>
run_normalization=<RUN_NORMALIZATION_FLAG>
run_slice=<RUN_SLICE_FLAG>
nx=${number_scales}

freq1=<FREQ1>
freq2=<FREQ2>
nf=<NUMBER_FREQUENCY>
sigma=<SIGMA>
interpolation=<INTERPOLATION_MODE>
v_tuning_thickness=<VELOCITY_TUNING_THICKNESS>
smooth=<SMOOTH>

number_scales=${nf}
process_id=$$

if [ ${input_type} == "landmark3D" ]
then
   if [ ${lm_min_line} -gt ${lm_max_line} ]
   then
      minep=${lm_max_line}
      maxep=${lm_min_line}
      if [ ${lm_inc_line} -lt 0 ]
      then
         incep=`expr ${lm_inc_line} \* -1`
      fi
   else
      maxep=${lm_max_line}
      minep=${lm_min_line}
      incep=${lm_inc_line}
   fi

   if [ ${lm_min_trace} -gt ${lm_max_trace} ]
   then
      mincdp=${lm_max_trace}
      maxcdp=${lm_min_trace}
      if [ ${lm_inc_trace} -lt 0 ]
      then
         inccdp=`expr ${lm_inc_trace} \* -1`
      fi
   else
      maxcdp=${lm_max_trace}
      mincdp=${lm_min_trace}
      inccdp=${lm_inc_trace}
   fi
fi

ncdp=`expr ${maxcdp} - ${mincdp}`
ncdp=`expr ${ncdp} / ${inccdp}`
ncdp=`expr ${ncdp} + 1`

nep=`expr ${maxep} - ${minep}`
nep=`expr ${nep} / ${incep}`
nep=`expr ${nep} + 1`


sec=`echo "scale=1; ${tmin} / 1000" | bc`

mincdpevent=${mincdp}
maxcdpevent=${maxcdp}
minepevent=${minep}
maxepevent=${maxep}

echo "project=" ${project}
echo "lm_in_project=" ${lm_in_project}
echo "lm_out_project=" ${lm_out_projec}
echo "lm_in_project_type=" ${lm_in_project_type}
echo "lm_out_project_type=" ${lm_out_project_type}
echo "lm_in_volume=" ${lm_in_volume}
#echo "lm_out_volume=" ${lm_out_volume}
echo "lm_min_line=" ${lm_min_line}
echo "lm_max_line=" ${lm_max_line}
echo "lm_inc_line=" ${lm_inc_line}
echo "lm_min_trace=" ${lm_min_trace}
echo "lm_max_trace=" ${lm_max_trace}
echo "lm_inc_trace=" ${lm_inc_trace}
echo "mincdp=" ${mincdp}
echo "maxcdp=" ${maxcdp}
echo "inccdp=" ${inccdp}
echo "minep=" ${minep}
echo "maxep=" ${maxep}
echo "incep=" ${incep}

echo "number_scales=" ${number_scales}
echo "out_scale_list=" ${out_scale_list}
echo "tmin=" ${tmin}
echo "horizon_files=" ${horizon_files}
echo "lm_horizon_files=" ${lm_horizon_files}
echo "horizon_times=" ${horizon_times}
echo "tmax=" ${tmax}
echo "dt=" ${dt}
echo "nx=" ${nx}
echo "run_normalization=" ${run_normalization}
echo "ncdp=" ${ncdp}
echo "nep=" ${nep}
echo "sec=" ${sec}
echo "gamma=" ${gamma}
echo "horizon_type=" ${horizon_type}

echo "freq1=" ${freq1}
echo "freq2=" ${freq2}
echo "nf=" ${nf}
echo "q=" ${sigma}
echo "interpolation=" ${interpolation}
echo "v_tuning_thickness=" ${v_tuning_thickness}
echo "smooth=" ${smooth}

dirname=`pwd`
echo "$0 running from ${dirname}"
cd $QISPACE/$QIPROJECT
echo "pwd=" `pwd`

if [ ! -d ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET_DIR} ]
   then
    mkdir -p ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET_DIR}
fi
if [ $? -ne 0 ]
then
echo "Error in creating ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET_DIR}"
echo "exit_status=$?"
exit $?
fi

#
# spec_decomp.sh
#
OUT_DATASET_FILENAME=${OUT_DATASET_FILENAME}_cwt
OUT_DATASET=${OUT_DATASET}_cwt
echo "------"
echo "About to run spec_decomp.sh"
echo "IN_DATASET =" ${IN_DATASET}
echo "IN_DATASET_FILENAME =" ${IN_DATASET_FILENAME}
echo "OUT_DATASET=" ${OUT_DATASET}
echo "OUT_DATASET_FILENAME=" ${OUT_DATASET_FILENAME}
echo "------"

echo "$QISEISMICS" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat
bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}

#put this in temp directory

if [ ! -d ${QISPACE}/${QIPROJECT}/tmp ]
   then
    mkdir -p ${QISPACE}/${QIPROJECT}/tmp
fi
if [ $? -ne 0 ]
then
echo "Error in creating ${QISPACE}/${QIPROJECT}/tmp"
echo "exit_status=$?"
exit $?
fi

bhpwavelet > tmp/wavelet.su

if [ ${input_type} == "landmark3D" ]
then
    echo "$QISEISMICS" > $QISPACE/$QIPROJECT/$QIDATASETS/${lm_in_volume_base}.dat
    bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${lm_in_volume_base}.dat filename=${lm_in_volume_base}
    lmread lm_project=${lm_in_project} lm_ptype=${lm_in_project_type} lm_brickname=${lm_in_volume} \
    iline=${lm_min_line}:${lm_max_line}:${lm_inc_line} \
    xline=${lm_min_trace}:${lm_max_trace}:${lm_inc_trace} tmin=${lm_min_time} tmax=${lm_max_time} | \
    bhpwritecube init=yes key1=cdp,${mincdp},${inccdp},${ncdp} key2=ep,${minep},${incep},${nep}\
    pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${lm_in_volume_base}.dat filename=${lm_in_volume_base}

    lmread lm_project=${lm_in_project} lm_ptype=${lm_in_project_type} lm_brickname=${lm_in_volume} \
    iline=${lm_min_line}:${lm_max_line}:${lm_inc_line} \
    xline=${lm_min_trace}:${lm_max_trace}:${lm_inc_trace} tmin=${lm_min_time} tmax=${lm_max_time} | \
    bhpContinuousWaveletTransform freq1=${freq1} freq2=${freq2} nf=${nf} interpolation=${interpolation} q=${sigma} velocity=${v_tuning_thickness} smooth=${smooth} | \
    bhpwritecube init=yes key3=cdp,${mincdp},${inccdp},${ncdp} key1=ep,${minep},${incep},${nep} key2=cdpt,1,1,${number_scales} \
    pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}

    if [ $? -ne 0 ]
    then
    echo "Error in running lmread | bhpContinuousWaveletTransform | bhpwritecube"
    echo "$0 job ends abnormally"
    echo "exit_status=$?"
    exit $?
    fi
elif [ ${input_type} == "bhpsu" ]
then
    bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${IN_DATASET}.dat filename=${IN_DATASET_FILENAME} keys=ep,cdp keylist=${minep}-${maxep}[${incep}]:${mincdp}-${maxcdp}[${inccdp}] |
    bhpContinuousWaveletTransform freq1=${freq1} freq2=${freq2} nf=${nf} interpolation=${interpolation} q=${sigma} velocity=${v_tuning_thickness} smooth=${smooth} |
    bhpwritecube init=yes key3=cdp,${mincdp},${inccdp},${ncdp} key1=ep,${minep},${incep},${nep} key2=cdpt,1,1,${number_scales} \
        pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}
    if [ $? -ne 0 ]
    then
    echo "Error in running bhpread | bhpContinuousWaveletTransform | bhpwritecube"
    echo "$0 job ends abnormally"
    echo "exit_status=$?"
    exit $?
    fi

else
    echo "Unsupported seismic data input type: ${input_type}"
    exit 1
fi

rm tmp/wavelet.su

echo "end of spec_decomp.sh"


if [ ${run_normalization} = "yes" ]
then
echo "nx=" ${nx}
#
# normavg.sh
#
    IN_DATASET_FILENAME=${OUT_DATASET_FILENAME}
    IN_DATASET=${OUT_DATASET}
    OUT_DATASET_FILENAME=${OUT_DATASET_FILENAME}_norm
    OUT_DATASET=${OUT_DATASET}_norm
    echo "------"
    echo "About to run normavg.sh"
    echo "IN_DATASET =" ${IN_DATASET}
    echo "IN_DATASET_FILENAME =" ${IN_DATASET_FILENAME}
    echo "OUT_DATASET=" ${OUT_DATASET}
    echo "OUT_DATASET_FILENAME=" ${OUT_DATASET_FILENAME}
    echo "------"

    echo "$QISEISMICS" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat
    bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}

    bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${IN_DATASET}.dat filename=${IN_DATASET_FILENAME} keys=ep,cdp,cdpt |
    #write this to a temporaary directory
    bhpavg nx=${nx} > tmp/avg_${IN_DATASET_FILENAME}_merge.su

    bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${IN_DATASET}.dat filename=${IN_DATASET_FILENAME} keys=ep,cdp,cdpt |
    bhpnorm gamma=${gamma} nx=${nx} avg=tmp/avg_${IN_DATASET_FILENAME}_merge.su |
    bhpwritecube init=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME} \
        key1=ep,${minep},${incep},${nep} key3=cdp,${mincdp},${inccdp},${ncdp} key2=cdpt,1,1,${number_scales}

    if [ $? -ne 0 ]
    then
    echo "Error in running bhpread | bhpnorm | bhpwritecube"
    echo "$0 job ends abnormally"
    echo "exit_status=$?"
    exit $?
    fi


    rm tmp/avg_${IN_DATASET_FILENAME}_merge.su
    echo "end of normavg.sh"


fi

#
# slice_all.sh
#
if [ ${run_slice} = "yes" ]
then
    if [ ${horizon_type} == "landmark" ]
    then
       if [ ! -d ${QISPACE}/${QIPROJECT}/${QIHORIZONS} ]
       then
        mkdir ${QISPACE}/${QIPROJECT}/${QIHORIZONS}
       fi
       lmexphor lm_project=${lm_in_project} lm_ptype=${lm_in_project_type} \
           lm_horizons=${lm_horizon_files} minline=${lm_min_line} maxline=${lm_max_line} \
           mintrace=${lm_min_trace} maxtrace=${lm_max_trace} dir=${QISPACE}/${QIPROJECT}/${QIHORIZONS}
    fi

    if [ ${horizon_type} == "bhpsu" ]
    then
        ii=0
        IFS=,
        for i in ${horizon_files}
        do
            horizon_array[${ii}]=$i
            ii=`expr ${ii} + 1`
        done


        horizon_files="";
        horizon_count_1=`expr ${#horizon_array[*]} - 1`
        for ((  jj = 0 ;  jj < ${#horizon_array[*]};  jj++  ))
        do
            IFS=:
            kk=0
            for k in ${horizon_array[$jj]}
            do
                if [ ${kk} = 0 ]
                then
                    dataset=$k
                    echo "dataset=${dataset}"
                fi
                if [ ${kk} = 1 ]
                then
                    horizon_name=$k
                    echo "horizon_name=${horizon_name}"
                fi
                kk=`expr ${kk} + 1`
            done
            bhpread keys=ep,cdp filename=${dataset} pathlist=${QISPACE}/${QIPROJECT}/${QIDATASETS}/${dataset}.dat \
            horizons=${horizon_name} |\
            bhpstorehdr keys=ep,cdp horizons=yes file=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${horizon_name}.xyz | \
            surange

            exit_status=$?
            echo "exit_status=$exit_status"
            if [ ${exit_status} != 0 ]
            then
                echo "Error in running bhpread | bhpstorehdr | surange"
                echo "$0 job ends abnormally"
                exit 1
            fi
            horizon_files=${horizon_files}${horizon_name}.xyz
            if [ ${jj} -lt ${horizon_count_1} ]
            then
                horizon_files=${horizon_files},
            fi
        done
    fi

    IN_DATASET_FILENAME=${OUT_DATASET_FILENAME}
    IN_DATASET=${OUT_DATASET}
    OUT_DATASET_FILENAME=${OUT_DATASET_FILENAME}_slice
    OUT_DATASET=${OUT_DATASET}_slice
    echo "------"
    echo "About to run slice_all.sh"
    echo "IN_DATASET =" ${IN_DATASET}
    echo "IN_DATASET_FILENAME =" ${IN_DATASET_FILENAME}
    echo "OUT_DATASET=" ${OUT_DATASET}
    echo "OUT_DATASET_FILENAME=" ${OUT_DATASET_FILENAME}
    echo "------"


    echo "$QISEISMICS" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat
    bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}


    ii=0;
    IFS=,
    for i in ${horizon_files}
    do
    #echo $i
    file_array[${ii}]=$i
    ii=`expr ${ii} + 1`
    done

    for ((  jj = 0 ;  jj < ${#file_array[*]};  jj++  ))
    do
      echo "file_array ${file_array[$jj]}"
    done

    array=( f1 f2 d1 d2 ungpow unscale )
    for ((  j = 0 ;  j < ${#array[*]};  j++  ))
    do
      echo "float header ${array[$j]}"
    done

    if [ ${#array[*]} -ge ${#file_array[*]} ]
    then
      header_number=${#file_array[*]}
    else
      header_number=${#array[*]}
    fi

    echo "header_number = " ${header_number}
    header_number_minus1=`expr ${header_number} - 1`

    command="bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${IN_DATASET}.dat filename=${IN_DATASET_FILENAME} keys=ep,cdpt,cdp keylist=${minepevent}-${maxepevent}:*:${mincdpevent}-${maxcdpevent}"

    slice_cmd="| bhpslice hlist="
    key_cmd_base="| sushw key="
    key_cmd=""
    for ((  k = 0 ;  k < ${header_number};  k++  ))
    do
      if [ $k -eq 0 ]
      then
        key_cmd="${key_cmd}${key_cmd_base}${array[$k]} a=${sec} "
      else
        key_cmd="${key_cmd}${key_cmd_base}${array[$k]} a=0 "
      fi
    done

    for ((  k = 0 ;  k < ${header_number};  k++  ))
    do
      command=${command}" | bhploadhdr key=ep,cdp,${array[$k]} infile=${QIHORIZONS}/${file_array[$k]} gridtype=R extrap=no "
      slice_cmd=${slice_cmd}${array[$k]}
      if [ $k -lt ${header_number_minus1} ]
      then
        slice_cmd=${slice_cmd},
      fi
    done

    slice_cmd="${slice_cmd} times=${horizon_times} tmin=${tmin} tmax=${tmax} dt=${dt} "

    command="${command}${slice_cmd}${key_cmd} | bhpwritecube init=yes key3=cdp,${mincdp},${inccdp},${ncdp} key2=cdpt,1,1,${number_scales} key1=ep,${minep},${incep},${nep} \
            pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}"


    temp_script="`basename $0`.tmp"
    echo "generating temporary script: ${temp_script}"
    echo "#!/bin/sh" > ${dirname}/${temp_script}
    echo "cd `pwd`" >> ${dirname}/${temp_script}
    echo "${command}" >> ${dirname}/${temp_script}
    echo "${command}"
    chmod u+x ${dirname}/${temp_script}
    sh -c ${dirname}/${temp_script}
    if [ $? -eq 0 ]
    then
        echo "removing temporary script: ${temp_script}"
        rm ${dirname}/${temp_script}
    else
        echo "$0 job ends abnormally"
        exit 1
    fi

    IN_DATASET_FILENAME=${OUT_DATASET_FILENAME}
    IN_DATASET=${OUT_DATASET}
    OUT_DATASET_FILENAME=${OUT_DATASET_FILENAME}_transpose_ep
    OUT_DATASET=${OUT_DATASET}_transpose_ep

    echo "------"
    echo "About to run bhptranspose"
    echo "IN_DATASET =" ${IN_DATASET}
    echo "IN_DATASET_FILENAME =" ${IN_DATASET_FILENAME}
    echo "OUT_DATASET=" ${OUT_DATASET}
    echo "OUT_DATASET_FILENAME=" ${OUT_DATASET_FILENAME}
    echo "------"

    #echo -e "$QISEISMICS" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}_transpose_cdp.dat
    echo "$QISEISMICS" > $QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat
    #bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}_transpose_cdp.dat filename=${OUT_DATASET_FILENAME}_transpose_cdp
    bhpio delete=yes pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME}

    #bhptranspose pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}_slice_transpose_cdp.dat  filename=${OUT_DATASET_FILENAME}
    bhptranspose pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat  filename=${IN_DATASET_FILENAME}
    if [ $? -ne 0 ]
    then
    echo "Error in running bhptranspose"
    echo "$0 job ends abnormally"
    echo "exit_status=$?"
    exit $?
    fi

    echo "end of slice_all.sh for the spec decomp"

fi

if [ ${output_type} == "landmark3D" ]
then
    ii=0;
    IFS=,
    for i in ${out_scale_list}
    do
    scale_array[${ii}]=$i
    ii=`expr ${ii} + 1`
    done

    for ((  jj = 0 ;  jj < ${#scale_array[*]};  jj++  ))
    do
      echo "scale_array ${scale_array[${jj}]}"
      old_ow_pmpath=$OW_PMPATH
      export OW_PMPATH=$OW_PMPATH_DM
      echo "Before Landmark access OW_PMPATH=$OW_PMPATH"
      bhpread pathlist=$QISPACE/$QIPROJECT/$QIDATASETS/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME} keys=ep,cdpt,cdp keylist=*:${scale_array[$jj]}:* |
      lmwrite lm_project=${lm_out_project} lm_ptype=${lm_out_project_type} lm_brickname=${OUT_DATASET_FILENAME}_${scale_array[$jj]}.bri
      if [ $? -ne 0 ]
      then
      echo "Error in running bhpread | lmwrite"
      echo "exit_status=$?"
      echo "$0 job ends abnormally"
      exit $?
      fi

      export OW_PMPATH=$old_ow_pmpath
      echo "After Landmark access OW_PMPATH=$OW_PMPATH"
    done
fi

exit_status=$?
echo "exit_status=$exit_status"
if [ ${exit_status} != 0 ]
then
    echo "$0 job ends abnormally"
    exit 1
fi
echo "$0 job ends normally"

