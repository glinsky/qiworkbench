//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.util.colorbar;

/** 
 * A new Button for point.
 * @author leely@mail.nankai.edu.cn
 * @author LiLi
 * @version 1.0 
 */

import javax.swing.JButton;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Dimension;

public class PointButton extends JButton {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** flag :to index the button along with points_value*/
	private int flag;

	/**point color*/
	private Color col;

	public static final int BUTTON_WIDTH = 10;

	public static final int BUTTON_HEIGHT = 20;

	/**
	 * Constructor with parametres 
	 * @param  color 
	 * @param f
	 */
	public PointButton(Color color, int f) {
		flag = f;
		col = color;
		this.setPreferredSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		this.setRolloverEnabled(false);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.black);
		if (col.equals(Color.black))
			g.setColor(Color.white);
		g.drawRect(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		g.setColor(col);
		g.fillRect(1, 1, BUTTON_WIDTH - 2, BUTTON_HEIGHT - 2);
	}

	/** 
	 * Get the flag
	 * @return int
	 */
	public int getFlag() {
		return flag;
	}

	/** 
	 * Set the flag
	 * @param flag
	 */
	public void setFlag(int flag) {
		this.flag = flag;
	}

	/** 
	 * Get the color
	 * @return Color
	 */
	public Color getColor() {
		return col;
	}

	/** Set the color
	 * @param color
	 */
	public void setColor(Color color) {
		this.col = color;
	}
}