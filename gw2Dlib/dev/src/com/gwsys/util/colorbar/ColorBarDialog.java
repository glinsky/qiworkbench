//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.util.colorbar;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Vector;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

/**
 * This is GUI for editing colormap and implements
 * the ColormapIO interface.
 * 
 * @author Hua
 *
 */
public final class ColorBarDialog extends JDialog implements ColormapIO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1673554514281484335L;

	private Vector colorListeners = new Vector();

	JPanel jPanel1 = new JPanel();

	TitledBorder titledBorder1;

	JPanel jPanel2 = new JPanel();

	JPanel jPanel3 = new JPanel();

	TitledBorder titledBorder2;

	TitledBorder titledBorder3;

	JPanel jPanel4 = new JPanel();

	JTextField jTextField1 = new JTextField();

	JLabel jLabel1 = new JLabel();

	GridLayout gridLayout4 = new GridLayout();

	JButton Reverse = new JButton();

	JButton Apply = new JButton();

	JButton Load = new JButton();

	JButton Ok = new JButton();

	JButton Save = new JButton();

	JButton Close = new JButton();

	ColorBar colorBar = new ColorBar();

	String xmlFileName = null;

	public ColorBarDialog() {
		buildGUI();
	}

	public ColorBarDialog(ColorChangeListener listener) {
		addColorChangeListener(listener);
	}

	private void buildGUI() {
		titledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(
				Color.white, new Color(148, 145, 140)), "Color Bar");
		titledBorder2 = new TitledBorder(BorderFactory.createEtchedBorder(
				Color.white, new Color(148, 145, 140)), "");
		titledBorder3 = new TitledBorder(BorderFactory.createEtchedBorder(
				Color.white, new Color(148, 145, 140)), "Actions");
		this.getContentPane().setLayout(new BorderLayout());
		jPanel1.setBorder(titledBorder1);
		jPanel1.setLayout(new BorderLayout());
		jPanel2.setBorder(titledBorder2);
		jPanel2.setLayout(new BorderLayout());
		jPanel3.setBorder(titledBorder3);
		jPanel3.setLayout(gridLayout4);
		jPanel4.setLayout(new BorderLayout());
		jTextField1.setText("Default");
		jLabel1.setText("Name");
		gridLayout4.setColumns(4);// modify the size of the columns
		gridLayout4.setRows(2);
		Reverse.setText("Reverse");
		Reverse.addActionListener(new ColorBarViewer_Reverse_actionAdapter());
		Apply.setText("Apply");
		Apply.addActionListener(new ColorBarViewer_Apply_actionAdapter());
		Load.setActionCommand("LoadCMP");
		Load.setText("Load CMP");
		Load.addActionListener(new ColorBarViewer_Load_actionAdapter());
		Ok.setText("OK");
		Ok.addActionListener(new ColorBarViewer_Ok_actionAdapter());
		Save.setActionCommand("SaveCMP");
		Save.setText("Save CMP");
		Save.addActionListener(new ColorBarViewer_Save_actionAdapter());
		Close.setText("Close");
		Close.addActionListener(new ColorBarViewer_Close_actionAdapter());
		this.getContentPane().add(jPanel1, BorderLayout.CENTER);
		jPanel1.add(jPanel2, BorderLayout.NORTH);
		jPanel2.add(jPanel4, BorderLayout.NORTH);
		jPanel4.add(jLabel1, BorderLayout.WEST);
		jPanel4.add(jTextField1, BorderLayout.CENTER);
		jPanel2.add(colorBar, BorderLayout.SOUTH);

		jPanel1.add(jPanel3, BorderLayout.CENTER);
		jPanel3.add(Reverse, null);
		jPanel3.add(Load, null);
		jPanel3.add(Save, null);
		jPanel3.add(Apply, null);
		jPanel3.add(Ok, null);
		jPanel3.add(Close, null);

		this.setTitle("Settings of ColorBar");
		pack();
	}

	void Reverse_actionPerformed(ActionEvent e) {
		colorBar.reverseColor();
		String name = jTextField1.getText();
		if (name.endsWith("-reverse"))
			jTextField1
					.setText(name.substring(0, name.lastIndexOf("-reverse")));
		else
			jTextField1.setText(jTextField1.getText() + "-reverse");
	}

	void Load_actionPerformed(ActionEvent e) throws IOException{
		JFileChooser tt = new JFileChooser();
		tt.addChoosableFileFilter(new MyFilter("cmp", "cmp File"));
		
		if (JFileChooser.APPROVE_OPTION == tt.showOpenDialog(this)) {
			xmlFileName = tt.getSelectedFile().getPath();
			File newFile = new File(xmlFileName);
			if (newFile.exists()) {
				Scanner sc = new Scanner(newFile);
				String str=sc.nextLine();
				if (str.indexOf("xml")>0){
					ReadAndWriteXML link = new ReadAndWriteXML(xmlFileName);
					RecordCell cell = link.readFromXml();
					jPanel2.remove(colorBar);
					colorBar = new ColorBar(cell.getNumberPoint(), cell.getPoints());

					jTextField1.setText(cell.getName());
				}else{
					int n,r,g,b, number=Integer.parseInt(str.substring(str.indexOf("=")+1,str.length()).trim());
					float acum=0;
					float step=1.0f/(number-1);
					ColorPoint arreglo[] = new ColorPoint[number];
					for (n=0;n<number;n++){
						r=Integer.parseInt(sc.next());
						g=Integer.parseInt(sc.next());
						b=Integer.parseInt(sc.next());
						arreglo[n]= new ColorPoint(new Color(r,g,b), acum);
						acum += step;	
						sc.next();
					}
					
					jPanel2.remove(colorBar);
					colorBar = new ColorBar(number, arreglo);
					jTextField1.setText(str.substring(0, str.indexOf("=")-1));
				}
				sc.close();
				jPanel2.add(colorBar, BorderLayout.CENTER);
				this.setTitle("ColorBar : [" + xmlFileName + "]");
				this.pack();
				this.repaint();
			}
		}
	}
	
	void Save_actionPerformed(ActionEvent e) throws IOException{
		JFileChooser tt = new JFileChooser();
		tt.addChoosableFileFilter(new MyFilter("cmp", "cmp File"));
		String currFileName = null;
		if (JFileChooser.APPROVE_OPTION == tt.showSaveDialog(this)) {
			currFileName = tt.getSelectedFile().getPath();
			xmlFileName = currFileName;
		}
		if (currFileName != null) {
			if (currFileName.endsWith(".cmp"))
				currFileName = currFileName.substring(0, currFileName
						.lastIndexOf(".cmp"));
			currFileName = currFileName + ".cmp";
			RecordCell cell = new RecordCell(jTextField1.getText(), colorBar
					.getNumber_point(), colorBar.getPoints_value());
			ReadAndWriteXML link = new ReadAndWriteXML(currFileName);
			link.writeToXml(cell);
		}
	}

	void Apply_actionPerformed(ActionEvent e) {
		ColorChangedEvent event = new ColorChangedEvent(getColorBar());
		//apply to listener
		for (int i = colorListeners.size()-1; i>=0; i--){
			((ColorChangeListener)colorListeners.get(i)).colormapChanged(event);
		}
	}

	void Ok_actionPerformed(ActionEvent e) {
		Apply_actionPerformed(e);
		dispose();

	}

	void Close_actionPerformed(ActionEvent e) {
		dispose();
	}

	
	public ColorBar getColorBar() {
		return colorBar;
	}

	public void addColorChangeListener(ColorChangeListener listener) {
		colorListeners.addElement(listener);
	}


	public void removeColorChangeListener(ColorChangeListener listener) {
		colorListeners.removeElement(listener);
	}

	class MyFilter extends javax.swing.filechooser.FileFilter {
		String extension, description;

		public MyFilter(String extension, String description) {
			this.extension = extension;
			this.description = description;
		}

		public boolean accept(File file) {
			if (file.getName().endsWith(extension)) {
				return true;
			} else if (file.isDirectory()) {
				return true;
			}
			return false;
		}

		public String getDescription() {
			return this.description;
		}
	}
	
	class ColorBarViewer_Ok_actionAdapter implements ActionListener {
		public void actionPerformed(ActionEvent e) {
		  Ok_actionPerformed(e);
		}
	}

	class ColorBarViewer_Close_actionAdapter implements ActionListener {

		public void actionPerformed(ActionEvent e) {
		  Close_actionPerformed(e);
		}
	}
	
	class ColorBarViewer_Save_actionAdapter implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
		  try {
			Save_actionPerformed(e);
		  } catch (IOException e1) {
			
			e1.printStackTrace();
		  }
		}
	}

	class ColorBarViewer_Apply_actionAdapter implements ActionListener {
		public void actionPerformed(ActionEvent e) {
		  Apply_actionPerformed(e);
		}
	}
	
	class ColorBarViewer_Reverse_actionAdapter implements ActionListener {
	
		public void actionPerformed(ActionEvent e) {
		  Reverse_actionPerformed(e);
		}
	}

	class ColorBarViewer_Load_actionAdapter implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
		  try {
			Load_actionPerformed(e);
		  } catch (IOException e1) {
			
			e1.printStackTrace();
		  }
		}
	}
	
	public boolean saveColormap(ColorBar colormap) {
		if (colormap!=null)
			colorBar = colormap;
		try{
			Save_actionPerformed(null);
			return true;
		}catch(IOException e){
			return false;
		}	
	}

	public boolean loadColormap(ColorBar colormap) {
		if (colormap!=null)
			colorBar = colormap;
		try{
			Load_actionPerformed(null);
			return true;
		}catch(IOException e){
			return false;
		}		
	}
}
