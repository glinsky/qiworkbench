//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.util.colorbar;

/** 
 * ColorBar Core
 * @author leely@mail.nankai.edu.cn
 * @author LiLi
 * @version 1.0 
 * @since Aug 21, 2006 LookAndFeel cannot be set in this class.
 */

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JColorChooser;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.IndexColorModel;
import java.nio.ByteBuffer;

import com.gwsys.util.colorbar.PointButton;
import com.gwsys.util.colorbar.ColorPoint;

public class ColorBar extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** How many points are there in the ColorBar */
	private int number_point;

	/** An anrry to recorde the color and position of each point*/
	private ColorPoint points_value[];

	/** each point is in fact a JButton*/
	private PointButton points_view[];

	public static final int COLORBAR_HEIGHT = 25;

	public static final int COLORBAR_WIDTH = 500;

	public static final Color BACKGROUD_COLOR = new Color(255, 0, 0);

	/** Constructor with no parameter. */
	public ColorBar() {         
		this(ColorBar.getDefaultColorBar());
	}

	/**
	 * Constructor with parameters .
	 * @param  num Number of Points.
	 * @param color Array of ColorPoint.
	 */
	public ColorBar(int num, ColorPoint[] color) {
		super();
		number_point = num;
		points_value = color;
		appendButton();
		appendStyle();
	}

	/**
	 * Constructor with parametres 
	 * @param  colors The pararmeter colors must be at least have two color,
	 * one is at the left of the bar,and one is at right.
	 *
	 */
	public ColorBar(Color[] colors) {
		super();
		number_point = colors.length;
		points_value = new ColorPoint[number_point];
		double po;
		for (int i = 0; i < number_point; i++) {
			po = (i + 0.0) / (number_point - 1);
			points_value[i] = new ColorPoint(colors[i], po);
		}
		appendButton();
		appendStyle();
	}

	/**
	 * Copy constructor. Creates a gradient bar that is a duplicate of another.
	 * @param dup
	 */
	public ColorBar(ColorBar dup) {
		super();

		number_point = dup.number_point;
		points_value = dup.points_value;
		appendButton();
		appendStyle();
	}

	/**
	 * Create a default colrobar
	 * 
	 */
	public static ColorBar getDefaultColorBar() {
		ColorPoint g[] = new ColorPoint[5];
		g[0] = new ColorPoint(Color.blue, 0);
		g[1] = new ColorPoint(Color.cyan, 0.25);
		g[2] = new ColorPoint(Color.green, 0.5);
		g[3] = new ColorPoint(Color.yellow, 0.75);
		g[4] = new ColorPoint(Color.red, 1);
		ColorBar temp = new ColorBar(5, g);
		return temp;
	}

	/**
	 * converse the color of the colorbar
	 * 
	 */
	public void reverseColor() {
		ColorPoint newValue[] = new ColorPoint[number_point];
		for (int i = 0; i < number_point; i++) {
			newValue[i] = new ColorPoint(points_value[number_point - 1 - i]);
			newValue[i].setPosition(1 - newValue[i].getPosition());
		}
		points_value = newValue;

		dependButton();
		appendButton();

		this.updateUI();
	}

	/**
	 * get the number of the points
	 * @return  int
	 */
	public int getNumber_point() {
		return number_point;
	}

	/**
	 * get the Values of the colorbar
	 * @return  points_value
	 */
	public ColorPoint[] getPoints_value() {
		return points_value;
	}

	/**
	 * Some settings about ui
	 * @return  void
	 */
	void appendStyle() {
		this.setPreferredSize(new Dimension(COLORBAR_WIDTH, COLORBAR_HEIGHT));
		this.addMouseListener(new mouseC());
		this.setBackground(BACKGROUD_COLOR);		
	}

	/**
	 *append buttons to the colorbar
	 */
	void appendButton() {
		points_view = new PointButton[number_point];
		for (int i = 0; i < number_point; i++) {
			points_view[i] = new PointButton(points_value[i].getColor(), i);
			points_view[i].addMouseListener(new mouseA());
			points_view[i].addMouseMotionListener(new mouseB());
			this.add(points_view[i]);
		}
	}

	/**
	 *dis-append buttons to the colorbar
	 */
	void dependButton() {
		for (int i = 0; i < number_point; i++) {
			points_view[i].removeMouseListener(new mouseA());
			points_view[i].removeMouseMotionListener(new mouseB());
			this.remove(points_view[i]);
		}
	}

	/**
	 *draw the panel of the colorbar
	 */
	void viewColor(Graphics g) {
		g.clearRect(0, 0, (int) getSize().getWidth(), (int) getSize()
				.getHeight());

		for (int i = 0; i < number_point - 1; i++) {

			int redChange = points_value[i + 1].getColor().getRed()
					- points_value[i].getColor().getRed();
			int greenChange = points_value[i + 1].getColor().getGreen()
					- points_value[i].getColor().getGreen();
			int blueChange = points_value[i + 1].getColor().getBlue()
					- points_value[i].getColor().getBlue();
			double width = (this.getSize().getWidth() - PointButton.BUTTON_WIDTH)
					* (points_value[i + 1].getPosition() - points_value[i]
							.getPosition());

			float rpp = (float) (redChange / width);
			float gpp = (float) (greenChange / width);
			float bpp = (float) (blueChange / width);

			int start = (int) ((this.getSize().getWidth() - PointButton.BUTTON_WIDTH) * (points_value[i]
					.getPosition()));
			int end = (int) ((this.getSize().getWidth() - PointButton.BUTTON_WIDTH) * (points_value[i + 1]
					.getPosition()));
			for (int j = start; j < end; j++) {
				g.setColor(new Color(points_value[i].getColor().getRed()
						+ (int) ((j - start) * rpp), points_value[i].getColor()
						.getGreen()
						+ (int) ((j - start) * gpp), points_value[i].getColor()
						.getBlue()
						+ (int) ((j - start) * bpp)));
				g.drawLine(j, 0, j, (int) this.getSize().getHeight()
						- PointButton.BUTTON_HEIGHT / 2);
			}
		}

		g.setColor(new Color(0, 0, 0));
		g.drawLine(0, (int) this.getSize().getHeight()
				- PointButton.BUTTON_HEIGHT / 2, (int) (this.getSize()
				.getWidth() - PointButton.BUTTON_WIDTH), (int) this.getSize()
				.getHeight()
				- PointButton.BUTTON_HEIGHT / 2);
	}

	/**
	 *set the positions of buttons
	 */
	void exportButton() {

		this.setLayout(null);
		for (int i = 0; i < number_point; i++) {
			int po = (int) ((this.getSize().getWidth() - PointButton.BUTTON_WIDTH) * points_value[i]
					.getPosition());
			points_view[i].setBounds(po, (int) this.getSize().getHeight()
					- PointButton.BUTTON_HEIGHT, PointButton.BUTTON_WIDTH,
					PointButton.BUTTON_HEIGHT);
		}
	}

	/** 
	 * Get color of given position
	 * @param  position ,the min of this value is 0,and max is 1.When it is 0,the position of point is left of the bar,it is 1 the position of point is right
	 * @return Color
	 */
	public Color getColor(double position) {
		/*
		int index = 0;
		for (int i = 0; i < number_point - 1; i++) {
			if (position == points_value[i].getPosition())
				return points_value[i].getColor();
			if (position < points_value[i].getPosition())
				break;
			index = i;
		}

		int redChange = points_value[index + 1].getColor().getRed()
				- points_value[index].getColor().getRed();
		int greenChange = points_value[index + 1].getColor().getGreen()
				- points_value[index].getColor().getGreen();
		int blueChange = points_value[index + 1].getColor().getBlue()
				- points_value[index].getColor().getBlue();
		double width = (this.getSize().getWidth() - PointButton.BUTTON_WIDTH)
				* (points_value[index + 1].getPosition() - points_value[index]
						.getPosition());

		float rpp = (float) (redChange / width);
		float gpp = (float) (greenChange / width);
		float bpp = (float) (blueChange / width);

		int start = (int) ((this.getSize().getWidth() - PointButton.BUTTON_WIDTH) * (points_value[index]
				.getPosition()));
		int end = (int) ((this.getSize().getWidth() - PointButton.BUTTON_WIDTH) * position);

		return new Color(points_value[index].getColor().getRed()
				+ (int) ((end - start) * rpp), points_value[index].getColor()
				.getGreen()
				+ (int) ((end - start) * gpp), points_value[index].getColor()
				.getBlue()
				+ (int) ((end - start) * bpp));
	}*/
		
		ColorPoint before = null;
		ColorPoint after = null;
		ColorPoint p = null;

		//check boundary positions first
		if (position <= 0)
			{
			return points_value[0].getColor();
			}
		if (position >= 1)
			{
			return points_value[points_value.length-1].getColor();
			}

		// find before and after points
		for (int i = 0; i<points_view.length; i++)
			{
			p = points_value[i];
			double positionValue = p.getPosition();
			if (positionValue < position)
				{
				before = p;
				} else if (positionValue == position)
				{
				// return the extact match, if it exists
				return p.getColor();
				} else if (positionValue > position)
				{
				after = p;
				break;
				}
			}

		if (before == null || after == null)
			{ // we can't perform this lookup
			return null;
			}
		double fraction = (position - before.getPosition()) / 
		(after.getPosition() - before.getPosition());
		float[] startComponents = before.getColor().getComponents(null);
		float[] endComponents = after.getColor().getComponents(null);
		float[] components = new float[startComponents.length];
		for (int colourCtr = 0; colourCtr < components.length; colourCtr++)
			{
			float value = (float) ((endComponents[colourCtr] - 
					startComponents[colourCtr]) * fraction + 
					startComponents[colourCtr]);
			components[colourCtr] = value;
			}
		return new Color(components[0], components[1], components[2], 
				components[3]);

	}
	/** 
	 * Get color array from this colorbar setting.
	 * 
	 * @return Color[]
	 */
	public Color[] getColorArray() {
		Color temp[] = new Color[number_point];
		for (int i = 0; i < number_point; i++) {
			temp[i] = new Color(points_value[i].getColor().getRed(),
					points_value[i].getColor().getGreen(), points_value[i]
							.getColor().getBlue());
		}
		return temp;
	}

	public void paintComponent(Graphics g) {
		super.paintComponents(g);
		viewColor(g);
		exportButton();
	}

	/** 
	 * build a colorbar by given colors.
	 * @param  cols
	 */
	public void setColorArray(Color[] cols) {
		Color[] colors = cols;
		if (cols == null)
			colors = new Color[]{Color.yellow, Color.yellow};
		if (cols.length == 1)
			colors = new Color[]{cols[0], cols[0]};
		dependButton();
		number_point = colors.length;
		points_value = new ColorPoint[number_point];
		double po;
		for (int i = 0; i < number_point; i++) {
			po = (i + 0.0) / (number_point - 1);
			points_value[i] = new ColorPoint(colors[i], po);
		}

		appendButton();
	}

	/** change the color of one points
	 * @param  e
	 */
	void changePointColor(MouseEvent e) {
		PointButton press = (PointButton) e.getSource();
		int flag = press.getFlag();
		
		Color getcol = JColorChooser.showDialog(this, "Choose a color",
				points_value[flag].getColor());

		if (getcol != null) {
			points_value[flag].setColor(getcol);
			points_view[flag].setColor(getcol);
			this.updateUI();
		}
	}

	/** change the position of one points
	 * @param  e
	 */
	void changePointPosition(MouseEvent e) {
		PointButton press = (PointButton) e.getSource();
		int flag = press.getFlag();

		if (flag != 0 && flag != number_point - 1) {
			double prePo = points_value[flag - 1].getPosition();
			double nextPo = points_value[flag + 1].getPosition();
			double po = (e.getPoint().getX() / (this.getSize().getWidth() - PointButton.BUTTON_WIDTH));
			if (points_value[flag].getPosition() + po < nextPo
					&& points_value[flag].getPosition() + po > prePo)
				points_value[flag].setPosition(points_value[flag].getPosition()
						+ po);
			this.updateUI();
		}
	}

	/** delete a button by given point
	 * @param  index
	 */
	void deletePoint(int index) {
		for (int i = index; i < number_point - 1; i++) {
			points_value[i].setColor(points_value[i + 1].getColor());
			points_value[i].setPosition(points_value[i + 1].getPosition());
		}

		dependButton();
		number_point--;
		appendButton();

		this.updateUI();
	}

	/** add a button by given  position
	 * @param  position
	 */
	void addPoint(double position) {
		//find nearest flag
		if (position <= 0
				|| position >= (this.getSize().getWidth() - PointButton.BUTTON_WIDTH))
			return;
		position = position
				/ (this.getSize().getWidth() - PointButton.BUTTON_WIDTH);
		int i, index = 0;
		for (i = 0; i < number_point - 1; i++) {
			if (position == points_value[i].getPosition())
				return;
			if (position < points_value[i].getPosition())
				break;
			index = i;
		}

		ColorPoint newValue[] = new ColorPoint[number_point + 1];
		for (i = 0; i <= index; i++)
			newValue[i] = new ColorPoint(points_value[i]);
		newValue[index + 1] = new ColorPoint(getColor(position), position);
		for (i = index + 2; i <= number_point; i++)
			newValue[i] = new ColorPoint(points_value[i - 1]);
		points_value = newValue;

		dependButton();
		number_point++;
		appendButton();

		this.updateUI();

	}

	class mouseA extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2 && e.getButton() == 1) {
				changePointColor(e);
			}

			if (e.getClickCount() == 1 && e.getButton() == 3) {
				if (((PointButton) e.getSource()).getFlag() != 0
						&& ((PointButton) e.getSource()).getFlag() != number_point - 1) {
					deletePoint(((PointButton) e.getSource()).getFlag());
				}
			}
		}
	}

	class mouseB extends MouseMotionAdapter {
		public void mouseDragged(MouseEvent e) {
			if (e.getModifiers() == 16)
				changePointPosition(e);
		}
	}

	class mouseC extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {

			if (e.getButton() == 3 && e.getClickCount() == 1) {
				double po = e.getPoint().getX();
				addPoint(po);
			}
		}
	}

	public IndexColorModel getIndexColorModel() {
		Color[] colorArray = getColorArray();

		byte[] colorBytes = new byte[colorArray.length * 4];

		ByteBuffer bb = ByteBuffer.wrap(colorBytes);

		byte  alpha = 0;
		boolean transparent = true;
		// putUnsignedByte
		for (int i = 0; i < colorArray.length; i++) {
			bb.put((byte) (colorArray[i].getRed() & 0xff));
			bb.put((byte) (colorArray[i].getGreen() & 0xff));
			bb.put((byte) (colorArray[i].getBlue() & 0xff));
			alpha = (byte) (colorArray[i].getAlpha() & 0xff);
			//if (alpha == 0xff)
				//transparent = false;
			bb.put(alpha);

		}

		IndexColorModel indexColorModel = new IndexColorModel(8, colorArray.length,
				colorBytes, 0, transparent);
		return indexColorModel;
	}
}