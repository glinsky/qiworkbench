//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.util.colorbar;

import java.awt.Color;
import java.lang.String;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.dom.DOMSource;

import org.w3c.dom.Document;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gwsys.util.colorbar.ColorPoint;
import com.gwsys.util.colorbar.RecordCell;

/**
 * @author leely@mail.nankai.edu.cn
 * @author LiLi
 * @version 1.0 
 */

public class ReadAndWriteXML {
	/**
	 *record date about a colorbar
	 */
	private RecordCell record;

	/**
	 * cmp file name
	 */
	private String filename;

	public ReadAndWriteXML(String file) {
		filename = file;
	}

	/**
	 * Read cmp file 
	 * @return RecordCell
	 */
	public RecordCell readFromXml() throws IOException
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		try {
			db = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException pce) {
			System.out.println(pce);
		}

		Document doc = null;
		try {
			doc = db.parse(filename);
		} catch (DOMException dom) {
			System.out.println(dom);
		} catch (SAXException sax) {
			System.out.println(sax);
		}

		Element root = doc.getDocumentElement();
		int num_points = Integer.parseInt(root.getAttribute("number_point"));
		record = new RecordCell(num_points);
		record.setName(root.getAttribute("name"));

		NodeList points = root.getElementsByTagName("colorpoint");
		num_points = Math.min(points.getLength(), num_points);
		for (int i = 0; i < num_points; i++) {
			Element point = (Element) points.item(i);
			record.getPoints()[i] = new ColorPoint(new Color(Integer
					.parseInt(point.getAttribute("color_red")), Integer
					.parseInt(point.getAttribute("color_green")), Integer
					.parseInt(point.getAttribute("color_blue"))), Double
					.parseDouble(point.getAttribute("position")));
		}

		return record;
	}

	/**
	 * write cmp file
	 * 
	 * @param record
	 */
	public void writeToXml(RecordCell record) throws IOException{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		try {
			db = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException pce) {
			System.out.println(pce);
		}
		Document doc = db.newDocument();

		Element root = doc.createElement("colorbar");
		root.setAttribute("name", record.getName());
		root.setAttribute("number_point",
				(new Integer(record.getNumberPoint())).toString());
		doc.appendChild(root);

		for (int i = 0; i < record.getPoints().length; i++) {
			Element point = doc.createElement("colorpoint");
			point.setAttribute("position", (new Double(record.getPoints()[i]
					.getPosition())).toString());
			point.setAttribute("color_red", (new Integer(record.getPoints()[i]
					.getColor().getRed())).toString());
			point.setAttribute("color_green", (new Integer(
					record.getPoints()[i].getColor().getGreen())).toString());
			point.setAttribute("color_blue", (new Integer(record.getPoints()[i]
					.getColor().getBlue())).toString());
			root.appendChild(point);
		}

		try {
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			DOMSource source = new DOMSource(doc);
			PrintWriter pw = new PrintWriter(new FileOutputStream(filename));
			StreamResult result = new StreamResult(pw);
			transformer.transform(source, result);
		} catch (TransformerException e) {
			e.printStackTrace();
		} 

	}

}