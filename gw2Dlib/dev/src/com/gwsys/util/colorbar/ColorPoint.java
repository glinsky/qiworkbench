//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.util.colorbar;

import java.awt.Color;

/**
 * @author leely@mail.nankai.edu.cn
 * @author LiLi
 * @version 1.0
 */
public class ColorPoint {
	private Color color;

	private double position;

	/** Constructor with no parameter. */
	public ColorPoint() {
	}

	/**
	 * Constructor with parameters. 
	 * @param color, 
	 * @param position
	 */
	public ColorPoint(Color color, double position) {
		this.color = color;
		this.position = position;
	}

	public ColorPoint(ColorPoint v) {
		color = v.getColor();
		position = v.getPosition();
	}

	/**
	 * Get the color.
	 * 
	 * @return Color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Set the color.
	 * 
	 * @param color
	 */

	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * Get the position.
	 * @return position
	 */

	public double getPosition() {
		return position;
	}

	/**
	 * Set the position
	 * 
	 * @param pos
	 */
	public void setPosition(double pos) {
		position = pos;
	}
}