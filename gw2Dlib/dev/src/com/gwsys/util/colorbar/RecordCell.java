//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.util.colorbar;

/**
 * Defines the cell to record.
 * @author leely@mail.nankai.edu.cn
 * @version 1.0
 */

import com.gwsys.util.colorbar.ColorPoint;

public class RecordCell {
	private String name;

	private int number_point;

	private ColorPoint points[];

	public RecordCell(int n) {
		number_point = n;
		points = new ColorPoint[n];
	}

	public RecordCell(String name, int number_point, ColorPoint points[]) {
		this.name = name;
		this.number_point = number_point;
		this.points = points;
	}

	/**
	 * Set the name
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the name
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get the number of points
	 * @return int
	 */
	public int getNumberPoint() {
		return number_point;
	}

	/**
	 * Set the colors in the Value
	 * 
	 * @param points
	 */
	public void setPoints(ColorPoint points[]) {
		this.points = points;
	}

	/**
	 * Get the colors in the Value
	 * 
	 * @return ColorPoint Array
	 */
	public ColorPoint[] getPoints() {
		return points;
	}

	public String toString() {
		String temp = "name:[" + name + "],number_point:[" + number_point
				+ "],points:";
		if (points != null)
			for (int i = 0; i < points.length; i++) {
				temp = temp + i + ",(" + points[i].getColor() + ","
						+ points[i].getPosition() + ") ";
			}
		return temp;
	}
}