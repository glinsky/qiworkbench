//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.ruler;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;

/**
 * Defines the RulerModel to work with RulerView class.
 * 
 * @author Bob
 *
 */
public interface RulerModel {
	/**
	 * The ruler direction is horizontal.
	 */
	public static final int HORIZONTAL = 0;
	/**
	 * The ruler direction is vertical.
	 */
    public static final int VERTICAL = 1;
    /**
     * Gets the start value for this ruler.
     * @return
     */
	public double getStart();
	  
	/**
	 * Gets the end value for this ruler.
	 * @return
	 */
	public double getEnd();

	/**
	 * Gets the length in pixels for major tick.
	 * @return
	 */
	public int getMajorTickLength();
	/**
	 * Gets the length in pixels for minor tick.
	 * @return
	 */
	public int getMinorTickLength();
	/**
	 * Checks if the current tick belongs to major or minor tick.
	 * @return
	 */
	public boolean isMajorTick();
	
	/**
	 * Returns the direction of ruler (HORIZONTAL or VERTICAL).
	 * @return
	 */
	public int getOrientation();
	
	/**
	 * Gets the Title for this ruler.
	 * @return
	 */
	public String getTitle();
	
	/**
	 * Get the unit string for this ruler.
	 * @return the unit string for this ruler.
	 */
	public String getUnitSymbol();
	
	/**
	 * Gets the Font for this tick label.
	 * @return
	 */
	public Font getLabelFont();
	/**
	 * Gets the Font for this title of ruler.
	 * @return
	 */
	public Font getTitleFont();
	
	/**
	 * Add listener for this model.
	 * @param listener
	 */
	public void addListener(RulerModelListener listener);
	  
	/**
	 * Remove listener from this model.
	 * @param listener
	 */
	public void removeListener(RulerModelListener listener);
	
	/**
	 * Gets the color for label.
	 * @return
	 */
	public Color getTextColor();
	
	/**
	 * Sets the flag of visibility.
	 * @param vis
	 */
	public void  setTickVisible(boolean vis);
	/**
	 * Gets the color for background.
	 * @return
	 */
	public Color getBackground();
	
	/**
	 * Returns the increment in device space.
	 * @return increment.
	 */
    public int getIncrement();
	
	/**
	 * Sets the display space for this ruler.
	 * @param length
	 */
	public void setLengthInPixels(int length);
	/**
	 * Sets the model size in device space.
	 * @param size the model size in device space.
	 */
	public void setDefaultSize(Dimension size);
	/**
	 * Returns the default size in one dimension. The other dimension is
	 * determinded by RulerView.
	 * @return The size in one dimension.
	 */
	public int getDefaultSize();
	
	/**
	 * Render the ruler ticks and labels.
	 * @param g2d Graphics2D
	 */
	public void render(Graphics2D g2d);
}
