//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.ruler;

import java.util.NoSuchElementException;


/**
 * 
 * Implements the TickDefinition with both major tick step value and minor tick step value 
 * 
 * @author li
 *
 */
public class MajorMinorTick implements TickDefinition {

	private double majorDataStep, minorDataStep;	// the major tick step and the minor tick step	
	private double dataStart, dataStop;				// the range of the ruler
	private double position;						// the current position of the tick (use in draw ruler)
	private int minorTickIndex;						// the current index of the minor tick
	private int majorTickIndex;		 				// the current index of the major tick
	private int minorMinIndex;						// the minimum index of the minor tick
	private int minorMaxIndex;						// the maxium index of the minor tick		 
	private int majorMinIndex;						// the minimum index of the major tick
	private int majorMaxIndex;						// the maxium index of the major tick
	private boolean majorTick = false;				// the sign of deciding weahter the current tick is major tick or not   
	
	/**
	 * 
	 * @param majorStep		the step of major tick
	 * @param minorStep		the step of minor tick
	 */
	public MajorMinorTick(double majorStep, double minorStep){
		majorDataStep = majorStep;
		minorDataStep = minorStep;
		dataStart = 0;
		dataStop = 0;
	}
	
	/**
	 * @param start			the first number of the ruler
	 * @param stop			the last number of the ruler 
	 * @param majorStep		the step of major tick
	 * @param minorStep		the step of minor tick
	 */
	
	public MajorMinorTick(double start, double stop,double majorStep, double minorStep){
		majorDataStep = start>stop ? -Math.abs(majorStep) : Math.abs(majorStep);
		minorDataStep = start>stop ? -Math.abs(minorStep) : Math.abs(minorStep);
		dataStart = start;
		dataStop = stop;
	}
	
	/**
	 * To tell you weather the index comes to the last or not.
	 * 
	 * @return If the index comes to the last, the value will be false. Otherwise it will be true.
	 */
	
	public boolean moreTicks() {
		return (minorTickIndex<=minorMaxIndex)&&(majorTickIndex<=majorTickIndex);
	}
	
	/**
	 * 
	 * Get the data about the current tick.
	 * 
	 * @return The data about the current tick.
	 * 
	 */

	public Object getTickData() {
		if(dataStart>dataStop){
			//int a= 5;			
		}
		if (moreTicks()) {
			if( minorTickIndex * Math.abs(minorDataStep) > majorTickIndex * Math.abs(majorDataStep)){
				position = majorTickIndex * majorDataStep;
				Double obj = new Double( majorTickIndex * majorDataStep + dataStart);
				majorTickIndex++;
				majorTick = false;
				return obj;
			}
			else if( minorTickIndex * Math.abs(minorDataStep) == majorTickIndex * Math.abs(majorDataStep)){
				position = majorTickIndex * majorDataStep;
				Double obj = new Double( majorTickIndex * majorDataStep + dataStart);
				majorTickIndex++;
				minorTickIndex++;
				majorTick = true;
				return obj;
			}
			else{
				position = minorTickIndex * minorDataStep;
				Double obj = new Double( minorTickIndex * minorDataStep + dataStart);
				minorTickIndex++;
				majorTick = false;
				return obj;
			}
//			position = tickIndex * drange.getStepValue();
//			Double obj = new Double(tickIndex * drange.getStepValue() + drange.getStartValue());
//			//go to next tick
//			tickIndex++;
//			return obj;
		}
		else {
			System.out.println( "" + majorMaxIndex);
			System.out.println( "" + minorMaxIndex);
			System.out.println( "" + majorTickIndex);
			System.out.println( "" + minorTickIndex);
			throw new NoSuchElementException();
		}

	}
	
	/**
	 * 
	 * Get the current position of tick.
	 * 
	 *  @return the current position of tick
	 * 
	 */

	public double getTickPosition() {
		return Math.abs(position);
	}

	/**
	 * 
	 * Get the range of the ruler.
	 * 
	 * @return range of the ruler
	 * 
	 */
	
	
	public DataRange getModelRange() {
		return new DataRange(dataStart,dataStop,minorDataStep);
	}
	
	/**
	 * 
	 * Set the range of the ruler
	 * 
	 * @param start		the first number of the ruler
	 * 
	 * @param stop		the last number of the ruler
	 */

	public void setModelRange(double start, double stop) {
		dataStart = start;
		dataStop = stop;
	}
	
	/**
	 *  (non-Javadoc)
	 * @see com.gwsys.gw2d.ruler.TickDefinition#setVisibleRange(double, double, double)
	 */

	public void setVisibleRange(double minV, double maxV, double scale) {
		if (dataStart==dataStop||Math.abs(minorDataStep)>=Math.abs(majorDataStep))
			throw new IllegalArgumentException();
/*		
	    int minorStartIndex = (int)Math.abs(Math.ceil( (dataStart-dataStart)/minorDataStep )); 
	    int minorStopIndex = (int)Math.abs(Math.floor( (dataStop-dataStart)/minorDataStep )); 
	    minorMinIndex  = (int)Math.floor(minV/scale/Math.abs(minorDataStep));
		minorMaxIndex  = (int)Math.ceil(maxV/scale/Math.abs(minorDataStep));
	    if( minorMinIndex < minorStartIndex ) 
			minorMinIndex = minorStartIndex;
	    if( minorMaxIndex > minorStopIndex )  
			minorMaxIndex = minorStopIndex;
*/
		minorMinIndex = (int)Math.abs(Math.ceil( (dataStart-dataStart)/minorDataStep )); 
		minorMaxIndex =	(int)Math.abs(Math.floor( (dataStop-dataStart)/minorDataStep ));
			
		minorTickIndex = minorMinIndex;

/*		
		int majorStartIndex = (int)Math.abs(Math.ceil( (dataStart-dataStart)/majorDataStep )); 
	    int majorStopIndex = (int)Math.abs(Math.floor( (dataStop-dataStart)/majorDataStep )); 
	    majorMinIndex  = (int)Math.floor(minV/scale/Math.abs(majorDataStep));
		majorMaxIndex  = (int)Math.ceil(maxV/scale/Math.abs(majorDataStep));
	    if( majorMinIndex < majorStartIndex ) 
			majorMinIndex = majorStartIndex;
	    if( majorMaxIndex > majorStopIndex )  
			majorMaxIndex = majorStopIndex;
*/
		majorMinIndex = (int)Math.abs(Math.ceil( (dataStart-dataStart)/majorDataStep ));
		majorMaxIndex = (int)Math.abs(Math.floor( (dataStop-dataStart)/majorDataStep )); 
		
		majorTickIndex = majorMinIndex;
	}
	
	/**
	 * 
	 * Judge the current tick .
	 * 
	 * @return If the current tick is major, it will be true. Otherwise it will be false. 
	 */
	public boolean isMajorTick(){
		return majorTick;
	}

}
