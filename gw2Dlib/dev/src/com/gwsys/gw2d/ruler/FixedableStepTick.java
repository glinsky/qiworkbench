//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.ruler;

import java.util.NoSuchElementException;

/**
 * Implements the TickDefinition with fixed step value in user space.
 * 
 * @author Hua
 *
 */
public class FixedableStepTick implements TickDefinition {

	/**
	 * Data range in user data space.
	 */
	private DataRange drange;  
	private double position;
	private int tickIndex, maxIndex, minIndex;
	
	/**
	 * Defines the tick in model space woth fixed step value.
	 * @param start Start value in model space.
	 * @param stop End value in model space.
	 * @param step Fixed step value.
	 */
	public FixedableStepTick(double start, double stop, double step) {
		drange = new DataRange(start, stop, step);
	}
	
	/**
	 * @see com.gwsys.gw2d.ruler.TickDefinition#moreTicks()
	 */
	public boolean moreTicks() {
		return tickIndex <= maxIndex;
	}

	/**
	 * @see com.gwsys.gw2d.ruler.TickDefinition#getTickData()
	 */
	public Object getTickData() {
		if (moreTicks()) {
			
			position = tickIndex * drange.getStepValue();
			Double obj = new Double(tickIndex * drange.getStepValue() + drange.getStartValue());
			//go to next tick
			tickIndex++;
			return obj;
		} else {
			throw new NoSuchElementException();
		}
	}
	
	/**
	 * Returns the tick value in model space.
	 */
	public double getTickPosition(){
		return Math.abs(position);
	}

	/**
	 * The scale is from model to device.
	 * @param scale "scale = device/model"
	 * @param minV start value in screen space.
	 * @param maxV stop value in screen space.
	 */
	public void setVisibleRange(double minV, double maxV, double scale ){
		if (drange == null)
			throw new IllegalArgumentException();
		//modelScale = scale;
	    int startIndex = drange.getStartIndex(drange.getStepValue(), true);
	    int stopIndex = drange.getStopIndex(drange.getStepValue(), false);

	    minIndex  = (int)Math.floor(minV/scale/Math.abs(drange.getStepValue()));
		maxIndex  = (int)Math.ceil(maxV/scale/Math.abs(drange.getStepValue()));

	    if( minIndex < startIndex ) 
			minIndex = startIndex;
	    if( maxIndex > stopIndex )  
			maxIndex = stopIndex;

		tickIndex = minIndex;

	}
	
	/**
	 * @inheritDoc
	 */
	public DataRange getModelRange(){
		return drange;
	}
	
	/**
	 * @inheritDoc
	 */
	public void setModelRange(double start, double stop){
		drange.setStart(start);
		drange.setStop(stop);
	}
}
