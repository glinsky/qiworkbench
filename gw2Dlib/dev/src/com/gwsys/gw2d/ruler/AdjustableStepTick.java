//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.gw2d.ruler;

/**
 * Implements the adjustable step behavior of TickDefinition.
 * The gap between twp ticks in screen space is required. The adjusted value is
 * based on this gap value and rounded up to closest value.
 * @author Hua.
 *
 */
public class AdjustableStepTick implements TickDefinition {

	private int minGap = 1;
	/**
	 * Data range in user data space.
	 */
	private DataRange drange;  
	private double modelScale = 1, position;
	private int tickIndex, maxIndex, minIndex;
	/**
	 * @param gap The minimum value between two ticks in device space.
	 * @param start The start value associated with this tick.
	 * @param stop The stop value associated with this tick.
	 */
	public AdjustableStepTick(double start, double stop, int gap) {
		minGap = gap;
		drange = new DataRange(start, stop);
	}

	/**
	 * @see com.gwsys.gw2d.ruler.TickDefinition#moreTicks()
	 */
	public boolean moreTicks() {
		return tickIndex <= maxIndex;
	}

	/**
	 * @return Object
	 */
	public Object getTickData() {
		if (moreTicks()) {			
			position = tickIndex * drange.getStepValue();
			Double obj = new Double(tickIndex * drange.getStepValue() + drange.getStartValue());
			//go to next tick
			tickIndex++;
			return obj;
		} else{
			return new Double(tickIndex * drange.getStepValue() + drange.getStartValue());			
		}
	}

	/**
	 * Returns the tick position in model space.
	 * @return abstract value.
	 */
	public double getTickPosition() {
		return Math.abs(position);
	}

	/**
	 * @see com.gwsys.gw2d.ruler.TickDefinition#getModelRange()
	 */
	public DataRange getModelRange() {
		return drange;
	}

	/**
	 * @see com.gwsys.gw2d.ruler.TickDefinition#setModelRange(double, double)
	 */
	public void setModelRange(double start, double stop) {
		drange.setStart(start);
		drange.setStop(stop);
	}

	/**
	 * @param scale The scale factor between model space and screen space.
	 */
	public void setVisibleRange(double minV, double maxV, double scale ){
		if (drange == null)
			throw new IllegalArgumentException("There is no Model Range!");
		modelScale = scale;

		//calculate the fittable model step		
		double modelStep = Math.abs( minGap / modelScale );
		//round it up to times of ten
		double timeten = Math.floor( Math.log( modelStep ) / Math.log(10));
	    double powerten = Math.pow( 10.0,  timeten);
	    int    diff = (int) Math.ceil( modelStep / powerten );

		drange.setStep( diff * powerten );
		//////////////////////////////////
		int startIndex = drange.getStartIndex(drange.getStepValue(), true);
	    int stopIndex = drange.getStopIndex(drange.getStepValue(), false);

	    minIndex  = (int)Math.floor(minV/scale/Math.abs(drange.getStepValue()));
		maxIndex  = (int)Math.ceil(maxV/scale/Math.abs(drange.getStepValue()));

	    if( minIndex < startIndex ) 
			minIndex = startIndex;
	    if( maxIndex > stopIndex )  
			maxIndex = stopIndex;

		tickIndex = minIndex;
	}

}
