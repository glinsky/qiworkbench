//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.ruler;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.text.NumberFormat;

/**
 * This model is associated with a data range including start and end values.
 * After the setLengthInPixels() method is called, the starting and ending
 * values are mapped to the values in device space.
 * @author Hua
 *
 */
public class RangeRulerModel extends AbstractRulerModel {

	private int orientation = 0, longTick = 10, shortTick = 7;
	private TickDefinition tickDef;
	private String title = "XY", unit = "";

	private int increment=25, maxViewSize;

	private NumberFormat dformat=NumberFormat.getNumberInstance();
	private double rulerScale = 1;
	/**
	 * Constructor with adjustable tick step model.
	 * @param mode RulerModel HORIZONTAL/VERTICAL.
	 * @param range TickDefinition.
	 */
	public RangeRulerModel(int mode, TickDefinition range){
		if (mode!=HORIZONTAL&&mode!=VERTICAL)
			throw new IllegalArgumentException(
					"Mode is supported only HORIZONTAL/VERTICAL now!");
		orientation = mode;
		tickDef = range;
		dformat.setGroupingUsed(false);
	}


	/**
	 * @see RulerModel#getStart()
	 */
	public double getStart() {
		return tickDef.getModelRange().getStartValue();
	}

	/**
	 * @see RulerModel#getEnd()
	 */
	public double getEnd() {
		return tickDef.getModelRange().getStopValue();
	}

	/**
	 * @see RulerModel#getOrientation()
	 */
	public int getOrientation() {
		return orientation;
	}

	/**
	 * @see RulerModel#getTitle()
	 */
	public String getTitle() {
		return title;
	}

	public int getMajorTickLength() {
		return longTick;
	}
	public int getMinorTickLength() {
		return shortTick;
	}

	public String getUnitSymbol(){
		return unit;
	}

	/**
	 * Returns the increment in device space.
	 * @return the increment in device space.
	 */
    public int getIncrement(){
		return increment;
    }

    /**
     * Check if drawing label on this tick.
     */
	public boolean isMajorTick(){
		if (tickDef instanceof MajorMinorTick)
			return ((MajorMinorTick)tickDef).isMajorTick();
		return true;
	}

	/**
	 * Sets the model size in device space.
	 */
	public void setLengthInPixels(int length){
		maxViewSize = length;
		rulerScale = Math.abs(maxViewSize /tickDef.getModelRange().getDifference());

		if (!tickDef.getModelRange().isFixedStep()){
			tickDef.setVisibleRange(0, maxViewSize, rulerScale);

		}
		increment = (int)Math.abs(Math.ceil(tickDef.getModelRange().getStepValue()*rulerScale));

	}

	protected void drawTitle(Graphics2D g2d){

	}

	protected void drawBaseLine(Graphics2D g2d){
		g2d.setColor(getTextColor());
		if (getOrientation() == RulerModel.HORIZONTAL) {
            g2d.drawLine(0, getDefaultSize()-1, maxViewSize, getDefaultSize()-1);
        } else {
			g2d.drawLine(getDefaultSize()-1, 0, getDefaultSize()-1, maxViewSize);
        }
	}

	protected void drawTickLabels(Graphics2D g2d){
        // Some vars we need.
        int end = 0;
        //int start = 0;
        int tickLength = 0;
		Rectangle drawHere = g2d.getClipBounds();

        // Use clipping bounds to calculate first and last tick locations.
        if (getOrientation() == RulerModel.HORIZONTAL) {
			if (increment != 0){
				//start = (drawHere.x / increment) * increment;
				end = (((drawHere.x + drawHere.width) / increment) + 1)*increment;
			}else{
				//start = (drawHere.x);
				end = (drawHere.x + drawHere.width);
			}
			tickDef.setVisibleRange(drawHere.getMinX(), drawHere.getMaxX(), rulerScale);
        } else {
			if (increment != 0){
				//start = (drawHere.y / increment) * increment;
	            end = (((drawHere.y + drawHere.height) / increment) + 1)* increment;
			}else{
				//start = (drawHere.y);
				end = (drawHere.y + drawHere.height);
			}

			tickDef.setVisibleRange(drawHere.getMinY(), drawHere.getMaxY(), rulerScale);
        }

		// Do the ruler labels in a small font that's black.
        g2d.setFont(getLabelFont());
        g2d.setColor(getTextColor());

		if(increment/rulerScale>1)
			dformat.setMaximumFractionDigits(0);
		else if(increment/rulerScale>0.01)
			dformat.setMaximumFractionDigits(2);
		else
			dformat.setMaximumFractionDigits(4);
		int i = 0;
        // ticks and labels
        String text = dformat.format(tickDef.getModelRange().getStartValue());
		int textW = g2d.getFontMetrics( this.getLabelFont() ).stringWidth(text);
		int textH = g2d.getFontMetrics( this.getLabelFont() ).getHeight();
		//draw first tick
		tickLength = getMajorTickLength();

		while (tickDef.moreTicks())	{
			text = dformat.format(tickDef.getTickData());
			i = (int)(tickDef.getTickPosition()*rulerScale);
            if (isMajorTick())  {
                tickLength = this.getMajorTickLength();
                //text = this.getTickLabel();
            } else {
                tickLength = this.getMinorTickLength();
                text = null;
            }

            if (tickLength != 0) {
                if (this.getOrientation() == RulerModel.HORIZONTAL) {
                    g2d.drawLine(i, getDefaultSize()-1, i,
							getDefaultSize()-tickLength);
                    if (text != null)
                        g2d.drawString(text, i, getDefaultSize()-tickLength);
                } else {
                    g2d.drawLine(getDefaultSize()-1, i,
							getDefaultSize()-tickLength, i);
                    if (text != null){
						textW = g2d.getFontMetrics( this.getLabelFont() ).stringWidth(text);
						if (i<10)
							g2d.drawString(text, getDefaultSize()-tickLength-textW, i+textH);
						else
			            g2d.drawString(text, getDefaultSize()-tickLength-textW, i+textH/2);
                    }
                }
            }

        }
		//draw last label
		if (end == maxViewSize) {
			text = dformat.format(tickDef.getTickData());
			int sw = g2d.getFontMetrics( this.getLabelFont() ).stringWidth(text);
            tickLength = getMajorTickLength();
            if (getOrientation() == RulerModel.HORIZONTAL) {
                g2d.drawLine(maxViewSize-1, getDefaultSize()-1, maxViewSize-1, getDefaultSize()-tickLength);
                g2d.drawString(text, maxViewSize - sw, getDefaultSize()-tickLength);
            } else {
                g2d.drawLine(getDefaultSize()-1, maxViewSize-1, getDefaultSize()-tickLength, maxViewSize-1);
                g2d.drawString(text, getDefaultSize()-tickLength-sw, maxViewSize - 2);
            }
            text = null;
        }
	}
}
