//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.gw2d.ruler;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Vector;

/**
 * Defines the abstract ruler model to implements common api of RulerModel.
 * @author Hua
 *
 */
public abstract class AbstractRulerModel implements RulerModel{

	private Color background = Color.white;//new Color(230, 163, 4);
	private Color foreground = Color.black;
	private Vector<RulerModelListener> listeners = new Vector<RulerModelListener>();
	private Font textFont = new Font("SansSerif", Font.PLAIN, 10);
	private Font titleFont = new Font("SansSerif", Font.PLAIN, 12);
	//private static Font textFont = new Font("SansSerif", Font.ITALIC, 12) ;
	private int defaultSize = 35;

	private boolean tickVisible = true;


	/**
	 * @inheritDoc
	 */
	public void addListener(RulerModelListener listener) {
		listeners.add(listener);

	}

	/**
	 * @inheritDoc
	 */
	public void removeListener(RulerModelListener listener) {
		listeners.removeElement(listener);

	}


	/**
	 * @inheritDoc
	 */
	public Font getLabelFont() {
		return textFont;
	}

	/**
	 * @inheritDoc
	 */
	public Font getTitleFont() {
		return titleFont;
	}
	/**
	 * @inheritDoc
	 */
	public Color getTextColor(){
		return foreground;
	}
	/**
	 * @inheritDoc
	 */
	public Color getBackground(){
		return background;
	}
	/**
	 * @inheritDoc
	 */
	public void render(Graphics2D g2d){
		Rectangle drawHere = g2d.getClipBounds();

        // Fill clipping area with dirty brown/orange.
        g2d.setColor(getBackground());
        g2d.fillRect(drawHere.x, drawHere.y, drawHere.width, drawHere.height);

		drawTitle(g2d);
		drawBaseLine(g2d);
		if (tickVisible)
			drawTickLabels(g2d);
	}

	protected abstract void drawTitle(Graphics2D g2d);

	protected abstract void drawBaseLine(Graphics2D g2d);

	protected abstract void drawTickLabels(Graphics2D g2d);

	/**
	 * @inheritDoc
	 */
	public void setDefaultSize(Dimension size){
		if (this.getOrientation()==HORIZONTAL){
			defaultSize = size.height;
			setLengthInPixels(size.width);
		}else{
			defaultSize = size.width;
			setLengthInPixels(size.height);
		}
	}
	/**
	 * Returns the default size in one dimension. The other dimension is
	 * determinded by RulerView.
	 * @return The size in one dimension.
	 */
	public int getDefaultSize(){
		return defaultSize;
	}

	/**
	 * @inheritDoc
	 */
	public void  setTickVisible(boolean vis){
		tickVisible = vis;
	}
}
