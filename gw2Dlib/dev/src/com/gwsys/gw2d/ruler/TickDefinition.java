//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.ruler;

/**
 * Defines the interface for tick objects in user space.
 * It will be used for both RulerModel and GridLine.
 * 
 * @author Hua
 *
 */
public interface TickDefinition {

	/**
	 * Returns the status of tick store. If <code>true</code>,
	 * user retrives the tick object by method getTickData().
	 * @return the status of tick store.
	 */
	public boolean moreTicks();
	
	/**
	 * Retrives the object associated with current tick.
	 * @return
	 */
	public Object getTickData();
	
	/**
	 * Retrives the current tick position.
	 * @return
	 */
	public double getTickPosition();
	
	/**
	 * Gets the DataRange associated with this ruler.
	 * @return
	 */
	public DataRange getModelRange();
	
	/**
	 * Sets the data range for this ruler.
	 * @param start
	 * @param stop
	 */
	public void setModelRange(double start, double stop);
	
	/**
	 * Updates the visible range.
	 * @param minV Start value in device space.
	 * @param maxV
	 * @param scale
	 */
	public void setVisibleRange(double minV, double maxV, double scale );
}
