//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.gw2d.ruler;

/**
 * This utility class defines the start, end and step value.
 * 
 * @author Hua
 *
 */
public class DataRange {

	private double start, step=1, stop;
	private boolean fixedStep = false;
	/**
	 * 
	 */
	public DataRange(double start, double stop) {
		this.start = start;
		this.stop = stop;
		fixedStep = false;
	}
	
	/**
	 * 
	 */
	public DataRange(double start, double stop, double step) {
		this.start = start;
		this.stop = stop;		
		this.step = start > stop ? -Math.abs(step) : Math.abs(step);
		fixedStep = true;
	}
	
	public double getStartValue(){
		return start;
	}
	
	public double getStopValue(){
		return stop;
	}
	
	public double getStepValue(){
		return step;
	}
	
	public boolean isBackward(){
		return start > stop;
	}
	
	public boolean isFixedStep(){
		return fixedStep;
	}
	
	public double getDifference(){
		return stop-start;
	}
	
	public void setStep(double step){
		if (isBackward())
			this.step = -Math.abs(step);
		else
			this.step = Math.abs(step);
	}
	public void setStart(double start){
		this.start = start;
	}
	public void setStop(double stop){
		this.stop = stop;
	}
	public int getStartIndex(double step, boolean ceil){
		if (ceil)
			return (int)Math.abs(Math.ceil( (start-start)/step ));
		return (int)Math.abs(Math.floor( (start-start)/step ));
	}
	
	public int getStopIndex(double step, boolean ceil){
		if (ceil)
			return (int)Math.abs(Math.ceil( (stop-start)/step ));
		return (int)Math.abs(Math.floor( (stop-start)/step ));
	}

}
