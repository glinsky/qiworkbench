//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.ruler;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JComponent;

/**
 * RulerView supports multiple RulerModels to draw Ruler.
 * There are more ruler models defined by different RulerModels.
 * User must call setPreferredWidth/Height() in order to display correctly.
 * 
 * @author adam.
 */

public class RulerView extends JComponent {
    
    /**
	 * ID for this class.
	 */
	private static final long serialVersionUID = 3257289132127825969L;
	
	private RulerModel model;
    

	/**
	 * Constructor.
	 * @param model RulerModel
	 */
    public RulerView(RulerModel model) {
        this.model = model;
    }

	/**
	 * Returns the increment in device space.
	 * @return the increment in device space.
	 */
    public int getIncrement() {
        return model.getIncrement();
    }

	/**
	 * Sets the height of size in screen space.
	 * @param ph the height of size in screen space.
	 */
    public void setPreferredHeight(int ph) {
		model.setLengthInPixels(ph);
        setPreferredSize(new Dimension(model.getDefaultSize(), ph));
    }
	
	/**
	 * Sets the width of size in screen space.
	 * @param pw the width of size in screen space.
	 */
    public void setPreferredWidth(int pw) {
		model.setLengthInPixels(pw);
        setPreferredSize(new Dimension(pw, model.getDefaultSize()));
    }

	/**
	 * Overrids this method in order to update the model.
	 */
	public void setPreferredSize(Dimension size){
		model.setDefaultSize(size);		
		super.setPreferredSize(size);
		repaint();
	}
	
	/**
	 * Overrids this method in order to do customized rendering.
	 */
    protected void paintComponent(Graphics g) {
        
		Graphics2D g2d = (Graphics2D)g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
				RenderingHints.VALUE_ANTIALIAS_ON);

		model.render(g2d);
		
    }
    
    /**
     * Resets the RulerModel.
     * @param rulerModel RulerModel.
     */
    public void setModel(RulerModel rulerModel){
    	model = rulerModel;
    }
}
