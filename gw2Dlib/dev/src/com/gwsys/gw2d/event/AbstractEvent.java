//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.gw2d.event;

import java.util.EventObject;

/**
 * Defines Abstract Event used in this project.
 * It extends from java.util.EventObject.
 * It contains "eventMessage" and "eventID" properties.
 */
public abstract class AbstractEvent extends EventObject {

 	private int eventID;

	private Object eventMessage;


	/**
	 * Constructs event with given parameters.
	 * @param source	the event source
	 * @param message	the event message
	 * @param eventid		the event ID
	 */
	public AbstractEvent(Object source, Object message, int eventid) {
		super(source);
		eventMessage = message;
		eventID = eventid;
	}

	/**
	 * returns the message object associated with this event.
	 *
	 * @return the message object associated with this event.
	 */
	public Object getEventMessage() {
		return eventMessage;
	}

	/**
	 * Gets the event ID.
	 * @return The values are defined in special class.
	 */
	public int getEventID() {
		return eventID;
	}

}
