//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.gw2d.event;

import com.gwsys.gw2d.event.AbstractEvent;

/**
 * Defines the event related to model changes.
 */
public class ModelChangeEvent extends AbstractEvent{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3545516196909626934L;
	
	/** 
	 * Data Model is changed.
	 */
	public final static int MODEL_CHANGED = 10;
    

	/** 
	 * Layer is inserted.
	 */
	public final static int LAYER_INSERTED = 11;
	
	/** 
	 * Layer is removed.
	 */
	public final static int LAYER_REMOVED = 12;
	
	/**
	 * Constructs event with given parameters.
	 * @param source	the event source
	 * @param message	the event message
	 * @param id		the event ID
	 */
	public ModelChangeEvent(Object source, Object message, int id) {
		super(source, message, id);
	}
}