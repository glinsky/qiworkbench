//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.gw2d.event;

import com.gwsys.gw2d.event.AbstractEvent;

/**
 * Defines the event object related to view changes.
 */
 public class ViewChangeEvent extends AbstractEvent{

	/**
	 * serial id.
	 */
	private static final long serialVersionUID = 3257844368286299449L;

	/** event type.
	 */
	public final static int VIEW_CHANGED = 0;

	/**
	* Constructs new event object with parameters.
	* @param source source of event.
	* @param paras parameters of event.
	* @param eid ID of event
	*/
	public ViewChangeEvent(Object source, Object paras, int eid) {
		super(source, paras, eid);
	}
}