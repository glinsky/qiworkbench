//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.OverlayLayout;

import com.gwsys.gw2d.event.ViewChangeEvent;
import com.gwsys.gw2d.event.ViewChangeListener;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.GWUtilities;
import com.gwsys.gw2d.util.Transform2D;

/**
 * This container supports multiple layered visuable components.
 * It uses the javax.swing.OverlayLayout to manage components over
 * the top of each other.</p>
 * It implements the MouseListener, MouseMotionListener, ViewChangeListener,
 * and ComponentListener in order to delegate all events to its child componnets.
 *
 * By default, there is no cache available. When enableCache(true) is called,
 * the container will make a cache for visible area. This feature can improve the
 * rendering performance in multi-views (many MultipleViewContainers) application.
 *
 * @author Hua, Li
 */
public class MultipleViewContainer extends VisuableComponent implements
		MouseListener, MouseMotionListener,
		ViewChangeListener, ComponentListener {

	/**
	 * UID
	 */
	private static final long serialVersionUID = 3689347732376663863L;

	/**
	 * The flag for redrawing components.
	 */
	private boolean _enableCache = false;

	/**
	 *The current model limits in virtual (device) space.
	 */
	protected Dimension _limits;

	/**
	 * Image for cache purpose
	 */
	private Image cacheImage = null;

	/**
	 * store the view rectangle in the cache
	 */
	private Rectangle cacheRect = null;
	/**
	 * Stores transformation.
	 */
	private Transform2D transform2d;

	/**
	 * List of views.
	 */
	private List<VisuableComponent> viewList = new ArrayList<VisuableComponent>();

	/**
	 * cache size = visiblesize * cacheFactor
	 */
	//private static final int cacheFactor = 2;

	/**
	 * Contructor.
	 */
	public MultipleViewContainer() {
		super();
		setLimits(0, 0);
		putClientProperty("EnableWindowBlit", Boolean.TRUE);
		setLayout(new OverlayLayout(this));
		setBorder(BorderFactory.createEmptyBorder());
	}

	/**
	 * Adds the component to this container at the given index.
	 * @inheritDoc
	 */
	protected void addImpl(Component comp, Object constraints, int index) {

		if (comp instanceof VisuableComponent) {
			VisuableComponent view = (VisuableComponent)comp;
			view.setOpaque(false);
			view.setSize(getSize());
			view.addComponentListener(this);
			view.addViewEventListener(this);
			Dimension size = view.getActualSize();
			boolean change = false;
			change = size.width > getActualSize().width;
			size.width = Math.max(size.width, getActualSize().width);
			change = change && size.height > getActualSize().height;
			size.height = Math.max(size.height, getActualSize().height);
			if (change) {
				setLimits(size.width, size.height);
			}
			view.setDirtyMark(true);
			super.addImpl(view, constraints, index);
			if (index<0)
				viewList.add(view);
			else
				viewList.add(index, view);
			super.setDirtyMark(true);
		} else
			throw new IllegalArgumentException(
					"This component cannot be added into MultipleViewContainer");

	}
	/**
	 * Invoked when the component has been made invisible.
	 * @param e		the componentevent
	 */
	public void componentHidden(ComponentEvent e) {
	}

	/**
	 * Invoked when the component's position changes.
	 * @param e		the component event
	 */
	public void componentMoved(ComponentEvent e) {
	}

	/**
	 * Invoked when the component's size changes.
	 *
	 * @param e		the component event
	 */
	public void componentResized(ComponentEvent e) {
		if (e.getComponent()==null)
			return;
		Dimension size = e.getComponent().getSize();

		int maxWidth = 0;
		int maxHeight = 0;
		JComponent jcom;
		for (int i = 0; i < viewList.size(); i++) {
			jcom = (JComponent) viewList.get(i);
			if (viewList.get(i) instanceof VisuableComponent) {
				jcom.setSize(size.width, size.height);
				maxWidth = Math.max(maxWidth, ((VisuableComponent)jcom).getActualSize().width);
				maxHeight = Math.max(maxHeight, ((VisuableComponent)jcom).getActualSize().height);
			} else {
				maxWidth = Math.max(maxWidth, jcom.getSize().width);
				maxHeight = Math.max(maxHeight, jcom.getSize().height);
			}
		}

		size.width = Math.max(size.width, maxWidth);
		size.height = Math.max(size.height, maxHeight);

		if (_limits.width != size.width && _limits.height != size.height) {
			setLimits(size.width, size.height);
		}

		ViewChangeEvent event = new ViewChangeEvent(
				this, new Bound2D(0, 0, getActualSize().width,
						getActualSize().height), ViewChangeEvent.VIEW_CHANGED);
		notifyViewListeners(event);
	}


	/**
	 * Invoked when the component has been made visible.
	 * @param e		the component event
	 */
	public void componentShown(ComponentEvent e) {
	}

	/**
	 * Creates new cache and paints context.
	 *
	 */
	protected void createCache(){
		if (!_enableCache)
			return;
		cacheRect = getVisibleRect();
		if (cacheImage==null||cacheRect.width!=cacheImage.getWidth(null)||
				cacheRect.height!=cacheImage.getHeight(null))
			cacheImage = createImage(cacheRect.width, cacheRect.height);
		Graphics2D gg = (Graphics2D)cacheImage.getGraphics();
		AffineTransform tr = AffineTransform.getTranslateInstance(
				-cacheRect.x,-cacheRect.y);
		gg.setTransform(tr);
		paintAlls(gg);

	}

	/**
	 * Enables the cache feature or not for this container.
	 * @param enable <i>true</i> Enables the cache feature.
	 */
	public void enableCache(boolean enable){
		_enableCache = enable;
	}

	/**
	 * Finds the current size in device space.
	 *
	 * @return the <code>Dimension</code> object.
	 */
	public Dimension getActualSize() {
		return new Dimension(_limits);
	}

	/**
	 * Gets the number of views in the container.
	 *
	 * @return the number of views in the container.
	 */
	public int getNumberOfViews() {
		return viewList.size();
	}

	/**
	 * Returns the size of scrollable part of component.
	 *
	 * @return the size of scrollable part of component
	 */
	public Dimension getScrollableAmount() {

		JComponent comp;
		Dimension size;
		Dimension maxsize = new Dimension(0, 0);

		for (int i = 0; i < viewList.size(); i++) {
			comp = (JComponent) viewList.get(i);

			size = comp.getSize();

			if (size.width > maxsize.width)
				maxsize.width = size.width;
			if (size.height > maxsize.height)
				maxsize.height = size.height;
		}

		return maxsize;
	}

	/**
	 * Retrieves the model transformation.
	 *
	 * @return the <code>Transform2D</code> object
	 */
	public Transform2D getTransformation() {
		if (transform2d == null) {
			transform2d = new Transform2D();
		}
		return transform2d;
	}


	/**
	 * Retrieves the component at the given index of container.
	 *
	 * @param index	the index for the component to be retrieved.
	 *
	 * @return JComponent at the given index of container.
	 */
	public JComponent getView(int index) {
		if (index >= 0 && index < viewList.size()) {
			return (JComponent) (viewList.get(index));
		}
		return null;
	}

	/**
	 * Gets  a list of views in this container.
	 * @return a list of views
	 */
	public List getViewList(){
		return viewList;
	}

	/**
	 * Returns the visible amount of scrollable part of component.
	 *
	 * @return the visible amount of scrollable part of component
	 */
	public Dimension getVisibleAmount() {
		return getScrollableAmount();
	}

	/**
	 * Invalidate the whole view.
	 */
	public void invalidate() {
		super.setDirtyMark(true);
		notifyViewListeners();
	}

	/**
	 *
	 * @inheritDoc
	 */
	public void mouseClicked(MouseEvent e) {

		for (int i = viewList.size() - 1; i >= 0; i--) {
			if ( viewList.get(i) instanceof MouseListener) {
				((MouseListener) (viewList.get(i))).mouseClicked(e);
			}
		}
	}


	/**
	 *
	 * @inheritDoc
	 */
	public void mouseDragged(MouseEvent e) {

		for (int i = viewList.size() - 1; i >= 0; i--) {
			if ( viewList.get(i) instanceof MouseListener) {
				((MouseMotionListener) (viewList.get(i))).mouseDragged(e);
			}
		}
	}

	/**
	 *
	 * @inheritDoc
	 */
	public void mouseEntered(MouseEvent e) {

		for (int i = viewList.size() - 1; i >= 0; i--) {
			if ( viewList.get(i) instanceof MouseListener) {
				((MouseListener) (viewList.get(i))).mouseEntered(e);
			}
		}
	}

	/**
	 *
	 * @inheritDoc
	 */
	public void mouseExited(MouseEvent e) {

		for (int i = viewList.size() - 1; i >= 0; i--) {
			if ( viewList.get(i) instanceof MouseListener) {
				((MouseListener) (viewList.get(i))).mouseExited(e);
			}
		}
	}

	/**
	 *
	 * @inheritDoc
	 */
	public void mouseMoved(MouseEvent e) {

		for (int i = viewList.size() - 1; i >= 0; i--) {
			if ( viewList.get(i) instanceof MouseListener) {
				((MouseMotionListener) (viewList.get(i))).mouseMoved(e);
			}
		}
	}

	/**
	 *
	 * @inheritDoc
	 */
	public void mousePressed(MouseEvent e) {

		for (int i = viewList.size() - 1; i >= 0; i--) {
			if ( viewList.get(i) instanceof MouseListener) {
				((MouseListener) (viewList.get(i))).mousePressed(e);
			}
		}
	}

	/**
	 *
	 * @inheritDoc
	 */
	public void mouseReleased(MouseEvent e) {

		for (int i = viewList.size() - 1; i >= 0; i--) {
			if ( viewList.get(i) instanceof MouseListener) {
				((MouseListener) (viewList.get(i))).mouseReleased(e);
			}
		}
	}

	/**
	 * Moves the specified component to the bottom of the container.
	 *
	 * @param   view    the view component to be moved
	 */
	public void moveViewToBottom(JComponent view) {

		if (viewList.indexOf(view) != -1) {
			remove(view);
			add(view, 0);

			//let above component transparent
			if (viewList.size() > 1) {
				JComponent jcom = (JComponent) viewList.get(1);
				jcom.setOpaque(false);

				if (jcom instanceof VisuableComponent)
					((VisuableComponent) jcom).setDirtyMark(true);
			}

			super.setDirtyMark(true);
		} else {
			throw new IllegalArgumentException(
					"This container does not contain the given component");
		}
	}

	/**
	 * Moves the specified component to the top of the container.
	 *
	 * @param   view    the view component to be moved.
	 */
	public void moveViewToTop(JComponent view) {

		if (viewList.indexOf(view) != -1) {
			remove(view);
			add(view);

			super.setDirtyMark(true);
		} else {
			throw new IllegalArgumentException(
					"This container does not contain the given component");
		}
	}

	/**
	 * Notifies the view listeners.
	 */
	public void notifyViewListeners() {
		Dimension size = getActualSize();
		Bound2D bound = new Bound2D(0, 0, size.width, size.height);
		notifyViewListeners(bound);
	}

	/**
	 * Notifies the view listeners with given bound.
	 *
	 * @param bound	the given bound in device space
	 */
	public void notifyViewListeners(Bound2D bound) {
		if (bound == null) {
			Dimension size = getActualSize();
			bound = new Bound2D(0, 0, size.width, size.height);
		}
		ViewChangeEvent event = new ViewChangeEvent(
				this, bound, ViewChangeEvent.VIEW_CHANGED);
		notifyViewListeners(event);
	}

	/**
	 * Paints all the components.
	 *
	 * @param g	Graphics
	 */
	private void paintAlls(Graphics g) {
		if (isOpaque()) {
			Rectangle clip = g.getClipBounds();
			g.setColor(getBackground());
			if (clip!=null)
				g.fillRect(clip.x, clip.y, clip.width, clip.height);
		}

		for (int i = 0; i < viewList.size(); i++) {

			JComponent pv = (JComponent) viewList.get(i);

			pv.setOpaque(i == 0 && isOpaque());

			pv.paint(g);
		}
	}

	/**
	 *
	 * @inheritDoc
	 */
	public void paintChildren(Graphics g) {

		if (viewList.size() > 0) {
			if (_enableCache){
				paintWithCache(g);
			}else
				paintAlls(g);
		}

		super.setDirtyMark(false);
	}

	/**
	 * Paint itself nothing.
	 *
	 * @param g	Graphics
	 */
	public void paintComponent(Graphics g) {

	}

	private void paintWithCache(Graphics g){
		//int w = (getPreferredSize().width<getVisibleRect().width*cacheFactor)?getPreferredSize().width:getVisibleRect().width*cacheFactor;
		//int h = (getPreferredSize().height<getVisibleRect().height*cacheFactor)?getPreferredSize().height:getVisibleRect().height*cacheFactor;
		//large cache has bug in position, use visibleRect now
		if(cacheRect == null){
			cacheRect = getVisibleRect();//new Rectangle(0,0,w,h);
			super.setDirtyMark(true);
		}
		else if (!cacheRect.equals(getVisibleRect()))
			super.setDirtyMark(true);
		/*
		else{
			if(!cacheRect.contains(getVisibleRect())){
				cacheRect = new Rectangle(
					(getVisibleRect().x - (w-getVisibleRect().width)/2>0)?(getVisibleRect().x - (w-getVisibleRect().width)/2):0,
					(getVisibleRect().y - (h-getVisibleRect().height)/2>0)?(getVisibleRect().y - (h-getVisibleRect().height)/2):0,
				    w,h);
				_dirtymark = true;
			}
		}*/

		if (isDirty()){
			createCache();
		}

		g.drawImage(cacheImage, cacheRect.x,cacheRect.y, null);

	}

	/**
	 * Removes the component in specified index.
	 *
	 * @param index the index of the component to be removed.
	 */
	public void remove(int index) {

		if (index < 0 || index >= viewList.size()) {
			return;
		}

		super.remove(index);

		Component comp = (Component) viewList.get(index);

		if (comp instanceof VisuableComponent) {
			VisuableComponent view=(VisuableComponent) comp;
			view.removeComponentListener(this);
			view.removeViewEventListener(this);
			viewList.remove(view);
			if (index == 0 && viewList.size() > 0) {
				if ( viewList.get(0) instanceof VisuableComponent) {
					((VisuableComponent) viewList.get(0)).setDirtyMark(true);
				}
			}
		}
		super.setDirtyMark(true);
	}

	/**
	 * Removes all the components from the stack.
	 */
	public void removeAll() {
		for (int i = viewList.size() - 1; i >= 0; i--) {
			remove(i);
		}
	}

	/**
	 * Rescales child views in model space.
	 *
	 * @param x       the x coordinate in modeling coordinate system
	 * @param y       the y coordinate in modeling coordinate system
	 * @param xScale  the x coordinate scale factor
	 * @param yScale  the y coordinate scale factor
	 */
	protected void scaleChildren(double x, double y, double xScale,
			double yScale) {
		for (int i = 0; i < viewList.size(); i++) {
			if (viewList.get(i) instanceof VisuableComponent)
				((VisuableComponent) viewList.get(i)).scaleTo(x, y, xScale,
						yScale);
		}
	}

	/**
	 * Calculates limits after scaling from limits of child views.
	 */
	protected void scaleLimits() {
		_limits.setSize(0, 0);

		for (int i = 0; i < viewList.size(); i++) {
			Dimension size = null;

			if (viewList.get(i) instanceof VisuableComponent) {
				size = ((VisuableComponent) viewList.get(i)).getActualSize();
			} else {
				size = ((JComponent) viewList.get(i)).getSize();
			}

			if (size.width > _limits.width)
				_limits.width = size.width;
			if (size.height > _limits.height)
				_limits.height = size.height;
		}
		super.setDirtyMark(true);
	}

	/**
	 * Scales the view from the specified point in user data space,
	 * maps this point to the center of view and redraws view.
	 *
	 * @param x       the x coordinate in modeling coordinate system
	 * @param y       the y coordinate in modeling coordinate system
	 * @param xScale  the x coordinate scale factor
	 * @param yScale  the y coordinate scale factor
	 */
	public void scaleTo(double x, double y, double xScale, double yScale) {

		scaleChildren(x, y, xScale, yScale);

		GWUtilities.scaleTransformation(xScale, yScale, this
				.getTransformation());

		scaleLimits();
	}

	/**
	 * Update this actual size of component and revalidate
	 * @param size this actual size
	 */
	public void setActualSize(Dimension size){
		_limits = size;
		super.setDirtyMark(true);
	}

	/**
	 * Sets the bounding box of component. <br>
	 * It forces to set all children at (0,0) location.
	 * @inheritDoc
	 */
	public void setBounds(int x, int y, int w, int h) {
		super.setBounds(x, y, w, h);

		for (int i = 0; i < viewList.size(); i++) {
			JComponent com = (JComponent) viewList.get(i);

			com.setOpaque(i == 0 && isOpaque());

			com.setBounds(0, 0, w, h);
		}

	}

	/**
	 * Defines the new limit of container in device space.
	 * This method notifies listeners.
	 *
	 * @param w			the new width of container.
	 * @param h			the new height of container.
	 */
	public void setLimits(int w, int h) {
		_limits = new Dimension(w, h);
		super.setDirtyMark(true);
		notifyChangeListeners();
		notifyViewListeners();
	}


	/**
	 * Defines the new modeling clip to draw.
	 *
	 * @param clip	Shape as clipping arew in modeling space.
	 */
	public void setModelClip(Shape clip) {
		for (int i = 0; i < viewList.size(); i++) {
			((VisuableComponent)(viewList.get(i))).setModelClip(clip);
		}
	}

	/**
	 * ScrollPane uses this size to control scrollbar.
	 */
	public void setPreferredSize(Dimension size){
		_limits = size;
		super.setDirtyMark(true);
	}

	/**
	 * Updates the Transform2D object to this container.
	 *
	 * @param transform		the new transformation object for this view.
	 */
	public void setTransformation(Transform2D transform) {
		setTransformation(transform, true);
	}

	/**
	 * Updates the <code>Transform2D</code> object to all containers.
	 * @param trans		the new transformation object.
	 * @param child	<i>true</i> passing it to subview.
	 */
	private void setTransformation(Transform2D trans, boolean child) {

		transform2d = trans;

		_limits.setSize(0, 0);
		boolean changed = false;
		Dimension eachsize = null;

		for (int i = 0; i < viewList.size(); i++) {
			if (viewList.get(i) instanceof VisuableComponent) {
				if (child) {
					double[] matrix = new double[9];
					trans.getMatrix(matrix);
					((VisuableComponent) (viewList.get(i)))
							.setTransformation(new Transform2D(matrix));
				}
				eachsize = ((VisuableComponent) (viewList.get(i))).getActualSize();

				if (eachsize.width > _limits.width || eachsize.height > _limits.height) {

					eachsize.width = Math.max(eachsize.width, _limits.width);
					eachsize.height = Math.max(eachsize.height, _limits.height);
					changed = true;
					_limits.setSize(eachsize.width, eachsize.height);
				}
			}
		}
		if (changed) { //notify listeners
			setLimits(eachsize.width, eachsize.height);
			setDirtyMark(true);
		}
		repaint();
	}

	/**
	 * Updates the <code>Transform2D</code> to this view only.
	 *
	 * @param transform    the new transformation object..
	 */
	public void setTransformationWithoutPropogation(Transform2D transform) {

		setTransformation(transform, false);
	}


	/**
	 * Handles view change events.
	 *
	 * @param event	ViewChangeEvent
	 */
	public void viewChanged(ViewChangeEvent event) {
		super.setDirtyMark(true);
		notifyViewListeners(event);
	}

	/**
	 * @inheritDoc
	 */
	public void setViewPosition(Point p){
		for (int i=0; i<viewList.size(); i++){
			viewList.get(i).setViewPosition(p);
		}
		super.setViewPosition(p);
	}
}
