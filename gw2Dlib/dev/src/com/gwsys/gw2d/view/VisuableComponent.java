//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.gw2d.view;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Shape;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.gwsys.gw2d.event.ViewChangeEvent;
import com.gwsys.gw2d.event.ViewChangeListener;
import com.gwsys.gw2d.util.Bound2D;

/**
 * The abstract class implements Visuable interface and 
 * extends javax.swing.JComponent.
 * 
 */
public abstract class VisuableComponent extends JComponent implements Visuable {

	//member variables
	private ArrayList<ChangeListener> changeListeners = new ArrayList<ChangeListener>();
	private boolean dirtyCache = true, notifyListeners;
	private ArrayList<ViewChangeListener> viewListeners = new ArrayList<ViewChangeListener>();
	private Point viewPosition= new Point(0, 0);

	/**
	 * Adds a change listener to this component.
	 * @param listener the change listener to be added.
	 */
	public void addChangeListener(ChangeListener listener) {
		changeListeners.add(listener);
	}
	
	/**
	 * Adds a new listener to this component.
	 * @param listener the view listener to be added.
	 */
	public void addViewEventListener(ViewChangeListener listener) {
		viewListeners.add(listener);
	}

	/**
	 * Gets the actual size of component.
	 *
	 * @return the actual size of component.
	 */
	public abstract Dimension getActualSize();
	/**
	 * @inheritDoc
	 */
	public Dimension getMaximumSize(){
		return getActualSize();
	}
	/**
	 * @inheritDoc
	 */
	public Dimension getMinimumSize(){
		return getActualSize();
	}

	/**
	 * Gets the current state of the notification flag.
	 *
	 * @return	The notification flag.
	 */
	public boolean getNotificationFlag() {
		return notifyListeners;
	}

	/**
	 * @inheritDoc
	 */
	public Dimension getPreferredSize(){
		Dimension vbounds = getActualSize();
		if (vbounds.width==0||vbounds.height==0)
			return super.getPreferredSize();
		return getActualSize();
	}
	
	/**
	 * Retrieves the view position of the component.
	 *
	 * @return the view position of component
	 */
	public Point getViewPosition() {
		return viewPosition;
	}
	
	/**
	 * Returns the visible size of component.
	 * 
	 * @return the visible size of component
	 */
	public Dimension getVisibleAmount() {
		return getSize();
	}
	
	/**
	 * Check if the cache is dirty.
	 * @return flag of cache.
	 */
	public boolean isDirty(){
		return dirtyCache;
	}
	
	/**
	 * Notifies the change listeners.
	 */
	public void notifyChangeListeners() {
		notifyChangeListeners(new ChangeEvent(this));
	}
	
	/**
	 * Notifies the change listeners with given event.
	 *
	 * @param e the change event
	 */
	public void notifyChangeListeners(ChangeEvent e) {

		for (int i = 0; i < changeListeners.size(); i++) {
			ChangeListener lis = (ChangeListener) changeListeners.get(i);
			lis.stateChanged(e);
		}
	}
	
	/**
	 * Notifies view listeners.
	 * @param event		the invalidating view event
	 */
	public void notifyViewListeners(ViewChangeEvent event) {
		if (viewListeners.size() > 0 && getNotificationFlag()) {
			for (int i = 0; i < viewListeners.size(); i++) {
				((ViewChangeListener) viewListeners.get(i)).viewChanged(event);
			}
		} else if (event.getEventMessage() instanceof Bound2D){
			//if there is no listener, repaint itself
			Bound2D bound = (Bound2D) event.getEventMessage();
			setActualSize(bound.getBounds().getSize());
			repaint((int)bound.getMinX(), (int)bound.getMinY(),
				(int)bound.getMaxX(), (int)bound.getMaxY());
		}
	}

	/**
	 * Removes a change listener from this component.
	 * @param listener the change listener to be removed.
	 */
	public void removeChangeListener(ChangeListener listener) {
		changeListeners.remove(listener);
	}
	

	/**
	 * Removes the specified listener.
	 *
	 * @param listener the view listener to be removed.
	 */
	public void removeViewEventListener(ViewChangeListener listener) {
		viewListeners.remove(listener);
	}

	/**
	 * Update the actual size and revalidate component.
	 * @param size the actual size
	 */
	public abstract void setActualSize(Dimension size);
	

	public void setDirtyMark(boolean dirty){
		this.dirtyCache = dirty;
	}

	/**
	 * Create a clip area with a shape.
	 * @param clip Shape.
	 */	
	public abstract void setModelClip(Shape clip);


	/**
	 * Changes the notification flag value.
	 *
	 * @param notify	The notification flag
	 */
	public void setNotificationFlag(boolean notify) {
		notifyListeners = notify;
	}
	
	/**
	 * Sets the view position for the component.
	 *
	 * @param  pos the view position of component
	 */
	public void setViewPosition(Point pos) {
		viewPosition = pos; //use this position to locate visible area
	}

}
