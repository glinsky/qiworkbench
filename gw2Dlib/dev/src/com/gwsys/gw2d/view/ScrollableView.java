//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.view;

import com.gwsys.gw2d.util.Transform2D;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Color;
import java.awt.Dimension;

import java.awt.event.AdjustmentListener;
import java.awt.event.AdjustmentEvent;

import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * This container contains nine visuable components in ScrollableLayout.
 * It contains VisuableComponent in the center.
 * @see javax.swing.JScrollPane.
 * @since Oct 11, 2006 Update scroll bar positions
 */
public class ScrollableView extends JScrollPane implements
		ChangeListener, AdjustmentListener {
	/**
	 * Serialization
	 */
	private static final long serialVersionUID = 3834586604273481008L;

	/**
	 * The component is placed at the center of view.
	 */
	private VisuableComponent centerView;

	/** List of change listeners for this component */
	private List<ChangeListener> changeListeners = new ArrayList<ChangeListener>();

	/**
	 * The component is placed at the left of the view.
	 *
	 */
	private VisuableComponent leftCom;


	/**
	 * The component is placed at the top of view.
	 *
	 */
	private VisuableComponent topCom;

	/**
	 * Creates empty view with scroll bar always.
	 */
	public ScrollableView() {
		this(null);
	}

	/**
	 * Creates view of VisuableComponent.
	 *
	 * @param view  VisuableComponent.
	 */
	public ScrollableView(VisuableComponent view) {

		super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		DataViewport viewport = new DataViewport();
		if (view!=null)
			viewport.setView(view);
		setViewport(viewport);
		setBorder(BorderFactory.createEmptyBorder());
		getVerticalScrollBar().addAdjustmentListener(this);
		getHorizontalScrollBar().addAdjustmentListener(this);
	}


	/**
	 * Adds a change listener to this component.
	 * @param listen the change listener to be added.
	 */
	public void addChangeListener(ChangeListener listen) {
		changeListeners.add(listen);
	}


	/**
	 * Receives adjustment events from scroll bars. Sends the event to
	 * change listener to adjusts the view position of component.
	 *
	 * @param e	the adjustment event
	 */
	public void adjustmentValueChanged(AdjustmentEvent e) {
		notifyChangeListeners();
	}

	/**
	 * Returns the visible amount of viewport after scrollbar is disappered.
	 *
	 * @return the visible amount of scrollable part of component
	 */
	public Dimension getFullViewSize() {
		Dimension size=this.getViewport().getViewRect().getSize();
		if (getVerticalScrollBarPolicy()==JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED)
			size.width += this.getVerticalScrollBar().getSize().width;
		if (getHorizontalScrollBarPolicy()==JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED)
			size.height+= this.getHorizontalScrollBar().getSize().height;
		return size;
	}
	/**
	 * Gets the boundary of the view component.
	 *
	 * @return the boundary of the view component.
	 */
	public Dimension getLimits() {

		if (centerView != null) {
			return centerView.getActualSize();
		}

		return getSize();
	}

	/**
	 * Returns the size of scrollable part of component.
	 * The size of scrollable part of component may be less
	 * then then virtual size of component.</P>
	 *
	 * @return the size of scrollable part of component
	 */
	public Dimension getScrollableAmount() {

		if (centerView != null) {
			return centerView.getScrollableAmount();

		} else {
			return new Dimension(0, 0);
		}
	}

	/**
	 * Retrieves the transformation of the view component.
	 *
	 * @return the transformation of the view component
	 */
	public Transform2D getTransformation() {

		if (centerView != null)
			return centerView.getTransformation();

		return new Transform2D();
	}

	/**
	 * <P>Retrieves the viewport position of the view component contained by this plot.
	 *
	 * @return the viewport coordinates of view component in virtual space
	 */
	public Point getViewPosition() {
		if (centerView != null) {
			return centerView.getViewPosition();
		} else {
			return getViewport().getViewPosition();
		}
	}

	/**
	 * Returns the visible amount of scrollable part of component.
	 *
	 * @return the visible amount of scrollable part of component
	 */
	public Dimension getVisibleAmount() {
		if (centerView != null) {
			return  centerView.getVisibleAmount();

		} else {
			return new Dimension(0, 0);
		}
	}

	/**
	 * Notifies the change listeners with this instance.
	 */
	public void notifyChangeListeners() {

		ChangeEvent e = new ChangeEvent(this);
		this.notifyChangeListeners(e);
	}

	/**
	 * Notifies the change listeners with given event.
	 *
	 * @param e the change event
	 */
	public void notifyChangeListeners(ChangeEvent e) {

		for (int i = 0; i < changeListeners.size(); i++) {
			ChangeListener listen = (ChangeListener) changeListeners.get(i);
			listen.stateChanged(e);
		}
	}

	/**
	 * Removes a change listener from this component.
	 *
	 * @param listen the change listener to remove
	 */
	public void removeChangeListener(ChangeListener listen) {
		changeListeners.remove(listen);
	}


	/**
	 * Scales the view from the specified point in model space,
	 * maps this point to the center of view and redraws view.</P>
	 *
	 * @param x       the x coordinate in modeling coordinate system
	 * @param y       the y coordinate in modeling coordinate system
	 * @param xScale  the x coordinate scale factor
	 * @param yScale  the y coordinate scale factor
	 */
	public void scaleTo(double x, double y, double xScale, double yScale) {

		if (centerView != null )
			centerView.scaleTo(x, y, xScale, yScale);

		if (topCom != null)
			topCom.scaleTo(x, y, xScale, yScale);

		if (leftCom != null)
			leftCom.scaleTo(x, y, xScale, yScale);

	}

	/**
	 * Sets the new boundary for this view.
	 *
	 * @param x the x point of this component.
	 * @param y the y point of this component.
	 * @param w the width of this component.
	 * @param h the height of this component.
	 */
	public void setBounds(int x, int y, int w, int h) {

		super.setBounds(x, y, w, h);

		invalidate();
	}

	public void setHorizontalAxis(JComponent ann){
		this.setColumnHeaderView(ann);
	}

	public void setVerticalAxis(JComponent ann){
		this.setRowHeaderView(ann);
	}

	/**
	 * Sets the view component for the plot.
	 *
	 * @param view  VisuableComponent.
	 */
	public void setView(VisuableComponent view) {

		setBackground(Color.white);
		setOpaque(true);

		// remove old one
		if (centerView != null) {
			centerView.removeChangeListener(this);
			remove(centerView);
			centerView = null;
		}

		// set the new
		if (view != null) {
			centerView = view;
			centerView.addChangeListener(this);

			this.setViewportView(centerView);
		}

	}

	/**
	 * Changes the position of the view inside the view component.
	 * The repainting action is performed.
	 * @param x the x coordinate of new position
	 * @param y the y coordinate of new position
	 */
	public void setViewPosition(int x, int y) {
		Point newPos = new Point(x, y);
		this.getViewport().setViewPosition(newPos);
		if (centerView!=null)
			centerView.setViewPosition(newPos);
		
	}

	/**
	 * Handles a change event.
	 *
	 * @param e	the change event
	 */
	public void stateChanged(ChangeEvent e) {
		notifyChangeListeners(e);
	}
	
	/**
	 * overrids to update position
	 */
	protected void paintComponent(Graphics g){
		Point newPos  = getViewPosition();
		getHorizontalScrollBar().setValue(newPos.x);
		getVerticalScrollBar().setValue(newPos.y);
		super.paintComponent(g);
	}

}
