//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 

package com.gwsys.gw2d.view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;


/**
 * Defines the container with javax.swing.BoxLayout. Components added into 
 * this container may be laid out either horizontally or vertically. This
 * container can be scaled to given factors.
 * @author Hua
 */
public class BoxContainer extends JPanel implements ComponentListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3690191062090004785L;

	private int orientation = BoxLayout.X_AXIS;
	/**
	 * Creates box container with orientation constants.
	 *
	 * @param orient Either BoxLayout.X_AXIS or BoxLayout.Y_AXIS.
	 */
	public BoxContainer(int orient) {
		super();
		if (orient!=BoxLayout.X_AXIS&&orient!=BoxLayout.Y_AXIS)
			throw new IllegalArgumentException(
					"Must be BoxLayout.X_AXIS or BoxLayout.Y_AXIS!");
		
		super.setLayout(new BoxLayout(this, orient));
		orientation = orient;
	}


	/**
	 * Overrids the method to allow the BoxLayout only.
	 * @param layout the layout manager.
	 */
	public void setLayout(LayoutManager layout) {
	}
	
	/**
	 * Scales every components in this container.
	 *
	 * @param xScale  the scale factor in x coordinate.
	 * @param yScale  the scale factor in y coordinate.
	 */
	public void scaleTo( double x, double y, double xScale, double yScale) {
		//scale one direction
		for (int i = 0; i < getComponentCount(); i++) {
			JComponent comp = (JComponent) getComponent(i);
			Dimension size = comp.getPreferredSize();	
			if (orientation == BoxLayout.Y_AXIS)
				size.width *= xScale;
			else
				size.height *= yScale;
			comp.setPreferredSize(size);
		}
	}
	
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
	}

	/**
	 * Implements API in ComponentListener.
	 * @param arg0 ComponentEvent
	 */
	public void componentResized(ComponentEvent arg0) {
		//		pass into subcomponents
		for (int i = 0; i < getComponentCount(); i++) {
			JComponent comp = (JComponent) getComponent(i);
			if (comp instanceof ComponentListener)
				((ComponentListener)comp).componentResized(arg0);
		}
	}
	
	/**
	 * @inheritDoc
	 */
	public void componentMoved(ComponentEvent arg0) {
				
	}
	
	/**
	 * @inheritDoc
	 */
	public void componentShown(ComponentEvent arg0) {
				
	}
	
	/**
	 * @inheritDoc
	 */
	public void componentHidden(ComponentEvent arg0) {
				
	}
}
