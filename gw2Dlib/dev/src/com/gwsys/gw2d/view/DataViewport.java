//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
// 
package com.gwsys.gw2d.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JViewport;
//import javax.swing.Timer;

/**
 * An extension of javax.swing.JViewport that allows pulggable customized
 * painting on the viewport. This customized viewport applies BACKINGSTORE
 * SCROLL MODE.
 * 
 * @author Adam.
 *
 */
public class DataViewport extends JViewport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3906933395572144432L;

	private transient boolean repaintAll = true;
	
	/**
	 * A Timer invokes the repaint method.
	 */
	//private transient Timer repaintTimer; to repaint automatically.
	
	/**
	 * Creates a viewport with BACKINGSTORE_SCROLL_MODE.
	 */
	public DataViewport() {
		setOpaque(true);
		setBackground(Color.white);
		setScrollMode(JViewport.BACKINGSTORE_SCROLL_MODE);
	}

	/**
	 * Paints the just exposed area
	 */
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		//custom painting
		//compute the blit area
		//
	}
	
	protected void paintChildren(Graphics g){
		super.paintChildren(g);
		//custom painting
		//g.drawImage(overlay);
	}
	
	//Overrid method to workaround bug in JViewport.
	
	/**
	 * Overrid method of JViewport.
	 */
	public Point getViewPosition(){
		if (this.getComponentCount()==0)
			return new Point(0,0);
		else
			return super.getViewPosition();
	}
	
	/**
	 * Overrid method of JViewport.
	 */
	public void setViewPosition(Point p){
		repaintAll = true;
		if (this.getComponentCount()>0)
			super.setViewPosition(p);
	}
	
	/**
	 * Overrid method of JViewport.
	 */
	public Component getView(){
		if (this.getComponentCount()>0)
			return getComponent(0);
		return null;
	}
	//*******************************************
	
	/**
	 * To draw overlay content on the top.
	 */
	public void paintOverlay(Graphics g){
		if (repaintAll)
			repaint();
		g.drawImage(this.backingStoreImage, 0, 0, this);
	}
}
