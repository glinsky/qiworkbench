//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp.
// and distributed by BHP Billiton Petroleum under license.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License Version 2 as as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import com.gwsys.gw2d.event.AbstractEvent;
import com.gwsys.gw2d.event.ModelChangeEvent;
import com.gwsys.gw2d.event.ModelChangeListener;
import com.gwsys.gw2d.event.ViewChangeEvent;
import com.gwsys.gw2d.model.AbstractDataModel;
import com.gwsys.gw2d.model.Scene2DPainter;
import com.gwsys.gw2d.shape.AbstractDataShape;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.GWUtilities;
import com.gwsys.gw2d.util.Transform2D;

/**
 * This container contains one data model and handle special mouse events.
 *
 * @author Hua.
 */
public class SimpleViewContainer extends VisuableComponent implements
		MouseListener, MouseMotionListener {

	/**
	 * UID
	 */
	private static final long serialVersionUID = 3256441387187975732L;

	/**
	 * The cache image used for double buffering.
	 */
	private Image _cacheimage;

	/**
	 * The size of this component in device space.
	 */
	private Dimension devLimit;

	private Bound2D reusedBound = new Bound2D();

	/**
	 * The data model rendered by this view.
	 */
	private AbstractDataModel dataModel = null;

	/** The Scene2DPainter */
	private Scene2DPainter painter;

	/**
	 * The clipping area in model space.
	 */
	private Shape clipArea = null;
	/**
	 * Transform2d
	 */
	private Transform2D transform2d;

	/**
	 * Creates a new view. At least the model must be specified additionaly
	 * before view will appear on the screen.
	 */
	public SimpleViewContainer() {
		setLimits(0, 0);
		setDoubleBuffered(false);
		//setBackground(Color.gray);
		setOpaque(false);
		putClientProperty("EnableWindowBlit", Boolean.TRUE);

	}

	/**
	 * Creates a view that with specified model. Default identity
	 * transformation will be used.
	 * </P>
	 *
	 * @param model
	 *            the model that will be rendered in this view
	 */
	public SimpleViewContainer(AbstractDataModel model) {
		this();
		setModel(model);
		setTransformation(new Transform2D());
	}

	/**
	 * Adds the specified component to this container at the given index. This
	 * method also notifies the layout manager to add the component to this
	 * container's layout using the constraints object. This is an overriding
	 * method to handle any attempt to add components to the plot view.
	 *
	 * @param comp
	 *            the component to add
	 * @param constraints
	 *            the constraints for laying out the component
	 * @param index
	 *            the container index
	 */
	protected void addImpl(Component comp, Object constraints, int index) {
		throw new IllegalArgumentException("PlotView doesn't accept children");
	}

	/**
	 * Finds the current virtual space (device space) limits.
	 *
	 * @return the <code>Dimension</code> object for the virtual space
	 */
	public Dimension getActualSize() {
		return new Dimension(devLimit);
	}

	/**
	 * Returns the cache image
	 *
	 * @return the cache image
	 */
	public Image getCache() {
		return _cacheimage;
	}

	/**
	 * Gets the stored reference to the model object.
	 *
	 * @return the AbstractDataModel
	 */
	public AbstractDataModel getModel() {
		return (AbstractDataModel) dataModel;
	}

	/**
	 * Retrieves the current clipping area in model space.
	 *
	 * @return the current clipping area in model space.
	 */
	public Shape getModelClip() {
		return clipArea;
	}

	/**
	 * Returns the size of scrollable part of component. The size of scrollable
	 * part of component may be less then then virtual size of component.
	 *
	 * @return the size of scrollable part of component
	 */
	public Dimension getScrollableAmount() {
		return getActualSize();
	}

	/**
	 * Retrieves the model transformation.
	 *
	 * @return the <code>Transform2D</code> object
	 */
	public Transform2D getTransformation() {
		if (transform2d == null) {
			transform2d = new Transform2D();
		}
		return transform2d;
	}

	/**
	 * Handles the mouse button clicked events on this component.
	 *
	 * @param e
	 *            the mouse event
	 */
	public void mouseClicked(MouseEvent e) {
	}

	/**
	 * Handles the mouse dragged events on a component.
	 *
	 * @param e
	 *            the mouse event
	 */
	public void mouseDragged(MouseEvent e) {
	}

	/**
	 * Handles the mouse entered events on this component.
	 *
	 * @param e
	 *            the mouse event
	 */
	public void mouseEntered(MouseEvent e) {
	}

	/**
	 * Handles the mouse exited events on this component.
	 *
	 * @param e
	 *            the mouse event
	 */
	public void mouseExited(MouseEvent e) {
	}

	/**
	 * Handles the mouse moved events on a component.
	 *
	 * @param e
	 *            the mouse event
	 */
	public void mouseMoved(MouseEvent e) {
	}

	/**
	 * Handles the mouse button pressed events on this component.
	 *
	 * @param e
	 *            the mouse event
	 */
	public void mousePressed(MouseEvent e) {
	}

	/**
	 * Handles the mouse button released events on a component.
	 *
	 * @param e
	 *            the mouse event
	 */
	public void mouseReleased(MouseEvent e) {
	}

	/**
	 * Notifies the view listeners.
	 */
	public void notifyViewListeners() {
		Dimension l = getActualSize();
		Bound2D invBBox = new Bound2D(0, 0, l.width, l.height);
		notifyViewListeners(invBBox);
	}

	/**
	 * Notifies the view listeners that a specific rectangular area of view (in
	 * device space) is invalidated.
	 *
	 * @param invBBox
	 *            the invalidated rectangular area in device space; if this
	 *            argument is <CODE>null</CODE> the whole view will be
	 *            invalidated
	 */
	public void notifyViewListeners(Bound2D invBBox) {

		if (invBBox == null) {
			Dimension l = getActualSize();
			invBBox = new Bound2D(0, 0, l.width, l.height);
		}

		ViewChangeEvent event = new ViewChangeEvent(this, invBBox,
				ViewChangeEvent.VIEW_CHANGED);
		notifyViewListeners(event);
	}

	/**
	 * Paints the view by calling Scene2DPainter.
	 *
	 * @param g
	 *            the <code>Graphics</code> from Java 2D.
	 */
	public void paintComponent(Graphics g) {
		//System.out.println("Paint in SimpleView");
		Rectangle clip = this.getVisibleRect();//g.getClipBounds();
		if (clip == null)
			return;

		super.setDirtyMark(false);

		if (transform2d == null) {
			transform2d = new Transform2D();
		}


		if (isOpaque()) {
			g.setColor(getBackground());
			g.fillRect(clip.x, clip.y, clip.width, clip.height);
		}

		Point pos = getViewPosition();

		g.setClip(pos.x, pos.y, clip.width, clip.height);

		if (clipArea != null) {
			Shape subbox = transform2d.createTransformedShape(clipArea);
			((Graphics2D) g).clip(subbox);
		}

		if (painter == null)
			painter = new Scene2DPainter();
		else
			painter.reset();

		painter.setGraphics((Graphics2D) g);

		painter.setTransformation(transform2d);

		transform2d.inverseTransform(clip, reusedBound);

		if (dataModel != null) {
			dataModel.render(painter, reusedBound);
		}
		g.dispose();

	}

	/**
	 * Redraws entire plot view.
	 */
	protected void redraw() {
		redraw(0, 0, getSize().width, getSize().height);
	}

	/**
	 * Invokes the repaint process in the specified rectangle.
	 *
	 * @param x
	 *            the x-coordinate of the rectangle
	 * @param y
	 *            the y-coordinate of the rectangle
	 * @param w
	 *            the width of the rectangle
	 * @param h
	 *            the height of the rectangle
	 */
	protected void redraw(int x, int y, int w, int h) {
		repaint(0, x, y, w, h);
	}

	/**
	 * Scales the view from the specified point in model space, maps this point
	 * to the center of view and redraws view.
	 *
	 * @param x
	 *            the x coordinate in modeling coordinate system
	 * @param y
	 *            the y coordinate in modeling coordinate system
	 * @param xScale
	 *            the x coordinate scale factor
	 * @param yScale
	 *            the y coordinate scale factor
	 */
	public void scaleTo(double x, double y, double xScale, double yScale) {

		GWUtilities.scaleTransformation(xScale, yScale, this
				.getTransformation());
		if (dataModel != null && dataModel.getBoundingBox() != null) {
			devLimit.setSize((int) Math.abs(dataModel.getBoundingBox().width
					* transform2d.getScaleX()), (int) Math.abs(dataModel
					.getBoundingBox().height
					* transform2d.getScaleY()));
		} else {
			devLimit.setSize(0, 0);
		}

		setDirtyMark(true);
		notifyViewListeners(new ViewChangeEvent(this, this,
				ViewChangeEvent.VIEW_CHANGED));
	}

	/**
	 * Calculates the bounding box from model.
	 *
	 * @param e AbstractEvent
	 * @return Bound2D
	 */
	private Bound2D searchBound(AbstractEvent e) {

		if (e != null) {
			Object obj = e.getEventMessage();
			if (obj instanceof Bound2D) {
				return (Bound2D)obj;
			}else if (obj instanceof AbstractEvent) {
				return searchBound((AbstractEvent) obj);
			}else if (obj instanceof AbstractDataShape) {
				AbstractDataShape sh = (AbstractDataShape) obj;
				return sh.getBoundingBox(getTransformation());
			}else{
				if (e.getSource() instanceof AbstractDataShape) {
					AbstractDataShape sh = (AbstractDataShape) e.getSource();
					return sh.getBoundingBox(getTransformation());

				}else
					return null;
			}
		}else
			return null;
	}

	/**
	 * Update the actual size and revalidate component.
	 * @param size the actual size
	 */
	public void setActualSize(Dimension size){
		devLimit = size;
	}

	/**
	 * Sets the dirty mark to specified value.
	 */
	public void setDirtyMark(boolean mark) {
		if (mark){
			super.setDirtyMark(mark);
			setPreferredSize(devLimit);
			this.revalidate();
		}
	}

	/**
	 * Defines the new virtual space (device space) limits. This method invokes
	 * change notification and view listeners.
	 *
	 * @param w
	 *            the new bounding box width
	 * @param h
	 *            the new bounding box height
	 */
	public void setLimits(int w, int h) {
		Dimension old_limits = devLimit;
		devLimit = new Dimension(w, h);
		setPreferredSize(devLimit);
		notifyChangeListeners();

		Bound2D invBBox = new Bound2D(0, 0, devLimit.width, devLimit.height);
		if (old_limits != null)
			invBBox = invBBox.union(new Bound2D(0, 0, old_limits.width,
					old_limits.height));
		notifyViewListeners(invBBox);

	}

	/**
	 * Attaches the model object to this plot view.
	 *
	 * @param model
	 *            the AbstractDataModel
	 */
	public void setModel(AbstractDataModel model) {
		dataModel = model;
		model.addModelEventListener(new ModelListener());
	}

	/**
	 * Defines the new modeling clip to draw.
	 *
	 * @param clip
	 *            Shape as clipping arew in modeling space.
	 */
	public void setModelClip(Shape clip) {
		clipArea = clip;
	}
	/**
	 * Set the actual size of component as preferred size.
	 */
	public void setPreferredSize(Dimension size){
		devLimit = size;
		this.notifyViewListeners();
	}
	/**
	 * Attaches the Transform2D object to this view.
	 * @param trans2d
	 *            the model transformation object for this plot view
	 */
	public void setTransformation(Transform2D trans2d) {
		setTransformationWithoutRepaint(trans2d);

		this.notifyViewListeners();
	}

	/**
	 * Attaches the Transform2D object to this view without  repainting.
	 *
	 * @param trans2d
	 *            the model transformation object for this plot view
	 */
	public void setTransformationWithoutRepaint(Transform2D trans2d) {
		boolean change = false;

		transform2d = trans2d;

		if (dataModel != null && dataModel.getBoundingBox() != null) {
			int w, h;

			if (trans2d == null) {
				w = (int) Math.abs(dataModel.getBoundingBox().getWidth());
				h = (int) Math.abs(dataModel.getBoundingBox().getHeight());
			} else {
				w = (int) Math.abs(dataModel.getBoundingBox().getWidth()
						* trans2d.getScaleX());
				h = (int) Math.abs(dataModel.getBoundingBox().getHeight()
						* trans2d.getScaleY());
			}
			change = (devLimit == null) || (devLimit.getWidth() != w)
					|| (devLimit.getHeight() != h);

			devLimit = new Dimension(w, h);
			if (change)
				notifyChangeListeners();
		}
		setDirtyMark(true);
	}

	/**
	 * Implementation of ModelChangeListener
	 */
	private class ModelListener implements ModelChangeListener {

		public void modelUpdated(ModelChangeEvent e) {

			// Calculates bound from user space to device space.
			Bound2D deviceBound = searchBound(e);
			if (getTransformation() != null && deviceBound != null)
				deviceBound = getTransformation().transform(deviceBound);

			notifyViewListeners(deviceBound);
		}

	}

}
