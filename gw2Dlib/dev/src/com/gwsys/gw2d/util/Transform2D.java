//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.util;

import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * This is customized version of AffineTransform in order to transform 
 * one Bound2D to another Bound2D. 
 * 
 */
public class Transform2D extends AffineTransform {

	/**
	 * serial id
	 */
	private static final long serialVersionUID = 3258130237031527990L;
	//re use instances
	private Point2D before = new Point2D.Double(0, 0);

	private Point2D after = new Point2D.Double(0, 0);

	/**
	 * Constructs the identity AffineTransform.
	 */
	public Transform2D(){
		this.setToIdentity();
	}

	/**
	 * Constructs a new Transform2D from an array of double precision values 
	 * representing either the 4 non-translation entries or the 6 specifiable 
	 * entries of the 3x3 transformation matrix. The values are retrieved 
	 * from the array as { m00 m10 m01 m11 [m02 m12]}. 
	 *
	 * @param matrix double array matrix.
	 */
	public Transform2D(double[] matrix) {
		super(matrix);
	}

	/**
	 * Constructs a new Transform2D from  4 values representing 
	 * scaling and translatation elements in matrix.
	 *
	 * @param scaleX the scale factor along x axis.
	 * @param scaleY the scale factor along y axis.
	 * @param shiftX the translatation along x axis.
	 * @param shiftY the translatation along y axis.
	 */
	public Transform2D(double scaleX, double scaleY, double shiftX, double shiftY) {
		super(scaleX, 0, 0, scaleY, shiftX, shiftY);
	}

	/**
	 * Constructs a Transform2D from two bound coordinates.
	 * @param srcBound	the data bound in user coordinates.
	 * @param dstBound	the screen coordinates.
	 */
	public Transform2D(Bound2D srcBound, Bound2D dstBound) {
		this(srcBound, dstBound, false, true);
	}


	/**
	 * Constructs a Transform2D from user space bound to device space.
	 * With the given flipped and ratio-fixed flags.
	 *
	 * @param srcBound	the source rectangle in user space.
	 * @param devBound	the destination rectangle in device space.
	 * @param flipped	the flag to flip destination rectangle.
	 * @param ratioFixed  the flag to maintain the aspect ratio or not.
	 */
	public Transform2D(Bound2D srcBound, Bound2D devBound, 
			boolean flipped,
			boolean ratioFixed) {

		setTransformation(srcBound, devBound, flipped, ratioFixed);
		
	}
	/**
	 * Sets the transformation with new two Bounds.
	 *
	 * @param srcBound	the source rectangle in user space.
	 * @param devBound	the destination rectangle in device space.
	 * @param flipped	the flag to flip destination rectangle along X.
	 * @param ratioFixed  the flag to maintain the aspect ratio or not.
	 */
	public void setTransformation(Bound2D srcBound, Bound2D devBound,
			boolean flipped, boolean ratioFixed) {
		//Ref:Hill, F. Computer Graphics.Englewood Cliffs, NJ:Prentice Hall,1990.
		setToIdentity();
		//reflect object or not
		if (flipped) {
			scale(1, -1);
			translate(0, -devBound.getHeight());
		}
		
		double scaleX = devBound.getWidth() / srcBound.getWidth();
		double scaleY = devBound.getHeight() / srcBound.getHeight();
		//remain same scale factor or not
		if (ratioFixed) {
			if (scaleX > scaleY)
				scaleX = scaleY;
			else
				scaleY = scaleX;
		}

		if (ratioFixed && flipped && scaleY > scaleX) {
			translate(devBound.getMinX(), devBound.getMaxY());
			scale(scaleX, scaleY);
			translate(-srcBound.getMinX(), -srcBound.getMaxY());
		} else {
			translate(devBound.getMinX(), devBound.getMinY());
			scale(scaleX, scaleY);
			translate(-srcBound.getMinX(), -srcBound.getMinY());
		}
	}

	/**
	 * Performs an inverse transformation for given source Rectangle.
	 * The behavior is same as inverseTransform(srcRect, null).
	 * @param srcRect the source bound.
	 *
	 * @return the inversed transformed bound.
	 */
	public Bound2D inverseTransform(Rectangle2D srcRect) {		
		return inverseTransform(srcRect, null);
	}

	/**
	 * Performs an inverse transformation for given source Rectangle.
	 *
	 * @param srcRect the source bound.
	 * @param dstRect the inversed transformation bound.
	 *
	 * @return the inversed transformation bound or null if exception
	 */
	public Bound2D inverseTransform(Rectangle2D srcRect, Bound2D dstRect) {
		double x1, y1, x2, y2;
		
		try {
			before.setLocation(srcRect.getMinX(), srcRect.getMinY());
			inverseTransform(before, after);

			x1 = after.getX();
			y1 = after.getY();

			before.setLocation(srcRect.getMaxX(), srcRect.getMaxY());
			inverseTransform(before, after);

			x2 = after.getX();
			y2 = after.getY();
		} catch (java.awt.geom.NoninvertibleTransformException e) {
			return null;
		}
		
		if (dstRect != null) {
			dstRect.setRect(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x1
					- x2), Math.abs(y1 - y2));
			return dstRect;
		} else {
			return new Bound2D(x1, y1, x2, y2);
		}
	}

	/**
	 * Returns a transformed Bound2D.
	 * @param srcBound the source Bound2D.
	 * @return New transformed Bound2D.
	 */
	public Bound2D transform(Bound2D srcBound) {
		//transform 4 points
		double[] newPoints = new double[8];
		before.setLocation(srcBound.getMinX(), srcBound.getMinY());
		transform(before, after);
		newPoints[0] = after.getX();
		newPoints[1] = after.getY();
		before.setLocation(srcBound.getMinX(), srcBound.getMaxY());
		transform(before, after);
		newPoints[2] = after.getX();
		newPoints[3] = after.getY();
		before.setLocation(srcBound.getMaxX(), srcBound.getMinY());
		transform(before, after);
		newPoints[4] = after.getX();
		newPoints[5] = after.getY();
		before.setLocation(srcBound.getMaxX(), srcBound.getMaxY());
		transform(before, after);
		newPoints[6] = after.getX();
		newPoints[7] = after.getY();
		//get min and max values
		double minX = Double.POSITIVE_INFINITY;
		double minY = Double.POSITIVE_INFINITY;
		double maxX = Double.NEGATIVE_INFINITY;
		double maxY = Double.NEGATIVE_INFINITY;
		for (int i = 0; i < newPoints.length; i+=2) {
			if (minX > newPoints[i])
				minX = newPoints[i];
			if (maxX < newPoints[i])
				maxX = newPoints[i];
			if (minY > newPoints[i + 1])
				minY = newPoints[i + 1];
			if (maxY < newPoints[i + 1])
				maxY = newPoints[i + 1];
		}

		return new Bound2D(minX, minY, maxX, maxY);
	}

}
