//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.util;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import javax.imageio.ImageIO;


/**
 * Provides all singleton utility methods and logger for application.
 * @author Adam
 */
public final class GWUtilities {

	private static RenderingHints renderHints = null;

	private static boolean debugging = false;

	private static Logger logger = Logger.getLogger("GWUtilities");

	/**
	 * Returns the default rendering hints that contain KEY_RENDERING
	 * and KEY_COLOR_RENDERING with VALUE_*_SPEED.
	 *
	 * @return  predefined hints.
	 */
	public static RenderingHints getRenderingHints() {
		if (renderHints == null) {
			renderHints = new RenderingHints(null);

			renderHints.put(RenderingHints.KEY_RENDERING,
					RenderingHints.VALUE_RENDER_SPEED);
			renderHints.put(RenderingHints.KEY_COLOR_RENDERING,
					RenderingHints.VALUE_COLOR_RENDER_SPEED);

		}
		return renderHints;
	}

	/**
	 * Changes the state of debugging.
	 *
	 * @param  enable Flag to allow debugging.
	 */
	public static void enableDebuggingInfo(boolean enable) {
		debugging = enable;
	}

	/**
	 *  Prints out the debugging information to logger file or
	 *  standard output.
	 *
	 * @param  info  debugging information
	 */
	public static void info(String info) {
		if (debugging)
			System.out.println(info);
		else
			logger.info(info);
	}
	/**
	 *  Prints out the warning information to logger file or
	 *  standard output.
	 *
	 * @param  info  debugging information
	 */
	public static void warning(String info){
		if (debugging)
			System.out.println(info);
		else
			logger.warning(info);
	}
	
	/**
	 * Converts the Image Object to BufferedImage (TYPE_INT_RGB).
	 * @param image Image Object.
	 * @return BufferedImage.
	 */
	public static BufferedImage toBufferedImage(Image image) {
		int width = image.getWidth(null);
		int height = image.getHeight(null);

		BufferedImage bimage = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		Graphics ig = bimage.createGraphics();
		ig.drawImage(image, 0, 0, null);

		return bimage;
	}

	/**
	 * Reads the image file and Converts to BufferedImage. 
	 * @param imageFile
	 * @return BufferedImage or null if exception occurs.
	 */
	public static BufferedImage loadBufferedImage(String imageFile) {
		File file = new File(imageFile);
		try {
			return ImageIO.read(file);
		} catch (IOException e) {
			logger.severe(e.getMessage());
			return null;
		}
	}

	/**
	 * Scales a Transform2D instance using the given parameters.
	 * Note that this transformation changes not only scaling element 
	 * but also translation element.
	 * @param xFactor  the x coordinate scale factor
	 * @param yFactor  the y coordinate scale factor
	 * @param transform  the transformation instance to be scaled.
	 */
	public static void scaleTransformation(double xFactor, double yFactor,
			Transform2D transform) {

		double m00 = transform.getScaleX()* xFactor;
		double m11 = transform.getScaleY()* yFactor;
		double m02 = transform.getTranslateX() * xFactor;
		double m12 = transform.getTranslateY() * yFactor;
		double m01 = transform.getShearX();
		double m10 = transform.getShearY();
		
		transform.setTransform(m00, m10, m01, m11, m02,	m12);
	}

	/**
	 * Creates the text layout by given string and font.
	 * @param text Text content.
	 * @param font Font of Text.
	 * @return TextLayout
	 */
	public static TextLayout createTextLayout(String text, Font font) {

		boolean antiAliased;
		boolean fracMetrics;

		if (renderHints.containsValue(RenderingHints.VALUE_ANTIALIAS_ON))
			antiAliased = true;
		else
			antiAliased = false;

		if (renderHints
				.containsValue(RenderingHints.VALUE_FRACTIONALMETRICS_ON))
			fracMetrics = true;
		else
			fracMetrics = false;

		FontRenderContext frc = new FontRenderContext(null, antiAliased,
				fracMetrics);

		return new TextLayout(text, font, frc);

	}
	
	public static void main(String[] m){
		
	}
}
