//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.util;

import java.awt.geom.Rectangle2D;
import java.awt.Rectangle;

/**
 * Defines the bound region with extension of Rectangle2D.
 * Some new behaviors are added by requirement of BHP 2D Viewer.
 * @author hua.
 */
public class Bound2D extends Rectangle2D.Double {
	/**
	 * Constructs a rectangle with two points (0,0) and (1,1).
	 */
	public Bound2D() {
		this(0, 0, 1, 1);
	}

	/**
	 * Constructs a rectangle with four user-defined values.
	 * These points make up the two opposite corners of the rectangle.
	 *
	 * @param x1	the x-coordinate of one corner
	 * @param y1	the y-coordinate of one corner
	 * @param x2	the x-coordinate of the opposite corner
	 * @param y2	the y coordinate of the opposite corner
	 */
	public Bound2D(double x1, double y1, double x2, double y2) {
		super(Math.min(x1, x2), Math.min(y1, y2), 
				Math.abs(x1 - x2), Math.abs(y1 - y2));
	}

	/**
	 * Constructs a copy of the rectangle.
	 *
	 * @param rect	the another bound range.
	 */
	public Bound2D(Bound2D rect) {
		this.setRect(rect);
	}

	/**
	 * Constructs a Bound2D using the java.awt.Rectangle.
	 * 
	 *
	 * @param rect	java.awt.Rectangle.
	 */
	public Bound2D(Rectangle rect) {
		this.setRect(rect);
	}

	/**
	 * @inheritDoc
	 */
	public boolean equals(Object obj) {
		if (obj instanceof Bound2D) {
			return super.equals(obj);
		} else {
			return false;
		}
	}

	/**
	 * Returns a rectangle that is the intersection of the current
	 * rectangle and the specified rectangle.
	 *
	 * @param bound		the rectangle to be compared.
	 * @return the rectangle of intersection or Bound2D(0,0,0,0).
	 */
	public Bound2D intersection(Bound2D bound) {
		if (this.intersects(bound))
			return new Bound2D(
				getMinX() > bound.getMinX() ? getMinX() : bound.getMinX(),
				getMinY() > bound.getMinY() ? getMinY() : bound.getMinY(),
				getMaxX() < bound.getMaxX() ? getMaxX() : bound.getMaxX(),
				getMaxY() < bound.getMaxY() ? getMaxY() : bound.getMaxY());
		return new Bound2D(0,0,0,0);
	}

	/**
	 * Multiplies each coordinate of this rectangle by the user-defined
	 * value.
	 *
	 * @param scale		the value to be multiplied.
	 */
	public void multiply(double scale) {
		this.setRect(this.getX() * scale, this.getY() * scale, 
				this.getWidth() * scale, this.getHeight() * scale);
	}

	/**
	 * Gets a string which specifies the coordinates of this rectangle.
	 *
	 * @return A string with the format: "Bound: (minx,miny:maxx,maxy)"
	 */
	public String toString() {
		return "Bound: " + "(" + getMinX() + "," + getMinY() + ":" + getMaxX()
				+ "," + getMaxY() +")";
	}

	/**
	 * Returns a new Bound2D object representing the union of this 
	 * Bound2D with the specified Bound2D.
	 *
	 * @param bound		the specified Bound2D.
	 *
	 * @return	a new Bound2D containing both rectangles.
	 */
	public Bound2D union(Bound2D bound) {
		Rectangle2D rect=createUnion(bound);
		return new Bound2D( rect.getMinX(),	rect.getMinY(),
							rect.getMaxX(),	rect.getMaxY());
	}

	/**
	 * Checks if two regions are intersective. <br>
	 * This method is overrided to make sure to return <i>true</i>
	 * if their outlines overlap each other.
	 *
	 *
	 * @param x the x coordinate of the specified rectangular area.
	 * @param y the y coordinate of the specified rectangular area .
	 * @param w the width of the specified set of rectangular coordinates.
	 * @param h the height of the specified set of rectangular coordinates.
	 * 
	 * @return true if this bound intersects the specified region.
	 *
	 */
	public boolean intersects(double x, double y, double w, double h) {
		if (getX()==x&&getY()==y&&getWidth()==w&&getHeight()==h)
			return true;
		else
			return super.intersects(x,y,w,h);
		
	}

}
