//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.gw2d.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.geom.AffineTransform;

import javax.swing.JComponent;

import com.gwsys.gw2d.model.RenderingAttribute;

/**
 * Displays labels with specified orientation.
 * Normally, the label stays in the visible area all time regardless
 * viewport.
 * This component has to reponse ComponentEvent in order to resize its size.
 * @author Hua
 *
 */
public class OrientedLabel extends JComponent implements ComponentListener{
	/**
	 * SUID
	 */
	private static final long serialVersionUID = 3258408447950009913L;
	public static final int ALIGNMENT_LEFT = 0;
	public static final int ALIGNMENT_RIGHT = 1;
	public static final int ALIGNMENT_TOP = 2;
	public static final int ALIGNMENT_BOTTOM = 3;
	public static final int ALIGNMENT_VERTICAL_CENTER = 4;
	public static final int ALIGNMENT_HORIZONTAL_CENTER = 5;
	//private members
	private String content;
	private Dimension size = new Dimension(10, 20);
	private boolean scrollable = true; //to reponse scrollbar event
	private int align = 0, offset = 5;
	private RenderingAttribute attribute;
	/**
	 * Creates CornerLabel with Center Alignment.
	 */
	public OrientedLabel(String label) {
		this(label, ALIGNMENT_HORIZONTAL_CENTER);
	}
	/**
	 * 
	 */
	public OrientedLabel(String label, int alignment) {
		content = label;
		if (content==null)
			throw new IllegalArgumentException("The label cannot be null.");
		//count the default size
		if (alignment == ALIGNMENT_VERTICAL_CENTER)
			size.setSize(size.height, content.length()*size.getWidth());
		else
			size.setSize(content.length()*size.getWidth(), size.height);
		align = alignment;
		setBackground(Color.white);
	}
	
	protected void paintComponent(Graphics g){
		Rectangle current = g.getClipBounds();
		Rectangle visible = this.getVisibleRect();
		if (visible.width > current.width || visible.height > current.height)
			current.setBounds(visible);
		Graphics2D g2d = (Graphics2D)g;
		if (!scrollable)
			current.setBounds(0,0,getSize().width, getSize().height);
		g2d.setColor(getBackground());
		g2d.fill(g2d.getClipBounds());
		if (attribute!=null){
			if (attribute.getFont()!=null)
				g2d.setFont(attribute.getFont());
			if (attribute.getLineColor()!=null)
				g2d.setColor(attribute.getLineColor());
		}else
			g2d.setColor(Color.black);
		
		int length = g2d.getFontMetrics(g2d.getFont()).stringWidth(content);
		int high = g2d.getFontMetrics(g2d.getFont()).getHeight();
		switch(align){
		case ALIGNMENT_LEFT:
			g2d.drawString(content, current.x+offset, (current.height+high)/3);
			break;
		case ALIGNMENT_RIGHT:
			g2d.drawString(content, current.x+current.width-length-offset, (current.height+high)/3);
			break;
		case ALIGNMENT_HORIZONTAL_CENTER:
			g2d.drawString(content, current.x+(current.width-length)/2, (current.height+high)/3);
			break;
		case ALIGNMENT_VERTICAL_CENTER:
			AffineTransform af = g2d.getTransform();
			g2d.rotate(-Math.PI/2, current.width/2, current.y+current.height/2);
			g2d.drawString(content, (current.width-length)/2, current.y+(current.height+high)/2);
			g2d.setTransform(af);
			break;
		}
	}
	
	/**
	 * Sets the attribute ( color, font) for the label.
	 *
	 * @param attribute the attribute for the label
	 */
	public void setLabelAttribute( RenderingAttribute attribute ) {
		if (attribute.getFont()!=null){
			size.setSize(attribute.getFont().getSize()*content.length(), 
					attribute.getFont().getSize());
		}
		this.attribute = attribute;
	}
	
	public Dimension getPreferredSize(){
		return size;
	}
	
	public void setPreferredSize(Dimension size){
		this.size = size;
		revalidate();
	}
	
	public void setText(String text){
		content = text;
		repaint();
	}
	
	/**
	 * Implements API in ComponentListener.
	 * @param arg0
	 */
	public void componentResized(ComponentEvent arg0) {
		setSize(arg0.getComponent().getSize());
		repaint();
		
	}
	public void componentMoved(ComponentEvent arg0) {
		
	}
	public void componentShown(ComponentEvent arg0) {
		
	}
	public void componentHidden(ComponentEvent arg0) {
		
	}
	
	/**
	 * Sets the flag to decide where to draw label.<br>
	 * By default, it is true in order to reponse scrollbar event.
	 * @param scrol Scrollable Flag
	 */
	public void setScrollable(boolean scrol){
		this.scrollable = scrol;
	}
}
