//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.gw2d.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import com.gwsys.gw2d.model.Attribute2D;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.gw2d.model.DataShape;

/**
 * This class is responsible for choosing Line styles.
 * It contains special renderer to draw a line inside cell of combobox.
 * The order of style matches the constants defined in the 
 * "com.gwsys.gw2d.model.Attribute2D" interface.
 * @author  Hua
 */
public class LineStyleChooser extends JComboBox {

	/**
	 * UID.
	 */
	private static final long serialVersionUID = 3257005453916715312L;
	
	private static final Integer[] LINE_STYLES = {
			new Integer(Attribute2D.LINE_STYLE_SOLID),
			new Integer(Attribute2D.LINE_STYLE_EMPTY),
			new Integer(Attribute2D.LINE_STYLE_DASH),
			new Integer(Attribute2D.LINE_STYLE_DOT),
			new Integer(Attribute2D.LINE_STYLE_DASH_DOT),
			new Integer(Attribute2D.LINE_STYLE_DASH_DOT_DOT) };

	/**
	 * Constructor.
	 */
	public LineStyleChooser() {

		super(LINE_STYLES);
		setToolTipText("Choose Line Style");

		LineStyleRenderer renderer = new LineStyleRenderer();
		setRenderer(renderer);
		setMaximumSize(renderer.getPreferredSize());
		setMinimumSize(renderer.getPreferredSize());
	}

	/**
	 * The object is associated with Line Attribute.
	 */
	public void setAttribute(DataShape shape) {
		RenderingAttribute attr = (RenderingAttribute) (shape.getAttribute());
		if (attr == null)
			return;
		attr.setLineStyle(((Integer)getSelectedItem()).intValue());
	}

	/**
	 * Sets the new line style for this chooser.
	 */
	public void setLineStyle(int lineStyle) {
		setSelectedItem(new Integer(lineStyle));
	}

	/**
	 * Get the current selected line style .
	 */
	public int getLineStyle() {
		return ((Integer) getSelectedItem()).intValue();
	}

	static class LineStyleRenderer extends JPanel implements ListCellRenderer {
		/**
		 * UID.
		 */
		private static final long serialVersionUID = 3691035465663067191L;
		
		private Integer style;
		
		/**
		 * Constructor.
		 *
		 */
		public LineStyleRenderer(){
			setLayout(null);
			setPreferredSize(new Dimension(90, 20));
		}
		
		/**
		 * interface ListCellRenderer.
		 */
		public Component getListCellRendererComponent(JList list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {
			if (isSelected) {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
			} else {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
			}

			style = (Integer)value;

			return this;
		}

		//base on codes in examples in JDK
		protected void paintComponent(Graphics g) {
			Dimension d = getSize();

			Graphics2D g2 = (Graphics2D) g;
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);

			g2.setColor(getBackground());
			g2.fillRect(0, 0, d.width, d.height);

			g2.setColor(Color.black);
			
			
			float dash[]=null;
			switch(style.intValue()){
			case Attribute2D.LINE_STYLE_SOLID:
				g2.setStroke(new BasicStroke(3, 1, 1, 1));
				g2.drawLine(5, d.height / 2, d.width - 10, d.height / 2);
				break;
			case Attribute2D.LINE_STYLE_EMPTY:
				g2.setStroke(new BasicStroke(0.3f));
				g2.drawRect(5, d.height / 2 - 2, d.width - 10, 2);
				break;
			case Attribute2D.LINE_STYLE_DOT:
				dash = new float[]{ 1, 5 };
				g2.setStroke(new BasicStroke(3, 1, 1, 1, dash, 0));
				g2.drawLine(5, d.height / 2, d.width - 10, d.height / 2);
				break;
			case Attribute2D.LINE_STYLE_DASH:
				dash = new float[]{ 5, 5 };
				g2.setStroke(new BasicStroke(3, 1, 1, 1, dash, 0));
				g2.drawLine(5, d.height / 2, d.width - 10, d.height / 2);
				break;
			case Attribute2D.LINE_STYLE_DASH_DOT:
				dash = new float[]{ 5, 5, 1, 5 };
				g2.setStroke(new BasicStroke(3, 1, 1, 1, dash, 0));
				g2.drawLine(5, d.height / 2, d.width - 10, d.height / 2);
				break;
			case Attribute2D.LINE_STYLE_DASH_DOT_DOT:
				dash = new float[]{ 5, 5, 1, 5, 1, 5 };
				g2.setStroke(new BasicStroke(3, 1, 1, 1, dash, 0));
				g2.drawLine(5, d.height / 2, d.width - 10, d.height / 2);
				break;
			}
		}
	}
}