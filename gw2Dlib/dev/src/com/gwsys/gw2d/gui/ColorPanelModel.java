//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.gui;

import java.awt.Color;

/**
 * Defines a color model interface which contains algorithm of color array.
 */
public interface ColorPanelModel {

	/**
	 * Sets the new color array.
	 * @param colors Array of colors.
	 */
	public void setColorArray(Color[] colors);

	/**
	 * Returns the array of colors.
	 * @return an array of colors
	 */
	public Color[] getColorArray();

	/**
	 * Gets the label associated with first color element.
	 * @return
	 */
	public String getFirstLabel();
	/**
	 * Gets the label associated with last color element.
	 * @return
	 */
	public String getLastLabel();
	/**
	 * Gets the label associated with middle color element.
	 * @return
	 */
	public String getMiddleLabel();
	/**
	 * Gets the all labels.
	 * @return
	 */
	public String[] getLabels();
	
	/**
	 * Sets the data range associated with color array.
	 * @param first Start value.
	 * @param last  End value.
	 */
	public void setRanges(double first, double last);
}