//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.gw2d.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import com.gwsys.gw2d.model.DataShape;
import com.gwsys.gw2d.model.RenderingAttribute;

/**
 * This class is responsible for choosing size for associated line.
 * It contains special renderer to draw a line inside cell of combobox.
 */
public class LineWidthChooser extends JComboBox {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3257845493584572471L;
	private static final Integer[] LINE_SIZES = { new Integer(1),
			new Integer(2), new Integer(3), new Integer(4), new Integer(5),
			new Integer(6), new Integer(7), new Integer(8), new Integer(9),
			new Integer(10), new Integer(11), new Integer(12) };

	/**
	 *  Constructor.
	 */
	public LineWidthChooser() {
		super(LINE_SIZES);
		setToolTipText("Choose Line Width");

		LineWidthRenderer renderer = new LineWidthRenderer();
		renderer.setPreferredSize(new Dimension(90, 20));
		setRenderer(renderer);

		setMaximumSize(new Dimension(80, 20));
		setMinimumSize(new Dimension(80, 20));
	}

	/**
	 * Sets the selected line width to assocaiated object.
	 */
	public void commitValue(DataShape shape) {
		RenderingAttribute attr = (RenderingAttribute) (shape.getAttribute());
		if (attr == null)
			return;
		attr.setLineWidth(getLineWidth());
	}

	/**
	 * Set the new  line width.
	 */
	public void setLineWidth(int lineWidth) {
		if (lineWidth > 0 && lineWidth <= 12) {
			setSelectedItem(new Integer(lineWidth));
		}
	}

	/**
	 * Get the selected line width.
	 */
	public int getLineWidth() {
		return ((Integer) getSelectedItem()).intValue();
	}

	static class LineWidthRenderer extends JPanel implements ListCellRenderer {
	    /**
		 * 
		 */
		private static final long serialVersionUID = 3834310613869474612L;
		private Integer value;
		private Font font=new Font("Arial", Font.PLAIN, 10);
		
		/**
		 * Constructor.
		 */
	    public LineWidthRenderer() {
	        setLayout(null);
	    }

	    /**
	     * Interface ListCellRenderer
	     */
	    public Component getListCellRendererComponent( JList list,
	                                                   Object value,
	                                                   int index,
	                                                   boolean isSelected,
	                                                   boolean cellHasFocus)
	    {
	        
	        if (isSelected || cellHasFocus) {
	            setBackground(list.getSelectionBackground());
	            setForeground(list.getSelectionForeground());
	        } else {
	              setBackground(list.getBackground());
	              setForeground(list.getForeground());
	        }
			this.value = (Integer)value;

	        return this;
	    }

	    /**
	     * Overrids the method of JPanel.
	     */
	    protected void paintComponent(Graphics g) {

	        Dimension d = getSize();

	        Graphics2D g2d = (Graphics2D) g;
	        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

	        g2d.setColor(getBackground());
	        g2d.fillRect(0, 0, d.width, d.height);

	        g2d.setColor(Color.black);
	        g2d.setFont(font);
	        g2d.drawString(value.toString(),5, d.height/2 + 2);
	        g2d.setStroke(new BasicStroke(value.intValue(), BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
	        g2d.drawLine(d.width/2 - 10, d.height/2, d.width - 10, d.height/2);

	    }
	}

}