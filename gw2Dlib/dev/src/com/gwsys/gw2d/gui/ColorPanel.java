//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JPanel;

/**
 * ColorBar is a panel to draw color map in the current area.
 * If the parent component is resized, the color area is repainted.
 * Support vertical display now.
 * 
 */
public class ColorPanel extends JPanel implements ComponentListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3257844368387158833L;

	private String panelName;

	private ColorPanelModel colorModel;

	private Color background;

	private Dimension defaultSize;
	
	private int startX=10, startY=40, widthBar=15;
	/**
	 * Creates a color bar with a specified color bar model.
	 */
	public ColorPanel(ColorPanelModel model, String name) {
		//guess the size of labels
		int labelSize = model.getFirstLabel().length();
		labelSize = Math.max(labelSize, model.getLastLabel().length());
		defaultSize = new Dimension(labelSize*10+startX+widthBar, 300);
		panelName = name;
		background = Color.white;
		colorModel = model;
	}
	/**
	 * @inheritDoc
	 */
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
		Dimension rect = getSize();
		g2d.setColor(background);
		g2d.fillRect(0,0, rect.width, rect.height);
		Color[] colors = colorModel.getColorArray();
		
		float step = (rect.height-2*startY)/(float)(colors.length-1);
		for (int i=0; i<colors.length-1;i++){
		      g2d.setPaint(new GradientPaint(
					  startX, startY+i*step, colors[i],
		              startX, startY+(i+1)*step, colors[i+1]));
		      g2d.fillRect(
					  startX, startY+(int)(i*step), widthBar, (int)(step+1) );
		}

		g2d.setColor(Color.black);
		
		String[] labels = colorModel.getLabels();
		step = (rect.height-2*startY)/(labels.length-1);
		for (int k=0; k<labels.length; k++)
			g2d.drawString(labels[k], startX+widthBar+2, startY+k*step);
		
		g2d=null;
	}
	/**
	 * @inheritDoc
	 */
	public Dimension getPreferredSize(){
		if (defaultSize==null)
			return new Dimension(60,300);
		return defaultSize;
	}
	/**
	 * @inheritDoc
	 */
	public void componentResized(ComponentEvent arg0) {
		repaint();		
	}
	/**
	 * @inheritDoc
	 */
	public void componentMoved(ComponentEvent arg0) {
		repaint();		
	}
	/**
	 * @inheritDoc
	 */
	public void componentShown(ComponentEvent arg0) {
		repaint();
	}
	/**
	 * @inheritDoc
	 */
	public void componentHidden(ComponentEvent arg0) {
			
	}

	/**
	 * Gets the name of this panel.
	 */
	public String getName(){
		return panelName;
	}
	
	/**
	 * Retrives the color model used in this panel.
	 * @return the color model used in this panel.
	 */
	public ColorPanelModel getColorPanelModel(){
		return colorModel;
	}
}
