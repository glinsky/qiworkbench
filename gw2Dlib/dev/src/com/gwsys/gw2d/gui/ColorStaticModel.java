//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.gui;

import java.awt.Color;
import java.text.NumberFormat;

/**
 * This model provides a set of static colors.
 * 
 */
public class ColorStaticModel implements ColorPanelModel {

	private Color[] colors;
	private double firstValue,lastValue;

	private NumberFormat dformat;
	/**
	 * Constructs.
	 */
	public ColorStaticModel(Color[] colors) {
		this.colors = colors;

	}

	/**
	 * Returns the array of colors.
	 * @return an array of colors
	 */
	public Color[] getColorArray() {
		return colors;
	}

	/**
	 * Sets the new color array.
	 * @param colors the new color array
	 */
	public void setColorArray(Color[] colors) {

		this.colors = colors;
	}
	/**
	 * @inheritDoc
	 */
	public String getFirstLabel() {
		return dformat.format(firstValue);
	}
	/**
	 * @inheritDoc
	 */
	public String getMiddleLabel(){
		return dformat.format((lastValue+firstValue)/2);
	}
	/**
	 * @inheritDoc
	 */
	public String getLastLabel() {
		return dformat.format(lastValue);
	}
	/**
	 * @inheritDoc
	 */
	public void setRanges(double first, double last){

		firstValue = first;
		lastValue = last;
	}
	/**
	 * @inheritDoc
	 */
	public String[] getLabels(){
		return new String[]{dformat.format(firstValue),
				dformat.format((lastValue+firstValue)/2),dformat.format(lastValue)};
	}

}