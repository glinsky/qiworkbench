//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//

package com.gwsys.gw2d.gui;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import com.gwsys.gw2d.model.DataShape;
import com.gwsys.gw2d.model.Attribute2D;
import com.gwsys.gw2d.model.AbstractDataLayer;
import com.gwsys.gw2d.model.CommonDataModel;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.gw2d.shape.LineShape;
import com.gwsys.gw2d.shape.Polyline2D;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;
import com.gwsys.gw2d.view.VisuableComponent;

/**
 * Creates polyline on the overlay layer with mouse movement.
 */
public class DynamicLineCreator {

	private AbstractDataLayer currentLayer;

	private VisuableComponent currentView;

	private AbstractDataLayer overlayLayer;

	private VisuableComponent overlayView;

	private boolean enabled;

	private DataShape lineShape;

	private RenderingAttribute lineAttr = null;

	private ArrayList pointList = new ArrayList();

	private ArrayList shapeList = new ArrayList();

	private int indexPoints;

	private double minX = 0, minY = 0, maxX = 0, maxY = 0;

	/**
	 * Constructor. Create Shape in Overlay layer.
	 * User should provide two simple views with layer instances.
	 */
	public DynamicLineCreator(CommonDataModel data_model,
			AbstractDataLayer data_layer, VisuableComponent data_view,
			CommonDataModel overlay_model, AbstractDataLayer overlay_layer,
			VisuableComponent overlay_view) {
		currentLayer = data_layer;
		currentView = data_view;
		overlayLayer = overlay_layer;
		overlayView = overlay_view;
		enabled = false;
		init();
	}

	/**
	 * Removes all points and repaints this dirty area.
	 */
	public void removeAllPoints() {
		for (int i = 0; i < shapeList.size(); i++) {
			overlayLayer.removeShape((LineShape) shapeList.get(i));
		}
		currentLayer.removeShape(lineShape);
		lineShape = null;
		calculateBoundary();
		overlayView.repaint((int) minX, (int) minY, (int) maxX, (int) maxY);
		init();
	}

	private void init() {
		pointList.clear();
		indexPoints = 0;
		shapeList.clear();
		clearBoundary();
	}

	/**
	 * Adds a point in the creating path.
	 * 
	 * @param point
	 *            mouse point
	 */
	public void addPoint(Point point) {
		Bound2D trBox = new Bound2D(point.x, point.y, point.x + 1, point.y + 1);
		Transform2D tr = currentView.getTransformation();
		if (tr == null)
			tr = new Transform2D();
		trBox = tr.inverseTransform(trBox);
		if (lineAttr == null)
			createDefaultAttribute();
		if (indexPoints > 0) {
			double[] lastPoint = (double[]) pointList.get(indexPoints - 1);
			((LineShape) shapeList.get(indexPoints - 1)).setLine(lastPoint[0],
					lastPoint[1], trBox.x, trBox.y);

		}
		pointList.add(indexPoints, new double[] { trBox.x, trBox.y });
		LineShape line = new LineShape(trBox.x, trBox.y, trBox.x, trBox.y);
		line.setAttribute(lineAttr);
		overlayLayer.addShape(line);
		shapeList.add(line);

		indexPoints++;
	}

	/**
	 * Finish the creating process.
	 * 
	 * @param point
	 *            mouse point
	 */
	public void commitPoint(Point point) {
		if (indexPoints > 0) {
			setEnabled(false);
			
			double[] pointX = new double[pointList.size()];
			double[] pointY = new double[pointList.size()];
			for (int i = 0; i < pointList.size(); i++) {
				double[] pointXY = (double[]) pointList.get(i);
				pointX[i] = pointXY[0];
				pointY[i] = pointXY[1];
			}
			lineShape = new Polyline2D(indexPoints, pointX, pointY);
			lineShape.setAttribute(lineAttr);
			currentLayer.addShape(lineShape);
			//remove all temp lines
			for (int i = 0; i < shapeList.size(); i++) {
				overlayLayer.removeShape((LineShape) shapeList.get(i));
			}
			calculateBoundary();
			overlayView.repaint((int) minX, (int) minY, (int) maxX,
					(int) maxY);
			indexPoints = 0;
			shapeList.clear();
		}

	}

	/**
	 * Move the lase point to this new point.
	 * 
	 * @param point
	 *            mouse point
	 */
	public void movePoint(Point point) {
		if (indexPoints > 0) {

			Bound2D bound = new Bound2D(point.x, point.y, point.x + 1,
					point.y + 1);
			Transform2D tr = currentView.getTransformation();
			if (tr == null)
				tr = new Transform2D();
			bound = tr.inverseTransform(bound);

			if (shapeList.get(indexPoints - 1) != null) {
				double[] lastPoint = (double[]) pointList.get(indexPoints - 1);
				((LineShape) shapeList.get(indexPoints - 1)).setLine(
						lastPoint[0], lastPoint[1], bound.x, bound.y);
			}

		}

	}

	/**
	 * Returns the status of process.
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Enable the creating process or not.
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Sets the graphic attribute for the shapes to be created.
	 */
	public void setAttribute(RenderingAttribute attribute) {
		lineAttr = attribute;
	}

	/**
	 * Gets the last shape created.
	 * 
	 * @return the last shape created.
	 */
	public DataShape getLastShape() {
		return lineShape;
	}

	private void createDefaultAttribute() {
		lineAttr = new RenderingAttribute();
		lineAttr.setLineColor(Color.black);
		lineAttr.setLineStyle(Attribute2D.LINE_STYLE_SOLID);
		lineAttr.setFillStyle(Attribute2D.FILL_STYLE_EMPTY);
		lineAttr.setLineWidth(1);
	}

	/**
	 * 
	 */
	private void calculateBoundary() {
		double[] temp = null;
		for (int i = 0; i < pointList.size(); i++) {
			temp = (double[]) pointList.get(i);
			if (minX > temp[0])
				minX = temp[0];
			if (maxX < temp[0])
				maxX = temp[0];
			if (minY > temp[1])
				minY = temp[1];
			if (maxY < temp[1])
				maxY = temp[1];
		}

	}

	private void clearBoundary() {
		minX = 0;
		maxX = 0;
		minY = 0;
		maxY = 0;
	}
}

