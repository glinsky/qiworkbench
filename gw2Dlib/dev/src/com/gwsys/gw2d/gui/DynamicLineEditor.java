//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.gui;

import com.gwsys.gw2d.model.Attribute2D;
import com.gwsys.gw2d.model.CommonDataModel;
import com.gwsys.gw2d.model.CommonShapeLayer;
import com.gwsys.gw2d.model.DataLayer;
import com.gwsys.gw2d.model.DataShape;
import com.gwsys.gw2d.model.RenderingAttribute;

import com.gwsys.gw2d.shape.PointGroup;
import com.gwsys.gw2d.shape.Polyline2D;
import com.gwsys.gw2d.shape.RectSymbolPainter;
import com.gwsys.gw2d.shape.SymbolShape;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;
import com.gwsys.gw2d.view.VisuableComponent;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;

/**
 * This class performs the shape selection and editing with mouse event.
 *
 */
public class DynamicLineEditor {
	private DataLayer currentLayer, controlLayer;

	private CommonDataModel currentModel;

	private VisuableComponent currentView;

	private Transform2D transformation;

	private Attribute2D pointAttr = null;
	
	private DataShape lastSelectedShape;
	/**
	 * Constructor.
	 * @param model The model inside editing view.
	 * @param view The view to be edited.
	 * @param layer The layer contains shape to be selected.
	 */
	public DynamicLineEditor(CommonDataModel model, VisuableComponent view,
			DataLayer layer) {
		currentModel = model;
		currentView = view;
		currentLayer = layer;
		transformation = view.getTransformation();
		if (transformation == null)
			transformation = new Transform2D();
	}

	/**
	 * Gets the last selected shape.
	 * @return the last selected DataShape.
	 */
	public DataShape getLastSelectedShape() {
		return lastSelectedShape;
	}


	/**
	 * Adds the shape to the selected list.
	 * The symbol is shown on the geometry points.
	 * @param ds DataShape.
	 */
	public void addShapeToSelected(DataShape ds) {
		lastSelectedShape = ds;
		if (ds != null){				
			if (ds instanceof PointGroup){				
				//add all points
				int num = ((PointGroup)ds).getSize();
				if (pointAttr == null)
					createDefaultAttribute();
				for (int j=0; j<num; j++){
					SymbolShape point = new SymbolShape(
							((PointGroup)ds).getXAt(j), ((PointGroup)ds).getYAt(j),
							5,5, SymbolShape.LOCATION_CENTER, 
							new RectSymbolPainter(), false, true);
					point.setAttribute(pointAttr);
					controlLayer.addShape(point);
					
				}
				
			}
		}
	}
	
	/**
	 * Select the shape with mouse event.
	 * @param me MouseEvent
	 */
	public void doSelection(MouseEvent me){
		
		//select geometry by boundary
		if (controlLayer!=null){
			//remove all selected shape first
			controlLayer.removeAllShapes();
			Point pos = new Point(currentView.getViewPosition().x+me.getX(),
					currentView.getViewPosition().y+me.getY());
			Iterator it = currentLayer.getShapes();
			DataShape ds = null;
			while (it.hasNext()){
				ds = (DataShape)it.next();
				if ((ds instanceof Polyline2D)&&
					(ds.getBoundingBox(transformation).contains(pos)))
						break;
				else 
					ds = null;
			}
			addShapeToSelected(ds);
			
		}else{
			controlLayer = new CommonShapeLayer();
			currentModel.addLayer(controlLayer);
		}
		
	}
	/**
	 * Edit the selected shape.
	 * @param me MouseEvent
	 */
	public void doEdition(MouseEvent me){
		Point pos = new Point(currentView.getViewPosition().x+me.getX(),
				currentView.getViewPosition().y+me.getY());
		
		if (me.getID() == MouseEvent.MOUSE_DRAGGED){
			Iterator it = controlLayer.getShapes();
			DataShape ds = null;
			Rectangle2D symRect = new Rectangle2D.Double();
			int index = 0;
			while (it.hasNext()){
				ds = (DataShape)it.next();
				Rectangle2D bound=ds.getBoundingBox(transformation);
				symRect.setRect(bound.getMinX(),bound.getMinY(),2*bound.getWidth(), 2*bound.getHeight());
				
				if ((ds instanceof SymbolShape) && symRect.contains(pos))
						break;
				else 
					ds = null;
				index++;
			}
			//move the point
			if (ds != null){	
				
				Point2D oldLoc = new Point2D.Double(pos.getX(),pos.getY());
				Point2D newLoc = new Point2D.Double();
				try {
					transformation.inverseTransform(oldLoc, newLoc);
				} catch (NoninvertibleTransformException e) {
					return;
				}
				((SymbolShape)ds).setLocation(newLoc.getX(),newLoc.getY());
				
				((PointGroup)lastSelectedShape).setPoint(index, newLoc.getX(),newLoc.getY());
			}
		}
	}
	
	/**
	 * Delete the selected shape.
	 *
	 */
	public void doDeletion(){
		
		if (lastSelectedShape!=null&&controlLayer!=null){
			currentLayer.removeShape(lastSelectedShape);
			controlLayer.removeAllShapes();		
			currentModel.removeLayer(controlLayer);
			Bound2D b2d=lastSelectedShape.getBoundingBox(transformation);
			currentView.repaint((int)b2d.getMinX(),(int)b2d.getMinY(),
					(int)b2d.getMaxX(),(int)b2d.getMaxY());
		}
			
		lastSelectedShape=null;
	}
	
	/**
	 * Sets the new attribute for selection.
	 * @param attr Attribute2D
	 */
	public void setSelectionAttribute(Attribute2D attr){
		pointAttr = attr;
	}
	
	private void createDefaultAttribute() {
		pointAttr = new RenderingAttribute();
		pointAttr.setLineColor(Color.black);
		pointAttr.setFillPaint(Color.red);
		pointAttr.setLineStyle(Attribute2D.LINE_STYLE_SOLID);
		pointAttr.setFillStyle(Attribute2D.FILL_STYLE_SOLID);
		pointAttr.setLineWidth(1);
	}
}

