//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.model;

import java.util.Vector;

import com.gwsys.gw2d.event.AttributeChangeListener;
import com.gwsys.gw2d.event.AttributeEvent;
import com.gwsys.gw2d.event.LayerChangeEvent;
import com.gwsys.gw2d.event.LayerChangeListener;
import com.gwsys.gw2d.event.ModelChangeEvent;
import com.gwsys.gw2d.event.ModelChangeListener;
import com.gwsys.gw2d.util.Bound2D;

/**
 * The Abstract DataModel.
 */
public abstract class AbstractDataModel implements DataModel,
	LayerChangeListener, AttributeChangeListener {

	/** Bounding box in this model */
	private Bound2D bound2d = new Bound2D(0, 0, 0, 0);

	/** Calculation flag, notification flag, Reverse rendering flag*/
	private boolean reDo = false, notifyEvent = true, reversedRender = false;

	/** Event listener */
	private Vector modelListeners = new Vector();

	/**
	 * Constructs a container model.
	 *
	 */
	public AbstractDataModel() {
	}


	/**
	 * Registers this ModelChangeListener for this object.
	 * @param listener ModelChangeListener.
	 */
	public void addModelEventListener(ModelChangeListener listener) {
		modelListeners.add(listener);
	}

	/**
	 * The implementation of AttributeChangeListener.
	 * @param event the attribute event
	 */
	public void attributeChanged(AttributeEvent event) {
		invalidateModel();
	}

	/**
	 * Returns the bounding box.
	 * @return the bounding box.
	 */
	public Bound2D getBoundingBox() {
		return bound2d;
	}

	/**
	 * Gets the rendering order.
	 * @return the rendering order.
	 */
	public boolean getReversed(){
		return reversedRender;
	}

	/**
	 * Invalidates model to notify all listeners.
	 */
	public void invalidateModel() {
		if (!notifyEvent)
			return;
		Bound2D rect = getBoundingBox();
		ModelChangeEvent event = new ModelChangeEvent(this, rect,
				ModelChangeEvent.MODEL_CHANGED);
		for (int i = 0; i < modelListeners.size(); i++)
			((ModelChangeListener) modelListeners.get(i)).modelUpdated(event);

	}

	/**
	 * Check if bounding box calculation was enabled.
	 * @return <i>true</i> if enabled;
	 *		   <i>false</i> otherwise.
	 */
	public boolean isBoundingBoxCalculationEnabled() {
		return reDo;
	}

	/**
	 * Checks if notification is enabled.
	 * @return <i>true</i> if enabled;
	 *		   <i>false</i> otherwise.
	 */
	public boolean isNotificationEnabled() {
		return notifyEvent;
	}

	/** 
	 * The implementation of LayerChangeListener.
	 * @param e LayerChangeEvent
	 */
	public void layerUpdated(LayerChangeEvent e) {
		if (!notifyEvent) 			
			return;
		
		ModelChangeEvent event = new ModelChangeEvent(this, e,
				ModelChangeEvent.MODEL_CHANGED);
		for (int i = 0; i < modelListeners.size(); i++) {
			((ModelChangeListener) modelListeners.get(i)).modelUpdated(event);
		}
	}

	/**
	 * Removes the ModelChangeListener from this object.
	 * @param listener The listener to be removed.
	 */
	public void removeModelEventListener(ModelChangeListener listener) {
		modelListeners.remove(listener);
	}

	/**
	 * Sets the bounding box of this model to the specified rectangle.
	 * @param b the bounding box to be used for this model
	 */
	public void setBoundingBox(Bound2D b) {
		bound2d = b;

		invalidateModel();
	}

	/**
	 * Sets bounding box calculations automatically.
	 * @param auto <i>true</i> if enabled;
	 *		   <i>false</i> otherwise.
	 */

	public void setBoundingBoxCalculation(boolean auto) {
		reDo = auto;
	}

	/**
	 * Sets notification enabled for invalidation events.
	 * @param flag The Evnet Notification Flag.
	 */

	public void setNotificationFlag(boolean flag){
		notifyEvent = flag;
	}

	/**
	 * Changes the rendering order as reversed.
	 * @param reversed Flag that indicates rendering order.
	 */
	public void setReversed(boolean reversed){
		reversedRender = reversed;
	}
}
