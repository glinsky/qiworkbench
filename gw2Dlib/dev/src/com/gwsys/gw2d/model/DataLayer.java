//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.model;

import java.util.Iterator;

import com.gwsys.gw2d.event.LayerChangeListener;
import com.gwsys.gw2d.model.ScenePainter;
import com.gwsys.gw2d.util.Bound2D;

/**
 * Defines the layer property.
 */
public interface DataLayer{
	/**
	 * Returns the bounding box of this model.
	 * @return the bounding box.
	 */
	public Bound2D getBoundingBox();

	/**
	 * Sets the bounding box for this model.
	 * @param bbox the bounding box.
	 */
	public void setBoundingBox(Bound2D bbox);

	/**
	 * Sets the bounding box calculation flag.
	 * @param enable flag of bounding box calculation.
	 */

	public void enableBoundingBoxCalculation(boolean enable);


	/**
	 * Checks if bounding box calculation was enabled programatically.
	 * @return flag of bounding box calculation.
	*/
	public boolean isBoundingBoxCalculationEnabled();

	/**
	 * Tests if this layer is visible and hence renderable.
	 * @return 	flag of visibility.
	 */
	public boolean isVisible();

	/**
	 * Sets the rendering flag of this layer.
	 * @param vis flag of visibility.
	 */
	public void setVisible(boolean vis);

	/**
	 * Sets the selection flag of this layer.
	 * @param sele Flag of Selection.
	 */
	public void setSelectable(boolean sele);

	/**
	 * Tests if this layer is selectable.
	 * @return  Flag of Selection.
	 */
	public boolean isSelectable();

 	/**
	 * Gets the total number of shapes in this layer.
	 *
	 * @return the number of shapes
	 */
	public int getTotalShapes();

	/**
	 * Gets an iterator of shapes in this layer.
	 * @return the iterator for all the shapes in the layer.
	 */
	public Iterator getShapes();
	
	/**
	 * Add the DataShape into Layer.
	 * @param shape DataShape
	 */
	public void addShape(DataShape shape);
	/**
	 * Remove the DataShape from Layer.
	 * @param shape DataShape
	 */
	public void removeShape(DataShape shape);
	/**
	 * Remove all shapes inside Layer.
	 *
	 */
	public void removeAllShapes();
	/**
	 * Renders the layer into painter.
	 * @param painter ScenePainter.
	 * @param bound The bounding area to be repainted.
	 */
	public void render(ScenePainter painter, Bound2D bound);

	/**
	 * Invoked by the view system to pass rendering parameters.
	 * @param painter ScenePainter.
	 * @param bound The bounding area to be repainted.
	 * @param reverse Flag indicates reverse order for rendering.
	 */
	public void render(ScenePainter painter, Bound2D bound, boolean reverse);
	
	/**
	 * Invalidates the whole layer.
	 */
	public void invalidateLayer();
	
 	/**
	 * Invalidates the specified bbox in the layer.
	 * @param bbox	the region to be invalidated
	 */
	public void invalidateLayer(Bound2D bbox);

 	/**
	 * Invalidates the specified shape in the layer.
	 * @param shape	the shape to be invalidated
	 */
	public void invalidateLayer(DataShape shape);

	/**
	 * Registers an event listener.
	 * @param listener	the event listener to be added.
  	 */
	public void addLayerEventListener(LayerChangeListener listener);
		
	/**
	 * Removes an event listener.
	 * @param listener	the event listener to be removed.
  	 */
	public void removeLayerEventListener(LayerChangeListener listener);

	/**
	 * Tests whether invalidation messages are propagated
	 * @return 	<CODE>true</CODE> if invalidation messages are propagated, 
	 * 		<CODE>false</CODE> otherwise.
	 */
	public boolean isNotificationEnabled();
	
	/**
	 * Sets the propagation flag for invalidation messages.
	 * @param note if <CODE>true</CODE> invalidation messages are propagated, 
	 *			<CODE>false</CODE> otherwise.
	 */
	public void setNotification(boolean note);

}