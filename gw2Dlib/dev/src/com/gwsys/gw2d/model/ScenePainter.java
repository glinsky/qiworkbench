//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.model;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;

import com.gwsys.gw2d.model.DataShape;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;

/**
 * The painter carries the information in order to render scene.
 *
 *	@author
 */

public interface ScenePainter {

	/**
	 * Draws the outline of the specified java2D shape.
	 *
	 * @param shape a Java 2D shape to be rendered
	 */
	public void draw(java.awt.Shape shape);
	
	/**
	 * Fills color or texture in the specified Java 2D shape.
	 *
	 * @param shape a Java 2D shape to be rendered
	 */
	public void fill(java.awt.Shape shape);

	/**
	 * Gets the original AffineTransform
	 * @return AffineTransform
	 */
	public AffineTransform getAffineTransform();

	/**
	 * Gets the current attribute. 
	 *
	 * @return the current attribute
	 */
	public Attribute2D getAttribute();

	/**
	 * Gets the device clipping area.
	 * @return the device clipping area
	 */
	public Rectangle getClipping();

	/**
	 * Retrieves the Java2D graphics object.
	 *
	 * @return Java2D graphics object
	 */
	public Graphics2D getGraphics();


	/**
	 * Gets the current transformation from model to device coordinates.
	 *
	 * @return  the current transformation
	 */
	public Transform2D getTransformation();

	/**
	 * Renders the shape with the specified rendering bounding box.
	 * 
	 * @param shape DataShape the shape to be rendered.
	 * @param bound Bound2D the required bounding box.
	 */
	public void render(DataShape shape, Bound2D bound);
	
	/**
	 * Sets the current attribute. 
	 *
	 * @param attr the current attribute
	 */
	public void setAttribute(Attribute2D attr);

 	/**
	 * Sets the device clipping area with the specified rectangle.
	 *
	 * @param x1	the X coordinate of the left top corner
	 * @param y1	the Y coordinate of the left top corner
	 * @param x2	the X coordinate of the right bottom corner
	 * @param y2	the Y coordinate of the right bottom corner
	 */
	public void setClipping(int x1, int y1, int x2, int y2);

 	/**
	 * Sets the device clipping area with the specified shape.
	 *
	 * @param shape shape used to define the clipping area.
	 */
	public void setClipping(Shape shape);

	/**
	 * Sets the current transformation.
	 *
	 * @param tr the current transformation
	 */
	public void setTransformation(Transform2D tr);
}

