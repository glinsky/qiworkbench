//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.model;

import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.GWUtilities;
import com.gwsys.gw2d.util.Transform2D;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

/**
 * ScenePainter is created in ViewContainer and passed through all shapes.
 * 
 * @author
 */
public class Scene2DPainter implements ScenePainter {

	private Attribute2D attrib2d = null;

	private Graphics2D graphics2d = null;

	private Transform2D transform = null;

	private AffineTransform affineTransform = null;

	private DataShape currentShape;

	/**
	 * Constructor.
	 */
	public Scene2DPainter() {
	}

	/**
	 * Resets.
	 */
	public void reset() {
		attrib2d = null;
		graphics2d = null;
		transform = null;
		affineTransform = null;
		currentShape = null;
	}

	/**
	 * Renders a shape located inside the specified bounding box.
	 * 
	 * @param shape
	 *            the shape to render
	 * @param bound
	 *            the bounding box of the shape to render
	 */
	public void render(DataShape shape, Bound2D bound) {
		currentShape = shape;
		shape.render(this, bound);
	}

	/**
	 * Retrieves the clipping area.
	 * 
	 * @return the rectangle defining the clipping area
	 */
	public Rectangle getClipping() {
		if (graphics2d == null) {
			return null;
		} else {
			return graphics2d.getClipBounds();
		}
	}

	/**
	 * Sets the clipping area defined by two corner points.
	 * 
	 * @param x1
	 *            the x-coordinate of the left top corner
	 * @param y1
	 *            the y-coordinate of the left top corner
	 * @param x2
	 *            the x-coordinate of the right bottom corner
	 * @param y2
	 *            the y-coordinate of the right bottom corner
	 */
	public void setClipping(int x1, int y1, int x2, int y2) {
		if (graphics2d != null) {
			graphics2d.setClip(x1, y1, x2 - x1, y2 - y1);
		}
	}

	/**
	 * Sets the clipping area based on the shape.
	 * 
	 * @param shape
	 *            <code>java.awt.Shape</code>
	 */
	public void setClipping(Shape shape) {
		if (graphics2d != null) {
			if (shape != null)
				graphics2d.clip(shape);
			else
				graphics2d.setClip(shape);
		}
	}

	/**
	 * Sets the attribute to the his painter.
	 * 
	 * @param attr
	 *            Attribute2D
	 * 
	 */
	public void setAttribute(Attribute2D attr) {
		attrib2d = attr;
		Stroke stroke = attrib2d.getStroke();
		if (stroke != null && (!stroke.equals(graphics2d.getStroke()))) {
			graphics2d.setStroke(stroke);
		}

		if (attrib2d.getFont() != null
				&& (!attrib2d.getFont().equals(graphics2d.getFont()))) {

			graphics2d.setFont(attrib2d.getFont());
		}

	}

	/**
	 * Sets the attribute to the his painter.
	 * 
	 * @return Attribute2D
	 * 
	 */
	public Attribute2D getAttribute() {
		return attrib2d;
	}

	/**
	 * Gets the current transformation.
	 * 
	 * @return the current transformation.
	 */
	public Transform2D getTransformation() {
		return transform;
	}

	/**
	 * Sets the transformation to the painter.
	 * 
	 * @param trans
	 *            the new Transform2D.
	 */
	public void setTransformation(Transform2D trans) {
		transform = trans;
	}

	/**
	 * Returns the graphics object.
	 * 
	 * @return Graphics2D associated with this painter.
	 */
	public Graphics2D getGraphics() {
		return graphics2d;
	}

	/**
	 * Updates the new Graphics2D instance.
	 * 
	 * @param g2d
	 *            the <code>Graphics2D</code> instance.
	 */
	public void setGraphics(Graphics2D g2d) {

		graphics2d = g2d;
		affineTransform = graphics2d.getTransform();

		if (transform != null) {
			graphics2d.setTransform(transform);
		}

		graphics2d.setRenderingHints(GWUtilities.getRenderingHints());
	}

	/**
	 * Gets the original AffineTransform
	 * 
	 * @return AffineTransform
	 */
	public AffineTransform getAffineTransform() {
		return affineTransform;
	}

	/**
	 * Draws the shape.
	 * 
	 * @param shape
	 *            the java.awt.Shape.
	 */
	public void draw(Shape shape) {

		if (attrib2d == null
				|| attrib2d.getLineStyle() == Attribute2D.LINE_STYLE_EMPTY) {

			return;
		}
		graphics2d.setPaint(attrib2d.getLineColor());

		if (currentShape != null && currentShape.isFixedSize()) {
			graphics2d.draw(shape);
		} else {
			graphics2d.draw(transform.createTransformedShape(shape));
		}
	}

	/**
	 * Fills the current shape.
	 * 
	 * @param shape
	 *            the Shape object.
	 */
	public void fill(Shape shape) {

		if (attrib2d == null
				|| attrib2d.getFillStyle() == Attribute2D.FILL_STYLE_EMPTY) {
			return;
		}

		graphics2d.setPaint(attrib2d.getFillPaint());

		if (currentShape != null && currentShape.isFixedSize()) {
			graphics2d.fill(shape);
		} else {
			graphics2d.fill(transform.createTransformedShape(shape));
		}
	}

}
