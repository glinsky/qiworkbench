//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.model;

import java.util.Iterator;

import com.gwsys.gw2d.util.Bound2D;

/**
 * This class defines a data structure that manages multiple layers.
 * There is a list containing all layers.
 * @author Hua
 */

public interface DataModel {
	/**
	 * Returns the bounding box of this model.
	 * @return the model bounding box.
	 */
	public Bound2D getBoundingBox();

	/**
	 * Sets the bounding box for this model.
	 * @param bound the model bounding box.
	 */
	public void setBoundingBox(Bound2D bound);

	/**
	 * Enables bounding box calculation automatically.
	 * @param enable	the state of bounding box calculation.
	 */
	public void setBoundingBoxCalculation(boolean enable);

	/**
	 * Check if bounding box calculation was enabled automatically.
 	 * @return	the state of bounding box calculation.
	 */
	public boolean isBoundingBoxCalculationEnabled();


	/**
	 * Invoked by the view system to render this structure.
	 * @param paint   the ScenePainter renderer.
 	 * @param bound   the region to be repainted.
	 */
	public void render(ScenePainter paint, Bound2D bound);

	/**
	 * Invoked by the view system to render this structure.
	 * @param paint    	the ScenePainter renderer.
 	 * @param bound   	the region to be repainted.
	 * @param reverse 	the order for rendering.
	 */
	public void render(ScenePainter paint, Bound2D bound, boolean reverse);
	
	/**
	 * Adds a specific layer to the model.
	 * @param layer the layer to be added to the model.
	 */
	public void addLayer(DataLayer layer);
	
	/**
	 * Inserts the layer at a specific index in the model.
	 *
	 * @param layer 	the layer to be inserted to the model.
	 * @param index 	the index where the layer is to be inserted.
	 */
	public void insertLayer(DataLayer layer, int index);
	
 	/**
 	 * Removes the specific layer from the model.
	 *
	 * @param layer 	the layer to be removed.
	 */
	public void removeLayer(DataLayer layer);
	
	/**
	 * Moves the layer associated with the specific index to the first of the 
	 * layer list.  
	 * @param index the index of the associated layer.
	 */
	
	public void moveLayerFirst(int index);
	
	/**
	 * Moves the layer associated with the specific index to the last of the 
	 * layer list.  
	 * @param index the index of the associated layer.
	 */

	public void moveLayerLast(int index);
	
	/**
 	 * Swaps the two layers with given index numbers.
 	 * @param index1 	the stack position of one layer
	 * @param index2 	the stack position of another layer
	 */
	public void swapLayers(int index1, int index2);
	
	/**
	 * Returns an iterator of the layers in this model.
	 * @return 	an iterator of the layers
	 */
	public Iterator getLayers();

	/**
	 * Retrieves the layer from the specified index location.
	 *
	 * @param index	the index number associated with a layer.
	 * @return	the DataLayer. 
	 */
	public DataLayer getLayer(int index);


}