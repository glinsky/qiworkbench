//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

import com.gwsys.gw2d.event.LayerChangeEvent;
import com.gwsys.gw2d.event.LayerChangeListener;
import com.gwsys.gw2d.event.ShapeChangeEvent;
import com.gwsys.gw2d.event.ShapeChangeListener;
import com.gwsys.gw2d.util.Bound2D;


/**
 * A layer manages all shape objects. It is a ShapeChangeListener
 * to reponse the ShapeChangeEvent. By default, the bound of this layer
 * is not calaculated by all shapes.
 * 
 * @author Hua
 */
public abstract class AbstractDataLayer implements DataLayer,
		ShapeChangeListener {

	/** Event listeners. */
	private Vector _listeners = new Vector();

	/** flags  */
	private boolean _visible = true, _notify = true, _selectable = true,
	recalculateBound = false;

	/** The bounding box of this layer.  */
	private Bound2D bound;

	/** Stores shapes */
	protected ArrayList shapeList = new ArrayList();

	/**
	 * Constructor.
	 *
	 */
	protected AbstractDataLayer(){
		
	}
	

	/**
	 * Adds a listener for this DataLayer.
	 * @param listener the LayerChangeListener to be added.
	 */
	public void addLayerEventListener(LayerChangeListener listener) {
		_listeners.add(listener);
	}

	/**
	 * Adds a shape to this layer and notifies the layer's listeners.
	 * 
	 * @param shape the shape to be added.
	 */
	public void addShape(DataShape shape) {
		
		if (recalculateBound) {
			if (bound == null) 
				bound = shape.getBoundingBox(null);
			else 
				bound.union(shape.getBoundingBox(null));
			
		}
		shapeList.add(shape);
		shape.addShapeChangeListener(this);
		if (_notify) {
		LayerChangeEvent event = new LayerChangeEvent(this, bound,
				LayerChangeEvent.SHAPE_INSERTED);
		for (int i = 0; i < _listeners.size(); i++) 
			((LayerChangeListener) _listeners.get(i)).layerUpdated(event);
		}

	}

	/**
	 * Inserts the shape at the given index or at the end.
	 *
	 * @param shape    the shape to be inserted.
	 *
	 * @param index    the position.
	 */
	public void addShapeAtIndex(DataShape shape, int index) {
		
		if (index >= shapeList.size())
			shapeList.add(shape);
		else
			shapeList.add(index, shape);
		shape.addShapeChangeListener(this);
		if (recalculateBound) {
			if (bound == null) 
				bound = shape.getBoundingBox(null);
			else 
				bound.union(shape.getBoundingBox(null));
			
		}
		if (_notify) {
		LayerChangeEvent event = new LayerChangeEvent(this, bound,
				LayerChangeEvent.SHAPE_INSERTED);
		for (int i = 0; i < _listeners.size(); i++) 
			((LayerChangeListener) _listeners.get(i)).layerUpdated(event);
		}

	}

	/**
	 * Checks if a shape is stored in this layer.
	 *
	 * @param shape 	the shape to be tested.
	 * @return the status of shape.
	 */
	public boolean containsShape(DataShape shape) {
		if (shape == null) {
			return false;
		}
		return shapeList.contains(shape);
	}

	/**
	 * Enable or disable bounding box calculations.
	 * @param enable bounding box calculation mode
	 */
	public void enableBoundingBoxCalculation(boolean enable) {
		recalculateBound = enable;
	}

	/**
	 * Returns the bounding box of this layer.
	 * @return the bounding box.
	 */
	public Bound2D getBoundingBox() {
		return bound;
	}

	/**
	 * Gets the shape specified by the index.
	 *
	 * @param index	the specified index
	 * @return DataShape or null.
	 */ 
	public DataShape getShape(int index) {
		if (index >= shapeList.size())
			return null;
		return (DataShape) shapeList.get(index);
	}
	
	/**
	 * Gets the index of the specified shape in this layer.
	 *
	 * @param shape	the shape whose index is required.
	 * @return		the index of the shape.
	 */
	public int getShapeIndex(DataShape shape) {
		return shapeList.indexOf(shape);
	}

	/**
	 * Gets an iterator of shapes in this layer.
	 * @return the iterator for all the shapes in the layer.
	 */
	public Iterator getShapes() {
		return shapeList.iterator();
	}

	/**
	 * Invalidates this layer to redraw.
	 */
	public void invalidateLayer() {
		Bound2D bound = null;
		invalidateLayer(bound);
	}

	/**
	 * Invalidates a region in this layer.
	 *
	 * @param bound bounding box or region to invalidate
	 */
	public void invalidateLayer(Bound2D bound) {
		if (_notify){
			
			LayerChangeEvent event = new LayerChangeEvent(this, bound,
					LayerChangeEvent.LAYER_CHANGED);

			for (int i = 0; i < _listeners.size(); i++)
				((LayerChangeListener) _listeners.get(i)).layerUpdated(event);
			}
	}
	/**
	 * Invalidates a shape in this layer.
	 *
	 * @param shape the shape to be invalidated
	 */
	public void invalidateLayer(DataShape shape) {
		invalidateLayer(shape.getBoundingBox(null));

	}


	/**
	 * Return  the status of bound calculation.
	 *
	 * @return  the status of bound calculation.
	 */
	public boolean isBoundingBoxCalculationEnabled() {
		return recalculateBound;
	}

	/**
	 * Returns the flag of event notification.
	 *
	 * @return 	the flag of event notification.
	 */
	public boolean isNotificationEnabled() {
		return _notify;
	}

	/**
	 * Returns the flag of selectability.
	 *
	 * @return the flag of selectability.
	 */
	public boolean isSelectable() {
		return _selectable;
	}

	/**
	 * Returns the status of visibility.
	 *
	 * @return the status of visibility.
	 */
	public boolean isVisible() {
		return _visible;
	}

	/**
	 * Removes all shapes from this layer.
	 */
	public void removeAllShapes() {

		for (int i = 0; i < shapeList.size(); i++) {
			((DataShape) shapeList.get(i)).removeShapeChangeListener(this);

		}
		if (_notify) {
			
			LayerChangeEvent event = new LayerChangeEvent(this, bound,
					LayerChangeEvent.SHAPE_REMOVED);

			for (int i = 0; i < _listeners.size(); i++) 
				((LayerChangeListener) _listeners.get(i)).layerUpdated(event);
		}

		shapeList.clear();
	}
	

	/**
	 * Removes a layer change listener from this DataLayer.
	 *
	 * @param listener the LayerChangeListener to be removed.
	 */
	public void removeLayerEventListener(LayerChangeListener listener) {
		_listeners.remove(listener);
	}

	/**
	 * Removes a shape from this layer and notifies the layer's listeners.
	 *
	 * @param shape the shape to be removed
	 */
	public void removeShape(DataShape shape) {
		if (shape == null)
			return;
		shapeList.remove(shape);
		shape.removeShapeChangeListener(this);
		if (_notify) {
		
		LayerChangeEvent event = new LayerChangeEvent(this, bound,
				LayerChangeEvent.SHAPE_REMOVED);

		for (int i = 0; i < _listeners.size(); i++) 
			((LayerChangeListener) _listeners.get(i)).layerUpdated(event);
		}
	}

	/**
	 * Sets the bounding box for this layer.
	 * @param bound the new bounding box.
	 */
	public void setBoundingBox(Bound2D bound) {
		this.bound = bound;
	}


	/**
	 * Enables or disables event notification. 
	 *
	 * @param enable the flag of event notification.
	 */
	public void setNotification(boolean enable) {
		_notify = enable;
	}

	/**
	 * Set the layer selectable or not.
	 *
	 * @param select the flag of selectability.
	 */
	public void setSelectable(boolean select) {
		_selectable = select;
	}

	/**
	 * Sets the visibility of this layer.
	 * @param vis the visibility of this layer
	 */
	public void setVisible(boolean vis) {
		_visible = vis;

		if (_notify) {
			LayerChangeEvent event;
			if (vis)
				event = new LayerChangeEvent(this, null,
						LayerChangeEvent.LAYER_SHOWN);

			else
				event = new LayerChangeEvent(this, null,
						LayerChangeEvent.LAYER_HIDED);
			for (int i = 0; i < _listeners.size(); i++)
				((LayerChangeListener) _listeners.get(i)).layerUpdated(event);
		}
	}

	/** 
	 * Implements the LayerChnageListener.
	 * @param e ShapeChangeEvent to be processed.
	 */
	public void shapeUpdated(ShapeChangeEvent e) {
		if (_notify) {

			LayerChangeEvent event = new LayerChangeEvent(this, e,
					LayerChangeEvent.LAYER_CHANGED);

			for (int i = 0; i < _listeners.size(); i++)
				((LayerChangeListener) _listeners.get(i)).layerUpdated(event);
		}
	}

}
