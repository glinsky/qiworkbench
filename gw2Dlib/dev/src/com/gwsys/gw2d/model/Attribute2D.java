//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.model;

import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Stroke;

import com.gwsys.gw2d.event.AttributeChangeListener;

/**
 * This interface of drawing properties.
 */
public interface Attribute2D extends AttributeChangeListener
{
	/** Empty fill style. */
    public final static int FILL_STYLE_EMPTY				= 2;

    /** Solid fill style. */
    public final static int FILL_STYLE_SOLID				= 1;


	/** Solid line style. */
    public final static int LINE_STYLE_SOLID			= 1;
	
	/** Empty line style. */
    public final static int LINE_STYLE_EMPTY			= 2;
    /** Dashed line style. */
    public final static int LINE_STYLE_DASH				= 3;

    /** Dotted line style. */
    public final static int LINE_STYLE_DOT				= 4;
    /** dashed and one dotted line style. */
    public final static int LINE_STYLE_DASH_DOT			= 5;

    /** dashed and two dotted line style. */
    public final static int LINE_STYLE_DASH_DOT_DOT		= 6;

	/**
     * Adds attribute change listener.
     * @param listener	attribute change listener
     */
    public void addAttributeEventListener(AttributeChangeListener listener);
	
	/**
     * Changes the Status of notification.
     * @param notify   Status of Notification.
     */
    public void enableEventNotification(boolean notify);
	
	/**
     * Overrids method of Object.
     * @inheritDoc
     */
    public boolean equals(Object obj);
	
    /**
     * Retrives the current paint.
     * @return the current paint.
     */
	public Paint getFillPaint();
	
	public int getFillStyle();
	
	public Font getFont();
	
	public Color getLineColor();
	
	public int getLineStyle();
	
	public float getLineWidth();
	
	public Stroke getStroke();
	
	/**
	 * @inheritDoc
	 */
	public int hashCode();
	
	/**
     * Gets the current Status of notification.
     * @return    Status of Notification.
     */
    public boolean isEventNotified();
	
	/**
     * Removes attribute change listeners.
     * @param listener	attribute change listener
     */
    public void removeAttributeEventListener(AttributeChangeListener listener);
	
    /**
     * Sets the new Paint.
     * @param paint
     */
    public void setFillPaint(Paint paint);

    public void setFillStyle(int style);

    public void setFont(Font font);

	public void setLineColor(Color color);
	
    public void setLineStyle(int style);

    public void setLineWidth(float width);
}
