//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.model;

import java.awt.Color;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;
import com.gwsys.gw2d.ruler.TickDefinition;
import com.gwsys.gw2d.shape.LineShape;

import java.util.*;

/**
 * Draw grid line in the layer with given step.
 * @author li
 *
 */
public class GridLayer extends AbstractDataLayer {
	
	private boolean showHorizontalGrid = true;	
	//Wheather show the horizontal grid	line
	private boolean showVerticalGrid = true;	
	//Wheather show the vertical grid line
	
	private TickDefinition htg = null,vtg = null;	
	//the horizontal grid line and the vertical grid line definition
		
	private RenderingAttribute attr;	// the attribute of the grid line
	
	private Bound2D modelSpace;			
	
	private boolean changed = true;
	
	private ArrayList lines = new ArrayList();
	
	
	/**
	 * Constructor.
	 * @param modelSpace	Logic space.
	 * @param htg			The horizontal grid line definition.
	 * @param vtg			The vertical grid line definition.
	 */
	public GridLayer(Bound2D modelSpace,TickDefinition htg, TickDefinition vtg){
		attr.setLineStyle(Attribute2D.LINE_STYLE_SOLID);
		attr.setLineColor(Color.LIGHT_GRAY);
		this.modelSpace = modelSpace;
		this.htg = htg;
		this.vtg = vtg;
		changed = true;
	}
	
	/**
	 * Constructor.
	 * @param modelSpace	Logic space.
	 * @param attr			The attribute of the grid line.
	 * @param htg			The horizontal grid line definition.
	 * @param vtg			The vertical grid line definition.
	 */
	public GridLayer(Bound2D modelSpace,RenderingAttribute attr, TickDefinition htg, TickDefinition vtg){
		this.modelSpace = modelSpace;
		this.htg = htg;
		this.vtg = vtg;
		this.attr = attr;
		changed = true;
	}
	
	/**
	 * Set the logic space.
	 * 
	 * @param modelSpace	the new logic space
	 */
	
	public void setModelSpace(Bound2D modelSpace){
		this.modelSpace = modelSpace;
		changed = true;
	}
	
	/**
	 * 
	 * Get the current logic space.
	 * 
	 * @return the current logic space
	 */
	
	public Bound2D getModelSpace(){
		return modelSpace;
	}
	
	/**
	 * Set the horizontal tick definition.
	 * 
	 * @param htg	the new horizontal tick definition
	 */
	
	public void setHorizontalTick(TickDefinition htg){
		this.htg = htg;
		changed = true;
	}
	
	/**
	 * Get the current horizontal tick definition.
	 * 
	 * @return 	the current horizontal tick definition
	 */
	
	public TickDefinition getHorizontalTick(){
		return htg;
	}
	
	/**
	 * Set the current vertical tick definition
	 * 
	 * @param vtg 	the new vertical tick definition
	 */	
	public void setVerticalTick(TickDefinition vtg){
		this.vtg = vtg;
		changed = true;
	}
	
	/**
	 * Get the current vertical tick definition.
	 * 
	 * @return the current vertical tick definition
	 */	
	public TickDefinition getVerticalTick(){
		return vtg;
	}
	
	
	/**
	 *
	 * Set the attribute of the grid line in grid layer.
	 * 
	 * @param attr	the attribute of the grid line in grid layer. 
	 */
	public void setAttribute(RenderingAttribute attr){
		this.attr = attr;
		changed = true;
	}
	
	/**
	 * 
	 * Get the attribute of the grid line in grid layer.
	 * 
	 * @return		the attribute of the grid line in grid layer
	 */
	
	public RenderingAttribute getAttribute(){
		return attr;
	}
	
	/**
	 * 
	 * Enable (or disable) the horizontal grid line
	 * 
	 * @param hShow		The horizontal grid line will be enable, if it is true.
	 */
	public void setShowHorizontalGrid(boolean hShow){
		showHorizontalGrid = hShow;
		changed = true;
	}
	
	/**
	 * Enable (or disable) the vertical grid line
	 * 
	 * @param vShow		The vertical grid line will be enable, if it is true.
	 */
	public void setShowVerticalGrid(boolean vShow){
		showVerticalGrid = vShow;
		changed = true;
	}

	/**
	 * Get the horizontal grid line porperty.
	 * 
	 * @return The horizontal grid line is enabled if it is true.
	 */
	public boolean isShowHorizontalGrid(){
		return showHorizontalGrid;
	}
	
	/**
	 * 
	 * Get the vertical grid line porperty.
	 * 
	 * @return The vertical grid line is enabled if it is true.
	 */
	public boolean isShowVerticalGrid(){
		return showVerticalGrid;
	}
	
	/**
	 * 
	 * Get the number of shape in the layer.
	 * 
	 * @return the number of shape in the layer.
	 * 
	 */
	
	public int getTotalShapes() {
		return shapeList.size();
	}
	
	/**
	 * 
	 * Invoked by the view system to render this layer.
	 * @inheritDoc
	 * 
	 */	
	public void render(ScenePainter painter, Bound2D bound) {
		
		render(painter, bound, false);
	}
	/**
	 * 
	 * Invoked by the view system to render this layer in a preferred order
	 * 
	 */ 		
	public void render(ScenePainter painter, Bound2D bound, boolean reverse) {
		
		double pos;
		if(changed){
			lines.clear();
			//If there is horizontal tick generator
			if (htg != null  && showHorizontalGrid) {
				double min = bound.getMinY();
				double max = bound.getMaxY();
				double scale = painter.getTransformation().getScaleY();
			
				htg.setVisibleRange(min / scale, max / scale, Double.MIN_VALUE);
				
				// Set the grid lines
				while (htg.moreTicks()) {
					htg.getTickData();
					pos = htg.getTickPosition();
					LineShape line = new LineShape(modelSpace.x, pos, modelSpace.x + modelSpace.width, pos);
					line.setAttribute(attr);
					lines.add(line);
				}
			}
			// 	If there is vertical tick generator
			if (vtg != null && showVerticalGrid) {
				double min = bound.getMinX();
				double max = bound.getMaxX();
				double scale = painter.getTransformation().getScaleX();
				
				vtg.setVisibleRange(min / scale, max / scale, Double.MIN_VALUE);
				// Set the grid lines
				while (vtg.moreTicks()) {
					vtg.getTickData();
					pos = vtg.getTickPosition();
					LineShape line = new LineShape(pos, modelSpace.y, pos, modelSpace.y + modelSpace.height);
					line.setAttribute(attr);
					lines.add(line);
				}
			}
	
			changed=false;

		}
	
		Transform2D transformation = painter.getTransformation();
		
		Bound2D shapeBound;
		
		//draw the grid line in the layer
		if (reverse){
			for (int i = lines.size()-1; i >= 0 ; i--) {
				DataShape shape = (DataShape) lines.get(i);

				if (shape != null && shape.isVisible()) {
					shapeBound = shape.getBoundingBox(transformation);

					if (bound.intersects(shapeBound.x, shapeBound.y, shapeBound.width,
							shapeBound.height)) {
						painter.render(shape, bound);
					}
				}
			}
		}else{
			for (int i = 0; i < lines.size(); i++) {
				DataShape shape = (DataShape) lines.get(i);

				if (shape != null && shape.isVisible()) {
					shapeBound = shape.getBoundingBox(transformation);
					if (bound.intersects(shapeBound.x, shapeBound.y, shapeBound.width,
							shapeBound.height)) {
						painter.render(shape, bound);
					}
				}
			}
		}
		
		//draw the shapes in the layer
		if (reverse){
			for (int i = shapeList.size()-1; i >= 0 ; i--) {
				DataShape shape = (DataShape) shapeList.get(i);

				if (shape != null && shape.isVisible()) {
					shapeBound = shape.getBoundingBox(transformation);

					if (bound.intersects(shapeBound.x, shapeBound.y, shapeBound.width,
							shapeBound.height)) {

						painter.render(shape, bound);
					}
				}
			}
		}else{
			for (int i = 0; i < shapeList.size(); i++) {
				DataShape shape = (DataShape) shapeList.get(i);

				if (shape != null && shape.isVisible()) {
					shapeBound = shape.getBoundingBox(transformation);

					if (bound.intersects(shapeBound.x, shapeBound.y, shapeBound.width,
							shapeBound.height)) {

						painter.render(shape, bound);
					}
				}
			}
		}
	}

}
