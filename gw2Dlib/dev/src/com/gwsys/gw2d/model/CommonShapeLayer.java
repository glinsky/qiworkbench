//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.model;

import com.gwsys.gw2d.model.AbstractDataLayer;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;


/**
 * Implementation of DataLayer, which manages the shapes in this layer.
 * Shapes are rendered by the order of addition.
 * @author Hua
 */
public class CommonShapeLayer extends AbstractDataLayer {

	/**
	 * Constructs a layer
	 */
	public CommonShapeLayer() {
	}

	/**
	 * Gets the total number of shapes in this layer.
	 *
	 * @return the number of shapes
	 */
	public final int getTotalShapes() {
		return shapeList.size();
	}

	/**
	 * Moves the specified shape to the top of layer.
	 * @param shape 	the shape to be moved front
	 */
	public void moveShapeFront(DataShape shape) {
		// last element is rendered on top
		if (shapeList.contains(shape)) {
			shapeList.remove(shape);
			shapeList.add(shapeList.size(), shape);
		}
	}

	/**
	 * Invoked by the view system to render this layer.
	 *
	 * @param painter 	 the model renderer
	 * @param bound the region
	 */
	public void render(ScenePainter painter, Bound2D bound) {

		render(painter, bound, false);
	}

	/**
	 * Invoked by the view system to render this layer in a preferred order.
	 *
	 * @param painter 	 the model renderer
	 * @param bound 	the region
	 * @param order	 flag indicating the rendering order.
	 */
	public void render(ScenePainter painter, Bound2D bound, boolean order) {

		Transform2D transformation = painter.getTransformation();
		
		Bound2D shapeBound;
		
		if (order){
			for (int i = shapeList.size()-1; i >= 0 ; i--) {
				DataShape shape = (DataShape) shapeList.get(i);

				if (shape != null && shape.isVisible()) {
					shapeBound = shape.getBoundingBox(transformation);

					if (bound.intersects(shapeBound.x, shapeBound.y, shapeBound.width,
							shapeBound.height)) {

						painter.render(shape, bound);
					}
				}
			}
		}else{
			for (int i = 0; i < shapeList.size(); i++) {
				DataShape shape = (DataShape) shapeList.get(i);

				if (shape != null && shape.isVisible()) {
					shapeBound = shape.getBoundingBox(transformation);

					if (bound.intersects(shapeBound.x, shapeBound.y, shapeBound.width,
							shapeBound.height)) {

						painter.render(shape, bound);
					}
				}
			}
		}
		
	}

}