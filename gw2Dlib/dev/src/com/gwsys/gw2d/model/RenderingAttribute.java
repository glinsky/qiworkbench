//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.model;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Stroke;
import java.util.Vector;

import com.gwsys.gw2d.event.AttributeChangeListener;
import com.gwsys.gw2d.event.AttributeEvent;
import com.gwsys.gw2d.util.GWUtilities;

/**
 * Implementation of Attribute2D.
 *
 */
public class RenderingAttribute implements Attribute2D {

	private static int MITER_LIMIT = 5;

	private Vector eventListeners;

	private Paint fillColor = Color.black;

	private int fillStyle = FILL_STYLE_SOLID;

	private Color lineColor = Color.black;

	private int lineStyle = LINE_STYLE_SOLID;

	private float lineWidth = 1;

	private boolean notifyListeners;

	private Stroke penStroke = null;

	private Font textFont = null;//defaultFont;problem on Linux

	/**
	 * Constructs a rendering attribute.
	 */
	public RenderingAttribute() {

	}

	/**
	 * Constructs a rendering attribute with listener.
	 * 
	 * @param listener the listener to be added.
	 */
	public RenderingAttribute(AttributeChangeListener listener) {

		eventListeners = new Vector();

		eventListeners.add(listener);
	}

	/**
	 * Registers an event listener to this attribute.
	 *
	 * @param lis the event listener being added to this attribute.
	 */
	public void addAttributeEventListener(AttributeChangeListener lis) {

		if (eventListeners == null)
			eventListeners = new Vector();

		eventListeners.add(lis);
	}

	/**
	 * Handles attribute change events.
	 *
	 * @param e the attribute change event.
	 */
	public void attributeChanged(AttributeEvent e) {
		if (eventListeners != null && notifyListeners) {

			GWUtilities.info("Attribute is changed");
			for (int i = 0; i < eventListeners.size(); i++) {
				((AttributeChangeListener) eventListeners.get(i))
						.attributeChanged(e);
			}
		}

	}

	/**
	 * Sets the notification flag.
	 *
	 * @param notify the notification flag.
	 */
	public void enableEventNotification(boolean notify) {
		notifyListeners = notify;
	}

	/**
	 * Returns the fill color.
	 *
	 * @return The fill color.
	 */
	public Paint getFillPaint() {
		return fillColor;
	}

	/**
	 * Gets the fill style.
	 *
	 * @return The fill style.
	 */
	public int getFillStyle() {
		return fillStyle;
	}

	/**
	 * Gets the font associated with this attribute.
	 *
	 * @return the current font of this attribute.
	 */
	public Font getFont() {
		return textFont;
	}

	/**
	 * Gets the line color value of this attribute.
	 *
	 * @return the line color
	 */
	public Color getLineColor() {
		return lineColor;
	}

	/**
	 * Returns the line style.
	 *
	 *	@return An integer value of the line style.
	 *
	 */
	public int getLineStyle() {
		return lineStyle;
	}

	/**
	 * Returns the line width of this attribute.
	 *
	 * @return the line width.
	 */
	public float getLineWidth() {
		return lineWidth;
	}

	/**
	 * Gets the Java 2D Stroke object for this attribute.
	 *
	 * @return The Java 2D Stroke object.
	 */
	public Stroke getStroke() {
		if (penStroke == null) {
			float[] dash = null;
			switch (lineStyle) {
			case Attribute2D.LINE_STYLE_SOLID:
			case Attribute2D.LINE_STYLE_EMPTY:
				penStroke = new BasicStroke(lineWidth);
				break;
			case Attribute2D.LINE_STYLE_DOT:
				dash = new float[] { 0, 6, 0, 6 };
				penStroke = new BasicStroke(lineWidth, BasicStroke.CAP_BUTT,
						BasicStroke.JOIN_MITER, MITER_LIMIT, dash, 0);
				break;
			case Attribute2D.LINE_STYLE_DASH:
				dash = new float[] { 5 };
				penStroke = new BasicStroke(lineWidth, BasicStroke.CAP_BUTT,
						BasicStroke.JOIN_MITER, MITER_LIMIT, dash, 0);
				break;
			case Attribute2D.LINE_STYLE_DASH_DOT:
				dash = new float[] { 6, 6, 1, 6 };
				penStroke = new BasicStroke(lineWidth, BasicStroke.CAP_BUTT,
						BasicStroke.JOIN_MITER, MITER_LIMIT, dash, 0);
				break;
			case Attribute2D.LINE_STYLE_DASH_DOT_DOT:
				dash = new float[] { 6, 6, 1, 6, 1, 6 };
				penStroke = new BasicStroke(lineWidth, BasicStroke.CAP_BUTT,
						BasicStroke.JOIN_MITER, MITER_LIMIT, dash, 0);
				break;
			}
		}
		return penStroke;
	}

	/**
	 * Gets the current state of notification.
	 *
	 * @return	the current state of notification.
	 */
	public boolean isEventNotified() {
		return notifyListeners;
	}

	/**
	 * Removes an event listener.
	 *
	 * @param lis the event listener to be removed.
	 */
	public void removeAttributeEventListener(AttributeChangeListener lis) {

		if (eventListeners != null)
			eventListeners.remove(lis);
	}

	/**
	 * Sets the fill color.
	 *
	 * @param paint The fill color to be used.
	 */
	public void setFillPaint(Paint paint) {
		fillColor = paint;

		if (notifyListeners)
			attributeChanged(new AttributeEvent(
					this, paint, AttributeEvent.FILL_CHANGED));
	}

	/**
	 * Sets the fill style.
	 *
	 * @param style The fill style to be used.
	 */
	public void setFillStyle(int style) {

		fillStyle = style;

		if (notifyListeners)
			attributeChanged(new AttributeEvent(
					this, null, AttributeEvent.FILL_CHANGED));
	}

	/**
	 * Sets the font associated with this attribute.
	 *
	 * @param font The font to be used.
	 */
	public void setFont(Font font) {
		textFont = font;

		if (notifyListeners)
			attributeChanged(new AttributeEvent(
					this, font, AttributeEvent.TEXT_CHNAGED));
	}

	/**
	 * Sets the line color value of this attribute.
	 *
	 * @param color	the line color
	 */
	public void setLineColor(Color color) {
		lineColor = color;

		if (notifyListeners)
			attributeChanged(new AttributeEvent(
					this, color, AttributeEvent.STROK_CHANGED));
	}

	/**
	 * Sets the line style for this attribute.
	 *
	 * @param lineStyle the line style constant defined in Attribute2D.
	 */
	public void setLineStyle(int lineStyle) {

		penStroke = null;
		this.lineStyle = lineStyle;

		if (notifyListeners)
			attributeChanged(new AttributeEvent(
					this, null, AttributeEvent.STROK_CHANGED));
	}

	/**
	 * Sets the line width for this attribute.
	 *
	 * @param width the line width.
	 */
	public void setLineWidth(float width) {
		penStroke = null;
		lineWidth = width;

		if (notifyListeners)
			attributeChanged(new AttributeEvent(
					this, null, AttributeEvent.STROK_CHANGED));
	}

}
