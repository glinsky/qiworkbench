//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.gwsys.gw2d.util.Bound2D;

/**
 * This class is subclass of AbstractDataModel.
 * This acts as layer manager to control the layers.
 * @author Hua
 */
public class CommonDataModel extends AbstractDataModel {

	private List alayerList = new ArrayList();

	/**
	 * Constructs an empty container model.
	 *
	 */
	public CommonDataModel() {
		super();
	}

	/**
	 * Adds a layer to the container model.
	 * @param layer the layer to be added to the container model
	 */
	public void addLayer(DataLayer layer) {
		alayerList.add(layer);

		layer.addLayerEventListener(this);

		invalidateModel();
	}

	/**
	 * Returns the layer at the specified index.
	 * @param index the specified index.
	 * @return the layer
	 */

	public DataLayer getLayer(int index) {
		return (DataLayer) alayerList.get(index);
	}

	/**
	 * Gets the layers contained in this model.
	 * @return the iterator of layers contained in this model
	 */
	public Iterator getLayers() {
		return alayerList.iterator();
	}

	/**
	 * Insert a layer into this model at the specified index.
	 * @param layer the layer to be inserted into the model
	 * @param index specified location to add the layer
	 */
	public void insertLayer(DataLayer layer, int index) {
		alayerList.add(index, layer);
		layer.addLayerEventListener(this);

		invalidateModel();
	}

	/**
	 * Moves the layer specified by the index on top of
	 * other layers.
	 * @param index index of the layer to be set first
	 */
	public void moveLayerFirst(int index) {
		Object moved = alayerList.get(index);
		alayerList.remove(index);
		alayerList.add(moved);

		invalidateModel();
	}

	/**
	 * Sets the layer specified by the index as the last layer.
	 * @param index index of the layer to be set last
	 */
	public void moveLayerLast(int index) {
		Object moved = alayerList.get(index);
		alayerList.remove(index);
		alayerList.add(0, moved);
		invalidateModel();
	}

	/**
	 * Removes the specified layer from this container.
	 * @param layer the layer to be removed from the model
	 */
	public void removeLayer(DataLayer layer) {
		alayerList.remove(layer);

		layer.removeLayerEventListener(this);

		invalidateModel();
	}

	/**
	 * Invoked by the view system.
	 * @param painter the renderer contains rendering information.
	 * @param bound the bounding box to be rendered.
	 */
	public void render(ScenePainter painter, Bound2D bound) {
		render(painter, bound, false);		
	}

	/**
	 *
	 * Invoked by the view system.
	 * @param painter the renderer contains rendering information.
	 * @param bound the bounding box to be rendered.
	 * @param reversed indicates reversed order for rendering.
	 */
	public void render(ScenePainter painter, Bound2D bound, boolean reversed) {

		if (reversed) {
			for (int i = alayerList.size() - 1; i >= 0; i--) {
				DataLayer layer = (DataLayer) alayerList.get(i);

				if (layer != null && layer.isVisible()) {
					{
						layer.render(painter, bound, reversed);
					}
				}
			}
		} else {
			for (int i = 0; i < alayerList.size(); i++) {
				DataLayer layer = (DataLayer) alayerList.get(i);

				if (layer != null && layer.isVisible()) {

					layer.render(painter, bound);
				}
			}
		}
	}

	/**
	 * Swaps two layers by specifying their positions.
	 * @param index1 The first layer position.
	 * @param index2 The second layer position.
	 */
	public void swapLayers(int index1, int index2) {
		DataLayer layer1 = (DataLayer) alayerList.get(index1);
		DataLayer layer2 = (DataLayer) alayerList.get(index2);

		alayerList.set(index2, layer1);
		alayerList.set(index1, layer2);

		invalidateModel();
	}

}
