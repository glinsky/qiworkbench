//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.model;

import com.gwsys.gw2d.event.ShapeChangeListener;
import com.gwsys.gw2d.model.ScenePainter;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;

/**
 * The interface defines the shape objects.
 */
public interface DataShape {

	/**
 	 * Registers a shape event listener to this shape. 
 	 *
 	 * @param listener ShapeChangeListener
 	 */
 	public void addShapeChangeListener(ShapeChangeListener listener);

	/**
	 * Retrieves the attribute associated with this shape.
	 *
	 * @return Attribute2D.
	 */
	public Attribute2D getAttribute();

	/**
	 * Retrives the bounding area of this shape in the given transform.
	 * @param trans the given transform.
	 * @return Bound2D
	 */
	public Bound2D getBoundingBox(Transform2D trans);

	/**
	 * Returns the user specified object.
	 * @return the user specified object.
	 */
	public Object getUserData();

	/**
	 * Checks if the size of the shape is fixed. 
	 * @return The flag to indicate the scaling.
	 */
	public boolean isFixedSize();

	/**
	 * Check if the shape is visible.
	 * @return The visibility flag.
	 */
	public boolean isVisible();

 	/**
	 * Removes the ShapeChangeListener.
   	 * @param listener ShapeChangeListener
	 */
 	public void removeShapeChangeListener(ShapeChangeListener listener) ;

 	/**
	 * Draws the shape. 
	 * @param painter shape renderer object.
	 * @param bound the region to be drawn.
	 */
	public void render(ScenePainter painter, Bound2D bound);

	/**
	 * Sets the attribute associated with this shape.
	 *
	 * @param attr 	the attribute associated with this shape.
	 */
	public void setAttribute(Attribute2D attr);

	
	/**
	 * Sets the user specified object.
	 * @param data the user specified object.
	 */
	public void setUserData(Object data);
	
	/**
	 * Sets the visibility of the shape.
	 * @param vis The visibility flag.
	 */
	public void setVisible(boolean vis);

} 
