//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.shape;

import com.gwsys.gw2d.event.ShapeChangeEvent;
import com.gwsys.gw2d.event.ShapeChangeListener;
import com.gwsys.gw2d.model.Attribute2D;
import com.gwsys.gw2d.model.DataShape;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;

/**
 * Abstract implementation of DataShape handles the basic behaviors of DataShape.
 * Such as setting attribute, visible, selectable properties.
 * 
 */
public abstract class AbstractDataShape implements DataShape{
	protected float rotateAngle = 0.0f;

	/**
	 * shape attribute
	 */
	private Attribute2D attribute = null;

	/**
	 * listener of this object.
	 */
	private ShapeChangeListener shapeListener = null;
	
	/**
	 * user data associated with this object.
	 */
	private Object userData = null;
	
	private boolean visible = true, selectable= true, responsEvent=true;

	/**
	 * Registers a ShapeChangeListener. 
	 * @param listener	the event listener for this shape
	 *
	 */
	public void addShapeChangeListener(ShapeChangeListener listener) {
		shapeListener = listener;
		//only one listener for this object
	}

	/**
	 * Retrieves the attribute associated with this shape.
	 *
	 * @return Attribute2D
	 */
	public Attribute2D getAttribute() {
		return attribute;
	}

	/**
	 * Returns the bounding box of this shape.
	 *
	 * @param transform the Transform2D from model to device space
	 * @return the bounding box of the shape
	 */
	protected Bound2D getBoundingBoxWithoutRotation(Transform2D transform) {
		return getBoundingBox(transform);
	}

	/**
	 * Obtains the angle of rotation.
	 *
	 * @return the rotation angle.
	 *
	 */
	public float getRotationAngle() {
		return rotateAngle;
	}

	/**
	 * Retrieves the user data object.
	 *
	 * @return the user data object.
	 */
	public Object getUserData() {
		return userData;
	}

	/**
	 * Invalidates this shape.
	 */
	public void invalidateShape() {
		invalidateShape(this);
	}

	/**
	 * Invalidates the bounding region.
	 *
	 * @param bound	the bounding box.
	 */
	public void invalidateShape(Bound2D bound) {
		if (isNotificationEnabled()&&shapeListener != null) {
			shapeListener.shapeUpdated(new ShapeChangeEvent(
					this, bound, ShapeChangeEvent.SHAPE_CHANGED));
		}
	}

	/**
	 * Invalidates a DataShape.
	 *
	 * @param shape DataShape.
	 */
	public void invalidateShape(DataShape shape) {
		if (isNotificationEnabled()&&shapeListener != null) {
			shapeListener.shapeUpdated(new ShapeChangeEvent(
					this, shape, ShapeChangeEvent.SHAPE_CHANGED));
		}
	}

	/**
	 * @inheritDoc
	 */
	public boolean isFixedSize(){
		return false;
	}

	/**
	 * Retrieves the event notification flag. 
	 *
	 * @return 	the event notification flag. 
	 */
	public boolean isNotificationEnabled() {
		return responsEvent;
	}

	/**
	 * Checks the selectable flag.
	 *
	 * @return	the selectable flag.
	 */
	public boolean isSelectable() {
		return selectable;
	}

	/**
	 * Checks the state of the shape visibility flag.
	 *
	 * @return	the state of the shape visibility flag.
	 */
	public boolean isVisible() {
		return visible;
	}
	
	/**
	 * Removes a ShapeChangeListener from this object.
	 *
	 * @param listener	the shape event listener to remove.
	 * 
	 */
	public void removeShapeChangeListener(ShapeChangeListener listener) {
		if (shapeListener == listener || listener == null) {
			shapeListener = null;
		}
	}

	/**
	 * Sets the attribute associated with this shape.
	 * @param attr 	Attribute2D
	 */
	public void setAttribute(Attribute2D attr) {
		attribute = attr;
	}

	/**
	 * Sets the event notification flag. 
	 *
	 * @param notify the boolean notification flag.
	 */
	public void setNotification(boolean notify) {
		responsEvent = notify;
	}

	/**
	 * Sets the selectable flag.
	 *
	 * @param select the selectable flag.
	 */
	public void setSelectable(boolean select) {
		selectable = select;
	}

	/**
	 * Sets an user object associated with this shape.
	 *
	 * @param data the user data object.
	 */
	public void setUserData(Object data) {

		userData = data;
	}

	/**
	 * Sets the visibility of this shape. 
	 * @param vis the visibility of this shape. 
	 */
	public void setVisible(boolean vis) {
		visible = vis;

		if (isNotificationEnabled()&&shapeListener != null) {
			
			if (vis) {
				shapeListener.shapeUpdated(new ShapeChangeEvent(this, null,
						ShapeChangeEvent.SHAPE_SHOWN));
			} else {
				shapeListener.shapeUpdated(new ShapeChangeEvent(this, null,
						ShapeChangeEvent.SHAPE_HIDED));
			}
		}
	}

} 
