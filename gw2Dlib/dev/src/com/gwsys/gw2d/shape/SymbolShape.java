//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.shape;

import com.gwsys.gw2d.model.ScenePainter;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;

/**
 * Defines a symbol in this shape.
 * This contains SymbolPainter to draw real symbol shape.
 * 
 */
public class SymbolShape extends LocationBasedShape {
	/**
	 * The symbol painter.
	 */
	private SymbolPainter symPainter;

	/**
	 * Constructor.
	 */
	public SymbolShape() {
	}

	/**
	 * Creates a symbol shape.
	 * @inheritDoc
	 */
	public SymbolShape(double x, double y, double w, double h, int alignment,
			SymbolPainter painter, boolean fixedpoint, boolean fixedsize) {

		super(x, y, w, h, 0, alignment, fixedpoint, fixedsize);

		symPainter = painter;
	}

	/**
	 * Gets painter.
	 *
	 * @return	SymbolPainter
	 */
	public SymbolPainter getSymbolPainter() {
		return symPainter;
	}

	/**
	 * Renders the symbols.
	 *
	 * @param painter	the <i>ScenePainter</i> object
	 * @param bound	the visible area.
	 */
	public void render(ScenePainter painter, Bound2D bound) {

		if (isVisible() && getAttribute() != null && symPainter != null) {

			Transform2D superTrans = painter.getTransformation();

			painter.setAttribute(getAttribute());

			Bound2D box = getBoundingBoxWithoutRotation(superTrans);

			box = superTrans.transform(box);
			Bound2D devBound = new Bound2D(0, 0, boundWidth, boundHeight);

			symPainter.draw(painter, new Transform2D(devBound, box, false, false), this);

		}
	}

	/**
	 * Sets new painter.
	 *
	 * @param painter	SymbolPainter.
	 */
	public void setSymbolPainter(SymbolPainter painter) {

		boolean vis = isVisible();

		if (vis) {
			setVisible(false);
			invalidateShape();
		}

		symPainter = painter;
		
		setVisible(vis);

		invalidateShape();
	}

}
