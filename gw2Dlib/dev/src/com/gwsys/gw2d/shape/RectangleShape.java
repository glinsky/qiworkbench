//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.shape;

import java.awt.Shape;

import com.gwsys.gw2d.model.ScenePainter;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;

/**
 *	Creates a rectangle shape.
 *
 */
public class RectangleShape extends AbstractDataShape {
	/**  Represent the shape Rectangle2D in Java 2D*/
	private Bound2D bound;

	/**
	 * Constructs a new RectangleShape with two points at (0,0)
	 * and (1,1).
	 */
	public RectangleShape() {
		bound = new Bound2D(0, 0, 1, 1);
	}

	/**
	 * Constructs and initializes a RectangleShape from the specified 
	 * double coordinates. 
	 *
	 * @param  x1	the x coordinate of the left-top corner.
	 * @param  y1 	the y coordinate of the left-top corner.
	 * @param  x2	the x coordinate of the right-bottom corner.
	 * @param  y2  	the y coordinate of the right-bottom corner.
	 */
	public RectangleShape(double x1, double y1, double x2, double y2) {
		bound = new Bound2D(x1, y1, x2, y2);
	}

	/**
	 * Updates the new values of rectangle.
	 *
	 * @param rect	Rectangle.
	 */
	public void setRectangle(Bound2D rect) {

		boolean vis = isVisible();
		if (vis) {
			setVisible(false);
			invalidateShape();
		}
		bound = rect;
		setVisible(vis);
		invalidateShape();
	}

	/**
	 * Retrieves the coordinates of this rectangle.
	 * Used by Bhp Viewer RubberView.
	 * @return Rectangle Bound.
	 */
	public Bound2D getRectangle() {
		return bound;
	}

	/**
	 * Gets the bounding box for this rectangle. 
	 * @param trans	the transformation object
	 * @return		the Bound2D object.
	 */
	public Bound2D getBoundingBox(Transform2D trans) {
		if (rotateAngle == 0.0)
			return bound;

		Transform2D tr = new Transform2D();
		tr.rotate(rotateAngle, bound.getCenterX(), bound.getCenterY());
		return tr.transform(bound);
	}

	/**
	 * Draws this rectangle. 
	 * @param painter	a <i>ScenePainter </i> object.
	 * @param rect	the drawing bounding box.
	 */
	public void render(ScenePainter painter, Bound2D rect) {
		if (isVisible() && getAttribute() != null) {
			painter.setAttribute(getAttribute());
			Transform2D tr = new Transform2D();
			tr.rotate(rotateAngle, bound.getCenterX(), bound.getCenterY());
			Shape shape = tr.createTransformedShape(bound);
			painter.fill(shape);
			painter.draw(shape);
		}
	}
}
