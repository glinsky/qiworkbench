//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.shape;

import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.NoninvertibleTransformException;

import java.awt.Point;

/**
 * Defines the location based shape, such as Text, Image, and Symbol.
 * 
 */
public abstract class LocationBasedShape extends AbstractDataShape {
	/**
	 * Locate at the center of the bounding box.
	 */
	public static final int LOCATION_CENTER = 5;

	/**
	 * Locate at the bottom left corner of the bounding box.
	 */
	public static final int LOCATION_LEFT_BOTTOM = 2;

	/**
	 * Locate at the top left corner of the bounding box.
	 */
	public static final int LOCATION_LEFT_TOP = 1;

	/**
	 * Locate at the bottom right corner of the bounding box.
	 */
	public static final int LOCATION_RIGHT_BOTTOM = 4;

	/**
	 * Locate at the top right corner of the bounding box.
	 */
	public static final int LOCATION_RIGHT_TOP = 3;

	/** The alignment around point. */
	private int alignToPoint;

	/** Bound of this shape. */
	private Bound2D bound;

	/**
	 * Size of the bounding box.
	 */
	protected double boundWidth, boundHeight;

	/**
	 * the flag about the point and size in device space.
	 */
	private boolean fixPoint, fixSize;

	/** The anchor point. */
	private Point2D location;

	private boolean reDoCalculation;

	private Transform2D transform2d;

	/**
	 * Creates an anchored shape object with an anchor point at (0,0), zero
	 * rotation angle and fixed location in device space.
	 * 
	 */
	public LocationBasedShape() {
		location = new Point(0, 0);
		rotateAngle = 0;
		alignToPoint = LOCATION_LEFT_TOP;
		fixSize = false;
		fixPoint = true;
		reDoCalculation = true;
	}

	/**
	 * Creates an anchored shape object with the given values.
	 * 
	 * @param xLoc
	 *            the x coordinate of the anchor point
	 * @param yLoc
	 *            the y coordinate of the anchor point
	 * @param width
	 *            the width of the bounding box.
	 * @param height
	 *            the height of the bounding box.
	 * @param angle
	 *            the angle of rotation in radians.
	 * @param alignment
	 *            the defined alignment option..
	 * @param fixedLoc
	 *            the flag of fixed location or not.
	 * @param fixedSize
	 *            the flag of fixed size or not.
	 * 
	 */
	public LocationBasedShape(double xLoc, double yLoc, double width,
			double height, float angle, int alignment, boolean fixedLoc,
			boolean fixedSize) {
		location = new Point2D.Double(xLoc, yLoc);
		boundWidth = width;
		boundHeight = height;

		if (boundWidth < 0 || boundHeight < 0) {
			throw new IllegalArgumentException(
					"The width and height should not be nagative values");
		}
		alignToPoint = alignment;
		rotateAngle = angle;
		fixPoint = fixedLoc;
		fixSize = fixedSize;
		reDoCalculation = true;
	}

	/**
	 * Calculates the bounding box from user space to device space for this
	 * shape based on the alignment and transformation.
	 * 
	 * @param xLoc
	 *            the x coordinate of the anchor point.
	 * @param yLoc
	 *            the y coordinate of the anchor point.
	 * @param width
	 *            the width of the bounding box.
	 * @param height
	 *            the height of the bounding box.
	 * @param trans
	 *            the transformation.
	 * @param outBound
	 *            the output rectangle bound.
	 * 
	 * @return the bounding box of this shape.
	 */
	protected Bound2D calculateBound(double xLoc, double yLoc, double width,
			double height, Transform2D trans, Bound2D outBound) {

		if (outBound == null)
			outBound = new Bound2D(xLoc, yLoc, xLoc, yLoc);
		if (trans != null) {
			switch (alignToPoint) {

			case LOCATION_LEFT_TOP:
				if (trans.getScaleY() >= 0) {
					outBound.setRect(xLoc, yLoc, width, height);
				} else {
					outBound.setRect(xLoc, yLoc - height, width, height);
				}
				break;

			case LOCATION_LEFT_BOTTOM:
				if (trans.getScaleY() >= 0) {
					outBound.setRect(xLoc, yLoc + height, width, height);
				} else {
					outBound.setRect(xLoc, yLoc, width, height);
				}
				break;

			case LOCATION_RIGHT_TOP:
				if (trans.getScaleY() >= 0) {
					outBound.setRect(xLoc - width, yLoc, width, height);
				} else {
					outBound.setRect(xLoc - width, yLoc - height, width, height);
				}
				break;

			case LOCATION_RIGHT_BOTTOM:
				if (trans.getScaleY() >= 0) {
					outBound.setRect(xLoc - width, yLoc + height, width, height);

				} else {
					outBound.setRect(xLoc - width, yLoc, width, height);
				}
				break;

			case LOCATION_CENTER:
			default:
				outBound.setRect(xLoc - width / 2, yLoc - height / 2,
							width, height);

			}
		} 

		return outBound;
	}

	/**
	 * Retrieves the current anchor alignment value.
	 * 
	 * @return the defined alignment in this class.
	 * 
	 */
	public int getAlignment() {
		return alignToPoint;
	}

	/**
	 * Returns the bounding box of this shape based on the transformation.
	 * 
	 * @param trans
	 *            the transformation.
	 * 
	 * @return the bounding box of this shape.
	 */
	public Bound2D getBoundingBox(Transform2D trans) {

		if (reDoCalculation || bound == null || transform2d == null
				|| !(transform2d.equals(trans))) {

			reDoCalculation = (trans == null ? true : false);

			transform2d = (trans != null) ? (Transform2D) trans.clone() : null;
			double x = location.getX();
			double y = location.getY();
			bound = getBoundingBoxWithoutRotation(x, y, boundWidth, boundHeight, 
					trans, null);

			if (trans == null)
				return bound;

			Transform2D temp = new Transform2D();
			if (rotateAngle != 0.0) {
				// there are four situations
				if (!fixSize) {
					if (fixPoint) {
						Point2D point = new Point2D.Double(x, y);
						try {
							point = trans.inverseTransform(point, null);
						} catch (NoninvertibleTransformException e) {
						}
						temp.rotate(rotateAngle, point.getX(), point.getY());
					} else
						temp.rotate(rotateAngle, x, y);
					
				} else {
					if (!fixPoint) {
						Point2D point = new Point2D.Double(x, y);
						point = trans.transform(point, null);
						double angle = trans.getScaleY() >= 0 ? rotateAngle
								: -rotateAngle;
						temp.rotate(angle, point.getX(), point.getY());
					} else {
						double angle = trans.getScaleY() >= 0 ? rotateAngle
								: -rotateAngle;
						temp.rotate(angle, x, y);
					}
					bound = trans.transform(bound);	
				}
				Rectangle2D rect = temp.createTransformedShape(bound).getBounds2D();

				bound = new Bound2D(rect.getX(), rect.getY(), rect.getX()
						+ rect.getWidth(), rect.getY() + rect.getHeight());
				if (fixSize)
					bound = trans.inverseTransform(bound);
			}
		}

		return bound;
	}

	/**
	 * Returns the bounding box regardless rotation.
	 * 
	 * @param xLoc
	 *            the x coordinate of the anchor point.
	 * @param yLoc
	 *            the y coordinate of the anchor point.
	 * @param width
	 *            the width of the bounding box.
	 * @param height
	 *            the height of the bounding box.
	 * @param trans
	 *            the transform form model to device space.
	 * @param bound
	 *            the output rectangle.
	 * 
	 * @return the bounding box.
	 */
	protected Bound2D getBoundingBoxWithoutRotation(double xLoc, double yLoc,
			double width, double height, Transform2D trans, Bound2D bound) {

		if (trans == null) {
			if (bound != null) {
				bound.setRect(xLoc, yLoc, 0, 0);
				return bound;
			} else {
				return new Bound2D(xLoc, yLoc, xLoc, yLoc);
			}
		}

		if (isFixedSize()) {
			if (fixPoint) {
				Point2D src = new Point2D.Double(xLoc, yLoc);
				Point2D dst = new Point2D.Double();

				try {
					trans.inverseTransform(src, dst);
				} catch (NoninvertibleTransformException e) {
					dst = src;
				}

				xLoc = dst.getX();
				yLoc = dst.getY();
			}

			if (fixSize) {
				width /= trans.getScaleX();
				height /= trans.getScaleY();
			}			
		}
		return calculateBound(xLoc, yLoc, width, height, trans, bound);
	}

	/**
	 * Gets the bounding box of this shape without rotation.
	 * @inheritDoc
	 */
	public Bound2D getBoundingBoxWithoutRotation(Transform2D tr) {
		return getBoundingBoxWithoutRotation(location.getX(), location.getY(),
				boundWidth, boundHeight, tr, null);
	}

	/**
	 * Gets the bounding box of this shape without rotation .
	 * @return the bounding box
	 */
	public Bound2D getBoundingBoxWithoutRotation(Transform2D tr, Bound2D bound) {
		return getBoundingBoxWithoutRotation(location.getX(), location.getY(),
				boundWidth, boundHeight, tr, bound);
	}

	/**
	 * Obtains the height of the bounding box.
	 * 
	 * @return the bounding box height
	 */
	public double getHeight() {
		return boundHeight;
	}

	/**
	 * Returns the location point in device space of this shape.
	 * 
	 * @param trans
	 *            the transformation object
	 * 
	 * @return the anchor point of this shape.
	 */
	public Point2D getLocation(Transform2D trans) {
		Point2D src = new Point2D.Double(location.getX(), location.getY());
		if (!fixPoint && trans != null) {
			Point2D transPos = new Point2D.Double();
			trans.transform(src, transPos);
			return transPos;
		} else {
			return src;
		}
	}

	/**
	 * Gets the x coordinate for the anchor point.
	 * 
	 * @return the x coordinate for the point.
	 */
	public double getLocationX() {
		return location.getX();
	}

	/**
	 * Gets the y coordinate for the anchor point.
	 * 
	 * @return the y coordinate for the point.
	 */
	public double getLocationY() {
		return location.getY();
	}

	/**
	 * Gets the width of the bounding box.
	 * 
	 * @return the bounding box width
	 */
	public double getWidth() {
		return boundWidth;
	}

	/**
	 * Checks the flag of Calculation.
	 * @return the flag of Calculation.
	 */
	protected boolean isDoCalculation(){
		return reDoCalculation;
	}
	
	/**
	 * Checks if the shape's size is fixed in device space
	 * 
	 * @return status of location size.
	 */
	public boolean isFixedDimension() {
		return fixSize;
	}

	/**
	 * Checks if the shape's location is fixed in device space
	 * 
	 * @return status of location point.
	 */
	public boolean isFixedLocation() {
		return fixPoint;
	}

	/**
	 * Checks if the geometry of shape is fixed in device space.
	 * 
	 * @return <i>true</i> if both point and size are fixed.
	 */
	public boolean isFixedSize() {
		return (fixPoint || fixSize);
	}

	/**
	 * Setss the alignment.
	 * 
	 * @param alignment
	 *            the alignment defined in this class.
	 * 
	 */
	public void setAlignment(int alignment) {
		alignToPoint = alignment;
	}

	/**
	 * Sets the falg to calculate the bound.
	 * 
	 */
	protected void setCalculationFlag(boolean redo) {
		reDoCalculation = redo;
	}

	/**
	 * Sets the value of the location point.
	 * 
	 * @param x
	 *            the x coordinate of the anchor point.
	 * @param y
	 *            the y coordinate of the anchor point.
	 */
	public void setLocation(double x, double y) {
		setValues(x, y, true);

	}

	/**
	 * Sets the width and height of this shape.
	 * 
	 * @param w
	 *            the width of the bounding box
	 * @param h
	 *            the height of the bounding box
	 */
	public void setSize(double w, double h) {
		setValues(w, h, false);
	}

	private void setValues(double wx, double hy, boolean isLoc) {
		if (isLoc) {
			if ((location.getX() == wx) && (location.getY() == hy))
				return;
		} else {
			if ((boundWidth == wx) && (boundHeight == hy))
				return;
		}

		boolean vis = isVisible();

		if (vis) {
			setVisible(false);
			invalidateShape();
		}
		if (isLoc) {
			location.setLocation(wx, hy);
		} else {
			boundWidth = wx;
			boundHeight = hy;
		}
		reDoCalculation = true;

		setVisible(vis);

		invalidateShape();
	}
}
