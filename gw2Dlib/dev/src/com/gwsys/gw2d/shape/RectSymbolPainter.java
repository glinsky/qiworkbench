//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.shape;

import java.awt.geom.Rectangle2D;

import com.gwsys.gw2d.model.DataShape;
import com.gwsys.gw2d.model.ScenePainter;
import com.gwsys.gw2d.util.Transform2D;

/**
 * Creates a painter to draw a rectangle symbol.
 * @author Hua
 *
 */
public class RectSymbolPainter implements SymbolPainter {
	/**
	 * Constructor.
	 *
	 */
	public RectSymbolPainter(){
		
	}
	/**
	 * method in SymbolPainter
	 */
	public void draw(ScenePainter painter, Transform2D tr, DataShape shape) {
		LocationBasedShape base = (LocationBasedShape)shape;

		Rectangle2D line = new Rectangle2D.Double(
				base.getLocationX(), base.getLocationY(), base.getWidth(),base.getHeight());
		painter.fill(line);
		painter.draw(line);
		
	}
}
