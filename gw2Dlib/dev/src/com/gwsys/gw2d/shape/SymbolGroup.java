//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.shape;

import com.gwsys.gw2d.model.ScenePainter;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;

/**
 * SymbolGroup contains a set of same type of Symbols.
 * 
 * @author Hua
 */
public class SymbolGroup extends SymbolShape {

	/**
	 * bound values
	 */
	private Bound2D modBound;

	/**
	 * Array of X coordinate points.
	 */
	protected double[] coordsX;

	/**
	 * Array of Y coordinate points.
	 */
	protected double[] coordsY;

	/**
	 * Number of points
	 */
	private int numPoints;

	/**
	 *  Constructs symbols of a group at the given locations. 
	 *  @param xs Array defined x coordinates.
	 *  @param ys Array defined y coordinates.
	 *  @inheritDoc
	 * 	@see SymbolShape
	 */
	public SymbolGroup(double[] xs, double[] ys, int number, double w, double h,
			int alignment, SymbolPainter painter, boolean fixedpoint,
			boolean fixedsize) {

		super(0, 0, w, h, alignment, painter, fixedpoint, fixedsize);

		coordsX = new double[number];
		coordsY = new double[number];
		numPoints =  number;
		for (int i=0; i<number; i++){
			coordsX[i]=xs[i];
			coordsY[i]=ys[i];
		}
		calculateBound();
	}

	/**
	 * Calculate Bound.
	 */
	private void calculateBound() {
		double minX=0, maxX=0, minY=0, maxY=0;
		for (int i = 0; i < numPoints; i++) {
			if (i == 0 || minX < coordsX[i])
				minX = coordsX[i];
			if (i == 0 || maxX > coordsX[i])
				maxX = coordsX[i];
			if (i == 0 || minY < coordsY[i])
				minY = coordsY[i];
			if (i == 0 || maxY > coordsY[i])
				maxY = coordsY[i];
		}
		modBound = new Bound2D(minX, minY, maxX, maxY);
		setCalculationFlag(false);
	}

	/**
	 * Returns the bounding box of this symbol. 
	 * @param tr the transformation.
	 *
	 * @return the bounding box of symbol.
	 */
	public Bound2D getBoundingBox(Transform2D tr) {
		calculateBound();

		if (tr == null)
			return modBound;

		if (isFixedLocation()) {
			modBound = tr.inverseTransform(modBound);			
		}

		Bound2D symbolRect = calculateBound(0, 0, boundWidth, boundHeight, tr, null);

		if (isFixedDimension()) {
			Transform2D temp = new Transform2D();
			temp.setToScale(1.0 / tr.getScaleX(), 1.0 / tr.getScaleY());
			symbolRect = temp.transform(symbolRect);
		} 

		return new Bound2D(modBound.getX() + symbolRect.getMinX(), 
				modBound.getY() + symbolRect.getMinY(), 
				modBound.getX()	+ modBound.getWidth() + symbolRect.getMaxX(), 
				modBound.getY() + modBound.getHeight() + symbolRect.getMaxY());
	}

	/**
	 * Gets the number of points used to define the shape.
	 * @return number of points.
	 */
	public int getSize() {
		return numPoints;
	}

	/**
	 * Render the symbols.
	 *
	 * @param painter	ScenePainter.
	 * @param bound	the visible area.
	 */
	public void render(ScenePainter painter, Bound2D bound) {

		if (isVisible() && getAttribute() != null && getSymbolPainter() != null) {
			Transform2D temp = painter.getTransformation();

			painter.setTransformation(new Transform2D());
			
			painter.setAttribute(getAttribute());

			Bound2D devBound = new Bound2D(0, 0, boundWidth, boundHeight);

			for (int i = 0; i < numPoints; i++) {
				setCalculationFlag(true);
				Bound2D box = getBoundingBoxWithoutRotation(coordsX[i], coordsY[i],
						boundWidth, boundHeight, temp, null);
				
				getSymbolPainter().draw(painter, 
						new Transform2D(devBound, temp.transform(box), false, false), this);
			}

			painter.setTransformation(temp);
		}
	}
}
