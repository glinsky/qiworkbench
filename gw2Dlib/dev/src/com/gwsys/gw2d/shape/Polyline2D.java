//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.shape;

import com.gwsys.gw2d.model.ScenePainter;
import com.gwsys.gw2d.util.Bound2D;

/**
 * Implements Polyline to links all points together.
 * 
 */
public class Polyline2D extends PointGroup {
	/**
	 * Constructs a new polyline using an x and y array.
	 * @param number The number of points.
	 * @param xpoint The X coordinates array.
	 * @param ypoint The Y coordinates array.
	 */
	public Polyline2D(int number, double[] xpoint, double[] ypoint) {
		numPoints = number;

		arrayX = new double[numPoints];
		arrayY = new double[numPoints];
		for (int i = 0; i < numPoints; i++) {
			arrayX[i] = xpoint[i];
			arrayY[i] = ypoint[i];
		}
	}

	/**
	 * Creates a clone of itself. Used by BhpEditorController.
	 * @return a clone of this shape.
	 */
	public Object getGeometry() {
		Polyline2D poly2d = new Polyline2D(
				numPoints, arrayX, arrayY);
		return poly2d;
	}

	/**
	 * Renders the shape.
	 * @param painter 		ScenePainter.
	 * @param bound 		the visible bound.
	 */
	public void render(ScenePainter painter, Bound2D bound) {
		painter.setAttribute(getAttribute());
		painter.draw(getRotatedShape());
	}

}
