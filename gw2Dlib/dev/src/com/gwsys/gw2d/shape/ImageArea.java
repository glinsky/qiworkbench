//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.shape;

import com.gwsys.gw2d.model.ScenePainter;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;

import java.awt.Graphics2D;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

/**
 * The data shape container BufferedImage.
 * 
 */
public class ImageArea extends LocationBasedShape {
	/** The buffered image in this shape */
	protected BufferedImage _image;

	/**
	 * Creates ImageArea.
	 * 
	 * @param x
	 *            the x coordinate of the anchor point.
	 * @param y
	 *            the y coordinate of the anchor point.
	 * @param w
	 *            the width of the bounding box.
	 * @param h
	 *            the height of the bounding box.
	 * @param angle
	 *            the angle of rotation in radians.
	 * @param alignment
	 *            the alignment option.
	 * @param image
	 *            the BufferedImage to be displayed.
	 * @param fixedLoc
	 *            <i>true</i> if the location is fixed in device space;
	 *            <i>false</i> otherwise.
	 * @param fixedsize
	 *            <i>true</i> if size is fixed; <i>false</i> otherwise.
	 * 
	 */
	public ImageArea(double x, double y, double w, double h, float angle,
			int alignment, BufferedImage image, boolean fixedLoc,
			boolean fixedsize) {
		super(x, y, w, h, angle, alignment, fixedLoc, fixedsize);
		_image = image;
	}

	/**
	 * Creates ImageArea.
	 * 
	 * @param x
	 *            the x coordinate of the anchor point.
	 * @param y
	 *            the y coordinate of the anchor point.
	 * @param w
	 *            the width of the bounding box.
	 * @param h
	 *            the height of the bounding box.
	 * @param image
	 *            the BufferedImage to be displayed.
	 * @param fixedLoc
	 *            <i>true</i> if the location is fixed in device space;
	 *            <i>false</i> otherwise.
	 * @param fixedsize
	 *            <i>true</i> if size is fixed; <i>false</i> otherwise.
	 */
	public ImageArea(double x, double y, double w, double h,
			BufferedImage image, boolean fixedLoc, boolean fixedsize) {
		this(x, y, w, h, 0, LOCATION_LEFT_TOP, image, fixedLoc, fixedsize);
	}

	/**
	 * Image associated with this DataShape.
	 * 
	 * @return Image associated with this DataShape.
	 */
	public BufferedImage getImage() {
		return _image;
	}

	/**
	 * Renders this shape into painter.
	 * 
	 * @param painter
	 *            the shape renderer
	 * @param visibleBound
	 *            the bounding box
	 */
	public void render(ScenePainter painter, Bound2D visibleBound) {
		if (isVisible() && getAttribute() != null && _image != null) {

			painter.setAttribute(getAttribute());

			Bound2D box = getBoundingBoxWithoutRotation(painter
					.getTransformation());
			Transform2D t2d = painter.getTransformation();
			box = t2d.transform(box);
			Bound2D modelBound = new Bound2D(0, 0, _image.getWidth(), _image
					.getHeight());
			Transform2D transform2d = new Transform2D(modelBound, box, false,
					false);
			Graphics2D g2d = painter.getGraphics();
			
			if (isFixedDimension()) {
				Point2D modLoc = getLocation(t2d);

				double angle = t2d.getScaleY() >= 0 ? getRotationAngle()
						: -getRotationAngle();

				g2d.rotate(angle, modLoc.getX(), modLoc.getY());
				g2d.transform(transform2d);		

			} else {
				Bound2D dBox = transform2d.transform(modelBound);
				Bound2D mBox = t2d.inverseTransform(dBox);
				if (t2d.getScaleY() < 0) { 
					mBox.y += mBox.height;
					mBox.height = -mBox.height;
				}
				double xLoc = getLocationX();
				double yLoc = getLocationY();
				if (isFixedLocation() ){

					Point2D devLoc = new Point2D.Double(xLoc, yLoc);
					Point2D modLoc = new Point2D.Double();
					try {
						modLoc = t2d.inverseTransform(devLoc, modLoc);
						xLoc = modLoc.getX();
						yLoc = modLoc.getY();
					} catch (NoninvertibleTransformException e) {
					}
				}				
				g2d.transform(t2d);
				g2d.rotate(getRotationAngle(), xLoc, yLoc);
				g2d.transform(new Transform2D(modelBound, mBox, false, false));
			}
			g2d.drawImage(_image, 0, 0, null);
			g2d.setTransform(painter.getAffineTransform());
		}
	}

}