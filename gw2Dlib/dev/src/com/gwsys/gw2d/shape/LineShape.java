//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.shape;

import java.awt.geom.Line2D;
import java.awt.Shape;

import com.gwsys.gw2d.model.ScenePainter;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;

/**
 * A shape adapter represents Line2D defined in Java2D API.
 *
 */
public class LineShape extends AbstractDataShape {
	private Line2D.Double j2dLine;

	/**
	 * Constructs Line2D.Double.
	 */
	public LineShape() {
		j2dLine = new Line2D.Double();
	}

	/**
	 * Constructs Line2D.Double.
	 *
	 * @param x1	the x1 coordinate
	 * @param y1	the y1 coordinate
	 * @param x2	the x2 coordinate
	 * @param y2	the y2 coordinate
	 *
	 */
	public LineShape(double x1, double y1, double x2, double y2) {
		j2dLine = new Line2D.Double(x1, y1, x2, y2);
	}

	/**
	 * Calculates the bound.
	 * @return Bound2D
	 */
	private Bound2D calculateBound() {
		double x1 = j2dLine.getX1();
		double x2 = j2dLine.getX2();
		double y1 = j2dLine.getY1();
		double y2 = j2dLine.getY2();
		if (x1 == x2) {
			x1 += 0.000001;
		}
		if (y1 == y2) {
			y1 += 0.000001;
		}

		return new Bound2D(x1, y1, x2, y2);
	}

	/**
	 * Gets the bounding box of the shape.
	 *
	 * @param  trans	Transform2D
	 *
	 * @return	Bound2D.
	 */
	public Bound2D getBoundingBox(Transform2D trans) {
		Bound2D bbox = calculateBound();
		if (rotateAngle == 0.)
			return bbox;

		Transform2D tr = new Transform2D();
		tr.rotate(rotateAngle, bbox.getCenterX(), bbox.getCenterY());
		return tr.transform(bbox);
	}

	/**
	 * Returns the bounding box in model space without rotation.
	 *
	 * @param tr Transform2D.
	 * @return the bounding box of the shape.
	 */
	protected Bound2D getBoundingBoxWithoutRotation(Transform2D tr) {
		return calculateBound();
	}
	
	/**
	 * Gets the rotated shape.
	 *
	 * @return rotated Shape
	 */
	private Shape getRotatedShape() {
		if (rotateAngle == 0.0)
			return j2dLine;

		Bound2D bound = calculateBound();
		Transform2D tr = new Transform2D();
		tr.rotate(rotateAngle, bound.getCenterX(), bound.getCenterY());
		return tr.createTransformedShape(j2dLine);
	}

	/**
	 * Renders the shape.
	 *
	 * @param  painter 	ScenePainter
	 * @param  bound	The visible bound.
	 */
	public void render(ScenePainter painter, Bound2D bound) {
		if (isVisible() && getAttribute() != null) {
			painter.setAttribute(getAttribute());
			painter.draw(getRotatedShape());
		}
	}

	/**
	 * Sets the Line with the specified values.
	 *
	 * @param x1	the x1 coordinate
	 * @param y1	the y1 coordinate
	 * @param x2	the x2 coordinate
	 * @param y2	the y2 coordinate
	 */
	public void setLine(double x1, double y1, double x2, double y2) {
		boolean vis = isVisible();
		if (vis) {
			setVisible(false);
			invalidateShape();
		}
		j2dLine.setLine(x1, y1, x2, y2);
		setVisible(vis);
		invalidateShape();
	}

}