//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.shape;

import java.awt.Shape;
import java.awt.geom.GeneralPath;

import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;

/**
 * The shape object is defined by points.
 * Those points constructe java.awt.Shape.
 * 
 */
public abstract class PointGroup extends AbstractDataShape {
	
	/**
	 * Flag to set bounding box recalculation.  
	 */
	private boolean reDo = true;
	/**
	 * The array used to define the X coordinates of all the points.
	 */
	protected double[] arrayX = null;

	/**
	 * The array used to define the Y coordinates of all the points.
	 */
	protected double[] arrayY = null;

	/**
	 * The bounding box of this shape.
	 */
	protected Bound2D bound = new Bound2D();

	/**
	 * The number of points.
	 */
	protected int numPoints = 0;

	private GeneralPath pointPath = new GeneralPath();

	/**
	 * Constructor.
	 */
	public PointGroup() {
	}

	/**
	 * Gets the bounding box.
	 * @param tr 	the transformation object.
	 * @return Bound2D
	 */
	public Bound2D getBoundingBox(Transform2D tr) {
		getBoundingBoxWithoutRotation(tr);

		if (rotateAngle == 0.0)
			return bound;

		Transform2D t = new Transform2D();
		t.rotate(rotateAngle, bound.getCenterX(), bound.getCenterY());

		return t.transform(bound);
	}

	/**
	 * Returns the bounding box of the data in model space without rotation.
	 *
	 * @param tr the transformation from model to device space.
	 * @return the bounding box of the shape
	 */
	protected Bound2D getBoundingBoxWithoutRotation(Transform2D tr) {
		if (!reDo)
			return bound;

		double minx = Double.MAX_VALUE;
		double miny = Double.MAX_VALUE;
		double maxx = -1. * Double.MAX_VALUE;
		double maxy = -1. * Double.MAX_VALUE;
		double x, y;

		for (int i = 0; i < numPoints; i++) {
			x = arrayX[i];
			y = arrayY[i];
			if (x < minx)
				minx = x;
			if (x > maxx)
				maxx = x;

			if (y < miny)
				miny = y;
			if (y > maxy)
				maxy = y;
		}
		final double EPS = 0.00001;
		if (minx >= maxx - EPS) { 
			maxx += EPS;
			if (numPoints > 0)
				arrayX[0] += EPS;
		}
		if (miny >= maxy - EPS) {
			maxy += EPS;
			if (numPoints > 0)
				arrayY[0] += EPS;
		}
		bound.setFrameFromDiagonal(minx, miny, maxx, maxy);

		reDo = false;
		return bound;
	}

	/**
	 * Gets the rotated java2d shape.
	 * @return the rotated shape object
	 */
	protected Shape getRotatedShape() {
		pointPath=new GeneralPath();
		pointPath.moveTo((float)arrayX[0], (float)arrayY[0]);
		for (int i=1; i<numPoints; i++){
			pointPath.lineTo((float)arrayX[i], (float)arrayY[i]);
		}
		if (rotateAngle == 0.0)
			return pointPath;

		Transform2D tr = new Transform2D();
		tr.rotate(rotateAngle, bound.getCenterX(), bound.getCenterY());

		return tr.createTransformedShape(pointPath); 
	}


	/**
	 * Gets the number of points.
	 * @return number of points.
	 */
	public int getSize() {
		return numPoints;
	}

	/**
	 * Gets the X coordinate of the point at given index. 
	 *
	 * @param index The index of point.
	 *
	 * @return the X coordinate value of the point.
	 */
	public double getXAt(int index) {
		if (validIndex(index) == false) {
			return 0.0;
		}
		return arrayX[index];
	}

	/**
	 * Gets the Y coordinate of the point at given index. 

	 * @param index The index of point.
	 * @return the Y coordinate value of the point.
	 */
	public double getYAt(int index) {
		if (validIndex(index) == false) {
			return 0.0;
		}
		return arrayY[index];
	}

	/**
	 * Sets the geometry.
	 * @param number the number of points in the group.
	 * @param xpoint the X coordinate of the points in the group
	 * @param ypoint the Y coordinate of the points in the group
	 */
	public void setCoordinates(int number, double[] xpoint, double[] ypoint) {
		boolean vis = isVisible();
		if (vis) {
			setVisible(false);
			invalidateShape();
		}

		numPoints = number;

		arrayX = new double[numPoints];
		arrayY = new double[numPoints];
		for (int i = 0; i < numPoints; i++) {
			arrayX[i] = xpoint[i];
			arrayY[i] = ypoint[i];
		}

		reDo = true;

		setVisible(vis);

		invalidateShape();
	}

	/**
	 * Moves the point to new location with given values.
	 * @param index 	the index of point.
	 * @param x 		the X coordinate.
	 * @param y 		the Y coordinate.
	 * @return <i>true</i> if successful;
	 *						<i>false</i> otherwise.
	 */
	public boolean setPoint(int index, double x, double y) {
		if (validIndex(index) == false) {
			return false;
		}

		boolean vis = isVisible();
		if (vis) {
			setVisible(false);
			invalidateShape();
		}

		arrayX[index] = x;
		arrayY[index] = y;

		reDo = true;

		setVisible(vis);

		invalidateShape();
		return true;
	}

	/**
	 * Checks if the index is a valid value.
	 * @param index 	the index value.
	 * @return true if valid; false otherwise.
	 */
	private boolean validIndex(int index) {

		if (arrayX == null || arrayY == null || index < 0 ||
				index >= arrayX.length || index >= arrayY.length) {
			return false;
		}

		return true;
	}

}
