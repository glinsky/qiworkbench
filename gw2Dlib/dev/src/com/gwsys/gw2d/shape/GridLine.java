//
// gw2DLib - a devkit for a 2D seismic viewer
// This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
// and distributed by BHP Billiton Petroleum under license. 
//
// This program is free software; you can redistribute it and/or modify it 
// under the terms of the GNU General Public License Version 2 as as published 
// by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
// more details.
// 
// You should have received a copy of the GNU General Public License along with 
// this program; if not, write to the Free Software Foundation, Inc., 
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// or visit the link http://www.gnu.org/licenses/gpl.txt.
//
// To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
// or visit http://qiworkbench.org to learn more.
//
package com.gwsys.gw2d.shape;

import com.gwsys.gw2d.model.DataShape;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.gw2d.model.ScenePainter;
import com.gwsys.gw2d.ruler.TickDefinition;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;

/**
 * The grid line is a special line shape. It utilizes the tick definition
 * from axis to draw lines n view panel.
 * @author Hua
 *
 */
public class GridLine extends AbstractDataShape {

	private TickDefinition horTick;

	private TickDefinition verTick;

	private Bound2D bound;

	private RenderingAttribute defaultAttr = new RenderingAttribute();

	/**
	 * Constructor.
	 */
	public GridLine(Bound2D bound, TickDefinition htick, TickDefinition vtick) {
		super();
		this.bound = bound;
		horTick = htick;
		verTick = vtick;
		horTick.setModelRange(bound.getMinY(), bound.getMaxY());
		verTick.setModelRange(bound.getMinX(), bound.getMaxX());
	}

	/**
	 * @inheritDoc
	 */
	public Bound2D getBoundingBox(Transform2D tr) {
		return bound;
	}

	/**
	 * @inheritDoc
	 */
	public void render(ScenePainter painter, Bound2D visibleBound) {
		double pos;
		DataShape shape;

		if (isVisible()) {

			// Check if the shape inside visible area
			if (!bound.intersects(visibleBound.x, visibleBound.y, 
					visibleBound.width, visibleBound.height))
				return;

			Bound2D intersectedBound = bound.intersection(visibleBound);

			//horizontal tick 
			if (horTick != null) {
				double min = intersectedBound.getMinY();
				double max = intersectedBound.getMaxY();
				double scale = painter.getTransformation().getScaleY();

				horTick.setVisibleRange(min / scale, max / scale, scale);

				while (horTick.moreTicks()) {
					pos = horTick.getTickPosition();
					shape = new LineShape(bound.x, pos, bound.x + bound.width, pos);
					if (getAttribute() != null)
						shape.setAttribute(getAttribute());
					else
						shape.setAttribute(defaultAttr);
					painter.render(shape, visibleBound);
				}
			}

			//vertical tick
			if (verTick != null) {
				double min = intersectedBound.getMinX();
				double max = intersectedBound.getMaxX();
				double scale = painter.getTransformation().getScaleX();

				verTick.setVisibleRange(min / scale, max / scale, scale);

				while (verTick.moreTicks()) {
					pos = verTick.getTickPosition();

					shape = new LineShape(pos, visibleBound.y, pos, visibleBound.y + visibleBound.height);
					if (getAttribute() != null)
						shape.setAttribute(getAttribute());
					else
						shape.setAttribute(defaultAttr);
					painter.render(shape, visibleBound);
						
				}
			}
		}
	}

	/**
	 * Sets the new data bound.
	 * @param bound     the new bound.
	 * @param repaint	flag to repaint.
	 * @throws IllegalArgumentException if null.
	 */
	public void setBoundingBox(Bound2D bound, boolean repaint) {

		if (bound == null)
			throw new IllegalArgumentException(
					"The bound parameter can not be null");

		this.bound = bound;

		if (repaint)
			invalidateShape();
	}
}
