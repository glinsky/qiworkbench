package com.bhpb.devtools;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class WindowCloseAdapter extends WindowAdapter {
    private DialogApp app;

    public WindowCloseAdapter(DialogApp app) {
        this.app = app;
    }

    public void windowClosing(WindowEvent we) {
        System.out.println("Invoked WindowClosing.");
        app.exitApp();
    }
}
