/*
 * StreamConnector.java
 *
 * Created on September 17, 2007, 10:27 AM
 *
 * Reads from an BufferedInputStream and writes to a BufferedOutputStream,
 * sleeping for an optionally specified interval when blocked.
 *
 */

package com.bhpb.devtools.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * @author folsw9
 */
public class StreamConnector implements Runnable {
    private static final int DEFAULT_BLOCKED_SLEEP_TIME = 100; // sleep 100 milliseconds when blocked
    private static final int MAX_BLOCKED_SLEEP_TIME = 10000;   // wait for 10 seconds at most
    
    private boolean copyCompleted = false;
    private boolean normalTermination = false;
    private BufferedInputStream inStream;
    private BufferedOutputStream outStream;
    private Logger logger = Logger.getLogger(StreamConnector.class.toString());
    private long bytesRead = 0;
    private long totalBytesRead = 0;
    private long fileSize;
    private long totalWaitTime = 0;
    private String exceptionMessage = "No exception occurred";
    
    /** Creates a new instance of StreamConnector */
    public StreamConnector(BufferedInputStream inStream, BufferedOutputStream outStream, long fileSize) {
        if (inStream == null)
            throw new IllegalArgumentException("Cannot initialize StreamConnector with null BufferedInputStream");
        if (outStream == null)
            throw new IllegalArgumentException("Cannot initialize StreamConnector with null BufferedOutputStream");
        if (fileSize == 0)
            throw new IllegalArgumentException("Cannot initialize StreamConnector with 0-length fileSize");
        
        this.inStream = inStream;
        this.outStream = outStream;
        this.fileSize = fileSize;
    }
    
     /**
     * Copies a file from one BufferedInputStream to another, indicating to the user when the inputstream is blocked.
     * Note that this method does NOT close the streams even when handling an exception.
     *
     * @param in InputStream of the source file
     * @param out OutputStream of the destination file
     * @param flen the size of the file being copied
     */
    public synchronized void run() {
        try {
            do {
                int bytesAvailable = inStream.available();
 
                if (bytesAvailable > 0) {
                    byte[] byteBuffer = new byte[bytesAvailable];
                
                    inStream.read(byteBuffer);
                    outStream.write(byteBuffer);
            
                    bytesRead += bytesAvailable;
                    totalBytesRead += bytesAvailable;
                } else {
                    logger.fine("No bytes Available");
                    logger.fine("Update descriptor or jar inputstream appears to be blocked... waiting 100 milleseconds...");
                    totalWaitTime += DEFAULT_BLOCKED_SLEEP_TIME;
            
                    try {
                        Thread.sleep(DEFAULT_BLOCKED_SLEEP_TIME);
                    } catch (InterruptedException ie) {
                        Thread.interrupted();
                    }
                }
                Thread.yield();
            } while (hasMoreInputData() && (totalWaitTime < MAX_BLOCKED_SLEEP_TIME));
        
            copyCompleted = true;
            normalTermination = true;
        } catch (IOException ioe) {
            logger.warning("IOException occurred: " + ioe);
            exceptionMessage = ioe.getMessage();
            copyCompleted = true;
            normalTermination = false;
        }
    }
    
    /**
     * @return the number of bytes read since the last call to this method
     */
    public synchronized long getBytesReadSinceLastCheck() {
        long bytesSinceLastCheck = bytesRead;
        bytesRead = 0;
        return bytesSinceLastCheck;
    }
    
    private synchronized boolean hasMoreInputData() {
        return totalBytesRead < fileSize;
    }
    
    public synchronized boolean isCopyCompleted() {
        return copyCompleted;
    }
    
    public synchronized boolean isNormalTermination() {
        return normalTermination;
    }
    
    public String getExceptionMessage() {
        return exceptionMessage;
    }
}
