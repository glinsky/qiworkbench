/*
 * CommandLineJobImpl.java
 *
 * Created on September 12, 2007, 9:47 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.devtools.util;

import com.bhpb.devtools.util.CommandLineJob.STREAM;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author folsw9
 */
public class CommandLineJobImpl implements CommandLineJob {
    protected BufferedReader stderrReader = null;
    protected BufferedReader stdoutReader = null;
    protected Process process = null;
    //changed from Runnable to specific implementation because the Mock needs to access the StreamToStringListConverter
    //directly as there is not process on which to waitFor()
    protected StreamToStringListConverter stdoutWorker = null;
    protected StreamToStringListConverter stderrWorker = null;

    protected volatile List<String> stdoutText = new LinkedList();
    protected volatile List<String> stderrText = new LinkedList();
    
    private String[] commandAndArguments = null;
    
    /** Creates a new instance of CommandLineJobImpl */
    public CommandLineJobImpl(String[] commandAndArguments) {
        if ((commandAndArguments == null) || (commandAndArguments.length == 0))
            throw new IllegalArgumentException("commandAndArguments array must be non-null and of length > 0");
        this.commandAndArguments = commandAndArguments;
    }
    
    /**
     * Returns true if and only if the line at (0-based) a index has been read from stream.
     */
    public boolean isLineAvailable(STREAM stream, int index) {
        return index < getNumLines(stream);
    }
    
    public String getCommandAndArguments() {
        StringBuffer sbuf = new StringBuffer();
        boolean isCmd = true; // true only for the first element of commandAndArguments[], used for formatting the String representation of the process
        for (String cmdToken : commandAndArguments) {
            if (isCmd == false) {
                sbuf.append(" ");
            }
            sbuf.append(cmdToken);
            isCmd = false; // the next element of commandAndArguments[] is an argument, not a command
        }
        return sbuf.toString();
    }
    
    public String getLineByIndex(STREAM stream, int index) {
        List<String> textLines = getLineListByStream(stream);
        synchronized (textLines) {
            return textLines.get(index);
        }
    }
    
    /**
     * @param stream Enum indicating which stream's data to scan by line for matches
     * @param regex regular expression to match
     *
     * @return String[] containing every line read from stream matching regex
     */
    public String[] getLinesMatching(STREAM stream, String regex) {
        List<String> textLines = getLineListByStream(stream);
        List<String> resultSet = new LinkedList();
        
        synchronized (textLines) {
            Pattern p = Pattern.compile(regex);
            Iterator<String> iter = textLines.iterator();
            while (iter.hasNext()) {
                String line = iter.next();
                Matcher m = p.matcher(line); // get a matcher object
                if (m.find())
                    resultSet.add(line);
            }
        }
        
        return resultSet.toArray(new String[0]);
    }
    
    public int getNumLines(STREAM stream) {
        List<String> streamText = getLineListByStream(stream);
        synchronized (streamText) {
            return streamText.size();
        }
    }
    
    /**
     * Depending on the platform, sufficiently verbose stdout/stderr output from the system command
     * may cause this method to deadlock.  If this occurs, a CommandLineJobImpl that reads from an output file should be used instead.
     */
    public void start() throws IOException {
        process = new ProcessBuilder(commandAndArguments).start();
        try {
            setStreamReader(STREAM.STDOUT, new BufferedReader(new InputStreamReader(process.getInputStream())));
            setStreamReader(STREAM.STDERR, new BufferedReader(new InputStreamReader(process.getErrorStream())));
        
            stdoutWorker = new StreamToStringListConverter(stdoutReader, stdoutText);
            stderrWorker = new StreamToStringListConverter(stderrReader, stderrText);
          
            new Thread(stdoutWorker).start();
			
            new Thread(stderrWorker).start();

        } finally {
            try {
				//If the worker was not initialized but the buffered stream reader was created, close it
				if ((stdoutWorker == null) && (stdoutReader != null))
					stdoutReader.close();
            } finally { // cascaded in case the attempt to close the first stream throws an IOException
                //If the worker was not initialized but the buffered stream reader was created, close it
                if ((stderrWorker == null) && (stderrReader != null))
                    stderrReader.close();
            }
        }
    }
    
    /**
     * Block until the process has completed or delay*iterations milliseconds has elapsed
     * @return false if process was not initialized prior to expiration of delay, otherwise block until
     *  process completes then returns true
     */
    public boolean waitFor(int delay, int iterations) throws InterruptedException {
        int elapsedIterations = 0;
        while ((process == null) && (elapsedIterations < iterations)){
            try {
                Thread.sleep(delay);
            } catch (InterruptedException ie) {
                Thread.interrupted();
            }
            elapsedIterations++;
        }
        if (process != null) {
            process.waitFor();
            return true;
        }
        else
            return false;
    }
    
    /**
     * Block until line #index has been read from stream, waiting for specified time between checking availability.
     * 
     * @param the BufferedStream to check for String availability
     * @param index 0-based index of the line number to check for availability
     * @delay the amount of time in milliseconds to wait between checking availability
     * @iterations the maximum number of times to check availability (no limit if 0)
     *
     * @return true if the line is available
     * @return false if the line did not become available after delay*iterations milliseconds,
     *  or if the stream was closed prior to the line becoming available
     *
     * @throws IllegalArgumentException if delay or iterations are negative
     */
    public boolean waitUntilAvailable(STREAM stream, int index, int delay, int iterations) {
        if (delay < 0) throw new IllegalArgumentException("Cannot wait for a negative number of milliseconds: " + delay);
        if (iterations < 0) throw new IllegalArgumentException("Cannot wait for a negative number of iterations: " + iterations);
        
        boolean lineFound = isLineAvailable(stream, index);
        int elapsedAvailabilityChecks = 0;
        while ((!lineFound) && (iterations > 0) && (elapsedAvailabilityChecks < iterations)) {
            try {
                Thread.sleep(delay);
            } catch (InterruptedException ie) {
                Thread.interrupted();
            }
            lineFound = isLineAvailable(stream, index);
            
            elapsedAvailabilityChecks++;
        }
        
        return lineFound;
    }
    
    protected void setStreamReader(STREAM stream, BufferedReader reader) {
        switch (stream) {
            case STDOUT :
                stdoutReader = reader;
                break;
            case STDERR :
                stderrReader = reader;
                break;
            default :
                throw new IllegalArgumentException("stream must be one of " + STREAM.STDOUT + " or " + STREAM.STDERR +", but was " + stream);
        }
    }
    
    private List<String> getLineListByStream(STREAM stream) {
                switch (stream) {
            case STDOUT :
                return stdoutText;
            case STDERR :
                return stderrText;
            default :
                throw new IllegalArgumentException("stream must be one of " + STREAM.STDOUT + " or " + STREAM.STDERR);
        }
    }
}
