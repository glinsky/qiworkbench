package com.bhpb.devtools;

import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Dimension;

import com.bhpb.devtools.util.CommandLineJob;
import com.bhpb.devtools.util.CommandLineJobImpl;

public class Debugger extends DialogApp {
/*
    private String scriptComment = "#This script runs the qi workbench jnlp using javaws.\n\
#The -agentpath argument given in JAVA_VM_ARGS causes the JVM to pause on startup\n\
#until the Netbeans profiler connects to it.";
*/

    private static final String VM_ARGS = "-Dcom.sun.management.jmxremote -Djava.util.logging.config.file=/home/folsw9/logging.properties";
    private static final String JAVA_EXEC_CMD = "/bin/java";
    private static final String WS_OPTS="-Xbootclasspath/a:/usr/java/jdk1.5.0_11/jre/lib/javaws.jar:/usr/java/jdk1.5.0_11/jre/lib/deploy.jar -classpath /usr/java/jdk1.5.0_11/jre/lib/deploy.jar -Djnlpx.home=/usr/java/jdk1.5.0_11/jre/bin -Djnlpx.splashport=37985 -Djnlpx.vmargs=$DEBUG -Djnlpx.jvm=/usr/java/jdk1.5.0_11/jre/bin/java -Djnlpx.remove=true -Djava.security.policy=file:/usr/java/jdk1.5.0_11/jre/lib/security/javaws.policy -DtrustProxy=true -Xverify:remote -Djnlpx.heapsize=NULL,NULL -Djava.util.logging.config.file=/home/folsw9/logging.properties";
    private static final String WEBSTART_JAR="/lib/javaws.jar";
    private static final String WEBSTART_MAIN="com.sun.javaws.Main";
    private static final String DEBUG_FLAGS="-Xdebug -Dcom.sun.management.jmxremote";
    private static final String JDPA_FLAGS="-Xrunjdwp:transport=dt_socket,address=8888,server=y";
    private static final String JNLP_URL="http://houu8572:8080/qiWorkbench/jsp/qiw_httpinvoker_jnlp.jsp?install=true";

    private JPanel panel; // used to create modal message dialog in case of exception
    private JTextArea scriptTextArea;

    public void decoratePanel(JPanel panel) {
        this.panel = panel;

        System.out.println("In Debugger.decoratePanel()...");
        panel.setLayout(new BorderLayout());

        scriptTextArea = new JTextArea();
        scriptTextArea.setColumns(80);
        scriptTextArea.setLineWrap(true);
        panel.add(scriptTextArea, BorderLayout.CENTER);
        scriptTextArea.setText(generateScript());

        JButton goButton = new JButton("Go!");
        goButton.addActionListener(new GoListener(this));
        panel.add(goButton, BorderLayout.SOUTH);
    }

    private String generateScript() {
        StringBuilder sb = new StringBuilder();

        sb.append(getJavaExecutableCommand());
        sb.append(" ");
        sb.append(getVM_ARGS());
        sb.append(" ");
        sb.append(getWS_OPTS());
        sb.append(" ");
        sb.append("-classpath");
        sb.append(" ");
        sb.append(getWebstartJar());
        sb.append(" ");
        sb.append(getWebstartMain());
        sb.append(" ");
        sb.append(getDebugFlags());
        sb.append(" ");
        sb.append(getJDPA_FLAGS());
        sb.append(" ");
        sb.append(getJnlpUrl());

        return sb.toString();
    }

    private String getJavaExecutableCommand() {
	return System.getProperty("java.home") + JAVA_EXEC_CMD;
    }

    private String getVM_ARGS() {
        return VM_ARGS;
    }

    private String getWS_OPTS() {
        return WS_OPTS;
    }

    private String getWebstartJar() {
       return System.getProperty("java_home") + WEBSTART_JAR;
    }

    private String getWebstartMain() {
       return WEBSTART_MAIN;
    }

    private String getDebugFlags() {
        return DEBUG_FLAGS;
    }

    private String getJDPA_FLAGS() {
        return JDPA_FLAGS;
    }

    private String getJnlpUrl() {
        return JNLP_URL;
    }

    public String[] getScript() {
        return scriptTextArea.getText().split(" ");
    }

    public JPanel getPanel() {
        return panel;
    }

    class GoListener implements ActionListener {
        private Debugger debugger;

        public GoListener(Debugger debugger) {
            this.debugger = debugger;
        }

        public void actionPerformed(ActionEvent ae) {
            CommandLineJob job = new CommandLineJobImpl(debugger.getScript());
            JOptionPane.showMessageDialog(debugger.getPanel(), "Webstart application launched in debug mode.  You may now invoke Run-->Attach Debugger using SocketAttach (localhost:8888).");

            try {
                job.start();
            } catch (IOException ioe) {
                JOptionPane.showMessageDialog(debugger.getPanel(), "Unable to start script job due to: " + ioe.getMessage());
            }

        }
    }
}
