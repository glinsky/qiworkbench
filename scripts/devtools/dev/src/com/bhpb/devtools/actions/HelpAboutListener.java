package com.bhpb.devtools.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HelpAboutListener implements ActionListener {
    public void actionPerformed(ActionEvent ae) {
        System.out.println("Performing HelpAboutListener's action.");
    }
}
