package com.bhpb.devtools.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.bhpb.devtools.DialogApp;

public class FileExitListener implements ActionListener {
    private DialogApp dialogApp;

    public FileExitListener(DialogApp dialogApp) {
        this.dialogApp = dialogApp;
    }

    public void actionPerformed(ActionEvent ae) {
        System.out.println("Performing FileExitListener's action.");
        dialogApp.exitApp();
    }
}
