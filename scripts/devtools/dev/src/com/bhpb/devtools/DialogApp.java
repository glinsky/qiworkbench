package com.bhpb.devtools;

import javax.swing.JDialog;
import javax.swing.JDialog;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import com.bhpb.devtools.actions.FileExitListener;
import com.bhpb.devtools.actions.HelpAboutListener;

public abstract class DialogApp implements Runnable {
    private static Dimension DEFAULT_INITIAL_SIZE = new Dimension(400,400);
    private static Thread debuggerThread;

    private boolean applicationClosing = false;
    private JDialog dlg;

    public static void main(String[] args) {
        System.out.println("Ran Debugger with args:");
	for (String arg : args) {
            System.out.println(arg);	
        }	
        Debugger debugger = new Debugger();
        debuggerThread = new Thread(debugger);
        debuggerThread.start();
        System.out.println("Debugger console started...");
    }

    private JDialog createDialog() {
	dlg = new JDialog();

        System.out.println("Adding WindowCloseAdapter to dialog.");
        dlg.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        dlg.addWindowListener(new WindowCloseAdapter(this));
        addMenuBar(dlg);

        JPanel panel = new JPanel();
        panel.setPreferredSize(DEFAULT_INITIAL_SIZE);

        decoratePanel(panel);

        dlg.getContentPane().add(panel);
        return dlg;	
    }

    public void run() {
        System.out.println("Creating dialog...");
        dlg = createDialog();
        System.out.println("Setting dialog visible...");
        dlg.setVisible(true);
        dlg.pack();
        System.out.println("Beginning to poll for applicationClosing...");
        while (applicationClosing == false) {
	    try {
                //System.out.println("Sleeping for 1 second.");
                Thread.sleep(1000);
            } catch (InterruptedException ie) {
                System.out.println("While waiting 1 second before polling applicationClosing, caught: " + ie.getMessage());
            }
	}
        dlg.dispose();
    }

    /**
     * Begin shutting down the application.
     */
    public void exitApp() {
        System.out.println("Exiting debugger...");
        applicationClosing = true;
    }

    protected abstract void decoratePanel(JPanel panel);

    private void addMenuBar(JDialog dlg) {
        JMenuBar menubar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");
        JMenuItem fileExitItem = new JMenuItem("Exit");
        fileExitItem.addActionListener(new FileExitListener(this));
        fileMenu.add(fileExitItem);

        JMenu helpMenu = new JMenu("Help");
        JMenuItem helpAboutItem = new JMenuItem("About");
        helpAboutItem.addActionListener(new HelpAboutListener());

        helpMenu.add(helpAboutItem);

        menubar.add(fileMenu);
        menubar.add(helpMenu);

        dlg.setJMenuBar(menubar);
    }
}
