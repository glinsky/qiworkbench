# gen_public_qiwb.sh - Checks out all PUBLIC svn projects listed in projects.txt
# from their 'dev' branches, builds the qiWorkbench and all dependencies,
# deploys the qiWorkbench to localhost's $CATALINA_HOME/webapps,
# starts the qiWorkbench, and launches the newly built client.
#
# last modified by Woody Folsom 4/24/2009
#
# Usage: 'gen_qiwb.sh' - build everything and launch the qiWorkbench
#
# Please Note - This script should only be run once.  Thereafter, running
# cd ../qiwbBuilder/dev
# ant
# ./deploy_qiwb.sh
# is sufficient to build and deploy the qiWorkbench.
#
# This script requires:
# bash or compatible shell
# J2SE 1.5+ SDK with Java webstart (tested with 1.5.0_06+ 32 bit and 1.6.0_13 64 bit)
# Ant 1.5+ (tested up to Ant 1.7.1)
# ant-contrib-1.03b and svnant jars
# Apache Tomcat 5.5 or 6.0 installed on port 8080
# Note: Tomcat 6.X will NOT work without modifying the qiWb project build.xml
# or creating a symlink from $CATALINA_HOME/common/lib to $CATALINA_HOME/lib
# due to unresolved issues regarding how to import sevlet-api.jar.

# Possible exit codes
# -1 no CATALINA_HOME environment variable
# -2 no QIWB_CONFIG environment variable
# -3 no QIDEP_DIR environment variable
# -4 no thawte_houpet security certificate in the QIDEP_DIR directory (not available from SVN) 

#############################
#initialize the required environment variables
CONF_FILE_NAME=qiwbConfig.xml
#############################

#Check that CATALINA_HOME is set
if [ -z $CATALINA_HOME ]
then
  echo "The CATALINA_HOME environment variable must be set for installation to proceed."
  exit -1
fi

#Check that QIWB_CONFIG is set
if [ -z $QIWB_CONFIG ]
then
  echo "The QIWB_CONFIG environment variable must be set to $CATALINA_HOME/conf/$CONF_FILE_NAME for installation to proceed."
  exit -2
fi

#Check that QIDEP_DIR is set
if [ -z $QIDEP_DIR ]
then
  echo "The QIDEP_DIR environment variable must be set to a directory writeable by the current user for installation to proceed."
  exit -3
fi

#Check that QIDEP_DIR includes security certificate
if [ ! -f $QIDEP_DIR/thawte_houpet ]
then
  echo "The QIDEP_DIR directory does not contain the thawte_houpet security certicate.  Please copy this file into the QIDEP_DIR directory and run this script again."
  exit -4
fi

#Create the QIDEP_DIR/dev_libs subdirectory
if [ -d $QIDEP_DIR/dev_libs ]
then
  rm -f $QIDEP_DIR/dev_libs/*
else
  mkdir $QIDEP_DIR/dev_libs
fi

#Create the QIDEP_DIR/lib subdirectory
if [ -d $QIDEP_DIR/lib ]
then
  rm -f $QIDEP_DIR/lib/*
else
  mkdir $QIDEP_DIR/lib
fi

#Check all public projects out from SVN
cp -f public_projects.txt ..
cp -f checkoutall.sh ..
cd ..
./checkoutall.sh public_projects.txt

#Copy libs from qiWb project's template QIDEP_DIR directory
cp qiWb/dev/qidep_dir/* $QIDEP_DIR/
cp qiWb/dev/qidep_dir/lib/* $QIDEP_DIR/lib/

#Begin complete build of qiWb war and all BHPB and public jars
cd qiwbBuilder/dev
ant buildPublic

#Copy QIWB_CONFIG to $CATALINA_HOME/conf
echo "Copying $CONFIG_FILE_NAME to $CATALINA_HOME/conf..."
cp -f dist/dev/$CONF_FILE_NAME $CATALINA_HOME/conf/$CONF_FILE_NAME

#Deploy QIWB
./deploy_qi_workbench.sh

echo "Waiting 5 seconds while Tomcat starts..."
sleep 5

echo "Launching the qiWorkbench client"
javaws http://localhost:8080/qiWorkbench/jsp/qiw_httpinvoker_jnlp.jsp?install=true
