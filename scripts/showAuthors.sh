# recursively print which author last changed every file in svn repository, from
# the point at which the script is invoked

for name in `svn list --recursive`; do author=`svn info  $name | grep 'Last Changed Author' | awk '{print $4}'`; echo $author last changed $name; done
