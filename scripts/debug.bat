#To specify a jnlp file for debugging, argument $1 must be the extension of the associated jnlp file.
#houu8591 - '8951'
#Houston PUC - 'Houston'
#Perth PUC - 'Perth'

set JAVAWS_VM_ARGS="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8888"
%JAVA_HOME%\bin\javaws debug_qiwb.jnlp.%1
