#checkoutAll.sh - Checks out all svn projects listed in
#the file specified by argument $1
#for branch specified by the first and only command-line argument.
#
#Last modified by Woody Folsom on 6/10/2008
#Copy this file and projects.txt into the directory which contains
#all qiWorkbench SVN projects.

#Usage: 'checkoutAll.sh' - Updates all 'dev' branch code for projects in projects.txt.
#exit codes:
#-1 no arguments given but at least 1 is required
#-2 filename argument specified but file does not exist

if [ -z $1 ]
then
  echo "This script requires at least 1 argument, the name of the file listing projects to be checked out from SVN"
  exit -1
else
  projectsfile=$1
  if [ ! -f $projectsfile ]
  then
    echo "Unable to parse project names from file: ${projectsfile}"
    exit -2
  fi
fi

if [ -z $2 ]
then
  tagdir="dev"
  echo "'$2' is zero length, checking out: ${tagdir}"
else
  tagdir="tags/${2}"
  echo "'$2' is nonzero length, checking out: ${tagdir}"
fi

echo "Checking out code under <project>/${tagdir} for all projects..."

for j in `cat $1`
do
  if [ ! -d ${j}/${tagdir} ]
  then
    if [ ! ${j}/${tagdir} == "dev" ]
    then
      if [ ! -d ${j}/tags ]
      then
        echo "Tag directory is not 'dev' and does not exist, creating directory 'tags'"
        mkdir tags
      fi
    fi
    echo "Creating tag directory: ${j}/${tagdir}"
    mkdir ${j}/${tagdir}
    echo "Checking out project: ${j} at tag: ${tagdir}"
    svn co http://www.qiworkbench.org/repos/${j}/${tagdir} ${j}/${tagdir}
  else
    echo "Unable to check out project: ${j} for tag ${tagdir}, this directory already exists. Remove it or use update instead."
  fi
done
