#!/usr/local/bin/bash
#This script runs the qi workbench jnlp using javaws.
#The -agentpath argument given in JAVA_VM_ARGS causes the JVM to pause on startup
#until the Netbeans profiler connects to it.

#JAVAWS_VM_ARGS="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,address=8200,timeout=10000"

#/usr/java/jdk/bin/javaws qiw_httpinvoker_jnlp.jsp

#rem *** The following parameters must come from the JNLP file ***
export VM_ARGS="-Dcom.sun.management.jmxremote"
URL="http://localhost:8080/qiWorkbench/jsp/qiw_httpinvoker_jnlp.jsp?install=false"
 
#rem *** Enable the following line to have Webstart dump all possible logs ***
#WS_DEBUG="-XX:TraceBasic=true -XX:TraceNetwork=true -XX:TraceCache=true -XX:TraceTemp=true -XX:TraceSecurity=true -XX:TraceExtensions=true"
 
#rem *** Enable the following line create a JNLP session on startup ***
export JDPA="-Xrunjdwp:transport=dt_socket,address=8888,server=y"
 
export DEBUG="-Xdebug $JDPA -Dcom.sun.management.jmxremote"

export JAVAWS_TRACE_NATIVE=1

export WS_OPTS="-Xbootclasspath/a:/usr/java/jdk1.5.0_11/jre/lib/javaws.jar:/usr/java/jdk1.5.0_11/jre/lib/deploy.jar -classpath /usr/java/jdk1.5.0_11/jre/lib/deploy.jar -Djnlpx.home=/usr/java/jdk1.5.0_11/jre/bin -Djnlpx.splashport=37985 -Djnlpx.vmargs=$DEBUG -Djnlpx.jvm=/usr/java/jdk1.5.0_11/jre/bin/java -Djnlpx.remove=true -Djava.security.policy=file:/usr/java/jdk1.5.0_11/jre/lib/security/javaws.policy -DtrustProxy=true -Xverify:remote -Djnlpx.heapsize=NULL,NULL"
 
$JAVA_HOME/jre/bin/java $VM_ARGS -esa $WS_OPTS -classpath $JAVA_HOME/jre/lib/javaws.jar com.sun.javaws.Main $WS_DEBUG $URL
#$JAVA_HOME/jre/bin/javaws qiw_httpinvoker_jnlp.jsp
