






<?xml version="1.0" encoding="UTF-8"?>
<jnlp codebase="http://localhost:8080/qiWorkbench/jsp/"
          href="http://localhost:8080/qiWorkbench/jsp/qiw_httpinvoker_jnlp.jsp?install=false">
    <information>
        <title>qiWorkbench v1.2</title>
        <vendor>BHP Billiton Petroleum</vendor>
        <description>Seismic toolkit for Quantitative Interpretation</description>
        <offline-allowed/>
        <icon href="images/QIW_icon.gif"/>
        <icon href="images/QIW_splash.gif" kind="splash"/>
        <shortcut><desktop/></shortcut>
    </information>
    <security>
        <all-permissions/>
    </security>
    <resources>
        <j2se version="1.5"/>
        <jar href="../lib/qiWorkbench.jar"/>
        <jar href="../lib/client_filechooser_job_asciiIO.jar"/>        
        <jar href="../lib/gw2Dlib.jar"/>
        <jar href="../lib/gwSeismicLib.jar"/>
        <jar href="../lib/qiwb_2Dviewer.jar"/>
        <jar href="../lib/commons-httpclient-3.0.jar"/>
        <jar href="../lib/commons-logging.jar"/>
        <jar href="../lib/commons-codec-1.3.jar"/>
        <jar href="../lib/xstream-1.2.jar"/>
        <jar href="../lib/xom-1.1.jar"/>
        <jar href="../lib/xpp3-1.1.3.4.O.jar"/>
        <jar href="../lib/activation.jar"/>
        <jar href="../lib/mail.jar"/>
        <jar href="../lib/jhall.jar"/>
        <jar href="../lib/swing-layout-1.0.jar"/>

		<!-- Access property parameters with System.getProperty("param") -->
		<property name="install" value="false" />
		<property name=deployServerURL value=http://localhost:8080 />
    </resources>
    <application-desc main-class="com.bhpb.qiworkbench.messageFramework.MessageDispatcher"/>
</jnlp>


