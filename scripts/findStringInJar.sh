#!/usr/local/bin/bash

searchstr=$1

if [ -n "${searchstr}" ]
then
  for file in $( find . | grep '\.jar' )
  do
    echo searching file:  $file for ${searchstr}...
#  echo "executing: unzip -l $file | grep '${searchstr}'"
    unzip -l $file | grep -i ${searchstr}
  done
else
  echo "usage: findStringInJar <string>"
fi
