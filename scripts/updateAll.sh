#updateAll.sh - Updates all svn projects listed in projects.txt
#
#Last modified by Woody Folsom on 6/10/2008
#Copy this file and projects.txt into the directory which contains
#all qiWorkbench SVN projects.  Ensure that all projects listed
#in the text file have been checked out (use checkoutAll.sh to do this
#automatically).  Note that it is necessary
#to at least check out the 'dev' and 'tags' subdirectories for any desired tags, although
#not necessarily any lower-level files or directories.

#Usage: 'updateAll.sh' - Updates all 'dev' branch code.
#       'updateAll tag1234' - Updates all code under <project>/tags/tag1234

if [ -z $1 ]
then
  tagdir="dev"
  echo "'$1' is zero length, updating: ${tagdir}"
else
  tagdir="tags/${1}"
  echo "'$1' is nonzero length, updating: ${tagdir}"
fi

echo "Updating code under <project>/${tagdir} for all projects..."

for j in `cat projects.txt`
do
  echo "Updating project: ${j}..."
  svn update ${j}/${tagdir}
done
