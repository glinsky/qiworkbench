#!/usr/local/bin/bash
#This script runs the qi workbench jnlp using javaws.
#The -agentpath argument given in JAVA_VM_ARGS causes the JVM to pause on startup
#until the Netbeans profiler connects to it.
#You must specify the last 4 digits of the BHPB hostname or else localhost as argument $1

export JAVAWS_VM_ARGS="-agentpath:/opt/netbeans-6.0.1/profiler2/lib/deployed/jdk15/linux/libprofilerinterface.so=/opt/netbeans-6.0.1/profiler2/lib,5140"

export JAVAWS_TRACE_NATIVE=1

#edit the line below to point to JDK 1.5.0_X if your current version of Java is 1.6
#netbeans 6.1 seems to only allow profiling against 1.5
/usr/java/jdk1.5.0_11/bin/javaws ../debug_qiwb.jnlp.$1
