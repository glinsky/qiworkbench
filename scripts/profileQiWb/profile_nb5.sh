#!/usr/local/bin/bash
#This script runs the qi workbench jnlp using javaws.
#The -agentpath argument given in JAVA_VM_ARGS causes the JVM to pause on startup
#until the Netbeans profiler connects to it.

export JAVAWS_VM_ARGS="-agentpath:/scratch/netbeans-5.5.1/profiler1/lib/deployed/jdk15/linux/libprofilerinterface.so=/scratch/netbeans-5.5.1/profiler1/lib,5140"

export JAVAWS_TRACE_NATIVE=1

/usr/java/jdk/bin/javaws qiw_httpinvoker_jnlp.jsp
