package com.bhpb.AmplitudeExtraction;

import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.bhpb.qiworkbench.compAPI.DataDataDescriptorTransferable;
import com.bhpb.qiworkbench.compAPI.DataDataDescriptor;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.AmplitudeExtraction.AmplitudeExtraction;
/*
 * DropTextField.java
 *
 * Created on July 11, 2007, 3:17 PM
 */

/**
 * Extention of JTextField that allows Drag and Drop
 * @author Marcus Vaal
 */
public class DropTextField extends JTextField{
	
	private static Logger logger = Logger.getLogger(DropTextField.class.getName());
    
    /**
     * DropTextField Constructor
     * @param action Drag and Drop Action
     */
    public DropTextField(int action) {
        super();
        if (action != DnDConstants.ACTION_NONE &&
        	action != DnDConstants.ACTION_COPY &&
        	action != DnDConstants.ACTION_MOVE &&
        	action != DnDConstants.ACTION_COPY_OR_MOVE &&      
        	action != DnDConstants.ACTION_LINK
            ) throw new IllegalArgumentException("action" + action);

        this.acceptableActions = action;
        //this.dtListener = new DTListener();

        this.dropTarget = new DropTarget(this, this.acceptableActions, new DTListener(),true);
    }
    
    /**
     * DropTextField Constructor
     * @param action Drag and Drop Action
     * @param ampExt component using the DropTextField
     * @param displayType The type of textfield the file is being dropped into
     */
    public DropTextField(int action, AmplitudeExtraction ampExt, String displayType) {
        super();
        if (action != DnDConstants.ACTION_NONE &&
        	action != DnDConstants.ACTION_COPY &&
        	action != DnDConstants.ACTION_MOVE &&
        	action != DnDConstants.ACTION_COPY_OR_MOVE &&      
        	action != DnDConstants.ACTION_LINK
            ) throw new IllegalArgumentException("action" + action);

        this.acceptableActions = action;
        this.ampExt = ampExt;
        this.displayType = displayType;
        //this.dtListener = new DTListener();

        this.dropTarget = new DropTarget(this, this.acceptableActions, new DTListener(),true);
    }


    class DTListener implements DropTargetListener {
	    
	    /**
	     * Checks if the DragFlavor is suppored by the text area being dragged over
	     * @param evt DropTargetDragEvent
	     * @return Returns true if drag flavor is supported, else it returns false
	     */
	    private boolean isDragFlavorSupported(DropTargetDragEvent evt) {
			boolean ok=false;
			DataFlavor chosen = null;
			if (evt.isDataFlavorSupported(DataDataDescriptorTransferable.dataDataDescriptorFlavor)) {
				chosen = DataDataDescriptorTransferable.dataDataDescriptorFlavor;
				ok=true;
			} else if (evt.isDataFlavorSupported(DataDataDescriptorTransferable.localDataDataDescriptorFlavor)) {
				chosen = DataDataDescriptorTransferable.localDataDataDescriptorFlavor;
				ok=true;
			} else if (evt.isDataFlavorSupported(new DataFlavor(QIWConstants.LOCAL_DATA_DATA_DESCRIPTOR, "Local DataDataDescriptor"))) {
				chosen = new DataFlavor(QIWConstants.LOCAL_DATA_DATA_DESCRIPTOR, "Local DataDataDescriptor");
				ok=true;
			} else if (evt.isDataFlavorSupported(new DataFlavor(DataDataDescriptor.class, "Non local DataDataDescriptor"))) {
				chosen = new DataFlavor(DataDataDescriptor.class, "Non local DataDataDescriptor");
				ok=true;
			}
			
			Object data=null;
			try {
				data = evt.getTransferable().getTransferData(chosen);
				if (data == null) {
					throw new NullPointerException();
				}
			} catch (Throwable thrown) {
				logger.finest("Couldn't get transfer data: " + thrown.getMessage());
				logger.finest(thrown.toString());
				return false;
			}
			//Checks the displayType to see if it is an acceptable Drag and Drop item
			if(displayType != null) {
				if(displayType.equals("InputCube")){
					if(!((DataDataDescriptor)data).getGroupName().equals("Datasets")) {
						ok = false;
					}
				} else if(displayType.equals("TopHorizon") || displayType.equals("BottomHorizon") || displayType.equals("bhpsuHorizon")){
					if(!((DataDataDescriptor)data).getGroupName().equals("Horizons")) {
						ok = false;
					}
				} 
			}
			
			return ok;
	    }
	    
	    /**
	     * Chooses the Drop Flavor
	     * @param evt Drop TargetDropEvent
	     * @return DataFlavor of the correct type
	     */
	    private DataFlavor chooseDropFlavor(DropTargetDropEvent evt) {
			if (evt.isLocalTransfer() == true && evt.isDataFlavorSupported(DataDataDescriptorTransferable.localDataDataDescriptorFlavor)) {
				return DataDataDescriptorTransferable.localDataDataDescriptorFlavor;
			}
			
			DataFlavor chosen = null;
			if (evt.isDataFlavorSupported(DataDataDescriptorTransferable.dataDataDescriptorFlavor)) {
				chosen = DataDataDescriptorTransferable.dataDataDescriptorFlavor;
			} else if (evt.isDataFlavorSupported(DataDataDescriptorTransferable.localDataDataDescriptorFlavor)) {
				chosen = DataDataDescriptorTransferable.localDataDataDescriptorFlavor;
			} else if (evt.isDataFlavorSupported(new DataFlavor(QIWConstants.LOCAL_DATA_DATA_DESCRIPTOR, "Local DataDataDescriptor"))) {
				chosen = new DataFlavor(QIWConstants.LOCAL_DATA_DATA_DESCRIPTOR, "Local DataDataDescriptor");
			} else if (evt.isDataFlavorSupported(new DataFlavor(DataDataDescriptor.class, "Non local DataDataDescriptor"))) {
				chosen = new DataFlavor(DataDataDescriptor.class, "Non local DataDataDescriptor");
			}
			
			return chosen;
		}
	
	    /**
	     * Checks if Drag is Ok
	     * @param evt DropTargetDragEvent
	     * @return Returns true if drop action is allowed, the drop action is exceptable, and DataFlavor is supported, else returns false
	     */
	    private boolean isDragOk(DropTargetDragEvent evt) {
		    if(isDragFlavorSupported(evt) == false) {
		    	return false;
		    }
		      
		    int dropAction = evt.getDropAction();      
		
		    if ((dropAction & DropTextField.this.acceptableActions) == 0)
		    	return false;
		    return true;
	    }
	
	    /**
	     * Invoked to signify a Drag and Drop dragged into the text field
	     */
	    public void dragEnter(DropTargetDragEvent evt) {
			if(isDragOk(evt) == false) {
				evt.rejectDrag();
				return;
			}
			evt.acceptDrag(evt.getDropAction());
		}
	
	    /**
	     * Invoked to signify a Drag and Drop dragged over the text field
	     */
	    public void dragOver(DropTargetDragEvent evt) {
			if(isDragOk(evt) == false) {
				evt.rejectDrag();
				return;
			}
			evt.acceptDrag(evt.getDropAction());
	    }
	    
	    /**
		 * Called if the user has modified the current drop gesture 
	     */ 
	    public void dropActionChanged(DropTargetDragEvent evt) {
			if(isDragOk(evt) == false) {
				evt.rejectDrag();
				return;
			}
			evt.acceptDrag(evt.getDropAction());
		}
	    
	    /**
	     * Invoked to signify a Drag and Drop exited the text field
	     */
	    public void dragExit(DropTargetEvent evt) {
	    	//Empty
	    }
	
	    /**
	     * Invoked to signify a Drag and Drop item dropped into the text field
	     */
	    public void drop(DropTargetDropEvent evt) {
			
			DataFlavor chosen = chooseDropFlavor(evt);
			if (chosen == null) {
				System.err.println( "No flavor match found" );
				evt.rejectDrop();      	
				return;
			}
			logger.finest("Chosen data flavor is " + chosen.getMimeType());
			
			int sa = evt.getSourceActions();      
			
			if ( ( sa & DropTextField.this.acceptableActions ) == 0 ) {
				logger.finest("No action match found");
				evt.rejectDrop();      		
				return;
			}
			
			Object data=null;
			try {
				evt.acceptDrop(DropTextField.this.acceptableActions);
				  
				data = evt.getTransferable().getTransferData(chosen);
				if (data == null) {
					throw new NullPointerException();
				}
			} catch ( Throwable thrown ) {
				logger.finest("Couldn't get transfer data: " + thrown.getMessage());
				logger.finest(thrown.toString());
				evt.dropComplete(false);
				return;
			}
			
			//Checks if the data object is a DataDataDescriptor
			if (data instanceof DataDataDescriptor) {
				String displayString = "";
				//Checks to see which field the dragged object is allowed in
				if(displayType != null) {
					if(displayType.equals("InputCube")){
						ampExt.setInput(((DataDataDescriptor)data).getPath());
						ampExt.setEntireDatasetButtonEnabled(true);
					} else if(displayType.equals("TopHorizon")){
						ampExt.setTopHorizon(((DataDataDescriptor)data).getPath());
					} else if(displayType.equals("BottomHorizon")){
						ampExt.setBottomHorizon(((DataDataDescriptor)data).getPath());
					} else if(displayType.equals("bhpsuHorizon")){
						ampExt.setHorizon(((DataDataDescriptor)data).getPath());
					}
				}
			} else {
				evt.dropComplete(false);
				return;
			}

			evt.dropComplete(true);      
	    }
	}

	private DropTarget dropTarget;
	//private DropTargetListener dtListener;
	private AmplitudeExtraction ampExt;
	private String displayType = "";
	private int acceptableActions = DnDConstants.ACTION_COPY;  

}
