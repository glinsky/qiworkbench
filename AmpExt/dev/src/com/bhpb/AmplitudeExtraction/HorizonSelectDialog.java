package com.bhpb.AmplitudeExtraction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/*
 * HorizonSelectDialog.java
 *
 * Created on September 12, 2006, 5:13 PM
 */

/**
 *
 * @author  l
 */
public class HorizonSelectDialog extends java.awt.Dialog {
	private static Logger logger = Logger.getLogger(HorizonSelectDialog.class
			.getName());
	private AmplitudeExtraction parent;
	private javax.swing.JTextField target;
	//private LandmarkProjectHandler projectHandler;
	private String [] horizons;
	//private int projectType = 3;
    /** Creates new form ProjectSelectDialog */
    public HorizonSelectDialog(AmplitudeExtraction parent, javax.swing.JTextField target, boolean modal, String [] horizons) {
    	super(JOptionPane.getFrameForComponent(parent), modal);
    	this.parent = parent;
    	this.target = target;
    	this.setTitle("Select A Horizon Dataset");
    	//projectHandler = new  LandmarkProjectHandler();
        //by default get all projects
        //String sPrjType = parent.getLandmarkProjectType();

        //if(sPrjType.equals("2D"))
        //	projectType = 2;
        //else if(sPrjType.equals("3D"))
        //	projectType = 3;

        //horizons = projectHandler.getHorizonList(parent.getSelectedLandmarkProject(), projectType);
        this.horizons = horizons.clone();
        initComponents();
        this.setLocationRelativeTo(parent);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">
    private void initComponents() {
        jScrollPane1 = new javax.swing.JScrollPane();
        horizonList = new javax.swing.JList();
        horizonListLabel = new javax.swing.JLabel();
        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 204));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        horizonList.setBackground(new java.awt.Color(204, 255, 255));
        horizonList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        horizonList.setModel(new javax.swing.AbstractListModel() {
            public int getSize() { return horizons.length; }
            public Object getElementAt(int i) { return horizons[i]; }
        });

        horizonList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                okButton.setEnabled(true);
            }
    	});
        MouseListener mouseListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
            	if (e.getClickCount() == 2) {
            		okButton.doClick();
            	}
            }
        };
        horizonList.addMouseListener(mouseListener);
        horizonList.addKeyListener(new ListSearcher(horizonList,okButton));
        jScrollPane1.setViewportView(horizonList);

        horizonListLabel.setBackground(new java.awt.Color(255, 255, 255));
        horizonListLabel.setText("Horizon List:");

        okButton.setText("OK");
        okButton.setEnabled(false);
        okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String selectedHorizon = (String)horizonList.getSelectedValue();
				//if(parent.horizonAlreadyChosen(selectedHorizon)){
				//	JOptionPane.showMessageDialog(parent, selectedHorizon + " is already chosen.", "Already Chosen", JOptionPane.WARNING_MESSAGE);
				//	 return;
				//}
				parent.setHorizonDataPath(target,selectedHorizon);
				closeDialog(e);
			}
		});
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
		org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
			layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
			.add(layout.createSequentialGroup()
				.addContainerGap()
				.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
					.add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
					.add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
						.add(okButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 67, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
						.add(cancelButton))
					.add(horizonListLabel))
				.addContainerGap())
		);

		layout.linkSize(new java.awt.Component[] {cancelButton, okButton}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

		layout.setVerticalGroup(
			layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
			.add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
				.addContainerGap()
				.add(horizonListLabel)
				.add(9, 9, 9)
				.add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 217, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 15, Short.MAX_VALUE)
				.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
					.add(cancelButton)
					.add(okButton))
				.addContainerGap())
		);
		pack();
    }// </editor-fold>

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	closeDialog(evt);
    }

    private void closeDialog(java.awt.event.ActionEvent evt) {
        setVisible(false);
        dispose();
    }
    /** Closes the dialog */
    private void closeDialog(java.awt.event.WindowEvent evt) {
        setVisible(false);
        dispose();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	String[] strings = {"item 1","item 2"};
                new HorizonSelectDialog(null, null, true,strings).setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify
    private javax.swing.JButton cancelButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton okButton;
    private javax.swing.JList horizonList;
    private javax.swing.JLabel horizonListLabel;
    // End of variables declaration

}
