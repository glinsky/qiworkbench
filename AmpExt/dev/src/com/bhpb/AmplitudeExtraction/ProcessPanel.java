package com.bhpb.AmplitudeExtraction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.bhpb.services.landmark.LandmarkServices;

/*
 * ProcessPanel.java
 *
 * Created on October 24, 2007, 1:59 PM
 */

/**
 *
 * @author
 */
public class ProcessPanel extends javax.swing.JPanel {
	private static Logger logger = Logger.getLogger(ProcessPanel.class.getName());
	private AmplitudeExtraction parentGUI;
    /** Creates new form ProcessPanel */
    public ProcessPanel(AmplitudeExtraction parent) {
    	parentGUI = parent;
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">
    private void initComponents() {
        rotateCheckBox = new javax.swing.JCheckBox();
        rotateDegreeTextField = new javax.swing.JTextField();
        rotateDegreeLabel1 = new javax.swing.JLabel();
        rotateDegreeLabel2 = new javax.swing.JLabel();
        rotateDegreeLabel3 = new javax.swing.JLabel();
        runSumCheckBox = new javax.swing.JCheckBox();
        widthLabel = new javax.swing.JLabel();
        widthTextField = new javax.swing.JTextField();
        exportToBhpsuCheckBox = new javax.swing.JCheckBox();
        exportToBhpsuCheckBox.setEnabled(false);
        lm2BhpsuPrefixLabel = new javax.swing.JLabel();
        bhpsuCubePrefixTextField = new javax.swing.JTextField();
        widthUnitLabel = new javax.swing.JLabel();
        exportToLandmarkCheckBox = new javax.swing.JCheckBox();
        landmarkProjectLabel = new javax.swing.JLabel();
        landmarkProjectTextField = new javax.swing.JTextField();
        landmarkProjectListButton = new javax.swing.JButton();
        volumeNameLabel = new javax.swing.JLabel();
        volumeNameTextField = new javax.swing.JTextField();
        brickExtLabel = new javax.swing.JLabel();
        notRunAmpExtCheckBox = new javax.swing.JCheckBox();
        notRunAmpExtCheckBox.setEnabled(false);
        interpolateHorizonCheckBox = new javax.swing.JCheckBox();

        notRunAmpExtCheckBox.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        		if(notRunAmpExtCheckBox.isSelected()){
        			if(!runSumCheckBox.isSelected() && !rotateCheckBox.isSelected()){
        				notRunAmpExtCheckBox.setSelected(false);
        				return;
        			}
        			exportToLandmarkCheckBox.setEnabled(true);
        			parentGUI.setRunAmpExtEabled(false);
        		}else{
        			exportToLandmarkCheckBox.setEnabled(true);
        			parentGUI.setRunAmpExtEabled(true);
        		}
        	}
        });
        setBorder(javax.swing.BorderFactory.createTitledBorder("Data Processing"));
        rotateCheckBox.setText("Rotate");
        rotateCheckBox.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        rotateCheckBox.setMargin(new java.awt.Insets(0, 0, 0, 0));

        rotateDegreeLabel1.setText("(0 ~");

        ImageIcon icon = new ImageIcon(this.getClass().getResource("/icon/plusminus.gif"));
        rotateDegreeLabel3.setText("360)");
        rotateDegreeLabel2.setIcon(icon);

        //rotateDegreeLabel2.setDisabledIcon(new javax.swing.ImageIcon(imgURL));
        rotateDegreeLabel2.setIconTextGap(2);

        //rotateDegreeLabel2.setIcon(new javax.swing.ImageIcon("/home/lilt9/qiWork/AmpExt/dev/src/icon/plusminus.jpeg"));
        //rotateDegreeLabel2.setText("360)");
        //rotateDegreeLabel2.setDisabledIcon(new javax.swing.ImageIcon("/home/lilt9/qiWork/AmpExt/dev/src/icon/plusminus.jpeg"));
        //rotateDegreeLabel2.setIconTextGap(2);

        runSumCheckBox.setText("Run Sum");
        runSumCheckBox.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        runSumCheckBox.setMargin(new java.awt.Insets(0, 0, 0, 0));

        widthLabel.setText("Width:");

        exportToBhpsuCheckBox.setText("Export result to BHP-SU Cube");
        exportToBhpsuCheckBox.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        exportToBhpsuCheckBox.setMargin(new java.awt.Insets(0, 0, 0, 0));

        lm2BhpsuPrefixLabel.setText("Prefix:");

        widthUnitLabel.setText("sec");

        exportToLandmarkCheckBox.setText(" Export to Landmark");
        exportToLandmarkCheckBox.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        exportToLandmarkCheckBox.setMargin(new java.awt.Insets(0, 0, 0, 0));

        landmarkProjectLabel.setText("Project:");

        landmarkProjectListButton.setText("List");

        volumeNameLabel.setText("Volume Name or Process Level:");

        volumeNameTextField.setToolTipText("3D volume name can have up to 36 characters but can not have more than 4 consecutive integers at end.");

        brickExtLabel.setText(".bri ( 3D )");

        landmarkProjectTextField.setEnabled(false);
        landmarkProjectListButton.setEnabled(false);
    	exportToLandmarkCheckBox.setEnabled(false);
        volumeNameTextField.setEnabled(false);
        rotateDegreeTextField.setEnabled(false);
        widthTextField.setEnabled(false);
        bhpsuCubePrefixTextField.setEnabled(false);

    	exportToLandmarkCheckBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				if(exportToLandmarkCheckBox.isSelected()){
					if(!runSumCheckBox.isSelected() && !rotateCheckBox.isSelected()){
						exportToLandmarkCheckBox.setSelected(false);
        				return;
        			}
					if(!notRunAmpExtCheckBox.isSelected() && !exportToBhpsuCheckBox.isSelected()){
						exportToLandmarkCheckBox.setSelected(false);
        				return;

					}
					landmarkProjectListButton.setEnabled(true);
					landmarkProjectTextField.setEnabled(true);
					volumeNameTextField.setEnabled(true);
				}else{
					landmarkProjectListButton.setEnabled(false);
					landmarkProjectTextField.setEnabled(false);
					volumeNameTextField.setEnabled(false);
				}
            }
		});


        landmarkProjectListButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(landmarkAgent == null)
                    landmarkAgent = LandmarkServices.getInstance();
                java.awt.Dialog dialog = new ProjectSelectDialog(landmarkAgent,parentGUI,true,"landmark_runsum_out");
                dialog.setLocationRelativeTo(JOptionPane.getFrameForComponent(parentGUI));
                dialog.setVisible(true);

            }
        });

        rotateDegreeTextField.setText("0"); //default

        widthTextField.setText("1"); //default

        setBorder(javax.swing.BorderFactory.createTitledBorder("Data Processing Parameters"));
        rotateCheckBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				if(rotateCheckBox.isSelected()){
					rotateDegreeTextField.setEnabled(true);
					exportToBhpsuCheckBox.setEnabled(true);
					if(exportToBhpsuCheckBox.isSelected())
						exportToLandmarkCheckBox.setEnabled(true);
					notRunAmpExtCheckBox.setEnabled(true);
				}else{
					exportToBhpsuCheckBox.setSelected(false);
					exportToBhpsuCheckBox.setEnabled(false);
					exportToLandmarkCheckBox.setSelected(false);
					exportToLandmarkCheckBox.setEnabled(false);
					notRunAmpExtCheckBox.setSelected(false);
					notRunAmpExtCheckBox.setEnabled(false);
					rotateDegreeTextField.setEnabled(false);
				}
            }
		});



        runSumCheckBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				if(runSumCheckBox.isSelected()){
					widthTextField.setEnabled(true);
					exportToBhpsuCheckBox.setEnabled(true);
					if(exportToBhpsuCheckBox.isSelected())
						exportToLandmarkCheckBox.setEnabled(true);
					notRunAmpExtCheckBox.setEnabled(true);
				}else{
					if(!rotateCheckBox.isSelected()){
						exportToBhpsuCheckBox.setSelected(false);
						exportToBhpsuCheckBox.setEnabled(false);
						exportToLandmarkCheckBox.setSelected(false);
						exportToLandmarkCheckBox.setEnabled(false);
						notRunAmpExtCheckBox.setSelected(false);
						notRunAmpExtCheckBox.setEnabled(false);
					}
					widthTextField.setEnabled(false);
				}
            }
		});

        exportToBhpsuCheckBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				if(exportToBhpsuCheckBox.isSelected()){
					if(!runSumCheckBox.isSelected() && !rotateCheckBox.isSelected()){
						exportToBhpsuCheckBox.setSelected(false);
        				return;
        			}
					bhpsuCubePrefixTextField.setEnabled(true);
					exportToLandmarkCheckBox.setEnabled(true);
				}else{
					bhpsuCubePrefixTextField.setEnabled(false);
					if(!notRunAmpExtCheckBox.isSelected()){
						exportToLandmarkCheckBox.setSelected(false);
						exportToLandmarkCheckBox.setEnabled(false);
					}
				}
            }
		});

        notRunAmpExtCheckBox.setText("Opt not to run Amplitude Extraction");
        notRunAmpExtCheckBox.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        notRunAmpExtCheckBox.setMargin(new java.awt.Insets(0, 0, 0, 0));

        interpolateHorizonCheckBox.setText(" Interpolate Horizon");
        interpolateHorizonCheckBox.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        interpolateHorizonCheckBox.setMargin(new java.awt.Insets(0, 0, 0, 0));

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(layout.createSequentialGroup()
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(layout.createSequentialGroup()
                                        .add(rotateCheckBox)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(rotateDegreeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 54, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(rotateDegreeLabel1)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(rotateDegreeLabel2)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(rotateDegreeLabel3))
                                    .add(layout.createSequentialGroup()
                                        .add(runSumCheckBox)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(widthLabel)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(widthTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 62, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .add(widthUnitLabel)))
                                .add(32, 32, 32))
                            .add(layout.createSequentialGroup()
                                .add(exportToBhpsuCheckBox)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(lm2BhpsuPrefixLabel)))
                        .add(2, 2, 2)
                        .add(bhpsuCubePrefixTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 367, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(layout.createSequentialGroup()
                                .add(exportToLandmarkCheckBox)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(landmarkProjectLabel))
                            .add(volumeNameLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(landmarkProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 120, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(landmarkProjectListButton))
                            .add(layout.createSequentialGroup()
                                .add(volumeNameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 353, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(2, 2, 2)
                                .add(brickExtLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 68, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                    .add(notRunAmpExtCheckBox)
                    .add(interpolateHorizonCheckBox))
                .addContainerGap(88, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(27, 27, 27)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(rotateCheckBox)
                    .add(rotateDegreeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(rotateDegreeLabel1)
                    .add(rotateDegreeLabel2)
                    .add(rotateDegreeLabel3))
                .add(25, 25, 25)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(runSumCheckBox)
                    .add(widthLabel)
                    .add(widthTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(widthUnitLabel))
                .add(21, 21, 21)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(exportToBhpsuCheckBox)
                    .add(lm2BhpsuPrefixLabel)
                    .add(bhpsuCubePrefixTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(23, 23, 23)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(exportToLandmarkCheckBox)
                    .add(landmarkProjectLabel)
                    .add(landmarkProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(landmarkProjectListButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(volumeNameLabel)
                    .add(volumeNameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(brickExtLabel))
                .add(15, 15, 15)
                .add(notRunAmpExtCheckBox)
                .add(20, 20, 20)
                .add(interpolateHorizonCheckBox)
                .addContainerGap(49, Short.MAX_VALUE))
        );
    }// </editor-fold>
        
        /*
        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(layout.createSequentialGroup()
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(layout.createSequentialGroup()
                                        .add(rotateCheckBox)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(rotateDegreeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 54, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(rotateDegreeLabel1)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(rotateDegreeLabel2)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(rotateDegreeLabel3))
                                    .add(layout.createSequentialGroup()
                                        .add(runSumCheckBox)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(widthLabel)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(widthTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 62, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .add(widthUnitLabel)))
                                .add(32, 32, 32))
                            .add(layout.createSequentialGroup()
                                .add(exportToBhpsuCheckBox)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(lm2BhpsuPrefixLabel)))
                        .add(2, 2, 2)
                        .add(bhpsuCubePrefixTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 367, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(layout.createSequentialGroup()
                                .add(exportToLandmarkCheckBox)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(landmarkProjectLabel))
                            .add(volumeNameLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(landmarkProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 120, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(landmarkProjectListButton))
                            .add(layout.createSequentialGroup()
                                .add(volumeNameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 353, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(2, 2, 2)
                                .add(brickExtLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 150, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                    .add(notRunAmpExtCheckBox))
                .addContainerGap(88, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(27, 27, 27)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(rotateCheckBox)
                    .add(rotateDegreeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(rotateDegreeLabel1)
                    .add(rotateDegreeLabel2)
                    .add(rotateDegreeLabel3))
                .add(25, 25, 25)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(runSumCheckBox)
                    .add(widthLabel)
                    .add(widthTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(widthUnitLabel))
                .add(21, 21, 21)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(exportToBhpsuCheckBox)
                    .add(lm2BhpsuPrefixLabel)
                    .add(bhpsuCubePrefixTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(23, 23, 23)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(exportToLandmarkCheckBox)
                    .add(landmarkProjectLabel)
                    .add(landmarkProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(landmarkProjectListButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(volumeNameLabel)
                    .add(volumeNameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(brickExtLabel))
                .add(15, 15, 15)
                .add(notRunAmpExtCheckBox)
                .addContainerGap(49, Short.MAX_VALUE))
        );
    }// </editor-fold> */


    public boolean validateDataProcessingFields(){
    	if(rotateCheckBox.isSelected()){
    		String text = rotateDegreeTextField.getText();
            if (text == null || text.trim().length() == 0) {
                JOptionPane.showMessageDialog(this, "The rotate degree field is empty.", "Missing data entry", JOptionPane.WARNING_MESSAGE);
                rotateDegreeTextField.requestFocus();
                return false;
            }

            float val = -1;
            try {
            	 val = Float.parseFloat(text);
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(this, "The rotate degree field must be a numeric value.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                rotateDegreeTextField.requestFocus();
                return false;
            }
    	}

    	if(runSumCheckBox.isSelected()){
    		String text = widthTextField.getText();
            if (text == null || text.trim().length() == 0) {
                JOptionPane.showMessageDialog(this, "The Run Sum Width field is empty.", "Missing data entry", JOptionPane.WARNING_MESSAGE);
                widthTextField.requestFocus();
                return false;
            }

            float val = -1;
            try {
            	 val = Float.parseFloat(text);
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(this, "The Run Sum Width field must be a numeric value.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                widthTextField.requestFocus();
                return false;
            }

            if(val < 0){
            	JOptionPane.showMessageDialog(this, "The Run Sum Width field must be greater than or equal to 0.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            	widthTextField.requestFocus();
            	return false;
            }
    	}

    	if(exportToLandmarkCheckBox.isSelected()){
    		if(landmarkProjectTextField.getText().trim().length() == 0){
    			JOptionPane.showMessageDialog(this, "The Landmark project name field is empty.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
    			landmarkProjectListButton.requestFocus();
            	return false;
    		}
    	}

    	if(exportToLandmarkCheckBox.isSelected()){
    		if(volumeNameTextField.getText().trim().length() == 0){
    			JOptionPane.showMessageDialog(this, "The volume name field is empty.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
    			volumeNameTextField.requestFocus();
            	return false;
    		}
    		if(landmarkAgent == null){
				landmarkAgent = LandmarkServices.getInstance();
			}

			int type = landmarkAgent.getLandmarkProjectType(landmarkProjectTextField.getText().trim());
			if((parentGUI.isLandmark3DSelected() && type != 3) || (parentGUI.isLandmark2DSelected() && type != 2)){
    			JOptionPane.showMessageDialog(this, "The output Landmark project type does not match with the input project type.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
    			landmarkProjectListButton.requestFocus();
    			return false;
    		}

			if(parentGUI.isLandmark3DSelected()){
				if(!landmarkProjectTextField.getText().trim().equals(parentGUI.getLandmarkProjectText())){
					int n = JOptionPane.showConfirmDialog(
		                    parentGUI, "The output Landmark project name is not the same as the input Landmark project name. Is this ok?",
		                    "Confirm to proceed",
		                    JOptionPane.YES_NO_OPTION);
		            if(n != JOptionPane.YES_OPTION){
		            	landmarkProjectListButton.requestFocus();
		            	return false;
		            }
				}
    		}

			if(parentGUI.isLandmark2DSelected()){
				if(!landmarkProjectTextField.getText().trim().equals(parentGUI.getLandmark2DProjectText())){
					int n = JOptionPane.showConfirmDialog(
		                    parentGUI, "The output Landmark project name is not the same as the input Landmark project name. Is this ok?",
		                    "Confirm to proceed",
		                    JOptionPane.YES_NO_OPTION);
		            if(n != JOptionPane.YES_OPTION){
		            	landmarkProjectListButton.requestFocus();
		            	return false;
		            }
				}
    		}

			String name = volumeNameTextField.getText().trim();
    		if(type == 3 && name.length() > 36){
    			JOptionPane.showMessageDialog(this, "The volume name for 3D brick file can have up to 36 characters.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
    			volumeNameTextField.requestFocus();
            	return false;
    		}
    		if(type == 2 && name.length() != 9){
    			JOptionPane.showMessageDialog(this,"The size of the volume name " + "\"" + name + "\"" +
        				" is not equal to 9 characters. \nThe name of Landmark 2D process level and version can take up to 9 characters.",
        				"Invalid data entry", JOptionPane.WARNING_MESSAGE);
    			volumeNameTextField.requestFocus();
                return false;
    		}
    		
    		if(type == 2 && parentGUI.getLandmark2DProjectText().equals(landmarkProjectTextField.getText().trim())){
	    		String processLevel = parentGUI.getSelectedLandmark2DProcessLevel();
	    		if(processLevel != null){
	    			if(processLevel.equals(name)){
	    				int n = JOptionPane.showConfirmDialog(
			                    parentGUI, "The output Landmark 2D process level name is the same as input. The input process level may be overwritten. Is this ok?",
			                    "Confirm to proceed",
			                    JOptionPane.YES_NO_OPTION);
			            if(n != JOptionPane.YES_OPTION){
			            	volumeNameTextField.requestFocus();
			            	return false;
			            }
	    			}
	    		}
			}
    	}
    	if(notRunAmpExtCheckBox.isSelected()){
    		if(!exportToBhpsuCheckBox.isSelected() && !exportToLandmarkCheckBox.isSelected()){
    			JOptionPane.showMessageDialog(this, "Must select either \"Export result to BHP-SU cube\" or \"Export to Landmark\", if you opt not to run Amplitude Extraction.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
    			return false;
    		}
    		if(!runSumCheckBox.isSelected() && !rotateCheckBox.isSelected()){
    			JOptionPane.showMessageDialog(this, "Must select either \"Rotate\" or \"Run Sum\", if you opt not to run Amplitude Extraction.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
    			return false;
    		}
    	}
    	
    	//if(!notRunAmpExtCheckBox.isSelected()){
    	//	if(runSumCheckBox.isSelected() || rotateCheckBox.isSelected()){
    	//		if(!exportToBhpsuCheckBox.isSelected()){
    	//			JOptionPane.showMessageDialog(this, "Must select \"Export result to BHP-SU cube\" to perform amplitude extraction.",
        //               "Invalid data entry", JOptionPane.WARNING_MESSAGE);
    	//			return false;
    	//		}
    	//	}
    	//}
    	return true;
    }

    public void setLandmarkOutProjectName(String name){
    	if(landmarkAgent == null)
    		landmarkAgent = LandmarkServices.getInstance();
    	int ptype = landmarkAgent.getLandmarkProjectType(name);
    	if(ptype == 2){
    		brickExtLabel.setText(" (process level - 2D)");
    		//if(parentGUI.getLandmark2DProjectText().equals(name)){
    		//	String processLevel = parentGUI.getSelectedLandmark2DProcessLevel();
    		//	if(processLevel != null)
    		//		volumeNameTextField.setText(processLevel);
    		//}
    		volumeNameTextField.setToolTipText("2D process level and version name can have up to 9 characters.");
    	}
    	else if(ptype == 3)
    		brickExtLabel.setText(".bri ( 3D )");
    	landmarkProjectTextField.setText(name);
    }

    public boolean isExportToLandmarkChecked() {
    	return exportToLandmarkCheckBox.isSelected();
    }

    public String getVolumeName(){
    	return volumeNameTextField.getText().trim();
    }

    public String getLandmarkProjectName(){
    	return landmarkProjectTextField.getText().trim();
    }

    public boolean isRotateChecked() {
    	return rotateCheckBox.isSelected();
    }

    public boolean isRunsumChecked(){
    	return runSumCheckBox.isSelected();
    }

    public boolean isNotRunAmpExtChecked(){
    	return notRunAmpExtCheckBox.isSelected();
    }

    public String genState(){
    	StringBuffer content = new StringBuffer();
    	//if(rotateCheckBox.isSelected() || runSumCheckBox.isSelected() || exportToBhpsuCheckBox.isSelected()){
		content.append("<DataProcessing");
		if(rotateCheckBox.isSelected())
			content.append(" rotateChecked=\"" + "yes" + "\"\n");
		if(rotateDegreeTextField.getText().trim().length() > 0)
			content.append(" rotate=\"" + rotateDegreeTextField.getText().trim() + "\"\n");
		if(runSumCheckBox.isSelected())
			content.append(" runsumChecked=\"" + "yes" + "\"\n");
		if(widthTextField.getText().trim().length() > 0)
			content.append(" runsum_width=\"" + widthTextField.getText().trim() + "\"\n");
		if(exportToBhpsuCheckBox.isSelected())
			content.append(" bhpsuChecked=\"" + "yes" + "\"\n");
		if(bhpsuCubePrefixTextField.getText().trim().length() > 0)
			content.append(" bhpsu_prefix=\"" + bhpsuCubePrefixTextField.getText().trim() + "\"\n");

		if(exportToLandmarkCheckBox.isSelected())
			content.append(" landmarkChecked=\"" + "yes" + "\"\n");
		if(landmarkProjectTextField.getText().trim().length() > 0)
			content.append(" lm_project=\"" + landmarkProjectTextField.getText().trim() + "\"\n");
		if(volumeNameTextField.getText().trim().length() > 0)
			content.append(" lm_volume_name=\"" + volumeNameTextField.getText().trim() + "\"\n");

		if(notRunAmpExtCheckBox.isSelected()){
			content.append(" runAmpExt=\"" + "no" + "\"\n");
		}
		
		if(interpolateHorizonCheckBox.isSelected()){
			content.append(" interpolate=\"" + "yes" + "\"\n");
		}

		content.append("/>\n");
    	//}
    	return content.toString();
    }

    public boolean hasStates(){
    	return rotateCheckBox.isSelected() || runSumCheckBox.isSelected() || exportToBhpsuCheckBox.isSelected() || interpolateHorizonCheckBox.isSelected();
    }

    public void restoreState(Node node){
    	Element el = (Element)node;

    	String attr = el.getAttribute("rotateChecked");
    	if(attr != null && attr.equals("yes")){
			rotateCheckBox.setSelected(true);
			rotateDegreeTextField.setEnabled(true);
    	}else{
    		rotateCheckBox.setSelected(false);
    		rotateDegreeTextField.setEnabled(false);
    	}

    	attr = el.getAttribute("rotate");
    	if(attr != null && attr.length() > 0){
    		rotateDegreeTextField.setText(attr);
    	}

    	attr = el.getAttribute("runsumChecked");
		if(attr != null && attr.equals("yes")){
			runSumCheckBox.setSelected(true);
    		widthTextField.setEnabled(true);
    	}else{
    		runSumCheckBox.setSelected(false);
    		widthTextField.setEnabled(false);
    	}

    	attr = el.getAttribute("runsum_width");
    	if(attr != null && attr.length() > 0){
    		widthTextField.setText(attr);
    	}

    	attr = el.getAttribute("runAmpExt");
		if(attr != null && attr.length() > 0){
			notRunAmpExtCheckBox.setSelected(true);
			notRunAmpExtCheckBox.setEnabled(true);
    	}

    	attr = el.getAttribute("bhpsuChecked");
		if(attr != null && attr.equals("yes")){
			exportToBhpsuCheckBox.setSelected(true);
			exportToBhpsuCheckBox.setEnabled(true);
			bhpsuCubePrefixTextField.setEnabled(true);
		}else{
			exportToBhpsuCheckBox.setSelected(false);
			bhpsuCubePrefixTextField.setEnabled(false);
		}

		attr = el.getAttribute("interpolate");
		if(attr != null && attr.trim().equals("yes"))
			interpolateHorizonCheckBox.setSelected(true);
		else
			interpolateHorizonCheckBox.setSelected(false);

		attr = el.getAttribute("landmarkChecked");
		if(attr != null && attr.equals("yes")){
			exportToLandmarkCheckBox.setEnabled(true);
			exportToLandmarkCheckBox.setSelected(true);
			landmarkProjectListButton.setEnabled(true);
			landmarkProjectTextField.setEnabled(true);
			volumeNameTextField.setEnabled(true);
		}else{
			exportToLandmarkCheckBox.setSelected(false);
			landmarkProjectListButton.setEnabled(false);
			landmarkProjectTextField.setEnabled(false);
			volumeNameTextField.setEnabled(false);
		}

		attr = el.getAttribute("lm_project");
		if(attr != null && attr.length() > 0){
			landmarkProjectTextField.setText(attr);
		}

		attr = el.getAttribute("lm_volume_name");
		if(attr != null && attr.length() > 0){
			volumeNameTextField.setText(attr);
    	}
    }

    public float getRotationPhase(){
    	if(rotateCheckBox.isSelected()){
    		String deg = rotateDegreeTextField.getText().trim();
    		if(deg.length() > 0){
    			try{
    				return Float.valueOf(deg).floatValue();
    			}catch(Exception e){
    				logger.info(deg + " is not a valid number.");
    				return -1;
    			}
    		}
    	}
    	return -1;
    }

    public float getRunsumWidth(){
    	if(runSumCheckBox.isSelected()){
    		String w = widthTextField.getText().trim();
    		if(w.length() > 0){
    			try{
    				return Float.valueOf(w).floatValue();
    			}catch(Exception e){
    				logger.info(w + " is not a valid number.");
    				return -1;
    			}
    		}
    	}
    	return -1;
    }

    public boolean isExportBhpsuChecked(){
    	return exportToBhpsuCheckBox.isSelected();
    }
    
    public boolean isInterpolateHorizonChecked(){
    	return interpolateHorizonCheckBox.isSelected();
    }

    public String getExportSeismicPrefix(){
    	return bhpsuCubePrefixTextField.getText().trim();
    }

    // Variables declaration - do not modify
    private javax.swing.JTextField bhpsuCubePrefixTextField;
    private javax.swing.JLabel brickExtLabel;
    private javax.swing.JCheckBox exportToBhpsuCheckBox;
    private javax.swing.JCheckBox exportToLandmarkCheckBox;
    private javax.swing.JCheckBox interpolateHorizonCheckBox;    
    private javax.swing.JLabel landmarkProjectLabel;
    private javax.swing.JButton landmarkProjectListButton;
    private javax.swing.JTextField landmarkProjectTextField;
    private javax.swing.JLabel lm2BhpsuPrefixLabel;
    private javax.swing.JCheckBox rotateCheckBox;
    private javax.swing.JLabel rotateDegreeLabel1;
    private javax.swing.JLabel rotateDegreeLabel2;
    private javax.swing.JLabel rotateDegreeLabel3;
    private javax.swing.JTextField rotateDegreeTextField;
    private javax.swing.JCheckBox runSumCheckBox;
    private javax.swing.JLabel volumeNameLabel;
    private javax.swing.JTextField volumeNameTextField;
    private javax.swing.JLabel widthLabel;
    private javax.swing.JTextField widthTextField;
    private javax.swing.JLabel widthUnitLabel;
    private LandmarkServices landmarkAgent;
    private javax.swing.JCheckBox notRunAmpExtCheckBox;
    // End of variables declaration

}
