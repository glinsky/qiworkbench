/*
###########################################################################
# AmpExt - horizon-based Secant amplitude and area extraction
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
 */
package com.bhpb.AmplitudeExtraction;

import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.client.util.QiFileChooserDescriptor;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ComponentStateUtils;
import com.bhpb.qiworkbench.compAPI.ControlConstants;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.ProgressUtil;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.compAPI.XmlUtils;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.services.landmark.LandmarkServices;
import com.bhpb.qiworkbench.util.ExceptionMessageFormatter;
import com.bhpb.qiworkbench.workbench.QiComponentBase;

/**
 * Amplitude Extraction plugin.It is started as a thread by the Messenger
 * Dispatcher that executes the "Activate plugin" command.
 *
 * @author Bob Miller
 * @author Gil Hansen
 * @version 1.2
 */
public class AmpExtPlugin extends QiComponentBase implements IqiWorkbenchComponent, Runnable {

    private static Logger logger = Logger.getLogger(AmpExtPlugin.class.getName());

    // messaging mgr for this class only
    private MessagingManager messagingMgr;

    //private LandmarkProjectHandler projectHandler;
    private LandmarkServices landmarkAgent;
    // gui component
    private AmplitudeExtraction gui;

    // my thread
    private static Thread pluginThread;
    /** CID for component instance. Generated before the thread is started and carried as the thread's name. */
    private String myCID = "";

    protected String getCID() {
        return myCID;
    }
    /** Component descriptor of associated qiProject Manager. null means no association with a PM has been made. */
    private ComponentDescriptor projMgrDesc = null;
    /** Metadata of project associated with */
    private QiProjectDescriptor qiProjectDesc = new QiProjectDescriptor();

    public QiProjectDescriptor getQiProjectDescriptor() {
        return qiProjectDesc;
    }

    public IComponentDescriptor getComponentDescriptor() {
        return messagingMgr.getMyComponentDesc();
    }

    /**
     * Get the component descriptor of the associated PM and therefore project.
     * @return Component descriptor of the associated PM
     */
    public ComponentDescriptor getQiProjMgrDescriptor() {
        return projMgrDesc;
    }

    /**
     * Get AmpExt agent's messaging manager.
     * @return MessagingManager for the AmpExt agent.
     */
    public MessagingManager getMessagingMgr() {
        return messagingMgr;
    }

    // wait is used to suspend message processing
    private boolean wait = false;

    // boolean used to stop plugin thread
    private boolean pleaseStop = false;
    private static int saveAsCount = 0;
    /** Display step */
    private int stepState = ControlConstants.SESSION_STEP;
    /** Step action */
    private int actionState = ControlConstants.START_SESSION;
    /** Finite state machine for displaying the results of a generated script */
    private DisplayTransducer displayTransducer;

    /**
     * Initialize the plugin component:
     * <ul>
     * <li>Create its messaging manager</li>
     * <li>Register with the Message Dispatcher</li>
     * </ul>
     */
    public void init() {
        try {
            QIWConstants.SYNC_LOCK.lock();
            messagingMgr = new MessagingManager();

            myCID = Thread.currentThread().getName();

            // register self with the Message Dispatcher
            messagingMgr.registerComponent(QIWConstants.PLUGIN_AGENT_COMP, AmpExtConstants.AMP_EXT_PLUGIN_NAME, myCID);

            // notify the startup method initialization finished
            QIWConstants.SYNC_LOCK.unlock();
            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in AmpExtPlugin.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

    /**
     * Generate state information into xml string format
     * @return AmpExt's state info represented in XML.
     */
    public String genState() {
        StringBuffer content = new StringBuffer();
        IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
        //TODO WorkbenchStateManager will need to be changed to conform to the package change of AmpExtPlugin in ServletDispatcher
        content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc) + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + CompDescUtils.getDescPreferredDisplayName(desc) + "\">\n");
        content.append(gui.saveState());
        content.append("</component>\n");

        return content.toString();
    }

    /**
     * Generate state information into xml string format in repsonse to save as command
     * @return  String
     */
    public String genStateAsClone() {
        StringBuffer content = new StringBuffer();
        IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
        String displayName = "";
        saveAsCount++;
        if (saveAsCount == 1) {
            displayName = "CopyOf" + CompDescUtils.getDescPreferredDisplayName(desc);
        } else {
            displayName = "Copy(" + saveAsCount + ")Of" + CompDescUtils.getDescPreferredDisplayName(desc);
        }
        content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc) + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + displayName + "\">\n");
        content.append(gui.saveState());
        content.append("</component>\n");

        return content.toString();
    }

    /** restore plugin and it's gui to previous condition
     *
     * @param node xml containing saved state variables
     */
    void restoreState(Node node) {
        String preferredDisplayName = ((Element) node).getAttribute("preferredDisplayName");
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals(AmplitudeExtraction.class.getName())) {
                    //get project descriptor from state since buildGUI needs it
                    qiProjectDesc = getProjectDesc(child);

                    //NOTE; The constructor will extract the project and fileSystem from the
                    //project descriptor.
                    gui = new AmplitudeExtraction(this);
                    gui.restoreState(child);

                    if (preferredDisplayName != null && preferredDisplayName.trim().length() > 0) {
                        CompDescUtils.setDescPreferredDisplayName(messagingMgr.getMyComponentDesc(), preferredDisplayName);
                        gui.renamePlugin(preferredDisplayName);
                    }
                }
            }
        }
    }

    /**
     * Import state saved in a XML file. Use existing GUI. User not asked to save current
     * state first, because after import, saving state will save imported state. The preferred
     * display name is not changed. Therefore, the name in the GUI title and component tree node
     * don't have to change.
     *
     * @param node XML containing saved state variables
     */
    void importState(Node node) {
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals(AmplitudeExtraction.class.getName())) {
                    //get project descriptor from state since GUI needs it
                    //keep the associated project
//                    qiProjectDesc = getProjectDesc(child);
                    gui.restoreState(child);

                //set title of window
                //Not necessary to reset the title of the window, for the project didn't change
//                    String projectName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
//                    gui.resetTitle(projectName);
                }
            }
        }
    }

    /**
     * Extract the project descriptor from the saved state. If the state
     * does not contain a project descriptor, form one from the implicit
     * project so backward compatible.
     *
     * @return Project descriptor for project associated with the bhpViewer.
     */
    private QiProjectDescriptor getProjectDesc(Node node) {
        QiProjectDescriptor projDesc = null;
        /*
        if (!node.getNodeName().equals(BhpViewerBase.class.getName())) {
        ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(), "restoreState error: State not bhpViewer's but of " + node.getNodeName());
        return;
        }
         */
        NodeList children = node.getChildNodes();
        String nodeName;
        Node child;
        for (int i = 0; i < children.getLength(); i++) {
            child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                nodeName = child.getNodeName();
                if (nodeName.equals("QiProjectDescriptor")) {
                    projDesc = (QiProjectDescriptor) XmlUtils.XmlToObject(child);
                    break;
                }
            }
        }

        //Check if saved state has a project descriptor. If not, create
        //one using the implicit project so backward compatible.
        if (projDesc == null) {
            projDesc = new QiProjectDescriptor();
            //get path of implicit project from Message Dispatcher
            String projPath = messagingMgr.getProject();
            QiProjectDescUtils.setQiSpace(projDesc, projPath);
            //leave relative location of project as "/"
            int idx = projPath.lastIndexOf("\\");
            if (idx == -1) {
                idx = projPath.lastIndexOf("/");
            }
            String projectName = projPath.substring(idx + 1);
            QiProjectDescUtils.setQiProjectName(projDesc, projectName);
        }

        return projDesc;
    }

    /**
     * Have the state manager store the state
     */
    public void saveState() {
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SAVE_COMP_CMD, stMgrDesc,
                QIWConstants.STRING_TYPE, messagingMgr.getMyComponentDesc());
    }

    public void saveStateAsClone() {
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SAVE_COMP_AS_CLONE_CMD, stMgrDesc,
                QIWConstants.STRING_TYPE, messagingMgr.getMyComponentDesc());
    }

    /**
     * call the state manager to store the state then quit the component
     */
    public void saveStateThenQuit() {
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SAVE_COMP_THEN_QUIT_CMD, stMgrDesc,
                QIWConstants.STRING_TYPE, messagingMgr.getMyComponentDesc());
    }

    /** Initialize the plugin, then start processing messages its receives.
     * The pleaseStop flag is set when user has terminated the GUI, and it is time to quit
     * The wait flag is set by doFileChooser during initialization
     * If a message has the skip flag set, it is to be handled by the GUI, not this class
     */
    public void run() {
        // initialize the Amplitude Extraction plugin
        init();

        while (wait) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ie) {
                logger.warning("While waiting for init() to complete, caught: " + ie.getMessage());
            }
        }

        String myPid = QiProjectDescUtils.getPid(qiProjectDesc);
        while (projMgrDesc == null && !myPid.equals("")) {
            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_ASSOC_PROJMGR_CMD, QIWConstants.STRING_TYPE, myPid);
            try {
                Thread.sleep(1000);
                if (!messagingMgr.isMsgQueueEmpty()) {
                    break;
                }
            } catch (InterruptedException ie) {
                logger.warning("While waiting for projMgrDesc, caught: " + ie.getMessage());
            }
        }

        //process any messages received from other components
        while (!pleaseStop) {
            //wait until a message arrives
            IQiWorkbenchMsg msg = messagingMgr.peekNextMsg();
            if (msg != null) {
                if (msg.getMsgKind().equals(QIWConstants.DATA_MSG) && msg.skip()) {
                    if (messagingMgr.getMatchingOutstandingRequest(msg) == null) {
                        msg = messagingMgr.getNextMsg();
                        logger.warning("Consuming response message which lacks a matching outstanding request. Response msg: " + msg);
                    }
                } else {
                    msg = messagingMgr.getNextMsgWait();
                    try {
                        processMsg(msg);
                    } catch (Exception e) {
                        logger.warning(ExceptionMessageFormatter.getFormattedMessage(e));
                    }
                }
            }
        }
    }

    /**
     * Find the request matching the response and process the response based on the request.
     * @param msg QiWorkbenchMsg that came off the queue
     */
    public void processMsg(IQiWorkbenchMsg msg) {
        /** Request that matches the response */
        IQiWorkbenchMsg request = null;

        //log message traffic
        logger.fine("msg=" + MsgUtils.toString(msg));

        // Check if a response. If so, process and consume response
        if (messagingMgr.isResponseMsg(msg)) {
            request = messagingMgr.checkForMatchingRequest(msg);
            String cmd = MsgUtils.getMsgCommand(request);
            // check if from the message dispatcher
            if (messagingMgr.isResponseFromMsgDispatcher(msg)) {
                if (cmd.equals(QIWConstants.NULL_CMD)) {
                    return;
                }
            } else if (cmd.equals(QIWConstants.SUBMIT_JOB_CMD)) {
                String jobID = (String) msg.getContent();
                logger.info("jobID=" + jobID);
            } else if (cmd.equals(QIWConstants.GET_RESULT_FROM_QI_FILE_CHOOSER_CMD)) {
                QiFileChooserDescriptor desc = (QiFileChooserDescriptor) msg.getContent();
                if (desc.getReturnCode() == JFileChooser.APPROVE_OPTION) {
                    List<String> list = desc.getSelectedFileNameList();
                    if (!desc.isMultiSelectionEnabled()) { //single file selection mode
                        String filesep = messagingMgr.getServerOSFileSeparator();
                        String filePath = desc.getSelectedFilePathBase() + filesep + list.get(0);
                        String command = desc.getMessageCommand();
                        //Component comp = desc.getParentGUI();
                        if (command.equals(AmpExtConstants.GET_SEISMIC_DATASET_CMD)) {
                            Map map = desc.getAdditionalProperties();
                            if (map != null) {
                                javax.swing.JTextField targetField = (javax.swing.JTextField) map.get("target");
                                if (targetField != null) {
                                    runBHPIOTask(filePath, targetField);
                                }
                            }

                        }
                    }
                }
            } // route the result from file chooser service back to its caller
            else if (cmd.equals(QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD)) {
                ArrayList list = (ArrayList) MsgUtils.getMsgContent(msg);
                final String selectionPath = (String) list.get(1);

                if (((Integer) list.get(0)).intValue() == JFileChooser.APPROVE_OPTION) {
                    String fileName = (String) list.get(1);
                    String action = (String) list.get(3);
                    if (action.equals(AmpExtConstants.AMP_EXT_SELECT_INPUT)) {
                        //Note: fileName the full pathname. Needs to be saved as part of the
                        //state so header limits re-obtained.
                        gui.setInput(fileName);
                        gui.setEntireDatasetButtonEnabled(true);
                    } else if (action.equals(AmpExtConstants.AMP_EXT_SELECT_TOP_HORIZON)) {
                        gui.setTopHorizon(fileName);
                    } else if (action.equals(AmpExtConstants.AMP_EXT_SELECT_BOTTOM_HORIZON)) {
                        gui.setBottomHorizon(fileName);
                    } else if (action.equals(AmpExtConstants.AMP_EXT_SELECT_HORIZON)) {
                        gui.setHorizon(fileName);
                    } //                  if (action.equals(AmpExtConstants.AMP_EXT_LOAD_SCRIPT))
                    //                      gui.loadScript(fileName);
                    /*
                    if (action.equals(AmpExtConstants.AMP_EXT_SAVE_SCRIPT))
                    gui.saveScript(fileName);
                    if (action.equals(AmpExtConstants.AMP_EXT_EXECUTE_SCRIPT)) {
                    gui.saveScript(fileName);
                    gui.execScript(fileName);
                    }
                     */ else if (action.equals(QIWConstants.EXPORT_COMP_STATE)) {
                        System.out.println("AmpExt: export state to " + selectionPath);
                        //Write XML state to the specified file and location
                        String pmState = genState();
                        String xmlHeader = "<?xml version=\"1.0\" ?>\n";
                        //TODO: handle remote case - use an IO service
                        try {
                            ComponentStateUtils.writeState(xmlHeader + pmState, selectionPath, messagingMgr);
                        } catch (QiwIOException qioe) {
                            //TODO notify user cannot write state file
                            logger.finest(qioe.getMessage());
                        }
                    } else if (action.equals(QIWConstants.IMPORT_COMP_STATE)) {
                        //TODO: handle remote case - use an IO service
                        //check if file exists
                        if (ComponentStateUtils.stateFileExists(selectionPath, messagingMgr)) {
                            try {
                                String pmState = ComponentStateUtils.readState(selectionPath, messagingMgr);
                                Node compNode = ComponentStateUtils.getComponentNode(pmState);
                                if (compNode == null) {
                                    JOptionPane.showMessageDialog(gui, "Can't convert XML state to XML object", "Internal Processing Error", JOptionPane.ERROR_MESSAGE);
                                } else {
                                    //check for matching component type
                                    String compType = ((Element) compNode).getAttribute("componentType");
                                    String expectedCompType = messagingMgr.getRegisteredComponentDisplayNameByDescriptor(messagingMgr.getMyComponentDesc());
                                    if (!compType.equals(expectedCompType)) {
                                        JOptionPane.showMessageDialog(gui, "State file not for this qiComponent type. Select again", "Selection Error", JOptionPane.ERROR_MESSAGE);
                                    } else {
                                        //Note: don't ask if want to save state before importing because
                                        //      for future saves, the imported state will be saved.

                                        //import state
                                        importState(compNode);
                                    }
                                }
                            } catch (QiwIOException qioe) {
                                JOptionPane.showMessageDialog(gui, "Can't read state file. Select again.", "Processing Error", JOptionPane.ERROR_MESSAGE);
                                logger.finest(qioe.getMessage());
                            }
                        } else {
                            JOptionPane.showMessageDialog(gui, "State file doesn't exist or is not a file. Select again.", "Selection Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            } else if (cmd.equals(QIWConstants.GET_PROJ_INFO_CMD)) {
                ArrayList projInfo = (ArrayList) msg.getContent();
                qiProjectDesc = (QiProjectDescriptor) projInfo.get(1);

                //reset window title (if GUI up)
                if (gui != null) {
                    String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
                    gui.resetTitle(projName);
                }
            } else if (cmd.equals(QIWConstants.GET_ASSOC_PROJMGR_CMD)) {
                ArrayList projInfo = (ArrayList) msg.getContent();
                projMgrDesc = (ComponentDescriptor) projInfo.get(0);
                qiProjectDesc = (QiProjectDescriptor) projInfo.get(1);
                String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
                gui.resetTitle(projName);
            } else if (cmd.equals(QIWConstants.CONTROL_QIVIEWER_CMD)) {
                displayTransducer.processResp(msg);
            } // TODO other possible responses...
            else {
                logger.warning("AmpExtPlugin: Response to " + cmd + " command not processed " + MsgUtils.toString(msg));
            }
            return;
        } //Check if a request. If so, process and send back a response
        else if (messagingMgr.isRequestMsg(msg)) {
            String cmd = MsgUtils.getMsgCommand(msg);
            // deactivate plugin came from user, tell gui to quit and stop run method
            if (cmd.equals(QIWConstants.DEACTIVATE_PLUGIN_CMD) || cmd.equals(QIWConstants.REMOVE_COMPONENT_CMD) || cmd.equals(QIWConstants.DEACTIVATE_COMPONENT_CMD) || cmd.equals(QIWConstants.REMOVE_PLUGIN_CMD)) {
                if (AmpExtConstants.DEBUG_PRINT > 0) {
                    logger.info(CompDescUtils.getDescPreferredDisplayName(messagingMgr.getMyComponentDesc()) + " deleting");
                }
                try {
                    if (gui != null && gui.isVisible()) {
                        gui.closeAmpExtGUI();
                    }
                    // unregister plugin
                    messagingMgr.unregisterComponent(messagingMgr.getComponentDesc(myCID));
                } catch (Exception e) {
                    IQiWorkbenchMsg res = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Unknown exception occurred with " + myCID);
                    messagingMgr.routeMsg(res);
                }
                pleaseStop = true;
                messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, myCID + " is successfully deactivated.");
            } else if (cmd.equals(QIWConstants.OPEN_COMPONENT_GUI_CMD)) {
                if (gui != null) {
                    gui.setVisible(true);
                }
            } else if (cmd.equals(QIWConstants.CLOSE_COMPONENT_GUI_CMD)) {
                if (AmpExtConstants.DEBUG_PRINT > 0) {
                    System.out.println(myCID + " quitting");
                }
                messagingMgr.sendResponse(msg, Component.class.getName(), (Component) gui);
                if (gui != null && gui.isVisible()) {

                    gui.closeAmpExtGUI();
                }
            } else if (cmd.equals(QIWConstants.SET_PREFERRED_DISPLAY_NAME_CMD)) {
                String preferredDisplayName = (String) MsgUtils.getMsgContent(msg);
                if (preferredDisplayName != null && preferredDisplayName.trim().length() > 0) {
                    messagingMgr.getMyComponentDesc().setPreferredDisplayName(preferredDisplayName);
                    gui.renamePlugin(preferredDisplayName);
                }
                messagingMgr.sendResponse(msg, QIWConstants.COMP_DESC_TYPE, messagingMgr.getMyComponentDesc());
            } // save state
            else if (cmd.equals(QIWConstants.SAVE_COMP_CMD)) {
                messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, genState());
            } //save component state as clone
            else if (cmd.equals(QIWConstants.SAVE_COMP_AS_CLONE_CMD)) {
                messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, genStateAsClone());
            // restore state
            } else if (cmd.equals(QIWConstants.RESTORE_COMP_CMD)) {
                Node node = (Node) MsgUtils.getMsgContent(msg);
                //displayNode(node);
                restoreState(node);

                // Create the finite state machine that will drive the bhpViewer when
                // displaying the results of the generated AmpExt script.
                displayTransducer = new DisplayTransducer(gui, messagingMgr, stepState, actionState);

                messagingMgr.sendResponse(msg, AmplitudeExtraction.class.getName(), gui);
            } // invoke is done after activate
            else if (cmd.equals(QIWConstants.INVOKE_SELF_CMD)) {
                // Do nothing if don't have a GUI.
                // Create the qiComponent's GUI, but don't make it visible. It is
                // up to the Workbench Manager who requested the qiComponent to be
                // activated. Pass GUI the CID of its parent.
                // check to see if content incudes xml information which means invoke self will
                // do the restoration
                ArrayList msgList = (ArrayList) msg.getContent();
                String invokeType = (String) msgList.get(0);
                if (invokeType.equals(QIWConstants.COMPONENT_INVOKE_RESTORED)) {
                    Node node = ((Node) ((ArrayList) MsgUtils.getMsgContent(msg)).get(2));
                    //displayNode(node);
                    restoreState(node);
                } else if (invokeType.equals(QIWConstants.COMPONENT_INVOKE_NEW)) {
                    gui = new AmplitudeExtraction(this);
                    java.awt.Point p = (java.awt.Point) msgList.get(1);
                    if (p != null) {
                        gui.setLocation(p);
                    }
                }

                // Create the finite state machine that will drive the bhpViewer when
                // displaying the results of the generated AmpExt script.
                displayTransducer = new DisplayTransducer(gui, messagingMgr, stepState, actionState);

                //Send a normal response back with the qiComponent's JInternalFrame and
                //let the Workbench Manager add it to the desktop and make
                //it visible.
                messagingMgr.sendResponse(msg, AmplitudeExtraction.class.getName(), gui);
            } // user selected rename plugin menu item
            else if (cmd.equals(QIWConstants.RENAME_COMPONENT)) {
                ArrayList<String> names = new ArrayList<String>(2);
                names = (ArrayList) MsgUtils.getMsgContent(msg);
                if (AmpExtConstants.DEBUG_PRINT > 0) {
                    System.out.println("Rename myself from " + names.get(0) + " to " + names.get(1));
                }
                gui.renamePlugin(names.get(1));
            } else if (cmd.equals(QIWConstants.NOTIFY_ASSOC_PROJMGR_CMD)) {
                //Just a notification. No response required or expected.
                projMgrDesc = (ComponentDescriptor) msg.getContent();
                //Note: Notification couldn't contain info about PM's project because GUI may not be up yet.
                if (projMgrDesc != null) {
                    messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_PROJ_INFO_CMD, projMgrDesc);
                }
            } else if (cmd.equals(QIWConstants.NOTIFY_PROJ_INFO_CHANGED_CMD)) {
                ArrayList info = (ArrayList) msg.getContent();
                ComponentDescriptor pmDesc = (ComponentDescriptor) info.get(0);
                QiProjectDescriptor projDesc = (QiProjectDescriptor) info.get(1);
                //ignore message if not from associated PM
                if (projMgrDesc != null && pmDesc.getCID().equals(projMgrDesc.getCID())) {
                    qiProjectDesc = projDesc;
                    //update window title
                    String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
                    gui.resetTitle(projName);
                }
            } else {
                logger.warning("AmpExtPlugin: Request not processed, requeued: " + MsgUtils.toString(msg));
            }
            return;
        }
    }
    int depth = 0;

    private void displayNode(Node node) {
        StringBuffer sep = new StringBuffer();
        if (node instanceof Element) {
            Element e = (Element) node;
            for (int k = 0; k < depth; k++) {
                sep.append("  ");
            }
            System.out.println(sep + node.getNodeName());
            NamedNodeMap map = e.getAttributes();
            sep.append("  ");
            for (int j = 0; j < map.getLength(); j++) {
                String name = map.item(j).getNodeName();
                String value = e.getAttribute(name);
                System.out.println(sep.toString() + name + "=" + value);
            }
        }

        if (node.hasChildNodes()) {
            depth++;
        }
        NodeList children = node.getChildNodes();

        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            displayNode(child);
        }
    }

    /** Quit qiComponent..
     */
    public void deactivateSelf() {
        //ask WorkbenchManager to remove it from workbench GUI then send it back to self to deactivate self
        IComponentDescriptor wbMgr = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_MGR_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.QUIT_COMPONENT_CMD, wbMgr, QIWConstants.STRING_TYPE, messagingMgr.getMyComponentDesc());

        return;
    }

    /**
     * showErrorDialog is called when ErrorDialogService is needed
     * @param type is QIWConstants.ERROR_DIALOG or QIWConstants.WARNING_DIALOG
     * @param message is one-line message
     * @param stack is StackTrace
     * @param causes is list of possible causes
     * @param suggestions is list of suggested remedies
     */
    protected void showErrorDialog(String type, StackTraceElement[] stack, String message,
            String[] causes, String[] suggestions) {
        ArrayList list = new ArrayList(7);
        // component
        list.add(gui);
        // message type
        list.add(type);
        // caller's display name
        list.add(getComponentDescriptor().getDisplayName());
        // stack trace
        list.add(stack);
        // message
        list.add(message);
        // possible causes
        list.add(causes);
        // suggested remedies
        list.add(suggestions);
        // send message to start error service
        getErrorService(list);
    }
    private List<Object> datasetInfoList;

    /**
     * Get the seismic dataset information by calling SU script bhpio given the data input, then retrieve the output,
     * parse the output and form the data structure
     * @param filePath file path of the seismic dataset input
     * @return status indicating normal 0 or abnormal non 0
     */
    private int getSeismicDatasetInfo(String filePath) {
        if (filePath == null || filePath.trim().length() == 0) {
            return 1;
        }
        ArrayList<String> params = new ArrayList<String>();
        // make sure script is executable
        params.add(messagingMgr.getLocationPref());
        params.add("bash");
        params.add("-c");
        //String fileDir = filePath.substring(0,filePath.lastIndexOf(File.separator));
        String filesep = messagingMgr.getServerOSFileSeparator();
        String fileDir = filePath.substring(0, filePath.lastIndexOf(filesep));
        //String fileName = filePath.substring(filePath.lastIndexOf(File.separator)+1);
        String fileName = filePath.substring(filePath.lastIndexOf(filesep) + 1);
        fileName = fileName.substring(0, fileName.lastIndexOf(".dat"));
        params.add(" source ~/.bashrc; cd " + fileDir + "; bhpio filename=" + fileName);
        params.add("true");//blocking
        // submit job for execution
        int status = 0;
        int errorStatus = 0;
        String errorContent = "";

        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.SUBMIT_JOB_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        //QiWorkbenchMsg response = messagingMgr.getNextMsgWait();
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, 10000);
        if (response == null || MsgUtils.isResponseAbnormal(response)) {
            if (response != null) {
                errorStatus = MsgUtils.getMsgStatusCode(response);
                errorContent = (String) MsgUtils.getMsgContent(response);
                logger.info("SUBMIT_JOB_CMD errorContent = " + errorContent);
                JOptionPane.showMessageDialog(gui, "Error in running SU script.",
                        "Problem in running SU script: " + errorContent, JOptionPane.WARNING_MESSAGE);
            }
            //return status = -1;
            return errorStatus;
        }
        String jobID = (String) MsgUtils.getMsgContent(response);
        status = MsgUtils.getMsgStatusCode(response);
        logger.info("SUBMIT_JOB_CMD jobID = " + jobID);
        logger.info("status of SUBMIT_JOB_CMD normal response " + status);
        //get stdout
        params.clear();
        //params.add(QIWConstants.LOCAL_SERVICE_PREF);
        params.add(messagingMgr.getLocationPref());
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_JOB_OUTPUT_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        response = messagingMgr.getMatchingResponseWait(msgID, 10000);

        if (response == null || MsgUtils.isResponseAbnormal(response)) {
            if (response != null) {
                errorStatus = MsgUtils.getMsgStatusCode(response);
                errorContent = (String) MsgUtils.getMsgContent(response);
                logger.info("GET_JOB_OUTPUT_CMD errorContent= " + errorContent);
            }
            //return status = -1;
            return errorStatus;
        } else {
            ArrayList stdOut = (ArrayList) MsgUtils.getMsgContent(response);
            status = MsgUtils.getMsgStatusCode(response);
            logger.info("status of GET_JOB_OUTPUT_CMD normal response " + status);
            System.out.println("stdOut.size() of GET_JOB_OUTPUT_CMD =" + stdOut.size());
            for (int i = 0; i < stdOut.size(); i++) {
                System.out.println(stdOut.get(i));
            }
            datasetInfoList = parseBHPIOOutput(stdOut);
            logger.info("eismicDataset header info = " + datasetInfoList.get(0));
            logger.info("shorizon info = " + datasetInfoList.get(1));
        }

        //get stderr
        params.clear();
        //params.add(QIWConstants.LOCAL_SERVICE_PREF);
        params.add(messagingMgr.getLocationPref());
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        response = messagingMgr.getMatchingResponseWait(msgID, 10000);

        if (response == null || MsgUtils.isResponseAbnormal(response)) {
            if (response != null) {
                errorStatus = MsgUtils.getMsgStatusCode(response);
                errorContent = (String) MsgUtils.getMsgContent(response);
                logger.info("GET_JOB_ERROR_OUTPUT_CMD errorContent = " + errorContent);
                JOptionPane.showMessageDialog(gui, "Error in running SU script.",
                        "Problem in running SU script: " + errorContent, JOptionPane.WARNING_MESSAGE);
                return status;
            }
            //return status = -1;
            return status;
        } else {
            ArrayList stdErr = (ArrayList) MsgUtils.getMsgContent(response);

            for (int i = 0; i < stdErr.size(); i++) {
                logger.info("GET_JOB_ERROR_OUTPUT_CMD " + stdErr.get(i));
            }

            status = MsgUtils.getMsgStatusCode(response);
            logger.info("status of GET_JOB_ERROR_OUTPUT_CMD normal response " + status);
            if (stdErr.size() > 0) {
                JOptionPane.showMessageDialog(gui, "Error in running SU script:" + stdErr,
                        "Problem in running SU script", JOptionPane.WARNING_MESSAGE);
            }
        }

        //Wait until the command has finished
        params.clear();
        //params.add(QIWConstants.LOCAL_SERVICE_PREF);
        params.add(messagingMgr.getLocationPref());
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        response = messagingMgr.getMatchingResponseWait(msgID, 10000);
        if (response == null || MsgUtils.isResponseAbnormal(response)) {
            if (response != null) {
                errorStatus = MsgUtils.getMsgStatusCode(response);
                errorContent = (String) MsgUtils.getMsgContent(response);
                logger.info("WAIT_FOR_JOB_EXIT_CMD errorContent = " + errorContent);
            }
            //return status = -1;
            return status;
        } else {
            status = (Integer) MsgUtils.getMsgContent(response);
            logger.info("status of WAIT_FOR_JOB_EXIT_CMD normal response " + status);
        }
        // get exitValue
        params.clear();
        //params.add(QIWConstants.LOCAL_SERVICE_PREF);
        params.add(messagingMgr.getLocationPref());
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_JOB_STATUS_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        response = messagingMgr.getMatchingResponseWait(msgID, 10000);
        if (response == null) {
            return -1;
        } else {
            //return status = (Integer)MsgUtils.getMsgContent(response);
            status = (Integer) MsgUtils.getMsgContent(response);
            logger.info("status of GET_JOB_STATUS_CMD normal job status " + status);
            return status;

        }
    }

    private List<Object> parseBHPIOOutput(List<String> list) {
        if (list == null || list.size() == 0) {
            return null;
        }

        boolean foundHorizon = false;
        List<String> list2 = new ArrayList<String>();
        boolean foundHeaderKey = false;
        String horizonLine = null;
        String datasetName = "";
        for (String s : list) {
            if (foundHeaderKey && s.trim().startsWith("Name:")) {
                list2.add(s);
            }
            if (foundHorizon && s.trim().startsWith("HORIZONS:")) {
                horizonLine = s;
            }
            if (s.startsWith("Number of Header Keys:")) {
                foundHeaderKey = true;
            }
            if (s.contains("is HORIZON data")) {
                int ind = s.indexOf(" ");
                if (ind != -1) {
                    datasetName = s.substring(0, ind);
                }
                foundHorizon = true;
            }
        }
        if (datasetName.length() > 0) {
            datasetName += ":";
        }
        Map<String, DataSetHeaderKeyInfo> map = new HashMap<String, DataSetHeaderKeyInfo>();
        for (String s : list2) {
            String[] s2 = s.split(",");
            //String tag = "Name: ";
            String name = s2[0].substring(s2[0].indexOf(":") + 1).trim();
            // tag = "Min: ";
            String min = s2[1].substring(s2[1].indexOf(":") + 1).trim();
            //tag = "Max: ";
            String max = s2[2].substring(s2[2].indexOf(":") + 1).trim();
            //tag = "Incr: ";
            String incr = s2[3].substring(s2[3].indexOf(":") + 1).trim();
            DataSetHeaderKeyInfo ds = new DataSetHeaderKeyInfo(name, Double.valueOf(min).doubleValue(), Double.valueOf(max).doubleValue(), Double.valueOf(incr).doubleValue());
            map.put(name, ds);
        }

        List<String> horizonList = new ArrayList<String>();
        if (horizonLine != null) {
            int ind = horizonLine.indexOf("HORIZONS:");
            String[] horizons = null;
            if (ind != -1) {
                horizonLine = horizonLine.substring("HORIZONS:".length());
                horizons = horizonLine.trim().split(" +");
            }
            if (horizons != null) {
                for (String s : horizons) {
                    horizonList.add(datasetName + s);
                }
            }
        }
        List<Object> infoList = new ArrayList<Object>();
        infoList.add(map);
        infoList.add(horizonList);
        return infoList;
    }

    private void runBHPIOTask(String file, Component cmp) {
        final Component comp = cmp;
        final String filePath = file;
        Runnable heavyRunnable = new Runnable() {

            public void run() {
                String title = "Gathering input dataset information";
                String text = "Please wait while system is gathering input dataset information....";

                Icon icon = null; //new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));

                com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(comp, 100, true, 500, title, text, icon);
                //monitor.start("");
                ProgressUtil.start(monitor, "");
                int status = getSeismicDatasetInfo(filePath);

                if (ProgressUtil.isCancel(monitor)) {
                    return;
                }
                logger.info("runBHPIOTask status = " + status);
                //if(status == 0 || status == 1 || status == -1){
                if (status == 0) {
                    ProgressUtil.setCurrent(monitor, null, ProgressUtil.getTotal(monitor));
                    List<String> horizonList = (List<String>) datasetInfoList.get(1);
                    if (horizonList == null) {
                        return;
                    }
                    if (horizonList != null && horizonList.size() == 0) {
                        JOptionPane.showMessageDialog(gui, "The selected dataset does not appear to be a HORIZON set. Please choose another dataset and try again.",
                                "Wrong Dataset", JOptionPane.WARNING_MESSAGE);
                        return;
                    }
                    Object[] horizons = horizonList.toArray();
                    //String filesep = messagingMgr.getServerOSFileSeparator();
                    //int ind1 = filePath.lastIndexOf(filesep);
                    //int ind2 = filePath.lastIndexOf(".");
                    //String dataset = "";
                    //if(ind2 != -1)
                    //  dataset = filePath.substring(ind1+1, ind2);
                    //if(dataset.length() > 0)
                    //  dataset += ":";
                    String[] horizonArray = new String[horizons.length];
                    for (int i = 0; i < horizons.length; i++) {
                        String hs = (String) horizons[i];
                        int index = hs.toLowerCase().lastIndexOf(".xyz");
                        if (index != -1) {
                            hs = hs.substring(0, index);
                        }
                        horizonArray[i] = hs;
                    }
                    if (comp instanceof javax.swing.JTextField) {
                        javax.swing.JTextField field = (javax.swing.JTextField) comp;
                        new HorizonSelectDialog(gui, field, true, horizonArray).setVisible(true);
                    }
                } else {
                    ProgressUtil.setCurrent(monitor, null, ProgressUtil.getTotal(monitor));
                }
            }
        };
        new Thread(heavyRunnable).start();

    }

    /**
     * showInternalErrorDialog starts error service for showing internal errors, for example, unexpected NULL etc
     * @param stack stackTrace
     * @param message one-line error message
     */
    public void showInternalErrorDialog(StackTraceElement[] stack, String message) {
        showErrorDialog(QIWConstants.ERROR_DIALOG, stack, message,
                new String[]{"Internal plugin error"},
                new String[]{"Contact workbench support"});
    }

    public QiProjectDescriptor getProjectDescriptor() {
        return qiProjectDesc;
    }

    public List<String> getUnmadeQiProjDescDirs() {
        List params = new ArrayList();
        params.add(messagingMgr.getLocationPref());
        params.add(qiProjectDesc);
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_UNMADE_QIPROJ_DESC_DIRS_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId, 5000);

        if (resp == null) {
            logger.info("Response returning null from command GET_UNMADE_QIPROJ_DESC_DIRS_CMD. Consult qiWorkbench technical support.");
            return null;
        } else if (resp.isAbnormalStatus()) {
            logger.info("Abnormal response from GET_UNMADE_QIPROJ_DESC_DIRS_CMD command:  in getting unmade directories that do not yet exists to meet QiProject's meta data specification. " + (String) resp.getContent());
            JOptionPane.showMessageDialog(gui, "Error in getting directories that do not yet exist to meet QiProject's meta data specification." + " cause: " + (String) resp.getContent(), "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            return null;
        } else {
            return (List<String>) (resp.getContent());
        }
    }

    public boolean makeQiProjDescDirs(List<String> list) {
        if (list == null || list.size() == 0) {
            return true;
        }

        List params = new ArrayList();
        params.add(messagingMgr.getLocationPref());
        params.add(list);
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CREATE_DIRECTORIES_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId, 5000);

        if (resp == null) {
            logger.info("Response returning null from command CREATE_DIRECTORIES_CMD. Consult qiWorkbench technical support.");
            return false;
        } else if (resp.isAbnormalStatus()) {
            logger.info("Abnormal response from CREATE_DIRECTORIES_CMD command:  in getting unmade directories that do not yet exists to meet QiProject's meta data specification. " + (String) resp.getContent());
            JOptionPane.showMessageDialog(gui, "Error in getting directories that do not yet exist to meet QiProject's meta data specification." + " cause: " + (String) resp.getContent(), "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            return false;
        } else {
            logger.info("Normal response from CREATE_DIRECTORIES_CMD command:  " + (String) resp.getContent());
            String result = (String) resp.getContent();
            if (result.equals("success")) {
                return true;
            }
            return false;
        }
    }

    /**
     * getErrorService starts ErrorDialogService
     * @param list ArrayList of arguments
     */
    protected void getErrorService(ArrayList list) {
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_ERROR_DIALOG_SERVICE_CMD, true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, 10000);
        if (response == null || response.isAbnormalStatus()) {
            JOptionPane.showMessageDialog(null, "Error getting Error Service");
            return;
        }

        ComponentDescriptor errorServiceDesc = (ComponentDescriptor) response.getContent();
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.INVOKE_ERROR_DIALOG_SERVICE_CMD,
                errorServiceDesc, QIWConstants.ARRAYLIST_TYPE, list, true);
        messagingMgr.getMatchingResponseWait(msgID, 10000);
    }

    /** Invoke File Chooser Service
     * @param list Passed through to fileChooser from caller
     *
     */
    public void callFileChooser(QiFileChooserDescriptor desc) {
        if (desc == null) {
            return;
        }
        ComponentDescriptor fileChooser = null;
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD, true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, 10000);
        if (response == null) {
            logger.warning("Respone to get file chooser service returning null due to timed out");
            return;
        } else if (MsgUtils.isResponseAbnormal(response)) {
            logger.warning("Internal error occurring in getting file chooser service. Cause: " + MsgUtils.getMsgContent(response));
            return;
        } else {
            fileChooser = (ComponentDescriptor) MsgUtils.getMsgContent(response);
        }

        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_QI_FILE_CHOOSER_CMD, fileChooser, QiFileChooserDescriptor.class.getName(), desc);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_RESULT_FROM_QI_FILE_CHOOSER_CMD, fileChooser);
        return;
    }

    /** Choose a file using the FileChooser service.
     * @param list Input parameters to the file chooser
     */
    public void callFileChooser(ArrayList list) {
        // suspend message processing until file chooser responds
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD, true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, 10000);
        // restart processing
        ComponentDescriptor fileChooser = null;

        if (response == null) {
            logger.severe("Internal error occurring in getting file chooser service returning null, could be race condition");
            showInternalErrorDialog(Thread.currentThread().getStackTrace(), "Error loading file chooser service");
            return;
        } else if (MsgUtils.isResponseAbnormal(response)) {
            logger.warning("Internal error occurring in getting file chooser service. Cause: " + MsgUtils.getMsgContent(response));
            showInternalErrorDialog(Thread.currentThread().getStackTrace(), "Error loading file chooser service");
            return;
        } else {
            fileChooser = (ComponentDescriptor) MsgUtils.getMsgContent(response);
        }

        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_FILE_CHOOSER_CMD,
                fileChooser, QIWConstants.ARRAYLIST_TYPE, list);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD,
                fileChooser);
        return;
    }

    /** Choose a directory using the FileChooser service.
     * @param list Input parameters to the file chooser
     */
    /** Launch the Amplitude Extraction component:
     *  <ul>
     *  <li>Start up the plugin thread which will initialize the component.</li>
     *  </ul>
     * <p>
     * NOTE: Each thread's init() must finish before the next thread is
     * started. This is accomplished by monitoring the SYNC_LOCK object.
     */
    public static void main(String[] args) {
        AmpExtPlugin aePluginInstance = new AmpExtPlugin();
        //CID has to be passed in since no way to get it back out
        String cid = args[0];
        // use the CID as the name of the thread
        pluginThread = new Thread(aePluginInstance, cid);
        pluginThread.start();
        long threadId = pluginThread.getId();
        logger.info("Amplitude Extraction Thread-" + Long.toString(threadId) + " started");
        // When the plugin's init() is finished, it will release the lock

        // wait until the plugin's init() has finished
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();
        logger.info("Amplitude Extraction main finished");
    }

    public void get2DLinesHorizonsIntersetionSet(String project, AmplitudeExtraction cmp, String[] lines) {
        if (project == null || project.trim().length() == 0) {
            return;
        }
        if (landmarkAgent == null) {
            landmarkAgent = LandmarkServices.getInstance();
        }
        final AmplitudeExtraction comp = cmp;
        final String prj = project;
        final String[] fLineNames = lines;
        Runnable heavyRunnable = new Runnable() {

            public void run() {
                String title = "Gathering Lines/Horizons Information";
                String text = "Please wait while system is gathering lines/horizons information....";

                Icon icon = null; //new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));

                com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(comp, 100, true, 500, title, text, icon);
                //monitor.start("");
                ProgressUtil.start(monitor, "");
                final String[] out = landmarkAgent.getLandmarkProjectHandler().get2DHorizonListByLines(prj, fLineNames);

                if (ProgressUtil.isCancel(monitor)) {
                    return;
                }

                if (out != null) {
                    ProgressUtil.setCurrent(monitor, null, ProgressUtil.getTotal(monitor));
                    horizonIntersectionArray = out;
                }
            }
        };
        Thread t = new Thread(heavyRunnable);
        t.start();
        try {
            // the current thread will wait until the job done
            t.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    static class Worker extends Thread{
    	private String [] out;
    	private List<Object> params;
    	private LandmarkServices agent = LandmarkServices.getInstance();
    	public Worker(List<Object> params){
    		this.params = params;
    	}
    	public void run(){
    		out = agent.getLandmarkProjectHandler().get2DRelatedLinesByHorizonName((String)params.get(0), (String)params.get(1));
    	}
    	public String[] getOutPut(){
    		return out.clone();
    	}
    }
    
    public String[] get2DRelatedLinesByHorizonName(String project, String horizon){
    	List<Object> params = new ArrayList<Object>();
    	params.add(project);
    	params.add(horizon);
    	Worker worker = new Worker(params);
    	worker.start();
    	try{
    		worker.join();
    	}catch(InterruptedException e){}
    	return worker.getOutPut();
    }
    
    public void getUnrelated2DLinesByHorizons(String project, Component cmp, String[] lines, List<String> horizons) {
        if (project == null || project.trim().length() == 0) {
            return;
        }
        if (landmarkAgent == null) {
            landmarkAgent = LandmarkServices.getInstance();
        }
        final Component comp = cmp;
        final String prj = project;
        final String[] fLineNames = lines;
        final Object[] fHorizonNames = horizons.toArray();
        Runnable heavyRunnable = new Runnable() {
            public void run() {
                //String title = "Gathering Lines/Horizons Information";
                //String text = "Please wait while system is gathering lines/horizons information....";

                //Icon icon = null; //new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));

                //com.bhpb.qiworkbench.workbench.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(comp, 100, true, 500, title, text, icon);
                //monitor.start("");
                //ProgressUtil.start(monitor, "");
                
                String[] array = new String[fHorizonNames.length];
                for(int i = 0; fHorizonNames != null && i < fHorizonNames.length; i++)
                	array[i] = (String)fHorizonNames[i];
                unrelated2DLinesArray = landmarkAgent.getLandmarkProjectHandler().get2DNonRelatedLinesByHorizonArray(prj, fLineNames, array);
                //if (ProgressUtil.isCancel(monitor)) {
                //    return;
                //}
                //if (out != null) {
                //    unrelated2DLinesArray = out;
                //}
                //ProgressUtil.setCurrent(monitor, null, ProgressUtil.getTotal(monitor));
                //return;
            }
        };
        Thread t = new Thread(heavyRunnable);
        t.start();
        try {
            // the current thread will wait until the job done
            t.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    

    //the intersection of each line's horizon sets
    private String[] horizonIntersectionArray;
    //  the unrelated 2D lines set
    private String[] unrelated2DLinesArray;

    public String[] getHorizonIntersectionArray() {
        return horizonIntersectionArray.clone();
    }
    
    public String[] getUnrelated2DLineArray() {
        return unrelated2DLinesArray.clone();
    }    

    public void openLandmark2DHorizonDialog(String project, int projectType, String[] lineNames, AmplitudeExtraction cmp, javax.swing.JTextField target) {
        if (project == null || project.trim().length() == 0 || !(projectType >= 2 && projectType <= 3)) {
            return;
        }
        if (landmarkAgent == null) {
            landmarkAgent = LandmarkServices.getInstance();
        }
        final AmplitudeExtraction comp = cmp;
        final String prj = project;
        final String[] fLineNames = lineNames;
        final javax.swing.JTextField ftarget = target;
        Runnable heavyRunnable = new Runnable() {

            public void run() {
                String title = "Gathering Horizon Information";
                String text = "Please wait while system is gathering horizon information....";

                Icon icon = null; //new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));

                com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(comp, 100, true, 500, title, text, icon);
                //monitor.start("");
                ProgressUtil.start(monitor, "");
                final String[] out = landmarkAgent.getLandmarkProjectHandler().get2DHorizonListByLines(prj, fLineNames);

                if (ProgressUtil.isCancel(monitor)) {
                    return;
                }

                if (out != null) {
                    ProgressUtil.setCurrent(monitor, null, ProgressUtil.getTotal(monitor));
                    //monitor.setCurrent(null,monitor.getTotal());
                    Runnable updateAComponent = new Runnable() {

                        public void run() {
                            new HorizonSelectDialog(comp, ftarget, true, out).setVisible(true);
                        }
                    };
                    SwingUtilities.invokeLater(updateAComponent);
                }
            }
        };
        new Thread(heavyRunnable).start();
    }

    public void openLandmarkHorizonDialog(String project, int projectType, AmplitudeExtraction cmp, javax.swing.JTextField target) {
        if (project == null || project.trim().length() == 0 || !(projectType >= 2 && projectType <= 3)) {
            return;
        }
        if (landmarkAgent == null) {
            landmarkAgent = LandmarkServices.getInstance();
        }
        final AmplitudeExtraction comp = cmp;
        final String prj = project;
        final int prjType = projectType;
        final javax.swing.JTextField ftarget = target;
        Runnable heavyRunnable = new Runnable() {

            public void run() {
                String title = "Gathering Horizon Information";
                String text = "Please wait while system is gathering horizon information....";

                Icon icon = null; //new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));

                com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(comp, 100, true, 500, title, text, icon);
                //monitor.start("");
                ProgressUtil.start(monitor, "");
                final String[] out = landmarkAgent.getLandmarkProjectHandler().getHorizonList(prj, prjType);

                if (ProgressUtil.isCancel(monitor)) {
                    return;
                }

                if (out != null) {
                    ProgressUtil.setCurrent(monitor, null, ProgressUtil.getTotal(monitor));
                    //monitor.setCurrent(null,monitor.getTotal());
                    Runnable updateAComponent = new Runnable() {

                        public void run() {
                            new HorizonSelectDialog(comp, ftarget, true, out).setVisible(true);
                        }
                    };
                    SwingUtilities.invokeLater(updateAComponent);
                }
            }
        };
        new Thread(heavyRunnable).start();
    }
}