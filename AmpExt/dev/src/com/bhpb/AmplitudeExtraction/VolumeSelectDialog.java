package com.bhpb.AmplitudeExtraction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.logging.Logger;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.bhpb.services.landmark.LandmarkServices;
import com.bhpb.services.landmark.jni.LandmarkProjectHandler;

//import com.bhpb.qiworkbench.client.jni.LandmarkProjectHandler;

/*
 * ProjectSelectDialog.java
 *
 * Created on September 12, 2006, 5:13 PM
 */

/**
 *
 * @author  
 */
public class VolumeSelectDialog extends java.awt.Dialog {
	private static Logger logger = Logger.getLogger(VolumeSelectDialog.class
			.getName());
	private LandmarkServices agent;
	private String [] datasets = new String[0];
	//private LandmarkProjectHandler projectHandler;
	private AmplitudeExtraction parent;
	private int projectType = 3;
    /** Creates new form ProjectSelectDialog */
    public VolumeSelectDialog(LandmarkServices agent, AmplitudeExtraction parent, boolean modal) {
        super(JOptionPane.getFrameForComponent(parent), modal);
        this.agent = agent;
        this.parent = parent;
        this.setTitle("Select A Volume");
        int type = 3;
        if(agent != null)
        	type = agent.getLandmarkProjectType(parent.getLandmarkProjectText());
        projectType = type;
        String ext = "*.*";
        if(type == 2)
        	ext = "*01.2v2_glb";
        else if(type == 3)
        	ext = "*.bri";

        if(parent != null)
        	datasets = agent.getLandmarkVolumeList(parent.getLandmarkProjectText(), type, ext);
        initComponents();
        this.setLocationRelativeTo(parent);
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">
    private void initComponents() {
        jScrollPane1 = new javax.swing.JScrollPane();
        datasetList = new javax.swing.JList();
        datasetList.setSize(300, datasetList.getHeight());
        datasetListLabel = new javax.swing.JLabel();
        dataFormatTypeComboBox = new javax.swing.JComboBox();
        dataFormatTypeComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox) e.getSource();
				String  ext  = (String)cb.getSelectedItem();
				if(projectType == 2) 
					ext = "*01." + ext;
				else if(projectType == 3)
					ext = "*." + ext;
		        //datasets = projectHandler.getDatasetList(parent.getSelectedLandmarkProject(), projectType, ext);
		        datasets = agent.getLandmarkVolumeList(parent.getLandmarkProjectText(), projectType, ext);
				datasetList.setListData(datasets);
				
				okButton.setEnabled(false);
			}
		});

        formatTypeLabel = new javax.swing.JLabel();
        okButton = new javax.swing.JButton();
        okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String selectedVolume = (String)datasetList.getSelectedValue();
				if(selectedVolume.endsWith("01.bri")){
					int index = selectedVolume.indexOf("00001.");
					if(index != -1){
						selectedVolume = selectedVolume.substring(0, index) + ".bri";
					}
				}
				parent.setVolumeTextField(selectedVolume);
				parent.setLineTraceFieldEnabled(true);
				int ltArray [][] = agent.getLandmarkLineTraceInfo(parent.getLandmarkProjectText(), projectType, selectedVolume);
				parent.setLineTraceDefaultValue(ltArray);
				closeDialog(e);
			}
		});
        cancelButton = new javax.swing.JButton();
        
        setBackground(new java.awt.Color(255, 255, 204));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        datasetList.setBackground(new java.awt.Color(204, 255, 255));
        datasetList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        
        datasetList.setModel(new javax.swing.AbstractListModel() {
            //String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 5", "Item 5", "Item 5", "Item 5", "Item 5", "Item 5", "Item 5", "Item 5", "Item 5", "Item 5", "Item 5", "Item 5", "Item 5" };
            public int getSize() { return datasets.length; }
            public Object getElementAt(int i) { return datasets[i]; }
        });
        
        datasetList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                okButton.setEnabled(true);
            }
    	});
        datasetList.addKeyListener(new ListSearcher(datasetList,okButton));
        MouseListener mouseListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
            	if (e.getClickCount() == 2) {
            		okButton.doClick();
            	}
            }
        };
        datasetList.addMouseListener(mouseListener);
        jScrollPane1.setViewportView(datasetList);

        datasetListLabel.setBackground(new java.awt.Color(255, 255, 255));
        datasetListLabel.setText("Dataset List:");
        if(projectType ==3)
        	dataFormatTypeComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "bri", "cmp", "3dv" }));
        else if(projectType == 2)
        	dataFormatTypeComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "2v2_glb" }));

        formatTypeLabel.setText("Dataset Format:");

        okButton.setText("OK");
        okButton.setEnabled(false);
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	closeDialog(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(25, 25, 25)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(datasetListLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 99, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 300, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(layout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(formatTypeLabel))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                        .add(20, 20, 20)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(cancelButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 80, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(dataFormatTypeComboBox, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 80, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(okButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 80, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .add(39, 39, 39))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(datasetListLabel)
                    .add(formatTypeLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(dataFormatTypeComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 134, Short.MAX_VALUE)
                        .add(okButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(cancelButton))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE))
                .addContainerGap())
        );
        pack();
    }// </editor-fold>
    
    private void closeDialog(java.awt.event.ActionEvent evt) {
        setVisible(false);
        dispose();
    }
    
    /** Closes the dialog */
    private void closeDialog(java.awt.event.WindowEvent evt) {                             
        setVisible(false);
        dispose();
    }                            
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VolumeSelectDialog(null,null, true).setVisible(true);
            }
        });
    }
    
    
    // Variables declaration - do not modify
    private javax.swing.JButton cancelButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton okButton;
    private javax.swing.JList datasetList;
    private javax.swing.JLabel datasetListLabel;
    private javax.swing.JComboBox dataFormatTypeComboBox;
    private javax.swing.JLabel formatTypeLabel;
    // End of variables declaration
    
}
