/*
###########################################################################
# AmpExt - horizon-based Secant amplitude and area extraction
# This program module Copyright (C) 2006-2009, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.AmplitudeExtraction;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.bhpb.qiworkbench.compAPI.CommonUtil;
import com.bhpb.qiworkbench.compAPI.ComponentUtils;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

public class HelpAbout extends JDialog {
    private static Logger logger = Logger.getLogger(HelpAbout.class.getName());
    private ImageIcon _icon;
    private Component parent;
    private JTextArea textArea = new JTextArea(40,40);
    private JScrollPane scrollPane = new JScrollPane();
    private JEditorPane aboutTextEditorPane = new JEditorPane();
    private JButton licenseAbout;
    private Properties props;
    public HelpAbout(Component parent, Properties props) {
        //super(parent,title);
        this.props = props;
        this.parent = parent;
        _icon = new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
          public void run() {
              createAndShowGUI();
          }
        });
    }

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    public void createAndShowGUI(){
        //String viewerVersion = "AmpExt version 1.0";
        //JEditorPane aboutTextEditorPane = new JEditorPane();
        aboutTextEditorPane.setEditable(false);
        aboutTextEditorPane.setContentType("text/html");

        aboutTextEditorPane.setAutoscrolls(true);
        textArea.setEnabled(false);
        textArea.setFont(new java.awt.Font("Arial", 0, 14));
        textArea.setRows(20);

        scrollPane.setViewportView(aboutTextEditorPane);
        textArea.setLayout(new FlowLayout(FlowLayout.CENTER));
        //textArea.setBorder(BorderFactory.createTitledBorder(" License "));
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setEditable(false);
        aboutTextEditorPane.setText(getAboutHtmlText());
        //textArea.setText(getAboutText());
        textArea.setCaretPosition(0);
        //final JDialog dialog = new JDialog(JOptionPane.getRootFrame(), "About AmpExt");
        final JPanel panel = new JPanel(new BorderLayout());
        panel.setBackground(Color.WHITE);
        panel.add(new JLabel(_icon), BorderLayout.CENTER);
        //dialog.getContentPane().add(new JLabel(_icon), BorderLayout.WEST);
        getContentPane().add(panel, BorderLayout.WEST);
        getContentPane().add(scrollPane, BorderLayout.EAST);
        //dialog.getContentPane().add(aboutTextEditorPane, BorderLayout.EAST);
        licenseAbout = new JButton("License");
        licenseAbout.setActionCommand("viewLicense");
        licenseAbout.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                //dialog.dispose();
                //Component[] comps = dialog.getContentPane().getComponents();
                //for(int i = 0; i < comps.length; i++){
                //  System.out.println("name " + comps[i].toString());
                //}
                //dialog.getContentPane().add(panel, BorderLayout.WEST);
                //dialog.remove(aboutTextEditorPane);
                if(licenseAbout.getActionCommand().equals("viewLicense")){
                    String license = getLicenseHtmlText();
                    aboutTextEditorPane.setText(license);
                    aboutTextEditorPane.scrollToReference(license.substring(0,10));
                    licenseAbout = new JButton("About");
                    licenseAbout.setActionCommand("viewAbout");
                }else if(licenseAbout.getActionCommand().equals("viewAbout")){
                    aboutTextEditorPane.setText(getAboutHtmlText());
                    licenseAbout = new JButton("License");
                    licenseAbout.setActionCommand("viewLicense");
                }

                //dialog.getContentPane().invalidate();
                //new ViewLicense();
            }
        });
        JButton okButton = new JButton("Close");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                dispose();
            }
        });
        JPanel bpanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        bpanel.add(licenseAbout);
        bpanel.add(okButton);
        getContentPane().add(bpanel, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(parent);
        //int x = parent.getBounds().x + (parent.getBounds().width/2) - (dialog.getBounds().width/2);
        //int y = parent.getBounds().y + (parent.getBounds().height/2) - (dialog.getBounds().height/2);
        //dialog.setLocation(x,y);
        setVisible(true);
    }



    private String getAboutText(){
        String viewerVersion = "AmpExt - horizon-based Secant amplitude and area extraction";
        StringBuffer sb = new StringBuffer();
        sb.append(viewerVersion + " \n\n");

        String versionProp = "";
        String buildProp = "";

        if(props != null){
            versionProp = props.getProperty("project.version");
            buildProp = props.getProperty("project.build");
        }

        if(versionProp == null) {
             versionProp = "";
        }

        if(buildProp == null) {
            buildProp = "";
        }

        sb.append("Version: " + versionProp + "\n");
        sb.append("Build id: " + buildProp + "\n\n");

        sb.append("This program module Copyright (C) 2006-2009, ");
        sb.append("BHP Billiton Petroleum and is licensed under ");
        sb.append("the BSD License. ");
        sb.append("\n\n");
        sb.append("AmpExt comes with ABSOLUTELY NO WARRANTY. ");
        sb.append("\n\n");
        sb.append("Click on the License button for more details. ");
        return sb.toString();

    }

    private String getAboutHtmlText(){
        String viewerVersion = "AmpExt - horizon-based Secant amplitude and area extraction";
        StringBuffer sb = new StringBuffer();
        sb.append("<font face=\"Arial\" size=3><b>");
        sb.append(viewerVersion+"</b><br><br>");


        String versionProp = "";
        String buildProp = "";

        if(props != null){
            versionProp = props.getProperty("project.version");
            buildProp = props.getProperty("project.build");
        }

        if(versionProp == null) {
             versionProp = "";
        }

        if(buildProp == null) {
            buildProp = "";
        }

        sb.append("Version: " + versionProp + "<br>");
        sb.append("Build id: " + buildProp + "<br><br>");

        sb.append("This program module Copyright (C) 2006-2009,<br>");
        sb.append("BHP Billiton Petroleum and is licensed under<br>");
        sb.append("the BSD License.");
        sb.append("<br><br>");
        sb.append("AmpExt comes with ABSOLUTELY NO WARRANTY. ");
        sb.append("<br><br>");
        sb.append("Click on the <b>License</b> button for more details. ");
        sb.append("<br><br>");
        sb.append("<br><br>");
        sb.append("</font>");
        return sb.toString();

    }

    private String getLicenseHtmlText(){
        StringBuffer sb = new StringBuffer();
        sb.append("<font face=\"Arial\" size=3>");
        sb.append("<b>AmpExt</b> - horizon-based Secant amplitude and area extraction<br><br>");
        sb.append("This program module Copyright (C) 2006-2009, BHP Billiton Petroleum<br>");
        sb.append("and is licensed under the following BSD License.<br>");
        sb.append("<br>");
        sb.append("Redistribution and use in source and binary forms, with or <br>");
        sb.append("without modification, are permitted provided that the <br>");
        sb.append("following conditions are met:<br>");
        sb.append("<br>");
        sb.append("<ul>");
        sb.append("<li>Redistributions of source code must retain the <br>");
        sb.append("above copyright notice, this list of conditions and <br>");
        sb.append("the following disclaimer.<br>");
        sb.append("</li>");
        sb.append("<li>Redistributions in binary form must reproduce <br>");
        sb.append("the above copyright notice, this list of conditions <br>");
        sb.append("and the following disclaimer in the documentation <br>");
        sb.append("and/or other materials provided with the distribution.");
        sb.append("</li>");
        sb.append("<li>Neither the name of BHP Billiton Petroleum <br>");
        sb.append("nor the names of its contributors may be used to <br>");
        sb.append("endorse or promote products derived from this <br>");
        sb.append("software without specific prior written permission.");
        sb.append("</li>");
        sb.append("</ul>");
        sb.append("<br>");
        sb.append("THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS <br>");
        sb.append("AND CONTRIBUTORS \"AS IS\" AND ANY EXPRESS OR <br>");
        sb.append("IMPLIED WARRANTIES, INCLUDING, BUT NOTLIMITED TO, <br>");
        sb.append("THE IMPLIED WARRANTIES OF MERCHANTABILITY AND <br>");
        sb.append("FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. <br>");
        sb.append("IN NO EVENT SHALL THE COPYRIGHT OWNER OR <br>");
        sb.append("CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, <br>");
        sb.append("INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL <br>");
        sb.append("DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT <br>");
        sb.append("OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, <br>");
        sb.append("OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER <br>");
        sb.append("CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN <br>");
        sb.append("CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING  <br>");
        sb.append("NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT <br>");
        sb.append("OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF <br>");
        sb.append("THE POSSIBILITY OF SUCH DAMAGE.");
        sb.append("</font>");
        return sb.toString();

    }
    private String getLicense(){
        StringBuffer license = new StringBuffer();
        BufferedReader br = null;
        String str;
        try {
            InputStream io = this.getClass().getResourceAsStream("/license.txt");
            br = new BufferedReader(new InputStreamReader(io));
            while ((str = br.readLine()) != null)
                license.append(str + "\n");

            System.out.println("license" + license.toString());
        } catch (FileNotFoundException fnfe) {
            license.append(fnfe.getMessage());
        } catch (IOException ioe) {
            license.append(ioe.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception ignored) {
                    license.append(ignored.getMessage());
                }
            }
        }
        return license.toString();
    }
    private void readFileIntoTextArea(JTextArea license) {
        BufferedReader br = null;
        String str;
        try {
            InputStream io = this.getClass().getResourceAsStream("/license.txt");
            br = new BufferedReader(new InputStreamReader(io));
            while ((str = br.readLine()) != null)
                license.append(str + "\n");

            System.out.println("license" + license.getText());
        } catch (FileNotFoundException fnfe) {
            license.append(fnfe.getMessage());
        } catch (IOException ioe) {
            license.append(ioe.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception ignored) {
                    license.append(ignored.getMessage());
                }
            }
        }
    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        //JFrame frame = new JFrame();
        //frame.addActionListener(new WaveletDecompAbout(null));

    }

}
