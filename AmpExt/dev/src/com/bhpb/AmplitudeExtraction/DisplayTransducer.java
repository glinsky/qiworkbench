/*
###########################################################################
# AmpExt - horizon-based Secant amplitude and area extraction
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.AmplitudeExtraction;

import java.lang.NumberFormatException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ControlConstants;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Finite state machine (FSM) for displaying the results of AmpExt.
 */
public class DisplayTransducer {
    /** Display step */
    private int stepState = ControlConstants.SESSION_STEP;
    /** Step action */
    private int actionState = ControlConstants.START_SESSION;
    MessagingManager messagingMgr;
    // GUI component
    private AmplitudeExtraction gui;
    /** Component descriptor of the qiViewer being controlled */
    ComponentDescriptor qiViewerDesc;
    
    /** Indicator that session was terminated either due to an error in the transduced or the user cancelled displaying the results */
    boolean sessionTerminated = false;
	boolean ignoreResponse = false;

    String sessionID = "";
    //command action
    ArrayList actions = new ArrayList();
    //parameters for multi-action commands
    ArrayList<String> actionParams1 = new ArrayList<String>();
    ArrayList<String> actionParams2 = new ArrayList<String>();
    ArrayList<String> actionParams3 = new ArrayList<String>();
    ArrayList<String> actionParams4 = new ArrayList<String>();
    ArrayList<String> actionParams5 = new ArrayList<String>();
    ArrayList<String> actionParams6 = new ArrayList<String>();

    //Window characteristics: Window ID, size (width, height), position (x, y)
    String xsecWID1 = "", xsecWID2 = "", xsecWID3 = "", xsecWID4 = "";
    String mapWID1 = "", mapWID2 = "";
    int xsecWidth1 = 0, xsecHeight1 = 0, xsecWidth2 = 0, xsecHeight2 = 0,
        xsecWidth3 = 0, xsecHeight3 = 0, xsecWidth4 = 0, xsecHeight4 = 0;
    int mapWidth1 = 0, mapHeight1 = 0, mapWidth2 = 0, mapHeight2 = 0;
    int xsecX1 = 0, xsecY1 = 0, xsecX2 = 0, xsecY2 = 0, xsecX3 = 0,
        xsecY3 = 0, xsecX4 = 0, xsecY4 = 0;
    int mapX1 = 0, mapY1 = 0, mapX2 = 0, mapY2 = 0;
    int viewerWidth = 50, viewerHeight = 50;    //leave room around the edges
    //Note: layer IDs unique across all windows
    String layerID1 = "", layerID2 = "", layerID3 = "", layerID4 = "";
    String layerID5 = "", layerID6 = "";
    String layerPropsID1 = "", layerPropsID2 = "", layerPropsID3 = "";
    String layerPropsID4 = "", layerPropsID5 = "", layerPropsID6 = "";
	
	String annotatedLayer1 = "";
    
    public static final String DATASET_SUFFIX = ".dat";

    /**
     * Constructor. Initialize the finite state machine.
     * @param gui AmpExt GUI.
     * @param messagingMgr AmpExt Agent's messaging manager.
     * @param stepState The state (display step) in which to start.
     * @param actionState The action within a step in which to start.
     */
    public DisplayTransducer(AmplitudeExtraction gui, MessagingManager messagingMgr, int stepState, int actionState) {
        this.stepState = stepState;
        this.actionState = actionState;
        this.messagingMgr = messagingMgr;
        this.gui = gui;
    }

    /**
     * Process the response message from a CONTROL_QIVIEWER command.
     * The actions in the request are specified by the actionState
     * which is relative to the step (stepState) in the display process.
     * <p>
     * Step state 0, action 0 is start_control_session.
     * <br> Step state 0, action 1 is end_control_session.
     * @param resp The response message.
     */
    public void processResp(IQiWorkbenchMsg resp) {
System.out.println("AmpExt::processResp: response="+resp);
        //Check if the user terminated the control session
        if (resp.isAbnormalStatus()) {
            String errmsg = (String)resp.getContent();
            if (errmsg.contains("CANCELLED")) {
                endControlSession(errmsg);
            }
        }
        
        switch (stepState) {
            case ControlConstants.SESSION_STEP:
                switch (actionState) {
                    case ControlConstants.START_SESSION:
                        //Check if session terminated and reinitialized for 
                        //reuse. NOTE: This consumes the response from the
                        //request to end the session.
                        if (ignoreResponse) {
                            //prepare to start a new session
                            //NOTE: The response from a CONTROL_QIVIEWER_CMD
                            //triggers the start of a new session.
                            ignoreResponse = false;
							sessionTerminated = false;
                            break;
                        }
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot start a display session");
                        } else
                        //Can only display results if input is BHP-SU
                        if (!gui.isBhpsuInput() || !gui.isBhpsuOutput()) {
                            endControlSession("Cannot display results: Input not BHP-SU");
                        } else {
                            ArrayList outs = (ArrayList)resp.getContent();
                            sessionID = (String)outs.get(0);
                            qiViewerDesc = (ComponentDescriptor)outs.get(1);

                            stepState = AmpExtConstants.GET_LAYER_PROPS1;
							actionState = AmpExtConstants.INIT_PROGRESS_DIALOG;
							
							actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.INIT_PROGRESS_DIALOG_ACTION);
							actionParams1.add("title=Display AmpExt Results");
							actionParams1.add("windows=6");
							actionParams1.add("layers="+ ("4,4,1,1,1,1"));
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case ControlConstants.END_SESSION:
                        //Prepare for next control session (in case use the same transducer)
                        initState();
                        break;

                    default:
                }
                break;
                
            //XSec Window #1
            case AmpExtConstants.GET_LAYER_PROPS1:
                switch (actionState) {
					case AmpExtConstants.INIT_PROGRESS_DIALOG:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot initialize the progress dialog\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.GET_LAYER_PROPS;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getInputCubePath());
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
					
                    case AmpExtConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for XSection window #1.\n"+resp.getContent());
                        } else {
                            layerPropsID1 = (String)resp.getContent();

                            actionState = AmpExtConstants.GET_LAYER_PROP;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID1);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.ep");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.GET_LAYER_PROP:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get input cube's full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = AmpExtConstants.SET_LAYER_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID1);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.ep");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID1);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.cdp");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID1);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.ep");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);
							
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams4.clear();
							actionParams4.add("layerPropsID="+layerPropsID1);
							actions.add(actionParams4);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID1);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case AmpExtConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set input cube's parameters.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.SET_LAYER_PROPS2;

                            actions.clear(); actionParams1.clear();

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID1);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=RasterizingType.Wiggle_Trace");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID1);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=RasterizingType.Positive_Fill");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID1);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=RasterizingType.Variable_Density");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID1);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        } break;
                            
                    case AmpExtConstants.SET_LAYER_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set layer display properties.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.CREATE_WINDOW;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID1);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getInputCubePath());
							actionParams1.add("ltype="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        } break;
                        
                    case AmpExtConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot open XSection window #1.\n"+resp.getContent());
                        } else {
                            ArrayList ids = (ArrayList)resp.getContent();
                            xsecWID1 = (String)ids.get(0);
                            layerID1 = (String)ids.get(1);
                           
                            actionState = AmpExtConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=SynchronizeHorizontalScrolling");
                            actionParams1.add("value=off");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID1);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Synchronization properties.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.SET_ANNO_PROPS ;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams1.add("field=Horizontal.SelectedKeys");
                            actionParams1.add("value=cdp,ep");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID1);
                            actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams2.add("field=Horizontal.SynchronizationKey");
                            actionParams2.add("value=cdp");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID1);
                            actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Annotation properties.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.ADD_LAYER;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=p1");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case AmpExtConstants.ADD_LAYER:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add horizon layer p1\n\n"+resp.getContent()+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.ADD_LAYER2;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=p2");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.ADD_LAYER2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add horizon layer p2\n\n"+resp.getContent()+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.ADD_LAYER3;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=p3");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case AmpExtConstants.ADD_LAYER3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add horizon layer p3\n\n"+resp.getContent()+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.RESIZE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            //determine height and width
                            xsecHeight1 = 400; xsecWidth1 = 400;

                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("width="+xsecWidth1);
                            actionParams1.add("height="+xsecHeight1);
                            actions.add(actionParams1);
                            
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID1);
                            actionParams2.add("direction=All");
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot resize XSection Window #1.\n"+resp.getContent());
                        } else {
                            stepState = AmpExtConstants.GET_LAYER_PROPS2;
                            actionState = AmpExtConstants.GET_LAYER_PROPS;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getInputCubePath());
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

            //Xsec Window #2
            case AmpExtConstants.GET_LAYER_PROPS2:
                switch (actionState) {
                    case AmpExtConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for XSection window #2.\n"+resp.getContent());
                        } else {
                            layerPropsID2 = (String)resp.getContent();

                            actionState = AmpExtConstants.GET_LAYER_PROP;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID2);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdp");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case AmpExtConstants.GET_LAYER_PROP:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get input cube's full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = AmpExtConstants.SET_LAYER_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID2);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdp");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID2);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.ep");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID2);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.ep");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);
							
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams4.clear();
							actionParams4.add("layerPropsID="+layerPropsID2);
							actions.add(actionParams4);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID2);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case AmpExtConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set input cube's parameters.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.SET_LAYER_PROPS1;

                            actions.clear(); actionParams1.clear();

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID2);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=RasterizingType.Wiggle_Trace");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID2);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=RasterizingType.Positive_Fill");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID2);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=RasterizingType.Variable_Density");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID2);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        } break;
                            
                    case AmpExtConstants.SET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set layer display properties.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.CREATE_WINDOW;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID2);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getInputCubePath());
							actionParams1.add("ltype="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        } break;
                        
                    case AmpExtConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot open XSection window #2.\n"+resp.getContent());
                        } else {
                            ArrayList ids = (ArrayList)resp.getContent();
                            xsecWID2 = (String)ids.get(0);
                            layerID2 = (String)ids.get(1);
                            
                            actionState = AmpExtConstants.SET_SYNC_PROPS ;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=SynchronizeHorizontalScrolling");
                            actionParams1.add("value=off");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID2);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Synchronization properties.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.SET_ANNO_PROPS ;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams1.add("field=Horizontal.SelectedKeys");
                            actionParams1.add("value=cdp,ep");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID2);
                            actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams2.add("field=Horizontal.SynchronizationKey");
                            actionParams2.add("value=ep");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID2);
                            actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Annotation properties.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.ADD_LAYER;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=p1");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case AmpExtConstants.ADD_LAYER:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add horizon layer p1\n\n"+resp.getContent()+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.ADD_LAYER2;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=p2");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.ADD_LAYER2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add horizon layer p2\n\n"+resp.getContent()+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.ADD_LAYER3;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=p3");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case AmpExtConstants.ADD_LAYER3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add horizon layer p3\n\n"+resp.getContent()+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.RESIZE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            //determine height and width
                            xsecHeight2 = 400; xsecWidth2 = 400;
                            //deterimine positon of window next to window #1
                            xsecX2 = xsecWidth1+1; xsecY2 = 0;
                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("width="+xsecWidth2);
                            actionParams1.add("height="+xsecHeight2);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID2);
                            actionParams2.add("x="+xsecX2);
                            actionParams2.add("y="+xsecY2);
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID2);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);


                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot resize XSection Window #2.\n"+resp.getContent());
                        } else {
                            stepState = AmpExtConstants.GET_LAYER_PROPS5;
                            actionState = AmpExtConstants.GET_LAYER_PROPS;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            String outPath = gui.getOutputCubePath();
                            //append '_transpose_ep to name of filee
                            int idx = outPath.indexOf(DATASET_SUFFIX);
                            if (idx != -1) {
                                outPath = outPath.replace(DATASET_SUFFIX, "_transpose_ep"+DATASET_SUFFIX);
                            }
                            actionParams1.add("file="+outPath);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

            //Map Window #1
            case AmpExtConstants.GET_LAYER_PROPS5:
                switch (actionState) {
                    case AmpExtConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for Map window #1.\n"+resp.getContent());
                        } else {
                            layerPropsID5 = (String)resp.getContent();
                    
                            actionState = AmpExtConstants.SET_LAYER_PROPS;

                            actions.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.clear();
                            actionParams1.add("layerPropsID="+layerPropsID5);
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=HorizonSetting");
                            actionParams1.add("value=amp");
                            actions.add(actionParams1);
							
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams2.clear();
							actionParams2.add("layerPropsID="+layerPropsID5);
							actions.add(actionParams2);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID5);
                            actionParams3.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case AmpExtConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set dataset properties for Map window #1.\n"+resp.getContent());
                        } else {
                            actionState =  AmpExtConstants.SET_LAYER_PROPS1;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams1.clear();
                            actionParams1.add("layerPropsID="+layerPropsID5);
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams1.add("colormap=rainbow");
                            actions.add(actionParams1);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID5);
                            actionParams2.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                            
                    case AmpExtConstants.SET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set layer's properties of Map window #1.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.CREATE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID5);
                            actionParams1.add("type=Map");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputMapPath());
							actionParams1.add("ltype="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=amp");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                    
                    case AmpExtConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot open Map window #1.\n"+resp.getContent());
                        } else {
                            ArrayList ids = (ArrayList)resp.getContent();
                            mapWID1 = (String)ids.get(0);
                            layerID5 = (String)ids.get(1);

                            actionState = AmpExtConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+mapWID1);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=ListenToHorizontalScale");
                            actionParams1.add("value=off");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+mapWID1);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToVerticalScale");
                            actionParams2.add("value=off");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+mapWID1);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToScrollPosition");
                            actionParams3.add("value=off");
                            actions.add(actionParams3);

                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+mapWID1);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=ListenToCursorPosition");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);

                            actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+mapWID1);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams5.add("field=Broadcast");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams6.clear();
                            actionParams6.add("winID="+mapWID1);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams6);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Synchronization properties.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.SET_SCALE_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+mapWID1);
                            actionParams1.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actionParams1.add("field=LockAspectRatio");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+mapWID1);
                            actionParams2.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_SCALE_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Scale properties on Map window #1.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.RESIZE_WINDOW;

                            //determine height and widt1
                            mapHeight1 = 400; mapWidth1 = 600;
                            //deterimine positon of window next to window #2
                            mapX1 = xsecX2+xsecWidth2+1; mapY1 = 0;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+mapWID1);
                            //fixed apparent copy-paste bug, the following 2 lines used 'mapWidth2' and 'mapHeight2',
                            //which are 0 at this point
                            actionParams1.add("width="+mapWidth1);
                            actionParams1.add("height="+mapHeight1);
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.add("winID="+mapWID1);
                            actionParams2.add("x="+mapX1);
                            actionParams2.add("y="+mapY1);
                            actions.add(actionParams2);
                            
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+mapWID1);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot resize Map Window #1.\n"+resp.getContent());
                        } else {
                            stepState = AmpExtConstants.GET_LAYER_PROPS6;
                            actionState = AmpExtConstants.GET_LAYER_PROPS;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            String outPath = gui.getOutputCubePath();
                            //append '_transpose_ep to name of filee
                            int idx = outPath.indexOf(DATASET_SUFFIX);
                            if (idx != -1) {
                                outPath = outPath.replace(DATASET_SUFFIX, "_transpose_ep"+DATASET_SUFFIX);
                            }
                            actionParams1.add("file="+outPath);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

            //Map Window #2
            case AmpExtConstants.GET_LAYER_PROPS6:
                switch (actionState) {
                    case AmpExtConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for Map window #2.\n"+resp.getContent());
                        } else {
                            layerPropsID6 = (String)resp.getContent();
                    
                            actionState = AmpExtConstants.SET_LAYER_PROPS;

                            actions.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.clear();
                            actionParams1.add("layerPropsID="+layerPropsID6);
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=HorizonSetting");
                            actionParams1.add("value=p3");
                            actions.add(actionParams1);
							
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams2.clear();
							actionParams2.add("layerPropsID="+layerPropsID6);
							actions.add(actionParams2);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID6);
                            actionParams3.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case AmpExtConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set dataset properties for Map window #2.\n"+resp.getContent());
                        } else {
                            actionState =  AmpExtConstants.SET_LAYER_PROPS1;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams1.clear();
                            actionParams1.add("layerPropsID="+layerPropsID6);
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams1.add("colormap=grey_scale");
                            actions.add(actionParams1);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID6);
                            actionParams2.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                            
                    case AmpExtConstants.SET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set layer's properties of Map window #2.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.CREATE_WINDOW;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID6);
                            actionParams1.add("type=Map");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputMapPath());
							actionParams1.add("ltype="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=p3");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                    
                    case AmpExtConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot open Map window #2.\n"+resp.getContent());
                        } else {
                            ArrayList ids = (ArrayList)resp.getContent();
                            mapWID2 = (String)ids.get(0);
                            layerID6 = (String)ids.get(1);

                            actionState = AmpExtConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+mapWID2);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=ListenToHorizontalScale");
                            actionParams1.add("value=off");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+mapWID2);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToVerticalScale");
                            actionParams2.add("value=off");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+mapWID2);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToScrollPosition");
                            actionParams3.add("value=off");
                            actions.add(actionParams3);

                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+mapWID2);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=ListenToCursorPosition");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);

                            actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+mapWID2);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams5.add("field=Broadcast");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams6.clear();
                            actionParams6.add("winID="+mapWID2);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams6);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Synchronization properties for Map window #2.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.SET_SCALE_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+mapWID2);
                            actionParams1.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actionParams1.add("field=LockAspectRatio");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+mapWID2);
                            actionParams2.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_SCALE_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Scale properties for Map window #2.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.RESIZE_WINDOW;

                            //determine height and widt1
                            mapHeight2 = 400; mapWidth2 = 600;
                            //deterimine positon of window under map window #1
                            mapX2 = xsecX2+xsecWidth2+1; mapY2 = mapY1+mapHeight1+1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+mapWID2);
                            actionParams1.add("width="+mapWidth2);
                            actionParams1.add("height="+mapHeight2);
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.add("winID="+mapWID2);
                            actionParams2.add("x="+mapX2);
                            actionParams2.add("y="+mapY2);
                            actions.add(actionParams2);
                            
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+mapWID2);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot resize Map window #2.\n"+resp.getContent());
                        } else {
                            stepState = AmpExtConstants.GET_LAYER_PROPS3;
                            actionState = AmpExtConstants.GET_LAYER_PROPS;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

            //XSec Window #3
            case AmpExtConstants.GET_LAYER_PROPS3:
                switch (actionState) {
                    case AmpExtConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for XSection window #3.\n"+resp.getContent());
                        } else {
                            layerPropsID3 = (String)resp.getContent();

                            actionState = AmpExtConstants.GET_LAYER_PROP;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID3);
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.ep");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case AmpExtConstants.GET_LAYER_PROP:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get output cube's full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = AmpExtConstants.SET_LAYER_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID3);
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.ep");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID3);
                            actionParams2.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.cdp");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID3);
                            actionParams3.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.ep");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID3);
                            actionParams4.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams4.add("field=HorizonSetting");
                            actionParams4.add("value=amp");
                            actions.add(actionParams4);
							
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams5.clear();
							actionParams5.add("layerPropsID="+layerPropsID3);
							actions.add(actionParams5);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID3);
                            actionParams6.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams6);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case AmpExtConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set output cube's parameters.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.CREATE_WINDOW;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID3);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
							actionParams1.add("ltype="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=amp");
                            actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        } break;
                        
                    case AmpExtConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot open XSection window #3.\n"+resp.getContent());
                        } else {
                            ArrayList ids = (ArrayList)resp.getContent();
                            xsecWID3 = (String)ids.get(0);
                            layerID3 = annotatedLayer1 = (String)ids.get(1);
                            
                            actionState = AmpExtConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=ListenToHorizontalScale");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID3);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToVerticalScale");
                            actionParams2.add("value=off");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID3);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToScrollPosition");
                            actionParams3.add("value=off");
                            actions.add(actionParams3);

                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID3);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=ListenToCursorPosition");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);

                            actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+xsecWID3);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams5.add("field=Broadcast");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams6.clear();
                            actionParams6.add("winID="+xsecWID3);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams6);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                   case AmpExtConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Synchronization properties for XSection window #3.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.SET_ANNO_PROPS ;

                            //Must set the annotation layer first
                            actions.clear(); actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID3);
                            actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams3.add("field=AnnotatedLayer");
                            actionParams3.add("value="+annotatedLayer1);
                            actions.add(actionParams3);
                            
                            //Clear the list of horizontal selected keys
                            actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+xsecWID3);
                            actionParams5.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams5.add("field=Horizontal.SelectedKeys");
                            actionParams5.add("value=");
                            actions.add(actionParams5);
                            
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams1.add("field=Horizontal.SelectedKeys");
                            actionParams1.add("value=cdp,ep");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID3);
                            actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams2.add("field=Horizontal.SynchronizationKey");
                            actionParams2.add("value=cdp");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("winID="+xsecWID3);
                            actionParams4.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Annotation properties.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.RESIZE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            //determine height and width
                            xsecHeight3 = 200; xsecWidth3 = 400;
                            //deterimine positon of window under XSection window #1
                            xsecX3 = xsecX1; xsecY3 = xsecY1 + xsecHeight1 + 1;

                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("width="+xsecWidth3);
                            actionParams1.add("height="+xsecHeight3);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID3);
                            actionParams2.add("x="+xsecX3);
                            actionParams2.add("y="+xsecY3);
                            actions.add(actionParams2);
                            
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID3);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot resize XSection Window #3.\n"+resp.getContent());
                        } else {
                            stepState = AmpExtConstants.GET_LAYER_PROPS4;
                            actionState = AmpExtConstants.GET_LAYER_PROPS;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

            //XSec Window #4
            case AmpExtConstants.GET_LAYER_PROPS4:
                switch (actionState) {
                    case AmpExtConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for XSection window #4.\n"+resp.getContent());
                        } else {
                            layerPropsID4 = (String)resp.getContent();

                            actionState = AmpExtConstants.GET_LAYER_PROP;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID4);
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdp");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case AmpExtConstants.GET_LAYER_PROP:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get output cube's full range.\n"+resp.getContent());
                        } else {
                            String fullRange = (String)resp.getContent();
                            Double chosenRange = getMidpoint(fullRange);

                            actionState = AmpExtConstants.SET_LAYER_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID4);
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdp");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID4);
                            actionParams2.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.ep");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID4);
                            actionParams3.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.cdp");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID4);
                            actionParams4.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams4.add("field=HorizonSetting");
                            actionParams4.add("value=amp");
                            actions.add(actionParams4);
							
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams5.clear();
							actionParams5.add("layerPropsID="+layerPropsID4);
							actions.add(actionParams5);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID4);
                            actionParams6.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams6);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case AmpExtConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set output cube's parameters.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.CREATE_WINDOW;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID4);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
							actionParams1.add("ltype="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=amp");
                            actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        } break;
                        
                    case AmpExtConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot open XSection window #3.\n"+resp.getContent());
                        } else {
                            ArrayList ids = (ArrayList)resp.getContent();
                            xsecWID4 = (String)ids.get(0);
                            layerID4 = annotatedLayer1 = (String)ids.get(1);
                            
                            actionState = AmpExtConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID4);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=ListenToHorizontalScale");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID4);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToVerticalScale");
                            actionParams2.add("value=off");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID4);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToScrollPosition");
                            actionParams3.add("value=off");
                            actions.add(actionParams3);

                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID4);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=ListenToCursorPosition");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);

                            actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+xsecWID4);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams5.add("field=Broadcast");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams6.clear();
                            actionParams6.add("winID="+xsecWID4);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams6);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Synchronization properties for XSection window #4.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.SET_ANNO_PROPS ;

                            //Must set the annotation layer first
                            actions.clear(); actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID4);
                            actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams3.add("field=AnnotatedLayer");
                            actionParams3.add("value="+annotatedLayer1);
                            actions.add(actionParams3);
                            
                            //Clear the list of horizontal selected keys
                            actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+xsecWID4);
                            actionParams5.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams5.add("field=Horizontal.SelectedKeys");
                            actionParams5.add("value=");
                            actions.add(actionParams5);
                            
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID4);
                            actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams1.add("field=Horizontal.SelectedKeys");
                            actionParams1.add("value=cdp,ep");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID4);
                            actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams2.add("field=Horizontal.SynchronizationKey");
                            actionParams2.add("value=ep");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("winID="+xsecWID4);
                            actionParams4.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Annotation properties for XSection window #4.\n"+resp.getContent());
                        } else {
                            actionState = AmpExtConstants.RESIZE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            //determine height and width
                            xsecHeight4 = 200; xsecWidth4 = 400;
                            //deterimine positon of window under XSection window #2
                            xsecX4 = xsecX2; xsecY4 = xsecY2 + xsecHeight2 + 1;

                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+xsecWID4);
                            actionParams1.add("width="+xsecWidth4);
                            actionParams1.add("height="+xsecHeight4);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID4);
                            actionParams2.add("x="+xsecX4);
                            actionParams2.add("y="+xsecY4);
                            actions.add(actionParams2);
                            
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID4);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.RESIZE_VIEWER_ACTION);
                            actionParams4.clear();

                            int w1 = xsecWidth1 + xsecWidth2 + mapWidth1;
                            int w2 = xsecWidth3 + xsecWidth4 + mapWidth2;
                            viewerWidth += w1>w2 ? w1 : w2;

                            int h1 = xsecY3 + xsecHeight3;
                            int h2 = xsecY4 + xsecHeight4;
                            int h3 = mapY2 + mapHeight2;
                            int maxh = h1>h2 ? h1 : h2;
                            viewerHeight += maxh>h3 ? maxh : h3;

                            actionParams4.add("width="+viewerWidth);
                            actionParams4.add("height="+viewerHeight);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot resize XSection Window #4.\n"+resp.getContent());
                        } else {
							actionState = AmpExtConstants.END_PROGRESS_DIALOG;
							
							actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.END_PROGRESS_DIALOG_ACTION);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case AmpExtConstants.END_PROGRESS_DIALOG:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot end process dialog\n"+resp.getContent());
                        } else {
                            stepState = ControlConstants.SESSION_STEP;
                            actionState = ControlConstants.END_SESSION;

                            actions.clear(); actionParams1.clear();
                            //End the control session
                            actions.add("action="+ControlConstants.END_CONTROL_SESSION_ACTION);
                            actionParams1.add("sessionID="+sessionID);
                            actions.add(actionParams1);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End the control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

            default:
                //Internal error: No legal step transition state
                //End the control session
                endControlSession("Internal Error: No legal step transition for state "+actionState);
        }
    }
	
	/**
	 * End the control session because of an error. Close the progress dialog.
	 * @param errorMsg The reason why the control session is being terminated.
	 */
	private void endControlSession(String errorMsg) {
		sessionTerminated = true;
		
		//Display reason ending the control session
		JOptionPane.showMessageDialog(gui, errorMsg+"\nCannot continue displaying the results.", "Display Error", JOptionPane.ERROR_MESSAGE);
		
		//End the control session
        stepState = ControlConstants.SESSION_STEP;
        actionState = ControlConstants.END_SESSION;
							
		actions.clear(); actionParams1.clear();
		actions.add("action="+ControlConstants.END_PROGRESS_DIALOG_ACTION);
		actions.add(actionParams1);
		actions.add("action="+ControlConstants.END_CONTROL_SESSION_ACTION);
		actionParams1.add("sessionID="+sessionID);
		actions.add(actionParams1);
		
		messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
	}
    
    /**
     * Initialize the transducer's state variables. Invoked when displaying
     * results is ended so the transducer can be called again.
     */
    private void initState() {
        xsecWID1 = ""; xsecWID2 = ""; xsecWID3 = ""; xsecWID4 = "";
        mapWID1 = ""; mapWID2 = "";
        xsecWidth1 = 0; xsecHeight1 = 0; xsecWidth2 = 0; xsecHeight2 = 0;
        xsecWidth3 = 0; xsecHeight3 = 0; xsecWidth4 = 0; xsecHeight4 = 0;
        mapWidth1 = 0; mapHeight1 = 0; mapWidth2 = 0; mapHeight2 = 0;
        xsecX1 = 0; xsecY1 = 0; xsecX2 = 0; xsecY2 = 0; xsecX3 = 0;
        xsecY3 = 0; xsecX4 = 0; xsecY4 = 0;
        mapX1 = 0; mapY1 = 0; mapX2 = 0; mapY2 = 0;
        viewerWidth = 50; viewerHeight = 50;
        layerID1 = ""; layerID2 = ""; layerID3 = ""; layerID4 = "";
        layerID5 = ""; layerID6 = "";
        layerPropsID1 = ""; layerPropsID2 = ""; layerPropsID3 = "";
        layerPropsID4 = ""; layerPropsID5 = ""; layerPropsID6 = "";
        annotatedLayer1 = "";
        
        stepState = ControlConstants.SESSION_STEP;
        actionState = ControlConstants.START_SESSION;
        
		//Check is forced control session to terminate (due to an error or the
		//user cancelled). If so, the response needs to be ignored.
        ignoreResponse = sessionTerminated ? true : false;
     }
	 
	/**
	 * Get the midpoint of a key's full range. The full range is a single
	 * value when input is Landmark 2D, i.e., the ep for a single line is a
	 * single value.
	 * @param fullRange Key's full range
	 * @return Midpoint of the full range.
	 */
	Double getMidpoint(String fullRange) {
		double midpoint = 0, min = 0, max = 0, incr = 1;
		
		//Parse the full range
		try {
			//Check if full range a single value
			if (fullRange.indexOf("-") == -1) {
				return Double.parseDouble(fullRange);
			}
				
			//full range: min-max[incr]
			String[] fr1 = fullRange.split("-");
			String[] fr2 = fr1[1].split("\\[");
			String[] fr3 = fr2[1].split("\\]");

			min = Double.parseDouble(fr1[0]);
			max = Double.parseDouble(fr2[0]);
			incr = Double.parseDouble(fr3[0]);
			
			midpoint = (max - min)/2;
		} catch (NumberFormatException nfe) {
		}
		
		//Adjust midpoint by taking incr into account
		return min + (midpoint - (midpoint % incr));
	 }
}
