/*
###########################################################################
# AmpExt - horizon-based Secant amplitude and area extraction
# This program module Copyright (C) 2006-2007, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.AmplitudeExtraction;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Point;
import java.awt.dnd.DnDConstants;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.ButtonGroup;
import javax.swing.DefaultCellEditor;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.client.util.GenericFileFilter;
import com.bhpb.qiworkbench.client.util.QiFileChooserDescriptor;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ComponentStateUtils;
import com.bhpb.qiworkbench.compAPI.ComponentUtils;
import com.bhpb.qiworkbench.compAPI.ControlConstants;
import com.bhpb.qiworkbench.compAPI.JobManager;
import com.bhpb.qiworkbench.compAPI.JobMonitor;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.compAPI.XmlUtils;
import com.bhpb.services.landmark.LandmarkServices;

/**
 * The GUI for the Amplitude Extraction qiComponent. It is not a separate component so
 * it has no message queue. Instead it use its plugin agent for handling message passing.
 * It is not necessary to start the GUI as a thread because by definition a Swing GUI is a separate thread.
 *
 * @author Bob Miller
 * @author Gil Hansen
 */
public class AmplitudeExtraction extends JInternalFrame {
    private static Logger logger = Logger.getLogger(AmplitudeExtraction.class.getName());

    /** Index of the Status tab */
    private static final int STATUS_TAB_INDEX = 1;
    public final String TN_SEISMIC_DATA_INPUT = "Seismic Data Input";
    public final String TN_SEISMIC_DATA_PROCESS = "Seismic Data Process";
    public final String TN_OUTPUTS = "Outputs";
    public final String TN_HORIZON_DATA_INPUT = "Horizon Data Input";
    public final String TN_JOB_STATUS = "Job Status";
    public final String TABLE_HEADING_BIAS = "Bias (ms)";
    public final String TABLE_HEADING_SEARCH_ABOVE = "Search Above (#S)";
    public final String TABLE_HEADING_SEARCH_BELOW = "Search Below (#S)";

    private AmpExtPlugin agent;
    private LandmarkServices landmarkAgent;
    //private AmpExtFileManager fileManager;
    // project from WorkbenchManager less path
    String project = "";
    QiProjectDescriptor qiProjectDesc;
    // project path
    String fileSystem = "";
    /** File separator for server OS. */
    String filesep = "/";

    HelpBroker AmplitudeExtractionHB;

    ArrayList<ArrayList<String>> headerLimits;

    Component gui;

    //Internal state that must be saved/restored
    //  Full path of the input cube
    String inputCubeDataset = "";

    //Horizon parameters
    String topHorizonBias = "";
    String topHorizonAbove = "";
    String topHorizonBelow = "";
    String bottomHorizonBias = "";
    String bottomHorizonAbove = "";
    String bottomHorizonBelow = "";
    String horizonBias = "";
    String horizonAbove = "";
    String horizonBelow = "";

    String landmark3DTopHorizonBias = "";
    String landmark3DTopHorizonAbove = "";
    String landmark3DTopHorizonBelow = "";
    String landmark3DBottomHorizonBias = "";
    String landmark3DBottomHorizonAbove = "";
    String landmark3DBottomHorizonBelow = "";
    String landmark3DHorizonBias = "";
    String landmark3DHorizonAbove = "";
    String landmark3DHorizonBelow = "";
    String landmark2DTopHorizonBias = "";
    String landmark2DTopHorizonAbove = "";
    String landmark2DTopHorizonBelow = "";
    String landmark2DBottomHorizonBias = "";
    String landmark2DBottomHorizonAbove = "";
    String landmark2DBottomHorizonBelow = "";
    String landmark2DHorizonBias = "";
    String landmark2DHorizonAbove = "";
    String landmark2DHorizonBelow = "";


    //Horizon parameters (row, col) in table
    public static final int HORIZON_BIAS_ROW = 0;
    public static final int HORIZON_BIAS_COL = 0;
    public static final int HORIZON_ABOVE_ROW = 0;
    public static final int HORIZON_ABOVE_COL = 1;
    public static final int HORIZON_BELOW_ROW = 0;
    public static final int HORIZON_BELOW_COL = 2;
    public static final int TOP_HORIZON_BIAS_ROW = 0;
    public static final int TOP_HORIZON_BIAS_COL = 1;
    public static final int TOP_HORIZON_ABOVE_ROW = 0;
    public static final int TOP_HORIZON_ABOVE_COL = 2;
    public static final int TOP_HORIZON_BELOW_ROW = 0;
    public static final int TOP_HORIZON_BELOW_COL = 3;
    public static final int BOTTOM_HORIZON_BIAS_ROW = 1;
    public static final int BOTTOM_HORIZON_BIAS_COL = 1;
    public static final int BOTTOM_HORIZON_ABOVE_ROW = 1;
    public static final int BOTTOM_HORIZON_ABOVE_COL = 2;
    public static final int BOTTOM_HORIZON_BELOW_ROW = 1;
    public static final int BOTTOM_HORIZON_BELOW_COL = 3;

    //Validation error sources for horizon parameters
    public static final String HORIZON_BIAS_SOURCE = "Horizon Bias";
    public static final String HORIZON_ABOVE_SOURCE = "Horizon Above";
    public static final String HORIZON_BELOW_SOURCE = "Horizon Beloe";
    public static final String TOP_HORIZON_BIAS_SOURCE = "Top Horizon Bias";
    public static final String TOP_HORIZON_ABOVE_SOURCE = "Top Horizon Above";
    public static final String TOP_HORIZON_BELOW_SOURCE = "Top Horizon Below";
    public static final String BOTTOM_HORIZON_BIAS_SOURCE = "Bottom Horizon Bias";
    public static final String BOTTOM_HORIZON_ABOVE_SOURCE = "Bottom Horizon Above";
    public static final String BOTTOM_HORIZON_BELOW_SOURCE = "Bottom Horizon Below";
    //Validation error causes for horizon parameters
    public static final String MISSING_VALUES_ERROR = "Some horizon parameters not specified";
    public static final String NOT_AN_INTEGER_ERROR = "not an integer";
    public static final String IS_NEGATIVE_ERROR = "is negative";
    //Validation error fixes for horizon parameters
    public static final String ENTER_VALUES_FIX = "Enter value(s).";
    public static final String REENTER_VALUE_FIX = "Change entry.";

    //Validation error sources for ranges
    public static final String EP_SOURCE = "ep";
    public static final String CDP_SOURCE = "cdp";
    public static final String EP_FROM_SOURCE = "ep From";
    public static final String EP_TO_SOURCE = "ep To";
    public static final String EP_BY_SOURCE = "ep By";
    public static final String CDP_FROM_SOURCE = "cdp From";
    public static final String CDP_TO_SOURCE = "cdp To";
    public static final String CDP_BY_SOURCE = "cdp By";
    //Validation error causes for ranges
    public static final String MISSING_RANGES_ERROR = "Some ranges not specified";
    public static final String NOT_POSITIVE_ERROR = "<= 0";
    public static final String FROM_GREATER_TO_ERROR = "From > To";
    public static final String BY_NOT_MULTIPLE_ERROR = "not a multiple of default By";
    //Validation error fixes for ranges

    //Horizon modes
    public static final int EDGE_MODE = 0;
    public static final int POINT_MODE = 1;

    //which horizon mode currently in; either edge or point
    int horizonMode = EDGE_MODE;

    TableModel edgeModeHorizonParamsTableModel = null;
    TableModel pointModeHorizonParamsTableModel = null;
    TableModel landmark3DEdgeModeHorizonParamsTableModel = null;
    TableModel landmark2DEdgeModeHorizonParamsTableModel = null;
    TableModel landmark3DPointModeHorizonParamsTableModel = null;
    TableModel landmark2DPointModeHorizonParamsTableModel = null;

    //State of horizon parameter settings
    String edgeModeHorizonSettings[][] = new String[2][3];
    String landmark3DEdgeModeHorizonSettings[][] = new String[2][3];
    String landmark2DEdgeModeHorizonSettings[][] = new String[2][3];
    String pointModeHorizonSettings[][] = new String[1][3];
    String landmark3DPointModeHorizonSettings[][] = new String[1][3];
    String landmark2DPointModeHorizonSettings[][] = new String[1][3];

    //Default range By values
    int epIncrDefault = 1;
    int cdpIncrDefault = 1;

    private int selectedProcessLevelIndex = 0;
    private Vector selectedLines = new Vector();
    private Map<String,LineInfo> selectedLineMap = new HashMap<String,LineInfo>();
    private String selectedLineName;
    /** Job monitor for the generated script executing as a job */
    JobMonitor jobMonitor = null;
    /** Job manager for the generated script executing as a job */
    JobManager jobManager = null;

    /** Constructor
     * @param agent AmpExtPlugin agent
     */
    public AmplitudeExtraction(AmpExtPlugin agent) {
        this(agent, QiProjectDescUtils.getQiProjectName(agent.getQiProjectDescriptor()), QiProjectDescUtils.getQiSpace(agent.getQiProjectDescriptor()));
    }
    /** Constructor
     * @param agent AmpExtPlugin agent
     */
    public AmplitudeExtraction() {
        startGUI();
    }
    /** Constructor
     * @param agent AmpExtPlugin agent
     * @param project Directory of qiProject releative to the qiSpace
     * @param fileSystem Absolute path of qiSpace
     */
    public AmplitudeExtraction(AmpExtPlugin agent, String project, String fileSystem) {
        this.project = project;
        this.fileSystem = fileSystem;
        this.agent = agent;
        filesep = agent.getMessagingMgr().getServerOSFileSeparator();

//        qiProjectDesc = agent.getQiProjectDescriptor();
        // Get my Component for use in locating dialogs
        gui = (Component)this;
        //fileManager = new AmpExtFileManager(agent);

        //make frame iconable and maximizable.
        this.setIconifiable(true);
        this.setMaximizable(true);
        this.setResizable(true);

        //Initialize horizon parameter settings
        for (int i=0; i<2; i++)
            for (int j=0; j<3; j++) {
                edgeModeHorizonSettings[i][j] = "";
                if (i<1) pointModeHorizonSettings[i][j] = "";
            }

        for (int i=0; i<2; i++)
            for (int j=0; j<3; j++) {
                landmark3DEdgeModeHorizonSettings[i][j] = "";
                landmark2DEdgeModeHorizonSettings[i][j] = "";
                if (i<1){
                    landmark3DPointModeHorizonSettings[i][j] = "";
                    landmark2DPointModeHorizonSettings[i][j] = "";
                }
            }

        startGUI();

//        entireCubeRangeButton.setEnabled(false);
        pickRangeFromViewerButton.setEnabled(false);

        // Disable ability to check status and cancel a job until Run Script selected.
        checkStatusButton.setEnabled(false);
        statusMenuItem.setEnabled(false);
        cancelRunButton.setEnabled(false);
        cancelRunMenuItem.setEnabled(false);
        // Disable ability to display the results until script execution finishes.
        displayResultsButton.setEnabled(false);
        displayResultsMenuItem.setEnabled(false);
    }

    private void resetShotpointInfo(){
        selectedLineMap = new HashMap<String,LineInfo>();
        selectedLineName = "";
        shotpointTextField.setText("");
        shotpointToTextField.setText("");
        shotpointByTextField.setText("");
    }

    private boolean validateLandmarkInput(){
        final Component comp = this;
        if(!landmarkInRadioButton.isSelected())
            return true;

        int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
        if(landmarkAgent == null)
            landmarkAgent = LandmarkServices.getInstance();

        if(landmark3DRadioButton.isSelected()){
            String text = landmarkInProjectTextField.getText();
            if (text == null || text.trim().length() == 0) {
                JOptionPane.showMessageDialog(comp, "The " + landmarkProjectLabel.getText()
                        + " field is empty.", "Missing data entry", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                landmarkInProjectTextField.requestFocus();
                return false;
            }

            if (!landmarkAgent.isValidLandmarkProject(text)) {
                JOptionPane.showMessageDialog(comp, "Can not find project name " + landmarkInProjectTextField.getText()
                        + ". Check to see if it is valid Landmark project", "Project not found",    JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                landmarkInProjectTextField.requestFocus();
                return false;
            }


            text = volumeTextField.getText();
            if (text == null || text.trim().length() == 0) {
                JOptionPane.showMessageDialog(comp, "The " + volumeLabel.getText()
                        + " field is empty.", "Missing data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                volumeTextField.requestFocus();
                return false;
            }

            if(!validateLineTraceTextField(linesTextField,linesToTextField,linesByTextField,lineTraceArray[0]))
                return false;
            if(!validateLineTraceTextField(tracesTextField,tracesToTextField,tracesByTextField,lineTraceArray[1]))
                return false;
        }else if(landmark2DRadioButton.isSelected()){
            String text = landmarkIn2DProjectTextField.getText();
            if (text == null || text.trim().length() == 0) {
                JOptionPane.showMessageDialog(comp, "The " + landmarkProjectLabel.getText()
                    + " field is empty.", "Missing data entry", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                landmarkIn2DProjectTextField.requestFocus();
                return false;
            }

            if (!landmarkAgent.isValidLandmarkProject(text)) {
                JOptionPane.showMessageDialog(comp, "Can not find project name " + landmarkIn2DProjectTextField.getText()
                        + ". Check to see if it is valid Landmark project", "Project not found",    JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                landmarkIn2DProjectTextField.requestFocus();
                return false;
            }

            if(seismic2DFileComboBox.getSelectedIndex() == -1 || seismic2DFileComboBox.getSelectedIndex() == 0){
                JOptionPane.showMessageDialog(comp, "Must select a process level and version", "Process Level not selected",    JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                seismic2DFileComboBox.requestFocus();
                return false;
            }

            ListModel lm = selectedSeismic2DLineList.getModel();
            if(lm.getSize() < 1){
                JOptionPane.showMessageDialog(comp, "Must select at lease one seismic line.", "Seismic 2D Line not selected",    JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                rightArrowButton.requestFocus();
                return false;
            }

            Object[]  keys = selectedLineMap.keySet().toArray();
            for(int i = 0; keys!= null && i < keys.length; i++){
                if(!validateShotPointTextField((String)keys[i]))
                    return false;
            }
        }
        return true;
    }


    private boolean validateShotPointTextField(String linename) {
        if(linename == null || linename.trim().length() == 0)
            return false;
        int ind = 0;
        for(int i = 0; i < selectedSeismic2DLineList.getModel().getSize(); i++){
            if(selectedSeismic2DLineList.getModel().getElementAt(i) != null
                    && selectedSeismic2DLineList.getModel().getElementAt(i).equals(linename)){
                ind = i;
                break;
            }
        }
        LineInfo li = selectedLineMap.get(linename);

        final Component comp = this;
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);

        float fromVal = li.getMinShotPoint();

        float toVal = li.getMaxShotPoint();

        float by = li.getIncShotPoint();

        if(li.getIncShotPoint_def() > 0){
            if(by < li.getIncShotPoint_def()){
                JOptionPane.showMessageDialog(comp, "The increment value must not less than the defaul increment value (" + li.getIncShotPoint_def() + "). Reset to default.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointByTextField.requestFocus();
                shotpointByTextField.setText(String.valueOf(li.getIncShotPoint_def()));
                return false;
            }
        } else if(li.getIncShotPoint_def() < 0){
            if(by > li.getIncShotPoint_def()){
                JOptionPane.showMessageDialog(comp, "The increment value must not greater than the defaul value (" + li.getIncShotPoint_def() + "). Reset to default.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointByTextField.requestFocus();
                shotpointByTextField.setText(String.valueOf(li.getIncShotPoint_def()));
                return false;
            }
        }

        if(li.getIncShotPoint_def() != 0){
            int by_def = (int)(li.getIncShotPoint_def()*100);
            int by1 = (int)(by*100);
            int rem = by1 % by_def;
            if(rem != 0){
                JOptionPane.showMessageDialog(comp, "The increment value must be divisible by the default value (" + li.getIncShotPoint_def() + "). Reset to a valid one",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointByTextField.requestFocus();
                int fac = by1 - rem;
                float f = (float)(fac * 0.01);
                shotpointByTextField.setText(String.valueOf(f));
                return false;
            }
        }
        if(li.getIncShotPoint_def() > 0){
            if (!(fromVal >= li.getMinShotPoint_def())) {
                JOptionPane.showMessageDialog(comp,
                        "The vaule of \"from\" field must not be less than the default \"from\" value ("
                                + li.getMinShotPoint_def() + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointTextField.requestFocus();
                shotpointTextField.setText(String.valueOf(li.getMinShotPoint_def()));
                return false;
            }
            if (toVal > li.getMaxShotPoint_def()) {
                JOptionPane.showMessageDialog(comp,
                        "The value of \"to\" field must not be greater than the default \"to\" value ("
                                + li.getMaxShotPoint_def() + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointToTextField.requestFocus();
                shotpointToTextField.setText(String.valueOf(li.getMaxShotPoint_def()));
                return false;
            }
            if (!(toVal >= fromVal)) {
                JOptionPane.showMessageDialog(comp,
                        "The value of \"to\" field must be greater than the value of \"from\" field("
                                + fromVal + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointToTextField.requestFocus();
                shotpointToTextField.setText(String.valueOf(li.getMaxShotPoint_def()));
                return false;
            }
        } else {
            if (fromVal > li.getMinShotPoint_def()) {
                JOptionPane.showMessageDialog(comp,
                        "The value of \"from\" field must not be greater than the default \"from\" value ("
                                + li.getMinShotPoint_def() + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointTextField.requestFocus();
                shotpointTextField.setText(String.valueOf(li.getMinShotPoint_def()));
                return false;
            }
            if (!(toVal <= fromVal)) {
                JOptionPane.showMessageDialog(comp,
                        "The value of \"to\" field " +  toVal + " must be less than or equal to the value of \"from\" field " + fromVal + ". Reset to default \"to\" field."
                                , "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointToTextField.requestFocus();
                shotpointToTextField.setText(String.valueOf(li.getMaxShotPoint_def()));
                return false;
            }
            if(!(toVal >= li.getMaxShotPoint_def())){
                JOptionPane.showMessageDialog(comp,
                        "The value of \"to\" field must be greater than or equals to the value of \"from\" field. Reset to default \"to\" value ("
                        + li.getMaxShotPoint_def() + ")"
                                , "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointToTextField.requestFocus();
                shotpointToTextField.setText(String.valueOf(li.getMaxShotPoint_def()));
                return false;
            }
        }
/*
        if(li.getIncShotPoint_def() != 0){
            int rem = (int)((fromVal-li.getMinShotPoint_def())*100) % (int)(li.getIncShotPoint_def()*100);
            if(rem != 0){
                JOptionPane.showMessageDialog(comp,
                        "The difference of \"from\" field value(" + fromVal + ") and default \"from\" field value(" +
                            + li.getMinShotPoint_def() + ") must be divisible by the default increment (" + li.getIncShotPoint_def() + "). Reset to a valid one.", "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                selectedSeismic2DLineList.setSelectedIndex(ind);
                shotpointTextField.requestFocus();
                int fac = Math.round((fromVal-li.getMinShotPoint_def()) / li.getIncShotPoint_def());
                float val = li.getMinShotPoint_def() + (fac * li.getIncShotPoint_def());
                Format fmt = new Format("%.2f");
                shotpointTextField.setText(fmt.format(val));
                return false;
            }
        }

        if(by != 0){
            int by1 = (int)(by*100);
            int fromVal1 = (int)(fromVal*100);
            int toVal1 = (int)(toVal*100);
            int rem =  ((toVal1 - fromVal1) % by1);
            if(rem != 0){
             JOptionPane.showMessageDialog(comp,"The difference of \"to\" field value (" + toVal + ") and \"from\" field value (" + fromVal + ") must be divisible by the increment " + by + ". Reset to a valid one.",
                     "Invalid data entry",JOptionPane.WARNING_MESSAGE);
             ampExtTabbedPane.setSelectedIndex(tabIdx);
             selectedSeismic2DLineList.setSelectedIndex(ind);
             shotpointToTextField.requestFocus();
             //if(rem > 0){
             int to = toVal1 - rem;
             float val = (float)(to * 0.01);
                 //float next = toVal - rem;
                 //if(next == fromVal)
                //   next += by;
                 //shotpointToTextField.setText(String.valueOf(next));
             shotpointToTextField.setText(String.valueOf(val));
             //}else{
            //   float next = toVal + rem;
            //   if(next == fromVal)
            //       next += by;
            //   shotpointToTextField.setText(String.valueOf(next));
             //}
             return false;
            }
        }
*/
        return true;
    }

    
    private List<String> getUnmatched2DLinesHorizons(){
        ListModel lm = selectedSeismic2DLineList.getModel();
        String [] lines = new String[lm.getSize()];
        for(int i = 0; i < lm.getSize(); i++){
            lines[i] = (String)lm.getElementAt(i);
        }
        List<String> horizons = new ArrayList<String>();
        if(edgeModeRadioButton.isSelected()){
            if(landmark2DTopHorizonTextField.getText().trim().equals(landmark2DBottomHorizonTextField.getText().trim())){
                horizons.add(landmark2DTopHorizonTextField.getText().trim());
            }else{
                horizons.add(landmark2DTopHorizonTextField.getText().trim());
                horizons.add(landmark2DBottomHorizonTextField.getText().trim());
            }
        }else if(pointModeRadioButton.isSelected()){
            horizons.add(landmark2DHorizonTextField.getText().trim());
        }
        agent.get2DLinesHorizonsIntersetionSet(landmarkIn2DProjectTextField.getText().trim(), this, lines);
        String[] horizonSet = agent.getHorizonIntersectionArray();

        List<String> hList;
        List<String> umatched = new ArrayList<String>();
        if(horizonSet != null){
            hList = Arrays.asList(horizonSet);
            for(int i = 0; i < horizons.size(); i++){
            //for(int j = 0; j < hList.size(); j++){
                if(!hList.contains(horizons.get(i))){
                    umatched.add(horizons.get(i));
                }
            //  }
            }
        }
        return umatched;
    }

    /**
     * From the given list of selected lines and the list of horizon names, find lines which do not match all the 
     * given horizons
     * @param lines  a list of selected lines
     * @param horizons  a list of chosen horizons 
     */
    private List<String> getUnmatched2DLinesByHorizons(){
        ListModel lm = selectedSeismic2DLineList.getModel();
        String [] lines = new String[lm.getSize()];
        for(int i = 0; i < lm.getSize(); i++){
            lines[i] = (String)lm.getElementAt(i);
        }
        List<String> horizons = new ArrayList<String>();
        if(edgeModeRadioButton.isSelected()){
            if(landmark2DTopHorizonTextField.getText().trim().equals(landmark2DBottomHorizonTextField.getText().trim())){
                horizons.add(landmark2DTopHorizonTextField.getText().trim());
            }else{
                horizons.add(landmark2DTopHorizonTextField.getText().trim());
                horizons.add(landmark2DBottomHorizonTextField.getText().trim());
            }
        }else if(pointModeRadioButton.isSelected()){
            horizons.add(landmark2DHorizonTextField.getText().trim());
        }
        System.out.println("ltl is here lines");
        for(int i = 0; i < lines.length; i++){
        System.out.print("\"" + lines[i] + "\",");
        }
        System.out.println("\nltl is here");
        System.out.println("ltl is here horizons=" + horizons);
        agent.getUnrelated2DLinesByHorizons(landmarkIn2DProjectTextField.getText().trim(), gui, lines, horizons);
        String[] linesSet = agent.getUnrelated2DLineArray();
        System.out.println("ltl is here linesSet");
        for(int i = 0; linesSet != null && i < linesSet.length; i++){
        System.out.print("\"" + linesSet[i] + "\",");
        }
        System.out.println("\nltl is here");
        List<String> umatched = new ArrayList<String>();
        Set set = new HashSet();
        for(String s : linesSet){
        	if(!set.contains(s)){
        		set.add(s);
        		umatched.add(s);
        	}
        }
        
        return umatched;
    }
    
    private boolean validateLineTraceTextField(JTextField from, JTextField to, JTextField inc, int[] def) {
        final Component comp = this;
        String str = from.getText().trim();
        int idx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
        int fromVal = -1;
        try {
            fromVal = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be an integer value. Reset to default \"from\" value (" + def[0] + ").",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(idx);
            from.requestFocus();
            from.setText(String.valueOf(def[0]));
            return false;
        }

        int toVal = -1;
        try {
            toVal = Integer.valueOf(to.getText().trim()).intValue();
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be an integer value. Reset to default \"to\" value (" + def[1] + ").",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(idx);
            to.requestFocus();
            to.setText(String.valueOf(def[1]));
            return false;
        }

        int by = -1;
        try {
            by = Integer.parseInt(inc.getText().trim());
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be an integer value. Reset to default increment value (" + def[2] + ").",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);

            ampExtTabbedPane.setSelectedIndex(idx);
            inc.requestFocus();
            inc.setText(String.valueOf(def[2]));
            return false;
        }
        if(by == 0){
            JOptionPane.showMessageDialog(comp, "Increment must not be 0. Reset to the default value (" + def[2] + ").",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);

            ampExtTabbedPane.setSelectedIndex(idx);
            inc.requestFocus();
            inc.setText(String.valueOf(def[2]));
            return false;
        }

        if(def[2] > 0){
            if(by < def[2]){
                JOptionPane.showMessageDialog(comp, "The increment value must not less than the defaul value (" + def[2] + "). Reset to default.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);

                ampExtTabbedPane.setSelectedIndex(idx);
                inc.requestFocus();
                inc.setText(String.valueOf(def[2]));
                return false;
            }
        } else if(def[2] < 0){
            if(by > def[2]){
                JOptionPane.showMessageDialog(comp, "The increment value must not greater than the defaul value (" + def[2] + "). Reset to default.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);

                ampExtTabbedPane.setSelectedIndex(idx);
                inc.requestFocus();
                inc.setText(String.valueOf(def[2]));
                return false;
            }
        }

        if(def[2] != 0){
            int rem = by % def[2];
            if(rem != 0){
                JOptionPane.showMessageDialog(comp, "The increment value must be divisible by the default value (" + def[2] +  "). Reset to a valid one",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);

                ampExtTabbedPane.setSelectedIndex(idx);
                by = by - rem;
                inc.requestFocus();
                inc.setText(String.valueOf(by));
                return false;
            }
        }
        if(def[2] > 0){
            if (!(fromVal >= def[0])) {
                JOptionPane.showMessageDialog(comp,
                        "The vaule of \"from\" field must not be less than the default \"from\" value ("
                         + def[0] + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(idx);
                from.requestFocus();
                from.setText(String.valueOf(def[0]));
                return false;
            }

            int rem = (fromVal-def[0]) % def[2];
            if(rem != 0){

                JOptionPane.showMessageDialog(comp,
                      "The difference of " + fromVal + " (from) and " + def[0] + " (default \"from\" value) must be divisible by "
                           + def[2] + ". Reset to a valid one.", "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(idx);
                fromVal = fromVal - rem;
                from.requestFocus();
                from.setText(String.valueOf(fromVal));
                return false;
            }
            if (toVal > def[1]) {
                JOptionPane.showMessageDialog(comp,
                        "The value of \"to\" field must not be greater than the default \"to\" value ("
                                + def[1] + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(idx);
                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }
            if (!(toVal >= fromVal)) {
                JOptionPane.showMessageDialog(comp,
                        "The value of \"to\" field must be greater than the value of \"from\" field("
                                + fromVal + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(idx);
                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }
            rem = (toVal - fromVal) % by;
            if(rem != 0){
                JOptionPane.showMessageDialog(comp,
                      "The difference of " + toVal + " (\"to\" field) and " + fromVal + " (\"from\" field) must be divisible by increment "
                           + by + ". Reset to a valid one.", "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(idx);
                to.requestFocus();
                toVal = toVal - rem;
                to.setText(String.valueOf(toVal));
                return false;
            }
        } else {
            if (fromVal > def[0]) {
                JOptionPane.showMessageDialog(comp,
                        "The value of \"from\" field must not be greater than the default \"from\" value ("
                         + def[0] + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(idx);
                from.requestFocus();
                from.setText(String.valueOf(def[0]));
                return false;
            }
            int rem = (fromVal - def[0]) % def[2];
            if(rem != 0){

                JOptionPane.showMessageDialog(comp,
                  "The difference of \"from\" field value(" + fromVal + ") and default \"from\" field value(" + def[0] + ") must be divisible by the default increment ("
                       + def[2] + "). Reset to a valid one.", "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(idx);
                fromVal = fromVal - rem;
                from.requestFocus();
                from.setText(String.valueOf(fromVal));
                return false;
            }

            if (!(toVal <= fromVal)) {
                JOptionPane.showMessageDialog(comp,
                        "The value of \"to\" field " +  toVal + " must be less than or equal to the value of \"from\" field " + fromVal + ". Reset to default \"to\" field."
                         + fromVal, "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(idx);
                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }

            rem = (toVal - fromVal) % by;
            if(rem != 0){

                JOptionPane.showMessageDialog(comp,
                      "The difference of " + toVal + " (\"to\" field) and " + fromVal + " (\"from\" field) must be divisible by the increment "
                           + by + ". Reset to a valid one.", "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(idx);
                toVal = toVal - rem;
                to.requestFocus();
                to.setText(String.valueOf(toVal));
                return false;
            }

            if(!(toVal >= def[1])){
                JOptionPane.showMessageDialog(comp,
                        "The value of \"to\" field must be greater than or equals to the value of \"from\" field. Reset to default \"to\" value ("
                          + def[1] + ")", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(idx);
                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }
        }

        if(def[2] != 0){
            int rem = (fromVal-def[0]) % def[2];
            if(rem != 0){

                JOptionPane.showMessageDialog(comp,
                      "The difference of " + fromVal + " and " + def[0] + " must be divisible by "
                           + def[2], "Invalid data entry",JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(idx);
                fromVal = fromVal - rem;
                from.requestFocus();
                from.setText(String.valueOf(fromVal));
                return false;
            }
        }

        if(by != 0){
            int rem = (toVal - fromVal) % by;
            if(rem != 0){
             JOptionPane.showMessageDialog(comp,"Must be multiple of the increment " + by + " from the value " + fromVal ,
                     "Invalid data entry",JOptionPane.WARNING_MESSAGE);
             ampExtTabbedPane.setSelectedIndex(idx);
             to.requestFocus();
             if(rem > 0){
                 int next = toVal - rem;
                 if(next == fromVal)
                     next += by;
                 to.setText(String.valueOf(next));
             }else{
                 int next = toVal + rem;
                 if(next == fromVal)
                     next += by;
                 to.setText(String.valueOf(next));
             }
             return false;
            }
        }

        return true;
    }

    private boolean validateLineTraceTextFieldOld(JTextField from, JTextField to, JTextField inc, int[] def) {
        final Component comp = this;
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
        String str = from.getText().trim();
        int fromVal = -1;
        try {
            fromVal = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(tabIdx);
            from.requestFocus();
            from.setText(String.valueOf(def[0]));
            return false;
        }

        int toVal = -1;
        try {
            toVal = Integer.valueOf(to.getText().trim()).intValue();
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(tabIdx);
            to.requestFocus();
            to.setText(String.valueOf(def[1]));
            return false;
        }

        int by = -1;
        try {
            by = Integer.parseInt(inc.getText().trim());
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "Must be a numeric value.",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(tabIdx);
            inc.requestFocus();
            inc.setText(String.valueOf(def[2]));
            return false;
        }

        if(def[2] > 0){
            if(by < def[2]){
                JOptionPane.showMessageDialog(comp, "Must not less than the defaul value " + def[2],
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                inc.requestFocus();
                inc.setText(String.valueOf(def[2]));
                return false;
            }
        } else if(def[2] < 0){
            if(by > def[2]){
                JOptionPane.showMessageDialog(comp, "Must not greater than the defaul value " + def[2],
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                inc.requestFocus();
                inc.setText(String.valueOf(def[2]));
                return false;
            }
        }

        if(def[2] != 0){
            int rem = by % def[2];
            if(rem != 0){
                JOptionPane.showMessageDialog(comp, "Must be multiple of the value " + def[2],
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                inc.requestFocus();
                inc.setText(String.valueOf(def[2]));
                return false;
            }
        }
        if(def[2] > 0){
            if (!(fromVal >= def[0])) {
                JOptionPane.showMessageDialog(comp,
                        "Must not be less than the value "
                                + def[0], "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                from.requestFocus();
                from.setText(String.valueOf(def[0]));
                return false;
            }
            if (toVal > def[1]) {
                JOptionPane.showMessageDialog(comp,
                        "Must not be greater than the value ("
                                + def[1] + ")", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }
            if (!(toVal > fromVal)) {
                JOptionPane.showMessageDialog(comp,
                        "Must be greater than the value ("
                                + fromVal + ")", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }
        } else {
            if (fromVal > def[0]) {
                JOptionPane.showMessageDialog(comp,
                        "Must not be greater than the value ("
                                + def[0] + ")", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                from.requestFocus();
                from.setText(String.valueOf(lineTraceArray[0][0]));
                return false;
            }
            if (!(toVal < fromVal)) {
                JOptionPane.showMessageDialog(comp,
                        "Must be less than the value "
                                + fromVal, "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }
            if(!(toVal >= def[1])){
                JOptionPane.showMessageDialog(comp,
                        "Must be greater than or equals to the value "
                                + def[1], "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }
        }

        if(def[2] != 0 && ((fromVal-def[0]) % def[2]) != 0){
            JOptionPane.showMessageDialog(comp,
                    "Must be multiple of the increment " + def[2] + " from the value "
                            + def[0], "Invalid data entry",
                    JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(tabIdx);
            from.requestFocus();
            from.setText(String.valueOf(def[0]));
            return false;
        }

        if(by != 0){
            int rem = (toVal - fromVal) % by;
            if(rem != 0){
             JOptionPane.showMessageDialog(comp,"Must be multiple of the increment " + by + " from the value " + fromVal ,
                     "Invalid data entry",JOptionPane.WARNING_MESSAGE);
             ampExtTabbedPane.setSelectedIndex(tabIdx);
             to.requestFocus();
             if(rem > 0){
                 int next = toVal - rem;
                 if(next == fromVal)
                     next += by;
                 to.setText(String.valueOf(next));
             }else{
                 int next = toVal + rem;
                 if(next == fromVal)
                     next += by;
                 to.setText(String.valueOf(next));
             }
             return false;
            }
        }

        return true;
    }



    /*
    private boolean isValidLandmarkProject(String project){
        List<ProjectInfo> projects = agent.getLandmarkAllProjects();
        for(int i = 0; projects != null && i < projects.size(); i++){
            if(projects.get(i).getName().equals(project))
                return true;
        }
        return false;
    }
    */
    public String getLandmarkProjectText(){
        return landmarkInProjectTextField.getText().trim();
    }

    public String getLandmark2DProjectText(){
        return landmarkIn2DProjectTextField.getText().trim();
    }

    public String getSelectedLandmark2DProcessLevel(){
        return (String)seismic2DFileComboBox.getSelectedItem();
    }

    public void setLineTraceDefaultValue(int[][] infoArray) {
        if (infoArray == null)
            return;
        lineTraceArray = infoArray.clone();
        fillLineTraceFields(lineTraceArray);
    }

    public boolean horizonAlreadyChosen(String selected){
        if(selected == null || selected.trim().length() == 0)
            return false;
        if(landmark3DRadioButton.isSelected() && landmarkHorizonRadioButton.isSelected()){
            if(edgeModeRadioButton.isSelected()){
                if(landmark3DTopHorizonTextField != null && landmark3DTopHorizonTextField.getText().trim().length() > 0)
                    if(selected.equals(landmark3DTopHorizonTextField.getText().trim()))
                        return true;
                if(landmark3DBottomHorizonTextField != null && landmark3DBottomHorizonTextField.getText().trim().length() > 0)
                    if(selected.equals(landmark3DBottomHorizonTextField.getText().trim()))
                        return true;
            }
        }else if(landmark2DRadioButton.isSelected() && landmarkHorizonRadioButton.isSelected()){
            if(edgeModeRadioButton.isSelected()){
                if(landmark2DTopHorizonTextField != null && landmark2DTopHorizonTextField.getText().trim().length() > 0)
                    if(selected.equals(landmark2DTopHorizonTextField.getText().trim()))
                        return true;
                if(landmark2DBottomHorizonTextField != null && landmark2DBottomHorizonTextField.getText().trim().length() > 0)
                    if(selected.equals(landmark2DBottomHorizonTextField.getText().trim()))
                        return true;
            }
        }else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            if(bhpsuTopHorizonTextField != null && bhpsuTopHorizonTextField.getText().trim().length() > 0)
                if(selected.equals(bhpsuTopHorizonTextField.getText().trim()))
                    return true;
            if(bhpsuBottomHorizonTextField != null && bhpsuBottomHorizonTextField.getText().trim().length() > 0)
                if(selected.equals(bhpsuBottomHorizonTextField.getText().trim()))
                    return true;
        }

        return false;
    }

    public void setHorizonDataPath(javax.swing.JTextField target, String path) {
        if(target == landmark3DTopHorizonTextField)
            landmark3DTopHorizonTextField.setText(path);
        if(target == landmark3DBottomHorizonTextField)
            landmark3DBottomHorizonTextField.setText(path);
        if(target == landmark3DHorizonTextField)
            landmark3DHorizonTextField.setText(path);
        if(target == landmark2DTopHorizonTextField)
            landmark2DTopHorizonTextField.setText(path);
        if(target == landmark2DBottomHorizonTextField)
            landmark2DBottomHorizonTextField.setText(path);
        if(target == landmark2DHorizonTextField)
            landmark2DHorizonTextField.setText(path);
        if(target == bhpsuTopHorizonTextField)
            bhpsuTopHorizonTextField.setText(path);
        if(target == bhpsuBottomHorizonTextField)
            bhpsuBottomHorizonTextField.setText(path);
        if(target == bhpsuHorizonTextField)
            bhpsuHorizonTextField.setText(path);
    }
    private Map<String,List<String>> landmark2DfileLinesMap = new HashMap<String,List<String>>();
    public void setLandmarkIn2DProjectTextField(String text){
        landmarkIn2DProjectTextField.setText(text);
        String [] files = new String[1];
        if(text.trim().length() > 0){
            if(landmarkAgent == null)
                landmarkAgent = LandmarkServices.getInstance();
            
            landmark2DfileLinesMap = landmarkAgent.get2v2FileLinesMap(text.trim());
            Set set= landmark2DfileLinesMap.keySet () ;
            files = new String[set.size()+1];
            Iterator iter = set.iterator () ;
            files[0] = "Select one";
            int i = 1;
            while ( iter.hasNext())  {
                Object obj = iter.next();
                files[i++]=(String)obj;
            }
            
        }
        Arrays.sort(files,1,files.length);
        seismic2DFileComboBox.setModel(new javax.swing.DefaultComboBoxModel(files));
        seismic2DLineList.setListData(new Vector());
        selectedLines = new Vector();
        selectedSeismic2DLineList.setListData(selectedLines);
        selectedProcessLevelIndex = 0;
        resetShotpointInfo();

        if(landmarkOutputRadioButton.isSelected()){
            if(!landmark2DOutProjectTextField.isEnabled())
                landmark2DOutProjectTextField.setEnabled(true);
            landmark2DOutProjectTextField.setText(text);
            landmarkOutListButton.setEnabled(false);
            landmark2DOutProjectTextField.setEditable(false);
        }
    }
    public void setLandmarkOutProjectName(String name){
        landmarkOutProjectTextField.setText(name);
    }

    public void setLandmarkRunsumOutProjectName(String name){
        processPanel.setLandmarkOutProjectName(name);
    }
    public void setSelectedLandmarkProject(String text){
        landmarkInProjectTextField.setText(text);
        if(landmarkOutputRadioButton.isSelected()){
            if(!landmarkOutProjectTextField.isEnabled())
                landmarkOutProjectTextField.setEnabled(true);
            landmarkOutProjectTextField.setText(text);
            landmarkOutListButton.setEnabled(false);
            landmarkOutProjectTextField.setEditable(false);
        }

    }
    private void fillLineTraceFields(int[][] infoArray){
        linesTextField.setText(String.valueOf(infoArray[0][0]));
        linesToTextField.setText(String.valueOf(infoArray[0][1]));
        linesByTextField.setText(String.valueOf(infoArray[0][2]));
        tracesTextField.setText(String.valueOf(infoArray[1][0]));
        tracesToTextField.setText(String.valueOf(infoArray[1][1]));
        tracesByTextField.setText(String.valueOf(infoArray[1][2]));
    }

    public void setLineTraceFieldEnabled(boolean bool){
        entireVolumeButton.setEnabled(bool);
        linesTextField.setText("");
        linesTextField.setEditable(bool);
        linesToTextField.setText("");
        linesToTextField.setEditable(bool);
        linesByTextField.setText("");
        linesByTextField.setEditable(bool);
        tracesTextField.setText("");
        tracesTextField.setEditable(bool);
        tracesToTextField.setText("");
        tracesToTextField.setEditable(bool);
        tracesByTextField.setText("");
        tracesByTextField.setEditable(bool);
    }

    public void setVolumeTextField(String text){
        volumeTextField.setText(text);
    }
    /**
     * Save the state information for the Amplitude Extraction GUI.
     * @return xml string.
     */
    public String saveState(){
        StringBuffer content = new StringBuffer();
        content.append("<" + this.getClass().getName() + " ");
        content.append("project=\"" + project + "\" ");
        content.append("fileSystem=\"" + fileSystem + "\" ");

        content.append("height=\"" + this.getHeight() + "\" ");
        content.append("width=\"" + this.getWidth() + "\" ");
        content.append("x=\"" + this.getX() + "\" ");
        content.append("y=\"" + this.getY() + "\" ");
        if(bhpsuInRadioButton.isSelected()){
            content.append("inputType=\"" + "BHP SU" + "\" ");
        }else if(landmarkInRadioButton.isSelected()){
            content.append("inputType=\"" + "Landmark" + "\" ");
            if(landmark3DRadioButton.isSelected())
                content.append("landmarkInputType=\"" + "3D" + "\" ");
            else if(landmark2DRadioButton.isSelected())
                content.append("landmarkInputType=\"" + "2D" + "\" ");
        }
        if(bhpsuOutputRadioButton.isSelected()){
            content.append("outputType=\"" + "BHP SU" + "\" ");
        }else if(landmarkOutputRadioButton.isSelected()){
            content.append("outputType=\"" + "Landmark and BHP SU" + "\" ");
            //content.append("landmarkOutputProject=\"" + landmarkOutProjectTextField.getText() + "\" ");
        }

        if (edgeModeRadioButton.isSelected( ))
            content.append("horizonMode=\"" + "Edge" + "\" ");
        else if(pointModeRadioButton.isSelected())
            content.append("horizonMode=\"" + "Point" + "\" ");

        if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            content.append("horizonInputType=\"" + "BHP SU" + "\" >\n");
        }else if(landmarkHorizonRadioButton.isSelected()){
            content.append("horizonInputType=\"" + "Landmark" + "\" >\n");
        }

        String temp = inputCubeTextField.getText() == null ? "" : inputCubeTextField.getText();
        content.append("<BHPSU inputText=\"" + temp + "\" ");
        content.append("inputCubeDataset=\"" + inputCubeDataset + "\" ");
        temp = outputCubeTextField.getText() ==  null ? "" : outputCubeTextField.getText();
        content.append("outputText=\"" + temp + "\" ");
        temp = outputHorizonsTextField.getText() ==  null ? "" : outputHorizonsTextField.getText();
        content.append("outputHorizonsText=\"" + temp + "\" ");
        temp = landmarkOutputCubeTextField.getText() ==  null ? "" : landmarkOutputCubeTextField.getText();
        content.append("landmarkOutputText=\"" + temp + "\" ");
        temp = landmarkOutputHorizonsTextField.getText() ==  null ? "" : landmarkOutputHorizonsTextField.getText();
        content.append("landmarkOutputHorizonsText=\"" + temp + "\" ");
        content.append("landmarkOutputProject=\"" + landmarkOutProjectTextField.getText() + "\" ");
        if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            if (edgeModeRadioButton.isSelected( ))
                content.append("horizonMode=\"" + "Edge" + "\" ");
            else if(pointModeRadioButton.isSelected())
                content.append("horizonMode=\"" + "Point" + "\" ");
            if(bhpsuHorizonDatasetRadioButton.isSelected())
                content.append("horizonSource=\"" + AmpExtConstants.HORIZON_SOURCE_BHPSU + "\" ");
            else if(bhpsuHorizonRadioButton.isSelected())
                content.append("horizonSource=\"" + AmpExtConstants.HORIZON_SOURCE_ASCII + "\" ");
        }
        //int modeRadioSelectedIndex = -1;
        //if(bhpsuHorizonRadioButton.isSelected()){
            //if (edgeModeRadioButton.isSelected( )) {
            //    modeRadioSelectedIndex = 0;
                //content.append("modeRadioSelectedIndex=\"" + modeRadioSelectedIndex + "\" ");

        temp = bhpsuTopHorizonTextField.getText() == null ? "" : bhpsuTopHorizonTextField.getText();
        content.append("topHorizonText=\"" + temp + "\" ");
        temp = bhpsuBottomHorizonTextField.getText() == null ? "" : bhpsuBottomHorizonTextField.getText();
        content.append("bottomHorizonText=\"" + temp + "\" ");
        content.append("topHorizonBias=\"" + topHorizonBias + "\" ");
        content.append("bottomHorizonBias=\"" + bottomHorizonBias + "\" ");
        content.append("searchAboveTop=\"" + topHorizonAbove + "\" ");
        content.append("searchBelowTop=\"" + topHorizonBelow + "\" ");
        content.append("searchAboveBottom=\"" + bottomHorizonAbove + "\" ");
        content.append("searchBelowBottom=\"" + bottomHorizonBelow + "\" ");
            //} else if (pointModeRadioButton.isSelected()){
            //    modeRadioSelectedIndex = 1;
            //    content.append("modeRadioSelectedIndex=\"" + modeRadioSelectedIndex + "\" ");
        temp = bhpsuHorizonTextField.getText() == null ? "" : bhpsuHorizonTextField.getText();
        content.append("horizonText=\"" + temp + "\" ");
        content.append("horizonBias=\"" + horizonBias + "\" ");
        content.append("searchAbove=\"" + horizonAbove + "\" ");
        content.append("searchBelow=\"" + horizonBelow + "\" ");
           // }
                /*
        }else if(landmarkHorizonRadioButton.isSelected()){
            if (edgeModeRadioButton.isSelected()) {
                modeRadioSelectedIndex = 0;
                content.append("modeRadioSelectedIndex=\"" + modeRadioSelectedIndex + "\" ");
                temp = landmark3DTopHorizonTextField.getText() == null ? "" : landmark3DTopHorizonTextField.getText();
                content.append("landmarkTopHorizonText=\"" + temp + "\" ");
                temp = landmark3DBottomHorizonTextField.getText() == null ? "" : landmark3DBottomHorizonTextField.getText();
                content.append("landmarkBottomHorizonText=\"" + temp + "\" ");
                content.append("landmarkTopHorizonBias=\"" + landmark3DTopHorizonBias + "\" ");
                content.append("landmarkBottomHorizonBias=\"" + landmark3DBottomHorizonBias + "\" ");
                content.append("landmarkSearchAboveTop=\"" + landmark3DTopHorizonAbove + "\" ");
                content.append("landmarkSearchBelowTop=\"" + landmark3DTopHorizonBelow + "\" ");
                content.append("landmarkSearchAboveBottom=\"" + landmark3DBottomHorizonAbove + "\" ");
                content.append("landmarkSearchBelowBottom=\"" + landmark3DBottomHorizonBelow + "\" ");
            } else if (pointModeRadioButton.isSelected()){
                modeRadioSelectedIndex = 1;
                content.append("modeRadioSelectedIndex=\"" + modeRadioSelectedIndex + "\" ");
                temp = landmark3DHorizonTextField.getText() == null ? "" : landmark3DHorizonTextField.getText();
                content.append("landmarkHorizonText=\"" + temp + "\" ");
                content.append("landmarkHorizonBias=\"" + landmark3DHorizonBias + "\" ");
                content.append("landmarkSearchAbove=\"" + landmark3DHorizonAbove + "\" ");
                content.append("landmarkSearchBelow=\"" + landmark3DHorizonBelow + "\" ");
            }

        }*/
        //int typeRadioSelectedIndex = -1;
        //if (troughTypeRadioButton.isSelected()) typeRadioSelectedIndex = 0;
        //if (peakTypeRadioButton.isSelected()) typeRadioSelectedIndex = 1;
        //if (absTypeRadioButton.isSelected()) typeRadioSelectedIndex = 2;
        //content.append("typeRadioSelectedIndex=\"" + typeRadioSelectedIndex + "\" ");
        content.append("targetType=\"" + getTargetType(bhpsuHorizonTargetType) + "\" ");

        StringBuffer headerLimitsString = new StringBuffer();
        if (headerLimits != null && headerLimits.size() != 0) {
            for (int i = 0; i < headerLimits.size(); i++) {
                ArrayList<String> lst = headerLimits.get(i);
                for (int j = 0; j < lst.size(); j++) {
                    headerLimitsString.append(lst.get(j));
                    if (j < lst.size() - 1)
                        headerLimitsString.append(",");
                }
                if (i < headerLimits.size() - 1)
                    headerLimitsString.append(":");
            }
            if (AmpExtConstants.DEBUG_PRINT > 0) System.out.println("headerLimitsString="+headerLimitsString);
        }

        content.append("headerLimits=\"" + headerLimitsString.toString() + "\" ");

        //save the ranges
        content.append("epFromEntry=\"" + epFromTextField.getText() + "\" ");
        content.append("epToEntry=\"" + epToTextField.getText() + "\" ");
        content.append("epByEntry=\"" + epByTextField.getText() + "\" ");
        content.append("cdpFromEntry=\"" + cdpFromTextField.getText() + "\" ");
        content.append("cdpToEntry=\"" + cdpToTextField.getText() + "\" ");
        content.append("cdpByEntry=\"" + cdpByTextField.getText() + "\" />\n");
        content.append("<Landmark3D ");
        temp = landmarkInProjectTextField.getText() == null ? "" : landmarkInProjectTextField.getText().trim();
        content.append("landmark3DProject=\""  + temp + "\" ");
        temp = volumeTextField.getText() == null ? "" : volumeTextField.getText().trim();
        content.append("landmark3DInputVolume=\"" + temp + "\" ");
        temp = linesTextField.getText() == null ? "" : linesTextField.getText().trim();
        content.append("min_inline=\"" + temp + "\" ");
        temp = linesToTextField.getText() == null ? "" : linesToTextField.getText().trim();
        content.append("max_inline=\"" + temp + "\" ");
        temp = linesByTextField.getText() == null ? "" : linesByTextField.getText().trim();
        content.append("inc_inline=\"" + temp + "\" ");
        temp = tracesTextField.getText() == null ? "" : tracesTextField.getText().trim();
        content.append("min_trace=\"" + temp + "\" ");
        temp = tracesToTextField.getText() == null ? "" : tracesToTextField.getText().trim();
        content.append("max_trace=\"" + temp + "\" ");
        temp = tracesByTextField.getText() == null ? "" : tracesByTextField.getText().trim();
        content.append("inc_trace=\"" + temp + "\" ");

        if(lineTraceArray[0][0] != 0)
            content.append("default_min_line=\"" + lineTraceArray[0][0] + "\" ");
        else
            content.append("default_min_line=\"" + "" + "\" ");
        if(lineTraceArray[0][1] != 0)
            content.append("default_max_line=\"" + lineTraceArray[0][1] + "\" ");
        else
            content.append("default_max_line=\"" + "" + "\" ");

        if(lineTraceArray[0][2] != 0)
            content.append("default_inc_line=\"" + lineTraceArray[0][2] + "\" ");
        else
            content.append("default_inc_line=\"" + "" + "\" ");
        if(lineTraceArray[1][0] != 0)
            content.append("default_min_trace=\"" + lineTraceArray[1][0] + "\" ");
        else
            content.append("default_min_trace=\"" + "" + "\" ");
        if(lineTraceArray[1][1] != 0)
            content.append("default_max_trace=\"" + lineTraceArray[1][1] + "\" ");
        else
            content.append("default_max_trace=\"" + "" + "\" ");

        content.append("targetType=\"" + getTargetType(landmark3DHorizonTargetType) + "\" ");
        temp = landmark3DOutputCubeTextField.getText() ==  null ? "" : landmark3DOutputCubeTextField.getText();
        content.append("outputText=\"" + temp + "\" ");
        temp = landmark3DOutputHorizonsTextField.getText() ==  null ? "" : landmark3DOutputHorizonsTextField.getText();
        content.append("outputHorizonsText=\"" + temp + "\" ");
        content.append("landmarkOutputProject=\"" + landmark3DOutProjectTextField.getText() + "\" ");
        temp = landmark3DTopHorizonTextField.getText() ==  null ? "" : landmark3DTopHorizonTextField.getText();
        content.append("topHorizonText=\"" + temp + "\" ");
        temp = landmark3DBottomHorizonTextField.getText() ==  null ? "" : landmark3DBottomHorizonTextField.getText();
        content.append("bottomHorizonText=\"" + temp + "\" ");
        temp = landmark3DHorizonTextField.getText() ==  null ? "" : landmark3DHorizonTextField.getText();
        content.append("horizonText=\"" + temp + "\" ");
        content.append("topHorizonBias=\"" + landmark3DTopHorizonBias + "\" ");
        content.append("bottomHorizonBias=\"" + landmark3DBottomHorizonBias + "\" ");
        content.append("searchAboveTop=\"" + landmark3DTopHorizonAbove + "\" ");
        content.append("searchBelowTop=\"" + landmark3DTopHorizonBelow + "\" ");
        content.append("searchAboveBottom=\"" + landmark3DBottomHorizonAbove + "\" ");
        content.append("searchBelowBottom=\"" + landmark3DBottomHorizonBelow + "\" ");
        content.append("horizonBias=\"" + landmark3DHorizonBias + "\" ");
        content.append("searchAbove=\"" + landmark3DHorizonAbove + "\" ");
        content.append("searchBelow=\"" + landmark3DHorizonBelow + "\" ");
        if(lineTraceArray[1][2] != 0)
            content.append("default_inc_trace=\"" + lineTraceArray[1][2] + "\" />\n");
        else
            content.append("default_inc_trace=\"" + "" + "\" />\n");

        content.append("<Landmark2D ");
        temp = landmarkIn2DProjectTextField.getText() == null ? "" : landmarkIn2DProjectTextField.getText().trim();
        content.append("landmark2DProject=\""  + temp + "\" ");
        int index = seismic2DFileComboBox.getSelectedIndex();
        if(index > 0){
            String item = (String)seismic2DFileComboBox.getSelectedItem();
            content.append("landmark2D_seismic_file_process_level=\""  + item + "\" ");
        }else
            content.append("landmark2D_seismic_file_process_level=\""  + "" + "\" ");
        //ListModel list = selectedSeismic2DLineList.getModel();
        StringBuffer linelist = new StringBuffer();
        //for(int i = 0; list != null && i < list.getSize(); i++){
        //  linelist += (String)list.getElementAt(i);
        //  if(i < (list.getSize() -1))
        //      linelist += ",";
        //}
        Object[]  keys = selectedLineMap.keySet().toArray();
        for(int i = 0; keys!= null && i < keys.length; i++){
            linelist.append((String)keys[i] + ",");
            LineInfo li = selectedLineMap.get(keys[i]);
            linelist.append(li.getMinShotPoint_def() + ":" + li.getMaxShotPoint_def() + ":" + li.getIncShotPoint_def() + ",");
            linelist.append(li.getMinShotPoint() + ":" + li.getMaxShotPoint() + ":" + li.getIncShotPoint());
            if(i < (keys.length -1))
                linelist.append("~");
        }
        content.append("targetType=\"" + getTargetType(landmark2DHorizonTargetType) + "\" ");
        temp = landmark2DOutputCubeTextField.getText() ==  null ? "" : landmark2DOutputCubeTextField.getText();
        content.append("outputText=\"" + temp + "\" ");
        temp = landmark2DOutputHorizonsTextField.getText() ==  null ? "" : landmark2DOutputHorizonsTextField.getText();
        content.append("outputHorizonsText=\"" + temp + "\" ");
        content.append("landmarkOutputProject=\"" + landmark2DOutProjectTextField.getText() + "\" ");
        temp = landmark2DTopHorizonTextField.getText() ==  null ? "" : landmark2DTopHorizonTextField.getText();
        content.append("topHorizonText=\"" + temp + "\" ");
        temp = landmark2DBottomHorizonTextField.getText() ==  null ? "" : landmark2DBottomHorizonTextField.getText();
        content.append("bottomHorizonText=\"" + temp + "\" ");
        temp = landmark2DHorizonTextField.getText() ==  null ? "" : landmark2DHorizonTextField.getText();
        content.append("horizonText=\"" + temp + "\" ");
        content.append("topHorizonBias=\"" + landmark2DTopHorizonBias + "\" ");
        content.append("bottomHorizonBias=\"" + landmark2DBottomHorizonBias + "\" ");
        content.append("searchAboveTop=\"" + landmark2DTopHorizonAbove + "\" ");
        content.append("searchBelowTop=\"" + landmark2DTopHorizonBelow + "\" ");
        content.append("searchAboveBottom=\"" + landmark2DBottomHorizonAbove + "\" ");
        content.append("searchBelowBottom=\"" + landmark2DBottomHorizonBelow + "\" ");
        content.append("horizonBias=\"" + landmark2DHorizonBias + "\" ");
        content.append("searchAbove=\"" + landmark2DHorizonAbove + "\" ");
        content.append("searchBelow=\"" + landmark2DHorizonBelow + "\" ");
        String selectedLine = (String)selectedSeismic2DLineList.getSelectedValue();
        selectedLine = (selectedLine == null) ? "" : selectedLine.trim();
        content.append("landmark2D_selected_line=\""  + selectedLine + "\" ");
        content.append("landmark2D_seismic_lines=\""  + linelist + "\" />\n");

        if(processPanel.hasStates())
            content.append(processPanel.genState());
        //save the project descriptor
        String pdXml = XmlUtils.objectToXml(agent.getQiProjectDescriptor());
        content.append(pdXml);

        //save the job monitor if there is one
        if (jobMonitor != null) {
            String jmXml = XmlUtils.objectToXml(jobMonitor);
            content.append(jmXml);
        }

        content.append("</" + this.getClass().getName() + ">\n");
        return content.toString();
    }

    private String getTargetType(int type){
        if(type == 1)
            return "Trough";
        else if(type == 2)
            return "Peak";
        else if (type == 3)
            return "Abs";
        return "";
    }
    /**
     * Convert a string to a list of lists.
     * @param source A string with each comma separated list separated by a '|'
     * @return List of lists
     */
    private ArrayList<ArrayList<String>> converStringToList(String source){
        if (source == null || source.trim().length() == 0)
            return null;

        String [] strArray1 = source.split(":");
        ArrayList<ArrayList<String>> list1 = new ArrayList<ArrayList<String>>();
        for (int i = 0; i < strArray1.length; i++) {
            ArrayList<String> list2 = new ArrayList<String>();
            System.out.println("strArray1["+i+"]:"+strArray1[i]);
            String [] strArray2 = strArray1[i].split(",");
            for (int j = 0; j < strArray2.length; j++) {
                list2.add(strArray2[j]);
            }
            list1.add(list2);
        }

        return list1;
    }

    /**
     * Restore the state of the Amplitude Extraction GUI.
     * <p>
     * NOTE: The project descriptor is restored separately (by the AmpExt agent)
     * before restoring the rest of the state, because the GUI builder needs it.
     *
     * @param node XML tree node denoting the GUI element.
     */
    public void restoreState(Node node) {
        Element el = (Element)node;
        this.project = el.getAttribute("project");
        this.fileSystem = el.getAttribute("fileSystem");
        this.inputCubeDataset = el.getAttribute("inputCubeDataset");

        String hStr = el.getAttribute("height");
        if (hStr.trim().length() == 0)
            hStr = "298";  //default
        int height = Integer.valueOf(hStr).intValue();
        String wStr = el.getAttribute("width");
        if (wStr.trim().length() == 0)
            wStr = "761";  //default
        int width = Integer.valueOf(wStr).intValue();
        String X = el.getAttribute("x");
        if (X.trim().length() == 0)
            X = "0";    //default

        int x = Integer.valueOf(X).intValue();
        String Y = el.getAttribute("y");
        if (Y.trim().length() == 0)
            Y = "0";  //default

        int y = Integer.valueOf(Y).intValue();
        this.setSize(width,height);
        this.setLocation(x,y);

        NodeList children = node.getChildNodes();
        String nodeName;
        Node child;
        boolean isOldSet = true;
        for (int i = 0; i < children.getLength(); i++) {
            child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                nodeName = child.getNodeName();
                if (nodeName.equals("BHPSU")) {
                    isOldSet = false;
                    restoreBhpsu(child);
                }else if(nodeName.equals("Landmark3D")) {
                    isOldSet = false;
                    restoreLandmark3D(child);
                }else if(nodeName.equals("Landmark2D")) {
                    isOldSet = false;
                    restoreLandmark2D(child);
                }else if(nodeName.equals("DataProcessing")) {
                    processPanel.restoreState(child);
                }
            }
        }

        if(isOldSet){
            restoreOldSet(el);
        }

        String inputType = el.getAttribute("inputType");
        if(inputType != null && inputType.equals("Landmark")){
            landmarkInRadioButton.setSelected(true);
            String landmarkInputType = el.getAttribute("landmarkInputType");
            if(landmarkInputType != null && landmarkInputType.equals("2D"))
                landmark2DRadioButton.setSelected(true);
            else
                landmark3DRadioButton.setSelected(true);
            switch2LandmarkInput();
        }else{
            bhpsuInRadioButton.setSelected(true);
            switch2BhpsuInput();
        }

        String outputType = el.getAttribute("outputType");
        if(outputType != null && outputType.equals("Landmark and BHP SU")){
            landmarkOutputRadioButton.setSelected(true);
        }else
            bhpsuOutputRadioButton.setSelected(true);

        String horizonInputType = el.getAttribute("horizonInputType");
        if(horizonInputType != null && horizonInputType.equals("Landmark"))
            landmarkHorizonRadioButton.setSelected(true);
        //else{
        //  String horizonSource = el.getAttribute("horizonSource");
        //  if(horizonSource != null){
        //      if(horizonSource.equals(AmpExtConstants.HORIZON_SOURCE_ASCII))
        //          bhpsuHorizonRadioButton.setSelected(true);
        //      else if(horizonSource.equals(AmpExtConstants.HORIZON_SOURCE_BHPSU))
        //          bhpsuHorizonDatasetRadioButton.setSelected(true);
        //  }

        //}

        String hmode = el.getAttribute("horizonMode");
        if(hmode != null && hmode.equals("Point")){
            horizonMode = AmplitudeExtraction.POINT_MODE;
            pointModeRadioButton.setSelected(true);
        } else {
            horizonMode = AmplitudeExtraction.EDGE_MODE;
            edgeModeRadioButton.setSelected(true);
        }

        horizonInputTabActionPerformed(null);
        outputTypeRadioButtonActionPerformed1(null);

        if(processPanel.isNotRunAmpExtChecked())
        	setRunAmpExtEabled(false);
        	
        // restore job monitor if one was saved
        jobMonitor = restoreJobMonitor(node);

        // if a job monitor was saved, restore job monitor GUI components
        if (jobMonitor != null) {
            // create a job manager for the script.
            jobManager = new JobManager(agent.getMessagingMgr(), this);

            // update GUI job monitoring components
//            Runnable updateGUI = new Runnable() {
//                public void run() {
                    stateLabel.setText(jobMonitor.getJobStatus());
                    if (jobMonitor.isJobEnded()) {
                        runMenuItem.setEnabled(true);
                        runScriptButton.setEnabled(true);
                        writeScriptMenuItem.setEnabled(true);
                        writeScriptButton.setEnabled(true);
                        displayResultsMenuItem.setEnabled(true);
                        displayResultsButton.setEnabled(true);
                        // Disable ability to check status and cancel a job until Run Script selected.
                        checkStatusButton.setEnabled(false);
                        statusMenuItem.setEnabled(false);
                        cancelRunButton.setEnabled(false);
                        cancelRunMenuItem.setEnabled(false);
                    } else {
                        runMenuItem.setEnabled(false);
                        runScriptButton.setEnabled(false);
                        writeScriptMenuItem.setEnabled(false);
                        writeScriptButton.setEnabled(false);
                        displayResultsMenuItem.setEnabled(false);
                        displayResultsButton.setEnabled(false);
                        // Enable ability to check status and cancel a job .
                        checkStatusButton.setEnabled(true);
                        statusMenuItem.setEnabled(true);
                        cancelRunButton.setEnabled(true);
                        cancelRunMenuItem.setEnabled(true);
                    }

                    // restore stderr and stdout text at the time job status saved
                    stdoutTextArea.setText(jobMonitor.fetchStdoutText());
                    stderrTextArea.setText(jobMonitor.fetchStderrText());

                    String text = jobMonitor.getScriptPath() + "\n";
                    text += jobMonitor.getStderrPath() + "\n";
                    text += jobMonitor.getStdoutPath() + "\n";
                    text += "have been created in " + jobMonitor.getScriptDir();
                    fileSummaryTextArea.setText(text);
//                }
//            };
//            javax.swing.SwingUtilities.invokeLater(updateGUI);
        }
    }

    private void restoreOldSet(Element el){
        int index = Integer.valueOf(el.getAttribute("typeRadioSelectedIndex")).intValue();
        if (index == 0) troughTypeRadioButton.setSelected(true);
        if (index == 1) peakTypeRadioButton.setSelected(true);
        if (index == 2) absTypeRadioButton.setSelected(true);
        this.inputCubeTextField.setText(el.getAttribute("inputText"));
        this.outputCubeTextField.setText(el.getAttribute("outputText"));
        this.outputHorizonsTextField.setText(el.getAttribute("outputHorizonsText"));

        String mode = el.getAttribute("modeRadioSelectedIndex");
        if (mode.trim().length() == 0)
            mode = "0";
        index =  Integer.valueOf(mode).intValue();
        if (index == 0) edgeModeRadioButton.setSelected(true);
        if (index == 1) pointModeRadioButton.setSelected(true);

        if (index == 0) {// edge mode
            this.bhpsuTopHorizonTextField.setText(el.getAttribute("topHorizonText"));
            this.bhpsuBottomHorizonTextField.setText(el.getAttribute("bottomHorizonText"));

            topHorizonBias = el.getAttribute("topHorizonBias");
            edgeModeHorizonParamsTableModel.setValueAt(topHorizonBias, TOP_HORIZON_BIAS_ROW, TOP_HORIZON_BIAS_COL);
            edgeModeHorizonSettings[0][0] = topHorizonBias;
            bottomHorizonBias = el.getAttribute("bottomHorizonBias");
            edgeModeHorizonParamsTableModel.setValueAt(bottomHorizonBias, BOTTOM_HORIZON_BIAS_ROW, BOTTOM_HORIZON_BIAS_COL);
            edgeModeHorizonSettings[1][0] = bottomHorizonBias;

            topHorizonAbove = el.getAttribute("searchAboveTop");
            edgeModeHorizonParamsTableModel.setValueAt(topHorizonAbove, TOP_HORIZON_ABOVE_ROW, TOP_HORIZON_ABOVE_COL);
            edgeModeHorizonSettings[0][1] = topHorizonAbove;

            bottomHorizonAbove = el.getAttribute("searchAboveBottom");
            edgeModeHorizonParamsTableModel.setValueAt(bottomHorizonAbove, BOTTOM_HORIZON_ABOVE_ROW, BOTTOM_HORIZON_ABOVE_COL);
            edgeModeHorizonSettings[1][1] = bottomHorizonAbove;

            topHorizonBelow = el.getAttribute("searchBelowTop");
            edgeModeHorizonParamsTableModel.setValueAt(topHorizonBelow, TOP_HORIZON_BELOW_ROW, TOP_HORIZON_BELOW_COL);
            edgeModeHorizonSettings[0][2] = topHorizonBelow;

            bottomHorizonBelow = el.getAttribute("searchBelowBottom");
            edgeModeHorizonParamsTableModel.setValueAt(bottomHorizonBelow, BOTTOM_HORIZON_BELOW_ROW, BOTTOM_HORIZON_BELOW_COL);
            edgeModeHorizonSettings[1][2] = bottomHorizonBelow;

            switch2EdgeMode();
        } else if (index == 1) { // point mode
            bhpsuHorizonTextField.setText(el.getAttribute("horizonText"));

            horizonBias = el.getAttribute("horizonBias");
            pointModeHorizonParamsTableModel.setValueAt(horizonBias, HORIZON_BIAS_ROW, HORIZON_BIAS_COL);
            pointModeHorizonSettings[0][0] = horizonBias;

            horizonAbove = el.getAttribute("searchAbove");
            pointModeHorizonParamsTableModel.setValueAt(horizonAbove, HORIZON_ABOVE_ROW, HORIZON_ABOVE_COL);
            pointModeHorizonSettings[0][1] = horizonAbove;

            horizonBelow = el.getAttribute("searchBelow");
            pointModeHorizonParamsTableModel.setValueAt(horizonBelow, HORIZON_BELOW_ROW, HORIZON_BELOW_COL);
            pointModeHorizonSettings[0][2] = horizonBelow;

            switch2PointMode();
        }

        ArrayList<ArrayList<String>> list = converStringToList(el.getAttribute("headerLimits"));
        if (list != null) {
            headerLimits = list;
        }

        //NOTE: Everything restored below this point was not in version 1.0
        //restore ranges
        String epFromEntry = el.getAttribute("epFromEntry");
        if (epFromEntry == null) {
            initRanges(headerLimits);
        } else {
            //if one exists, they all exist
            epFromTextField.setText(epFromEntry);
            epToTextField.setText(el.getAttribute("epToEntry"));
            epByTextField.setText(el.getAttribute("epByEntry"));
            cdpFromTextField.setText(el.getAttribute("cdpFromEntry"));
            cdpToTextField.setText(el.getAttribute("cdpToEntry"));
            cdpByTextField.setText(el.getAttribute("cdpByEntry"));
        }
        setEntireDatasetButtonEnabled(true);
    }
    private void restoreBhpsu(Node node){
        Element el = (Element)node;
        String target  = el.getAttribute("targetType");
        /*
        if (target.equals("Trough")){
            troughTypeRadioButton.setSelected(true);
        }else if (target.equals("Peak")){
            peakTypeRadioButton.setSelected(true);
        }else if (target.equals("Abs")){
            absTypeRadioButton.setSelected(true);
        }else {
            troughTypeRadioButton.setSelected(true); //default
        }
        */

        if (target.equals("Trough")) bhpsuHorizonTargetType = 1;
        else if (target.equals("Peak")) bhpsuHorizonTargetType = 2;
        else if (target.equals("Abs")) bhpsuHorizonTargetType = 3;
        else bhpsuHorizonTargetType = 1; //default
        this.inputCubeTextField.setText(el.getAttribute("inputText"));
        this.outputCubeTextField.setText(el.getAttribute("outputText"));
        this.outputHorizonsTextField.setText(el.getAttribute("outputHorizonsText"));
        this.landmarkOutputCubeTextField.setText(el.getAttribute("landmarkOutputText"));
        this.landmarkOutputHorizonsTextField.setText(el.getAttribute("landmarkOutputHorizonsText"));
        this.bhpsuTopHorizonTextField.setText(el.getAttribute("topHorizonText"));
        this.bhpsuBottomHorizonTextField.setText(el.getAttribute("bottomHorizonText"));

        String horizonSource = el.getAttribute("horizonSource");
        if(horizonSource != null){
            if(horizonSource.equals(AmpExtConstants.HORIZON_SOURCE_BHPSU))
                bhpsuHorizonDatasetRadioButton.setSelected(true);
            else
                bhpsuHorizonRadioButton.setSelected(true);
        }

        String outLmProject = el.getAttribute("landmarkOutputProject");
        outLmProject = (outLmProject == null) ? "" : outLmProject.trim();
        landmarkOutProjectTextField.setText(outLmProject);
        topHorizonBias = el.getAttribute("topHorizonBias");
        edgeModeHorizonParamsTableModel.setValueAt(topHorizonBias, TOP_HORIZON_BIAS_ROW, TOP_HORIZON_BIAS_COL);
        edgeModeHorizonSettings[0][0] = topHorizonBias;
        bottomHorizonBias = el.getAttribute("bottomHorizonBias");
        edgeModeHorizonParamsTableModel.setValueAt(bottomHorizonBias, BOTTOM_HORIZON_BIAS_ROW, BOTTOM_HORIZON_BIAS_COL);
        edgeModeHorizonSettings[1][0] = bottomHorizonBias;

        topHorizonAbove = el.getAttribute("searchAboveTop");
        edgeModeHorizonParamsTableModel.setValueAt(topHorizonAbove, TOP_HORIZON_ABOVE_ROW, TOP_HORIZON_ABOVE_COL);
        edgeModeHorizonSettings[0][1] = topHorizonAbove;

        bottomHorizonAbove = el.getAttribute("searchAboveBottom");
        edgeModeHorizonParamsTableModel.setValueAt(bottomHorizonAbove, BOTTOM_HORIZON_ABOVE_ROW, BOTTOM_HORIZON_ABOVE_COL);
        edgeModeHorizonSettings[1][1] = bottomHorizonAbove;

        topHorizonBelow = el.getAttribute("searchBelowTop");
        edgeModeHorizonParamsTableModel.setValueAt(topHorizonBelow, TOP_HORIZON_BELOW_ROW, TOP_HORIZON_BELOW_COL);
        edgeModeHorizonSettings[0][2] = topHorizonBelow;

        bottomHorizonBelow = el.getAttribute("searchBelowBottom");
        edgeModeHorizonParamsTableModel.setValueAt(bottomHorizonBelow, BOTTOM_HORIZON_BELOW_ROW, BOTTOM_HORIZON_BELOW_COL);
        edgeModeHorizonSettings[1][2] = bottomHorizonBelow;
        bhpsuHorizonTextField.setText(el.getAttribute("horizonText"));

        horizonBias = el.getAttribute("horizonBias");
        pointModeHorizonParamsTableModel = horizonParametersTable.getModel();
        pointModeHorizonParamsTableModel.setValueAt(horizonBias, HORIZON_BIAS_ROW, HORIZON_BIAS_COL);
        pointModeHorizonSettings[0][0] = horizonBias;

        horizonAbove = el.getAttribute("searchAbove");
        pointModeHorizonParamsTableModel.setValueAt(horizonAbove, HORIZON_ABOVE_ROW, HORIZON_ABOVE_COL);
        pointModeHorizonSettings[0][1] = horizonAbove;

        horizonBelow = el.getAttribute("searchBelow");
        pointModeHorizonParamsTableModel.setValueAt(horizonBelow, HORIZON_BELOW_ROW, HORIZON_BELOW_COL);
        pointModeHorizonSettings[0][2] = horizonBelow;
        /*
        if(pointModeRadioButton.isSelected())
            switch2PointMode();
        else
            switch2EdgeMode();
            */
        ArrayList<ArrayList<String>> list = converStringToList(el.getAttribute("headerLimits"));
        if (list != null) {
            headerLimits = list;
        }

        //NOTE: Everything restored below this point was not in version 1.0
        //restore ranges
        String epFromEntry = el.getAttribute("epFromEntry");
        if (epFromEntry == null) {
            initRanges(headerLimits);
        } else {
            //if one exists, they all exist
            epFromTextField.setText(epFromEntry);
            epToTextField.setText(el.getAttribute("epToEntry"));
            epByTextField.setText(el.getAttribute("epByEntry"));
            cdpFromTextField.setText(el.getAttribute("cdpFromEntry"));
            cdpToTextField.setText(el.getAttribute("cdpToEntry"));
            cdpByTextField.setText(el.getAttribute("cdpByEntry"));
        }
        setEntireDatasetButtonEnabled(true);
    }

    private void restoreLandmark3D(Node node){
        Element el = (Element)node;
        String target  = el.getAttribute("targetType");
        if (target.equals("Trough")) landmark3DHorizonTargetType = 1;
        else if (target.equals("Peak")) landmark3DHorizonTargetType = 2;
        else if (target.equals("Abs")) landmark3DHorizonTargetType = 3;
        else landmark3DHorizonTargetType = 1; //default
        landmarkInProjectTextField.setText(el.getAttribute("landmark3DProject"));
        String landmarkInputVolume = el.getAttribute("landmark3DInputVolume");
        String outLmProject = el.getAttribute("landmarkOutputProject");
        outLmProject = (outLmProject == null) ? "" : outLmProject.trim();
        landmark3DOutProjectTextField.setText(outLmProject);
        if(landmarkInputVolume == null || landmarkInputVolume.trim().length() == 0){
            landmarkInputVolume = "";
            setLineTraceFieldEnabled(false);
        }else{
            setLineTraceFieldEnabled(true);
            volumeTextField.setText(landmarkInputVolume);
        }
        String minlines = el.getAttribute("min_inline");
        if(minlines == null)
            minlines = "";
        linesTextField.setText(minlines);
        String maxlines = el.getAttribute("max_inline");
        if(maxlines == null)
            maxlines = "";
        linesToTextField.setText(maxlines);
        String inclines = el.getAttribute("inc_inline");
        if(inclines == null)
            inclines = "";
        linesByTextField.setText(inclines);

        String mintraces = el.getAttribute("min_trace");
        if(mintraces == null)
            mintraces = "";
        tracesTextField.setText(mintraces);
        String maxtraces = el.getAttribute("max_trace");
        if(maxtraces == null)
            maxtraces = "";
        tracesToTextField.setText(maxtraces);
        String inctraces = el.getAttribute("inc_trace");
        if(inctraces == null)
            inctraces = "";
        tracesByTextField.setText(inctraces);

        String default_min_line = el.getAttribute("default_min_line");
        if(default_min_line == null)
            default_min_line = "";
        if(default_min_line.trim().length() > 0)
            lineTraceArray[0][0] = Integer.valueOf(default_min_line).intValue();
        String default_max_line = el.getAttribute("default_max_line");
        if(default_max_line == null)
            default_max_line = "";
        if(default_max_line.trim().length() > 0)
            lineTraceArray[0][1] = Integer.valueOf(default_max_line).intValue();
        String default_inc_line = el.getAttribute("default_inc_line");
        if(default_inc_line == null)
            default_inc_line = "";
        if(default_inc_line.trim().length() > 0)
            lineTraceArray[0][2] = Integer.valueOf(default_inc_line).intValue();

        String default_min_trace = el.getAttribute("default_min_trace");
        if(default_min_trace == null)
            default_min_trace = "";
        if(default_min_trace.trim().length() > 0)
            lineTraceArray[1][0] = Integer.valueOf(default_min_trace).intValue();
        String default_max_trace = el.getAttribute("default_max_trace");
        if(default_max_trace == null)
            default_max_trace = "";
        if(default_max_trace.trim().length() > 0)
            lineTraceArray[1][1] = Integer.valueOf(default_max_trace).intValue();
        String default_inc_trace = el.getAttribute("default_inc_trace");
        if(default_inc_trace == null)
            default_inc_trace = "";
        if(default_inc_trace.trim().length() > 0)
            lineTraceArray[1][2] = Integer.valueOf(default_inc_trace).intValue();

        this.landmark3DOutputCubeTextField.setText(el.getAttribute("outputText"));
        this.landmark3DOutputHorizonsTextField.setText(el.getAttribute("outputHorizonsText"));
        this.landmark3DTopHorizonTextField.setText(el.getAttribute("topHorizonText"));
        this.landmark3DBottomHorizonTextField.setText(el.getAttribute("bottomHorizonText"));
        this.landmark3DHorizonTextField.setText(el.getAttribute("horizonText"));

        landmark3DTopHorizonBias = el.getAttribute("topHorizonBias");
        landmark3DEdgeModeHorizonParamsTableModel.setValueAt(landmark3DTopHorizonBias, TOP_HORIZON_BIAS_ROW, TOP_HORIZON_BIAS_COL);
        landmark3DEdgeModeHorizonSettings[0][0] = landmark3DTopHorizonBias;

        landmark3DBottomHorizonBias = el.getAttribute("bottomHorizonBias");
        landmark3DEdgeModeHorizonParamsTableModel.setValueAt(landmark3DBottomHorizonBias, BOTTOM_HORIZON_BIAS_ROW, BOTTOM_HORIZON_BIAS_COL);
        landmark3DEdgeModeHorizonSettings[1][0] = landmark3DBottomHorizonBias;

        landmark3DTopHorizonAbove = el.getAttribute("searchAboveTop");
        landmark3DEdgeModeHorizonParamsTableModel.setValueAt(landmark3DTopHorizonAbove, TOP_HORIZON_ABOVE_ROW, TOP_HORIZON_ABOVE_COL);
        landmark3DEdgeModeHorizonSettings[0][1] = landmark3DTopHorizonAbove;

        landmark3DBottomHorizonAbove = el.getAttribute("searchAboveBottom");
        landmark3DEdgeModeHorizonParamsTableModel.setValueAt(landmark3DBottomHorizonAbove, BOTTOM_HORIZON_ABOVE_ROW, BOTTOM_HORIZON_ABOVE_COL);
        landmark3DEdgeModeHorizonSettings[1][1] = landmark3DBottomHorizonAbove;

        landmark3DTopHorizonBelow = el.getAttribute("searchBelowTop");
        landmark3DEdgeModeHorizonParamsTableModel.setValueAt(landmark3DTopHorizonBelow, TOP_HORIZON_BELOW_ROW, TOP_HORIZON_BELOW_COL);
        landmark3DEdgeModeHorizonSettings[0][2] = landmark3DTopHorizonBelow;

        landmark3DBottomHorizonBelow = el.getAttribute("searchBelowBottom");
        landmark3DEdgeModeHorizonParamsTableModel.setValueAt(landmark3DBottomHorizonBelow, BOTTOM_HORIZON_BELOW_ROW, BOTTOM_HORIZON_BELOW_COL);
        landmark3DEdgeModeHorizonSettings[1][2] = landmark3DBottomHorizonBelow;

        landmark3DHorizonBias = el.getAttribute("horizonBias");
        //pointModeHorizonParamsTableModel = horizonParametersTable.getModel();
        landmark3DPointModeHorizonParamsTableModel.setValueAt(landmark3DHorizonBias, HORIZON_BIAS_ROW, HORIZON_BIAS_COL);
        landmark3DPointModeHorizonSettings[0][0] = landmark3DHorizonBias;

        landmark3DHorizonAbove = el.getAttribute("searchAbove");
        landmark3DPointModeHorizonParamsTableModel.setValueAt(landmark3DHorizonAbove, HORIZON_ABOVE_ROW, HORIZON_ABOVE_COL);
        landmark3DPointModeHorizonSettings[0][1] = landmark3DHorizonAbove;

        landmark3DHorizonBelow = el.getAttribute("searchBelow");
        landmark3DPointModeHorizonParamsTableModel.setValueAt(landmark3DHorizonBelow, HORIZON_BELOW_ROW, HORIZON_BELOW_COL);
        landmark3DPointModeHorizonSettings[0][2] = landmark3DHorizonBelow;
    }

    private void restoreLandmark2D(Node node){
        Element el = (Element)node;
        String target  = el.getAttribute("targetType");
        if (target.equals("Trough")) landmark2DHorizonTargetType = 1;
        else if (target.equals("Peak")) landmark2DHorizonTargetType = 2;
        else if (target.equals("Abs")) landmark2DHorizonTargetType = 3;
        else landmark2DHorizonTargetType = 1; //default
        String proj = el.getAttribute("landmark2DProject");
        proj = (proj == null) ? "" : proj.trim();
        boolean invalidProjectFound = false;
        
        if(proj.length() > 0 && landmarkAgent.isValidLandmarkProject(proj)){
            if(landmarkAgent == null)
                landmarkAgent = LandmarkServices.getInstance();
            setLandmarkIn2DProjectTextField(proj);
        }else{
            invalidProjectFound = true;
            logger.warning("Intended to restore an invalid Landmark 2D project " + proj);
        }
        String outLmProject = el.getAttribute("landmarkOutputProject");
        outLmProject = (outLmProject == null) ? "" : outLmProject.trim();
        landmark2DOutProjectTextField.setText(outLmProject);
        this.landmark2DOutputCubeTextField.setText(el.getAttribute("outputText"));
        this.landmark2DOutputHorizonsTextField.setText(el.getAttribute("outputHorizonsText"));
        this.landmark2DTopHorizonTextField.setText(el.getAttribute("topHorizonText"));
        this.landmark2DBottomHorizonTextField.setText(el.getAttribute("bottomHorizonText"));

        landmark2DTopHorizonBias = el.getAttribute("topHorizonBias");
        landmark2DEdgeModeHorizonParamsTableModel.setValueAt(landmark2DTopHorizonBias, TOP_HORIZON_BIAS_ROW, TOP_HORIZON_BIAS_COL);
        landmark2DEdgeModeHorizonSettings[0][0] = landmark2DTopHorizonBias;
        landmark2DBottomHorizonBias = el.getAttribute("bottomHorizonBias");
        landmark2DEdgeModeHorizonParamsTableModel.setValueAt(landmark2DBottomHorizonBias, BOTTOM_HORIZON_BIAS_ROW, BOTTOM_HORIZON_BIAS_COL);
        landmark2DEdgeModeHorizonSettings[1][0] = landmark2DBottomHorizonBias;

        landmark2DTopHorizonAbove = el.getAttribute("searchAboveTop");
        landmark2DEdgeModeHorizonParamsTableModel.setValueAt(landmark2DTopHorizonAbove, TOP_HORIZON_ABOVE_ROW, TOP_HORIZON_ABOVE_COL);
        landmark2DEdgeModeHorizonSettings[0][1] = landmark2DTopHorizonAbove;

        landmark2DBottomHorizonAbove = el.getAttribute("searchAboveBottom");
        landmark2DEdgeModeHorizonParamsTableModel.setValueAt(landmark2DBottomHorizonAbove, BOTTOM_HORIZON_ABOVE_ROW, BOTTOM_HORIZON_ABOVE_COL);
        landmark2DEdgeModeHorizonSettings[1][1] = landmark2DBottomHorizonAbove;

        landmark2DTopHorizonBelow = el.getAttribute("searchBelowTop");
        landmark2DEdgeModeHorizonParamsTableModel.setValueAt(landmark2DTopHorizonBelow, TOP_HORIZON_BELOW_ROW, TOP_HORIZON_BELOW_COL);
        landmark2DEdgeModeHorizonSettings[0][2] = landmark2DTopHorizonBelow;

        landmark2DBottomHorizonBelow = el.getAttribute("searchBelowBottom");
        landmark2DEdgeModeHorizonParamsTableModel.setValueAt(landmark2DBottomHorizonBelow, BOTTOM_HORIZON_BELOW_ROW, BOTTOM_HORIZON_BELOW_COL);
        landmark2DEdgeModeHorizonSettings[1][2] = landmark2DBottomHorizonBelow;
        landmark2DHorizonTextField.setText(el.getAttribute("horizonText"));

        landmark2DHorizonBias = el.getAttribute("horizonBias");
        //landmark2DPointModeHorizonParamsTableModel = landmark2DHorizonParametersTable.getModel();
        landmark2DPointModeHorizonParamsTableModel.setValueAt(landmark2DHorizonBias, HORIZON_BIAS_ROW, HORIZON_BIAS_COL);
        landmark2DPointModeHorizonSettings[0][0] = landmark2DHorizonBias;

        landmark2DHorizonAbove = el.getAttribute("searchAbove");
        landmark2DPointModeHorizonParamsTableModel.setValueAt(landmark2DHorizonAbove, HORIZON_ABOVE_ROW, HORIZON_ABOVE_COL);
        landmark2DPointModeHorizonSettings[0][1] = landmark2DHorizonAbove;

        landmark2DHorizonBelow = el.getAttribute("searchBelow");
        landmark2DPointModeHorizonParamsTableModel.setValueAt(landmark2DHorizonBelow, HORIZON_BELOW_ROW, HORIZON_BELOW_COL);
        landmark2DPointModeHorizonSettings[0][2] = landmark2DHorizonBelow;
        String process_level = el.getAttribute("landmark2D_seismic_file_process_level");
        process_level = (process_level == null) ? "" : process_level.trim();

        javax.swing.ComboBoxModel model = seismic2DFileComboBox.getModel();
        int index = -1;
        for(int i = 0; i < model.getSize(); i++){
            if(model.getElementAt(i) != null && model.getElementAt(i).equals(process_level)){
                index = i;
                break;
            }
        }
        if(index != -1)
            seismic2DFileComboBox.setSelectedIndex(index);
        String lines = el.getAttribute("landmark2D_seismic_lines");
        lines = (lines == null) ? "" : lines.trim();
        String [] lineArrs = new String[0];
        String [] contents = null;
        String [] def_ranges = null;
        String [] ranges = null;
        if(lines.length() > 0){
            lineArrs = lines.split("~");
            selectedLineMap = new HashMap<String,LineInfo>();
        }

        if(!invalidProjectFound){
        for(int i = 0; lineArrs != null && i < lineArrs.length; i++){
            if(landmarkAgent == null)
                landmarkAgent = LandmarkServices.getInstance();

            contents = lineArrs[i].split(",");

            def_ranges = contents[1].split(":");
            int linenum = landmarkAgent.get2DLineNumberByName(proj,contents[0]);
            LineInfo li = new LineInfo(proj,contents[0],linenum,Float.valueOf(def_ranges[0]),Float.valueOf(def_ranges[1]),
                    Float.valueOf(def_ranges[2]));
            ranges = contents[2].split(":");
            li.setMinShotPoint(Float.valueOf(ranges[0]));
            li.setMaxShotPoint(Float.valueOf(ranges[1]));
            li.setIncShotPoint(Float.valueOf(ranges[2]));
            selectedLineMap.put(contents[0], li);
        }
        }else{
            logger.warning("Can not process further due to invalid landmark project " + proj + " found. Restore process incomplete.");
        }
        //String [] lineArr = lines.split(",");
        Vector<String> v = new Vector<String>();
        for(String s : selectedLineMap.keySet()){
            v.add(s);
        }
        Collections.sort(v);
        selectedSeismic2DLineList.setListData(v);
        selectedLines = v;
        String landmark2D_selected_line = el.getAttribute("landmark2D_selected_line");
        landmark2D_selected_line = (landmark2D_selected_line == null) ? "" : landmark2D_selected_line.trim();
        selectedLineName = landmark2D_selected_line;
        //int ind = 0;
        //for(int i = 0; i < selectedSeismic2DLineList.getModel().getSize(); i++){
        //  if(selectedSeismic2DLineList.getModel().getElementAt(i) != null && selectedSeismic2DLineList.getModel().getElementAt(i).equals(landmark2D_selected_line)){
        //      ind = i;
        //      break;
        //  }
        //}
        //selectedSeismic2DLineList.setSelectedValue(landmark2D_selected_line, true);
        //selectedSeismic2DLineList.setSelectedIndex(ind);

    }

    /* Extract the job monitor from the saved state. If the state
     * does not contain a job monitor, return null.
     *
     * @return Job monitor for the generated script.
     */
    private JobMonitor restoreJobMonitor(Node node) {
        JobMonitor jobMonitor = null;

        NodeList children = node.getChildNodes();
        String nodeName;
        Node child;
        for (int i = 0; i < children.getLength(); i++) {
          child = children.item(i);
          if (child.getNodeType() == Node.ELEMENT_NODE) {
            nodeName = child.getNodeName();
            if (nodeName.equals("JobMonitor")) {
                jobMonitor = (JobMonitor)XmlUtils.XmlToObject(child);
                break;
            }
          }
        }

        return jobMonitor;
    }

    private void startGUI() {
        // help
        try {
            ClassLoader cl = AmplitudeExtraction.class.getClassLoader();
            String helpsetName = "com/bhpb/help/AmplitudeExtractionHelp.hs";
            URL helpSetURL = HelpSet.findHelpSet(cl,helpsetName);
            HelpSet AmplitudeExtractionHS = new HelpSet(cl,helpSetURL);
            AmplitudeExtractionHB = AmplitudeExtractionHS.createHelpBroker();
        } catch (Exception e) {
            agent.showErrorDialog(QIWConstants.WARNING_DIALOG, Thread.currentThread().getStackTrace(),
                              "Error loading Java help", new String[]{""},
                              new String[] {"Contact workbench suport"});
        }

        initComponents();

        //Group the horizon radio buttons.
        ButtonGroup horizonGroup = new ButtonGroup();
        horizonGroup.add(edgeModeRadioButton);
        horizonGroup.add(pointModeRadioButton);

        //Group the target type radio buttons
        ButtonGroup targetTypeGroup = new ButtonGroup();
        targetTypeGroup.add(troughTypeRadioButton);
        targetTypeGroup.add(peakTypeRadioButton);
        targetTypeGroup.add(absTypeRadioButton);

        //add action handler to horizon mode radio buttons
        edgeModeRadioButton.addActionListener(new ButtonListener());
        edgeModeRadioButton.setActionCommand("2H");
        pointModeRadioButton.addActionListener(new ButtonListener());
        pointModeRadioButton.setActionCommand("1H");

        edgeModeHorizonParamsTableModel = horizonsParametersTable.getModel();
        addEdgeModeHorizonParamsListener(edgeModeHorizonParamsTableModel);
        landmark3DEdgeModeHorizonParamsTableModel = landmark3DHorizonsParametersTable.getModel();
        addLandmark3DEdgeModeHorizonParamsListener(landmark3DEdgeModeHorizonParamsTableModel);
        landmark2DEdgeModeHorizonParamsTableModel = landmark2DHorizonsParametersTable.getModel();
        addLandmark2DEdgeModeHorizonParamsListener(landmark2DEdgeModeHorizonParamsTableModel);
        landmark3DPointModeHorizonParamsTableModel = landmark3DHorizonParametersTable.getModel();
        addLandmark3DPointModeHorizonParamsListener(landmark3DPointModeHorizonParamsTableModel);
        landmark2DPointModeHorizonParamsTableModel = landmark2DHorizonParametersTable.getModel();
        addLandmark2DPointModeHorizonParamsListener(landmark2DPointModeHorizonParamsTableModel);
        //make column headers bold font
        JTableHeader header = horizonsParametersTable.getTableHeader();
        final Font boldFont = header.getFont().deriveFont(Font.BOLD);
        final TableCellRenderer headerRenderer = header.getDefaultRenderer();
        header.setDefaultRenderer( new TableCellRenderer() {
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = headerRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( boldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });
//      make column headers bold font
        JTableHeader lm3dheader = landmark3DHorizonsParametersTable.getTableHeader();
        final Font lm3dboldFont = lm3dheader.getFont().deriveFont(Font.BOLD);
        final TableCellRenderer lm3dheaderRenderer = header.getDefaultRenderer();
        lm3dheader.setDefaultRenderer( new TableCellRenderer() {
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = lm3dheaderRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( lm3dboldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });

        JTableHeader lm2dheader = landmark2DHorizonsParametersTable.getTableHeader();
        final Font lm2dboldFont = lm2dheader.getFont().deriveFont(Font.BOLD);
        final TableCellRenderer lm2dheaderRenderer = header.getDefaultRenderer();
        lm2dheader.setDefaultRenderer( new TableCellRenderer() {
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = lm2dheaderRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( lm2dboldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });

        //set cell color renderer.
        TableCellColorRenderer cellRenderer = new TableCellColorRenderer();
        horizonsParametersTable.setDefaultRenderer(String.class, cellRenderer);
        landmark3DHorizonsParametersTable.setDefaultRenderer(String.class, cellRenderer);
        landmark2DHorizonsParametersTable.setDefaultRenderer(String.class, cellRenderer);

        //make column headers bold font
        header = horizonParametersTable.getTableHeader();
        final TableCellRenderer headerRenderer1 = header.getDefaultRenderer();
        header.setDefaultRenderer( new TableCellRenderer() {
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = headerRenderer1.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( boldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });
//      make column headers bold font
        lm3dheader = landmark3DHorizonParametersTable.getTableHeader();
        final TableCellRenderer lm3dheaderRenderer1 = lm3dheader.getDefaultRenderer();
        lm3dheader.setDefaultRenderer( new TableCellRenderer() {
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = lm3dheaderRenderer1.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( lm3dboldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });
        lm2dheader = landmark2DHorizonParametersTable.getTableHeader();
        final TableCellRenderer lm2dheaderRenderer1 = lm2dheader.getDefaultRenderer();
        lm2dheader.setDefaultRenderer( new TableCellRenderer() {
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = lm2dheaderRenderer1.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( lm2dboldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });
        TableCellColorRenderer cellRenderer2 = new TableCellColorRenderer();
        horizonParametersTable.setDefaultRenderer(String.class, cellRenderer2);
        landmark3DHorizonParametersTable.setDefaultRenderer(String.class, cellRenderer2);
        landmark2DHorizonParametersTable.setDefaultRenderer(String.class, cellRenderer2);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        final Component comp = this;
        final AmplitudeExtraction parent = this;

        ampExtTabbedPane = new javax.swing.JTabbedPane();
//      handle switching tabs
        parametersPanel = new javax.swing.JPanel();
        inputParametersPanel = new javax.swing.JPanel();
        inputPanel = new javax.swing.JPanel();
        inputDataPanel = new javax.swing.JPanel();

        landmarkInRadioButton = new javax.swing.JRadioButton();
        //landmarkInputRadioButton.addActionListener(new ButtonListener());
        //landmarkInputRadioButton.setActionCommand("LandmarkInput");
        landmarkInRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //landmarkInRadioButtonActionPerformed(evt);
                if(landmarkInRadioButton.isSelected()){
                    if(validateLandmarkEnvironment() == false){
                        bhpsuInRadioButton.setSelected(true);
                        return;
                    }
                }

                try{
                    landmarkAgent = LandmarkServices.getInstance();
                }catch(UnsatisfiedLinkError e){
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(gui,e.getMessage() + ". Contact workbench support.",
                            "Linking Error",JOptionPane.WARNING_MESSAGE);
                    bhpsuInRadioButton.setSelected(true);
                    return;
                }catch(NoClassDefFoundError e){
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(gui,"Problem in getting the associated library for handling Landmark projects. Contact workbench support.",
                            "Linking Error",JOptionPane.WARNING_MESSAGE);
                    bhpsuInRadioButton.setSelected(true);
                    return;
                }catch(Exception e){
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(gui,"Problem in getting the associated library for handling Landmark projects. Contact workbench support.",
                            "Linking Error",JOptionPane.WARNING_MESSAGE);
                    bhpsuInRadioButton.setSelected(true);
                    return;
                }
                switch2LandmarkInput();

                }
        });

        bhpsuInRadioButton = new javax.swing.JRadioButton();
        bhpsuInRadioButton.setSelected(true);
        bhpsuInRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                landmarkOutListButton.setEnabled(true);
                landmarkOutProjectTextField.setEnabled(true);
                //landmarkOutProjectTextField.setText("");
                switch2BhpsuInput();
            }
        });


        ButtonGroup formatGroupIn = new ButtonGroup();
        formatGroupIn.add(landmarkInRadioButton);
        formatGroupIn.add(bhpsuInRadioButton);
        bhpsuOutputRadioButton = new javax.swing.JRadioButton();
        bhpsuOutputRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outputTypeRadioButtonActionPerformed1(evt);
            }
        });

        bhpsuOutputRadioButton.setSelected(true);
        landmarkOutputRadioButton = new javax.swing.JRadioButton();
        landmarkOutputRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outputTypeRadioButtonActionPerformed1(evt);
            }
        });

        ButtonGroup formatGroupOut = new ButtonGroup();
        formatGroupOut.add(bhpsuOutputRadioButton);
        formatGroupOut.add(landmarkOutputRadioButton);

        landmarkHorizonRadioButton = new javax.swing.JRadioButton();
        landmarkHorizonRadioButton.addActionListener(new ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                horizonInputTypeRadioButtonActionPerformed(evt);
            }
        });
        bhpsuHorizonRadioButton = new javax.swing.JRadioButton();
        bhpsuHorizonRadioButton.addActionListener(new ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                horizonInputTypeRadioButtonActionPerformed(evt);
            }
        });

        bhpsuHorizonDatasetRadioButton = new javax.swing.JRadioButton();
        bhpsuHorizonDatasetRadioButton.addActionListener(new ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                horizonInputTypeRadioButtonActionPerformed(evt);
            }
        });
        bhpsuHorizonRadioButton.setSelected(true);
        ButtonGroup horizonGroup = new ButtonGroup();
        horizonGroup.add(landmarkHorizonRadioButton);
        horizonGroup.add(bhpsuHorizonRadioButton);
        horizonGroup.add(bhpsuHorizonDatasetRadioButton);

        inputCubeLabel = new javax.swing.JLabel();
        inputCubeTextField = new DropTextField(DnDConstants.ACTION_COPY, this, "InputCube");
        inputCubeTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputCubeTextFieldActionPerformed(evt);
            }
        });
        inputCubeBrowseButton = new javax.swing.JButton();
        rangeLabel = new javax.swing.JLabel();
        cdpFromLabel = new javax.swing.JLabel();
        epFromLabel = new javax.swing.JLabel();
        cdpFromTextField = new javax.swing.JTextField();
        epFromTextField = new javax.swing.JTextField();
        cdpToLabel = new javax.swing.JLabel();
        epToLabel = new javax.swing.JLabel();
        cdpToTextField = new javax.swing.JTextField();
        epToTextField = new javax.swing.JTextField();
        cdpByLabel = new javax.swing.JLabel();
        cdpByLabel1 = new javax.swing.JLabel();
        cdpByTextField = new javax.swing.JTextField();
        epByTextField = new javax.swing.JTextField();
        entireCubeRangeButton = new javax.swing.JButton();
        pickRangeFromViewerButton = new javax.swing.JButton();
        horizonsPanel = new javax.swing.JPanel();
        horizonDataPanel = new javax.swing.JPanel();
        edgeModeRadioButton = new javax.swing.JRadioButton();
        pointModeRadioButton = new javax.swing.JRadioButton();
        topHorizonLabel = new javax.swing.JLabel();
        landmark3DTopHorizonTextField = new javax.swing.JTextField();
        landmark2DTopHorizonTextField = new javax.swing.JTextField();
        bhpsuTopHorizonTextField  = new DropTextField(DnDConstants.ACTION_COPY, this, "TopHorizon");
        topHorizonBrowseButton = new javax.swing.JButton();
        bottomHorizonLabel = new javax.swing.JLabel();
        landmark3DBottomHorizonTextField = new javax.swing.JTextField();
        landmark2DBottomHorizonTextField = new javax.swing.JTextField();
        bhpsuBottomHorizonTextField = new DropTextField(DnDConstants.ACTION_COPY, this, "BottomHorizon");
        bottomHorizonBrowseButton = new javax.swing.JButton();
        horizonsParametersPanel = new javax.swing.JPanel();
        landmark3DHorizonsParametersPanel = new javax.swing.JPanel();
        landmark2DHorizonsParametersPanel = new javax.swing.JPanel();
        horizonsParametersScrollPane = new javax.swing.JScrollPane();
        landmark3DHorizonsParametersScrollPane = new javax.swing.JScrollPane();
        landmark2DHorizonsParametersScrollPane = new javax.swing.JScrollPane();
        horizonsParametersTable = new javax.swing.JTable();
        clearButton = new javax.swing.JButton("Clear");

        clearButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectedLines = new Vector();
                selectedSeismic2DLineList.setListData(selectedLines);
                selectedLineMap.clear();
            }
        });
        horizonsParametersTable.setFont(new java.awt.Font("Arial", 0, 11));
        horizonsParametersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"Top", "", "", ""},
                {"Bottom", "", "", ""}
            },
            new String [] {
                "Horizon", TABLE_HEADING_BIAS, TABLE_HEADING_SEARCH_ABOVE, TABLE_HEADING_SEARCH_BELOW
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });

        //HorizonParamJTextField hptf = new HorizonParamJTextField(horizonsParametersTable.getModel());
        //HorizonParamCellEditor hpce = new HorizonParamCellEditor(hptf);
        //horizonsParametersTable.setDefaultEditor(String.class,hpce);

        horizonsParametersTable.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e) {
                javax.swing.table.DefaultTableModel tm = (javax.swing.table.DefaultTableModel)horizonsParametersTable.getModel();
                int col = horizonsParametersTable.getEditingColumn();
                int row = horizonsParametersTable.getEditingRow();

                Component comp = horizonsParametersTable.getEditorComponent();
                if(comp instanceof JTextField){
                    JTextField tf = (JTextField)comp;
                    tm.setValueAt(tf.getText(), row, col);
                    if(row == 0 && col == 1){
                        topHorizonBias = tf.getText();
                    }else if(row == 0 && col == 2){
                        topHorizonAbove = tf.getText();
                    }else if(row == 0 && col == 3){
                        topHorizonBelow = tf.getText();
                    }else if(row == 1 && col == 1){
                        bottomHorizonBias = tf.getText();
                    }else if(row == 1 && col == 2){
                        bottomHorizonAbove = tf.getText();
                    }else if(row == 1 && col == 3){
                        bottomHorizonBelow = tf.getText();;
                    }
                }
                //tm.fireTableDataChanged();
            }
        });

        landmark3DHorizonsParametersTable = new javax.swing.JTable();
        landmark3DHorizonsParametersTable.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e) {
                javax.swing.table.DefaultTableModel tm = (javax.swing.table.DefaultTableModel)landmark3DHorizonsParametersTable.getModel();
                int col = landmark3DHorizonsParametersTable.getEditingColumn();
                int row = landmark3DHorizonsParametersTable.getEditingRow();

                Component comp = landmark3DHorizonsParametersTable.getEditorComponent();
                if(comp instanceof JTextField){
                    JTextField tf = (JTextField)comp;
                    tm.setValueAt(tf.getText(), row, col);
                    if(row == 0 && col == 1){
                        landmark3DTopHorizonBias = tf.getText();
                    }else if(row == 0 && col == 2){
                        landmark3DTopHorizonAbove = tf.getText();
                    }else if(row == 0 && col == 3){
                        landmark3DTopHorizonBelow = tf.getText();
                    }else if(row == 1 && col == 1){
                        landmark3DBottomHorizonBias = tf.getText();
                    }else if(row == 1 && col == 2){
                        landmark3DBottomHorizonAbove = tf.getText();
                    }else if(row == 1 && col == 3){
                        landmark3DBottomHorizonBelow = tf.getText();;
                    }
                }
                //tm.fireTableDataChanged();
            }
        });
        landmark3DHorizonsParametersTable.setFont(new java.awt.Font("Arial", 0, 11));
        landmark3DHorizonsParametersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"Top", "", "", ""},
                {"Bottom", "", "", ""}
            },
            new String [] {
                "Horizon", TABLE_HEADING_BIAS, TABLE_HEADING_SEARCH_ABOVE, TABLE_HEADING_SEARCH_BELOW
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        landmark2DHorizonsParametersTable = new javax.swing.JTable();
        landmark2DHorizonsParametersTable.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e) {
                javax.swing.table.DefaultTableModel tm = (javax.swing.table.DefaultTableModel)landmark2DHorizonsParametersTable.getModel();
                int col = landmark2DHorizonsParametersTable.getEditingColumn();
                int row = landmark2DHorizonsParametersTable.getEditingRow();

                Component comp = landmark2DHorizonsParametersTable.getEditorComponent();
                if(comp instanceof JTextField){
                    JTextField tf = (JTextField)comp;
                    tm.setValueAt(tf.getText(), row, col);
                    if(row == 0 && col == 1){
                        landmark2DTopHorizonBias = tf.getText();
                    }else if(row == 0 && col == 2){
                        landmark2DTopHorizonAbove = tf.getText();
                    }else if(row == 0 && col == 3){
                        landmark2DTopHorizonBelow = tf.getText();
                    }else if(row == 1 && col == 1){
                        landmark2DBottomHorizonBias = tf.getText();
                    }else if(row == 1 && col == 2){
                        landmark2DBottomHorizonAbove = tf.getText();
                    }else if(row == 1 && col == 3){
                        landmark2DBottomHorizonBelow = tf.getText();;
                    }
                }
                //tm.fireTableDataChanged();
            }
        });
        landmark2DHorizonsParametersTable.setFont(new java.awt.Font("Arial", 0, 11));
        landmark2DHorizonsParametersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"Top", "", "", ""},
                {"Bottom", "", "", ""}
            },
            new String [] {
                "Horizon", TABLE_HEADING_BIAS, TABLE_HEADING_SEARCH_ABOVE, TABLE_HEADING_SEARCH_BELOW
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });

        targetTypePanel = new javax.swing.JPanel();
        landmark3DTargetTypePanel = new javax.swing.JPanel();
        landmark2DTargetTypePanel = new javax.swing.JPanel();
        troughTypeRadioButton = new javax.swing.JRadioButton();
        troughTypeRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(landmarkInRadioButton.isSelected()){
                    if(landmark3DRadioButton.isSelected())
                        landmark3DHorizonTargetType = 1;
                    else if(landmark2DRadioButton.isSelected())
                        landmark2DHorizonTargetType = 1;
                }
                else // bhpsu
                    bhpsuHorizonTargetType = 1;
            }
        });
        peakTypeRadioButton = new javax.swing.JRadioButton();
        peakTypeRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(landmarkInRadioButton.isSelected()){
                    if(landmark3DRadioButton.isSelected())
                        landmark3DHorizonTargetType = 2;
                    else if(landmark2DRadioButton.isSelected())
                        landmark2DHorizonTargetType = 2;
                }
                else // bhpsu
                    bhpsuHorizonTargetType = 2;
            }
        });
        absTypeRadioButton = new javax.swing.JRadioButton();
        absTypeRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(landmarkInRadioButton.isSelected()){
                    if(landmark3DRadioButton.isSelected())
                        landmark3DHorizonTargetType = 3;
                    else if(landmark2DRadioButton.isSelected())
                        landmark2DHorizonTargetType = 3;
                }
                else // bhpsu
                    bhpsuHorizonTargetType = 3;
            }
        });
        outputPanel = new javax.swing.JPanel();
        landmarkOutProjectLabel = new javax.swing.JLabel();
        landmarkOutProjectTextField = new javax.swing.JTextField();
        landmark3DOutProjectTextField = new javax.swing.JTextField();
        landmark2DOutProjectTextField = new javax.swing.JTextField();
        landmarkOutListButton = new javax.swing.JButton();
        landmarkOutListButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(landmarkAgent == null)
                    landmarkAgent = LandmarkServices.getInstance();
                java.awt.Dialog dialog = new ProjectSelectDialog(landmarkAgent,parent,true,"landmark_out");
                dialog.setLocationRelativeTo(JOptionPane.getFrameForComponent(comp));
                dialog.setVisible(true);
            }
        });
        outputCubeLabel = new javax.swing.JLabel();
        outputCubeTextField = new javax.swing.JTextField();
        landmark3DOutputCubeTextField = new javax.swing.JTextField();
        landmark2DOutputCubeTextField = new javax.swing.JTextField();
        landmarkOutputCubeTextField = new javax.swing.JTextField();
        outputHorizonsLabel = new javax.swing.JLabel();
        outputHorizonsTextField = new javax.swing.JTextField();
        landmark3DOutputHorizonsTextField = new javax.swing.JTextField();
        landmark2DOutputHorizonsTextField = new javax.swing.JTextField();
        landmarkOutputHorizonsTextField = new javax.swing.JTextField();
        horizonLabel = new javax.swing.JLabel();
        bhpsuHorizonTextField = new DropTextField(DnDConstants.ACTION_COPY, this, "bhpsuHorizon");
        landmark3DHorizonTextField = new javax.swing.JTextField();
        landmark2DHorizonTextField = new javax.swing.JTextField();
        horizonBrowseButton = new javax.swing.JButton();
        horizonParametersPanel = new javax.swing.JPanel();
        landmark3DHorizonParametersPanel = new javax.swing.JPanel();
        landmark2DHorizonParametersPanel = new javax.swing.JPanel();
        horizonParametersScrollPane = new javax.swing.JScrollPane();
        landmark3DHorizonParametersScrollPane = new javax.swing.JScrollPane();
        landmark2DHorizonParametersScrollPane = new javax.swing.JScrollPane();
        horizonParametersTable = new javax.swing.JTable();
        horizonParametersTable.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e) {
                javax.swing.table.DefaultTableModel tm = (javax.swing.table.DefaultTableModel)horizonParametersTable.getModel();
                int col = horizonParametersTable.getEditingColumn();
                int row = horizonParametersTable.getEditingRow();
                Component comp = horizonParametersTable.getEditorComponent();
                if(comp instanceof JTextField){
                    JTextField tf = (JTextField)comp;
                    tm.setValueAt(tf.getText(), row, col);
                    if(row == 0 && col == 0){
                        horizonBias = tf.getText();
                    }else if(row == 0 && col == 1){
                        horizonAbove = tf.getText();
                    }else if(row == 0 && col == 2){
                        horizonBelow = tf.getText();
                    }
                }
                //tm.fireTableDataChanged();
            }
        });
        horizonParametersTable.setFont(new java.awt.Font("Arial", 0, 11));
        horizonParametersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "", ""}
            },
            new String [] {
                TABLE_HEADING_BIAS, TABLE_HEADING_SEARCH_ABOVE, TABLE_HEADING_SEARCH_BELOW
            }
        ));
        landmark3DHorizonParametersTable = new javax.swing.JTable();
        landmark3DHorizonParametersTable.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e) {
                javax.swing.table.DefaultTableModel tm = (javax.swing.table.DefaultTableModel)landmark3DHorizonParametersTable.getModel();
                int col = landmark3DHorizonParametersTable.getEditingColumn();
                int row = landmark3DHorizonParametersTable.getEditingRow();

                Component comp = landmark3DHorizonParametersTable.getEditorComponent();
                if(comp instanceof JTextField){
                    JTextField tf = (JTextField)comp;
                    tm.setValueAt(tf.getText(), row, col);
                    if(row == 0 && col == 0){
                        landmark3DHorizonBias = tf.getText();
                    }else if(row == 0 && col == 1){
                        landmark3DHorizonAbove = tf.getText();
                    }else if(row == 0 && col == 2){
                        landmark3DHorizonBelow = tf.getText();
                    }
                }
                //tm.fireTableDataChanged();
            }
        });
        landmark3DHorizonParametersTable.setFont(new java.awt.Font("Arial", 0, 11));
        landmark3DHorizonParametersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                    {"", "", ""}
            },
            new String [] {
                TABLE_HEADING_BIAS, TABLE_HEADING_SEARCH_ABOVE, TABLE_HEADING_SEARCH_BELOW
            }
        ));
        landmark2DHorizonParametersTable = new javax.swing.JTable();
        landmark2DHorizonParametersTable.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e) {
                javax.swing.table.DefaultTableModel tm = (javax.swing.table.DefaultTableModel)landmark2DHorizonParametersTable.getModel();
                int col = landmark2DHorizonParametersTable.getEditingColumn();
                int row = landmark2DHorizonParametersTable.getEditingRow();

                Component comp = landmark2DHorizonParametersTable.getEditorComponent();
                if(comp instanceof JTextField){
                    JTextField tf = (JTextField)comp;
                    tm.setValueAt(tf.getText(), row, col);
                    if(row == 0 && col == 0){
                        landmark2DHorizonBias = tf.getText();
                    }else if(row == 0 && col == 1){
                        landmark2DHorizonAbove = tf.getText();
                    }else if(row == 0 && col == 2){
                        landmark2DHorizonBelow = tf.getText();
                    }
                }
                //tm.fireTableDataChanged();
            }
        });
        landmark2DHorizonParametersTable.setFont(new java.awt.Font("Arial", 0, 11));
        landmark2DHorizonParametersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "", ""}
            },
            new String [] {
                TABLE_HEADING_BIAS, TABLE_HEADING_SEARCH_ABOVE, TABLE_HEADING_SEARCH_BELOW
            }
        ));
        statusPanel = new javax.swing.JPanel();
        statusLabel = new javax.swing.JLabel();
        stateLabel = new javax.swing.JLabel();
        checkStatusButton = new javax.swing.JButton();
        detailsPanel = new javax.swing.JPanel();
        detailsSplitPane = new javax.swing.JSplitPane();
        stderrScrollPane = new javax.swing.JScrollPane();
        stderrTextArea = new javax.swing.JTextArea();
        stdoutScrollPane = new javax.swing.JScrollPane();
        stdoutTextArea = new javax.swing.JTextArea();
        cancelRunButton = new javax.swing.JButton();
        runScriptButton = new javax.swing.JButton();
        writeScriptButton = new javax.swing.JButton();
        displayResultsButton = new javax.swing.JButton();
        fileSummaryPanel = new javax.swing.JPanel();
        fileSummaryScrollPane = new javax.swing.JScrollPane();
        fileSummaryTextArea = new javax.swing.JTextArea();
        ampExtMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        saveMenuItem = new javax.swing.JMenuItem();
        saveAsMenuItem = new javax.swing.JMenuItem();
        saveQuitMenuItem = new javax.swing.JMenuItem();
        quitMenuItem = new javax.swing.JMenuItem();
        fileSeparator = new javax.swing.JSeparator();
        importMenuItem = new javax.swing.JMenuItem();
        exportMenuItem = new javax.swing.JMenuItem();
        executeMenu = new javax.swing.JMenu();
        writeScriptMenuItem = new javax.swing.JMenuItem();
        runMenuItem = new javax.swing.JMenuItem();
        cancelRunMenuItem = new javax.swing.JMenuItem();
        statusMenuItem = new javax.swing.JMenuItem();
        displayResultsMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        aboutMenuItem = new javax.swing.JMenuItem();



        landmarkIn2D3DDataPanel = new javax.swing.JPanel();
        seismicFileLabel = new javax.swing.JLabel();
        seismicLinesPanel = new javax.swing.JPanel();
        selectedLineListScrollPane = new javax.swing.JScrollPane();
        selectedSeismic2DLineList = new javax.swing.JList();
        seismic2DLineList = new javax.swing.JList();
        MouseListener mouseListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    if(e.getSource().equals(selectedSeismic2DLineList))
                        leftArrowButton.doClick();
                    else if(e.getSource().equals(seismic2DLineList))
                        rightArrowButton.doClick();
                }
                if (e.getClickCount() == 1) {
                    if(e.getSource().equals(selectedSeismic2DLineList)){
                        String s = (String)selectedSeismic2DLineList.getSelectedValue();
                        if(s != null){
                            selectedLineName = s;
                            LineInfo li = selectedLineMap.get((String)selectedSeismic2DLineList.getSelectedValue());
                            if(li != null){
                                shotpointTextField.setText(String.valueOf(li.getMinShotPoint()));
                                shotpointToTextField.setText(String.valueOf(li.getMaxShotPoint()));
                                shotpointByTextField.setText(String.valueOf(li.getIncShotPoint()));
                            }
                        }
                    }
                }
            }
        };
        selectedSeismic2DLineList.addMouseListener(mouseListener);
        seismic2DLineList.addMouseListener(mouseListener);
        selectedLinesLabel = new javax.swing.JLabel();
        rightArrowButton = new javax.swing.JButton();
        rightArrowButton.addActionListener(new ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Object[] objs = seismic2DLineList.getSelectedValues();
                if(objs != null){
                    Vector v = new Vector();
                    ListModel model = selectedSeismic2DLineList.getModel();
                    for(int i = 0; model != null && i < model.getSize(); i++){
                        v.add(model.getElementAt(i));
                    }
                    if(landmarkAgent == null)
                        landmarkAgent = LandmarkServices.getInstance();
                    for(int i = 0; i < objs.length; i++){
                        if(!v.contains(objs[i])){
                            v.add(objs[i]);
                            float range[] = landmarkAgent.getLandmarkProjectHandler().getShotpointRangeInfoBy2DLine
                            (landmarkIn2DProjectTextField.getText().trim(), (String)objs[i]);
                            LineInfo li = new LineInfo(landmarkIn2DProjectTextField.getText().trim(),(String)(objs[i]),(int)range[0], range[1], range[2], range[3]);
                            li.setMinShotPoint(range[1]);
                            li.setMaxShotPoint(range[2]);
                            li.setIncShotPoint(range[3]);
                            selectedLineMap.put((String)objs[i],li);
                        }
                    }
                    Collections.sort(v);
                    selectedSeismic2DLineList.setListData(v);
                    selectedLines = v;
                }
            }
        });
        leftArrowButton = new javax.swing.JButton();
        leftArrowButton.addActionListener(new ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Object[] objs = selectedSeismic2DLineList.getSelectedValues();
                if(objs != null){
                    Vector v = new Vector();
                    ListModel model = selectedSeismic2DLineList.getModel();
                    for(int i = 0; model != null && i < model.getSize(); i++){
                        v.add(model.getElementAt(i));
                    }
                    for(int i = 0; i < objs.length; i++){
                        v.remove(objs[i]);
                        selectedLineMap.remove((String)objs[i]);
                    }
                    selectedSeismic2DLineList.setListData(v);
                }
            }
        });
        landmarkIn2D3DDataPanel = new javax.swing.JPanel();
        outputDataPanel = new javax.swing.JPanel();
        list2DProjectsButton = new javax.swing.JButton();
        list2DProjectsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(landmarkAgent == null)
                    landmarkAgent = LandmarkServices.getInstance();
                java.awt.Dialog dialog = new ProjectSelectDialog(landmarkAgent,parent,true,"landmark_in");
                dialog.setLocationRelativeTo(JOptionPane.getFrameForComponent(comp));
                dialog.setVisible(true);

            }
        });
        seismic2DFileComboBox = new javax.swing.JComboBox();
        seismic2DFileComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                javax.swing.JComboBox cb = (javax.swing.JComboBox) e.getSource();
                int idx =  cb.getSelectedIndex();
                if(idx == -1 || cb.getSelectedItem().equals("Select one")){
                    seismic2DLineList.setListData(new Vector());
                    selectedLines = new Vector();
                    selectedSeismic2DLineList.setListData(selectedLines);
                    resetShotpointInfo();
                    return;
                }
                if(idx == selectedProcessLevelIndex)
                    return;
                selectedProcessLevelIndex= idx;

                String item = (String)cb.getSelectedItem();
                List<String> list = landmark2DfileLinesMap.get(item);
                Vector<String> v;
                if(list == null)
                    v = new Vector<String>();
                else
                    v = new Vector<String>(list);
                seismic2DLineList.setListData(v);
                selectedLines = new Vector();
                selectedSeismic2DLineList.setListData(selectedLines);
                resetShotpointInfo();
            }
        });

        lineListScrollPane = new javax.swing.JScrollPane();
        linesLabel = new javax.swing.JLabel();
        linesListButton = new javax.swing.JButton();
        seismicFileLabel = new javax.swing.JLabel();
        shotpointLabel = new javax.swing.JLabel();
        entireVolume2DButton = new javax.swing.JButton();
        entireVolume2DButton.addActionListener(new ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(selectedLineName != null && selectedLineName.trim().length() > 0){
                    LineInfo li = selectedLineMap.get((String)selectedSeismic2DLineList.getSelectedValue());
                    shotpointTextField.setText(String.valueOf(li.getMinShotPoint_def()));
                    shotpointToTextField.setText(String.valueOf(li.getMaxShotPoint_def()));
                    shotpointByTextField.setText(String.valueOf(li.getIncShotPoint_def()));
                    shotpointTextField.setText(String.valueOf(li.getMinShotPoint_def()));
                    shotpointToTextField.setText(String.valueOf(li.getMaxShotPoint_def()));
                    shotpointByTextField.setText(String.valueOf(li.getIncShotPoint_def()));
                    li.setMinShotPoint(li.getMinShotPoint_def());
                    li.setMaxShotPoint(li.getMaxShotPoint_def());
                    li.setIncShotPoint(li.getIncShotPoint_def());
                    selectedLineMap.put(selectedLineName, li);
                }
            }
        });
        select2DButton = new javax.swing.JButton();
        seismicFileListButton = new javax.swing.JButton();
        landmarkIn2DProjectTextField = new javax.swing.JTextField();
        linesInTextField = new javax.swing.JTextField();
        seismicFileTextField = new javax.swing.JTextField();
        shotpointToTextField = new javax.swing.JTextField();
        shotpointToTextField.addFocusListener(new FocusAdapter() {
              public void focusLost(FocusEvent e) {
                  float num;
                  try{
                      num = Float.valueOf(shotpointToTextField.getText());
                  }catch(Exception ex){
                      JOptionPane.showMessageDialog(comp, "The maxmum shotpoint "
                              + shotpointToTextField.getText() + " is not a valid number.",
                              "Number Format Exception", JOptionPane.WARNING_MESSAGE);
                      return;
                  }
                  //String s = (String)selectedSeismic2DLineList.getSelectedValue();
                  if(selectedLineName != null && selectedLineName.trim().length() > 0){
                      LineInfo li = selectedLineMap.get(selectedLineName);
                      li.setMaxShotPoint(num);
                      selectedLineMap.put(selectedLineName, li);
                  }
              }
        });
        shotpointTextField = new javax.swing.JTextField();
        shotpointTextField.addFocusListener(new FocusAdapter() {
              public void focusLost(FocusEvent e) {
                  float num;
                  try{
                      num = Float.valueOf(shotpointTextField.getText());
                  }catch(Exception ex){
                      JOptionPane.showMessageDialog(comp, "The minimum shotpoint "
                              + shotpointTextField.getText() + " is not a valid number.",
                              "Number Format Exception", JOptionPane.WARNING_MESSAGE);
                      return;
                  }
                  //String s = (String)selectedSeismic2DLineList.getSelectedValue();
                  logger.info("selected line=" + selectedLineName);
                  if(selectedLineName != null && selectedLineName.trim().length() > 0){
                      LineInfo li = selectedLineMap.get(selectedLineName);
                      li.setMinShotPoint(num);
                      selectedLineMap.put(selectedLineName, li);
                  }
              }
        });
        shotpointByTextField = new javax.swing.JTextField();
        shotpointByTextField.addFocusListener(new FocusAdapter() {
              public void focusLost(FocusEvent e) {
                  float num;
                  try{
                      num = Float.valueOf(shotpointByTextField.getText());
                  }catch(Exception ex){
                      JOptionPane.showMessageDialog(comp, "The shotpoint increment "
                              + shotpointByTextField.getText() + " is not a valid number.",
                              "Number Format Exception", JOptionPane.WARNING_MESSAGE);
                      return;
                  }
                  //String s = (String)selectedSeismic2DLineList.getSelectedValue();
                  if(selectedLineName != null && selectedLineName.trim().length() > 0){
                      LineInfo li = selectedLineMap.get(selectedLineName);
                      if(li != null){
                          li.setIncShotPoint(num);
                          selectedLineMap.put(selectedLineName, li);
                      }
                  }
              }
        });
        landmark3DRadioButton = new javax.swing.JRadioButton();
        landmark3DRadioButton.setSelected(true);
        landmark3DRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                landmark3DOutProjectTextField.setEnabled(true);
                landmark3DOutProjectTextField.setText("");
                switch2LandmarkInput();
            }
        });
        landmark2DRadioButton = new javax.swing.JRadioButton();
        landmark2DRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                landmark2DOutProjectTextField.setEnabled(true);
                landmark2DOutProjectTextField.setText("");
                switch2LandmarkInput();
            }
        });
        ButtonGroup ptypeGroup = new ButtonGroup();
        ptypeGroup.add(landmark3DRadioButton);
        ptypeGroup.add(landmark2DRadioButton);

        landmarkInProjectTextField = new javax.swing.JTextField();
        volumeTextField = new javax.swing.JTextField();
        landmarkProjectLabel = new javax.swing.JLabel();
        listProjectsButton = new javax.swing.JButton();

        listProjectsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(landmarkAgent == null)
                    landmarkAgent = LandmarkServices.getInstance();
                java.awt.Dialog dialog = new ProjectSelectDialog(landmarkAgent,parent,true,"landmark_in");
                dialog.setLocationRelativeTo(JOptionPane.getFrameForComponent(comp));
                dialog.setVisible(true);
            }
        });
        volumeLabel = new javax.swing.JLabel();
        listVolumesButton = new javax.swing.JButton();
        listVolumesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //new ProjectSelectDialog((java.awt.Frame)SwingUtilities.getWindowAncestor(comp),true).setVisible(true);
                if(landmarkInProjectTextField.getText() == null || landmarkInProjectTextField.getText().trim().length() == 0){
                    JOptionPane.showMessageDialog(comp, "Please choose a SeisWorks project before retrieving volume information.",
                            "Invalid data entry", JOptionPane.WARNING_MESSAGE);
                    listProjectsButton.requestFocus();
                    return;
                }
                if(landmarkAgent == null)
                    landmarkAgent = LandmarkServices.getInstance();
                java.awt.Dialog dialog = new VolumeSelectDialog(landmarkAgent,parent,true);
                dialog.setLocationRelativeTo(JOptionPane.getFrameForComponent(comp));
                dialog.setVisible(true);
            }
        });
        landmarkRangeLabel = new javax.swing.JLabel();
        rangeLinesLabel = new javax.swing.JLabel();
        linesToLabel = new javax.swing.JLabel();
        linesByLabel = new javax.swing.JLabel();
        tracesLabel = new javax.swing.JLabel();
        tracesToLabel = new javax.swing.JLabel();
        tracesByLabel = new javax.swing.JLabel();
        entireVolumeButton = new javax.swing.JButton();
        entireVolumeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(lineTraceArray == null)
                    return;
                setLineTraceDefaultValue(lineTraceArray);
            }
        });
        selectButton = new javax.swing.JButton();
        tracesTextField = new javax.swing.JTextField();
        tracesToTextField = new javax.swing.JTextField();
        tracesByTextField = new javax.swing.JTextField();
        linesTextField = new javax.swing.JTextField();
        linesToTextField = new javax.swing.JTextField();
        linesByTextField = new javax.swing.JTextField();
        /*
        exportToBhpsuCheckBox = new javax.swing.JCheckBox("Export to BHP-SU");
        lm2BhpsuPrefixLabel = new javax.swing.JLabel("Prefix");
        lm2BhpsuPrefixTextField = new javax.swing.JTextField();
        lm2BhpsuPrefixTextField.setEnabled(false);
        exportToBhpsuCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(exportToBhpsuCheckBox.isSelected()){
                    lm2BhpsuPrefixTextField.setEnabled(true);
                }else{
                    lm2BhpsuPrefixTextField.setEnabled(false);
                }
            }
        });
        */
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Amplitude Extraction Plugin");
        ampExtTabbedPane.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(153, 0, 0), new java.awt.Color(153, 0, 153), new java.awt.Color(153, 153, 255), new java.awt.Color(0, 0, 0)));
        ampExtTabbedPane.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                ampExtTabbedPaneStateChanged(evt);
            }
        });

        inputPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Input", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));
        inputCubeLabel.setText("Input Cube");

        inputCubeBrowseButton.setText("Browse");
        inputCubeBrowseButton.setActionCommand(AmpExtConstants.AMP_EXT_SELECT_INPUT);
        inputCubeBrowseButton.addActionListener(new ButtonListener());

        rangeLabel.setText("Range:");

        cdpFromLabel.setText("cdp");

        epFromLabel.setText("ep");

        cdpToLabel.setText("to");

        epToLabel.setText("to");

        cdpByLabel.setText("by");

        cdpByLabel1.setText("by");

        entireCubeRangeButton.setText("Entire Dataset");
        entireCubeRangeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                entireCubeRangeButtonActionPerformed(evt);
            }
        });

        pickRangeFromViewerButton.setText("Pick from Viewer");
        formBhpsuInputDataPanel();
        formInputPanel();

        //form Horizons panel, edge mode
        formEdgeModeHorizonPanel();
        formHorizonsPanel();
        troughTypeRadioButton.setSelected(true);
        troughTypeRadioButton.setText("Trough");
        troughTypeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        troughTypeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        peakTypeRadioButton.setText("Peak");
        peakTypeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        peakTypeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        absTypeRadioButton.setText("Abs");
        absTypeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        absTypeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));


        //formOutputDataPanel();
        //formOutputPanel();

        // form Horizons panel, point mode
        layoutInputParametersTabbedPane();
        ampExtTabbedPane.addTab(TN_SEISMIC_DATA_INPUT, inputParametersPanel);
        processPanel = new ProcessPanel(this);
        ampExtTabbedPane.addTab(TN_SEISMIC_DATA_PROCESS, processPanel);
        // lay out Parameters tabbed pane
        formTargetPanel(targetTypePanel,bhpsuHorizonTargetType);
        layoutParametersTabbedPane(targetTypePanel);
        ampExtTabbedPane.addTab(TN_HORIZON_DATA_INPUT, parametersPanel);


        outputDataPanel = new javax.swing.JPanel();
        outputPanel = new javax.swing.JPanel();
        formOutputDataPanel();
        formOutputPanel();


        ampExtTabbedPane.addTab(TN_OUTPUTS, outputPanel);


        statusLabel.setText("Status:");
//        stateLabel.setText("Running");

        checkStatusButton.setText("Check Status");
        checkStatusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkStatusButtonActionPerformed(evt);
            }
        });

        detailsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Status Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12)));
        detailsSplitPane.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        detailsSplitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        detailsSplitPane.setOneTouchExpandable(true);
        stderrTextArea.setColumns(20);
        stderrTextArea.setRows(5);
        stderrScrollPane.setViewportView(stderrTextArea);

        detailsSplitPane.setTopComponent(stderrScrollPane);

        stdoutTextArea.setColumns(20);
        stdoutTextArea.setRows(5);
        stdoutScrollPane.setViewportView(stdoutTextArea);

        detailsSplitPane.setRightComponent(stdoutScrollPane);

        org.jdesktop.layout.GroupLayout detailsPanelLayout = new org.jdesktop.layout.GroupLayout(detailsPanel);
        detailsPanel.setLayout(detailsPanelLayout);
        detailsPanelLayout.setHorizontalGroup(
            detailsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(detailsSplitPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 697, Short.MAX_VALUE)
        );
        detailsPanelLayout.setVerticalGroup(
            detailsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, detailsSplitPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 440, Short.MAX_VALUE)
        );

        cancelRunButton.setText("Cancel Run");
        cancelRunButton.setToolTipText("Cancel script execution");
        cancelRunButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelRunButtonActionPerformed(evt);
            }
        });

        displayResultsButton.setText("Display Results");
        displayResultsButton.setToolTipText("Display script results in qiViewer");
        displayResultsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                displayResultsButtonActionPerformed(evt);
            }
        });

        runScriptButton.setText("Run Script");
        runScriptButton.setToolTipText("Save and run generated script");
        runScriptButton.addActionListener(new ButtonListener());
        runScriptButton.setActionCommand(AmpExtConstants.AMP_EXT_EXECUTE_SCRIPT);
/*
        runScriptButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runScriptButtonActionPerformed(evt);
            }
        });
*/
        writeScriptButton.setText("Write Script");
        writeScriptButton.setToolTipText("Save generated script");
        writeScriptButton.addActionListener(new ButtonListener());
        writeScriptButton.setActionCommand(AmpExtConstants.AMP_EXT_SAVE_SCRIPT);
/*
        writeScriptButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                writeScriptButtonActionPerformed(evt);
            }
        });
*/
        fileSummaryPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "File Summary", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12)));
        fileSummaryTextArea.setColumns(20);
        fileSummaryTextArea.setRows(5);
        fileSummaryScrollPane.setViewportView(fileSummaryTextArea);

        org.jdesktop.layout.GroupLayout fileSummaryPanelLayout = new org.jdesktop.layout.GroupLayout(fileSummaryPanel);
        fileSummaryPanel.setLayout(fileSummaryPanelLayout);
        fileSummaryPanelLayout.setHorizontalGroup(
            fileSummaryPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(fileSummaryScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 697, Short.MAX_VALUE)
        );
        fileSummaryPanelLayout.setVerticalGroup(
            fileSummaryPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(fileSummaryPanelLayout.createSequentialGroup()
                .add(fileSummaryScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 100, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                )
        );

        org.jdesktop.layout.GroupLayout statusPanelLayout = new org.jdesktop.layout.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(statusPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, detailsPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(statusPanelLayout.createSequentialGroup()
                        .add(statusLabel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(stateLabel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 263, Short.MAX_VALUE)
                        .add(writeScriptButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(runScriptButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(displayResultsButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(checkStatusButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(cancelRunButton))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, fileSummaryPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(statusPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(statusLabel)
                    .add(stateLabel)
                    .add(cancelRunButton)
                    .add(checkStatusButton)
                    .add(displayResultsButton)
                    .add(runScriptButton)
                    .add(writeScriptButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(detailsPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(fileSummaryPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        ampExtTabbedPane.addTab(TN_JOB_STATUS, statusPanel);

        fileMenu.setMnemonic('F');
        fileMenu.setText("File");



        importMenuItem.setMnemonic('I');
        importMenuItem.setText("Import");
        importMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(importMenuItem);

        exportMenuItem.setMnemonic('E');
        exportMenuItem.setText("Export");
        exportMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exportMenuItem);
        fileMenu.add(fileSeparator);

        quitMenuItem.setMnemonic('Q');
        quitMenuItem.setText("Quit");
        quitMenuItem.setToolTipText("Quit without saving state");
        quitMenuItem.addActionListener(new ButtonListener());
        quitMenuItem.setActionCommand("Quit");
        fileMenu.add(quitMenuItem);

        saveMenuItem.setMnemonic('S');
        saveMenuItem.setText("Save");
        saveMenuItem.setToolTipText("Save state");
        saveMenuItem.addActionListener(new ButtonListener());
        saveMenuItem.setActionCommand("SaveState");
/*
        saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuItemActionPerformed(evt);
            }
        });
*/
        fileMenu.add(saveMenuItem);

        saveAsMenuItem.setMnemonic('A');
        saveAsMenuItem.setText("Save As");
        saveAsMenuItem.setToolTipText("Save state as a clone.");
        saveAsMenuItem.addActionListener(new ButtonListener());
        saveAsMenuItem.setActionCommand("SaveAsState");

        fileMenu.add(saveAsMenuItem);

        saveQuitMenuItem.setMnemonic('U');
        saveQuitMenuItem.setText("Save, Quit");
        saveQuitMenuItem.setToolTipText("Save state then Quit");
        saveQuitMenuItem.addActionListener(new ButtonListener());
        saveQuitMenuItem.setActionCommand("SaveStateThenQuit");
/*
        saveQuitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveQuitMenuItemActionPerformed(evt);
            }
        });
*/
        fileMenu.add(saveQuitMenuItem);

        ampExtMenuBar.add(fileMenu);

        executeMenu.setMnemonic('E');
        executeMenu.setText("Execute");

        writeScriptMenuItem.setMnemonic('W');
        writeScriptMenuItem.setText("Write Script");
        writeScriptMenuItem.setToolTipText("Save generated script");
        writeScriptMenuItem.addActionListener(new ButtonListener());
        writeScriptMenuItem.setActionCommand(AmpExtConstants.AMP_EXT_SAVE_SCRIPT);
        executeMenu.add(writeScriptMenuItem);

        runMenuItem.setMnemonic('R');
        runMenuItem.setText("Run Script");
        runMenuItem.setToolTipText("Save and run generated script");
        runMenuItem.addActionListener(new ButtonListener());
        runMenuItem.setActionCommand(AmpExtConstants.AMP_EXT_EXECUTE_SCRIPT);
        executeMenu.add(runMenuItem);

        displayResultsMenuItem.setMnemonic('D');
        displayResultsMenuItem.setText("Display Results");
        displayResultsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                displayResultsButtonActionPerformed(evt);
            }
        });
        executeMenu.add(displayResultsMenuItem);

        cancelRunMenuItem.setMnemonic('C');
        cancelRunMenuItem.setText("Cancel Run");
        cancelRunMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelRunButtonActionPerformed(evt);
            }
        });
        executeMenu.add(cancelRunMenuItem);

        statusMenuItem.setMnemonic('S');
        statusMenuItem.setText("Check Status");
        statusMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkStatusButtonActionPerformed(evt);
            }
        });
        executeMenu.add(statusMenuItem);
        statusMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkStatusButtonActionPerformed(evt);
            }
        });

        ampExtMenuBar.add(executeMenu);

        helpMenu.setMnemonic('H');
        helpMenu.setText("Help");
        JMenuItem ampExtMenuItem = new JMenuItem("Amplitude Extraction");
        ampExtMenuItem.setMnemonic('M');
        ampExtMenuItem.setToolTipText("View help on Amplitude Extraction");
        helpMenu.add(ampExtMenuItem);
        AmplitudeExtractionHB.enableHelpOnButton(ampExtMenuItem,"Secant Amplitude Extraction",null);

        JMenuItem controlPanelMenuItem = new JMenuItem("Control Panel");
        controlPanelMenuItem.setMnemonic('C');
        controlPanelMenuItem.setToolTipText("View help on Control Panel");
        helpMenu.add(controlPanelMenuItem);
        AmplitudeExtractionHB.enableHelpOnButton(controlPanelMenuItem,"help_on_control",null);

        JMenuItem horizonMenuItem = new JMenuItem("Horizon");
        horizonMenuItem.setMnemonic('Z');
        horizonMenuItem.setToolTipText("View help on Horizon");
        helpMenu.add(horizonMenuItem);
        AmplitudeExtractionHB.enableHelpOnButton(horizonMenuItem,"help_on_horizon",null);

        JMenuItem horizonParaMenuItem = new JMenuItem("Horizon Parameters");
        horizonParaMenuItem.setMnemonic('P');
        horizonParaMenuItem.setToolTipText("View help on Horizon Parameters");
        helpMenu.add(horizonParaMenuItem);
        AmplitudeExtractionHB.enableHelpOnButton(horizonParaMenuItem,"help_on_horizon_params",null);

        JMenuItem inOutMenuItem = new JMenuItem("Input and Output");
        inOutMenuItem.setMnemonic('I');
        inOutMenuItem.setToolTipText("View help on Input and Output");
        helpMenu.add(inOutMenuItem);
        AmplitudeExtractionHB.enableHelpOnButton(inOutMenuItem,"help_on_input_and_output",null);

        JMenuItem targetTypeMenuItem = new JMenuItem("Target Type");
        targetTypeMenuItem.setMnemonic('T');
        targetTypeMenuItem.setToolTipText("View help on Target Type");
        helpMenu.add(targetTypeMenuItem);
        AmplitudeExtractionHB.enableHelpOnButton(targetTypeMenuItem,"help_on_target",null);
        helpMenu.addSeparator();
        aboutMenuItem.setText("About...");
        aboutMenuItem.setMnemonic('A');
        aboutMenuItem.setToolTipText("View About AmpExt");
        helpMenu.add(aboutMenuItem);

        aboutMenuItem.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(props == null){
                    String compName = agent.getMessagingMgr().getRegisteredComponentDisplayNameByDescriptor(agent.getComponentDescriptor());
                    props = ComponentUtils.getComponentVersionProperies(compName);
                }
                new HelpAbout(comp,props);
            }
        });
        helpMenu.add(aboutMenuItem);

        ampExtMenuBar.add(helpMenu);

        setJMenuBar(ampExtMenuBar);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, ampExtTabbedPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 742, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                //.addContainerGap()
                .add(ampExtTabbedPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 676, Short.MAX_VALUE)
            )
        );
        // </editor-fold>//GEN-END:initComponents

        // get full path of project from message dispatcher
        //String p = agent.getMessagingMgr().getProject();
        //project = p.substring(p.lastIndexOf(filesep)+1);

        QiProjectDescriptor qpDesc = agent.getQiProjectDescriptor();
        project = QiProjectDescUtils.getQiProjectName(qpDesc);
        //fileSystem = p.substring(0,p.lastIndexOf(filesep));
        fileSystem = QiProjectDescUtils.getQiSpace(qpDesc) + QiProjectDescUtils.getQiProjectReloc(qpDesc);
        //QiProjectDescUtils.setQiSpace(qiProjectDesc, fileSystem);
        //QiProjectDescUtils.setQiProjectReloc(qiProjectDesc, project);
/*
        String title = "";
        ComponentDescriptor myDesc = agent.getMessagingMgr().getMyComponentDesc();
        String pdn = CompDescUtils.getDescPreferredDisplayName(myDesc);
        if (pdn.trim().length() > 0)
            title = pdn.trim();
        else
            title = CompDescUtils.getDescDisplayName(myDesc);

        this.setTitle(title + "  Project: " + project);
        setSize(752,790);
        //pack();
*/

        setEntireDatasetButtonEnabled(false);
        setLineTraceFieldEnabled(false);
        setSize(752,730);
        resetTitle(project);


    }

    private void convertShotpointsToTraces(){
        Object[]  keys = selectedLineMap.keySet().toArray();
        if(landmarkAgent == null)
            landmarkAgent = LandmarkServices.getInstance();
        for(int i = 0; keys!= null && i < keys.length; i++){
            LineInfo li = selectedLineMap.get((String)keys[i]);
            float [] arr = landmarkAgent.convertShotpointsTo2DTraces(li.getProjectName(),(String)keys[i],
                    li.getMinShotPoint(), li.getMaxShotPoint(), li.getIncShotPoint());
            li.setMinTrace((int)arr[0]);
            li.setMaxTrace((int)arr[1]);
            li.setIncTrace((int)arr[2]);
            selectedLineMap.put((String)keys[i], li);
        }

    }

    public void setEntireDatasetButtonEnabled(boolean enable) {
        cdpFromTextField.setEnabled(enable);
        cdpToTextField.setEditable(enable);
        cdpByTextField.setEditable(enable);
        epFromTextField.setEnabled(enable);
        epToTextField.setEditable(enable);
        epByTextField.setEditable(enable);
        entireCubeRangeButton.setEnabled(enable);
    }

    private void formTargetPanel(javax.swing.JPanel panel, int type){
        panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Target Type", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));
        if(type == 1)
            troughTypeRadioButton.setSelected(true);
        else if(type == 2)
            peakTypeRadioButton.setSelected(true);
        else if(type == 3)
            absTypeRadioButton.setSelected(true);
        org.jdesktop.layout.GroupLayout targetTypePanelLayout = new org.jdesktop.layout.GroupLayout(panel);
        panel.setLayout(targetTypePanelLayout);
        targetTypePanelLayout.setHorizontalGroup(
            targetTypePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(targetTypePanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(troughTypeRadioButton)
                .add(19, 19, 19)
                .add(peakTypeRadioButton)
                .add(19, 19, 19)
                .add(absTypeRadioButton)
                .addContainerGap(613, Short.MAX_VALUE))
        );
        targetTypePanelLayout.setVerticalGroup(
            targetTypePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(targetTypePanelLayout.createSequentialGroup()
                .add(targetTypePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(troughTypeRadioButton)
                    .add(peakTypeRadioButton)
                    .add(absTypeRadioButton))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }
    private void formInputPanel(){
        inputPanel = new javax.swing.JPanel();
        landmarkInRadioButton.setText("Landmark");
        landmarkInRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        landmarkInRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        bhpsuInRadioButton.setText("BHP SU");
        bhpsuInRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        bhpsuInRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        inputPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Input", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));
        org.jdesktop.layout.GroupLayout inputPanelLayout = new org.jdesktop.layout.GroupLayout(inputPanel);
        inputPanel.setLayout(inputPanelLayout);
        inputPanelLayout.setHorizontalGroup(
            inputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(inputPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkInRadioButton)
                .add(20, 20, 20)
                .add(bhpsuInRadioButton)
                .addContainerGap())
                //.addContainerGap(495, Short.MAX_VALUE))
            //.add(inputPanelLayout.createSequentialGroup()
                //.addContainerGap()
                .add(inputDataPanel,org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                //.addContainerGap())
                );
        inputPanelLayout.setVerticalGroup(
            inputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(inputPanelLayout.createSequentialGroup()

                .add(inputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkInRadioButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 15, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(bhpsuInRadioButton))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 8, Short.MAX_VALUE)
                    .addContainerGap(10,10)
                .add(inputDataPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                //.addContainerGap()
                )
        );

    }


    private boolean validateLandmarkParameters(){
        final Component comp = this;
        if(!landmarkOutputRadioButton.isSelected())
            return true;
        if(landmarkAgent == null)
            landmarkAgent = LandmarkServices.getInstance();
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        if(landmarkOutProjectTextField == null || landmarkOutProjectTextField.getText().trim().length() == 0){
            JOptionPane.showMessageDialog(comp, "Must specify a Landmark project for the output volume.", "Landmark project is missing",    JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(tabIdx);
            landmarkOutListButton.requestFocus();
            return false;
        }else{
            if (!landmarkAgent.isValidLandmarkProject(landmarkOutProjectTextField.getText().trim())) {
                JOptionPane.showMessageDialog(comp, "Can not find project name " + landmarkOutProjectTextField.getText().trim()
                        + ". Check to see if it is valid Landmark project", "Project not found",    JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                landmarkOutProjectTextField.requestFocus();
                return false;
            }

            if(landmarkInRadioButton.isSelected()){
                if(!landmarkInProjectTextField.getText().trim().equals(landmarkOutProjectTextField.getText().trim())){
                    JOptionPane.showMessageDialog(comp, "Projects mismatch between the input and the output.", "Landmark project mismatches",   JOptionPane.WARNING_MESSAGE);
                    ampExtTabbedPane.setSelectedIndex(tabIdx);
                    landmarkOutListButton.requestFocus();
                    return false;
                }
            }
        }

        String text = outputCubeTextField.getText().trim();
        if (text == null || text.trim().length() == 0) {
            JOptionPane.showMessageDialog(comp, "The " + outputCubeLabel.getText()
                    + " field is empty.", "Missing data entry", JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(tabIdx);
            outputCubeTextField.requestFocus();
            return false;
        }
        if(text.endsWith(filesep)){
            JOptionPane.showMessageDialog(comp, "The " + outputCubeLabel.getText()
                    + " field can not end with " + filesep, "Error in data entry",  JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(tabIdx);
            outputCubeTextField.requestFocus();
            return false;
        }
        return true;
    }

    private void formLandmarkInputDataPanel() {
        inputDataPanel = new javax.swing.JPanel();
        landmark3DRadioButton.setText("3 D");
        landmark3DRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        landmark3DRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        landmark2DRadioButton.setText("2 D");
        landmark2DRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        landmark2DRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        //inputDataPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        inputDataPanel.setBorder(javax.swing.BorderFactory.createRaisedBevelBorder());
        org.jdesktop.layout.GroupLayout landmarkInPanelLayout = new org.jdesktop.layout.GroupLayout(inputDataPanel);
        inputDataPanel.setLayout(landmarkInPanelLayout);
        landmarkInPanelLayout.setHorizontalGroup(
            landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkInPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn2D3DDataPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(landmarkInPanelLayout.createSequentialGroup()
                        .add(landmark3DRadioButton)
                        .add(16, 16, 16)
                        .add(landmark2DRadioButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 104, Short.MAX_VALUE)
                        //.add(exportToBhpsuCheckBox)
                        //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        //.add(lm2BhpsuPrefixLabel)
                        //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        //.add(lm2BhpsuPrefixTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 74, Short.MAX_VALUE)
                        .addContainerGap()))
                .addContainerGap())
        );
        landmarkInPanelLayout.setVerticalGroup(
            landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkInPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmark3DRadioButton)
                    .add(landmark2DRadioButton)
                    //.add(exportToBhpsuCheckBox)
                    //.add(lm2BhpsuPrefixLabel)
                    //.add(lm2BhpsuPrefixTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    )
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkIn2D3DDataPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }

    public boolean isLandmark3DSelected(){
        return landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected();

    }
    /*
     * Check is the data input is Landmark type
     */
    public boolean isLandmarkSelected(){
        return landmarkInRadioButton.isSelected();
    }

    public boolean isLandmark2DSelected (){
        return landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected();
    }


    private void formLandmark2DPanel(){
        landmarkIn2D3DDataPanel = new javax.swing.JPanel();
        //landmarkIn2D3DDataPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        landmarkIn2D3DDataPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        landmarkProjectLabel.setText("SeisWorks Project:");

        list2DProjectsButton.setText("List...");

        seismicFileLabel.setText("Process Level:");

        shotpointLabel.setText("Shotpoint Range:");

        tracesToLabel.setText("to");

        tracesByLabel.setText("by");

        entireVolume2DButton.setText("Entire Line");

        select2DButton.setText("Select from map");
        select2DButton.setEnabled(false);
        seismic2DFileComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        linesLabel.setText("All Associated Seismic Lines:");
        String [] files = new String[0];
        if(landmark2DfileLinesMap != null){
            Set set= landmark2DfileLinesMap.keySet ();
            files = new String[set.size()+1];
            Iterator iter = set.iterator () ;
            files[0] = "Select one";
            int i = 1;
            while(iter.hasNext()){
                Object obj = iter.next();
                files[i++] = (String)obj;
            }

        }
        /*
        while ( iter.hasNext())  {
            files[]
           Object Obj = iter.next ();
           System.out.println ( "Key: "+ Obj);

           List<String>  list = (List<String>) map.get(Obj);
           System.out.println ( "size: "+ list.size());
           for(int i = 0; i < list.size(); i++){
               String s = list.get(i);
                System.out.println(s) ;
           }
        }
        */
        Arrays.sort(files,1,files.length);
        seismic2DFileComboBox.setModel(new javax.swing.DefaultComboBoxModel(files));
        String [] items = {};
        if(seismic2DFileComboBox.getSelectedIndex() != -1 && landmark2DfileLinesMap.keySet().size() > 0){
            Set set= landmark2DfileLinesMap.keySet ();
            //selectedProcessLevelIndex = seismic2DFileComboBox.getSelectedIndex();
            seismic2DFileComboBox.setSelectedIndex(selectedProcessLevelIndex);
            if(selectedProcessLevelIndex > 0){
                String item = (String)seismic2DFileComboBox.getSelectedItem();
                List<String> list = landmark2DfileLinesMap.get(item);
                if(list != null){
                    items = new String[list.size()];
                    for(int i = 0; i < list.size(); i++){
                        items[i] = list.get(i);
                    }
                }
                seismic2DLineList.setListData(items);
            }
        }
        final String []  fitems = items;
        seismic2DLineList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = fitems;
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        seismic2DLineList.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(127, 157, 185)));
        lineListScrollPane.setViewportView(seismic2DLineList);


        String [] strs = new String[selectedLines.size()];
        for(int i = 0 ; i < selectedLines.size(); i++){
            strs[i] = (String)selectedLines.get(i);
        }

        final String[] fstrs = strs;
        selectedSeismic2DLineList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = fstrs;
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });

        if(selectedLineName != null && selectedLineName.trim().length() > 0){
            int ind = -1;
            for(int i = 0; i < selectedSeismic2DLineList.getModel().getSize(); i++){
                if(selectedSeismic2DLineList.getModel().getElementAt(i) != null && selectedSeismic2DLineList.getModel().getElementAt(i).equals(selectedLineName)){
                    ind = i;
                    break;
                }
            }
            if(ind != -1){
                selectedSeismic2DLineList.setSelectedIndex(ind);
                LineInfo li = selectedLineMap.get(selectedLineName);
                shotpointTextField.setText(String.valueOf(li.getMinShotPoint()));
                shotpointToTextField.setText(String.valueOf(li.getMaxShotPoint()));
                shotpointByTextField.setText(String.valueOf(li.getIncShotPoint()));
            }
        }

        selectedSeismic2DLineList.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(127, 157, 185)));
        selectedLineListScrollPane.setViewportView(selectedSeismic2DLineList);
        selectedLinesLabel.setText("Selected Lines:");

        rightArrowButton.setText(">>");

        leftArrowButton.setText("<<");

        seismicLinesPanel = new javax.swing.JPanel();
        org.jdesktop.layout.GroupLayout seismicLinesPanelLayout = new org.jdesktop.layout.GroupLayout(seismicLinesPanel);
        seismicLinesPanel.setLayout(seismicLinesPanelLayout);
        seismicLinesPanelLayout.setHorizontalGroup(
            seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, seismicLinesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(seismicLinesPanelLayout.createSequentialGroup()
                        .add(lineListScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 240, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(15, 15, 15)
                        .add(seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(rightArrowButton)
                            .add(leftArrowButton)))
                    .add(linesLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 27, Short.MAX_VALUE)
                .add(seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(selectedLineListScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 249, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(seismicLinesPanelLayout.createSequentialGroup()
                            .addContainerGap()
                            .add(selectedLinesLabel)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(clearButton))
                            )
                .addContainerGap())
        );
        seismicLinesPanelLayout.setVerticalGroup(
            seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(seismicLinesPanelLayout.createSequentialGroup()
                .add(seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(seismicLinesPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .add(seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                    .add(selectedLinesLabel)
                                    .add(clearButton))
                            .add(linesLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(seismicLinesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(seismicLinesPanelLayout.createSequentialGroup()
                                .add(lineListScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 319, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED))
                            .add(selectedLineListScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 319, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(seismicLinesPanelLayout.createSequentialGroup()
                        .add(108, 108, 108)
                        .add(rightArrowButton)
                        .add(16, 16, 16)
                        .add(leftArrowButton)))
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout landmarkIn2D3DDataPanelLayout = new org.jdesktop.layout.GroupLayout(landmarkIn2D3DDataPanel);
        landmarkIn2D3DDataPanel.setLayout(landmarkIn2D3DDataPanelLayout);
        landmarkIn2D3DDataPanelLayout.setHorizontalGroup(
            landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                        .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(seismicFileLabel)
                            .add(landmarkProjectLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(landmarkIn2DProjectTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 394, Short.MAX_VALUE))
                                    .add(seismic2DFileComboBox, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(list2DProjectsButton)
                                .add(74, 74, 74))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                                        .add(shotpointLabel)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(shotpointTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(12, 12, 12)
                                        .add(tracesToLabel)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED))
                                    .add(entireVolume2DButton))
                                .add(18, 18, 18)
                                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(select2DButton)
                                    .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(shotpointToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(13, 13, 13)
                                        .add(tracesByLabel)
                                        .add(12, 12, 12)
                                        .add(shotpointByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 50, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                                .add(37, 37, 37))))
                    .add(seismicLinesPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        landmarkIn2D3DDataPanelLayout.setVerticalGroup(
            landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkProjectLabel)
                    .add(landmarkIn2DProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(list2DProjectsButton))
                .add(14, 14, 14)
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(seismicFileLabel)
                    .add(seismic2DFileComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(21, 21, 21)
                .add(seismicLinesPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(14, 14, 14)
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(shotpointToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(shotpointByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tracesByLabel)
                    .add(tracesToLabel)
                    .add(shotpointLabel)
                    .add(shotpointTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 21, Short.MAX_VALUE)
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(select2DButton)
                    .add(entireVolume2DButton))
                .addContainerGap())
        );


    }

    private void formLandmark2DPanelOld2(){
        landmarkIn2D3DDataPanel = new javax.swing.JPanel();

        landmarkIn2D3DDataPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        landmarkProjectLabel.setText("SeisWorks Project:");

        list2DProjectsButton.setText("List...");

        linesLabel.setText("Seismic Lines:");

        landmarkRangeLabel.setText("Range:");

        seismicFileLabel.setText("Seismic File:");

        shotpointLabel.setText("Shotpoints");

        tracesToLabel.setText("to");

        tracesByLabel.setText("by");

        entireVolume2DButton.setText("Entire Volume");

        select2DButton.setText("Select from map");
        String [] files = new String[1];
        //if(landmarkIn2DProjectTextField != null && landmarkIn2DProjectTextField.getText().trim().length() > 0){
            //Map<String,List<String>> map = landmarkAgent.get2v2FileLinesMap(landmarkIn2DProjectTextField.getText().trim());
        if(landmark2DfileLinesMap != null){
            Set set= landmark2DfileLinesMap.keySet ();
            files = new String[set.size()];
            Iterator iter = set.iterator () ;
            int i = 0;
            while(iter.hasNext()){
                Object obj = iter.next();
                files[i++] = (String)obj;
            }

        }
        /*
        while ( iter.hasNext())  {
            files[]
           Object Obj = iter.next ();
           System.out.println ( "Key: "+ Obj);

           List<String>  list = (List<String>) map.get(Obj);
           System.out.println ( "size: "+ list.size());
           for(int i = 0; i < list.size(); i++){
               String s = list.get(i);
                System.out.println(s) ;
           }
        }
        */
        seismic2DFileComboBox.setModel(new javax.swing.DefaultComboBoxModel(files));
        String [] items = new String[1];
        if(seismic2DFileComboBox.getSelectedIndex() != -1 && landmark2DfileLinesMap != null){
            String item = (String)seismic2DFileComboBox.getSelectedItem();
            List<String> list = landmark2DfileLinesMap.get(item);
            if(list != null){
                items = new String[list.size()];
                for(int i = 0; i < items.length; i++){
                    items[i] = list.get(i);
                }
            }
            seismic2DLineList.setListData(items);
        }
        //final String []  fitems = items;
        //seismic2DLineList.setModel(new javax.swing.AbstractListModel() {
        //  String[] strings = fitems;
        //  public int getSize() { return strings.length; }
        //  public Object getElementAt(int i) { return strings[i]; }
        //});
        lineListScrollPane.setViewportView(seismic2DLineList);

        org.jdesktop.layout.GroupLayout landmarkIn2D3DDataPanelLayout = new org.jdesktop.layout.GroupLayout(landmarkIn2D3DDataPanel);
        landmarkIn2D3DDataPanel.setLayout(landmarkIn2D3DDataPanelLayout);
        landmarkIn2D3DDataPanelLayout.setHorizontalGroup(
            landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(linesLabel)
                    .add(seismicFileLabel)
                    .add(landmarkProjectLabel)
                    .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                        .add(landmarkRangeLabel)
                        .add(30, 30, 30)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                        .add(shotpointLabel)
                        .add(14, 14, 14)
                        .add(shotpointTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 53, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(14, 14, 14)
                        .add(tracesToLabel)
                        .add(20, 20, 20)
                        .add(shotpointToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 53, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(14, 14, 14)
                        .add(tracesByLabel)
                        .add(16, 16, 16)
                        .add(shotpointByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 40, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(238, Short.MAX_VALUE))
                    .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                        .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, lineListScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 394, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                                    .add(entireVolume2DButton)
                                    .add(22, 22, 22)
                                    .add(select2DButton))
                                .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(landmarkIn2DProjectTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 394, Short.MAX_VALUE))
                                .add(seismic2DFileComboBox, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(list2DProjectsButton)
                        .add(74, 74, 74))))
        );
        landmarkIn2D3DDataPanelLayout.setVerticalGroup(
            landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkIn2D3DDataPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkProjectLabel)
                    .add(landmarkIn2DProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(list2DProjectsButton))
                .add(14, 14, 14)
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(seismicFileLabel)
                    .add(seismic2DFileComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(20, 20, 20)
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(linesLabel)
                    .add(lineListScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 372, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(20, 20, 20)
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkRangeLabel)
                    .add(shotpointLabel)
                    .add(shotpointTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tracesToLabel)
                    .add(shotpointToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(shotpointByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tracesByLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 16, Short.MAX_VALUE)
                .add(landmarkIn2D3DDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(entireVolume2DButton)
                    .add(select2DButton))
                .addContainerGap())
);
    }

    private void formLandmarkOutputBhpsuInDataPanel(){
        outputDataPanel = new javax.swing.JPanel();
        outputDataPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        outputCubeLabel.setText("Output Cube's Relative Path/Name");

        outputHorizonsLabel.setText("Output Horizons' Relative Path/Name");

        landmarkOutProjectLabel.setText("Landmark Project");

        landmarkOutListButton.setText("List...");

        org.jdesktop.layout.GroupLayout outputDataPanelLayout = new org.jdesktop.layout.GroupLayout(outputDataPanel);
        outputDataPanel.setLayout(outputDataPanelLayout);
        outputDataPanelLayout.setHorizontalGroup(
            outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(outputDataPanelLayout.createSequentialGroup()
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(outputDataPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(outputCubeLabel)
                            .add(outputHorizonsLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, outputDataPanelLayout.createSequentialGroup()
                        .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(landmarkOutProjectLabel)
                        .add(12, 12, 12)))
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(outputDataPanelLayout.createSequentialGroup()
                        .add(landmarkOutProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 311, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkOutListButton))
                    .add(landmarkOutputCubeTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
                    .add(landmarkOutputHorizonsTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE))
                .addContainerGap())
        );
        outputDataPanelLayout.setVerticalGroup(
            outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(outputDataPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(outputCubeLabel)
                    .add(landmarkOutputCubeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(outputHorizonsLabel)
                    .add(landmarkOutputHorizonsTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkOutProjectLabel)
                    .add(landmarkOutListButton)
                    .add(landmarkOutProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }


    private void formLandmark3DOutputDataPanel(){
        outputDataPanel = new javax.swing.JPanel();
        outputDataPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        outputCubeLabel.setText("Output Cube's Relative Path/Name");

        outputHorizonsLabel.setText("Output Horizons' Relative Path/Name");

        landmarkOutProjectLabel.setText("Landmark Project");

        landmarkOutListButton.setText("List...");

        org.jdesktop.layout.GroupLayout outputDataPanelLayout = new org.jdesktop.layout.GroupLayout(outputDataPanel);
        outputDataPanel.setLayout(outputDataPanelLayout);
        outputDataPanelLayout.setHorizontalGroup(
            outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(outputDataPanelLayout.createSequentialGroup()
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(outputDataPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(outputCubeLabel)
                            .add(outputHorizonsLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, outputDataPanelLayout.createSequentialGroup()
                        .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(landmarkOutProjectLabel)
                        .add(12, 12, 12)))
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(outputDataPanelLayout.createSequentialGroup()
                        .add(landmark3DOutProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 311, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkOutListButton))
                    .add(landmark3DOutputCubeTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
                    .add(landmark3DOutputHorizonsTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE))
                .addContainerGap())
        );
        outputDataPanelLayout.setVerticalGroup(
            outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(outputDataPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(outputCubeLabel)
                    .add(landmark3DOutputCubeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(outputHorizonsLabel)
                    .add(landmark3DOutputHorizonsTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkOutProjectLabel)
                    .add(landmarkOutListButton)
                    .add(landmark3DOutProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }

    private void formLandmark2DOutputDataPanel(){
        outputDataPanel = new javax.swing.JPanel();
        outputDataPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        landmark2DOutputCubeTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outputCubeTextFieldActionPerformed(evt);
            }
        });

        landmark2DOutputHorizonsTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outputHorizonsTextFieldActionPerformed(evt);
            }
        });

        outputCubeLabel.setText("Output Cube's Relative Path/Name");

        outputHorizonsLabel.setText("Output Horizons' Relative Path/Name");

        landmarkOutProjectLabel.setText("Landmark Project");

        landmarkOutListButton.setText("List...");

        org.jdesktop.layout.GroupLayout outputDataPanelLayout = new org.jdesktop.layout.GroupLayout(outputDataPanel);
        outputDataPanel.setLayout(outputDataPanelLayout);
        outputDataPanelLayout.setHorizontalGroup(
            outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(outputDataPanelLayout.createSequentialGroup()
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(outputDataPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(outputCubeLabel)
                            .add(outputHorizonsLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, outputDataPanelLayout.createSequentialGroup()
                        .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(landmarkOutProjectLabel)
                        .add(12, 12, 12)))
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(outputDataPanelLayout.createSequentialGroup()
                        .add(landmark2DOutProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 311, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkOutListButton))
                    .add(landmark2DOutputCubeTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
                    .add(landmark2DOutputHorizonsTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE))
                .addContainerGap())
        );
        outputDataPanelLayout.setVerticalGroup(
            outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(outputDataPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(outputCubeLabel)
                    .add(landmark2DOutputCubeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(outputHorizonsLabel)
                    .add(landmark2DOutputHorizonsTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkOutProjectLabel)
                    .add(landmarkOutListButton)
                    .add(landmark2DOutProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }

    private void formOutputDataPanel(){
        outputDataPanel = new javax.swing.JPanel();
        outputDataPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        outputCubeTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outputCubeTextFieldActionPerformed(evt);
            }
        });

        outputHorizonsTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outputHorizonsTextFieldActionPerformed(evt);
            }
        });

        outputCubeLabel.setText("Output Cube's Relative Path/Name");

        outputHorizonsLabel.setText("Output Horizons' Relative Path/Name");

        org.jdesktop.layout.GroupLayout outputDataPanelLayout = new org.jdesktop.layout.GroupLayout(outputDataPanel);
        outputDataPanel.setLayout(outputDataPanelLayout);
        outputDataPanelLayout.setHorizontalGroup(
            outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(outputDataPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(outputCubeLabel)
                    .add(outputHorizonsLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(outputHorizonsTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 400, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(outputCubeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 400, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        outputDataPanelLayout.setVerticalGroup(
            outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(outputDataPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(outputCubeLabel)
                    .add(outputCubeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(outputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(outputHorizonsLabel)
                    .add(outputHorizonsTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }

    private void formLandmark2DPanelOld(){
        landmarkIn2D3DDataPanel = new javax.swing.JPanel();

        landmarkIn2D3DDataPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        landmarkProjectLabel.setText("SeisWorks Project:");
        list2DProjectsButton.setText("List...");
        linesLabel.setText("Seismic Lines:");
        linesListButton.setText("List...");
        seismicFileLabel.setText("Seismic Files:");
        shotpointLabel.setText("Shotpoints");
        entireVolume2DButton.setText("Entire Volume");
        select2DButton.setText("Select from map");
        seismicFileListButton.setText("List...");

        org.jdesktop.layout.GroupLayout landmarkIn2DPanelLayout = new org.jdesktop.layout.GroupLayout(landmarkIn2D3DDataPanel);
        landmarkIn2D3DDataPanel.setLayout(landmarkIn2DPanelLayout);
        landmarkIn2DPanelLayout.setHorizontalGroup(
            landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkIn2DPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(landmarkProjectLabel)
                    .add(landmarkIn2DPanelLayout.createSequentialGroup()
                        .add(landmarkRangeLabel)
                        .add(27, 27, 27))
                    .add(seismicFileLabel)
                    .add(linesLabel))
                .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn2DPanelLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(landmarkIn2DProjectTextField,org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
                            .add(linesInTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
                            .add(seismicFileTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)))
                    .add(landmarkIn2DPanelLayout.createSequentialGroup()
                        .add(shotpointLabel)
                        .add(14, 14, 14)
                        .add(shotpointTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 53, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(15, 15, 15)
                        .add(tracesToLabel)
                        .add(12, 12, 12)
                        .add(shotpointToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 53, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(16, 16, 16)
                        .add(tracesByLabel)
                        .add(13, 13, 13)
                        .add(shotpointByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 40, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(landmarkIn2DPanelLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(entireVolume2DButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(select2DButton)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(list2DProjectsButton)
                    .add(linesListButton)
                    .add(seismicFileListButton))
                .addContainerGap()
                    )
        );
        landmarkIn2DPanelLayout.setVerticalGroup(
            landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkIn2DPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(list2DProjectsButton)
                    .add(landmarkProjectLabel)
                    .add(landmarkIn2DProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(linesListButton)
                    .add(linesInTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(linesLabel))
                .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn2DPanelLayout.createSequentialGroup()
                        .add(12, 12, 12)
                        //.add(8, 8, 8)
                        .add(seismicFileLabel))
                    .add(landmarkIn2DPanelLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(seismicFileTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(seismicFileListButton))))
                .add(12, 12, 12)
                //.add(8, 8, 8)
                .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkRangeLabel)
                    .add(shotpointLabel)
                    .add(tracesToLabel)
                    .add(shotpointToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(shotpointTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tracesByLabel)
                    .add(shotpointByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                //.add(8, 8, 8)
                .add(landmarkIn2DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(select2DButton)
                    .add(entireVolume2DButton))
                .addContainerGap(183, Short.MAX_VALUE)
                    //.addContainerGap()
                    )
        );
    }


    private void formLandmark3DPanel() {
        landmarkIn2D3DDataPanel = new javax.swing.JPanel();

        landmarkIn2D3DDataPanel.setBorder(javax.swing.BorderFactory
                .createTitledBorder(""));
        landmarkProjectLabel.setText("SeisWorks Project:");

        listProjectsButton.setText("List...");

        volumeLabel.setText("Volume:");

        listVolumesButton.setText("List...");

        landmarkRangeLabel.setText("Range:");

        rangeLinesLabel.setText("Lines");

        linesToLabel.setText("to");

        linesByLabel.setText("by");

        tracesLabel.setText("Traces");

        tracesToLabel.setText("to");

        tracesByLabel.setText("by");

        entireVolumeButton.setText("Entire Volume");

        selectButton.setText("Select from map");
        selectButton.setEnabled(false);
        org.jdesktop.layout.GroupLayout landmarkIn3DPanelLayout = new org.jdesktop.layout.GroupLayout(landmarkIn2D3DDataPanel);
        landmarkIn2D3DDataPanel.setLayout(landmarkIn3DPanelLayout);
        landmarkIn3DPanelLayout.setHorizontalGroup(
            landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkIn3DPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn3DPanelLayout.createSequentialGroup()
                        .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(volumeLabel)
                            .add(landmarkProjectLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(landmarkInProjectTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
                            .add(volumeTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)))
                    .add(landmarkIn3DPanelLayout.createSequentialGroup()
                        .add(landmarkRangeLabel)
                        .add(17, 17, 17)
                        .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(landmarkIn3DPanelLayout.createSequentialGroup()
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(rangeLinesLabel)
                                    .add(tracesLabel))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(tracesTextField)
                                    .add(linesTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE))
                                .add(14, 14, 14)
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(linesToLabel)
                                    .add(tracesToLabel))
                                .add(13, 13, 13)
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(tracesToTextField)
                                    .add(linesToTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 49, Short.MAX_VALUE))
                                .add(16, 16, 16)
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(tracesByLabel)
                                    .add(linesByLabel))
                                .add(12, 12, 12)
                                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(tracesByTextField)
                                    .add(linesByTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)))
                            .add(landmarkIn3DPanelLayout.createSequentialGroup()
                                .add(entireVolumeButton)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(selectButton)))))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(listProjectsButton)
                    .add(listVolumesButton))
                .addContainerGap())
        );
        landmarkIn3DPanelLayout.setVerticalGroup(
            landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkIn3DPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(listProjectsButton)
                    .add(landmarkProjectLabel)
                    .add(landmarkInProjectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(12, 12, 12)
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(volumeLabel)
                        .add(volumeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(listVolumesButton))
                .add(12, 12, 12)
                //.add(8, 8, 8)
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkRangeLabel)
                    .add(rangeLinesLabel)
                    .add(linesToLabel)
                    .add(linesToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(linesTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(linesByLabel)
                    .add(linesByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(12, 12, 12)
                .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkIn3DPanelLayout.createSequentialGroup()
                        .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(tracesLabel)
                                .add(tracesTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(tracesToLabel)
                                .add(tracesToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(tracesByLabel)))
                        .add(18, 18, 18)
                        //.add(8, 8, 8)
                        .add(landmarkIn3DPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(entireVolumeButton)
                            .add(selectButton)))
                    .add(tracesByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(196, Short.MAX_VALUE)
                //.addContainerGap()
                 )
        );

    }
    /**
     * Form the edge mode Horizons panel.
     */
    private void formEdgeModeHorizonPanel() {
        //horizonDataPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Horizons", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));
        horizonDataPanel.setBorder(javax.swing.BorderFactory.createRaisedBevelBorder());
        edgeModeRadioButton.setSelected(true);
        edgeModeRadioButton.setText("Edge Mode (2 Horizons)");
        edgeModeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        edgeModeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        pointModeRadioButton.setText("Point Mode (1 Horizon)");
        pointModeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        pointModeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        topHorizonLabel.setText("Top Horizon");
        topHorizonBrowseButton = new javax.swing.JButton();
        topHorizonBrowseButton.setText("Browse");

        topHorizonBrowseButton.setActionCommand(AmpExtConstants.AMP_EXT_SELECT_TOP_HORIZON);
        topHorizonBrowseButton.addActionListener(new ButtonListener());
        bottomHorizonLabel.setText("Bottom Horizon");
        bottomHorizonBrowseButton = new javax.swing.JButton();
        bottomHorizonBrowseButton.setText("Browse");

        bottomHorizonBrowseButton.setActionCommand(AmpExtConstants.AMP_EXT_SELECT_BOTTOM_HORIZON);
        bottomHorizonBrowseButton.addActionListener(new ButtonListener());

        horizonsParametersPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Horizon Parameters", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));
        /*
        horizonsParametersTable.setFont(new java.awt.Font("Arial", 0, 11));
        horizonsParametersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"Top", "", "", ""},
                {"Bottom", "", "", ""}
            },
            new String [] {
                "Horizon", TABLE_HEADING_BIAS, TABLE_HEADING_SEARCH_ABOVE, TABLE_HEADING_SEARCH_BELOW
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        */
        horizonsParametersScrollPane.setViewportView(horizonsParametersTable);

        org.jdesktop.layout.GroupLayout horizonsParametersPanelLayout = new org.jdesktop.layout.GroupLayout(horizonsParametersPanel);
        horizonsParametersPanel.setLayout(horizonsParametersPanelLayout);
        horizonsParametersPanelLayout.setHorizontalGroup(
            horizonsParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsParametersPanelLayout.createSequentialGroup()
                .add(horizonsParametersScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(300, Short.MAX_VALUE))
        );
        horizonsParametersPanelLayout.setVerticalGroup(
            horizonsParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsParametersScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 55, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
        formBhpsuEdgeModeHorizonInPanel();
        horizonDataPanel.getAccessibleContext().setAccessibleName("Horizons");
    }

    /**
     * Form the edge mode Horizons panel.
     */
    private void formLandmark3DEdgeModeHorizonPanel() {
        //horizonDataPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Horizons", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));
        horizonDataPanel.setBorder(javax.swing.BorderFactory.createRaisedBevelBorder());
        edgeModeRadioButton.setSelected(true);
        edgeModeRadioButton.setText("Edge Mode (2 Horizons)");
        edgeModeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        edgeModeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        pointModeRadioButton.setText("Point Mode (1 Horizon)");
        pointModeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        pointModeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        topHorizonLabel.setText("Top Horizon");
        topHorizonBrowseButton = new javax.swing.JButton();
        topHorizonBrowseButton.setText("Browse");

        if(landmarkHorizonRadioButton.isSelected()){
            final AmplitudeExtraction comp = this;
            topHorizonBrowseButton.addActionListener(new ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    int projectType = 0;
                    if(landmarkAgent == null)
                        landmarkAgent = LandmarkServices.getInstance();
                    if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                        if(landmarkInProjectTextField.getText() == null  || landmarkInProjectTextField.getText().trim().length() == 0){
                            JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                        projectType = landmarkAgent.getLandmarkProjectType(landmarkInProjectTextField.getText().trim());

                    }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                        if(landmarkIn2DProjectTextField.getText() == null  || landmarkIn2DProjectTextField.getText().trim().length() == 0){
                            JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                        projectType = landmarkAgent.getLandmarkProjectType(landmarkIn2DProjectTextField.getText().trim());
                        ListModel lm = selectedSeismic2DLineList.getModel();
                        if(lm.getSize() == 0){
                            JOptionPane.showMessageDialog(comp, "Must have line(s) in the " + "\"" +"Selected Lines:" + "\"" + " list box." ,
                                    "No Landmark 2D lines Found", JOptionPane.WARNING_MESSAGE);
                                int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                                ampExtTabbedPane.setSelectedIndex(tabIdx);
                                return;
                        }
                    }else if(bhpsuInRadioButton.isSelected()){
                        JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                    }
                    if(projectType < 2 || projectType > 3){
                        JOptionPane.showMessageDialog(comp, "Invalid project type found.",
                            "Invalid Project Type", JOptionPane.WARNING_MESSAGE);
                        int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                        ampExtTabbedPane.setSelectedIndex(tabIdx);
                        return;
                    }
                    if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                        agent.openLandmarkHorizonDialog(landmarkInProjectTextField.getText().trim(),projectType,comp,landmark3DTopHorizonTextField);
                    }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                        ListModel lm = selectedSeismic2DLineList.getModel();
                        String [] lines = new String[lm.getSize()];
                        for(int i = 0; i < lm.getSize(); i++){
                            lines[i] = (String)lm.getElementAt(i);
                        }
                        agent.openLandmark2DHorizonDialog(landmarkIn2DProjectTextField.getText().trim(),projectType,lines, comp,landmark2DTopHorizonTextField);
                    }
                }
            });
        }else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            topHorizonBrowseButton.setActionCommand(AmpExtConstants.AMP_EXT_SELECT_TOP_HORIZON);
            topHorizonBrowseButton.addActionListener(new ButtonListener());
        }

        bottomHorizonLabel.setText("Bottom Horizon");
        bottomHorizonBrowseButton = new javax.swing.JButton();
        bottomHorizonBrowseButton.setText("Browse");

        if(landmarkHorizonRadioButton.isSelected()){
            final AmplitudeExtraction comp = this;
            bottomHorizonBrowseButton.addActionListener(new ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    int projectType = 0;
                    if(landmarkAgent == null)
                        landmarkAgent = LandmarkServices.getInstance();
                    if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                        if(landmarkInProjectTextField.getText() == null  || landmarkInProjectTextField.getText().trim().length() == 0){
                            JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                        projectType = landmarkAgent.getLandmarkProjectType(landmarkInProjectTextField.getText().trim());

                    }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                        if(landmarkIn2DProjectTextField.getText() == null  || landmarkIn2DProjectTextField.getText().trim().length() == 0){
                            JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                        projectType = landmarkAgent.getLandmarkProjectType(landmarkIn2DProjectTextField.getText().trim());

                        ListModel lm = selectedSeismic2DLineList.getModel();
                        if(lm.getSize() == 0){
                            JOptionPane.showMessageDialog(comp, "Must have line(s) in the " + "\"" +"Selected Lines:" + "\"" + " list box." ,
                                    "No Landmark 2D lines Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                    }else if(bhpsuInRadioButton.isSelected()){
                        JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                    }
                    if(projectType < 2 || projectType > 3){
                        JOptionPane.showMessageDialog(comp, "Invalid project type found.",
                            "Invalid Project Type", JOptionPane.WARNING_MESSAGE);
                        int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                        ampExtTabbedPane.setSelectedIndex(tabIdx);
                        return;
                    }
                    if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                        agent.openLandmarkHorizonDialog(landmarkInProjectTextField.getText().trim(),projectType,comp,landmark3DBottomHorizonTextField);
                    }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                        //agent.openLandmarkHorizonDialog(landmarkIn2DProjectTextField.getText().trim(),projectType,comp,landmark2DBottomHorizonTextField);
                        ListModel lm = selectedSeismic2DLineList.getModel();
                        String [] lines = new String[lm.getSize()];
                        for(int i = 0; i < lm.getSize(); i++){
                            lines[i] = (String)lm.getElementAt(i);
                        }
                        agent.openLandmark2DHorizonDialog(landmarkIn2DProjectTextField.getText().trim(),projectType,lines, comp,landmark2DBottomHorizonTextField);
                    }
                }
            });
        }else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            bottomHorizonBrowseButton.setActionCommand(AmpExtConstants.AMP_EXT_SELECT_BOTTOM_HORIZON);
            bottomHorizonBrowseButton.addActionListener(new ButtonListener());
        }

        landmark3DHorizonsParametersPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Horizon Parameters", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));
        /*
        landmark3DHorizonsParametersTable.setFont(new java.awt.Font("Arial", 0, 11));
        landmark3DHorizonsParametersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"Top", "", "", ""},
                {"Bottom", "", "", ""}
            },
            new String [] {
                "Horizon", "Bias", "Search Above", "Search Below"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });*/
        landmark3DHorizonsParametersScrollPane.setViewportView(landmark3DHorizonsParametersTable);

        org.jdesktop.layout.GroupLayout horizonsParametersPanelLayout = new org.jdesktop.layout.GroupLayout(landmark3DHorizonsParametersPanel);
        landmark3DHorizonsParametersPanel.setLayout(horizonsParametersPanelLayout);
        horizonsParametersPanelLayout.setHorizontalGroup(
            horizonsParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsParametersPanelLayout.createSequentialGroup()
                .add(landmark3DHorizonsParametersScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(300, Short.MAX_VALUE))
        );
        horizonsParametersPanelLayout.setVerticalGroup(
            horizonsParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmark3DHorizonsParametersScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 55, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
        if(landmarkHorizonRadioButton.isSelected())
            formLandmark3DEdgeModeHorizonInPanel();
        else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected())
            formBhpsuEdgeModeHorizonInPanel();
        horizonDataPanel.getAccessibleContext().setAccessibleName("Horizons");
    }

    private void formLandmark2DEdgeModeHorizonPanel() {
        //horizonDataPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Horizons", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));
        horizonDataPanel.setBorder(javax.swing.BorderFactory.createRaisedBevelBorder());
        edgeModeRadioButton.setSelected(true);
        edgeModeRadioButton.setText("Edge Mode (2 Horizons)");
        edgeModeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        edgeModeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        pointModeRadioButton.setText("Point Mode (1 Horizon)");
        pointModeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        pointModeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        topHorizonLabel.setText("Top Horizon");
        topHorizonBrowseButton = new javax.swing.JButton();
        topHorizonBrowseButton.setText("Browse");

        if(landmarkHorizonRadioButton.isSelected()){
            final AmplitudeExtraction comp = this;
            topHorizonBrowseButton.addActionListener(new ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    int projectType = 0;
                    if(landmarkAgent == null)
                        landmarkAgent = LandmarkServices.getInstance();
                    if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                        if(landmarkInProjectTextField.getText() == null  || landmarkInProjectTextField.getText().trim().length() == 0){
                            JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                        projectType = landmarkAgent.getLandmarkProjectType(landmarkInProjectTextField.getText().trim());

                    }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                        if(landmarkIn2DProjectTextField.getText() == null  || landmarkIn2DProjectTextField.getText().trim().length() == 0){
                            JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                        projectType = landmarkAgent.getLandmarkProjectType(landmarkIn2DProjectTextField.getText().trim());
                        ListModel lm = selectedSeismic2DLineList.getModel();
                        if(lm.getSize() == 0){
                            JOptionPane.showMessageDialog(comp, "Must have line(s) in the " + "\"" +"Selected Lines:" + "\"" + " list box." ,
                                    "No Landmark 2D lines Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                    }else if(bhpsuInRadioButton.isSelected()){
                        JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                    }
                    if(projectType < 2 || projectType > 3){
                        JOptionPane.showMessageDialog(comp, "Invalid project type found.",
                            "Invalid Project Type", JOptionPane.WARNING_MESSAGE);
                        int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                        ampExtTabbedPane.setSelectedIndex(tabIdx);
                        return;
                    }
                    if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                        agent.openLandmarkHorizonDialog(landmarkInProjectTextField.getText().trim(),projectType,comp,landmark3DTopHorizonTextField);
                    }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                        //agent.openLandmarkHorizonDialog(landmarkIn2DProjectTextField.getText().trim(),projectType,comp,landmark2DTopHorizonTextField);
                        ListModel lm = selectedSeismic2DLineList.getModel();
                        String [] lines = new String[lm.getSize()];
                        for(int i = 0; i < lm.getSize(); i++){
                            lines[i] = (String)lm.getElementAt(i);
                        }
                        agent.openLandmark2DHorizonDialog(landmarkIn2DProjectTextField.getText().trim(),projectType,lines, comp,landmark2DTopHorizonTextField);
                    }
                }
            });
        }else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            topHorizonBrowseButton.setActionCommand(AmpExtConstants.AMP_EXT_SELECT_TOP_HORIZON);
            topHorizonBrowseButton.addActionListener(new ButtonListener());
        }

        bottomHorizonLabel.setText("Bottom Horizon");
        bottomHorizonBrowseButton = new javax.swing.JButton();
        bottomHorizonBrowseButton.setText("Browse");

        if(landmarkHorizonRadioButton.isSelected()){
            final AmplitudeExtraction comp = this;
            bottomHorizonBrowseButton.addActionListener(new ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    int projectType = 0;
                    if(landmarkAgent == null)
                        landmarkAgent = LandmarkServices.getInstance();
                    if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                        if(landmarkInProjectTextField.getText() == null  || landmarkInProjectTextField.getText().trim().length() == 0){
                            JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                        projectType = landmarkAgent.getLandmarkProjectType(landmarkInProjectTextField.getText().trim());
                    }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                        if(landmarkIn2DProjectTextField.getText() == null  || landmarkIn2DProjectTextField.getText().trim().length() == 0){
                            JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                        projectType = landmarkAgent.getLandmarkProjectType(landmarkIn2DProjectTextField.getText().trim());
                        ListModel lm = selectedSeismic2DLineList.getModel();
                        if(lm.getSize() == 0){
                            JOptionPane.showMessageDialog(comp, "Must have line(s) in the " + "\"" +"Selected Lines:" + "\"" + " list box." ,
                                    "No Landmark 2D lines Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                    }else if(bhpsuInRadioButton.isSelected()){
                        JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                    }
                    if(projectType < 2 || projectType > 3){
                        JOptionPane.showMessageDialog(comp, "Invalid project type found.",
                            "Invalid Project Type", JOptionPane.WARNING_MESSAGE);
                        int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                        ampExtTabbedPane.setSelectedIndex(tabIdx);
                        return;
                    }
                    if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                        agent.openLandmarkHorizonDialog(landmarkInProjectTextField.getText().trim(),projectType,comp,landmark3DBottomHorizonTextField);
                    }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                        //agent.openLandmarkHorizonDialog(landmarkIn2DProjectTextField.getText().trim(),projectType,comp,landmark2DBottomHorizonTextField);
                         ListModel lm = selectedSeismic2DLineList.getModel();
                         String [] lines = new String[lm.getSize()];
                         for(int i = 0; i < lm.getSize(); i++){
                            lines[i] = (String)lm.getElementAt(i);
                         }
                         agent.openLandmark2DHorizonDialog(landmarkIn2DProjectTextField.getText().trim(),projectType,lines, comp,landmark2DBottomHorizonTextField);
                    }
                }
            });
        }else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            bottomHorizonBrowseButton.setActionCommand(AmpExtConstants.AMP_EXT_SELECT_BOTTOM_HORIZON);
            bottomHorizonBrowseButton.addActionListener(new ButtonListener());
        }

        landmark2DHorizonsParametersPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Horizon Parameters", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));
        /*
        landmark2DHorizonsParametersTable.setFont(new java.awt.Font("Arial", 0, 11));
        landmark2DHorizonsParametersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"Top", "", "", ""},
                {"Bottom", "", "", ""}
            },
            new String [] {
                "Horizon", "Bias", "Search Above", "Search Below"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        */
        landmark2DHorizonsParametersScrollPane.setViewportView(landmark2DHorizonsParametersTable);

        org.jdesktop.layout.GroupLayout horizonsParametersPanelLayout = new org.jdesktop.layout.GroupLayout(landmark2DHorizonsParametersPanel);
        landmark2DHorizonsParametersPanel.setLayout(horizonsParametersPanelLayout);
        horizonsParametersPanelLayout.setHorizontalGroup(
            horizonsParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsParametersPanelLayout.createSequentialGroup()
                .add(landmark2DHorizonsParametersScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(300, Short.MAX_VALUE))
        );
        horizonsParametersPanelLayout.setVerticalGroup(
            horizonsParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmark2DHorizonsParametersScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 55, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
        if(landmarkHorizonRadioButton.isSelected())
            formLandmark2DEdgeModeHorizonInPanel();
        else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected())
            formBhpsuEdgeModeHorizonInPanel();
        horizonDataPanel.getAccessibleContext().setAccessibleName("Horizons");
    }

    private void formLandmark3DEdgeModeHorizonInPanel(){
        org.jdesktop.layout.GroupLayout horizonsPanelLayout = new org.jdesktop.layout.GroupLayout(horizonDataPanel);
        horizonDataPanel.setLayout(horizonsPanelLayout);
        horizonsPanelLayout.setHorizontalGroup(
            horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmark3DHorizonsParametersPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(horizonsPanelLayout.createSequentialGroup()
                        .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(edgeModeRadioButton)
                            .add(topHorizonLabel)
                            .add(bottomHorizonLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(horizonsPanelLayout.createSequentialGroup()
                                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(landmark3DTopHorizonTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 291, Short.MAX_VALUE)
                                    .add(landmark3DBottomHorizonTextField,org. jdesktop.layout.GroupLayout.DEFAULT_SIZE, 291, Short.MAX_VALUE))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(bottomHorizonBrowseButton)
                                    .add(topHorizonBrowseButton)))
                            .add(pointModeRadioButton))))
                .addContainerGap())
        );
        horizonsPanelLayout.setVerticalGroup(
            horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(edgeModeRadioButton)
                    .add(pointModeRadioButton))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(16,16,16)
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(topHorizonLabel)
                    .add(landmark3DTopHorizonTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(topHorizonBrowseButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(bottomHorizonLabel)
                    .add(landmark3DBottomHorizonTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(bottomHorizonBrowseButton))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(16,16,16)
                .add(landmark3DHorizonsParametersPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

    }

    private void formLandmark2DEdgeModeHorizonInPanel(){
        org.jdesktop.layout.GroupLayout horizonsPanelLayout = new org.jdesktop.layout.GroupLayout(horizonDataPanel);
        horizonDataPanel.setLayout(horizonsPanelLayout);
        horizonsPanelLayout.setHorizontalGroup(
            horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmark2DHorizonsParametersPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(horizonsPanelLayout.createSequentialGroup()
                        .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(edgeModeRadioButton)
                            .add(topHorizonLabel)
                            .add(bottomHorizonLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(horizonsPanelLayout.createSequentialGroup()
                                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(landmark2DTopHorizonTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 291, Short.MAX_VALUE)
                                    .add(landmark2DBottomHorizonTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 291, Short.MAX_VALUE))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(bottomHorizonBrowseButton)
                                    .add(topHorizonBrowseButton)))
                            .add(pointModeRadioButton))))
                .addContainerGap())
        );
        horizonsPanelLayout.setVerticalGroup(
            horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(edgeModeRadioButton)
                    .add(pointModeRadioButton))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(16,16,16)
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(topHorizonLabel)
                    .add(landmark2DTopHorizonTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(topHorizonBrowseButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(bottomHorizonLabel)
                    .add(landmark2DBottomHorizonTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(bottomHorizonBrowseButton))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(16,16,16)
                .add(landmark2DHorizonsParametersPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }

    private void formBhpsuEdgeModeHorizonInPanel(){
        org.jdesktop.layout.GroupLayout horizonsPanelLayout = new org.jdesktop.layout.GroupLayout(horizonDataPanel);
        horizonDataPanel.setLayout(horizonsPanelLayout);
        horizonsPanelLayout.setHorizontalGroup(
            horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(horizonsParametersPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(horizonsPanelLayout.createSequentialGroup()
                        .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(edgeModeRadioButton)
                            .add(topHorizonLabel)
                            .add(bottomHorizonLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(horizonsPanelLayout.createSequentialGroup()
                                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(bhpsuTopHorizonTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 291, Short.MAX_VALUE)
                                    .add(bhpsuBottomHorizonTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 291, Short.MAX_VALUE))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(bottomHorizonBrowseButton)
                                    .add(topHorizonBrowseButton)))
                            .add(pointModeRadioButton))))
                .addContainerGap())
        );
        horizonsPanelLayout.setVerticalGroup(
            horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(edgeModeRadioButton)
                    .add(pointModeRadioButton))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(16,16,16)
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(topHorizonLabel)
                    .add(bhpsuTopHorizonTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(topHorizonBrowseButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(bottomHorizonLabel)
                    .add(bhpsuBottomHorizonTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(bottomHorizonBrowseButton))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(16,16,16)
                .add(horizonsParametersPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

    }

    private void formOutputPanel(){
        outputPanel = new javax.swing.JPanel();
        outputPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Output", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));
        landmarkOutputRadioButton.setText("Landmark & BHP SU");
        landmarkOutputRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        landmarkOutputRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        bhpsuOutputRadioButton.setText("BHP SU");
        bhpsuOutputRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        bhpsuOutputRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        org.jdesktop.layout.GroupLayout outputPanelLayout = new org.jdesktop.layout.GroupLayout(outputPanel);
        outputPanel.setLayout(outputPanelLayout);
        outputPanelLayout.setHorizontalGroup(
            outputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(outputPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkOutputRadioButton)
                .add(21, 21, 21)
                .add(bhpsuOutputRadioButton)
                .addContainerGap(464, Short.MAX_VALUE))
            .add(outputDataPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        outputPanelLayout.setVerticalGroup(
            outputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(outputPanelLayout.createSequentialGroup()
                //.addContainerGap()
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(outputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkOutputRadioButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 15, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(bhpsuOutputRadioButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                //.addContainerGap()
                .add(outputDataPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE))
        );
    }
    private void formBhpsuInputDataPanel(){
        inputDataPanel = new javax.swing.JPanel();
        inputDataPanel.setBorder(javax.swing.BorderFactory.createRaisedBevelBorder());
        //inputDataPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        org.jdesktop.layout.GroupLayout inputDataPanelLayout = new org.jdesktop.layout.GroupLayout(inputDataPanel);
        inputDataPanel.setLayout(inputDataPanelLayout);
        inputDataPanelLayout.setHorizontalGroup(
            inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(inputDataPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(rangeLabel)
                    .add(inputCubeLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED,20,20)
                .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(inputDataPanelLayout.createSequentialGroup()
                        .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(inputDataPanelLayout.createSequentialGroup()
                                .add(23, 23, 23)
                                .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(epFromLabel)
                                    .add(cdpFromLabel))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(epFromTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 66, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(cdpFromTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 66, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .add(16, 16, 16))
                            .add(inputDataPanelLayout.createSequentialGroup()
                                .add(13, 13, 13)
                                .add(entireCubeRangeButton)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)))
                        .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(inputDataPanelLayout.createSequentialGroup()
                                .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(epToLabel)
                                    .add(cdpToLabel))
                                .add(20, 20, 20)
                                .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(epToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 66, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(cdpToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 66, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .add(12, 12, 12)
                                .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(cdpByLabel1)
                                    .add(cdpByLabel))
                                .add(14, 14, 14)
                                .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(cdpByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 66, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(epByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 66, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                            .add(pickRangeFromViewerButton))
                        .add(54, 54, 54))
                    .add(inputDataPanelLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(inputCubeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 370, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)))
                .add(7, 7, 7)
                .add(inputCubeBrowseButton)
                .add(109, 109, 109))
        );
        inputDataPanelLayout.setVerticalGroup(
            inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(inputDataPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(inputCubeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(inputCubeLabel))
                    .add(inputCubeBrowseButton))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(12, 12, 12)
                .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(inputDataPanelLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, cdpFromLabel)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, cdpFromTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, cdpToLabel)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, cdpToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, cdpByLabel)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, cdpByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(12, 12, 12)
                        .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(epFromLabel)
                                .add(epFromTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(epToLabel)
                                .add(epToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(cdpByLabel1)
                                .add(epByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                    .add(inputDataPanelLayout.createSequentialGroup()
                        .add(17, 17, 17)
                        .add(rangeLabel)))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(18, 18, 18)
                .add(inputDataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(entireCubeRangeButton)
                    .add(pickRangeFromViewerButton))
                    .addContainerGap(183, Short.MAX_VALUE))
        );

    }
    /**
     * Form the SU point mode Horizons panel.
     */
    private void formPointModeHorizonPanel() {
        //horizonDataPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Horizon", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));
        horizonDataPanel.setBorder(javax.swing.BorderFactory.createRaisedBevelBorder());
        edgeModeRadioButton.setText("Edge Mode (2 Horizons)");
        edgeModeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        edgeModeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        pointModeRadioButton.setSelected(true);
        pointModeRadioButton.setText("Point Mode (1 Horizon)");
        pointModeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        pointModeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        horizonLabel.setText("Horizon");
        horizonBrowseButton = new javax.swing.JButton();
        horizonBrowseButton.setText("Browse");

        horizonBrowseButton.setActionCommand(AmpExtConstants.AMP_EXT_SELECT_HORIZON);
        horizonBrowseButton.addActionListener(new ButtonListener());

        horizonsParametersPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Horizon Parameters", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));

        horizonParametersTable.setFont(new java.awt.Font("Arial", 0, 11));
        horizonParametersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "", ""}
            },
            new String [] {
                    TABLE_HEADING_BIAS, TABLE_HEADING_SEARCH_ABOVE, TABLE_HEADING_SEARCH_BELOW
            }
        ) {
            boolean[] canEdit = new boolean [] {
                    true, true, true
                };

                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit [columnIndex];
                }
            });

        horizonsParametersScrollPane.setViewportView(horizonParametersTable);

        org.jdesktop.layout.GroupLayout horizonsParametersPanelLayout = new org.jdesktop.layout.GroupLayout(horizonsParametersPanel);
        horizonsParametersPanel.setLayout(horizonsParametersPanelLayout);
        horizonsParametersPanelLayout.setHorizontalGroup(
            horizonsParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsParametersPanelLayout.createSequentialGroup()
                .add(horizonsParametersScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(300, Short.MAX_VALUE))
        );
        horizonsParametersPanelLayout.setVerticalGroup(
            horizonsParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsParametersPanelLayout.createSequentialGroup()
                .add(horizonsParametersScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
                .addContainerGap())
        );

        formBhpsuPointModeHorizonInPanel();
    }

    private void formLandmark3DPointModeHorizonPanel(){
        //horizonDataPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Horizon", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));
        horizonDataPanel.setBorder(javax.swing.BorderFactory.createRaisedBevelBorder());
        edgeModeRadioButton.setText("Edge Mode (2 Horizons)");
        edgeModeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        edgeModeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        pointModeRadioButton.setSelected(true);
        pointModeRadioButton.setText("Point Mode (1 Horizon)");
        pointModeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        pointModeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        horizonLabel.setText("Horizon");
        horizonBrowseButton = new javax.swing.JButton();
        horizonBrowseButton.setText("Browse");


        if(landmarkHorizonRadioButton.isSelected()){
            final AmplitudeExtraction comp = this;
            horizonBrowseButton.addActionListener(new ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    int projectType = 0;
                    if(landmarkAgent == null)
                        landmarkAgent = LandmarkServices.getInstance();

                    if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                        if(landmarkInProjectTextField.getText() == null  || landmarkInProjectTextField.getText().trim().length() == 0){
                            JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                        projectType = landmarkAgent.getLandmarkProjectType(landmarkInProjectTextField.getText().trim());

                    }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                        if(landmarkIn2DProjectTextField.getText() == null  || landmarkIn2DProjectTextField.getText().trim().length() == 0){
                            JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                        projectType = landmarkAgent.getLandmarkProjectType(landmarkIn2DProjectTextField.getText().trim());
                        ListModel lm = selectedSeismic2DLineList.getModel();
                        if(lm.getSize() == 0){
                            JOptionPane.showMessageDialog(comp, "Must have line(s) in the " + "\"" +"Selected Lines:" + "\"" + " list box." ,
                                    "No Landmark 2D lines Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                    }else if(bhpsuInRadioButton.isSelected()){
                        JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                    }
                    if(projectType < 2 || projectType > 3){
                        JOptionPane.showMessageDialog(comp, "Invalid project type found.",
                            "Invalid Project Type", JOptionPane.WARNING_MESSAGE);
                        int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                        ampExtTabbedPane.setSelectedIndex(tabIdx);
                        return;
                    }
                    if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                        agent.openLandmarkHorizonDialog(landmarkInProjectTextField.getText().trim(),projectType,comp,landmark3DHorizonTextField);
                    }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                        //agent.openLandmarkHorizonDialog(landmarkIn2DProjectTextField.getText().trim(),projectType,comp,landmark2DHorizonTextField);
                        ListModel lm = selectedSeismic2DLineList.getModel();
                        String [] lines = new String[lm.getSize()];
                        for(int i = 0; i < lm.getSize(); i++){
                            lines[i] = (String)lm.getElementAt(i);
                        }
                        agent.openLandmark2DHorizonDialog(landmarkIn2DProjectTextField.getText().trim(),projectType,lines, comp,landmark2DHorizonTextField);
                    }
                }
            });
        }else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            horizonBrowseButton.setActionCommand(AmpExtConstants.AMP_EXT_SELECT_HORIZON);
            horizonBrowseButton.addActionListener(new ButtonListener());

        }


        landmark3DHorizonsParametersPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Horizon Parameters", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));
        /*landmark3DHorizonParametersTable.setFont(new java.awt.Font("Arial", 0, 11));
        landmark3DHorizonParametersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "", ""}
            },
            new String [] {
                "Bias", "Search Above", "Search Below"
            }
        ));
        */
        landmark3DHorizonsParametersScrollPane.setViewportView(landmark3DHorizonParametersTable);

        org.jdesktop.layout.GroupLayout horizonsParametersPanelLayout = new org.jdesktop.layout.GroupLayout(landmark3DHorizonsParametersPanel);
        landmark3DHorizonsParametersPanel.setLayout(horizonsParametersPanelLayout);
        horizonsParametersPanelLayout.setHorizontalGroup(
            horizonsParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsParametersPanelLayout.createSequentialGroup()
                .add(landmark3DHorizonsParametersScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(300, Short.MAX_VALUE))
        );
        horizonsParametersPanelLayout.setVerticalGroup(
            horizonsParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsParametersPanelLayout.createSequentialGroup()
                .add(landmark3DHorizonsParametersScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
                .addContainerGap())
        );

        if(landmarkHorizonRadioButton.isSelected())
            formLandmark3DPointModeHorizonInPanel();
        else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected())
            formBhpsuPointModeHorizonInPanel();
   }

    private void formLandmark2DPointModeHorizonPanel(){
        //horizonDataPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Horizon", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));
        horizonDataPanel.setBorder(javax.swing.BorderFactory.createRaisedBevelBorder());
        edgeModeRadioButton.setText("Edge Mode (2 Horizons)");
        edgeModeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        edgeModeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        pointModeRadioButton.setSelected(true);
        pointModeRadioButton.setText("Point Mode (1 Horizon)");
        pointModeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        pointModeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        horizonLabel.setText("Horizon");
        horizonBrowseButton = new javax.swing.JButton();
        horizonBrowseButton.setText("Browse");


        if(landmarkHorizonRadioButton.isSelected()){
            final AmplitudeExtraction comp = this;
            horizonBrowseButton.addActionListener(new ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    int projectType = 0;
                    if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                        if(landmarkInProjectTextField.getText() == null  || landmarkInProjectTextField.getText().trim().length() == 0){
                            JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                        projectType = landmarkAgent.getLandmarkProjectType(landmarkInProjectTextField.getText().trim());

                    }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                        if(landmarkIn2DProjectTextField.getText() == null  || landmarkIn2DProjectTextField.getText().trim().length() == 0){
                            JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                        projectType = landmarkAgent.getLandmarkProjectType(landmarkIn2DProjectTextField.getText().trim());
                        ListModel lm = selectedSeismic2DLineList.getModel();
                        if(lm.getSize() == 0){
                            JOptionPane.showMessageDialog(comp, "Must have line(s) in the " + "\"" +"Selected Lines:" + "\"" + " list box." ,
                                    "No Landmark 2D lines Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                        }
                    }else if(bhpsuInRadioButton.isSelected()){
                        JOptionPane.showMessageDialog(comp, "A Landmark project has to be set first.",
                                "No Landmark Project Found", JOptionPane.WARNING_MESSAGE);
                            int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                            ampExtTabbedPane.setSelectedIndex(tabIdx);
                            return;
                    }
                    if(projectType < 2 || projectType > 3){
                        JOptionPane.showMessageDialog(comp, "Invalid project type found.",
                            "Invalid Project Type", JOptionPane.WARNING_MESSAGE);
                        int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
                        ampExtTabbedPane.setSelectedIndex(tabIdx);
                        return;
                    }
                    if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                        agent.openLandmarkHorizonDialog(landmarkInProjectTextField.getText().trim(),projectType,comp,landmark2DHorizonTextField);
                    }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                        //agent.openLandmarkHorizonDialog(landmarkIn2DProjectTextField.getText().trim(),projectType,comp,landmark2DHorizonTextField);
                        ListModel lm = selectedSeismic2DLineList.getModel();
                        String [] lines = new String[lm.getSize()];
                        for(int i = 0; i < lm.getSize(); i++){
                            lines[i] = (String)lm.getElementAt(i);
                        }
                        agent.openLandmark2DHorizonDialog(landmarkIn2DProjectTextField.getText().trim(),projectType,lines, comp,landmark2DHorizonTextField);
                    }
                }
            });
        }else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            horizonBrowseButton.setActionCommand(AmpExtConstants.AMP_EXT_SELECT_HORIZON);
            horizonBrowseButton.addActionListener(new ButtonListener());

        }


        landmark2DHorizonsParametersPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Horizon Parameters", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));
        /*landmark2DHorizonParametersTable.setFont(new java.awt.Font("Arial", 0, 11));
        landmark2DHorizonParametersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "", ""}
            },
            new String [] {
                "Bias", "Search Above", "Search Below"
            }
        ));
        */
        landmark2DHorizonsParametersScrollPane.setViewportView(landmark2DHorizonParametersTable);

        org.jdesktop.layout.GroupLayout horizonsParametersPanelLayout = new org.jdesktop.layout.GroupLayout(landmark2DHorizonsParametersPanel);
        landmark2DHorizonsParametersPanel.setLayout(horizonsParametersPanelLayout);
        horizonsParametersPanelLayout.setHorizontalGroup(
            horizonsParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsParametersPanelLayout.createSequentialGroup()
                .add(landmark2DHorizonsParametersScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(300, Short.MAX_VALUE))
        );
        horizonsParametersPanelLayout.setVerticalGroup(
            horizonsParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsParametersPanelLayout.createSequentialGroup()
                .add(landmark2DHorizonsParametersScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
                .addContainerGap())
        );

        if(landmarkHorizonRadioButton.isSelected())
            formLandmark2DPointModeHorizonInPanel();
        else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected())
            formBhpsuPointModeHorizonInPanel();

   }

    private void formLandmark3DPointModeHorizonInPanel(){

        org.jdesktop.layout.GroupLayout horizonsPanelLayout = new org.jdesktop.layout.GroupLayout(horizonDataPanel);
        horizonDataPanel.setLayout(horizonsPanelLayout);
        horizonsPanelLayout.setHorizontalGroup(
            horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmark3DHorizonsParametersPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(horizonsPanelLayout.createSequentialGroup()
                        .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(edgeModeRadioButton)
                            .add(horizonLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(horizonsPanelLayout.createSequentialGroup()
                                .add(landmark3DHorizonTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 291, Short.MAX_VALUE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(horizonBrowseButton))
                            .add(pointModeRadioButton))))
                .addContainerGap())
        );
        horizonsPanelLayout.setVerticalGroup(
            horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(edgeModeRadioButton)
                    .add(pointModeRadioButton))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(16,16,16)
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(horizonLabel)
                    .add(landmark3DHorizonTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(horizonBrowseButton))
                .add(26, 26, 26)
                .add(landmark3DHorizonsParametersPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

    }

    private void formLandmark2DPointModeHorizonInPanel(){

        org.jdesktop.layout.GroupLayout horizonsPanelLayout = new org.jdesktop.layout.GroupLayout(horizonDataPanel);
        horizonDataPanel.setLayout(horizonsPanelLayout);
        horizonsPanelLayout.setHorizontalGroup(
            horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmark2DHorizonsParametersPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(horizonsPanelLayout.createSequentialGroup()
                        .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(edgeModeRadioButton)
                            .add(horizonLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(horizonsPanelLayout.createSequentialGroup()
                                .add(landmark2DHorizonTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 291, Short.MAX_VALUE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(horizonBrowseButton))
                            .add(pointModeRadioButton))))
                .addContainerGap())
        );
        horizonsPanelLayout.setVerticalGroup(
            horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(edgeModeRadioButton)
                    .add(pointModeRadioButton))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(16,16,16)
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(horizonLabel)
                    .add(landmark2DHorizonTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(horizonBrowseButton))
                .add(26, 26, 26)
                .add(landmark2DHorizonsParametersPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

    }

    private void formBhpsuPointModeHorizonInPanel(){
        org.jdesktop.layout.GroupLayout horizonsPanelLayout = new org.jdesktop.layout.GroupLayout(horizonDataPanel);
        horizonDataPanel.setLayout(horizonsPanelLayout);
        horizonsPanelLayout.setHorizontalGroup(
            horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(horizonsParametersPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(horizonsPanelLayout.createSequentialGroup()
                        .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(edgeModeRadioButton)
                            .add(horizonLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(horizonsPanelLayout.createSequentialGroup()
                                .add(bhpsuHorizonTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 291, Short.MAX_VALUE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(horizonBrowseButton))
                            .add(pointModeRadioButton))))
                .addContainerGap())
        );
        horizonsPanelLayout.setVerticalGroup(
            horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(edgeModeRadioButton)
                    .add(pointModeRadioButton))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(16,16,16)
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(horizonLabel)
                    .add(bhpsuHorizonTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(horizonBrowseButton))
                .add(26, 26, 26)
                .add(horizonsParametersPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

    }
    /**
     * Layout parameters tabbed pane
     */
    private void layoutParametersTabbedPane(javax.swing.JPanel targetPanel) {
        org.jdesktop.layout.GroupLayout parametersPanelLayout = new org.jdesktop.layout.GroupLayout(parametersPanel);
        parametersPanel.setLayout(parametersPanelLayout);
        parametersPanelLayout.setHorizontalGroup(
            parametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(parametersPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(parametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    //.add(org.jdesktop.layout.GroupLayout.TRAILING, inputPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    //.add(outputPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(horizonsPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, targetPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        parametersPanelLayout.setVerticalGroup(
            parametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(parametersPanelLayout.createSequentialGroup()
                .addContainerGap()
                //.add(inputPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                //.add(outputPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(horizonsPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(targetPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .addContainerGap(39, Short.MAX_VALUE))
        );
    }

    /**
     * Layout input parameters tabbed pane
     */
    private void layoutInputParametersTabbedPane() {
        org.jdesktop.layout.GroupLayout inputParametersPanelLayout = new org.jdesktop.layout.GroupLayout(inputParametersPanel);
        inputParametersPanel.setLayout(inputParametersPanelLayout);
        inputParametersPanelLayout.setHorizontalGroup(
                inputParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(inputParametersPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(inputParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(inputPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    )
                .addContainerGap())
        );
        inputParametersPanelLayout.setVerticalGroup(
                inputParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(inputParametersPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(inputPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .addContainerGap())
        );
    }

    /**
     * Set listener on edge mode horizon parameters table to catch when cells edited and capture value entered
     */
    private void addEdgeModeHorizonParamsListener(TableModel model) {
        //Capture changes to table cells
        model.addTableModelListener(new TableModelListener() {
            public void tableChanged(TableModelEvent tme) {
                int row = tme.getFirstRow();
                int column = tme.getColumn();
                if(column == -1 || row == -1)
                    return;
                TableModel model = (TableModel)tme.getSource();
                String columnName = model.getColumnName(column);
                String val = (String)model.getValueAt(row, column);
                if (val == null) val = "";
                if (row == 0) {
                    if (columnName.equals(TABLE_HEADING_BIAS)) topHorizonBias = val;
                    else if (columnName.equals(TABLE_HEADING_SEARCH_ABOVE)) topHorizonAbove = val;
                    else if (columnName.equals(TABLE_HEADING_SEARCH_BELOW)) topHorizonBelow = val;
                } else {
                    if (columnName.equals(TABLE_HEADING_BIAS)) bottomHorizonBias = val;
                    else if (columnName.equals(TABLE_HEADING_SEARCH_ABOVE)) bottomHorizonAbove = val;
                    else if (columnName.equals(TABLE_HEADING_SEARCH_BELOW)) bottomHorizonBelow = val;
                }

                //save setting
                edgeModeHorizonSettings[row][column-1] = val;
            }
        });
    }

    /**
     * Set listener on edge mode horizon parameters table to catch when cells edited and capture value entered
     */
    private void addLandmark3DEdgeModeHorizonParamsListener(TableModel model) {
        //Capture changes to table cells
        model.addTableModelListener(new TableModelListener() {
            public void tableChanged(TableModelEvent tme) {
                int row = tme.getFirstRow();
                int column = tme.getColumn();
                if(column == -1 || row == -1)
                    return;
                TableModel model = (TableModel)tme.getSource();
                String columnName = model.getColumnName(column);
                String val = (String)model.getValueAt(row, column);
                if (val == null) val = "";
                if (row == 0) {
                    if (columnName.equals(TABLE_HEADING_BIAS)) landmark3DTopHorizonBias = val;
                    else if (columnName.equals(TABLE_HEADING_SEARCH_ABOVE)) landmark3DTopHorizonAbove = val;
                    else if (columnName.equals(TABLE_HEADING_SEARCH_BELOW)) landmark3DTopHorizonBelow = val;
                } else {
                    if (columnName.equals(TABLE_HEADING_BIAS)) landmark3DBottomHorizonBias = val;
                    else if (columnName.equals(TABLE_HEADING_SEARCH_ABOVE)) landmark3DBottomHorizonAbove = val;
                    else if (columnName.equals(TABLE_HEADING_SEARCH_BELOW)) landmark3DBottomHorizonBelow = val;
                }

                //save setting
                landmark3DEdgeModeHorizonSettings[row][column-1] = val;
            }
        });
    }

    /**
     * Set listener on edge mode horizon parameters table to catch when cells edited and capture value entered
     */
    private void addLandmark2DEdgeModeHorizonParamsListener(TableModel model) {
        //Capture changes to table cells
        model.addTableModelListener(new TableModelListener() {
            public void tableChanged(TableModelEvent tme) {
                int row = tme.getFirstRow();
                int column = tme.getColumn();
                if(column == -1 || row == -1)
                    return;
                TableModel model = (TableModel)tme.getSource();
                String columnName = model.getColumnName(column);
                String val = (String)model.getValueAt(row, column);
                if (val == null) val = "";
                if (row == 0) {
                    if (columnName.equals(TABLE_HEADING_BIAS)) landmark2DTopHorizonBias = val;
                    else if (columnName.equals(TABLE_HEADING_SEARCH_ABOVE)) landmark2DTopHorizonAbove = val;
                    else if (columnName.equals(TABLE_HEADING_SEARCH_BELOW)) landmark2DTopHorizonBelow = val;
                } else {
                    if (columnName.equals(TABLE_HEADING_BIAS)) landmark2DBottomHorizonBias = val;
                    else if (columnName.equals(TABLE_HEADING_SEARCH_ABOVE)) landmark2DBottomHorizonAbove = val;
                    else if (columnName.equals(TABLE_HEADING_SEARCH_BELOW)) landmark2DBottomHorizonBelow = val;
                }

                //save setting
                landmark2DEdgeModeHorizonSettings[row][column-1] = val;
            }
        });
    }

    /**
     * Set listener on point mode horizon parameters table to catch when cells edited and capture value entered
     */
    private void addPointModeHorizonParamsListener(TableModel model) {
        model.addTableModelListener(new TableModelListener() {
            public void tableChanged(TableModelEvent tme) {
                int row = tme.getFirstRow();    //has to be 0
                int column = tme.getColumn();
                if(column == -1 || row == -1)
                    return;
                TableModel model = (TableModel)tme.getSource();
                String columnName = model.getColumnName(column);
                String val = (String)model.getValueAt(row, column);
                if (val == null) val = "";
                if (columnName.equals(TABLE_HEADING_BIAS)) horizonBias = val;
                else if (columnName.equals(TABLE_HEADING_SEARCH_ABOVE)) horizonAbove = val;
                else if (columnName.equals(TABLE_HEADING_SEARCH_BELOW)) horizonBelow = val;

                //save setting
                pointModeHorizonSettings[row][column] = val;
            }
        });
    }

    /**
     * Set listener on point mode horizon parameters table to catch when cells edited and capture value entered
     */
    private void addLandmark3DPointModeHorizonParamsListener(TableModel model) {
        model.addTableModelListener(new TableModelListener() {
            public void tableChanged(TableModelEvent tme) {
                int row = tme.getFirstRow();    //has to be 0
                int column = tme.getColumn();
                if(column == -1 || row == -1)
                    return;
                TableModel model = (TableModel)tme.getSource();
                String columnName = model.getColumnName(column);
                String val = (String)model.getValueAt(row, column);
                if (val == null) val = "";
                if (columnName.equals(TABLE_HEADING_BIAS)) landmark3DHorizonBias = val;
                else if (columnName.equals(TABLE_HEADING_SEARCH_ABOVE)) landmark3DHorizonAbove = val;
                else if (columnName.equals(TABLE_HEADING_SEARCH_BELOW)) landmark3DHorizonBelow = val;

                //save setting
                landmark3DPointModeHorizonSettings[row][column] = val;
            }
        });
    }

    private void addLandmark2DPointModeHorizonParamsListener(TableModel model) {
        model.addTableModelListener(new TableModelListener() {
            public void tableChanged(TableModelEvent tme) {
                int row = tme.getFirstRow();    //has to be 0
                int column = tme.getColumn();
                if(column == -1 || row == -1)
                    return;
                TableModel model = (TableModel)tme.getSource();
                String columnName = model.getColumnName(column);
                String val = (String)model.getValueAt(row, column);
                if (val == null) val = "";
                if (columnName.equals(TABLE_HEADING_BIAS)) landmark2DHorizonBias = val;
                else if (columnName.equals(TABLE_HEADING_SEARCH_ABOVE)) landmark2DHorizonAbove = val;
                else if (columnName.equals(TABLE_HEADING_SEARCH_BELOW)) landmark2DHorizonBelow = val;

                //save setting
                landmark2DPointModeHorizonSettings[row][column] = val;
            }
        });
    }

    /**
     * Validate ranges. The validation rules are:
     * <p>
     * <ul>
     * <li>A value must be entered for each range
     * <li>From, To and By must be integers
     * <li>From, To and By must be > 0
     * <li>From < To
     * <li>By a multiple of the default
     * </ul>
     * <p>
     * Note: The number of items is (To-From+1)/By even though the remainer is not zero.
     * That is, From is an upper bound and we round down.
     */
    private boolean validateRanges() {
        if(!bhpsuInRadioButton.isSelected())
            return true;
        String errorSource = "", errorCause = "", errorFix = "";

        //check a value entered for each range
        errorSource = "";
        errorCause = MISSING_RANGES_ERROR;
        errorFix = ENTER_VALUES_FIX;
        //check a value entered for each range

        String epFromEntry = epFromTextField.getText();
        String epToEntry = epToTextField.getText();
        String epIncrEntry = epByTextField.getText();
        String cdpFromEntry = cdpFromTextField.getText();
        String cdpToEntry = cdpToTextField.getText();
        String cdpIncrEntry = cdpByTextField.getText();
        if (epFromEntry.equals("") || epToEntry.equals("") || epIncrEntry.equals("") ||
            cdpFromEntry.equals("") || cdpToEntry.equals("") || cdpIncrEntry.equals("")) {
            rangeValidationError(errorSource, errorCause, errorFix);
            return false;
        }

        //check each range value an integer > 0
        int epFrom = 0;
        int epTo = 0;
        int epIncr = 0;
        int cdpFrom = 0;
        int cdpTo = 0;
        int cdpIncr = 0;

        errorCause = NOT_AN_INTEGER_ERROR;
        errorFix = REENTER_VALUE_FIX;
        try {
            errorSource = EP_FROM_SOURCE;
            epFrom = Integer.parseInt(epFromEntry);
            if (epFrom <= 0) {
                errorCause = NOT_POSITIVE_ERROR;
                rangeValidationError(errorSource, errorCause, errorFix);
                return false;
            }
        } catch (NumberFormatException nfe) {
            rangeValidationError(errorSource, errorCause, errorFix);
            return false;
        }
        try {
            errorSource = EP_TO_SOURCE;
            epTo = Integer.parseInt(epToEntry);
            if (epTo <= 0) {
                errorCause = NOT_POSITIVE_ERROR;
                rangeValidationError(errorSource, errorCause, errorFix);
                return false;
            }
        } catch (NumberFormatException nfe) {
            rangeValidationError(errorSource, errorCause, errorFix);
            return false;
        }
        try {
            errorSource = EP_BY_SOURCE;
            epIncr = Integer.parseInt(epIncrEntry);
            if (epIncr <= 0) {
                errorCause = NOT_POSITIVE_ERROR;
                rangeValidationError(errorSource, errorCause, errorFix);
                return false;
            }
        } catch (NumberFormatException nfe) {
            rangeValidationError(errorSource, errorCause, errorFix);
            return false;
        }

        try {
            errorSource = CDP_FROM_SOURCE;
            cdpFrom = Integer.parseInt(cdpFromEntry);
            if (cdpFrom <= 0) {
                errorCause = NOT_POSITIVE_ERROR;
                rangeValidationError(errorSource, errorCause, errorFix);
                return false;
            }
        } catch (NumberFormatException nfe) {
            rangeValidationError(errorSource, errorCause, errorFix);
            return false;
        }
        try {
            errorSource = CDP_TO_SOURCE;
            cdpTo = Integer.parseInt(cdpToEntry);
            if (cdpTo <= 0) {
                errorCause = NOT_POSITIVE_ERROR;
                rangeValidationError(errorSource, errorCause, errorFix);
                return false;
            }
        } catch (NumberFormatException nfe) {
            rangeValidationError(errorSource, errorCause, errorFix);
            return false;
        }
        try {
            errorSource = CDP_BY_SOURCE;
            cdpIncr = Integer.parseInt(cdpIncrEntry);
            if (cdpIncr <= 0) {
                errorCause = NOT_POSITIVE_ERROR;
                rangeValidationError(errorSource, errorCause, errorFix);
                return false;
            }
        } catch (NumberFormatException nfe) {
            rangeValidationError(errorSource, errorCause, errorFix);
            return false;
        }

        //check From < To
        errorCause = FROM_GREATER_TO_ERROR;
        errorFix = REENTER_VALUE_FIX;
        errorSource = EP_SOURCE;
        if (epFrom > epTo) {
            rangeValidationError(errorSource, errorCause, errorFix);
            return false;
        }
        errorSource = CDP_SOURCE;
        if (cdpFrom > cdpTo) {
            rangeValidationError(errorSource, errorCause, errorFix);
            return false;
        }

        //check By a multiple of the default
        errorCause = BY_NOT_MULTIPLE_ERROR;
        errorFix = REENTER_VALUE_FIX;
        errorSource = EP_BY_SOURCE;
        if (epIncr % epIncrDefault != 0) {
            rangeValidationError(errorSource, errorCause, errorFix);
            return false;
        }
        errorSource = CDP_BY_SOURCE;
        if (cdpIncr % cdpIncrDefault != 0) {
            rangeValidationError(errorSource, errorCause, errorFix);
            return false;
        }

        return true;
    }

    private void rangeValidationError(String errorSource, String errorCause, String errorFix) {
        JOptionPane.showMessageDialog(this,
            errorSource + " " + errorCause + ". " + errorFix,
            "Range Error",
            JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Validate text fields, i.e., textual (non-numeric) inputs.
     * All text fields must be non-blank.
     * @return true if validation passed; otherwise, false.
     */
    private boolean validateTextFields() {
        int inputIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);
        int paramIdx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        int outIdx = ampExtTabbedPane.indexOfTab(TN_OUTPUTS);
        int processIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_PROCESS);
        final Component comp = this;
        if(bhpsuInRadioButton.isSelected()){
            if (inputCubeTextField.getText().equals("")) {
                JOptionPane.showMessageDialog(comp, "Select Input Before Saving Script", "Input File Error", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(inputIdx);
                inputCubeBrowseButton.requestFocus();
                return false;
            }
        }

        if(!processPanel.validateDataProcessingFields()){
            ampExtTabbedPane.setSelectedIndex(processIdx);
            return false;
        }

        if(!processPanel.isNotRunAmpExtChecked()){
        if(landmarkOutputRadioButton.isSelected()){
            if(landmarkAgent == null)
                landmarkAgent = LandmarkServices.getInstance();
            if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                if(landmark2DOutProjectTextField == null || landmark2DOutProjectTextField.getText().trim().length() == 0){
                    JOptionPane.showMessageDialog(this, "Must specify a Landmark 2D project for the output volume.", "Landmark project is missing", JOptionPane.WARNING_MESSAGE);
                    ampExtTabbedPane.setSelectedIndex(outIdx);
                    landmarkOutListButton.requestFocus();
                    return false;
                }else{
                    if (!landmarkAgent.isValidLandmarkProject(landmark2DOutProjectTextField.getText().trim())) {
                        JOptionPane.showMessageDialog(this, "Can not find project name " + landmark2DOutProjectTextField.getText().trim()
                                + ". Check to see if it is valid Landmark project", "Project not found",    JOptionPane.WARNING_MESSAGE);
                        ampExtTabbedPane.setSelectedIndex(outIdx);
                        landmark2DOutProjectTextField.requestFocus();
                        return false;
                    }

                    if(!landmarkIn2DProjectTextField.getText().trim().equals(landmark2DOutProjectTextField.getText().trim())){
                        JOptionPane.showMessageDialog(this, "2D Projects mismatch between the input and the output.", "Landmark project mismatches",    JOptionPane.WARNING_MESSAGE);
                        ampExtTabbedPane.setSelectedIndex(outIdx);
                        landmarkOutListButton.requestFocus();
                        return false;
                    }
                }
            }else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                if(landmark3DOutProjectTextField == null || landmark3DOutProjectTextField.getText().trim().length() == 0){
                    JOptionPane.showMessageDialog(this, "Must specify a Landmark 3D project for the output volume.", "Landmark project is missing", JOptionPane.WARNING_MESSAGE);
                    ampExtTabbedPane.setSelectedIndex(outIdx);
                    landmarkOutListButton.requestFocus();
                    return false;
                }else{
                    if (!landmarkAgent.isValidLandmarkProject(landmark3DOutProjectTextField.getText().trim())) {
                        JOptionPane.showMessageDialog(this, "Can not find project name " + landmark3DOutProjectTextField.getText().trim()
                                + ". Check to see if it is valid Landmark project", "Project not found",    JOptionPane.WARNING_MESSAGE);
                        ampExtTabbedPane.setSelectedIndex(outIdx);
                        landmark3DOutProjectTextField.requestFocus();
                        return false;
                    }

                    if(!landmarkInProjectTextField.getText().trim().equals(landmark3DOutProjectTextField.getText().trim())){
                        JOptionPane.showMessageDialog(this, "3D Projects mismatch between the input and the output.", "Landmark project mismatches",    JOptionPane.WARNING_MESSAGE);
                        ampExtTabbedPane.setSelectedIndex(outIdx);
                        landmarkOutListButton.requestFocus();
                        return false;
                    }
                }
            }else{ //bhpsu input landmark output
                if(landmarkOutProjectTextField == null || landmarkOutProjectTextField.getText().trim().length() == 0){
                    JOptionPane.showMessageDialog(this, "Must specify a Landmark project for the output volume.", "Landmark project is missing",    JOptionPane.WARNING_MESSAGE);
                    ampExtTabbedPane.setSelectedIndex(outIdx);
                    landmarkOutListButton.requestFocus();
                    return false;
                }else{
                    if(landmarkAgent == null)
                        landmarkAgent = LandmarkServices.getInstance();

                    if (!landmarkAgent.isValidLandmarkProject(landmarkOutProjectTextField.getText().trim())) {
                        JOptionPane.showMessageDialog(this, "Can not find project name " + landmarkOutProjectTextField.getText().trim()
                                + ". Check to see if it is valid Landmark project", "Project not found",    JOptionPane.WARNING_MESSAGE);
                        ampExtTabbedPane.setSelectedIndex(outIdx);
                        landmarkOutProjectTextField.requestFocus();
                        return false;
                    }
                }
            }
        }

        /*if (outputCubeTextField.getText().equals("")) {
            JOptionPane.showMessageDialog(this,"Select Output Before Saving Script", "Input File Error", JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(paramIdx);
            outputCubeTextField.requestFocus();
            return false;
        }

        if(outputCubeTextField.getText().trim().endsWith(filesep)){
            JOptionPane.showMessageDialog(this, "The " + outputCubeLabel.getText()
                    + " field can not end with " + filesep, "Error in data entry",  JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(paramIdx);
            outputCubeTextField.requestFocus();
            return false;
        }

        if (outputHorizonsTextField.getText().equals("")) {
            JOptionPane.showMessageDialog(this,"Select Output Horizons Pathname Before Saving Script", "Input File Error", JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(paramIdx);
            outputHorizonsTextField.requestFocus();
            return false;
        }

        if(outputHorizonsTextField.getText().trim().endsWith(filesep)){
            JOptionPane.showMessageDialog(this, "The " + outputHorizonsLabel.getText()
                    + " field can not end with " + filesep, "Error in data entry",  JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(paramIdx);
            outputHorizonsTextField.requestFocus();
            return false;
        }
*/

        if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            if (edgeModeRadioButton.isSelected() && bhpsuTopHorizonTextField.getText().equals("")) {
                JOptionPane.showMessageDialog(this,"Select Top Horizon Before Saving Script", "Input File Error", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                topHorizonBrowseButton.requestFocus();
                return false;
            }

            if (edgeModeRadioButton.isSelected() && bhpsuBottomHorizonTextField.getText().equals("")) {
                JOptionPane.showMessageDialog(this,"Select Bottom Horizon Before Saving Script", "Input File Error", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                bottomHorizonBrowseButton.requestFocus();
                return false;
            }

            if (pointModeRadioButton.isSelected() && bhpsuHorizonTextField.getText().equals("")) {
                JOptionPane.showMessageDialog(this,"Select Horizon Before Saving Script", "Input File Error", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                horizonBrowseButton.requestFocus();
                return false;
            }
        }else if(landmarkHorizonRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
            if (edgeModeRadioButton.isSelected() && landmark3DTopHorizonTextField.getText().equals("")) {
                JOptionPane.showMessageDialog(this,"Select Top Horizon Before Saving Script", "Input File Error", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                topHorizonBrowseButton.requestFocus();
                return false;
            }

            if (edgeModeRadioButton.isSelected() && landmark3DBottomHorizonTextField.getText().equals("")) {
                JOptionPane.showMessageDialog(this,"Select Bottom Horizon Before Saving Script", "Input File Error", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                bottomHorizonBrowseButton.requestFocus();
                return false;
            }

            if (pointModeRadioButton.isSelected() && landmark3DHorizonTextField.getText().equals("")) {
                JOptionPane.showMessageDialog(this,"Select Horizon Before Saving Script", "Input File Error", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                horizonBrowseButton.requestFocus();
                return false;
            }
        }else if(landmarkHorizonRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
            if (edgeModeRadioButton.isSelected() && landmark2DTopHorizonTextField.getText().equals("")) {
                JOptionPane.showMessageDialog(this,"Select Top Horizon Before Saving Script", "Input File Error", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                topHorizonBrowseButton.requestFocus();
                return false;
            }

            if (edgeModeRadioButton.isSelected() && landmark2DBottomHorizonTextField.getText().equals("")) {
                JOptionPane.showMessageDialog(this,"Select Bottom Horizon Before Saving Script", "Input File Error", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                bottomHorizonBrowseButton.requestFocus();
                return false;
            }

            if (pointModeRadioButton.isSelected() && landmark2DHorizonTextField.getText().equals("")) {
                JOptionPane.showMessageDialog(this,"Select Horizon Before Saving Script", "Input File Error", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                horizonBrowseButton.requestFocus();
                return false;
            }
        }



        if (!validateHorizonParams1())
            return false;

        if(bhpsuOutputRadioButton.isSelected()){
            return validateOutputPanel(outputCubeTextField,outputHorizonsTextField);
        }else if(landmarkOutputRadioButton.isSelected()){
            if(landmarkInRadioButton.isSelected()){
                if(landmark2DRadioButton.isSelected()){
                    return validateOutputPanel(landmark2DOutputCubeTextField,landmark2DOutputHorizonsTextField);
                }else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                    return validateOutputPanel(landmark3DOutputCubeTextField,landmark3DOutputHorizonsTextField);
                }
            }else if(bhpsuInRadioButton.isSelected()){
                return validateOutputPanel(landmarkOutputCubeTextField,landmarkOutputHorizonsTextField);
            }
        }
        }
        return true;
    }

    private boolean validateOutputPanel(javax.swing.JTextField cubeField,javax.swing.JTextField horizonField){
        int paramIdx = ampExtTabbedPane.indexOfTab(TN_OUTPUTS);

        if (cubeField.getText().equals("")) {
            JOptionPane.showMessageDialog(this,"Select Output Before Saving Script", "Input File Error", JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(paramIdx);
            cubeField.requestFocus();
            return false;
        }

        if(cubeField.getText().trim().endsWith(filesep)){
            JOptionPane.showMessageDialog(this, "The " + outputCubeLabel.getText()
                    + " field can not end with " + filesep, "Error in data entry",  JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(paramIdx);
            cubeField.requestFocus();
            return false;
        }

        if (horizonField.getText().equals("")) {
            JOptionPane.showMessageDialog(this,"Select Output Horizons Pathname Before Saving Script", "Input File Error", JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(paramIdx);
            horizonField.requestFocus();
            return false;
        }

        if(horizonField.getText().trim().endsWith(filesep)){
            JOptionPane.showMessageDialog(this, "The " + outputHorizonsLabel.getText()
                    + " field can not end with " + filesep, "Error in data entry",  JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(paramIdx);
            horizonField.requestFocus();
            return false;
        }
        return true;
    }
    /**
     * Validate horizon parameters. The validation rules are:
     * <p>
     * <ul>
     * <li>A value must be entered for each horizon parameter
     * <li>Bias must be an integer, positive or negative
     * <li>Search Above must be an integer, greater or equal to zero
     * <li>Search Below must be an integer, greater or equal to zero
     * </ul>
     * @return true if validation passed; otherwise, false.
     */
    private boolean validateHorizonParams1() {
        if(!processPanel.isNotRunAmpExtChecked()){
        if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            if (edgeModeRadioButton.isSelected()) {
                return validateEdgeModeHorizonParams(topHorizonBias, topHorizonAbove, topHorizonBelow, bottomHorizonBias, bottomHorizonAbove, bottomHorizonBelow, horizonsParametersTable);
            }else if (pointModeRadioButton.isSelected()){
                return validatePointModeHorizonParams(horizonBias, horizonAbove, horizonBelow, horizonParametersTable);
            }
        }else if(landmarkHorizonRadioButton.isSelected()){
            if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                if (edgeModeRadioButton.isSelected()) {
                    return validateEdgeModeHorizonParams(landmark2DTopHorizonBias, landmark2DTopHorizonAbove, landmark2DTopHorizonBelow, landmark2DBottomHorizonBias, landmark2DBottomHorizonAbove, landmark2DBottomHorizonBelow, landmark2DHorizonsParametersTable);
                }else if (pointModeRadioButton.isSelected()){
                    return validatePointModeHorizonParams(landmark2DHorizonBias, landmark2DHorizonAbove, landmark2DHorizonBelow, landmark2DHorizonParametersTable);
                }
            }else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                if (edgeModeRadioButton.isSelected()) {
                    return validateEdgeModeHorizonParams(landmark3DTopHorizonBias, landmark3DTopHorizonAbove, landmark3DTopHorizonBelow, landmark3DBottomHorizonBias, landmark3DBottomHorizonAbove, landmark3DBottomHorizonBelow, landmark3DHorizonsParametersTable);
                }else if (pointModeRadioButton.isSelected()){
                    return validatePointModeHorizonParams(landmark3DHorizonBias, landmark3DHorizonAbove, landmark3DHorizonBelow, landmark3DHorizonParametersTable);
                }
            }else{//bhpsu data input but landmark horizon input
                if (edgeModeRadioButton.isSelected()) {
                    return validateEdgeModeHorizonParams(topHorizonBias, topHorizonAbove, topHorizonBelow, bottomHorizonBias, bottomHorizonAbove, bottomHorizonBelow, horizonsParametersTable);
                }else if (pointModeRadioButton.isSelected()){
                    return validatePointModeHorizonParams(horizonBias, horizonAbove, horizonBelow, horizonParametersTable);
                }
            }
        }
        }
        return true;
    }

    private boolean validatePointModeHorizonParams(String bias, String above, String below, JTable table){
        String errorSource = "", errorCause = "", errorFix = "";
        errorCause = MISSING_VALUES_ERROR;
        errorFix = ENTER_VALUES_FIX;
        int idx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        //check a value entered for all horizon parameters
        //if (horizonBias.equals("") || horizonAbove.equals("") || horizonBelow.equals("")) {
        //    horizonParamValidationError(errorSource, errorCause, errorFix);
        //    return false;
        //}
        if(bias.trim().length() == 0){
            JOptionPane.showMessageDialog(this,
                    "Horizon Bias value is empty.",
                    "Horizon Parameters Error",
                    JOptionPane.WARNING_MESSAGE);
            //table.requestFocus();
            ampExtTabbedPane.setSelectedIndex(idx);
            //table.editCellAt(0,0);
            return false;
        }
        if(above.trim().length() == 0){
            JOptionPane.showMessageDialog(this,
                    "Horizon Search Above value is empty.",
                    "Horizon Parameters Error",
                    JOptionPane.WARNING_MESSAGE);
            //table.requestFocus();
            ampExtTabbedPane.setSelectedIndex(idx);
            //table.editCellAt(0,1);
            return false;
        }
        if(below.trim().length() == 0){
            JOptionPane.showMessageDialog(this,
                    "Horizon Search Below value is empty.",
                    "Horizon Parameters Error",
                    JOptionPane.WARNING_MESSAGE);
            //table.requestFocus();
            ampExtTabbedPane.setSelectedIndex(idx);
            //table.editCellAt(0,2);
            return false;
        }

        //check value type and range
        int val = 0;
        errorCause = NOT_AN_INTEGER_ERROR;
        errorFix = REENTER_VALUE_FIX;
        try {
            errorSource = HORIZON_BIAS_SOURCE;
            val = Integer.parseInt(bias);

            errorSource = HORIZON_ABOVE_SOURCE;
            val = Integer.parseInt(above);
            if (val < 0) {
                errorCause = IS_NEGATIVE_ERROR;
                horizonParamValidationError(errorSource, errorCause, errorFix);
                ampExtTabbedPane.setSelectedIndex(idx);
                //table.editCellAt(0,1);
                return false;
            }

            errorSource = HORIZON_BELOW_SOURCE;
            val = Integer.parseInt(below);
            if (val < 0) {
                errorCause = IS_NEGATIVE_ERROR;
                horizonParamValidationError(errorSource, errorCause, errorFix);
                ampExtTabbedPane.setSelectedIndex(idx);
                //table.editCellAt(0,2);
                return false;
            }
        } catch (NumberFormatException nfe) {
            horizonParamValidationError(errorSource, errorCause, errorFix);
            ampExtTabbedPane.setSelectedIndex(idx);
            /*if(errorSource == HORIZON_BIAS_SOURCE)
                table.editCellAt(0,0);
            else if(errorSource == HORIZON_ABOVE_SOURCE)
                table.editCellAt(0,1);
            else if(errorSource == HORIZON_BELOW_SOURCE)
                table.editCellAt(0,2);
                */
            return false;
        }
        return true;
    }
    private boolean validateEdgeModeHorizonParams(String topBias, String topAbove, String topBelow, String bottomBias,String bottomAbove,
            String bottomBelow, JTable table){
        //check a value entered for each horizon parameter
        String errorSource = "", errorCause = "", errorFix = "";
        errorSource = "";
        errorCause = MISSING_VALUES_ERROR;
        errorFix = ENTER_VALUES_FIX;
        int idx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        //check a value entered for all horizon parameters
        if(topBias.trim().length() == 0){
            JOptionPane.showMessageDialog(this,
                    "Top Horizon Bias value is empty.",
                    "Horizon Parameters Error",
                    JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(idx);
            //table.requestFocus();
            //table.editCellAt(0,1);
            return false;
        }
        if(topAbove.trim().length() == 0){
            JOptionPane.showMessageDialog(this,
                    "Top Horizon Search Above value is empty.",
                    "Horizon Parameters Error",
                    JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(idx);
            //table.requestFocus();
            //table.editCellAt(0,2);
            return false;
        }
        if(topBelow.trim().length() == 0){
            JOptionPane.showMessageDialog(this,
                    "Top Horizon Search Below value is empty.",
                    "Horizon Parameters Error",
                    JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(idx);
            //table.requestFocus();
            //table.editCellAt(0,3);
            return false;
        }
        if(bottomBias.trim().length() == 0){
            JOptionPane.showMessageDialog(this,
                    "Bottom Horizon Bias value is empty.",
                    "Horizon Parameters Error",
                    JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(idx);
            //table.requestFocus();
            //table.editCellAt(1,1);
            return false;
        }
        if(bottomAbove.trim().length() == 0){
            JOptionPane.showMessageDialog(this,
                    "Bottom Horizon Search Above value is empty.",
                    "Horizon Parameters Error",
                    JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(idx);
            //table.requestFocus();
            //table.editCellAt(1,2);
            return false;
        }

        if(bottomBelow.trim().length() == 0){
            JOptionPane.showMessageDialog(this,
                    "Bottom Horizon Search Below value is empty.",
                    "Horizon Parameters Error",
                    JOptionPane.WARNING_MESSAGE);
            ampExtTabbedPane.setSelectedIndex(idx);
            //table.requestFocus();
            //table.editCellAt(1,3);
            return false;
        }


        //check value type and range
        int val = 0;
        errorCause = NOT_AN_INTEGER_ERROR;
        errorFix = REENTER_VALUE_FIX;
        try {
            errorSource = TOP_HORIZON_BIAS_SOURCE;
            val = Integer.parseInt(topBias);

            errorSource = BOTTOM_HORIZON_BIAS_SOURCE;
            val = Integer.parseInt(bottomBias);

            errorSource = TOP_HORIZON_ABOVE_SOURCE;
            val = Integer.parseInt(topAbove);
            if (val < 0) {
                errorCause = IS_NEGATIVE_ERROR;
                horizonParamValidationError(errorSource, errorCause, errorFix);
                ampExtTabbedPane.setSelectedIndex(idx);
                //table.requestFocus();
                //table.editCellAt(0,2);
                return false;
            }

            errorSource = BOTTOM_HORIZON_ABOVE_SOURCE;
            val = Integer.parseInt(bottomAbove);
            if (val < 0) {
                errorCause = IS_NEGATIVE_ERROR;
                horizonParamValidationError(errorSource, errorCause, errorFix);
                ampExtTabbedPane.setSelectedIndex(idx);
                //table.requestFocus();
                //table.editCellAt(1,2);
                return false;
            }

            errorSource = TOP_HORIZON_BELOW_SOURCE;
            val = Integer.parseInt(topBelow);
            if (val < 0) {
                errorCause = IS_NEGATIVE_ERROR;
                horizonParamValidationError(errorSource, errorCause, errorFix);
                ampExtTabbedPane.setSelectedIndex(idx);
                //table.requestFocus();
                //table.editCellAt(0,3);
                return false;
            }

            errorSource = BOTTOM_HORIZON_BELOW_SOURCE;
            val = Integer.parseInt(bottomBelow);
            if (val < 0) {
                errorCause = IS_NEGATIVE_ERROR;
                horizonParamValidationError(errorSource, errorCause, errorFix);
                ampExtTabbedPane.setSelectedIndex(idx);
                //table.requestFocus();
                //table.editCellAt(1,3);
                return false;
            }
        } catch (NumberFormatException nfe) {
            horizonParamValidationError(errorSource, errorCause, errorFix);
            ampExtTabbedPane.setSelectedIndex(idx);
            //table.requestFocus();
            /*
            if(errorSource == TOP_HORIZON_BIAS_SOURCE)
                table.editCellAt(0,1);
            else if(errorSource == TOP_HORIZON_ABOVE_SOURCE)
                table.editCellAt(0,2);
            else if(errorSource == TOP_HORIZON_BELOW_SOURCE)
                table.editCellAt(0,3);
            else if(errorSource == BOTTOM_HORIZON_BIAS_SOURCE)
                table.editCellAt(1,1);
            else if(errorSource == BOTTOM_HORIZON_ABOVE_SOURCE)
                table.editCellAt(1,2);
            else if(errorSource == BOTTOM_HORIZON_BELOW_SOURCE)
                table.editCellAt(1,3);
*/
            return false;
        }
        return true;

    }

    /**
     * Validate horizon parameters. The validation rules are:
     * <p>
     * <ul>
     * <li>A value must be entered for each horizon parameter
     * <li>Bias must be an integer, positive or negative
     * <li>Search Above must be an integer, greater or equal to zero
     * <li>Search Below must be an integer, greater or equal to zero
     * </ul>
     * @return true if validation passed; otherwise, false.
     */
    private boolean validateHorizonParams() {
        String errorSource = "", errorCause = "", errorFix = "";
        int paramIdx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        if (edgeModeRadioButton.isSelected()) {
            //check a value entered for each horizon parameter
            errorSource = "";
            errorCause = MISSING_VALUES_ERROR;
            errorFix = ENTER_VALUES_FIX;
            //check a value entered for all horizon parameters
            if(topHorizonBias.trim().length() == 0){
                JOptionPane.showMessageDialog(this,
                        "Top Horizon Bias value is empty.",
                        "Horizon Parameters Error",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                //horizonsParametersTable.requestFocus();
                //horizonsParametersTable.editCellAt(0,1);
                return false;
            }
            if(topHorizonAbove.trim().length() == 0){
                JOptionPane.showMessageDialog(this,
                        "Top Horizon Search Above value is empty.",
                        "Horizon Parameters Error",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                //horizonsParametersTable.requestFocus();
                //horizonsParametersTable.editCellAt(0,2);
                return false;
            }
            if(topHorizonBelow.trim().length() == 0){
                JOptionPane.showMessageDialog(this,
                        "Top Horizon Search Below value is empty.",
                        "Horizon Parameters Error",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                //horizonsParametersTable.requestFocus();
                //horizonsParametersTable.editCellAt(0,3);
                return false;
            }
            if(bottomHorizonBias.trim().length() == 0){
                JOptionPane.showMessageDialog(this,
                        "Bottom Horizon Bias value is empty.",
                        "Horizon Parameters Error",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                //horizonsParametersTable.requestFocus();
                //horizonsParametersTable.editCellAt(1,1);
                return false;
            }
            if(bottomHorizonAbove.trim().length() == 0){
                JOptionPane.showMessageDialog(this,
                        "Bottom Horizon Search Above value is empty.",
                        "Horizon Parameters Error",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                //horizonsParametersTable.requestFocus();
                //horizonsParametersTable.editCellAt(1,2);
                return false;
            }

            if(bottomHorizonBelow.trim().length() == 0){
                JOptionPane.showMessageDialog(this,
                        "Bottom Horizon Search Below value is empty.",
                        "Horizon Parameters Error",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                //horizonsParametersTable.requestFocus();
                //horizonsParametersTable.editCellAt(1,3);
                return false;
            }


            //if (topHorizonBias.equals("") || topHorizonAbove.equals("") || topHorizonBelow.equals("") ||
            //    bottomHorizonBias.equals("") || bottomHorizonAbove.equals("") || bottomHorizonBelow.equals("")) {
            //    horizonParamValidationError(errorSource, errorCause, errorFix);
            //    ampExtTabbedPane.setSelectedIndex(paramIdx);
            //    return false;
            //}

            //check value type and range
            int val = 0;
            errorCause = NOT_AN_INTEGER_ERROR;
            errorFix = REENTER_VALUE_FIX;
            try {
                errorSource = TOP_HORIZON_BIAS_SOURCE;
                val = Integer.parseInt(topHorizonBias);

                errorSource = BOTTOM_HORIZON_BIAS_SOURCE;
                val = Integer.parseInt(bottomHorizonBias);

                errorSource = TOP_HORIZON_ABOVE_SOURCE;
                val = Integer.parseInt(topHorizonAbove);
                if (val < 0) {
                    errorCause = IS_NEGATIVE_ERROR;
                    horizonParamValidationError(errorSource, errorCause, errorFix);
                    ampExtTabbedPane.setSelectedIndex(paramIdx);
                    //horizonsParametersTable.requestFocus();
                    //horizonsParametersTable.editCellAt(0,2);
                    return false;
                }

                errorSource = BOTTOM_HORIZON_ABOVE_SOURCE;
                val = Integer.parseInt(bottomHorizonAbove);
                if (val < 0) {
                    errorCause = IS_NEGATIVE_ERROR;
                    horizonParamValidationError(errorSource, errorCause, errorFix);
                    ampExtTabbedPane.setSelectedIndex(paramIdx);
                    //horizonsParametersTable.requestFocus();
                    //horizonsParametersTable.editCellAt(1,2);
                    return false;
                }

                errorSource = TOP_HORIZON_BELOW_SOURCE;
                val = Integer.parseInt(topHorizonBelow);
                if (val < 0) {
                    errorCause = IS_NEGATIVE_ERROR;
                    horizonParamValidationError(errorSource, errorCause, errorFix);
                    ampExtTabbedPane.setSelectedIndex(paramIdx);
                    //horizonsParametersTable.requestFocus();
                    //horizonsParametersTable.editCellAt(0,3);
                    return false;
                }

                errorSource = BOTTOM_HORIZON_BELOW_SOURCE;
                val = Integer.parseInt(bottomHorizonBelow);
                if (val < 0) {
                    errorCause = IS_NEGATIVE_ERROR;
                    horizonParamValidationError(errorSource, errorCause, errorFix);
                    ampExtTabbedPane.setSelectedIndex(paramIdx);
                    //horizonsParametersTable.requestFocus();
                    //horizonsParametersTable.editCellAt(1,3);
                    return false;
                }
            } catch (NumberFormatException nfe) {
                horizonParamValidationError(errorSource, errorCause, errorFix);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                //horizonsParametersTable.requestFocus();
                /*
                if(errorSource == TOP_HORIZON_BIAS_SOURCE)
                    horizonsParametersTable.editCellAt(0,1);
                else if(errorSource == TOP_HORIZON_ABOVE_SOURCE)
                    horizonsParametersTable.editCellAt(0,2);
                else if(errorSource == TOP_HORIZON_BELOW_SOURCE)
                    horizonsParametersTable.editCellAt(0,3);
                else if(errorSource == BOTTOM_HORIZON_BIAS_SOURCE)
                    horizonsParametersTable.editCellAt(1,1);
                else if(errorSource == BOTTOM_HORIZON_ABOVE_SOURCE)
                    horizonsParametersTable.editCellAt(1,2);
                else if(errorSource == BOTTOM_HORIZON_BELOW_SOURCE)
                    horizonsParametersTable.editCellAt(1,3);
                */
                return false;
            }
        } else {
            errorSource = "";
            errorCause = MISSING_VALUES_ERROR;
            errorFix = ENTER_VALUES_FIX;
            //check a value entered for all horizon parameters
            //if (horizonBias.equals("") || horizonAbove.equals("") || horizonBelow.equals("")) {
            //    horizonParamValidationError(errorSource, errorCause, errorFix);
            //    return false;
            //}
            if(horizonBias.trim().length() == 0){
                JOptionPane.showMessageDialog(this,
                        "Horizon Bias value is empty.",
                        "Horizon Parameters Error",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                //horizonParametersTable.requestFocus();
                //horizonParametersTable.editCellAt(0,0);
                return false;
            }
            if(horizonAbove.trim().length() == 0){
                JOptionPane.showMessageDialog(this,
                        "Horizon Search Above value is empty.",
                        "Horizon Parameters Error",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                //horizonParametersTable.requestFocus();
                //horizonParametersTable.editCellAt(0,1);
                return false;
            }
            if(horizonBelow.trim().length() == 0){
                JOptionPane.showMessageDialog(this,
                        "Horizon Search Below value is empty.",
                        "Horizon Parameters Error",
                        JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                //horizonParametersTable.requestFocus();
                //horizonParametersTable.editCellAt(0,2);
                return false;
            }

            //check value type and range
            int val = 0;
            errorCause = NOT_AN_INTEGER_ERROR;
            errorFix = REENTER_VALUE_FIX;
            try {
                errorSource = HORIZON_BIAS_SOURCE;
                val = Integer.parseInt(horizonBias);

                errorSource = HORIZON_ABOVE_SOURCE;
                val = Integer.parseInt(horizonAbove);
                if (val < 0) {
                    errorCause = IS_NEGATIVE_ERROR;
                    horizonParamValidationError(errorSource, errorCause, errorFix);
                    ampExtTabbedPane.setSelectedIndex(paramIdx);
                    //horizonParametersTable.requestFocus();
                    //horizonParametersTable.editCellAt(0,1);
                    return false;
                }

                errorSource = HORIZON_BELOW_SOURCE;
                val = Integer.parseInt(horizonBelow);
                if (val < 0) {
                    errorCause = IS_NEGATIVE_ERROR;
                    horizonParamValidationError(errorSource, errorCause, errorFix);
                    ampExtTabbedPane.setSelectedIndex(paramIdx);
                    //horizonParametersTable.requestFocus();
                    //horizonParametersTable.editCellAt(0,2);
                    return false;
                }
            } catch (NumberFormatException nfe) {
                horizonParamValidationError(errorSource, errorCause, errorFix);
                ampExtTabbedPane.setSelectedIndex(paramIdx);
                //horizonParametersTable.requestFocus();
                /*
                if(errorSource == HORIZON_BIAS_SOURCE)
                    horizonParametersTable.editCellAt(0,0);
                else if(errorSource == HORIZON_ABOVE_SOURCE)
                    horizonParametersTable.editCellAt(0,1);
                else if(errorSource == HORIZON_BELOW_SOURCE)
                    horizonParametersTable.editCellAt(0,2);
                    */
                return false;
            }
        }

        return true;
    }

    private void horizonParamValidationError(String errorSource, String errorCause, String errorFix) {
        JOptionPane.showMessageDialog(this,
            errorSource + " " + errorCause + ". " + errorFix,
            "Horizon Parameters Error",
            JOptionPane.WARNING_MESSAGE);
    }

/*
    private void selectPMActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_saveAsMenuItemActionPerformed
        //Get a list of qiProjectManager instances from the Workbench Manager. There is always at least one.
        ComponentDescriptor wbMgrDesc = agent.getMessagingMgr().getComponentDescFromDisplayName(QIWConstants.WORKBENCH_MGR_NAME);
        agent.getMessagingMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_ACTIVE_PROJMGRS_CMD, wbMgrDesc);
    }// GEN-LAST:event_saveAsMenuItemActionPerformed
*/

    private void horizonInputTypeRadioButtonActionPerformed(java.awt.event.ActionEvent evt){
        //if(landmarkHorizonRadioButton.isSelected()){
        //  topHorizonBrowseButton = new javax.swing.JButton();
        //  bottomHorizonBrowseButton = new javax.swing.JButton();
        //}else if(bhpsuHorizonRadioButton.isSelected())
        //  horizonBrowseButton = new javax.swing.JButton();
        if(landmarkHorizonRadioButton.isSelected() && bhpsuInRadioButton.isSelected()){
            JOptionPane.showMessageDialog(this,
                    "Landmark horizon input is not yet fully supported for BHP SU seismic data input.",
                    "Services not yet fully supported",
                    JOptionPane.WARNING_MESSAGE);
            bhpsuHorizonRadioButton.setSelected(true);
            return;
        }
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        // Note: radio buttons automatically set correctly since in a group

        horizonsPanel = new javax.swing.JPanel();
        horizonDataPanel = new javax.swing.JPanel();
        parametersPanel = new javax.swing.JPanel();
        if(landmarkHorizonRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
            landmark3DHorizonsParametersPanel = new javax.swing.JPanel();
            if(edgeModeRadioButton.isSelected())
                formLandmark3DEdgeModeHorizonPanel();
            else if(pointModeRadioButton.isSelected())
                formLandmark3DPointModeHorizonPanel();
        }else if(landmarkHorizonRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
            landmark2DHorizonsParametersPanel = new javax.swing.JPanel();
            if(edgeModeRadioButton.isSelected())
                formLandmark2DEdgeModeHorizonPanel();
            else if(pointModeRadioButton.isSelected())
                formLandmark2DPointModeHorizonPanel();
        }else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            horizonsParametersPanel = new javax.swing.JPanel();
            if(edgeModeRadioButton.isSelected())
                formEdgeModeHorizonPanel();
            else if(pointModeRadioButton.isSelected())
                formPointModeHorizonPanel();
        }

        formHorizonsPanel();
        if(landmarkInRadioButton.isSelected()){
            if(landmark3DRadioButton.isSelected()){
                landmark3DTargetTypePanel = new javax.swing.JPanel();
                formTargetPanel(landmark3DTargetTypePanel, landmark3DHorizonTargetType);
                layoutParametersTabbedPane(landmark3DTargetTypePanel);
            }else if(landmark2DRadioButton.isSelected()){
                landmark2DTargetTypePanel = new javax.swing.JPanel();
                formTargetPanel(landmark2DTargetTypePanel, landmark2DHorizonTargetType);
                layoutParametersTabbedPane(landmark2DTargetTypePanel);
            }
        }else{ // ascii  or bhpsu dataset
            targetTypePanel = new javax.swing.JPanel();
            formTargetPanel(targetTypePanel,bhpsuHorizonTargetType);
            layoutParametersTabbedPane(targetTypePanel);
        }
        if(landmarkHorizonRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
            if(edgeModeRadioButton.isSelected()){
                landmark3DEdgeModeHorizonParamsTableModel = landmark3DHorizonsParametersTable.getModel();
                addLandmark3DEdgeModeHorizonParamsListener(landmark3DEdgeModeHorizonParamsTableModel);
                for (int i=0; i<2; i++)
                    for (int j=0; j<3; j++)
                        landmark3DEdgeModeHorizonParamsTableModel.setValueAt(landmark3DEdgeModeHorizonSettings[i][j], i, j+1);
            }else if(pointModeRadioButton.isSelected()){
                landmark3DPointModeHorizonParamsTableModel = landmark3DHorizonParametersTable.getModel();
                addLandmark3DPointModeHorizonParamsListener(landmark3DPointModeHorizonParamsTableModel);
                //restore table cell settings for point mode
                for (int j=0; j<3; j++)
                    landmark3DPointModeHorizonParamsTableModel.setValueAt(landmark3DPointModeHorizonSettings[0][j], 0, j);
            }
        }else if(landmarkHorizonRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
            if(edgeModeRadioButton.isSelected()){
                landmark2DEdgeModeHorizonParamsTableModel = landmark2DHorizonsParametersTable.getModel();
                addLandmark2DEdgeModeHorizonParamsListener(landmark2DEdgeModeHorizonParamsTableModel);
                for (int i=0; i<2; i++)
                    for (int j=0; j<3; j++)
                        landmark2DEdgeModeHorizonParamsTableModel.setValueAt(landmark2DEdgeModeHorizonSettings[i][j], i, j+1);
            }else if(pointModeRadioButton.isSelected()){
                landmark2DPointModeHorizonParamsTableModel = landmark2DHorizonParametersTable.getModel();
                addLandmark2DPointModeHorizonParamsListener(landmark2DPointModeHorizonParamsTableModel);
                //restore table cell settings for point mode
                for (int j=0; j<3; j++)
                    landmark2DPointModeHorizonParamsTableModel.setValueAt(landmark2DPointModeHorizonSettings[0][j], 0, j);
            }
        }else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            if(edgeModeRadioButton.isSelected()){
                edgeModeHorizonParamsTableModel = horizonsParametersTable.getModel();
                addEdgeModeHorizonParamsListener(edgeModeHorizonParamsTableModel);
                for (int i=0; i<2; i++)
                    for (int j=0; j<3; j++)
                        edgeModeHorizonParamsTableModel.setValueAt(edgeModeHorizonSettings[i][j], i, j+1);
            }else if(pointModeRadioButton.isSelected()){
                pointModeHorizonParamsTableModel = horizonParametersTable.getModel();
                addPointModeHorizonParamsListener(pointModeHorizonParamsTableModel);
                //restore table cell settings for point mode
                for (int j=0; j<3; j++)
                    pointModeHorizonParamsTableModel.setValueAt(pointModeHorizonSettings[0][j], 0, j);
            }
        }
        ampExtTabbedPane.setComponentAt(tabIdx, parametersPanel);
    }

    private void horizonInputTabActionPerformed(java.awt.event.ActionEvent evt){
        /*
        if(bhpsuOutputRadioButton.isSelected())
            formOutputDataPanel();
        else if(landmarkOutputRadioButton.isSelected()){
            if(landmarkInRadioButton.isSelected()){
                if(landmark3DRadioButton.isSelected()){
                    if(landmarkInProjectTextField.getText().trim().length() > 0){
                        if(!landmarkOutProjectTextField.isEnabled())
                            landmarkOutProjectTextField.setEnabled(true);
                        landmarkOutProjectTextField.setText(landmarkInProjectTextField.getText().trim());
                        landmarkOutProjectTextField.setEnabled(false);
                        landmarkOutListButton.setEnabled(false);
                    }
                    formLandmark3DOutputDataPanel();
                }
                else if(landmark2DRadioButton.isSelected()){
                    if(landmarkIn2DProjectTextField.getText().trim().length() > 0){
                        if(!landmarkOutProjectTextField.isEnabled())
                            landmarkOutProjectTextField.setEnabled(true);
                        landmarkOutProjectTextField.setText(landmarkIn2DProjectTextField.getText().trim());
                        landmarkOutProjectTextField.setEnabled(false);
                        landmarkOutListButton.setEnabled(false);
                    }
                    formLandmark2DOutputDataPanel();
                }
            }else if(bhpsuInRadioButton.isSelected()){
                formLandmarkOutputBhpsuInDataPanel();
            }
        }
        formOutputPanel();
        */
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        // Note: radio buttons automatically set correctly since in a group
        horizonsPanel = new javax.swing.JPanel();
        horizonDataPanel = new javax.swing.JPanel();
        parametersPanel = new javax.swing.JPanel();

        if(landmarkHorizonRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
            landmark3DHorizonsParametersPanel = new javax.swing.JPanel();
            if(edgeModeRadioButton.isSelected())
                formLandmark3DEdgeModeHorizonPanel();
            else if(pointModeRadioButton.isSelected())
                formLandmark3DPointModeHorizonPanel();
        }else if(landmarkHorizonRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
            landmark2DHorizonsParametersPanel = new javax.swing.JPanel();
            if(edgeModeRadioButton.isSelected())
                formLandmark2DEdgeModeHorizonPanel();
            else if(pointModeRadioButton.isSelected())
                formLandmark2DPointModeHorizonPanel();
        }else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            horizonsParametersPanel = new javax.swing.JPanel();
            if(edgeModeRadioButton.isSelected())
                formEdgeModeHorizonPanel();
            else if(pointModeRadioButton.isSelected())
                formPointModeHorizonPanel();
        }
        formHorizonsPanel();
        if(landmarkInRadioButton.isSelected()){
            if(landmark3DRadioButton.isSelected()){
                landmark3DTargetTypePanel = new javax.swing.JPanel();
                formTargetPanel(landmark3DTargetTypePanel, landmark3DHorizonTargetType);
                layoutParametersTabbedPane(landmark3DTargetTypePanel);
            }else if(landmark2DRadioButton.isSelected()){
                landmark2DTargetTypePanel = new javax.swing.JPanel();
                formTargetPanel(landmark2DTargetTypePanel, landmark2DHorizonTargetType);
                layoutParametersTabbedPane(landmark2DTargetTypePanel);
            }
        }else{ // bhpsu
            targetTypePanel = new javax.swing.JPanel();
            formTargetPanel(targetTypePanel,bhpsuHorizonTargetType);
            layoutParametersTabbedPane(targetTypePanel);
        }


        if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
            if(edgeModeRadioButton.isSelected()){
                edgeModeHorizonParamsTableModel = horizonsParametersTable.getModel();
                addEdgeModeHorizonParamsListener(edgeModeHorizonParamsTableModel);
                //restore table cell settings for edge mode
                for (int i=0; i<2; i++)
                    for (int j=0; j<3; j++)
                        edgeModeHorizonParamsTableModel.setValueAt(edgeModeHorizonSettings[i][j], i, j+1);
            }else if(pointModeRadioButton.isSelected()){
                pointModeHorizonParamsTableModel = horizonParametersTable.getModel();
                addPointModeHorizonParamsListener(pointModeHorizonParamsTableModel);

                //restore table cell settings for point mode
                for (int j=0; j<3; j++)
                    pointModeHorizonParamsTableModel.setValueAt(pointModeHorizonSettings[0][j], 0, j);
            }
        }else if(landmarkHorizonRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
            if(edgeModeRadioButton.isSelected()){
                landmark3DEdgeModeHorizonParamsTableModel = landmark3DHorizonsParametersTable.getModel();
                addLandmark3DEdgeModeHorizonParamsListener(landmark3DEdgeModeHorizonParamsTableModel);
                //restore table cell settings for edge mode
                for (int i=0; i<2; i++)
                    for (int j=0; j<3; j++)
                        landmark3DEdgeModeHorizonParamsTableModel.setValueAt(landmark3DEdgeModeHorizonSettings[i][j], i, j+1);
            }else if(pointModeRadioButton.isSelected()){
                landmark3DPointModeHorizonParamsTableModel = landmark3DHorizonParametersTable.getModel();
                addLandmark3DPointModeHorizonParamsListener(landmark3DPointModeHorizonParamsTableModel);

                //restore table cell settings for point mode
                for (int j=0; j<3; j++)
                    landmark3DPointModeHorizonParamsTableModel.setValueAt(landmark3DPointModeHorizonSettings[0][j], 0, j);
            }
        }else if(landmarkHorizonRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
            if(edgeModeRadioButton.isSelected()){
                landmark2DEdgeModeHorizonParamsTableModel = landmark2DHorizonsParametersTable.getModel();
                addLandmark2DEdgeModeHorizonParamsListener(landmark2DEdgeModeHorizonParamsTableModel);
                //restore table cell settings for edge mode
                for (int i=0; i<2; i++)
                    for (int j=0; j<3; j++)
                        landmark2DEdgeModeHorizonParamsTableModel.setValueAt(landmark2DEdgeModeHorizonSettings[i][j], i, j+1);
            }else if(pointModeRadioButton.isSelected()){
                landmark2DPointModeHorizonParamsTableModel = landmark2DHorizonParametersTable.getModel();
                addLandmark2DPointModeHorizonParamsListener(landmark2DPointModeHorizonParamsTableModel);

                //restore table cell settings for point mode
                for (int j=0; j<3; j++)
                    landmark2DPointModeHorizonParamsTableModel.setValueAt(landmark2DPointModeHorizonSettings[0][j], 0, j);
            }
        }
        ampExtTabbedPane.setComponentAt(tabIdx, parametersPanel);
    }

    private void outputTypeRadioButtonActionPerformed1(java.awt.event.ActionEvent evt){
        if(landmarkOutputRadioButton.isSelected()){
            if(validateLandmarkEnvironment() == false){
                bhpsuOutputRadioButton.setSelected(true);
                return;
            }
            try{
                landmarkAgent = LandmarkServices.getInstance();
            }catch(UnsatisfiedLinkError e){
                e.printStackTrace();
                JOptionPane.showMessageDialog(gui,e.getMessage() + ". Contact workbench support.",
                        "Linking Error",JOptionPane.WARNING_MESSAGE);
                bhpsuOutputRadioButton.setSelected(true);
                return;
            }catch(NoClassDefFoundError e){
                e.printStackTrace();
                JOptionPane.showMessageDialog(gui,"Problem in getting the associated library for handling Landmark projects. Contact workbench support.",
                        "Linking Error",JOptionPane.WARNING_MESSAGE);
                bhpsuOutputRadioButton.setSelected(true);
                return;
            }catch(Exception e){
                e.printStackTrace();
                JOptionPane.showMessageDialog(gui,"Problem in getting the associated library for handling Landmark projects. Contact workbench support.",
                        "Linking Error",JOptionPane.WARNING_MESSAGE);
                bhpsuOutputRadioButton.setSelected(true);
                return;
            }
        }


        if(bhpsuOutputRadioButton.isSelected())
            formOutputDataPanel();
        else if(landmarkOutputRadioButton.isSelected()){
            if(landmarkInRadioButton.isSelected()){
                if(landmark3DRadioButton.isSelected()){
                    if(landmarkInProjectTextField.getText().trim().length() > 0){
                        if(!landmark3DOutProjectTextField.isEnabled())
                            landmark3DOutProjectTextField.setEnabled(true);
                        landmark3DOutProjectTextField.setText(landmarkInProjectTextField.getText().trim());
                        landmark3DOutProjectTextField.setEnabled(false);
                        landmarkOutListButton.setEnabled(false);
                    }
                    formLandmark3DOutputDataPanel();
                }
                else if(landmark2DRadioButton.isSelected()){
                    if(landmarkIn2DProjectTextField.getText().trim().length() > 0){
                        if(!landmark2DOutProjectTextField.isEnabled())
                            landmark2DOutProjectTextField.setEnabled(true);
                        landmark2DOutProjectTextField.setText(landmarkIn2DProjectTextField.getText().trim());
                        landmark2DOutProjectTextField.setEnabled(false);
                        landmarkOutListButton.setEnabled(false);
                    }
                    formLandmark2DOutputDataPanel();
                }
            }else if(bhpsuInRadioButton.isSelected()){
                formLandmarkOutputBhpsuInDataPanel();
            }

        }
        formOutputPanel();
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_OUTPUTS);
        ampExtTabbedPane.setComponentAt(tabIdx, outputPanel);
    }

    private void horizonBrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_horizonBrowseButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_horizonBrowseButtonActionPerformed

    private void horizonTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_horizonTextFieldActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_horizonTextFieldActionPerformed

    private void bottomHorizonBrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottomHorizonBrowseButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_bottomHorizonBrowseButtonActionPerformed

    private void bottomHorizonTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottomHorizonTextFieldActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_bottomHorizonTextFieldActionPerformed

    private void topHorizonTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_topHorizonTextFieldActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_topHorizonTextFieldActionPerformed

    private void pointModeRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pointModeRadioButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_pointModeRadioButtonActionPerformed

    private void edgeModeRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edgeModeRadioButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_edgeModeRadioButtonActionPerformed

    private void outputHorizonsTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_outputHorizonsTextFieldActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_outputHorizonsTextFieldActionPerformed

    private void outputCubeTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_outputCubeTextFieldActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_outputCubeTextFieldActionPerformed

    private void epByTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_epByTextFieldActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_epByTextFieldActionPerformed

    private void epToTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_epToTextFieldActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_epToTextFieldActionPerformed

    private void epFromTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_epFromTextFieldActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_epFromTextFieldActionPerformed

    private void cdpByTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cdpByTextFieldActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_cdpByTextFieldActionPerformed

    private void cdpToTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cdpToTextFieldActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_cdpToTextFieldActionPerformed

    private void cdpFromTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cdpFromTextFieldActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_cdpFromTextFieldActionPerformed

    private void inputCubeBrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputCubeBrowseButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_inputCubeBrowseButtonActionPerformed

    private void inputCubeTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputCubeTextFieldActionPerformed
        if((inputCubeTextField.getText().endsWith(".dat") && (new File(inputCubeTextField.getText())).exists()) ||
                (new File(inputCubeTextField.getText() + ".dat")).exists()) {
            if(inputCubeTextField.getText().endsWith(".dat")) {
                this.setInput(inputCubeTextField.getText());
            } else {
                this.setInput(inputCubeTextField.getText()+".dat");
            }
            this.setEntireDatasetButtonEnabled(true);
        }
    }//GEN-LAST:event_inputCubeTextFieldActionPerformed

    private void pickRangeFromViewerButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pickRangeFromViewerButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_pickRangeFromViewerButtonActionPerformed

    private void topHorizonBrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_topHorizonBrowseButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_topHorizonBrowseButtonActionPerformed

    private void entireCubeRangeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_entireCubeRangeButtonActionPerformed
        //reset ranges to their defaults, i.e., values in header
        initRanges(headerLimits);
    }//GEN-LAST:event_entireCubeRangeButtonActionPerformed

    private void displayResultsButtonActionPerformed(java.awt.event.ActionEvent evt) {
//GEN-FIRST:event_displayResultsButtonActionPerformed
        // Launch a new 2D viewer (qiViewer) for displaying the AmpExt results
        ComponentDescriptor qiViewerDesc = ComponentUtils.launchNewComponent(QIWConstants.QI_VIEWER_NAME, agent.getMessagingMgr());

        //CANNOT proceed if qiViewerDesc null
        if (qiViewerDesc == null) {
            JOptionPane.showMessageDialog(this, "Cannot open a new qiViewer for displaying results", "Launch Error", JOptionPane.ERROR_MESSAGE);
        } else {
            //Start a control session
            ArrayList actions = new ArrayList();
            actions.add("action="+ControlConstants.START_CONTROL_SESSION_ACTION);
            ArrayList actionParams = new ArrayList();
            actionParams.add(qiViewerDesc);
            actions.add(actionParams);
            agent.getMessagingMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
        }
    }//GEN-LAST:event_displayResultsButtonActionPerformed

    private void cancelRunButtonActionPerformed(java.awt.event.ActionEvent evt) {
//GEN-FIRST:event_cancelRunButtonActionPerformed
        //NOTE: cancelJob will update the status of the job
        if (jobManager.cancelJob(jobMonitor)) {
            // update GUI job monitoring components
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    stateLabel.setText(jobMonitor.getJobStatus());
                    // enable another script to be generated and executed
                    runMenuItem.setEnabled(true);
                    runScriptButton.setEnabled(true);
                    writeScriptMenuItem.setEnabled(true);
                    writeScriptButton.setEnabled(true);
                    displayResultsMenuItem.setEnabled(false);
                    displayResultsButton.setEnabled(false);
                    // Disable ability to check status and cancel a job until Run Script selected.
                    checkStatusButton.setEnabled(false);
                    statusMenuItem.setEnabled(false);
                    cancelRunButton.setEnabled(false);
                    cancelRunMenuItem.setEnabled(false);

                    stdoutTextArea.setText(jobMonitor.fetchStdoutText());
                    stderrTextArea.setText(jobMonitor.fetchStderrText());
                }
            });
        }
    }//GEN-LAST:event_cancelRunButtonActionPerformed

    private void runScriptButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runScriptButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_runScriptButtonActionPerformed

    private void writeScriptButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_writeScriptButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_writeScriptButtonActionPerformed

    private void checkStatusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkStatusButtonActionPerformed
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_JOB_STATUS);
        ampExtTabbedPane.setSelectedIndex(tabIdx);
        //logger.info("job_status " + jobMonitor.getJobStatus());
        if (!jobMonitor.isJobSubmitted()) {
            JOptionPane.showMessageDialog(this, "Job Not Yet Submitted.", "Job Unsubmitted", JOptionPane.WARNING_MESSAGE);
            stateLabel.setText(jobMonitor.getJobStatus());
            runMenuItem.setEnabled(true);
            runScriptButton.setEnabled(true);
            writeScriptMenuItem.setEnabled(true);
            writeScriptButton.setEnabled(true);
            displayResultsMenuItem.setEnabled(true);
            displayResultsButton.setEnabled(true);
            // Disable ability to check status and cancel a job.
            checkStatusButton.setEnabled(false);
            statusMenuItem.setEnabled(false);
            cancelRunButton.setEnabled(false);
            cancelRunMenuItem.setEnabled(false);
            return;
        }

        jobMonitor.updateStatus(jobManager);

        // update GUI job monitoring components
        Runnable updateGUI = new Runnable() {
            public void run() {
                stateLabel.setText(jobMonitor.getJobStatus());
                if (jobMonitor.isJobEnded()) {
                    // enable another script to be generated and executed
                    runMenuItem.setEnabled(true);
                    runScriptButton.setEnabled(true);
                    writeScriptMenuItem.setEnabled(true);
                    writeScriptButton.setEnabled(true);
                    displayResultsMenuItem.setEnabled(true);
                    displayResultsButton.setEnabled(true);
                    // Disable ability to check status and cancel a job until Run Script selected.
                    checkStatusButton.setEnabled(false);
                    statusMenuItem.setEnabled(false);
                    cancelRunButton.setEnabled(false);
                    cancelRunMenuItem.setEnabled(false);
                } else {
                    runMenuItem.setEnabled(false);
                    runScriptButton.setEnabled(false);
                    writeScriptMenuItem.setEnabled(false);
                    writeScriptButton.setEnabled(false);
                    displayResultsMenuItem.setEnabled(false);
                    displayResultsButton.setEnabled(false);
                    // Enable ability to check status and cancel a job .
                    checkStatusButton.setEnabled(true);
                    statusMenuItem.setEnabled(true);
                    cancelRunButton.setEnabled(true);
                    cancelRunMenuItem.setEnabled(true);
                }
                stdoutTextArea.setText(jobMonitor.fetchStdoutText());
                stderrTextArea.setText(jobMonitor.fetchStderrText());
            }
        };
        javax.swing.SwingUtilities.invokeLater(updateGUI);
    }//GEN-LAST:event_checkStatusButtonActionPerformed

    private void ampExtTabbedPaneStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_ampExtTabbedPaneStateChanged
        int idx = ampExtTabbedPane.getSelectedIndex();
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_OUTPUTS);
        if(tabIdx == idx){
            if(landmarkOutputRadioButton.isSelected() && landmarkInRadioButton.isSelected()){
                if(landmark3DRadioButton.isSelected()){
                    landmark3DOutProjectTextField.setEnabled(true);
                    if(landmarkInProjectTextField.getText().trim().length() > 0){
                        landmark3DOutProjectTextField.setText(landmarkInProjectTextField.getText().trim());
                        landmark3DOutProjectTextField.setEnabled(false);
                        landmarkOutListButton.setEnabled(false);
                    }else{
                        landmark3DOutProjectTextField.setEnabled(true);
                        landmark3DOutProjectTextField.setEditable(true);
                        landmarkOutListButton.setEnabled(true);
                    }
                    formLandmark3DOutputDataPanel();
                }else if(landmark2DRadioButton.isSelected()){
                    landmark2DOutProjectTextField.setEnabled(true);
                    if(landmarkIn2DProjectTextField.getText().trim().length() > 0){
                        landmark2DOutProjectTextField.setText(landmarkIn2DProjectTextField.getText().trim());
                        landmark2DOutProjectTextField.setEnabled(false);
                        landmarkOutListButton.setEnabled(false);
                    }else{
                        landmark2DOutProjectTextField.setEnabled(true);
                        landmark2DOutProjectTextField.setEditable(true);
                        landmarkOutListButton.setEnabled(true);
                    }
                    formLandmark2DOutputDataPanel();
                }
            }else if(landmarkOutputRadioButton.isSelected() && bhpsuInRadioButton.isSelected()){
                landmarkOutProjectTextField.setEnabled(true);
                landmarkOutProjectTextField.setEditable(true);
                landmarkOutListButton.setEnabled(true);
                formLandmarkOutputBhpsuInDataPanel();
            }
            //outputTypeRadioButtonActionPerformed(null);
            //formOutputDataPanel();
            formOutputPanel();
            ampExtTabbedPane.setComponentAt(tabIdx, outputPanel);
        }
        int tabHorIdx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        if(idx == tabHorIdx){
            //if(bhpsuInRadioButton.isSelected())
            //  bhpsuHorizonRadioButton.setSelected(true);
            horizonInputTabActionPerformed(null);
        }

    }//GEN-LAST:event_ampExtTabbedPaneStateChanged

    private void importMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importMenuItemActionPerformed
        //Use a file chooser to select the state file
        QiProjectDescriptor projDesc = agent.getQiProjectDescriptor();
        ComponentStateUtils.callFileChooser(this, QiProjectDescUtils.getXmlPath(projDesc), QIWConstants.IMPORT_COMP_STATE, agent.getComponentDescriptor(), agent.getMessagingMgr());

        //NOTE: agent will invoke a file chooser service. When it processes the GET_RESULT_FROM_FILE_CHOOSER_CMD,
        //     it will read the state of the PM in XML and restore it using its restoreState() method.
    }//GEN-LAST:event_importMenuItemActionPerformed

    private void exportMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportMenuItemActionPerformed
        //Use a file chooser to specify the path of the file where the qiComponent's state will be saved.
        QiProjectDescriptor projDesc = agent.getQiProjectDescriptor();
        ComponentStateUtils.callFileChooser(gui, QiProjectDescUtils.getXmlPath(projDesc), QIWConstants.EXPORT_COMP_STATE, agent.getComponentDescriptor(), agent.getMessagingMgr());

        //NOTE:  agent will invoke a file chooser service. When it processes the GET_RESULT_FROM_FILE_CHOOSER_CMD,
        //     it will capture the state of the PM in XML by invoking its genState() method. Then it will
        //     invoke an IO service to write the state to a .xml file.
    }//GEN-LAST:event_exportMenuItemActionPerformed

    private void saveQuitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveQuitMenuItemActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_saveQuitMenuItemActionPerformed

    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuItemActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_saveMenuItemActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    //private javax.swing.JCheckBox exportToBhpsuCheckBox;
    //private javax.swing.JLabel lm2BhpsuPrefixLabel;
    //private javax.swing.JTextField lm2BhpsuPrefixTextField;


    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JRadioButton absTypeRadioButton;
    private javax.swing.JMenuBar ampExtMenuBar;
    private javax.swing.JTabbedPane ampExtTabbedPane;
    private javax.swing.JButton bottomHorizonBrowseButton;
    private javax.swing.JPanel outputDataPanel;
    private javax.swing.JLabel bottomHorizonLabel;
    private javax.swing.JTextField landmark3DBottomHorizonTextField;
    private javax.swing.JTextField landmark2DBottomHorizonTextField;
    private javax.swing.JTextField bhpsuBottomHorizonTextField;
    private javax.swing.JButton cancelRunButton;
    private javax.swing.JMenuItem cancelRunMenuItem;
    private javax.swing.JLabel cdpByLabel;
    private javax.swing.JLabel cdpByLabel1;
    private javax.swing.JTextField cdpByTextField;
    private javax.swing.JLabel cdpFromLabel;
    private javax.swing.JTextField cdpFromTextField;
    private javax.swing.JLabel cdpToLabel;
    private javax.swing.JTextField cdpToTextField;
    private javax.swing.JButton checkStatusButton;
    private javax.swing.JPanel detailsPanel;
    private javax.swing.JSplitPane detailsSplitPane;
    private javax.swing.JRadioButton edgeModeRadioButton;
    private javax.swing.JMenu editMenu;
    private javax.swing.JButton entireCubeRangeButton;
    private javax.swing.JButton entireVolumeButton;
    private javax.swing.JTextField epByTextField;
    private javax.swing.JLabel epFromLabel;
    private javax.swing.JTextField epFromTextField;
    private javax.swing.JLabel epToLabel;
    private javax.swing.JTextField epToTextField;
    private javax.swing.JMenu executeMenu;
    private javax.swing.JMenuItem exportMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JSeparator fileSeparator;
    private javax.swing.JPanel fileSummaryPanel;
    private javax.swing.JScrollPane fileSummaryScrollPane;
    private javax.swing.JTextArea fileSummaryTextArea;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JButton horizonBrowseButton;
    private javax.swing.JLabel horizonLabel;
    private javax.swing.JButton displayResultsButton;
    private javax.swing.JMenuItem displayResultsMenuItem;

    private javax.swing.JPanel landmarkIn2D3DDataPanel;
    private javax.swing.JButton list2DProjectsButton;
    private javax.swing.JComboBox seismic2DFileComboBox;
    private javax.swing.JScrollPane lineListScrollPane;
    private javax.swing.JScrollPane selectedLineListScrollPane;
    private javax.swing.JList seismic2DLineList;
    private javax.swing.JList selectedSeismic2DLineList;
    private javax.swing.JPanel seismicLinesPanel;
    private javax.swing.JLabel linesLabel;
    private javax.swing.JLabel selectedLinesLabel;
    private javax.swing.JButton linesListButton;
    private javax.swing.JLabel seismicFileLabel;
    private javax.swing.JLabel shotpointLabel;
    private javax.swing.JButton entireVolume2DButton;
    private javax.swing.JButton select2DButton;
    private javax.swing.JButton seismicFileListButton;
    private javax.swing.JTextField landmarkIn2DProjectTextField;
    private javax.swing.JTextField linesInTextField;
    private javax.swing.JTextField seismicFileTextField;
    private javax.swing.JTextField shotpointToTextField;
    private javax.swing.JTextField shotpointTextField;
    private javax.swing.JTextField shotpointByTextField;
    private javax.swing.JRadioButton landmark3DRadioButton;
    private javax.swing.JRadioButton landmark2DRadioButton;
    private javax.swing.JTextField landmarkInProjectTextField;
    private javax.swing.JTextField volumeTextField;

    private javax.swing.JPanel horizonParametersPanel;
    private javax.swing.JPanel landmark3DHorizonParametersPanel;
    private javax.swing.JPanel landmark2DHorizonParametersPanel;
    private javax.swing.JScrollPane horizonParametersScrollPane;
    private javax.swing.JScrollPane landmark3DHorizonParametersScrollPane;
    private javax.swing.JScrollPane landmark2DHorizonParametersScrollPane;
    private javax.swing.JTable horizonParametersTable;
    private javax.swing.JTable landmark3DHorizonParametersTable;
    private javax.swing.JTable landmark2DHorizonParametersTable;
    private javax.swing.JTextField landmark3DHorizonTextField;
    private javax.swing.JTextField landmark2DHorizonTextField;
    private javax.swing.JTextField bhpsuHorizonTextField;
    private javax.swing.JPanel horizonsPanel;
    private javax.swing.JPanel horizonDataPanel;
    private javax.swing.JPanel horizonsParametersPanel;
    private javax.swing.JPanel landmark3DHorizonsParametersPanel;
    private javax.swing.JPanel landmark2DHorizonsParametersPanel;
    private javax.swing.JScrollPane horizonsParametersScrollPane;
    private javax.swing.JScrollPane landmark3DHorizonsParametersScrollPane;
    private javax.swing.JScrollPane landmark2DHorizonsParametersScrollPane;
    private javax.swing.JTable horizonsParametersTable;
    private javax.swing.JTable landmark3DHorizonsParametersTable;
    private javax.swing.JTable landmark2DHorizonsParametersTable;
    private javax.swing.JMenuItem importMenuItem;
    private javax.swing.JButton inputCubeBrowseButton;
    private javax.swing.JLabel inputCubeLabel;
    private javax.swing.JTextField inputCubeTextField;
    private javax.swing.JPanel inputPanel;
    private javax.swing.JPanel inputDataPanel;
    private javax.swing.JRadioButton landmarkInRadioButton;
    private javax.swing.JRadioButton bhpsuInRadioButton;
    private javax.swing.JRadioButton bhpsuOutputRadioButton;
    private javax.swing.JRadioButton landmarkOutputRadioButton;
    private javax.swing.JRadioButton landmarkHorizonRadioButton;
    private javax.swing.JRadioButton bhpsuHorizonRadioButton;
    private javax.swing.JRadioButton bhpsuHorizonDatasetRadioButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel landmarkInPanel;
    private javax.swing.JLabel landmarkProjectLabel;
    private javax.swing.JLabel landmarkRangeLabel;
    private javax.swing.JLabel linesByLabel;
    private javax.swing.JTextField linesByTextField;
    private javax.swing.JTextField linesTextField;
    private javax.swing.JLabel linesToLabel;
    private javax.swing.JTextField linesToTextField;
    private javax.swing.JButton listProjectsButton;
    private javax.swing.JButton leftArrowButton;
    private javax.swing.JButton rightArrowButton;
    private javax.swing.JButton listVolumesButton;
    private javax.swing.JLabel outputCubeLabel;
    private javax.swing.JTextField outputCubeTextField;//input bhpsu output bhpsu
    private javax.swing.JTextField landmark3DOutputCubeTextField; //input landmark 3D output landmark 3D
    private javax.swing.JTextField landmark2DOutputCubeTextField; //input landmark 2D output landmark 2D
    private javax.swing.JTextField landmarkOutputCubeTextField; //input bhpsu output landmark
    private javax.swing.JLabel outputHorizonsLabel;
    private javax.swing.JTextField outputHorizonsTextField; //input bhpsu output bhpsu
    private javax.swing.JTextField landmark3DOutputHorizonsTextField;//input landmark 3D output landmark 3D
    private javax.swing.JTextField landmark2DOutputHorizonsTextField;//input landmark 2D output landmark 2D
    private javax.swing.JTextField landmarkOutputHorizonsTextField; //input bhpsu output landmark
    private javax.swing.JPanel outputPanel;
    private javax.swing.JLabel landmarkOutProjectLabel;
    private javax.swing.JTextField landmarkOutProjectTextField; //input bhpsu output landmark
    private javax.swing.JTextField landmark3DOutProjectTextField;
    private javax.swing.JTextField landmark2DOutProjectTextField;
    private javax.swing.JButton landmarkOutListButton;
    private javax.swing.JPanel parametersPanel;
    private ProcessPanel processPanel;
    private javax.swing.JPanel inputParametersPanel;
    private javax.swing.JRadioButton peakTypeRadioButton;
    private javax.swing.JButton pickRangeFromViewerButton;
    private javax.swing.JRadioButton pointModeRadioButton;
    private javax.swing.JTextField projectTextField;
    private javax.swing.JMenuItem quitMenuItem;
    private javax.swing.JLabel rangeLabel;
    private javax.swing.JLabel rangeLinesLabel;
    private javax.swing.JMenuItem runMenuItem;
    private javax.swing.JButton runScriptButton;
    private javax.swing.JMenuItem saveMenuItem;
    private javax.swing.JMenuItem saveAsMenuItem;
    private javax.swing.JMenuItem saveQuitMenuItem;
    private javax.swing.JButton selectButton;
    private javax.swing.JLabel stateLabel;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JMenuItem statusMenuItem;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JScrollPane stderrScrollPane;
    private javax.swing.JTextArea stderrTextArea;
    private javax.swing.JScrollPane stdoutScrollPane;
    private javax.swing.JTextArea stdoutTextArea;
    private javax.swing.JPanel targetTypePanel;
    private javax.swing.JPanel landmark3DTargetTypePanel;
    private javax.swing.JPanel landmark2DTargetTypePanel;
    private javax.swing.JButton topHorizonBrowseButton;
    private javax.swing.JLabel topHorizonLabel;
    private javax.swing.JTextField landmark3DTopHorizonTextField;
    private javax.swing.JTextField landmark2DTopHorizonTextField;
    private javax.swing.JTextField bhpsuTopHorizonTextField;
    private javax.swing.JLabel tracesByLabel;
    private javax.swing.JTextField tracesByTextField;
    private javax.swing.JLabel tracesLabel;
    private javax.swing.JTextField tracesTextField;
    private javax.swing.JLabel tracesToLabel;
    private javax.swing.JTextField tracesToTextField;
    private javax.swing.JRadioButton troughTypeRadioButton;
    private javax.swing.JLabel volumeLabel;
    private javax.swing.JTextField voumeTextField;
    private javax.swing.JButton writeScriptButton;
    private javax.swing.JMenuItem writeScriptMenuItem;
    private javax.swing.JButton clearButton;
    private int lineTraceArray[][] = {{0,0,0},{0,0,0}};
    private int defaultEpFrom = -1;
    private int defaultEpTo = -1;
    private int defaultEpBy = -1;
    private int defaultCdpFrom = -1;
    private int defaultCdpTo = -1;
    private int defaultCdpBy = -1;
    private int landmark3DHorizonTargetType  = 1; //Trough by default
    private int landmark2DHorizonTargetType  = 1; //Trough by default
    private int bhpsuHorizonTargetType  = 1; //Trough by default
    private Properties props;
    // End of variables declaration//GEN-END:variables

    private void switch2BhpsuInput(){
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);

        formBhpsuInputDataPanel();
        formInputPanel();

        //horizonsPanel = new javax.swing.JPanel();
        //horizonDataPanel = new javax.swing.JPanel();
        inputParametersPanel = new javax.swing.JPanel();
        //horizonsParametersPanel = new javax.swing.JPanel();

        //if(edgeModeRadioButton.isSelected()){
        //  formEdgeModeHorizonPanel();
        //}else if(pointModeRadioButton.isSelected()){
        //  formPointModeHorizonPanel();
        //}
        //formHorizonsPanel();
        layoutInputParametersTabbedPane();

        //if(pointModeRadioButton.isSelected()){
        //  pointModeHorizonParamsTableModel = horizonParametersTable.getModel();
        //  addPointModeHorizonParamsListener(pointModeHorizonParamsTableModel);
            //restore table cell settings for point mode
        //  for (int j=0; j<3; j++)
        //      pointModeHorizonParamsTableModel.setValueAt(pointModeHorizonSettings[0][j], 0, j);
        //}else if(edgeModeRadioButton.isSelected()){
        //  edgeModeHorizonParamsTableModel = horizonsParametersTable.getModel();
        //  addEdgeModeHorizonParamsListener(edgeModeHorizonParamsTableModel);
            //restore table cell settings for edge mode
        //  for (int i=0; i<2; i++)
        //      for (int j=0; j<3; j++)
        //          edgeModeHorizonParamsTableModel.setValueAt(edgeModeHorizonSettings[i][j], i, j+1);
        //}
        ampExtTabbedPane.setComponentAt(tabIdx, inputParametersPanel);
    }
    private void switch2LandmarkInput(){
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_SEISMIC_DATA_INPUT);

        if(landmark3DRadioButton.isSelected())
            formLandmark3DPanel();
        else if(landmark2DRadioButton.isSelected())
            formLandmark2DPanel();
        formLandmarkInputDataPanel();
        formInputPanel();

        //horizonsPanel = new javax.swing.JPanel();
        //horizonDataPanel = new javax.swing.JPanel();
        inputParametersPanel = new javax.swing.JPanel();
        //horizonsParametersPanel = new javax.swing.JPanel();

        //if(edgeModeRadioButton.isSelected()){
        //  formEdgeModeHorizonPanel();
        //}else if(pointModeRadioButton.isSelected()){
        //  formPointModeHorizonPanel();
        //}
        //formHorizonsPanel();
        layoutInputParametersTabbedPane();

        //if(pointModeRadioButton.isSelected()){
        //  pointModeHorizonParamsTableModel = horizonParametersTable.getModel();
        //  addPointModeHorizonParamsListener(pointModeHorizonParamsTableModel);
            //restore table cell settings for point mode
        //  for (int j=0; j<3; j++)
        //      pointModeHorizonParamsTableModel.setValueAt(pointModeHorizonSettings[0][j], 0, j);
        //}else if(edgeModeRadioButton.isSelected()){
        //  edgeModeHorizonParamsTableModel = horizonsParametersTable.getModel();
        //  addEdgeModeHorizonParamsListener(edgeModeHorizonParamsTableModel);
            //restore table cell settings for edge mode
        //  for (int i=0; i<2; i++)
        //      for (int j=0; j<3; j++)
        //          edgeModeHorizonParamsTableModel.setValueAt(edgeModeHorizonSettings[i][j], i, j+1);
        //}
        ampExtTabbedPane.setComponentAt(tabIdx, inputParametersPanel);
    }
    /**
     * Switch to edge mode, i.e., specifying two horizons
     */
    private void switch2EdgeMode() {
        if (horizonMode == EDGE_MODE) return;
        horizonMode = EDGE_MODE;

        //Note: table cell settings for point mode already captured by change listener
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        // Note: radio buttons automatically set correctly since in a group

        horizonsPanel = new javax.swing.JPanel();
        horizonDataPanel = new javax.swing.JPanel();
        parametersPanel = new javax.swing.JPanel();
        horizonsParametersPanel = new javax.swing.JPanel();
        formEdgeModeHorizonPanel();
        formHorizonsPanel();


        targetTypePanel = new javax.swing.JPanel();
        formTargetPanel(targetTypePanel,bhpsuHorizonTargetType);
        layoutParametersTabbedPane(targetTypePanel);
        edgeModeHorizonParamsTableModel = horizonsParametersTable.getModel();
        addEdgeModeHorizonParamsListener(edgeModeHorizonParamsTableModel);
//        ((DefaultTableModel)edgeModeHorizonParamsTableModel).fireTableRowsUpdated(1,1);
//        ((DefaultTableModel)edgeModeHorizonParamsTableModel).fireTableRowsUpdated(2,2);

        //restore table cell settings for edge mode
        for (int i=0; i<2; i++)
            for (int j=0; j<3; j++)
                edgeModeHorizonParamsTableModel.setValueAt(edgeModeHorizonSettings[i][j], i, j+1);

        ampExtTabbedPane.setComponentAt(tabIdx, parametersPanel);
    }

    /**
     * Switch to edge mode, i.e., specifying two horizons
     */
    private void switch2Landmark3DEdgeMode() {
        if (horizonMode == EDGE_MODE) return;
        horizonMode = EDGE_MODE;

        //Note: table cell settings for point mode already captured by change listener
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        // Note: radio buttons automatically set correctly since in a group

        horizonsPanel = new javax.swing.JPanel();
        horizonDataPanel = new javax.swing.JPanel();
        parametersPanel = new javax.swing.JPanel();
        landmark3DHorizonsParametersPanel = new javax.swing.JPanel();
        formLandmark3DEdgeModeHorizonPanel();
        formHorizonsPanel();

        landmark3DTargetTypePanel = new javax.swing.JPanel();
        formTargetPanel(landmark3DTargetTypePanel, landmark3DHorizonTargetType);
        layoutParametersTabbedPane(landmark3DTargetTypePanel);
        landmark3DEdgeModeHorizonParamsTableModel = landmark3DHorizonsParametersTable.getModel();
        addLandmark3DEdgeModeHorizonParamsListener(landmark3DEdgeModeHorizonParamsTableModel);
//        ((DefaultTableModel)edgeModeHorizonParamsTableModel).fireTableRowsUpdated(1,1);
//        ((DefaultTableModel)edgeModeHorizonParamsTableModel).fireTableRowsUpdated(2,2);

        //restore table cell settings for edge mode
        for (int i=0; i<2; i++)
            for (int j=0; j<3; j++)
                landmark3DEdgeModeHorizonParamsTableModel.setValueAt(landmark3DEdgeModeHorizonSettings[i][j], i, j+1);

        ampExtTabbedPane.setComponentAt(tabIdx, parametersPanel);
    }

    /**
     * Switch to edge mode, i.e., specifying two horizons
     */
    private void switch2Landmark2DEdgeMode() {
        if (horizonMode == EDGE_MODE) return;
        horizonMode = EDGE_MODE;

        //Note: table cell settings for point mode already captured by change listener
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        // Note: radio buttons automatically set correctly since in a group

        horizonsPanel = new javax.swing.JPanel();
        horizonDataPanel = new javax.swing.JPanel();
        parametersPanel = new javax.swing.JPanel();
        landmark2DHorizonsParametersPanel = new javax.swing.JPanel();
        formLandmark2DEdgeModeHorizonPanel();
        formHorizonsPanel();

        landmark2DTargetTypePanel = new javax.swing.JPanel();
        formTargetPanel(landmark2DTargetTypePanel, landmark2DHorizonTargetType);
        layoutParametersTabbedPane(landmark2DTargetTypePanel);
        landmark2DEdgeModeHorizonParamsTableModel = landmark2DHorizonsParametersTable.getModel();
        addLandmark2DEdgeModeHorizonParamsListener(landmark2DEdgeModeHorizonParamsTableModel);
//        ((DefaultTableModel)edgeModeHorizonParamsTableModel).fireTableRowsUpdated(1,1);
//        ((DefaultTableModel)edgeModeHorizonParamsTableModel).fireTableRowsUpdated(2,2);

        //restore table cell settings for edge mode
        for (int i=0; i<2; i++)
            for (int j=0; j<3; j++)
                landmark2DEdgeModeHorizonParamsTableModel.setValueAt(landmark2DEdgeModeHorizonSettings[i][j], i, j+1);

        ampExtTabbedPane.setComponentAt(tabIdx, parametersPanel);
    }

    /**
     * Switch to point mode, i.e., specifying one horizon
     */
    private void switch2PointMode() {
        if (horizonMode == POINT_MODE) return;
        horizonMode = POINT_MODE;

        //Note: table cell settings for edge mode already captured by change listener
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        // Note: radio buttons automatically set correctly since in a group

        horizonsPanel = new javax.swing.JPanel();
        horizonDataPanel = new javax.swing.JPanel();
        parametersPanel = new javax.swing.JPanel();
        horizonsParametersPanel = new javax.swing.JPanel();
        formPointModeHorizonPanel();
        formHorizonsPanel();




        targetTypePanel = new javax.swing.JPanel();
        formTargetPanel(targetTypePanel,bhpsuHorizonTargetType);

        layoutParametersTabbedPane(targetTypePanel);
        pointModeHorizonParamsTableModel = horizonParametersTable.getModel();
        addPointModeHorizonParamsListener(pointModeHorizonParamsTableModel);

        //restore table cell settings for point mode
        for (int j=0; j<3; j++)
            pointModeHorizonParamsTableModel.setValueAt(pointModeHorizonSettings[0][j], 0, j);

        ampExtTabbedPane.setComponentAt(tabIdx, parametersPanel);
    }

    /**
     * Switch to point mode, i.e., specifying one horizon
     */
    private void switch2Landmark3DPointMode() {
        if (horizonMode == POINT_MODE) return;
        horizonMode = POINT_MODE;

        //Note: table cell settings for edge mode already captured by change listener
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        // Note: radio buttons automatically set correctly since in a group

        horizonsPanel = new javax.swing.JPanel();
        horizonDataPanel = new javax.swing.JPanel();
        parametersPanel = new javax.swing.JPanel();
        landmark3DHorizonsParametersPanel = new javax.swing.JPanel();
        formLandmark3DPointModeHorizonPanel();
        formHorizonsPanel();



        landmark3DTargetTypePanel = new javax.swing.JPanel();
        formTargetPanel(landmark3DTargetTypePanel, landmark3DHorizonTargetType);
        layoutParametersTabbedPane(landmark3DTargetTypePanel);
        landmark3DPointModeHorizonParamsTableModel = landmark3DHorizonParametersTable.getModel();
        addLandmark3DPointModeHorizonParamsListener(landmark3DPointModeHorizonParamsTableModel);

        //restore table cell settings for point mode
        for (int j=0; j<3; j++)
            landmark3DPointModeHorizonParamsTableModel.setValueAt(landmark3DPointModeHorizonSettings[0][j], 0, j);

        ampExtTabbedPane.setComponentAt(tabIdx, parametersPanel);
    }

    private void switch2Landmark2DPointMode() {
        if (horizonMode == POINT_MODE) return;
        horizonMode = POINT_MODE;

        //Note: table cell settings for edge mode already captured by change listener
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        // Note: radio buttons automatically set correctly since in a group

        horizonsPanel = new javax.swing.JPanel();
        horizonDataPanel = new javax.swing.JPanel();
        parametersPanel = new javax.swing.JPanel();
        landmark2DHorizonsParametersPanel = new javax.swing.JPanel();
        formLandmark2DPointModeHorizonPanel();
        formHorizonsPanel();



        landmark2DTargetTypePanel = new javax.swing.JPanel();
        formTargetPanel(landmark2DTargetTypePanel, landmark2DHorizonTargetType);
        layoutParametersTabbedPane(landmark2DTargetTypePanel);
        landmark2DPointModeHorizonParamsTableModel = landmark2DHorizonParametersTable.getModel();
        addLandmark2DPointModeHorizonParamsListener(landmark2DPointModeHorizonParamsTableModel);

        //restore table cell settings for point mode
        for (int j=0; j<3; j++)
            landmark2DPointModeHorizonParamsTableModel.setValueAt(landmark2DPointModeHorizonSettings[0][j], 0, j);

        ampExtTabbedPane.setComponentAt(tabIdx, parametersPanel);
    }

    private void formHorizonsPanel(){
        horizonsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Horizons", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12)));
        landmarkHorizonRadioButton.setText("Landmark");
        landmarkHorizonRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        landmarkHorizonRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        bhpsuHorizonRadioButton.setText("ASCII");
        bhpsuHorizonRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        bhpsuHorizonRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        bhpsuHorizonDatasetRadioButton.setText("BHP SU");
        bhpsuHorizonDatasetRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        bhpsuHorizonDatasetRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        org.jdesktop.layout.GroupLayout horizonsPanelLayout = new org.jdesktop.layout.GroupLayout(horizonsPanel);
        horizonsPanel.setLayout(horizonsPanelLayout);
        horizonsPanelLayout.setHorizontalGroup(
            horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkHorizonRadioButton)
                .add(20, 20, 20)
                .add(bhpsuHorizonRadioButton)
                .add(20, 20, 20)
                .add(bhpsuHorizonDatasetRadioButton)
                .addContainerGap(495, Short.MAX_VALUE))
            .add(horizonDataPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        horizonsPanelLayout.setVerticalGroup(
            horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(horizonsPanelLayout.createSequentialGroup()
                .add(horizonsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkHorizonRadioButton)
                    .add(bhpsuHorizonRadioButton)
                    .add(bhpsuHorizonDatasetRadioButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 13, Short.MAX_VALUE)
                .add(horizonDataPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
    }

    /** Set title to new name.
     *
     * @param name from plugin message processor
     */
    public void renamePlugin(String name) {
//        this.setTitle(name + "  Project: " + project);
        QiProjectDescriptor qpDesc = agent.getQiProjectDescriptor();
        project = QiProjectDescUtils.getQiProjectName(qpDesc);
        resetTitle(project);
    }

    /** Get the location of the GUI,.
     *
     * @return x, y
     */
    // TODO replace with ComponentListener ??
    protected Point getMyLocation() {
        return getLocation();
    }

    /** Execute the generated script.
     * @param fileName Name of the script file selected by the user using the file chooser service.
     * @deprecated No longer go thru a file chooser to specify the name of the script file.
     * Instead, from a unique name from a base name.
     */
    public void execScript(String fileName) {
        String script = "";
        if (!fileName.equals(""))
            script = fileName.substring(fileName.lastIndexOf(filesep)+1, fileName.lastIndexOf("."));
        else {
            agent.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                    "Script not saved by the file chooser service");
            return;
        }

        executeScript(script);
    }

    /**
     * Submit the generated script, which has already been saved, for execution.
     * Initialize GUI job monitoring components.
     *
     * @param scriptPrefix Prefix of script to be executed.
     */
    private void executeScript(String scriptPrefix) {
        this.gui = this;

        //Get the most recent project descriptor
        qiProjectDesc = agent.getQiProjectDescriptor();
        String scriptDir = QiProjectDescUtils.getTempPath(qiProjectDesc);
        String filePrefix = scriptDir + filesep + scriptPrefix;
        // Create a job monitor for the script.
        jobMonitor = new JobMonitor(filePrefix, scriptDir, scriptPrefix);
        // Create a job manager for the script.
        jobManager = new JobManager(agent.getMessagingMgr(), this);

        String scriptFile = jobMonitor.getScriptPath();
        String stdoutFile = jobMonitor.getStdoutPath();
        String stderrFile = jobMonitor.getStderrPath();

        ArrayList params = new ArrayList();
        // make sure script is executable
        params.add("cd " + scriptDir + "; chmod u+x "
                + scriptFile + "; sh " + scriptFile + " 1>" + stdoutFile + " 2>" + stderrFile);
        params.add("false");
        // submit the script as a possibly long running job
        jobManager.submitJob(params);

        jobMonitor.setJobStatusToRunning();

        // update GUI job monitoring components
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                stateLabel.setText(jobMonitor.getJobStatus());
                // disable the ability to run another generated script
                runMenuItem.setEnabled(false);
                runScriptButton.setEnabled(false);
                writeScriptMenuItem.setEnabled(false);
                writeScriptButton.setEnabled(false);
                displayResultsMenuItem.setEnabled(false);
                displayResultsButton.setEnabled(false);
                // enable the ability to check status and cancel the job
                checkStatusButton.setEnabled(true);
                statusMenuItem.setEnabled(true);
                cancelRunButton.setEnabled(true);
                cancelRunMenuItem.setEnabled(true);

                stdoutTextArea.setText("");
                stderrTextArea.setText("");

                String text = jobMonitor.getScriptPath() + "\n";
                text += jobMonitor.getStderrPath() + "\n";
                text += jobMonitor.getStdoutPath() + "\n";
                text += "have been created in " + jobMonitor.getScriptDir();
                fileSummaryTextArea.setText(text);
            }
        });


/* Deprecated. Monitor the job's execution instead.
        int status = AmpExtConstants.OK_STATUS;
        status = fileManager.executeScript(gui, filePath);
        if (status != AmpExtConstants.OK_STATUS)
            agent.showErrorDialog(QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                            "Execution of generated script failed",
                            new String[] {"Script was not saved by 'Save Parameters to Script' button",
                                          "Script has SU syntax error",
                                         "A file referenced by the script is missing"},
                            new String[] {"Try running the script manually",
                                          "If it runs OK, contact workbench support",
                                          "If it fails, try 'Save Parameters to Script' then 'Execute Script'",
                                          "If it fails again, contact workbench support"});
*/
    }

    /**
     * Load parameters from script
     * @param fileName returned from file chooser service
     *
     * @deprecated Functionality to execute an existing script removed. Always generate the script to be executed.
     */
    public void loadScript(String fileName) {
        int status = AmpExtConstants.OK_STATUS;
        if (!fileName.equals("")) {
            String script = fileName.substring(fileName.lastIndexOf(filesep)+1,fileName.lastIndexOf("."));
            status = loadParamsFromScript(script);
            if (status == AmpExtConstants.OK_STATUS) {
                ArrayList<String> seismicDirs = QiProjectDescUtils.getSeismicDirs(agent.getQiProjectDescriptor());
                //assume first seismic dir the primary dir
                //TODO Use bhpio command. Do when put in adjustable ranges
                headerLimits = new AmpExtFileManager(agent).getHeaderLimits(gui,fileSystem + filesep + project + "/"+seismicDirs.get(0)+"/" +inputCubeTextField.getText() + "_0000.0001.HDR");
                initRanges(headerLimits);
                if (AmpExtConstants.DEBUG_PRINT > 0) showHeaderLimits();
            }
        } else {
            agent.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                    "No script returned from file chooser service");
        }
    }

    /**
     * Have the file manager load a script file; then extract parameters and fill GUI
     * @param file filename part of /path/script.sh
     * @return integer status, 0=OK, else failure
     *
     * @deprecated Functionality to execute an existing script removed. Always generate the script to be executed.
     */
    private int loadParamsFromScript(String file) {
        int status = AmpExtConstants.OK_STATUS;
        String value = "";
        //Get the most recent project descriptor
        qiProjectDesc = agent.getQiProjectDescriptor();
        String scriptPath = QiProjectDescUtils.getTempPath(qiProjectDesc);
        // build path to script file and load it
        String filePath = scriptPath + filesep + file + ".sh";
        ArrayList<String> script = new AmpExtFileManager(agent).loadScriptFromFile(gui,filePath);
        if (script.isEmpty()) {
            agent.showErrorDialog(QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                       "Script file " + filePath + " is empty",
                       new String[] {"Script was not saved with 'Save Parameters to Script'",
                                     "'Save Parameters to Script' failed",
                                     "Internal workbench error"},
                       new String[] {"Try 'Save Parameters to Script', then 'Load Parameters from Script'",
                                     "If that fails, contact workbench support"});
            return AmpExtConstants.ERROR_STATUS;
        }

        // input filename
        value = findParam(script,"bhpreadcube",1,"filename");
        if (value.equals("")) {
            agent.showErrorDialog(QIWConstants.WARNING_DIALOG,Thread.currentThread().getStackTrace(),
                       "Input Filename not found in script",new String[] {"bhpreadcube command missing"},
                       new String[] {"Set Input parameter with 'Select Input' button",
                                     "If script contains bhpreadcube command, contact workbench support"});
        } else
            inputCubeTextField.setText(value);

        // output filename
        value = findParam(script,"bhpwritecube",1,"filename");
        if (value.equals("")) {
            agent.showErrorDialog(QIWConstants.WARNING_DIALOG,Thread.currentThread().getStackTrace(),
                       "Output Filename not found in script",new String[] {"bhpwritecube command missing"},
                       new String[] {"Set Output parameter with 'Select Output' button",
                                     "If script contains bhpwritecube command, contact workbench support"});
        } else
            outputCubeTextField.setText(value);

        // two horizons
        if (edgeModeRadioButton.isSelected()) {
            // top horizon
            value = findParam(script,"bhploadhdr",1,"file");
            if (value.equals(""))
                agent.showErrorDialog(QIWConstants.WARNING_DIALOG,Thread.currentThread().getStackTrace(),
                             "Top Horizon not found in script",new String[] {"bhloadhhdr command missing"},
                             new String[] {"Set Top Horizon parameter with 'Select Top Horizon' button",
                                           "If script contains bhploadhdr command, contact workbench support"});
            else {
                value = value.substring(value.lastIndexOf(filesep)+1);
                value = value.substring(0,value.lastIndexOf("."));
                bhpsuTopHorizonTextField.setText(value);
            }

            // bottom horizon
            value = findParam(script,"bhploadhdr",2,"file");
            if (value.equals("")) {
                agent.showErrorDialog(QIWConstants.WARNING_DIALOG,Thread.currentThread().getStackTrace(),
                             "Bottom Horizon not found in script",new String[] {"bhloadhhdr command missing"},
                             new String[] {"Set Bottom Horizon parameter with 'Select Top Horizon' button",
                                           "If script contains bhploadhdr command, contact workbench support"});
            } else {
                value = value.substring(value.lastIndexOf(filesep)+1);
                value = value.substring(0,value.lastIndexOf("."));
                bhpsuBottomHorizonTextField.setText(value);
            }

            // top horizon bias
            value = findParam(script,"bhploadhdr",1,"bias");
            if (value.equals("")) {
                agent.showErrorDialog(QIWConstants.WARNING_DIALOG,Thread.currentThread().getStackTrace(),
                             "Top Horizon Bias not found in script",new String[] {"bhloadhhdr command missing"},
                             new String[] {"Set Top Horizon Bias parameter by entering text",
                                           "If script contains bhploadhdr command, contact workbench support"});
            } else
                edgeModeHorizonParamsTableModel.setValueAt(value, TOP_HORIZON_BIAS_ROW, TOP_HORIZON_BIAS_COL);

            // bottom horizon bias
            value = findParam(script,"bhploadhdr",2,"bias");
            if (value.equals(""))
                agent.showErrorDialog(QIWConstants.WARNING_DIALOG,Thread.currentThread().getStackTrace(),
                             "Bottom Horizon Bias not found in script",
                             new String[] {"bhloadhhdr command missing"},
                             new String[] {"Set Bottom Horizon Bias parameter by entering text",
                                           "If script contains bhploadhdr command, contact workbench support"});
            else
                edgeModeHorizonParamsTableModel.setValueAt(value, BOTTOM_HORIZON_BIAS_ROW, BOTTOM_HORIZON_BIAS_COL);
        }
        // one horizon
        else {
            // horizon
            value = findParam(script,"bhploadhdr",1,"file");
            if (value.equals("")) {
                agent.showErrorDialog(QIWConstants.WARNING_DIALOG,Thread.currentThread().getStackTrace(),
                         "Horizon parameter not found in script",
                         new String[] {"bhloadhhdr command missing"},
                         new String[] {"Set Horizon with 'Select Horizon' button",
                                       "If script contains bhploadhdr command, contact workbench support"});
            } else {
                value = value.substring(value.lastIndexOf(filesep)+1);
                value = value.substring(0,value.lastIndexOf("."));
                bhpsuHorizonTextField.setText(value);
            }

            // bias
            value = findParam(script,"bhploadhdr",1,"bias");
            if (value.equals(""))
                agent.showErrorDialog(QIWConstants.WARNING_DIALOG,Thread.currentThread().getStackTrace(),
                         "Horizon Bias not found in script",
                         new String[] {"bhloadhhdr command missing"},
                         new String[] {"Set Horizon Bias by entering text"});
            else
                pointModeHorizonParamsTableModel.setValueAt(value, HORIZON_BIAS_ROW, HORIZON_BIAS_COL);
        }

        // type
        value = findParam(script,"bhpintegrate",1,"type");
        if (value.equals(""))
            agent.showErrorDialog(QIWConstants.WARNING_DIALOG,Thread.currentThread().getStackTrace(),
                       "Target Type not found in script",
                       new String[] {"bhintegrate command missing"},
                       new String[] {"Set Target Type by selecting Peak, Trough, or Abs button"});
        else if (value.equals("trough"))
            troughTypeRadioButton.setSelected(true);
        else if (value.equals("peak"))
            peakTypeRadioButton.setSelected(true);
        else if (value.equals("abs"))
            absTypeRadioButton.setSelected(true);

        // search
        value = findParam(script,"bhpintegrate",1,"search");
        if (value.equals(""))
            agent.showErrorDialog(QIWConstants.WARNING_DIALOG,Thread.currentThread().getStackTrace(),
                       "bhpintegrate search values not found in script",
                       new String[] {"bhintegrate command missing"},
                       new String[] {"Set Search Above Top, etc by entering text"});
        else {
            String search[] = value.split(",");
            // edge mode (two horizons)
            if (edgeModeRadioButton.isSelected()) {
                if (search.length < 4)
                    agent.showErrorDialog(QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                           "Error analysing search values",
                           new String[] {"bhintegrate command missing"},
                           new String[] {"Set Search Above Top, etc by entering text"});
                else {
                    topHorizonAbove = search[0];
                    edgeModeHorizonParamsTableModel.setValueAt(topHorizonAbove, TOP_HORIZON_ABOVE_ROW, TOP_HORIZON_ABOVE_COL);
                    topHorizonBelow = search[1];
                    edgeModeHorizonParamsTableModel.setValueAt(topHorizonBelow, TOP_HORIZON_BELOW_ROW, TOP_HORIZON_BELOW_COL);
                    bottomHorizonAbove = search[2];
                    edgeModeHorizonParamsTableModel.setValueAt(bottomHorizonAbove, BOTTOM_HORIZON_ABOVE_ROW, BOTTOM_HORIZON_ABOVE_COL);
                    bottomHorizonBelow = search[3];
                    edgeModeHorizonParamsTableModel.setValueAt(bottomHorizonBelow, BOTTOM_HORIZON_BELOW_ROW, BOTTOM_HORIZON_BELOW_COL);
                }
            }
            // point mode (1 horizon)
            else {
                if (search.length < 2)
                    agent.showErrorDialog(QIWConstants.WARNING_DIALOG,Thread.currentThread().getStackTrace(),
                           "Error analysing search values",
                           new String[] {"bhintegrate command missing"},
                           new String[] {"Set Search Above Top, etc by entering text"});
                else {
                    horizonAbove = search[0];
                    pointModeHorizonParamsTableModel.setValueAt(horizonAbove, HORIZON_ABOVE_ROW, HORIZON_ABOVE_COL);
                    horizonBelow = search[1];
                    pointModeHorizonParamsTableModel.setValueAt(horizonBelow, HORIZON_BELOW_ROW, HORIZON_BELOW_COL);
                }
            }
        }

        return AmpExtConstants.OK_STATUS;
    }

    /** save parameters to script
     * @param fileName returned from file chooser service
     * @deprecated No longer go thru a file chooser to specify the name of the script file.
     * Instead, from a unique name from a base name.
     */
    public void saveScript(String fileName) {
        if (!fileName.equals(""))
            saveParamsToScript(fileName);
        else
            agent.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                    "No file path returned from file chooser service");
    }

    /**
     * Write the generated script to a unique named file. The file name is created
     * from a base name.
     * @return Prefix of script file, i.e., name of script file without .sh suffix,
     * if file written; otherwise the empty string.
     */
    private String saveScript() {
        String baseName = "";
        if(bhpsuOutputRadioButton.isSelected()){
            baseName = createBaseName(outputCubeTextField.getText().trim());
        }else if(landmarkOutputRadioButton.isSelected()){
            if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                baseName = createBaseName(landmark2DOutputCubeTextField.getText());
            }else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                baseName = createBaseName(landmark3DOutputCubeTextField.getText());
            }else if(bhpsuInRadioButton.isSelected()){
                baseName = createBaseName(landmarkOutputCubeTextField.getText());
            }
        }

        if(processPanel.isNotRunAmpExtChecked()){
            if(isLandmark3DSelected())
                baseName = volumeTextField.getText().trim().substring(0, volumeTextField.getText().trim().lastIndexOf("."));
            else if(isLandmark2DSelected()){
                baseName = (String)seismic2DFileComboBox.getSelectedItem();
            }else if(bhpsuInRadioButton.isSelected()){
                baseName = inputCubeTextField.getText().trim();
            }
        }
        String scriptPrefix = createScriptPrefix(baseName);

        String fileName = scriptPrefix + ".sh";
        //Get the most recent project descriptor
        qiProjectDesc = agent.getQiProjectDescriptor();
        String scriptDir = QiProjectDescUtils.getTempPath(qiProjectDesc);
        String filePath = scriptDir + filesep + fileName;
        return saveParamsToScript(filePath) ? scriptPrefix : "";
    }

    /**
     * Create the base name of the script file.
     * @param rootRelPath Relative path of an output file the end of which is the base name.
     * @return Base name of the script file
     */
    private String createBaseName(String rootRelPath) {
        String baseName = rootRelPath;

        int ind = baseName.lastIndexOf(filesep);
        if (ind != -1) {
            baseName = baseName.substring(ind+1);
        }

        return baseName;
    }

    /**
     * Create a unique name for the generated script from a base name.
     */
    private String createScriptPrefix(String baseName) {
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat formatter = new SimpleDateFormat(
            "yyyy_MM_dd_HH_mm_ss_SSS");
        String scriptPrefix = baseName + "_" + formatter.format(ts);

        return scriptPrefix;
    }

    /**
     * Initialize ranges for line (ep) and trace (cdp) lines. That is,
     * initialize range textfields. Information obtained from input cube's header.
     * @param headerLimits Input cube's header limits.
     */
    private void initRanges(ArrayList<ArrayList<String>> headerLimits) {
        String lineKey = headerLimits.get(0).get(0);
        String traceKey = headerLimits.get(1).get(0);

        if (lineKey.equals("ep")) {
            epFromTextField.setText(headerLimits.get(0).get(1));
            defaultEpFrom = Integer.valueOf(headerLimits.get(0).get(1));
            epToTextField.setText(headerLimits.get(0).get(2));
            defaultEpTo = Integer.valueOf(headerLimits.get(0).get(2));
            epByTextField.setText(headerLimits.get(0).get(3));
            defaultEpBy = Integer.valueOf(headerLimits.get(0).get(3));

        } else {
            epFromTextField.setText(headerLimits.get(1).get(1));
            epToTextField.setText(headerLimits.get(1).get(2));
            epByTextField .setText(headerLimits.get(1).get(3));
            defaultEpFrom = Integer.valueOf(headerLimits.get(1).get(1));
            defaultEpTo = Integer.valueOf(headerLimits.get(1).get(2));
            defaultEpBy = Integer.valueOf(headerLimits.get(1).get(3));
        }
        epIncrDefault = Integer.parseInt(epByTextField.getText());

        if (traceKey.equals("cdp")) {
            cdpFromTextField.setText(headerLimits.get(1).get(1));
            cdpToTextField.setText(headerLimits.get(1).get(2));
            cdpByTextField.setText(headerLimits.get(1).get(3));
            defaultCdpFrom = Integer.valueOf(headerLimits.get(1).get(1));
            defaultCdpTo = Integer.valueOf(headerLimits.get(1).get(2));
            defaultCdpBy = Integer.valueOf(headerLimits.get(1).get(3));
        } else {
            cdpFromTextField.setText(headerLimits.get(0).get(1));
            cdpToTextField.setText(headerLimits.get(0).get(2));
            cdpByTextField.setText(headerLimits.get(0).get(3));
            defaultCdpFrom = Integer.valueOf(headerLimits.get(1).get(1));
            defaultCdpTo = Integer.valueOf(headerLimits.get(1).get(2));
            defaultCdpBy = Integer.valueOf(headerLimits.get(1).get(3));
        }
        cdpIncrDefault = Integer.parseInt(cdpByTextField.getText());
    }

    /** Generate the script and send it to the file manager to save.
     *
     * @param file /path/script.sh
     * @return true if file written successfully; otherwise, false.
     */
    private  boolean saveParamsToScript(String file) {
        if(bhpsuInRadioButton.isSelected()){
            String lineKey = headerLimits.get(0).get(0);
            String traceKey = headerLimits.get(1).get(0);

            // Check keys are in the correct order
            if (lineKey.equals("cdp") && traceKey.equals("ep")) {
                //warning
                JOptionPane.showMessageDialog(this,
                    "line (ep, bytes 17-20), trace (cdp, bytes 21-24) flipped.",
                    "Line,Trace Order Warning",
                    JOptionPane.WARNING_MESSAGE);
            } else
            if (!lineKey.equals("ep") || !traceKey.equals("cdp")) {
                //error
                JOptionPane.showMessageDialog(this,
                    "line should be ep (bytes 17-20), trace should be cdp (bytes 21-24)",
                    "Line,Trace Error",
                    JOptionPane.WARNING_MESSAGE);
                return false;
            }
        }
        //Get the most recent project descriptor
        qiProjectDesc = agent.getQiProjectDescriptor();

        ArrayList<String> script = new ArrayList<String>();
        String command = "";

        // source ~/.bashrc
        // make sure the binary of SU and BHP-SU on PATH
        command = "source ~/.bashrc";
        script.add(command);

        // alias rm
        // silently remove files so script can run standalone
        command = "alias rm=\"rm -f\"";
        script.add(command);

        // alias cp
        command = "alias cp=cp";
        script.add(command);
        script.add("#set -x");
        script.add("");
        script.add("# Parameters specified in the qiProject");

        // export QISPACE
        String qiSpace = QiProjectDescUtils.getQiSpace(qiProjectDesc);
        script.add("# qiSpace containing one or more projects");
        command = "export QISPACE=\"" + qiSpace + "\"";
        script.add(command);

        // export QIPROJECT
        String qiProject = QiProjectDescUtils.getQiProjectReloc(qiProjectDesc);
        script.add("# location of qiProject relative to ${QISPACE}");
        command = "export QIPROJECT=\"" + qiProject + "\"";
        script.add(command);

        // export QIDATASETS
        String qiDatasets = QiProjectDescUtils.getDatasetsReloc(qiProjectDesc);
        script.add("# location of datasets relative to ${QISPACE}/${QIPROJECT}");
        command = "export QIDATASETS=\"" + qiDatasets + "\"";
        script.add(command);

        // export QIHORIZONS
        String qiHorizons = QiProjectDescUtils.getHorizonsReloc(qiProjectDesc);
        script.add("# location of horizons relative to ${QISPACE}/${QIPROJECT}");
        command = "export QIHORIZONS=\"" + qiHorizons + "\"";
        script.add(command);

        // export QISEISMICS
        ArrayList<String> seismicDirs = QiProjectDescUtils.getSeismicDirs(qiProjectDesc);
        StringBuffer qiSeismics = new StringBuffer();
		StringBuffer format = new StringBuffer();
        //Note: there is always at least 1 seismic directory
        //int lastIndex = seismicDirs.size()-1;
        //for (int i=0; i<=lastIndex; i++) {
        //   qiSeismics += seismicDirs.get(i);
        //    if (i != lastIndex) qiSeismics += "\n";
        //}
        String projectPath = QiProjectDescUtils.getProjectPath(qiProjectDesc);
        for(int i = 0; seismicDirs != null && i < seismicDirs.size(); i++){
            qiSeismics.append(projectPath + filesep + seismicDirs.get(i));
			format.append("%s");
            if (i < seismicDirs.size() -1){
                qiSeismics.append(" ");
				format.append("\n");
            }
        }
        script.add("# list of seismic directories separated by a blank");
        command = "export QISEISMICS=\"" +  qiSeismics.toString() + "\"";
		script.add(command);
		command = "export FORMAT=\"" +  format.toString() + "\"";
        script.add(command);

        script.add("");
        script.add("# Parameters specified in the AmpExt GUI");
        script.add("# path of input dataset file relative to ${QIDATASETS}; file name has no suffix for .dat is assumed");
        String inDataset = inputCubeTextField.getText();
        command = "export IN_DATASET=\"" + inDataset + "\"";
        script.add(command);
        script.add("# path of output dataset file relative to ${QIDATASETS}; file name has no suffix for .dat is assumed");
        String outDataset = "";

        if(bhpsuOutputRadioButton.isSelected()){
            outDataset = outputCubeTextField.getText();
        }else if(landmarkOutputRadioButton.isSelected()){
            if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                outDataset = landmark2DOutputCubeTextField.getText();
            }else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                outDataset = landmark3DOutputCubeTextField.getText();
            }else if(bhpsuInRadioButton.isSelected()){
                outDataset = landmarkOutputCubeTextField.getText();
            }
        }
        command = "export OUT_DATASET=\"" + outDataset + "\"";
        script.add(command);
        //check if edge mode (2 horizons) or point mode (1 horizon)
        if(bhpsuHorizonRadioButton.isSelected()){
            if (edgeModeRadioButton.isSelected()) {    // edge mode
                script.add("# path of first selected horizon file relative to ${QIHORIZONS}; file name has no suffix for .xyz is assumed");
                String horizon = bhpsuTopHorizonTextField.getText().trim();
                int ind = horizon.lastIndexOf(".xyz");
                if(ind != -1){
                    horizon = horizon.substring(0, ind);
                }
                String [] elements = horizon.split(":");
                if(elements.length == 1)
                    horizon = elements[0];
                else if(elements.length == 2)
                    horizon = elements[1];
                else{
                    JOptionPane.showMessageDialog(this, "Top Horizon field value is not in correct format for further processing.", "Invalid Data Entry",    JOptionPane.WARNING_MESSAGE);
                    bhpsuTopHorizonTextField.requestFocus();
                    return false;
                }
                command = "export IN_HORIZON1=\"" + horizon + "\"";
                script.add(command);
                script.add("# path of second selected horizon file (if any) relative to ${QIHORIZONS}; file name has no suffix for .xyz is assumed");
                horizon = bhpsuBottomHorizonTextField.getText().trim();
                ind = horizon.lastIndexOf(".xyz");
                if(ind != -1){
                    horizon = horizon.substring(0, ind);
                }
                elements = horizon.split(":");
                if(elements.length == 1)
                    horizon = elements[0];
                else if(elements.length == 2)
                    horizon = elements[1];
                else{
                    JOptionPane.showMessageDialog(this, "Bottom Horizon field value is not in correct format for further processing.", "Invalid Data Entry",    JOptionPane.WARNING_MESSAGE);
                    bhpsuBottomHorizonTextField.requestFocus();
                    return false;
                }
                command = "export IN_HORIZON2=\"" + horizon + "\"";
                script.add(command);
            } else {    // point mode
                script.add("# path of selected horizon file relative to ${QIHORIZONS}; file name has no suffix for .xyz is assumed");
                String horizon = bhpsuHorizonTextField.getText().trim();
                int ind = horizon.lastIndexOf(".xyz");
                if (ind != -1){
                    horizon = horizon.substring(0, ind);
                }

                String [] elements = horizon.split(":");
                if (elements.length == 1)
                    horizon = elements[0];
                else if(elements.length == 2)
                    horizon = elements[1];
                else{
                    JOptionPane.showMessageDialog(this, "The Horizon field value is not in correct format for further processing.", "Invalid Data Entry",    JOptionPane.WARNING_MESSAGE);
                    bhpsuHorizonTextField.requestFocus();
                    return false;
                }
                command = "export IN_HORIZON=\"" + horizon + "\"";
                script.add(command);
            }
        } else if(bhpsuHorizonDatasetRadioButton.isSelected()){
            if (edgeModeRadioButton.isSelected()) {    // edge mode
                script.add("# path of first selected horizon file relative to ${QIHORIZONS}; file name has no suffix for .xyz is assumed");
                String horizon = bhpsuTopHorizonTextField.getText().trim();
                String [] elements = horizon.split(":");
                if(elements == null || elements.length != 2){
                    JOptionPane.showMessageDialog(this, "Top Horizon field value is not in correct format (dataset:horizon) for further processing.", "Invalid Data Entry",    JOptionPane.WARNING_MESSAGE);
                    bhpsuTopHorizonTextField.requestFocus();
                    return false;
                }
                int ind = elements[1].lastIndexOf(".xyz");
                if(ind != -1){
                    horizon = elements[1].substring(0, ind);
                }else
                    horizon = elements[1];

                command = "export IN_HORIZON1=\"" + horizon + "\"";
                script.add(command);
                command = "export IN_HORIZON_DATASET1=\"" + elements[0] + "\"";
                script.add(command);
                script.add("# path of second selected horizon file (if any) relative to ${QIHORIZONS}; file name has no suffix for .xyz is assumed");
                horizon = bhpsuBottomHorizonTextField.getText().trim();
                elements = horizon.split(":");
                if(elements == null || elements.length != 2){
                    JOptionPane.showMessageDialog(this, "Bottom Horizon field value is not in correct format (dataset:horizon) for further processing.", "Invalid Data Entry",    JOptionPane.WARNING_MESSAGE);
                    bhpsuBottomHorizonTextField.requestFocus();
                    return false;
                }
                ind = elements[1].lastIndexOf(".xyz");
                if(ind != -1){
                    horizon = horizon.substring(0, ind);
                } else
                    horizon = elements[1];
                command = "export IN_HORIZON2=\"" + horizon + "\"";
                script.add(command);
                command = "export IN_HORIZON_DATASET2=\"" + elements[0] + "\"";
                script.add(command);
            } else {    // point mode
                script.add("# path of selected horizon file relative to ${QIHORIZONS}; file name has no suffix for .xyz is assumed");
                String horizon = bhpsuHorizonTextField.getText().trim();
                String [] elements = horizon.split(":");
                if(elements == null || elements.length != 2){
                    JOptionPane.showMessageDialog(this, "Top Horizon field value is not in correct format (dataset:horizon) for further processing.", "Invalid Data Entry",    JOptionPane.WARNING_MESSAGE);
                    bhpsuTopHorizonTextField.requestFocus();
                    return false;
                }
                int ind = elements[1].lastIndexOf(".xyz");
                if(ind != -1){
                    horizon = horizon.substring(0, ind);
                }else
                    horizon = elements[1];
                command = "export IN_HORIZON=\"" + horizon + "\"";
                script.add(command);
                command = "export IN_HORIZON_DATASET=\"" + elements[0] + "\"";
                script.add(command);
            }
        } else if(landmarkHorizonRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
            if (edgeModeRadioButton.isSelected()) {    // edge mode
                script.add("# path of first selected horizon file relative to ${QIHORIZONS}; file name has no suffix for .xyz is assumed");
                command = "export IN_HORIZON1=\"" + landmark3DTopHorizonTextField.getText() + "\"";
                script.add(command);
                script.add("# path of second selected horizon file (if any) relative to ${QIHORIZONS}; file name has no suffix for .xyz is assumed");
                command = "export IN_HORIZON2=\"" + landmark3DBottomHorizonTextField.getText() + "\"";
                script.add(command);
            } else {    // point mode
                script.add("# path of selected horizon file relative to ${QIHORIZONS}; file name has no suffix for .xyz is assumed");
                command = "export IN_HORIZON=\"" + landmark3DHorizonTextField.getText() + "\"";
                script.add(command);
            }

        } else if(landmarkHorizonRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
            if (edgeModeRadioButton.isSelected()) {    // edge mode
                script.add("# path of first selected horizon file relative to ${QIHORIZONS}; file name has no suffix for .xyz is assumed");
                command = "export IN_HORIZON1=\"" + landmark2DTopHorizonTextField.getText() + "\"";
                script.add(command);
                script.add("# path of second selected horizon file (if any) relative to ${QIHORIZONS}; file name has no suffix for .xyz is assumed");
                command = "export IN_HORIZON2=\"" + landmark2DBottomHorizonTextField.getText() + "\"";
                script.add(command);
            } else {    // point mode
                script.add("# path of selected horizon file relative to ${QIHORIZONS}; file name has no suffix for .xyz is assumed");
                command = "export IN_HORIZON=\"" + landmark2DHorizonTextField.getText() + "\"";
                script.add(command);
            }
        }
        script.add("# path of output horizons relative to ${QIHORIZONS}; file name is prefixed to generated horizons");
        String outHorizonset = "";
        if(bhpsuOutputRadioButton.isSelected()){
            outHorizonset = outputHorizonsTextField.getText();
        }else if(landmarkOutputRadioButton.isSelected()){
            if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                outHorizonset = landmark2DOutputHorizonsTextField.getText();
            }else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                outHorizonset = landmark3DOutputHorizonsTextField.getText();
            }else if(bhpsuInRadioButton.isSelected()){
                outHorizonset = landmarkOutputHorizonsTextField.getText();
            }
        }
        command = "export OUT_HORIZON=\"" + outHorizonset + "\"";
        script.add(command);

        script.add("");
        String filename = "";
        int idx;
        if(bhpsuInRadioButton.isSelected()){
            idx = inDataset.indexOf(filesep);
            filename = (idx == -1) ? inDataset : inDataset.substring(inDataset.lastIndexOf(filesep)+1);
            command = "export IN_DATASET_FILENAME=\"" + filename + "\"";
            script.add(command);
        }
        idx = outDataset.indexOf(filesep);
        filename = (idx == -1) ? outDataset : outDataset.substring(outDataset.lastIndexOf(filesep)+1);
        command = "export OUT_DATASET_FILENAME=\"" + filename + "\"";
        script.add(command);
        script.add("");
        
        idx = outHorizonset.indexOf(filesep);
        filename = (idx == -1) ? outHorizonset : outHorizonset.substring(outDataset.lastIndexOf(filesep)+1);
        command = "export OUT_HORIZON_BASENAME=\"" + filename + "\"";
        script.add(command);
        script.add("");
        
        if(bhpsuHorizonDatasetRadioButton.isSelected()){
            script.add("echo -e \"\\nGenerating ASCII horizon(s).\\n\"");
            if (edgeModeRadioButton.isSelected()) {    // edge mode
                command = "bhpread keys=ep,cdp filename=${IN_HORIZON_DATASET1} pathlist=${QISPACE}/${QIPROJECT}/${QIDATASETS}/${IN_HORIZON_DATASET1}.dat \\\n";
                command += " horizons=${IN_HORIZON1} | \\\n";
                command += " bhpstorehdr keys=ep,cdp horizons=yes file=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${IN_HORIZON1}.xyz | \\\n";
                command += " surange";
                script.add(command);
                if(!bhpsuBottomHorizonTextField.getText().trim().equals(bhpsuTopHorizonTextField.getText().trim())){
                    command = "bhpread keys=ep,cdp filename=${IN_HORIZON_DATASET2} pathlist=${QISPACE}/${QIPROJECT}/${QIDATASETS}/${IN_HORIZON_DATASET2}.dat \\\n";
                    command += " horizons=${IN_HORIZON2} | \\\n";
                    command += " bhpstorehdr keys=ep,cdp horizons=yes file=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${IN_HORIZON2}.xyz | \\\n";
                    command += " surange";
                    script.add(command);
                }
            }else if(pointModeRadioButton.isSelected()){ // point mode
                command = "bhpread keys=ep,cdp filename=${IN_HORIZON_DATASET} pathlist=${QISPACE}/${QIPROJECT}/${QIDATASETS}/${IN_HORIZON_DATASET}.dat \\\n";
                command += " horizons=${IN_HORIZON} | \\\n";
                command += " bhpstorehdr keys=ep,cdp horizons=yes file=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${IN_HORIZON}.xyz | \\\n";
                command += " surange";
                script.add(command);
            }
        }

        float phase = processPanel.getRotationPhase();
        String runSumCommand = "";
        String extendedRunSumFileName = "";
        if(phase != -1){
            float phasefac = phase % 360;
            if(phasefac < 0)
                phasefac += 360;
            phasefac = phasefac/180 * (-1);
            runSumCommand += "\nsufrac phasefac=" + phasefac + " | \\";
            extendedRunSumFileName += "_rotate";
        }
        float width = processPanel.getRunsumWidth();
        if(width != -1){
            runSumCommand += "\nsuintegrate | suzeromeangate width=" + width + " | \\";
            extendedRunSumFileName += "_runsum";
        }

        String extendedOutputFileName = "";


        if(bhpsuInRadioButton.isSelected() || (landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected())){
            // create .dat files
            if(!processPanel.isNotRunAmpExtChecked()){
			script.add("echo -e \"\\nCreate \" ${OUT_DATASET_FILENAME}.dat \"file\\n\"");
            command = "echo ${QISEISMICS} > ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}.dat";
//			command = "touch ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}.dat";
			script.add(command);
			//command = "printf ${FORMAT} ${QISEISMICS} > ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}.dat";
            //script.add(command);
			script.add("echo -e \"\\nCreate \" ${OUT_DATASET_FILENAME}_transpose_ep.dat \"file\\n\"");
            command = "echo ${QISEISMICS} > ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}_transpose_ep.dat";
//			command = "touch ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}_transpose_ep.dat";
			script.add(command);
			//command = "printf ${FORMAT} ${QISEISMICS} > ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}_transpose_ep.dat";
            //script.add(command);
            script.add("");
            }
            String prefix = "";
            if(processPanel.isExportBhpsuChecked() || processPanel.isNotRunAmpExtChecked()){
                if(processPanel.getExportSeismicPrefix().length() > 0)
                    if(processPanel.getExportSeismicPrefix().trim().length() > 0)
                        prefix = processPanel.getExportSeismicPrefix() + "_";
                if(extendedRunSumFileName.length() > 0){
                    if(bhpsuInRadioButton.isSelected())
                        extendedOutputFileName = prefix + "${IN_DATASET_FILENAME}" + extendedRunSumFileName;
                    else if (landmark3DRadioButton.isSelected()){
                        String volumeBaseName = volumeTextField.getText().trim().substring(0, volumeTextField.getText().trim().lastIndexOf("."));
                        extendedOutputFileName = prefix + volumeBaseName  + extendedRunSumFileName;
                    }
                }else{
                    if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                        String volumeBaseName = volumeTextField.getText().trim().substring(0, volumeTextField.getText().trim().lastIndexOf("."));
                        extendedOutputFileName = prefix + volumeBaseName;
                    }else
                        extendedOutputFileName = "";
                }
                if(extendedOutputFileName.length() > 0){
                    // create .dat files
                    command = "echo ${QISEISMICS} > ${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName + ".dat";
					//command = "printf ${FORMAT} ${QISEISMICS} > ${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName + ".dat";
                    script.add(command);
                    script.add("");

                    script.add("echo -e \"\\nRemoving existing output files if they exists.\\n\"");
                    // bhpio delete output if exists
                    command = "bhpio filename=";
                    command += extendedOutputFileName;
                    command += " pathlist=";
                    command += "${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName;
                    command += ".dat";
                    command += " delete=yes";
                    script.add(command);
                    script.add("");
                }
            }

            if(!processPanel.isNotRunAmpExtChecked()){
                // bhpio delete output if exists
				script.add("echo Remove seismic files for ${OUT_DATASET_FILENAME}");
                command = "bhpio filename=";
                command += "${OUT_DATASET_FILENAME}";
                command += " pathlist=";
                command += "${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}";
                command += ".dat";
                command += " delete=yes";
                script.add(command);

                // bhpio delete output if exists
				script.add("echo Remove seismic files for ${OUT_DATASET_FILENAME}_transpose_ep");
                command = "bhpio filename=";
                command += "${OUT_DATASET_FILENAME}";
                command += "_transpose_";
                command += "ep";
                command += " pathlist=";
                command += "${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}";
                command += "_transpose_";
                command += "ep";
                command += ".dat";
                command += " delete=yes";
                script.add(command);
                script.add("");
            }
        }

        int epFrom=0,epTo=0,epIncr=0,epLength=0,cdpFrom=0,cdpTo=0,cdpIncr=0,cdpLength=0;
        if(landmarkInRadioButton.isSelected()){
            if(landmark3DRadioButton.isSelected()){
                int from = Integer.valueOf(linesTextField.getText().trim());
                int to = Integer.valueOf(linesToTextField.getText().trim());
                int by = Integer.valueOf(linesByTextField.getText().trim());
                if(by < 0)
                    by = by * (-1);
                if(from > to){
                    epFrom = to;
                    epTo = from;
                }else{
                    epFrom = from;
                    epTo = to;
                }
                epIncr = by;
                epLength = ((epTo - epFrom + epIncr) / epIncr);
            }else if(landmark2DRadioButton.isSelected()){
            }
        }else if(bhpsuInRadioButton.isSelected()){
            epFrom = Integer.parseInt(epFromTextField.getText());
            epTo = Integer.parseInt(epToTextField.getText());
            epIncr = Integer.parseInt(epByTextField.getText());
            epLength = ((epTo - epFrom + epIncr) / epIncr);
        }

        if(landmarkInRadioButton.isSelected()){
            if(landmark3DRadioButton.isSelected()){
                int from = Integer.valueOf(tracesTextField.getText().trim());
                int to = Integer.valueOf(tracesToTextField.getText().trim());
                int by = Integer.valueOf(tracesByTextField.getText().trim());
                if(by < 0)
                    by = by * (-1);
                if(from > to){
                    cdpFrom = to;
                    cdpTo = from;
                }else{
                    cdpFrom = from;
                    cdpTo = to;
                }
                cdpIncr = by;
                cdpLength = ((cdpTo - cdpFrom + cdpIncr) / cdpIncr);
            }
        }else if(bhpsuInRadioButton.isSelected()){
            cdpFrom = Integer.parseInt(cdpFromTextField.getText());
            cdpTo = Integer.parseInt(cdpToTextField.getText());
            cdpIncr = Integer.parseInt(cdpByTextField.getText());
            cdpLength = ((cdpTo - cdpFrom + cdpIncr) / cdpIncr);
        }

        //landmark horizon export if any
        if(landmarkHorizonRadioButton.isSelected() && landmark3DRadioButton.isSelected() && !processPanel.isNotRunAmpExtChecked()){
            script.add("#Exporting the Landmark horizon(s) to .xyz format");
            command = "lmexphor lm_project=" + landmarkInProjectTextField.getText().trim();
            command += " lm_ptype=3";
            if(edgeModeRadioButton.isSelected()){
                if(!landmark3DTopHorizonTextField.getText().trim().equals(landmark3DBottomHorizonTextField.getText().trim()))
                    command += " lm_horizons=" + landmark3DTopHorizonTextField.getText().trim() + "," + landmark3DBottomHorizonTextField.getText().trim();
                else
                    command += " lm_horizons=" + landmark3DTopHorizonTextField.getText().trim();
            }else if(pointModeRadioButton.isSelected()){
                command += " lm_horizons=" + landmark3DHorizonTextField.getText().trim();
            }
            command += " minline=" + linesTextField.getText().trim();
            command += " maxline=" + linesToTextField.getText().trim();
            command += " mintrace=" + tracesTextField.getText().trim();
            command += " maxtrace=" + tracesToTextField.getText().trim();
            command += " dir=${QISPACE}/${QIPROJECT}/${QIHORIZONS}";
            script.add(command);
            ///script.add("");
            genCheckReturnCodes(script,"lmexphor","");
        }
        if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
        	genScriptExportingLandmark3DToBhpsu(script);
        	//if(!processPanel.isExportBhpsuChecked()){
        	//	script.add("old_ow_pmpath=$OW_PMPATH");
            //	script.add("export OW_PMPATH=$OW_PMPATH_DM");
            //	script.add("echo \"Before Landmark access OW_PMPATH=$OW_PMPATH\"");
        	//}
            command = "lmread lm_project=" + landmarkInProjectTextField.getText().trim();
            command += " lm_ptype=3";
            command += " lm_brickname=" + volumeTextField.getText().trim();
            command += " iline=";
            command += linesTextField.getText().trim() + ":" + linesToTextField.getText().trim() + ":" + linesByTextField.getText().trim();
            command += " xline=";
            command += tracesTextField.getText().trim() + ":" + tracesToTextField.getText().trim() + ":" + tracesByTextField.getText().trim();
            command += " | \\";

            if(runSumCommand.length() > 0)
                command += runSumCommand;
            if(extendedOutputFileName.length() > 0){
                if(!processPanel.isNotRunAmpExtChecked() || processPanel.isExportBhpsuChecked()){
                command += "\nbhpwritecube init=yes filename=" + extendedOutputFileName + " \\\n";
                command += " pathlist=${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName + ".dat \\\n";
                command += " key1=ep," + epFrom + "," + epIncr + "," + epLength + " key2=cdp," + cdpFrom + "," + cdpIncr + "," + cdpLength;
                script.add(command);
                ///script.add("");
                genCheckReturnCodes(script,"lmread ~ bhpwritecube","");
                }else{
                    script.add(command);
                }
                if(processPanel.isExportToLandmarkChecked()){
                    if(!processPanel.isNotRunAmpExtChecked() || processPanel.isExportBhpsuChecked()){
                	script.add("old_ow_pmpath=$OW_PMPATH");
                	script.add("export OW_PMPATH=$OW_PMPATH_DM");
                	script.add("echo \"Before Landmark access OW_PMPATH=$OW_PMPATH\"");                    	
                    script.add("#Writing seismic data to the Landmark application.");
                    command = "bhpreadcube filename=";
                    command += extendedOutputFileName;
                    command += " keys=";
                    command += "ep";
                    command += ",";
                    command += "cdp";
                    command += " pathlist=";
                    command += "${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName;
                    command += ".dat";
                    command += " keylist="+epFrom+"-"+epTo+"["+epIncr+"]:"+cdpFrom+"-"+cdpTo+"["+cdpIncr+"]";
                    command += " | \\";
                    script.add(command);
                    }
                    if(landmarkAgent == null){
                        landmarkAgent = LandmarkServices.getInstance();
                    }
                    int ptype = landmarkAgent.getLandmarkProjectType(processPanel.getLandmarkProjectName());
                    command = "lmwrite lm_project=" + processPanel.getLandmarkProjectName() + " lm_ptype=" + ptype + " lm_brickname=" + processPanel.getVolumeName() + ".bri";
                    script.add(command);
                    script.add("export OW_PMPATH=$old_ow_pmpath");
                    script.add("echo \"After Landmark access OW_PMPATH=$OW_PMPATH\"");
                    ///script.add("");
                    genCheckReturnCodes(script,"bhpreadcube | lmwrite","");
                }
                command = "bhpreadcube filename=";
                command += extendedOutputFileName;
                command += " keys=";
                command += "ep";
                command += ",";
                command += "cdp";
                command += " pathlist=";
                command += "${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName;
                command += ".dat";
                command += " keylist="+epFrom+"-"+epTo+"["+epIncr+"]:"+cdpFrom+"-"+cdpTo+"["+cdpIncr+"]";
                command += " | \\";
            }

        }else if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
            //JOptionPane.showMessageDialog(this,
            //        "Landmark 2D is not yet currently supported.",
            //        "Unsupported Error",
            //        JOptionPane.WARNING_MESSAGE);
            //    return false;
            script.add("#Reading seismic data from the 2D line(s).");

            convertShotpointsToTraces();
            Object[] keys = selectedLineMap.keySet().toArray();
            String lineNameList = "";
            String shotpoints = "";
            String traces = "";

            for(int i = 0; keys!= null && i < keys.length; i++){
            	if(processPanel.isNotRunAmpExtChecked() || related2DLines.contains(keys[i])){
	                lineNameList += keys[i];
	                LineInfo li = selectedLineMap.get((String)keys[i]);
	                shotpoints += li.lineName + ":" + li.lineNumber + ":" + li.minShotPoint + ":" + li.maxShotPoint + ":" + li.incShotPoint;
	                traces += li.lineName + ":" + li.lineNumber + ":" + li.minTrace + ":" + li.maxTrace + ":" + li.incTrace;
	                if(i < (keys.length - 1)){
	                    lineNameList += ",";
	                    shotpoints += ",";
	                    traces += ",";
	                }
            	}
            }

            String horizons = "";
            if(edgeModeRadioButton.isSelected()){
                if(landmark2DTopHorizonTextField.getText().trim().
                        equals(landmark2DBottomHorizonTextField.getText().trim()))
                    horizons=landmark2DTopHorizonTextField.getText().trim();
                else
                    horizons=landmark2DTopHorizonTextField.getText().trim() + ","
                         + landmark2DBottomHorizonTextField.getText().trim();
            }else if(pointModeRadioButton.isSelected()){
                horizons=landmark2DHorizonTextField.getText().trim();
            }
            script.add("#line_name_list=l_name1,l_name2,l_name3,...");
            script.add("linename_list=" + lineNameList);
            script.add("#line_shotpoint_list=l_name1:l_num1:min_spn1:max_spn1:inc_spn1,l_name2:l_num2:min_spn2:max_spn2:inc_spn2,...");
            script.add("line_shotpoint_list=" + shotpoints);
            script.add("#line_trace_list=l_name1:l_num1:min_tr1:max_tr1:inc_tr1,l_name2:l_num2:min_tr2:max_tr2:inc_tr2,....");
            script.add("line_trace_list=" + traces);
            if(landmarkHorizonRadioButton.isSelected())
                script.add("horizon_list=" + horizons);

            script.add("ii=0");
            script.add("IFS=,");
            script.add("for i in ${line_shotpoint_list}");
            script.add("do");
            script.add("    line_shotpoint_array[${ii}]=$i");
            script.add("    ii=`expr ${ii} + 1`");
            script.add("done");
            script.add("");
            script.add("ii=0");
            script.add("for ((  jj = 0 ;  jj < ${#line_shotpoint_array[*]};  jj++  ))");
            script.add("do");
            script.add("    IFS=:");
            script.add("    for k in ${line_shotpoint_array[${jj}]}");
            script.add("    do");
            script.add("        line_shotpoint_detail[${ii}]=$k");
            script.add("        ii=`expr ${ii} + 1`");
            script.add("    done");
            script.add("done");
            script.add("");
            script.add("ii=0");
            script.add("IFS=,");
            script.add("for i in ${line_trace_list}");
            script.add("do");
            script.add("    line_trace_array[${ii}]=$i");
            script.add("    ii=`expr ${ii} + 1`");
            script.add("done");
            script.add("");
            script.add("ii=0");
            script.add("for ((  jj = 0 ;  jj < ${#line_trace_array[*]};  jj++  ))");
            script.add("do");
            script.add("    IFS=:");
            script.add("    for k in ${line_trace_array[${jj}]}");
            script.add("    do");
            script.add("        line_trace_detail[${ii}]=$k");
            script.add("        ii=`expr ${ii} + 1`");
            script.add("    done");
            script.add("done");
            script.add("");
            script.add("ii=0");
            script.add("kkk=0");
            script.add("for ((  jj = 0 ;  jj < ${#line_shotpoint_detail[*]};  jj++  ))");
            script.add("do");
            script.add("    kk=`expr ${jj} % 5`");
            script.add("    if [ ${kk} = 0 ]");
            script.add("    then");
            script.add("        linenum=`expr ${jj} + 1`");
            script.add("        mins=`expr ${jj} + 2`");
            script.add("        maxs=`expr ${jj} + 3`");
            script.add("        inc=`expr ${jj} + 4`");
            script.add("        line_name_array[${ii}]=${line_shotpoint_detail[${jj}]}");
            script.add("        ii=`expr ${ii} + 1`");
            script.add("        echo line name=${line_shotpoint_detail[${jj}]} linenum=${line_shotpoint_detail[${linenum}]}\\");
            script.add("           min_shotpoint=${line_shotpoint_detail[${mins}]} max_shotpoint=${line_shotpoint_detail[${maxs}]}\\");
            script.add("           increment=${line_shotpoint_detail[${inc}]}");
            if(!processPanel.isNotRunAmpExtChecked()){
                script.add("        echo  ${QISEISMICS} > ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}_${line_shotpoint_detail[${jj}]}.dat");
                script.add("        echo  ${QISEISMICS} > ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}_${line_shotpoint_detail[${jj}]}_transpose_ep.dat");
				//script.add("        printf ${FORMAT} ${QISEISMICS} > ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}_${line_shotpoint_detail[${jj}]}.dat");
                //script.add("        printf ${FORMAT} ${QISEISMICS} > ${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}_${line_shotpoint_detail[${jj}]}_transpose_ep.dat");	
            }

            float phase2 = processPanel.getRotationPhase();
            String runSumCommand2 = "";
            String extendedRunSumFileName2 = "";
            if(phase2 != -1){
                float phasefac = phase2 % 360;
                if(phasefac < 0)
                    phasefac += 360;
                phasefac = phasefac/180 * (-1);
                runSumCommand2 += "     sufrac phasefac=" + phasefac + " | \\";
                extendedRunSumFileName2 += "_rotate";
            }
            float width2 = processPanel.getRunsumWidth();
            if(width2 != -1){
                if(runSumCommand2.length() > 0)
                    runSumCommand2 += "\n";
                runSumCommand2 += "     suintegrate | suzeromeangate width=" + width + " | \\";
                extendedRunSumFileName2 += "_runsum";
            }

            if(processPanel.isExportBhpsuChecked()){
                String prefix2 = "";
                if(processPanel.getExportSeismicPrefix().length() > 0)
                    if(processPanel.getExportSeismicPrefix().trim().length() > 0)
                        prefix2 = processPanel.getExportSeismicPrefix() + "_";
                if(extendedRunSumFileName2.length() > 0){
                    extendedOutputFileName = prefix2 + "${line_shotpoint_detail[${jj}]}" + extendedRunSumFileName2;
                }else{
                    extendedOutputFileName = prefix2 + "${line_shotpoint_detail[${jj}]}";
                }
                if(extendedOutputFileName.length() > 0){
                    // create .dat files
                    command = "     echo ${QISEISMICS} > ${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName + ".dat";
					//command = "     printf ${FORMAT} ${QISEISMICS} > ${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName + ".dat";
                    script.add(command);
                    script.add("");

                    script.add("        echo -e \"\\nRemoving existing output files if they exists.\\n\"");
                    // bhpio delete output if exists
                    command = "     bhpio filename=";
                    command += extendedOutputFileName;
                    command += " pathlist=";
                    command += "${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName;
                    command += ".dat";
                    command += " delete=yes";
                    script.add(command);
                }
            }

            if(!processPanel.isNotRunAmpExtChecked()){
                script.add("        bhpio filename=${OUT_DATASET_FILENAME}_${line_shotpoint_detail[${jj}]}\\");
                script.add("         pathlist=${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}_${line_shotpoint_detail[${jj}]}.dat delete=yes");
                script.add("        bhpio filename=${OUT_DATASET_FILENAME}_${line_shotpoint_detail[${jj}]}_transpose_ep\\");
                script.add("         pathlist=${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}_${line_shotpoint_detail[${jj}]}_transpose_ep.dat delete=yes");

            if(landmarkHorizonRadioButton.isSelected()){
                script.add("        lmexp2dhor lm_project=" + landmarkIn2DProjectTextField.getText().trim() + " lm_linename=${line_shotpoint_detail[${jj}]} mintrace=${line_trace_detail[${mins}]} maxtrace=${line_trace_detail[${maxs}]} \\");
                script.add("            lm_horizons=${horizon_list} dir=${QISPACE}/${QIPROJECT}/${QIHORIZONS}");
                if(edgeModeRadioButton.isSelected()){
                    script.add("        IN_HORIZON1=${line_shotpoint_detail[${jj}]}_" + landmark2DTopHorizonTextField.getText().trim());
                    script.add("        IN_HORIZON2=${line_shotpoint_detail[${jj}]}_" + landmark2DBottomHorizonTextField.getText().trim());
                }else if(pointModeRadioButton.isSelected()){
                    script.add("        IN_HORIZON=${line_shotpoint_detail[${jj}]}_" + landmark2DHorizonTextField.getText().trim());
                }
                genCheckReturnCodes(script,"lmexp2dhor","       ");
            }
            }
            script.add("        if [ ${line_trace_detail[${mins}]} -gt ${line_trace_detail[${maxs}]} ]");
            script.add("        then");
            script.add("            mincdp=${line_trace_detail[${maxs}]}");
            script.add("            maxcdp=${line_trace_detail[${mins}]}");
            script.add("            if [ ${line_trace_detail[${inc}]} -lt 0 ]");
            script.add("            then");
            script.add("                inccdp=`expr ${line_trace_detail[${inc}]} \\* -1`");
            script.add("            fi");
            script.add("        else");
            script.add("            maxcdp=${line_trace_detail[${maxs}]}");
            script.add("            mincdp=${line_trace_detail[${mins}]}");
            script.add("            inccdp=${line_trace_detail[${inc}]}");
            script.add("        fi");
            script.add("        ncdp=`expr ${maxcdp} - ${mincdp}`");
            script.add("        ncdp=`expr ${ncdp} / ${inccdp}`");
            script.add("        ncdp=`expr ${ncdp} + 1`");
            script.add("        minep=${line_trace_detail[${linenum}]}");
            script.add("        incep=1");
            script.add("        nep=1");
            String project = landmarkIn2DProjectTextField.getText().trim();
            script.add("        #Reading seismic data from the Landmark application.");
            String process_Level = (String)seismic2DFileComboBox.getSelectedItem();
            
            if(!processPanel.isExportBhpsuChecked()){
        		script.add("old_ow_pmpath=$OW_PMPATH");
            	script.add("export OW_PMPATH=$OW_PMPATH_DM");
            	script.add("echo \"Before Landmark access OW_PMPATH=$OW_PMPATH\"");
        	}
            script.add("        lmread2d lm_project=" + project + " process_level=" + process_Level + "\\");
            script.add("           line_name=${line_shotpoint_detail[${jj}]}\\");
            script.add("           shotpoints=${line_shotpoint_detail[${mins}]}:${line_shotpoint_detail[${maxs}]}:${line_shotpoint_detail[${inc}]} | \\");
            command = "";


            if(runSumCommand2.length() > 0)
                command = runSumCommand2;

            //if(extendedOutputFileName.length() > 0){
                if((!processPanel.isNotRunAmpExtChecked() || processPanel.isExportBhpsuChecked()) && extendedOutputFileName.length() > 0){
                command += "\n      bhpwritecube init=yes filename=" + extendedOutputFileName + " \\\n";
                command += "          pathlist=${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName + ".dat \\\n";
                command += "          key1=ep,${minep},${incep},${nep} key2=cdp,${mincdp},${inccdp},${ncdp}";
                script.add(command);
                //script.add("");
                genCheckReturnCodesForLm2D(script,"lmread2d ~ bhpwritecube","       ","${line_shotpoint_detail[${jj}]}");
                }else{
                    if(command.length() > 0)
                    script.add(command);
                }
                if(processPanel.isExportToLandmarkChecked()){
                    if(!processPanel.isNotRunAmpExtChecked() || processPanel.isExportBhpsuChecked()){
                    	script.add("old_ow_pmpath=$OW_PMPATH");
                    	script.add("export OW_PMPATH=$OW_PMPATH_DM");
                    	script.add("echo \"Before Landmark access OW_PMPATH=$OW_PMPATH\"");
	                    script.add("        #Writing seismic data to the Landmark application.");
	                    command = "     bhpreadcube filename=";
	                    command += extendedOutputFileName;
	                    command += " keys=";
	                    command += "ep";
	                    command += ",";
	                    command += "cdp" + " \\\n";
	                    command += "         pathlist=";
	                    command += "${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName;
	                    command += ".dat" + " \\\n";
	                    command += "         keylist=${minep}-${minep}[${incep}]:${mincdp}-${maxcdp}[${inccdp}]";
	                    command += " | \\";
	                    script.add(command);
                    }
                    if(landmarkAgent == null){
                        landmarkAgent = LandmarkServices.getInstance();
                    }
                    int ptype = landmarkAgent.getLandmarkProjectType(processPanel.getLandmarkProjectName());
                    command = "     lmwrite2d lm_project=" + processPanel.getLandmarkProjectName() + " lm_ptype=" + ptype + " lm_plevel=" + processPanel.getVolumeName() + " lm_lines=${line_shotpoint_detail[${jj}]}";
                    script.add(command);
                    script.add("export OW_PMPATH=$old_ow_pmpath");
                    script.add("echo \"After Landmark access OW_PMPATH=$OW_PMPATH\"");
                    //script.add("");
                    if(processPanel.isNotRunAmpExtChecked() && !processPanel.isExportBhpsuChecked())
                        genCheckReturnCodesForLm2D(script,"lmread2d | lmwrite2d","      ","${line_shotpoint_detail[${jj}]}");
                    else
                        genCheckReturnCodesForLm2D(script,"bhpreadcube | lmwrite2d","       ","${line_shotpoint_detail[${jj}]}");
                    
                    if(!processPanel.isNotRunAmpExtChecked() && processPanel.isExportBhpsuChecked()){
                    	command = "     bhpreadcube filename=";
	                    command += extendedOutputFileName;
	                    command += " keys=";
	                    command += "ep";
	                    command += ",";
	                    command += "cdp" + " \\\n";
	                    command += "         pathlist=";
	                    command += "${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName;
	                    command += ".dat" + " \\\n";
	                    command += "         keylist=${minep}-${minep}[${incep}]:${mincdp}-${maxcdp}[${inccdp}]";
	                    command += " | \\";
	                    script.add(command);
                    }
                }else{
                	if(!processPanel.isNotRunAmpExtChecked() || processPanel.isExportBhpsuChecked()){
	                    //script.add("        #Writing seismic data to the Landmark application.");
	                    //command = "     bhpreadcube filename=";
	                    //command += extendedOutputFileName;
	                    //command += " keys=";
	                    //command += "ep";
	                    //command += ",";
	                    //command += "cdp" + " \\\n";
	                    //command += "         pathlist=";
	                    //command += "${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName;
	                    //command += ".dat" + " \\\n";
	                    //command += "         keylist=${minep}-${minep}[${incep}]:${mincdp}-${maxcdp}[${inccdp}]";
	                    //command += " | \\";
	                    //script.add(command);
                    }
                }
                /*
                command = "     bhpreadcube filename=";
                command += extendedOutputFileName + " \\\n";
                command += "         keys=";
                command += "ep";
                command += ",";
                command += "cdp" + " \\\n";
                command += "         pathlist=";
                command += "${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName;
                command += ".dat" + " \\\n";
                command += "         keylist=${minep}-${minep}[$incep]:${mincdp}-${maxcdp}[${inccdp}]";
                command += " | \\";
                */
            //}
            //if(command.length() > 0 && !processPanel.isNotRunAmpExtChecked())
            //    script.add(command);
            if(!processPanel.isNotRunAmpExtChecked()){
            if (edgeModeRadioButton.isSelected()) {    //edge mode
                // bhploadhdr for top horizon
                command = "     bhploadhdr keys=";
                command += "ep";
                command += ",";
                command += "cdp";
                if(!processPanel.isInterpolateHorizonChecked())
                    command += " hdrs=igc interp=no file=";
                else
                    command += " hdrs=igc interp=yes file=";
                command += "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${IN_HORIZON1}";
                command += ".xyz bias=";
                if(landmarkHorizonRadioButton.isSelected())
                    command += landmark2DTopHorizonBias;
                else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected())
                    command += topHorizonBias;

                command += " | \\";
                script.add(command);

                // bhploadhdr for bottom horizon
                command = "     bhploadhdr keys=";
                command += "ep";
                command += ",";
                command += "cdp";
                if(!processPanel.isInterpolateHorizonChecked())
                    command += " hdrs=igi interp=no file=";
                else
                    command += " hdrs=igi interp=yes file=";
                command += "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${IN_HORIZON2}";
                command += ".xyz bias=";
                if(landmarkHorizonRadioButton.isSelected())
                    command += landmark2DBottomHorizonBias;
                else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected())
                    command += bottomHorizonBias;

                command += " | \\";
                script.add(command);
            } else {    // point mode
                command = "     bhploadhdr keys=";
                command += "ep";
                command += ",";
                command += "cdp";
                if(!processPanel.isInterpolateHorizonChecked())
                    command += " hdrs=sut interp=no file=";
                else
                    command += " hdrs=sut interp=yes file=";

                command += "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${IN_HORIZON}";
                command += ".xyz bias=";
                if(landmarkHorizonRadioButton.isSelected()){
                    command += landmark2DHorizonBias;
                }else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
                    command += horizonBias;
                }
                command += " | \\";
                script.add(command);
            }
            //  bhpintegrate
            if (edgeModeRadioButton.isSelected())
                command = "     bhpintegrate mode=edge type=";
            else if(pointModeRadioButton.isSelected())
                command = "     bhpintegrate mode=point type=";
            if (troughTypeRadioButton.isSelected())
              command += "trough";
            else if (peakTypeRadioButton.isSelected())
              command += "peak";
            else if (absTypeRadioButton.isSelected())
              command += "abs";
            command += " search=";
            if (edgeModeRadioButton.isSelected()) {    // edge mode
                if(landmarkHorizonRadioButton.isSelected()){
                    command += landmark2DTopHorizonAbove;
                    command += ",";
                    command += landmark2DTopHorizonBelow;
                    command += ",";
                    command += landmark2DBottomHorizonAbove;
                    command += ",";
                    command += landmark2DBottomHorizonBelow;
                    command += " | \\";
                    script.add(command);
                }else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
                    command += topHorizonAbove;
                    command += ",";
                    command += topHorizonBelow;
                    command += ",";
                    command += bottomHorizonAbove;
                    command += ",";
                    command += bottomHorizonBelow;
                    command += " | \\";
                    script.add(command);
                }
            } else {    //point mode
                if(landmarkHorizonRadioButton.isSelected()){
                    command += landmark2DHorizonAbove;
                    command += ",";
                    command += landmark2DHorizonBelow;
                    command += " | \\";
                    script.add(command);
                }else if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
                    command += horizonAbove;
                    command += ",";
                    command += horizonBelow;
                    command += " | \\";
                    script.add(command);
                }
            }
            script.add("        bhpstorehdr keys=ep,cdp hdrs=d2 file=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_${line_shotpoint_detail[${jj}]}_area.xyz | \\");
            script.add("        bhpstorehdr keys=ep,cdp hdrs=f1 file=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_${line_shotpoint_detail[${jj}]}_width.xyz | \\");
            script.add("        bhpstorehdr keys=ep,cdp hdrs=f2 file=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_${line_shotpoint_detail[${jj}]}_amp.xyz | \\");
            script.add("        bhpstorehdr keys=ep,cdp hdrs=igc file=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_${line_shotpoint_detail[${jj}]}_p1.xyz | \\");
            script.add("        bhpstorehdr keys=ep,cdp hdrs=igi file=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_${line_shotpoint_detail[${jj}]}_p2.xyz | \\");
            script.add("        bhpstorehdr keys=ep,cdp hdrs=corr file=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_${line_shotpoint_detail[${jj}]}_p3.xyz | \\");
            script.add("        bhpcopyhdr hdrs=d2,f1,f2,igc,igi,corr samples=1,2,3,4,5,6 | sushw key=ns a=6 | \\");

            script.add("        bhpwritecube init=yes filename=${OUT_DATASET_FILENAME}_${line_shotpoint_detail[${jj}]}\\");
            script.add("         pathlist=${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}_${line_shotpoint_detail[${jj}]}.dat key1=ep,${minep},${incep},${nep} \\");
            script.add("         key2=cdp,${mincdp},${inccdp},${ncdp} horizons=area,width,amp,p1,p2,p3");
            genCheckReturnCodesForLm2D(script,"bhpreadcube | bhploadhdr | bhpintegrate | bhpstorehdr | bhpcopyhdr | bhpwritecube","     ","${line_shotpoint_detail[${jj}]}");

            script.add("        bhptranspose filename=${OUT_DATASET_FILENAME}_${line_shotpoint_detail[${jj}]} \\");
            script.add("         pathlist=${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}_${line_shotpoint_detail[${jj}]}.dat");
            genCheckReturnCodesForLm2D(script,"bhptranspose","      ","${line_shotpoint_detail[${jj}]}");
            if(landmarkOutputRadioButton.isSelected()){
                script.add("        #Importing horizon(s) to Landmark application.");
                project = landmark2DOutProjectTextField.getText().trim();
                script.add("        lmimp2dhor1 lm_project=" + project + " hname=${OUT_HORIZON_BASENAME}_area" + " horizon=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_${line_shotpoint_detail[${jj}]}_area.xyz");
                script.add("        lmimp2dhor1 lm_project=" + project + " hname=${OUT_HORIZON_BASENAME}_width" + " horizon=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_${line_shotpoint_detail[${jj}]}_width.xyz");
                script.add("        lmimp2dhor1 lm_project=" + project + " hname=${OUT_HORIZON_BASENAME}_amp" + " horizon=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_${line_shotpoint_detail[${jj}]}_amp.xyz");
                script.add("        lmimp2dhor1 lm_project=" + project + " hname=${OUT_HORIZON_BASENAME}_p1" + " horizon=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_${line_shotpoint_detail[${jj}]}_p1.xyz");
                script.add("        lmimp2dhor1 lm_project=" + project + " hname=${OUT_HORIZON_BASENAME}_p2" + " horizon=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_${line_shotpoint_detail[${jj}]}_p2.xyz");
                script.add("        lmimp2dhor1 lm_project=" + project + " hname=${OUT_HORIZON_BASENAME}_p3" + " horizon=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_${line_shotpoint_detail[${jj}]}_p3.xyz");
                genCheckReturnCodes(script,"lmimp2dhor1","      ");
            }
            }
            script.add("    fi");
            script.add("done");
            script.add("if [ ${#failed_line_array[@]} -gt 0 ]");
            script.add("then");
            script.add("    echo -e \"Job summary for Landmark 2D line(s).\"");
            script.add("    echo -e \"${#failed_line_array[@]} line(s) did not succeed in job execution:\"");

            script.add("    for (( jjjj = 0; jjjj < ${#failed_line_array[@]}; jjjj++ ))");
            script.add("    do");
            script.add("        echo ${failed_line_array[$jjjj]}");
            script.add("    done");
            script.add("    echo $0 job ends abnormally");
            script.add("    exit 1");
            script.add("fi");
        }else if(bhpsuInRadioButton.isSelected()){
            command = "bhpreadcube filename=";
            command += "${IN_DATASET_FILENAME}";
            command += " keys=";
            command += "ep";
            command += ",";
            command += "cdp";
            command += " pathlist=";
            command += "${QISPACE}/${QIPROJECT}/${QIDATASETS}/${IN_DATASET}";
            command += ".dat";
//    NOTE: Delay change until test
//            command += " missingdata=fill | \\";
            command += " keylist="+epFrom+"-"+epTo+"["+epIncr+"]:"+cdpFrom+"-"+cdpTo+"["+cdpIncr+"]";
            command += " | \\";

            if(runSumCommand.length() > 0)
                command += runSumCommand;
            if(extendedOutputFileName.length() > 0){
                if(!processPanel.isNotRunAmpExtChecked() || processPanel.isExportBhpsuChecked()){
                command += "\n bhpwritecube init=yes filename=" + extendedOutputFileName + " \\\n";
                command += " pathlist=${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName + ".dat \\\n";
                command += " key1=ep," + epFrom + "," + epIncr + "," + epLength + " key2=cdp," + cdpFrom + "," + cdpIncr + "," + cdpLength;
                script.add(command);
                //script.add("");
                genCheckReturnCodes(script,"bhpreadcube ~ bhpwritecube","");
                }else{
                    script.add(command);
                }
                if(processPanel.isExportToLandmarkChecked()){
                	script.add("old_ow_pmpath=$OW_PMPATH");
                	script.add("export OW_PMPATH=$OW_PMPATH_DM");
                	script.add("echo \"Before Landmark access OW_PMPATH=$OW_PMPATH\"");
                    if(!processPanel.isNotRunAmpExtChecked() || processPanel.isExportBhpsuChecked()){
                    script.add("#Writing seismic data to the Landmark application.");
                    command = "bhpreadcube filename=";
                    command += extendedOutputFileName;
                    command += " keys=";
                    command += "ep";
                    command += ",";
                    command += "cdp";
                    command += " pathlist=";
                    command += "${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName;
                    command += ".dat";
                    command += " keylist="+epFrom+"-"+epTo+"["+epIncr+"]:"+cdpFrom+"-"+cdpTo+"["+cdpIncr+"]";
                    command += " | \\";
                    script.add(command);
                    }
                    if(landmarkAgent == null){
                        landmarkAgent = LandmarkServices.getInstance();
                    }
                    int ptype = landmarkAgent.getLandmarkProjectType(processPanel.getLandmarkProjectName());
                    if(ptype == 3){
                        command = "lmwrite lm_project=" + processPanel.getLandmarkProjectName() + " lm_ptype=" + ptype + " lm_brickname=" + processPanel.getVolumeName() + ".bri";
                        script.add(command);
                        script.add("export OW_PMPATH=$old_ow_pmpath");
                        script.add("echo \"After Landmark access OW_PMPATH=$OW_PMPATH\"");
                        //script.add("");
                        if(processPanel.isNotRunAmpExtChecked() && !processPanel.isExportBhpsuChecked())
                            genCheckReturnCodes(script,"lmread | lmwrite","");
                        else
                            genCheckReturnCodes(script,"bhpreadcube | lmwrite","");
                    }else{
                        script.add("echo -e \"Export BHP-SU to Landmark 2D is not currently supported.\\n\"");
                    }
                }

                command = "bhpreadcube filename=";
                command += extendedOutputFileName;
                command += " keys=";
                command += "ep";
                command += ",";
                command += "cdp";
                command += " pathlist=";
                command += "${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + extendedOutputFileName;
                command += ".dat";
                command += " keylist="+epFrom+"-"+epTo+"["+epIncr+"]:"+cdpFrom+"-"+cdpTo+"["+cdpIncr+"]";
                command += " | \\";
            }
        }
        if(!processPanel.isNotRunAmpExtChecked()){
        if(bhpsuInRadioButton.isSelected() || (landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected())){
            script.add(command);
            if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
                if (edgeModeRadioButton.isSelected()) {    //edge mode
                    // bhploadhdr for top horizon
                    command = "bhploadhdr keys=";
                    command += "ep";
                    command += ",";
                    command += "cdp";
                    if(!processPanel.isInterpolateHorizonChecked())
                        command += " hdrs=igc interp=no file=";
                    else
                        command += " hdrs=igc interp=yes file=";

                    command += "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${IN_HORIZON1}";
                    command += ".xyz bias=";
                    command += topHorizonBias;
                    command += " | \\";
                    script.add(command);

                    // bhploadhdr for bottom horizon
                    command = "bhploadhdr keys=";
                    command += "ep";
                    command += ",";
                    command += "cdp";
                    if(!processPanel.isInterpolateHorizonChecked())
                        command += " hdrs=igi interp=no file=";
                    else
                        command += " hdrs=igi interp=yes file=";

                    command += "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${IN_HORIZON2}";
                    command += ".xyz bias=";
                    command += bottomHorizonBias;
                    command += " | \\";
                    script.add(command);
                } else {    // point mode
                    command = "bhploadhdr keys=";
                    command += "ep";
                    command += ",";
                    command += "cdp";
                    if(!processPanel.isInterpolateHorizonChecked())
                        command += " hdrs=sut interp=no file=";
                    else
                        command += " hdrs=sut interp=yes file=";

                    command += "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${IN_HORIZON}";
                    command += ".xyz bias=";
                    command += horizonBias;
                    command += " | \\";
                    script.add(command);
                }
            }else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                if (edgeModeRadioButton.isSelected()) {    //edge mode
                    // bhploadhdr for top horizon
                    command = "bhploadhdr keys=";
                    command += "ep";
                    command += ",";
                    command += "cdp";
                    if(!processPanel.isInterpolateHorizonChecked())
                        command += " hdrs=igc interp=no file=";
                    else
                        command += " hdrs=igc interp=yes file=";

                    command += "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${IN_HORIZON1}";
                    command += ".xyz bias=";
                    command += landmark3DTopHorizonBias;
                    command += " | \\";
                    script.add(command);

                    // bhploadhdr for bottom horizon
                    command = "bhploadhdr keys=";
                    command += "ep";
                    command += ",";
                    command += "cdp";
                    if(!processPanel.isInterpolateHorizonChecked())
                        command += " hdrs=igi interp=no file=";
                    else
                        command += " hdrs=igi interp=yes file=";

                    command += "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${IN_HORIZON2}";
                    command += ".xyz bias=";
                    command += landmark3DBottomHorizonBias;
                    command += " | \\";
                    script.add(command);
                } else {    // point mode
                    command = "bhploadhdr keys=";
                    command += "ep";
                    command += ",";
                    command += "cdp";
                    if(!processPanel.isInterpolateHorizonChecked())
                        command += " hdrs=sut interp=no file=";
                    else
                        command += " hdrs=sut interp=yes file=";
                    command += "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${IN_HORIZON}";
                    command += ".xyz bias=";
                    command += landmark3DHorizonBias;
                    command += " | \\";
                    script.add(command);
                }
            }
            //bhpintegrate
            if (edgeModeRadioButton.isSelected())
                command = "bhpintegrate mode=edge type=";
            else if(pointModeRadioButton.isSelected())
                command = "bhpintegrate mode=point type=";
            if (troughTypeRadioButton.isSelected())
              command += "trough";
            else if (peakTypeRadioButton.isSelected())
              command += "peak";
            else if (absTypeRadioButton.isSelected())
              command += "abs";
            command += " search=";
            if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
                if (edgeModeRadioButton.isSelected()) {    // edge mode
                    command += topHorizonAbove;
                    command += ",";
                    command += topHorizonBelow;
                    command += ",";
                    command += bottomHorizonAbove;
                    command += ",";
                    command += bottomHorizonBelow;
                    command += " | \\";
                    script.add(command);
                } else {    //point mode
                    command += horizonAbove;
                    command += ",";
                    command += horizonBelow;
                    command += " | \\";
                    script.add(command);
                }
            }else if(landmarkHorizonRadioButton.isSelected()){
                if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                    if (edgeModeRadioButton.isSelected()) {    // edge mode
                        command += landmark3DTopHorizonAbove;
                        command += ",";
                        command += landmark3DTopHorizonBelow;
                        command += ",";
                        command += landmark3DBottomHorizonAbove;
                        command += ",";
                        command += landmark3DBottomHorizonBelow;
                        command += " | \\";
                        script.add(command);
                    } else {    //point mode
                        command += landmark3DHorizonAbove;
                        command += ",";
                        command += landmark3DHorizonBelow;
                        command += " | \\";
                        script.add(command);
                    }
                }
            }

            //bhpstorehdr
            command = "bhpstorehdr keys=";
            command += "ep";
            command += ",";
            command += "cdp";
            command += " hdrs=d2 file=";
            command += "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_";
            command += "area.xyz | \\";
            script.add(command);

            //bhpstorehdr
            command = "bhpstorehdr keys=";
            command += "ep";
            command += ",";
            command += "cdp";
            command += " hdrs=f1 file=";
            command += "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_";
            command += "width.xyz | \\";
            script.add(command);

            //bhpstorehdr
            command = "bhpstorehdr keys=";
            command += "ep";
            command += ",";
            command += "cdp";
            command += " hdrs=f2 file=";
            command += "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_";
            command += "amp.xyz | \\";
            script.add(command);

            //bhpstorehdr
            command = "bhpstorehdr keys=";
            command += "ep";
            command += ",";
            command += "cdp";
            command += " hdrs=igc file=";
            command += "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_";
            command += "p1.xyz | \\";
            script.add(command);

            //bhpstorehdr
            command = "bhpstorehdr keys=";
            command += "ep";
            command += ",";
            command += "cdp";
            command += " hdrs=igi file=";
            command += "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_";
            command += "p2.xyz | \\";
            script.add(command);

            //bhpstorehdr
            command = "bhpstorehdr keys=";
            command += "ep";
            command += ",";
            command += "cdp";
            command += " hdrs=corr file=";
            command += "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_";
            command += "p3.xyz | \\";
            script.add(command);

            //bhpcopyhdr
            command = "bhpcopyhdr hdrs=d2,f1,f2,igc,igi,corr samples=1,2,3,4,5,6 | sushw key=ns a=6 | \\";
            script.add(command);

            //bhpwritecube
            command = "bhpwritecube init=yes filename=";
            command += "$OUT_DATASET_FILENAME";
            command += " pathlist=";
            command += "${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}";
            command += ".dat key1=";
            command += "ep";
            command += ",";

            command += epFrom;
            command += ",";
            command += epIncr;
            command += ",";
            command += epLength;




            command += " key2=";
            command += "cdp";
            command += ",";
            command += cdpFrom;
            command += ",";
            command += cdpIncr;
            command += ",";
            command += cdpLength;

            command += " horizons=" + "area," + "width," + "amp," + "p1," + "p2," + "p3";
            script.add(command);
            ////script.add("");
            genCheckReturnCodes(script, "bhpreadcube | bhploadhdr | bhpintegrate | bhpstorehdr | bhpcopyhdr | bhpwritecube","");
            //bhptranspose
            command = "bhptranspose filename=";
            command += "$OUT_DATASET_FILENAME";
            command += " pathlist=";
            command += "${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}";
            command += ".dat";
            script.add(command);
            genCheckReturnCodes(script,"bhptranspose","");
            //indicate script ran successfully.
            ///script.add("");
            if(landmarkOutputRadioButton.isSelected()){
                //script.add("#Writing seismic data back to Landmark application.");
                //command = "bhpread pathlist=${QISPACE}/${QIPROJECT}/${QIDATASETS}/${OUT_DATASET}.dat filename=${OUT_DATASET_FILENAME} keys=ep,cdp | \\";
                //script.add(command);
                String pname = "";
                if(landmarkAgent == null){
                    landmarkAgent = LandmarkServices.getInstance();
                }
                int ptype;
                if(landmarkInRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                    pname = landmark2DOutProjectTextField.getText().trim();
                    ptype = landmarkAgent.getLandmarkProjectType(pname);
                }else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                    pname = landmark3DOutProjectTextField.getText().trim();
                    ptype = landmarkAgent.getLandmarkProjectType(pname);
                }else{ //bhpsu input
                    pname = landmarkOutProjectTextField.getText().trim();
                    ptype = landmarkAgent.getLandmarkProjectType(pname);
                }
                //command = "lmwrite lm_project=" + pname;
                //command += " lm_ptype=" + ptype;
                //command += " lm_brickname=${OUT_DATASET_FILENAME}.bri \\";
                //script.add(command);
                //script.add("");
                script.add("#Importing horizon(s) to Landmark application.");
                if(ptype == 3){
                    command = "lmimphor lm_project=" + pname;
                    command += " lm_ptype=" + ptype;
                    command += " horizons=";
                    command += "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_area.xyz," +
                            "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_width.xyz," +
                            "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_amp.xyz," +
                            "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_p1.xyz," +
                            "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_p2.xyz," +
                            "${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_p3.xyz";
                    script.add(command);
                    ///script.add("");
                    genCheckReturnCodes(script,"lmimphor","");
                }else if(ptype == 2){
                	script.add("lmimp2dhor1 lm_project=" + pname + " hname=${OUT_HORIZON_BASENAME}_area" + " horizon=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_area.xyz");
                	script.add("lmimp2dhor1 lm_project=" + pname + " hname=${OUT_HORIZON_BASENAME}_width" + " horizon=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_width.xyz");
                	script.add("lmimp2dhor1 lm_project=" + pname + " hname=${OUT_HORIZON_BASENAME}_amp" + " horizon=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_amp.xyz");
                	script.add("lmimp2dhor1 lm_project=" + pname + " hname=${OUT_HORIZON_BASENAME}_p1" + " horizon=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_p1.xyz");
                	script.add("lmimp2dhor1 lm_project=" + pname + " hname=${OUT_HORIZON_BASENAME}_p2" + " horizon=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_p2.xyz");
                	script.add("lmimp2dhor1 lm_project=" + pname + " hname=${OUT_HORIZON_BASENAME}_p3" + " horizon=${QISPACE}/${QIPROJECT}/${QIHORIZONS}/${OUT_HORIZON}_p3.xyz");
                    genCheckReturnCodes(script,"lmimp2dhor1","");
                }
            }
        }

        script.add("exit_status=$?");
        script.add("echo exit_status=${exit_status}");
        script.add("if [ ${exit_status} != 0 ]");
        script.add("then");
        script.add("    echo $0 job ends abnormally");
        script.add("    exit 1");
        script.add("fi");
        }
        command = "echo \"$0 job ends normally\"";
        script.add(command);

        return new AmpExtFileManager(agent).saveScriptToFile(gui, script, file);
    }

    private void genScriptExportingLandmark3DToBhpsu(ArrayList<String> script){
		if(!landmark3DRadioButton.isSelected() || script == null)
			return;
		int epFrom=0,epTo=0,epIncr=0,epLength=0,cdpFrom=0,cdpTo=0,cdpIncr=0,cdpLength=0;
        int from = Integer.valueOf(linesTextField.getText().trim());
        int to = Integer.valueOf(linesToTextField.getText().trim());
        int by = Integer.valueOf(linesByTextField.getText().trim());
        if(by < 0)
            by = by * (-1);
        if(from > to){
            epFrom = to;
            epTo = from;
        }else{
            epFrom = from;
            epTo = to;
        }
        epIncr = by;
        epLength = ((epTo - epFrom + epIncr) / epIncr);
        
        
        from = Integer.valueOf(tracesTextField.getText().trim());
        to = Integer.valueOf(tracesToTextField.getText().trim());
        by = Integer.valueOf(tracesByTextField.getText().trim());
        if(by < 0)
            by = by * (-1);
        if(from > to){
            cdpFrom = to;
            cdpTo = from;
        }else{
            cdpFrom = from;
            cdpTo = to;
        }
        cdpIncr = by;
        cdpLength = ((cdpTo - cdpFrom + cdpIncr) / cdpIncr);
        
        String volumeName = volumeTextField.getText().trim(); 
        String datasetName = "";
        int ind = volumeName.lastIndexOf(".");
        if(ind != -1){
        	datasetName = volumeName.substring(0,ind);
        }
        
        script.add("echo \"Export Landmark 3D volume " + volumeName + " to BHPSU format " + datasetName + ".dat\"");
        script.add("echo \"Create " + datasetName + ".dat file \"");
        String command = "echo ${QISEISMICS} > ${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + datasetName + ".dat";
        script.add(command);
        script.add("echo  \"Removing existing output files if they exists.\"");
        // bhpio delete output if exists
        command = "bhpio filename=";
        command += datasetName;
        command += " pathlist=";
        command += "${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + datasetName;
        command += ".dat";
        command += " delete=yes";
        script.add(command);
        script.add("");
		command = "lmread lm_project=" + landmarkInProjectTextField.getText().trim();
        command += " lm_ptype=3";
        command += " lm_brickname=" + volumeName;

        command += " iline=";
        command += linesTextField.getText().trim() + ":" + linesToTextField.getText().trim() + ":" + linesByTextField.getText().trim();
        command += " xline=";
        command += tracesTextField.getText().trim() + ":" + tracesToTextField.getText().trim() + ":" + tracesByTextField.getText().trim();
        command += " | \\";
        command += "\nbhpwritecube init=yes filename=" + datasetName + " \\\n";
        command += " pathlist=${QISPACE}/${QIPROJECT}/${QIDATASETS}/" + datasetName + ".dat \\\n";
        command += " key1=ep," + epFrom + "," + epIncr + "," + epLength + " key2=cdp," + cdpFrom + "," + cdpIncr + "," + cdpLength;
        script.add(command);
        genCheckReturnCodes(script,"lmread | bhpwritecube", "");
	}    
    
    private void genCheckReturnCodes(ArrayList<String> script, String jobName, String prependSpace){
        script.add("");
        script.add(prependSpace + "return_code=$?");
        script.add(prependSpace + "echo -e return_code=${return_code} for job \"(" + jobName + ")\"");
        script.add(prependSpace + "if [ ${return_code} != 0 ]");
        script.add(prependSpace + "then");
        script.add(prependSpace + " echo $0 job ends abnormally");
        script.add(prependSpace + " exit 1");
        script.add(prependSpace + "fi");
        script.add("");
    }

    private void genCheckReturnCodesForLm2D(ArrayList<String> script, String jobName, String prependSpace, String lineName){
        script.add("");
        script.add(prependSpace + "return_code=$?");
        script.add(prependSpace + "echo -e return_code=${return_code} for line name \"" + lineName + "\"");
        script.add(prependSpace + "if [ ${return_code} != 0 ]");
        script.add(prependSpace + "then");
        script.add(prependSpace + "   failed_line_array[$kkk]=" + lineName);
        script.add(prependSpace + "   kkk=`expr $kkk + 1`");
        script.add(prependSpace + "   echo -e \"" + jobName + "\" for line name \"" + lineName + "\" NOT successful.");
        //script.add(prependSpace + "   exit 1");
        script.add(prependSpace + "   continue");
        script.add(prependSpace + "fi");
        script.add("");
    }
    /**
     * findParam finds a requested parameter=value pair in a script and returns value
     * @param script ArrayList to search
     * @param command to look for
     * @param which integer specifying which occurence to look for
     * @param param parameter name to look for within command
     * @return String value of parameter
     */
    private static String findParam(ArrayList<String> script, String command, int which, String param) {
        String fields[] = null;
        String names[] = null;
        for (int i=0,j=0; i<script.size(); i++) {
            fields = script.get(i).split(" ");
            if (fields[0].equals(command)) {
                j++;
                if (j == which) {
                    for (int k=1; k<fields.length; k++) {
                        names = fields[k].split("=");
                        if (names[0].equals(param))
                        return names[1];
                    }
                }
            }
        }
        return "";
    }

    /** Debug code: print header limits
     *
     */
    private void showHeaderLimits() {
        for (int i=0; i<headerLimits.size(); i++) {
            ArrayList<String> list = headerLimits.get(i);
            System.out.println("headerLimits["+i+"]:");
            for (int j=0; j<list.size(); j++)
                System.out.println(list.get(j));
        }
    }

    final private Color GRAYISH_COLOR = new Color(180, 180, 200);
    class TableCellColorRenderer extends DefaultTableCellRenderer {
        public TableCellColorRenderer() {
            setOpaque(true); //MUST do this for background to show up.
        }

        public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
            setBackground(GRAYISH_COLOR);
            setForeground(Color.BLACK);
            super.getTableCellRendererComponent(table, value, selected, focused, row, column);
            return this;
        }
    }

    /** Button listener for GUI
     *
     * @author Bob Miller
     * @author Gil Hansen
     */
    public class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            QiProjectDescriptor projDesc = agent.getQiProjectDescriptor();
            ArrayList list = new ArrayList();
            //1st element the parent GUI object
            list.add(0, gui);
            //2nd element is the dialog title
            list.add(1, "");
            //3rd element is a list that contains current directory to start with
            //and a flag (yes or no) indicating if an already remembered directory
            //should be used instead
            ArrayList lst = new ArrayList();
            lst.add(QiProjectDescUtils.getTempPath(projDesc));
            lst.add("no");
            list.add(2, lst);
            //4th element is the file filter
            list.add(3, getFileFilter(new String[]{".sh",".SH"}, "Script file (*.sh)"));
            //5th element is the navigation flag
            list.add(4, true);
            //6th element is the producer component descriptor
            list.add(5, agent.getMessagingMgr().getMyComponentDesc());
            //7th element is the type of file chooser either Open or Save
            list.add(6, QIWConstants.FILE_CHOOSER_TYPE_OPEN);
            //8th element is the message command
            list.add(7, "");
            //9th element is the default pretext for the file name if user fails to pretext it. If no pretext is required then use em   pty String
            list.add(8, "");
            //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
            list.add(9, ".sh");
            //11th element is the target tomcat url where the file chooser chooses
            list.add(10, agent.getMessagingMgr().getTomcatURL());
            String action = e.getActionCommand();

            if (action.equals(AmpExtConstants.AMP_EXT_EXECUTE_SCRIPT)) {
                // Save generated script and execute
                if(landmarkInRadioButton.isSelected())
                        if(!validateLandmarkInput())
                            return;
                if (!validateTextFields())
                    return;

                if(! validateRanges())
                    return;
                //if(!validate2DLineHorizonCrossMatch())
                if(!validate2DHorizons())
                    return;
                if(ensureProjectSetup())
                {
                    String scriptPrefix = saveScript();
                    //execute the script if it was successfully written to a file
                    if (!scriptPrefix.equals("")) {
                        //NOTE: executeScript will set the file summary text area
                        executeScript(scriptPrefix);
                    }
                 }else
                    return;
            }
            else if (action.equals(AmpExtConstants.AMP_EXT_SAVE_SCRIPT)) {
                if(landmarkInRadioButton.isSelected()){
                //    if(landmark3DRadioButton.isSelected()){
                    if(!validateLandmarkInput())
                        return;
                        //if(!validateLandmarkParameters())
                        //  return;
                    if(landmark2DRadioButton.isSelected()){
                          //if(!validate2DLineHorizonCrossMatch())
                          if(!validate2DHorizons())
                            return;
                    }
                }
                if (!validateTextFields())
                    return;
                if(!validateHorizonParams1())
                    return;
                if(!validateRanges())
                    return;

                if(!ensureProjectSetup()){
                    return;
                }
                final String scriptPrefix = saveScript();
                    // update the file summary text area if the script was successfully
                    // written to a file.
                if (!scriptPrefix.equals("")) {
                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                             String scriptDir = QiProjectDescUtils.getTempPath(qiProjectDesc);
                              String text = scriptPrefix + ".sh\n";
                              text += "has been created in " + scriptDir;
                              fileSummaryTextArea.setText(text);
                        }
                     });
                }
            }
            else if (action.equals(AmpExtConstants.AMP_EXT_SELECT_INPUT)) {
                list.set(1, "Select Input Cube");
                //3rd element is a list that contains the directory to start with
                //and a flag (yes or no) indicating whether or not to remember the
                //start directory the next time
                lst = new ArrayList();
                lst.add(QiProjectDescUtils.getDatasetsPath(projDesc));
                lst.add("no");
                list.set(2, lst);
                list.set(3, getFileFilter(new String[]{".dat",".DAT"}, "Data File (*.dat)"));
                list.set(7, AmpExtConstants.AMP_EXT_SELECT_INPUT);
                list.set(9, ".dat");
                try {
                    agent.callFileChooser(list);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            else if (action.equals(AmpExtConstants.AMP_EXT_SELECT_TOP_HORIZON) && bhpsuHorizonRadioButton.isSelected()) {
                list.set(1, AmpExtConstants.AMP_EXT_SELECT_TOP_HORIZON);
                //3rd element is a list that contains the directory to start with
                //and a flag (yes or no) indicating whether or not to remember the
                //start directory the next time
                lst = new ArrayList();
                lst.add(QiProjectDescUtils.getHorizonsPath(projDesc));
                lst.add("no");
                list.set(2, lst);
                list.set(3, getFileFilter(new String[]{".xyz",".XYZ"}, "Horizon File (*.xyz)"));
                list.set(7, AmpExtConstants.AMP_EXT_SELECT_TOP_HORIZON);
                list.set(9, ".xyz");
                try {
                    agent.callFileChooser(list);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }else if (action.equals(AmpExtConstants.AMP_EXT_SELECT_TOP_HORIZON)  && bhpsuHorizonDatasetRadioButton.isSelected()) {
                QiFileChooserDescriptor desc = new QiFileChooserDescriptor();
                desc.setParentGUI(gui);
                desc.setTitle("Select BHP-SU Dataset");
                desc.setHomeDirectory(QiProjectDescUtils.getDatasetsPath(projDesc));
                desc.setDirectoryRememberedEnabled(false);
                String [] filterList = { ".dat",".DAT" };
                GenericFileFilter filter =  new GenericFileFilter(filterList,"Data Files (*.dat)");
                desc.setFileFilter(filter);
                desc.setNavigationUpwardEnabled(false);
                desc.setMultiSelectionEnabled(false);
                desc.setetProducerComponentDescriptor(agent.getComponentDescriptor());
                desc.setDialogType(javax.swing.JFileChooser.OPEN_DIALOG);
                desc.setMessageCommand(AmpExtConstants.GET_SEISMIC_DATASET_CMD);
                desc.setServerUrl(agent.getMessagingMgr().getTomcatURL());
                Map props = new HashMap();
                props.put("target", bhpsuTopHorizonTextField);
                desc.setAdditionalProperties(props);
                agent.callFileChooser(desc);
            }
            else if (action.equals(AmpExtConstants.AMP_EXT_SELECT_BOTTOM_HORIZON) && bhpsuHorizonDatasetRadioButton.isSelected()) {
                QiFileChooserDescriptor desc = new QiFileChooserDescriptor();
                desc.setParentGUI(gui);
                desc.setTitle("Select BHP-SU Dataset");
                desc.setHomeDirectory(QiProjectDescUtils.getDatasetsPath(projDesc));
                desc.setDirectoryRememberedEnabled(false);
                String [] filterList = { ".dat",".DAT" };
                GenericFileFilter filter =  new GenericFileFilter(filterList,"Data Files (*.dat)");
                desc.setFileFilter(filter);
                desc.setNavigationUpwardEnabled(false);
                desc.setMultiSelectionEnabled(false);
                desc.setetProducerComponentDescriptor(agent.getComponentDescriptor());
                desc.setDialogType(javax.swing.JFileChooser.OPEN_DIALOG);
                desc.setMessageCommand(AmpExtConstants.GET_SEISMIC_DATASET_CMD);
                desc.setServerUrl(agent.getMessagingMgr().getTomcatURL());
                Map props = new HashMap();
                props.put("target", bhpsuBottomHorizonTextField);
                desc.setAdditionalProperties(props);
                agent.callFileChooser(desc);
            }
            else if (action.equals(AmpExtConstants.AMP_EXT_SELECT_BOTTOM_HORIZON)) {
                list.set(1, AmpExtConstants.AMP_EXT_SELECT_BOTTOM_HORIZON);
                //3rd element is a list that contains the directory to start with
                //and a flag (yes or no) indicating whether or not to remember the
                //start directory the next time
                lst = new ArrayList();
                lst.add(QiProjectDescUtils.getHorizonsPath(projDesc));
                lst.add("no");
                list.set(2, lst);
                list.set(3, getFileFilter(new String[]{".xyz",".XYZ"}, "Horizon File (*.xyz)"));
                list.set(7, AmpExtConstants.AMP_EXT_SELECT_BOTTOM_HORIZON);
                list.set(9, ".xyz");
                try {
                    agent.callFileChooser(list);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            else if (action.equals(AmpExtConstants.AMP_EXT_SELECT_HORIZON) && bhpsuHorizonDatasetRadioButton.isSelected()) {
                QiFileChooserDescriptor desc = new QiFileChooserDescriptor();
                desc.setParentGUI(gui);
                desc.setTitle("Select BHP-SU Dataset");
                desc.setHomeDirectory(QiProjectDescUtils.getDatasetsPath(projDesc));
                desc.setDirectoryRememberedEnabled(false);
                String [] filterList = { ".dat",".DAT" };
                GenericFileFilter filter =  new GenericFileFilter(filterList,"Data Files (*.dat)");
                desc.setFileFilter(filter);
                desc.setNavigationUpwardEnabled(false);
                desc.setMultiSelectionEnabled(false);
                desc.setetProducerComponentDescriptor(agent.getComponentDescriptor());
                desc.setDialogType(javax.swing.JFileChooser.OPEN_DIALOG);
                desc.setMessageCommand(AmpExtConstants.GET_SEISMIC_DATASET_CMD);
                desc.setServerUrl(agent.getMessagingMgr().getTomcatURL());
                Map props = new HashMap();
                props.put("target", bhpsuHorizonTextField);
                desc.setAdditionalProperties(props);
                agent.callFileChooser(desc);
            }
            else if (action.equals(AmpExtConstants.AMP_EXT_SELECT_HORIZON)) {
                list.set(1, AmpExtConstants.AMP_EXT_SELECT_HORIZON);
                //3rd element is a list that contains the directory to start with
                //and a flag (yes or no) indicating whether or not to remember the
                //start directory the next time
                lst = new ArrayList();
                lst.add(QiProjectDescUtils.getHorizonsPath(projDesc));
                lst.add("no");
                list.set(2, lst);
                list.set(3, getFileFilter(new String[]{".xyz",".XYZ"}, "Horizon File (*.xyz)"));
                list.set(7, AmpExtConstants.AMP_EXT_SELECT_HORIZON);
                list.set(9, ".xyz");
                try {
                    agent.callFileChooser(list);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            else if (action.equals("2H")){
                if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected()){
                    switch2EdgeMode();
                }else if(landmarkHorizonRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
                    switch2Landmark3DEdgeMode();
                }else if(landmarkHorizonRadioButton.isSelected() && landmark2DRadioButton.isSelected()){
                    switch2Landmark2DEdgeMode();
                }
            }else if (action.equals("1H")){
                if(bhpsuHorizonRadioButton.isSelected() || bhpsuHorizonDatasetRadioButton.isSelected())
                    switch2PointMode();
                else if(landmarkHorizonRadioButton.isSelected() && landmark3DRadioButton.isSelected())
                    switch2Landmark3DPointMode();
                else if(landmarkHorizonRadioButton.isSelected() && landmark2DRadioButton.isSelected())
                    switch2Landmark2DPointMode();
            }else if (action.equals("SaveState"))
                agent.saveState();
            else if (action.equals("SaveAsState"))
                agent.saveStateAsClone();
            else if (action.equals("SaveStateThenQuit"))
                agent.saveStateThenQuit();
            else if (action.equals("Quit")) {
                // send request to Workbench Manager
                agent.deactivateSelf();
            }
        }
    }

    private Set related2DLines = new HashSet();
    private boolean validate2DHorizons(){
        if(!(landmarkHorizonRadioButton.isSelected() && landmark2DRadioButton.isSelected()) || processPanel.isNotRunAmpExtChecked())
            return true;
        List<String> horizons = new ArrayList<String>();
        if(edgeModeRadioButton.isSelected()){
            if(landmark2DTopHorizonTextField.getText().trim().equals(landmark2DBottomHorizonTextField.getText().trim())){
                horizons.add(landmark2DTopHorizonTextField.getText().trim());
            }else{
                horizons.add(landmark2DTopHorizonTextField.getText().trim());
                horizons.add(landmark2DBottomHorizonTextField.getText().trim());
            }
        }else if(pointModeRadioButton.isSelected()){
            horizons.add(landmark2DHorizonTextField.getText().trim());
        }
        
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        Set set = new HashSet();
        related2DLines.clear();
        for(int i = 0; horizons != null && i < horizons.size(); i++){
        	String[] lines = agent.get2DRelatedLinesByHorizonName(landmarkIn2DProjectTextField.getText().trim(),horizons.get(i));
        	if(lines == null || lines.length == 0){
        		JOptionPane.showMessageDialog(gui, "Can not find any 2D lines associated with the horizon: " + horizons.get(i),
                "Horizon Not Found", JOptionPane.WARNING_MESSAGE);
        		ampExtTabbedPane.setSelectedIndex(tabIdx);
        		return false;
        	}
        	for(String s : lines){
        		if(!related2DLines.contains(s))
        			related2DLines.add(s);
        	}
        }
        return true;
    }    
    
    private boolean validate2DLineHorizonCrossMatch(){
        if(!(landmarkHorizonRadioButton.isSelected() && landmark2DRadioButton.isSelected()) || processPanel.isNotRunAmpExtChecked())
            return true;
        List<String> lt = getUnmatched2DLinesByHorizons();
        StringBuffer buf = new StringBuffer();
        for(int i = 0; lt != null && i < lt.size(); i++){
        	buf.append(lt.get(i) + "\n");
        }
        //List<String> lt = getUnmatched2DLinesHorizons();
        int tabIdx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        if(lt != null && lt.size() > 0){
            //for(String s : lt){
            	int n = JOptionPane.showConfirmDialog(
	                    gui, "The following line(s) " + buf.toString() + " does not match with the selected horizon(s). Do you want to ignore them and proceed with the process?",
	                    "Confirm to proceed",
	                    JOptionPane.YES_NO_OPTION);
	            if(n != JOptionPane.YES_OPTION){
	            	return false;
	            }else{
	            	ampExtTabbedPane.setSelectedIndex(tabIdx);
	            	return true;
	            }
                /*JOptionPane.showMessageDialog(gui, s + " does not match with all the selected lines.",
                    "Line/Horizon Mismatched", JOptionPane.WARNING_MESSAGE);
                ampExtTabbedPane.setSelectedIndex(tabIdx);
                if(edgeModeRadioButton.isSelected()){
                    if(landmark2DTopHorizonTextField.getText().trim().equals(s)){
                        //landmark2DTopHorizonTextField.setText("");
                        topHorizonBrowseButton.requestFocus();
                        return false;
                    }
                    if(landmark2DBottomHorizonTextField.getText().trim().equals(s)){
                        //landmark2DBottomHorizonTextField.setText("");
                        bottomHorizonBrowseButton.requestFocus();
                        return false;
                    }
                }else if(pointModeRadioButton.isSelected()){
                    if(landmark2DHorizonTextField.getText().trim().equals(s)){
                        //landmark2DHorizonTextField.setText("");
                        horizonBrowseButton.requestFocus();
                        return false;
                    }
                }
            //}*/
        }
        return true;
    }
    private boolean pass = false;
    private boolean ensureProjectSetup(){
        List<String> list = agent.getUnmadeQiProjDescDirs();
        String projectRoot = QiProjectDescUtils.getProjectPath(agent.getProjectDescriptor());

        if(list != null && list.size() > 0){
            int returnStatus = QiProjectDescUtils.showQiProjectMetaDataSetupDialog(
                    JOptionPane.getFrameForComponent(this), true, list, projectRoot);
            if(returnStatus == JOptionPane.CANCEL_OPTION)
                return false;
            if(list.size() > 0){
                final List<String> flist = list;
                Runnable heavyRunnable = new Runnable(){
                    public void run(){
                        if(agent.makeQiProjDescDirs(flist))
                            pass = true;
                        else
                            pass = false;
                    }
                };
                Thread thread = new Thread(heavyRunnable);
                thread.start();
                try{
                    thread.join();
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
                if(!pass){
                    logger.info("Creating QiProject directory structure not pass.");
                    return false;
                }
                logger.info("Creating QiProject directory structure succeeds.");
                return true;
            }
        }
        return true;
    }
    /** Generate file filter for use by file chooser service
     * @param filterList extensions to use
     * @return filter
     */
    private FileFilter getFileFilter(String[] filterList, String fileType) {
        FileFilter filter = (FileFilter) new GenericFileFilter(filterList, fileType);
        return filter;
    }

    //private DataSetHeaderKeyInfo timeRangeInfo;
    /** Set input filename returned by file chooser service.
     *
     * @param fileName Full path of .dat file selected by a file chooser service
     */
    public void setInput(String fileName) {
        System.out.println("setInput File Name: " + fileName);
        if (!fileName.equals("")) {
            // The input cube dataset's full pathname needs to be saved as part of the state.
            if (inputCubeDataset.equals("")) inputCubeDataset = fileName;
            String name = fileName.substring(fileName.lastIndexOf(filesep)+1,fileName.lastIndexOf("."));
            AmpExtFileManager fileManager = new AmpExtFileManager(agent);
            String path = fileManager.getPath(gui,fileName);
            headerLimits = fileManager.getHeaderLimits(gui, path + filesep + name + "_0000.0001.HDR");
            if (headerLimits.isEmpty()) {
                agent.showErrorDialog(QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                         "Error reading header files for " + fileName,
                         new String[] {"File was never written","File is corrupted"},
                         new String[] {"Use 'bhpio' command to check file",
                                       "If it doesn't exist, use bhpwritecube to make it",
                                       "If it is OK, contact workbench support"});
            } else {
                initRanges(headerLimits);
                inputCubeTextField.setText(name);
                if (AmpExtConstants.DEBUG_PRINT > 0)
                    showHeaderLimits();
            }
            //timeRangeInfo = fileManager.getTimeRangeInfo(gui, path + filesep + name + "_0000.HDR");
        } else {
            agent.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                    "No file returned from file chooser service");
        }
    }

    /** Set output filename returned by file chooser service
     *
     * @param fileName returned from file chooser service
     */
    public void setOutput(String fileName) {
        if (!fileName.equals("")) {
            String name = fileName.substring(fileName.lastIndexOf(filesep)+1,fileName.lastIndexOf("."));
            outputCubeTextField.setText(name);
        } else {
            agent.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                    "No file returned from file chooser service");
        }
    }

    /** Set top horizon filename returned from file chooser service
     * @param fileName returned from file chooser service
     */
    public void setTopHorizon(String fileName) {
        if (!fileName.equals("")) {
            //String name = fileName.substring(fileName.lastIndexOf(filesep)+1,fileName.lastIndexOf("."));
            String horizonDirPath = QiProjectDescUtils.getHorizonsPath(agent.getQiProjectDescriptor());
            int ind = fileName.lastIndexOf(".");
            String name = "";
            if(ind != -1)
                name = fileName.substring(horizonDirPath.length()+1,fileName.lastIndexOf("."));
            else
                name = fileName.substring(horizonDirPath.length()+1);
            bhpsuTopHorizonTextField.setText(name);
        } else {
            agent.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                    "No file returned from file chooser service");
        }
    }

    /** Set bottom horizon filename returned from file chooser service
     * @param fileName returned from file chooser service
     */
    public void setBottomHorizon(String fileName) {
        if (!fileName.equals("")) {
            //String name = fileName.substring(fileName.lastIndexOf(filesep)+1,fileName.lastIndexOf("."));
            String horizonDirPath = QiProjectDescUtils.getHorizonsPath(agent.getQiProjectDescriptor());
            int ind = fileName.lastIndexOf(".");
            String name = "";
            if(ind != -1)
                name = fileName.substring(horizonDirPath.length()+1,fileName.lastIndexOf("."));
            else
                name = fileName.substring(horizonDirPath.length()+1);
            bhpsuBottomHorizonTextField.setText(name);
        } else {
            agent.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                    "No file returned from file chooser service");
        }
    }

    /** Set horizon filename returned from file chooser service
     * @param fileName returned from file chooser service
     */
    public void setHorizon(String fileName) {
        if (!fileName.equals("")) {
            //String name = fileName.substring(fileName.lastIndexOf(filesep)+1,fileName.lastIndexOf("."));
            String horizonDirPath = QiProjectDescUtils.getHorizonsPath(agent.getQiProjectDescriptor());
            int ind = fileName.lastIndexOf(".");
            String name = "";
            if(ind != -1)
                name = fileName.substring(horizonDirPath.length()+1,fileName.lastIndexOf("."));
            else
                name = fileName.substring(horizonDirPath.length()+1);
            bhpsuHorizonTextField.setText(name);
        } else {
            agent.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                    "No file returned from file chooser service");
        }
    }

    /**
     * Update the stdout text area.
     */
    public void setStdoutTextArea(String text) {
        final String txt = text;
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                stdoutTextArea.setText(txt);
            }
        });
    }

    /**
     * Add to the stdout text area.
     */
    public void addToStdoutTextArea(String text) {
        String tx = stdoutTextArea.getText();
        final String txt = tx + "\n" + text;
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                stdoutTextArea.setText(txt);
            }
        });
    }

    public void setRunAmpExtEabled(boolean bool){
        int tabHorIdx = ampExtTabbedPane.indexOfTab(TN_HORIZON_DATA_INPUT);
        int tabOutputsIdx = ampExtTabbedPane.indexOfTab(TN_OUTPUTS);
        ampExtTabbedPane.setEnabledAt(tabHorIdx, bool);
        ampExtTabbedPane.setEnabledAt(tabOutputsIdx, bool);
    }
    /**
     * Update the stderr text area.
     */
    public void setStderrTextArea(String text) {
        final String txt = text;
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                stderrTextArea.setText(txt);
            }
        });
    }

    /**
     * Close GUI and dispose, called by plugin when user has terminated gui
     *
     */
    public void closeAmpExtGUI() {
        setVisible(false);
        dispose();
    }

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    private static void createAndShowGUI() {
        JFrame frame = new JFrame();
        // Make sure we have nice window decorations.
        frame.setDefaultLookAndFeelDecorated(true);

        // Create and set up the window.
        JInternalFrame decom = new AmplitudeExtraction();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JDesktopPane desktop = new JDesktopPane();
        frame.setContentPane(desktop);
        desktop.add(decom);
        decom.setVisible(true);

        // Display the window.
        frame.setSize(decom.getWidth(), decom.getHeight());
        frame.setVisible(true);
    }

    private boolean validateLandmarkEnvironment(){
        String os = System.getProperty("os.name");
        if(os.startsWith("Windows")){
            JOptionPane.showMessageDialog(this, "Landmark services are not currently supported in Windows environment.",
                    "Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
            bhpsuInRadioButton.setSelected(true);
            return false;
        }
        Map envmap = System.getenv();

        if(!envmap.containsKey("OWHOME")){
            JOptionPane.showMessageDialog(this, "Environment variable OWHOME must be set prior to using Landmark services.",
                    "Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
            bhpsuInRadioButton.setSelected(true);
            return false;
        }
        if(!envmap.containsKey("ORACLE_HOME")){
            JOptionPane.showMessageDialog(this, "Environment variable ORACLE_HOME must be set prior to using Landmark services.",
                    "Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
            bhpsuInRadioButton.setSelected(true);
            return false;
        }
        if(!envmap.containsKey("OW_PMPATH")){
            JOptionPane.showMessageDialog(this, "Environment variable OW_PMPATH must be set prior to using Landmark services.",
                    "Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
            bhpsuInRadioButton.setSelected(true);
            return false;
        }
        if(!envmap.containsKey("LM_LICENSE_FILE")){
            JOptionPane.showMessageDialog(this, "Environment variable LM_LICENSE_FILE must be set prior to using Landmark services.",
                    "Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
            bhpsuInRadioButton.setSelected(true);
            return false;
        }
        if(!envmap.containsKey("LD_LIBRARY_PATH")){
            JOptionPane.showMessageDialog(this, "Environment variable LD_LIBRARY_PATH must be set prior to using Landmark services.",
                    "Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
            bhpsuInRadioButton.setSelected(true);
            return false;
        }/*else{
            String value = (String)envmap.get("LD_LIBRARY_PATH");
            String ldpath = AmpExtConstants.JNI_LIBS_LOCATION;
            if(!value.contains(ldpath)){
                JOptionPane.showMessageDialog(this,"Environment variable LD_LIBRARY_PATH must be set to include "
                        + ldpath + " prior to using Landmark services.",
                        "Environment Variable Not Properly Set", JOptionPane.WARNING_MESSAGE);
                bhpsuInRadioButton.setSelected(true);
                return false;
            }
        }
        File lib = new File(AmpExtConstants.JNI_LIBS_LOCATION + "/lib"
                + AmpExtConstants.LANDMARK_JNI_LIB_NAME + ".so");
        if(!lib.exists()){
            JOptionPane.showMessageDialog(this,"Can not find the library file: lib" + AmpExtConstants.LANDMARK_JNI_LIB_NAME + ".so from "
                    + AmpExtConstants.JNI_LIBS_LOCATION + " required before using Landmark services.",
                    "Required Library Not Found", JOptionPane.WARNING_MESSAGE);
            bhpsuInRadioButton.setSelected(true);
            return false;
        }
        */
        return true;
    }

    private void landmarkInRadioButtonActionPerformed(
            java.awt.event.ActionEvent evt) {

        if(validateLandmarkEnvironment() == false){
            bhpsuInRadioButton.setSelected(true);
            return;
        }
/*
        if(landmarkAgent.getLandmarkAllProjects() == null){
            if(!agent.populateLandmarkProjects()){
                bhpsuInRadioButton.setSelected(true);
                return;
            }
        }
        */
        switch2LandmarkInput();

    }



    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    /**
     * Reset the GUI's title
     *
     * @param name Name of the associated project.
     */
    public void resetTitle(String projName) {
        String compName = "";
        IComponentDescriptor agentDesc = agent.getComponentDescriptor();
        String pdn = CompDescUtils.getDescPreferredDisplayName(agentDesc);
        String screenName = CompDescUtils.getDescDisplayName(agentDesc);
        if (pdn.trim().length() > 0)
            compName = pdn.trim();
        else
            compName = screenName;

        //get the type of qiComponent
        int idx = screenName.indexOf("#");
        if (idx != -1) screenName = screenName.substring(0, idx);
        this.setTitle(screenName+": " + compName + "  Project: " + projName);
    }

static class LineInfo{
        private String projectName;
        private String lineName;
        private int lineNumber = 0;
        private float minShotPoint = 0;
        private float maxShotPoint = 0;
        private float incShotPoint = 0;
        private int minTrace = 0;
        private int maxTrace = 0;
        private int incTrace = 0;
        private float minShotPoint_def;
        private float maxShotPoint_def;
        private float incShotPoint_def;

        public LineInfo(String pname, String lname, int lineNum, float defMinSnt, float defMaxSnt, float defIncSnt){
            projectName = pname;
            lineName = lname;
            lineNumber = lineNum;
            minShotPoint_def = defMinSnt;
            maxShotPoint_def = defMaxSnt;
            incShotPoint_def = defIncSnt;
        }
        public String getProjectName(){
            return projectName;
        }
        public int getlineNumber(){
            return lineNumber;
        }
        public float getMinShotPoint(){
            return minShotPoint;
        }
        public float getMaxShotPoint(){
            return maxShotPoint;
        }
        public float getIncShotPoint(){
            return incShotPoint;
        }

        public int getMinTrace(){
            return minTrace;
        }
        public int getMaxTrace(){
            return maxTrace;
        }
        public int getIncTrace(){
            return incTrace;
        }

        public float getMinShotPoint_def(){
            return minShotPoint_def;
        }
        public float getMaxShotPoint_def(){
            return maxShotPoint_def;
        }
        public float getIncShotPoint_def(){
            return incShotPoint_def;
        }

        public void setProjectName(String name){
            projectName = name;
        }


        public void setLineNumber(int pnt){
            lineNumber = pnt;
        }
        public void setMinShotPoint(float pnt){
            minShotPoint = pnt;
        }
        public void setMaxShotPoint(float pnt){
            maxShotPoint = pnt;
        }
        public void setIncShotPoint(float pnt){
            incShotPoint = pnt;
        }

        public void setMinTrace(int tr){
            minTrace = tr;
        }
        public void setMaxTrace(int tr){
            maxTrace = tr;
        }
        public void setIncTrace(int tr){
            incTrace = tr;
        }

        public String toString(){
            return "Project Name=" + projectName + " Line Name=" + lineName + "Line Num= " + lineNumber + " Default Range= (" + minShotPoint_def + "," + maxShotPoint_def
            + "," +  incShotPoint_def + ") Actual Range= (" + minShotPoint + "," + maxShotPoint
            + "," +  incShotPoint + ") Actual Trace= (" + minTrace + "," + maxTrace + "," + incTrace + ")";
        }
    }

    static class HorizonParamJTextField extends JTextField {
        public HorizonParamJTextField(javax.swing.table.TableModel model) {
            super();
            final javax.swing.table.DefaultTableModel fmodel = (javax.swing.table.DefaultTableModel)model;
            addFocusListener(new FocusAdapter() {
                public void focusLost(FocusEvent e) {
                    fmodel.fireTableDataChanged();
                }
            });
            //addMouseListener(...//etc
       }
    }

    static class HorizonParamCellEditor extends DefaultCellEditor {
        HorizonParamJTextField textfield;
       public HorizonParamCellEditor(HorizonParamJTextField t ) {
          super(t);
          textfield = t;
       }

       // This method is optional. It makes the JTable behave like an Excell spreadsheet
       public Component getTableCellEditorComponent(JTable table,Object value, boolean isSelected,int row, int column) {
          Component c = super.getTableCellEditorComponent(table,value,isSelected,row,column);
          if (isSelected)
             textfield.selectAll();
          return c;
       }
    }

    // Accessors for GUI fields
    /**
     * Get the name of the input cube.
     * @returns Name of the input cube.
     */
    public String getInputCube() {
    	String name = "";
    	if(bhpsuInRadioButton.isSelected())
    		name = inputCubeTextField.getText().trim();
    	else if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected()){
    		String volumeName = volumeTextField.getText().trim();
    		int ind = volumeName.lastIndexOf(".");
    		if(ind != -1)
    			volumeName = volumeName.substring(0, ind);
    		name = volumeName;
    	}
    	return name;
    }

    /**
     * Get the full pathname of the input cube
     * @returns Full path of the input cube
     */
    public String getInputCubePath() {
        String filename = getInputCube();
        String filedir = QiProjectDescUtils.getDatasetsPath(agent.getQiProjectDescriptor());
        String filesep = agent.getMessagingMgr().getServerOSFileSeparator();
        return filedir + filesep + filename  + ".dat";
    }

    /**
     * Get the name of the output cube.
     * @returns Name of the output cube.
     */
    public String getOutputCube() {
    	String name = "";
    	if(bhpsuOutputRadioButton.isSelected())
    		name = outputCubeTextField.getText().trim();
    	else if(landmarkOutputRadioButton.isSelected()){
    		if(landmarkInRadioButton.isSelected() && landmark3DRadioButton.isSelected())
    			name = landmark3DOutputCubeTextField.getText().trim();
    	}
    	return name;
    }

    /**
     * Get the full pathname of the output cube
     * @returns Full path of the output cube
     */
    public String getOutputCubePath() {
        String filename = getOutputCube();
        String filedir = QiProjectDescUtils.getDatasetsPath(agent.getQiProjectDescriptor());
        String filesep = agent.getMessagingMgr().getServerOSFileSeparator();
        return filedir + filesep + filename  + ".dat";
    }

    /**
     * Get the full pathname of the output map
     * @returns Full path of the output map
     */
    public String getOutputMapPath() {
        String filename = outputCubeTextField.getText();
        String filedir = QiProjectDescUtils.getDatasetsPath(agent.getQiProjectDescriptor());
        String filesep = agent.getMessagingMgr().getServerOSFileSeparator();
        return filedir + filesep + filename  + "_transpose_ep.dat";
    }
    
    public boolean isBhpsuInput() {
        return bhpsuInRadioButton.isSelected();
    }
    
    public boolean isBhpsuOutput() {
        return bhpsuOutputRadioButton.isSelected();
    }
}
