package com.bhpb.AmplitudeExtraction;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.ListModel;


public	class ListSearcher extends KeyAdapter {
	protected JList m_list;
	protected ListModel m_model;
	protected String m_key = "";
	protected long m_time = 0;
	private JButton ok;
	public static final int CHAR_DELTA = 1000;

	public ListSearcher(JList list,JButton ok)
	{
		m_list = list;
		m_model = m_list.getModel();
		this.ok = ok;
	}

	public void keyTyped(KeyEvent e)
	{
		char ch = e.getKeyChar();
		if (!Character.isLetterOrDigit(ch)){
			if ((int)ch == KeyEvent.VK_ENTER){
				ok.doClick();
			}
			return;
		}
		if (m_time+CHAR_DELTA < System.currentTimeMillis())
			m_key = "";
		m_time = System.currentTimeMillis();

		m_key += Character.toLowerCase(ch);
		for (int k = 0; k < m_model.getSize(); k++)
		{
			String str = ((String)m_model.getElementAt(k)).toLowerCase();
			if (str.startsWith(m_key))
			{
				m_list.setSelectedIndex(k);
				m_list.ensureIndexIsVisible(k);
				break;
			}
		}
	}

}
