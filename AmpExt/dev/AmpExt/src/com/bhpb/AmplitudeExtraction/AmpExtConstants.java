/*
###########################################################################
# AmpExt - horizon-based Secant amplitude and area extraction
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.AmplitudeExtraction;

import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Constants used within Amplitude Extraction
 *
 * @author Gil Hansen
 * @version 1.0
 */
public final class AmpExtConstants {
    /**
     * Prevent object construction outside of this class.
     */
    private AmpExtConstants() {}

    /** plugin's display name */
    public static final String AMP_EXT_PLUGIN_NAME = "ampExt";

    /** plugin GUI's display name */
    public static final String AMP_EXT_GUI_NAME = "Amplitude Extraction GUI";

    // debug print: 0=none, 1=moderate, 2=verbose
    public static final int DEBUG_PRINT = 2;

    // integer status
    public static final int OK_STATUS = 0;
    public static final int ERROR_STATUS = 1;

    // messages used for file chooser service
    public static final String AMP_EXT_SELECT_INPUT = "Select Input";
    public static final String GET_SEISMIC_DATASET_CMD = "getSeismicDataset";
    public static final String AMP_EXT_SELECT_OUTPUT = "Select Output";
    public static final String AMP_EXT_SELECT_TOP_HORIZON = "Select Top Horizon";
    public static final String AMP_EXT_SELECT_BOTTOM_HORIZON = "Select Bottom Horizon";
    public static final String AMP_EXT_SELECT_HORIZON = "Select Horizon";
//    public static final String AMP_EXT_LOAD_SCRIPT = "Load Script";
    public static final String AMP_EXT_EXECUTE_SCRIPT = "Execute Script";
    public static final String AMP_EXT_SAVE_SCRIPT = "Save Script";
    public static final String LANDMARK_JNI_LIB_NAME = "landmarkprojects";
    public static final String JNI_LIBS_LOCATION = System.getProperty("user.home") + "/" + QIWConstants.QIWB_WORKING_DIR + "/qiLib";
    public static final String HORIZON_SOURCE_ASCII = "0";
    public static final String HORIZON_SOURCE_BHPSU = "1";
    public static final String HORIZON_SOURCE_LANDMARK_3D = "2";
    public static final String HORIZON_SOURCE_LANDMARK_2D = "3";

    // STEP STATES for display finite state machine
//    public static final int CREATE_XSEC_WIN1 = 1;
//    public static final int CREATE_XSEC_WIN2 = 2;
    public static final int CREATE_MAP_WIN1 = 3;
    public static final int CREATE_MAP_WIN2 = 4;
//    public static final int CREATE_XSEC_WIN3 = 5;
//    public static final int CREATE_XSEC_WIN4 = 6;
    public static final int GET_LAYER_PROPS1 = 7;
    public static final int GET_LAYER_PROPS2 = 8;
    public static final int GET_LAYER_PROPS3 = 9;
    public static final int GET_LAYER_PROPS4 = 10;

    // ACTION STATES for display transducer
    public static final int CREATE_WINDOW = 1;
    public static final int LOAD_DATA = 2;
    public static final int GET_LAYER_PROP = 3;
    public static final int SET_LAYER_PROPS = 4;
    public static final int RESIZE_WINDOW = 5;
    public static final int SET_LAYER_PROPS1 = 6;
    public static final int SET_LAYER_PROPS2 = 7;
    public static final int GET_SYNC_PROP = 8;
    public static final int SET_SYNC_PROPS = 9;
    public static final int GET_ANNO_PROP = 10;
    public static final int SET_ANNO_PROPS = 11;
    public static final int ADD_LAYER = 12;
    public static final int GET_LAYER_PROP1 = 13;
    public static final int DISPLAY_WINDOW = 14;
    public static final int SET_BROADCAST = 15;
    public static final int GET_SCALE_PROP = 16;
    public static final int SET_SCALE_PROPS = 17;
    public static final int SELECT_WIN_TOOL = 18;
    public static final int SET_LAYER_PROPS3 = 19;
    public static final int RESIZE_VIEWER = 20;
    public static final int GET_LAYER_PROPS = 21;
	public static final int ADD_LAYER2 = 22;
	public static final int ADD_LAYER3 = 23;
	public static final int INIT_PROGRESS_DIALOG = 24;
	public static final int END_PROGRESS_DIALOG = 25;
}
