/*
###########################################################################
# AmpExt - horizon-based Secant amplitude and area extraction
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.AmplitudeExtraction;

import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import java.awt.Component;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import com.bhpb.qiworkbench.compAPI.JobManager;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

/** Manage script files and data files.
 *
 * @author ahmilb
 *
 */
public class AmpExtFileManager {

  // java logging utility
  private static Logger logger = Logger.getLogger(AmpExtFileManager.class.getName());
  // plugin agent component, needed for messaging
  private AmpExtPlugin agent;
  // workbench job manager
  private JobManager jobManager;
  /** constructor captures plugin agent component
   *
   * @param agent plugin component
   */
  public AmpExtFileManager(AmpExtPlugin agent){
      this.agent = agent;
  }

  /**
   * Uses a Job Service to execute script file selected in AmpFileChooser
   * @param comp parent component for dialog display
   * @param filePath path to script file
   * return status, OK_STATUS or status from JobManager
   *
   * @deprecated Monitor the job instead of waiting for it to run to completion.
   */
  public int executeScript(Component comp, String filePath) {
    ArrayList<String> params = new ArrayList<String>();
    // make sure script is executable
    params.add(agent.getMessagingMgr().getLocationPref());
    params.add("sh");
    params.add("-c");
    params.add("chmod +x " + filePath);
//so will compile    jobManager = new JobManager(agent.getMessagingMgr());
    jobManager = new JobManager(agent.getMessagingMgr(),comp);
    int status = jobManager.submitJobWait(params);
    if (status != AmpExtConstants.OK_STATUS) return status;

    // Run the script
    ArrayList<String> params1 = new ArrayList<String>();
    params1.add(agent.getMessagingMgr().getLocationPref());
    params1.add("sh");
    params1.add("-c");
    params1.add(filePath);
    
    status = jobManager.submitJobWait(params1);
    ArrayList<String> stderr = jobManager.getStdErr();
    if (status != AmpExtConstants.OK_STATUS) {
      // dump stderr to console
      for (int i=0; i<stderr.size(); i++)
        System.out.println(stderr.get(i));
    } else {
      ArrayList<String> part = new ArrayList<String>();
      for (int i=0; i<stderr.size(); i++) {
        if (stderr.get(i).contains("wrote")) {
          part.add(stderr.get(i));
        }
      }
      // show number of traces written
      JOptionPane.showMessageDialog(comp, part.get(0) + "\n" + part.get(1),"Job Results",
                                    JOptionPane.INFORMATION_MESSAGE);
    }
    return status;
  }

  /**
   * Read the *.dat file for a selected dataset, and return it so
   * AmplitudeExtraction knows where to find header limits file.
   * @param comp Parent Component for dialogs.
   * @param path Full path/file of *.dat file.
   * @return First line of the *.dat file.
   */
  public String getPath(Component comp, String path) {
    String pathName = "";
    ArrayList<String> params = new ArrayList<String>();
    params.add(agent.getMessagingMgr().getLocationPref());
    params.add(path);
    String msgID = agent.getMessagingMgr().sendRequest(QIWConstants.CMD_MSG,QIWConstants.FILE_READ_CMD,
                                                       QIWConstants.ARRAYLIST_TYPE,params,true);
    IQiWorkbenchMsg response = agent.getMessagingMgr().getMatchingResponseWait(msgID,10000);
    if(response == null || MsgUtils.isResponseAbnormal(response)) {
      logger.finest("IO error:"+(String)MsgUtils.getMsgContent(response));
      agent.showErrorDialog(QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                            "Error occured while reading" + path,
                            new String[] {"Permission error"},
                            new String[] {"Verify " + path + "  is readable by you",
                                          "If yes, contact workbench support"});
    }
    else {
      ArrayList<String> list = (ArrayList<String>)MsgUtils.getMsgContent(response);
      if(list.isEmpty())
        agent.showErrorDialog(QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                              path + " is empty",
                              new String[] {"Must have at least one seismic path"},
                              new String[] {"Add seismic directory name to " + path});
      else
        pathName = list.get(0);
    }
    return pathName;
  }

  /** loadScriptFromFile loads a selected script file
   * @param comp is parent Componant for dialogs
   * @param path is full path/file os script to load
   * @return ArrayList contains script
   *
   */
  public ArrayList<String> loadScriptFromFile(Component comp, String path) {
    ArrayList<String> script = new ArrayList<String>();
    // call service to get script
    ArrayList<String> params = new ArrayList<String>();
    params.add(agent.getMessagingMgr().getLocationPref());
    params.add(path);
    String msgID = agent.getMessagingMgr().sendRequest(QIWConstants.CMD_MSG,QIWConstants.FILE_READ_CMD,
                                                        QIWConstants.ARRAYLIST_TYPE,params,true);
    IQiWorkbenchMsg response = agent.getMessagingMgr().getMatchingResponseWait(msgID,10000);
    if(response == null || MsgUtils.isResponseAbnormal(response)) {
      logger.finest("IO error:"+(String)MsgUtils.getMsgContent(response));
      agent.showErrorDialog(QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                            "Error occured while reading" + path,
                            new String[] {"Permission error"},
                            new String[] {"Verify " + path + "  is readable by you",
                                          "If yes, contact workbench support"});
    }
    else
      script = (ArrayList<String>)MsgUtils.getMsgContent(response);
    return script;
  }

  /**
   * saveScriptToFile saves a script to a selected file
   * @param comp is parent Component for dialogs
   * @param script ArrayList containing script
   * @param path is /path/file to write.
   *   It is inserted as the first element of the ArrayList for IO Services
   * @return true if file written; otherwise, false.
   */
  public boolean saveScriptToFile(Component comp, ArrayList<String> script, String path) {
    //Add the path of the file want to write to to the head of the list
    script.add(0, path);
    //Add the location of the IO to the head of the list
    script.add(0,agent.getMessagingMgr().getLocationPref());
    String msgID = agent.getMessagingMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.FILE_WRITE_CMD,
                                                       QIWConstants.ARRAYLIST_TYPE,script,true);
    // wait for the response. Note: There can only be 1 response since there
    // is a separate IO service performing the IO which is not performing
    // parallel IO.
    IQiWorkbenchMsg response = agent.getMessagingMgr().getMatchingResponseWait(msgID,10000);
    if(response == null || MsgUtils.isResponseAbnormal(response)) {
      logger.finest("IO error:"+(String)MsgUtils.getMsgContent(response));
      agent.showErrorDialog(QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                            "Error occured while writing" + path,
                            new String[] {"Permission error"},
                            new String[] {"Verify " + path + "  is writable by you",
                                          "If yes, contact workbench support"});
      return false;
    }

    return true;
  }

  /** Read the header limits file for a selected dataset.
   * @param comp is parent Component for dialogs
   * @param path is /path/file of file to read
   * @return ArrayList containing header information
   */
  public ArrayList<ArrayList<String>> getHeaderLimits(Component comp, String path) {
    ArrayList<ArrayList<String>> headerLimits = new ArrayList<ArrayList<String>>(2);
    ArrayList keyInfo = new ArrayList<String>(5);
    // call service to get header limits file
    ArrayList<String> params = new ArrayList<String>();
    params.add(agent.getMessagingMgr().getLocationPref());
    params.add(path);
    String msgID = agent.getMessagingMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.FILE_READ_CMD,
                                                       QIWConstants.ARRAYLIST_TYPE,params,true);
    IQiWorkbenchMsg response = agent.getMessagingMgr().getMatchingResponseWait(msgID,10000);
    if (response == null || MsgUtils.isResponseAbnormal(response)) {
      logger.finest("IO error:"+(String)MsgUtils.getMsgContent(response));
      agent.showErrorDialog(QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                            "Error occured while reading" + path,
                            new String[] {"Permission error"},
                            new String[] {"Verify " + path + "  is readable by you",
                                          "If yes, contact workbench support"});
      return headerLimits;
    }
    ArrayList<String> temp = (ArrayList<String>)MsgUtils.getMsgContent(response);
    if(AmpExtConstants.DEBUG_PRINT > 0) {
      for(int i=0; i<temp.size(); i++)
        System.out.println(temp.get(i));
    }
    // parse temp into header limits arrays
    String[] fields;
    if(temp.size() < 10)
      agent.showErrorDialog(QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                            "Error occured while processing" + path,
                            new String[] {"Dataset must have at least two keys"},
                            new String[] {"Use input data with at leasr rwo keys",
                                          "If datset already has two keys, contact workbench support"});
    //NOTE: First line is 'HDRNAME=...' which is a key (either ep or cdp)
    //We are only interested in the 1st 5 attributes: HDRNAME, MIN, MAX, INCR and SCALAR
    //Extract info about the 1st key
    for(int i=0; i<5; i++) {
      fields = temp.get(i).split("=");
      keyInfo.add(fields[1].trim());
    }
    headerLimits.add(keyInfo);
    keyInfo = new ArrayList<String>(5);
    //There may be other attributes separating the keys, e.g., NUM, ORDER, TYPE,
    //DATATYPE, VLEN, VLOC. Scan for the next key.
    int keyidx = 5;
    for (int j=5; j<temp.size(); j++) {
        if (temp.get(j).startsWith("HDRNAME")) {
            keyidx = j;
            break;
        }
    }
    //Extract info about the 2nd key
    for (int i=keyidx; i<keyidx+5; i++) {
      fields = temp.get(i).split("=");
      keyInfo.add(fields[1].trim());
    }
    headerLimits.add(keyInfo);
    if(AmpExtConstants.DEBUG_PRINT > 0) {
      System.out.println("Header Limits Key 1: " + headerLimits.get(0).get(0) + "  " +
                         headerLimits.get(0).get(1) + "  " + headerLimits.get(0).get(2)  + "  " +
                         headerLimits.get(0).get(3)  + "  " +headerLimits.get(0).get(4));
      System.out.println("Header Limits Key 2: " + headerLimits.get(1).get(0) + "  " +
                         headerLimits.get(1).get(1) + "  " + headerLimits.get(1).get(2)  + "  " +
                         headerLimits.get(1).get(3)  + "  " +headerLimits.get(0).get(4));
    }
    return headerLimits;
  }

  public DataSetHeaderKeyInfo getTimeRangeInfo(Component comp, String path) {
	  	String key = "";
	    double min=0, max=0, inc=0;
	    
	    // call service to get header limits file
	    ArrayList<String> params = new ArrayList<String>();
	    params.add(agent.getMessagingMgr().getLocationPref());
	    params.add(path);
	    String msgID = agent.getMessagingMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.FILE_READ_CMD,
	                                                       QIWConstants.ARRAYLIST_TYPE,params,true);
	    IQiWorkbenchMsg response = agent.getMessagingMgr().getMatchingResponseWait(msgID,10000);
	    if (response == null || MsgUtils.isResponseAbnormal(response)) {
	      logger.finest("IO error:"+(String)MsgUtils.getMsgContent(response));
	      agent.showErrorDialog(QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
	                            "Error occured while reading" + path,
	                            new String[] {"Permission error"},
	                            new String[] {"Verify " + path + "  is readable by you",
	                                          "If yes, contact workbench support"});
	      return new DataSetHeaderKeyInfo(key,min,max,inc);
	    }
	    ArrayList<String> temp = (ArrayList<String>)MsgUtils.getMsgContent(response);
	    if(AmpExtConstants.DEBUG_PRINT > 0) {
	      for(int i=0; i<temp.size(); i++)
	        System.out.println(temp.get(i));
	    }
	    // parse temp into header limits arrays
	    String[] fields;
	    if(temp.size() < 10)
	      agent.showErrorDialog(QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
	                            "Error occured while processing" + path,
	                            new String[] {"Dataset must have at least two keys"},
	                            new String[] {"Use input data with at leasr rwo keys",
	                                          "If datset already has two keys, contact workbench support"});
	    for(int i=0; i<temp.size(); i++) {
	    	if(temp.get(i).startsWith("VKEY")){
	    		fields = temp.get(i).split("=");
	    		key = fields[1].trim();
	    	}
	    	if(temp.get(i).startsWith("VMIN")){
	    		fields = temp.get(i).split("=");
	    		min = Double.valueOf(fields[1].trim());
	    	}
	    	if(temp.get(i).startsWith("VMAX")){
	    		fields = temp.get(i).split("=");
	    		max = Double.valueOf(fields[1].trim());
	    	}
	    	if(temp.get(i).startsWith("VINC")){
	    		fields = temp.get(i).split("=");
	    		inc = Double.valueOf(fields[1].trim());
	    	}
	    }
	    DataSetHeaderKeyInfo ds = new DataSetHeaderKeyInfo(key,min,max,inc);
	    return ds;
	  }

}
