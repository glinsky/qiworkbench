/*
###########################################################################
# AmpExt - horizon-based Secant amplitude and area extraction
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.AmplitudeExtraction;

import java.lang.NumberFormatException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ControlConstants;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Finite state machine (FSM) for displaying the results of AmpExt.
 */
public class DisplayTransducer {
    /** Display step */
    private int stepState = ControlConstants.SESSION_STEP;
    /** Step action */
    private int actionState = ControlConstants.START_SESSION;
    MessagingManager messagingMgr;
    // GUI component
    private AmplitudeExtraction gui;
    /** Component descriptor of the qiViewer being controlled */
    ComponentDescriptor qiViewerDesc;

    String sessionID = "";
    //command action
    ArrayList actions = new ArrayList();
    //parameters for multi-action commands
    ArrayList<String> actionParams1 = new ArrayList<String>();
    ArrayList<String> actionParams2 = new ArrayList<String>();
    ArrayList<String> actionParams3 = new ArrayList<String>();
    ArrayList<String> actionParams4 = new ArrayList<String>();
    ArrayList<String> actionParams5 = new ArrayList<String>();
    ArrayList<String> actionParams6 = new ArrayList<String>();

    //Window characteristics: Window ID, size (width, height), position (x, y)
    String xsecWID1 = "", xsecWID2 = "", xsecWID3 = "", xsecWID4 = "";
    String mapWID1 = "", mapWID2 = "";
    int xsecWidth1 = 0, xsecHeight1 = 0, xsecWidth2 = 0, xsecHeight2 = 0,
        xsecWidth3 = 0, xsecHeight3 = 0, xsecWidth4 = 0, xsecHeight4 = 0;
    int mapWidth1 = 0, mapHeight1 = 0, mapWidth2 = 0, mapHeight2 = 0;
    int xsecX1 = 0, xsecY1 = 0, xsecX2 = 0, xsecY2 = 0, xsecX3 = 0,
        xsecY3 = 0, xsecX4 = 0, xsecY4 = 0;
    int mapX1 = 0, mapY1 = 0, mapX2 = 0, mapY2 = 0;
    int viewerWidth = 50, viewerHeight = 50;    //leave room around the edges
    //Note: layer IDs unique across all windows
    String layerID1 = "", layerID2 = "", layerID3 = "", layerID4 = "";
    String layerID5 = "", layerID6 = "";
    String layerPropsID1 = "", layerPropsID2 = "", layerPropsID3 = "";
    String layerPropsID4 = "";
	
	String annotatedLayer1 = "";

    /**
     * Constructor. Initialize the finite state machine.
     * @param gui AmpExt GUI.
     * @param messagingMgr AmpExt Agent's messaging manager.
     * @param stepState The state (display step) in which to start.
     * @param actionState The action within a step in which to start.
     */
    public DisplayTransducer(AmplitudeExtraction gui, MessagingManager messagingMgr, int stepState, int actionState) {
        this.stepState = stepState;
        this.actionState = actionState;
        this.messagingMgr = messagingMgr;
        this.gui = gui;
    }

    /**
     * Process the response message from a CONTROL_QIVIEWER command.
     * The actions in the request are specified by the actionState
     * which is relative to the step (stepState) in the display process.
     * <p>
     * Step state 0, action 0 is start_control_session.
     * <br> Step state 0, action 1 is end_control_session.
     * @param resp The response message.
     */
    public void processResp(IQiWorkbenchMsg resp) {
System.out.println("AmpExt::processResp: response="+resp);
        switch (stepState) {
            case ControlConstants.SESSION_STEP:
                switch (actionState) {
                    case ControlConstants.START_SESSION:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot start a display session.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            ArrayList outs = (ArrayList)resp.getContent();
                            sessionID = (String)outs.get(0);
                            qiViewerDesc = (ComponentDescriptor)outs.get(1);

                            stepState = AmpExtConstants.GET_LAYER_PROPS1;
							actionState = AmpExtConstants.INIT_PROGRESS_DIALOG;
							
							actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.INIT_PROGRESS_DIALOG_ACTION);
							actionParams1.add("title=Display AmpExt Results");
							actionParams1.add("windows=6");
							actionParams1.add("layers="+ ("4,4,1,1,1,1"));
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case ControlConstants.END_SESSION:
                        //Prepare for next control session (in case use the same transducer)
                        xsecWidth1 = 0; xsecHeight1 = 0;
                        xsecWidth2 = 0; xsecHeight2 = 0;
                        xsecWidth3 = 0; xsecHeight3 = 0;
                        xsecWidth4 = 0; xsecHeight4 = 0;
                        mapWidth1 = 0; mapHeight1 = 0;
                        mapWidth2 = 0; mapHeight2 = 0;
                        
                        stepState = ControlConstants.SESSION_STEP;
                        actionState = ControlConstants.START_SESSION;
                        break;

                    default:
                }
                break;
                
            case AmpExtConstants.GET_LAYER_PROPS1:
                switch (actionState) {
					case AmpExtConstants.INIT_PROGRESS_DIALOG:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot initialize the progress dialog", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.GET_LAYER_PROPS;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getInputCubePath());
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
					
                    case AmpExtConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot get layer properties for XSection window #1.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            layerPropsID1 = (String)resp.getContent();

                            actionState = AmpExtConstants.GET_LAYER_PROP;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID1);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.ep");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.GET_LAYER_PROP:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot get input cube's full range.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            String fullRange = (String)resp.getContent();
                            //full range: min-max[incr]
                            String[] fr1 = fullRange.split("-");
                            String [] fr2 = fr1[1].split("\\[");
                            String [] fr3 = fr2[1].split("\\]");
                            double min = 0, max = 0, incr = 1;
                            try {
                                min = Double.parseDouble(fr1[0]);
                                max = Double.parseDouble(fr2[0]);
                                incr = Double.parseDouble(fr3[0]);
                            } catch (NumberFormatException nfe) {
                            }
                            Double midpoint = (max - min)/2;
                            Double chosenRange = min + (midpoint - (midpoint % incr));

                            actionState = AmpExtConstants.SET_LAYER_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID1);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.ep");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID1);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.cdp");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID1);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.ep");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID1);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case AmpExtConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set input cube's parameters.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.SET_LAYER_PROPS2;

                            actions.clear(); actionParams1.clear();

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID1);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=RasterizingType.Wiggle_Trace");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID1);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=RasterizingType.Positive_Fill");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID1);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=RasterizingType.Variable_Density");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID1);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        } break;
                            
                    case AmpExtConstants.SET_LAYER_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set layer display properties.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.CREATE_WINDOW;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID1);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getInputCubePath());
							actionParams1.add("ltype="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        } break;
                        
                    case AmpExtConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot open XSection window #1.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            ArrayList ids = (ArrayList)resp.getContent();
                            xsecWID1 = (String)ids.get(0);
                            layerID1 = (String)ids.get(1);
                           
                            actionState = AmpExtConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=SynchronizeHorizontalScrolling");
                            actionParams1.add("value=off");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID1);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set Synchronization properties.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.SET_ANNO_PROPS ;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams1.add("field=Horizontal.SelectedKeys");
                            actionParams1.add("value=cdp,ep");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID1);
                            actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams2.add("field=Horizontal.SynchronizationKey");
                            actionParams2.add("value=cdp");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID1);
                            actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set Annotation properties.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.ADD_LAYER;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=p1");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case AmpExtConstants.ADD_LAYER:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot add horizon layer p1\n"+resp.getContent(), "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.ADD_LAYER2;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=p2");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.ADD_LAYER2:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot add horizon layer p2\n"+resp.getContent(), "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.ADD_LAYER3;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=p3");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case AmpExtConstants.ADD_LAYER3:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot add horizon layer p3\n"+resp.getContent(), "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.RESIZE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            //determine height and width
                            xsecHeight1 = 400; xsecWidth1 = 400;

                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("width="+xsecWidth1);
                            actionParams1.add("height="+xsecHeight1);
                            actions.add(actionParams1);
                            
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID1);
                            actionParams2.add("direction=All");
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot resize XSection Window #1.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            stepState = AmpExtConstants.GET_LAYER_PROPS2;
                            actionState = AmpExtConstants.GET_LAYER_PROPS;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getInputCubePath());
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

            case AmpExtConstants.GET_LAYER_PROPS2:
                switch (actionState) {
                    case AmpExtConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot get layer properties for XSection window #2.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            layerPropsID2 = (String)resp.getContent();

                            actionState = AmpExtConstants.GET_LAYER_PROP;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID2);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdp");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case AmpExtConstants.GET_LAYER_PROP:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot get input cube's full range.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            String fullRange = (String)resp.getContent();
                            //full range: min-max[incr]
                            String[] fr1 = fullRange.split("-");
                            String [] fr2 = fr1[1].split("\\[");
                            String [] fr3 = fr2[1].split("\\]");
                            double min = 0, max = 0, incr = 1;
                            try {
                                min = Double.parseDouble(fr1[0]);
                                max = Double.parseDouble(fr2[0]);
                                incr = Double.parseDouble(fr3[0]);
                            } catch (NumberFormatException nfe) {
                            }
                            Double midpoint = (max - min)/2;
                            Double chosenRange = min + (midpoint - (midpoint % incr));

                            actionState = AmpExtConstants.SET_LAYER_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID2);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdp");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID2);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.ep");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID2);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.ep");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID2);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case AmpExtConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set input cube's parameters.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.SET_LAYER_PROPS1;

                            actions.clear(); actionParams1.clear();

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID2);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=RasterizingType.Wiggle_Trace");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID2);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=RasterizingType.Positive_Fill");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID2);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=RasterizingType.Variable_Density");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID2);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        } break;
                            
                    case AmpExtConstants.SET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set layer display properties.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            layerPropsID2 = (String)resp.getContent();
                            actionState = AmpExtConstants.CREATE_WINDOW;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID2);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getInputCubePath());
							actionParams1.add("ltype="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        } break;
                        
                    case AmpExtConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot open XSection window #2.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            ArrayList ids = (ArrayList)resp.getContent();
                            xsecWID2 = (String)ids.get(0);
                            layerID2 = (String)ids.get(1);
                            
                            actionState = AmpExtConstants.SET_SYNC_PROPS ;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=SynchronizeHorizontalScrolling");
                            actionParams1.add("value=off");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID2);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set Synchronization properties.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.SET_ANNO_PROPS ;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams1.add("field=Horizontal.SelectedKeys");
                            actionParams1.add("value=cdp,ep");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID2);
                            actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams2.add("field=Horizontal.SynchronizationKey");
                            actionParams2.add("value=ep");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID2);
                            actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set Annotation properties.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.ADD_LAYER;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=p1");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case AmpExtConstants.ADD_LAYER:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot add horizon layer p1\n"+resp.getContent(), "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.ADD_LAYER2;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=p2");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.ADD_LAYER2:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot add horizon layer p2\n"+resp.getContent(), "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.ADD_LAYER3;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=p3");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case AmpExtConstants.ADD_LAYER3:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot add horizon layer p3\n"+resp.getContent(), "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.RESIZE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            //determine height and width
                            xsecHeight2 = 400; xsecWidth2 = 400;
                            //deterimine positon of window next to window #1
                            xsecX2 = xsecWidth1+1; xsecY2 = 0;
                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("width="+xsecWidth2);
                            actionParams1.add("height="+xsecHeight2);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID2);
                            actionParams2.add("x="+xsecX2);
                            actionParams2.add("y="+xsecY2);
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID2);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);


                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot resize XSection Window #1.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            stepState = AmpExtConstants.CREATE_MAP_WIN1;
                            actionState = AmpExtConstants.CREATE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                            actionParams1.add("type=Map");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputMapPath());
							actionParams1.add("ltype="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=amp");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

            case AmpExtConstants.CREATE_MAP_WIN1:
                switch (actionState) {
                    case AmpExtConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot open Map window #1.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            ArrayList ids = (ArrayList)resp.getContent();
                            mapWID1 = (String)ids.get(0);
                            layerID5 = (String)ids.get(1);

                            actionState = AmpExtConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+mapWID1);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=ListenToHorizontalScale");
                            actionParams1.add("value=off");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+mapWID1);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToVerticalScale");
                            actionParams2.add("value=off");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+mapWID1);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToScrollPosition");
                            actionParams3.add("value=off");
                            actions.add(actionParams3);

                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+mapWID1);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=ListenToCursorPosition");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);

                            actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+mapWID1);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams5.add("field=Broadcast");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams6.clear();
                            actionParams6.add("winID="+mapWID1);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams6);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set Synchronization properties.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.SET_SCALE_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+mapWID1);
                            actionParams1.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actionParams1.add("field=LockAspectRatio");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+mapWID1);
                            actionParams2.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_SCALE_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set Scale properties on Map window #1.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.RESIZE_WINDOW;

                            //determine height and widt1
                            mapHeight1 = 400; mapWidth1 = 600;
                            //deterimine positon of window next to window #2
                            mapX1 = xsecX2+xsecWidth2+1; mapY1 = 0;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+mapWID1);
                            //fixed apparent copy-paste bug, the following 2 lines used 'mapWidth2' and 'mapHeight2',
                            //which are 0 at this point
                            actionParams1.add("width="+mapWidth1);
                            actionParams1.add("height="+mapHeight1);
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.add("winID="+mapWID1);
                            actionParams2.add("x="+mapX1);
                            actionParams2.add("y="+mapY1);
                            actions.add(actionParams2);
                            
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+mapWID1);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot resize Map Window #1.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            stepState = AmpExtConstants.CREATE_MAP_WIN2;
                            actionState = AmpExtConstants.CREATE_WINDOW;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                            actionParams1.add("type=Map");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputMapPath());
							actionParams1.add("ltype="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=p3");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

            case AmpExtConstants.CREATE_MAP_WIN2:
                switch (actionState) {
                    case AmpExtConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot open Map window #2.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            ArrayList ids = (ArrayList)resp.getContent();
                            mapWID2 = (String)ids.get(0);
                            layerID6 = (String)ids.get(1);

                            actionState = AmpExtConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+mapWID2);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=ListenToHorizontalScale");
                            actionParams1.add("value=off");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+mapWID2);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToVerticalScale");
                            actionParams2.add("value=off");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+mapWID2);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToScrollPosition");
                            actionParams3.add("value=off");
                            actions.add(actionParams3);

                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+mapWID2);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=ListenToCursorPosition");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);

                            actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+mapWID2);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams5.add("field=Broadcast");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams6.clear();
                            actionParams6.add("winID="+mapWID2);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams6);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set Synchronization properties for Map window #2.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.SET_SCALE_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+mapWID2);
                            actionParams1.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actionParams1.add("field=LockAspectRatio");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+mapWID2);
                            actionParams2.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actions.add(actionParams2);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_SCALE_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set Scale properties for Map window #2.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.RESIZE_WINDOW;

                            //determine height and widt1
                            mapHeight2 = 400; mapWidth2 = 600;
                            //deterimine positon of window under map window #1
                            mapX2 = xsecX2+xsecWidth2+1; mapY2 = mapY1+mapHeight1+1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+mapWID2);
                            actionParams1.add("width="+mapWidth2);
                            actionParams1.add("height="+mapHeight2);
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.add("winID="+mapWID2);
                            actionParams2.add("x="+mapX2);
                            actionParams2.add("y="+mapY2);
                            actions.add(actionParams2);
                            
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+mapWID2);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot resize Map window #2.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            stepState = AmpExtConstants.GET_LAYER_PROPS3;
                            actionState = AmpExtConstants.GET_LAYER_PROPS;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

            case AmpExtConstants.GET_LAYER_PROPS3:
                switch (actionState) {
                    case AmpExtConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot get layer properties for XSection window #3.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            layerPropsID3 = (String)resp.getContent();

                            actionState = AmpExtConstants.GET_LAYER_PROP;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID3);
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.ep");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case AmpExtConstants.GET_LAYER_PROP:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot get output cube's full range.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            String fullRange = (String)resp.getContent();
                            //full range: min-max[incr]
                            String[] fr1 = fullRange.split("-");
                            String [] fr2 = fr1[1].split("\\[");
                            String [] fr3 = fr2[1].split("\\]");
                            double min = 0, max = 0, incr = 1;
                            try {
                                min = Double.parseDouble(fr1[0]);
                                max = Double.parseDouble(fr2[0]);
                                incr = Double.parseDouble(fr3[0]);
                            } catch (NumberFormatException nfe) {
                            }
                            Double midpoint = (max - min)/2;
                            Double chosenRange = min + (midpoint - (midpoint % incr));

                            actionState = AmpExtConstants.SET_LAYER_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID3);
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.ep");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID3);
                            actionParams2.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.cdp");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID3);
                            actionParams3.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.ep");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID3);
                            actionParams4.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams4.add("field=HorizonSetting");
                            actionParams4.add("value=amp");
                            actions.add(actionParams4);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID3);
                            actionParams5.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case AmpExtConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set output cube's parameters.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            layerPropsID3 = (String)resp.getContent();
                            actionState = AmpExtConstants.CREATE_WINDOW;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID3);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
							actionParams1.add("ltype="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=amp");
                            actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        } break;
                        
                    case AmpExtConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot open XSection window #3.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            ArrayList ids = (ArrayList)resp.getContent();
                            xsecWID3 = (String)ids.get(0);
                            layerID3 = annotatedLayer1 = (String)ids.get(1);
                            
                            actionState = AmpExtConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=ListenToHorizontalScale");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID3);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToVerticalScale");
                            actionParams2.add("value=off");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID3);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToScrollPosition");
                            actionParams3.add("value=off");
                            actions.add(actionParams3);

                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID3);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=ListenToCursorPosition");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);

                            actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+xsecWID3);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams5.add("field=Broadcast");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams6.clear();
                            actionParams6.add("winID="+xsecWID3);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams6);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                   case AmpExtConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set Synchronization properties for XSection window #3.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.SET_ANNO_PROPS ;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams1.add("field=Horizontal.SelectedKeys");
                            actionParams1.add("value=cdp,ep");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID3);
                            actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams2.add("field=Horizontal.SynchronizationKey");
                            actionParams2.add("value=cdp");
                            actions.add(actionParams2);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID3);
                            actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams3.add("field=AnnotatedLayer");
                            actionParams3.add("value="+annotatedLayer1);
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("winID="+xsecWID3);
                            actionParams4.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set Annotation properties.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.RESIZE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            //determine height and width
                            xsecHeight3 = 200; xsecWidth3 = 400;
                            //deterimine positon of window under XSection window #1
                            xsecX3 = xsecX1; xsecY3 = xsecY1 + xsecHeight1 + 1;

                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("width="+xsecWidth3);
                            actionParams1.add("height="+xsecHeight3);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID3);
                            actionParams2.add("x="+xsecX3);
                            actionParams2.add("y="+xsecY3);
                            actions.add(actionParams2);
                            
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID3);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot resize XSection Window #3.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            stepState = AmpExtConstants.GET_LAYER_PROPS4;
                            actionState = AmpExtConstants.GET_LAYER_PROPS;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

            case AmpExtConstants.GET_LAYER_PROPS4:
                switch (actionState) {
                    case AmpExtConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot get layer properties for XSection window #4.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            layerPropsID4 = (String)resp.getContent();

                            actionState = AmpExtConstants.GET_LAYER_PROP;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID4);
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=FullRange.cdp");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case AmpExtConstants.GET_LAYER_PROP:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot get output cube's full range.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            String fullRange = (String)resp.getContent();
                            //full range: min-max[incr]
                            String[] fr1 = fullRange.split("-");
                            String [] fr2 = fr1[1].split("\\[");
                            String [] fr3 = fr2[1].split("\\]");
                            double min = 0, max = 0, incr = 1;
                            try {
                                min = Double.parseDouble(fr1[0]);
                                max = Double.parseDouble(fr2[0]);
                                incr = Double.parseDouble(fr3[0]);
                            } catch (NumberFormatException nfe) {
                            }
                            Double midpoint = (max - min)/2;
                            Double chosenRange = min + (midpoint - (midpoint % incr));

                            actionState = AmpExtConstants.SET_LAYER_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID4);
                            actionParams1.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.cdp");
                            actionParams1.add("value="+chosenRange);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID4);
                            actionParams2.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams2.add("field=ChosenRange.ep");
                            actionParams2.add("value=*");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID4);
                            actionParams3.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams3.add("field=Synchronize.cdp");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID4);
                            actionParams4.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams4.add("field=HorizonSetting");
                            actionParams4.add("value=amp");
                            actions.add(actionParams4);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID4);
                            actionParams5.add("type="+ControlConstants.HORIZON_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case AmpExtConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set output cube's parameters.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            layerPropsID4 = (String)resp.getContent();
                            actionState = AmpExtConstants.CREATE_WINDOW;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID4);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+gui.getOutputCubePath());
							actionParams1.add("ltype="+ControlConstants.HORIZON_LAYER);
							actionParams1.add("horizon=amp");
                            actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        } break;
                        
                    case AmpExtConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot open XSection window #3.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            ArrayList ids = (ArrayList)resp.getContent();
                            xsecWID4 = (String)ids.get(0);
                            layerID4 = annotatedLayer1 = (String)ids.get(1);
                            
                            actionState = AmpExtConstants.SET_SYNC_PROPS;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID4);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=ListenToHorizontalScale");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID4);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToVerticalScale");
                            actionParams2.add("value=off");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID4);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToScrollPosition");
                            actionParams3.add("value=off");
                            actions.add(actionParams3);

                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID4);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=ListenToCursorPosition");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);

                            actionParams5.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams5.add("winID="+xsecWID4);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams5.add("field=Broadcast");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams6.clear();
                            actionParams6.add("winID="+xsecWID4);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams6);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set Synchronization properties for XSection window #4.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.SET_ANNO_PROPS ;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID4);
                            actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams1.add("field=Horizontal.SelectedKeys");
                            actionParams1.add("value=cdp,ep");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID4);
                            actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams2.add("field=Horizontal.SynchronizationKey");
                            actionParams2.add("value=ep");
                            actions.add(actionParams2);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID4);
                            actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams3.add("field=AnnotatedLayer");
                            actionParams3.add("value="+annotatedLayer1);
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams4.clear();
                            actionParams4.add("winID="+xsecWID4);
                            actionParams4.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot set Annotation properties for XSection window #4.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            actionState = AmpExtConstants.RESIZE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            //determine height and width
                            xsecHeight4 = 200; xsecWidth4 = 400;
                            //deterimine positon of window under XSection window #2
                            xsecX4 = xsecX2; xsecY4 = xsecY2 + xsecHeight2 + 1;

                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+xsecWID4);
                            actionParams1.add("width="+xsecWidth4);
                            actionParams1.add("height="+xsecHeight4);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID4);
                            actionParams2.add("x="+xsecX4);
                            actionParams2.add("y="+xsecY4);
                            actions.add(actionParams2);
                            
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID4);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.RESIZE_VIEWER_ACTION);
                            actionParams4.clear();

                            int w1 = xsecWidth1 + xsecWidth2 + mapWidth1;
                            int w2 = xsecWidth3 + xsecWidth4 + mapWidth2;
                            viewerWidth += w1>w2 ? w1 : w2;

                            int h1 = xsecY3 + xsecHeight3;
                            int h2 = xsecY4 + xsecHeight4;
                            int h3 = mapY2 + mapHeight2;
                            int maxh = h1>h2 ? h1 : h2;
                            viewerHeight += maxh>h3 ? maxh : h3;

                            actionParams4.add("width="+viewerWidth);
                            actionParams4.add("height="+viewerHeight);
                            actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case AmpExtConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot resize XSection Window #4.", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
							actionState = AmpExtConstants.END_PROGRESS_DIALOG;
							
							actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.END_PROGRESS_DIALOG_ACTION);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case AmpExtConstants.END_PROGRESS_DIALOG:
                        if (resp.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(gui, "Cannot end process dialog", "Display Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            stepState = ControlConstants.SESSION_STEP;
                            actionState = ControlConstants.END_SESSION;

                            actions.clear(); actionParams1.clear();
                            //End the control session
                            actions.add("action="+ControlConstants.END_CONTROL_SESSION_ACTION);
                            actionParams1.add("sessionID="+sessionID);
                            actions.add(actionParams1);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End the control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

            default:
                //Internal error: No legal step transition state
                //End the control session
                endControlSession("Internal Error: No legal step transition for state "+actionState);
        }
    }
	
	/**
	 * End the control session because of an error.
	 * @param errorMsg The reason why the control session is being terminated.
	 */
	private void endControlSession(String errorMsg) {
		//Display reason ending the control session
		JOptionPane.showMessageDialog(gui, errorMsg+"\nCannot continue displaying the results.", "Display Error", JOptionPane.ERROR_MESSAGE);
		
		//End the control session
        stepState = ControlConstants.SESSION_STEP;
        actionState = ControlConstants.END_SESSION;
							
		actions.clear(); actionParams1.clear();
		actions.add("action="+ControlConstants.END_CONTROL_SESSION_ACTION);
		actionParams1.add("sessionID="+sessionID);
		actions.add(actionParams1);
		
		messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
	}
}
