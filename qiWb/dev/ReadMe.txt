ReadMe for qiWorkbench 1.4
============================

For details on what is new in this release, read the Release Notes
available from the User's Corner > Install/Update Webpage at
http://www.qiworkbench.org

SYSTEM REQUIREMENTS

1. Install the latest Java Runtime Environment (JRE) 5.0 which includes
WebStart on each end-users machine and the server that will house the
Tomcat server to which the qiWorkbench will be deployed. The JRE is
available from http://java.sun.com/j2se/1.5/; follow the installation
instructions. For a Linux machine, download the 32-bit version because
the 64-bit version does not (yet) include WebStart.

Starting with the 1.3 production release, it is no longer recommended a
runtime Tomcat server be installed on end-user machines. Instead, users
should use a Primary Update Center (PUC) where the qiWorkbench is
deployed as the runtime Tomcat server. However, a developer or user may want to
set up a Tomcat on their machine, manually install the qiWorkbench (see below)
and launch the qiWorkbench from http://localhost:8080. They would follow the
Manual Installation Instructions given below.

The next step pertains to a Linux or Mac OS X platform. The qiWorkbench is
written in Java and will run on Windows platforms. However, some qiComponents
generate and execute scripts that call BHP SU and Seismic Unix commands.
These scripts will not execute on a Windows platform. There is a commercial
version of Seismic Unix for Windows, but BHP SU, which is written in C, has
not been ported to Windows. Other qiComponents, such as the bhpViewer and the
XML Editor, do not generate such scripts and will run on Windows.

* Install the qiApps per the installation instructions in the README file. The
qiApps includes BHP-SU and Seismic Unix for a 32-bit Unix OS. The qiApps TAR
file is available at http://www.qiworkbench.org/qiapps. Download the latest
qi_apps_dist_*.tgz.

AUTO INSTALLATION INSTRUCTIONS (Users)

Starting with the 1.3 production release, it is no longer recommended to
manually install the qiWorkbench and its qiComponents on end-user machines.
Instead, they are installed automatically from a Primary Update Center (PUC)
where the workbench and its components are deployed. One can use the public PUC
at http://www.qiworkbench.org/qiWorkbench or set up a local PUC. For the latter,
see the document "How to set up a qiWorkbench Deployment Update Center".

To use the public PUC enter the URL http://www.qiworkbench.org/qiWorkbench in your
browser and in the displayed Webpage, Step 2, click on the 'with auto component updates'
link. The qiWorkbench will be installed and launched by Java WebStart. The workbench
will create the .qiworkbench/qicomponents/ directory in the end-user's home directory,
and populate it with the available external components.

When the workbench is launched, the user is asked to specify a runtime Tomcat
server. This may be the PUC if it can access project files; otherwise, it must
be another machine or the user's machine on which Tomcat has been installed.

MANUAL INSTALLATION INSTRUCTIONS (Developers)

1. Install the Tomcat server on the machine to which the qiWorkbench will
be deployed. The latest version of Tomcat 5.X is available from
http://tomcat.apache.org. This Tomcat server is referred to as the deployment
Tomcat server in the documentation. It can also be used as the runtime Tomcat
server.

2. Download the qiWorkbench WAR file and deploy it to your Tomcat server's
webapps subdirectory. The WAR file is available from the Developer's Corner >
Resources/Download Webpage at http://www.qiworkbench.org.

3. Download the binary bundles for the available external qiComponents and
manually install (unzip) them in the .qiworkbench/qicomponents/ directory of your
home directory. This directory is automatically created the first time the qiWorkbench
is installed and started. Available external qiComponents can be downloaded
from the Developer's Corner > Resources/Download Webpage at  http://www.qiworkbench.org.

UPDATE CENTER SETUP

Documentation on how to set up an update center can be found at Developer's
Corner > Documentation.

DOCUMENTATION

There are two sets of documentation, one for a developer and one for an
end-user. Both sets are available at http://www.qiworkbench.org. The former
can be obtained from the Developer's Corner > Documentation Webpage;
the latter can be obtained from the User's Corner > Documentation.
The Developers Documentation includes the qiWorkbench's JavaDoc API.

COMPONENT DEVELOPMENT

Documentation on how to develop a qiComponent is part of the Developer
Documentation.

Development requires the qiwbCompAPI JAR file that matches the target release.
It is available from the Developer's Corner > Resources/Downloads Webpage.

LAUNCHING THE WORKBENCH

If the qiWorkbench was installed from an Update Center, it can always be
launched by navigating to the Update Center and click on the links in the
qiWorkbench's main Webpage. One link is without auto component updates, the
other is with it.

If using your own deployment Tomcat server on which the qiWorkbench WAR file
has been deployed and then started (via 'startup.sh' on aLinux / Mac OS X
platform or 'startup' on a Windows platform in the Tomcat bin directory),
users just need to enter the URL of the Tomcat server followed by /qiWorkbench
in their browser's address bar and follow the instructions in the rendered Webpage.
Java WebStart will install (or update) the qiWorkbench on their machine and then
launch it. If the deployment Tomcat server is the end-user's machine, the URL to
enter is http://localhost:8080/qiWorkbench. NOTE: The qiComponents must be
manually installed unless the deployment Tomcat server is also made into an Update
Center. How to do this is documented in "How to set up a qiWorkbench Deployment
Update Center" available from the Developer's Corner > Resources/Downloads Webpage.

Note: A deployment Tomcat server, i.e., PUC, can also be used as a runtime Tomcat
server if it can access project files. The public PUC fails this criteria.

LICENSES

The qiWorkbench is open-source software under the GPLv2 license except
for the qiwbCompAPI classes required to develop a qiComponent that are
under the BSD license. The text of these licenses is available from the
User and Developer download Webpages at http://www.qiworkbench.org.

CONTACT INFORMATION

Queries about the qiWorkbench can be sent to info@qiworkbench.org

April 2009
