/*
 * MessageDispatcher_getResponseWait_Timeout_And_Response_Test.java
 *
 * Created on November 14, 2007, 10:02 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.qiworkbench.messageFramework;

import com.bhpb.qiworkbench.IqiMessageHandler;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.testutils.MockQiComponent;
import com.bhpb.testutils.QiWorkbenchProxyFactory;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 *
 * @author folsw9
 */
public class MessageDispatcher_getResponseWait_Timeout_And_Response_Test extends TestCase {
    QiWorkbenchProxyFactory factory;
    MockQiComponent mock1;
    MockQiComponent mock2;
    IqiMessageHandler comp1;
    IqiMessageHandler comp2;
    
    private static final Logger logger = Logger.getLogger(MessageDispatcher_getResponseWait_Timeout_And_Response_Test.class.toString());
    
    @Override
    public void setUp() throws NoSuchMethodException, InterruptedException {
        factory = new QiWorkbenchProxyFactory();
        assertNotNull(factory);
        
        setUpMessageDispatcher();
        mock1 = setUpMockQiComponent();
        mock2 = setUpMockQiComponent();
        
        comp1 = setUpProxy(mock1);
        comp2 = setUpProxy(mock2);
    }
    
    public MessageDispatcher_getResponseWait_Timeout_And_Response_Test() {
    }
    
    private void setUpMessageDispatcher() {
        System.setProperty("deployServerURL","http://localhost:8080");   
        System.setProperty("install","false");
        
        MessageDispatcher.mainNoGUI();
        
        MessageDispatcher dispatcher  = MessageDispatcher.getInstance();
        assertTrue(dispatcher.isInitSuccessful());
    }
    
    private MockQiComponent setUpMockQiComponent()  {
        MockQiComponent mock = new MockQiComponent();
        assertNotNull(mock);
        mock.isInitFinished(); // test mock functionality
        
        return mock;
    }
    
    private IqiMessageHandler setUpProxy(IqiMessageHandler component) throws NoSuchMethodException {
        Object proxy = factory.createProxy(component);
        assertNotNull(proxy);
        
        IqiMessageHandler proxyComponent = (IqiMessageHandler) proxy;
        proxyComponent.isInitFinished(); // test proxy invocation of mock functionality
        
        return (IqiMessageHandler) proxyComponent;
    }
    
    public void testMessageDispatcher_getResponseWait_Timeout_And_Response() throws InterruptedException, NoSuchMethodException {
        mock1.startComponent("MOCK1");
        mock2.startComponent("MOCK2");
        
        Thread.sleep(2000);

        //TODO this should either get the same lock as MockQiComponent.init() or else getCID() should be synchronized on that lock
        logger.info("Mock1.getCID() = " + comp1.getCID());
        logger.info("Mock2.getCID() = " + comp2.getCID());
        
        IComponentDescriptor cd1 = MessageDispatcher.getInstance().getComponentDesc(comp1.getCID());
        IComponentDescriptor cd2 = MessageDispatcher.getInstance().getComponentDesc(comp2.getCID());
        
        assertNotNull("md.getComponentDesc() returned null comp descriptor for CID: " +comp1.getCID(), cd1);
        assertNotNull("md.getComponentDesc() returned null comp descriptor for CID: " +comp2.getCID(), cd2);
            
        //QiWorkbenchMsg fastSynchronousMsg = new QiWorkbenchMsg(comp1.getCID(), comp2.getCID(), "CMD", 
        //        "SEND_TEST_RESPONSE", "FAST_TEST_MSG",  QIWConstants.STRING_TYPE, "Test Content", true);
        //mock1.getMessagingMgr().routeMsg(fastSynchronousMsg);
        //    public String sendRequest(String routing, String command, ComponentDescriptor targetDesc, String contentType, Object content, boolean skip) {

        String fastMsgID = mock1.getMessagingMgr().sendRequest("CMD", 
                 "SEND_TEST_RESPONSE", cd2, QIWConstants.STRING_TYPE, "Test Content", true);
        IQiWorkbenchMsg fastResponse = mock1.getMessagingMgr().getMatchingResponseWait(fastMsgID, 5000);
        assertTrue("Response from fast test msg '" + fastMsgID + "' should not have been null after 5 seconds", fastResponse != null);

        //Now, add a delay to comp2's processMsg method and try again
        factory.addDelay(comp2, 6000, "processMsg", IQiWorkbenchMsg.class);  // add 6 second delay to invocation of isInitFinished()
        
        //required to invoke proxy behavior because processMsg() is not called through the IqiMessageHandler interface,
        //but the component's processMsg method is invoked directly from its own run() method!
        mock2.setProcessorDelegate(comp2);
        
        //QiWorkbenchMsg slowSynchronousMsg = new QiWorkbenchMsg(comp1.getCID(), comp2.getCID(), "CMD", 
        //        "SEND_TEST_RESPONSE", "SLOW_TEST_MSG",  QIWConstants.STRING_TYPE, "Test Content", true);
        
        assertTrue("Message queue should be empty before slow synchronous message test, but it was not",mock1.getMessagingMgr().isMsgQueueEmpty());
        String slowMsgID = mock1.getMessagingMgr().sendRequest("CMD", 
                 "SEND_TEST_RESPONSE", cd2, QIWConstants.STRING_TYPE, "Test Content", true);
       
        //mock1.getMessagingMgr().routeMsg(slowSynchronousMsg);        
        IQiWorkbenchMsg slowResponse = mock1.getMessagingMgr().getMatchingResponseWait(slowMsgID, 5000);
        assertTrue("Response from slow test msg " + slowMsgID + " should have been null after 5 seconds due to 6 second proxy delay, but was non-null.", slowResponse == null);
        
        Thread.sleep(3000);
        
        assertTrue("Message queue should be empty if the late response was discarded, but it was not",mock1.getMessagingMgr().isMsgQueueEmpty());
        
    }
}