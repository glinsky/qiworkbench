/*
 * MessageDispatcherTest.java
 * JUnit based test
 *
 * Created on September 19, 2007, 9:47 AM
 */

package com.bhpb.qiworkbench.messageFramework;

import junit.framework.TestCase;

/**
 *
 * @author folsw9
 */
public class MessageDispatcherTest extends TestCase {
    
    public MessageDispatcherTest(String testName) {
        super(testName);
    }

    /**
     * Invokes MessageDispatcher.main.  Relies on MessageDispatcher being a singleton to shut down the created instance.
     */
    public void testInitMessageDispatcherNoGUIsuccessful() throws Exception {
        System.setProperty("deployServerURL","http://localhost:8080");   
        System.setProperty("install","false");
        
        MessageDispatcher.mainNoGUI();
        
        MessageDispatcher dispatcher  = MessageDispatcher.getInstance();
        assertTrue(dispatcher.isInitSuccessful());
    }
}
