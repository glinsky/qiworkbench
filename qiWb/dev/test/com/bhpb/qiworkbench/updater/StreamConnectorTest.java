/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiworkbench.updater;

import com.bhpb.qiworkbench.util.StreamConnector;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Logger;
import junit.framework.*;

public class StreamConnectorTest extends TestCase {
   private static final Logger logger = Logger.getLogger(StreamConnectorTest.class.toString());
    private static final String TEST_URL_ROOT = "http://localhost:8080/qiWorkbench/";
    
    private static final String GOOD_SOURCE_FILE = "index.jsp";
    private static final String BAD_SOURCE_FILE = "NON_EXISTANT_FILE";
    
    private static final String GOOD_DESTINATION_DIRECTORY = "valid_temp_directory";
    private static final String GOOD_DESTINATION_FILE = GOOD_SOURCE_FILE;
    
    private static final String BAD_DESTINATION_DIRECTORY = "NON_EXISTANT_DIRECTORY";
    
    private static final String STREAM_CONNECTOR_TEST_DIR = GOOD_DESTINATION_DIRECTORY + "_streamConnectorTest";
    private static final String SEQUENTIAL_STREAM_CONNECTOR_TEST_DIR = GOOD_DESTINATION_DIRECTORY + "_sequentialStreamConnectorTest";
    private static final String PARALLEL_STREAM_CONNECTOR_TEST_DIR = GOOD_DESTINATION_DIRECTORY + "_parallelStreamConnectorCopyTest";
    
    public StreamConnectorTest(String testName) {
        super(testName);
    }

    protected void setUp() throws Exception {
        logger.info("Deleting existing directories, if any, needed for use as temporary directories by this test case...");
        deleteAllTempDirectories();
    }

    protected void tearDown() throws Exception {
        logger.info("Deleting any temporary directories, if any, left over due to test case errors...");
        deleteAllTempDirectories();
    } 
    
    private void deleteAllTempDirectories() {
        deleteDirectory(STREAM_CONNECTOR_TEST_DIR);
        deleteDirectory(SEQUENTIAL_STREAM_CONNECTOR_TEST_DIR);
        deleteDirectory(PARALLEL_STREAM_CONNECTOR_TEST_DIR);
    }
    
    private void deleteDirectory(String dirName) {
        File tempDirectory = new File(dirName);
        if (tempDirectory.exists()) {
            logger.info("Deleting temp directory: " + dirName);
            if (tempDirectory.delete() == false) {
                logger.info("Unable to delete temp directory: " + dirName);
            }
        }    
    }
    
    public void testStreamConnectorCopy() throws IOException {
         File tempDirectory = createDirectory(STREAM_CONNECTOR_TEST_DIR);
         
         String tempFileName = tempDirectory + System.getProperty("file.separator") + GOOD_DESTINATION_FILE;
         File tempOutFile = new File(tempFileName);
         URL url = new URL(TEST_URL_ROOT + GOOD_SOURCE_FILE);
         URLConnection connection = url.openConnection();
         BufferedInputStream inStream = new BufferedInputStream(connection.getInputStream());
         BufferedOutputStream outStream = new BufferedOutputStream(new FileOutputStream(tempOutFile));

         StreamConnector streamConnector = new StreamConnector(inStream, outStream, connection.getContentLength());
         
         Thread worker = new Thread(streamConnector);
         worker.start();
         while(streamConnector.isCopyCompleted() == false) {
             Thread.yield();
         }
         
         assertTrue("streamConnector.isNormalTermination() == false", streamConnector.isNormalTermination() == true);
         
         if (inStream != null)
             inStream.close();
         
         if (outStream != null)
             outStream.close();
         
         if (deleteFile(tempFileName) == false)
             fail("Could not delete file: " + tempFileName);
         
         if (deleteDirectory(tempDirectory) == false)
             fail("Could not delete directory: " + tempDirectory);
    }
    
    public void testSequentialCopiesFromURL() throws IOException {      
        File tempDirectory = createDirectory(SEQUENTIAL_STREAM_CONNECTOR_TEST_DIR);        
         
        String tempFileName1 = tempDirectory + System.getProperty("file.separator") + GOOD_DESTINATION_FILE;
        File tempOutFile1 = new File(tempFileName1);
        URL url = new URL(TEST_URL_ROOT + GOOD_SOURCE_FILE);
        URLConnection connection1 = url.openConnection();    
        
        BufferedInputStream inStream1 = new BufferedInputStream(connection1.getInputStream());
        BufferedOutputStream outStream1 = new BufferedOutputStream(new FileOutputStream(tempOutFile1));

        try {         
           StreamConnector streamConnector1 = new StreamConnector(inStream1, outStream1, connection1.getContentLength());
           Thread worker = new Thread(streamConnector1);
           worker.start();
           while(streamConnector1.isCopyCompleted() == false) {
               Thread.yield();
           }
           assertTrue("streamConnector1.isNormalTermination() == false", streamConnector1.isNormalTermination() == true);
        } finally {
           if (inStream1 != null)
               inStream1.close();
         
           if (outStream1 != null)
               outStream1.close();
        }    
         
        String tempFileName2 = tempDirectory + System.getProperty("file.separator") + GOOD_DESTINATION_FILE + "2";
        File tempOutFile2 = new File(tempFileName2);    
        URLConnection connection2 = url.openConnection(); 
        
        BufferedInputStream inStream2 = new BufferedInputStream(connection2.getInputStream());
        BufferedOutputStream outStream2 = new BufferedOutputStream(new FileOutputStream(tempOutFile2));
        try { 
            StreamConnector streamConnector2 = new StreamConnector(inStream2, outStream2, connection2.getContentLength());
            Thread worker2 = new Thread(streamConnector2);
            worker2.start();
            while(streamConnector2.isCopyCompleted() == false) {
                Thread.yield();
            }
            assertTrue("streamConnector2.isNormalTermination() == false", streamConnector2.isNormalTermination() == true);
        } finally {
            if (inStream2 != null)
                inStream2.close();
         
            if (outStream2 != null)
                outStream2.close();
        }
        
        //Test fix for counter not being set to 0 each call to the UpdateWizard's copy method - causes second and subsequent copies to be 0 length'
        if (tempOutFile2.length() == 0)
            fail("Second streamConnector created 0-length file");
         
        if (deleteFile(tempFileName1) == false)
            fail("Could not delete file: " + tempFileName1);
         
        if (deleteFile(tempFileName2) == false)
            fail("Could not delete file: " + tempFileName2);
         
        if (deleteDirectory(tempDirectory) == false)
            fail("Could not delete directory: " + tempDirectory);
    }
    
    public void testParallelCopiesFromURL() throws IOException {
         File tempDirectory = createDirectory(PARALLEL_STREAM_CONNECTOR_TEST_DIR);        
         
         String tempFileName1 = tempDirectory + System.getProperty("file.separator") + GOOD_DESTINATION_FILE;
         File tempOutFile1 = new File(tempFileName1);
         
         URL url = new URL(TEST_URL_ROOT + GOOD_SOURCE_FILE);
         URLConnection connection1 = url.openConnection();    
        
         BufferedInputStream inStream1 = new BufferedInputStream(connection1.getInputStream());
         BufferedOutputStream outStream1 = new BufferedOutputStream(new FileOutputStream(tempOutFile1));
                  
         String tempFileName2 = tempDirectory + System.getProperty("file.separator") + GOOD_DESTINATION_FILE + "2";
         File tempOutFile2 = new File(tempFileName2);

         URLConnection connection2 = url.openConnection(); 
         BufferedInputStream inStream2 = new BufferedInputStream(connection2.getInputStream());
         BufferedOutputStream outStream2 = new BufferedOutputStream(new FileOutputStream(tempOutFile2));
         
         try {
            StreamConnector streamConnector1 = new StreamConnector(inStream1, outStream1, connection1.getContentLength());
            Thread worker = new Thread(streamConnector1);
         
            StreamConnector streamConnector2 = new StreamConnector(inStream2, outStream2, connection2.getContentLength());
            Thread worker2 = new Thread(streamConnector2);
         
            worker.start();
            worker2.start();
         
            while((streamConnector1.isCopyCompleted() == false) || (streamConnector2.isCopyCompleted() == false)) {
                Thread.yield();
            }
            
            assertTrue("streamConnector1 terminated abnormally", streamConnector1.isNormalTermination() == true);
            assertTrue("streamConnector2 terminated abnormally", streamConnector2.isNormalTermination() == true);
         } finally {
            if (inStream1 != null)
                inStream1.close();
         
            if (outStream1 != null)
                outStream1.close();
         
            if (inStream2 != null)
                inStream2.close();
         
            if (outStream2 != null)
                outStream2.close();
         }
         
         //Test fix for counter not being set to 0 each call to the UpdateWizard's copy method - causes second and subsequent copies to be 0 length'
         if ((tempOutFile1.length() == 0) || (tempOutFile2.length() == 0))
             fail("Output file 1 or 2 has length 0");
         
         if (deleteFile(tempFileName1) == false)
             fail("Could not delete file: " + tempFileName1);
         if (deleteFile(tempFileName2) == false)
             fail("Could not delete file: " + tempFileName2);
         
         if (deleteDirectory(tempDirectory) == false)
             fail("Could not delete directory: " + tempDirectory);
    }
    
    /**
     * Create a directory with the specified name
     *
     * @throws IllegalArgumentException if dirName is null or specifies a file or directory that already exists that already exists.
     *
     * @return File representing the new directory.
     */
    private File createDirectory(String dirName) throws IllegalArgumentException {
        if (dirName == null)
            throw new IllegalArgumentException("Cannot create directory given a NULL name");
       
        File newDirectory = new File(dirName);
        
        if (newDirectory.exists()) {
            throw new IllegalArgumentException("Cannot create directory " + dirName + ", it already exists");
        }
        
        newDirectory.mkdir();
        return newDirectory;
    }
    
    /**
     * Delete the specified file if it exists and is a directory.
     *
     * @throws IllegalArgumentException if directory is null, non-existant or not a directory
     */
    private boolean deleteDirectory(File directory) throws IllegalArgumentException {
        
        if (directory == null)
            throw new IllegalArgumentException("Cannot delete directory - argument is NULL");
        
        if (directory.exists()) {
            if (directory.isDirectory()) {
                return directory.delete();
            } else {
                throw new IllegalArgumentException("Cannot delete directory " + directory + ", it is not a directory");
            }
        } else {
            throw new IllegalArgumentException("Cannot delete directory " + directory + ", it does not exist");
        }
    }
    
    /**
     * Delete the specified file if it exists and is not a directory.
     *
     * @throws IllegalArgumentException if file is null, non-existant or is directory
     */
    private boolean deleteFile(String filename) throws IllegalArgumentException {
        
        File file = new File(filename);
        
        if (file == null)
            throw new IllegalArgumentException("Cannot delete file - argument is NULL");
        
        if (file.exists()) {
            if (file.isDirectory()) {
                throw new IllegalArgumentException("Cannot delete file " + file + ", it is a directory");
            } else {
                return file.delete();
            }
        } else {
            throw new IllegalArgumentException("Cannot delete file " + file + ", it does not exist");
        }
    }
}
