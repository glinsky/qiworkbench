/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2007-2008  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench.util;

import java.io.File;
import java.io.IOException;
import junit.framework.TestCase;

public class CommandLineJobImplTest extends TestCase {

    @Override
    protected void setUp() throws Exception {
    }

    @Override
    protected void tearDown() throws Exception {
    }

    private String getPath() {
        return System.getProperty("basedir") + File.separator + "test" + File.separator + "data" + File.separator;
    }

    public void testGetProcessInfoJobStarted() throws IOException {
        CommandLineJob topJob = new CommandLineJobImpl(ClientCapabilityDetector.LINUX_CMD);
        ClientCapabilityDetector.PLATFORM platform = ClientCapabilityDetector.getPlatformByOsName("Linux");
        ClientCapabilityDetector.parsePhysicalMemoryFromStdout(topJob, platform);
        String procInfo = topJob.getProcInfo();
        assertTrue("Expected: 'Commandline job exit value: ' but actual result of mockJob.getProcInfo() was: "
                +procInfo,
                procInfo.startsWith("Commandline job exit value: "));
    }

    public void testGetProcessInfoJobNotStarted() throws IOException {
        CommandLineJob topJob = new CommandLineJobImpl(ClientCapabilityDetector.LINUX_CMD);
        ClientCapabilityDetector.PLATFORM platform = ClientCapabilityDetector.getPlatformByOsName("Linux");
        assertTrue(topJob.getProcInfo().equals("Unable to get process info because process is null."));
    }
}