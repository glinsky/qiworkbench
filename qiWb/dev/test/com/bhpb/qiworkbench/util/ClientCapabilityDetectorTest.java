/*
 * ClientCapabilityDetectorTest.java
 *
 * Created on September 12, 2007, 9:30 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.qiworkbench.util;

import java.io.File;
import junit.framework.TestCase;

/**
 *
 * @author folsw9
 */
public class ClientCapabilityDetectorTest extends TestCase {
    private final String MAX_32_BIT_HEAP = "2147483648";
    private final String MAX_32_BIT_HEAP_IN_KB = "2097152k";
    //changed from 1215m to 1024m to reflect new hard-coded limit for windows
    //this is a workaround for the fact that some windows xp systems cannot
    //create JVMs with maxheapsize > ~1.4GB, even with sufficient memory available.
    private final String WINDOWS_XP_HEAP_IN_MB = "1024m";
    private final String WINDOWS_VISTA_HEAP_IN_MB = "541m";
    private final String MIN_WINDOWS_HEAP = "512m";
    private final String LINUX_16_GB_12_USED_HEAP = "1443192k";
    private final String DARWIN_HEAP = "1932735283";

    public ClientCapabilityDetectorTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
    }
    
    @Override
    protected void tearDown() throws Exception {
    } 
        
    private String getPath() {
        return System.getProperty("basedir") + File.separator + "test" + File.separator + "data" + File.separator;
    }
    
    public void testLinuxJobOutputParsing() throws Exception {
        CommandLineJob mockJob = new CommandLineJobMock(new File(getPath() + "LinuxTopOutput.txt"));
        
        
        ClientCapabilityDetector.PLATFORM platform = ClientCapabilityDetector.getPlatformByOsName("Linux");
        String memResult = ClientCapabilityDetector.parsePhysicalMemoryFromStdout(mockJob, platform);
        memResult = ClientCapabilityDetector.getHeapStringInJVMformat(memResult, platform);
        
        assertEquals(MAX_32_BIT_HEAP_IN_KB, memResult);
    }
    
    public void testLinux16gbRamJobOutputParsing() throws Exception {
        CommandLineJob mockJob = new CommandLineJobMock(new File(getPath() + "Linux_16gb_12used.txt"));
        
        
        ClientCapabilityDetector.PLATFORM platform = ClientCapabilityDetector.getPlatformByOsName("Linux");
        String memResult = ClientCapabilityDetector.parsePhysicalMemoryFromStdout(mockJob, platform);
        memResult = ClientCapabilityDetector.getHeapStringInJVMformat(memResult, platform);
        
        assertEquals("Test failure - expected: " + LINUX_16_GB_12_USED_HEAP + ", actual: " + memResult,
                LINUX_16_GB_12_USED_HEAP, 
                memResult);
    }
    
    public void testWindowsXPjobOutputParsing() throws Exception {
        CommandLineJob mockJob = new CommandLineJobMock(new File(getPath() + "WindowsXPsystemInfoOutput.txt"));
        
        ClientCapabilityDetector.PLATFORM platform = ClientCapabilityDetector.getPlatformByOsName("Windows XP");
        String memResult = ClientCapabilityDetector.parsePhysicalMemoryFromStdout(mockJob, platform);
        memResult = ClientCapabilityDetector.getHeapStringInJVMformat(memResult, platform);
        
        assertEquals("Test failure - expected: " + WINDOWS_XP_HEAP_IN_MB + ", actual: " + memResult,WINDOWS_XP_HEAP_IN_MB, memResult);
    }

    public void testLinuxQuadCorejobOutputParsing() throws Exception {
        CommandLineJob mockJob = new CommandLineJobMock(new File(getPath() + "LinuxTopQuadCoreOutput.txt"));
        
        ClientCapabilityDetector.PLATFORM platform = ClientCapabilityDetector.getPlatformByOsName("Linux");
        String memResult = ClientCapabilityDetector.parsePhysicalMemoryFromStdout(mockJob, platform);
        memResult = ClientCapabilityDetector.getHeapStringInJVMformat(memResult, platform);
        
        assertEquals(MAX_32_BIT_HEAP_IN_KB, memResult);
    }
        
    public void testVistajobOutputParsing() throws Exception {
        CommandLineJob mockJob = new CommandLineJobMock(new File(getPath() + "VistaSystemInfoOutput.txt"));
        
        ClientCapabilityDetector.PLATFORM platform = ClientCapabilityDetector.getPlatformByOsName("Windows Vista");
        String memResult = ClientCapabilityDetector.parsePhysicalMemoryFromStdout(mockJob, platform);
        memResult = ClientCapabilityDetector.getHeapStringInJVMformat(memResult, platform);
        
        assertEquals(WINDOWS_VISTA_HEAP_IN_MB, memResult);
    }
    
    /**
     * Validates that the ClientCapabilityDetector returns 512m when the output of
     * systeminfo is less than 512m, e.q. 320m
     */
    public void testWindowsXPbelow512ram() throws Exception {
        CommandLineJob mockJob = new CommandLineJobMock(new File(getPath() + "WindowsXPbelow512ram.txt"));
            
        ClientCapabilityDetector.PLATFORM platform = ClientCapabilityDetector.getPlatformByOsName("Windows XP");
        String memResult = ClientCapabilityDetector.parsePhysicalMemoryFromStdout(mockJob, platform);
        memResult = ClientCapabilityDetector.getHeapStringInJVMformat(memResult, platform);
        
        assertEquals(MIN_WINDOWS_HEAP, memResult);
    }
        
    public void testDarwinJobOutputParsing() throws Exception {
        CommandLineJob mockJob = new CommandLineJobMock(new File(getPath() + "DarwinSysctlOutput18g.txt"));
        
        ClientCapabilityDetector.PLATFORM platform = ClientCapabilityDetector.getPlatformByOsName("Mac OSX");
        String memResult = ClientCapabilityDetector.parsePhysicalMemoryFromStdout(mockJob, platform);
        memResult = ClientCapabilityDetector.getHeapStringInJVMformat(memResult, platform);
        
        assertEquals(MAX_32_BIT_HEAP, memResult);
    }
    
    public void testDarwinJobOutputParsing2() throws Exception {
        CommandLineJob mockJob = new CommandLineJobMock(new File(getPath() + "DarwinSysctlOutput2g.txt"));
        
        ClientCapabilityDetector.PLATFORM platform = ClientCapabilityDetector.getPlatformByOsName("Mac OSX");
        String memResult = ClientCapabilityDetector.parsePhysicalMemoryFromStdout(mockJob, platform);
        memResult = ClientCapabilityDetector.getHeapStringInJVMformat(memResult, platform);
        
        assertEquals(DARWIN_HEAP, memResult);
    }
    
    public void testInvalidInput() throws Exception {
        CommandLineJob mockJob = new CommandLineJobMock(new File(getPath() + "ClientCapabilityDetectorInvalidInput.txt"));
        
        String memResult = null;
        
        for (ClientCapabilityDetector.PLATFORM platform : ClientCapabilityDetector.PLATFORM.values()) {
            memResult = ClientCapabilityDetector.parsePhysicalMemoryFromStdout(mockJob, platform);
            assertEquals("parsePhysicalMemoryFromStdout did not return the empty string given invalid input", "", memResult);
        }
    }
    
    /**
     * Asserts that the heapsize=1024m if for any reason the output from the platform-specific
     * command to get the heap size is an empty stream.  It cannot return an empty string or else
     * the method which formats the OS-specific heap description will throw an IllegalArgumentException.
     */
    public void testXPnoStdOut() throws Exception {
        CommandLineJob mockJob = new CommandLineJobMock(new File(getPath() + "DLLerrorNoStdOutFromSystemInfo.txt"));
        
        String memResult = null;
        
        for (ClientCapabilityDetector.PLATFORM platform : ClientCapabilityDetector.PLATFORM.values()) {
            memResult = ClientCapabilityDetector.parsePhysicalMemoryFromStdout(mockJob, platform);
            String jvmHeapSizeParam = ClientCapabilityDetector.getHeapStringInJVMformat(memResult, platform);
            assertNotNull("jvmHeapSizeParam should be a default value, not null.", jvmHeapSizeParam);
            assertTrue("jvmHeapSizeParam should be a default value, not the empty string.", ClientCapabilityDetector.DEFAULT_MAX_HEAP_SIZE.equals(jvmHeapSizeParam));
        }
    }
}
