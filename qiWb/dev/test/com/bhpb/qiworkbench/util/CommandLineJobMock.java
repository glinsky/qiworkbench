/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2007-2008  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiworkbench.util;

import com.bhpb.qiworkbench.util.CommandLineJob.STREAM;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CommandLineJobMock extends CommandLineJobImpl implements CommandLineJob{
    // this does nothing because CommandLineJobMock.start() will not invoke the superclass start() method
    private static final String[] mockCommandAndArgs = {"MOCK_COMMAND"};
    
    public CommandLineJobMock(File mockOutputTextFile) throws FileNotFoundException {
        super(mockCommandAndArgs);
        BufferedReader reader = new BufferedReader(new FileReader(mockOutputTextFile));
        super.setStreamReader(STREAM.STDOUT, reader);
    }
    
    /**
     * Identical to CommandLineJobImpl.start() but does not start a Process. 
     */
    @Override
    public void start() throws IOException {
        try {
            //TODO open empty file for stderr
            //setStreamReader(STREAM.STDERR, new BufferedReader(new InputStreamReader(process.getErrorStream())));
        
            stdoutWorker = new StreamToStringListConverter(stdoutReader, stdoutText);
            //stderrWorker = new StreamToStringListConverter(stderrReader, stderrText);
            
            new Thread(stdoutWorker).start();
            //new Thread(stderrWorker).start();
        } finally {
            try {
            //If the worker was not initialized but the buffered stream reader was created, close it
            if ((stdoutWorker == null) && (stdoutReader != null))
                stdoutReader.close();
            } finally { // cascaded in case the attempt to close the first stream throws an IOException
                //If the worker was not initialized but the buffered stream reader was created, close it
                if ((stderrWorker == null) && (stderrReader != null))
                    stderrReader.close();
            }
        }
    }
    
    /**
     * Simulates blocking until process completion by waiting until the StreamToStringListConverter has consumed
     * all available data from the BufferedReader.
     *
     * @param delay time to wait between calls to isFinished()
     * @param iterations number of times to call isFinished()
     *
     * @ return true if the StreamToStringListConverter finished running before delay*iterations milliseconds
     */
    @Override
    public boolean waitFor(int delay, int iterations) throws InterruptedException {
        int elapsedIterations = 0;
        while ((stdoutWorker.isFinished() == false) && (elapsedIterations < iterations)){
            try {
                Thread.sleep(delay);
            } catch (InterruptedException ie) {
                Thread.interrupted();
            }
            elapsedIterations++;
        }
        return stdoutWorker.isFinished();
    }
}
