package com.bhpb.qiworkbench.client.services;

import com.bhpb.geoio.filesystems.FileSystemConstants;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import com.bhpb.qiworkbench.IqiMessageHandler;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher_getResponseWait_Timeout_And_Response_Test;
import com.bhpb.testutils.MockQiComponent;
import com.bhpb.testutils.QiWorkbenchProxyFactory;
import java.util.ArrayList;
import java.util.logging.Logger;
import junit.framework.TestCase;

public class GeoIOservice_EventMetadata_SetSelectedHorizonsTest extends TestCase {
    QiWorkbenchProxyFactory factory;
    MockQiComponent mock1;
    IqiMessageHandler comp1;
    
    private static final Logger logger = Logger.getLogger(MessageDispatcher_getResponseWait_Timeout_And_Response_Test.class.toString());
    private static final String TEST_FILE_NAME = "/scratch/qiProjects/demo/datasets/migrated_stack3_cube_events.dat";
    private static final int TIMEOUT = 30*60*1000; //30 minutes
    
    @Override
    public void setUp() throws NoSuchMethodException, InterruptedException {
        factory = new QiWorkbenchProxyFactory();
        assertNotNull(factory);
        
        setUpMessageDispatcher();
        mock1 = setUpMockQiComponent();
        
        comp1 = setUpProxy(mock1);
    }
    
    public GeoIOservice_EventMetadata_SetSelectedHorizonsTest() {
    }
    
    private void setUpMessageDispatcher() {
        System.setProperty("deployServerURL","http://localhost:8080");   
        System.setProperty("install","false");
        
        MessageDispatcher.mainNoGUI();
        
        MessageDispatcher dispatcher  = MessageDispatcher.getInstance();
        assertTrue(dispatcher.isInitSuccessful());
    }
    
    private MockQiComponent setUpMockQiComponent()  {
        MockQiComponent mock = new MockQiComponent();
        assertNotNull(mock);
        mock.isInitFinished(); // test mock functionality
        
        return mock;
    }
    
    private IqiMessageHandler setUpProxy(IqiMessageHandler component) throws NoSuchMethodException {
        Object proxy = factory.createProxy(component);
        assertNotNull(proxy);
        
        IqiMessageHandler proxyComponent = (IqiMessageHandler) proxy;
        proxyComponent.isInitFinished(); // test proxy invocation of mock functionality
        
        return (IqiMessageHandler) proxyComponent;
    }
    
    public void testGetMetadata() throws InterruptedException, NoSuchMethodException {
        mock1.startComponent("MOCK1");
        
        Thread.sleep(2000);

        //TODO this should either get the same lock as MockQiComponent.init() or else getCID() should be synchronized on that lock
        logger.info("Mock1.getCID() = " + comp1.getCID());
        
        IComponentDescriptor cd1 = MessageDispatcher.getInstance().getComponentDesc(comp1.getCID());
        
        assertNotNull("md.getComponentDesc() returned null comp descriptor for CID: " +comp1.getCID(), cd1);
        
        getMetadata();
    }
    
    private void getMetadata() {
        logger.info("Getting metadata for " + TEST_FILE_NAME);
        ArrayList<String> params = new ArrayList<String>();
        params.add(QIWConstants.REMOTE_SERVICE_PREF);
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(TEST_FILE_NAME);
        params.add(FileSystemConstants.FILE_METADATA_DATA);
        
        IMessagingManager msgMgr = mock1.getMessagingManager();
        String geoIORequestID = msgMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);
        
        if (geoIORequestID == null) {
            String errorMsg = "Unable to send " + QIWConstants.READ_GEOFILE_CMD;
            fail(errorMsg);
        } else {
            // wait for the response, which will come.
            // The time to read the data is indefinite because it depends on the
            // amount of data to be read. Wait up to 1/2 hour. Either all of
            // the data will have been read by then or an abnormal response is
            // returned because of an IO error.
            // TODO: update this with some sort of progress indicator
            IQiWorkbenchMsg response = msgMgr.getMatchingResponseWait(geoIORequestID, TIMEOUT);
            if (response == null) {
                String errorMsg = "Unable to get response of " + QIWConstants.READ_GEOFILE_CMD + " after " + TIMEOUT + " seconds.";
                logger.warning(errorMsg);
                fail(errorMsg);
            } else {
                if (response.isAbnormalStatus()) {
                    String errorMsg = "Unable to get metadata for geoFile " + TEST_FILE_NAME;
                    fail(errorMsg);
                }
                Object contentType = response.getContentType();
                Object content = response.getContent();
                
                if (QIWConstants.ARRAYLIST_TYPE.equals(contentType)) {
                    ArrayList contentList = (ArrayList) content;
                    
                    //validate this is a response with the geoFile's metadata
                    String fileFormat = (String) contentList.get(0);
                    String dataType = (String) contentList.get(1);
                    if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                            !dataType.equals(FileSystemConstants.FILE_METADATA_DATA)) {
                        String errorMsg = "Not a response containing geoFile metadata"+response;
                        logger.warning(errorMsg);
                        fail(errorMsg);
                    }
                    
                    SeismicFileMetadata metadata = (SeismicFileMetadata)contentList.get(2);
                    
                    assertNotNull("Got NULL metadata from non-null, non-abnormal response: " + response.toString());
                    
                    logger.info("METADATA for " + TEST_FILE_NAME);
                    
                    logger.info(metadata.toString());
                    
                    //assertTrue("metadata does not have the correct test values for file: " + TEST_FILE_NAME, hasCorrectTestValues(metadata));
                    assertNotNull(metadata.getHorizons());
                    assertEquals(metadata.getHorizons().size(), 5);
                    
                    logger.info("metadata.getHorizons().size() == 5");
                    logger.info("metadata.getHorizons().get(0)=" + metadata.getHorizons().get(0));
                    
                    HorizonProperties hProps= new HorizonProperties(metadata);
                    
                    hProps.setSelectedHorizon("null3");
                    
                    //does not currently work - always returns an ArrayList of size 0
                    String selectedHorizonFromHprops = hProps.getSelectedHorizon();
                    
                    assertNotNull(selectedHorizonFromHprops);
                    
                    logger.info("Selected Horizon: " + selectedHorizonFromHprops);
                }
            }
        }
    }
}