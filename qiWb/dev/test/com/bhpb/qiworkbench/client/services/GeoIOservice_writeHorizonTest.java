package com.bhpb.qiworkbench.client.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.StringTokenizer;

import com.bhpb.geoio.datasystems.BhpSuHorizonDataObject;
import com.bhpb.geoio.filesystems.FileSystemConstants;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import com.bhpb.geoio.filesystems.properties.SeismicProperties;
import com.bhpb.geoio.util.GeoIOAdapter;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.IqiMessageHandler;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
import com.bhpb.qiworkbench.util.CommandLineJob;
import com.bhpb.qiworkbench.util.CommandLineJob.STREAM;
import com.bhpb.qiworkbench.util.CommandLineJobImpl;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher_getResponseWait_Timeout_And_Response_Test;
import com.bhpb.testutils.MockQiComponent;
import com.bhpb.testutils.QiWorkbenchProxyFactory;

import junit.framework.TestCase;

public class GeoIOservice_writeHorizonTest extends TestCase {
    QiWorkbenchProxyFactory factory;
    MockQiComponent mock1;
    IqiMessageHandler comp1;
    private static final Logger logger = Logger.getLogger(MessageDispatcher_getResponseWait_Timeout_And_Response_Test.class.toString());
    
    private static final int TIMEOUT = 30 * 60 * 1000; //30 minutes
    public static final int DEFAULT_DELAY = 100; // wait 100ms between checking to see if commandLineJob has new lines available

    public static final int DEFAULT_ITERATIONS = 100; // wait a maximum of 100 times (10 seconds total)

    /** Expected horizon values */
    static public final String P1_SAMPLES = "TaranakiAmpExt_p1.xyz";
    static final String baseDir = System.getProperty("basedir");
    static String filesep = File.separator;
    static final String baseTestFilePath = baseDir + filesep + "test" + filesep + "data" + filesep;
    private static final String DATASET_PATH =
            baseTestFilePath + "taranaki_ampext.dat";
    static final String SELECTED_HORIZON = "p1";
    static final String SEISMIC_DIR = "horizWriteTest";
    
    public String[] SVN_CMD = {"bash", "-c", "source ~/.bashrc; svn revert "+ baseTestFilePath+SEISMIC_DIR+filesep+"*"};

    @Override
    public void setUp() throws NoSuchMethodException, InterruptedException, IOException {
        factory = new QiWorkbenchProxyFactory();
        assertNotNull(factory);

        setUpMessageDispatcher();
        mock1 = setUpMockQiComponent();

        comp1 = setUpProxy(mock1);

        createDatFile();
        updateHeaderFile(baseTestFilePath + SEISMIC_DIR, "taranaki_ampext");
    }
    
    @Override
    public void tearDown() {
        //Restore dataset to its original values
        CommandLineJob commandLineJob = new CommandLineJobImpl(SVN_CMD);

        try {
            System.out.println("Executing "+commandLineJob.getCommandAndArguments());
            commandLineJob.start();
            if (commandLineJob.waitFor(DEFAULT_DELAY, DEFAULT_ITERATIONS) == false) {
                System.out.println("Warning: command-line process failed to start after 10 seconds - aborting.");
            } else {
                System.out.println(commandLineJob.getProcInfo());
                int outLines = commandLineJob.getNumLines(STREAM.STDOUT);
                System.out.println("stdout: ");
                for (int i = 0; i<outLines; i++) {
                    System.out.println(commandLineJob.getLineByIndex(STREAM.STDOUT, i));
                }
                int errLines = commandLineJob.getNumLines(STREAM.STDERR);
                System.out.println("stderr: ");
                for (int i = 0; i<errLines; i++) {
                    System.out.println(commandLineJob.getLineByIndex(STREAM.STDERR, i));
                }
               
                System.out.println("Restored dataset using SVN");
            }
        } catch (InterruptedException ie) {
            System.out.println("Command interrupted while waiting for it to complete.");
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.print("IO Exception caught: " + ioe.getMessage());
        }
     
    }

    private void updateHeaderFile(String partitionName, String baseFileName) throws IOException, FileNotFoundException {
        File headerFile = new File(partitionName + filesep + baseFileName + "_0000.HDR");
        BufferedReader reader = new BufferedReader(new FileReader(headerFile));
        List<String> outputLines = new ArrayList<String>();
        String line;
        try {
            do {
                line = reader.readLine();
                if (line != null) {
                    if (line.startsWith("PARTITION")) {
						int idx = line.indexOf(baseFileName);
						String suffix = line.substring(idx+baseFileName.length());
                        outputLines.add("PARTITION = " + partitionName + filesep + baseFileName + suffix);
                    } else {
                        outputLines.add(line);
                    }
                }
            } while (line != null);
        } finally {
            if (reader != null);
            reader.close();
        }

        BufferedWriter writer = new BufferedWriter(new FileWriter(headerFile));
        try {
            for (String outputLine : outputLines) {
                writer.write(outputLine);
                writer.write("\n");
            }
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    /**
     * Create a dataset file with a hardcoded path to the directory containing
     * the BHP-SU seismic files.
     */
    private void createDatFile() throws IOException {
        File datFile = new File(DATASET_PATH);
//        datFile.deleteOnExit();
        FileWriter writer = null;
        try {
            writer = new FileWriter(datFile);
            writer.write(baseTestFilePath + SEISMIC_DIR);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    public GeoIOservice_writeHorizonTest() {
    }

    private void setUpMessageDispatcher() {
        System.setProperty("deployServerURL", "http://localhost:8080");
        System.setProperty("install", "false");

        MessageDispatcher.mainNoGUI();

        MessageDispatcher dispatcher = MessageDispatcher.getInstance();
        assertTrue(dispatcher.isInitSuccessful());
    }

    private MockQiComponent setUpMockQiComponent() {
        MockQiComponent mock = new MockQiComponent();
        assertNotNull(mock);
        mock.isInitFinished(); // test mock functionality

        return mock;
    }

    private IqiMessageHandler setUpProxy(IqiMessageHandler component) throws NoSuchMethodException {
        Object proxy = factory.createProxy(component);
        assertNotNull(proxy);

        IqiMessageHandler proxyComponent = (IqiMessageHandler) proxy;
        proxyComponent.isInitFinished(); // test proxy invocation of mock functionality

        return (IqiMessageHandler) proxyComponent;
    }

    /** Test writing a horizon */
    public void testWriteHorizon1() throws InterruptedException, NoSuchMethodException {
        mock1.startComponent("MOCK1");

        Thread.sleep(2000);
        mock1.setTomcatLoc("http://localhost:8080");
        mock1.setLocationPref(QIWConstants.REMOTE_PREF);

        ArrayList<String> remoteOSInfoList = mock1.getRemoteOSInfo(QIWConstants.REMOTE_PREF);
        MessageDispatcher.getInstance().setRemoteOSInfoList(remoteOSInfoList);

        //TODO this should either get the same lock as MockQiComponent.init() or else getCID() should be synchronized on that lock
        logger.info("Mock1.getCID() = " + comp1.getCID());

        IComponentDescriptor cd1 = MessageDispatcher.getInstance().getComponentDesc(comp1.getCID());

        assertNotNull("md.getComponentDesc() returned null comp descriptor for CID: " + comp1.getCID(), cd1);
        GeoIOAdapter geoIOAdapter = new GeoIOAdapter(mock1.getMessagingManager());

        SeismicFileMetadata metadata = getMetadata();
        HorizonProperties geoioDatasetProps = new HorizonProperties(metadata);
        geoioDatasetProps.setSelectedHorizon(SELECTED_HORIZON);
        GeoFileDataSummary summary = getSummary(geoioDatasetProps);

        assertNotNull(summary);

        geoioDatasetProps.setSummary(summary);

        logger.info("Summary data: ");
        logger.info(summary.toString());

        BhpSuHorizonDataObject[] horizons = getHorizons(geoioDatasetProps, geoIOAdapter);

        //assertTrue("summary data does not have the correct test values for file: " + DATASET_PATH, hasCorrectTestValues(summary));
        logger.info("Performing initial validation of first HorizonDataObject returned when reading " + SELECTED_HORIZON);
        float[] samples = getSamples(horizons);
        validate(samples, baseTestFilePath + P1_SAMPLES);

        logger.info("Creating 'picked' horizon test data.");
        float[] pickedHorizon = createPickedHorizon(12859);

        //now write the 'picked' Horizon, read it back, assert equality
        String status = geoIOAdapter.writeXsecHorizon(SELECTED_HORIZON, geoioDatasetProps, pickedHorizon);
        assertEquals("Picked horizon not written out", "", status);
        
        //Read SELECTED_HORIZON and assert equality with the test data
        horizons = getHorizons(geoioDatasetProps, geoIOAdapter);
        samples = getSamples(horizons);
        assertEquals("Horizon samples read from " + DATASET_PATH + " were not equivalent to written samples", pickedHorizon, samples);
        
        //Note: The original dataset is restored by tearDown() which is executed
        //even if the test fails.
    }

    private void assertEquals(String errMsg, float[] expected, float[] actual) {
        if (expected == null && actual == null) {
            return;
        }

        if (expected == null) {
            fail(errMsg + ", array of expected values was null but actual array was non-null");
        }

        if (actual == null) {
            fail(errMsg + ", array of expected values was non-null but actual array was null");
        }

        if (expected.length != actual.length) {
            fail(errMsg + ", expected array length was " + expected.length + " but actual array length was " + actual.length);
        }

        for (int i = 0; i < expected.length; i++) {
            if (expected[i] != actual[i]) {
                fail(errMsg + ": expected[" + i + "] = " + expected[i] + ", " + "actual[" + i + "] = " + actual[i]);
            }
        }
    }

    /**
     * Perform sanity check on number of BhpSuHorizonDataObjects and concatenate
     * samples from all horizon traces into a single data array.
     *
     * @param horizons
     *
     * @return
     */
    private float[] getSamples(BhpSuHorizonDataObject[] horizons) {
        assertNotNull(horizons);
        int numHorizons = horizons.length;

        logger.info("Total number of horizons read: " + numHorizons);

        assertEquals(12859, numHorizons);
        assertEquals(1, horizons[0].getFloatVector().length);

        float[] mutableHorizonArray = new float[numHorizons];

        for (int i = 0; i < numHorizons; i++) {
            mutableHorizonArray[i] = horizons[i].getFloatVector()[0];
        }

        return mutableHorizonArray;
    }

    private float[] createPickedHorizon(int size) {
        float[] samples = new float[size];
        for (int i = 0; i < size; i++) {
            samples[i] = i % 256; // values run 0..255 and repeat

        }
        return samples;
    }

    /**
     * Check if actual horizon values equal the expected values.
     * @param samples Actual horizon values
     * @param testFileName File containing the expected values.
     */
    private void validate(float[] samples, String testFileName) {
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(testFileName));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);

            int cnt = 0;
            int numSamples = samples.length;
            float val = 0;
            String line = ebr.readLine();

            // compare actual and expected results
            while (line != null && cnt < numSamples) {
                StringTokenizer st = new StringTokenizer(line);
                //extract horizon (3rd token) as a float
                st.nextToken();
                st.nextToken();
                String sample = st.nextToken();
                val = 0;
                try {
                    val = Float.parseFloat(sample);
                } catch (NumberFormatException nfe) {
                    fail("Unable to parse test data from " + P1_SAMPLES + ": " + nfe.getMessage());
                }
                assertEquals(P1_SAMPLES + ": samples are incorrect", val, samples[cnt]);
                line = ebr.readLine();
                cnt++;
            }
            if (cnt != numSamples) {
                fail("More actual samples than expected: actual=" + val);
            }
            if (line != null) {
                fail("More expected samples than actual: expected=" + line);
            }
        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected horizon samples file: " + P1_SAMPLES + "; error message: " + fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error reading expected horizon samples: " + P1_SAMPLES + "; error message: " + ioe.getMessage());
        }
    }

    private SeismicFileMetadata getMetadata() {
        SeismicFileMetadata metadata = null;

        logger.info("Getting metadata for " + DATASET_PATH);
        ArrayList<String> params = new ArrayList<String>();
        params.add(QIWConstants.REMOTE_SERVICE_PREF);
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(DATASET_PATH);
        params.add(FileSystemConstants.FILE_METADATA_DATA);

        IMessagingManager msgMgr = mock1.getMessagingManager();
        String geoIORequestID = msgMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);

        if (geoIORequestID == null) {
            String errorMsg = "Unable to send " + QIWConstants.READ_GEOFILE_CMD;
            fail(errorMsg);
        } else {
            // wait for the response, which will come.
            // The time to read the data is indefinite because it depends on the
            // amount of data to be read. Wait up to 1/2 hour. Either all of
            // the data will have been read by then or an abnormal response is
            // returned because of an IO error.
            // TODO: update this with some sort of progress indicator
            IQiWorkbenchMsg response = msgMgr.getMatchingResponseWait(geoIORequestID, TIMEOUT);
            if (response == null) {
                String errorMsg = "Unable to get response of " + QIWConstants.READ_GEOFILE_CMD + " after " + TIMEOUT + " seconds.";
                logger.warning(errorMsg);
                fail(errorMsg);
            } else {
                if (response.isAbnormalStatus()) {
                    String errorMsg = "Unable to get metadata for geoFile " + DATASET_PATH;
                    fail(errorMsg);
                }
                Object contentType = response.getContentType();
                Object content = response.getContent();

                if (QIWConstants.ARRAYLIST_TYPE.equals(contentType)) {
                    ArrayList contentList = (ArrayList) content;

                    //validate this is a response with the geoFile's metadata
                    String fileFormat = (String) contentList.get(0);
                    String dataType = (String) contentList.get(1);
                    if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                            !dataType.equals(FileSystemConstants.FILE_METADATA_DATA)) {
                        String errorMsg = "Not a response containing geoFile metadata" + response;
                        logger.warning(errorMsg);
                        fail(errorMsg);
                    }

                    metadata = (SeismicFileMetadata) contentList.get(2);

                    assertNotNull("Got NULL metadata from non-null, non-abnormal response: " + response.toString());

                    logger.info("METADATA for " + DATASET_PATH);

                    logger.info(metadata.toString());

                }
            }
        }

        return metadata;
    }

    private GeoFileDataSummary getSummary(SeismicProperties geoioDatasetProps) {
        GeoFileDataSummary summaryData = null;

        IMessagingManager msgMgr = mock1.getMessagingManager();

        ArrayList params = new ArrayList<String>();
        params.add(mock1.getMessagingManager().getLocationPref());
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(geoioDatasetProps);

        params.add(FileSystemConstants.TRACE_SUMMARY_DATA);

        String geoIORequestID = msgMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);


        if (geoIORequestID == null) {
            String errorMsg = "Unable to send " + QIWConstants.READ_GEOFILE_CMD;
            logger.warning(errorMsg);
            fail(errorMsg);
        } else {
            // wait for the response, which will come.
            // The time to read the data is indefinite because it depends on the
            // amount of data to be read. Wait up to 1/2 hour. Either all of
            // the data will have been read by then or an abnormal response is
            // returned because of an IO error.
            // TODO: update this with some sort of progress indicator
            IQiWorkbenchMsg response = msgMgr.getMatchingResponseWait(geoIORequestID, TIMEOUT);
            if (response == null) {
                String errorMsg = "Unable to get response of " + QIWConstants.READ_GEOFILE_CMD + " after " + TIMEOUT + " seconds.";
                logger.warning(errorMsg);
                fail(errorMsg);
            } else {
                if (response.isAbnormalStatus()) {
                    String errorMsg = "Unable to get TRACE_SUMMARY_DATA for datasetProperties " + geoioDatasetProps.toString();
                    logger.warning(errorMsg);
                    fail(errorMsg);
                }
                Object contentType = response.getContentType();
                Object content = response.getContent();

                if (QIWConstants.ARRAYLIST_TYPE.equals(contentType)) {
                    ArrayList contentList = (ArrayList) content;

                    //validate this is a response with the geoFile's metadata
                    String fileFormat = (String) contentList.get(0);
                    String dataType = (String) contentList.get(1);
                    if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                            !dataType.equals(FileSystemConstants.TRACE_SUMMARY_DATA)) {
                        String errorMsg = "Not a response containing geoFile TRACE_SUMMARY_DATA" + response;
                        logger.warning(errorMsg);
                        fail(errorMsg);
                    }

                    summaryData = (GeoFileDataSummary) contentList.get(2);
                    logger.fine("TRACE_SUMMARY_DATA for datasetProperties" + geoioDatasetProps);
                    logger.fine(summaryData.toString());

                    geoioDatasetProps.setSummary(summaryData);

                    logger.fine("Summary data set for dataSetProperties.");
                }
            }
        }
        return summaryData;
    }

    public BhpSuHorizonDataObject[] getHorizons(HorizonProperties hProps, GeoIOAdapter geoIOadapter) {
        return geoIOadapter.getHorizons(hProps);
    }
}
