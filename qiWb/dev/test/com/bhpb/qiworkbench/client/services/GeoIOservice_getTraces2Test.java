/*
 * Test the correct number of traces are read for a given set of keys
 * and their full range. Test the trace values are correct for their
 * chosen ranges.
 */

package com.bhpb.qiworkbench.client.services;

import com.bhpb.geoio.datasystems.DataSystemConstants.GeoKeyType;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;

import com.bhpb.geoio.filesystems.FileSystemConstants;
import com.bhpb.geoio.filesystems.geofileio.GeoReadData;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import com.bhpb.geoio.filesystems.properties.SeismicProperties;

import com.bhpb.qiworkbench.IqiMessageHandler;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IDataObject;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher_getResponseWait_Timeout_And_Response_Test;
import com.bhpb.testutils.MockQiComponent;
import com.bhpb.testutils.QiWorkbenchProxyFactory;
import java.util.ArrayList;
import java.util.logging.Logger;
import junit.framework.TestCase;

public class GeoIOservice_getTraces2Test extends TestCase {
    QiWorkbenchProxyFactory factory;
    MockQiComponent mock1;
    IqiMessageHandler comp1;

    private static final Logger logger = Logger.getLogger(MessageDispatcher_getResponseWait_Timeout_And_Response_Test.class.toString());
    private static final String TEST_FILE_NAME = "/scratch/qiProjects/demo/datasets/gathers.dat";
    private static final int TIMEOUT = 30*60*1000; //30 minutes
    
    /** Expected trace 0 values */
    static public final String TRACE0_SAMPLES = "GathersTrace0.txt";
    static public final String TRACE48_SAMPLES = "GathersTrace48.txt";
    static public final String TRACE95_SAMPLES = "GathersTrace95.txt";
    
    static final String baseDir = System.getProperty("basedir");
    static final String baseTestFilePath = baseDir + File.separator + "test" + File.separator + "data" + File.separator;

    @Override
    public void setUp() throws NoSuchMethodException, InterruptedException {
        factory = new QiWorkbenchProxyFactory();
        assertNotNull(factory);

        setUpMessageDispatcher();
        mock1 = setUpMockQiComponent();

        comp1 = setUpProxy(mock1);
    }

    public GeoIOservice_getTraces2Test() {
    }

    private void setUpMessageDispatcher() {
        System.setProperty("deployServerURL","http://localhost:8080");
        System.setProperty("install","false");

        MessageDispatcher.mainNoGUI();

        MessageDispatcher dispatcher  = MessageDispatcher.getInstance();
        assertTrue(dispatcher.isInitSuccessful());
    }

    private MockQiComponent setUpMockQiComponent()  {
        MockQiComponent mock = new MockQiComponent();
        assertNotNull(mock);
        mock.isInitFinished(); // test mock functionality

        return mock;
    }

    private IqiMessageHandler setUpProxy(IqiMessageHandler component) throws NoSuchMethodException {
        Object proxy = factory.createProxy(component);
        assertNotNull(proxy);

        IqiMessageHandler proxyComponent = (IqiMessageHandler) proxy;
        proxyComponent.isInitFinished(); // test proxy invocation of mock functionality

        return (IqiMessageHandler) proxyComponent;
    }

    /** Test getting the traces for chosen ranges */
    public void testGetTraces2() throws InterruptedException, NoSuchMethodException {
        mock1.startComponent("MOCK1");

        Thread.sleep(2000);
        mock1.setTomcatLoc("http://localhost:8080");
        mock1.setLocationPref(QIWConstants.REMOTE_PREF);

        ArrayList<String> remoteOSInfoList = mock1.getRemoteOSInfo(QIWConstants.REMOTE_PREF);
        MessageDispatcher.getInstance().setRemoteOSInfoList(remoteOSInfoList);

        //TODO this should either get the same lock as MockQiComponent.init() or else getCID() should be synchronized on that lock
        logger.info("Mock1.getCID() = " + comp1.getCID());

        IComponentDescriptor cd1 = MessageDispatcher.getInstance().getComponentDesc(comp1.getCID());

        assertNotNull("md.getComponentDesc() returned null comp descriptor for CID: " +comp1.getCID(), cd1);

        SeismicFileMetadata metadata = getMetadata();
        SeismicProperties geoioDatasetProps = new SeismicProperties(metadata);
        //set chosen ranges and their order
        geoioDatasetProps.setKeyOrder("ep", 1);
        geoioDatasetProps.setKeyOrder("cdp", 2);
        geoioDatasetProps.setKeyOrder("offset", 3);
        geoioDatasetProps.setKeyOrder("tracl", 4);
        //2
        DiscreteKeyRange range = new DiscreteKeyRange(3015, 3016, 1, GeoKeyType.REGULAR_TYPE);
        geoioDatasetProps.setKeyChosenRange("ep", range);
        //16
        range = new DiscreteKeyRange(1400, 1415, 1, GeoKeyType.REGULAR_TYPE);
        geoioDatasetProps.setKeyChosenRange("cdp", range);
        //3
        range = new DiscreteKeyRange(3650, 3850, 100, GeoKeyType.IRREGULAR_TYPE);
        geoioDatasetProps.setKeyChosenRange("offset", range);
        //2001 samples per trace
        range = new DiscreteKeyRange(0.0, 8000.0, 4.0, GeoKeyType.NO_TYPE);
        geoioDatasetProps.setKeyChosenRange("trac;", range);

        GeoFileDataSummary summary = getSummary(geoioDatasetProps);

        assertNotNull(summary);

        geoioDatasetProps.setSummary(summary);

        logger.info("Summary data: ");
        logger.info(summary.toString());

        GeoReadData geoReadData = getTraces(geoioDatasetProps);

        //assertTrue("summary data does not have the correct test values for file: " + TEST_FILE_NAME, hasCorrectTestValues(summary));
        //assetNotNull(traces);
        assertNotNull(geoReadData);

        logger.info("Waiting 5 seconds to read all trace data");

        Thread.sleep(5000);

        //assertTrue(geoReadData.isAllDataRead());
        if (geoReadData.isAllDataRead() == false) {
            logger.warning("All data was not read after 10 seconds.");
            logger.info("status was abnormal: " + geoReadData.isAbnormalStatus());
        }

        int numTraces = 0;

	IDataObject trace0 = null, trace48 = null, trace95 = null;
        while (geoReadData.hasMoreRecords()) {
            IDataObject trace = geoReadData.nextRecord();
            assertNotNull(trace);
	    if (numTraces == 0) trace0 = trace;
	    else if (numTraces == 48) trace48 = trace;
	    else if (numTraces == 95) trace95 = trace;
            numTraces++;
        }

        logger.info("Total number of traces read: " + numTraces);
        logger.info("processed: " + geoReadData.getProcessedRecords());
        logger.info("remaining: " + geoReadData.getRemainingRecords());

        assertEquals("Didn't read expected number of traces", numTraces, 2*16*3);
	assertNotNull("Trace 0 not read", trace0);
	assertNotNull("Trace 48 not read", trace48);
	assertNotNull("Trace 95 not read", trace95);
	
	/** Expected line */
        String eline = "";
        /** Actual line */
        String aline = "";
	
	float[] samples = trace0.getFloatVector();
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+TRACE0_SAMPLES));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            int cnt = 0;
            int numSamples = samples.length;

            System.out.println("Checking " + samples.length + " samples...");
            // compare actual and expected results line by line
            while (eline != null && cnt < numSamples) {
	    	aline = Float.toString(samples[cnt]);
//System.out.println("expected=\""+eline+"\"");
//System.out.println("actual=\""+aline+"\"");
		assertEquals(TRACE0_SAMPLES+": samples are incorrect", eline, aline); 
                eline = ebr.readLine();
		cnt++;
            }
            if (cnt != numSamples)
                fail("More actual samples than expected: actual="+aline);
            if (eline != null)
                fail("More expected samples than actual: expected="+eline);
        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected trace samples file: "+TRACE0_SAMPLES+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error reading expected trace samples: "+TRACE0_SAMPLES+"; error message: "+ioe.getMessage());
        }
	
	samples = trace48.getFloatVector();
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+TRACE48_SAMPLES));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            int cnt = 0;
            int numSamples = samples.length;

            // compare actual and expected results line by line
            while (eline != null && cnt < numSamples) {
	    	aline = Float.toString(samples[cnt]);
//System.out.println("expected=\""+eline+"\"");
//System.out.println("actual=\""+aline+"\"");
		assertEquals(TRACE48_SAMPLES+": samples are incorrect", eline, aline); 
                eline = ebr.readLine();
		cnt++;
            }
            if (cnt != numSamples)
                fail("More actual samples than expected: actual="+aline);
            if (eline != null)
                fail("More expected samples than actual: expected="+eline);
        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected trace samples file: "+TRACE48_SAMPLES+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error reading expected trace samples: "+TRACE48_SAMPLES+"; error message: "+ioe.getMessage());
        }
	
	samples = trace95.getFloatVector();
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+TRACE95_SAMPLES));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            int cnt = 0;
            int numSamples = samples.length;

            // compare actual and expected results line by line
            while (eline != null && cnt < numSamples) {
	    	aline = Float.toString(samples[cnt]);
//System.out.println("expected=\""+eline+"\"");
//System.out.println("actual=\""+aline+"\"");
		assertEquals(TRACE95_SAMPLES+": samples are incorrect", eline, aline); 
                eline = ebr.readLine();
		cnt++;
            }
            if (cnt != numSamples)
                fail("More actual samples than expected: actual="+aline);
            if (eline != null)
                fail("More expected samples than actual: expected="+eline);
        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected trace samples file: "+TRACE95_SAMPLES+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error reading expected trace samples: "+TRACE95_SAMPLES+"; error message: "+ioe.getMessage());
        }
    }

    private SeismicFileMetadata getMetadata() {
        SeismicFileMetadata metadata = null;

        logger.info("Getting metadata for " + TEST_FILE_NAME);
        ArrayList<String> params = new ArrayList<String>();
        params.add(QIWConstants.REMOTE_SERVICE_PREF);
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(TEST_FILE_NAME);
        params.add(FileSystemConstants.FILE_METADATA_DATA);

        IMessagingManager msgMgr = mock1.getMessagingManager();
        String geoIORequestID = msgMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);

        if (geoIORequestID == null) {
            String errorMsg = "Unable to send " + QIWConstants.READ_GEOFILE_CMD;
            fail(errorMsg);
        } else {
            // wait for the response, which will come.
            // The time to read the data is indefinite because it depends on the
            // amount of data to be read. Wait up to 1/2 hour. Either all of
            // the data will have been read by then or an abnormal response is
            // returned because of an IO error.
            // TODO: update this with some sort of progress indicator
            IQiWorkbenchMsg response = msgMgr.getMatchingResponseWait(geoIORequestID, TIMEOUT);
            if (response == null) {
                String errorMsg = "Unable to get response of " + QIWConstants.READ_GEOFILE_CMD + " after " + TIMEOUT + " seconds.";
                logger.warning(errorMsg);
                fail(errorMsg);
            } else {
                if (response.isAbnormalStatus()) {
                    String errorMsg = "Unable to get metadata for geoFile " + TEST_FILE_NAME;
                    fail(errorMsg);
                }
                Object contentType = response.getContentType();
                Object content = response.getContent();

                if (QIWConstants.ARRAYLIST_TYPE.equals(contentType)) {
                    ArrayList contentList = (ArrayList) content;

                    //validate this is a response with the geoFile's metadata
                    String fileFormat = (String) contentList.get(0);
                    String dataType = (String) contentList.get(1);
                    if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                            !dataType.equals(FileSystemConstants.FILE_METADATA_DATA)) {
                        String errorMsg = "Not a response containing geoFile metadata"+response;
                        logger.warning(errorMsg);
                        fail(errorMsg);
                    }

                    metadata = (SeismicFileMetadata)contentList.get(2);

                    assertNotNull("Got NULL metadata from non-null, non-abnormal response: " + response.toString());

                    logger.info("METADATA for " + TEST_FILE_NAME);

                    logger.info(metadata.toString());

                }
            }
        }

        return metadata;
    }

    private GeoFileDataSummary getSummary(SeismicProperties geoioDatasetProps) {
        GeoFileDataSummary summaryData = null;
        SeismicFileMetadata metadata = (SeismicFileMetadata)geoioDatasetProps.getMetadata();

        IMessagingManager msgMgr = mock1.getMessagingManager();

        ArrayList params = new ArrayList<String>();
        params.add(mock1.getMessagingManager().getLocationPref());
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(geoioDatasetProps);

        params.add(FileSystemConstants.TRACE_SUMMARY_DATA);

        String geoIORequestID = msgMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);


        if (geoIORequestID == null) {
            String errorMsg = "Unable to send " + QIWConstants.READ_GEOFILE_CMD;
            logger.warning(errorMsg);
            fail(errorMsg);
        } else {
            // wait for the response, which will come.
            // The time to read the data is indefinite because it depends on the
            // amount of data to be read. Wait up to 1/2 hour. Either all of
            // the data will have been read by then or an abnormal response is
            // returned because of an IO error.
            // TODO: update this with some sort of progress indicator
            IQiWorkbenchMsg response = msgMgr.getMatchingResponseWait(geoIORequestID, TIMEOUT);
            if (response == null) {
                String errorMsg = "Unable to get response of " + QIWConstants.READ_GEOFILE_CMD + " after " + TIMEOUT + " seconds.";
                logger.warning(errorMsg);
                fail(errorMsg);
            } else {
                if (response.isAbnormalStatus()) {
                    String errorMsg = "Unable to get TRACE_SUMMARY_DATA for datasetProperties " + geoioDatasetProps.toString();
                    logger.warning(errorMsg);
                    fail(errorMsg);
                }
                Object contentType = response.getContentType();
                Object content = response.getContent();

                if (QIWConstants.ARRAYLIST_TYPE.equals(contentType)) {
                    ArrayList contentList = (ArrayList) content;

                    //validate this is a response with the geoFile's metadata
                    String fileFormat = (String) contentList.get(0);
                    String dataType = (String) contentList.get(1);
                    if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                            !dataType.equals(FileSystemConstants.TRACE_SUMMARY_DATA)) {
                        String errorMsg = "Not a response containing geoFile TRACE_SUMMARY_DATA" + response;
                        logger.warning(errorMsg);
                        fail(errorMsg);
                    }

                    summaryData = (GeoFileDataSummary )contentList.get(2);
                    logger.fine("TRACE_SUMMARY_DATA for datasetProperties" + geoioDatasetProps);
                    logger.fine(summaryData.toString());

                    geoioDatasetProps.setSummary(summaryData);

                    logger.fine("Summary data set for dataSetProperties.");
                }
            }
        }
        return summaryData;
    }

    public GeoReadData getTraces(com.bhpb.geoio.filesystems.properties.SeismicProperties dsProps) {
        IMessagingManager msgMgr = mock1.getMessagingManager();

        ArrayList params = new ArrayList<String>();
        params.add(mock1.getLocationPref());
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(dsProps);
        params.add(FileSystemConstants.TRACE_DATA);
        params.add("0");
        params.add(new Integer(dsProps.getSummary().getNumberOfTraces()).toString());

        String geoIORequestID = msgMgr.sendRequest(QIWConstants.CMD_MSG,
            QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);


        GeoReadData result = null;

        if (geoIORequestID == null) {
            String errorMsg = "Unable to send " + QIWConstants.READ_GEOFILE_CMD;
            logger.warning(errorMsg);
        } else {
            // wait for the response, which will come.
            // The time to read the data is indefinite because it depends on the
            // amount of data to be read. Wait up to 1/2 hour. Either all of
            // the data will have been read by then or an abnormal response is
            // returned because of an IO error.
            // TODO: update this with some sort of progress indicator
            IQiWorkbenchMsg response = msgMgr.getMatchingResponseWait(geoIORequestID, TIMEOUT);
            if (response == null) {
                String errorMsg = "Unable to get response of " + QIWConstants.READ_GEOFILE_CMD + " after " + TIMEOUT + " seconds.";
                logger.warning(errorMsg);
            } else {
                if (response.isAbnormalStatus()) {
                    String errorMsg = "Unable to get TRACE_DATA for datasetProperties " + dsProps.toString();
                    logger.warning(errorMsg);
                }
                Object contentType = response.getContentType();
                Object content = response.getContent();

                if (QIWConstants.ARRAYLIST_TYPE.equals(contentType)) {
                    ArrayList contentList = (ArrayList) content;

                    //validate this is a response with the geoFile's metadata
                    String fileFormat = (String) contentList.get(0);
                    String dataType = (String) contentList.get(1);
                    if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                            !dataType.equals(FileSystemConstants.TRACE_DATA)) {
                        String errorMsg = "Not a response containing geoFile TRACE_SUMMARY_DATA" + response;
                        logger.warning(errorMsg);
                    }

                    //GeoFileDataSummary summaryData = (GeoFileDataSummary )contentList.get(2);
                    logger.fine("GeoReadData for datasetProperties" + dsProps);
                    result = (GeoReadData) contentList.get(2);

                    logger.fine("Trace data for datasetProperties successfully read.");
                }
            }
        }
        return result;
    }
}
