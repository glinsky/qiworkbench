package com.bhpb.qiworkbench.client.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.StringTokenizer;

import com.bhpb.geoio.filesystems.FileSystemConstants;
import com.bhpb.geoio.filesystems.geofileio.GeoReadData;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import com.bhpb.geoio.filesystems.properties.SeismicProperties;

import com.bhpb.qiworkbench.IqiMessageHandler;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IDataObject;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher_getResponseWait_Timeout_And_Response_Test;
import com.bhpb.testutils.MockQiComponent;
import com.bhpb.testutils.QiWorkbenchProxyFactory;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import junit.framework.TestCase;

public class GeoIOservice_getHorizon1Test extends TestCase {

    QiWorkbenchProxyFactory factory;
    MockQiComponent mock1;
    IqiMessageHandler comp1;
    private static final Logger logger = Logger.getLogger(MessageDispatcher_getResponseWait_Timeout_And_Response_Test.class.toString());
    protected static final String baseDir = System.getProperty("basedir");
    protected static final String baseTestFilePath = baseDir + File.separator + "test" + File.separator + "data" + File.separator;
    private static final String TEST_FILE_NAME =
            baseTestFilePath + "horizons.dat";
    private static final int TIMEOUT = 30 * 60 * 1000; //30 minutes
    private final String DEPTH_SAMPLES = "Horizons1Test_depth.xyz";

    @Override
    public void setUp() throws NoSuchMethodException, InterruptedException,
            IOException {
        factory = new QiWorkbenchProxyFactory();
        assertNotNull(factory);

        setUpMessageDispatcher();
        mock1 = setUpMockQiComponent();

        comp1 = setUpProxy(mock1);

        createDatFile();
    }

    /**
     * Creates a file named test/data/horizons.dat with a hardcoded path to the
     * test/data/horizHeaderTest directory, which is required by bhpsu when
     * used by the geoIOservice.
     */
    private void createDatFile() throws IOException {
        File datFile = new File(TEST_FILE_NAME);
        datFile.deleteOnExit();
        FileWriter writer = null;
        try {
            writer = new FileWriter(datFile);
            writer.write(baseTestFilePath + "horizHeaderTest");
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    public GeoIOservice_getHorizon1Test() {
    }

    private void setUpMessageDispatcher() {
        System.setProperty("deployServerURL", "http://localhost:8080");
        System.setProperty("install", "false");

        MessageDispatcher.mainNoGUI();

        MessageDispatcher dispatcher = MessageDispatcher.getInstance();
        assertTrue(dispatcher.isInitSuccessful());
    }

    private MockQiComponent setUpMockQiComponent() {
        MockQiComponent mock = new MockQiComponent();
        assertNotNull(mock);
        mock.isInitFinished(); // test mock functionality

        return mock;
    }

    private IqiMessageHandler setUpProxy(IqiMessageHandler component) throws NoSuchMethodException {
        Object proxy = factory.createProxy(component);
        assertNotNull(proxy);

        IqiMessageHandler proxyComponent = (IqiMessageHandler) proxy;
        proxyComponent.isInitFinished(); // test proxy invocation of mock functionality

        return (IqiMessageHandler) proxyComponent;
    }

    /** Test getting a horizon */
    public void testGetHorizon1() throws InterruptedException, NoSuchMethodException {
        mock1.startComponent("MOCK1");

        Thread.sleep(2000);
        mock1.setTomcatLoc("http://localhost:8080");
        mock1.setLocationPref(QIWConstants.REMOTE_PREF);

        ArrayList<String> remoteOSInfoList = mock1.getRemoteOSInfo(QIWConstants.REMOTE_PREF);
        MessageDispatcher.getInstance().setRemoteOSInfoList(remoteOSInfoList);

        //TODO this should either get the same lock as MockQiComponent.init() or else getCID() should be synchronized on that lock
        logger.info("Mock1.getCID() = " + comp1.getCID());

        IComponentDescriptor cd1 = MessageDispatcher.getInstance().getComponentDesc(comp1.getCID());

        assertNotNull("md.getComponentDesc() returned null comp descriptor for CID: " + comp1.getCID(), cd1);

        SeismicFileMetadata metadata = getMetadata();
        HorizonProperties geoioDatasetProps = new HorizonProperties(metadata);
        geoioDatasetProps.setSelectedHorizon("depth");

        GeoFileDataSummary summary = getSummary(geoioDatasetProps);

        assertNotNull(summary);

        geoioDatasetProps.setSummary(summary);

        logger.info("Summary data: ");
        logger.info(summary.toString());

        GeoReadData geoReadData = getHorizon(geoioDatasetProps);

        //assertTrue("summary data does not have the correct test values for file: " + TEST_FILE_NAME, hasCorrectTestValues(summary));
        assertNotNull(geoReadData);

        //assertTrue(geoReadData.isAllDataRead());
        long startTime = System.currentTimeMillis();
        while (!geoReadData.isAllDataRead() && System.currentTimeMillis() - startTime < 10000) {
            Thread.sleep(1000);
        }

        if (!geoReadData.isAllDataRead()) {
            fail("Timed out waiting to read horizon '" + metadata.getHorizons().get(0) +
                    "'data from: " + metadata.getGeoFilePath());
        }

        int numHorizons = 0;
        List<IDataObject> depthHorizons = new LinkedList<IDataObject>();

        while (geoReadData.hasMoreRecords()) {
            depthHorizons.add(geoReadData.nextRecord());
            numHorizons++;
        }

        logger.info("Total number of horizons read: " + numHorizons);
        logger.info("processed: " + geoReadData.getProcessedRecords());
        logger.info("remaining: " + geoReadData.getRemainingRecords());

        //9801 horizon 'traces' read
        assertEquals(9801, depthHorizons.size());
        //each horizon has 1 sample
        assertEquals(1, summary.getSamplesPerTrace());

        String line = "";
        float[] samples = new float[depthHorizons.size()];
        for (int i = 0; i < depthHorizons.size(); i++) {
            samples[i] = depthHorizons.get(i).getFloatVector()[0];
        }
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath + DEPTH_SAMPLES));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            line = ebr.readLine();

            int cnt = 0;
            int numSamples = samples.length;
            float val = 0;

            // compare actual and expected results
            while (line != null && cnt < numSamples) {
                StringTokenizer st = new StringTokenizer(line);
                //extract horizon (3rd token) as a float
                st.nextToken();
                st.nextToken();
                String sample = st.nextToken();
                val = 0;
                try {
                    val = Float.parseFloat(sample);
                } catch (NumberFormatException nfe) {
                    fail("Unable to parse line third token of line #" + cnt + " as int due to: " + nfe.getMessage());
                }
                //System.out.println("expected=\""+samples[cnt]+"\"");
                //System.out.println("actual=\""+val+"\"");
                assertEquals(DEPTH_SAMPLES + ": Samples are incorrect at line: " + cnt, samples[cnt], val);
                line = ebr.readLine();
                cnt++;
            }
            if (cnt != numSamples) {
                fail("More actual samples than expected: actual=" + val);
            }
            if (line != null) {
                fail("More expected samples than actual: expected=" + line);
            }
        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected horizon samples file: " + DEPTH_SAMPLES + "; error message: " + fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error reading expected horizon samples: " + DEPTH_SAMPLES + "; error message: " + ioe.getMessage());
        }
    }

    private SeismicFileMetadata getMetadata() {
        SeismicFileMetadata metadata = null;

        logger.info("Getting metadata for " + TEST_FILE_NAME);
        ArrayList<String> params = new ArrayList<String>();
        params.add(QIWConstants.REMOTE_SERVICE_PREF);
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(TEST_FILE_NAME);
        params.add(FileSystemConstants.FILE_METADATA_DATA);

        IMessagingManager msgMgr = mock1.getMessagingManager();
        String geoIORequestID = msgMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);

        if (geoIORequestID == null) {
            String errorMsg = "Unable to send " + QIWConstants.READ_GEOFILE_CMD;
            fail(errorMsg);
        } else {
            // wait for the response, which will come.
            // The time to read the data is indefinite because it depends on the
            // amount of data to be read. Wait up to 1/2 hour. Either all of
            // the data will have been read by then or an abnormal response is
            // returned because of an IO error.
            // TODO: update this with some sort of progress indicator
            IQiWorkbenchMsg response = msgMgr.getMatchingResponseWait(geoIORequestID, TIMEOUT);
            if (response == null) {
                String errorMsg = "Unable to get response of " + QIWConstants.READ_GEOFILE_CMD + " after " + TIMEOUT + " seconds.";
                logger.warning(errorMsg);
                fail(errorMsg);
            } else {
                if (response.isAbnormalStatus()) {
                    String errorMsg = "Unable to get metadata for geoFile " + TEST_FILE_NAME;
                    fail(errorMsg);
                }
                Object contentType = response.getContentType();
                Object content = response.getContent();

                if (QIWConstants.ARRAYLIST_TYPE.equals(contentType)) {
                    ArrayList contentList = (ArrayList) content;

                    //validate this is a response with the geoFile's metadata
                    String fileFormat = (String) contentList.get(0);
                    String dataType = (String) contentList.get(1);
                    if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                            !dataType.equals(FileSystemConstants.FILE_METADATA_DATA)) {
                        String errorMsg = "Not a response containing geoFile metadata" + response;
                        logger.warning(errorMsg);
                        fail(errorMsg);
                    }

                    metadata = (SeismicFileMetadata) contentList.get(2);

                    assertNotNull("Got NULL metadata from non-null, non-abnormal response: " + response.toString());

                    logger.info("METADATA for " + TEST_FILE_NAME);

                    logger.info(metadata.toString());

                }
            }
        }

        return metadata;
    }

    private GeoFileDataSummary getSummary(SeismicProperties geoioDatasetProps) {
        GeoFileDataSummary summaryData = null;
        //SeismicFileMetadata metadata = (SeismicFileMetadata)geoioDatasetProps.getMetadata();

        IMessagingManager msgMgr = mock1.getMessagingManager();

        ArrayList params = new ArrayList<String>();
        params.add(mock1.getMessagingManager().getLocationPref());
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(geoioDatasetProps);

        params.add(FileSystemConstants.TRACE_SUMMARY_DATA);

        String geoIORequestID = msgMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);


        if (geoIORequestID == null) {
            String errorMsg = "Unable to send " + QIWConstants.READ_GEOFILE_CMD;
            logger.warning(errorMsg);
            fail(errorMsg);
        } else {
            // wait for the response, which will come.
            // The time to read the data is indefinite because it depends on the
            // amount of data to be read. Wait up to 1/2 hour. Either all of
            // the data will have been read by then or an abnormal response is
            // returned because of an IO error.
            // TODO: update this with some sort of progress indicator
            IQiWorkbenchMsg response = msgMgr.getMatchingResponseWait(geoIORequestID, TIMEOUT);
            if (response == null) {
                String errorMsg = "Unable to get response of " + QIWConstants.READ_GEOFILE_CMD + " after " + TIMEOUT + " seconds.";
                logger.warning(errorMsg);
                fail(errorMsg);
            } else {
                if (response.isAbnormalStatus()) {
                    String errorMsg = "Unable to get TRACE_SUMMARY_DATA for datasetProperties " + geoioDatasetProps.toString();
                    logger.warning(errorMsg);
                    fail(errorMsg);
                }
                Object contentType = response.getContentType();
                Object content = response.getContent();

                if (QIWConstants.ARRAYLIST_TYPE.equals(contentType)) {
                    ArrayList contentList = (ArrayList) content;

                    //validate this is a response with the geoFile's metadata
                    String fileFormat = (String) contentList.get(0);
                    String dataType = (String) contentList.get(1);
                    if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                            !dataType.equals(FileSystemConstants.TRACE_SUMMARY_DATA)) {
                        String errorMsg = "Not a response containing geoFile TRACE_SUMMARY_DATA" + response;
                        logger.warning(errorMsg);
                        fail(errorMsg);
                    }

                    summaryData = (GeoFileDataSummary) contentList.get(2);
                    logger.fine("TRACE_SUMMARY_DATA for datasetProperties" + geoioDatasetProps);
                    logger.fine(summaryData.toString());

                    geoioDatasetProps.setSummary(summaryData);

                    logger.fine("Summary data set for dataSetProperties.");
                }
            }
        }
        return summaryData;
    }

    public GeoReadData getHorizon(com.bhpb.geoio.filesystems.properties.SeismicProperties dsProps) {
        IMessagingManager msgMgr = mock1.getMessagingManager();

        ArrayList params = new ArrayList<String>();
        params.add(mock1.getLocationPref());
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(dsProps);
        params.add(FileSystemConstants.HORIZON_DATA);
        params.add("depth");
        params.add("0");

        String geoIORequestID = msgMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);


        GeoReadData result = null;

        if (geoIORequestID == null) {
            String errorMsg = "Unable to send " + QIWConstants.READ_GEOFILE_CMD;
            logger.warning(errorMsg);
        } else {
            // wait for the response, which will come.
            // The time to read the data is indefinite because it depends on the
            // amount of data to be read. Wait up to 1/2 hour. Either all of
            // the data will have been read by then or an abnormal response is
            // returned because of an IO error.
            // TODO: update this with some sort of progress indicator
            IQiWorkbenchMsg response = msgMgr.getMatchingResponseWait(geoIORequestID, TIMEOUT);
            if (response == null) {
                String errorMsg = "Unable to get response of " + QIWConstants.READ_GEOFILE_CMD + " after " + TIMEOUT + " seconds.";
                logger.warning(errorMsg);
            } else {
                if (response.isAbnormalStatus()) {
                    String errorMsg = "Unable to get HORIZON_DATA for datasetProperties " + dsProps.toString();
                    logger.warning(errorMsg);
                }
                Object contentType = response.getContentType();
                Object content = response.getContent();

                if (QIWConstants.ARRAYLIST_TYPE.equals(contentType)) {
                    ArrayList contentList = (ArrayList) content;

                    //validate this is a response with the geoFile's metadata
                    String fileFormat = (String) contentList.get(0);
                    String dataType = (String) contentList.get(1);
                    if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                            !dataType.equals(FileSystemConstants.HORIZON_DATA)) {
                        String errorMsg = "Not a response containing geoFile horizon" + response;
                        logger.warning(errorMsg);
                    }

                    logger.fine("GeoReadData for datasetProperties" + dsProps);
                    result = (GeoReadData) contentList.get(2);

                    logger.fine("Horizon data for datasetProperties successfully read.");
                }
            }
        }
        return result;
    }
}
