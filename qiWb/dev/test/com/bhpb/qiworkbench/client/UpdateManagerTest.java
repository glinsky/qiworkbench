/*
 * UpdateManagerTest.java
 * JUnit based test
 *
 * Created on January 31, 2008, 1:16 PM
 */

package com.bhpb.qiworkbench.client;

import junit.framework.TestCase;

/**
 *
 * @author folsw9
 */
public class UpdateManagerTest extends TestCase {
    
    public UpdateManagerTest(String testName) {
        super(testName);
    }

    protected void setUp() throws Exception {
    }

    protected void tearDown() throws Exception {
    }

    public void testIsComponentCompatible() {
    //These test scenarios are taken directly from the isComponentCompatible JavaDoc
    assertTrue(UpdateManager.isVersionCompatible("1.2.3", "1.3.0")); //required version is lower
    assertTrue(UpdateManager.isVersionCompatible("4.5.6", "4.5.6")); //exact version match
    assertTrue(UpdateManager.isVersionCompatible("7.0", "7.0.2")); //required major and minor version match, specific point release not required
    assertTrue(UpdateManager.isVersionCompatible("8.9.0", "8.9.0")); //exact version match with qiWb version 8.9, inferred point release 0
    
    assertFalse(UpdateManager.isVersionCompatible("1.2.3", "1.2.2")); //required version is higher
    assertFalse(UpdateManager.isVersionCompatible("4.5.6", "4.5")); //required version is not compatible with qiWb version 4.5, inferred point release 0       
    }
}