/*
 * PreferenceUtilsTest.java
 * JUnit based test
 *
 * Created on September 13, 2007, 1:43 PM
 */

package com.bhpb.qiworkbench.client.util;

import java.io.File;
import com.bhpb.qiworkbench.QiwbPreferences;
import com.bhpb.qiworkbench.QiwIOException;
import junit.framework.TestCase;

/**
 *
 * @author folsw9
 */
public class PreferenceUtilsTest extends TestCase {
    
    String baseDir = System.getProperty("basedir");
    String baseTestFilePath = baseDir + File.separator + "test" + File.separator + "data" + File.separator;
    
    public PreferenceUtilsTest(String testName) {
        super(testName);
    }

    protected void setUp() throws Exception {
    }

    protected void tearDown() throws Exception {
    }

    public void testPrefFileExists() {
        assertTrue("File qiwbPreferences_v12.xml does not exist", PreferenceUtils.prefFileExists(baseTestFilePath, "qiwbPreferences_v12.xml"));
        assertTrue("File qiwbPreferences_v12.xml does not exist", PreferenceUtils.prefFileExists(baseTestFilePath, "qiwbPreferences_v12.xml"));

        assertTrue("File qiwbPreferences_v12.xml does not exist", PreferenceUtils.prefFileExists(baseTestFilePath));
        
        assertFalse("Incorrectly detected existance of non-existent file", PreferenceUtils.prefFileExists(baseTestFilePath, "NO_SUCH_FILE"));
    }

    public void testWritePrefs() throws Exception {
        String tempDirectoryName = baseDir + File.separator + "temp";
        File tempDirectory = new File(tempDirectoryName);
        tempDirectory.mkdir();
        
        if (tempDirectory.exists()) {
            QiwbPreferences prefs = PreferenceUtils.readPrefs(baseTestFilePath, "qiwbPreferences_v13.xml");
            PreferenceUtils.writePrefs(prefs, tempDirectoryName);
        
            String tempFileName = tempDirectoryName + File.separator + "qiwbPreferences.xml";
        
            File tempFile = new File(tempFileName);
        
            if (tempFile.exists()) {
                tempFile.delete();
            } else {
                fail("Failed serialize qiwbpreferences.xml file");
            }
        
            if (tempDirectory.exists())
                tempDirectory.delete();
            }
    }

    public void testReadPrefsV12() throws Exception {
        try {
            QiwbPreferences prefs = PreferenceUtils.readPrefs(baseTestFilePath, "qiwbPreferences_v12.xml");
            fail("reading qiwbpreferences.xml created by qiWorkbench v.1.2 did not throw QiwIOException");
        } catch (QiwIOException qiwIOException) {
            //do nothing but consume exception so that test case passes
        }
    }
    
    public void testReadPrefsV13() throws Exception {
        QiwbPreferences prefs = PreferenceUtils.readPrefs(baseTestFilePath, "qiwbPreferences_v13.xml");
        assertNotNull("PreferenceUtils.readPrefs(String baseDir, String fileName) did not throw an exception or return a non-null QiwbPreferences object", prefs);
    }
}
