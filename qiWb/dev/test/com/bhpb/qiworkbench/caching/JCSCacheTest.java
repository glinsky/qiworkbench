/*
 * Test the Cache Manager backed by JCS (Java Caching System)
 */

package com.bhpb.qiworkbench.caching;


import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.datasystems.BhpSuHorizonDataObject;
import com.bhpb.geoio.datasystems.SegyTraceDataObject;
import com.bhpb.geoio.datasystems.WellLogDataObject;

import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Gil Hansen
 */
public class JCSCacheTest {
    CacheManager cacheMgr = null;

    @Before
    public void setUp() {
        //Configure logging
        MessageDispatcher.configLogging();

        //initialize the cache manager with region caches
        cacheMgr = CacheManager.getInstance();
        assertNotNull("Could not create Cache Manager.", cacheMgr);
    }

    @After
    public void tearDown() {
        cacheMgr = null;
    }

    @Test
    public void testTraceCacheInsertion() {
        String regionName = CacheManagerConstants.TRACE_REGION_NAME;

        DataObject pojo = new SegyTraceDataObject();
        pojo.setDoID("segyTrace1");
        cacheMgr.insert("segyTrace1", regionName, pojo);

        pojo = new SegyTraceDataObject();
        pojo.setDoID("segyTrace2");
        cacheMgr.insert("segyTrace2", regionName, pojo);

        pojo = new SegyTraceDataObject();
        pojo.setDoID("segyTrace3");
        cacheMgr.insert("segyTrace3", regionName, pojo);

        pojo = new SegyTraceDataObject();
        pojo.setDoID("segyTrace4");
        cacheMgr.insert("segyTrace4", regionName, pojo);

        pojo = new SegyTraceDataObject();
        pojo.setDoID("segyTrace5");
        cacheMgr.insert("segyTrace5", regionName, pojo);

        pojo = new SegyTraceDataObject();
        pojo.setDoID("segyTrace6");
        cacheMgr.insert("segyTrace6", regionName, pojo);

        int size = cacheMgr.getCacheSize(regionName);
        assertEquals("Failed to cache all traces.", 6, size);
    }

    @Ignore ("Broken for 37 builds as of 11/17/2008 due to NPE")
    @Test
    public void testHorizonCacheInsertion() {
        String regionName = CacheManagerConstants.HORIZON_REGION_NAME;

        DataObject pojo = new BhpSuHorizonDataObject();
        pojo.setDoID("horizon1");
        cacheMgr.insert("horizon1", regionName, pojo);

        pojo = new BhpSuHorizonDataObject();
        pojo.setDoID("horizon2");
        cacheMgr.insert("horizon2", regionName, pojo);

        pojo = new BhpSuHorizonDataObject();
        pojo.setDoID("horizon3");
        cacheMgr.insert("horizon3", regionName, pojo);

        pojo = new BhpSuHorizonDataObject();
        pojo.setDoID("horizon4");
        cacheMgr.insert("horizon4", regionName, pojo);

        pojo = new BhpSuHorizonDataObject();
        pojo.setDoID("horizon5");
        cacheMgr.insert("horizon5", regionName, pojo);

        int size = cacheMgr.getCacheSize(regionName);
        assertEquals("Failed to cache all horizons.", 5, size);
    }

    @Test
    public void testWellLogCacheInsertion() {
        String regionName = CacheManagerConstants.WELL_LOG_REGION_NAME;

        DataObject pojo = new WellLogDataObject();
        pojo.setDoID("wellLog1");
        cacheMgr.insert("wellLog1", regionName, pojo);

        pojo = new WellLogDataObject();
        pojo.setDoID("wellLog2");
        cacheMgr.insert("wellLog2", regionName, pojo);

        pojo = new WellLogDataObject();
        pojo.setDoID("wellLog3");
        cacheMgr.insert("wellLog3", regionName, pojo);

        pojo = new WellLogDataObject();
        pojo.setDoID("wellLog4");
        cacheMgr.insert("wellLog4", regionName, pojo);

        int size = cacheMgr.getCacheSize(regionName);
        assertEquals("Failed to cache all well logs.", 4, size);
    }

    @Test
    public void testTraceCacheFetch() {
        String regionName = CacheManagerConstants.TRACE_REGION_NAME;

        DataObject pojo = (SegyTraceDataObject)cacheMgr.lookup("segyTrace1", regionName);
        assertEquals("Failed to find trace in cache", "segyTrace1", pojo.getDoID());

        pojo = (SegyTraceDataObject)cacheMgr.lookup("segyTrace2", regionName);
        assertEquals("Failed to find trace in cache", "segyTrace2", pojo.getDoID());

        pojo = (SegyTraceDataObject)cacheMgr.lookup("segyTrace3", regionName);
        assertEquals("Failed to find trace in cache", "segyTrace3", pojo.getDoID());

        pojo = (SegyTraceDataObject)cacheMgr.lookup("segyTrace4", regionName);
        assertEquals("Failed to find trace in cache", "segyTrace4", pojo.getDoID());

        pojo = (SegyTraceDataObject)cacheMgr.lookup("segyTrace5", regionName);
        assertEquals("Failed to find trace in cache", "segyTrace5", pojo.getDoID());

        pojo = (SegyTraceDataObject)cacheMgr.lookup("segyTrace6", regionName);
        assertEquals("Failed to find trace in cache", "segyTrace6", pojo.getDoID());
    }

    @Ignore ("Broken for 37 builds as of 11/17/2008 due to NPE")
    @Test
    public void testHorizonCacheFetch() {
        String regionName = CacheManagerConstants.HORIZON_REGION_NAME;

        DataObject pojo = (BhpSuHorizonDataObject)cacheMgr.lookup("horizon1", regionName);
        assertEquals("Failed to find horizon in cache", "horizon1", pojo.getDoID());

        pojo = (BhpSuHorizonDataObject)cacheMgr.lookup("horizon2", regionName);
        assertEquals("Failed to find horizon in cache", "horizon2", pojo.getDoID());

        pojo = (BhpSuHorizonDataObject)cacheMgr.lookup("horizon3", regionName);
        assertEquals("Failed to find horizon in cache", "horizon3", pojo.getDoID());

        pojo = (BhpSuHorizonDataObject)cacheMgr.lookup("horizon4", regionName);
        assertEquals("Failed to find horizon in cache", "horizon4", pojo.getDoID());

        pojo = (BhpSuHorizonDataObject)cacheMgr.lookup("horizon5", regionName);
        assertEquals("Failed to find horizon in cache", "horizon5", pojo.getDoID());
    }

    @Test
    public void testWellLogCacheFetch() {
        String regionName = CacheManagerConstants.WELL_LOG_REGION_NAME;

        DataObject pojo = (WellLogDataObject)cacheMgr.lookup("wellLog1", regionName);
        assertEquals("Failed to find well log in cache", "wellLog1", pojo.getDoID());

        pojo = (WellLogDataObject)cacheMgr.lookup("wellLog2", regionName);
        assertEquals("Failed to find well log in cache", "wellLog2", pojo.getDoID());

        pojo = (WellLogDataObject)cacheMgr.lookup("wellLog3", regionName);
        assertEquals("Failed to find well log in cache", "wellLog3", pojo.getDoID());

        pojo = (WellLogDataObject)cacheMgr.lookup("wellLog4", regionName);
        assertEquals("Failed to find well log in cache", "wellLog4", pojo.getDoID());
    }

    @Ignore ("Broken for 37 builds as of 11/17/2008 due to NPE")
    @Test
    public void testRemoveItem() {
        String regionName = CacheManagerConstants.TRACE_REGION_NAME;
        boolean removed = cacheMgr.remove("segyTrace4", regionName);
        assertEquals("Failed to remove segyTrace4", true, removed);
        int size = cacheMgr.getCacheSize(regionName);
        assertEquals("Failed to remove segyTrace4", 5, size);

        regionName = CacheManagerConstants.HORIZON_REGION_NAME;
        removed = cacheMgr.remove("horizon3", regionName);
        assertEquals("Failed to remove horizon3", true, removed);
        size = cacheMgr.getCacheSize(regionName);
        assertEquals("Failed to remove horizon3", 4, size);

        regionName = CacheManagerConstants.WELL_LOG_REGION_NAME;
        removed = cacheMgr.remove("wellLog2", regionName);
        assertEquals("Failed to remove wellLog2", true, removed);
        size = cacheMgr.getCacheSize(regionName);
        assertEquals("Failed to remove wellLog2", 3, size);
    }

    @Ignore ("Broken for 37 builds as of 11/17/2008 due to NPE")
    @Test
    public void testFreeItems() {
        String regionName = CacheManagerConstants.TRACE_REGION_NAME;
        int freed = cacheMgr.freeMemoryCache(2, regionName);
        assertEquals("Failed to remove 2 traces", 2, freed);
        int size = cacheMgr.getCacheSize(regionName);
        assertEquals("Failed to remove remove 2 traces", 3, size);

        regionName = CacheManagerConstants.HORIZON_REGION_NAME;
        freed = cacheMgr.freeMemoryCache(2, regionName);
        assertEquals("Failed to remove 2 horizons", 2, freed);
        size = cacheMgr.getCacheSize(regionName);
        assertEquals("Failed to remove 2 horizons", 2, size);

        regionName = CacheManagerConstants.WELL_LOG_REGION_NAME;
        freed = cacheMgr.freeMemoryCache(2, regionName);
        assertEquals("Failed to remove 2 well logs", 2, freed);
        size = cacheMgr.getCacheSize(regionName);
        assertEquals("Failed to remove 2 well logs", 1, size);
    }

    @Test
    public void testTraceCachePurge() {
        String regionName = CacheManagerConstants.TRACE_REGION_NAME;

        boolean purged = cacheMgr.purge(regionName);
        int size = cacheMgr.getCacheSize(regionName);
        assertEquals("Failed to purge trace cache", 0, size);
    }

    @Test
    public void testHorizonCachePurge() {
        String regionName = CacheManagerConstants.HORIZON_REGION_NAME;

        boolean purged = cacheMgr.purge(regionName);
        int size = cacheMgr.getCacheSize(regionName);
        assertEquals("Failed to purge horizon cache", 0, size);
    }

    @Test
    public void testWellLogCachePurge() {
        String regionName = CacheManagerConstants.WELL_LOG_REGION_NAME;

        boolean purged = cacheMgr.purge(regionName);
        int size = cacheMgr.getCacheSize(regionName);
        assertEquals("Failed to purge well log cache", 0, size);
    }

    @Ignore ("Broken for 37 builds as of 11/17/2008 due to NPE")
    @Test
    public void testCachePurge() {
        testTraceCacheInsertion();
        testHorizonCacheInsertion();
        testWellLogCacheInsertion();
        cacheMgr.purge();

        String regionName = CacheManagerConstants.TRACE_REGION_NAME;
        int size = cacheMgr.getCacheSize(regionName);
        regionName = CacheManagerConstants.HORIZON_REGION_NAME;
        size += cacheMgr.getCacheSize(regionName);
        regionName = CacheManagerConstants.WELL_LOG_REGION_NAME;
        size += cacheMgr.getCacheSize(regionName);

        assertEquals("Failed to purge entire cache", 0, size);
    }
}