/*
 * QiComponentProxy.java
 *
 * Created on November 13, 2007, 3:41 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.testutils;

import com.bhpb.qiworkbench.IqiMessageHandler;
import com.bhpb.testutils.QiWorkbenchProxyFactory.ProxyAction;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 */
public class QiComponentProxy implements InvocationHandler {
    private final Object obj;
    private static final Logger logger = Logger.getLogger(QiComponentProxy.class.toString());
    private final Map<Method, List<QiWorkbenchProxyFactory.ProxyAction>> proxyActionsByMethod;
    
    private QiComponentProxy(IqiMessageHandler realComponent) {
        this.obj = realComponent;
        proxyActionsByMethod = new HashMap<Method, List<QiWorkbenchProxyFactory.ProxyAction>>();
    }
    
    public static synchronized Object proxyFor(IqiMessageHandler realComponent) {
        Class objClass = realComponent.getClass();
        return Proxy.newProxyInstance(objClass.getClassLoader(),
                objClass.getInterfaces(),
                new QiComponentProxy(realComponent));
    }
    
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            logger.info("In QiComponentProxy.invoke, proxyActionsByMethod has " + proxyActionsByMethod.size() + " elements.");
            
            if (proxyActionsByMethod.size() > 0)
                logger.info("Key 0: " + proxyActionsByMethod.keySet().toArray()[0].toString());
            
            List proxyActionList = proxyActionsByMethod.get(method);
            Object returnValue = null;
            boolean invokeMethod = true;
            boolean overrideReturnValue = false;
            if (proxyActionList != null) {
                logger.info("ProxyActionList for method " + method + " has " + proxyActionList.size() + " elements");
                Iterator<ProxyAction> iter = proxyActionList.iterator();
                while (iter.hasNext()) {
                    ProxyAction action = iter.next();
                    if (action instanceof QiWorkbenchProxyFactory.ProxyDelay) {
                        QiWorkbenchProxyFactory.ProxyDelay delayAction = (QiWorkbenchProxyFactory.ProxyDelay) action;
                        logger.info("ProxyDelay action invoked - sleeping for " + delayAction.getDelayTime() + "ms");
                        Thread.sleep(delayAction.getDelayTime());
                    } else if (action instanceof QiWorkbenchProxyFactory.ProxyReturnOverride) {
                        QiWorkbenchProxyFactory.ProxyReturnOverride overrideAction = (QiWorkbenchProxyFactory.ProxyReturnOverride) action;
                        //only the last ProxyReturnOverride action determines the return value and whether method invocation actually occurs
                        returnValue = overrideAction.getReturnValue();
                        invokeMethod = overrideAction.isInvocationPerformed();
                        overrideReturnValue = true;
                    } else {
                        throw new IllegalArgumentException("InvokeProxyAction not yet implemented for type: " + action.getClass().getName());
                    }
                }
            } else {
                logger.info("ProxyActionList for method " + method + " is null");
            }
            
            Object methodReturnValue = null;
            
            if (overrideReturnValue) {
                if (invokeMethod)
                    method.invoke(obj, args);
                return returnValue;
            } else {
                return method.invoke(obj, args);
            }
            
        } catch (InvocationTargetException e) {
            throw e.getTargetException();
        }
    }
    
    protected void addProxyAction(QiWorkbenchProxyFactory.ProxyAction action, String methodName, Class... parameterTypes) 
        throws NoSuchMethodException
    {
        Method method = IqiMessageHandler.class.getMethod(methodName, parameterTypes);
        List<QiWorkbenchProxyFactory.ProxyAction> proxyActionList = proxyActionsByMethod.get(method);
        
        if (proxyActionList == null) {
            proxyActionList = new ArrayList<QiWorkbenchProxyFactory.ProxyAction>();
            proxyActionsByMethod.put(method, proxyActionList);
        }
        
        proxyActionList.add(action);
        
        logger.info("Added action " + action + " to proxyActionList for method " + method.getName());
        logger.info("Method " + method + " now has " + proxyActionsByMethod.get(method).size() + " proxy actions.");
    }
    
    protected void removeProxyActions(String methodName, Class... parameterTypes) 
        throws NoSuchMethodException
    {
        Method method = obj.getClass().getMethod(methodName, parameterTypes);
        
        List newList = new ArrayList<QiWorkbenchProxyFactory.ProxyAction>();
        
        proxyActionsByMethod.put(method, newList);
    }
}
