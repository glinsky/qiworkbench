/*
 * QiWorkbenchProxyFactoryTest.java
 *
 * Created on November 13, 2007, 3:33 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.testutils;

import com.bhpb.qiworkbench.IqiMessageHandler;
import java.lang.reflect.Proxy;
import junit.framework.TestCase;

/**
 *
 * @author folsw9
 */
public class QiWorkbenchProxyFactory_Test extends TestCase {
    private QiWorkbenchProxyFactory factory = null;
    
    /** Creates a new instance of QiWorkbenchProxyFactoryTest */
    public QiWorkbenchProxyFactory_Test() {
    }
    
    public void setUp() {
        factory = new QiWorkbenchProxyFactory();
        assertNotNull(factory);
    }
    
    public void tearDown() {
        
    }
    
    public void testCreateProxy() {
        MockQiComponent mock = new MockQiComponent();
        assertNotNull(mock);
        Object proxy = factory.createProxy(mock);
        assertNotNull(proxy);
        assertTrue(Proxy.isProxyClass(proxy.getClass()));
    }
    
    public void testAssignProxyAction() 
        throws NoSuchMethodException
    {
        MockQiComponent mock = new MockQiComponent();
        assertNotNull(mock);
        mock.isInitFinished(); // test mock functionality
        
        Object proxy = factory.createProxy(mock);
        assertNotNull(proxy);
        IqiMessageHandler component = (IqiMessageHandler) proxy;
        component.isInitFinished(); // test proxy invocation of mock functionality
        
        factory.addDelay(proxy, 1000, "isInitFinished");
        component.isInitFinished(); // test proxy invocation of mock with proxy delay action
    }
    
    public void testRemoveProxyAction() 
        throws NoSuchMethodException
    {
        MockQiComponent mock = new MockQiComponent();
        assertNotNull(mock);
        Object proxy = factory.createProxy(mock);
        assertNotNull(proxy);
        
        factory.removeProxyActions(proxy, "isInitFinished");
    }
}
