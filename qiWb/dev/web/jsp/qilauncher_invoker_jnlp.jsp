<%@ page
    contentType="application/x-java-jnlp-file"
    info="SDA JNLP"
%>
<%@ page import="java.util.Properties, java.io.InputStream, java.io.IOException, anttasks.createWorkbenchBuild" %>
<%  String href = "";
    StringBuffer codebaseBuffer = new StringBuffer();

    codebaseBuffer.append("http://");
    codebaseBuffer.append(request.getServerName());
    codebaseBuffer.append(':');
    codebaseBuffer.append(request.getServerPort());
 
    String deployServerURL = codebaseBuffer.toString();

    href = codebaseBuffer.toString() + request.getRequestURI();

    response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

    String uri = request.getRequestURI();
    int index = uri.lastIndexOf("/");
    if ( index > 0) {
        codebaseBuffer.append( uri.substring(0, index) );
    }
    codebaseBuffer.append('/'); %>
<%  Properties props = new Properties();
    Class me = createWorkbenchBuild.class;
    InputStream in = me.getResourceAsStream("/version.properties");
    try {
        props.load(in);
    }catch (IOException ioe) {
    //logger.warning("IO exception reading version.properties file:" + ioe.getMessage());
    } finally {
        try {
            if (in != null) in.close();
        } catch (IOException e) {}
    }
    String versionProp = props.getProperty("project.version");
    String providerProp = props.getProperty("project.provider");
    String workbenchJar = "../lib/qiWorkbench.jar"; %>
<%  java.util.Enumeration params = request.getParameterNames();
    String hrefParams = ""; //Parameters placed at the end of href so parameters can be read in JWS
    String propertyParams = ""; // Creates a string of property declarations based off passed parameters
    boolean atleastOneElement = false;
    while (params.hasMoreElements())
    {
        atleastOneElement = true;
        String paramName = (String) params.nextElement();
        String paramValues[] = request.getParameterValues(paramName);
        hrefParams = paramName + "=" + paramValues[0] + hrefParams;
        if(params.hasMoreElements()) {
            hrefParams = "&" + hrefParams;
        }
        propertyParams = "<property name=\"" + paramName + "\" value=\"" + paramValues[0] + "\" />" + propertyParams;
    }
    if(atleastOneElement) {
        hrefParams = "?" + hrefParams;
    }
%>
<?xml version="1.0" encoding="UTF-8"?>
<jnlp codebase="<%= codebaseBuffer.toString() %>"
          href="<%= href %><%= hrefParams %>">
    <information>
        <title>qiWorkbench v<%= versionProp %></title>
        <vendor><%= providerProp %></vendor>
        <description>Seismic toolkit for Quantitative Interpretation</description>
        <offline-allowed/>
        <icon href="images/QIW_icon.gif"/>
        <icon href="images/QIW_splash.gif" kind="splash"/>
    </information>
    <security>
        <all-permissions/>
    </security>
    <resources>
        <j2se version="1.5+" />
        <!--j2se version="1.5+" /-->
        <jar href="<%= workbenchJar %>"/>
	<!-- Access property parameters with System.getProperty("param") -->
	<%= propertyParams %>
	<property name=href value=<%= href %> />
        <property name=codebaseBuffer value=<%= codebaseBuffer.toString() %> />
    </resources>
    <application-desc main-class="com.bhpb.qiworkbench.util.ClientCapabilityDetector"/>
</jnlp>
