<%@page import="java.util.Properties, java.io.InputStream, java.io.IOException, anttasks.createWorkbenchBuild"%>
<script type="text/javascript">
    var tomcatURL = "";
    var tomcatLoc = "local";
    var projectRoot = "";
    var qiLauncher = "qilauncher_invoker_jnlp.jsp";
    var install = "?install=false";
    var launchLink = "";
    
    var demo1Project = "demo";
    var demo1Session = "workbenches/glinsky_met_demo_workbench.cfg";
    var demo2Project = "demo";
    var demo2Session = "workbenches/meg_demo_workbench_3.cfg";
    
    function getProjRoot(form) {
        projectRoot = form.projRoot.value;
        if (projectRoot == "") {
            alert("Project qiSpace cannot be empty");
            form.projRoot.focus();
        } 
    }
    
    function getTomcatUrl(form) {
        tomcatURL = form.tomcatUrl.value;
        var re = /:/g
        if (tomcatURL == "") {
            alert("Tomcat URL cannot be empty");
            form.tomcatUrl.focus();
        } else tomcatURL.replace(re, "%3A"); 
    }
    
    function getTomcatLoc(form) {
        if (form.rad[0].checked) tomcatLoc = "local";
        else tomcatLoc = "remote";
    }
    
    function validateParams(form) {
        getProjRoot(form);
        getTomcatUrl(form);
    }
    
    var pos1 = -1;
    var pos2 = -1;
    
    function genLinks() { 
        launchLink = "";
        launchLink += qiLauncher;
        launchLink += install;
        launchLink += "&runtimeURL=" + tomcatURL;
        launchLink += "&serverLoc=" + tomcatLoc;
        
        var launchLink1 = launchLink;
        var launchLink2 = launchLink;
        
        launchLink1 += "&project=" + projectRoot + "/" + demo1Project;
        launchLink1 += "&session=" + demo1Session;
        launchLink2 += "&project=" + projectRoot + "/" + demo2Project;
        launchLink2 += "&session=" + demo2Session;
//        alert("launchLink1="+launchLink1);
//        alert("launchLink2="+launchLink2);
        
        if (pos1 == -1) {
            for (var num=0; num<document.links.length; num++) {
                if (document.links[num].href.indexOf("launchlink1") != -1) {
                    pos1 = num;
                    break;
                }
            }
            if (pos1 != -1) {
                document.links[pos1].href = launchLink1;
            } else alert("Cannot find href launchlink1");
        } else document.links[pos1].href = launchLink1;
        
        if (pos2 == -1) {
            for (var num=0; num<document.links.length; num++) {
                if (document.links[num].href.indexOf("launchlink2") != -1) {
                    pos2 = num;
                    break;
                }
            }
            if (pos2 != -1) {
                document.links[pos2].href = launchLink2;
            } else alert("Cannot find href launchlink2");
        } else document.links[pos2].href = launchLink2;
    }
</script>
<%
    Properties props = new Properties();
    Class me = createWorkbenchBuild.class;
    InputStream in = me.getResourceAsStream("/version.properties");
    try {
        props.load(in);
    } catch (IOException ioe) {
        //logger.warning("IO exception reading version.properties file:" + ioe.getMessage());
    } finally {
        try {
            if (in != null) in.close();
        } catch (IOException e) {}
    }
    String versionProp = props.getProperty("project.version");
    String tomcatDefaultUrl = "http://localhost:8080";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/struts-html" prefix="html" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Launch Demos</title>
    <html:base />
    <link rel="stylesheet" type="text/css" href="css/ie.css"> <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body style="margin: 0pt;">
  <form action="" name=demos onsubmit="">
    <table border="0" cellpadding="0" cellspacing="0" width="760">
        <tbody>
            <tr>
                <td width="10"> &nbsp;
                </td>
                <td height="50" valign="bottom" width="195"><img src="images/QIW_logo.gif" alt="qiWorkbench" border="0">
                </td>
                <td class="sidebar_heading1" align="left" nowrap="nowrap" width="164">
                    <p>&nbsp;</p>
                    <p>The geoscience open-source toolkit for Quantitative Interpretation</p>
                </td>
                <td align="right" nowrap="nowrap" valign="top"> &nbsp;
                </td>
            </tr>
        </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
            <tr>
                <td class="bg_orange" height="20">  &nbsp;
                </td>
            </tr>
        </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td height="20">&nbsp;
                </td>
            </tr>
        </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="572">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td width="10"> &nbsp;
                                </td>
                                <td width="28"> &nbsp;
                                </td>
                                <td valign="top" width="534">
                                    <table border="0" cellpadding="0" cellspacing="0" width="534">
                                        <tbody>
                                            <tr>
                                                <td class="heading0">qiWorkbench (v<%= versionProp %>)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="heading1">Launch Demonstrations
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20">&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><label><b>Project qiSpace:&nbsp;&nbsp;</b> <input name="projRoot" size="80" type="text" onChange="getProjRoot(this.form);" /></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20">&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><label><b>Runtime Tomcat URL:</b> <input name="tomcatUrl" size="80" type="text" value=<%= tomcatDefaultUrl%> onChange="getTomcatUrl(this.form);" /></label>
                                                </td>
                                            </tr> 
                                            <tr>
                                              <td>
                                                <input type="radio" name="rad" value="local" checked> Local
                                                <input type="radio" name="rad" value="remote"> Remote
                                              </td>
                                            </tr>
                                            <tr>
                                                <td height="10">&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><input size="80" type="button" value="Set Params" onClick="getTomcatLoc(this.form); validateParams(this.form); genLinks();" />
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td height="20">&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="heading2">Demo 1: Model slice gather
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="launchlink1"><button name="demo1" type="button"><img
                                                     src="images/demo1_th.jpg" height="100" width="169"></button></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20">&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="heading2">Demo 2: Taranaki
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="launchlink2"><button name="demo2" type="button"><img
                                                     src="images/demo2_th.jpg" height="100" width="169"></button></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
  </form>  
</body>
</html>
