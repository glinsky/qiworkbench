<%@page import="java.util.Properties, java.io.InputStream, java.io.IOException, java.text.DateFormat, java.util.Date, anttasks.createWorkbenchBuild"%>
<script type="text/javascript">
    var sDate = "1/1/08";
    var eDate = "3/11/08";
    var chartBaseLink1 = "/qiWorkbench/GenModuleHistogram";
    var chartBaseLink2 = "/qiWorkbench/GenModuleTrends";

    function getStartDate(form) {
        sDate = form.startDate.value;
        if (sDate == "") {
            alert("Start date cannot be empty");
            form.startDate.focus();
        } 
    }
    
    function getEndDate(form) {
        eDate = form.endDate.value;
        if (eDate == "") {
            alert("End date cannot be empty");
            form.endDate.focus();
        } 
    }
    
    function validateDates(form) {
        getStartDate(form);
        getEndDate(form);
    }
</script>
<script language="JavaScript" src="js/simplecalendar.js" type="text/javascript"></script>

<%
    Properties props = new Properties();
    Class me = createWorkbenchBuild.class;
    InputStream in = me.getResourceAsStream("/version.properties");
    try {
        props.load(in);
    } catch (IOException ioe) {
        //logger.warning("IO exception reading version.properties file:" + ioe.getMessage());
    } finally {
        try {
            if (in != null) in.close();
        } catch (IOException e) {}
    }
    String versionProp = props.getProperty("project.version");
    String date = DateFormat.getDateInstance().format(new Date(System.currentTimeMillis()));
    
    //Get the start and end dates entered in the form
    String sDate = request.getParameter("startDate");
    String eDate = request.getParameter("endDate");
    String tScale = request.getParameter("scale");
    String chartBaseLink1 = "/qiWorkbench/GenModuleHistogram";
    String chartBaseLink2 = "/qiWorkbench/GenModuleTrends";
    String chartLink1 = chartBaseLink1+"?startDate="+sDate+"&endDate="+eDate+"&scale="+tScale;
    String chartLink2 = chartBaseLink2+"?startDate="+sDate+"&endDate="+eDate+"&scale="+tScale;
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/struts-html" prefix="html" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>qiWorkbench Usage Statistics</title>
    <html:base />
    <link rel="stylesheet" type="text/css" href="css/ie.css"> 
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/calendar.css">
</head>
<body style="margin: 0pt;">
  <form action="" name=stats onsubmit="" method=GET>  
    <table border="0" cellpadding="0" cellspacing="0" width="760">
        <tbody>
            <tr>
                <td width="10"> &nbsp;
                </td>
                <td height="50" valign="bottom" width="195"><img src="images/QIW_logo.gif" alt="qiWorkbench" border="0">
                </td>
                <td class="sidebar_heading1" align="left" nowrap="nowrap" width="164">
                    <p>&nbsp;</p>
                    <p>The geoscience open-source toolkit for Quantitative Interpretation</p>
                </td>
                <td align="right" nowrap="nowrap" valign="top"> &nbsp;
                </td>
            </tr>
        </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
            <tr>
                <td class="bg_orange" height="20">  &nbsp;
                </td>
            </tr>
        </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td height="20">&nbsp;
                </td>
            </tr>
        </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="600">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td width="10"> &nbsp;
                                </td>
                                <td width="28"> &nbsp;
                                </td>
                                <td valign="top" width="562">
                                    <table border="0" cellpadding="0" cellspacing="0" width="562">
                                        <tbody>
                                            <tr>
                                                <td class="heading0">qiWorkbench (v<%= versionProp %>)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="heading1">Usage Statistics as of <%= date%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <br/>
                                                    <b>Please pick a time period (default is 1m) and time scale (default is weekly):</b>
                                                    <p>
                                                  <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                      <td><b>Start date:&nbsp;</b> <input TYPE=text NAME=startDate SIZE=10 MAXLENGTH=10/></td>
                                                      <td><a href="javascript: void(0);" onmouseover="if (timeoutId) clearTimeout(timeoutId);window.status='Show Calendar';return true;" onmouseout="if (timeoutDelay) calendarTimeout();window.status='';" onclick="g_Calendar.show(event,'stats.startDate',false, 'mm/dd/yyyy'); return false;"><img src="images/calendar.gif" name="imgCalendar" width="34" height="22" border="0" alt=""></a></td>
                                                      <td><b>&nbsp;&nbsp;End date:&nbsp;</b> <input TYPE=text NAME=endDate SIZE=10 MAXLENGTH=10/></td>
                                                      <td><a href="javascript: void(0);" onmouseover="if (timeoutId) clearTimeout(timeoutId);window.status='Show Calendar';return true;" onmouseout="if (timeoutDelay) calendarTimeout();window.status='';" onclick="g_Calendar.show(event,'stats.endDate',false, 'mm/dd/yyyy'); return false;"><img src="images/calendar.gif" name="imgCalendar" width="34" height="22" border="0" alt=""></a></td>
                                                      <td><b>&nbsp;&nbsp;Time scale:&nbsp;</b></td>
                                                      <td>
                                                        <select name="scale" size="1">
                                                          <option>Weekly</option>
                                                          <option>Monthly</option>
                                                          <option>Yearly</option>
                                                        </select>
                                                      </td>
                                                    </tr>
                                                  </table>
                                                    <p>
                                                    <input TYPE="submit" VALUE="Generate Charts" onClick="validateDates(this.form);"/> 
                                                    <p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><img src=<%= chartLink1%> alt="Module Histogram" border="0">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20">&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><img src=<%= chartLink2%> alt="Module Trends" border="0">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20">&nbsp;
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table> 
  </form>
</body>
</html>
