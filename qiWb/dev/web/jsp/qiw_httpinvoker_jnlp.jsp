<%@ page contentType="application/x-java-jnlp-file"
    info="SDA JNLP" %>
<%@ page import="java.util.Properties, java.io.InputStream, java.io.IOException, anttasks.createWorkbenchBuild" %>
<%  String href = "";
    StringBuffer codebaseBuffer = new StringBuffer();

    codebaseBuffer.append("http://");
    codebaseBuffer.append(request.getServerName());
    codebaseBuffer.append(':');
    codebaseBuffer.append(request.getServerPort());
 
    String deployServerURL = codebaseBuffer.toString();

    href = codebaseBuffer.toString() + request.getRequestURI();

    response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

    String uri = request.getRequestURI();
    int index = uri.lastIndexOf("/");
    if ( index > 0) {
        codebaseBuffer.append( uri.substring(0, index) );
    }

    codebaseBuffer.append('/'); %>
<%  Properties props = new Properties();
    Class me = createWorkbenchBuild.class;
    InputStream in = me.getResourceAsStream("/version.properties");
    try {
        props.load(in);
    }catch (IOException ioe) {
    //logger.warning("IO exception reading version.properties file:" + ioe.getMessage());
    } finally {
        try {
            if (in != null) in.close();
        } catch (IOException e) {}
    }
    String versionProp = props.getProperty("project.version");
    String providerProp = props.getProperty("project.provider");
    String workbenchJar = "../lib/qiWorkbench.jar"; %>
<%  java.util.Enumeration params = request.getParameterNames();
    String hrefParams = ""; //Parameters placed at the end of href so parameters can be read in JWS
    String propertyParams = ""; // Creates a string of property declarations based off passed parameters
    boolean atleastOneElement = false;
    while (params.hasMoreElements()) {
        atleastOneElement = true;
        String paramName = (String) params.nextElement();
        String paramValues[] = request.getParameterValues(paramName);
        hrefParams = paramName + "=" + paramValues[0] + hrefParams;
        if(params.hasMoreElements()) {
            hrefParams = "&" + hrefParams;
        }
        propertyParams = "<property name=\"" + paramName + "\" value=\"" + paramValues[0] + "\" />" + propertyParams;
    }
    if(atleastOneElement) {
        hrefParams = "?" + hrefParams;
    } %>
<% String[] maxheapsizevalues = request.getParameterValues("maxheapsize");
    String maxheapsize="1024m";
    //TODO does this method always return at least 1 value if non-null?
    if ((maxheapsizevalues != null) && (maxheapsizevalues.length > 0))
        maxheapsize=maxheapsizevalues[0];
%>
<?xml version="1.0" encoding="UTF-8"?>
<jnlp codebase="<%= codebaseBuffer.toString() %>" >
    <information>
        <title>qiWorkbench v<%= versionProp %></title>
        <vendor><%= providerProp %></vendor>
        <description>Seismic toolkit for Quantitative Interpretation</description>
        <offline-allowed/>
        <icon href="images/QIW_icon.gif"/>
        <icon href="images/QIW_splash.gif" kind="splash"/>
        <shortcut><desktop/></shortcut>
    </information>
    <security>
        <all-permissions/>
    </security>
    <resources>
        <j2se version="1.5+" max-heap-size="<%= maxheapsize %>" java-vm-args="-Xincgc"/>
        <!--j2se version="1.5+" /-->
        <jar href="<%= workbenchJar %>"/>
        <!--These classes are now contained in the qiWorkbench.jar (client) -->
        <!--jar href="../lib/client_filechooser_job_asciiIO.jar"/-->        
        <jar href="../lib/commons-httpclient-3.0.jar"/>
        <jar href="../lib/commons-logging.jar"/>
        <jar href="../lib/commons-codec-1.3.jar"/>
        <jar href="../lib/xstream-1.3.jar"/>
        <jar href="../lib/xom-1.1.jar"/>
        <jar href="../lib/xpp3-1.1.3.4.O.jar"/>
        <jar href="../lib/activation.jar"/>
        <jar href="../lib/mail.jar"/>
        <jar href="../lib/jhall.jar"/>
        <jar href="../lib/geoIOlib.jar"/>
        <jar href="../lib/bhpsuIOlib.jar"/>
        <jar href="../lib/swing-layout-1.0.jar"/>
        <jar href="../lib/qiwbCompAPI.jar"/>
        <jar href="../lib/qiwbCompCommon.jar"/>
        <jar href="../lib/xercesImpl.jar"/>
        <!-- Access property parameters with System.getProperty("param") -->
        <%= propertyParams %>
        <property name=deployServerURL value=<%= deployServerURL %> />
    </resources>
    <application-desc main-class="com.bhpb.qiworkbench.messageFramework.MessageDispatcher"/>
</jnlp>
