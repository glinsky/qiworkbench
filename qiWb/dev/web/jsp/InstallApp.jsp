<%@page import="java.util.Properties, java.io.InputStream, java.io.IOException, anttasks.createWorkbenchBuild, com.bhpb.qiworkbench.server.usageCharts.UsageStats"%>
<%
    Properties props = new Properties();
    Class me = createWorkbenchBuild.class;
    InputStream in = me.getResourceAsStream("/version.properties");
    try {
        props.load(in);
    }catch (IOException ioe) {
        //logger.warning("IO exception reading version.properties file:" + ioe.getMessage());
    } finally {
        try {
            if (in != null) in.close();
        } catch (IOException e) {}
    }
    String versionProp = props.getProperty("project.version");
    
    String statsURL = UsageStats.getInstance().getStatsURL();
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/struts-html" prefix="html" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>qiWorkbench</title>
    <html:base />
    <link rel="stylesheet" type="text/css" href="css/ie.css"> <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body style="margin: 0pt;">
    <table border="0" cellpadding="0" cellspacing="0" width="760">
        <tbody>
            <tr>
                <td width="10"> &nbsp;
                </td>
                <td height="50" valign="bottom" width="195"><img src="images/QIW_logo.gif" alt="qiWorkbench" border="0">
                </td>
                <td class="sidebar_heading1" align="left" nowrap="nowrap" width="164">
                    <p>&nbsp;</p>
                    <p>The geoscience open-source toolkit for Quantitative Interpretation</p>
                </td>
                <td align="right" nowrap="nowrap" valign="top"> &nbsp;
                </td>
            </tr>
        </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
            <tr>
                <td class="bg_orange" height="20">  &nbsp;
                </td>
            </tr>
        </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td height="20">    &nbsp;
                </td>
            </tr>
        </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="572">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td width="10"> &nbsp;
                                </td>
                                <td width="28"> &nbsp;
                                </td>
                                <td valign="top" width="534">
                                    <table border="0" cellpadding="0" cellspacing="0" width="534">
                                        <tbody>
                                            <tr>
                                                <td class="heading0">qiWorkbench (v<%= versionProp %>)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="heading1">Installation
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Follow these steps to install the qiWorkbench application on your machine using
                                                    Java Web Start.</p>
                                                    <ol>
                                                        <li>Install Java Runtime Environment (JRE) 5.0 (1.5.0_06) or higher which includes Web Start. If you don't have
                                                        admin privledges to your machine, contact your sysadmin to do the install. If you
                                                        have admin rights, download the JRE install file for your platform from
                                                        <a target="_blank" href="java.sun.com/javase/downloads/">here </a>
                                                        and follow the installation instructions. If you have a Linux machine, download
                                                        the 32-bit version because the 64-bit version does not (yet) include Java Web Start.</li>
                                                        <li>Launch the qiWorkbench application <a href="qilauncher_invoker_jnlp.jsp?install=false">with no component updates</a> or <a href="qilauncher_invoker_jnlp.jsp?install=true">
                                                        with auto component updates</a> using Java Web Start. The following sequence of events will occur:
                                                        <ul>
                                                            <li><font color="#cc33cc">If you get an error dialog stating the qiWorkbench application failed to launch</font>, most likely your computer is using a proxy for network connections. To change this, run Java WebStart (from a command line invoke 'javaws') and in the General tab, click the Network Settings... button. Change from the "Use Browser Setting" radio button to the "Direct Connection" radio button and then click Apply. Relaunch the application.</li>
                                                            <li>The application's Splash screen will be displayed followed by a "Java Web Start"
                                                            window that shows the progress of the download.</li>
                                                            <li>A "Warning - Security" window will be displayed asking "Do you want to trust the signed
                                                            application distributed by 'BHP'?" It will also state the security certificate was issued by
                                                            Thawte which is trusted. Select the 'Yes' or 'Always' button.</li>
                                                            <li>[PC only] If this is the first time installing the application, a dialog window will be displayed
                                                            asking "Would you like to create desktop shortcut(s) for qiWorkbench v<%= versionProp %>?". Select the 'Yes'
                                                            button. A Web Start shortcut for the application may appear on your desktop.</li>
                                                            <li>The qiWorkbench window will be displayed on your desktop.</li>
                                                        </ul>
                                                    </ol>
                                                    <p>You can always launch the qiWorkbench from this Webpage using the link in Step 2 above. On a Windows platform,
                                                    you can also launch the qiWorkbench using its desktop icon (provided you choose to have one
                                                    created).</p>
                                                    <p>Each time the application is launched by Web Start, it will check if a
                                                    newer version is available and download it if there is.</p>
                                                    <p>After the first
                                                    installation, you can also launch the application using the Web Start console, but this is only recommended
                                                    for users familiar with Java.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="heading1">Launch Demonstrations
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>To see a demo of launching the qiWorkbench on an existing project with a saved session, click <a href="qidemos.jsp">here</a>. The displayed Webpage will have one or more buttons you can click to launch a demo. You must enter 
                                                    <ul>
                                                        <li>the URL of a runtime Tomcat server from which the project is accessible. The default is http://localhost:8080, i.e., your machine has Tomcat installed and running and contains the demo projects.</li>
                                                        <li>the path of the qiSpace containing all of the projects.</li>
                                                    </ul>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="heading1">View Usage Statistics</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>To view qiWorkbench usage charts, click <a href="<%= statsURL %>">here</a>. The displayed Webpage contains two charts over a reporting period: 1) a histogram of the number of uses of each qiComponent; and 2) a qiComponent trend chart. Both charts indicate the number of unique users involved and can be scaled to weekly, monthly or yearly.
                                                    </p>
                                                </td>
                                            </td>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
