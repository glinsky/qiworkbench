/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.mail;

import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
/**
 * File		: Smtp.java
 *            Model to hold javaMail Info
 * @author	: Charlie Jiang
 * @version    1.0
 *
 */
public class Smtp implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	
	  private String mailHost;
	  private List<String> emails;
	
	  private String from;
	  private List<String> tos;
	  private List<String> ccs;
	  
	  private List<String> attachs;
	  
	  private String subject;
	  private String content;
	
	public Properties getProps() {
		Properties prop = new Properties();
		prop.put(SmtpConst.MAIL_HOST_KEY, mailHost);
		return prop;
	}
	
	public List<String> getAttachs() {
		if (attachs == null) attachs = new ArrayList<String>();
		return attachs;
	}
	public void setAttachs(List<String> attachs) {
		this.attachs = attachs;
	}
	public List<String> getCcs() {
		if (ccs == null) ccs = new ArrayList<String>();
		if (ccs.size() == 0) {
			ccs.add(from);
		}
		return ccs;
	}
	public void setCcs(List<String> ccs) {
		this.ccs = ccs;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public List<String> getTos() {
		if (tos == null) tos = new ArrayList<String>();
		return tos;
	}
	public void setTos(List<String> tos) {
		this.tos = tos;
	}

	public List<String> getEmails() {
		return emails;
	}

	public void setEmails(List<String> emails) {
		this.emails = emails;
	}

	public String getMailHost() {
		return mailHost;
	}

	public void setMailHost(String mailHost) {
		this.mailHost = mailHost;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}	
	
	public void addEmail(String type, String mail) {
		if (SmtpConst.MAIL_LIST.equals(type)) {
			addMail(emails, mail);
		} else if (SmtpConst.TOS.equals(type)) {
			addMail(tos, mail);
		} else if (SmtpConst.CCS.equals(type)) {
			addMail(ccs, mail);
		}
	}
	
	private void addMail(List<String> list, String mail) {
		String temp = null;
		boolean found = false;
		if (list != null) {
			int size = list.size();
			for (int i = 0; i < size; i++) {
				temp = list.get(i);
				if (temp.equalsIgnoreCase(mail)) {
					found = true;
					break;
				}
			}
			if (!found) {
				list.add(mail);
			}
		}
	}
	
	public void delEmail(String mail) {
		if (mail == null) return;
		for (int i=0; i < emails.size(); i++) {
			String temp = emails.get(i);
			if (mail.equalsIgnoreCase(temp)) {
				emails.remove(i);
				break;
			}
		}
		  
	}
	
	public void addAttach(String attach) {
	   if (attachs == null) attachs = new ArrayList<String>();
	   attachs.add(attach);
	}
	
	public void addAttach1(String attach) {
		   if (attachs == null) attachs = new ArrayList<String>();
		   attachs.clear();
		   attachs.add(attach);
		}
	
	public void setList(String type, List<String> aList) {
		if (SmtpConst.MAIL_LIST.equals(type)) {
			emails = aList;
		} else if (SmtpConst.TOS.equals(type)) {
			tos = aList;
		} else if (SmtpConst.CCS.equals(type)) {
			ccs = aList;
		}
	}
	
    public Smtp defaultModel() {	
       mailHost = "smtp.amer.bhpbilliton.net";
       from     = "qiWorkbench@BHPBilliton.com";
       String charlie = "Charlie.Jiang@bhpbilliton.com";
  	   subject  = "XmlEditor Mail Distribution";
  	   content  = "Test Only";
  	   emails = new ArrayList<String>();
  	   emails.add(from);
  	   emails.add("Michael.E.Glinsky@BHPBilliton.com");
  	   emails.add("Charlie.Jiang@bhpbilliton.com");
  	   emails.add("Gilbert.Hansen@bhpbilliton.com");
  	   tos = new ArrayList<String>();
  	   tos.add(charlie);
  	   ccs = new ArrayList<String>();
  	   ccs.add(charlie);
	   return this;
	}
}