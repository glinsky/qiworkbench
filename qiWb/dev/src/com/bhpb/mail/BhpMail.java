/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.mail;

import java.util.List;
import java.util.Date;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.activation.DataSource;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;

/**
 * A common JavaMail Utility
 *
 * @author Charlie Jiang
 * @version 1.0
 */
public class BhpMail {
    static final Logger logger =
            Logger.getLogger(BhpMail.class.getName());
  public static  boolean send(Smtp model){
    Session session = null;
    boolean isSent = true;
    MimeMessage msg = null;
    
    try{
      session = Session.getDefaultInstance(model.getProps(), null);
      msg = new MimeMessage(session);
      
      msg.setFrom(new InternetAddress(model.getFrom()));
      
      int size = model.getTos().size();
      if (size == 0) return false;
      
      String [] TOs = model.getTos().toArray(new String[size]);
      InternetAddress[] addresses = new  InternetAddress[size];
      for (int i=0; i < TOs.length; i++){
    	  logger.info("----tos = " + TOs[i]);
          addresses[i] = new InternetAddress(TOs[i]);
      }
      
      size = model.getCcs().size();
      String [] CCs = model.getCcs().toArray(new String[size]);
      InternetAddress[] CCaddresses = null;
      CCaddresses = new  InternetAddress[size];
      for (int i=0; i< CCs.length; i++){
    	    logger.info("----ccs = " + CCs[i]);
            CCaddresses[i] = new InternetAddress(CCs[i]);
      }
   
      msg.setRecipients(javax.mail.Message.RecipientType.TO, addresses);
      msg.setRecipients(Message.RecipientType.CC, CCaddresses);
      msg.setSubject(model.getSubject());
      msg.setSentDate(new Date());

      // Create the body text
      Multipart multiparts = new MimeMultipart();
      MimeBodyPart mainBody = new MimeBodyPart();
      mainBody.setText(model.getContent());
      multiparts.addBodyPart(mainBody);

      size = 0;
      List<String> attachs = model.getAttachs();
      if (model.getAttachs() != null) {
    	  size = attachs.size();
      }
      if (size > 0){
    	String aName = null;
        for (int i=0; i< size; i++){
        	aName = attachs.get(i);
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            DataSource source = new FileDataSource(aName);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(source.getName());
            multiparts.addBodyPart(messageBodyPart);
          
        }
      }

      msg.setContent(multiparts);
      Transport.send(msg);
    } catch (Exception ex) {
      isSent = false;
      ex.printStackTrace();
    } finally {
      session = null;
    }
    
    return isSent;
 }
}