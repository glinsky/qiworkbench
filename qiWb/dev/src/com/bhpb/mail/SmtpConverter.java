/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.mail;

import java.util.List;
import java.util.ArrayList;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Java Object (Smtp) to XML and XML to Java Object (Smtp) Converter
 *
 * @author Charlie Jiang
 * @version 1.0
 */
public class SmtpConverter implements Converter {
	 
	 public boolean canConvert(Class clazz) {
		 boolean ret = clazz.equals(Smtp.class);
         return ret;
		 //return clazz.isInstance(Smtp.class);
         
     }

     public void marshal(Object value, HierarchicalStreamWriter writer,
                                       MarshallingContext context) {
    	 
    	 Smtp model = (Smtp)value; 
    	 writeProps(model, writer, context); 
    	 writeEmails(SmtpConst.MAIL_LIST, model.getEmails(), writer, context); 
     }
     
     private void writeProps(Smtp model, HierarchicalStreamWriter writer,
                    MarshallingContext context) {
    	 writer.startNode("mailhost");
    	 writer.setValue(model.getMailHost());
    	 writer.endNode();
    		 
         writer.startNode("subject");
    	 writer.setValue(model.getSubject());
    	 writer.endNode();
    		
    	 writer.startNode("from");
    	 writer.setValue(model.getFrom());
    	 writer.endNode();
		
     }  
     
     private void writeEmails(String nodeName, List<String> list, HierarchicalStreamWriter writer,
             MarshallingContext context) {
    	 
    	 int cnt = list.size();
		 writer.startNode(nodeName);
    	 for (int i = 0; i < cnt; i++) {  		 
    		 writer.startNode("email");
    		 writer.setValue(list.get(i));
    		 writer.endNode();
    	 }
    	 writer.endNode();
    	 
     }

     
     public Object unmarshal(HierarchicalStreamReader reader,
                                    UnmarshallingContext context) {
    	 
    	 Smtp model = new Smtp(); 
    	 while (reader.hasMoreChildren()) {
    		 reader.moveDown();
    		 String nodeName = reader.getNodeName();
    		 if ("mailhost".equals(nodeName)) {
    			 readProps(model, reader, context); 
    		 } else if (SmtpConst.MAIL_LIST.equals(nodeName)) {
    			 List<String> aList = readEmails(model, reader, context); 
    			 model.setList(nodeName, aList);
    		 } 
    	 }
         return model;
     }
	
     private void readProps(Smtp model, HierarchicalStreamReader reader,
                                                   UnmarshallingContext context) {
   
    	  model.setMailHost(reader.getValue());
    	  reader.moveUp();
    	  reader.moveDown();
 		  model.setSubject(reader.getValue());
 		  reader.moveUp();
 		  reader.moveDown();
		  model.setFrom(reader.getValue());
		  reader.moveUp();
     }
     
     private List<String> readEmails(Smtp model, HierarchicalStreamReader reader,
                                                 UnmarshallingContext context) {
    	 List<String> aList = new ArrayList<String>();
    	 while (reader.hasMoreChildren()) {
    			 reader.moveDown();
    			 aList.add(reader.getValue());
        		 reader.moveUp();
         }
    	 reader.moveUp();
    	 
    	 return aList;
     }
     
}
