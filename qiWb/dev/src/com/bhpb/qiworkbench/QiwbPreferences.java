/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Container for user preferences. Holds list of possible values for
 * qiProject directories, Tomcat server URLs and desktop state/session files
 * and their default (if specified).
 * <p>
 * qiProjects are associated with a server. Each server may have one default
 * qiproject per server.
 * <p>
 * Desktop state files are associated with a qiProject. Each qiProject may
 * have one default desktop per qiProject.
 *
 * @author Gil Hansen.
 * @version 1.1
 */
public class QiwbPreferences implements Serializable {

    private static Logger logger = Logger.getLogger(QiwbPreferences.class.getName());
    public static final String NO_DEFAULT = "";
    /**
     * List of URLs of allowable Tomcat servers where the qiWorkbench
     * is deployed.
     */
    ArrayList<String> servers;
    /**
     * URL of default Tomcat Server; "" if none. When the qiWorkbench
     * is launched, this is the server the app will use.
     */
    String defaultServer;
    /**
     * qiProjects associated with each server.The key is the
     * server. The value is the qiprojects associated with the server
     */
    HashMap<String, ArrayList<QiSpaceDescriptor>> qispaces;
    /**
     * Directory path of the default qiprojects; "" if none. When the qiWorkbench
     * is launched, this is the server the app will use.
     * <p>
     * Each server has only 1 default qiProject. The key is the server. The value
     * is the default qiProject.
     */
    HashMap<String, String> defaultQispaces;
    /**
     * Pathnames of desktop state files associated with each qiProject.
     * The key is the server+qiproject. The value is the desktop state files associated
     * with the qiproject.
     * <p>
     * Note: Different servers could have the same servers. Therefore the key is the
     * concatenation of the server and qiProject.
     */
    HashMap<String, ArrayList<String>> desktops;
    /**
     * Path of the default desktop state files to restore; "" if none. When the
     * qiWorkbench is launched, this is the desktop that is restored.
     * <p>
     * Each qiProject has only 1 default desktop. The key is the server+qipro9ject. The value
     * is the default desktop state file.
     * <p>
     * Note: Different servers could have the same qiProject. Therefore the key is the
     * concatenation of the server and qiProject.
     */
    HashMap<String, String> defaultDesktops;
    /**
     * Flag to indicate if the app should honor defaults on startup.
     * The default value is false. If there is a default server and default
     * project, launch the qiWorkbench if flag is true; otherwise, bring up
     * the Project Server Dialog.
     */
    boolean neverAskAgain = false;

    /** Constructor that initializes preferences */
    public QiwbPreferences() {
        servers = new ArrayList<String>();
        defaultServer = NO_DEFAULT;
        qispaces = new HashMap<String, ArrayList<QiSpaceDescriptor>>();
        defaultQispaces = new HashMap<String, String>();
        desktops = new HashMap<String, ArrayList<String>>();
        defaultDesktops = new HashMap<String, String>();
    }

    // METHODS FOR TOMCAT SERVERS
    /**
     * Initialize the list of allowable servers.
     *
     * @param serverURLs List of URLs of allowable servers.
     */
    public void initServers(ArrayList<String> serverURLs) {
        if (!servers.isEmpty()) {
            removeAllServers();
        }
        for (int i = 0; i < serverURLs.size(); i++) {
            addServer(serverURLs.get(i));
        }
    }

    /**
     * Add the entered URL of a Tomcat server to the list of allowable servers.
     *
     * @param serverURL Entered URL of a Tomcat server.
     * @return -1 if not a valid URL, 0 if already on the list, 1 if
     * add to the list
     */
    public int addServer(String serverURL) {
        // Validate URL: starts with 'http://", ends with ':<port>'
        if (!serverURL.startsWith("http://")) {
            return -1;
        //Removed to fix GNATS issue #???
        //server does not start up when running on port 8082 due to this check
        //TODO perform a more general validation of URL by sending an http request
        //if (!serverURL.endsWith(":"+QIWConstants.SERVER_PORT))
        //   return -1;

        // Check if server already on the list
        }
        for (int i = 0; i < servers.size(); i++) {
            if (servers.get(i).equals(serverURL)) {
                return 0;
            }
        }

        // URL not on the list; add to end of list
        servers.add(serverURL);
        // Associate an empty list of qiProjects with the server
        ArrayList<QiSpaceDescriptor> serverQispaces = new ArrayList<QiSpaceDescriptor>();
        qispaces.put(serverURL, serverQispaces);
        // Indicate no default qiProject for the selected server
        defaultQispaces.put(serverURL, NO_DEFAULT);

        return 1;
    }

    /**
     * Get list of allowable servers.
     *
     * @return List of allowable servers.
     */
    public ArrayList<String> getServerURLs() {
        return servers;
    }

    /**
     * Set the selected server as the default.
     *
     * @param serverURL URL of a Tomcat server selected in the list of
     * allowable servers.
     * @return 1 if a new default, 0 if already the default
     */
    public int setDefaultServer(String serverURL) {
        if (defaultServer.equals(serverURL)) {
            return 0;
        }
        defaultServer = serverURL;

        return 1;
    }

    /**
     * Get the URL of the default Tomcat server.
     *
     * @return URL of the default Tomcat server; empty string if no default.
     */
    public String getDefaultServer() {
        return defaultServer;
    }

    /**
     * Remove the selected Tomcat server from the list of allowable servers.
     * If the server removed is the default, indicate there is no default.
     *
     * @param serverURL URL of a Tomcat server selected in the list of
     * allowable servers.
     */
    public void removeServer(String serverURL) {
        // Find the selected server on the list
        for (int i = 0; i < servers.size(); i++) {
            if (servers.get(i).equals(serverURL)) {
                servers.remove(i);
                break;
            }
        }

        // Check if server removed is the default
        if (defaultServer.equals(serverURL)) {
            defaultServer = NO_DEFAULT;

        // Remove all qiProjects associated with server
        }
        ArrayList<QiSpaceDescriptor> serverQispaces = getServerQispaces(serverURL);
        for (int i = 0; i < serverQispaces.size(); i++) {
            // Note: removing the qiProject also removes desktops associated with the
            // qiproject and resets defaults, if necessary.
            removeQispace(serverURL, serverQispaces.get(i).getPath());
        }
        // Remove from qiProjects' list
        qispaces.remove(serverURL);
        // Remove from default qiProjects' list
        defaultQispaces.remove(serverURL);
    }

    /**
     * Remove all of the servers from the list of allowable servers.
     * Indicate there is no default server.
     * <p>
     * WARNING: Clearing the servers also clears all qiProjects, desktops
     * and defaults; the root of the associations has been deleted.
     *
     */
    public void removeAllServers() {
        servers.clear();
        defaultServer = NO_DEFAULT;

        // Also clear all associated qiProjects, desktops associated with
        // qiProjects and defaults.
        qispaces.clear();
        desktops.clear();
        defaultQispaces.clear();
        defaultDesktops.clear();
    }

    /**
     * Determine the location of a Tomcat server. If the server is the
     * user's machine, it is local; otherwise, remote. However, the user
     * can specify the location of the server in the Project Selector
     * dialog provided both the server and the project are not the defaults.
     * This means that the user may override the default behavior and
     * use remote IO when running on the same machine as tomcat.
     *
     * @param serverURL
     * @return Location of the Tomcat server
     */
    public static String getServerLoc(String serverURL) {
        //use the serverLoc environment variable if set (passed in as a URL param to jnslp jsp
        String serverLoc = System.getProperty("serverLoc");
        if (serverLoc != null) {
            if ("local".equals(serverLoc)) {
                return QIWConstants.LOCAL_PREF;
            } else if ("remote".equals(serverLoc)) {
                return QIWConstants.REMOTE_PREF;
            } else {
                throw new IllegalArgumentException("Cannot get server location because serverLoc is defined but is not a valid value: remote or local");
            }
        } else {
            if ((serverURL.startsWith("http://localhost")) ||
                    (serverURL.startsWith("http://127.0.0.1")) ||
                    (serverURL.toLowerCase().startsWith(("http://" + getHostName()).toLowerCase()))) {
                logger.info("based on serverURL: " + serverURL + ", default service preference is LOCAL");
                return QIWConstants.LOCAL_PREF;
            } else {
                logger.info("based on serverURL: " + serverURL + ", default service preference is REMOTE");
                return QIWConstants.REMOTE_PREF;
            }
        }
    }

    // METHODS FOR QISPACES
    /**
     * Add the entered qiProject to the list of allowable qiProjects for the
     * selected server, if it is not already on the list. The qiProject
     * directory is valid because it was selected using the FileChooserDialog.
     *
     * @param selectedServer The selected Tomcat server.
     * @param qsDesc qiProject metadata. Contains full path of qiProject.
     * @return 0 if already on the list, 1 if added to the list.
     */
    public int addQispace(String selectedServer, QiSpaceDescriptor qsDesc) {
        ArrayList<QiSpaceDescriptor> serverQispaces = getServerQispaces(selectedServer);
        String qispaceDir = qsDesc.getPath();

        // Check if qiProject already on the list
        for (int i = 0; i < serverQispaces.size(); i++) {
            if (serverQispaces.get(i).getPath().equals(qispaceDir)) {
                return 0;
            }
        }

        // qiProject not on the list; add to end of list
        serverQispaces.add(qsDesc);
        // Associate an empty list of deskops with the qiProject
        ArrayList<String> qispaceDesktops = new ArrayList<String>();
        String key = selectedServer + qispaceDir;
        desktops.put(key, qispaceDesktops);
        // Indicate no default qiProject
        defaultDesktops.put(key, NO_DEFAULT);

        return 1;
    }

    /**
     * Get list of valid qiProjects for the selected server. There is always a
     * list, but it may be empty.
     *
     * @param selectedServer The selected Tomcat server.
     * @return List of valid qiProjects.
     */
    public ArrayList<QiSpaceDescriptor> getServerQispaces(String selectedServer) {
        //Note: selected server is always on the list of allowed servers
        //Note: There is always a list, but it may be empty.
        ArrayList<QiSpaceDescriptor> serverQispaces = qispaces.get(selectedServer);
        if (serverQispaces == null) {
            serverQispaces = new ArrayList<QiSpaceDescriptor>();
            qispaces.put(selectedServer, serverQispaces);
        }
        return serverQispaces;
    }

    /**
     * Get the metadata for the qiProject on a server.
     *
     * @param selectedServer The selected Tomcat server.
     * @param qispaceDir Path of the qiProject.
     * @return Descriptor (metadata) of the qiproject; null the server
     * has no associated qiProjects or the qiProject doesn't exist on the
     * server's list of qiProjects.
     */
    public QiSpaceDescriptor getQispaceDesc(String selectedServer, String qispaceDir) {
        ArrayList<QiSpaceDescriptor> serverQispaces = qispaces.get(selectedServer);
        if (serverQispaces.isEmpty()) {
            return null;
        }
        for (int i = 0; i < serverQispaces.size(); i++) {
            if (serverQispaces.get(i).getPath().equals(qispaceDir)) {
                return serverQispaces.get(i);
            }
        }

        //qiProject not on server's list of qiProjects
        return null;
    }

    /**
     * Set the selected qiProject as the default for the selected server.
     *
     * @param selectedServer The selected Tomcat server.
     * @param qispaceDir Directory of a qiProject selected in the list of
     * valid qiProjects. "" if resetting to no default.
     * @return 1 if a new default, 0 if already the default
     */
    public int setDefaultQispace(String selectedServer, String qispaceDir) {
        if (qispaceDir.equals(defaultQispaces.get(selectedServer))) {
            return 0;
        }
        defaultQispaces.put(selectedServer, qispaceDir);

        return 1;
    }

    /**
     * Get the default qiProject directory for the selected server.
     *
     * @param selectedServer The selected Tomcat server.
     * @return Full path of the default qiProject directory; empty string
     * if no default.
     */
    public String getDefaultQispace(String selectedServer) {
        return defaultQispaces.get(selectedServer);
    }

    /**
     * Remove the selected qiProject from the list of valid qiProjects for the selected
     * server. If the qiProject removed is the default, indicate there is no default.
     * Also, remove any desktops associated with the qiProject.
     *
     * @param selectedServer The selected Tomcat server.
     * @param qispaceDir Path of a qiProject selected from the list of valid qiProjectss.
     */
    public void removeQispace(String selectedServer, String qispaceDir) {
        ArrayList<QiSpaceDescriptor> serverQispaces = getServerQispaces(selectedServer);

        // Find the selected qiProject on the list and remove it
        for (int i = 0; i < serverQispaces.size(); i++) {
            if (serverQispaces.get(i).getPath().equals(qispaceDir)) {
                serverQispaces.remove(i);
                break;
            }
        }

        // Check if qiProject removed is the default
        if (qispaceDir.equals(getDefaultQispace(selectedServer))) {
            setDefaultQispace(selectedServer, NO_DEFAULT);
        // Remove desktop state files associated with the qiProject
        }
        removeAllDesktops(selectedServer, qispaceDir);

        String key = selectedServer + qispaceDir;
        // Remove default desktop state file associated with the qiProject
        defaultDesktops.remove(selectedServer + qispaceDir);
        // Remove from desktops' list
        desktops.remove(key);
    }

    /**
     * Remove all of the qiProjects associated with the selected server.
     * Indicate there is no default qiProject.
     * <p>
     * WARNING: Clearing all qiProjectss associated with a server also clears all
     * desktops associated with the qiProject and the defaults.
     *
     * @param selectedServer The server the qiProjectss are associated with.
     */
    public void removeAllQispaces(String selectedServer) {
        ArrayList<QiSpaceDescriptor> serverQispaces = getServerQispaces(selectedServer);

        // remove all desktops associated with each qiProject
        for (int i = 0; i < serverQispaces.size(); i++) {
            String qispaceDir = serverQispaces.get(i).getPath();
            desktops.remove(selectedServer + qispaceDir);
        }

        // clear the list of qiProjectss associated with the selected server
        serverQispaces.clear();
        setDefaultQispace(selectedServer, NO_DEFAULT);
    }

    // METHODS FOR DESKTOP (WORKBENCH) STATE/SESSION FILES
    /**
     * Associate the specified state file with the selected qiProject and server.
     *
     * @param selectedServer The selected Tomcat server.
     * @param selectedQispace The selected qiProject on the selected server
     * @param stateFile (full) path of qiproject state file.
     * @return 0 already on the list, 1 if add to the list
     */
    public int addDesktop(String selectedServer, String selectedQispace, String stateFile) {
        logger.info("attempting to match stateFile: " + stateFile + " with getStateFiles(" + selectedServer + ", " + selectedQispace + ")");
        ArrayList<String> stateFiles = getStateFiles(selectedServer, selectedQispace);

        // Check if state file already on the list
        for (int i = 0; i < stateFiles.size(); i++) {
            if (stateFiles.get(i).equals(stateFile)) {
                return 0;
            }
        }

        // State file not on the list; add to end of list
        stateFiles.add(stateFile);
        addQispace(selectedServer, new QiSpaceDescriptor(selectedQispace, QiSpaceDescriptor.WORKSPACE_KIND));

        return 1;
    }

    /**
     * Get list of valid state files for the selected qiproject on the selected server.
     * There is always a list, but it may be empty.
     *
     * @param selectedServer The selected Tomcat server.  It is always on the 
     * list of allowed servers. 
     * @param selectedQispace The name of a valid qiSpce for the selectedServer.
     * 
     * @return List of pathnames of valid state files for the qiProject and server.
     * List of pathnames may be empty but not null.
     */
    public ArrayList<String> getStateFiles(String selectedServer, String selectedQispace) {
        ArrayList<String> stateFiles = desktops.get(selectedServer + selectedQispace);
        if (stateFiles == null) {
            logger.warning("desktops.get() returned a null arraylist of statefiles - adding empty ArrayList<String> to map of server+qiProject => stateFiles");
            stateFiles = new ArrayList<String>();
            desktops.put(selectedServer + selectedQispace, stateFiles);
        }
        return stateFiles;
    }

    /**
     * Set the selected state file as the default for the selected qiProject and server.
     *
     * @param selectedServer The selected Tomcat server.
     * @param qispaceDir Directory of a selected qiProject on the selected server.
     * @param stateFile Pathname of a state file selected in the list of
     * valid qiProjects. "" if resetting to no default.
     * @return 1 if a new default, 0 if already the default
     */
    public int setDefaultDesktop(String selectedServer, String selectedQispace, String stateFile) {
        String key = selectedServer + selectedQispace;
        if (stateFile.equals(defaultDesktops.get(key))) {
            return 0;
        }
        defaultDesktops.put(key, stateFile);

        return 1;
    }

    /**
     * Get the default state file for the selected qiProject on the selected server.
     *
     * @param selectedServer The selected Tomcat server.
     * @param qispaceDir Directory of a selected qiProject on the selected server.
     * @return Full path of the default desktop state file; empty string
     * if no default.
     */
    public String getDefaultDesktop(String selectedServer, String selectedQispace) {
        return defaultDesktops.get(selectedServer + selectedQispace);
    }

    /**
     * Remove the selected state file from the list associated with the selected
     * qiProject on the selected server If the state file removed is the default,
     * indicate there is no default.
     *
     * @param selectedServer The selected Tomcat server.
     * @param qispaceDir Directory of a selected qiProject on the selected server.
     * @param stateFile Pathname of a state file selected in the list of
     * valid files.
     */
    public void removeDesktop(String selectedServer, String selectedQispace, String stateFile) {
        ArrayList<String> stateFiles = getStateFiles(selectedServer, selectedQispace);

        // Find the state file on the list and remove it
        for (int i = 0; i < stateFiles.size(); i++) {
            if (stateFiles.get(i).equals(stateFile)) {
                stateFiles.remove(i);
                break;
            }
        }

        // Check if state file removed is the default
        if (stateFile.equals(getDefaultDesktop(selectedServer, selectedQispace))) {
            setDefaultDesktop(selectedServer, selectedQispace, NO_DEFAULT);
        }
    }

    /**
     * Remove all of the desktop state files associated with a selected qiProject on a
     * selected server. Indicate there is no default saved desktop for the qiProject.
     *
     * @param selectedServer The selected Tomcat server.
     * @param selectedQispace Path of a selected qiProject on the selected server.
     */
    public void removeAllDesktops(String selectedServer, String selectedQispace) {
        ArrayList<String> stateFiles = getStateFiles(selectedServer, selectedQispace);
        // remove all items in the list
        stateFiles.clear();
        setDefaultDesktop(selectedServer, selectedQispace, NO_DEFAULT);
    }

    /**
     * Set the boolean flag that determines whether defaults are honored or
     * ignored on workbench startup. Return value indicates if flag changed value..
     *
     * @param flag true means honor defaults; false means ignore defaults.
     * @return 1 if flag changed value; 0 if flag unchanged.
     */
    public int setNeverAskAgain(boolean flag) {
        Boolean oldFlag = new Boolean(neverAskAgain);
        Boolean newFlag = new Boolean(flag);
        int flagsSame = oldFlag.compareTo(newFlag);
        neverAskAgain = flag;
        return flagsSame == 0 ? 0 : 1;
    }

    /**
     * Get the boolean value to see if the app should honor the defaults at
     * start up or ignore them.If there is a default server and default
     * project, launch the qiWorkbench if flag is true; otherwise, bring up
     * the Project Server Dialog.
     *
     * @return true means honor defaults; false means ignore defaults.
     */
    public boolean getNeverAskAgain() {
        return neverAskAgain;
    }

    /**
     * Add server, dataPath and saveSet to preference config.
     * If called from the JNLP servlet, we need to set them as default
     * Note: calling function is responsible to validate arguments
     *
     * @param serverUrl The selected Tomcat server.
     * @param dataPath The selected qiProject on the selected server
     * @param kind The kind of qiSpace: "W" (workspace of projects), "P" (project)
     * @param dataSet (full) path of qiProject state file.
     * @return
     */
    public void addServerQispaceDesktop(String serverUrl, String dataPath, String qispaceKind, String saveSet, boolean isDefault) {
        addServer(serverUrl);
        addQispace(serverUrl, new QiSpaceDescriptor(dataPath, qispaceKind));
        addDesktop(serverUrl, dataPath, saveSet);
        if (isDefault) {
            setDefaultServer(serverUrl);
            setDefaultQispace(serverUrl, dataPath);
            setDefaultDesktop(serverUrl, dataPath, saveSet);
        }
    }

    /** Dump the preferences */
    @Override
    public String toString() {
        StringBuffer sbuf = new StringBuffer();

        sbuf.append("User Preferences:\n");
        String indent1 = "  ";
        String indent2 = indent1 + "  ";
        String indent3 = indent2 + "  ";
        sbuf.append(indent2 + "Default Server: " + getDefaultServer() + "\n");
        for (int i = 0; i < servers.size(); i++) {
            String server = servers.get(i);
            sbuf.append(indent2 + "Runtime Tomcat Server: " + server + "\n");

            sbuf.append(indent2 + "Default qiProject: " + getDefaultQispace(server) + "\n");
            ArrayList<QiSpaceDescriptor> serverQispaces = getServerQispaces(server);
            for (int j = 0; j < serverQispaces.size(); j++) {
                String qispace = serverQispaces.get(j).getPath();
                String qispaceKind = serverQispaces.get(j).getKind();
                sbuf.append(indent2 + "qiProject: " + qispace + " kind: " + qispaceKind + "\n");

                sbuf.append(indent3 + "Default state file" + getDefaultDesktop(server, qispace) + "\n");
                ArrayList<String> qispaceDesktops = getStateFiles(server, qispace);
                for (int k = 0; k < qispaceDesktops.size(); k++) {
                    String desktop = qispaceDesktops.get(k);
                    sbuf.append(indent3 + "Desktop: " + desktop + "\n");
                }
            }
        }
        sbuf.append(indent1 + "Never Ask Again Flag: " + getNeverAskAgain());

        return sbuf.toString();
    }

    /**
     * Returns the local hostname, or else the string "Unknown" if the local hostname cannot be determined.
     *
     * @return hostName String representing the hostname as returned by InetAddress.getLocalHost().getHostName();
     */
    public static String getHostName() {
        String hostname = "Unknown";
        try {
            java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
            if (localMachine != null) {
                hostname = localMachine.getHostName();
            } else {
                logger.warning("Unable to determine hostname - InetAddress.getLocalHost() returned null");
            }
        } catch (java.net.UnknownHostException uhe) {
            logger.warning("Unable to determine hostname due to unknown host exception: " + uhe.getMessage());
        } catch (Exception e) {
            logger.warning("Unable to determine hostname due to exception: " + e.getMessage());
        }
        return hostname;
    }
}