/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Abstracts the running of a command script via native process.
 */
public class JobAdapter {
    private static Logger logger = Logger.getLogger(JobAdapter.class.getName());

    /** Job executing */
    Process job = null;

    /** Process builder */
    ProcessBuilder pb = null;

    public static final int bufSize = 256;

    /** Standard out of a command. Each item was a byte buffer. Output will
     * be lost if another command is executed before it is requested.
     */
    StringBuffer outBufs = new StringBuffer();

    /** Standard err of a command. Each item was a byte buffer. Output will
     * be lost if another command is executed before it is requested.
     */
    StringBuffer errBufs = new StringBuffer();

    //default setting for when reset
    List<String> defaultCommand;
    File defaultWorkingDir;
    Map<String, String> defaultEnvironment;

    /*
     * Create instance of a job adapter that abstracts the interface to a
     * native OS process that executes a command. A command must be specified
     * before the job can be executed.
     */
    public JobAdapter() {
        pb = new ProcessBuilder(new ArrayList<String>());
        defaultCommand = pb.command();
        defaultWorkingDir = pb.directory();
        defaultEnvironment = pb.environment();
    }

    /**
     * Reset the job adapter to its original default values.
     */
    public void reset() {
        pb = pb.command(defaultCommand);
        pb = pb.directory(defaultWorkingDir);

//THE FOLLOWING CODE PREVENTS THE JOB FROM TERMINATING NORMALLY. UNCLEAR WHY SO
//DON'T RESET THE ENVIRN MAP. COULD BE A JDK BUG.
/*
        Map<String, String> envMap = pb.environment();
        try {
            envMap.clear();
            envMap.putAll(defaultEnvironment);
        } catch (UnsupportedOperationException uoe) {
            logger.finest("envirn map does not support clear/putAll operation");
            //there is nothing we can do about this, so ignore
        }
*/
    }

    /**
     * Reset the command to be executed by the OS.
     *
     * @param cmd OS command to be executed and its arguments.
     * @throws QiwJobException If the command is null or empty.
     */
    public void resetCommand(List<String> cmd) throws QiwJobException  {
        if (cmd == null) {
            throw new QiwJobException("command is null");
        }
        if (cmd.isEmpty()) {
            throw new QiwJobException("command is empty");
        }
        pb = pb.command(cmd);
    }

    /**
     * Have the OS execute the submitted command.
     *
     * @throws QiwJobException If command could not be executed.
     */
    public void executeCommand(boolean doBlock) throws QiwJobException {
        if (pb == null) {
            throw new QiwJobException("cannot create job");
        }

        //clear the list holding the capture standard out and err
        outBufs.setLength(0);
        errBufs.setLength(0);

        try {
            //The command list cannot be empty, so IndexOutOfBoundsException
            //cannot occur.

            job = pb.start();
            JobStreamDigester outJsd = new JobStreamDigester(job.getInputStream(), outBufs);
            JobStreamDigester errJsd = new JobStreamDigester(job.getErrorStream(), errBufs);
            // capture standard out and err in parallel with job execution
            outJsd.start();
            errJsd.start();
            if (doBlock) {
                try {
                    outJsd.join();
                    errJsd.join();
                } catch (InterruptedException ie) {
                    logger.warning("While attempting to join job stdout, stderr digesters, caught: " + ie.getMessage());
                }
            }
        } catch (IOException ioe) {
            throw new QiwJobException(ioe.getMessage());
        } catch (NullPointerException npe) {
            throw new QiwJobException("command in list is null");
        }
    }

    /**
     * Have the OS execute the submitted command.
     *
     * @param cmd OS command to be executed and its arguments.
     * @throws QiwJobException If command could not be executed.
     */
    public void executeCommand(List<String> cmd, boolean doBlock) throws QiwJobException {
        resetCommand(cmd);
        executeCommand(doBlock);
    }

    /**
     * Set the working directory for the job.
     *
     * @param workingDir Path of new working directory. null or the empty string
     * means to use the working
     * directory of the current Java process
     */
    public void setWorkingDirectory(String workingDir) {
        if (workingDir == null || workingDir.equals("")) {
            pb = pb.directory(null);
        } else {
            File wd = new File(workingDir);
            pb = pb.directory(wd);
        }
    }

    public Map<String,String> getJobEnvironment(){
    	return pb.environment();
    }
    /**
     * Add an environment variable to the environment of the current process.
     * If the environment already contains the env variable, its value will be
     * replaced.
     *
     * @param envVar Environment variable
     * @param value Value of the environement variable
     * @throws QiwJobException If the OS will not accept the env variable and/or
     * its value.
     */
    public void addEnvVar(String envVar, String value) throws QiwJobException {
        Map<String,String> env = pb.environment();
        if (env.isEmpty()) {
            throw new QiwJobException("system does not support environment variables");
        }
        try {
            Map envMap = pb.environment();
            envMap.put(envVar, value);
        } catch (UnsupportedOperationException uoe) {
            throw new QiwJobException("operating system does not permit env variable "+envVar+" to be modified");
        } catch (IllegalArgumentException iae) {
            throw new QiwJobException("unacceptable env variable ("+envVar+") or value ("+value+")");
        }
    }

    /**
     * Get the exit status of the job
     *
     * @return -1 if running; 0 if terminated normally; >0 if terminated abnormally
     */
    public int jobStatus() {
        if (job != null) {
            try {
                int status = job.exitValue();
                return status;
            } catch (IllegalThreadStateException itse) {
                //job process has not yet terminated
                return -1;
            }
        }

        return 1;   //job never started
    }

    /**
     * Check if the job is still executing.
     *
     * @return true if running; false if terminated or never started.
     */
    public boolean isRunning() {
        boolean running = false;
        if (job != null) {
            try {
                int status = job.exitValue();
                return false;
            } catch (IllegalThreadStateException itse) {
                //job process has not yet terminated
                return true;
            }
        }

        return running;
    }

    /**
     * Kill the job. Does nothing if the job has already terminated.
     */
    public void killJob() {
        //check if the job has already terminated
        if (!isRunning()) return;

        if (job != null) {
            job.destroy();
        }
    }

    /**
     * Wait for the job to terminate. The calling thread will be blocked until
     * the subprocess exits. Returns immediately if the job has already
     * terminated or was never started.
     *
     * @return -1 if never started; 0 if terminated normally; >0 if terminated
     * abnormally
     */
    public int waitForTermination() {
        if (job == null) return -1;

        try {
            return job.waitFor();
        } catch (InterruptedException ie) {
        }

        return 2;   //interrupted
    }

    /**
     * Convert list of strings there were byte buffers to a list of lines
     *
     * @param buffers Strings that were byte buffers
     * @return List of lines output by the job; empty list if no output.
     */
    private ArrayList<String> convertToLines(StringBuffer buffers) {
        ArrayList<String> lines = new ArrayList<String>();
        logger.fine("buffers.length="+buffers.length());
        if (buffers.length() == 0) return lines;

        String eol = "\r\n";   //Windows end a line with CRLF
        int eolSize = 2;
        int idx = buffers.indexOf(eol);
        if (idx == -1) {
            eol = "\r";    //Linux end a line with LF
            eolSize = 1;
            idx = buffers.indexOf(eol);
            if (idx == -1) {
                eol = "\n";    //Macs end a line with CR
                eolSize = 1;
                idx = buffers.indexOf(eol);
            }
        }

        int start=0, end=0;
        while (idx != -1) {
            end = idx;
            String line = buffers.substring(start, end);
            lines.add(line);
            start = end + eolSize;  //ignore end-of-line
            idx = buffers.indexOf(eol, start);
        }

        return lines;
    }

    /**
     * Get the standard output of the job which was captured while the job
     * was running by a separate parallel task. The output was captured as
     * a byte stream and saved as a list of strings. These strings are
     * converted to lines and returned.
     *
     * @return List of lines output by the job; empty list if no output.
     */
    public ArrayList<String> getJobOutput() throws QiwIOException {
        //Convert output to lines
        ArrayList<String> lines = convertToLines(outBufs);

        //if line starts with 'IO error:', throw a QiwIOException. There was
        //an IOException in reading the job's standard out
        if (lines.size() == 1) {
            String line = lines.get(0);
            if (line.startsWith("IO error:"))
                throw new QiwIOException("line");
        }

        return lines;
    }

    /**
     * Get the standard error of the job which was captured while the job
     * was running by a separate parallel task. The output was captured as
     * a byte stream and saved as a list of strings. These strings are
     * converted to lines and returned.
     *
     * @return List of error lines output by the job; empty list; empty list if
     * no error output.
     */
    public ArrayList<String> getJobErrors() throws QiwIOException {
        //Convert output to lines
        ArrayList<String> lines = convertToLines(errBufs);

        //if line starts with 'IO error:', throw a QiwIOException. There was
        //an IOException in reading the job's standard err
        if (lines.size() == 1) {
            String line = lines.get(0);
            if (line.startsWith("IO error:"))
                throw new QiwIOException("line");
        }

        return lines;
    }
}
