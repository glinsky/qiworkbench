/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench;

import com.bhpb.qiworkbench.api.IQiSpaceDescriptor;
import java.io.Serializable;

/**
 * Container for qiSpace metadata.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class QiSpaceDescriptor implements IQiSpaceDescriptor {

    /**
     * The absolute path of the qiSpace.
     * The qiSpace may be the path of the qiProject or the root
     * directory of one or more qiProjects.
     */
    private String qispacePath = "";

    /**
     * Kind of qiSpace: "W" (workspace of projects); "P" (project).
     * Default is a project.
     */
    private String qispaceKind = PROJECT_KIND;

    public QiSpaceDescriptor(String path, String kind) {
        qispacePath = path;
        qispaceKind = kind;
    }

    //GETTERS
    public String getPath() {
        return qispacePath;
    }
    public String getKind() {
        return qispaceKind;
    }

    //SETTERS
    public void setPath(String path) {
        qispacePath = path;
    }
    public void setKind(String kind) {
        qispaceKind = kind;
    }

    /** Display attributes of qiSpace */
    public String toString() {
        StringBuffer buf  = new StringBuffer();
        buf.append(this.getClass().toString());
        buf.append(": \n qiSpace path=");
        buf.append(this.qispacePath);
        buf.append(", Kind of qiSpace=");
        buf.append(this.qispaceKind);
        return buf.toString();
    }
}
