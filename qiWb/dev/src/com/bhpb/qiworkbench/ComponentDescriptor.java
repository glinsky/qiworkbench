/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench;

import com.bhpb.qiworkbench.api.IComponentDescriptor;
import java.io.Serializable;

import com.bhpb.qiworkbench.messageFramework.IMsgHandler;

/**
 * Container for a component's attributes. Passed amongst components so a given
 * component knows everything it needs to know about another component. In particular
 * knows its message handler so it can communicate with it.
 */
public class ComponentDescriptor implements IComponentDescriptor {
//  static final long serialVersionUID = 0;
    /** Component ID, a unique, system wide identifier  */
    private String cid = "";

    /** Component's message handler */
    private IMsgHandler msgHandler = null;

    /** Kind of component */
    private String componentKind = "";

    /** Disply name (unique). For example, ampExt, bhpViewer, DeliveryLite,
     *  Local Reader, Remote Reader, Local Writer, Remote Writer,
     *  Local Job Processor, Remote Job Processor, Cluster Job Processor,
     */
    private String displayName = "";

    /** Unique name assigned by system or renamed by user. */
    private String preferredDisplayName = "";

    /** Provider. For example, BHP Billiton Petroleum. */
    private String providerName = "";

    /** Version of component. */
    private String versionNumber = "";

    //Getters
    public String getCID() {
        return cid;
    }

    public Object clone(){
        try  {
              return super.clone ();
        }
        catch (CloneNotSupportedException e) {
          throw new Error ("This should never happen!");
        }
    }
    public IMsgHandler getMsgHandler() {
        return msgHandler;
    }
    public String getComponentKind() {
        return componentKind;
    }
    public String getDisplayName() {
        return displayName;
    }
    public String getProviderName() {
        return providerName;
    }
    public String getVersionNumber() {
        return versionNumber;
    }

    public String getPreferredDisplayName() {
        if(preferredDisplayName.trim().length() == 0)
            return displayName;
        return preferredDisplayName;
    }

    //Setters
    public void setCID(String cid) {
        this.cid = cid;
    }
    public void setMsgHandler(IMsgHandler msgHandler) {
        this.msgHandler = msgHandler;
    }
    public void setComponentKind(String compKind) {
        componentKind = compKind;
    }
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    public void setPreferredDisplayName(String displayName) {
        this.preferredDisplayName = displayName;
    }
    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }
    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public int compareTo(IComponentDescriptor desc) {
        if(preferredDisplayName.trim().length() != 0){
            if(desc.getPreferredDisplayName().trim().length() != 0)
                return preferredDisplayName.compareTo(desc.getPreferredDisplayName());
            else
                return preferredDisplayName.compareTo(desc.getPreferredDisplayName());
        }else{
            if(desc.getPreferredDisplayName().trim().length() != 0)
                return displayName.compareTo(desc.getPreferredDisplayName());
            else
                return displayName.compareTo(desc.getPreferredDisplayName());
        }
    }

    /** Display attributes of component */
    public String toString() {
        StringBuffer buf  = new StringBuffer();
        buf.append(this.getClass().toString());
        buf.append(": \n Component ID=");
        buf.append(cid);
        buf.append(", Msg Handler=");
        if (msgHandler == null) buf.append("NO_MSG_HANDLER");
        else buf.append(msgHandler.getClass().getName());
        buf.append(", Component Kind=");
        buf.append(componentKind);
        buf.append(", Display Name=");
        buf.append(displayName);
        buf.append(", Preferred Display Name=");
        buf.append(getPreferredDisplayName());
    buf.append(", Provider=");
        buf.append(getProviderName());
    buf.append(", Version=");
        buf.append(getVersionNumber());
        return buf.toString();
    }
}
