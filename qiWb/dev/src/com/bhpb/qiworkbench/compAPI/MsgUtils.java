/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.compAPI;

import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import java.util.logging.Logger;

/**
 * Message utilities.
 *
 * @author Gil Hansen.
 * @version 1.0
 */
public class MsgUtils {
    private static Logger logger = Logger.getLogger(MsgUtils.class.getName());

    static long lastMsgId = 0;
    /**
     * Generate a unique message ID (MSGID). The MSGID is the current time
     * and date. However, if the new MSGID is less than or equal to the last one,
     * the last one is incremented by 1 to make it unique.
     *
     * @return Unique message ID
     */
    static public String genMsgID() {
        long msgId = System.currentTimeMillis();
        if (msgId <= lastMsgId) msgId = ++lastMsgId;
        lastMsgId = msgId;
        return Long.toString(msgId);
    }

    /**
     * Get the message's ID.
     * @param msg The message
     * @return String message ID
     */
    static public String getMsgID(IQiWorkbenchMsg msg) {
        return msg.getMsgID();
    }

    /**
     * Swap CIDs. Used to turn a request into a response.
     */
    static public void swapCIDs(IQiWorkbenchMsg msg) {
        String cid = msg.getProducerCID();
        msg.setProducerCID(msg.getConsumerCID());
        msg.setConsumerCID(cid);
    }

    /**
     * Get the display name of a message status code.
     *
     * @return Display name of the message status code.
     */
    static public String getStatusName(int sc) {
        switch (sc) {
            case MsgStatus.SC_UNKNOWN:
                return MsgStatus.SC_UNKNOWN_NAME;

            case MsgStatus.SC_OK:
                return MsgStatus.SC_OK_NAME;

            case MsgStatus.SC_INTERNAL_ERROR:
                return MsgStatus.SC_INTERNAL_ERROR_NAME;

            case MsgStatus.SC_EXCEPTION:
                return MsgStatus.SC_EXCEPTION_NAME;

            case MsgStatus.SC_RESPONSE_TIMEOUT:
                return MsgStatus.SC_RESPONSE_TIMEOUT_NAME;

            case MsgStatus.SC_SERVER_DOWN:
                return MsgStatus.SC_SERVER_DOWN_NAME;

            case MsgStatus.SC_SERVER_DOES_NOT_EXIST:
                return MsgStatus.SC_SERVER_DOES_NOT_EXIST_NAME;

            case MsgStatus.SC_SOCKET_CHANNEL_ERROR:
                return MsgStatus.SC_SOCKET_CHANNEL_ERROR_NAME;
                
            case MsgStatus.SC_SOCKET_LIMIT_EXCEEDED:
                return MsgStatus.SC_SOCKET_LIMIT_EXCEEDED_NAME;

            default:
                return "";
        }
    }

    /**
     * Create a new abnormal response message. Populate with info from the request.
     *
     * @param request The request message that failed.
     * @param status The abnormal status code
     * @param cause The cause of the abnormal condition.
     * @return A QiWorkbench message marked with the abnormal condition
     *
     */
    static public IQiWorkbenchMsg genAbnormalMsg(IQiWorkbenchMsg request, int status, String cause) {
        // swap CIDs at the same time
        IQiWorkbenchMsg msg = new QiWorkbenchMsg(request.getConsumerCID(), request.getProducerCID(), QIWConstants.DATA_MSG, request.getCommand(), request.getMsgID(), QIWConstants.STRING_TYPE, cause,request.skip());
        msg.setStatusCode(status);

        return msg;
    }

    //TODO Convenience methods to analyze an abnormal message based on its
    //abnormal status code and the cause. Return a Cause Code (CC) that
    //encodes the result of the analysis.

    /**
     * Check if a response is abnormal.
     *
     * @param response The response message.
     * @return true if an abnormal response; otherwise, false.
     */
    static public boolean isResponseAbnormal(IQiWorkbenchMsg response) {
        return response.isAbnormalStatus();
    }

    /**
     * Get the content of a message. It is up to the caller to cast the
     * content to the proper type as specified in QIWConstants.
     *
     * @return Content of the message as an Object.
     */
     static public Object getMsgContent(IQiWorkbenchMsg msg) {
         return msg.getContent();
     }

     /**
      * Get message's command.
      *
      * @param msg A workbench request/response message.
      * @return Message's command
      */
     static public String getMsgCommand(IQiWorkbenchMsg msg) {
         return msg.getCommand();
     }

     /**
      * Get message's status code
      *
      * @param msg A workbench request/response message
      * @return Message's status code
      */
     static public int getMsgStatusCode(IQiWorkbenchMsg msg) {
         return msg.getStatusCode();
     }

     /**
      * Get string representation of a message.
      *
      * @param msg A workbench message.
      * @return String representation of the message
      */
    static public String toString(IQiWorkbenchMsg msg) {
        return msg.toString();
    }
}
