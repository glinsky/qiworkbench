package com.bhpb.qiworkbench.compAPI;
import java.awt.Dimension;
import java.awt.Insets;
import javax.swing.DefaultDesktopManager;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JViewport;

class MDIDesktopManager extends DefaultDesktopManager {

    private MDIDesktopPane desktop;

    /**
     * Construct the MDIDesktopManager.
     * @param desktop The desktop which will be managed by this instance.
     */
    public MDIDesktopManager(MDIDesktopPane desktop) {
        this.desktop = desktop;
    }

    /**
     * After the frame is resized, resize the MDIDesktopPane for proper
     * scrollbar behavior.
     * 
     * @param f The JComponent being resized
     */
    @Override
    public void endResizingFrame(JComponent f) {
        super.endResizingFrame(f);
        resizeDesktop();
    }

    /**
     * After the frame is dragged, resize the MDIDesktopPane for proper
     * scrollbar behavior.
     * 
     * @param f The JComponent being resized
     */
    @Override
    public void endDraggingFrame(JComponent f) {
        super.endDraggingFrame(f);
        resizeDesktop();
    }

    /**
     * Set the min, max and preferred sizes of this MDIDesktopPane based on the
     * visible rectangle of the enclosing JScrollPane, if any.  If there no
     * JScrollPane wraps this MDIDesktopPane, the sizes are not changed.
     */
    public void setCurrentVisibleSize() {
        JScrollPane scrollPane = getScrollPane();
        
        if (scrollPane != null) {
            int x = 0;
            int y = 0;
            Insets scrollInsets = getScrollPaneInsets();

            Dimension d = scrollPane.getVisibleRect().getSize();
            Insets scrollPaneInsets = getScrollPaneInsets();
            if (scrollPane.getBorder() != null && scrollPaneInsets != null) {
                d.setSize(d.getWidth() - scrollInsets.left - scrollInsets.right,
                        d.getHeight() - scrollInsets.top - scrollInsets.bottom);
            }

            d.setSize(d.getWidth() - 20, d.getHeight() - 20);
            desktop.setAllSizes( new Dimension(x, y));
            scrollPane.revalidate();
        }
    }

    private Insets getScrollPaneInsets() {
        JScrollPane scrollPane = getScrollPane();
        if (scrollPane != null) {
            return getScrollPane().getBorder().getBorderInsets(scrollPane);
        } else {
            return null;
        }
    }

    private JScrollPane getScrollPane() {
        if (desktop.getParent() instanceof JViewport) {
            JViewport viewPort = (JViewport) desktop.getParent();
            if (viewPort.getParent() instanceof JScrollPane) {
                return (JScrollPane) viewPort.getParent();
            }
        }
        return null;
    }

    /**
     * Set the minimum, maximum and preferred sizes of the associated MDIDesktopPane
     * to precisely fill the visible rectangle, except for the JScrollPane decoration
     * and its border and insets, if any.
     */
    //TODO This method causes the JScrollPane to be displayed if one or more JInternalFrames
    //overlap the area in which the scrollbars or the JScrollPane's border or insets would be drawn if
    //visible, even if the scrollbars would not normally need to be visible because
    //no JInternalFrames reach outside of the MDIDesktopPane's bounds.  Fix this bug.
    protected void resizeDesktop() {
        JScrollPane scrollPane = getScrollPane();
        if (scrollPane != null) {
            JInternalFrame allFrames[] = desktop.getAllFrames();
            int x = 0;
            int y = 0;
            
            for (int i = 0; i < allFrames.length; i++) {
                if (allFrames[i].getX() + allFrames[i].getWidth() > x) {
                    x = allFrames[i].getX() + allFrames[i].getWidth();
                }
                if (allFrames[i].getY() + allFrames[i].getHeight() > y) {
                    y = allFrames[i].getY() + allFrames[i].getHeight();
                }
            }
            Dimension d = scrollPane.getVisibleRect().getSize();
            
            Insets scrollPaneInsets = getScrollPaneInsets();
            if (scrollPane.getBorder() != null && scrollPaneInsets != null) {
                d.setSize(d.getWidth() - scrollPaneInsets.left - scrollPaneInsets.right,
                        d.getHeight() - scrollPaneInsets.top - scrollPaneInsets.bottom);
            }

            if (x <= d.getWidth()) {
                x = ((int) d.getWidth()) - 20;
            }
            if (y <= d.getHeight()) {
                y = ((int) d.getHeight()) - 20;
            }
            desktop.setAllSizes( new Dimension(x, y));
            scrollPane.revalidate();
        }
    }
}