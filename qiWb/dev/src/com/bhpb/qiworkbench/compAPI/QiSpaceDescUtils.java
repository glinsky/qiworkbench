/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.compAPI;

import java.lang.Class;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.QiSpaceDescriptor;

/**
 * qiSpace descriptor utilities.
 *
 * @author Gil Hansen.
 * @version 1.0
 */
public class QiSpaceDescUtils {
    private static Logger logger = Logger.getLogger(QiSpaceDescUtils.class.getName());

    /**
     * Get the absolute path of the qiSpace.
     * The qiSpace may be the path of a qiProject or the root
     * directory of one or more qiProjects.
     * @param desc qiSpace descriptor
     * @return The full path of the qiSpace
     */
    static public String getQiSpacePath(QiSpaceDescriptor desc) {
        return desc.getPath();
    }

    /**
     * Check if the qiSpace is a project.
     * @return true if the qiSpace is a project; otherwise, false
     */
    static public boolean isQiSpaceProject(QiSpaceDescriptor desc) {
        return desc.getKind().equals(QiSpaceDescriptor.PROJECT_KIND) ? true : false;
    }

    /**
     * Check if the qiSpace is a workspace of projects.
     * @return true if the qiSpace is a workspace; otherwise, false
     */
    static public boolean isQiSpaceWorkspace(QiSpaceDescriptor desc) {
        return desc.getKind().equals(QiSpaceDescriptor.WORKSPACE_KIND) ? true : false;
    }
}
