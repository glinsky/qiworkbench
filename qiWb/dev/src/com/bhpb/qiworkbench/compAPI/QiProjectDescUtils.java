/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.compAPI;

import java.awt.Component;
import java.lang.Class;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.io.File;

import javax.swing.JOptionPane;

import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.workbench.ProjectSetupConfirmDialog;
import com.bhpb.qiworkbench.workbench.UpdateProjectDirStructureDialog;

/**
 * qiProject descriptor utilities.
 *
 * @author Gil Hansen.
 * @version 1.0
 */
public class QiProjectDescUtils {
    private static Logger logger = Logger.getLogger(QiProjectDescUtils.class.getName());

    //Getters
    /**
     * Get the absolute path of the qiSpace that contains the project.
     * The qiSpace may be the path of the qiProject or the root
     * directory of one or more qiProjects.
     * @param desc qiProject descriptor
     * @return The path of the qiSpace
     */
    static public String getQiSpace(QiProjectDescriptor desc) {
        return desc.getQiSpace();
    }
    /**
     * Get the project ID.
     * @param desc qiProject descriptor
     * @return The ID of the project.
     */
    static public String getPid(QiProjectDescriptor desc) {
        return desc.getPid();
    }
    /**
     * Get the location of the qiProject relative to the qiSpace.
     * If the qiSpace contains one or more projects, the relative
     * location is the name of the qiProject; otherwise, the OS file
     * separator.
     * @param desc qiProject descriptor
     * @return The location of the qiProject relative to the qiSpace.
     */
    static public String getQiProjectReloc(QiProjectDescriptor desc) {
        return desc.getQiProjectReloc();
    }
    /**
     * Get the display name of the project. Typically, the name of the
     * directory containing the project data.
     * @param desc qiProject descriptor
     * @return The display name of the project.
     */
    static public String getQiProjectName(QiProjectDescriptor desc) {
        return desc.getQiProjectName();
    }
    /**
     * Get the location of the project's datasets relative to the qiProject.
     * The default is 'datasets'. 'int_datasets' will be recognized
     * for backward compatiblity with existing datasets.
     * @param desc qiProject descriptor
     * @return The location of the project's datasets relative to the qiProject.
     */
    static public String getDatasetsReloc(QiProjectDescriptor desc) {
        return desc.getDatasetsReloc();
    }
    /**
     * Get the location of the project's savesets relative to the qiProject.
     * The default is 'sessions'.
     * @param desc qiProject descriptor
     * @return The location of the project's savesets relative to the qiProject.
     */
    static public String getSavesetsReloc(QiProjectDescriptor desc) {
        return desc.getSavesetsReloc();
    }
    /**
     * Get the location of the project's horizons relative to the qiProject.
     * The default is 'horizons'.
     * @param desc qiProject descriptor
     * @return The location of the project's horizons relative to the qiProject.
     */
    static public String getHorizonsReloc(QiProjectDescriptor desc) {
        return desc.getHorizonsReloc();
    }
    /**
     * Get the location of the project's generated scripts relative to the qiProject.
     * The default is 'scripts'.
     * @param desc qiProject descriptor
     * @return The location of the project's hand written scripts relative to the qiProject.
     */
    static public String getScriptsReloc(QiProjectDescriptor desc) {
        return desc.getScriptsReloc();
    }
    /**
     * Get the location of the project's well logs relative to the qiProject.
     * The default is 'wells'.
     * @param desc qiProject descriptor
     * @return The location of the project's well logs relative to the qiProject.
     */
    static public String getWellsReloc(QiProjectDescriptor desc) {
        return desc.getWellsReloc();
    }
    /**
     * Get the location of the project's workbench sessions relative to the qiProject.
     * The default is 'workbenches'.
     * @param desc qiProject descriptor
     * @return The location of the project's workbench sessions relative to the qiProject.
     */
    static public String getWorkbenchesReloc(QiProjectDescriptor desc) {
        return desc.getWorkbenchesReloc();
    }
    /**
     * Get the location of the project's temporary directory relative to the qiProject.
     * The default is 'tmp'.
     * @param desc qiProject descriptor
     * @return The location of the project's temporary directory relative to the qiProject.
     */
    static public String getTempReloc(QiProjectDescriptor desc) {
        return desc.getTempReloc();
    }
    /**
     * Get the location of the project's XML directory relative to the qiProject.
     * The default is 'xml'.
     * @param desc qiProject descriptor
     * @return The location of the project's temporary directory relative to the qiProject.
     */
    static public String getXmlReloc(QiProjectDescriptor desc) {
        return desc.getXmlReloc();
    }
    /**
     * Get the location of the project's SEGY directory relative to the qiProject.
     * The default is 'segy'.
     * @param desc qiProject descriptor
     * @return The location of the project's SEGY directory relative to the qiProject.
     */
    static public String getSegyReloc(QiProjectDescriptor desc) {
        return desc.getSegyReloc();
    }
    
    /**
     * Get the list of seismic (BHP-SU) directories for the qiProject.
     * The default is a list of one: 'seismics'
     * @param desc qiProject descriptor
     * @return The list of a qiProject's seismic directories
     */
    static public ArrayList<String> getSeismicDirs(QiProjectDescriptor desc) {
        return desc.getSeismicDirs();
    }

    /**
     * Get the full path of the project directory.
     * @param desc qiProject descriptor
     * @return The full path of the project directory.
     */
    static public String getProjectPath(QiProjectDescriptor desc) {
        return desc.getQiSpace() + ((desc.getQiSpace().indexOf('/') != -1) ? "/" : "\\") + desc.getQiProjectReloc();
    }
    /**
     * Get the full path of the datasets directory.
     * @param desc qiProject descriptor
     * @return The full path of the datasets directory.
     */
    static public String getDatasetsPath(QiProjectDescriptor desc) {
        return getProjectPath(desc) + ((desc.getQiSpace().indexOf('/') != -1) ? "/" : "\\") + desc.getDatasetsReloc();
    }
    /**
     * Get the full path of the savesets directory.
     * @param desc qiProject descriptor
     * @return The full path of the savesets directory.
     */
    static public String getSavesetsPath(QiProjectDescriptor desc) {
        return getProjectPath(desc) + ((desc.getQiSpace().indexOf('/') != -1) ? "/" : "\\") + desc.getSavesetsReloc();
    }
    /**
     * Get the full path of the horizons directory.
     * @param desc qiProject descriptor
     * @return The full path of the horizons directory.
     */
    static public String getHorizonsPath(QiProjectDescriptor desc) {
        return getProjectPath(desc) + ((desc.getQiSpace().indexOf('/') != -1) ? "/" : "\\") + desc.getHorizonsReloc();
    }
    /**
     * Get the full path of the scripts directory.
     * @param desc qiProject descriptor
     * @return The full path of the scripts directory.
     */
    static public String getScriptsPath(QiProjectDescriptor desc) {
        return getProjectPath(desc) + ((desc.getQiSpace().indexOf('/') != -1) ? "/" : "\\") + desc.getScriptsReloc();
    }
    /**
     * Get the full path of the wells directory.
     * @param desc qiProject descriptor
     * @return The full path of the wells directory.
     */
    static public String getWellsPath(QiProjectDescriptor desc) {
        return getProjectPath(desc) + ((desc.getQiSpace().indexOf('/') != -1) ? "/" : "\\") + desc.getWellsReloc();
    }
    /**
     * Get the full path of the workbenches directory.
     * @param desc qiProject descriptor
     * @return The full path of the workbenches directory.
     */
    static public String getWorkbenchesPath(QiProjectDescriptor desc) {
        return getProjectPath(desc) + ((desc.getQiSpace().indexOf('/') != -1) ? "/" : "\\") + desc.getWorkbenchesReloc();
    }
    /**
     * Get the full path of the temporary directory.
     * @param desc qiProject descriptor
     * @return The full path of the temporary directory.
     */
    static public String getTempPath(QiProjectDescriptor desc) {
        return getProjectPath(desc) + ((desc.getQiSpace().indexOf('/') != -1) ? "/" : "\\") + desc.getTempReloc();
    }
    /**
     * Get the full path of the XML directory.
     * @param desc qiProject descriptor
     * @return The full path of the XML directory.
     */
    static public String getXmlPath(QiProjectDescriptor desc) {
        return getProjectPath(desc) + ((desc.getQiSpace().indexOf('/') != -1) ? "/" : "\\") + desc.getXmlReloc();
    }
    /**
     * Get the full path of the SEGY directory.
     * @param desc qiProject descriptor
     * @return The full path of the SEGY directory.
     */
    static public String getSegyPath(QiProjectDescriptor desc) {
        return getProjectPath(desc) + ((desc.getQiSpace().indexOf('/') != -1) ? "/" : "\\") + desc.getSegyReloc();
    }    
    /**
     * Get the full path of the seismic directories.
     * @param desc qiProject descriptor
     * @return The full path of the seismic directories.
     */
    static public ArrayList<String> getSeismicPaths(QiProjectDescriptor desc) {
        ArrayList<String> paths = new ArrayList<String>();
        ArrayList<String> dirs = desc.getSeismicDirs();
        for (int i=0; i<dirs.size(); i++) {
            paths.add(getProjectPath(desc) + ((desc.getQiSpace().indexOf('/') != -1) ? "/" : "\\") + dirs.get(i));
        }

        return paths;
    }

    //Setters
    /**
     * Set the project ID.
     * @param desc qiProject descriptor
     * @return The ID of the project.
     */
    static public void setPid(QiProjectDescriptor desc, String pid) {
        desc.setPid(pid);
    }
    /**
     * Set the absolute path of the qiSpace that contains the project.
     * @param desc qiProject descriptor
     * @param qiSpace The absolute path of the qiSpace that contains the project.
     */
    static public void setQiSpace(QiProjectDescriptor desc, String qiSpace) {
        desc.setQiSpace(qiSpace);
    }
    /**
     * Set the path of the qiProject relative to the qiSpace.
     * @param desc qiProject descriptor
     * @param qiProjectReloc The absolute path of the qiSpace that contains the project.
     */
    static public void setQiProjectReloc(QiProjectDescriptor desc, String qiProjectReloc) {
        desc.setQiProjectReloc(qiProjectReloc);
    }
    /**
     * Set the display name of the project.
     * @param desc qiProject descriptor
     * @param qiProjectName The display name of the project.
     */
    static public void setQiProjectName(QiProjectDescriptor desc, String qiProjectName) {
        desc.setQiProjectName(qiProjectName);
    }
    /**
     * Set the location of the project's datasets relative to the qiProject.
     * @param desc qiProject descriptor
     * @param datasetsReloc The location of the project's datasets relative to the qiProject.
     */
    static public void setDatasetsReloc(QiProjectDescriptor desc, String datasetsReloc) {
        desc.setDatasetsReloc(datasetsReloc);
    }
    /**
     * Set the location of the project's savesets relative to the qiProject.
     * @param desc qiProject descriptor
     * @param savesetsReloc The location of the project's savesets relative to the qiProject.
     */
    static public void setSavesetsReloc(QiProjectDescriptor desc, String savesetsReloc) {
        desc.setSavesetsReloc(savesetsReloc);
    }
    /**
     * Set the location of the project's horizons relative to the qiProject.
     * @param desc qiProject descriptor
     * @param horizonsReloc The location of the project's horizons relative to the qiProject.
     */
    static public void setHorizonsReloc(QiProjectDescriptor desc, String horizonsReloc) {
        desc.setHorizonsReloc(horizonsReloc);
    }
    /**
     * Set the location of the project's generated scripts relative to the qiProject.
     * @param desc qiProject descriptor
     * @param scriptsReloc The location of the project's generated scripts relative to the qiProject.
     */
    static public void setScriptsReloc(QiProjectDescriptor desc, String scriptsReloc) {
        desc.setScriptsReloc(scriptsReloc);
    }
    /**
     * Set the location of the project's well logs relative to the qiProject.
     * @param desc qiProject descriptor
     * @param wellsReloc The location of the project's well logs relative to the qiProject.
     */
    static public void setWellsReloc(QiProjectDescriptor desc, String wellsReloc) {
        desc.setWellsReloc(wellsReloc);
    }
    /**
     * Set the location of the project's workbenche sessions relative to the qiProject.
     * @param desc qiProject descriptor
     * @param workbenchesReloc The location of the project's workbench sessions relative to the qiProject.
     */
    static public void setWorkbenchesReloc(QiProjectDescriptor desc, String workbenchesReloc) {
        desc.setWorkbenchesReloc(workbenchesReloc);
    }
    /**
     * Set the location of the project's temporary directory relative to the qiProject.
     * @param desc qiProject descriptor
     * @param tempReloc The location of the project's temporary directory relative to the qiProject.
     */
    static public void setTempReloc(QiProjectDescriptor desc, String tempReloc) {
        desc.setTempReloc(tempReloc);
    }
    /**
     * Set the location of the project's XML directory relative to the qiProject.
     * @param desc qiProject descriptor
     * @param xmlReloc The location of the project's XML directory relative to the qiProject.
     */
    static public void setXmlReloc(QiProjectDescriptor desc, String xmlReloc) {
        desc.setXmlReloc(xmlReloc);
    }
    /**
     * Set the location of the project's SEGY directory relative to the qiProject.
     * @param desc qiProject descriptor
     * @param segyReloc The location of the project's SEGY directory relative to the qiProject.
     */
    static public void setSegyReloc(QiProjectDescriptor desc, String segyReloc) {
        desc.setSegyReloc(segyReloc);
    }
    /**
     * Set the list of seismic directories for the qiProject.
     * @param desc qiProject descriptor
     * @param seismicDirs The list of the project's seismic directories
     */
    static public void setSeismicDirs(QiProjectDescriptor desc, ArrayList<String> seismicDirs) {
        desc.setSeismicDirs(seismicDirs);
    }
    /**
     * Add a seismic directory to the list of the project's seismic directories.
     * Note: No check is made to determine if the seismic directory is already on the list.
     * @param desc qiProject descriptor
     * @param seismicDir Seismic directory
     */
    static public void addSeismicDir(QiProjectDescriptor desc, String seismicDir) {
        desc.getSeismicDirs().add(seismicDir);
    }

    /**
     * Show a dialog to confirm with user about setting up a list of project meta data
     * @param comp: parent GUI component which requests this dialog
     * @param modal: indicates if this dialog is modal
     * @param list: list of project directories that don't exist
     * @param projectRoot: Full path of the project
     * @return Exit status of dialog, i.e., whether it was cancelled or not.
     */
    public static int showQiProjectMetaDataSetupDialog(Component comp, boolean modal, List<String> list, String projectRoot) {
        ProjectSetupConfirmDialog dialog = new ProjectSetupConfirmDialog(comp, modal, list, projectRoot);
        dialog.setLocationRelativeTo(comp);
        dialog.setVisible(true);
        return dialog.getReturnStatus();
    }

    public static List<File> getUnMadeLocalDirs(QiProjectDescriptor projDesc, String filesep) {
        if (projDesc == null)
            return null;

        List<File> dirs = new ArrayList<File>();
        String root = getProjectPath(projDesc);
        File rootDir = new File(root);
        if (!rootDir.exists()){
            return null;
        }
        File aDir = new File(rootDir + filesep + projDesc.getDatasetsReloc());
        if (!aDir.exists()){
            dirs.add(aDir);
        }
        aDir = new File(rootDir + filesep + projDesc.getHorizonsReloc());
        if (!aDir.exists()){
            dirs.add(aDir);
        }
        aDir = new File(rootDir + filesep + projDesc.getSavesetsReloc());
        if (!aDir.exists()){
            dirs.add(aDir);
        }
        aDir = new File(rootDir + filesep + projDesc.getScriptsReloc());
        if (!aDir.exists()){
            dirs.add(aDir);
        }
        aDir = new File(rootDir + filesep + projDesc.getWellsReloc());
        if (!aDir.exists()){
            dirs.add(aDir);
        }
        aDir = new File(rootDir + filesep + projDesc.getWorkbenchesReloc());
        if (!aDir.exists()){
            dirs.add(aDir);
        }
        String tmpDir = projDesc.getTempReloc();
        if (tmpDir.startsWith(filesep + "tmp")) {
            aDir = new File(tmpDir);
            if (!aDir.exists()){
                dirs.add(aDir);
            }
        } else {
            aDir = new File(rootDir + filesep + projDesc.getTempReloc());
            if (!aDir.exists()) {
                dirs.add(aDir);
            }
        }
        aDir = new File(rootDir + filesep + projDesc.getXmlReloc());
        if (!aDir.exists()){
            dirs.add(aDir);
        }
        aDir = new File(rootDir + filesep + projDesc.getSegyReloc());
        if (!aDir.exists()){
            dirs.add(aDir);
        }

        
        List<String> list = projDesc.getSeismicDirs();
        for (int i = 0; list != null && i < list.size(); i++) {
            aDir = new File(rootDir + filesep + list.get(i));
            if (!aDir.exists()) {
                dirs.add(aDir);
            }
        }
        return dirs;
    }
}
