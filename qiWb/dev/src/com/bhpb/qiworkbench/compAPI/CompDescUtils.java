/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.compAPI;

import com.bhpb.qiworkbench.api.IComponentDescriptor;
import java.util.logging.Logger;

/**
 * Component Descriptor utilities.
 *
 * @author Gil Hansen.
 * @version 1.0
 */
public class CompDescUtils {
    private static Logger logger = Logger.getLogger(CompDescUtils.class.getName());

    /**
     * Get component's CID
     *
     * @param desc A component's descriptor
     * @return Component's CID
     */
    static public String getDescCID(IComponentDescriptor desc) {
        return desc.getCID();
    }

    /**
     * Get component's kind.
     *
     * @param desc A component's descriptor
     * @return String Kind of component
     */
    static public String getDescComponentKind(IComponentDescriptor desc) {
        return desc.getComponentKind();
    }

    /**
     * Get component's preferred display name.
     *
     * @param desc A component's descriptor
     * @return String Component's preferred display name
     */
    static public String getDescPreferredDisplayName(IComponentDescriptor desc) {
        return desc.getPreferredDisplayName();
    }

    /**
     * Get component's display name.
     *
     * @param desc A component's descriptor.
     * @return String Component's display name.
     */
    static public String getDescDisplayName(IComponentDescriptor desc) {
        return desc.getDisplayName();
    }

    /**
     * Set component's preferred display name
     *
     * @param desc A component's descriptor.
     * @param displayName Preferred display name.
     */
    static public void setDescPreferredDisplayName(IComponentDescriptor desc, String displayName) {
        desc.setPreferredDisplayName(displayName);
    }
}
