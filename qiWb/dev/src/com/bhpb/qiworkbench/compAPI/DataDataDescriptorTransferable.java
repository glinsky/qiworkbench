package com.bhpb.qiworkbench.compAPI;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.compAPI.DataDataDescriptor;
/*
 * DataDataDescriptorTransferable.java
 *
 * Created on July 11, 2007, 11:12 AM
 */

/**
 * DataDataDescriptorTransferable
 * @author Marcus Vaal
 */
public class DataDataDescriptorTransferable implements Transferable, ClipboardOwner {
	
	private static Logger logger = Logger.getLogger(DataDataDescriptorTransferable.class.getName());	
    
    public static DataFlavor dataDataDescriptorFlavor = null;
    public static DataFlavor localDataDataDescriptorFlavor = null;
    private DataDataDescriptor DDD;
    
    //Define the DataFlavors
    static {
        try {
            dataDataDescriptorFlavor = new DataFlavor(DataDataDescriptor.class, "Non local DataDataDescriptor");
            localDataDataDescriptorFlavor = new DataFlavor(QIWConstants.LOCAL_DATA_DATA_DESCRIPTOR, "Local DataDataDescriptor");
        } catch(Exception e) {
            logger.finest(e.toString());
        }
    }
    
    //Put flavors into array
    public static final DataFlavor[] flavors = {
        DataDataDescriptorTransferable.dataDataDescriptorFlavor,
        DataDataDescriptorTransferable.localDataDataDescriptorFlavor
    };
    
    private static final List flavorList = Arrays.asList( flavors );
    
    /**
     * DataDataDescriptorTransferable Constructor
     */
    public DataDataDescriptorTransferable() {
    }
    
    /**
     * DataDataDescriptorTransferable Constructor
     * @param DDD DataDataDescriptor Object to be transfered
     */
    public DataDataDescriptorTransferable(DataDataDescriptor DDD) {
        this.DDD = DDD;
    }

    /**
     * Called when ownership of the object is lost
     */
    public void lostOwnership(Clipboard clipboard, Transferable contents) {
    }

    /**
     * Gets the transferable Data Flavors
     * @return Array of DataFlavors
     */
    public DataFlavor[] getTransferDataFlavors() {
        return flavors;
    }

    /**
     * Checks if a DataFlavor is supported
     * @return Returns true if supported, false otherwise
     */
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return ( flavorList.contains( flavor ) );
    }

    /**
     * Gets Transferable Data
     * @return Object that is a DataDataDescriptor
     */
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        return this.DDD;
    }

    /**
     * Overrides Object toString
     */
    public String toString() {
        return "DataDataDescriptorTransferable";
    }

}
