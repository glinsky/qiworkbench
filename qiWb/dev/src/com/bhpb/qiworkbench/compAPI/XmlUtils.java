/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.compAPI;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * XML utilities.
 * 
 * @author Gil Hansen.
 * @author Woody Folsom added generic XML-related methods getNonTextChild(Node) and toFormattedXMLString(Document)
 * 
 * @version 1.1
 */
public class XmlUtils {
    private static Logger logger = Logger.getLogger(XmlUtils.class.getName());

    /**
     * Convert an object instance into equivalent XML.
     * <p>
     * Assumptions:
     * <ul>
     * <li>All fields are strings or ArrayList whose items are strings.</li>
     * <li>For each field fooBar, there is a getter of the form getFooBar.</li>
     * </ul>
     *
     * @param obj Instance of the object to convert to XML
     * @return The XML equivalent of the object
     */
    static public String objectToXml(Object obj) {
        StringBuffer sbuf = new StringBuffer();

        Class klass = obj.getClass();
        //get fully qualified name
        String klassName = klass.getName();
        int idx = klassName.lastIndexOf(".");
        String kName = klassName.substring(idx+1);
        sbuf.append("    <"+kName+" type=\""+klassName+"\">\n");

        Method[] methods = klass.getMethods();
        for (int i=0; i<methods.length; i++) {
            //Filter out getters
            //  get fully qualified name
            String methodName = methods[i].getName();
            idx = methodName.lastIndexOf(".");
            String mName = methodName.substring(idx+1);
            if (mName.startsWith("get")) {
                String field = mName.substring(3);
                if (field.equals("Class")) continue;
                sbuf.append("      <"+field);
                String returnType = methods[i].getReturnType().getName();
                sbuf.append(" type=\"");
                if (returnType.equals("java.lang.String")) {
                    sbuf.append(returnType+"\" ");
                    try {
                        String value = (String)methods[i].invoke(obj, new Object[] {});
                        sbuf.append("value=\""+value+"\"");
                    } catch (IllegalAccessException e) {
                        logger.severe("Internal Error: "+e);
                    } catch (InvocationTargetException e) {
                        logger.severe("Internal Error: "+e);
                    }
                    sbuf.append("/>\n");
                } else

                if (returnType.equals("java.util.ArrayList")) {
                    ArrayList items = null;
                    try {
                        items = (ArrayList)methods[i].invoke(obj, new Object[] {});
                    } catch (IllegalAccessException e) {
                        logger.severe("Internal Error: "+e);
                    } catch (InvocationTargetException e) {
                        logger.severe("Internal Error: "+e);
                    }

                    String itemType = "";
                    if (items != null && !items.isEmpty()) {
                        itemType = items.get(0).getClass().getName();
                    }

                    sbuf.append(returnType+"\" ");
                    sbuf.append("itemType=\""+itemType+"\">\n");

                    for (int j=0; items != null && j<items.size(); j++) {
                        sbuf.append("        <item value=\""+items.get(j)+"\" />\n");
                    }

                    sbuf.append("      </"+field+">\n");
                }
            }
        }

        sbuf.append("    </"+kName+">\n");

        return sbuf.toString();
    }

    /**
     * Reconstiute an object. Create an object of the type specified in
     * the root node. For each subnode, call the appropriate setter with
     * the value(s) specified in the subnode. If the name of the subnode
     * is FooBar, then use the setter setFooBar. If the setter does not
     * exist, ignore the subnode. Thus, if the new object is different from
     * the old object represented by the XML, all its fields will have
     * default values. Those fields that don't exist anymore are ignored in
     * the XML. New fields have default values. Matching fields are
     * set to the value in the XML.
     * <p>
     * Assumptions:
     * <ul>
     * <li>The class being created has a constructor with no arguments.</li>
     * <li>All fields are strings or ArrayList whose items are strings.</li>
     * <li>For each field fooBar, there is a setter of the form setFooBar.</li>
     * </ul>
     *
     * @return New object of the type specified in the root node whose attributes
     * that exist in the XML are set to the specified value. It is up to the
     * called to cast the object to the desired/expected type. Return null if
     * the object instance cannot be created.
     */
    static public Object XmlToObject(Node node) {
        HashMap<String, Method> setters = new HashMap<String, Method>();
        String objType = ((Element)node).getAttribute("type");

        //create an instance of the object
        Object obj = null;
        try {
            Class klassDef = Class.forName(objType);
            obj = klassDef.newInstance();
        } catch (InstantiationException e) {
            logger.severe("Internal Error: "+e);
        } catch (IllegalAccessException e) {
            logger.severe("Internal Error: "+e);
        } catch (ClassNotFoundException e) {
            logger.severe("Internal Error: "+e);
        }

        if (obj == null) return obj;

        //get a list of all of its setters
        Class klass = obj.getClass();
        Method[] methods = klass.getMethods();
        for (int i=0; i<methods.length; i++) {
            //Filter out setters
            //  get fully qualified name
            String methodName = methods[i].getName();
            int idx = methodName.lastIndexOf(".");
            String mName = methodName.substring(idx+1);
            if (mName.startsWith("set")) {
                setters.put(mName, methods[i]);
            }
        }

        //get value of each field and set in object
        NodeList children = node.getChildNodes();
        String nodeName;
        Node child;
        for (int i=0; i<children.getLength(); i++) {
            child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                nodeName = child.getNodeName();
                String setterName = "set"+nodeName;
                Method setter = setters.get(setterName);
                //if there is no setter for the field, ignore
                if (setter == null) continue;

                String argType = ((Element)child).getAttribute("type");
                if (argType.equals("java.lang.String")) {
                    String value = ((Element)child).getAttribute("value");
                    try {
                        setter.invoke(obj, new Object[] {value});
                    } catch (IllegalAccessException e) {
                        logger.severe("Internal Error: "+e);
                    } catch (InvocationTargetException e) {
                        logger.severe("Internal Error: "+e);
                    }
                } else

                if (argType.equals("java.util.ArrayList")) {
                    String itemType = ((Element)child).getAttribute("itemType");
                    if (!itemType.equals("java.lang.String")) continue;
                    ArrayList items = new ArrayList();
                    NodeList listChildren = child.getChildNodes();
                    Node item;
                    for (int j=0; j<listChildren.getLength(); j++) {
                        item = listChildren.item(j);
                        if (item.getNodeType() == Node.ELEMENT_NODE) {
                            String val = ((Element)item).getAttribute("value");
                            items.add(val);
                        }
                    }
                    try {
                        setter.invoke(obj, new Object[] {items});
                    } catch (IllegalAccessException e) {
                        logger.severe("Internal Error: "+e);
                    } catch (InvocationTargetException e) {
                        logger.severe("Internal Error: "+e);
                    }
                }
            }
        }

        return obj;
    }
    
    /**
     * Returns the first child node with a name matching childNodeName.
     * If no such child node exists, null is returned.
     * 
     * @param node the parent node
     * @param childNodeName exact String to match
     * 
     * @return child Node named childNodeName
     */
    public static Node getChild(Node node, String childNodeName) {
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node childNode = children.item(i);
            if (childNode.getNodeName().equals(childNodeName)) {
                return childNode;
            }
        }
        return null;
    }
    
    /**
     * Gets the first child of Node which does not have the name "#text".
     * 
     * @return The first non-text child of node or null if no such child exists
     */
    public static Node getNonTextChild(Node node) {
        NodeList children = node.getChildNodes();
        for (int i = 0;i < children.getLength(); i++) {
            Node child = children.item(i);
            if ("#text".equals(child.getNodeName()) == false) {
                return child;
            }
        }
        return null;
    }
    
        /**
     * Gets the Document as a formatted (indented) XML String.
     * Method taken from com.bhpb.util.Util.java.  The XMLDeclaration
     * is omitted, as this String is intended to become part of the body
     * of the overall workbench state.
     * 
     * @param doc complete w3c.dom.Document with arbitrary root
     * 
     * @return indented String reprsentation of doc
     * 
     * @throws IOException if an error occurs while serializing the Document
     */
    public static String toFormattedXMlString(Document doc) throws IOException {
        OutputFormat format = new OutputFormat(doc);
        format.setOmitXMLDeclaration(true);
        format.setIndenting(true);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        XMLSerializer serializer = new XMLSerializer(output, format);
        serializer.serialize(doc);
        return output.toString();
    }
}