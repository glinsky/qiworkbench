/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiworkbench.compAPI.model;

import java.io.Serializable;
/**
 * Title:        XmlEditor <br><br>
 * Description:  this class is used for plugin management<br><br>
 * @version 1.0
 */

public class NodeFile implements Serializable {
	    private static final long serialVersionUID = 1L;
	    
	    String displayName;
		String filePath;
		boolean buildIn;
		int    status = 0; //-1 invalid, 0 not loaded, 1 loaded;
		
		public NodeFile() {}
		
		public NodeFile(String fullPath) {
			int index        = fullPath.lastIndexOf("/");
			this.filePath    = fullPath;
			this.displayName = fullPath.substring(index+1);
			
		}
		
		public String getDisplayName() {
			return displayName;
		}
		public void setDisplayName(String displayName) {
			this.displayName = displayName;
		}
		public String getFilePath() {
			return filePath;
		}
		public void setFilePath(String filePath) {
			this.filePath = filePath;
		}
		public int getStatus() {
			return status;
		}
		public void setStatus(int status) {
			this.status = status;
		}
		public boolean isBuildIn() {
			return buildIn;
		}
		public void setBuildIn(boolean isBuildIn) {
			this.buildIn = isBuildIn;
		}
		
		public void setParams(String displayName, String filePath, boolean buildin) {
			this.displayName = displayName;
			this.filePath    = filePath;
			this.buildIn     = buildin;
		}
		
}