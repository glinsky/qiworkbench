/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiworkbench.compAPI.model;

import java.io.Serializable;
import java.io.File;
import java.util.List;
import java.util.ArrayList;
import com.bhpb.distribution.DistConst;
import com.bhpb.distribution.IDistribution;
/**
 * Title:        XmlEditor <br><br>
 * Description:  this class is used for plugin management<br><br>
 * @version 1.0
 */
public class PluginModel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	String lastXml;
	String activePackage;
	List<NodePlugin>  plugins         = new ArrayList<NodePlugin>();
	List<NodeSchema>  schemas         = new ArrayList<NodeSchema>();
	List<NodeFile>    xmlFiles        = new ArrayList<NodeFile>();
	List<NodeFile>    helpFiles       = new ArrayList<NodeFile>();
	List<NodePackage> customPackages  = new ArrayList<NodePackage>();
	List<NodePackage> buildinPackages = new ArrayList<NodePackage>();
	

	public String getActivePackage() {
		return activePackage;
	}

	public void setActivePackage(String activePackage) {
		this.activePackage = activePackage;
	}

	public List<NodePackage> getBuildinPackages() {
		return buildinPackages;
	}

	public void setBuildinPackages(List<NodePackage> buildinPackages) {
		this.buildinPackages = buildinPackages;
	}

	public List<NodePackage> getCustomPackages() {
		return customPackages;
	}

	public void addPackage(NodePackage nodePackage) {
		customPackages.add(nodePackage);
	}
	
	public void deletePackage(String displayName) {
		int size = customPackages.size();
		for (int i = 0; i < size; i++) {
			 NodePackage pack = customPackages.get(i);
			 if (pack.getDisplayName().equalsIgnoreCase(displayName)) {
				 customPackages.remove(i);
				break;
			}
		}
	}	

	public void updatePackage(String packName, String schemaName, String xml, String help, boolean edit) {
		NodePackage nodePack = findPackage(packName);
		nodePack.setSchema(schemaName);
		nodePack.setXml(xml);
		nodePack.setHelp(help);
		nodePack.setEditable(edit);
	}
	
	public void addBuildinPackage(NodePackage nodePackage) {
		buildinPackages.add(nodePackage);
	}
	
	public void setCustomPackages(List<NodePackage> customPackages) {
		this.customPackages = customPackages;
	}

	public List<NodeFile> getHelpFiles() {
		return helpFiles;
	}
	
	public List<String> getStrFiles(String type) {
		List<String> list = null;
		if ("schema".equals(type)) {
			list = getStrSchemas();
		} else if ("xml".equals(type)) {
			list = getStrXmlFiles();
		} else if ("help".equals(type)) {
			list = getStrHelpFiles();
		}
		return list;
	}
	
	public List<String> getStrHelpFiles() {
		int cnt = helpFiles.size();
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < cnt; i++) {
			NodeFile node = helpFiles.get(i);
			if (!node.isBuildIn())
			   list.add(node.getFilePath());
		}
		return list;
	}

	public void addHelp(NodeFile nodeFile) {
		helpFiles.add(nodeFile);
	}
	
	public void deleteHelp(String displayName) {
		int size = helpFiles.size();
		for (int i = 0; i < size; i++) {
			 NodeFile node = helpFiles.get(i);
			 if (node.getDisplayName().equalsIgnoreCase(displayName)) {
				helpFiles.remove(i);
				break;
			}
		}
	}
	
	public void setHelpFiles(List<NodeFile> helpFiles) {
		this.helpFiles = helpFiles;
	}

	public List<NodePlugin> getPlugins() {
		return plugins;
	}

	public void addPlugin(NodePlugin nodePlugin) {
		plugins.add(nodePlugin);
	}
	
	public void setPlugins(List<NodePlugin> plugins) {
		this.plugins = plugins;
	}

	public List<NodeSchema> getSchemas() {
		return schemas;
	}
	
	public List<String> getStrSchemas() {
		int cnt = schemas.size();
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < cnt; i++) {
			NodeSchema node = schemas.get(i);
			if (!node.isBuildIn())
			  list.add(node.getFilePath());
		}
		return list;
	}

	public void addSchema(NodeSchema nodeSchema) {
		schemas.add(nodeSchema);
	}
	
	public void deleteSchema(String displayName) {
		int size = schemas.size();
		for (int i = 0; i < size; i++) {
			 NodeSchema node = schemas.get(i);
			 if (node.getDisplayName().equalsIgnoreCase(displayName)) {
				schemas.remove(i);
				break;
			}
		}
	}
	
	public void setSchemas(List<NodeSchema> schemas) {
		this.schemas = schemas;
	}

	public List<NodeFile> getXmlFiles() {
		return xmlFiles;
	}
	
	public List<String> getStrXmlFiles() {
		int cnt = xmlFiles.size();
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < cnt; i++) {
			NodeFile node = xmlFiles.get(i);
			if (!node.isBuildIn())
			   list.add(node.getFilePath());
		}
		return list;
	}

	public void addXml(NodeFile nodeFile) {
		xmlFiles.add(nodeFile);
	}
	
	public void deleteXml(String displayName) {
		int size = xmlFiles.size();
		for (int i = 0; i < size; i++) {
			 NodeFile node = xmlFiles.get(i);
			 if (node.getDisplayName().equalsIgnoreCase(displayName)) {
				xmlFiles.remove(i);
				break;
			}
		}
	}
	
	public void setXmlFiles(List<NodeFile> xmlFiles) {
		this.xmlFiles = xmlFiles;
	}
	
	public PackageLoad getPackageLoad(IDistribution dist, String displayName) {
		String dir = null;
		PackageLoad load = new PackageLoad();
		load.setLastXml(this.getLastXml());
		NodePackage nodePack   = findPackage(displayName);
		load.setPackageName(nodePack.getDisplayName());
		load.setSchemaName(nodePack.getSchema());
		String path = "NONE";
		String temp = nodePack.getXml();
		NodeFile nodeFile = null;
		if (!("".equals(temp) || "NONE".equals(temp))) {
		   nodeFile = findXmlNode(temp);
		   dir = dist.getDirByName(DistConst.XML);
		   path = dir + File.separator + nodeFile.getFilePath();
	    }
		load.setXmlName(temp);
		load.setXmlFile(path);
		temp = nodePack.getHelp();
		path = "DEFAULT";
		if (!("DEFAULT".equals(temp) || "".equals(temp) || "NONE".equals(temp))) {
		   nodeFile = findHelpNode(temp);
		   dir = dist.getDirByName(DistConst.HELP);
		   path = dir + File.separator + nodeFile.getFilePath();
		}
		load.setHelpName(temp);
		load.setHelpFile(path);
		load.setEditable(nodePack.isEditable());
		String schemaName = nodePack.getSchema();
		String plugin = "Delivery";
		if ("NONE".equalsIgnoreCase(schemaName)) {
			load.setSchemaFile(schemaName);
		} else {
		    NodeSchema  nodeSchema = findSchema(schemaName);
		    dir = dist.getDirByName(DistConst.SCHEMA);
		    if (!nodeSchema.isBuildIn())
		       load.setSchemaFile(dir + File.separator +nodeSchema.getFilePath());
		    else 
		    	load.setSchemaFile(nodeSchema.getFilePath());
		    plugin = nodeSchema.getPlugin();
		}
		load.setPluginName(plugin);
		NodePlugin  nodePlugin = findPlugin(plugin);
		load.setConfigFile(nodePlugin.getConfigFile());
		load.setPluginClass(nodePlugin.getPluginClass());
        return load;
	}
	
	public NodePackage findPackage(String name) {
		NodePackage ret = null, pack = null;
		int size = buildinPackages.size();
		for (int i = 0; i < size; i++) {
			pack = buildinPackages.get(i);
		    if (pack.getDisplayName().equalsIgnoreCase(name)) {
				ret = pack;
				break;
			}
		}
		
		if (ret == null) {
			size = customPackages.size();
			for (int i = 0; i < size; i++) {
				 pack = customPackages.get(i);
				 if (pack.getDisplayName().equalsIgnoreCase(name)) {
					ret = pack;
					break;
				}
			}
		}
		if (ret == null) {
			ret = findPackage(activePackage);
		}
		return ret;
	}
	
	public NodeSchema findSchema(String name) {
		NodeSchema ret = null;
		int size = schemas.size();
		for (int i = 0; i < size; i++) {
			NodeSchema schema = schemas.get(i);
			if (name.equalsIgnoreCase(schema.getDisplayName())) {
				ret = schema;
				break;
			}
		}
		return ret;
	}
	
	public NodeFile findXmlNode(String name) {
		NodeFile ret = null;
		int size = xmlFiles.size();
		for (int i = 0; i < size; i++) {
			NodeFile file = xmlFiles.get(i);
			if (name.equalsIgnoreCase(file.getDisplayName())) {
				ret = file;
				break;
			}
		}
		return ret;
	}
	
	public NodeFile findHelpNode(String name) {
		NodeFile ret = null;
		int size = helpFiles.size();
		for (int i = 0; i < size; i++) {
			NodeFile file = helpFiles.get(i);
			if (name.equalsIgnoreCase(file.getDisplayName())) {
				ret = file;
				break;
			}
		}
		return ret;
	}
	
	public NodePlugin findPlugin(String name) {
		NodePlugin ret = null;
		int size = plugins.size();
		for (int i = 0; i < size; i++) {
			NodePlugin plugin = plugins.get(i);
			if (name.equalsIgnoreCase(plugin.getDisplayName())) {
				ret = plugin;
				break;
			}
		}
		return ret;
	}
	
	public String getPluginName(String schemaName) {
		String ret = null;
		int size = schemas.size();
		for (int i = 0; i < size; i++) {
			NodeSchema schema = schemas.get(i);
			if (schemaName.equalsIgnoreCase(schema.getDisplayName())) {
				ret = schema.getPlugin();
				break;
			}
		}
		return ret;
	}
	
	/*type = ADD_XSD, ADD_XML, ADD_HELP, ADD_PACK
	 *       DEL_XSD, DEL_XML, DEL_HELP, DEL_PACK, UPD_PACK
	 */
	public int err(String type, String displayName) {
		int err = 0;
		if ("ADD_SCHEMA".equals(type)) {
			err = errAddSchema(displayName);
		} else if ("ADD_XML".equals(type)) {
			err = errAddXml(displayName);
		} else if ("ADD_HELP".equals(type)) {
			err = errAddHelp(displayName);
		} else if ("DEL_XSD".equals(type)) {
			err = errDelSchema(displayName);
		} else if ("DEL_XML".equals(type)) {
			err = errDelXml(displayName);
		} else if ("DEL_HELP".equals(type)) {
			err = errDelHelp(displayName);
		} else if ("ADD_PACK".equals(type)) {
			err = errAddPack(displayName);
		}
		return err;
	}
	
	private int errAddPack(String displayName) {
		int err = 0;
		NodePackage pack;
		int size = buildinPackages.size();
		for (int i = 0; i < size; i++) {
			pack = buildinPackages.get(i);
		    if (pack.getDisplayName().equalsIgnoreCase(displayName)) {
				err = 1;
				break;
			}
		}
		
		if (err == 0) {
			size = customPackages.size();
			for (int i = 0; i < size; i++) {
				 pack = customPackages.get(i);
				 if (pack.getDisplayName().equalsIgnoreCase(displayName)) {
					err = 2;
					break;
				}
			}
		}
		
		return err;
	}
	
	private int errAddSchema(String displayName) {
		int err = 0;
		int size = schemas.size();
		for (int i = 0; i < size; i++) {
			 NodeSchema node = schemas.get(i);
			 if (node.getDisplayName().equalsIgnoreCase(displayName)) {
				err = 1;
				break;
			}
		}
		
		return err;
	}
	
	private int errDelSchema(String displayName) {
		int err = 0;
		
		int size = schemas.size();
		for (int i = 0; i < size; i++) {
			 NodeSchema node = schemas.get(i);
			 if (node.getDisplayName().equalsIgnoreCase(displayName)) {
				if (node.isBuildIn()) {
					err = 3;
					break;
				}
					
				
			}
		}
		
		NodePackage pack;
		if (err == 0) {
		  size = buildinPackages.size();
		  for (int i = 0; i < size; i++) {
			pack = buildinPackages.get(i);
		    if (pack.getSchema().equalsIgnoreCase(displayName)) {
				err = 1;
				break;
			}
		  }
	    }
		
		if (err == 0) {
			size = customPackages.size();
			for (int i = 0; i < size; i++) {
				 pack = customPackages.get(i);
				 if (pack.getSchema().equalsIgnoreCase(displayName)) {
					err = 2;
					break;
				}
			}
		}
		
		return err;
	}
	
	private int errAddXml(String displayName) {
		int err = 0;
		int size = xmlFiles.size();
		for (int i = 0; i < size; i++) {
			 NodeFile node = xmlFiles.get(i);
			 if (node.getDisplayName().equalsIgnoreCase(displayName)) {
				err = 1;
				break;
			}
		}
		
		return err;
	}
	
	private int errDelXml(String displayName) {
		int err = 0;
		NodePackage pack;
		int size = buildinPackages.size();
		for (int i = 0; i < size; i++) {
			pack = buildinPackages.get(i);
		    if (pack.getXml().equalsIgnoreCase(displayName)) {
				err = 1;
				break;
			}
		}
		
		if (err == 0) {
			size = customPackages.size();
			for (int i = 0; i < size; i++) {
				 pack = customPackages.get(i);
				 if (pack.getXml().equalsIgnoreCase(displayName)) {
					err = 2;
					break;
				}
			}
		}
		
		return err;
	}
	
	private int errAddHelp(String displayName) {
		int err = 0;
		int size = helpFiles.size();
		for (int i = 0; i < size; i++) {
			 NodeFile node = helpFiles.get(i);
			 if (node.getDisplayName().equalsIgnoreCase(displayName)) {
				err = 1;
				break;
			}
		}
		
		return err;
	}
	
	private int errDelHelp(String displayName) {
		int err = 0;
		NodePackage pack;
		int size = buildinPackages.size();
		for (int i = 0; i < size; i++) {
			pack = buildinPackages.get(i);
		    if (pack.getHelp().equalsIgnoreCase(displayName)) {
				err = 1;
				break;
			}
		}
		
		if (err == 0) {
			size = customPackages.size();
			for (int i = 0; i < size; i++) {
				 pack = customPackages.get(i);
				 if (pack.getHelp().equalsIgnoreCase(displayName)) {
					err = 2;
					break;
				}
			}
		}
		
		return err;
	}
	
	public PluginModel defaultModel() {
		
		activePackage = "script";
		lastXml = "";
		
		NodePackage  pack = new NodePackage();
		pack.setParams("xmlEditor", "NONE", "", "DEFAULT", true);
		buildinPackages.add(pack);
		
		pack = new NodePackage();
		pack.setParams("script", "script", "", "DEFAULT", true);
		buildinPackages.add(pack);
		
		pack = new NodePackage();
		pack.setParams("delivery", "delivery", "", "DEFAULT", true);
		buildinPackages.add(pack);
		
		pack = new NodePackage();
		pack.setParams("delivMassager", "delivMassager", "", "DEFAULT", true);
		buildinPackages.add(pack);
		
		pack = new NodePackage();
		pack.setParams("waveletExtraction", "waveletExtraction", "", "DEFAULT", true);
		buildinPackages.add(pack);
		
		NodeSchema schema = new NodeSchema();
    	schema.setParams("xmlEditor", "Delivery", "NONE", true);
    	schemas.add(schema);
    	
    	schema = new NodeSchema();
    	schema.setParams("script", "Script", "xsd/script.xsd", true);
    	schemas.add(schema);
    	
    	schema = new NodeSchema();
    	schema.setParams("delivery", "Delivery",  "xsd/delivery.xsd", true);
    	schemas.add(schema);
    	
    	schema = new NodeSchema();
    	schema.setParams("delivMassager", "Script", "xsd/deliveryMassager.xsd", true);
    	schemas.add(schema);
    	
    	schema = new NodeSchema();
    	schema.setParams("waveletExtraction", "Delivery", "xsd/waveletExtraction.xsd", true);
    	schemas.add(schema);
    	
    	NodePlugin pInfo = new NodePlugin();
    	pInfo.setParams("Delivery", "config/deliveryConfig.xml", "com.bhpb.xmleditor.plugins.delivery.DeliveryPlugin");
    	plugins.add(pInfo);
    	pInfo = new NodePlugin();
    	pInfo.setParams("Script", "config/scriptConfig.xml", "com.bhpb.xmleditor.plugins.script.ScriptPlugin");
    	plugins.add(pInfo);
    	
    	NodeFile fInfo = new NodeFile();
    	fInfo.setParams("NONE", "NONE", true);
    	xmlFiles.add(fInfo);
    	fInfo = new NodeFile();
    	fInfo.setParams("DEFAULT", "DEFAULT", true);
    	helpFiles.add(fInfo);
    	
	    return this;
	}

	public String getLastXml() {
		return lastXml;
	}

	public void setLastXml(String lastXml) {
		this.lastXml = lastXml;
	}
}