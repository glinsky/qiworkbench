package com.bhpb.qiworkbench.compAPI.model;

import java.io.FileWriter;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.util.logging.Logger;

public class TestPackageWrite {
    private static final Logger logger =
            Logger.getLogger(TestPackageWrite.class.getName());
        public static void main(String[] args) {
            XStream xStream = new XStream(new DomDriver());
            xStream.alias("pluginManager", PluginModel.class);
            xStream.registerConverter(new PluginModelConverter());
            FileWriter writer = null;
    		String xmlFile1 = "/home/jianc9/qiworkbench/pWrite.xml";
    		try {
    			   writer = new FileWriter(xmlFile1);
    			   PluginModel fModel = new PluginModel();
    			   fModel.setActivePackage("script");
    			   xStream.toXML(fModel.defaultModel(), writer);
    			   writer.flush();
    			   writer.close();
    			   logger.info("--write to file successfull: " + xmlFile1);
    			} catch (Exception e) {
    				e.printStackTrace();
    				logger.warning("failed: " + e.getMessage());
    			} finally {
    				if (writer != null) {
    					try {
    						writer.close();
    					}catch (Exception e) {}
    				}
    			}
        }
}

