/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiworkbench.compAPI.model;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
/**
 * Title:        XmlEditor <br><br>
 * Description:  this class is used for plugin management<br><br>
 * @version 1.0
 */
public class NodePackage implements Serializable {
		private static final long serialVersionUID = 1L;
		
		private String displayName;
		private String schema;
		private String xml;
		private String help;
		private boolean editable;
		
		public String getDisplayName() {
			return displayName;
		}
		public void setDisplayName(String displayName) {
			this.displayName = displayName;
		}
		public boolean isEditable() {
			return editable;
		}
		public void setEditable(boolean editable) {
			this.editable = editable;
		}
		public String getHelp() {
			return help;
		}
		public void setHelp(String help) {
			this.help = help;
		}
		public String getSchema() {
			return schema;
		}
		public void setSchema(String schema) {
			this.schema = schema;
		}
		public String getXml() {
			return xml;
		}
		public void setXml(String xml) {
			this.xml = xml;
		}
		
		public void setParams(String displayName, String schema, String xml, String help, boolean editable) {
			this.displayName = displayName;
			this.schema      = schema;
			this.xml         = xml;
			this.help        = help;
			this.editable    = editable;
		}		
}