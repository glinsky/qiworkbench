package com.bhpb.qiworkbench.compAPI.model;

import java.io.FileReader;
import java.io.FileWriter;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.util.logging.Logger;

public class TestPackageRead {
        private static final Logger logger =
                Logger.getLogger(TestPackageRead.class.getName());
        public static void main(String[] args) {
            XStream xStream = new XStream(new DomDriver());
            xStream.alias("pluginManager", PluginModel.class);
            xStream.registerConverter(new PluginModelConverter());
            FileWriter writer = null;
            String xmlFile1 = "/home/jianc9/qiworkbench/pWrite.xml";
    		String xmlFile2 = "/home/jianc9/qiworkbench/pWrite1.xml";
    		try {
    			   FileReader reader = new FileReader(xmlFile1);
    			   PluginModel model = (PluginModel)xStream.fromXML(reader);
    			   logger.info("--read from file successfull --- " + xmlFile1);
    			   writer = new FileWriter(xmlFile2);
    			   logger.info(xStream.toXML(model));
    			   xStream.toXML(model, writer);
    			   writer.flush();
    			   logger.info("--write to file successfull: " + xmlFile2);
    			} catch (Exception e) {
    				e.printStackTrace();
    				logger.warning("failed: " + e.getMessage());
    			} finally {
    				if (writer != null) {
    					try {
    						writer.close();
    					}catch (Exception e) {}
    				}
    			}
        }
}

