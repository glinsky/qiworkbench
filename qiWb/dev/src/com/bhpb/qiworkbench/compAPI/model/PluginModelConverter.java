/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiworkbench.compAPI.model;

import java.util.List;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class PluginModelConverter implements Converter {
	 
	 public boolean canConvert(Class clazz) {
         return clazz.equals(PluginModel.class);
     }

     public void marshal(Object value, HierarchicalStreamWriter writer,
                                       MarshallingContext context) {
    	 
    	 PluginModel pModel = (PluginModel)value; 
    	 writePlugins(pModel, writer, context); 
    	 writeSchemas(pModel, writer, context); 
    	 writeFiles("xmlFiles",   pModel.getXmlFiles(), writer, context); 
    	 writeFiles("helpFiles", pModel.getHelpFiles(), writer, context); 
    	 writePackages("customPackages",  pModel.getCustomPackages(), writer, context);  
    	 writePackages("buildinPackages", pModel.getBuildinPackages(), writer, context); 
    	 writeDefaultPackage(pModel, writer, context); 	 
     }
     
     private void writePackages(String nodeName, List<NodePackage> list, HierarchicalStreamWriter writer,
                                                    MarshallingContext context) {
  
    	 NodePackage temp = null;
    	 int cnt = list.size();
		 writer.startNode(nodeName);
    	 for (int i = 0; i < cnt; i++) {  		 
    		 temp = list.get(i); 
    		 writer.startNode("package");
    		 String name = temp.getDisplayName();
    		 if (name == null || name.length() == 0) name = "NONE";
    		 writer.addAttribute("displayName", name);
    		 
    		 writer.startNode("schema");
    		 writer.setValue(temp.getSchema());
    		 writer.endNode();
    		 
    		 writer.startNode("xml");
    		 writer.setValue(temp.getXml());
    		 writer.endNode();
    		 
    		 writer.startNode("help");
    		 writer.setValue(temp.getHelp());
    		 writer.endNode();
    		 
    		 writer.startNode("editable");
    		 writer.setValue(temp.isEditable() ? "true" : "false");
    		 writer.endNode();
    		 
    		 writer.endNode();
    	 }
    	 writer.endNode();
    	 
     }
     
     private void writeSchemas(PluginModel pModel, HierarchicalStreamWriter writer,
             MarshallingContext context) {
    	 
    	 NodeSchema temp = null;
    	 List<NodeSchema> list = pModel.getSchemas();
    	 int cnt = list.size();
		 writer.startNode("schemas");
    	 for (int i = 0; i < cnt; i++) {  		 
    		 temp = list.get(i); 
    		 writer.startNode("schema");
    		 String name = temp.getDisplayName();
    		 if (name == null || name.length() == 0) name = "NONE";
    		 writer.addAttribute("displayName", name);
    		 
    		 writer.startNode("buildin");
    		 String buildIn = temp.isBuildIn() ? "true" : "false";
    		 writer.setValue(buildIn);
    		 writer.endNode();
    		 
    		 writer.startNode("plugin");
    		 writer.setValue(temp.getPlugin());
    		 writer.endNode();
    		 
    		 writer.startNode("filePath");
    		 writer.setValue(temp.getFilePath());
    		 writer.endNode();
    		 writer.endNode();
    	 }
    	 writer.endNode();
    	  
     }
     
     private void writeFiles(String nodeName, List<NodeFile> list, HierarchicalStreamWriter writer,
             MarshallingContext context) {
    	 
    	 NodeFile temp = null;
    	 int cnt = list.size();
		 writer.startNode(nodeName);
    	 for (int i = 0; i < cnt; i++) {  		 
    		 temp = list.get(i); 
    		 writer.startNode("file");
    		 String name = temp.getDisplayName();
    		 if (name == null || name.length() == 0) name = "NONE";
    		 writer.addAttribute("displayName", name);
    		 
    		 writer.startNode("buildin");
    		 String buildIn = temp.isBuildIn() ? "true" : "false";
    		 writer.setValue(buildIn);
    		 writer.endNode();
    		 
    		 writer.startNode("filePath");
    		 writer.setValue(temp.getFilePath());
    		 writer.endNode();
    		 writer.endNode();
    	 }
    	 writer.endNode();
    	 
     }

     private void writePlugins(PluginModel pModel, HierarchicalStreamWriter writer,
                                                      MarshallingContext context) {
    	 NodePlugin pInfo = null; 
    	 List<NodePlugin> list = pModel.getPlugins();
    	 int cnt = list.size();
		 writer.startNode("plugins");
		 for (int i = 0; i < cnt; i++) {
    		 pInfo = list.get(i); 
    		 writer.startNode("plugin");
    		 writer.addAttribute("displayName", pInfo.getDisplayName());
    		 
    		 writer.startNode("config");
    		 writer.setValue(pInfo.getConfigFile());
    		 writer.endNode();
    		 
    		 writer.startNode("pluginClass");
    		 writer.setValue(pInfo.getPluginClass());
    		 writer.endNode();
    		 writer.endNode();
		 }
		 writer.endNode();	 
		 
     }

     private void writeDefaultPackage(PluginModel pModel, HierarchicalStreamWriter writer,
             MarshallingContext context) {
    	 String schema = pModel.getActivePackage();
		 writer.startNode("defaultPackage");
		 writer.setValue(schema);
		 writer.endNode();
		 
		 writer.startNode("lastXml");
		 writer.setValue(pModel.getLastXml());
		 writer.endNode();
     }
     
     
     public Object unmarshal(HierarchicalStreamReader reader,
                                    UnmarshallingContext context) {
    	 
    	 PluginModel pModel = new PluginModel(); 
    	 while (reader.hasMoreChildren()) {
    		 reader.moveDown();
    		 String nodeName = reader.getNodeName();
    		 if ("plugins".equals(nodeName)) {
    			 readPlugins(pModel, reader, context); 
    		 } else if ("schemas".equals(nodeName)) {
    			 readSchemas(pModel, reader, context); 
    		 } else if ("xmlFiles".equals(nodeName)) {
    			 readXmlFiles(pModel, reader, context); 
    		 } else if ("helpFiles".equals(nodeName)) {
    			 readHelpFiles(pModel, reader, context); 
    		 } else if ("buildinPackages".equals(nodeName)) {
    			 readBuildinPackages(pModel, reader, context); 
    		 } else if ("customPackages".equals(nodeName)) {
    			 readCustomPackages(pModel, reader, context);
    		 } else if ("defaultPackage".equals(nodeName)) {
    			 pModel.setActivePackage(reader.getValue()); 
    			 reader.moveUp();
    		 } else if ("lastXml".equals(nodeName)) { 
    			 pModel.setLastXml(reader.getValue()); 
    			 reader.moveUp();
    		 }
    	 }
         return pModel;
     }
	
     private void readPlugins(PluginModel pModel, HierarchicalStreamReader reader,
                                                   UnmarshallingContext context) {
    	 NodePlugin pInfo = null;
    	 while (reader.hasMoreChildren()) {
    			 reader.moveDown();
    			 pInfo = new NodePlugin(); 
    			 pInfo.setDisplayName(reader.getAttribute("displayName"));
    			 
    			 reader.moveDown();
        		 pInfo.setConfigFile(reader.getValue());
        		 reader.moveUp();
        		 
        		 reader.moveDown();
        		 pInfo.setPluginClass(reader.getValue());
        		 reader.moveUp();
        		 
        		 reader.moveUp();
        		 pModel.addPlugin(pInfo);
         }
    	 reader.moveUp();
     }
     
     private void readSchemas(PluginModel pModel, HierarchicalStreamReader reader,
                                                 UnmarshallingContext context) {
    	 NodeSchema sInfo = null;
    	 while (reader.hasMoreChildren()) {
    			 reader.moveDown();
    			 sInfo = new NodeSchema(); 
    			 sInfo.setDisplayName(reader.getAttribute("displayName"));
    			 
    			 reader.moveDown();
    			 String buildin = reader.getValue();
        		 sInfo.setBuildIn("true".equals(buildin) ? true : false);
        		 reader.moveUp();
        		 
        		 reader.moveDown();
        		 sInfo.setPlugin(reader.getValue());
        		 reader.moveUp();
        		 
        		 reader.moveDown();
        		 sInfo.setFilePath(reader.getValue());
        		 reader.moveUp();
        		 
        		 reader.moveUp();
        		 pModel.addSchema(sInfo);
         }
    	 reader.moveUp();
     }
     
     private void readXmlFiles(PluginModel pModel, HierarchicalStreamReader reader,
                                                        UnmarshallingContext context) {
    	 
    	 NodeFile fInfo = null;
    	 while (reader.hasMoreChildren()) {
    			 reader.moveDown();
    			 fInfo = new NodeFile(); 
    			 fInfo.setDisplayName(reader.getAttribute("displayName"));
    			 
    			 reader.moveDown();
    			 String buildin = reader.getValue();
        		 fInfo.setBuildIn("true".equals(buildin) ? true : false);
        		 reader.moveUp();
        		 
        		 reader.moveDown();
        		 fInfo.setFilePath(reader.getValue());
        		 reader.moveUp();
        		 
        		 reader.moveUp();
        		 pModel.addXml(fInfo);
         }
    	 reader.moveUp();
     }
     
     private void readHelpFiles(PluginModel pModel, HierarchicalStreamReader reader,
                                               UnmarshallingContext context) {
    
    	 NodeFile fInfo = null;
    	 while (reader.hasMoreChildren()) {
    			 reader.moveDown();
    			 fInfo = new NodeFile(); 
    			 fInfo.setDisplayName(reader.getAttribute("displayName"));
    			 
    			 reader.moveDown();
    			 String buildin = reader.getValue();
        		 fInfo.setBuildIn("true".equals(buildin) ? true : false);
        		 reader.moveUp();
        		 
        		 reader.moveDown();
        		 fInfo.setFilePath(reader.getValue());
        		 reader.moveUp();
        		 
        		 reader.moveUp();
        		 pModel.addHelp(fInfo);
         }
    	 reader.moveUp();
     }
     
     private void readBuildinPackages(PluginModel pModel, HierarchicalStreamReader reader,
                                                          UnmarshallingContext context) {
    	 
    	 NodePackage fInfo = null;
    	 while (reader.hasMoreChildren()) {
    			 reader.moveDown();
    			 fInfo = new NodePackage(); 
    			 fInfo.setDisplayName(reader.getAttribute("displayName"));
    			 
        		 reader.moveDown();
        		 fInfo.setSchema(reader.getValue());
        		 reader.moveUp();
        		 
        		 reader.moveDown();
        		 fInfo.setXml(reader.getValue());
        		 reader.moveUp();
        		 
        		 reader.moveDown();
        		 fInfo.setHelp(reader.getValue());
        		 reader.moveUp();
        		 
        		 
        		 reader.moveDown();
        		 String editable = reader.getValue();
        		 fInfo.setEditable(editable.equals("true") ? true : false);
        		 reader.moveUp();
        		 
        		 reader.moveUp();
        		 pModel.addBuildinPackage(fInfo);
         }
    	 reader.moveUp();
     }
     
     private void readCustomPackages(PluginModel pModel, HierarchicalStreamReader reader,
                                                       UnmarshallingContext context) {
    	 NodePackage fInfo = null;
    	 while (reader.hasMoreChildren()) {
    			 reader.moveDown();
    			 fInfo = new NodePackage(); 
    			 fInfo.setDisplayName(reader.getAttribute("displayName"));
    			 
        		 reader.moveDown();
        		 fInfo.setSchema(reader.getValue());
        		 reader.moveUp();
        		 
        		 reader.moveDown();
        		 fInfo.setXml(reader.getValue());
        		 reader.moveUp();
        		 
        		 reader.moveDown();
        		 fInfo.setHelp(reader.getValue());
        		 reader.moveUp();
        		 
        		 reader.moveDown();
        		 String editable = reader.getValue();
        		 fInfo.setEditable(editable.equals("true") ? true : false);
        		 reader.moveUp();
        		 
        		 reader.moveUp();
        		 pModel.addPackage(fInfo);
         }
    	 reader.moveUp();
     }
}
