/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiworkbench.compAPI.model;

import java.io.Serializable;
import java.util.HashMap;
/**
 * Title:        XmlEditor <br><br>
 * Description:  this class is used for plugin management<br><br>
 * @version 1.0
 */
public class PackageLoad implements Serializable {
		private static final long serialVersionUID = 1L;
		
		private String pkgSet;
		private String packageName;
		private String schemaName;
		private String schemaFile;
		
		private String pluginName;
		private String pluginClass;
		
		private String configFile;
		private String xmlName;
		private String xmlFile;
		private String lastXml;
		private String helpName;
		private String helpFile;
		HashMap<String, String> map = new HashMap<String, String>();
		
		private boolean editable = true;
		
		public boolean isEditable() {
			return editable;
		}

		public void setEditable(boolean editable) {
			this.editable = editable;
		}

		public PackageLoad() {
		}
		
		public String getConfigFile() {
			return configFile;
		}
		public void setConfigFile(String configFile) {
			this.configFile = configFile;
		}
		public String getHelpFile() {
			return helpFile;
		}
		public void setHelpFile(String helpFile) {
			this.helpFile = helpFile;
		}
		public String getPluginClass() {
			return pluginClass;
		}
		public void setPluginClass(String pluginClass) {
			this.pluginClass = pluginClass;
		}
		public String getPluginName() {
			return pluginName;
		}
		public void setPluginName(String pluginName) {
			this.pluginName = pluginName;
		}
		public String getSchemaFile() {
			return schemaFile;
		}
		public void setSchemaFile(String schemaFile) {
			this.schemaFile = schemaFile;
		}
		public String getSchemaName() {
			return schemaName;
		}
		public void setSchemaName(String schemaName) {
			this.schemaName = schemaName;
		}
		public String getXmlFile() {
			return xmlFile;
		}
		public void setXmlFile(String xmlFile) {
			this.xmlFile = xmlFile;
		}	
		
		
		public String getPackageName() {
			return packageName;
		}
		public void setPackageName(String packageName) {
			this.packageName = packageName;
		}

		public String getHelpName() {
			return helpName;
		}

		public void setHelpName(String helpName) {
			this.helpName = helpName;
		}

		public String getXmlName() {
			return xmlName;
		}

		public void setXmlName(String xmlName) {
			this.xmlName = xmlName;
		}
		
		/** Dump the preferences */
	     public String toString() {
	         StringBuffer sbuf = new StringBuffer();
	         sbuf.append("PackageLoad :\n");
	         sbuf.append("PackageName: "+packageName+"\n");
	         sbuf.append("schemaName: "+schemaName+"\n");
	         sbuf.append("schemaFile: "+schemaFile+"\n");
	         sbuf.append("pluginName: "+pluginName+"\n");
	         sbuf.append("xmlName: "+xmlName+"\n");
	         sbuf.append("xmlFile: "+xmlFile+"\n");
	         sbuf.append("helpName: "+helpName+"\n");
	         sbuf.append("lastXml: "+lastXml+"\n");
	         sbuf.append("helpFile: "+helpFile+"\n");
	         return sbuf.toString();
	     }

		public String getPkgSet() {
			return pkgSet;
		}

		public void setPkgSet(String pkgSet) {
			this.pkgSet = pkgSet;
		}

		public String getLastXml() {
			return lastXml;
		}

		public void setLastXml(String lastXml) {
			this.lastXml = lastXml;
		}
		
		public void setKeyValue(String key, String value) {
			map.put(key, value);
		}
		
		public String getKeyValue(String key) {
			return map.get(key);
		}
}