/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.compAPI;

import java.lang.Class;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.FileChooserDescriptor;

/**
 * Utilities for accessing the attributes of a file chooser descriptor.
 *
 * @author Gil Hansen.
 * @version 1.0
 */
public class FileChooserDescUtils {
    private static Logger logger = Logger.getLogger(FileChooserDescUtils.class.getName());

    /**
     * Get ID of response containing user's selection.
     *
     * @param desc A file chooser's descriptor
     * @return Message ID of user's selection
     */
    static public String getMsgID(FileChooserDescriptor desc) {
        return desc.getMsgID();
    }

    /**
     * Get the data type of the requester.
     *
     * @param desc A file chooser's descriptor
     * @return String Data type of requester
     */
    static public String getRequesterType(FileChooserDescriptor desc) {
        return desc.getRequesterType();
    }

    /**
     * Get instance of requester. It is up to the qiComponent agent to cast the
     * instance object to its content type.
     *
     * @param desc  A file chooser's descriptor
     * @return Object Instance of the requester
     */
    static public Object getRequesterInstance(FileChooserDescriptor desc) {
        return desc.getRequesterInstance();
    }
}
