/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2005  Santhosh Kumar T, 
# Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
# For more information about the original LGPL work by Santhosh Kumar T, see
# http://henterji.blogbus.com/logs/2005/11/2033950.html
###########################################################################
 */
package com.bhpb.qiworkbench.compAPI;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ProgressDialog2 extends JDialog implements ChangeListener,
        ActionListener {

    JLabel statusLabel = new JLabel();
    JLabel textLabel = new JLabel();
    JLabel iconLabel = new JLabel();
    JProgressBar progressBar = new JProgressBar();
    ProgressMonitor2 monitor;
    final Timer timer;

    public ProgressDialog2(Frame owner, ProgressMonitor2 monitor, String title, String text, Icon icon) throws HeadlessException {
        super(owner, title, true);
        init(monitor, text, icon);
        timer = initTimer(monitor.getMilliSecondsToWait());
    }

    private Timer initTimer(int millisToWait) {
        if (millisToWait <= 0) {
            return null;
        } else {
            return new Timer(monitor.getMilliSecondsToWait(), this);
        }
    }

    public ProgressDialog2(Dialog owner, ProgressMonitor2 monitor, String title, String text, Icon icon) throws HeadlessException {
        super(owner, title, true);
        init(monitor, text, icon);
        timer = initTimer(monitor.getMilliSecondsToWait());
    }

    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() != timer)
        SwingUtilities.invokeLater(
                new Runnable() {

                    public void run() {
                        updateGUI();
                    }
                });
    }

    private void init(final ProgressMonitor2 monitor, String text, Icon icon) {
        this.monitor = monitor;
        progressBar = new JProgressBar(0, monitor.getTotal());
        if (monitor.isIndeterminate()) {
            progressBar.setIndeterminate(true);
        } else {
            progressBar.setIndeterminate(false);
            progressBar.setStringPainted(true);
            progressBar.setValue(monitor.getCurrent());
        }
        statusLabel.setText(monitor.getStatus());

        Action cancelAction = new AbstractAction("Cancel") {

            public void actionPerformed(ActionEvent e) {
                monitor.setCanceled(true);
            }
        };

        JButton cancelButton = new JButton(cancelAction);

        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent we) {
                monitor.setCanceled(true);
            }
        });

        textLabel.setText(text);

        if (icon != null) {
            iconLabel.setIcon(icon);
        }

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(layout.createSequentialGroup().add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(layout.createSequentialGroup().addContainerGap().add(iconLabel).add(14, 14, 14).add(textLabel)).add(layout.createSequentialGroup().add(197, 197, 197).add(cancelButton)).add(layout.createSequentialGroup().addContainerGap().add(progressBar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE)).add(layout.createSequentialGroup().addContainerGap().add(statusLabel))).addContainerGap()));
        layout.setVerticalGroup(
                layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(layout.createSequentialGroup().add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(iconLabel).add(textLabel)).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 16, Short.MAX_VALUE).add(statusLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(progressBar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(14, 14, 14).add(cancelButton).addContainerGap()));
        pack();
    }

    public void stateChanged(final ChangeEvent ce) {
        SwingUtilities.invokeLater(
                new Runnable() {

                    public void run() {
                        updateGUI();
                    }
                });
    }

    public void startTimer() {
        if (timer != null) {
            timer.start();
        }
    }

    private void updateGUI() {
        if (!monitor.isIndeterminate()) {
            if (monitor.getText() != null) {
                textLabel.setText(monitor.getText());
            }
            progressBar.setIndeterminate(false);
            progressBar.setStringPainted(true);
        } else {
            progressBar.setIndeterminate(true);
        }
        if (monitor.getCurrent() != monitor.getTotal()) {
            statusLabel.setText(monitor.getStatus());
            progressBar.setValue(monitor.getCurrent());
        } else {
            dispose();
        }
    }
}