/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2005  Santhosh Kumar T, 
# Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
# For more information about the original LGPL work by Santhosh Kumar T, see
# http://henterji.blogbus.com/logs/2005/11/2033950.html
###########################################################################
 */
package com.bhpb.qiworkbench.compAPI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/*
 * ProgressMonitor - updated by Woody Folsom on 5/1/2008 to fix bug where
 * isCancel sometimes does not terminate the progress bar GUI. 
 * 
 */
public class ProgressMonitor {

    private boolean indeterminate;
    private boolean isCancel = false;
    private ChangeEvent ce = new ChangeEvent(this);
    //private ChangeListener changeListener;
    private int milliSecondsToWait = 500;// half second	
    private int total,  current = -1;
    private final List<ChangeListener> changeListeners = Collections.synchronizedList(new ArrayList<ChangeListener>(2));
    private String status;
    private String text;

    public ProgressMonitor(int total, boolean indeterminate, int milliSecondsToWait) {
        this.total = total;
        this.indeterminate = indeterminate;
        this.milliSecondsToWait = milliSecondsToWait;
    }

    public ProgressMonitor(int total, boolean indeterminate) {
        this.total = total;
        this.indeterminate = indeterminate;
    }

    public int getTotal() {
        return total;
    }

    public void setCancel(boolean cancel) {
        isCancel = cancel;
        setCurrent(null, getTotal());
        fireChangeEvent();
    }

    public boolean isCancel() {
        return isCancel;
    }

    public void start(String status) {
        if (current != -1) {
            throw new IllegalStateException("not started yet");
        }
        this.status = status;
        current = 0;
        fireChangeEvent();
    }

    public int getMilliSecondsToWait() {
        return milliSecondsToWait;
    }

    public int getCurrent() {
        return current;
    }

    public String getStatus() {
        return status;
    }

    public boolean isIndeterminate() {
        return indeterminate;
    }

    public void setIndeterminate(boolean indeterminate) {
        this.indeterminate = indeterminate;
    }

    public void setCurrent(String status, int current) {
        if (current == -1) {
            throw new IllegalStateException("not started yet");
        }

        this.current = current;

        if (status != null) {
            this.status = status;
        }

        fireChangeEvent();
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void addChangeListener(ChangeListener listener) {
        //changeListener = listener;
        synchronized(changeListeners) {
            changeListeners.add(listener);
        }
    }

    public void removeChangeListener(ChangeListener listener) {
        //changeListener = null;
        synchronized(changeListeners) {
            changeListeners.remove(listener);
        }
    }

    private void fireChangeEvent() {
        synchronized (changeListeners) {
            for (int i = 0; i < changeListeners.size(); i++) {
                //do not use iterator here, it is not thread-safe if the ArrayList is every changed in any way
                //for example, by add/remove changeListener
                changeListeners.get(i).stateChanged(ce);
            }
        }
    }
}