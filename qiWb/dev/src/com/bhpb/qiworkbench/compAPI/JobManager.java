/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.compAPI;

import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * A convenience class for submitting a script as a job.
 * <p>
 * Relies on the messaging manager of the qiComponent submitting the job.
 *
 * @author Bob Miller
 * @author Gil Hansen
 * @version 1.0
 */
public class JobManager {
    private static Logger logger = Logger.getLogger(JobManager.class.getName());

    private String jobID;
    private ArrayList<String> stdOut;
    private ArrayList<String> stdErr;
    private static int status;
    private int errorStatus;
    private String errorContent;
    /** Messaging manager of the qiComponent agent */
    private IMessagingManager messagingMgr;
    /** GUI of the qiComponent. Mainly used to display warning dialogs. */
    private Component qiCompGUI;
    public static final String killAllNameRelatedProcessIds = "kill_all_name_related_pids.sh";
    /**
     * Constructor.
     * @param messagingMgr Message manager of qiComponent's agent.
     */
    public JobManager(IMessagingManager messagingMgr, Component qiCompGUI) {
        this.messagingMgr = messagingMgr;
        this.qiCompGUI = qiCompGUI;
    }

  /**
   * Submit a job and get standard error and standard out. Method waits
   * until the script finishes execution. Used in those situations when
   * the script runs quickly, i.e., its execution is too short to monitor.
   * @param params SUBMIT_JOB_CMD parameters
   * @return 0 if script executes and terminates normally; otherwise, -1
   */
  public int submitJobWait(ArrayList<String> params) {
      String servicePrefLoc = params.get(0);

      // submit job for execution
      status = 0;
      errorStatus = 0;
      errorContent = "";
      String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SUBMIT_JOB_CMD, QIWConstants.ARRAYLIST_TYPE,params,true);
      IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, 100000);
      if (response == null || response.isAbnormalStatus()) {
        if (response != null) {
            errorStatus = response.getStatusCode();
            errorContent = (String)response.getContent();
        }
        return status = -1;
      }
      jobID = (String)response.getContent();

    //get stdout
    params.clear();
    params.add(servicePrefLoc);
    params.add(jobID);
    msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_JOB_OUTPUT_CMD, QIWConstants.ARRAYLIST_TYPE,params,true);
    response = messagingMgr.getMatchingResponseWait(msgID,10000);
    if(response == null || response.isAbnormalStatus()) {
      if(response != null) {
        errorStatus = response.getStatusCode();
        errorContent = (String)response.getContent();
      }
      return status = -1;
    }
    else
      stdOut = (ArrayList)response.getContent();

    //get stderr
    params.clear();
    params.add(servicePrefLoc);
    params.add(jobID);
    msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_JOB_ERROR_OUTPUT_CMD, QIWConstants.ARRAYLIST_TYPE,params,true);
    response = messagingMgr.getMatchingResponseWait(msgID,10000);
    if(response == null || response.isAbnormalStatus()) {
      if(response != null) {
        errorStatus = response.getStatusCode();
        errorContent = (String)response.getContent();
      }
      return status = -1;
    }
    else
      stdErr = (ArrayList)response.getContent();

    //Wait until the command has finished
    params.clear();
    params.add(servicePrefLoc);
    params.add(jobID);
    msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.WAIT_FOR_JOB_EXIT_CMD, QIWConstants.ARRAYLIST_TYPE,params,true);
    response = messagingMgr.getMatchingResponseWait(msgID,10000);
    if(response == null || response.isAbnormalStatus()) {
      if(response != null) {
        errorStatus = response.getStatusCode();
        errorContent = (String)response.getContent();
      }
      return status = -1;
    }
    else
      status = (Integer)response.getContent();

    // get exitValue
    params.clear();
    params.add(servicePrefLoc);
    params.add(jobID);
    msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_JOB_STATUS_CMD, QIWConstants.ARRAYLIST_TYPE,params,true);
    response = messagingMgr.getMatchingResponseWait(msgID,10000);
    if(response == null)
      return -1;
    else if(status != 0)
      return status = (Integer)response.getContent();

    //release the job
    params.clear();
    params.add(servicePrefLoc);
    params.add(jobID);
    msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.RELEASE_JOB_CMD, QIWConstants.ARRAYLIST_TYPE,params,true);
    response = messagingMgr.getMatchingResponseWait(msgID,10000);
    if(response == null || response.isAbnormalStatus()) {
      if(response != null) {
        errorStatus = response.getStatusCode();
        errorContent = (String)response.getContent();
      }
      return status = -1;
    }
    else
      return status = 0;
  }

    /**
     * Get standard out from last job submitted
     * @return standard out array list
     */
    public ArrayList<String> getStdOut() {
        return stdOut;
    }

    /**
     * Get standard error from last job submitted
     * @return standard error array list
     */
    public ArrayList<String> getStdErr() {
        return stdErr;
    }

    /**
     * Get last setting of errorStatus.
     * @return int Status of job
     */
    public int getErrorStatus() {
        return errorStatus;
    }

    /**
     * Get last setting of errorContent
     * @return String errorContent
     */
    public String getErrorContent() {
        return errorContent;
    }

    /**
     * Submit the generated script for execution. The agent will consume the response (in processMsg()).
     * @params Seeded input args to the SUBMIT_JOB command.
     */
    public void submitJob(List<String> params) {
        params.add(0, messagingMgr.getLocationPref());
        params.add(1, "sh");
        params.add(2, "-c");
        params.add("false");
        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SUBMIT_JOB_CMD, QIWConstants.ARRAYLIST_TYPE, params);
    }

    /**
     * Submit the generated script for execution. The agent will consume the response (in processMsg()).
     * @params enforced the location preferrence.
     * @params Seeded input args to the SUBMIT_JOB command.
     */
    public void submitJob(String locPref, List<String> params) {
        params.add(0, locPref);
        params.add(1, "sh");
        params.add(2, "-c");
        params.add("false");
        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SUBMIT_JOB_CMD, QIWConstants.ARRAYLIST_TYPE, params);
    }
    
    /**
     * Get process ID of the given shell script's name.
     * @param scriptName Name of the script, i.e., script prefix without the .sh suffix..
     * @return Process ID of the script job. Returns null if there is an error getting the pid or the empty string
     *  if the response contains no pid.
     */
    public ArrayList<String> getProcessID(String scriptPath) {
        scriptPath = scriptPath.replace("/", "\\/");
        ArrayList params = new ArrayList();
        String cmd = "ps ux | awk '/" + scriptPath +"/ && !/awk/ {print $2}'";
        params.add(0,messagingMgr.getLocationPref());
        params.add(1,"sh");
        params.add(2,"-c");
        params.add(cmd);
        params.add("true");
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SUBMIT_JOB_CMD, QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
        String scriptName = scriptPath;
        int ind = scriptName.lastIndexOf("/");
        if(ind != -1){
            scriptName = scriptName.substring(ind+1, scriptName.length());
        }

        if (response == null) {
            logger.warning("Response to SUBMIT_JOB_CMD getProcessId for " + scriptName + " is null due to timed out problem.");
            return null;
        } else if (response.isAbnormalStatus()) {
            logger.warning("Response to SUBMIT_JOB_CMD getProcessId for " + scriptName + " with abnormal status: " + (String)response.getContent());
            return null;
        } else {
            String jobID = (String)MsgUtils.getMsgContent(response);
            logger.info("SUBMIT_JOB_CMD jobID = " + jobID);

            //get stdout
            params.clear();
            params.add(messagingMgr.getLocationPref());
            params.add(jobID);
            msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_JOB_OUTPUT_CMD, QIWConstants.ARRAYLIST_TYPE,params,true);
            response = messagingMgr.getMatchingResponseWait(msgID,5000);

            if (response == null) {
                logger.warning("Response to GET_JOB_OUTPUT_CMD getProcessId for " + scriptName + " is null due to timed out problem.");
                return null;
            } else if (response.isAbnormalStatus()) {
                logger.warning("Response to GET_JOB_OUTPUT_CMD getProcessId for " + scriptName + " with abnormal status: " + (String)response.getContent());
                return null;
            } else {
                ArrayList stdOut = (ArrayList)response.getContent();
                logger.info("stdOut getProcessId for " + scriptName + " = " + stdOut);
                    return stdOut;
            }
        }
    }

    private boolean createCancelJobScript(String filePath){
        ArrayList params = new ArrayList();
        /*params.add(messagingMgr.getLocationPref());
        params.add(filePath);
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.CHECK_FILE_EXIST_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);
        String temp = "";
        if(resp != null && !MsgUtils.isResponseAbnormal(resp)){
            temp = (String)resp.getContent();
        }else if(resp == null){
            JOptionPane.showMessageDialog(qiCompGUI,"Timed out problem in running CHECK_FILE_EXIST_CMD.",
                    "IO Error",JOptionPane.WARNING_MESSAGE);
            temp = "error";
        }else{
            JOptionPane.showMessageDialog(qiCompGUI,"Error in running CHECK_FILE_EXIST_CMD. " + (String)MsgUtils.getMsgContent(resp),
                    "IO Error",JOptionPane.WARNING_MESSAGE);
            temp = "error";
        }
        if(temp.equals("yes"))
            return true;

        params.clear();
        */

        ArrayList<String> script = generateCancelScriptCode();


        script.add(0,filePath);
        script.add(0,messagingMgr.getLocationPref());
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.FILE_WRITE_CMD,
                QIWConstants.ARRAYLIST_TYPE,script,true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,10000);
        if(resp == null){
            logger.info("Messaging timed out in writing cancel job script.");
            JOptionPane.showMessageDialog(qiCompGUI,"Messaging timed out in writing the cancel job script. ",
                    "IO Error",JOptionPane.WARNING_MESSAGE);
            return false;

        }else if (MsgUtils.isResponseAbnormal(resp)){
            logger.info("IO error: " +(String)MsgUtils.getMsgContent(resp));
            JOptionPane.showMessageDialog(qiCompGUI,"IO Error in writing the cancel job script. " + (String)MsgUtils.getMsgContent(resp),
                    "IO Error",JOptionPane.WARNING_MESSAGE);
            return false;
        }else
            return true;

    }

    private ArrayList<String> generateCancelScriptCode(){
        ArrayList<String> script = new ArrayList<String>();
        script.add("arg=$1");
        script.add("len=${#arg}");
        script.add("if [ ${len} -eq 0 ]");
        script.add("then");
        script.add("    echo \"Usage: $0 arg\"");
        script.add("    exit");
        script.add("fi");
        String user = System.getProperty("user.name");
        script.add("pid=$(ps -o user,pid,ppid,command -ax | grep $1 | grep " + user + " | awk '{print $2}')");
        script.add("echo \"pid=${pid}\"");
        script.add("ii=0");
        script.add("for i in ${pid}");
        script.add("do");
        script.add("array[${ii}]=$i");
        script.add("ii=`expr ${ii} + 1`");
        script.add("done");
        //script.add("len=`expr ${#array[*]} - 1`");
        //script.add("echo \"len=${len}\"");
        script.add("if [ ${#array[*]} -gt 1 ]");
        script.add("then");
        script.add("    len=1");
        script.add("else");
        script.add("    len=${#array[*]}");
        script.add("fi");
        script.add("echo \"len=${len}\"");
        script.add("for ((  jj = ${len} ;  jj >= 0;  jj--  ))");
        script.add("do");
        script.add("    for child in $(ps -o pid,ppid -ax | awk \"{ if (\\$2 == ${array[${jj}]} ) { print \\$1 }}\")");
        script.add("    do");
        script.add("        echo \"Killing child process $child because ppid = ${array[${jj}]}\"");
        script.add("        kill $child");
        script.add("    done");
        script.add("done");
        return script;
    }
    /**
     * Cancel the given shell script job.
     * String scriptName Name of the script.
     * @return true if job successfully canceled; otherwise, false.
     */
    private boolean cancelJob(ArrayList<String> pids) {
        ArrayList params = new ArrayList();
        if(pids == null || pids.size() == 0){
            logger.info("No process IDs found to be cancelled.");
            return false;
        }
        String pidStr = "( ";
        for(int i = 0; pids != null && i < pids.size(); i++){
             pidStr += pids.get(i) + " ";
        }
        pidStr += ")";
        //String cmd = "kill -9 `ps ux | awk '/" + scriptName +"/ && !/awk/ {print $2}'`";
        String cmd = "x=" + pidStr + "; l=`expr ${#x[*]} - 1`; echo l=${l}; for (( i = ${l} ; i >= 0 ; i-- )) do echo ${x[$i]}; ar=`ps -o pid,ppid -ax | awk \"{ if (\\\\$2 == ${x[${i}]} ) { print \\$1 }}\"`; echo ar1=${ar}; ar=( ${ar} ); for (( j = 0 ; j < ${#ar[*]} ; j++ )) do echo Killing child process ${ar[$j]} because ppid=${x[${i}]}; kill ${ar[$j]}; done done";
        logger.info("cmd " + cmd);
        params.add(0,messagingMgr.getLocationPref());
        params.add(1,"sh");
        params.add(2,"-c");
        params.add(cmd);
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SUBMIT_JOB_CMD, QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, 5000);
        if (response == null) {
            logger.warning("Response to SUBMIT_JOB_CMD for Cancel Job timed out.");
            return false;
        } else if (response.isAbnormalStatus()) {
            logger.warning("Abnormal response to SUBMIT_JOB_CMD for Cancel Job: " + (String)response.getContent());
            return false;
        } else return true;
    }

    /**
     * Cancel the given shell script job.
     * String scriptName Name of the script.
     * @return true if job successfully canceled; otherwise, false.
     */
    private boolean cancelJob(String scriptPath, String logFilePath) {
        if(scriptPath == null || scriptPath.trim().length() == 0)
            return false;
        String filesep = messagingMgr.getServerOSFileSeparator();
        int index =  scriptPath.lastIndexOf(filesep);
        String scriptName = "";
        String scriptDir = messagingMgr.getUserHOME();

        if(index != -1){
            scriptName = scriptPath.substring(index+1);
            scriptDir = scriptPath.substring(0,index);
        }
        if(createCancelJobScript(scriptDir + filesep + killAllNameRelatedProcessIds) == false)
            return false;
        ArrayList params = new ArrayList();

        //String cmd = "kill -9 `ps ux | awk '/" + scriptName +"/ && !/awk/ {print $2}'`";
        String cmd = "cd " + scriptDir + "; chmod u+x " + killAllNameRelatedProcessIds + "; " + killAllNameRelatedProcessIds + " ./" + scriptName + " >> " + logFilePath;
        logger.info("cmd " + cmd);
        params.add(0,messagingMgr.getLocationPref());
        params.add(1,"sh");
        params.add(2,"-c");
        params.add(cmd);
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SUBMIT_JOB_CMD, QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, 5000);
        if (response == null) {
            logger.warning("Response to SUBMIT_JOB_CMD for Cancel Job timed out.");
            return false;
        } else if (response.isAbnormalStatus()) {
            logger.warning("Abnormal response to SUBMIT_JOB_CMD for Cancel Job: " + (String)response.getContent());
            return false;
        } else return true;
    }
    /**
     * Cancel the given shell script job.
     * @param jobMonitor Job monitor for the script..
     * @return true if job successfully canceled; otherwise, false.
     */
    public boolean cancelJob(JobMonitor jobMonitor) {
        if (jobMonitor != null &&
            (jobMonitor.isJobRunning() || jobMonitor.isJobStatusUnknown())) {
            ArrayList<String> pids = getProcessID(jobMonitor.getScriptPrefix() + ".sh");
            if (pids == null || pids.size() == 0) {
                JOptionPane.showMessageDialog(qiCompGUI, "No such process running", "Error In Cancel Job",
                        JOptionPane.WARNING_MESSAGE);
                return false;
            }

            //boolean jobCancelled = cancelJob(jobMonitor.getScriptPath(),);
            boolean jobCancelled = cancelJob(pids);
            jobMonitor.updateStatus(this);
            return jobCancelled;
        } else {
            JOptionPane.showMessageDialog(qiCompGUI, "No active job found to be cancelled.",
                    "No Active Job Found", JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }

    /**
     * Cancel the given shell script job by checking the full path of the script.
     * @param jobMonitor Job monitor for the script..
     * @return true if job successfully canceled; otherwise, false.
     */
    public boolean cancelJob1(JobMonitor jobMonitor) {
        if (jobMonitor != null &&
            (jobMonitor.isJobRunning() || jobMonitor.isJobStatusUnknown())) {
            ArrayList<String> pids = getProcessID(jobMonitor.getScriptPath());
            if (pids == null || pids.size() == 0) {
                JOptionPane.showMessageDialog(qiCompGUI, "No such process running", "Error In Cancel Job",
                        JOptionPane.WARNING_MESSAGE);
                return false;
            }

            //boolean jobCancelled = cancelJob(jobMonitor.getScriptPath(),);
            boolean jobCancelled = cancelJob(pids);
            jobMonitor.updateStatus1(this);
            return jobCancelled;
        } else {
            JOptionPane.showMessageDialog(qiCompGUI, "No active job found to be cancelled.",
                    "No Active Job Found", JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }

    /**
     * Cancel the given shell script job.
     * @param jobMonitor Job monitor for the script..
     * @return true if job successfully canceled; otherwise, false.
     */
    public boolean cancelJob2(JobMonitor jobMonitor) {
        if (jobMonitor != null &&
            (jobMonitor.isJobRunning() || jobMonitor.isJobStatusUnknown())) {
            ArrayList<String> pids = getProcessID(jobMonitor.getScriptPrefix() + ".sh");
            if (pids == null || pids.size() == 0) {
                JOptionPane.showMessageDialog(qiCompGUI, "No such process running", "Error In Cancel Job",
                        JOptionPane.WARNING_MESSAGE);
                return false;
            }

            //boolean jobCancelled = cancelJob(jobMonitor.getScriptPath(), jobMonitor.getStdoutPath(), pids);
            boolean jobCancelled = cancelJob(pids);
            jobMonitor.updateStatus2(this);
            return jobCancelled;
        } else {
            JOptionPane.showMessageDialog(qiCompGUI, "No active job found to be cancelled.",
                    "No Active Job Found", JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }

    /**
     * Get a job's output - stderr or stdout.
     * @param filePath Path of the output path
     * @param offset where the reader starts
     * @param lenght number of bytes to read
     * @return String or null if problems occur
     */
    public String getJobOutput(String filePath, long offset, long length) {
        byte[] bytes = null;
        List params1 = new ArrayList();
        params1.add(messagingMgr.getLocationPref());
        params1.add(filePath);  //the segy file path
        params1.add(Long.valueOf(offset));   //starting from the first byte
        params1.add(Long.valueOf(length)); //until position 3600
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.BINARY_FILE_READ_CMD,
                QIWConstants.ARRAYLIST_TYPE,params1,true);
        IQiWorkbenchMsg res = messagingMgr.getMatchingResponseWait(msgId,10000);
        if(res == null){
            logger.info("Null Response returned due to timed out.");
            return null;
        }else if(res.isAbnormalStatus()){
            String message = "Abnormal response returned due to " + res.getContent();
            logger.info(message);
            return null;
        }else{
            bytes = (byte[])res.getContent();
            if(bytes != null){
            	return new String(bytes);
            }
        }
        return null;
    }

        /**
     * Get a job's output - stderr or stdout.
     * @param outPath Path of the output path
     * @return List[0] "OK" if can read stderr or stdout; otherwise, "error".<br>
     *         List[1] List of stderr/stdout lines if the file can be read; otherwise, reason why the file
     * could not be read.
     */
    public ArrayList getJobOutput(String outPath) {
        ArrayList jobOutput = new ArrayList();
        ArrayList<String> lines = new ArrayList<String>();

        ArrayList params = new ArrayList();
        params.add(messagingMgr.getLocationPref());
        params.add(outPath);
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.FILE_READ_CMD, QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgID, 10000);
        if (resp == null) {
            logger.warning("Null Response returned due to FILE_READ_CMD timed out.");
            //JOptionPane.showMessageDialog(qiCompGUI,"Time out error running FILE_READ_CMD to read stderr/stdout.", "IO Error", JOptionPane.WARNING_MESSAGE);
            jobOutput.add("error");
            lines.add("IO Error: could not read job's stderr/stdout file.");
            jobOutput.add(lines);
        } else if (resp.isAbnormalStatus()) {
            String message = "Abnormal response returned due to " + resp.getContent();
            logger.info(message);
//            JOptionPane.showMessageDialog(qiCompGUI,"Error in running FILE_READ_CMD due to " + resp.getContent(), "IO Error", JOptionPane.WARNING_MESSAGE);
            jobOutput.add("error");
            lines.add((String)resp.getContent());
            jobOutput.add(lines);
        } else {
            jobOutput.add("OK");
            lines = (ArrayList<String>)resp.getContent();
            jobOutput.add(lines);
        }

        return jobOutput;
    }

}
