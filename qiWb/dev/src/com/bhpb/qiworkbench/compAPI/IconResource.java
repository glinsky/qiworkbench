/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.compAPI;

import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  Class to load image files to create icon. <br><br>
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class IconResource {
    private static final Logger logger =
            Logger.getLogger(IconResource.class.getName());
    public static final String LOGO_ICON_BHP = "/icon/BHP_Logo.gif";
    public static final String LOGO_ICON_QI = "/icon/qiIcon52x52.gif";
    public static final String LOGO_ICON_DUG = "/icon/DownUnder_Logo.gif";
    public static final String LOGO_ICON = "/icon/Logo_Set.gif";
    public static final String VIEWALL_ICON = "/icon/ViewAll.gif";
    public static final String ZOOMRESET_ICON = "/icon/ZoomReset.gif";
    public static final String ZOOMRUBBER_ICON = "/icon/ZoomRubber.gif";
    public static final String ZOOMIN_ICON = "/icon/ZoomIn.gif";
    public static final String ZOOMOUT_ICON = "/icon/ZoomOut.gif";
    public static final String STEPPLUS_ICON = "/icon/StepPlus.gif";
    public static final String STEPMINUS_ICON = "/icon/StepMinus.gif";
    public static final String SHOWLAYER_ICON = "/icon/ShowLayer.gif";
    public static final String HIDELAYER_ICON = "/icon/HideLayer.gif";

    public static final String SEISMICLAYER_ICON = "/icon/SeismicLayer.gif";
    public static final String MODELLAYER_ICON = "/icon/ModelLayer.gif";
    public static final String EVENTLAYER_ICON = "/icon/EventLayer.gif";
    public static final String UNKNOWNLAYER_ICON = "/icon/UnknownLayer.gif";

    public static final String ELIGIBLEFILE_ICON = "/icon/EligibleFile.gif";
    public static final String GENERALFILE_ICON = "/icon/GeneralFile.gif";
    public static final String DIRECTORY_ICON = "/icon/Directory.gif";
    public static final String HOME_ICON = "/icon/Home.gif";
    public static final String UP_ICON = "/icon/Up.gif";

    public static final String OSPCONNECT_ICON = "/icon/OspConnect.gif";
    public static final String OSPDISCONNECT_ICON = "/icon/OspConnectNo.gif";

    public static final String CSH_ICON = "/icon/Help.gif";
    public static final String CSHSM_ICON = "/icon/HelpSM.gif";

    public static final String PICKMODEPBP_ICON = "/icon/PickPBP.gif";
    public static final String PICKMODEPTP_ICON = "/icon/PickPTP.gif";
    public static final String PICKSNAPNO_ICON = "/icon/PickSnapNo.gif";
    public static final String PICKSNAPMAX_ICON = "/icon/PickSnapMax.gif";
    public static final String PICKSNAPMIN_ICON = "/icon/PickSnapMin.gif";
    public static final String PICKSNAPZERO_ICON = "/icon/PickSnapZero.gif";

    public static final String SNAPSHOT_ICON = "/icon/Snapshot.gif";
    public static final String PLAY_ICON = "/icon/Play.gif";
    public static final String PLAYNOW_ICON = "/icon/PlayNow.gif";
    public static final String STOP_ICON = "/icon/Stop.gif";
    public static final String SAVE_ICON = "/icon/Save.gif";
    public static final String EDIT_ICON = "/icon/Edit.gif";
    public static final String CLOSE_ICON = "/icon/Close.gif";
    public static final String REWIND_ICON = "/icon/Rewind.gif";
    public static final String FREWIND_ICON = "/icon/Rewind2.gif";
    public static final String FORWARD_ICON = "/icon/Forward.gif";
    public static final String FFORWARD_ICON = "/icon/Forward2.gif";
    public static final String PREFERENCE_ICON = "/icon/Option.gif";
    public static final String OPEN24_ICON = "/icon/Open24.gif";


    public static final String ERROR_ICON = "/icon/Error.gif";
    public static final String WARNING_ICON = "/icon/Warning.gif";
    public static final String INFO_ICON = "/icon/Info.gif";
    public static final String BIG_ERROR_ICON = "/icon/Big_Error.gif";
    public static final String BIG_WARNING_ICON = "/icon/Big_Warn.gif";

    private static IconResource _instance = null;

    protected IconResource() {
    }

    public static IconResource getInstance() {
        if (_instance == null) _instance = new IconResource();
        return _instance;
    }

    public ImageIcon getImageIcon(String imageFile) {
        ImageIcon result = null;
        try {
            result = new ImageIcon(this.getClass().getResource(imageFile));
        }
        catch (Exception ex) {
            logger.warning("IconResource.getImageIcon exception: ["+imageFile+"]");
            logger.warning("    " + ex.toString());
            return null;
        }
        return result;
    }
}
