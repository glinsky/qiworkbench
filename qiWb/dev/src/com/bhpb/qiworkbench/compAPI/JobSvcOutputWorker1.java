/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.compAPI;

public class JobSvcOutputWorker1 implements Runnable {
	private volatile JobManager jobManager;
	private volatile String output = new String();
	private volatile String path;
    private volatile long offset;
    private volatile long length;


	public JobSvcOutputWorker1(JobManager jobManager,String path, long offset, long length) {
		this.jobManager = jobManager;
		this.path = path;
        this.offset = offset;
        this.length = length;
	}

	public void run() {
		StringBuffer buf = new StringBuffer();
		String str = jobManager.getJobOutput(path, offset, length);
		if(str == null || str.length() == 0)
			return;
		buf.append(str);
		offset += str.length();
		while(str.length() != 0){
			str = jobManager.getJobOutput(path, offset, length);
			buf.append(str);
			offset += str.length();
			try{
				Thread.sleep(1000);
			}catch(InterruptedException e){}
		}
		output = buf.toString();
	}


	public String getOutput() {
		return output;
	}

    public long getOffset(){
        return offset;
    }
}