/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.compAPI;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Monitor the execution of a script. Usually, the script is generated and
 * executed by a qiComponent. Used instead of JobManager when the execution of
 * a script takes an appreciable amount of time.
 */
public class JobMonitor {
    private static Logger logger = Logger.getLogger(JobMonitor.class.getName());

    // Job Status
    public static final String JOB_UNDEFINED_STATUS = "Undefined";
    public static final String JOB_UNSUBMITTED_STATUS = "Job Unsubmitted";
    public static final String JOB_RUNNING_STATUS = "Job Running";
    public static final String JOB_ENDED_STATUS = "Job Ended";
    public static final String JOB_ENDED_NORMALLY_STATUS = "Job Ended Normally";
    public static final String JOB_ENDED_ABNORMALLY_STATUS = "Job Ended Abnormally";
    public static final String JOB_UNKNOWN_STATUS = "Unknown";

    /** Status of job */
    String jobStatus = JOB_UNDEFINED_STATUS;

    /** Script directory as specified in the project's properties */
    String scriptDir = "";
    /** Prefix to script file, i.e., the name of the script file without the .sh suffix. */
    String scriptPrefix = "";
    /**
     * Path prefix to stderr, stdout and script files. Usually
     * defaultPreferredScriptDir + filesep + scriptPrefix.
     */
    String jobFilePrefix = "";
    /** Full path of stderr file. */
    String stderrPath = "";
    /** Full path of stdout file. */
    String stdoutPath = "";
    /** Full path of script file. */
    String scriptPath = "";

    /** Lines in stdout. Set each time status is updated. */
    ArrayList<String> stdoutLines = null;
    /** Lines in stderr. Set each time status is updataed. */
    ArrayList<String> stderrLines = null;


    /** stdout string. Set each time status is updated. */
    StringBuilder stdout = new StringBuilder();
    /** stderr string. Set each time status is updataed. */
    StringBuilder stderr = new StringBuilder();

    /** Default constructor. Used to restore class instance from XML */
    public JobMonitor() { };

    /** Constructor.
     * @param jobFilePrefix Path prefix for all generated files.
     * @param scriptDir Path of directory housing generated script.
     * @param scriptPrefix Name of the generated script file without the .sh suffix.
     */
    public JobMonitor(String jobFilePrefix, String scriptDir, String scriptPrefix) {
        this.scriptDir = scriptDir;
        this.scriptPrefix = scriptPrefix;
        this.jobFilePrefix = jobFilePrefix;
        this.stderrPath = jobFilePrefix + ".err";
        this.stdoutPath = jobFilePrefix + ".out";
        this.scriptPath = jobFilePrefix + ".sh";
        this.jobStatus = JOB_UNSUBMITTED_STATUS;
    }

    /**
     * Get the status of the submitted job.
     * @return Status of the job.
     */
    public String getJobStatus() {
        if (jobStatus.equals(JOB_ENDED_STATUS)) {
            String lastLine = stdoutLines.get(stdoutLines.size()-1);
            int index = lastLine.lastIndexOf('\n');
            while(index != -1){
               lastLine = lastLine.substring(0, index);
               index = lastLine.lastIndexOf('\n');
            }

            String scriptName = scriptPrefix + ".sh";
            return lastLine.endsWith(scriptName + " job ends normally") ? JOB_ENDED_NORMALLY_STATUS : JOB_ENDED_ABNORMALLY_STATUS;
            //return lastLine.contains(scriptPath + " job ends normally") ? JOB_ENDED_NORMALLY_STATUS : JOB_ENDED_ABNORMALLY_STATUS;
        }
        return jobStatus;
    }

   /**
     * Get the status of the submitted job.
     * @return Status of the job.
     */
    public String getJobStatus1() {
        if (jobStatus.equals(JOB_ENDED_STATUS)) {
            String scriptName = scriptPrefix + ".sh";
            return stdout.toString().endsWith(scriptName + " job ends normally\n") ? JOB_ENDED_NORMALLY_STATUS : JOB_ENDED_ABNORMALLY_STATUS;
        }
        return jobStatus;
    }


    /**
     * Check if job has been submitted.
     *
     */
    public boolean isJobSubmitted() {
        return jobStatus.equals(JOB_UNSUBMITTED_STATUS) ? false : true;
    }

    /**
     * Check if job has is running.
     *
     */
    public boolean isJobRunning() {
        return jobStatus.equals(JOB_RUNNING_STATUS) ? true : false;
    }

    /**
     * Check if job has is has terminated.
     *
     */
    public boolean isJobEnded() {
        return (jobStatus.equals(JOB_ENDED_STATUS) ||
                jobStatus.equals(JOB_ENDED_NORMALLY_STATUS) ||
                jobStatus.equals(JOB_ENDED_ABNORMALLY_STATUS)) ? true : false;
    }

    /**
     * Check if job status is unknown.
     *
     */
    public boolean isJobStatusUnknown() {
        return jobStatus.equals(JOB_UNKNOWN_STATUS) ? true : false;
    }

    /**
     * Set the status of the job to 'running'
     */
    public void setJobStatusToRunning() {
        this.jobStatus = JOB_RUNNING_STATUS;
    }

    /**
     * Get the job file prefix.
     * @return Job file prefix.
     */
    public String getJobFilePrefix() {
        return jobFilePrefix;
    }
    /**
     * Get the path of the job's stderr file
     * @return Path of job's stderr file
     */
    public String getStderrPath() {
        return stderrPath;
    }

    /**
     * Get the path of the job's stdout file
     * @return Path of job's stdout file
     */
    public String getStdoutPath() {
        return stdoutPath;
    }

    /**
     * Get the path of the job's script file
     * @return Path of job's script file
     */
    public String getScriptPath() {
        return scriptPath;
    }

    /**
     * Get the directory of the job's script file
     * @return Directory of job's script file
     */
    public String getScriptDir() {
        return scriptDir;
    }

    /**
     * Get the name of the generated script file without the .sh suffix.
     * @return Name of the generated script.
     */
    public String getScriptPrefix() {
        return scriptPrefix;
    }

    public ArrayList<String> getStderrLines() {
        return stderrLines;
    }

    /**
     * Get the formatted text for stderr..
     * @return Latest stderr text.
     */
    public String fetchStderrText() {
        if (stderrLines == null) return "";

        StringBuffer sbuf = new StringBuffer();
        
        sbuf.append("Standard Error:\n\n");
        
        for (String s : stderrLines) {
            sbuf.append(s);
            sbuf.append("\n");
        }

        return sbuf.toString();
    }

    public String getStderrText(){
        return "Standard Error:\n\n" + stderr.toString();
    }

    public String getStdoutText(){
        return "Standard Output:\n\n" + stdout.toString();
    }

    public ArrayList<String> getStdoutLines() {
        return stdoutLines;
    }

    /**
     * Get the formatted text for stdout..
     * @return Latest stdout text.
     */
    public String fetchStdoutText() {
        if (stdoutLines == null) return "";

        StringBuffer sbuf = new StringBuffer();
        
        sbuf.append("Standard Output:\n\n");
        
        for (String s : stdoutLines) {
            sbuf.append(s);
            sbuf.append("\n");
        }

        return sbuf.toString();
    }

    //SETTERS. Required by restore state to create class instance
    public void setScriptDir(String scriptDir) {
        this.scriptDir = scriptDir;
    }
    public void setScriptPrefix(String scriptPrefix) {
        this.scriptPrefix = scriptPrefix;
    }
    public void setJobFilePrefix(String jobFilePrefix) {
        this.jobFilePrefix = jobFilePrefix;
    }
    public void setStderrPath(String stderrPath) {
        this.stderrPath = stderrPath;
    }
    public void setStdoutPath(String stdoutPath) {
        this.stdoutPath = stdoutPath;
    }
    public void setScriptPath(String scriptPath) {
        this.scriptPath = scriptPath;
    }
    public void setStderrLines(ArrayList<String> lines) {
        this.stderrLines = lines;
    }
    public void setStdoutLines(ArrayList<String> lines) {
        this.stdoutLines = lines;
    }
    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    /**
     * Update the status of the job. Invoked only if job exists.
     * @param jobManager Job manager for the job.
     * @return Updated status of the job as a string message.
     */
    public String updateStatus(final JobManager jobManager) {
        ArrayList<String> pids = jobManager.getProcessID(scriptPrefix);
        logger.fine("Update job status; pids = " + pids);

        String jobStatusMsg = "";

        if (pids != null && pids.size() > 0) {
            jobStatusMsg = JOB_RUNNING_STATUS;
        } else if (pids != null && pids.size() == 0) {
            jobStatus = JOB_ENDED_STATUS;
        } else {
            jobStatusMsg = JOB_UNKNOWN_STATUS;
        }

        JobSvcOutputWorker jobOutputWorker1 = new JobSvcOutputWorker(jobManager, stdoutPath);
        Thread worker1 = new Thread(jobOutputWorker1);


        JobSvcOutputWorker jobOutputWorker2 = new JobSvcOutputWorker(jobManager, stderrPath);
        Thread worker2 = new Thread(jobOutputWorker2);
        worker1.start();
        worker2.start();
        try{
        	worker1.join();
        	worker2.join();
        }catch(InterruptedException e){
        	e.printStackTrace();
        }
        stdoutLines = jobOutputWorker1.getOutput();
        if (jobOutputWorker1.getStatus().equals(JOB_UNKNOWN_STATUS)) {
        	jobStatusMsg = JOB_UNKNOWN_STATUS;
		}
        
		stderrLines = jobOutputWorker2.getOutput();
		if (jobOutputWorker2.getStatus().equals(JOB_UNKNOWN_STATUS)) {
			jobStatusMsg = JOB_UNKNOWN_STATUS;
		}
		
        if (jobStatus.equals(JOB_ENDED_STATUS)) {
            if(stdoutLines.size() >= 1){
                String lastLine = stdoutLines.get(stdoutLines.size()-1);
                int index = lastLine.lastIndexOf('\n');
                while(index != -1){
                   lastLine = lastLine.substring(0, index);
                   index = lastLine.lastIndexOf('\n');
                }

                if (lastLine.endsWith(scriptPath + " job ends normally"))
                    jobStatusMsg = JOB_ENDED_NORMALLY_STATUS;
                else
                    jobStatusMsg = JOB_ENDED_ABNORMALLY_STATUS;
            }else{
                jobStatusMsg = JOB_ENDED_ABNORMALLY_STATUS;
            }
        }


        return jobStatusMsg;
    }

    long stdoutOffset = 0;
    long stderrOffset = 0;
    long length = 102400;

    /**
     * Update the status of the job. Invoked only if job exists.
     * @param jobManager Job manager for the job.
     * @return Updated status of the job as a string message.
     */
    public String updateStatus1(final JobManager jobManager) {
        ArrayList<String> pids = jobManager.getProcessID(scriptPath);
        logger.fine("Update job status; pids = " + pids);

        String jobStatusMsg = "";

        if (pids != null && pids.size() > 0) {
            jobStatusMsg = JOB_RUNNING_STATUS;
        } else if (pids != null && pids.size() == 0) {
            jobStatus = JOB_ENDED_STATUS;
        } else {
            jobStatusMsg = JOB_UNKNOWN_STATUS;
        }

        JobSvcOutputWorker1 jobOutputWorker1 = new JobSvcOutputWorker1(jobManager, stdoutPath,stdoutOffset,length);
        Thread worker1 = new Thread(jobOutputWorker1);


        JobSvcOutputWorker1 jobOutputWorker2 = new JobSvcOutputWorker1(jobManager, stderrPath,stderrOffset,length);
        Thread worker2 = new Thread(jobOutputWorker2);
        worker1.start();
        worker2.start();
        try{
        	worker1.join();
        	worker2.join();
        }catch(InterruptedException e){
        	e.printStackTrace();
        }

        stdoutOffset = jobOutputWorker1.getOffset();
        stderrOffset = jobOutputWorker2.getOffset();

        stdout.append(jobOutputWorker1.getOutput());
		stderr.append(jobOutputWorker2.getOutput());
		

        if (jobStatus.equals(JOB_ENDED_STATUS) || jobStatus.equals(JOB_RUNNING_STATUS)) {
            String temp = stdout.toString();
            if (temp.endsWith(scriptPrefix + ".sh job ends normally\n")){
                jobStatusMsg = JOB_ENDED_NORMALLY_STATUS;
                jobStatus = JOB_ENDED_STATUS;
            }else{
                //ps id could be fake because user could view the script by using editor
                //where ps id is valid but the acutal job may already end.
                if(jobStatus.equals(JOB_RUNNING_STATUS)){
                    if(temp.endsWith(scriptPrefix + ".sh job ends abnormally\n")){
                        jobStatusMsg = JOB_ENDED_ABNORMALLY_STATUS;
                        jobStatus = JOB_ENDED_STATUS;
                    }
                }else{ //job ended
                    jobStatusMsg = JOB_ENDED_ABNORMALLY_STATUS;
                }
            }
        }

        return jobStatusMsg;
    }

    /**
     * Update the status of the job without fetching the stderr output. Invoked only if job exists.
     * @param jobManager Job manager for the job.
     * @return Updated status of the job as a string message.
     */
    public String updateStatus2(JobManager jobManager) {
        ArrayList<String> pids = jobManager.getProcessID(scriptPrefix);
        logger.fine("Update job status; pid = " + pids);

        String jobStatusMsg = "";

        if (pids != null && pids.size() > 0) {
            jobStatus = JOB_RUNNING_STATUS;
        } else if (pids != null && pids.size() == 0) {
            jobStatus = JOB_ENDED_STATUS;
        } else {
            jobStatus = JOB_UNKNOWN_STATUS;
            return jobStatus;
        }

        //ArrayList jobOutput = jobManager.getJobOutput(stdoutPath);
        //stdoutLines = (ArrayList<String>)jobOutput.get(1);
        //if (((String)jobOutput.get(0)).equals("error")) {
        //    jobStatus = JOB_UNKNOWN_STATUS;
        //    return JOB_UNKNOWN_STATUS;
        //}

        JobSvcOutputWorker jobOutputWorker1 = new JobSvcOutputWorker(jobManager, stdoutPath);
        Thread worker1 = new Thread(jobOutputWorker1);
        worker1.start();
        try{
        	worker1.join();
        }catch(InterruptedException e){
        	e.printStackTrace();
        }
        stdoutLines = jobOutputWorker1.getOutput();
        
        if (jobStatus.equals(JOB_ENDED_STATUS)) {
            String lastLine = stdoutLines.get(stdoutLines.size()-1);
            if (lastLine.endsWith(scriptPath + " job ends normally\n"))
                jobStatusMsg = JOB_ENDED_NORMALLY_STATUS;
            else
                jobStatusMsg = JOB_ENDED_ABNORMALLY_STATUS;
        }


        return jobStatusMsg;
    }

    /** Display attributes of project */
    public String toString() {
        StringBuffer buf  = new StringBuffer();
        buf.append(this.getClass().toString());
        buf.append(": \n Job Status=");
        buf.append(this.jobStatus);
        buf.append(", script Dir=");
        buf.append(this.scriptDir);
        buf.append(", Script Prefix=");
        buf.append(this.scriptPrefix);
        buf.append(", Job File Prefix=");
        buf.append(this.jobFilePrefix);
        buf.append(", stderr Path=");
        buf.append(this.stderrPath);
        buf.append(", stdout Path=");
        buf.append(this.stdoutPath);
        buf.append(", script Path=");
        buf.append(this.scriptPath);
        buf.append(", stderr Text=");
        buf.append(fetchStderrText());
        buf.append(", stdout Text=");
        buf.append(fetchStdoutText());
        return buf.toString();
    }
}
