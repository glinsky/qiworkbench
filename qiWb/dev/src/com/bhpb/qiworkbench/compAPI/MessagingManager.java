/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.compAPI;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.QiSpaceDescriptor;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;

import com.bhpb.qiworkbench.client.util.ComponentUtils;
import com.bhpb.qiworkbench.messageFramework.BlockingMsgHandler;
import com.bhpb.qiworkbench.messageFramework.ConcurrentMsgHandler;
import com.bhpb.qiworkbench.messageFramework.DispatcherConnector;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
import com.bhpb.qiworkbench.messageFramework.RequestList;
import com.bhpb.qiworkbench.workbench.WorkbenchGUI;
import com.bhpb.qiworkbench.workbench.WorkbenchManager;
import com.thoughtworks.xstream.XStream;

/**
 * Messaging manager for a qiComponent. This is the developer's API inteface
 * of a qiComponent. Basically these are high level interfaces
 * to functionality provided by the message framework. It avoids exposing details
 * of the framework's implementation.
 * <p>
 * The license of the API is BSD so it protects the
 * developer of a qiComponent from directly touching open-source code under the
 * GPL V2 license.
 * <p>
 * The MessagingManager carries messaging state for a component. Therefore, each
 * component has its own instance.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class MessagingManager implements IMessagingManager {
    private static Logger logger = Logger.getLogger(MessagingManager.class.getName());

    /** Message handler for the component */
    private BlockingMsgHandler msgHandler;

    /** Component's descriptor */
    private IComponentDescriptor compDesc = null;

    /** Message Dispatcher's component descriptor */
    IComponentDescriptor mdCompDesc = null;

    /** Message Dispatcher's message handler. Returned when the component
     *  registers itself with the Message Dispatcher. */
    private ConcurrentMsgHandler mdMsgHandler = null;

    /** Unique, system wide ID for the component (CID) */
    private String cid = "";

    /** Lock held by peek */
    private final ReentrantLock peekLock = new ReentrantLock();

    /** Wait queue for waiting takes */
    private final Condition notEmpty = peekLock.newCondition();
    /** List of requests waiting on for a response. Maintaining a list keeps the messages
     *  in the order they were sent. When the component gets a response, it can search the list
     *  for the command with the matching message ID. Based on the command sent, the viewer
     *  can determine how to process the response.
     * <p>
     *  It is the responsibility of the component to remove the request from the list.
     */
    private RequestList outstandingRequests = new RequestList();

    private volatile String runtimeServerFileSep = null;
    private volatile String runtimeServerLineSep = null;
    
    /**
     * Register component with the Message Dispatcher. Form message handler, CID,
     * descriptor.The register and save the Message Dispatcher's message handler
     * so can communicate with it.
     *
     * @param compKind Kind of component.
     * @param compName Display name of the component.
     * @param compCID Unique, system wide ID (CID) for the component.
     */
    public void registerComponent(String compKind, String compName, String compCID) {
        // create plugin's message handler
        msgHandler = new BlockingMsgHandler();

        // create descriptor for self
        compDesc = new ComponentDescriptor();
        compDesc.setCID(compCID);
        compDesc.setMsgHandler(msgHandler);
        compDesc.setComponentKind(compKind);
        compDesc.setDisplayName(ComponentUtils.genUniqueName(compName));
        logger.fine("component's descriptor formed:"+compDesc.toString());

        // register with the message dispatcher
        mdCompDesc = MessageDispatcher.getInstance().registerComponent(compDesc);
        mdMsgHandler = (ConcurrentMsgHandler)mdCompDesc.getMsgHandler();
    }

    /**
     * Request workbench desktop container
     * @return Workbench Desktop Pane
     */
    public JDesktopPane getWorkbenchDesktopPane(){
    	return ((WorkbenchGUI)WorkbenchManager.getInstance().getWorkbenchGUI()).getDesktopPane();
    }
    
    /*
     * Unregister a component with the Messgage Dispatcher. Sent when component has de-activated.
     * @param IComponentDescriptor Descriptor of de-activated component
     */
    public void unregisterComponent(IComponentDescriptor compDesc) {
      MessageDispatcher.getInstance().unregisterComponent(compDesc);
    }

    /**
     * Register component with the Message Dispatcher. Form message handler, CID,
     * descriptor.The register and save the Message Dispatcher's message handler
     * so can communicate with it.
     *
     * @param compKind Kind of component.
     * @param compName Display name of the component.
     * @param compCID Unique, system wide ID (CID) for the component.
     * @param isSystemComp if the registered component is a system wide component.
     */
    public void registerComponent(String compKind, String compName, String compCID, boolean isSystemComp) {
        // create plugin's message handler
        msgHandler = new BlockingMsgHandler();

        // create descriptor for self
        compDesc = new ComponentDescriptor();
        compDesc.setCID(compCID);
        compDesc.setMsgHandler(msgHandler);
        compDesc.setComponentKind(compKind);
        if(!isSystemComp)
            compDesc.setDisplayName(ComponentUtils.genUniqueName(compName));
        else
            compDesc.setDisplayName(compName);
        logger.fine("component's descriptor formed:"+compDesc.toString());

        // register with the message dispatcher
        mdCompDesc = MessageDispatcher.getInstance().registerComponent(compDesc);
        mdMsgHandler = (ConcurrentMsgHandler)mdCompDesc.getMsgHandler();
    }

    /**
     * Send a request with no content to the message dispatcher.
     *
     * @param routing Kind of command.
     * @param command Command to be executed by the message dispatcher.
     * @return ID of request message.
     */
    public String sendRequest(String routing, String command) {
        // compose the message
        IQiWorkbenchMsg request = new QiWorkbenchMsg(compDesc.getCID(),
                       mdCompDesc.getCID(),routing, command,
                       MsgUtils.genMsgID(),"","");
        // remember outstanding request
        outstandingRequests.addRequest(request);
        // send the message to the message dispatcher for processing
        mdMsgHandler.enqueue(request);
        return request.getMsgID();
    }

    /**
     * Send a request with no content to the message dispatcher.
     *
     * @param routing Kind of command.
     * @param command Command to be executed by the message dispatcher.
     * @param skip flag determining if the message should skip from component's main processMsg method mainly used by response type message.
     * @return ID of request message.
     */
    public String sendRequest(String routing, String command, boolean skip) {
        // compose the message
        IQiWorkbenchMsg request = new QiWorkbenchMsg(compDesc.getCID(),
                       mdCompDesc.getCID(),routing, command,
                       MsgUtils.genMsgID(),"","",skip);
        // remember outstanding request
        outstandingRequests.addRequest(request);
        // send the message to the message dispatcher for processing
        mdMsgHandler.enqueue(request);
        return request.getMsgID();
    }

    /**
     * Send a request with content to the message dispatcher.
     *
     * @param routing Kind of command.
     * @param command Command to be executed by the message dispatcher.
     * @param contentType Content's type
     * @param content Content object
     *
     * @return ID of request message.
     */
    public String sendRequest(String routing, String command, String contentType, Object content) {
        // compose the message
        IQiWorkbenchMsg request = new QiWorkbenchMsg(compDesc.getCID(),
                       mdCompDesc.getCID(),routing, command,
                       MsgUtils.genMsgID(),contentType, content);
        // remember outstanding request
        outstandingRequests.addRequest(request);
        logger.fine("Sending " + contentType + content + " from " + compDesc.getCID() + " to " + mdCompDesc.getCID());
        // send the message to the message dispatcher for processing
        mdMsgHandler.enqueue(request);
        return request.getMsgID();
    }

    /**
     * Send a request with content to the message dispatcher.
     *
     * @param routing Kind of command.
     * @param command Command to be executed by the message dispatcher.
     * @param contentType Content's type
     * @param content Content object
     * @param skip flag determining if the message should skip from component's main processMsg method mainly used by response type message.
     * @return ID of request message.
     */
    public String sendRequest(String routing, String command, String contentType, Object content, boolean skip) {
        // compose the message
        IQiWorkbenchMsg request = new QiWorkbenchMsg(compDesc.getCID(),
                       mdCompDesc.getCID(),routing, command,
                       MsgUtils.genMsgID(),contentType, content,skip);
        // remember outstanding request
        outstandingRequests.addRequest(request);
        logger.fine("Sending " + contentType + content + " from " + compDesc.getCID() + " to " + mdCompDesc.getCID());
        // send the message to the message dispatcher for processing
        mdMsgHandler.enqueue(request);
        return request.getMsgID();
    }

    /**
     * Send a request with no content to the designated consumer.
     *
     * @param routing Kind of command.
     * @param command Command to be executed by the target consumer.
     * @param targetDesc Descriptor of the consumer.
     * @return ID of request message.
     */
    public String sendRequest(String routing, String command, IComponentDescriptor targetDesc) {
        // compose the message
        IQiWorkbenchMsg request = new QiWorkbenchMsg(compDesc.getCID(),
                       targetDesc.getCID(), routing, command,
                       MsgUtils.genMsgID(),"","");
        // remember outstanding request
        outstandingRequests.addRequest(request);
        // send the message to the message dispatcher for processing
        mdMsgHandler.enqueue(request);
        return request.getMsgID();
    }

    /**
     * Send a request with no content to the designated consumer.
     *
     * @param routing Kind of command.
     * @param command Command to be executed by the target consumer.
     * @param targetDesc Descriptor of the consumer.
     * @param skip flag determining if the message should skip from component's main processMsg method mainly used by response type message.
     * @return ID of request message.
     */
    public String sendRequest(String routing, String command, IComponentDescriptor targetDesc, boolean skip) {
        // compose the message
        IQiWorkbenchMsg request = new QiWorkbenchMsg(compDesc.getCID(),
                       targetDesc.getCID(), routing, command,
                       MsgUtils.genMsgID(),"","", skip);
        // remember outstanding request
        outstandingRequests.addRequest(request);
        // send the message to the message dispatcher for processing
        mdMsgHandler.enqueue(request);
        return request.getMsgID();
    }

    /**
     * Send a request with content to the designated consumer.
     *
     * @param routing Kind of command.
     * @param command Command to be executed by the target consumer.
     * @param targetDesc Descriptor of the consumer.
     * @param contentType Content's type
     * @param content Content object
     * @return ID of request message.
     */
    public String sendRequest(String routing, String command, IComponentDescriptor targetDesc, String contentType, Object content) {
        // compose the message
        IQiWorkbenchMsg request = new QiWorkbenchMsg(compDesc.getCID(),
                       targetDesc.getCID(),routing, command,
                       MsgUtils.genMsgID(),contentType, content);
        // remember outstanding request
        outstandingRequests.addRequest(request);
        // send the message to the message dispatcher for processing
        mdMsgHandler.enqueue(request);
        return request.getMsgID();
    }

    /**
     * Send a request with content to the designated consumer.
     *
     * @param routing Kind of command.
     * @param command Command to be executed by the target consumer.
     * @param targetDesc Descriptor of the consumer.
     * @param contentType Content's type
     * @param content Content object
     * @param skip flag determining if the message should skip from component's main processMsg method mainly used by response type message.
     * @return ID of request message.
     */
    public String sendRequest(String routing, String command, IComponentDescriptor targetDesc, String contentType, Object content, boolean skip) {
        // compose the message
        IQiWorkbenchMsg request = new QiWorkbenchMsg(compDesc.getCID(),
                       targetDesc.getCID(),routing, command,
                       MsgUtils.genMsgID(),contentType, content, skip);
        // remember outstanding request
        outstandingRequests.addRequest(request);
        // send the message to the message dispatcher for processing
        mdMsgHandler.enqueue(request);
        return request.getMsgID();
    }

    /**
     * Send a normal response with content back to the original requester,
     *
     * @param request Original request message
     * @param contentType Content's type
     * @param content Content object
     */
    public void sendResponse(IQiWorkbenchMsg request, String contentType, Object content) {
        // compose the response message
        IQiWorkbenchMsg response = new QiWorkbenchMsg(request.getConsumerCID(),
                       request.getProducerCID(), QIWConstants.DATA_MSG, request.getCommand(),
                       request.getMsgID(), contentType, content,request.skip());
        response.setStatusCode(MsgStatus.SC_OK);
        // descriptor cannot be null because it is registered
        IComponentDescriptor requesterDesc = MessageDispatcher.getInstance().getComponentDesc(request.getProducerCID());
        // send the message back to the requester for processing
        requesterDesc.getMsgHandler().enqueue(response);
    }

    /**
     * Route a message to the consumer. Used to route an abnormal message.
     *
     * @param msg Message to be routed.
     */
    public void routeMsg(IQiWorkbenchMsg msg) {
        // descriptor cannot be null because it is registered
        IComponentDescriptor desc = MessageDispatcher.getInstance().getComponentDesc(msg.getConsumerCID());
        
        if (desc == null)
            logger.severe("MessageDispatcher.getInstance().getComponentDesc() returned null descriptor for message with consumer CID: " + msg.getConsumerCID());
        if (desc.getMsgHandler() == null)
            logger.severe("Cannot route msg, descriptor: " + desc.getCID() + " has null MsgHandler");
            
        desc.getMsgHandler().enqueue(msg);
    }

    /**
     * Send a request to the Servlet Dispatcher for processing.
     *
     * @param msg Request message to be sent
     * @return Response message
     */
    public IQiWorkbenchMsg sendRemoteRequest(IQiWorkbenchMsg request) {
        return DispatcherConnector.getInstance().sendRequestMsg(request);
    }

    /**
     * Check if the message queue is empty.
     * @return true f message queue empty; otherwise, false.
     */
    public boolean isMsgQueueEmpty() {
        return msgHandler.isQueueEmpty() ? true : false;
    }

    /**
     * Get next message from message queue. If the message queue is empty,
     * return immediately with null.
     *
     * @return Next message in queue; null if queue is empty.
     */
    public IQiWorkbenchMsg getNextMsg() {
        if (msgHandler.isQueueEmpty()) return null;
        return msgHandler.dequeue();
    }

    /**
     * Peek for next message from message queue. Wait until there is a message
     * on the queue.
     *
     * @return Next message in queue for peeking, the message will remain in the queue; null if queue is empty.
     */
    public IQiWorkbenchMsg peekNextMsg() {
        return msgHandler.peek2();
    }

    /**
     * Get next message from message queue. If the message queue is empty,
     * wait for a message to arrive.
     *
     * @return Next message in queue.
     */
    public IQiWorkbenchMsg getNextMsgWait() {
        // if queue is empty; blocks until message added to queue
        IQiWorkbenchMsg msg = msgHandler.dequeue();
        return msg;
    }

    /**
     * Get response from message queue with the matching message ID. If none,
     * return null.
     *
     * @param msgID Message ID waiting on for a response
     * @return Response message in queue with matching message ID; null if none.
     */
    public IQiWorkbenchMsg getMatchingResponse(String msgID) {
        if (msgHandler.isQueueEmpty()) return null;
        return msgHandler.findResponse(msgID);
    }

    public IQiWorkbenchMsg getMatchingOutstandingRequest(IQiWorkbenchMsg response) {
        return outstandingRequests.findMatchingRequest(response);
    }

    /**
     * Get response from message queue with the matching message ID. If there
     * is none, wait for the matching response to arrive up to the value of
     * QIWConstants.RESPONSE_TIMEOUT (5 minutes).  If no response is available
     * after the this duration has been reached, a null reponse is returned
     * and the request associated with the msgID is REMOVED from the outstanding message
     * queue.  Any subsequent call to {@link getMatchingResponse} or {@link getMatchingResponseWait}
     * will return a NULL response.
     *
     * @param msgID Message ID waiting on for a response
     * 
     * @return Response message in queue with matching message ID; null if
     * response doesn't arrive in specified amount of time.
     */
    public IQiWorkbenchMsg getMatchingResponseWait(String msgID) {
        return getMatchingResponseWait(msgID, QIWConstants.RESPONSE_TIMEOUT, false);
    }
    
    /**
     * Get response from message queue with the matching message ID. If there
     * is none, wait for the matching response to arrive up to the specified
     * duration of time (in milliseconds) expires.  If no response is available
     * after the requested duration has been reached, a null reponse is returned
     * and the request associated with the msgID is REMOVED from the outstanding message
     * queue.  Any subsequent call to {@link getMatchingResponse} or {@link getMatchingResponseWait}
     * will return a NULL response.
     *
     * @param msgID Message ID waiting on for a response
     * @param duration Amount of time in milliseconds to wait for a matching response.
     * @return Response message in queue with matching message ID; null if
     * response doesn't arrive in specified amount of time.
     */
    public IQiWorkbenchMsg getMatchingResponseWait(String msgID, long duration) {
        return getMatchingResponseWait(msgID, duration, false);
    }
    
    /**
     * Get response from message queue with the matching message ID. If there
     * is none, wait for the matching response to arrive up to the specified
     * duration of time (in milliseconds) expires.  If no response is available
     * after the requested duration has been reached, a null reponse is returned
     * and the request associated with the msgID is REMOVED from the outstanding message
     * queue.  Any subsequent call to {@link getMatchingResponse} or {@link getMatchingResponseWait}
     * will return a NULL response.
     *
     * @param msgID Message ID waiting on for a response
     * @param duration Amount of time in milliseconds to wait for a matching response.
     * @param promptForContinue if true, the user will be presented with a JOptionPane
     * asking whether to continue waiting for response after timeout has elapsed
     * 
     * @return Response message in queue with matching message ID; null if
     * response doesn't arrive in specified amount of time.
     */
    public IQiWorkbenchMsg getMatchingResponseWait(String msgID, long duration, boolean promptForContinue) {
        final int MATCH_WAIT_INCR = 100; //minimum wait time is 100ms
        long startTime = System.currentTimeMillis();
    	BooleanSettings setting = new BooleanSettings();

        IQiWorkbenchMsg request = outstandingRequests.findMatchingRequest(msgID);
        if (request == null) {
            logger.warning("Cannot get matching response for msg with ID#" + msgID
                    + " because no such message is currently on the outstanding message queue.");
        } else if (!request.skip()) {

            StringBuilder sbuilder = new StringBuilder();
            sbuilder.append("Call to getMatchingResponseWait(");
            sbuilder.append(msgID);
            sbuilder.append(", ");
            sbuilder.append(duration);
            sbuilder.append(", ");
            sbuilder.append(promptForContinue);
            sbuilder.append(") may fail because the request message has a 'skip' value of false.\n");
            sbuilder.append("Consequently, the waiting qiComponent may consume the response before it is detected by getMatchingResponseWait(...).");

            logger.warning(sbuilder.toString());
            logger.warning("Request message: " + request.toString());
        }
        
        boolean proceed = true;
        while(proceed){
	        while (System.currentTimeMillis() - startTime <= duration) {
	            if (!msgHandler.isQueueEmpty()) {
	                IQiWorkbenchMsg resp = msgHandler.findResponse(msgID);
	                if (resp != null) {
	                    return resp;
	                }
	            }
	            try {
	                Thread.sleep(MATCH_WAIT_INCR);
	            } catch (InterruptedException ie) {
	                logger.warning("Caught InterruptedException while sleeping in MessagingManager.getMatchingResponseWait with duration " 
	                    + duration + " ms. Returning no matching response(null).");
	                return null;
	            }
	        }
        
	        
	        //no response was received, remove the request from the oustanding message list
	        //now when the IMessageHandler receives the response, it can discard the non-matchable message
	        if(!promptForContinue){
	        	logger.info("Removing unmatched request from outstandingRequests queue due to wait time exceeding " + duration + " ms.");
	        	outstandingRequests.removeMatchingRequest(msgID);
	        	proceed = false;
	        }else{
	        	logger.info("Asking user to proceed or not");
	        	logger.info("Show confirm dialog flag (will not show if user enables not ask again)=" + setting.getValue());
	        	
				String text = "The system may need more time to finish the task. Do you want to proceed?";
				int n = CommonUtil.showConfirmDialog(null, text, "Confirm to continue with task?", JOptionPane.YES_NO_OPTION, setting);
	            if(n != JOptionPane.YES_OPTION){
					proceed = false;
					outstandingRequests.removeMatchingRequest(msgID); //remove outstanding request
					return null;
				}
	            startTime = System.currentTimeMillis();
			}
        }
        
        return null;
    }

    /**
     * Explicitly remove a matching request based on the  message ID
     *
     * @param msgID Message ID waiting on for a response
     */
    public void removeMatchingRequest(String msgID) {
        outstandingRequests.removeMatchingRequest(msgID);
    }

    
    
    /**
     * Check if response message is from the Message Dispatcher
     *
     * @param msg QiWorkbench response message
     * @return true if from dispatcher; otherwise, false
     */
    public boolean isResponseFromMsgDispatcher(IQiWorkbenchMsg msg) {
        return msg.getProducerCID().equals(mdCompDesc.getCID());
    }

    /**
     * Check if response message is from the requester of the message.
     *
     * @param msg QiWorkbench response message
     * @param requesterDesc Descriptor of the producer.
     * @return true if from dispatcher; otherwise, false
     */
    public boolean isResponseFromRequester(IQiWorkbenchMsg msg, IComponentDescriptor requesterDesc) {
        return msg.getProducerCID().equals(requesterDesc.getCID());
    }

    /**
     * Check if message is a request (command) message.
     *
     * @param msg QiWorkbench message
     * @return true if a request (command) message; otherwise, false
     */
    public boolean isRequestMsg(IQiWorkbenchMsg msg) {
        return msg.getMsgKind().equals(QIWConstants.CMD_MSG);
    }

    /**
     * Check if message is a response message.
     *
     * @param msg QiWorkbench message
     * @return true if a response message; otherwise, false
     */
    public boolean isResponseMsg(IQiWorkbenchMsg msg) {
        return msg.getMsgKind().equals(QIWConstants.DATA_MSG);
    }

    /**
     * Check if message is a trace (command data) message.
     *
     * @param msg QiWorkbench message
     * @return true if a trace message; otherwise, false
     */
    public boolean isTraceMsg(IQiWorkbenchMsg msg) {
        return msg.getMsgKind().equals(QIWConstants.DATA_CMD_MSG);
    }

    /**
     * Check for a request matching the response. If found, remove from the
     * list of outstanding requests.
     *
     * @param response message
     * @return Original request message if a match; otherwise, a NULL message.
     */
    public IQiWorkbenchMsg checkForMatchingRequest(IQiWorkbenchMsg response) {
        IQiWorkbenchMsg req = outstandingRequests.findMatchingRequest(response);
        if (req == null) {
            logger.severe("Internal error: cannot find matching request for response:"+response.toString());
            logger.severe("Outstanding requests: " + outstandingRequests.toString());
            
            //create dummy message so processing can continue
            return QiWorkbenchMsg.dummyMsg;
        } else {
            outstandingRequests.removeMatchineRequest(response);
            return req;
        }
    }

    /**
     * Get the component descriptor for this registered component, i.e.,
     * the component represented by this messaging manager.
     *
     * @return Component's descriptor if registered; otherwise, null
     */
    public IComponentDescriptor getMyComponentDesc() {
        return compDesc;
    }

    /**
     * Get a registered component's descriptor given its CID.
     *
     * @param cid CID of the component
     * @return Component's descriptor if registered; otherwise, null
     */
     public IComponentDescriptor getComponentDesc(String cid) {
         return MessageDispatcher.getInstance().getComponentDesc(cid);
     }

     /*
      * Get a registered component's decriptor given its display name
      * @param displayname Display name of a registered qiComponent
      * @return IComponentDescriptor or null if not found
      */
     public IComponentDescriptor getComponentDescFromDisplayName(String displayName) {
         String cid = MessageDispatcher.getInstance().getCidByName(displayName);
         return getComponentDesc(cid);
     }

   /**
    * Generate a component's CID given its class.
    *
    * @param compClass Class of component
    * @return Component's CID
    */
   public String genCID(Class compClass) {
       return ComponentUtils.genCID(compClass);
   }

   /**
    * Get job service preference, LOCAL or REMOTE
    * @return QIWConstants.LOCAL_SERVICE_PREF or QIWConstants.REMOTE_SERVICE_PREF
    *
    */
   public String getLocationPref() {
     if(MessageDispatcher.getInstance().getLocationPref().equals(QIWConstants.LOCAL_PREF))
       return QIWConstants.LOCAL_SERVICE_PREF;
     else
       return QIWConstants.REMOTE_SERVICE_PREF;
   }

    public void setLocationPref(String locationPref) {
        MessageDispatcher.getInstance().setLocationPref(locationPref);
    }
      
   /**
    * Get URL where tomcat lives
    * @return Tomcat URL from Msg Dispatcher
    *
    */
   public String getTomcatURL() {
       return MessageDispatcher.getInstance().getTomcatURL();
   }

   public void setTomcatURL(String tomcatURL) {
       MessageDispatcher.getInstance().setTomcatURL(tomcatURL);
   }
      
   /*
    * Rename a component
    * @param oldName Old display name
    * @param newName New display name
    */
   public void renameComponent(String oldName, String newName) {
       MessageDispatcher.getInstance().renameComponent(oldName,newName);
   }

   /* Get project path
    * @return Path of the project selected by the user
    */
   public String getProject() {
       return MessageDispatcher.getInstance().getProject();
   }

   /* Get qiSpace metadata
    * @return qiSpace descriptor
    */
   public QiSpaceDescriptor getQispaceDesc() {
       return MessageDispatcher.getInstance().getQispaceDesc();
   }

   /* Get user home directory
    * @return User's home directory under the OS they are running.
    */
   public String getUserHOME() {
       return MessageDispatcher.getInstance().getUserHOME();
   }

   /* Get the file separator of the OS under which the Tomcat server is executing.
    * @return File separator for the OS under which the Tomcat server is executing.
    */
   public String getServerOSFileSeparator() {
    if (runtimeServerFileSep == null) {
        runtimeServerFileSep = MessageDispatcher.getInstance().getServerOSFileSeparator();
    }
    return runtimeServerFileSep;
   }

   /* Get the line separator of the OS under which the Tomcat server is executing.
    * @return Line separator for the OS under which the Tomcat server is executing.
    */
   public String getServerOSLineSeparator() {
     if (runtimeServerLineSep == null) {
       runtimeServerLineSep = MessageDispatcher.getInstance().getServerOSLineSeparator();
     }
     return runtimeServerLineSep;
   }

   /**
    * Get information about the OS of the remote server.
    *
    * @return List of OS information, namely, name of OS, file separator.
    */
   public ArrayList<String> getRemoteOSInfo(){
       return MessageDispatcher.getInstance().getRemoteOSInfoList();
   }

   /**
    * Get the component parent display name information
    *
    * @return Display name of the parent qiComponent
    */
   public String getRegisteredComponentDisplayNameByDescriptor(IComponentDescriptor desc){
       return WorkbenchManager.getInstance().getRegisteredComponentDisplayNameByDescriptor(desc);
   }

   /**
    * Reset the message queue for the component.
    *
    */
   public void resetMsgQueue(){
       msgHandler.resetQueue();
   }

   public void displayMsgQueue(){
       msgHandler.displayMsgQueue();
   }

   /**
    *  Get the state of a qiComponent.
    *  @param key Which qiComponent
    *  @param name State name
    *  @return qiComponent's state.
    */
   public Object getState(String key, String name) {
       return mdMsgHandler.getState(key, name);

   }

   /**
    *  Save the state of a qiComponent. Only save a empty string at the moment.
    *  @param key  Which qiComponent
    *  @param name State name
    */
   public void saveState(String key, String name) {
       mdMsgHandler.saveState(key, name, compDesc.getCID());
   }

   /**
    *  Save the state of a qiComponent.
    *  @param key  Which qiComponent
    *  @param name State name
    *  @param val State
    */
   public void saveState(String key, String name, Object val) {
       mdMsgHandler.saveState(key, name, val);
   }

   /**
    *  Update the state of a qiComponent.
    *  @param key  Which qiComponent
    *  @param name State name
    *  @param val New state
    */
   public void updateState(String key, String oldName, String newName, Object val) {
       mdMsgHandler.updateState(key, oldName, newName, val);
   }

   /**
    *  Remove the state of a qiComponent
    *  @param key  Which qiComponent
    *  @param name State name
    */
   public synchronized void removeState(String key, String name) {
       mdMsgHandler.removeState(key, name);
   }

   /**
    *  Add a message to a qiComponent's message queue.
    *  @param targetDesc  Consumer of message.
    *  @param msg Message
    */
   public void addMessageTo(IComponentDescriptor targetDesc, IQiWorkbenchMsg msg) {
       targetDesc.getMsgHandler().enqueue(msg);
   }

   /**
    *  Load variables from file if necessary (deserilizing)
    *  @param key Which qiComponent
    *  @param name State name
    *  @param fromFile File containing variables.
    *  @param xStream XML deserializer
    *  @return
    */
   public Object loadFromFile(String key, String name, String fromFile, XStream xStream) {
       Object val = null;
       FileReader reader = null;
       try {
           reader = new FileReader(fromFile);
           val = xStream.fromXML(reader);
           saveState(key, name, val);
       } catch (Exception e) {
            logger.warning("---loadFromFile: failed to read file " + fromFile);
            e.printStackTrace();
       } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {}
            }
        }
       return val;
   }

   /**
    *  Save an serialized object (in XML) to a file.
    *  @param toFile File to save serialized object to.
    *  @param xStream XML serializer
    *  @param val Serialized object (in XML)
    */
   public void saveToFile(String toFile, XStream xStream, Object val) {
       FileWriter writer = null;
       try {
          writer = new FileWriter(toFile);
          xStream.toXML(val, writer);
          writer.flush();
       } catch (Exception e) {
            logger.warning("saveToFile: failed to save file for " + toFile);
       } finally {
            if (writer != null) {
                try {
                    writer.close();
                }catch (Exception e) {}
            }
        }
   }

}
