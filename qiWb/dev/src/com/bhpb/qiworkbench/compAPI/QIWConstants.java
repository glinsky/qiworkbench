/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006-2008, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.compAPI;

import java.awt.datatransfer.DataFlavor;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.QiSpaceDescriptor;
import java.util.HashMap;

/**
 * System wide constants. In particular contains all of the message commands.
 * Also, constants used in the construction and management of messages.
 *
 * @author Gil Hansen
 * @version 1.1
 */
public final class QIWConstants {
    /**
     * Prevent object construction outside of this class.
     */
    private QIWConstants(){}

    // LOCKS
    public static final Lock SYNC_LOCK = new ReentrantLock();

    /** Top level package */
    public static final String TOP_PACKAGE = "com.bhpb.qiworkbench";

    // COMPONENT KINDS
    /** Component is a plugin type */
    public static final String COMPONENT_PLUGIN_TYPE = "Plugin";
    /** Component is a viewer type */
    public static final String COMPONENT_VIEWER_TYPE = "Viewer";
    /** Component is geographics colormap editor agent */
    public static final String COLORMAP_EDITOR_AGENT = "ColormapEditorAgent";
    /** Component is a viewer agent */
    public static final String VIEWER_AGENT_COMP = "viewerAgent";
    /** Component is the visualizer of a viewer agent */
    public static final String VIEWER_VISUALIZER_COMP = "viewerVisualizer";
    /** Component is an agent plugin */
    public static final String PLUGIN_AGENT_COMP = "pluginAgent";
    /** Component is an agent's GUI */
    public static final String PLUGIN_GUI_COMP = "pluginGUI";
    /** Component is a project manager plugin */
    public static final String PLUGIN_PROJMGR_COMP = "pluginProjMgr";
    /** Component is a client-side service */
    public static final String LOCAL_SERVICE_COMP = "localService";
    /** Component is a server-side service */
    public static final String REMOTE_SERVICE_COMP = "remoteService";
    /** Component is a file chooser service module */
    public static final String FILE_CHOOSER_SERVICE_COMP = "fileChooserService";
    /** Component is a framework module */
    public static final String FRAMEWORK_COMP = "frameworkComp";
    /** Component is the workbench manager */
    public static final String WORKBENCH_COMP = "qiWorkbench";
    public static final String WORKBENCH_GUI_COMP = "workbenchGUI";
    /** Component is the workbench error dialog service */
    public static final String ERROR_DIALOG_SERVICE_COMP = "errorDialogService";

    // DISPLAY NAMES FOR CORE COMPONENTS
    //   Viewers
    /** BHP 2D viewer */
    public static final String BHP_VIEWER_NAME = "bhpViewer";
    public static final String QI_VIEWER_NAME = "qiViewer";
    public static final String COLORMAP_EDITOR_NAME = "ColormapEditor";
    public static final String XMLEDITOR_NAME = "xmlEditor";
    /** DownUnder 2D viewer */
//    public static final String DUGEO_VIEWER_NAME = "DUGEO 2D";
    /** G&W Systems 3D viewer */
//    public static final String GWS_VIEWER_NAME = "G&W 3D";
    /** Project Manager */
    public static final String QI_PROJECT_MANAGER_NAME = "qiProjectManager";

    //   Local Services
    /** Local IO Service */
    public static final String LOCAL_IO_SERVICE_NAME = "Local IO";
    /** Read ASCII data from a local file */
    public static final String LOCAL_READ_FILE = "Read Local File";
    /** Write ASCII data to a local file */
    public static final String LOCAL_WRITE_FILE = "Write Local File";
    /** Get local file list */
    public static final String LOCAL_GET_FILE_LIST = "Get Local File List";
    /** Get local directory file list */
    public static final String LOCAL_GET_DIR_FILE_LIST = "Get Local Directory File List";
    /** Read binary data from a local file */
    public static final String  LOCAL_READ_BINARY_FILE = "Read Local Binary File";
    /** Write binary data to a local file */
    public static final String  LOCAL_WRITE_BINARY_FILE = "Write Local Binary File";

    /** Local job processor service */
    public static final String LOCAL_JOB_SERVICE_NAME = "Local Job Processor";
    /** File chooser service name*/
    public static final String FILE_CHOOSER_SERVICE_NAME = "File Chooser Service";
    /** Workbench Error Dialog Service Name */
    public static final String ERROR_DIALOG_SERVICE_NAME = "Error Dialog Service";

    //   Remote Services
    /** IO Service */
    public static final String REMOTE_IO_SERVICE_NAME = "Remote IO";
    /** GeoIO Service */
    public static final String REMOTE_GEOIO_SERVICE_NAME = "Remote GeoIO";
    /** Read text data from a remote file */
    public static final String REMOTE_READ_FILE = "Read Remote File";
    /** Write text data to a remote file */
    public static final String REMOTE_WRITE_FILE = "Write Remote File";
    /** Get remote file list */
    public static final String REMOTE_GET_FILE_LIST = "Get Remote File List";
    /** Get remote directory file list */
    public static final String REMOTE_GET_DIR_FILE_LIST = "Get Remote Directory File List";
    /** Read binary data from a remote file */
    public static final String  REMOTE_READ_BINARY_FILE = "Read Remote Binary File";
    /** Write binary data to a remote file */
    public static final String  REMOTE_WRITE_BINARY_FILE = "Write Remote Binary File";

    /** Job processor service */
    public static final String REMOTE_JOB_SERVICE_NAME = "Remote Job Processor";
    /** Remote job processor service */
    public static final String REMOTE_CLUSTER_JOB_SERVICE_NAME = "Remote Cluster Job Processor";

    //   qiComponents (plugins)
    /** Display name of the QI Workbench Manager */
    public static final String WORKBENCH_MGR_NAME = "QI Workbench";
    /** Display name of the Message Dispatcher */
    public static final String MSG_DISPATCHER_NAME = "Message Dispatcher";
    /** Display name of the Servlet Dispatcher */
    public static final String SERVLET_DISPATCHER_NAME = "Servlet Dispatcher";
    /** Display name of the Workbench State Manager */
    public static final String WORKBENCH_STATE_MANAGER_NAME = "Workbench State Manager";
    /** Display name of a File Chooser Dialog */
    public static final String FILE_CHOOSER_DIALOG_NAME = "File Chooser Dialog";

    // MESSAGES
    //    Message Kinds
    /** Message is a command to be sent to its designated consumer */
    public static final String CMD_MSG = "CMD";
    /** Message is a command to be routed to other agents as determined
    by the Message Dispatcher's routing matrix */
    public static final String CMD_ROUTE_MSG = "CMD_ROUTE";
    /** Message is information to be returned to its designated producer */
    public static final String DATA_MSG = "DATA";
    /** Message is a command to be treated as data, not to be executed */
    public static final String DATA_CMD_MSG = "DATA_CMD";

    //    Content types
    /** Content is a Java String */
    public static final String STRING_TYPE = String.class.getName();
    /** Content is a Java ArrayList */
    public static final String ARRAYLIST_TYPE = ArrayList.class.getName();
    /** Content is a component descriptor */
    public static final String COMP_DESC_TYPE = ComponentDescriptor.class.getName();
    /** Content is a Java Integer */
    public static final String INTEGER_TYPE = Integer.class.getName();
    /** Content is qiSpace */
    public static final String QISPACE_TYPE = QiSpaceDescriptor.class.getName();
    /** Content is HashMap */
    public static final String HASH_MAP_TYPE = HashMap.class.getName();

    // COMMANDS

    // set tomcat
    public static final String SET_TOMCAT = "setTomcat";

    //    Plugin Agent Commands
    /**
     * Get the current project.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> Path of project's directory
     */
    public static final String GET_PROJECT_CMD = "getProject";

    //    Viewer Agent Commands

    //    Common Agent and Client Service Commands
    /** Null command. Does nothing. */
    public static final String NULL_CMD = "null";

    /**
     * Ping a component, usually the Message Dispatcher..
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> Acknowledgment received command.
     */
    public static final String PING_CMD = "ping";

    /**
     * Get the CID of a registered component based on its display name
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Display name of component
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> Component's CID.
     */
    public static final String GET_CID_CMD = "getCID";


    /**
     * Get the user preference information
     * <b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> QiwbPreferences
     * <br><b>Output:</b> user preference object
     */
    public static final String GET_USER_PREFERENCE_CMD = "getUserPreference";

    //    IO Commands
    /**
     * Get those directories which do not exist in the QiProject meta data.
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] an instance of QiProjectDescriptor</li>
     * </ul>
     * <b>Output type:</b> ArrayList of Strings
     * <br><b>Output:</b> List of directory names
     */

    public static final String GET_UNMADE_QIPROJ_DESC_DIRS_CMD = "getUnmadeQiProjDescDirs";

    /**
     * Read text data from a file.
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] Path of file to read</li>
     * </ul>
     * <b>Output type:</b> ArrayList of Strings
     * <br><b>Output:</b> List of lines in file
     */

    public static final String FILE_READ_CMD = "readFile";
    /**
     * Write text data to a file.
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] Path of file to write</li>
     * <li>[2]...[n] Lines to write out</li>
     * </ul>
     * <b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String FILE_WRITE_CMD = "writeFile";

    /**
     * Read binary data from a file.
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] Path of binary file to read</li>
     * <li>[2] offset -- if offset < 0, start from the beginning of the file</li>
     * <li>[3] length -- if the length < 0, take the whole file</li>
     * </ul>
     * <b>Output type:</b> array
     * <br><b>Output:</b> array of bytes
     */
    public static final String BINARY_FILE_READ_CMD = "readBinaryFile";

    /**
     * Write binary data to a file.
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] Path of file to write to</li>
     * <li>[2] file's format</li>
     * <li>[3] Binary data: type indicated by file's format</li>
     * </ul>
     * <b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String BINARY_FILE_WRITE_CMD = "writeBinaryFile";

    /**
     * Get the list of files in a directory..
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] Path of directory</li>
     * </ul>
     * <b>Output type:</b> ArrayList of Strings
     * <br><b>Output:</b> List of files in directory; empty if none
     */
    public static final String GET_FILE_LIST_CMD = "getFileList";

    /** @deprecated */
    public static final String GET_FILENAME_CMD = "getFileName";

    /**
     * Read text data from a local file.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Path of file to read
     * <br><b>Output type:</b> ArrayList of Strings
     * <br><b>Output:</b> List of lines in file
     */
    public static final String LOCAL_FILE_READ_CMD = "readLocalFile";

    /**
     * Write text data to a local file.
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] Path of file to write</li>
     * <li>[1]...[n] Lines to write out</li>
     * </ul>
     * <b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String LOCAL_FILE_WRITE_CMD = "writeLocalFile";

    /**
     * Read binary data from a local file.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Path of file to read
     * <br><b>Output type:</b> OutputStream (ByteArrayOutputStream / ServletOuputStream)
     * <br><b>Output:</b> Output stream
     */
    public static final String LOCAL_BINARY_FILE_READ_CMD = "readLocalBinaryFile";

    /**
     * Write binary data to a local file.
     * <br><b>Input type:</b> ArrayList
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] Path of file to write to</li>
     * <li>[1] file's format</li>
     * <li>[2] Binary data; type indicated by file's format</li>
     * </ul>
     * <b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String LOCAL_BINARY_FILE_WRITE_CMD = "writeLocalBinaryFile";

    /**
     * Get the list of files in a local directory..
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Path of directory
     * <br><b>Output type:</b> ArrayList of Strings
     * <br><b>Output:</b> List of files in directory; empty if none
     */
    public static final String GET_LOCAL_FILE_LIST_CMD = "getLocalFileList";

    /** @deprecated */
    public static final String START_LOCAL_JOB_CMD = "startLocalJob";

    /**
     * Read text data from a remote file.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Path of file to read
     * <b>Output type:</b> ArrayList of Strings
     * <br><b>Output:</b> List of lines in file
     */
    public static final String REMOTE_FILE_READ_CMD = "readRemoteFile";

    /**
     * Write text data to a remote file.
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] Path of file to write</li>
     * <li>[1]...[n] Lines to write out</li>
     * </ul>
     * <b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String REMOTE_FILE_WRITE_CMD = "writeRemoteFile";

    /**
     * Read binary data from a remote file.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Path of file to read
     * <br><b>Output type:</b> OutputStream (ByteArrayOutputStream / ServletOuputStream)
     * <br><b>Output:</b> Output stream
     */
    public static final String REMOTE_BINARY_FILE_READ_CMD = "readRemoteBinaryFile";

    /**
     * Write binary data to a remote file.
     * <br><b>Input type:</b> ArrayList
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] Path of file to write to</li>
     * <li>[1] File's format</li>
     * <li>[2] Initially a reference to the binary data which is extracted
     * and put in the file part of the multipart POST. The Servlet
     * Dispatcher replaces it with the FilePartition instance once it parses * it out of the POST.</li>
     * </ul>
     * <b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String REMOTE_BINARY_FILE_WRITE_CMD = "writeRemoteBinaryFile";

    /**
     * Get the list of files and directories in a local or remote directory..
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Path of directory
     * <b>Output type:</b> ArrayList of Files
     * <br><b>Output:</b> List of files in directory; empty if none
     */
    public static final String GET_DIR_FILE_LIST_CMD = "getDirFileList";

    /**
     * Get the list of files in a remote directory..
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Path of directory
     * <b>Output type:</b> ArrayList of File objects
     * <br><b>Output:</b> List of files in directory; empty if none
     */
    public static final String GET_REMOTE_FILE_LIST_CMD = "getRemoteFileList";

    /**
     * Get the remote OS information (OS name and file seperator)
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <b>Output type:</b> ArrayList of Strings
     * <br><b>Output:</b> List of Strings (OS name and file seperator); empty if none
     */
    public static final String GET_REMOTE_OS_INFO_CMD = "getRemoteOSInfo";

    /**
     * Get the list of files and directories in a remote directory..
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b>
     * <li>Path of directory</li>
     * <b>Output type:</b> ArrayList of Files
     * <br><b>Output:</b> List of files in directory; empty if none
     */
    public static final String GET_REMOTE_DIR_FILE_LIST_CMD = "getRemoteDirFileList";

    /**
     * Get the list of files and directories in a local directory..
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Path of directory
     * <b>Output type:</b> ArrayList of Files
     * <br><b>Output:</b> List of files in directory; empty if none
     */
    public static final String GET_LOCAL_DIR_FILE_LIST_CMD = "getLocalDirFileList";


    /**
     * Check to see if the given file exist
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] Path of file</li>
     * </ul>
     * <b>Output type:</b> Boolean
     * <br><b>Output:</b> true or false
     */
    public static final String CHECK_FILE_EXIST_CMD = "checkFileExist";

    /**
     * Copy a given source file to a new destination file
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] Path of file - source file</li>
     * <li>[2] Path of file - destination file</li>
     * </ul>
     * <b>Output type:</b> String
     * <br><b>Output:</b> status of action
     */
    public static final String COPY_FILE_CMD = "copyFile";

    /**
     * Move a given file to a new file name; only work for file only
     * directory type of file is not supported
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] Path of file - source file</li>
     * <li>[2] Path of file - destination file</li>
     * <li>[3] flag -f if specified, file overwrite will be enforced if name exists.</li>
     * </ul>
     * <b>Output type:</b> String
     * <br><b>Output:</b> status of action
     */
    public static final String MOVE_FILE_CMD = "moveFile";
    /**
     * Create the given directory
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] Path of directory</li>
     * </ul>
     * <b>Output type:</b> Boolean
     * <br><b>Output:</b> true or false
     */
    public static final String CREATE_DIRECTORY_CMD = "createDirectory";
    /**
     * Check to see if the given file exist in the local file system.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Path of file
     * <br><b>Output type:</b> Boolean
     * <br><b>Output:</b> true or false
     */
    public static final String CHECK_LOCAL_FILE_EXIST_CMD = "checkLocalFileExist";

    /**
     * Copy the given file to a new name in the local file system.
     * <br><b>Input type:</b> List
     * <br><b>Input parameter:</b> List of file path String
     * <ul>
     *     <li>[0] String: The base file for copy</li>
     *     <li>[1] String: The new file name</li>
     * </ul>
     * <br><b>Output type:</b> Boolean
     * <br><b>Output:</b> success or failure
     */
    public static final String COPY_LOCAL_FILE_CMD = "copyLocalFile";

    /**
     * Copy the given file to a new name in the local file system.
     * <br><b>Input type:</b> List
     * <br><b>Input parameter:</b> List of file path String
     * <ul>
     *     <li>[0] String: The base file for copy</li>
     *     <li>[1] String: The new file name</li>
     * </ul>
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> status of action
     */
    public static final String COPY_REMOTE_FILE_CMD = "copyRemoteFile";

    /**Move a given file to a new file name; only work for file type only
     * directory type of file is not supported
     * <br><b>Input type:</b> List
     * <br><b>Input parameter:</b> List of file path String
     * <ul>
     *     <li>[0] String: The file to be renamed</li>
     *     <li>[1] String: The new name</li>
     *     <li>[3] flag -f if specified, file overwrite will be enforced if name exists.</li>
     * </ul>
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> status of the action
     */
    public static final String MOVE_LOCAL_FILE_CMD = "moveLocalFile";

    /**Move a given file to a new file name; only work for file type only
     * directory type of file is not supported
     * <br><b>Input type:</b> List
     * <br><b>Input parameter:</b> List of file path String
     * <ul>
     *     <li>[0] String: The file to be renamed</li>
     *     <li>[1] String: The new name</li>
     *     <li>[3] flag -f if specified, file overwrite will be enforced if name exists.</li>
     * </ul>
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> status of the action
     */
    public static final String MOVE_REMOTE_FILE_CMD = "moveRemoteFile";
    /**
     * Delete the given file in the local file system.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> file path String
     * <br><b>Output type:</b> Boolean
     * <br><b>Output:</b> success or failure
     */
    public static final String DELETE_LOCAL_FILE_CMD = "deleteLocalFile";

    /**
     * Create the given directory in the local file system.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Path of directory
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> sucess or IO Exception message
     */
    public static final String CREATE_LOCAL_DIRECTORY_CMD = "createLocalDirectory";

    /**
     * Create the given directory in the remote file system.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Path of directory
     * <br><b>Output type:</b> Boolean
     * <br><b>Output:</b> true or false
     */
    public static final String CREATE_REMOTE_DIRECTORY_CMD = "createRemoteDirectory";

    /**
     * Create the given directory list in the file system.
     * <br><b>Input type:</b> ArrayList
     * <br><b>Input parameter:</b> List of directory path
     * <br><b>Output type:</b> Boolean
     * <br><b>Output:</b> true or false
     */
    public static final String CREATE_DIRECTORIES_CMD = "createDirectories";

    /**
     * Check to see if the given file exist in the remote file system.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Path of file
     * <br><b>Output type:</b> Boolean
     * <br><b>Output:</b> true or false
     */
    public static final String CHECK_REMOTE_FILE_EXIST_CMD = "checkRemoteFileExist";

    /**
     * Read ASCII data from a file.
     * <br><b>Input type:</b> String
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] Path of directory to find</li>
     * </ul>
     * <b>Output type:</b> Boolean
     * <br><b>Output:</b> true or false
     */
    public static final String FIND_DIRECTORY_CMD = "findDirectory";
    /**
     * Get the component descriptor from the file chooser service pool.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <b>Output type:</b> ComponentDescriptor
     * <br><b>Output:</b> ComponentDescriptor object.
     */
    public static final String GET_FILE_CHOOSER_SERVICE_CMD = "getFileChooserService";

    /**
     * Get the result from the file chooser.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <b>Output type:</b> ArrayList
     * <br><b>Output parameter:</b>
     *   <ul>
     *     <li>[0] Integer: return code: either JFileChooser.APPROVE_OPTION or JFileChooser.CANCEL_OPTION</li>
     *     <li>[1] String: selected file path by user</li>
     *     <li>[2] String: chooser type currently either Open or Save</li>
     *     <li>[3] String: original message command or action used for request</li>
     *   </ul>
     */
    public static final String GET_RESULT_FROM_FILE_CHOOSER_CMD = "getResultFromFileChooser";

    /**
     * Get the result from the file chooser.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <b>Output type:</b> ArrayList
     * <br><b>Output parameter:</b>
     * An instance of QiFileChooserDescriptor
     */
    public static final String GET_RESULT_FROM_QI_FILE_CHOOSER_CMD = "getResultFromQiFileChooser";


    /**
     * Return the component descriptor back to the file chooser service pool.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <b>Output type:</b> ArrayList
     * <br><b>Output:</b> ArrayList of FileChooserService ComponentDescriptor.
     */
    public static final String LOAD_JNI_LIBRARY_CMD = "loadJNILibrary";

    /**
     * Return the component descriptor back to the file chooser service pool.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <b>Output type:</b> ArrayList
     * <br><b>Output:</b> ArrayList of FileChooserService ComponentDescriptor.
     */
    public static final String RETURN_FILE_CHOOSER_SERVICE_CMD = "returnFileChooserService";

    /**
     * Invoke the file chooser.
     * <br><b>Input type:</b> ArrayList
     * <br><b>Input parameter:</b>
     *   <ul>
     *     <li>[0] Component: Parent GUI component</li>
     *     <li>[1] String: File Chooser Dialog title</li>
     *     <li>[2] ArrayList: List containing a starting directory name to start and a flag to use a previously memorized directory or not </li>
     *     <li>[3] FileFilter: File filter (default All)</li>
     *     <li>[4] boolean: flag for upward/downward navigation between project directory (default true)</li>
     *     <li>[5] producer class type: producer thread which issue the request</li>
     *     <li>[6] String: type of file chooser currently Open or Save</li>
     *     <li>[7] String: original message command or action used for request</li>
     *     <li>[8] String: default text to be prefixed on the file name when performing save request</li>
     *     <li>[9] String: text as default extension of the file name when performing save request</li>
     *     <li>[10] String: server url to access remotely for file choosing service</li>
     *   </ul>
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> file chooser dialog becomes visible
     */
    public static final String INVOKE_FILE_CHOOSER_CMD = "invokeFileChooser";

    /**
     * Invoke the QI file chooser.
     * <br><b>Input type:</b> QiFileChooserDescriptor
     * <br><b>Input parameter:</b>
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> QI file chooser dialog becomes visible
     */
    public static final String INVOKE_QI_FILE_CHOOSER_CMD = "invokeQiFileChooser";

    // Error Dialog Service Commands
    /**
     * Get the component descriptor from the error dialog service pool.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <b>Output type:</b> ComponentDescriptor
     * <br><b>Output:</b> ComponentDescriptor object.
     */
    public static final String GET_ERROR_DIALOG_SERVICE_CMD = "getErrorDialogService";
    /**
     * Invoke the error dialog service.
     * <br><b>Input type:</b> ArrayList
     * <br><b>Input parameter:</b>
     *   <ul>
     *     <li>[0] Component: Parent GUI component</li>
     *     <li>[1] String: ERROR_DIALOG  or WARNING_DIALOG</li>
     *     <li>[2] String: display name of caller</li>
     *     <li>[3] StackTraceElement[]: caller stack trace</li>
     *     <li>[4] String: one-line error message</li>
     *     <li>[5] String[]: list of possible causes</li>
     *     <li>[6] String[]: list of suggested remedies</li>
     *   </ul>
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> error dialog becomes visible
     */
    public static final String INVOKE_ERROR_DIALOG_SERVICE_CMD = "invokeErrorDialogService";
    /**
     * Return the component descriptor back to the error dialog service pool.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <b>Output type:</b> ArrayList
     * <br><b>Output:</b> ArrayList of FileChooserService ComponentDescriptor.
     */

    public static final String RETURN_ERROR_DIALOG_SERVICE_CMD = "returnErrorDialogService";

    //    Job Commands
    /**
     * Submit a program or script to be executed by the OS..
     * Added a final 'doBlock' parameter, which determines whether or not the jobAdapter
     * will wait until the job's stdout and stderr streams have been completely digested
     * before returning.  For long scripts, this parameter should be 'false'. For others,
     * such as 'ps' which MUST return output, it should be 'true'.  If this parameter is omitted
     * or the Nth parameter cannot be parsed as a Boolean, the jobAdapter WILL block.
     * 
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] OS location preference - local or remote</li>
     * <li>[1]...[n-1] Program/script to be invoked and its arguments, if any</li>
     * <li>[n] doBlock - "true" or "false"</li>
     * </ul>
     * <b>Output type:</b> String
     * <br><b>Output:</b> Unique job ID
     */

    public static final String SUBMIT_JOB_CMD = "submitJob";

    /**
     * Wait for a submitted job to terminate..
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] OS location preference - local or remote</li>
     * <li>[1] Job ID</li>
     * </ul>
     * <b>Output type:</b> Integer
     * <br><b>Output:</b> Status code - 0 if terminated normally; otherwise >0
     */
    public static final String WAIT_FOR_JOB_EXIT_CMD = "waitForJobTermination";

    /**
     * Create a job process...
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> OS location preference - local or remote
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> Unique job ID
     */
    public static final String CREATE_JOB_CMD = "createJob";

    /**
     * Execute previously specified program/script...
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] OS location preference - local or remote</li>
     * <li>[1] Job ID</li>
     * </ul>
     * <b>Output type:</b> String
     * <br><b>Output:</b> Unique job ID
     */
    public static final String EXECUTE_JOB_CMD = "executeJob";

    /**
     * Release job.
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] OS location preference - local or remote</li>
     * <li>[1] Job ID</li>
     * </ul>
     * <b>Output type:</b> ArrayList of Strings
     * <br><b>Output:</b>
     * <ul>
     * <li>OS location - local or remote</li>
     * <li>CID of job service</li>
     * <li>Job ID</li>
     * </ul>
     */
    public static final String RELEASE_JOB_CMD = "releaseJob";

    /**
     * Get the status of a job executed by the OS.
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] OS location preference - local or remote</li>
     * <li>[1] Job ID</li>
     * </ul>
     * <b>Output type:</b> Integer
     * <br><b>Output:</b> Status code - 0 if terminated; otherwise >0
     */
    public static final String GET_JOB_STATUS_CMD = "getJobStatus";

    /**
     * Get terminated job's standard output.
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] OS location preference - local or remote</li>
     * <li>[1] Job ID</li>
     * </ul>
     * <b>Output type:</b> ArrayList of Strings
     * <br><b>Output:</b> Lines of output
     */
    public static final String GET_JOB_OUTPUT_CMD = "getJobStdOut";

    /**
     * Get terminated job's standard error..
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] OS location preference - local or remote</li>
     * <li>[1] Job ID</li>
     * </ul>
     * <b>Output type:</b> ArrayList of Strings
     * <br><b>Output:</b> Lines of output
     */
    public static final String GET_JOB_ERROR_OUTPUT_CMD = "getJobStdErr";

    /**
     * Set the command (program or script) to be executed by the OS..
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] OS location preference - local or remote</li>
     * <li>[1] Job ID</li>
     * <li>[2]...[n] Program/script to be invoked and its arguments, if any</li>
     * </ul>
     * <b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String SET_JOB_COMMAND_CMD = "setJobCommand";

    /**
     * Set the job's working directory.
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] OS location preference - local or remote</li>
     * <li>[1] Job ID</li>
     * <li>[2] Path of working directory</li>
     * </ul>
     * <b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String SET_JOB_WORKING_DIR_CMD = "setJobWorkingDir";

    /**
     * Set the job's working directory.
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] OS location preference - local or remote</li>
     * <li>[1] Job ID</li>
     * <li>[2] Name of environment variable</li>
     * </ul>
     * <b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String SET_JOB_ENV_VAR_CMD = "setJobEnvVar";

    /**
     * Kill a job..
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] OS location preference - local or remote</li>
     * <li>[1] Job ID</li>
     * </ul>
     * <b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String KILL_JOB_CMD = "killJob";

    /** @deprecated */
    public static final String START_REMOTE_JOB_CMD = "startRemoteJob";

    /** Execute a cluster job. NOT IMPLEMENTED YET*/
    public static final String START_CLUSTER_JOB_CMD = "startClusterJob";

    //    Message Dispatcher Commands
    /**
     * Request the Servlet Dispatcher to register itself.
     * <br><b>Input type:</b> ComponentDescriptor
     * <br><b>Input parameter:</b> Servlet Dispatcher's component descriptor
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> Acknowledgment it registered itself.
     */
    public static final String REGISTER_SELF_CMD = "registerSelf";

    /**
     * Request a registered component to invoke itself.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A. Note: The CID of the component is contained in the target consumer of the request message.
     * <br><b>Output type:</b> Class name of the component
     * <br><b>Output:</b> JInternalFrame instance if component has a GUI.
     */
    public static final String INVOKE_SELF_CMD = "invokeSelf";

    /**
     * Request the Workbench Manager add a component node to its component tree.
     * <br><b>Input type:</b> ArrayList
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] Type of component</li>
     * <li>[1] Display name of component</li>
     * <li>[2] CID of parent node</li>
     * </ul>
     * <b>Output type:</b> N/A
     * <br><b>Output:</b> N/A.
     */
    public static final String ADD_COMPONENT_NODE_CMD = "addComponentNode";

    /**
     * Request the Workbench Manager remove a component node from its component tree.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> CID of the component whose node is to be removed
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A.
     */
    public static final String REMOVE_COMPONENT_NODE_CMD = "removeComponentNode";

    //    QI Workbench Manager Commands
    /**
     * Ping Servlet Dispatcher. Used to locate a Tomcat server that is running.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> URL of a potential Tomcat server
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A Note: Will get a normal response if ping succeds; otherwise, an abnormal response.
     */
    public static final String PING_SERVER_CMD = "pingServer";

    /**
     * Get the content of the server's qiWbconfig.xml file
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> ArrayList of lines of XML
     * <br><b>Output:</b> Content of qiWbconfig.XML file
     */
    public static final String GET_SERVER_CONFIG_CMD = "serverConfig";

    /**
     * Get the list of available client services.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> ArrayList of ComponentDescriptors
     * <br><b>Output:</b> List of available client services.
     */
     public static final String GET_LOCAL_SERVICES_CMD = "getLocalServiceList";

    /**
     * Get the list of server services.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> ArrayList of ComponentDescriptors
     * <br><b>Output:</b> List of available server services.
     */
    public static final String GET_REMOTE_SERVICES_CMD = "getRemoteServiceList";

    /**
     * Get the list of pre-registered qiComponents (plugins).
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> ArrayList of ComponentDescriptors
     * <br><b>Output:</b> List of registered plugins.
     */
    public static final String GET_PLUGINS_CMD = "getRegisteredPluginList";

    /**
     * Get the list of pre-registered viewer agents.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> ArrayList of ComponentDescriptors
     * <br><b>Output:</b> List of core viewers.
     */
    public static final String GET_VIEWER_AGENTS_CMD = "getPreregisteredViewerList";

    /**
     * Activate a qiComponent.
     * <br><b>Input type:</b> String or Node
     * <ul>
     * <li>String: activate a brand new component </li>
     * <li>Node:  restore a component </li>
     * </ul>
     * <br><b>Input parameter:</b> Display name of plugin or the XML tree child node representing the component to be restored.
     * <br><b>Output type:</b> ComponentDescriptor
     * <br><b>Output:</b> Component descriptor of plugin.
     */
    public static final String ACTIVATE_COMPONENT_CMD = "activateComponent";

    /**
     * Activate a plugin component.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Display name of plugin.
     * <br><b>Output type:</b> ComponentDescriptor
     * <br><b>Output:</b> Component descriptor of plugin.
     */
    public static final String ACTIVATE_PLUGIN_CMD = "activatePlugin";

    /**
     * Check if a qiComponent is trusted.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> component name.
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> "yes" or "no" indicating if user grants it or not
     */
    public static final String CHECK_COMPONENT_SECURITY_CMD = "checkComponentSecurity";

    /**
     * Set a preferred display name for a qiComponent.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> preferred display name of a component.
     * <br><b>Output type:</b> ComponentDescriptor
     * <br><b>Output:</b> Component descriptor of plugin.
     */
    public static final String SET_PREFERRED_DISPLAY_NAME_CMD = "setPreferredDisplayName";
    /**
     * Activate a viewer agent component.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Display name of viewer.
     * <br><b>Output type:</b> ComponentDescriptor
     * <br><b>Output:</b> Component descriptor of viewer.
     */
    public static final String ACTIVATE_VIEWER_AGENT_CMD = "activateViewerAgent";

    /**
     * Remove a qiComponent from the saved set.
     * <br><b>Input type:</b> ComponentDescriptor
     * <br><b>Input parameter:</b> Component descriptor of plugin.
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String REMOVE_COMPONENT_CMD = "removeComponent";

    /**
     * Remove  a plugin component.
     * <br><b>Input type:</b> ComponentDescriptor
     * <br><b>Input parameter:</b> Component descriptor of plugin.
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String REMOVE_PLUGIN_CMD = "deactivatePlugin";

    /**
     * Deactivate a plugin component.
     * <br><b>Input type:</b> ComponentDescriptor
     * <br><b>Input parameter:</b> Component descriptor of plugin.
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String DEACTIVATE_PLUGIN_CMD = "deactivatePlugin";

    /**
     * Deactivate a component.  Set the component UI to be invisible if is exists and deactivate the component thread
     * <br><b>Input type:</b> ComponentDescriptor
     * <br><b>Input parameter:</b> Component descriptor
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String DEACTIVATE_COMPONENT_CMD = "deactivateComponent";

    /**
     * Close a component UI. Component thread still live for activation.
     * <br><b>Input type:</b> ComponentDescriptor
     * <br><b>Input parameter:</b> Component descriptor
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String CLOSE_COMPONENT_GUI_CMD = "closeComponentGUI";

    /**
     * Open a component UI.
     * <br><b>Input type:</b> ComponentDescriptor
     * <br><b>Input parameter:</b> Component descriptor
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String OPEN_COMPONENT_GUI_CMD = "openComponentGUI";

    /**
     * Remove a viewer agent component.
     * <br><b>Input type:</b> ComponentDescriptor
     * <br><b>Input parameter:</b> Component descriptor of viewer.
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String REMOVE_VIEWER_AGENT_CMD = "deactivateViewerAgent";

    /**
     * Deactivate a viewer agent component.
     * <br><b>Input type:</b> ComponentDescriptor
     * <br><b>Input parameter:</b> Component descriptor of viewer.
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String DEACTIVATE_VIEWER_AGENT_CMD = "deactivateViewerAgent";

    /**
     * Serialize a qiComponent.
     * <br><b>Input type:</b> ComponentDescriptor
     * <br><b>Input parameter:</b> Component descriptor of target component.
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> XML String containing state information of the target component
     */
    public static final String SAVE_COMP_CMD = "saveComp";
    /**
     * Close a component UI if the component is already in the saved set (existing); Deactivate the component
     * if it is a new component
     * <br><b>Input type:</b> ComponentDescriptor
     * <br><b>Input parameter:</b> Component descriptor of target component.
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String QUIT_COMPONENT_CMD = "quitComponent";
    /**
     * Serialize a component then quit the component.
     * <br><b>Input type:</b> ComponentDescriptor
     * <br><b>Input parameter:</b> Component descriptor of target component.
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> XML String containing state information of the target component
     */
    public static final String SAVE_COMP_THEN_QUIT_CMD = "saveCompThenQuit";
    /**
     * Serialize a component.
     * <br><b>Input type:</b> ComponentDescriptor
     * <br><b>Input parameter:</b> Component descriptor of target component.
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> XML String containing exact state information of the
     * <br>target component to be saved in the saved set with the display name as CopyOfDisplayName
     */
    public static final String SAVE_COMP_AS_CLONE_CMD = "saveCompAsClone";
    /**
     * Serialize a desktop.
     * <br><b>Input type:</b> List
     * <br><b>Input parameter:</b> List of active components
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> XML String containing state information of the desktop
     */
    public static final String SAVE_DESKTOP_CMD = "saveDesktop";

    /**
     * Serialize a desktop, then quit the workbench.
     * <br><b>Input type:</b> List
     * <br><b>Input parameter:</b> List of active components
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> XML String containing state information of the desktop
     */
    public static final String SAVE_DESKTOP_THEN_QUIT_CMD = "saveDesktopThenQuit";
    /**
     * Save workbench gui related states such as which component is open or closed.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> XML String containing state information of the desktop
     */
    public static final String SAVE_WORKBENCH_STATES_CMD = "saveWorkbenchStates";

    /**
     * Restore a component.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> Compontent
     * <br><b>Output:</b> Component object
     */
    public static final String RESTORE_COMP_CMD = "restoreComp";

    /**
     * Restore the whole desktop.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String RESTORE_DESKTOP_CMD = "restoreDesktop";
    /**
     * Restore the default desktop.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> default desktop file path specified in the qiwbPrefferrences.xml
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String RESTORE_DEFAULT_DESKTOP_CMD = "restoreDefaultDesktop";

    /**
     * Get active plugins.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> HashMap
     * <br><b>Output:</b>  HashMap<String, ComponentDescriptor>
     * @deprecated Never used
     */
    public static final String GET_ACTIVE_PLUGINS_CMD = "getActivePlugins";

    /**
     * Get active viewers.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> HashMap
     * <br><b>Output:</b>  HashMap<String, ComponentDescriptor>
     * @deprecated Never used
     */
    public static final String GET_ACTIVE_VIEWERS_CMD = "getActiveViewers";

    /**
     * Get active plugins.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> HashMap
     * <br><b>Output:</b>  HashMap<String,ComponentDescriptor>
     * @deprecated Never used
     */
    public static final String ADD_ACTIVE_PLUGIN_CMD = "addActivePlugin";

    /**
     * Add active viewer.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> HashMap
     * <br><b>Output:</b>  HashMap<String,ComponentDescriptor>
     * @deprecated Never used
     */
    public static final String ADD_ACTIVE_VIEWER_CMD = "addActiveViewer";

    /**
     * Get a component descriptor.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> component's display name
     * <br><b>Output type:</b> ComponentDescriptor type
     * <br><b>Output:</b> ComponentDescriptor object
     */
    public static final String GET_COMP_DESC_CMD = "getComponentDescriptor";

    /**
     * Get the workbench GUI.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> object type
     * <br><b>Output:</b> GUI object
     */
    public static final String GET_WORKBENCH_GUI_CMD = "getWorkbenchGUI";

    /**
     * Get the viewer GUI.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> object type
     * <br><b>Output:</b> GUI object
     */
    public static final String GET_VIEWER_GUI_CMD = "getViewerGUI";

    /**
     * Get the Manifests of the core components..
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> ArrayList of hash maps
     * <br><b>Output:</b>
     * <ul>
     *    <li>[0] HashMap of jar file names</li>
     *    <li>[1] hashMap of manifests</li>
     * </ul>.
     */
    public static final String GET_CORE_MANIFESTS_CMD = "getCoreManifests";

    /**
     * Set an alternative name for the component.
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> String
     * <br><b>Output:</b>  a new name
     *
     */
    public static final String RENAME_COMPONENT_CMD = "renameComponent";

    /*
     * Rename a component (plugin or viewer)
     *
     */
    public static final String RENAME_COMPONENT = "renameComponent";

    /*
     * Save qiSpace command, sent by WorkbenchManager at startup and when "Rename Desktop" is selected
    */
    public static final String SAVE_QISPACE_CMD = "saveQispace";

    //    Servlet Dispatcher Commands

    // TOMCAT SERVER
    /** Root of the application under the Tomcat server */
    public static final String APP_ROOT = "qiWorkbench";
    /** Timeout in milliseconds for a response from the server */
    public static final int RESPONSE_TIMEOUT = 300000;
    /** Port */
    public static final String SERVER_PORT = "8080";

    // PLATFORM OPERATING SYSTEMS
    public static final String WINDOWS_OS = "Windows";
    public static final String UNIX_OS = "Unix";
    public static final String LINUS_OS = "Linux";
    public static final String IMAC_OS = "iMac";

    // PREFERENCES
    /** Run a service on the user's machine */
    public static final String LOCAL_SERVICE_PREF = "localService";
    /** Run a service on a machine other than the user's */
    public static final String REMOTE_SERVICE_PREF = "remoteService";
    // Tomcat location
    /** Tomcat running on user's machine */
    public static final String LOCAL_PREF = "localLoc";
    /** Tomcat running on a machine other than the user's */
    public static final String REMOTE_PREF = "remoteLoc";

    // COMPONENT ATTRIBUTES (in jar file's Manifest)
    /** Name of section containing the attributesw */
    public static final String COMP_ATTR_SECTION_NAME = "attributes";
    public static final String COMPONENT_TYPE_ATTR = "Component-Type";
    public static final String DISPLAY_NAME_ATTR = "Display-Name";
    public static final String DESCRIPTION_ATTR = "Description";
    public static final String DEPENDENT_JARS_ATTR = "Dependent-Jars";
    public static final String DEPENDENT_JNI_JARS_ATTR = "Dependent-JNI-Jars";
    public static final String TARGET_PLATFORM_ATTR = "Target-Platform";
    public static final String DOWNLOAD_URL_ATTR = "Download-URL";
    public static final String PROVIDER_ATTR = "Provider";
    public static final String VERSION_ATTR = "Version";
    public static final String PROJECT_MANAGER_ATTR = "Project-Manager";

    //Owner name who will maintain the current directory for the file chooser service
    public static final String WORKBENCH_STATE_MANAGER = "WorkBenchStateManager";

    //File Chooser Service related name constants
    public static final String FILE_CHOOSER_TYPE_SAVE = "Save";
    public static final String FILE_CHOOSER_TYPE_OPEN = "Open";
    //action request for file chooser service
    public static final String OPEN_SESSION_ACTION_BY_2D_VIEWER = "OpenSessionActionBy2DViewer";
    public static final String SAVE_MOVIE_VIDEO_ACTION_BY_2D_VIEWER = "SaveMovieVideoActionBy2DViewer";
    public static final String SAVE_MOVIE_IMAGE_ACTION_BY_2D_VIEWER = "SaveMovieImageActionBy2DViewer";
    public static final String EXPORT_COMP_STATE = "ExportComponentState";
    public static final String IMPORT_COMP_STATE = "ImportComponentState";

    // FILE FORMATS
    public static final String AVI_FORMAT = "video_avi";
    public static final String HORIZON_FORMAT = "geo_horz";
    public static final String JPEG_FORMAT = "image_jpeg";
    public static final String TEXT_FORMAT = "text";
    public static final String SEGY_FORMAT = "geo_segy";
    public static final String SEISMIC_FORMAT = "geo_seismic";

    // Error/Warning Dialog Types used by the ErrorDialog class
    public static final String ERROR_DIALOG = "error_dialog";
    public static final String WARNING_DIALOG = "warning_dialog";

    //Loading Messages
    // Deprecated. Data managed by a qiProjectManager
//    public static final String LOADING_PROJECT_DATA = "Busy analyzing project data which can take upwards of a minute.";

    //Error Messages
    public static final String ERROR_LOADING_TOMCAT = "Either the Tomcat server's URL is invalid or the specified Tomcat server is down. Click OK to enter the URL again.";

    // MISCELANEOUS
    /** Directory of the external qiComponents in the user's home directory */
    public static final String COMP_CACHE_DIR = "qicomponents";
    /** Root of qiWorkbench working directory in the user's home directory. */
    public static final String QIWB_WORKING_DIR = ".qiworkbench";
    /** Class name of the Servlet Dispatcher */
    public static final String SERVLET_DISPATCHER_CLASS = "com.bhpb.qiworkbench.server.ServletDispatcher";
    /** HttpClient transport error */
    public static final String HTTP_CONNECTION_REFUSED = "Connection refused: connect";
    /** File suffix for a state/session file */
    public static final String STATE_FILE_SUFFIX = ".cfg";
    /** Name of preference file */
    public static final String PREF_FILE_NAME = "qiwbPreferences.xml";
    public static final String COMPONENT_INVOKE_NEW = "ComponentInvokeSelfNew";
    public static final String COMPONENT_INVOKE_RESTORED = "ComponentInvokeSelfRestored";
    public static final String COMPONENT_VERSION_PROPERTIES = "version.properties";
    public static final String USAGE_LOG_FILE = "usage.log";
    /** End-of-Stream. A valid IEEE NaN which is not a valid trace value */
    public static final int EOS = 0xFFBBBBBC;
    public static final byte EOS_BYTE1 = (byte)0xFF;
    public static final byte EOS_BYTE2 = (byte)0xBB;
    public static final byte EOS_BYTE3 = (byte)0xBB;
    public static final byte EOS_BYTE4 = (byte)0xBC;
    public static final char EOS_CHAR1 = (char)0xFFBB;
    public static final char EOS_CHAR2 = (char)0xBBBC;

    // BHPVIEWER SEISMIC IO COMMANDS

    /** Content is a Java ByteBuffer */
    public static final String BYTEBUFFER_TYPE = ByteBuffer.class.getName();

    /** Read data from a local SEGY file */
    public static final String  LOCAL_READ_SEGY_DATA = "Read Local Segy Data";

    /** Read data from a remote SEGY file */
    public static final String  REMOTE_READ_SEGY_DATA = "Read Remote Segy Data";

    /** Segy file chooser command */
    public static final String OPEN_SEGY_DATA_BY_2D_VIEWER = "OpenSegyDataBy2DViewer";

    /**
     * Read data from a SEGY file.
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] Action: readReq=[readInfo | startRead | readTrace | endRead]
     * <li>[2]... Parameters: param1=value1 ...
     * </ul>
     * <b>Output type:</b> ByteBuffer
     * <br><b>Output:</b> Byte buffer containing the data requested
     */
    public static final String READ_SEGY_DATA_CMD = "readSegyData";

    /**
     * Read data from a remote SEGY file.
     * <br><b>Input type:</b> String
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] Path of SEGY file to read</li>
     * <li>[1] Action: readReq=[readInfo | startRead | readTrace | endRead]
     * <li>[2]... Parameters: param1=value1 ...
     * </ul>
     * <b>Output type:</b> ArrayList of Strings
     * <br><b>Output:</b>
     * <ul>
     * <li>[0] URL of remote Tomcat server</li>
     * <li>[1] socket port (integer) remote IO service will write data to</li>
     * </ul>
     * The ServletDispatcher creates a SocketChannel and reads
     * the data into a ByteBuffer, which is returned in the response.
     */
    public static final String REMOTE_READ_SEGY_DATA_CMD = "readRemoteSegyData";

    /**
     * Read data from a local SEGY file.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Path of file to read
     * <br><b>Output type:</b> OutputStream (ByteArrayOutputStream / ServletOuputStream)
     * <br><b>Output:</b> Output stream
     * <br/><br/>Note: bhpViewer does its own local SEGY read. This command
     * will be implemented when generic SIO is implemented, i.e., ISF.
     */
    public static final String LOCAL_READ_SEGY_DATA_CMD = "readLocalSegyData";

    /** Read data from a local BHP-SU file set. */
    public static final String  LOCAL_READ_BHPSU_DATA = "Read Local BhpSu Data";

    /** Read data from a remote BHP-SU file set.*/
    public static final String  REMOTE_READ_BHPSU_DATA = "Read Remote BhpSu Data";

    /** BhpViewer file chooser commands */
    public static final String OPEN_X_SECTION = "OpenXsection";
    
    /** New QIVIEWER file chooser / ControlSessionManager commands */
    /** Separate commands to read map/xsec properties and create map/xsec viewers/windows
        are no longer necessary */
    public static final String ADD_LAYER = "AddLayer";
    public static final String OPEN_WINDOW = "OpenWindow";
    
    /**
     * Read data from a BHP-SU file set.
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] Action: readReq=[startRead | readTrace | endRead]
     * <li>[2]... Parameters: param1=value1 ...
     * </ul>
     * <b>Output type:</b> ByteBuffer
     * <br><b>Output:</b> Byte buffer containing the data requested
     */
    public static final String READ_BHPSU_DATA_CMD = "readBhpsuData";

    /**
     * Read data from a remote BHP-SU file set.
     * <br><b>Input type:</b> ArrayList of Strings
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] Path of BHP-SU file to read, i.e., .dat file</li>
     * <li>[1] Action: readReq=[startRead | readTrace | endRead]
     * <li>[2]... Parameters: param1=value1 ...
     * </ul>
     * <b>Output type:</b> ArrayList of Strings
     * <br><b>Output:</b>
     * <ul>
     * <li>[0] URL of remote Tomcat server</li>
     * <li>[1] socket port (integer) remote IO service will write data to</li>
     * </ul>
     * The ServletDispatcher creates a SocketChannel and reads
     * the data into a ByteBuffer, which is returned in the response.
     */
    public static final String REMOTE_READ_BHPSU_DATA_CMD = "readRemoteBhpsuData";

    /**
     * Read data from a local BHP-SU file set.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Path of file to read
     * <br><b>Output type:</b> OutputStream (ByteArrayOutputStream / ServletOuputStream)
     * <br><b>Output:</b> Output stream
     * <br/><br/>Note: bhpViewer does its own local BHP-SU read. This command
     * will be implemented when generic SIO is implemented, i.e., ISF.
     */
    public static final String LOCAL_READ_BHPSU_DATA_CMD = "readLocalBhpsuData";

    // PROJECT MANAGER COMMANDS

    /**
     * Notify a qiComponent what project manager it is associated with. No response
     * is required or expected.
     * <br><b>Input type:</b> ComponentDescriptor
     * <br><b>Input parameter:</b> Component descriptor of a PM
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A.
     */
    public static final String NOTIFY_ASSOC_PROJMGR_CMD = "notifyAssocProjMgr";

    /**
     * Notify all non-PM qiComponents that project metadata has changed. No response
     * is required or expected.
     * <br><b>Input type:</b> ArrayList
     * <br><b>Input parameter:</b>
     * <ul>
     * <li>[0] Component descriptor of the PM</li>
     * <li>[1] Project descriptor of the PM
     * </ul>
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A.
     */
    public static final String NOTIFY_PROJ_INFO_CHANGED_CMD = "notifyProjInfoChanged";

    /**
     * Get the project manager associated with a project. Message broadcast by Message
     * Dispatcher to all PMs. Only PM with matching PID response. Guaranteed to be 1.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Project ID (PID)
     * <br><b>Output type:</b> ArrayList
     * <br><b>Output:</b>
     * <ul>
     * <li>[0] Component descriptor of the PM</li>
     * <li>[1] Project descriptor of the PM
     * </ul>
     */
    public static final String GET_ASSOC_PROJMGR_CMD = "getAssocProjMgr";

    /**
     * Get the information for a project managed by a PM (consumer of message).
     * <br><b>Input type:</b> N/A
     * <br><b>Input parameter:</b> N/A
     * <br><b>Output type:</b> ArrayList
     * <br><b>Output:</b> Information of project managed by PM (consumer of message).
     * <ul>
     * <li>[0] String: Project ID (PID)</li>
     * <li>[1] QiProjectDescriptor: project metadata</li>
     * </ul>
     */
    public static final String GET_PROJ_INFO_CMD = "getProjInfo";

    public static final String GET_AVAIL_COMPONENTS = "getAvailComponents";

    public static final String LOCAL_DATA_DATA_DESCRIPTOR = DataFlavor.javaJVMLocalObjectMimeType + "; class=com.bhpb.qiworkbench.compAPI.DataDataDescriptor";

    // COMPONENT CONTROL COMMANDS
    /**
     * Generic command sent by one qiComponent to the qiViewer to control its
     * behavior. It is up to the qiViewer to execute the actions() and return
     * a response.
     * <br><b>Input type:</b> ArrayList
     * <br><b>Input parameter:</b> (action, params) pairs
     * <ul>
     * <li>[0] String: action='<action>'</li>
     * <li>[1] ArrayList: List of action parameters: name=value pairs.</li>
     * </ul>
     * <br><b>Output type:</b> String or ArrayList depending on the action.
     * <br><b>Output:</b> Defined by the action. Usually a window or
     * dialog ID.
     */
    public static final String CONTROL_QIVIEWER_CMD = "controlQiviewer";
	
	/** Used by a qiComponent to control the XML editor */
	public static final String CONTROL_COMPONENT_CMD = "controlComponent";

    /**
     * Generic command sent by one component to another to check if a target component's
     * UI ready for further communcation
     * <br><b>Input type:</b> String
     * <br><b>Output type:</b> Boolean
     * <br><b>Output:</b> true or false
     */
    public static final String IS_COMPONENT_UI_READY_CMD = "isComponentUIReady";

    // MISC COMMANDS
    /**
     * Determine if the file path on a remote runtime Tomcat server is a directory.
     * <br><b>Input type:</b> String
     * <br><b>Input parameter:</b> Full path of the directory
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> "yes" if path exists and is a directory; otherwise, "no"
     */
    public static final String IS_REMOTE_DIRECTORY_CMD = "isRemoteDirectory";

    /**
     * Determine if the server is compatible with the supp.
     * <br><b>Input type:</b> Map
     * <br><b>Input parameters:</b> key: "version", value: String, key: "build", value: String
     * <br><b>Output type:</b> hMap
     * <br><b>Output:</b> If the server-side version check completes, the return code is MsgStatus.OK and a Map of
     * <br> key: (String) "compatible", value (Boolean) and
     * <br> key: (String) "reason", value (String) stating at a minimum the client and server version and build numbers
     */
    public static final String IS_COMPATIBLE_CLIENT_VERSION = "isCompatibleClientVersion";

    /**
     * Record usage stats. Used to send usage a client's usage stats to the STATS
     * server that gathers and process them. The URL of the STATS server is passed
     * directly to the DispatcherConnector which routes the message to the server.
     * <br><b>Input type:</b> ArrayList
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] Path of directory storing stats files</li>
     * <li>[1] List of stat log events</li>
     * </ul>
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A Note: Will get a normal response if the STATS server
     * accepts the stats; otherwise, an abnormal response.
     */
    public static final String RECORD_STATS_CMD = "recordStats";

    /**
     * Record the list of workbench modules which includes itself. Used as the X-axis
     * of the Module Usage historgram.
     * <br><b>Input type:</b> ArrayList
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] Path of directory storing stats files</li>
     * <li>[1] List of module display names</li>
     * </ul>
     * <br><b>Output type:</b> N/A
     * <br><b>Output:</b> N/A Note: Will get a normal response if the STATS server
     * saves the list; otherwise, an abnormal response.
     */
    public static final String RECORD_MODULES_CMD = "recordModules";

    /**
     * Create a default qiProject on a remote runtime Tomcat server.
     * <br><b>Input type:</b> ArrayList
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] Full path of new qiProject's directory to be created</li>
     * <li>[1] List of subdirectories to be created</li>
     * </ul>
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> "yes" if qiProject successfully created; otherwise, "no"
     */
    public static final String CREATE_REMOTE_PROJECT_CMD = "createRemoteProject";

    //GEOIO COMMANDS

    //geoIO operations
    public static final String GEOREAD_OP = "geoReadOp";
    public static final String GEOWRITE_OP = "geoWriteOp";

    /**
     * Read geoData from a geoFile
     * <br><b>Input type:</b> ArrayList
     * <br><b>Input parameters (Strings) when reading geoFile metadata:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] geoFormat of the geoFile</li>
     * <li>[2] full pathname of the geoFile</li>
     * <li>[3] type of geoData to be read (Metadata)</li>
     * <li>[4] Specification of data records to be read, i.e., record IDs.</li>
     * </ul>
     * <b>Input parameters when reading geoFile seismic traces:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] geoFormat of the geoFile</li>
     * <li>[2] DatasetProperties: geoFile properties</li>
     * <li>[3] type of geoData to be read (Seismic Traces)</li>
     * <li>[4] First seismic trace to read. Traces are in a contiguous range numbered
     * 0..NumberOfTraces-1 where NumberOfTraces is the number of seismic traces in
     * the range.</li>
     * <li>[5] Number of seismic traces to read</li>
     * </ul>
     * <b>Input parameters when reading trace summary information:</b>
     * <ul>
     * <li>[0] location preference: local or remote</li>
     * <li>[1] geoFormat of the geoFile</li>
     * <li>[2] DatasetProperties: geoFile's properties which contains its metadata</li>
     * <li>[3] type of geoData to be read (Summary)</li>
     * </ul>
     * <b>Input parameters when reading geoFile horizons stored in cross-section order:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] geoFormat of the geoFile</li>
     * <li>[2] DatasetProperties: geoFile properties (contains the view order)</li>
     * <li>[3] type of geoData to be read (Horizons)</li>
     * <li>[4] Horizon name</li>
     * </ul>
     * <b>Input parameters when reading geoFile horizons stored in map-view order:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] geoFormat of the geoFile</li>
     * <li>[2] DatasetProperties: geoFile properties (contains the view order)</li>
     * <li>[3] type of geoData to be read (Horizons)</li>
     * <li>[4] Horizon name</li>
     * <li>[5] First horizon trace to read. Traces are in a contiguous range numbered
     * 0..NumberOfTraces-1 where NumberOfTraces is the number of horizon traces in
     * the range.</li>
     * </ul>
     * <b>Input parameters when reading geoFile model traces:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] geoFormat of the geoFile</li>
     * <li>[2] DatasetProperties: geoFile properties</li>
     * <li>[3] type of geoData to be read (Model Traces)</li>
     * <li>[4] First model trace to read. Traces are in a contiguous range numbered
     * 0..NumberOfTraces-1 where NumberOfTraces is the number of model traces in
     * the range.</li>
     * <li>[5] Number of model traces to read</li>
     * <li>[6] List of properties to be included in a model trace. An empty list
     * implies all properties in the geoFile</li>
     * </ul>
     * <b>Output type:</b> ArrayList
     * <br><b>Output:</b>
     * <ul>
     * <li>[0] geoFormat of the geoFile</li>
     * <li>[1] type of geoData read</li>
     * <li>[2] object containing the geoData, e.g., GeoFileMetadata, GeoFileDataSummary
     * or GeoDataRead into which the geoIO service can write the geoData read.</li>
     * </ul>
     */
    public static final String READ_GEOFILE_CMD = "readGeoFile";

    /**
     * Write geoData to a geoFile
     * <br><b>Input type:</b> ArrayList<String>
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] geoFormat of the geoFile
     * <li>[2] full pathname of the geoFile
     * <li>[3] type of geoData to be written
     * <li>[4] Specification of data records to be written, i.e., record IDs.
     * </ul>
     * <b>Output type:</b> ArrayList
     * <br><b>Output:</b>
     * <ul>
     * <li>[0] geoFormat of the geoFile
     * <li>[1] type of geoData to be written
     * <li>[2] object containing the geoData, i.e., GeoWriteData, into which the client can insert the geoData to be written.</li>
     * </ul>
     * <b>Input parameters when writing a geoFile horizon stored in cross-section order:</b>
     * <ul>
     * <li>[0] IO Preference - remote</li>
     * <li>[1] geoFormat of the geoFile</li>
     * <li>[2] DatasetProperties: geoFile properties (contains the view order)</li>
     * <li>[3] type of geoData to be writen (Horizon)</li>
     * <li>[4] Horizon name</li>
     * <li>[5] Horizon values as byte[]</li>
     * </ul>
     * <br><b>Output type:</b> String
     * <br><b>Output:</b> status of the action
     */
    public static final String WRITE_GEOFILE_CMD = "writeGeoFile";

    /**
     * Get a remote geoIO service
     * <br><b>Input type:</b> ArrayList<String>
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] CID of the matching client-side geoIO service</li>
     * <li>[1] geoFormat of the geoFile
     * <li>[2] full pathname of the geoFile
     * <li>[3] type of geoData to be read or written
     * <li>[4] Type of service - read or write
     * </ul>
     * <b>Output type:</b> String
     * <br><b>Output:</b> CID of the server-side geoIO service
     */
    public static final String GET_REMOTE_GEOIO_SERVICE_CMD = "getRemoteGeoIoService";

    /**
     * Perform a geoIO operation. Sent to a server-side geoIO service to perform
     * a remote procedure call and return a response that contains a return
     * value or exception.
     * <br><b>Input type:</b> ArrayList
     * <br><b>Input parameter:</b> (action, params) pairs
     * <ul>
     * <li>[0] String: CID of server-side geoIO Service that will perform action(s)
     * <li>[1] String: action='<action>' defines the remote method to call</li>
     * <li>[2] ArrayList: List of action parameters: name=value pairs. Arguments to the remote method.</li>
     * </ul>
     * <br><b>Output type:</b> String or ArrayList depending on the action.
     * <br><b>Output:</b> Defined by the action.
     */
    public static final String PERFORM_GEOIO_OP_CMD = "performGeoIoOp";

    /**
     * Release a client-side geoIO Service
     * <br><b>Input type:</b> ArrayList<String>
     * <br><b>Input parameters:</b>
     * <ul>
     * <li>[0] CID of the client-side geoIO Service</li>
     * <li>[1] CID of the matching server-side geoIO Service if geoIO remote; otherwise, empty string</li>
     * </ul>
     * <b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String RELEASE_GEOIO_SERVICE_CMD = "releaseGeoIoService";

    /**
     * Release a server-side geoIO Service
     * <br><b>Input type:</b> String
     * <br><b>Input parameters:</b> CID of the server-side geoIO Service
     * <b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String RELEASE_REMOTE_GEOIO_SERVICE_CMD = "releaseRemoteGeoIoService";
    
    /**
     * Release a server socket. Sent by the client when it has read all the data
     * transmitted by the server over the socket assigned by the server. This
     * tells the server the socket is available for reuse. No response is 
     * required.
     * <br><b>Input type:</b> String
     * <br><b>Input parameters:</b>
     * <ul>
     * <li> Socket port (integer) </li>
     * </ul>
     * <b>Output type:</b> N/A
     * <br><b>Output:</b> N/A
     */
    public static final String RELEASE_SERVER_SOCKET_CMD = "releaseServerSocket";
}
