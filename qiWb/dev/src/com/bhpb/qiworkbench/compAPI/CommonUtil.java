/*
 ###########################################################################
 # qiWorkbench - an extensible platform for seismic interpretation
 # This program module Copyright (C) 2006, BHP Billiton Petroleum and is
 # licensed under the following BSD License.
 #
 # Redistribution and use in source and binary forms, with or without
 # modification, are permitted provided that the following conditions are met:
 #
 #  * Redistributions of source code must retain the above copyright notice,
 #    this list of conditions and the following disclaimer.
 #  * Redistributions in binary form must reproduce the above copyright notice,
 #    this list of conditions and the following disclaimer in the documentation
 #    and/or other materials provided with the distribution.
 #  * Neither the name of BHP Billiton Petroleum  nor the names of its
 #    contributors may be used to endorse or promote products derived from this
 #    software without specific prior written permission.
 #
 # THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 # AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 # IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 # ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 # LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 # CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 # SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 # INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 # CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 # ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 # POSSIBILITY OF SUCH DAMAGE.
 #
 ############################################################################
 */
/**
 * Common utilitity functions for all internal/external componemts
 *
 * @author Charlie Jiang
 * @author Gil Hansen
 * @version 1.1
 */
package com.bhpb.qiworkbench.compAPI;

import java.awt.Component;
import java.io.*;
import java.util.*;
import java.util.logging.Logger;
import java.util.zip.Adler32;
import java.util.zip.CheckedInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.swing.JOptionPane;


public class CommonUtil {
    private static final Logger logger =
            Logger.getLogger(CommonUtil.class.getName());

	static final int BUFFER = 2048;
	public static String HOME_DIR = "home_dir";

	public static String QIWB_DIR = "qiwb_dir";

	public static String COMP_DIR = "comp_dir";

	/**
	 * Get user's Home, .qiworkbench or components directory.
	 * 
	 * @param name
	 * @return string
	 */
	public static String getAppDir(String name) {
		String retDir = "";
		String homeDir = System.getenv("HOME"); // Unix platforms
		if (homeDir == null || homeDir.equals("")) {
			homeDir = System.getenv("USERPROFILE"); // Windows platforms
		}
		if (homeDir != null) {
			// If the end-user is on a Windows platform and their HOME
			// directory is on the network, force their home directory to
			// be C:\Documents and Settings\%USERNAME% so qiwbPreferences.xml
			// will be written.
			// NOTE: On Unix/Linux, $HOME may be on th network, but this is
			// not an issue because it is NSF mounted.
			if (homeDir.startsWith("/") | homeDir.startsWith("C:")) {
				// do nothing, homeDir id fine
			} else {
				String username = System.getenv("USERNAME");
				homeDir = "C:\\Documents and Settings\\" + username;
			}
		}

		if (HOME_DIR.equals(name)) {
			retDir = homeDir;
		} else if (QIWB_DIR.equals(name)) {
			retDir = homeDir + File.separator + QIWConstants.QIWB_WORKING_DIR;
		} else if (COMP_DIR.equals(name)) {
			retDir = homeDir + File.separator + QIWConstants.QIWB_WORKING_DIR
					+ File.separator + QIWConstants.COMP_CACHE_DIR;
		}
		return retDir;
	}
    
    /**
     * Get ID of user executing the workbench
     * @return userid of end-user
     */
     public static String getUserID() {
         String userHomeDir = getAppDir(HOME_DIR);
         String filesep = (userHomeDir.indexOf("/") != -1) ? "/" : "\\";
         return userHomeDir.substring(userHomeDir.lastIndexOf(filesep)+1);
     }

	/**
	 * Get external component's dir which is underneath .qiworkbench.
	 * @deprecated Structure of .qiworkbench directory changed.
	 * @param componentName Display name of qiComponent
	 * @return qiComponent's directory under .qiworkbench
	 */
	public static String getExternalDir(String componentName) {
		String qiwb = getAppDir(QIWB_DIR);
		String retDir = qiwb + File.separator + componentName;
		return retDir;
	}

	/**
	 * Get external component's config dir which is underneath .qiworkbench.
	 * @deprecated Structure of .qiworkbench directory changed.
	 * @param componentName Display name of qiComponent
	 * @return qiComponent's config directory
	 */
	public static String getExternalConfig(String componentName) {
		String retDir = getExternalDir(componentName) + File.separator
				+ "config";
		return retDir;
	}

	/**
	 * Get external component's distribution dir which is underneath .qiworkbench.
	 * @deprecated Structure of .qiworkbench directory changed.
	 * @param componentName Display name of qiComponent
	 * @return qiComponent's distribution directory
	 */
	public static String getExternalDist(String componentName) {
		String retDir = getExternalDir(componentName) + File.separator + "dist";
		return retDir;
	}

	public static byte[] readBytes(File f) throws IOException {
		byte[] buf = new byte[(int) f.length()];
		FileInputStream in = new FileInputStream(f);
		in.read(buf);
		in.close();
		return buf;
	}

	public static void writeBytes(byte[] buf, File f) throws IOException {
		FileOutputStream out = new FileOutputStream(f);
		out.write(buf);
		out.close();
	}

	public static void writeTextFile(String content, File file)
			throws IOException {
		writeTextFile(content, file, System.getProperty("file.encoding"));
	}

	public static void writeTextFile(String content, File file, String encoding)
			throws IOException {
		// OutputStreamWriter writer = new SmartOutputStreamWriter(new
		OutputStreamWriter writer = new OutputStreamWriter(
				new FileOutputStream(file), encoding);
		writer.write(content);
		writer.close();
	}

	/**
	 * Write text to text file if this means a change to the file content
	 * 
	 * @param content
	 *            Text to write
	 * @param file
	 *            File to be written. Will make a comparison before writing if
	 *            file already exists
	 * @param encoding
	 *            Text encoding to use
	 * @return boolean true if a change was actually made
	 */
	public static boolean writeChangedTextFile(String content, File file,
			String encoding) throws IOException {
		if (!file.exists()) {
			writeTextFile(content, file, encoding);
			return true;
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		OutputStreamWriter writer = new OutputStreamWriter(out, encoding);
		writer.write(content);
		writer.close();
		byte[] outBytes = out.toByteArray();
		byte[] inBytes = readBytes(file);
		if (!Arrays.equals(outBytes, inBytes)) {
			writeBytes(outBytes, file);
			return true;
		} else
			return false;
	}

	/**
	 * Read an equal sign separated list of name=value pairs into a Property
	 * object. Does not handle names with spaces and is limited to ISO-8859-1.
	 * See readMapFile()
	 */
	public static Properties readPropertyFile(File file) throws IOException {
		FileInputStream fis = null;
		try {
			Properties props = new Properties();
			fis = new FileInputStream(file);
			props.load(fis);
			return props;
		} finally {
			if (fis != null)
				fis.close();
		}
	}

	/*
	 * Returns true if the given line is a line that must be appended to the
	 * next line (from the Property object of SDK1.4)
	 */
	private static boolean continueLine(String line) {
		int slashCount = 0;
		int index = line.length() - 1;
		while ((index >= 0) && (line.charAt(index--) == '\\'))
			slashCount++;
		return (slashCount % 2 == 1);
	}

	/**
	 * Copy file or directory to destination. Preserves file modification date
	 * 
	 * @param src File/directory to copy
	 * @param dest Destination. May point either to a file or a directory
	 */
	public static void copyFile(File src, File dest) throws IOException {
		copyFile(src, dest, true);
	}

	/**
	 * Copy file or directory to destination. Preserves file modification date
	 * 
	 * @param name Name of file/directory to copy
	 * @param dest Destination. May point either to a file or a directory
	 */
	public static void copyFile(String name, File dest) throws IOException {
		copyFile(name, dest, true);
	}

	/**
	 * Copy file or directory to destination. Preserves file modification date
	 * 
	 * @param name Name of file/directory to copy
	 * @param dest Destination. May point either to a file or a directory
	 * @param forceCopy
	 *            Always copy, even if destination file already exist
	 */
	public static void copyFile(String name, File dest, boolean forceCopy)
			throws IOException {
		copyFile(new File(name), dest, forceCopy);
	}

	/**
	 * Copy file or directory to destination. Preserves file modification date
	 * 
	 * @param src File/directory to copy
	 * @param dest Destination. May point either to a file or a directory
	 * @param forceCopy
	 *            Always copy, even if destination file already exist
	 */
	public static void copyFile(File src, File dest, boolean forceCopy)
			throws IOException {
		if (src.isDirectory()) {
			if (!dest.isDirectory()) {
				throw new IOException(
						"copyFile: Cannot copy directory onto single file");
			}
			File[] files = src.listFiles();
			for (int i = 0; i < files.length; i++) {
				copyFile(files[i], dest, forceCopy);
			}
		} else {
			if (dest.isDirectory()) {
				dest = new File(dest, src.getName());
			}
			if (!forceCopy && dest.exists())
				return;
			if (dest.exists() && !dest.canWrite())
				return;

			// Preserve file modification date
			long lastModified = src.lastModified();
			// Start copying
			BufferedInputStream in = new BufferedInputStream(
					new FileInputStream(src));
			BufferedOutputStream out = new BufferedOutputStream(
					new FileOutputStream(dest));
			try {
				byte[] buffer = new byte[65536];
				while (true) {
					int bytesRead = in.read(buffer);
					if (bytesRead == -1) {
						break;
					}
					out.write(buffer, 0, bytesRead);
				}
			} finally {
				in.close();
				out.close();
				dest.setLastModified(lastModified);
			}
		}
	}

	/**
	 * Copy a directory (including its subdirectories) to destination directory.
	 * 
	 * @param srcPath Path to source directory
	 * @param dest Destination directory. Create one if not existing already
	 * @param forceCopy Always copy, even if destination files already exists
	 */
	public static void copyDirectoryContent(String srcPath, File dest,
			boolean forceCopy) throws IOException {
		copyDirectoryContent(new File(srcPath), dest, forceCopy);
	}

	public static void copyDirectoryContent(File srcDir, File dest,
			boolean forceCopy) throws IOException {
		dest.mkdir(); // create if nonexistent
		if (!srcDir.isDirectory()) {
			throw new IOException("Missing directory "
					+ srcDir.getAbsolutePath());
		}
		File[] files = srcDir.listFiles();
		for (int i = 0; i < files.length; i++) {
			if (files[i].isDirectory()) {
				copyDirectoryContent(files[i], new File(dest, files[i]
						.getName()), forceCopy); // Recurse
			} else if (files[i].isFile()) {
				copyFile(files[i].getAbsolutePath(), dest, forceCopy);
			}
		}
	}

	/**
	 * Strip file extension from file name (if extension exists)
	 */
	public static String baseName(String fullName) {
		int dotIndex = fullName.lastIndexOf('.');
		return (dotIndex != -1) ? fullName.substring(0, dotIndex) : fullName;
	}

	/**
	 * Return a path to file that is relative to rel
	 */
	public static String relativePath(File file, File rel) {
		boolean absolute = false;
		// if (!rel.isDirectory()) rel = rel.getParentFile();
		String fileString = file.getAbsolutePath();
		String relString = rel.getAbsolutePath();
		char[] filePath = fileString.toCharArray();
		char[] relPath = relString.toCharArray();
		StringBuffer result = new StringBuffer();

		// Step forward until the two paths differ
		int i;
		for (i = 0; i < filePath.length && i < relPath.length; i++) {
			if (filePath[i] != relPath[i]) {
				break;
			}
		}
		// Directory names might start equal so reverse to start of current path
		// element unless file is a subdirectory of rel
		if ((i < relPath.length && i < filePath.length)
				|| (relPath.length < filePath.length && filePath[i] != rel.separatorChar)) {
			while (i > 0 && relPath[i - 1] != rel.separatorChar) {
				i--;

			}
		}
		if (i != 0) { // Not different discs, add parent travel code: ../..
			StringTokenizer tokens = new StringTokenizer(
					relString.substring(i), rel.separator);
			while (tokens.hasMoreTokens()) {
				tokens.nextToken();
				result.append("../");
			}
		}
		// Append remaining path to file
		StringTokenizer tokens = new StringTokenizer(fileString.substring(i),
				file.separator);
		if (tokens.hasMoreTokens()) {
			result.append(tokens.nextToken());
		} else if (result.length() > 0
				&& result.charAt(result.length() - 1) == '/') {
			result.deleteCharAt(result.length() - 1); // Remove extra ending /
		}
		while (tokens.hasMoreTokens()) {
			result.append("/" + tokens.nextToken());
		}
		// Fix for empty relative path
		String res = result.toString();
		return res.equals("") ? "." : res;
	}

	/**
	 * Combine two path parts so that there is one and only one path separator
	 * between nomatter how each part looks.
	 * 
	 * @return String combined path
	 */
	public static String combinePaths(String part1, String part2, char separator) {
		if (part1.length() == 0)
			return part2;
		if (part2.length() == 0)
			return part1;

		int end, start;
		for (end = part1.length(); end > 0
				&& part1.charAt(end - 1) == separator; end--)
			;
		if (part1.endsWith("" + separator + separator))
			end++; // Respect double separators, eg http://
		for (start = 0; start < part2.length()
				&& part2.charAt(start) == separator; start++)
			;
		return part1.substring(0, end) + separator
				+ part2.substring(start, part2.length());
	}

	/**
	 * Return number of bytes a serialized version of the passed object would
	 * occupy
	 * 
	 * @param o Object, must implement serializable
	 */
	public static int sizeof(Object o) {
		try {
			ByteArrayOutputStream ba = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(ba);
			oos.writeObject(o);
			return ba.toByteArray().length;
		} catch (IOException ex) {
			throw new RuntimeException(ex.toString());
		}
	}

	/**
	 * Figure out the latest "last modified" file date of any file or directory
	 * under the directory passed as argument including files in subdirectories
	 */
	public static long deepLastModified(File dir) throws IOException {
		long lastModified = dir.lastModified();
		File[] files = dir.listFiles();
		for (int i = 0; i < files.length; i++) {
			// Changes to files doesn't update the directory date in Windows
			long lm = files[i].lastModified();
			if (files[i].isDirectory()) {
				lm = deepLastModified(files[i]);
			}
			if (lm > lastModified)
				lastModified = lm;
		}
		return lastModified;
	}
	
	public static void bhpZip(String toFile, String atDir) {
    	try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(toFile);
            ZipOutputStream  out  = new ZipOutputStream(new BufferedOutputStream(dest));
            
            byte data[] = new byte[BUFFER];
            File f = new File(atDir);
            String files[] = f.list();
            
            for (int i=0; i<files.length; i++) {
               logger.info("Adding: "+files[i]);
               FileInputStream fi = new FileInputStream(files[i]);
               origin = new BufferedInputStream(fi, BUFFER);
               ZipEntry entry = new ZipEntry(files[i]);
               out.putNextEntry(entry);
               int count;
               while((count = origin.read(data, 0, BUFFER)) != -1) {
                  out.write(data, 0, count);
               }
               origin.close();
            }
            
            out.close();
         } catch(Exception e) {
            e.printStackTrace();
         }
    }
    
    public static void bhpUnzip(String withFile, String toDir) {
    	try {
            BufferedOutputStream dest = null;
            FileInputStream fis = new FileInputStream(withFile);
            CheckedInputStream checksum = new CheckedInputStream(fis, new Adler32());
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(checksum));
            ZipEntry entry;
            while((entry = zis.getNextEntry()) != null) {
               logger.info("Extracting: " +entry);
               int count;
               byte data[] = new byte[BUFFER];
               // write the files to the disk
               FileOutputStream fos = new FileOutputStream(entry.getName());
               dest = new BufferedOutputStream(fos, BUFFER);
               while ((count = zis.read(data, 0, BUFFER)) != -1) {
                  dest.write(data, 0, count);
               }
               dest.flush();
               dest.close();
            }
            zis.close();
            logger.info("Checksum: " + checksum.getChecksum().getValue());
         } catch(Exception e) {
            e.printStackTrace();
         }
    }
    
    
	 /***
	  * Shows a confirmation dialog and with a message and check box that can
	  * be selected to not display the dialog in the future anymore.
	  *
	  * <p>If the specified setting is not <code>null</code> and it's value is
	  * <code>false</code> the dialog is not displayed and
	  * <code>JOptionPane.YES_OPTION</code> is returned.
	  *
	  * @param parent parent component used for centering the dialog
	  * @param message the message to show
	  * @param title the title of the message border
	  * @param optionType an int designating the options available on the
	  * dialog: <code>JOptionPane.YES_NO_OPTION</code>, or
	  * <code>JOptionPane.YES_NO_CANCEL_OPTION</code>
	  * @param setting used to store the reverse state of the
	  * <it>Do not ask me again</it> check box; if null, the state is not
	  * saved
	  * @return an int indicating the option selected by user
	  */
	 public static int showConfirmDialog(Component parent,
	 									 String message,
										 String title,
										 int optionType,
										BooleanSettings setting)
	 {
		 if (setting != null && !setting.getValue()) {
			 return JOptionPane.YES_OPTION;
		 }

		 javax.swing.JCheckBox doNotAskAgainCheckBox = new javax.swing.JCheckBox("Do not ask again");

		 javax.swing.JLabel messageLabel = new javax.swing.JLabel(message);
		 //messageLabel.setBorder(GUIHelper.createDefaultBorder(title));

		 Object[] content = new Object[] {
			 messageLabel,
			 javax.swing.Box.createVerticalStrut(10),
			 doNotAskAgainCheckBox,
			 javax.swing.Box.createVerticalStrut(5),
		 };

		 int response = JOptionPane.showConfirmDialog
			 (parent, content, title, optionType);

		 if(setting != null)
			 setting.setValue(!doNotAskAgainCheckBox.isSelected());

		 return response;
    }
}
