package com.bhpb.qiworkbench.compAPI;

import java.io.File;
import java.io.Serializable;
/*
 * DataDataDescriptor.java
 *
 * Created on July 11, 2007, 11:09 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 * DataDataDescriptor Object contains information about Data File used in the qiProjectManager
 * @author Marcus Vaal
 */
public class DataDataDescriptor implements Serializable,Comparable {
    
    private String name = ""; // String is serializable
    private String dirPath = "";
    private String groupName = "";

    /**
     * DataDataDescriptor Constuctor
     * @param name Name of object
     */
    public DataDataDescriptor( String name ) {
        this.name = name;
        this.dirPath = "";
        this.groupName = "";
    }
    
    /**
     * DataDataDescriptor Constuctor
     * @param name Name of the object
     * @param groupName Group of the object
     * @param dirPath Directory path of the object
     */
    public DataDataDescriptor( String name, String groupName, String dirPath ) {
        this.name = name;
        this.groupName = groupName;
        this.dirPath = dirPath;
    }

    /**
     * Gets the name of the object
     * @return Returns the name of the object
     */
    public String getName() {
        return name;
    }
    
    /**
     * Gets the group name of the object
     * @return Returns the group name of the object
     */
    public String getGroupName() {
    	return groupName;
    }

    /**
     * Gets the directory path of the object
     * @return Returns the directory path of the object
     */
    public String getDirPath() {
        return dirPath;
    }
    
    /**
     * Gets the complete path of the object
     * @return Returns the complet path of the object
     */
    public String getPath() {
    	return dirPath + File.separator + name;
    }

    /**
     * Implements Comparable compareTo
     */
	public int compareTo(Object obj) {
		if( !(obj instanceof DataDataDescriptor)) {
			throw new ClassCastException("Instance of DataDataDescriptor expected");
		}
		String anotherName = ((DataDataDescriptor)obj).getName();
		return this.name.compareTo(anotherName);
	}
    
	/**
	 * Overrides Object toString
	 */
    public String toString() {
    	return "DataDataDescriptor: Name-" + name + ", Group Name-" + groupName + ", Directory Path-" + dirPath;
    	//return this.name;
    }
    
}

//import com.bhpb.qiProjectManager.DataDataDescriptor;
// projectDataTree = new DragTree(root, DnDConstants.ACTION_COPY);
