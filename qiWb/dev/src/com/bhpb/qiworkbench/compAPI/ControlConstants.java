/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006-2007, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.compAPI;

/**
 * Constants used to control the actions of a qiComponent.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public final class ControlConstants {
    /**
     * Prevent object construction outside of this class.
     */
    private ControlConstants() {}

    // ACTIONS
    public static final String START_CONTROL_SESSION_ACTION = "startControlSession";
    public static final String END_CONTROL_SESSION_ACTION = "endControlSession";

    // qiViewer CONTROL ACTIONS
    public static final String OPEN_WINDOW_ACTION = "openWindow";
    public static final String LOAD_DATA_ACTION = "loadData";
    public static final String DISPLAY_DATA_ACTION = "displayData";
    public static final String RESIZE_WINDOW_ACTION = "resizeWindow";
    public static final String RESIZE_VIEWER_ACTION = "resizeViewer";
    public static final String VIEW_ALL_ACTION = "viewAll";
    public static final String ADD_LAYER_ACTION = "addLayer";
    public static final String POSITION_WINDOW_ACTION = "positionWindow";
    public static final String POSITION_SCROLLBAR_ACTION = "positionScrollbar";
	public static final String REDRAW_WINDOW_ACTION = "redrawWindow";

    public static final String GET_LAYER_PROP_ACTION = "getLayerProperty";
    public static final String SET_LAYER_PROP_ACTION = "setLayerProperty";
    public static final String SELECT_LAYER_PROP_ACTION = "selectLayerProperty";
	public static final String GET_LAYER_PROPS_ACTION = "getLayerProperties";
    public static final String OK_LAYER_PROPS_ACTION = "okLayerProperties";
    
    public static final String GET_DATASET_SUMMARY_ACTION = "getDatasetSummary";

    public static final String GET_WIN_PROP_ACTION = "getWinProperty";
    public static final String SET_WIN_PROP_ACTION = "setWinProperty";
    public static final String SELECT_WIN_PROP_ACTION = "selectWinProperty";
    public static final String OK_WIN_PROPS_ACTION = "okWinProperties";

    public static final String GET_PREF_ACTION = "getPreference";
    public static final String SET_PREF_ACTION = "setPreference";
    public static final String SELECT_PREF_ACTION = "selectPreference";
    public static final String OK_PREFS_ACTION = "okPreferences";

    public static final String SHOW_LAYER_ACTION = "showLayer";
    public static final String HIDE_LAYER_ACTION = "hideLayer";
    public static final String MOVE_LAYER_ACTION = "moveLayer";
    public static final String RESIZE_LAYERS_ACTION = "resizeLayers";
    public static final String SET_LAYER_COLOR_ACTION = "setLayerColor";

    public static final String SELECT_COLOR_MAP_ACTION = "selectColorMap";

    public static final String GET_TRACE_MIN_ACTION = "getTraceMin";
    public static final String GET_TRACE_MAX_ACTION = "getTraceMax";
	public static final String GET_TRACE_RANGE_ACTION = "getTraceRange";
	
	public static final String NOP_ACTION = "nop";
	
	public static final String SCROLL_ACTION = "scroll";
	public static final String ZOOM_ACTION = "zoom";
	public static final String SET_DIVIDER_ACTION = "setDivider";
	
	public static final String INIT_PROGRESS_DIALOG_ACTION = "initProgressDialog";
	public static final String END_PROGRESS_DIALOG_ACTION = "endProgressDialog";

    //DEPRECATED (used in bhpViewer's transducer)
    public static final String CREATE_WINDOW_ACTION = "createWindow";
    public static final String DISPLAY_WINDOW_ACTION = "displayWindow";
    public static final String OPEN_DIALOG_ACTION = "openDialog";
    public static final String GET_PROP_ACTION = "getProperty";
    public static final String SET_PROP_ACTION = "setProperty";
    public static final String SELECT_PROP_ACTION = "selectProperty";
    public static final String OK_ACTION = "ok";

    // STEP STATES
    public static final int SESSION_STEP = 0;

    // ACTION STATES
    // Session Step:
    public static final int START_SESSION = 1;
    public static final int END_SESSION = 2;

    // LAYERS
    public static final String SEISMIC_LAYER = "seismicLayer";
    public static final String MODEL_LAYER = "modelLayer";
    public static final String HORIZON_LAYER = "horizonLayer";
	public static final String XSECTION_LAYER = "xsecLayer";
	public static final String MAP_LAYER = "mapLayer";

    // LAYER PROPERTIES
    public static final String LAYER_DATASET_PROPERTIES = "layerDatasetProps";
    public static final String LAYER_LOCAL_SUBSET_PROPERTIES = "layerLocalSubsetProps";
    public static final String LAYER_DISPLAY_PROPERTIES = "layerDisplayProps";
    public static final String LAYER_SYNC_PROPERTIES = "layerSyncProps";

    // WINDOW PROPERTIES
    public static final String WIN_SCALE_PROPERTIES = "winScaleProps";
    public static final String WIN_ANNO_PROPERTIES = "winAnnoProps";
    public static final String WIN_SYNC_PROPERTIES = "winSyncProps";

    // PREFERENCES
    public static final String PICKING_PREFERENCES = "pickingPrefs";
    public static final String TRACKING_PREFERENCES = "trackingPrefs";
    public static final String HORIZON_PREFERENCES = "horizonPrefs";

    // SCROLLBARS
    public static final String VERTICAL_SCROLLBAR = "verticalScrollbar";
    public static final String HORIZONTAL_SCROLLBAR = "horizontalScrollbar";
	
	// SPLIT PANES
	public static final String LAYER_EXPLORER = "layerExplorer";
}
