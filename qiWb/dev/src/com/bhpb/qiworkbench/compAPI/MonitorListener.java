/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2005  Santhosh Kumar T, 
# Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
# For more information about the original LGPL work by Santhosh Kumar T, see
# http://henterji.blogbus.com/logs/2005/11/2033950.html
###########################################################################
*/
package com.bhpb.qiworkbench.compAPI;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MonitorListener implements ChangeListener, ActionListener {		
	ProgressMonitor monitor;		
	Window owner;		
	Timer timer;
	Icon icon;
	String title;
	String text;
	public MonitorListener(Window owner, ProgressMonitor monitor,String title, String text, Icon icon) {			
		this.owner = owner;			
		this.monitor = monitor;		
		this.title = title;
		this.text = text;
		this.icon = icon;
	}		
	public void stateChanged(ChangeEvent ce) {			
		ProgressMonitor monitor = (ProgressMonitor) ce.getSource();			
		if (monitor.getCurrent() != monitor.getTotal()) {				
			if (timer == null) {					
				timer = new Timer(monitor.getMilliSecondsToWait(), this);					
				timer.setRepeats(false);					
				timer.start();				
			}			
		} else {
			if (timer != null && timer.isRunning())					
				timer.stop();				
			monitor.removeChangeListener(this);			
		}		
	}		
	public void actionPerformed(ActionEvent e) {			
		monitor.removeChangeListener(this);
		ProgressDialog dlg;
		if(owner != null)
			dlg = owner instanceof Frame ? new ProgressDialog((Frame) owner, monitor,title,text,icon) : new ProgressDialog((Dialog) owner, monitor,title,text,icon);
		else{
			owner = new Frame();
			dlg = new ProgressDialog((Frame) owner, monitor,title,text,icon);
		}
		dlg.pack();
		int x,y;
		if(owner != null){
			int xx = owner.getBounds().x + owner.getBounds().width/2;
            int yy = owner.getBounds().y + owner.getBounds().height/2;
			x = xx - dlg.getBounds().width/2;
            y = yy - dlg.getBounds().height/2;
            dlg.setLocation(x,y);
			dlg.setLocationRelativeTo(owner);
		}else
			dlg.setLocationRelativeTo(null);
        //dlg.setSize(owner.getBounds().width,owner.getBounds().height);
        			
		dlg.setVisible(true);		
	}	
}	
