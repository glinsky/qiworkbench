/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.compAPI;
import java.awt.Component;
import java.awt.Window;

import javax.swing.Icon;
import javax.swing.SwingUtilities;

/**
 * Progress utilities.
 *
 * @version 1.0
 */
public class ProgressUtil {

    /**
     * Create a ProgressMonitor object.
     *
     * @param owner parent component where the modal ProgressDialog is based
     * @param total the total length of measurement which the ProgressBar take to finish
     * @param indeterminate the mode where the ProgressBar is based
     * @param milliSecondsToWait the length of time which ProgressDialog need to wait before becoming visible
     * @param title the title appears on the ProgressDialog
     * @param text the text describing the task that the ProgressMonitor is monitoring
     * @param icon the icon that may appear on lefthand side of the ProgressDialog
     * @return ProgressMonitor object
     */
    public static ProgressMonitor createModalProgressMonitor(Component owner,int total, boolean indeterminate, int milliSecondsToWait,String title, String text, Icon icon) {
        ProgressMonitor monitor = new ProgressMonitor(total, indeterminate,milliSecondsToWait);
        Window window;
        if(owner != null)
            window = owner instanceof Window ? (Window) owner : SwingUtilities.getWindowAncestor(owner);
        else
            window = null;

        monitor.addChangeListener(new MonitorListener(window, monitor,title, text, icon));
        return monitor;
    }

    /**
     * Set the current value of the given progress monitor.
     *
     * @param monitor ProgressMonitor object that current value is set
     * @param status the status of progress
     * @param current the current value the monitor's current value
     * @return
     */
    public static void setCurrent(ProgressMonitor monitor, String status, int current) {
        monitor.setCurrent(status,current);
    }

    /**
     * Start the given progress monitor.
     *
     * @param monitor ProgressMonitor object that will start
     * @param status the status of progress
     * @return
     */
    public static void start(ProgressMonitor monitor, String status) {
        monitor.start(status);
    }

    /**
     * Check to see if the given progress monitor is being cancelled.
     *
     * @param monitor ProgressMonitor object this method will act on
     * @return boolean
     */
    public static boolean isCancel(ProgressMonitor monitor) {
        return monitor.isCancel();
    }

    /**
     * Get the total value of the given progress monitor.
     *
     * @param monitor ProgressMonitor object this method will act on
     * @return total value
     */
    public static int getTotal(ProgressMonitor monitor) {
        return monitor.getTotal();
    }

    /**
     * Set the indeterminate flag to the given progress monitor.
     *
     * @param monitor ProgressMonitor object this method will act on
     * @param indeterminate true or false
     * @return
     */
    public static void setIndeterminate(ProgressMonitor monitor, boolean indeterminate) {
        monitor.setIndeterminate(indeterminate);
    }

    /**
     * Set the text describing the nature of the task of the given progress monitor.
     *
     * @param monitor ProgressMonitor object this method will act on
     * @param text
     * @return
     */
    public static void setText(ProgressMonitor monitor, String text) {
        monitor.setText(text);
    }
}





























































































