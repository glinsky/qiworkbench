/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2007, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.compAPI;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.swing.filechooser.FileFilter;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.util.ErrorDialogUtils;
import com.bhpb.qiworkbench.client.util.GenericFileFilter;

/**
 * Utilities to export and import the XML file representing the state of a qiComponent.
 *
 * @author Gil Hansen.
 * @version 1.0
 */
public class ComponentStateUtils {
    private static Logger logger = Logger.getLogger(ComponentStateUtils.class.getName());

    /**
     * Get the XML state file filter.
     * @return FileFilter
     */
    static private FileFilter getFileFilter() {
        String[] xmlExtensions = new String[] {".xml", ".XML"};
        FileFilter xmlFilter = (FileFilter) new GenericFileFilter(xmlExtensions, "State File (*.xml)");
        return xmlFilter;
    }

    /**
     * Invoke a file chooser for either importing or exporting the state of a qiComponent.
     * @param parent qiComponent GUI taking the import/export action.
     * @param dir Directory to start browsing from.
     * @param action Action to take, namely, import or export.
     * @param agentCompDesc Component descriptor of qiComponent agent.
     * @param agentMessagingMgr qiComponent agent Messaging Manager
     */
    static public void callFileChooser(Component parent, String dir, String action, IComponentDescriptor agentCompDesc, IMessagingManager agentMessagingMgr) {
        ArrayList list = new ArrayList();
        //true is Export, false if Import.
        boolean isExport = action.equals(QIWConstants.EXPORT_COMP_STATE) ? true : false;

        //1st element the parent GUI object
        list.add(parent);

        //2nd element is the dialog title
        //String title = isExport ? "Export qiComponent's State" : "Import qiComponent's State";
        String title;

        if (QIWConstants.EXPORT_COMP_STATE.equals(action)) {
            title = "Export qiComponent's State";
        } else if (QIWConstants.IMPORT_COMP_STATE.equals(action)) {
            title = "Import qiComponent's State";
        } else if (QIWConstants.OPEN_SESSION_ACTION_BY_2D_VIEWER.equals(action)) {
            title = "Import bhpViewer's State";
        } else {
            title = "Open";
        }

        list.add(title);

        //3rd element is a list that contains current directory to start with
        //and a flag (yes or no) indicating if an already remembered directory
        //should be used instead
        ArrayList lst = new ArrayList();
        lst.add(dir);
        lst.add("no");
        list.add(lst);

        //4th element is the file filter
        list.add(getFileFilter());

        //5th element is the navigation flag
        list.add(false);

        //6th element is the producer's/requester's component descriptor
        list.add(agentCompDesc);

        //7th element is the type of file chooser: either Open or Save
        String chooserType = isExport ? QIWConstants.FILE_CHOOSER_TYPE_SAVE : QIWConstants.FILE_CHOOSER_TYPE_OPEN;
        list.add(chooserType);

        //8th element is the action request for this service: either Import or Export
        list.add(action);

        //9th element is the default pretext for the file name if user fails to pretext it. If no pretext is required then use empty String
        String userName = ""; //System.getProperty("user.name");
        list.add(userName);

        //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
        list.add(".xml");

        //11th element is the target Tomcat URL where the file chooser chooses
        list.add(agentMessagingMgr.getTomcatURL());

        openFileChooser(list, agentMessagingMgr);
    }

    static private void openFileChooser(ArrayList list, IMessagingManager agentMessagingMgr) {
        String msgID = agentMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD, true);
        IQiWorkbenchMsg response = agentMessagingMgr.getMatchingResponseWait(msgID, 10000);
        if (response == null) {
            logger.warning("Internal error occurring in getting file chooser service returning null");
            ErrorDialogUtils.showInternalErrorDialog(Thread.currentThread().getStackTrace(), "Failed to start file chooser service");
            return;
        }
        if (response.isAbnormalStatus()) {
            logger.warning("Internal error occurring in getting file chooser service. Cause: " + response.getContent());
            ErrorDialogUtils.showInternalErrorDialog(Thread.currentThread().getStackTrace(), "Failed to start file chooser service");
            return;
        } else {
            ComponentDescriptor cd = (ComponentDescriptor)response.getContent();

            agentMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_FILE_CHOOSER_CMD, cd, QIWConstants.ARRAYLIST_TYPE, list);
            agentMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD, cd);
            return;
        }
    }

    /**
     * Convert an input stream of XML into an XML tree.
     * @param ins Input stream of XML.
     * @return XML tree
     * @throws FactoryConfigurationError
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    static private Element createXMLTree(InputStream ins) throws FactoryConfigurationError, ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        DocumentBuilder dbuilder = factory.newDocumentBuilder();
        Document doc = dbuilder.parse(ins);
        Element rt = doc.getDocumentElement();
        rt.normalize();
        return rt;
    }

    /**
     * Convert the XML state into an XML tree, i.e., Element object. If there are any
     * exceptions in the conversion process, thye are logged. It is up to the caller
     * to warn the user (when the XMl Element object returned is null.
     * @param xml qiComponent's state
     * @return The XML tree of the XML state
     */
    static private Element xmlToTree(String xml) {
        InputStream xmlInStream = null;
        Element root;
        try {
            xmlInStream = new ByteArrayInputStream(xml.getBytes());
            root = createXMLTree(xmlInStream);
            try {
                xmlInStream.close();
            } catch(IOException e) {
                logger.severe("INTERNAL ERROR: IOException converting XML state to XML object: "+e.getMessage());
                return null;
            }
        } catch (Exception e) {
            logger.severe("INTERNAL ERROR: Exception converting XML state to XML object: "+e.getMessage());
            if (xmlInStream != null) {
                try {
                    xmlInStream.close();
                } catch(IOException ioe) {
                    logger.severe("INTERNAL ERROR: IOException converting XML state to XML object: "+ioe.getMessage());
                }
            }

            return null;
        }

        return root;
    }

    /**
     * Get the <component> node in the qiComponent's state. It is input to the qiComponent's restoreState() method.
     * @param compState qiComponent's state represented in XML
     */
    static public Node getComponentNode(String compState) {
        Element root = xmlToTree(compState);

        return (root != null && root.getNodeName().equals("component")) ? root : null;
        
        
        
    }

    /**
     * Check if XML state file exists.
     *
     * @param stateFilePath Full path of XML state file.
     * @return true if preferences file exists; otherwise, false
     */
    static public boolean stateFileExists(String stateFilePath) {
        File stateFile = new File(stateFilePath);

        return (stateFile.exists() && stateFile.isFile()) ? true : false;
    }

    /**
     * Check if XML state file exists (remote or local).
     *
     * @param stateFilePath Full path of XML state file.
     * @return true if preferences file exists; otherwise, false
     * @param agentMessagingMgr message manager handling communication between file service and the component.
     */
    static public boolean stateFileExists(String stateFilePath, IMessagingManager agentMessagingMgr) {
        ArrayList params = new ArrayList();
        params.add(agentMessagingMgr.getLocationPref());
        params.add(stateFilePath);
        String msgId = agentMessagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.CHECK_FILE_EXIST_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg resp = agentMessagingMgr.getMatchingResponseWait(msgId,5000);
        String temp = "";
        if(resp != null && !MsgUtils.isResponseAbnormal(resp)){
            temp = (String)resp.getContent();
            if(temp.equals("yes"))
            	return true;
            else
            	return false;
        }else if(resp == null){
            logger.info("Timed out problem in running CHECK_FILE_EXIST_CMD.");
            return false;
        }else{
        	logger.info("Error in running CHECK_FILE_EXIST_CMD. " + (String)MsgUtils.getMsgContent(resp));
            return false;
        }

    }
    
    
    /**
     * Write qiComponent's XML state file (remote or local). File fill be overwritten if it already exists.
     *
     * @param compState The current state of the qiComponent represented in XML.
     * @param stateFilePath Full path of XML state file.
     * @param agentMessagingMgr message manager handling communication between file service and the component.
     *
     */
    static public void writeState(String compState, String stateFilePath, IMessagingManager agentMessagingMgr) throws QiwIOException {
        logger.fine(compState.toString());
        List<String> params = new ArrayList<String>();
        //Add location choice either local or remote
        params.add(agentMessagingMgr.getLocationPref());
        //Add the full path of the file
        params.add(stateFilePath);
        //Add the contents
        params.add(compState);

        
		String msgID = agentMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.FILE_WRITE_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		// wait for the response. Note: There can only be 1 response since there
		// is a separate IO service performing the IO which is not performing
		// parallel IO.
		IQiWorkbenchMsg response = agentMessagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null){
			logger.info("Messaging timed out.");
		}else if (MsgUtils.isResponseAbnormal(response)){
			logger.info("IO error: " +(String)MsgUtils.getMsgContent(response));
		}
    }
    
    /**
     * Write qiComponent's XML state file. File fill be overwritten if it already exists.
     *
     * @param compState The current state of the qiComponent represented in XML.
     * @param stateFilePath Full path of XML state file.
     *
     */
    static public void writeState(String compState, String stateFilePath) throws QiwIOException {
        logger.fine(compState.toString());

        // write XML state to the specified file.
        File stateFile = new File(stateFilePath);
        BufferedWriter bw = null;

        try {
            FileOutputStream fos = new FileOutputStream(stateFile);
            bw = new BufferedWriter(new OutputStreamWriter(fos));
            //write out the XML state
            bw.write(compState);
        } catch (IOException ioe) {
            if(ioe instanceof FileNotFoundException && ioe.getMessage().contains("Permission denied"))
                throw new QiwIOException("Insufficient privilege to write qiComponent's XML state file.");
            else
            throw new QiwIOException("IO exception writing XML state file:"+ioe.getMessage());
        } finally {
            try {
                if (bw != null) bw.close();
            } catch (IOException e) {}
        }
    }    

    /**
     * Read qiComponent's XML state file.
     *
     * @param xmlFileDir Directory containing the XML state file.
     * @return Component's state represented in XML
     */
    static public String readState(String stateFilePath) throws QiwIOException {
        // read the XML state
        File stateFile = new File(stateFilePath);

        if (!stateFile.exists()) throw new QiwIOException("state file does not exist; path="+stateFilePath);

        if (!stateFile.isFile()) throw new QiwIOException("state file is not a file; path="+stateFilePath);

        BufferedReader br = null;
        ArrayList<String> fileLines = new ArrayList<String>();
        try {
            FileInputStream fis = new FileInputStream(stateFile);
            br = new BufferedReader(new InputStreamReader(fis));
            String line = br.readLine();
            // read file line by line
            while (line != null) {
                fileLines.add(line);
                line = br.readLine();
            }
        } catch (IOException ioe) {
            throw new QiwIOException("IO exception reading preference file:"+ioe.getMessage());
        } finally {
            try {
                if (br != null) br.close();
            } catch (IOException e) {}
        }

        // Combine list elements into a string
        String compState = "";
        for (int i=0; i<fileLines.size(); i++) {
            compState += fileLines.get(i);
        }
        logger.finest("XML state:"+compState);

        return compState;
    }
    
    /**
     * Read qiComponent's XML state file (remote or local).
     *
     * @param xmlFileDir Directory containing the XML state file.
     * @return Component's state represented in XML
     * @param agentMessagingMgr message manager handling communication between file service and the component.
     */
    static public String readState(String stateFilePath, IMessagingManager agentMessagingMgr) throws QiwIOException {
    	ArrayList params = new ArrayList();
        params.add(agentMessagingMgr.getLocationPref());
        params.add(stateFilePath);
        String msgID = agentMessagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.FILE_READ_CMD,
                QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg resp = agentMessagingMgr.getMatchingResponseWait(msgID,10000);
        if(resp == null){
            logger.info("Null Response returned due to timed out.");
            return "";
        }else if(resp.isAbnormalStatus()){
            String message = "Abnormal response returned due to " + resp.getContent();
            logger.info(message);
            return "error";
        }else{
            ArrayList<String> fileLines = (ArrayList<String>)resp.getContent();
//          Combine list elements into a string
            StringBuffer buf = new StringBuffer();
            String compState = "";
            for (int i=0; i<fileLines.size(); i++) {
                buf.append(fileLines.get(i));
            }
            logger.finest("XML state:"+buf.toString());
            return buf.toString();
        }
    }    
}
