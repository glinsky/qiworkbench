/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2005  Santhosh Kumar T, 
# Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
# For more information about the original LGPL work by Santhosh Kumar T, see
# http://henterji.blogbus.com/logs/2005/11/2033950.html
###########################################################################
 */
package com.bhpb.qiworkbench.compAPI;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.Icon;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/*
 * ProgressMonitor2 - Rewrite of ProgressMonitor on 12/18/2008 by Woody Folsom
 * to resolve issues where ProgressDialog appears to hang at various points while
 * in non-indeterminate mode.
 * 
 */
public class ProgressMonitor2 {

    private final ChangeEvent changeEvent = new ChangeEvent(this);
    private final int total;
    private final List<ChangeListener> changeListeners = Collections.synchronizedList(new ArrayList<ChangeListener>(2));
    private final ProgressDialog2 progressDialog;

    private final boolean indeterminate;
    private boolean canceled = false;
    private int current = -1;
    private int milliSecondsToWait;
    private String status = "NULL";
    private String text = "NULL";

    /**
     * Constructs a ProgressMonitor2
     *
     * @param total the maximum progress of finite-length process
     * @param indeterminate if true, the process is of unknown duration
     * @param milliSecondsToWait maximum time to wait between updates of progress bar
     * @param title GUI title associated with this ProgressMonitor2
     * @param text GUI caption associated with this ProgressMonitor2
     * @param icon GUI icon associated with this ProgressMonitor2
     */
    public ProgressMonitor2(int total, boolean indeterminate, int milliSecondsToWait,
            String title, String text, Icon icon, Window owner) {
        this.indeterminate = indeterminate;
        this.milliSecondsToWait = milliSecondsToWait;
        this.total = total;

        progressDialog = createProgressDialog(owner, title, text, icon);
    }

    private void addChangeListener(ChangeListener listener) {
        synchronized (changeListeners) {
            changeListeners.add(listener);
        }
    }

    public int getMilliSecondsToWait() {
        return milliSecondsToWait;
    }

    public String getStatus() {
        return status;
    }

    public int getCurrent() {
        return current;
    }

    public String getText() {
        return text;
    }

    public int getTotal() {
        return total;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public boolean isIndeterminate() {
        return indeterminate;
    }

    private void fireChangeEvent() {
        synchronized (changeListeners) {
            for (ChangeListener listener : changeListeners) {
                listener.stateChanged(changeEvent);
            }
        }
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
        setCurrent(null, total);
        fireChangeEvent();
    }

    public void setCurrent(String status, int current) {
        if (current == -1) {
            throw new IllegalStateException("not started yet");
        }

        this.current = current;

        if (status != null) {
            this.status = status;
        }

        if (milliSecondsToWait == 0) {
            fireChangeEvent();
        }
    }

    public void setText(String text) {
        this.text = text;

        if (milliSecondsToWait == 0) {
            fireChangeEvent();
        }
    }

    /**
     * Makes the ProgressDialog2 visible and starts the timer if one is used
     * (if millisToWait != 0).
     *
     * @param status The initial status to display in the GUI
     */
    public void start(String status) {
        this.status = status;
        current = 0;

        new Thread(
                new Runnable() {
            public void run() {
                progressDialog.setVisible(true);
            }
        }
        ).start();

        fireChangeEvent();
    }

    private ProgressDialog2 createProgressDialog(Window owner, String title, String text, Icon icon) {
        ProgressDialog2 dlg;
        
        if (owner != null) {
            dlg = owner instanceof Frame
                    ? new ProgressDialog2((Frame) owner, this, title, text, icon)
                    : new ProgressDialog2((Dialog) owner, this, title, text, icon);
        } else {
            owner = new Frame();
            dlg = new ProgressDialog2((Frame) owner, this, title, text, icon);
        }

        dlg.pack();

        if (owner != null) {
            int xx = owner.getBounds().x + owner.getBounds().width / 2;
            int yy = owner.getBounds().y + owner.getBounds().height / 2;
            int x = xx - dlg.getBounds().width / 2;
            int y = yy - dlg.getBounds().height / 2;
            dlg.setLocation(x, y);
            dlg.setLocationRelativeTo(owner);
        } else {
            dlg.setLocationRelativeTo(null);
        }
        addChangeListener(dlg);

        return dlg;
    }
}