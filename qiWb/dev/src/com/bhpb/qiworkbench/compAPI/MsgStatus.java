/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.compAPI;

/**
 * Message status codes. Returned in a response message to indicate the
 * processing status of the request.
 * <p>
 * The content of the message contains the cause of an error or exception
 *
 * @author Gil Hansen
 * @version 1.0
 */
public final class MsgStatus {
    //NOTE: When you add a status code, also add it to the switch statment
    //in MsgUtils.getStatusName()

    /**
     * Prevent object construction outside of this class.
     */
    private MsgStatus(){}

    // Status codes
    /** Processing status unknown */
    public static final int SC_UNKNOWN = -1;
    public static final String SC_UNKNOWN_NAME = "UNKNOWN";

    /** Processing succeded OK */
    public static final int SC_OK = 200;
    public static final String SC_OK_NAME = "OK";

    /** Error from internal validation checks */
    public static final int SC_INTERNAL_ERROR = 300;
    public static final String SC_INTERNAL_ERROR_NAME = "INTERNAL_ERROR";

    /** Run time exception occurred, for example, null pointer, file not found,
     * IO error. */
    public static final int SC_EXCEPTION = 400;
    public static final String SC_EXCEPTION_NAME = "EXCEPTION";

    /** No response from consumer of message */
    public static final int SC_RESPONSE_TIMEOUT = 401;
    public static final String SC_RESPONSE_TIMEOUT_NAME = "RESPONSE_TIMEOUT";

    /** Server down because connection refused */
    public static final int SC_SERVER_DOWN = 402;
    public static final String SC_SERVER_DOWN_NAME = "SEVER_DOWN";

    public static final int SC_SERVER_DOES_NOT_EXIST = 403;
    public static final String SC_SERVER_DOES_NOT_EXIST_NAME = "SERVER_DOES_NOT_EXIST";

    /** Socket channel exception */
    public static final int SC_SOCKET_CHANNEL_ERROR = 404;
    public static final String SC_SOCKET_CHANNEL_ERROR_NAME = "SOCKET_CHANNEL_ERROR";
    
    /** Server socket channel allocation limit reached */
    public static final int SC_SOCKET_LIMIT_EXCEEDED = 405;
    public static final String SC_SOCKET_LIMIT_EXCEEDED_NAME = "SOCKET_LIMIT_EXCEEDED";
}
