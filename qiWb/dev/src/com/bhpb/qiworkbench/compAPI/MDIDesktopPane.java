/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2008  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench.compAPI;

import java.awt.Component;
import java.awt.Dimension;
import java.beans.PropertyVetoException;
import java.util.logging.Logger;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;

/**
 * Based on the MDIDesktopPane class by Gerald Nunn, which may be found under
 * 'resources' at http://www.javaworld.com/javaworld/jw-05-2001/jw-0525-mdi.html.
 * 
 * @author folsw9
 */
public class MDIDesktopPane extends JDesktopPane {

    //Offset used for cascade and add frame operations
    private int FRAME_OFFSET = 20;
    
    private static final Logger logger =
            Logger.getLogger(MDIDesktopPane.class.toString());
    private MDIDesktopManager desktopManager;

    public MDIDesktopPane() {
        desktopManager = new MDIDesktopManager(this);
        setDesktopManager(desktopManager);
    }

    /**
     * Add the requested JInternalFrame to this MDIDesktopPane, resizing the
     * desktop to that the scrollbars are displayed if necessary.
     * 
     * @param frame The frame to be added.
     * 
     * @return The added Component as per JDesktopPane.add(Component)
     */
    public Component add(JInternalFrame frame) {
        Component retval = super.add(frame);
        resizeDesktop();

        moveToFront(frame);
        frame.setVisible(true);
        try {
            frame.setSelected(true);
        } catch (PropertyVetoException pve) {
            logger.warning("Unable to set new frame selected due to PropertyVetoSelection: " + pve.getMessage());
            frame.toBack();
        }
        //so that scrollbars will be created if the added JInternalFrames already
        //exceed the dimensions of this MDIDesktopPane prior to being moved
        resizeDesktop();
        return retval;
    }

    /**
     * Cascade the JInternalFrames contained within this MDIDesktopPane in 
     * increments of FRAME_OFFSET, starting at coordinate 0,0.
     */
    public void cascadeFrames() {
        int x = 0;
        int y = 0;
        JInternalFrame allFrames[] = getAllFrames();

        desktopManager.setCurrentVisibleSize();
        int frameHeight = (getBounds().height - 5) - allFrames.length * FRAME_OFFSET;
        int frameWidth = (getBounds().width - 5) - allFrames.length * FRAME_OFFSET;
        for (int i = allFrames.length - 1; i >= 0; i--) {
            allFrames[i].setSize(frameWidth, frameHeight);
            allFrames[i].setLocation(x, y);
            x += FRAME_OFFSET;
            y += FRAME_OFFSET;
        }
    }

    /**
     * Invokes dispose on all child JInternalFrames and then resizes the desktop,
     * removing any scrollbars.
     */
    public void disposeAll() {
        JInternalFrame allFrames[] = getAllFrames();

        for (JInternalFrame frame : allFrames) {
            frame.dispose();
        }

        resizeDesktop();
    }

    private void resizeDesktop() {
        if (getParent() != null && isVisible()) {
            desktopManager.resizeDesktop();
        }
    }

    /**
     * Remove the (JInternalFrame) component from this MDIDesktopPane and resize
     * the MDIDesktopPane if necessary for proper scrollbar behavior.
     * 
     * @param c the Component to be removed
     */
    @Override
    public void remove(Component c) {
        super.remove(c);
        resizeDesktop();
    }

    /**
     * Set the minimum, maximum and preferred sizes of this MDIDesktopPane to the
     * indicated Dimension.  This is necessary for proper scrollbar behavior.
     * 
     * @param d
     */
    public void setAllSizes(Dimension d) {
        setMinimumSize(d);
        setMaximumSize(d);
        setPreferredSize(d);
    }

    /**
     * Set the visible boundary of this MDIDesktopPane, resizing it for
     * proper scrollbar behavior.
     * 
     * @param x the new x origin of the visible rectangle
     * @param y the new y origin of the visible rectange
     * @param w the width of the visible rectangle
     * @param h the height of the visible rectange
     */
    @Override
    public void setBounds(int x, int y, int w, int h) {
        super.setBounds(x, y, w, h);
        resizeDesktop();
    }

    /**
     * Tile the JInternalFrames contained within this MDIDesktopPane vertically,
     * so that each frame completely fills the width of the MDIDesktopPane's
     * visible rectangle.
     */
    public void tileFrames() {
        java.awt.Component allFrames[] = getAllFrames();
        if (allFrames.length == 0) {
            return;
        }
        desktopManager.setCurrentVisibleSize();
        int frameHeight = getBounds().height / allFrames.length;
        int y = 0;
        for (int i = 0; i < allFrames.length; i++) {
            allFrames[i].setSize(getBounds().width, frameHeight);
            allFrames[i].setLocation(0, y);
            y += frameHeight;
        }
    }
}