/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.compAPI;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.workbench.DecoratedNode;
import com.bhpb.qiworkbench.workbench.WorkbenchData;
import com.bhpb.qiworkbench.workbench.WorkbenchGUI;
import com.bhpb.qiworkbench.workbench.WorkbenchManager;
import com.bhpb.qiworkbench.workbench.WorkbenchStateManager;

/**
 * Component utilities.
 *
 * @author Gil Hansen.
 * @version 1.0
 */
public class ComponentUtils {
    private static Logger logger = Logger.getLogger(ComponentUtils.class.getName());

    // Messaging manager of qiComponent providing services to.
    private static MessagingManager compMessagingMgr;
    private static String compDisplayName;

    /**
     * Launch a new qiComponent instance by name. Called when display results of a
     * qiComponent, such as, AmpExt or WaveletDecomp..
     *
     * @param displayName Display name of the qiComponent to launch.
     * @param compMsgingMgr Messaging manager of the qiComponent
     * @return Component descriptor of the new qiComponent instance.
     */
    public static ComponentDescriptor launchNewComponent(String displayName, MessagingManager compMsgingMgr) {
        compMessagingMgr = compMsgingMgr;
        compDisplayName = displayName;

        compMessagingMgr = ((WorkbenchGUI)WorkbenchManager.getInstance().getWorkbenchGUI()).getMsgMgr();

        String compType = WorkbenchManager.getInstance().getRegisteredComponentTypeByDisplayName(displayName);
        ComponentDescriptor cd = null;
        if (compType != null) {
            if (compType.equals(QIWConstants.COMPONENT_PLUGIN_TYPE))
                cd = launchNewPlugin(displayName);
            else if(compType.equals(QIWConstants.COMPONENT_VIEWER_TYPE))
                cd = launchNewViewer(displayName);
            if (cd != null && !WorkbenchStateManager.getInstance().getCurrentNodeMap().containsKey(cd.getPreferredDisplayName())) {
                DecoratedNode node = new DecoratedNode(null, cd);
                node.setComponentClosed(false);
                WorkbenchStateManager.getInstance().setNodeToCurrentNodeMap(cd.getPreferredDisplayName(),node);
            }
        }
        return cd;
    }

    /**
     * Launch a viewer qiComponent.
     *
     * @param viewer Identifies qiComponent to be launched as a viewer type.
     * @return Component descriptor of the new qiComponent instance.
     */
    private static ComponentDescriptor launchNewViewer(String viewer) {
        // Send an activate viewer message to the message dispatcher
        String msgID = compMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.ACTIVATE_VIEWER_AGENT_CMD,
                QIWConstants.STRING_TYPE, viewer);
        IQiWorkbenchMsg response = compMessagingMgr.getMatchingResponseWait(msgID,10000);
        if (response == null) {
            logger.info("Response to activating viewer " + compDisplayName + "; returning null due to timed out problem");
            JOptionPane.showMessageDialog(WorkbenchData.gui, "Operation timed out", "Error loading "+ compDisplayName, JOptionPane.ERROR_MESSAGE);
            return null;
        } else if (response.isAbnormalStatus()) {
            JOptionPane.showMessageDialog(WorkbenchData.gui, (String)response.getContent(), "Error loading "+ compDisplayName, JOptionPane.ERROR_MESSAGE);
//            WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(), "Error occured while loading viewer "+ compDisplayName);
            return null;
        } else {
            //reroute response to the Workbench Manager for processing
            // make a new msg with ARRAYLIST_TYPE content
            ArrayList<Object> responseList = new ArrayList<Object>(2);
            ComponentDescriptor cd = (ComponentDescriptor)response.getContent();
            responseList.add(0,(Object)response.getContent());
            responseList.add(1,(Object)viewer);
            IQiWorkbenchMsg newMsg = new QiWorkbenchMsg(response.getProducerCID(), WorkbenchData.wbMgrCID, QIWConstants.DATA_MSG,
                    QIWConstants.ACTIVATE_VIEWER_AGENT_CMD, response.getMsgID(),
                    QIWConstants.ARRAYLIST_TYPE, responseList);
            compMessagingMgr.routeMsg(newMsg);
            return cd;
        }
    }

    /**
     * Launch a plug-in qiComponent.
     *
     * @param viewer Identifies qiComponent to be launched as a plug-in type.
     * @return Component descriptor of the new qiComponent instance.
     */
    private static ComponentDescriptor launchNewPlugin(String plugin) {
        // Send an activate plugin message to the message dispatcher
        logger.info("newPlugin1: Send message to activate an instance of " + plugin);
        String msgID = compMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.ACTIVATE_PLUGIN_CMD,
                QIWConstants.STRING_TYPE, plugin);
        IQiWorkbenchMsg response = compMessagingMgr.getMatchingResponseWait(msgID,10000);
        if (response == null) {
            logger.info("Response to activating plugin " + plugin + " returning null due to timed out problem");
            JOptionPane.showMessageDialog(WorkbenchData.gui, "Operation timed out", "Error loading "+ compDisplayName, JOptionPane.ERROR_MESSAGE);
            return null;
        } else if (response.isAbnormalStatus()) {
            JOptionPane.showMessageDialog(WorkbenchData.gui, (String)response.getContent(), "Error loading "+ compDisplayName, JOptionPane.ERROR_MESSAGE);
//            WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(), "Error occured while loading plugin");
            return null;
        } else {
            //reroute response to the Workbench Manager for processing
            // make a new msg with ARRAYLIST_TYPE content
            ArrayList<Object> responseList = new ArrayList<Object>(2);
            ComponentDescriptor cd = (ComponentDescriptor)response.getContent();
            responseList.add(0,(Object)response.getContent());
            responseList.add(1,(Object)plugin);
            QiWorkbenchMsg newMsg = new QiWorkbenchMsg(response.getProducerCID(),WorkbenchData.wbMgrCID,QIWConstants.DATA_MSG,
                    QIWConstants.ACTIVATE_PLUGIN_CMD,response.getMsgID(),
                    QIWConstants.ARRAYLIST_TYPE,responseList);
            compMessagingMgr.routeMsg(newMsg);

            return cd;
        }
    }
    
    /**
     * Get a component's version properties.
     *
     * @param compName - A component name (String) that is the same as the Display-Name in the component's manifest file.
     * @return Properies object - including version number, build number and component provider 
     */    
    public static Properties getComponentVersionProperies(String compName){
    	Properties props = new Properties();
    	
        String fileName = QIWConstants.COMPONENT_VERSION_PROPERTIES;
        
        String cacheDir = CommonUtil.getAppDir(CommonUtil.COMP_DIR);
        String jarName = compName + ".jar";
        InputStream instream = null;
        try{
	        URL url = new URL("jar:file:///" + cacheDir + File.separator + jarName + "!/");
	        // Get the jar file
	        JarURLConnection conn = (JarURLConnection)url.openConnection();
	        conn.setUseCaches(false);
	        JarFile jarfile = conn.getJarFile();
	        //Get the specific file from the jar
	        JarEntry jarEntry = jarfile.getJarEntry(fileName);
	        //Get the input stream from the file content
	        instream = jarfile.getInputStream(jarEntry);
	        props.load(instream);
        }catch(MalformedURLException e){
        	e.printStackTrace();
        }catch(IOException e){
        	e.printStackTrace();
        } finally {
            try {
                if (instream != null) instream.close();
            } catch (IOException e) {
                logger.fine("Excxeption closing properties file: " + fileName);
            }
        }
        return props;
        
    }
}
