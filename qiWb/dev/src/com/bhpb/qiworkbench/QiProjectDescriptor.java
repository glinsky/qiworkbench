/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Container for the attributes (metadata) of a qiProject.
 *
 * @author Gil Hansen.
 * @version 1.1
 */
public class QiProjectDescriptor implements Serializable, Comparable<QiProjectDescriptor>, Cloneable {
    /**
     * The project ID, a unique string that uniquely identifies a project.
     */
    private String pid = "";

    /**
     * The absolute path of the qiSpace that contains the project.
     * The qiSpace may be the path of the qiProject or the root
     * directory of one or more qiProjects.
     */
    private String qiSpace = "";

    /**
     * Location of the qiProject relative to the qiSpace.
     * If the qiSpace contains one or more projects, the relative
     * location is the name of the qiProject; otherwise, the OS file
     * separator.
     */
    private String qiProjectReloc = File.separator;

    /**
     * The display name of the project. Typically, the name of the
     * directory containing the project data.
     */
    private String qiProjectName = "";

    /**
     * Location of the project's datasets relative to the qiProject.
     * The default is 'datasets'. 'int_datasets' will be recognized
     * for backward compatiblity with existing datasets.
     */
    private String datasetsReloc = "datasets";

    /**
     * Location of the project's savesets relative to the qiProject.
     * The default is 'sessions'.
     */
    private String savesetsReloc = "sessions";

    /**
     * Location of the project's horizons relative to the qiProject.
     * The default is 'horizons'.
     */
    private String horizonsReloc = "horizons";

    /**
     * Location of the project's hand written scripts relative to the qiProject.
     * The default is 'scripts'.
     */
    private String scriptsReloc = "scripts";

    /**
     * Location of the project's well logs relative to the qiProject.
     * The default is 'wells'.
     */
    private String wellsReloc = "wells";

    /**
     * Location of the project's Worbkeches directory relative to the qiProject.
     * The default is 'workbences'. Repository for generated XML files such as exported project definitions.
     */
    private String workbenchesReloc = "workbenches";

    /**
     * Location of the project's temporary directory relative to the qiProject.
     * The default is 'tmp'. Repository for generated scripts.
     */
    private String tempReloc = "tmp";

    /**
     * Location of the project's XML directory relative to the qiProject.
     * The default is 'xml'. Repository for generated XML files such as exported project definitions.
     */
    private String xmlReloc = "xml";

    /**
     * Location of the SEGY files relative to the qiProject.
     * The default is 'tmp'. 
     */
    private String segyReloc = "segy";
    
    /**
     * Locations of seismic (BHP-SU) files relative to the qiProject. This list
     * is normally written to a .dat file. The default is a list of one: 'seismic'.
     */
    private ArrayList<String> seismicDirs = new ArrayList<String>();

    public QiProjectDescriptor() {
        seismicDirs.add("seismic");
    }

    //Getters
    public String getPid() {
        return pid;
    }
    public String getQiSpace() {
        return qiSpace;
    }
    public String getQiProjectReloc() {
        return qiProjectReloc;
    }
    public String getQiProjectName() {
        return qiProjectName;
    }
    public String getDatasetsReloc() {
        return datasetsReloc;
    }
    public String getSavesetsReloc() {
        return savesetsReloc;
    }
    public String getHorizonsReloc() {
        return horizonsReloc;
    }
    public String getScriptsReloc() {
        return scriptsReloc;
    }
    public String getWellsReloc() {
        return wellsReloc;
    }
    public String getWorkbenchesReloc() {
        return workbenchesReloc;
    }
    public String getTempReloc() {
        return tempReloc;
    }
    public String getSegyReloc() {
        return segyReloc;
    }
    public String getXmlReloc() {
        return xmlReloc;
    }
    public ArrayList<String> getSeismicDirs() {
        return seismicDirs;
    }

    //Setters
    public void setQiSpace(String qiSpace) {
        this.qiSpace = qiSpace;
    }
    public void setPid(String pid) {
        this.pid = pid;
    }
    public void setQiProjectReloc(String qiProjectReloc) {
        this.qiProjectReloc = qiProjectReloc;
    }
    public void setQiProjectName(String qiProjectName) {
        this.qiProjectName = qiProjectName;
    }
    public void setDatasetsReloc(String datasetsReloc) {
        this.datasetsReloc = datasetsReloc;
    }
    public void setSavesetsReloc(String savesetsReloc) {
        this.savesetsReloc = savesetsReloc;
    }
    public void setHorizonsReloc(String horizonsReloc) {
        this.horizonsReloc = horizonsReloc;
    }
    public void setScriptsReloc(String scriptsReloc) {
        this.scriptsReloc = scriptsReloc;
    }
    public void setWellsReloc(String wellsReloc) {
        this.wellsReloc = wellsReloc;
    }
    public void setWorkbenchesReloc(String workbenchesReloc) {
        this.workbenchesReloc = workbenchesReloc;
    }
    public void setTempReloc(String tempReloc) {
        this.tempReloc = tempReloc;
    }
    public void setXmlReloc(String xmlReloc) {
        this.xmlReloc = xmlReloc;
    }
    public void setSegyReloc(String segyReloc) {
        this.segyReloc = segyReloc;
    }
    public void setSeismicDirs(ArrayList<String> seismicDirs) {
        this.seismicDirs = seismicDirs;
    }
    public void addSeismicDir(String seismicDir) {
        this.seismicDirs.add(seismicDir);
    }

    /**
     * Clone project descriptor. Must be a deep clone because of seismic dirs.
     */
    public Object clone() {
        try  {
            QiProjectDescriptor clone = (QiProjectDescriptor)super.clone();
            ArrayList<String> sdirsClone = (ArrayList<String>)seismicDirs.clone();
            clone.setSeismicDirs(sdirsClone);
            return clone;
        } catch (CloneNotSupportedException e) {
          throw new Error("Can't clone QiProjectDescriptor object. This should never happen!");
        }
    }

    /**
     * Compare two project descriptors
     * @param project descriptor to compare against
     * @return 0 if identical; otherwise -1
     */
    public int compareTo(QiProjectDescriptor pd) {
        boolean identical = this.qiSpace.equals(pd.getQiSpace()) &&
            this.qiProjectReloc.equals(pd.getQiProjectReloc()) &&
            this.qiProjectName.equals(pd.getQiProjectName()) &&
            this.datasetsReloc.equals(pd.getDatasetsReloc()) &&
            this.savesetsReloc.equals(pd.getSavesetsReloc()) &&
            this.horizonsReloc.equals(pd.getHorizonsReloc()) &&
            this.scriptsReloc.equals(pd.getScriptsReloc()) &&
            this.wellsReloc.equals(pd.getWellsReloc()) &&
            this.workbenchesReloc.equals(pd.getWorkbenchesReloc()) &&
            this.tempReloc.equals(pd.getTempReloc()) &&
            this.segyReloc.equals(pd.getSegyReloc()) &&
            this.xmlReloc.equals(pd.getXmlReloc());

        ArrayList<String> sdirs = pd.getSeismicDirs();
        if (identical) {
            if (this.seismicDirs.size() != sdirs.size()) identical = false;
        }

        if (identical) {
            for (int i=0; i<this.seismicDirs.size(); i++) {
                if (!this.seismicDirs.get(i).equals(sdirs.get(i))) {
                    identical = false;
                    break;
                }
            }
        }

        return !identical ? -1 : 0;
    }

    /**
     * Diff two project descriptors for valid directories. Is is assumed the qiSpaces are
     * identical because it cannot be changed. Only directories whose relative path has
     * changed are checked to exist and be a directory. The directories must be on a
     * local runtime Tomcat server.
     * @param pd New project descriptor to diff against
     * @return comma separated list of values (in the input argument) that are different
     */
    public String diffTo(QiProjectDescriptor pd) {
        String diffList = "";
        boolean empty = true;
        String dirPath = qiSpace + File.separator;
        File dir = null;

        String val = pd.getQiProjectReloc();
        if (!this.qiProjectReloc.equals(val)) {
            dir = new File(dirPath+val);
            if (!dir.exists() || !dir.isDirectory()) {
                diffList += empty ? val : ","+val;
                empty = false;
            }
        }

        val = pd.getDatasetsReloc();
        if (!this.datasetsReloc.equals(val)) {
            dir = new File(dirPath+val);
            if (!dir.exists() || !dir.isDirectory()) {
                diffList += empty ? val : ","+val;
                empty = false;
            }
        }

        val = pd.getSavesetsReloc();
        if (!this.savesetsReloc.equals(val)) {
            dir = new File(dirPath+val);
            if (!dir.exists() || !dir.isDirectory()) {
                diffList += empty ? val : ","+val;
                empty = false;
            }
        }

        val = pd.getHorizonsReloc();
        if (!this.horizonsReloc.equals(val)) {
            dir = new File(dirPath+val);
            if (!dir.exists() || !dir.isDirectory()) {
                diffList += empty ? val : ","+val;
                empty = false;
            }
        }

        val = pd.getScriptsReloc();
        if (!this.scriptsReloc.equals(val)) {
            dir = new File(dirPath+val);
            if (!dir.exists() || !dir.isDirectory()) {
                diffList += empty ? val : ","+val;
                empty = false;
            }
        }

        val = pd.getWellsReloc();
        if (!this.wellsReloc.equals(val)) {
            dir = new File(dirPath+val);
            if (!dir.exists() || !dir.isDirectory()) {
                diffList += empty ? val : ","+val;
                empty = false;
            }
        }

        val = pd.getWorkbenchesReloc();
        if (!this.workbenchesReloc.equals(val)) {
            dir = new File(dirPath+val);
            if (!dir.exists() || !dir.isDirectory()) {
                diffList += empty ? val : ","+val;
                empty = false;
            }
        }

        val = pd.getTempReloc();
        if (!this.tempReloc.equals(val)) {
            dir = new File(dirPath+val);
            if (!dir.exists() || !dir.isDirectory()) {
                diffList += empty ? val : ","+val;
                empty = false;
            }
        }

        val = pd.getXmlReloc();
        if (!this.xmlReloc.equals(val)) {
            dir = new File(dirPath+val);
            if (!dir.exists() || !dir.isDirectory()) {
                diffList += empty ? val : ","+val;
                empty = false;
            }
        }

        val = pd.getSegyReloc();
        if (!this.segyReloc.equals(val)) {
            dir = new File(dirPath+val);
            if (!dir.exists() || !dir.isDirectory()) {
                diffList += empty ? val : ","+val;
                empty = false;
            }
        }
        
        ArrayList<String> sdirs = pd.getSeismicDirs();
        int oldSize = this.seismicDirs.size();

        for (int i=0; i<sdirs.size(); i++) {
            val = sdirs.get(i);
            //if the new project has more seismic directories, just check if the
            //exist and are a directory
            if (i >= oldSize) {
                dir = new File(dirPath+val);
                if (!dir.exists() || !dir.isDirectory()) {
                    diffList += empty ? val : ","+val;
                    empty = false;
                }
            } else {
                if (!this.seismicDirs.get(i).equals(val)) {
                    dir = new File(dirPath+val);
                    if (!dir.exists() || !dir.isDirectory()) {
                        diffList += empty ? val : ","+val;
                        empty = false;
                    }
                }
            }
        }

        return diffList;
    }

    /** Display attributes of project */
    public String toString() {
        StringBuffer buf  = new StringBuffer();
        buf.append(this.getClass().toString());
        buf.append(": \n qiSpace=");
        buf.append(this.qiSpace);
        buf.append(",\n project ID=");
        buf.append(this.pid);
        buf.append(",\n qiProject Reloc=");
        buf.append(this.qiProjectReloc);
        buf.append(",\n qiProject Name=");
        buf.append(this.qiProjectName);
        buf.append(",\n datasets Reloc=");
        buf.append(this.datasetsReloc);
        buf.append(",\n session Reloc=");
        buf.append(this.savesetsReloc);
        buf.append(",\n horizons Reloc=");
        buf.append(this.horizonsReloc);
        buf.append(",\n scripts Reloc=");
        buf.append(this.scriptsReloc);
        buf.append(",\n wells Reloc=");
        buf.append(this.wellsReloc);
        buf.append(",\n workbenches Reloc=");
        buf.append(this.workbenchesReloc);
        buf.append(",\n temp Reloc=");
        buf.append(this.tempReloc);
        buf.append(",\n XML Dirs= ");
        buf.append(this.xmlReloc);
        buf.append(",\n SEGY Reloc= ");
        buf.append(this.segyReloc);
        buf.append(",\n seismic Dirs= ");
        buf.append(this.seismicDirs);
        return buf.toString();
    }
}
