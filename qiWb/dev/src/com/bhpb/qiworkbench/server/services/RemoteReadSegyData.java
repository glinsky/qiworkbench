/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.server.services;

import java.io.*;
import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.nio.channels.ServerSocketChannel;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.StringTokenizer;

import com.gwsys.seismic.indexing.DataSliceType;
import com.gwsys.seismic.indexing.DataSourceKey;
import com.gwsys.seismic.indexing.DataSourceKeys;
import com.gwsys.seismic.indexing.DataSourcePath;
import com.gwsys.seismic.indexing.IndexedSegyTraceReader;
import com.gwsys.seismic.indexing.SegyDataset;
import com.gwsys.seismic.indexing.SegyDatasetFactory;
import com.gwsys.seismic.indexing.reader.DynamicSegyFormat;
import com.gwsys.seismic.indexing.reader.IndexedSegyReader;
import com.gwsys.seismic.io.SeismicMetaData;
import com.gwsys.seismic.reader.SeismicFormat;
import com.gwsys.seismic.reader.StandardSegyFormat;
import com.gwsys.seismic.util.SeismicDataUtil;

import com.bhpb.qiworkbench.server.SocketManager.BoundIOSocket;
import com.bhpb.qiworkbench.server.util.WriteDataToSocket;

/**
 * Read seismic data from specified SEGY file. There are four different.
 * kinds of read requests: readInfoData, startRead, readTrace, and endRead.
 *
 * @author Gil Hansen
 * @version 1.0
 */

public class RemoteReadSegyData {
    public static Logger logger = Logger.getLogger(RemoteReadSegyData.class.getName());

    //Port assigned by the server for the socket communication
    ServerSocketChannel channel = null;
    WriteDataToSocket writer = null;

    /**
     * Process the read action specified in the request
     * @param List of input parameters to the read action
     * @return empty string if read action performed successfully;
     * otherwise; a string indicating the reason why the request not
     * successfully completed.
     */
    public String processReadRequest(ArrayList params) {
        /** Server side channels to use for socket IO. */
        BoundIOSocket boundIOSocket = (BoundIOSocket)params.get(0);
        int port = boundIOSocket.getServerSocketChannel().socket().getLocalPort();
        logger.info("BhpSuIO::readSegyData Read SEGY data from server communication port="+port);
        //The last parameter is the workbench session which uniquely
        //identifies the client.
        boundIOSocket.setWorkbenchSessionID((String)params.get(params.size()-1)); 
        writer = new WriteDataToSocket(boundIOSocket);

        //Establish the server socket channel on the port so client can connect;
        //otherwise, there can be an arbitrary delay until the data is read.
        String status= writer.establishServerSocket();
logger.info("RemoteReadSegyData: status of establishServerSocket(): "+status);
        if (!status.equals("")) {
            logger.info("RemoteReadSegyData::processReadRequest Cannot establish the server port="+port);
            writer.releaseSocket();
            return status;
        }

        //Each parameter of the form name=value
        StringTokenizer tokenizer = new StringTokenizer((String)params.get(0), "=");
        String action = tokenizer.nextToken();
        //internal error if type is not "readReq"

        String readReq = tokenizer.nextToken();
        String opStatus = "";

        if (readReq.equals("readInfo")) {
            opStatus = readInfoData(params);
        } else if (readReq.equals("startRead")) {
            opStatus = startRead(params);
        } else if (readReq.equals("readTrace")) {
            opStatus = readTrace(params);
        } else if (readReq.equals("endRead")) {
            opStatus = endRead(params);
        }
        
        //release the socket
        writer.releaseSocket();

        return opStatus;
    }

    /**
     * Determine if the data source is a SEGY file.
     * @param filename Data source
     * @return true if a SEGY file; otherwise, false
     */
    private boolean isSegy(String filename) {
        String lcname = filename.toLowerCase();
        return (lcname.endsWith(".sgy") || lcname.endsWith(".segy") ||
                lcname.endsWith(".dat")) ? true : false;
    }

    /**
     * Read header info and write it back to the requester using a socket.
     *
     * @return empty string if data successfully written to the socket;
     * otherwise; a string indicating the reason why the request not
     * successfully completed.
     */
    private String readInfoData(ArrayList<String> params) {
        StringTokenizer tokenizer = new StringTokenizer(params.get(1), "=");
        String parameter1 = tokenizer.nextToken();
        //internal error if input parameter name not "filename"

        String filename = tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "";
        try {
            String dataname = filename;
            SegyDataset dataset = null;
            if (isSegy(filename)) {
                String[] files = {filename};
                String[] orders = {"BIG_ENDIAN"};
                dataset = SegyDatasetFactory.getDefaultFactory().createDataset("", files, orders);
            } else
                dataset = SegyDatasetFactory.getDefaultFactory().createDataset( filename );
            if (dataset.getProfileName() != null) dataname = dataset.getProfileName();
            String sampleKey = SeismicDataUtil.getSampleKeyName(dataset);

            StringBuffer rinfo = new StringBuffer();

            rinfo.append("rinfodata :");
            rinfo.append(dataname);
            rinfo.append("\n");

            rinfo.append("rinfopath :");
            rinfo.append( filename );
            rinfo.append("\n");

            rinfo.append("rinfoformat :");
            rinfo.append( dataset.getSegyFormatFile() );
            rinfo.append("\n");

            rinfo.append("rinfosamplekey :");
            rinfo.append( sampleKey );
            rinfo.append("\n");

            rinfo.append("rinfostartvalue :");
            rinfo.append( dataset.getStartValue());
            rinfo.append("\n");

            rinfo.append("rinfotranspose :");
            if (dataset.getDataOrder() == DataSliceType.DATA_MAP) {
                rinfo.append(dataset.getTransposedKey() + " " +
                             dataset.getTransposedName());
            }
            rinfo.append("\n");

            DataSourceKeys keys = dataset.getDatasourceKeys();
            DataSourceKey traclKey = null;
            String     traceKeyAttr = null;
            for( int i=0; i< keys.getCount(); ++i ) {
                DataSourceKey key = keys.getItemByIndex( i );
                rinfo.append("rinfokey :" + key.getName() + ":" +
                             getKeyAttributeString(key) + "\n");
            }
            SeismicMetaData metaData = SeismicDataUtil.createMetaData(dataset, null);
            rinfo.append("rinfokey :" + sampleKey + ":" +
                         SeismicDataUtil.getSamplesParameters(metaData) + "\n");

            //write info data to SocketChannel
            String opStatus = writer.write(rinfo.toString().getBytes());

            return opStatus;
        } catch (Exception ex) {
            logger.finest("RemoteReadSegyData::readInfoData:exception: "+ex);
            return "Error obtaining SEGY header info: "+ex.getMessage();
        }
    }

    /**
     * Read metadata and write it back to the requester using a socket.
     *
     * @return empty string if data successfully written to the socket;
     * otherwise; a string indicating the reason why the request not
     * successfully completed.
     */
    private String startRead(ArrayList<String> params) {
        String query = params.get(1);

        //internal error if input parameter does not start with "query"
        int idx = query.indexOf('=');
        String command = query.substring(idx+1);
        //Note: Cannot use tokenizer because a query contains '='

        StringTokenizer tokenizer = new StringTokenizer(params.get(2), "=");
        String formatFilename = tokenizer.nextToken();
        //internal error if input parameter name not "formatFilename"
        String formatFn = tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "";

        tokenizer = new StringTokenizer(params.get(3), "=");
        String sampleKey = tokenizer.nextToken();
        //internal error if input parameter name not "sampleKey"
        String sampleName = tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "";

        try {
            logger.finest("RemoteReadSegyData::startRead command=" + command);
            logger.finest("RemoteReadSegyData::startRead format=" + formatFn);
            logger.finest("RemoteReadSegyData::startRead sample=" + sampleName);
            String filename = parseFilename(command);
            SeismicFormat segyFormat = loadSegyFormat(formatFn);
            boolean indexedFile = true;
            if (isSegy(filename)) indexedFile = false;

            DataSourceKeys keys = SeismicDataUtil.parseKeys(command);
            IndexedSegyReader reader = null;
            String timeStart = "NaN";
            String timeEnd = "NaN";
            if (SeismicDataUtil.isPathData(command)) {
                if(!indexedFile)
                    throw new Exception("System does not support selection by path for standard segy files!!!" );
                if (keys != null) {
                    DataSourceKey tmpSampleKey = keys.getItemByName(sampleName);
                    if (tmpSampleKey != null) {
                        timeStart = tmpSampleKey.getMinValue();
                        timeEnd   = tmpSampleKey.getMaxValue();
                    }
                }
                DataSourcePath path = SeismicDataUtil.parsePath( keys );
                reader = new IndexedSegyReader(filename, segyFormat, path );
            } else {
                if (keys != null) SeismicDataUtil.checkFormatKeys(keys, segyFormat);

                // vertical range is not taken by indexing.jar
                DataSourceKeys keysNoSample = new DataSourceKeys();
                for (int i=0; i<keys.getCount(); i++) {
                    if (keys.getItemByIndex(i).getName().equals(sampleName)) {
                        timeStart = keys.getItemByIndex(i).getMinValue();
                        timeEnd = keys.getItemByIndex(i).getMaxValue();
                    } else keysNoSample.addItem( keys.getItemByIndex(i));
                }

                reader = new IndexedSegyReader(
                        filename, segyFormat, keysNoSample, indexedFile);
            }

            SeismicMetaData meta = reader.getMetaData();

            StringBuffer rinfo = new StringBuffer();
            rinfo.append(meta.getMinimumAmplitude() + " ");
            rinfo.append(meta.getMaximumAmplitude() + " ");
            rinfo.append(meta.getAverage() + " ");
            rinfo.append(meta.getRMS() + " ");
            rinfo.append(meta.getSamplesPerTrace() + " ");
            rinfo.append(meta.getNumberOfTraces() + " ");
            rinfo.append(meta.getSampleUnits() + " ");
            rinfo.append(meta.getSampleRate() + " ");
            rinfo.append(meta.getSampleStart() + " ");
            // last two number for sample start and end, according to query
            rinfo.append(timeStart + " ");
            rinfo.append(timeEnd);

            //write metadata to SocketChannel
            String opStatus = writer.write(rinfo.toString().getBytes());

            return opStatus;
        } catch (Exception ex) {
            logger.finest("RemoteReadSegyData::startRead:exception: "+ex);
            return "Error obtaining SEGY metadata info: "+ex.getMessage();
        }
    }

    /**
     * Read a seismic trace and write it back to the requester using a socket.
     *
     * @return empty string if trace successfully written to the socket;
     * otherwise; a string indicating the reason why the request not
     * successfully completed.
     */
    private String readTrace(ArrayList<String> params) {
        StringTokenizer tokenizer = new StringTokenizer(params.get(1), "=");
        String sampleKey = tokenizer.nextToken();
        //internal error if input parameter name not "sampleKey"
        String vkey = tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "";

        String query = params.get(2);
        //internal error if input parameter does not start with "query"
        int idx = query.indexOf('=');
        String command = query.substring(idx+1);
        //Note: Cannot use tokenizer because a query contains '='

        tokenizer = new StringTokenizer(params.get(3), "=");
        String start = tokenizer.nextToken();
        //internal error if input parameter name not "start"
        int traceStart = Integer.parseInt(tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "");

        tokenizer = new StringTokenizer(params.get(4), "=");
        String num = tokenizer.nextToken();
        //internal error if input parameter name not "num"
        int traceNum = Integer.parseInt(tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "");

        tokenizer = new StringTokenizer(params.get(5), "=");
        String startSample = tokenizer.nextToken();
        //internal error if input parameter name not "startSample"
        int sampleStart = Integer.parseInt(tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "");

        tokenizer = new StringTokenizer(params.get(6), "=");
        String samplesPerTrace = tokenizer.nextToken();
        //internal error if input parameter name not "samplesPerTrace"
        int sampleNum = Integer.parseInt(tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "");

        String filename = parseFilename(command);
        boolean indexedFile = true;
        SegyDataset dataset = null;
        IndexedSegyTraceReader reader = null;

        try {
            if (isSegy(filename)) {
                String[] files = {filename};
                String[] orders = {"BIG_ENDIAN"};
                dataset = SegyDatasetFactory.getDefaultFactory().createDataset("", files, orders);
            } else dataset = SegyDatasetFactory.getDefaultFactory().createDataset(filename);

            DataSourceKeys keys = SeismicDataUtil.parseKeys(command);

            if (SeismicDataUtil.isPathData(command)) {
                DataSourcePath path = SeismicDataUtil.parsePath(keys);
                reader = dataset.getTraceReader(path);
            } else {
                DataSourceKeys keysWithNoSample = new DataSourceKeys();
                for (int i=0; i<keys.getCount(); i++) {
                    if (keys.getItemByIndex(i).getName().equals(vkey)) continue;
                    keysWithNoSample.addItem( keys.getItemByIndex( i ) );
                }
                reader = dataset.getTraceReader(keysWithNoSample);
            }

            byte[] data = reader.getTracesData(traceStart, traceNum,
                sampleStart, sampleNum);

            //write trace to SocketChannel
            String opStatus = writer.write(data);

            return opStatus;
        } catch (IOException ioe) {
            logger.finest("RemoteReadSegyData::readTrace:exception: "+ioe);
            return "Error obtaining SEGY trace(s): "+ioe.getMessage();
        }
    }

    /**
     * Close out read action.
     *
     * @return empty string if clean up successful;
     * otherwise; a string indicating the reason why the request not
     * successfully completed.
     */
    private String endRead(ArrayList<String> params) {
        // clean up if necessary
        return "";
    }

    private String getKeyAttributeString( DataSourceKey key ) {
        String keyAttr = key.getMinValue() + "-" + key.getMaxValue();
        if( key.getIncrement() != "" ) keyAttr = keyAttr + "[" + key.getIncrement() + "]";
        return keyAttr;

    }

    private SeismicFormat loadSegyFormat(String formatFn) throws Exception {
        if (formatFn.length() == 0) {
            return new StandardSegyFormat();
        } else {
            DynamicSegyFormat dynamicSegyFormat = new DynamicSegyFormat();
            File file = new File(formatFn);
            //TODO: load using SocketChannel
            java.io.InputStream stream = new java.io.FileInputStream(file);
            dynamicSegyFormat.load(stream);
            stream.close();
            return dynamicSegyFormat;
        }
    }

    private String parseFilename(String parameters) {
        int pathlistIndex = parameters.indexOf("pathlist=");
        int pathlistEndIndex = parameters.indexOf("filename=")-1;
        String filename = parameters.substring(
                pathlistIndex+"pathlist=".length(),
                pathlistEndIndex).trim();
        return filename;
    }
}
