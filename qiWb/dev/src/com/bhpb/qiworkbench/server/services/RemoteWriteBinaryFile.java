/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.server.services;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.server.multipart.FilePartition;

import com.bhpb.qiworkbench.QiwIOException;
import java.io.RandomAccessFile;

/**
 * Write a binary file.
 */
public class RemoteWriteBinaryFile {
    private static Logger logger = Logger.getLogger(RemoteWriteBinaryFile.class.getName());

    /**
     * Write a binary file. If the file exists, it will be overwritten,
     * i.e., the binary data to be written will not be appended.
     *
     * @param listItems
     * <ul>
     * <li>[0] Path of file to write to</li>
     * <li>[1] File's format</li>
     * </ul>
     * <p>
     * If listItems(1) is QIWConstants.JPEG_FORMAT, the filepath and filename are in the file partition.
     * <p>
     * If listItems(1) is QIWConstants.HORIZON_FORMAT, then the additional elements are as follows
     * <ul>
     * <li>[2] values, byte[] of 32-bit floating-point values</li>
     * <li>[3] offset, initial position at which to start writing to the event file</li>
     * <li>[4] traceSize, the number of bytes to skip between each 4-byte trace value</li>
     * </ul>
     *
     * @throws QiwIOException If the file path is null or empty, or there is
     * an IO exception while writing to the file.
     */
    public void writeFile(ArrayList listItems) throws QiwIOException {
        String filePath = (String)listItems.get(0);
        if (filePath == null || filePath.length() == 0) throw new QiwIOException("Invalid file path="+filePath);

        String fileFormat = (String)listItems.get(1);
        try {
            if (fileFormat.equals(QIWConstants.JPEG_FORMAT)) {
                FilePartition filePart = (FilePartition)listItems.get(2);
                //NOTE: The filepath and filename are in the file partition.
                filePart.writeToFile();
            } else if(fileFormat.equals(QIWConstants.TEXT_FORMAT)) {
                String text = (String)listItems.get(2);
                BufferedWriter output = new BufferedWriter(new FileWriter(filePath));
                try {
                    output.write(text,0,text.length());
                    output.flush();
                } finally {
                    if (output != null) {
                        output.close();
                    }
                }
            } else if (fileFormat.equals(QIWConstants.HORIZON_FORMAT)) {
                byte[] horizonBytes = (byte[])listItems.get(2);
                int offset = ((Integer)listItems.get(3)).intValue();
                int traceSize = ((Integer)listItems.get(4)).intValue();
                
                if (horizonBytes.length % 4 != 0) {
                    throw new IllegalArgumentException("byte array horizonBytes must have a length which is divisble by 4, but it was: " + horizonBytes.length);
                }
  
                int nHorizonValues = horizonBytes.length / 4;
                
                logger.info("Beginning to write: " + nHorizonValues + " horizon values starting at offset + " + offset + ", skipping trace size: " + traceSize);
                
                RandomAccessFile output = new RandomAccessFile(filePath,"rw");
                logger.info("Initial file is " + output.length() + " bytes long.");
                
                int fileIndex = offset;
                try {
                    byte[] vals = new byte[4];
                    for (int horizonIndex=0; horizonIndex<nHorizonValues; horizonIndex++) {
                        System.arraycopy(horizonBytes, horizonIndex*4, vals, 0, 4);
                        try {
                            output.seek(fileIndex);
                            output.write(vals);
                        } catch (IndexOutOfBoundsException oob) {
                            logger.severe("Out of bounds exception attempting to write 4 bytes to " + filePath + " at index " + fileIndex);
                            throw oob;
                        }
                        fileIndex += traceSize;
                    }
                    logger.info("Wrote: " + horizonBytes.length + " bytes to file: " + filePath);
                } finally {
                    if (output != null)
                        output.close();
                }
            } else throw new QiwIOException("Cannot write remote binary file of unknown format: " + fileFormat);
        } catch (IOException ioe) {
            if(ioe instanceof FileNotFoundException && ioe.getMessage().contains("Permission denied")) {
                throw new QiwIOException("Insufficient privilege to write.");
            } else {
                throw new QiwIOException("IO exception writing binary file:"+ioe.getMessage());
            }
        }
    }
}