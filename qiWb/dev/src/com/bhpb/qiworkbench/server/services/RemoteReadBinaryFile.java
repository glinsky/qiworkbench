/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.server.services;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.QiwIOException;

/**
 * Read a text (ASCI) file.
 */
public class RemoteReadBinaryFile {
    private static Logger logger = Logger.getLogger(RemoteReadBinaryFile.class.getName());

    /**
     * Read a text file.
     *
     * @param filePath Path of the file.
     * @return List of each line in the file. A line does not contain any
     * line-termination characters. List is empty if file is empty.
     * @throws QiwIOException If the file path is null or empty, the path does
     * not exist, the path is not a file or there is an IO exception while
     * reading the file.
     */
    public ArrayList<String> readFile(String filePath) throws QiwIOException {
        if (filePath == null || filePath.length() == 0) throw new QiwIOException("invalid file path="+filePath);

        File f = new File(filePath);

        if (!f.exists()) throw new QiwIOException("specified file does not exist; path="+filePath);

        if (!f.isFile()) throw new QiwIOException("specified file is not a directory; path="+filePath);

        BufferedReader br = null;
        ArrayList<String> fileLines = new ArrayList<String>();
        try {
            FileInputStream fis = new FileInputStream(f);
            br = new BufferedReader(new InputStreamReader(fis));
            String line = br.readLine();
            // read file line by line
            while (line != null) {
                fileLines.add(line);
                line = br.readLine();
            }
        } catch (IOException ioe) {
            throw new QiwIOException("IO exception reading file:"+ioe.getMessage());
        } finally {
            try {
                if (br != null) br.close();
            } catch (IOException e) {}
        }

        return fileLines;
    }

        /**
	     * Read a binary file.
	     *
	     * @param filePath Path of the file.
	     * @param offset   starting position if offset < 0, then start from the beginning of the file.
	     * @param len      number of bytes,  if len < 0, then take the whole file.
	     * @return byte[]  array of bytes
	     * @throws QiwIOException If the file path is null or empty, the path does
	     * not exist, the path is not a file or there is an IO exception while
	     * reading the file.
	     */
	    public byte[] readFile(String filePath, long offset, long len) throws QiwIOException {
	        if (filePath == null || filePath.length() == 0) throw new QiwIOException("invalid file path="+filePath);

	        File f = new File(filePath);

	        if (!f.exists()) throw new QiwIOException("specified file does not exist; path="+filePath);

	        if (!f.isFile()) throw new QiwIOException("specified file is not a file; path="+filePath);

	        InputStream fis = null;
	        ByteArrayOutputStream outStream = null;
	        if(offset < 0)
	        	offset = 0;
	        byte[] bs = null;
	        try {
	            fis = new FileInputStream(f);
	            outStream = new ByteArrayOutputStream();
	            int b = -1;
	            int count = 0;
	            if(offset > -1 && len > 0){
	            	fis.skip(offset);
	            	while ((b = fis.read()) != -1 && count < len){
	            		outStream.write(b);
	            		count++;
	            	}
	            }else if(len < 0){
	            	//if offset or len is less than 0, then take the whole file
	            	while ((b = fis.read()) != -1){
	            		outStream.write(b);
	            	}
	            }
	            bs = outStream.toByteArray();
	        } catch (IOException ioe) {
	        	throw new QiwIOException(ioe.getLocalizedMessage());
	        } finally {
	            try {
	                if (fis != null) fis.close();
	                if(outStream != null) outStream.close();
	            } catch (IOException e) {}
	        }
	        return bs;
	    }

}
