/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.server.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.QiwIOException;

/**
 * Read a text (ASCI) file.
 */
public class RemoteReadFile {
    private static Logger logger = Logger.getLogger(RemoteReadFile.class.getName());

    /**
     * Read a text file.
     *
     * @param filePath Path of the file.
     * @return List of each line in the file. A line does not contain any
     * line-termination characters. List is empty if file is empty.
     * @throws QiwIOException If the file path is null or empty, the path does
     * not exist, the path is not a file or there is an IO exception while
     * reading the file.
     */
    public ArrayList<String> readFile(String filePath) throws QiwIOException {
        if (filePath == null || filePath.length() == 0) throw new QiwIOException("invalid file path="+filePath);

        File f = new File(filePath);

        if (!f.exists()) throw new QiwIOException("specified file does not exist; path="+filePath);

        if (!f.isFile()) throw new QiwIOException("specified file is not a directory; path="+filePath);

        BufferedReader br = null;
        ArrayList<String> fileLines = new ArrayList<String>();
        try {
            FileInputStream fis = new FileInputStream(f);
            br = new BufferedReader(new InputStreamReader(fis));
            String line = br.readLine();
            // read file line by line
            while (line != null) {
                fileLines.add(line);
                line = br.readLine();
            }
        } catch (IOException ioe) {
            throw new QiwIOException("IO exception reading file:"+ioe.getMessage());
        } finally {
            try {
                if (br != null) br.close();
            } catch (IOException e) {}
        }

        return fileLines;
    }
}
