/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2007-2008  BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.server.services;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.JobAdapter;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.QiwJobException;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IComponentUtils;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IServletDispatcher;
import com.bhpb.qiworkbench.client.util.ComponentUtils;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.IMsgHandler;
import com.bhpb.qiworkbench.messaging.ServerMessagingManager;
import com.bhpb.qiworkbench.server.ServletDispatcherInstance;
import com.bhpb.qiworkbench.server.util.ComponentUtilsInstance;
import com.bhpb.qiworkbench.workbench.QiComponentBase;

import com.bhpb.bhpsuio.BhpSuIO;
import com.bhpb.bhpsuio.ReadHorizon;

import com.bhpb.geoio.filesystems.FileSystemConstants;
import static com.bhpb.geoio.filesystems.FileSystemConstants.*;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import java.nio.ByteOrder;

/**
 * Server-side geoIO service, a component of qiWorkbench. It is started as a thread
 * by the Messenger Dispatcher and waits for geoIO commands involving socket IO.
 *
 * <p>
 * A geoIO Service follows the Object Adaptor Pattern, i.e., it wraps an instance of
 * a geoIO class that performs the actual IO and provides methods that call methods
 * in the instance of the wrapped class.
 */
public class GeoIOService extends QiComponentBase implements IqiWorkbenchComponent, Runnable {
    private static Logger logger = Logger.getLogger(GeoIOService.class.getName());
    
//TODO: Get the size of the trace header from the BHP-SU metadata.
    /** Size of a trace header block in bytes. */
    public static final int TRACE_HEADER_SIZE = 240;
    
    public static final int MINUTE_WAIT_TIME = 300000;
    
    private static IServletDispatcher servletDispatcher = new ServletDispatcherInstance();
    private static IComponentUtils componentUtils = new ComponentUtilsInstance();

    /** Messaging manager for service */
    private ServerMessagingManager messagingMgr;

    /** CID for service instance. Generated before the thread is started and carried as the thread's name. */
    private String myCID = "";

    public String getCID() {
        return myCID;
    }

    public IComponentDescriptor getComponentDesc() {
        return messagingMgr.getComponentDesc();
    }

    public IMsgHandler getMsgHandler() {
        return messagingMgr.getMsgHandler();
    }

    public ServerMessagingManager getMessagingMgr() {
        return (ServerMessagingManager)messagingMgr;
    }

    /** geoFile IO (read/write) instance */
//    GeoFileIO geoFileIO = null;

    /**
     * Initialize the geoIO service. Create its messaging manager which
     * creates it message handler and component descriptor.
     */
    public void init() {
        try {
            QIWConstants.SYNC_LOCK.lock();

            myCID = Thread.currentThread().getName();
            // create service's messaging manager which creates it message handler
            // and component descriptor
            messagingMgr = new ServerMessagingManager(myCID);

            // notify the startup method initialization finished
            QIWConstants.SYNC_LOCK.unlock();
            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in geoIOService.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

    /** Initialize the service.
     */
    public void run() {
        // initialize the geoIO service
        init();

        // Process IO request made on behalf of the Servlet Dispatcher for
        // another (usually client) component. The Servlet Dispatcher will send
        // the response back to the Message Dispatcher who will route it to the
        // requester.
        while (true) {
            // if queue is empty; blocks until request message added to queue
//            QiWorkbenchMsg msg = msgHandler.dequeue();
            IQiWorkbenchMsg msg = messagingMgr.getNextMsgWait();

            IQiWorkbenchMsg resp = processMsg(msg);

            // put response on queue; wait for it to be received
//            msgHandler.enqueue(resp);
            messagingMgr.putMsgWait(resp);
        }
    }

    /**
     * Process the request message.
     *
     * @param msg Request message
     * @return Response message; abnormal response if IO error
     */
    public IQiWorkbenchMsg processMsg(IQiWorkbenchMsg msg) {
        /** Request that matches the response */
        IQiWorkbenchMsg response = null;

        //log message traffic
        logger.fine("server geoIOService::procssMsg: msg="+msg.toString());

        //Check if a request. If so, process and send back a response
        if (msg.getMsgKind().equals(QIWConstants.CMD_MSG)) {
            String cmd = msg.getCommand();

            if (cmd.equals(QIWConstants.READ_GEOFILE_CMD)) {
                ArrayList params = (ArrayList)msg.getContent();
                String geoFormat = (String)params.get(1);
                String geoDataType = (String)params.get(3);

                if (geoFormat.equals(FileSystemConstants.BHP_SU_FORMAT) &&
                    geoDataType.equals(FileSystemConstants.TRACE_DATA)) {
                    //Read BHP-SU traces and send them to the client via socket channel
                    BhpSuIO bhpsuIO = new BhpSuIO();
                    String opStatus = bhpsuIO.readTraces(servletDispatcher, componentUtils, params);

                    //Check if read request performed successfully
                    if (!opStatus.equals("")) {
                        //send an abnormal response back
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, opStatus);
                        return response;
                    }

                    //Send an acknowledge response back indicating data
                    //successfully transmitted over socket.
                    response = new QiWorkbenchMsg(msg.getConsumerCID(),
                                   msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                                   msg.getMsgID(), "", "");
                    return response;
                } else

                if (geoFormat.equals(FileSystemConstants.BHP_SU_FORMAT) &&
                    geoDataType.equals(FileSystemConstants.HORIZON_DATA)) {
                    //Read BHP-SU horizon and send it to the client via socket channel
                    BhpSuIO bhpsuIO = new BhpSuIO();
                    DatasetProperties properties = (DatasetProperties)params.get(2);
                    GeoFileMetadata metadata = properties.getMetadata();
                    //Note: horizons stored in either cross-section order or map-view order
/*Read horizon data the same regardless if it is in XSec of Map view
                    String opStatus = (metadata.getGeoDataOrder() == GeoDataOrder.CROSS_SECTION_ORDER) ? bhpsuIO.readHorizon(servletDispatcher, componentUtils, params) : bhpsuIO.readHorizonTraces(servletDispatcher, componentUtils, params);
*/
                    String opStatus = bhpsuIO.readHorizonTraces(servletDispatcher, componentUtils, params);
                    //Check if read request performed successfully
                    if (!opStatus.equals("")) {
                        //send an abnormal response back
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, opStatus);
                        return response;
                    }

                    //Send an acknowledge response back indicating data
                    //successfully transmitted over socket.
                    response = new QiWorkbenchMsg(msg.getConsumerCID(),
                                   msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                                   msg.getMsgID(), "", "");
                    return response;
                } else

                if (geoFormat.equals(FileSystemConstants.BHP_SU_FORMAT) &&
                    geoDataType.equals(FileSystemConstants.MODEL_DATA)) {
                    //Read BHP-SU traces and send them to the client via socket channel
                    BhpSuIO bhpsuIO = new BhpSuIO();
                    String opStatus = bhpsuIO.readModelTraces(servletDispatcher, componentUtils, params);

                    //Check if read request performed successfully
                    if (!opStatus.equals("")) {
                        //send an abnormal response back
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, opStatus);
                        return response;
                    }

                    //Send an acknowledge response back indicating data
                    //successfully transmitted over socket.
                    response = new QiWorkbenchMsg(msg.getConsumerCID(),
                                   msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                                   msg.getMsgID(), "", "");
                    return response;
                }
            } else

            if (cmd.equals(QIWConstants.WRITE_GEOFILE_CMD)) {
                 ArrayList params = (ArrayList)msg.getContent();
                //extract command parameters
                this.ioPref = (String)params.get(0);
                this.geoFormat = (String)params.get(1);
                DatasetProperties dsProps = (HorizonProperties)params.get(2);
                this.geoDataType = (String)params.get(3);
                String hname = (String)params.get(4);
                float[] hvals = (float[])params.get(5);
                this.readService = false;
                
                GeoFileMetadata metadata = dsProps.getMetadata();
                GeoFileDataSummary summary = dsProps.getSummary();
                
                if (geoFormat.equals(FileSystemConstants.BHP_SU_FORMAT) &&
                    geoDataType.equals(FileSystemConstants.HORIZON_DATA)) {
                    //Check the BHP-SU horizon is in cross-section view
                    if (metadata.getGeoDataOrder() != GeoDataOrder.CROSS_SECTION_ORDER) {
                        //send an abnormal response back
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Error: Attempting to write a horizon that is not in cross-section view");
                        return response;
                    }
                    
                    //Read the horizon traces into a temp file
                    //Generate the path of the temp file to hold the horizon: BHP_<horizonName>.sutmp
                    String tmpDir = System.getProperty("java.io.tmpdir");
                    String tempPath = "";
                    File tempFile = null;
                    try {
                        tempFile = File.createTempFile("BHP_", ".sutmp", new File(tmpDir));
                    } catch (Exception ex) {
                    }
                    tempPath = tempFile.getAbsolutePath();
					
					JobAdapter jobAdapter = new JobAdapter();
                    String pathlist = metadata.getGeoFilePath();
                    String filename = metadata.getGeoFileName();
					
                    //SUBMIT JOB
                    boolean doBlock = true;

                    ArrayList<String> jobCmd =  ReadHorizon.genReadHorizonToFileScript(dsProps, hname, tempPath);
                    try {
                        jobAdapter.executeCommand(jobCmd, doBlock);
                    } catch (QiwJobException qje) {
                        //send an abnormal response back
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Error: " + qje.getMessage());
                        return response;
                    }
                    
                    //WAIT FOR JOB TO EXIT
                    int status = jobAdapter.waitForTermination();
                    //Check if horizon successfully read
                    if (status != 0) {
                        //send an abnormal response back
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Error: Failed to read horizon to temp file");
                        return response;
                    }

                    //RELEASE JOB
                    jobAdapter.reset();
                    
                    //decide whether or not to flip the byte order based on the _server's_
                    //byte order
                    String endianess = 
                            ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN ?
                                "1" : "0";
                    
                    byte[] vals = toByteArray(hvals, endianess);
                    
                    int traceSize = TRACE_HEADER_SIZE + 4 * summary.getSamplesPerTrace();
                    int traceOffset = TRACE_HEADER_SIZE;
                    
                    ArrayList args = new ArrayList();
                    args.add(tempPath);
                    args.add(QIWConstants.HORIZON_FORMAT);
                    args.add(vals);
                    args.add(Integer.valueOf(traceOffset));
                    args.add(Integer.valueOf(traceSize));
                    
                    try {
                        new RemoteWriteBinaryFile().writeFile(args);
                    } catch (QiwIOException qioe) {
                        //send an abnormal response back
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Error: Failed to update the temp file with new horizon data: "+qioe.getMessage());
                        return response;
                    }
                    
                    //Write the horizon out in BHP-SU format
                    
                    //SUBMIT JOB
                    doBlock = true;
                    jobCmd.clear();
                    jobCmd.add("bash");
                    jobCmd.add("-c");
                    
                    //bugfix: always use the server's native byte order
                    jobCmd.add("source ~/.bashrc; "+ "bhpwritecube" + " < " + tempPath + " filename=" + filename + " pathlist=" +
                            pathlist + " horizons=" + hname);
                    
                    logger.info("script to write hotizon: " + jobCmd);
                    try {
                        jobAdapter.executeCommand(jobCmd, doBlock);
                    } catch (QiwJobException qje) {
                        //send an abnormal response back
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Error: " + qje.getMessage());
                        return response;
                    }
                    
                    //WAIT FOR JOB TO EXIT
                    status = jobAdapter.waitForTermination();
                    //Check if horizon successfully read
                    if (status != 0) {
                        //send an abnormal response back
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Error: Failed to write horizon");
                        return response;
                    }
					
                    //Send a normal response back
                    response = new QiWorkbenchMsg(msg.getConsumerCID(),
                            msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                            msg.getMsgID(), QIWConstants.INTEGER_TYPE, new Integer(status));
                    response.setStatusCode(MsgStatus.SC_OK);

                    //RELEASE JOB
                    jobAdapter.reset();
                }

                return response;
            }
        }

        logger.warning("geoIO message not processed:"+msg.toString());
        return null;
    }
    
    /**
     * Convert a float to a byte array
     * @param value Float value
     * @param bv Byte array to store the converted float.
     */
    private void convertToBytes(float value, byte[] bv) {
        int bits = Float.floatToIntBits(value);
        bv[0] = (byte)((bits & 0xff000000) >> 24);
        bv[1] = (byte)((bits & 0xff0000) >> 16);
        bv[2] = (byte)((bits & 0xff00) >> 8);
        bv[3] = (byte)(bits & 0xff);
    }

    /**
     * Convert a vector of floats to a byte array taking endianess into account.
     * @param values Vector of floats
     * @param endianess "0" is little endian, "1" is big endian
     * @return Byte array of floats
     */
    private byte[] toByteArray(float[] values, String endianess) {
        byte[] vals = new byte[4*values.length];

        byte[] bvalue = new byte[4];
        
        for (int i=0; i<values.length; i++) {
            convertToBytes(values[i], bvalue);
            int offset = 4*i;
            if (endianess.equals("1")) {
                vals[offset] = bvalue[0];
                vals[offset+1] = bvalue[1];
                vals[offset+2] = bvalue[2];
                vals[offset+3] = bvalue[3];
            } else {
                vals[offset+3] = bvalue[0];
                vals[offset+2] = bvalue[1];
                vals[offset+1] = bvalue[2];
                vals[offset] = bvalue[3];
            }
        }
        
        return vals;
    } 

    /** Format of the geoFile */
    private String geoFormat = "";

    /** Full pathname of the geoFile */
    private String geoFile = "";

    /** Type of geoData */
    private String geoDataType = "";

    /** IO preference, i.e., local or remote */
    private String ioPref = "";

    /** true if this geoIO Service is a read service; otherwise, false. */
    private boolean readService;

    //GETTERS

    /**
     * Check if this geoIO service is a write service
     */
    public boolean isWriteService() {
        return !readService;
    }

    /**
     * Check if this geoIO service is a read service
     */
    public boolean isReadService() {
        return readService;
    }

    //SETTERS

    public void setGeoFormat(String geoFormat) {
        this.geoFormat = geoFormat;
    }

    public void setGeoDataType(String geoDataType) {
        this.geoDataType = geoDataType;
    }

    public void setGeoFile(String pathname) {
        this.geoFile = pathname;
    }

    public void setIoPref(String ioPref) {
        this.ioPref = ioPref;
    }

    /** Launch the geoIO service:
     *  <ul>
     *  <li>Start up the service thread which will initialize the service.</li>
     *  </ul>
     * <p>
     * NOTE: Each thread's init() must finish before the next thread is
     * started. This is accompished by monitoring the SYNC_LOCK object.
     */
    public static void main(String[] args) {
        GeoIOService geoIoServiceInstance = new GeoIOService();
        // get a CID for the new component instance
        String cid = ComponentUtils.genCID(IOService.class);
        // use the CID as the name of the thread
        Thread serviceThread = new Thread(geoIoServiceInstance, cid);
        serviceThread.start();
        long threadId = serviceThread.getId();
        logger.info("Remote geoIO Service Thread-"+Long.toString(threadId)+" started");

        // When the services's init() is finished, it will release the lock
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();
    }
}
