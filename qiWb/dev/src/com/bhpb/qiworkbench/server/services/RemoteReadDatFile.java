/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.server.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.SocketChannel;
import java.nio.channels.ServerSocketChannel;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.StringTokenizer;

import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IServerJobService;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.server.ServletDispatcher;
import com.bhpb.qiworkbench.server.util.ComponentUtils;
import com.bhpb.qiworkbench.server.util.WriteDataToSocket;
import com.bhpb.qiworkbench.server.SocketManager.BoundIOSocket;

/**
 * Remote read seismic data pointed to by a .dat file. There are
 * four different types of requests: startRead, readTrace, endRead
 * and saveEvent <br>
 *
 * In the case of startRead, the request has two more parameters:
 * query is the bhpread command, outFilename is a suggested name
 * of the output file. A JobService is used to run
 * the bhpread process and the output of that process is redirected
 * to the output file. If this process finishs successfully, the
 * the bhpread summary will be run using the same
 * technique. If both succeed, the actual name of the output file
 * and the content of the read summary will be sent back to the client
 * as plain text. Otherwise, an error message is sent. The output file
 * generated here is a temporary file, it will be deleted eventually
 * by the application. <br>
 *
 * In the case of readTrace, the request has three more parameters.
 * parameter1 is the name of file for reading.
 * parameter2 is the offset indicates where to start the reading.
 * parameter3 indicates how much to read.
 * The file is opened as random access and the specified segement
 * of the file read. Whatever is read will be sent to the client
 * in the binary format. <br>
 *
 * In the case of endRead, the temporary file generated with bhpread
 * will be deleted. parameter1 is the name of the file that needs to
 * deleted. <br>
 *
 * saveEvent will update the temporary file and use bhpwrite to update
 * the real data. It is used to save the event after it is edit by the
 * user. In this case, the request has five more parameters.
 * parameter1 is the bhpwrite command needed to update the disk file.
 * parameter2 is the name of the temporary file.
 * parameter3 represents new values. it's a space separated string.
 * parameter4 is the size of each trace in the temporary file.
 * parameter5 is the offset in the trace where writing should begin.
 * The temporary file will be opened for writing, and each
 * trace updated with the new value. Then bhpwrite command will be run with
 * a JobService to update the real disk file. The stderr of the process
 * will be sent back to the client. If any problems happen, an error
 * message will be sent back. <br> <br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @author Gil hansen
 * @version 1.1
 */

public class RemoteReadDatFile {
    public static Logger logger = Logger.getLogger(RemoteReadDatFile.class.getName());
    //The number of trace blocks to read and process at once.
    private static final int TRACE_BLOCKS_PER_READ = 1000;

    ServerSocketChannel channel = null;
    WriteDataToSocket writer = null;

    /**
     * Process the action specified in the request
     * @param List of input parameters to the read action
     * @return empty string if read action performed successfully;
     * otherwise; a string indicating the reason why the request not
     * successfully completed.
     */
    public String processReadRequest(ArrayList params) {
logger.info("RemoteReadDatFile::processReadRequest: params="+params);
        /** Server side channels to use for socket IO. */
        BoundIOSocket boundIOSocket = (BoundIOSocket)params.remove(0);
        int port = boundIOSocket.getServerSocketChannel().socket().getLocalPort();
        logger.info("RemoteReadDatFile::processReadRequest Read dataset from server communication port="+port);
        //The last parameter is the workbench session which uniquely
        //identifies the client.
        boundIOSocket.setWorkbenchSessionID((String)params.get(params.size()-1)); 
        writer = new WriteDataToSocket(boundIOSocket);

        //Establish the server socket channel on the port so client can connect;
        //otherwise, there can be an arbitrary delay until the data is read.
        String status= writer.establishServerSocket();

        if (!status.equals("")) {
            logger.warning("RemoteReadDatFile::processReadRequest Cannot establish the server port="+port);
            logger.warning("Status: " + status);
            writer.releaseSocket();
            return status;
        }
		

        //Each parameter of the form name=value
        StringTokenizer tokenizer = new StringTokenizer((String)params.get(0), "=");
        String action = tokenizer.nextToken();
        //internal error if type is not "readReq"

        String readReq = tokenizer.nextToken();
        String opStatus = "";

        if (readReq.equals("saveEvent")) {
            //TODO: implement; currently Save Horizon menu item disabled
            saveEvent(params);
            opStatus = "Internal Error: BHP-SU save event not implemented"; //remove when implemented
        } else if (readReq.equals("startRead")) {
            opStatus = startRead(params);
        } else if (readReq.equals("readTrace")) {
            opStatus = readTraces(params);
        } else if (readReq.equals("endRead")) {
			//NOTE: bhpViewer never sends this readReq
            opStatus = endRead(params);
        }
        
        //release the socket
        writer.releaseSocket();
        
        logger.info("opStatus: " + opStatus);
        return opStatus;
    }

    private void saveEvent(ArrayList<String> params) {
//        response.setContentType("text/plain");
//        PrintWriter owriter = response.getWriter();

        String query = params.get(1);
        //internal error if input parameter does not start with "query"
        int idx = query.indexOf('=');
        String command = query.substring(idx+1);
//        String command = request.getParameter("parameter1");

        StringTokenizer tokenizer = new StringTokenizer(params.get(2), "=");
        String outFilename = tokenizer.nextToken();
        //internal error if input parameter name not "outFilename"
        String ofname = tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "";
//        String ofname = request.getParameter("parameter2");

        tokenizer = new StringTokenizer(params.get(3), "=");
        String newValues = tokenizer.nextToken();
        //internal error if input parameter name not "newValues"
        String valueString = tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "";
//        String valueString = request.getParameter("parameter3");

        tokenizer = new StringTokenizer(params.get(4), "=");
        String _traceSize = tokenizer.nextToken();
        //internal error if input parameter name not "traceSize"
        String traceSizeString = tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "";
//        String traceSizeString = request.getParameter("parameter4");

        tokenizer = new StringTokenizer(params.get(4), "=");
        String _traceOffset = tokenizer.nextToken();
        //internal error if input parameter name not "traceOffset"
        String traceOffsetString = tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "";
//        String traceOffsetString = request.getParameter("parameter5");

        int traceSize = 0;
        int traceOffset = 0;
        StringTokenizer stk = new StringTokenizer(valueString);
        int valueLength = stk.countTokens();
        double[] values = new double[valueLength];
        try {
            int index = 0;
            while(stk.hasMoreTokens()) {
                values[index] = Double.parseDouble(stk.nextToken());
                index++;
            }
            traceSize = Integer.parseInt(traceSizeString);
            traceOffset = Integer.parseInt(traceOffsetString);
        }
        catch (Exception ex1) {
            logger.warning("In RemoteReadDatFile.saveEvent, caught: " + ex1.getMessage());
            return;
        }

        RandomAccessFile wfile = null;
        try {
            wfile = new RandomAccessFile(ofname, "rw");
        }
        catch (Exception ex2) {
            logger.warning("In RemoteReadDatFile.saveEvent, caught: " + ex2.getMessage());
            return;
        }
        if (wfile == null) {
            logger.warning("In RemoteReadDatFile.saveEvent, caught wfile == null");
            return;
        }

        byte[] bvalue = new byte[4];
        try {
            for (int i=0; i<values.length; i++) {
                wfile.seek(i*traceSize + traceOffset);
                convertToBytes(values[i], bvalue);
                wfile.write(bvalue);
            }
        } catch (Exception ex3) {
            logger.warning("In RemoteReadDatFile.saveEvent, caught: " + ex3.getMessage());
            return;
        }

        String[] cmdArray = new String[3];
        cmdArray[0] = "sh";
        cmdArray[1] = "-c";
        cmdArray[2] = command;
        try {
            Process wprocess = Runtime.getRuntime().exec(cmdArray);
            BufferedReader reader = new BufferedReader(new InputStreamReader(wprocess.getErrorStream()));
            String output = reader.readLine();
            while(output != null) {
                output = reader.readLine();
            }
            int exitValue = wprocess.waitFor();
        }
        catch (Exception wex) {
            logger.warning("In RemoteReadDatFile.saveEvent, caught: " + wex.getMessage());
            return;
        }
    }

    /**
     * Read a dataset's seismic traces to a temp file and read the dataset's 
     * metadata and write it back to the requester using a socket.
     * @param params BHP-SU command and parameters
     *
     * @return empty string if data successfully written to the socket;
     * otherwise; a string indicating the reason why the request not
     * successfully completed.
     */
    private String startRead(ArrayList<String> params) {
        ArrayList<String> stdErr;
        ArrayList<String> stdOut;

        String query = params.get(1);
        //internal error if input parameter does not start with "query"
        int idx = query.indexOf('=');
        String command = query.substring(idx+1);

        StringTokenizer tokenizer = new StringTokenizer(params.get(2), "=");
        String outFilename = tokenizer.nextToken();
        //internal error if input parameter name not "outFilename"
        String ofname = tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "";

        StringBuffer rinfo = new StringBuffer();
        String opStatus = "";
        String eol = ServletDispatcher.getServerOSLineSeparator();
        String filesep = ServletDispatcher.getServerOSFileSeparator();

        IServerJobService jobService = ServletDispatcher.acquireRemoteJobService();
        
        String bhpsuCmd = "";

        try {
            String tmpDir = ofname.substring(0, ofname.lastIndexOf(filesep));
            ofname = File.createTempFile("BHP_", ".sutmp", new File(tmpDir)).getAbsolutePath();

            // read data to temp file
            params = new ArrayList<String>();
            params.add("bash");
            params.add("-c");
            bhpsuCmd = "source ~/.bashrc; "+ command + " > " + ofname + " out=" + ofname;
            params.add(bhpsuCmd);
            params.add("true"); // doBlock
            logger.info("RemoteReadDatFile::startRead read traces to temp file: bhpsuCmd="+bhpsuCmd);
            
            // Call remote JobService directly instead of routing request since class is not
            // a qiComponent and has no Messaging Manager
            // jobService = ServletDispatcher.acquireRemoteJobService();
            // create a job ID for the job service
            String jobID = ComponentUtils.genJobID();
            jobService.setJobID(jobID);

            // Compose the job request. Note: CIDs of producer and consumer irrelevant
            // since not routing request.
            IQiWorkbenchMsg request = new QiWorkbenchMsg("", "",
                       QIWConstants.CMD_MSG, QIWConstants.SUBMIT_JOB_CMD,
                       MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
            IQiWorkbenchMsg response = jobService.processMsg(request);
            if (response == null || response.isAbnormalStatus()) {
                // return inactive job service to pool
                ServletDispatcher.releaseRemoteJobService(jobService);
                return (String)response.getContent();
            }

            // Get jobs's stderr
            params.clear();
            params.add(jobID);
            request = new QiWorkbenchMsg("", "",
                       QIWConstants.CMD_MSG, QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
                       MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
            response = jobService.processMsg(request);
            if (response == null || response.isAbnormalStatus()) {
                // return inactive job service to pool
                ServletDispatcher.releaseRemoteJobService(jobService);
                return (String)response.getContent();
            }
            StringBuffer errorOutput = new StringBuffer();
            stdErr = (ArrayList<String>)response.getContent();
            for (int i=0; i<stdErr.size(); i++) errorOutput.append(stdErr.get(i));

            // Ignore stdOut

            // Wait until the command has finished
            params.clear();
            params.add(jobID);
            request = new QiWorkbenchMsg("", "",
                       QIWConstants.CMD_MSG, QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
                       MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
            response = jobService.processMsg(request);
            if (response == null || response.isAbnormalStatus()) {
                // return inactive job service to pool
                ServletDispatcher.releaseRemoteJobService(jobService);
                return (String)response.getContent();
            }

            int exitValue = ((Integer)response.getContent()).intValue(); //readProcess.waitFor();

            if (exitValue != 0) {     // exit abnormal
                File file = new File(ofname);
                file.delete();
                // return inactive job service to pool
                ServletDispatcher.releaseRemoteJobService(jobService);
                return "BHP-SU Read Abnormal Exit ["+command+"]\n" + errorOutput.toString();
            }

            // Release the job
            params.clear();
            params.add(jobID);
            request = new QiWorkbenchMsg("", "",
                       QIWConstants.CMD_MSG, QIWConstants.RELEASE_JOB_CMD,
                       MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
            response = jobService.processMsg(request);
            if (response == null || response.isAbnormalStatus()) {
                // return inactive job service to pool
                ServletDispatcher.releaseRemoteJobService(jobService);
                return (String)response.getContent();
            }

            jobID = ComponentUtils.genJobID();
            jobService.setJobID(jobID);

            int insertIndex = command.indexOf("keys=");
            String queryParameter = command.substring(0, insertIndex) +
                                    " request=summary " +
                                    command.substring(insertIndex);
            params = new ArrayList<String>();
            params.add("bash");
            params.add("-c");
            bhpsuCmd = "source ~/.bashrc; "+ queryParameter;
            params.add(bhpsuCmd);
            // Compose the job request. Note: CIDs of producer and consumer irrelevant
            // since not routing request.
            params.add("true"); //doBlock
            logger.info("RemoteReadDatFile::startRead read summary info: bhpsuCmd="+bhpsuCmd);
            
            request = new QiWorkbenchMsg("", "",
                       QIWConstants.CMD_MSG, QIWConstants.SUBMIT_JOB_CMD,
                       MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
            response = jobService.processMsg(request);
            if (response == null || response.isAbnormalStatus()) {
                // return inactive job service to pool
                ServletDispatcher.releaseRemoteJobService(jobService);
                return (String)response.getContent();
            }

            //StreamGobbler errorGobbler = new StreamGobbler(summaryProcess.getErrorStream());
            //errorGobbler.start();

            // Get job's stderr
            params.clear();
            //params.add(locationPref);
            params.add(jobID);
            request = new QiWorkbenchMsg("", "",
                       QIWConstants.CMD_MSG, QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
                       MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
            response = jobService.processMsg(request);
            if (response == null || response.isAbnormalStatus()) {
                // return inactive job service to pool
                ServletDispatcher.releaseRemoteJobService(jobService);
                return (String)response.getContent();
            }
            errorOutput = new StringBuffer();
            stdErr = (ArrayList<String>)response.getContent();
            for (int i=0; i<stdErr.size(); i++) errorOutput.append(stdErr.get(i));

            // Get job's stdout - used for stats
            params.clear();
            //params.add(locationPref);
            params.add(jobID);
            request = new QiWorkbenchMsg("", "",
                       QIWConstants.CMD_MSG, QIWConstants.GET_JOB_OUTPUT_CMD,
                       MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
            response = jobService.processMsg(request);
            if (response == null || response.isAbnormalStatus()) {
                // return inactive job service to pool
                ServletDispatcher.releaseRemoteJobService(jobService);
                return (String)response.getContent();
            }

            stdOut = (ArrayList<String>)response.getContent();
            //InputStream summaryIps = summaryProcess.getInputStream();
            //reader = new BufferedReader(new InputStreamReader(summaryIps));
            String summaryContent = ""; //null;
            //get last line of output
            if (!stdOut.isEmpty()) {
                summaryContent = stdOut.get(stdOut.size()-1);
            }

            // Wait until the command has finished
            params.clear();
            params.add(jobID);
            request = new QiWorkbenchMsg("", "",
                       QIWConstants.CMD_MSG, QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
                       MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
            response = jobService.processMsg(request);
            if (response == null || response.isAbnormalStatus()) {
                // return inactive job service to pool
                ServletDispatcher.releaseRemoteJobService(jobService);
                return (String)response.getContent();
            }

            exitValue = ((Integer)response.getContent()).intValue(); //summaryProcess.waitFor();
            if (exitValue != 0) {  // abnormal exit
                File file = new File(ofname);
                file.delete();
                // return inactive job service to pool
                ServletDispatcher.releaseRemoteJobService(jobService);
                return "BHP-SU Read Abnormal Exit ["+queryParameter+"]\n" + errorOutput.toString();
            }

            // Release the job
            params.clear();
            params.add(jobID);
            request = new QiWorkbenchMsg("", "",
                       QIWConstants.CMD_MSG, QIWConstants.RELEASE_JOB_CMD,
                       MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
            response = jobService.processMsg(request);
            if (response == null || response.isAbnormalStatus()) {
                // return inactive job service to pool
                ServletDispatcher.releaseRemoteJobService(jobService);
                return (String)response.getContent();
            }

            rinfo.append("BHPVIEWERNAME " + ofname + eol);
            rinfo.append(summaryContent + eol);

            //increase character sequence to hold 4 bytes
            rinfo.setLength(rinfo.length()+4);
            //Add EOS to mark end of data
            byte[] bytes = rinfo.toString().getBytes();
            int len = bytes.length;
            bytes[len-4] = QIWConstants.EOS_BYTE1;
            bytes[len-3] = QIWConstants.EOS_BYTE2;
            bytes[len-2] = QIWConstants.EOS_BYTE3;
            bytes[len-1] = QIWConstants.EOS_BYTE4;

            //write metadata to SocketChannel
            opStatus = writer.write(bytes);

            // return inactive job service to pool
            ServletDispatcher.releaseRemoteJobService(jobService);

            return opStatus;
        } catch (Exception ex) {
            rinfo.append("BHPVIEWERERROR" + eol);
            rinfo.append("summary exception " + ex.toString() + eol);
            //increase character sequence to hold 4 bytes
            rinfo.setLength(rinfo.length()+4);
            //Add EOS to mark end of data
            byte[] bytes = rinfo.toString().getBytes();
            int len = bytes.length;
            bytes[len-4] = QIWConstants.EOS_BYTE1;
            bytes[len-3] = QIWConstants.EOS_BYTE2;
            bytes[len-2] = QIWConstants.EOS_BYTE3;
            bytes[len-1] = QIWConstants.EOS_BYTE4;
            
            logger.severe("RemoteReadDatFile::readTraces: Error reading metadata. CAUSE: "+ex.toString());

            opStatus = writer.write(rinfo.toString().getBytes());

            // return inactive job service to pool
            ServletDispatcher.releaseRemoteJobService(jobService);
            return opStatus;
        }
    }

    /**
     * Read a range of seismic traces from the temporary file they were written to
     * and write them back to the requester using a socket.
     *
     * @return empty string if traces successfully written to the socket;
     * otherwise; a string indicating the reason why the request not
     * successfully completed.
     */
    private String readTraces(ArrayList<String> params) {
        StringTokenizer tokenizer = new StringTokenizer(params.get(1), "=");
        String remoteFile = tokenizer.nextToken();
        //internal error if input parameter name not "remoteFile"
        String fname = tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "";
        logger.info("RemoteReadDatFile::readTraces remoteFile="+remoteFile+", fname="+fname);

        tokenizer = new StringTokenizer(params.get(2), "=");
        String start = tokenizer.nextToken();
        //internal error if input parameter name not "offset"
        int offset = Integer.parseInt(tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "");

        tokenizer = new StringTokenizer(params.get(3), "=");
        String num = tokenizer.nextToken();
        //internal error if input parameter name not "length"; number of bytes
        //to read
        int length = Integer.parseInt(tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "0");
        
        /** Error message stating why read failed */
        String opStatus = "";

       RandomAccessFile file = null;
       try {
            file = new RandomAccessFile(fname, "r");
            //allocat space for data in file + EOS
            byte[] data = new byte[length+(Integer.SIZE/8)];
            file.seek(offset);
            file.readFully(data, offset, length);
            
            //Add EOS to mark end of data
            data[length] = QIWConstants.EOS_BYTE1;
            data[length+1] = QIWConstants.EOS_BYTE2;
            data[length+2] = QIWConstants.EOS_BYTE3;
            data[length+3] = QIWConstants.EOS_BYTE4;

            //write trace to SocketChannel
            opStatus = writer.write(data);

            file.close();

            return opStatus;
        } catch (IOException ioe) {
            logger.severe("RemoteReadDatFile::readTraces: exception: "+ioe);
            return "Error obtaining BHP-SU trace(s): "+ioe.getMessage();
        } finally {
            //Gil's 1/9/8 fix for too many open files exception
            if (file != null) {
                try {
                    file.close();
                } catch (Exception ex) {
                    logger.warning("Caught exception while attempting to close dat file: " + ex.getMessage());
                }
            }
        }
    }

    /**
     * Close out read action.
     *
     * @return empty string if clean up successful;
     * otherwise; a string indicating the reason why the request not
     * successfully completed.
     */
    private String endRead(ArrayList<String> params) {
        StringTokenizer tokenizer = new StringTokenizer(params.get(1), "=");
        String fileName = tokenizer.nextToken();
        //internal error if input parameter name not "fileName"
        String fname = tokenizer.hasMoreTokens() ? tokenizer.nextToken() : "";

        File file = new File(fname);
        if (file.exists() && file.isFile()) {
            file.delete();
            logger.finest("RemoteReadDatFile::endRead: Deleted file "+fname);
            return "";
        }

        logger.severe("RemoteReadDatFile::endRead: Could not delete file "+fname);
        return "Could not delete file used to read BHP-SU data: "+fname;
    }

    private void convertToBytes(double value, byte[] bv) {
        int bits = Float.floatToIntBits((float)value);
        bv[0] = (byte)((bits & 0xff000000) >> 24);
        bv[1] = (byte)((bits & 0xff0000) >> 16);
        bv[2] = (byte)((bits & 0xff00) >> 8);
        bv[3] = (byte)(bits & 0xff);
    }
}
