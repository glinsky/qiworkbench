/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.server.services;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.util.ComponentUtils;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.IMsgHandler;
import com.bhpb.qiworkbench.messaging.ServerMessagingManager;
import com.bhpb.qiworkbench.workbench.QiComponentBase;

/**
 * Remote IO service, a core component of qiWorkbench. It is started as a thread
 * by the Messenger Dispatcher and waits for an IO command. Remote IO means the
 * files are accessible from the machine running the Tomcat server. This is in
 * contrast to local IO which means the files are accessible from the user's
 * machine. By accessible we mean files on the machine or in a cross-mounted
 * file system. When Tomcat is running on the user's machine, local and remote
 * IO are the same.
 * <p>
 * There is one IO service per IO command. That is, an IO service only receives
 * one IO request which it processes.
 */
public class IOService extends QiComponentBase implements IqiWorkbenchComponent, Runnable {
    private static Logger logger = Logger.getLogger(IOService.class.getName());

    /** Messaging manager for service */
    private ServerMessagingManager messagingMgr;

    /** CID for component instance. Generated before the thread is started and carried as the thread's name. */
    private String myCID = "";

    public String getCID() {
        return myCID;
    }

    public IComponentDescriptor getComponentDesc() {
        return messagingMgr.getComponentDesc();
    }

    public IMsgHandler getMsgHandler() {
        return messagingMgr.getMsgHandler();
    }

    public ServerMessagingManager getMessagingMgr() {
        return messagingMgr;
    }

    /**
     * Initialize the IO service. Create its messaging manager which
     * creates it message handler and component descriptor.
     */
    public void init() {
        try {
            QIWConstants.SYNC_LOCK.lock();

            myCID = Thread.currentThread().getName();
            // create service's messaging manager which creates it message handler
            // and component descriptor
            this.messagingMgr = new ServerMessagingManager(myCID);

            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in IOService.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
            // notify the startup method initialization finished
            QIWConstants.SYNC_LOCK.unlock();
        }
    }

    /** Initialize the service.
     */
    public void run() {
        // initialize the IO service
        init();

        // Process IO request made on behalf of the Servlet Dispatcher for
        // another (usually client) component. The Servlet Dispatcher will send
        // the response back to the Message Dispatcher who will route it to the
        // requester.
        while(true) {
            // if queue is empty; blocks until request message added to queue
//            QiWorkbenchMsg msg = msgHandler.dequeue();
            IQiWorkbenchMsg msg = messagingMgr.getNextMsgWait();

            IQiWorkbenchMsg resp = processMsg(msg);

            // put response on queue; wait for it to be received
//            msgHandler.enqueue(resp);
            messagingMgr.putMsgWait(resp);
        }
    }

    /**
     * Process the request message.
     *
     * @param msg Request message
     * @return Response message; abnormal response if IO error
     */
    public IQiWorkbenchMsg processMsg(IQiWorkbenchMsg msg) {
        /** Request that matches the response */
        IQiWorkbenchMsg response = null;

        //log message traffic
        logger.fine("server IOService::procssMsg: msg="+msg.toString());

        //Check if a request. If so, process and send back a response
        if (msg.getMsgKind().equals(QIWConstants.CMD_MSG)) {
            String cmd = msg.getCommand();
            if (cmd.equals(QIWConstants.CREATE_REMOTE_DIRECTORY_CMD)) {
                String dirPath = (String)msg.getContent();
                try {
                    File dir = new File(dirPath);
                    if(dir.mkdirs())
                        response = new QiWorkbenchMsg(msg.getConsumerCID(),
                            msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                                    msg.getMsgID(), QIWConstants.STRING_TYPE, "success",msg.skip());
                    else
                        response = new QiWorkbenchMsg(msg.getConsumerCID(),
                            msg.getProducerCID(), QIWConstants.STRING_TYPE, msg.getCommand(),
                                    msg.getMsgID(), Boolean.class.getName(), "failure",msg.skip());
                    response.setStatusCode(MsgStatus.SC_OK);
                }catch(Exception qioe) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qioe.getMessage());
                    return response;
                }
                return response;
            } else if (cmd.equals(QIWConstants.CREATE_DIRECTORIES_CMD)) {
                List<String> dirs = (List<String>)msg.getContent();

                try {
                    boolean pass = true;
                    for(int i = 0; dirs != null && i < dirs.size(); i++){
                        String name = dirs.get(i);
                        File dir = new File(name);
                        if(!dir.mkdirs()){
                            pass = false;
                            break;
                        }
                    }
                    if(pass)
                        response = new QiWorkbenchMsg(msg.getConsumerCID(),
                            msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                                    msg.getMsgID(), QIWConstants.STRING_TYPE, "success",msg.skip());
                    else
                        response = new QiWorkbenchMsg(msg.getConsumerCID(),
                                msg.getProducerCID(), QIWConstants.STRING_TYPE, msg.getCommand(),
                                        msg.getMsgID(), Boolean.class.getName(), "failure",msg.skip());
                } catch(Exception qioe) {
                    qioe.printStackTrace();
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qioe.getMessage());
                    return response;
                }
                return response;
            } else
            if (cmd.equals(QIWConstants.CHECK_REMOTE_FILE_EXIST_CMD)) {
                String filePath = (String)msg.getContent();
                try {
                    File file = new File(filePath);
                    if(file.exists())
                        response = new QiWorkbenchMsg(msg.getConsumerCID(),
                        msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                                msg.getMsgID(), QIWConstants.STRING_TYPE, "yes",msg.skip());
                    else
                        response = new QiWorkbenchMsg(msg.getConsumerCID(),
                                msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                                        msg.getMsgID(), QIWConstants.STRING_TYPE, "no",msg.skip());
                    response.setStatusCode(MsgStatus.SC_OK);
                }catch(Exception e){
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+ e.getMessage());
                    return response;
                }
                return response;
            } else

            if (cmd.equals(QIWConstants.GET_REMOTE_DIR_FILE_LIST_CMD)) {
                String dirPath = (String)msg.getContent();

                ArrayList files = null;
                try {
                    files = new RemoteGetFileList().getDirectoryFileList(dirPath);
                } catch (QiwIOException qioe) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qioe.getMessage());
                    return response;
                }
                // send a normal response back
                response = new QiWorkbenchMsg(msg.getConsumerCID(),
                               msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                               msg.getMsgID(), QIWConstants.ARRAYLIST_TYPE, files,msg.skip());
                response.setStatusCode(MsgStatus.SC_OK);
                return response;
            } else

            if (cmd.equals(QIWConstants.GET_REMOTE_FILE_LIST_CMD)) {
                String dirPath = (String)msg.getContent();
                ArrayList files = null;
                try {
                    files = new RemoteGetFileList().getFileList(dirPath);
                } catch (QiwIOException qioe) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qioe.getMessage());
                    return response;
                }
                // send a normal response back
                response = new QiWorkbenchMsg(msg.getConsumerCID(),
                               msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                               msg.getMsgID(), QIWConstants.ARRAYLIST_TYPE, files);
                response.setStatusCode(MsgStatus.SC_OK);
                return response;
            } else

            if (cmd.equals(QIWConstants.REMOTE_FILE_READ_CMD)) {
                String filePath = (String)msg.getContent();
                ArrayList fileLines = null;
                try {
                    fileLines = new RemoteReadFile().readFile(filePath);
                } catch (QiwIOException qioe) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qioe.getMessage());
                    return response;
                }
                //Send a normal response back
                response = new QiWorkbenchMsg(msg.getConsumerCID(),
                               msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                               msg.getMsgID(), QIWConstants.ARRAYLIST_TYPE, fileLines);
                return response;
            } else

            if (cmd.equals(QIWConstants.GET_UNMADE_QIPROJ_DESC_DIRS_CMD)) {
                QiProjectDescriptor projDesc = (QiProjectDescriptor)msg.getContent();
                List<File> dirs = null;
                dirs = new RemoteGetFileList().getUnMadeDirs(projDesc,File.separator);
                List<String> listString = new ArrayList<String>();
                for(int i = 0; dirs != null && i < dirs.size(); i++){
                    listString.add(dirs.get(i).getAbsolutePath());
                }
                //Send a normal response back
                response = new QiWorkbenchMsg(msg.getConsumerCID(),
                               msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                               msg.getMsgID(), QIWConstants.ARRAYLIST_TYPE, listString);
                return response;
            } else

            if (cmd.equals(QIWConstants.REMOTE_FILE_WRITE_CMD)) {
                ArrayList<String> fileItems = (ArrayList<String>)msg.getContent();
                try {
                    new RemoteWriteFile().writeFile(fileItems);
                } catch (QiwIOException qioe) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qioe.getMessage());
                    return response;
                }
                //Send an acknowledge response back indicating file successfully
                //written.
                response = new QiWorkbenchMsg(msg.getConsumerCID(),
                               msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                               msg.getMsgID(), "", "",msg.skip());
                return response;
            } else

            if (cmd.equals(QIWConstants.REMOTE_BINARY_FILE_READ_CMD)) {
				ArrayList params = (ArrayList)msg.getContent();
				String filePath = (String)params.get(0);
				Long lOffset = (Long)params.get(1);
				Long lLen = (Long)params.get(2);
				long offset = lOffset.longValue();
				long len = lLen.longValue();
				byte[] bs = new byte[0];
				try {
					bs = new RemoteReadBinaryFile().readFile(filePath,offset,len);
                } catch (QiwIOException qioe) {
					//send an abnormal response back
					response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qioe.getMessage());
					return response;
				}
				//Send a normal response back
				response = new QiWorkbenchMsg(msg.getConsumerCID(),
					   msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
									   msg.getMsgID(), QIWConstants.ARRAYLIST_TYPE, bs);
                return response;
            } else

            if (cmd.equals(QIWConstants.REMOTE_BINARY_FILE_WRITE_CMD)) {
                ArrayList fileItems = (ArrayList)msg.getContent();
                try {
                    new RemoteWriteBinaryFile().writeFile(fileItems);
                } catch (QiwIOException qioe) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qioe.getMessage());
                    return response;
                }
                //Send an acknowledge response back indicating file successfully
                //written.
                response = new QiWorkbenchMsg(msg.getConsumerCID(),
                               msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                               msg.getMsgID(), "", "");
                return response;
            } else

            if (cmd.equals(QIWConstants.REMOTE_READ_SEGY_DATA_CMD)) {
                ArrayList<String> params = (ArrayList<String>)msg.getContent();

                //read SEGY data and send it to the client via socket channel
                String opStatus = new RemoteReadSegyData().processReadRequest(params);

                //Check if read request performed successfully
                if (!opStatus.equals("")) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, opStatus);
                    return response;
                }

                //Send an acknowledge response back indicating data
                //successfully transmitted via socket.
                response = new QiWorkbenchMsg(msg.getConsumerCID(),
                               msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                               msg.getMsgID(), "", "");
                return response;
            } else

            if (cmd.equals(QIWConstants.REMOTE_READ_BHPSU_DATA_CMD)) {
                ArrayList<String> params = (ArrayList<String>)msg.getContent();

                //read BHP-SU data and send it to the client via socket channel
                String opStatus = new RemoteReadDatFile().processReadRequest(params);

                //Check if read request performed successfully
                if (!opStatus.equals("")) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, opStatus);
                    return response;
                }

                //Send an acknowledge response back indicating data
                //successfully transmitted via socket.
                response = new QiWorkbenchMsg(msg.getConsumerCID(),
                               msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                               msg.getMsgID(), "", "");
                return response;
            }
        }

        logger.warning("IO message not processed:"+msg.toString());
        return null;
    }

    /** Launch the IO service:
     *  <ul>
     *  <li>Start up the service thread which will initialize the service.</li>
     *  </ul>
     * <p>
     * NOTE: Each thread's init() must finish before the next thread is
     * started. This is accompished by monitoring the SYNC_LOCK object.
     */
    public static void main(String[] args) {
        IOService ioServiceInstance = new IOService();
        // get a CID for the new component instance
        String cid = ComponentUtils.genCID(IOService.class);
        // use the CID as the name of the thread
        Thread serviceThread = new Thread(ioServiceInstance, cid);
        serviceThread.start();
        long threadId = serviceThread.getId();
        logger.info("Remote IO Service Thread-"+Long.toString(threadId)+" started");

        // When the services's init() is finished, it will release the lock
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();
    }
}