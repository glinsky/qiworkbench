/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.server.multipart;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.StringBuffer;
import java.util.logging.Logger;

import javax.servlet.ServletInputStream;

/**
 * The message partition of a multipart POST. The message in the partition
 * is read and saved until obtained by the getMsg() method.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class MsgPartition implements IPartition {
    private static Logger logger = Logger.getLogger(MsgPartition.class.getName());

    /** The XML serialization of the message */
    private byte[] msg;

    /** Name of the message partition */
    private String msgPartitionName;

    /** Content encoding */
    private String encoding;

    /**
     * Construct a message partition. Extract the message out of the message
     * partition in the multipart POST request.
     *
     * @param msgPartitionName The name of the message partition.
     * @param sis The Servlet input stream containing the message.
     * @param delimiter The boundary delimiter line that marks the end of the message.
     * @param encoding The encoding of the message.
     */
    MsgPartition(String msgPartitionName, ServletInputStream sis, String delimiter, String encoding) throws IOException {
        this.msgPartitionName = msgPartitionName;
        this.encoding = encoding;

        // Extract the message out of the message partition
        msg = extractMsg(sis, delimiter);
    }

    /**
     * Extract the message from the message partition. This advances the servlet input
     * stream to the next partition, namely, the file partition.
     *
     * @param sis The Servlet input stream containing the message.
     * @param delimiter The boundary delimiter line that marks the end of the message.
     */
    private byte[] extractMsg(ServletInputStream sis, String delimiter) throws IOException {
        // Read the message into a byte array
        PartitionInputStream pis = new PartitionInputStream(sis, delimiter);
        // Note: Output buffer automatically grows
        ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
        byte[] buff = new byte[256];

        int bytesRead;
        while ((bytesRead = pis.read(buff)) != -1) {
            baos.write(buff, 0, bytesRead);
        }

        // Close the IO streams
        baos.close(); pis.close();

        // Extract the message from the output stream
        return baos.toByteArray();
    }

    /**
     * Get the name of this partition.
     *
     * @return The name of the partition.
     */
    public String getPartitionName() {
        return msgPartitionName;
    }

    /**
     * Get the message in the specified encoding.
     *
     * @return Message as a string
     */
    public String getMsg() throws UnsupportedEncodingException {
        return new String(msg, encoding);
    }

    /**
     * Indicate this is a message partition.
     *
     * @return true.
     */
    public boolean isMsgPartition() {
        return true;
    }

    /**
     * Indicate this is NOT a file partition.
     *
     * @return true.
     */
    public boolean isFilePartition() {
        return false;
    }

    public String toString() {
        StringBuffer sbuf = new StringBuffer();
        sbuf.append("MsgPartition: msgPartitionName="+this.getPartitionName());
        sbuf.append("\n    encoding="+encoding);
        sbuf.append("\n    msg=");
        try {
            sbuf.append(this.getMsg());
        } catch (UnsupportedEncodingException uee) {
            //should not happen
        }

        return sbuf.toString();
    }
}
