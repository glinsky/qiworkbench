/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.server.multipart;

/**
 * Interface for a (message or file) partition of a multipart POST.
 *
 * @see MsgPartition
 * @see FilePartition
 *
 * @author Gil Hansen
 * @version 1.0
 */
public interface IPartition {
    /**
     * Get the name of this partition.
     *
     * @return The name of the partition.
     */
    public String getPartitionName();

    /**
     * Check if this is a message partition.
     *
     * @return true if this is a MsgPartition; otherwise, false.
     */
    public boolean isMsgPartition();

    /**
     * Check if this is a file partition.
     *
     * @return true if this is a FilePartition; otherwise, false.
     */
    public boolean isFilePartition();
}
