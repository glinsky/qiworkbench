/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.server.multipart;

import java.io.IOException;
import java.lang.StringBuffer;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletInputStream;

import com.bhpb.qiworkbench.server.multipart.FilePartition;
import com.bhpb.qiworkbench.server.multipart.MsgPartition;

/**
 * Parser for a multipart POST multipart request. A request is divided into
 * partitions delineated by a boundary. The parser will parse out each
 * partition. Typically there are two partitions: a XML serialized message
 * partition and a file partition.
 * <p>
 * The multipart specification is documented in RFC 1867,
 * available at <a href="http://www.ietf.org/rfc/rfc1867.txt">
 * http://www.ietf.org/rfc/rfc1867.txt</a>.
 *
 * @see MsgPartition
 * @see FilePartition
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class MultiPartitionParser {
    private static Logger logger = Logger.getLogger(MultiPartitionParser.class.getName());

    private final int BUFFER_LENGTH = 1024*8;

    /** Input stream containing partitions of the multipart POST */
    private ServletInputStream sis;

    /** Parts delimiter */
    private String boundary;

    /** The specified charset encoding for a partition's value. Initialized to the default */
    private String encoding = "ISO-8859-1";

    /**
     * Create a parser for a multipart request. Get the boundary delimiter
     * from the content type and position to the first boundary line after
     * which is the first partition.
     *
     * @param request The servlet request.
     */
    public MultiPartitionParser(HttpServletRequest request) throws IOException {
        //Note: Content type starts with "multipart/form-data"
        String ct = request.getContentType();

        //Extract the boundary contained in the content type line and save
        String boundary = parseBoundary(ct);
        this.boundary = boundary;
        logger.finest("boundary: "+boundary);

        //Get the Servlet input stream containing the partitions each separated
        //by a boundary.
        sis = request.getInputStream();

        //Position to the first boundary line
        while (true) {
            String line = readLine();
            if (line == null)
                throw new IOException("Misformed multipart POST: missing boundary");
            if (line.startsWith(boundary)) break;
        }
    }

    /**
     * Scan the next partition from the input stream. It will either be a
     * MsgPartition or a FilePartition. Uses the charset encoding specified
     * in the content type. The boundary line of the last partition has
     * already been scanned.
     * <p>
     * The end of the partition is marked by a boundary line which is scanned.
     * Between the two boundary line is a set of headers, a blank line and
     * the value of the partition.
     * <p>A typical set of headers is:
     * <br>    Content-Disposition: form-data; name="field1"; filename="file1.txt"
     * <br>    Content-Type: type/subtype; charset=ISO-8859-1
     * <br>    Content-Transfer-Encoding: 8bit
     *
     * @return Either a MsgPartition, FilePartition or null if no more partitions
     * @exception IOException if an IO exception occurred.
     */
    public IPartition scanNextPartition() throws IOException {
        // Scan the headers (if present) terminated by a blank line
        ArrayList<String> headers = new ArrayList<String>();

        String line = readLine();
        if (line == null || line.length() == 0) return null;

        //Read the headers until an empty line is reached
        //Note: A line starting with white space (blank or tab) is a continuation line
        while (line != null && line.length() > 0) {
            String nextLine = null;
            while (true) {
                nextLine = readLine();
                // append continutation lines
                if (nextLine != null &&
                    (nextLine.startsWith(" ") || nextLine.startsWith("\t"))) {
                    line = line + nextLine;
                } else break;
            }

            //Add the line to the list of headers
            headers.add(line);
            logger.finest("header line: "+line);
            line = nextLine;
        }

        if (line == null) return null;

        String partitionName = null;
        String filename = null;
        String pathname = null;
        String contentType = "";

        // Extract information from the headers
        for (int i=0; i<headers.size(); i++) {
            String header = headers.get(i);
            if (header.toLowerCase().startsWith("content-disposition:")) {
                String[] dispInfo = parseDispositionInfo(header);
                partitionName = dispInfo[1];
                filename = dispInfo[2];
                pathname = dispInfo[3];
            } else if (header.toLowerCase().startsWith("content-type:")) {
                String type = parseContentType(header);
                contentType = type != null ? type : "text/plain";
                String charset = parseCharset(header);
                encoding = charset != null ? charset : "ISO-8859-1";
                logger.finest("encoding: "+encoding);
            }
        }

        // Scan the content of the partition which is terminated by a boundary delimiter
        if (filename == null) {
            // A message partition
            return new MsgPartition(partitionName, sis, boundary, encoding);
        } else {
            // A file partition
            if (filename.equals("")) filename = null;

            return new FilePartition(partitionName, sis, boundary, encoding, contentType, filename, pathname);
        }
    }

    /** Buffer used by readLine() */
    private byte[] buff = new byte[BUFFER_LENGTH];

    /**
     * Read the next line of a multipart POST input.
     *
     * @return  The next line of input from the stream, or null if EOF
     * @exception IOException if an IO exception occurred.
     */
    private String readLine() throws IOException {
        int bytesRead;
        StringBuffer sbuff = new StringBuffer();

        do {
            bytesRead = sis.readLine(buff, 0, BUFFER_LENGTH);
            if (bytesRead != -1) {
                sbuff.append(new String(buff, 0, bytesRead, encoding));
            }
        } while (bytesRead == BUFFER_LENGTH);

        int len = sbuff.length();
        if (len == 0) return null;

        // Remove the trailing \n or \r\n
        if (len >= 2 && sbuff.charAt(len-2) == '\r') {
            sbuff.setLength(len-2);  // remove \r\n
        } else if (len >= 1 && sbuff.charAt(len-1) == '\n') {
            sbuff.setLength(len-1);  // remove \n
        }

        return sbuff.toString();
    }

    /**
     * Parse the boundary value from a "content-type:" line.
     *
     * @return The boundary line.
     */
    private String parseBoundary(String line) {
        int idx = line.lastIndexOf("boundary=");
        if (idx == -1) return "";

        String boundary = line.substring(idx+9);  // skip "boundary="
        // Strip off double quotes (if any) enclosing the value
        if (boundary.charAt(0) == '"') {
            idx = boundary.lastIndexOf('"');
            boundary = boundary.substring(1, idx);
        }

        //Note: A boundary line always starts with "--"
        return "--" + boundary;
    }

    /**
     * Parse disposition info from a "content-disposition:" line
     *
     * @return String[] of elements: disposition, name, filename (if any).
     * @exception IOException if the line is malformatted.
     */
    private String[] parseDispositionInfo(String line) throws IOException {
        // Return the line's data as an array: disposition, name, filename
        String[] dispInfo = new String[4];

        String origline = line;
        line = origline.toLowerCase();

        // Get the content disposition: "form-data"
        int start = line.indexOf("content-disposition: ");
        int end = line.indexOf(";");
        if (start == -1 || end == -1) {
            throw new IOException("Content disposition corrupt: " + origline);
        }
        String disp = line.substring(start+21, end);
        if (!disp.equals("form-data")) {
            throw new IOException("Invalid content disposition: " + disp);
        }

        // Get the partition name
        start = line.indexOf("name=\"", end);  // start at last semicolon
        end = line.indexOf("\"", start + 7);   // skip name=\"
        int startOffset = 6;
        if (start == -1 || end == -1) {
            start = line.indexOf("name=", end);
            end = line.indexOf(";", start + 6);
            if (start == -1) {
                throw new IOException("Content disposition has no field name: " + origline);
            } else if (end == -1) {
                end = line.length();
            }
            startOffset = 5;
        }
        String partitionName = origline.substring(start + startOffset, end);

        // Get the filename (if any) and pathname
        String filename = null;
        String pathname = null;
        start = line.indexOf("filename=\"", end + 2);
        end = line.indexOf("\"", start + 10);
        if (start != -1 && end != -1) {
            filename = origline.substring(start + 10, end);
            pathname = filename;
            // If the filename is the full path, extract the filename.
            int slash =
            Math.max(filename.lastIndexOf('/'), filename.lastIndexOf('\\'));
            if (slash > -1) {
                filename = filename.substring(slash + 1);
            }
        }

        dispInfo[0] = disp;
        dispInfo[1] = partitionName;
        dispInfo[2] = filename;
        dispInfo[3] = pathname;
        return dispInfo;
    }

    /**
     * Parse the content type from a line. It is between "content-type:"
     * and the first semicolon.
     *
     * @return Content type or null if the line was empty.
     * @exception IOException if the line is malformatted.
     */
    private String parseContentType(String line) throws IOException {
        line = line.toLowerCase();

        int end = line.indexOf(";");
        if (end == -1) end = line.length();

        return line.substring("content-type:".length(), end).trim();
    }

    /**
     * Parse the charset (if any) from a content type line. It occurs
     * after the "content-type:" that is terminated by a semicolor
     *
     * @return charset or null if none was specified
     * @exception IOException if the line is malformatted.
     */
    private String parseCharset(String line) throws IOException {
        line = line.toLowerCase();
        int start = line.indexOf("charset=");
        if (start == -1) return null;

        return line.substring(start+8);
    }
}
