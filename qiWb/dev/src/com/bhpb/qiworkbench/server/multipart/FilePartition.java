/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.server.multipart;

import java.io.File;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.ServletInputStream;

/**
 * The file partition of a multipart POST. The content of the file in the
 * partition is read and written out on-demand, i.e., by the writeToFile
 * method.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class FilePartition implements IPartition {
    private final int BUFFER_LENGTH = 1024*8;

    /** Name of the file partition */
    private String filePartitionName;

    /** Name of the file  */
    private String filename;

    /** Path of the file */
    private String pathname;

    /** Content encoding */
    private String encoding;

    /** Content type of the file */
    private String contentType;

    /** Input stream containing file data */
    private PartitionInputStream pis;

    /**
     * Construct a file partition. File partitions must be read in order.
     *
     *
     * @param filePartitionName The name of the file partition.
     * @param sis The ServletInputStream containing the file's content.
     * @param delimiter The boundary delimiter that marks the end of the file partition.
     * @param encoding The encoding of the file content.
     * @param contentType The content type of the file.
     * @param filename The file system name of the file.
     * @param pathname The full path of the file.
     *
     * @exception IOException If an IO exception occurred.
     */
    FilePartition(String filePartitionName, ServletInputStream sis, String delimiter, String encoding, String contentType, String filename, String pathname) throws IOException {
        this.filePartitionName = filePartitionName;
        this.filename = filename;
        this.pathname = pathname;
        this.encoding = encoding;
        this.contentType = contentType;
        pis = new PartitionInputStream(sis, delimiter);
    }

    /**
     * Get the name of this partition.
     *
     * @return The name of the partition.
     */
    public String getPartitionName() {
        return filePartitionName;
    }

    /**
     * Write the content of the file partition to a file. If the pathname
     * is a file, write to that file. If it is a directory, write to that
     * directory with the specified file name.
     * <p> The implicit parameters are the filename and pathname.
     *
     * @return Number of bytes written.
     * @exception IOException If an IO exception occurred.
     */
    public long writeToFile() throws IOException {
        if (filename == null || pathname == null) return 0;

        long bytesWrote = 0;

        OutputStream fos = null;
        try {
            File f = new File(pathname);
            File file = f.isDirectory() ? new File(f, filename) : f;

            fos = new BufferedOutputStream(new FileOutputStream(file));
            bytesWrote = writeToStream(fos);
        } finally {
            if (fos != null) fos.close();
        }

        return bytesWrote;
    }

    /**
     * Read the content of the file partition and write it to an output stream.
     *
     * @param os Output stream to write file content to.
     * @return Number of bytes written.
     * @exception IOException If an IO exception has occurred.
     */
    public long writeToStream(OutputStream os) throws IOException {
        long bytesWrote = 0;
        int bytesRead;
        byte[] buff = new byte[BUFFER_LENGTH];

        if (filename == null) return 0;

        // Read the file's content; end of content marked by a boundary line
        while((bytesRead = pis.read(buff)) != -1) {
            os.write(buff, 0, bytesRead);
            bytesWrote += bytesRead;
        }

        return bytesWrote;
    }

    /**
     * Indicate this is a file partition..
     *
     * @return true.
     */
    public boolean isFilePartition() {
        return true;
    }

    /**
     * Indicate this is NOT a message partition..
     *
     * @return true.
     */
    public boolean isMsgPartition() {
        return false;
    }

    public String toString() {
        StringBuffer sbuf = new StringBuffer();
        sbuf.append("FilePartition: filePartitionName="+this.getPartitionName()+", fileName="+filename+", pathName="+pathname);
        sbuf.append("\n    encoding="+encoding+", contentType="+contentType);

        return sbuf.toString();
    }
}
