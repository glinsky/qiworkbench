/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.server.multipart;

import java.io.FilterInputStream;
import java.io.IOException;
import java.lang.IllegalStateException;
import java.util.logging.Logger;

import javax.servlet.ServletInputStream;

/**
 * Filter a ServletInputStream containing the next partition in a multipart
 * POST; the end of the partition is marked by the specified boundary.
 * <p>Trims the "\r\n" added by ServletInputStream's readLine() method.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class PartitionInputStream extends FilterInputStream {
    private static Logger logger = Logger.getLogger(PartitionInputStream.class.getName());

    /** boundary line which marks the end of the partition */
    private String boundary;

    private final int BUFFER_LENGTH = 1024*64;

    /** input buffer */
    private byte [] inBuf = new byte[BUFFER_LENGTH];

    /** number of bytes already read into the input buffer */
    private int bytesRead  = 0;

    /** current position in the buffer */
    private int curpos = 0;

    /** end-of-partition flag indicating if the boundary line has been reached */
    private boolean eop = false;

    /**
     * Creates a <code>PartitionInputStream</code> which stops at the specified
     * boundary delimiter from a <code>ServletInputStream<code>.
     *
     * @param sis The ServletInputStream containing the partition.
     * @param delimiter The boundary delimiter marking the end of the partition.
     */
    PartitionInputStream(ServletInputStream sis, String boundary) throws IOException {
        super(sis);
        this.boundary = boundary;
    }

    /**
     * Fill up the input buffer from the ServletInputStream and check if
     * the end-of-partition delimiter (boundary) was read.
     * <p>Assumes the input buffer has exactly 2 unused characters (except
     * on the first call) which are used if a boundary is not the first
     * line read.
     */
    private void fillBuffer() throws IOException {
        if (eop) return;

        if (bytesRead > 0) {
            // check if there are 2 spare characters in the buffer
            if (bytesRead - curpos == 2) {
                // copy the spare chars to the start of the buffer
                System.arraycopy(inBuf, curpos, inBuf, 0, bytesRead - curpos);
                bytesRead -= curpos;
                curpos = 0;
            } else {
                // should never happen
                throw new IllegalStateException("fillBuffer() detected illegal buffer state");
            }
        }

        //Fill the entire buffer, starting at bytesRead, without splitting
        //a boundary.
        int read = 0;
        int boundaryLength = boundary.length();
        int maxRead = BUFFER_LENGTH - boundaryLength - 2;  // -2 is for /r/n
        while (bytesRead < maxRead) {
            read = ((ServletInputStream)in).readLine(inBuf, bytesRead, BUFFER_LENGTH - bytesRead);
            // heck if reached end-of-file
            if (read == -1) {
                throw new IOException("Unexpected end of partition");
            } else {
                // Check if the boundary read
                if (read >= boundaryLength) {
                    eop = true;
                    for (int i=0; i<boundaryLength; i++) {
                        if (boundary.charAt(i) != inBuf[bytesRead + i]) {
                            // boundary not read
                            eop = false;
                            break;
                        }
                    }
                    // If boundary read, ignore (do not include)
                    if (eop) break;
                }
            }
            // boundary not read, accept bytes into buffer
            bytesRead += read;
        }
    }

    /**
     * Read the next portion of the partition into the provided buffer.
     * <p>Implicit parameters are the input buffer, inBuf, into which
     * the content of the partition is read and the number of bytes,
     * bytesRead, read into inBuf.
     *
     * @param buf The buffer into which the partition data is read.
     * @return The total number of bytes read into the buffer; -1
     * when the partition's boundary has been reached.
     * @exception IOException If an IO error occurred.
     */
    public int read(byte buf[]) throws IOException {
        int totalBytesRead = 0;
        int bufLen = buf.length;

        if (bufLen == 0) return 0;

        int availBytes = bytesRead - curpos - 2;
        if (availBytes <= 0) {
            fillBuffer();
            availBytes = bytesRead - curpos - 2;
            if (availBytes <= 0) return -1;
        }

        int len = Math.min(bufLen, availBytes);
        System.arraycopy(inBuf, curpos, buf, 0, len);
        curpos += len;
        totalBytesRead += len;

        while (totalBytesRead < bufLen) {
            fillBuffer();
            availBytes = bytesRead - curpos - 2;
            if (availBytes <= 0) return totalBytesRead;
            len = Math.min(bufLen - totalBytesRead, availBytes);
            System.arraycopy(inBuf, curpos, buf, totalBytesRead, len);
            curpos += len;
            totalBytesRead += len;
        }

        return totalBytesRead;
    }

    /**
     * Returns the number of bytes that can be read from this input stream
     * without blocking.
     *
     * @return The number of bytes that can be read from the input stream
     * without blocking.
     * @exception IOException If an IO error occurred.
     */
    public int available() throws IOException {
        int avail = (bytesRead - curpos - 2) + in.available();
        // The number of available bytes cannot be negative
        return (avail < 0 ? 0 : avail);
    }

    /**
     * Close this input stream and releases any system resources
     * associated with the stream. Read any unread data in the partition
     * so positioned to read the next partition.
     *
     * @exception IOException If an IO error occurred.
     */
    public void close() throws IOException {
        if (!eop) {
            // Position to the next partition
            while (read(inBuf) != -1);
        }
    }
}
