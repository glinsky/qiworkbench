/*
 * ClientVersionChecker.java
 *
 * @author Woody Folsom woody.folsom@bhpbilliton.com:
 */

package com.bhpb.qiworkbench.server;

import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.util.PropertyUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Logger;

/**
 *
 * @author folsw9
 */
public class ClientVersionChecker {
    private static final Logger logger = Logger.getLogger(ClientVersionChecker.class.toString());

    private String serverVersion = null;
    private String serverBuild = null;

    public static enum CHECK_RESULT { COMPATIBLE,
                                CLIENT_VERSION_INCOMPATIBLE,
                                CLIENT_BUILD_INCOMPATIBLE,
                                SERVER_VERSION_INCOMPATIBLE,
                                SERVER_BUILD_INCOMPATIBLE,
                                EXACT_MATCH_FAILURE };

    public String getServerVersion() {
        return serverVersion;
    }

    public String getServerBuild() {
        return serverBuild;
    }

    public ClientVersionChecker() {
        Properties props = null;

        try {
            props = PropertyUtils.loadProperties();
            serverVersion = props.getProperty("project.version");
            serverBuild = props.getProperty("project.build");
        } catch (IOException ioe) {
            logger.warning("Could not get properties from PropertyUtils.loadProperties() due to IOException: " + ioe.getMessage() + ". Calls to getResponse() will produce abnormal reponses.");
        }
    }

    private Boolean getCompatibilityFlag(CHECK_RESULT result) {
        switch (result) {
            case CLIENT_VERSION_INCOMPATIBLE :
            case CLIENT_BUILD_INCOMPATIBLE :
            case SERVER_VERSION_INCOMPATIBLE :
            case SERVER_BUILD_INCOMPATIBLE :
            case EXACT_MATCH_FAILURE :
                return Boolean.FALSE;
            case COMPATIBLE :
                return Boolean.TRUE;
            default :
                throw new IllegalArgumentException("Unknown CHECK_RESULT enumerated value: " + result.toString());
        }

    }
    /**
     * Determines compatibility of this server with a client string.
     * See version.properties for more information.
     *
     * @param clientVersion String of the format project build.version-project.version
     */
    private CHECK_RESULT isValidClientVersion(String clientVersion, String clientBuild) {

        if (clientVersion == null)
            throw new IllegalArgumentException("Cannot determine client-server compatibility - clientVersion parameter is null.");

        if (clientBuild == null)
            throw new IllegalArgumentException("Cannot determine client-server compatibility - clientBuild parameter is null.");

        if ((clientVersion.compareTo(serverVersion) == 0) && (clientBuild.compareTo(serverBuild) == 0))
            return CHECK_RESULT.COMPATIBLE;

        return CHECK_RESULT.EXACT_MATCH_FAILURE;
    }

    /**
     * Returns a partial response describing the compatibility of the client and server.
     * This will be a response with code SC_OK, and a Map containing a string key compatible and Boolean value, and a String key
     * reason and String value describing the compatibility including client and server versions.
     *
     * This caller is responsible for setting the message's producer and consumer appropriately as this method does not require those variables to determine compatibility.
     */
    public IQiWorkbenchMsg getReponse(IQiWorkbenchMsg request) {
        IQiWorkbenchMsg response = new QiWorkbenchMsg();

        if (request == null) {
            logger.info("Returning abnormal response because request is null");
            response = getAbnormalReponse(request, "request message is null");
        } else {
            response = new QiWorkbenchMsg();
            response.setCommand(request.getCommand());
        }

        if ((serverVersion == null) || (serverBuild == null))
        {
            logger.info("Returning abnormal response because serverVersion and/or serverbuild is null");
            response = getAbnormalReponse(request, "serverVersion and/or serverBuild could not be read from version.properties file");
        }
        else
        {
            try {
                logger.info("Setting msgkind and content...");
                response.setMsgKind(QIWConstants.DATA_MSG);
                response.setContentType(QIWConstants.HASH_MAP_TYPE);
                logger.info("done");

                HashMap contentHashMap = new HashMap();

                logger.info("Getting project version and build from hashmap...");
                String clientVersion = request.getValueFromContentHashMap("project.version").toString();
                String clientBuild = request.getValueFromContentHashMap("project.build").toString();
                logger.info("done");

                logger.info("Checking isValidClientVersion and getCompatibilityFlag...");
                CHECK_RESULT result = isValidClientVersion(clientVersion, clientBuild);
                Boolean compatibilityFlag = getCompatibilityFlag(result);
                logger.info("done");

                String compatibilityReason = "Unknown - compatibility check not completed";

                logger.info("Formatting compatibilityReason string...");
                if (compatibilityFlag)
                    compatibilityReason = "client Version '" + clientVersion + "', Build '" + clientBuild + "' exactly matches server version and build";
                else
                    compatibilityReason = "client Version '" + clientVersion + "', Build '" + clientBuild + "' does not exactly match server Version '"
                            + serverVersion + "', Build '" + serverBuild + "'";
                logger.info("done");

                contentHashMap.put("compatible", compatibilityFlag);
                contentHashMap.put("reason", compatibilityReason);

                response.setContent(contentHashMap);
                response.setStatusCode(MsgStatus.SC_OK);

                logger.info("Returning hashmap: compatible --> " + compatibilityFlag + ", " + "reason -->" + compatibilityReason);
            } catch (Exception e) {
                response.setMsgKind(QIWConstants.DATA_MSG);
                response.setContentType(QIWConstants.STRING_TYPE);
                response.setContent("An exception occurred in ClientVersionChecker.getResponse(): " + e.getMessage());
            }
        }
        return response;
    }

    private IQiWorkbenchMsg getAbnormalReponse(IQiWorkbenchMsg request, String failureReason) {
        IQiWorkbenchMsg response = MsgUtils.genAbnormalMsg(request, MsgStatus.SC_INTERNAL_ERROR, "Internal error: Cannot perform client version check because " + failureReason);
        return response;
    }
}
