/*
###########################################################################Threa
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006-2008  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Logger;
import java.util.Set;

import com.bhpb.qiworkbench.server.usageCharts.UsageStats;

/**
 * Server-side Stats Manager that gathers stats from all end-users and processes
 * them in preparation for the generation and display of the stats charts.
 * There is a client-side Stats Manager that logs usage events and transmits
 * logged usage events to the server-side Stats Manager.
 * <p>
 * A usage event: timestamp module action machineID
 * <p>
 * The directory structure for the stats files is:
 * <pre>
 *      <root>
 *          lastProcessed.txt   //timestamp of the last data processed
 *          modules.txt         //list of workbench modules, including itself
 *          year_2008/
 *              weekly/         //raw usage events for each week of the year
 *                  week_1.txt
 *                  week_2.txt
 *                  ...
 *              week_1_summary.txt      //summarized usage for the week
 *              week_2_summary.txt
 *              ...
 *              month_1_summary.txt     //summarized usage for the month
 *              month_2_summary.txt
 *              ...
 *              year_summary.txt   //summarized usage for the year
 *          year_2009/
 *              ...
 * </pre>
 * <p>
 * The STATS server is always a Unix/Linux box, never a PC.
 * @author Gil Hansen
 * @version 1.0
 */
public class StatsManager {

    public static Logger logger = Logger.getLogger(StatsManager.class.getName());
    private static StatsManager singleton = null;

    //DIRECTORY AND FILE NAMES
    public static final String YEAR_PREFIX = "year_";
    public static final String MONTH_PREFIX = "month_";
    public static final String WEEK_PREFIX = "week_";
    public static final String WEEKLY_DIR = "weekly";
    public static final String SUMMARY_SUFFIX = "_summary.txt";
    public static final String STATS_SUFFIX = ".txt";
    public static final String MODULES_FILE = "modules.txt";
    public static final String LAST_PROCESSED_FILE = "lastProcessed.txt";
    public static final String FILE_LOCK_SUFFIX = ".lck";
    /** item separator in stats event */
    String statsep = " ";
    String filesep = "/";
    /** Path of directory holding stats files. */
    String basePath = "";
    /** Gregorian calendar for this local and timezone */
    GregorianCalendar calendar = null;

    private StatsManager() {
        calendar = new GregorianCalendar();
        //First week of the year is always 1
        calendar.setMinimalDaysInFirstWeek(1);
    }

    /**
     * Get the singleton instance of this class. If the Stats Manager doesn't
     * exist, create it.
     */
    public static StatsManager getInstance() {
        if (singleton == null) {
            singleton = new StatsManager();
        }
        return singleton;
    }

    /**
     * Record raw usage events in a weekly file. When data is for the next week,
     * consolidate the usage events for the previous week (if there is one) and
     * write the summary stats to a weekly stats file for that year.
     * <p>
     * Note: Method is called conditionally when the workbench is launched.
     * Therefore, class variable, basePath may not be set.
     * @param basePath Path of the directory holding stats files.
     * @param List of raw usage events
     * @return true if usage events successfully recorded; otherwise, false
     */
    synchronized public boolean recordRawEvents(String basePath, ArrayList<String> statEvents) {
        this.basePath = basePath;
        if (statEvents.size() == 0) {
            return true;
        }
        boolean eventsCaptured = true;

        String event = statEvents.get(0);
        String[] items = event.split(statsep);
        long timestamp = Long.parseLong(items[0]);
        int currentWeek = getWeekOfYear(timestamp);
        int currentYear = getYear(timestamp);
        String currentYearDir = basePath + filesep + YEAR_PREFIX + currentYear;
        String currentWeeklyDir = currentYearDir + filesep + WEEKLY_DIR;
        String currentWeeklyFile = currentWeeklyDir + filesep + WEEK_PREFIX + currentWeek + STATS_SUFFIX;

        //Make sure year directory exists
        File yearDir = new File(currentYearDir);
        if (!yearDir.isDirectory()) {
            yearDir.mkdir();
        }
        //Make sure weekly directory exists
        File weeklyDir = new File(currentWeeklyDir);
        if (!weeklyDir.isDirectory()) {
            weeklyDir.mkdir();
        }

        File weeklyFile = new File(currentWeeklyFile);
        BufferedWriter bw = null;
        try {
            FileOutputStream fos = new FileOutputStream(weeklyFile, true);
            bw = new BufferedWriter(new OutputStreamWriter(fos));

            //write out the stats events one-by-one
            for (int i = 0; i < statEvents.size(); i++) {
                event = statEvents.get(i);
                bw.write(event);
                bw.newLine();   //write the line separator

                //check if should start a new weekly file
                items = event.split(statsep);
                timestamp = Long.parseLong(items[0]);
                int weekOfYear = getWeekOfYear(timestamp);
                int year = getYear(timestamp);
                //check if crossed year boundary
                if (year > currentYear) {
                    currentYear = year;
                    currentYearDir = basePath + filesep + YEAR_PREFIX + currentYear;
                    yearDir = new File(currentYearDir);
                    yearDir.mkdir();
                    currentWeeklyDir = currentYearDir + filesep + WEEKLY_DIR;
                    weeklyDir = new File(currentWeeklyDir);
                    weeklyDir.mkdir();
                    currentWeeklyFile = currentWeeklyDir + filesep + WEEK_PREFIX + currentWeek + STATS_SUFFIX;
                    weeklyFile = new File(currentWeeklyFile);
                    fos = new FileOutputStream(weeklyFile, true);
                    bw = new BufferedWriter(new OutputStreamWriter(fos));
                } //check if crossed week boundary
                else if (weekOfYear > currentWeek) {
                    //close the existing weekly file
                    try {
                        if (bw != null) {
                            bw.close();
                        }
                    } catch (IOException e) {
                    }

                    //and prepare to write to a new weekly file
                    currentWeek = weekOfYear;
                    currentWeeklyFile = currentWeeklyDir + filesep + WEEK_PREFIX + currentWeek + STATS_SUFFIX;
                    weeklyFile = new File(currentWeeklyFile);
                    fos = new FileOutputStream(weeklyFile, true);
                    bw = new BufferedWriter(new OutputStreamWriter(fos));
                }
            }
        } catch (IOException ioe) {
            if (ioe instanceof FileNotFoundException && ioe.getMessage().contains("Permission denied")) {
                logger.warning("Insufficient privilege to write raw usage events.");
            } else {
                logger.warning("IO exception writing weekly stats file:" + WEEK_PREFIX + currentWeek + STATS_SUFFIX + ", CAUSE: " + ioe.getMessage());
            }
            eventsCaptured = false;
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (IOException e) {
            }
        }

        return eventsCaptured;
    }

    //**** MODULE UTILITIES ****
    /**
     * Save the list of workbench modules, i.e., merge the list
     * with those already saved. The list is used as the X-axis
     * for the Module Usage historgram.
     * <p>
     * Note: Method is always called when workbench launched. Therefore,
     * class variable, basepath, will be set.
     * @param basePath Path of the directory holding stats files.
     * @param modules List of modules
     * @return true if list of modules successfully saved; otherwise, false
     */
    public boolean recordModules(String basePath, ArrayList<String> modules) {
        this.basePath = basePath;
        String modulesPath = basePath + filesep + MODULES_FILE;
        File modulesFile = new File(modulesPath);

        //if there is no modules file, save the list passed in
        if (!modulesFile.exists()) {
            return saveModules(modulesFile, modules);
        }

        //get the current list of modules
        ArrayList<String> curModules = getModules(modulesFile);

        //merge the two lists: qiWorkbench name first, then modules in alphabetical order
        //but first, remove the first item in the list which is always the qiWorkbench
        String qiwbName = curModules.get(0);
        curModules.remove(0);
        modules.remove(0);
        ArrayList<String> mergedModules = new ArrayList<String>();
        mergedModules.addAll(curModules);
        mergedModules.addAll(modules);
        //sort list and remove duplicates
        String[] list = {""};
        list = mergedModules.toArray(list);
        java.util.Arrays.sort(list);
        mergedModules.clear();
        int i = 0, j = 1, last = list.length - 1;
        while (i < list.length) {
            mergedModules.add(list[i]);
            if (j > last) {
                break;
            }
            if (list[j].equals(list[i])) {
                //skip duplicate
                i += 2;
                j = i + 1;
            } else {
                i++;
                j++;
            }
        }
        //add workbench name back to head of list
        mergedModules.add(0, qiwbName);

        //save the merged list (replace the current saved list)
        return saveModules(modulesFile, mergedModules);
    }

    /**
     * Get the saved list of modules for which stats have been recorded
     * @param modulesFile Modules file
     * @return List of current modules
     */
    private ArrayList<String> getModules(File modulesFile) {
        ArrayList<String> modules = new ArrayList<String>();

        BufferedReader br = null;
        try {
            FileInputStream fis = new FileInputStream(modulesFile);
            br = new BufferedReader(new InputStreamReader(fis));
            String line = br.readLine();
            // read file line by line
            while (line != null) {
                modules.add(line);
                line = br.readLine();
            }
        } catch (IOException ioe) {
            logger.warning("IO exception reading modules file:" + ioe.getMessage());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
            }
        }

        return modules;
    }

    /**
     * Get the list of modules for which stats have been recorded.
     * Note: Called by the code to generate the Module histogram which
     * is invoked from the qiUsageStats.jsp page. The workbench does not
     * have to be active, in which case we use the default path of the
     * directory storing the stats.
     * @return List of current modules
     */
//TODO: persist the list of modules in a database
    public ArrayList<String> getModules() {
        String modulesPath = (basePath.equals("") ? ServerConstants.DEFAULT_STATS_DIR : basePath) + filesep + MODULES_FILE;
        logger.info("modulesPath=" + modulesPath);
        return getModules(new File(modulesPath));
    }

    /**
     * Save the list of workbench modules.
     * @param modulesFile Modules file
     * @param modules List of workbench modules
     * @return true if list of modules successfully saved; otherwise, false
     */
    private boolean saveModules(File modulesFile, ArrayList<String> modules) {
        boolean saved = true;

        BufferedWriter bw = null;

        try {
            FileOutputStream fos = new FileOutputStream(modulesFile);
            bw = new BufferedWriter(new OutputStreamWriter(fos));
            for (int i = 0; i < modules.size(); i++) {
                bw.write(modules.get(i));
                bw.newLine();   //write the line separator

            }
        } catch (IOException ioe) {
            if (ioe instanceof FileNotFoundException && ioe.getMessage().contains("Permission denied")) {
                logger.warning("WARNING: Insufficient privilege to write modules file: " + modulesFile.getPath());
            } else {
                logger.warning("WARNING: IO exception writing modules file:" + ioe.getMessage());
            }
            saved = false;
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (IOException e) {
            }
        }

        return saved;
    }

    //**** DATA UTILITIES ****
    /**
     * Get the week of the year corresponding to a timestamp
     * @param timestamp Time in milliseconds
     * @return Week corresponding to timestamp: 0-53
     */
    public int getWeekOfYear(long timestamp) {
        calendar.setTimeInMillis(timestamp);
        return calendar.get(Calendar.WEEK_OF_YEAR);
    }

    /**
     * Get the last week of the year, i.e., December 31 occurs in.
     */
    public int lastWeekOfYear(int year) {
        calendar.set(year, Calendar.DECEMBER, 31);
        return calendar.get(Calendar.WEEK_OF_YEAR);
    }

    /**
     * Get the month corresponding to a timestamp
     * @param timestamp Time in milliseconds
     * @return Month corresponding to timestamp: 1-12
     */
    public int getMonth(long timestamp) {
        calendar.setTimeInMillis(timestamp);
        //Note: January is month 0
        return calendar.get(Calendar.MONTH) + 1;
    }

    /**
     * Get the year corresponding to a timestamp
     * @param timestamp Time in milliseconds
     * @return Year corresponding to timestamp, e.g., 2008
     */
    public int getYear(long timestamp) {
        calendar.setTimeInMillis(timestamp);
        return calendar.get(Calendar.YEAR);
    }

    /**
     * Convert a date, mm/dd/yyyy, to a timestamp
     * @param date
     * @return timestamp
     */
    public long date2Timestamp(String date) {
        int dd, mm, yyyy;
        String[] items = date.split("/");
        try {
            mm = Integer.parseInt(items[0]);
            dd = Integer.parseInt(items[1]);
            yyyy = Integer.parseInt(items[2]);
            //Note: January is month 0
            calendar.set(yyyy, mm - 1, dd);
        } catch (NumberFormatException nfe) {
            return 0L;
        }

        return calendar.getTimeInMillis();
    }

    /** Convert a timestamp to a date, mm/dd/yyyy
     * @param timestamp
     * @return date
     */
    public String timestamp2date(long timestamp) {
        calendar.setTimeInMillis(timestamp);
        int mm = calendar.get(Calendar.MONTH) + 1;
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        int yyyy = calendar.get(Calendar.YEAR);
        return mm + "/" + dd + "/" + yyyy;
    }

    /**
     * Roll back the current calendar's month
     * @param months number of months to roll back
     * @return timestamp of rolled back date
     */
    public long rollbackMonth(int months) {
        int mm = calendar.get(Calendar.MONTH) + 1;
        calendar.roll(Calendar.MONTH, -months);
        //if crossed year boundary, roll back the year too
        if (calendar.get(Calendar.MONTH) + 1 > mm) {
            calendar.roll(Calendar.YEAR, false);
        }

        return calendar.getTimeInMillis();
    }

    /**
     * Set the calendar's current time
     * @param timestamp
     */
    public void setCurrentTime(long timestamp) {
        calendar.setTimeInMillis(timestamp);
    }

    //**** DATA PROCESSING UTILITIES ****
    /**
     * Get the date of the last data processed, i.e., its timestamp..
     * @return timestamp if lastProcessed.txt file exists; 0 if file doesn't exist or IO exception reading file; -1 if timestamp's format invalid.
     */
    private long getLastProcessedDate() {
        String lastProcessedPath = (basePath.equals("") ? ServerConstants.DEFAULT_STATS_DIR : basePath) + filesep + LAST_PROCESSED_FILE;
        File lastProcessedFile = new File(lastProcessedPath);
        if (!lastProcessedFile.exists()) {
            return 0L;
        }
        long timestamp = 0L;

        BufferedReader br = null;
        try {
            FileInputStream fis = new FileInputStream(lastProcessedFile);
            br = new BufferedReader(new InputStreamReader(fis));
            String line = br.readLine();
            timestamp = Long.parseLong(line);
        } catch (NumberFormatException nfe) {
            timestamp = -1L;
        } catch (IOException ioe) {
            logger.warning("IO exception reading last processed file:" + ioe.getMessage());
            timestamp = 0L;
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
            }
        }

        return timestamp;
    }

    /**
     * Save the date of the last data processed.
     * @param timestamp Date of the last data processed.
     * @return true if date saved; otherwise, false
     */
    private boolean saveLastProcessedDate(long timestamp) {
        String lastProcessedPath = (basePath.equals("") ? ServerConstants.DEFAULT_STATS_DIR : basePath) + filesep + LAST_PROCESSED_FILE;

        boolean saved = true;

        BufferedWriter bw = null;
        try {
            FileOutputStream fos = new FileOutputStream(new File(lastProcessedPath));
            bw = new BufferedWriter(new OutputStreamWriter(fos));
            bw.write(Long.toString(timestamp));
            bw.newLine();   //write the line separator

        } catch (IOException ioe) {
            if (ioe instanceof FileNotFoundException && ioe.getMessage().contains("Permission denied")) {
                logger.warning("WARNING: Insufficient privilege to write last processed  file.");
            } else {
                logger.warning("WARNING: IO exception writing last processed file:" + ioe.getMessage());
            }
            saved = false;
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (IOException e) {
            }
        }

        return saved;
    }

    /**
     * Process data for a week, i.e., summarize it
     * @param weekFile Week file to process
     * @param lastDateProcessed Timestamp of the last data processed
     * @param yearDir Directory for the year where week summary file is to be stored
     */
    private void processWeekData(File weekFile, long lastDateProcessed, File yearDir) {
        long firstTimestamp = 0L, lastTimestamp = 0L;
        //Number of times each module was launched
        HashMap<String, Integer> moduleStats = new HashMap<String, Integer>();
        //List of unique machineIDs whose size is the number of users
        HashSet<String> machineIDs = new HashSet<String>();
        //read the usage data and summarize
        BufferedReader br = null;
        try {
            FileInputStream fis = new FileInputStream(weekFile);
            br = new BufferedReader(new InputStreamReader(fis));
            String line = br.readLine();
            String[] parts = line.split(statsep);
            firstTimestamp = Long.parseLong(parts[0]);
            // read file line-by-line and process each line
            while (line != null) {
                //process line
                parts = line.split(statsep);
                lastTimestamp = Long.parseLong(parts[0]);
                String module = parts[1];
                String action = parts[2];
                String machineID = parts[3];

                if (action.equals(ServerConstants.LAUNCHED_ACTION)) {
                    //count the number of times a module is launched
                    String[] items = module.split("#");
                    String key = items[0];
                    Integer val = moduleStats.get(key);
                    if (val == null) {
                        moduleStats.put(key, new Integer(1));
                    } else {
                        moduleStats.put(key, new Integer(val.intValue() + 1));
                    }

                    //remember the number of unique users
                    machineIDs.add(machineID);
                }

                line = br.readLine();
            }

            //save the date of the last data processed
            if (lastTimestamp > lastDateProcessed) {
                saveLastProcessedDate(lastTimestamp);
            }
        } catch (IOException ioe) {
            logger.warning("IO exception reading week file " + weekFile.getName() + ":" + ioe.getMessage());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
            }
        }

        //save the summarized data for this week
        String weekID = WEEK_PREFIX + getWeekOfYear(firstTimestamp) + "_" + getYear(firstTimestamp);
        //1st line: weekID, start data, end date, number of unique users
        String header = weekID + statsep + Long.toString(firstTimestamp) + statsep + Long.toString(lastTimestamp) + statsep + machineIDs.size();
        //2nd line: list of machineIDs
        String users = "";
        Iterator<String> iter = machineIDs.iterator();
        while (iter.hasNext()) {
            users += iter.next();
            if (iter.hasNext()) {
                users += statsep;
            }
        }
        File summaryFile = new File(yearDir.getPath() + filesep + WEEK_PREFIX + getWeekOfYear(firstTimestamp) + SUMMARY_SUFFIX);
        while (isFileLocked(summaryFile)) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ie) {
            }
        }
        lockFile(summaryFile);

        BufferedWriter bw = null;
        try {
            FileOutputStream fos = new FileOutputStream(summaryFile);
            bw = new BufferedWriter(new OutputStreamWriter(fos));
            bw.write(header);
            bw.newLine();
            bw.write(users);
            bw.newLine();

            Set<String> keys = moduleStats.keySet();
            iter = keys.iterator();
            while (iter.hasNext()) {
                String key = iter.next();
                //form line: module name, number of uses
                String line = key + statsep + moduleStats.get(key).intValue();
                bw.write(line);
                bw.newLine();   //write the line separator

            }
        } catch (IOException ioe) {
            if (ioe instanceof FileNotFoundException && ioe.getMessage().contains("Permission denied")) {
                logger.warning("WARNING: Insufficient privilege to write week summary  file.");
            } else {
                logger.warning("WARNING: IO exception writing week sumary file:" + ioe.getMessage());
            }
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (IOException e) {
            }
        }
        unlockFile(summaryFile);
    }

    /**
     * Process any unprocessed raw event data for a year.
     * @param yearDir Year directory
     * @param lastWeekProcessed The last week of the year processed.
     * @param lastDateProcessed Timestamp of the last data processed
     * If 0, then nothing for this year has been processed
     */
    private void processYearData(File yearDir, int lastWeekProcessed, long lastDateProcessed) {
        String yearDirPath = yearDir.getPath();
        String weeklyDirPath = yearDirPath + filesep + WEEKLY_DIR;
        File weeklyDir = new File(weeklyDirPath);
        File[] files = weeklyDir.listFiles();
        //find the week_n files not fully processed
        for (int i = 0; i < files.length; i++) {
            if (!files[i].isFile()) {
                continue;
            }
            String weekFileName = files[i].getName();
            String[] parts = weekFileName.split("_");
            int idx = parts[1].indexOf(STATS_SUFFIX);
            //ignore hidden files
            if (idx == -1) {
                continue;
            }
            int week = Integer.parseInt(parts[1].substring(0, idx));
            //reprocess the data for the last week processed, for it may have been
            //partially processed, i.e., it contains new data
            if (week >= lastWeekProcessed) {
                processWeekData(files[i], lastDateProcessed, yearDir);
            }
        }

    //TODO: process the months of this year not fully processed

    //TODO: update the year's summary
    }

    /**
     * Prepare data for processing
     */
    synchronized public void prepareUsageData() {
        //process any unprocessed raw event data
        processData();
    }

    /**
     * Process any unprocessed raw event data. For each year, produce
     * the weekly, monthly and yearly summaries.
     */
    private void processData() {
        //If there is no base path for the usage stats directory, may be invoking
        //stats Webpage directory without going thru the workbench. Get the base
        //path from the qiWbConfig.xml file.
        if (basePath.equals("")) {
            this.basePath = UsageStats.getInstance().getStatsPath();

        //get the timestamp of the last data processed
        }
        long lastDateProcessed = getLastProcessedDate();
        int lastWeekProcessed = 0, lastYearProcessed = 0;
        if (lastDateProcessed > 0) {
            lastWeekProcessed = getWeekOfYear(lastDateProcessed);
            lastYearProcessed = getYear(lastDateProcessed);
        }

        File baseDir = new File(basePath.equals("") ? ServerConstants.DEFAULT_STATS_DIR : basePath);
        File[] files = baseDir.listFiles();
        //find the year_yyyy directories not fully processed
        for (int i = 0; i < files.length; i++) {
            if (!files[i].isDirectory()) {
                continue;
            }
            logger.info("stat dir:" + files[i].getName());
            String yearDirName = files[i].getName();
            String[] parts = yearDirName.split("_");
            int year = Integer.parseInt(parts[1]);
            logger.info("year=" + year + ", lastYearProcessed=" + lastYearProcessed + ", lastWeekProcessed=" + lastWeekProcessed);
            if (year < lastYearProcessed) {
                continue;
            }
            if (year == lastYearProcessed) {
                processYearData(files[i], lastWeekProcessed, lastDateProcessed);
            } else //indicate nothing for this year has been processed
            {
                processYearData(files[i], 0, 0);
            }
        }
    }

    /**
     * Get module usage for a week; extracted from a week summary file
     * @param weekSummaryFile A week summary file
     * @return Module usages for the week
     */
    public HashMap<String, Integer> getWeekModuleUsage(File weekSummaryFile) {
        HashMap<String, Integer> weekModuleUses = new HashMap<String, Integer>();

        BufferedReader br = null;
        try {
            FileInputStream fis = new FileInputStream(weekSummaryFile);
            br = new BufferedReader(new InputStreamReader(fis));
            //read 1st line: header
            String header = br.readLine();
            //read 2nd line: list of machineIDs
            String users = br.readLine();
            String line = br.readLine();
            // read file line-by-line
            while (line != null) {
                //process usage for module
                String[] items = line.split(statsep);
                weekModuleUses.put(items[0], new Integer(items[1]));
                line = br.readLine();
            }
        } catch (IOException ioe) {
            logger.warning("IO exception reading week summarization file:" + ioe.getMessage());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
            }
        }

        return weekModuleUses;
    }

    /**
     * Summarize usage in a year for those weeks within the reporting period
     * @param yearDir Year directory
     * @param startWeek First week to start summarization; 0 means summarize all weeks
     * in the year for which there is usage data.
     * @param endWeek Last week to include in summarization; 0 means summarize all
     * weeks in the year for which there is usage data
     * @return Summarized week usage for the year
     */
    private HashMap<String, Integer> summarizeWeekUsage(File yearDir, int startWeek, int endWeek) {
        HashMap<String, Integer> totalWeekUses = new HashMap<String, Integer>();

        File[] files = yearDir.listFiles();
        //find the week_n_summary files within the reporting period
        for (int i = 0; i < files.length; i++) {
            if (!files[i].isFile()) {
                continue;
            }
            String fileName = files[i].getName();
            //skip non-summary files
            if (!fileName.endsWith(SUMMARY_SUFFIX)) {
                continue;
            }
            if (!fileName.startsWith(WEEK_PREFIX)) {
                continue;
            }
            String[] parts = fileName.split("_");
            int week = Integer.parseInt(parts[1]);
            //process those weeks within the reporting period
            if (startWeek == 0 || endWeek == 0 || (week >= startWeek && week <= endWeek)) {
                while (isFileLocked(files[i])) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ie) {
                    }
                }
                lockFile(files[i]);
                BufferedReader br = null;
                try {
                    FileInputStream fis = new FileInputStream(files[i]);
                    br = new BufferedReader(new InputStreamReader(fis));
                    String header = br.readLine();
                    String users = br.readLine();
                    String line = br.readLine();
                    // read file line-by-line
                    while (line != null) {
                        //process usage for module
                        String[] items = line.split(statsep);
                        Integer val1 = totalWeekUses.get(items[0]);
                        int val2 = (val1 == null ? 0 : val1.intValue()) + Integer.parseInt(items[1]);
                        totalWeekUses.put(items[0], new Integer(val2));
                        line = br.readLine();
                    }
                } catch (IOException ioe) {
                    logger.warning("IO exception reading week summarization file:" + ioe.getMessage());
                } finally {
                    try {
                        if (br != null) {
                            br.close();
                        }
                    } catch (IOException e) {
                    }
                }

                unlockFile(files[i]);
            }
        }

        return totalWeekUses;
    }

    /**
     * Summarize module usage, per week, over the reporting period
     * @param startTimestamp Start date of reporting period
     * @param endTimestamp End date of reporting period
     * @return Summarized module usage data over the reporting period
     */
    private HashMap<String, Integer> summarizeModuleUsage(long startTimestamp, long endTimestamp) {
        int startWeek = getWeekOfYear(startTimestamp);
        int endWeek = getWeekOfYear(endTimestamp);
        int startYear = getYear(startTimestamp);
        int endYear = getYear(endTimestamp);

        HashMap<String, Integer> totalUses = new HashMap<String, Integer>();
        File baseDir = new File(basePath.equals("") ? ServerConstants.DEFAULT_STATS_DIR : basePath);
        File[] files = baseDir.listFiles();
        //find the year_yyyy directories to summarize
        for (int i = 0; i < files.length; i++) {
            if (!files[i].isDirectory()) {
                continue;
            }
            String yearDirName = files[i].getName();
            String[] parts = yearDirName.split("_");
            int year = Integer.parseInt(parts[1]);
            //exclude data not within the reporting period
            if (year < startYear || year > endYear) {
                continue;
            //summarize all the weeks in the year that fall into the reporting period
            }
            HashMap<String, Integer> totalWeekUses = summarizeWeekUsage(files[i], year == startYear ? startWeek : 0, year == endYear ? endWeek : 0);

            //add usage to the running total
            Set<String> keys = totalWeekUses.keySet();
            Iterator<String> iter = keys.iterator();
            while (iter.hasNext()) {
                String key = iter.next();
                Integer val1 = totalWeekUses.get(key);
                Integer val2 = totalUses.get(key);
                if (val2 == null) {
                    totalUses.put(key, val1);
                } else {
                    totalUses.put(key, new Integer(val1.intValue() + val2.intValue()));
                }
            }
        }

        return totalUses;
    }

    /**
     * Get the uses for the modules over the reporting period.
     * Used to generate the Module Usage histogram.
     * @param modules List of modules.
     * @param startTimestamp Start date of reporting period
     * @param endTimestamp End date of reporting period
     * @return Map of module names to their usage count over the reporting period.
     */
    public HashMap<String, Integer> getModuleUses(ArrayList<String> modules, long startTimestamp, long endTimestamp) {
        //summarize usage over the reporting period
        return summarizeModuleUsage(startTimestamp, endTimestamp);
    }

    /**
     * Get the modules usages per week over the reporting period.
     * Used to generate the Week Module Trends chart.
     * <p>
     * Note: The returned list is in ascending ordered by the week of the year
     * with the first item corresponding to the week of the year for the start
     * date and the last item corresponding to the week of the year for the end
     * date.
     * @param modules List of modules
     * @param startTimestamp Start date of reporting period
     * @param endTimestamp End date of reporting period
     * @return List of maps of module usages for each week over the reporting period.
     */
    public ArrayList<HashMap<String, Integer>> getWeeklyModuleUses(ArrayList<String> modules, long startTimestamp, long endTimestamp) {
        int startWeek = getWeekOfYear(startTimestamp);
        int endWeek = getWeekOfYear(endTimestamp);
        int startYear = getYear(startTimestamp);
        int endYear = getYear(endTimestamp);

        ArrayList<HashMap<String, Integer>> weeklyUses = new ArrayList<HashMap<String, Integer>>();

        int year = startYear;
        int week = startWeek;
        while (year <= endYear) {
            String yearDirPath = (basePath.equals("") ? ServerConstants.DEFAULT_STATS_DIR : basePath) + filesep + YEAR_PREFIX + year;
            File yearDir = new File(yearDirPath);
            if (!yearDir.isDirectory()) {
                year++;
                week = 1;
                continue;
            }

            int lastWeek = year == endYear ? endWeek : lastWeekOfYear(year);
            //get the module usages for all the weeks in the year that fall into the reporting period
            while (week <= lastWeek) {
                String weekSummaryFilePath = yearDirPath + filesep + WEEK_PREFIX + week + SUMMARY_SUFFIX;
                File weekFile = new File(weekSummaryFilePath);
                if (!weekFile.isFile()) {
                    //no stats for this week
                    weeklyUses.add(new HashMap<String, Integer>());
                } else {
                    while (isFileLocked(weekFile)) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException ie) {
                        }
                    }
                    lockFile(weekFile);
                    weeklyUses.add(getWeekModuleUsage(weekFile));
                    unlockFile(weekFile);
                }

                week++;
            }

            year++;
            week = 1;
        }
        logger.info("weekly uses: " + weeklyUses);
        return weeklyUses;
    }

    /**
     * Get the uses per month for the modules over the reporting period.
     * Used to generate the Month Module Trends chart.
     * <p>
     * Note: The returned list is in ascending ordered by the month of the year
     * with the first item corresponding to the month of the year for the start
     * date and the last item corresponding to the month of the year for the end
     * date.
     * @param modules List of modules
     * @param startTimestamp Start date of reporting period
     * @param endTimestamp End date of reporting period
     * @return List of maps of module usages for a month over the reporting period.
     */
    public ArrayList<HashMap<String, Integer>> getMonthlyModuleUses(ArrayList<String> modules, long startTimestamp, long endTimestamp) {
        int startMonth = getMonth(startTimestamp);
        int endMonth = getMonth(endTimestamp);
        int startYear = getYear(startTimestamp);
        int endYear = getYear(endTimestamp);

        ArrayList<HashMap<String, Integer>> monthlyUses = new ArrayList<HashMap<String, Integer>>();
        File baseDir = new File(basePath.equals("") ? ServerConstants.DEFAULT_STATS_DIR : basePath);
//TODO: implement		
        return monthlyUses;
    }

    /**
     * Get the uses per year for the modules over the reporting period.
     * Used to generate the Year Module Trends chart.
     * <p>
     * Note: The returned list is in ascending ordered by year
     * with the first item corresponding to the year for the start
     * date and the last item corresponding to the year for the end
     * date.
     * @param modules List of modules
     * @param startTimestamp Start date of reporting period
     * @param endTimestamp End date of reporting period
     * @return List of maps of module usages for a year over the reporting period.
     */
    public ArrayList<HashMap<String, Integer>> getYearlyModuleUses(ArrayList<String> modules, long startTimestamp, long endTimestamp) {
        int startYear = getYear(startTimestamp);
        int endYear = getYear(endTimestamp);

        ArrayList<HashMap<String, Integer>> yearlyUses = new ArrayList<HashMap<String, Integer>>();
        File baseDir = new File(basePath.equals("") ? ServerConstants.DEFAULT_STATS_DIR : basePath);
//TODO: implement		
        return yearlyUses;
    }

    /**
     * Get user count for a week; extracted from a week summary file
     * @param weekSummaryFile A week summary file
     * @return User count for the week
     */
    private Integer getWeekUserCnt(File weekSummaryFile) {
        Integer weekUserCnt = new Integer(0);
        BufferedReader br = null;
        try {
            FileInputStream fis = new FileInputStream(weekSummaryFile);
            br = new BufferedReader(new InputStreamReader(fis));
            //read 1st line: header
            String header = br.readLine();
            String[] items = header.split(statsep);
            //Note: the number of unique users is at the end of the line
            weekUserCnt = new Integer(items[items.length - 1]);
        } catch (IOException ioe) {
            logger.warning("IO exception reading week summarization file:" + ioe.getMessage());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
            }
        }

        return weekUserCnt;
    }

    /**
     * Get the number of users per week over the reporting period.
     * Used to generate the Week Module Trends chart.
     * <p>
     * Note: The returned list is in ascending ordered by the week of the year
     * with the first item corresponding to the week of the year for the start
     * date and the last item corresponding to the week of the year for the end
     * date.
     * @param startTimestamp Start date of reporting period
     * @param endTimestamp End date of reporting period
     * @return List of number of users for each week over the reporting period.
     */
    public ArrayList<Integer> getWeeklyUserCnts(long startTimestamp, long endTimestamp) {
        int startWeek = getWeekOfYear(startTimestamp);
        int endWeek = getWeekOfYear(endTimestamp);
        int startYear = getYear(startTimestamp);
        int endYear = getYear(endTimestamp);

        ArrayList<Integer> weeklyUserCnts = new ArrayList<Integer>();

        int year = startYear;
        int week = startWeek;
        while (year <= endYear) {
            String yearDirPath = (basePath.equals("") ? ServerConstants.DEFAULT_STATS_DIR : basePath) + filesep + YEAR_PREFIX + year;
            File yearDir = new File(yearDirPath);
            if (!yearDir.isDirectory()) {
                year++;
                week = 1;
                continue;
            }

            int lastWeek = year == endYear ? endWeek : lastWeekOfYear(year);
            //get the number of users for all the weeks in the year that fall into the reporting period
            while (week <= lastWeek) {
                String weekSummaryFilePath = yearDirPath + filesep + WEEK_PREFIX + week + SUMMARY_SUFFIX;
                File weekFile = new File(weekSummaryFilePath);
                if (!weekFile.isFile()) {
                    //no stats for this week
                    weeklyUserCnts.add(new Integer(0));
                } else {
                    while (isFileLocked(weekFile)) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException ie) {
                        }
                    }
                    lockFile(weekFile);
                    weeklyUserCnts.add(getWeekUserCnt(weekFile));
                    unlockFile(weekFile);
                }

                week++;
            }

            year++;
            week = 1;
        }
        logger.info("weekly user count: " + weeklyUserCnts);
        return weeklyUserCnts;
    }

    /**
     * Get set of unique users for a week; extracted from a week summary file
     * @param weekSummaryFile A week summary file
     * @return Set of unique users for the week
     */
    private HashSet<String> getWeekUsers(File weekSummaryFile) {
        HashSet<String> weekUsers = new HashSet<String>();
        BufferedReader br = null;
        try {
            FileInputStream fis = new FileInputStream(weekSummaryFile);
            br = new BufferedReader(new InputStreamReader(fis));
            //read 1st line: header
            String header = br.readLine();
            //read 2nd line: list of machineIDs
            String users = br.readLine();
            String[] items = users.split(statsep);
            for (int i = 0; i < items.length; i++) {
                weekUsers.add(items[i]);
            }
        } catch (IOException ioe) {
            logger.warning("IO exception reading week summarization file:" + ioe.getMessage());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
            }
        }

        return weekUsers;
    }

    /**
     * Get the set of unique users per week over the reporting period.
     * <p>
     * Note: The returned list is in ascending ordered by the week of the year
     * with the first item corresponding to the week of the year for the start
     * date and the last item corresponding to the week of the year for the end
     * date.
     * @param startTimestamp Start date of reporting period
     * @param endTimestamp End date of reporting period
     * @return List of sets of users for each week over the reporting period.
     */
    public ArrayList<HashSet<String>> getWeeklyUsers(long startTimestamp, long endTimestamp) {
        int startWeek = getWeekOfYear(startTimestamp);
        int endWeek = getWeekOfYear(endTimestamp);
        int startYear = getYear(startTimestamp);
        int endYear = getYear(endTimestamp);

        ArrayList<HashSet<String>> weeklyUsers = new ArrayList<HashSet<String>>();

        int year = startYear;
        int week = startWeek;
        while (year <= endYear) {
            String yearDirPath = (basePath.equals("") ? ServerConstants.DEFAULT_STATS_DIR : basePath) + filesep + YEAR_PREFIX + year;
            File yearDir = new File(yearDirPath);
            if (!yearDir.isDirectory()) {
                year++;
                week = 1;
                continue;
            }

            int lastWeek = year == endYear ? endWeek : lastWeekOfYear(year);
            //get the set of users for all the weeks in the year that fall into the reporting period
            while (week <= lastWeek) {
                String weekSummaryFilePath = yearDirPath + filesep + WEEK_PREFIX + week + SUMMARY_SUFFIX;
                File weekFile = new File(weekSummaryFilePath);
                if (!weekFile.isFile()) {
                    //no stats for this week
                    weeklyUsers.add(new HashSet<String>());
                } else {
                    while (isFileLocked(weekFile)) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException ie) {
                        }
                    }
                    lockFile(weekFile);
                    weeklyUsers.add(getWeekUsers(weekFile));
                    unlockFile(weekFile);
                }

                week++;
            }

            year++;
            week = 1;
        }
        logger.info("weekly users: " + weeklyUsers);
        return weeklyUsers;
    }

    //FILE ACCESS UTILITIES
    /**
     * Check if file is locked.
     * @param f File to be checked
     * @return true if locked; otherwise, false
     */
    synchronized private boolean isFileLocked(File f) {
        String filePath = f.getAbsolutePath();
        int idx = filePath.indexOf(".");
        File lock = new File(filePath.substring(0, idx) + FILE_LOCK_SUFFIX);
        return lock.exists() ? true : false;
    }

    /**
     * Lock file. Assumes file is not locked; no check.
     * @param f File to be locked.
     * @param true if file is successfully locked; otherwise, false
     */
    synchronized private boolean lockFile(File f) {
        String filePath = f.getAbsolutePath();
        int idx = filePath.indexOf(".");
        File lock = new File(filePath.substring(0, idx) + FILE_LOCK_SUFFIX);
        try {
            return lock.createNewFile();
        } catch (IOException ioe) {
            return false;
        }
    }

    /**
     * Unlock file. Assumes file is locked; no check.
     * @param f File that is locked.
     * @return true if file is successfully unlocked; otherwise, false
     */
    synchronized private boolean unlockFile(File f) {
        String filePath = f.getAbsolutePath();
        int idx = filePath.indexOf(".");
        File lock = new File(filePath.substring(0, idx) + FILE_LOCK_SUFFIX);
        return lock.delete();
    }
}