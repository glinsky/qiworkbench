/**
 * Generate the qiWorkbench module trends chart and output as a PNG to the HTTP response.
 */

package com.bhpb.qiworkbench.server.usageCharts;

import java.awt.Color;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleInsets;

import com.bhpb.qiworkbench.server.ServerConstants;
import com.bhpb.qiworkbench.server.StatsManager;

public class GenModuleTrends extends HttpServlet {
    public static Logger logger = Logger.getLogger(GenModuleTrends.class.getName());
    
    /** Start date of reporting period */
    String startDate = "";
    /** End date of reporting period */
    String endDate = "";
    /** Time scale: weekly, monthly or yearly */
    String timeScale = "";

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }
    
    /**
     * Process a GET request.
     *
     * @param request  the request.
     * @param response  the response.
     *
     * @throws ServletException if there is a servlet related problem.
     * @throws IOException if there is an I/O problem.
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        startDate = request.getParameter("startDate");
        endDate = request.getParameter("endDate");
        timeScale = request.getParameter("scale");

        long startTimestamp, endTimestamp;
        StatsManager statsMgr = StatsManager.getInstance();

        if (endDate == null || endDate.equals("") || endDate.equals("null")) {
            //set end time to default, i.e., to today
            endTimestamp = System.currentTimeMillis();
        } else {
            endTimestamp = statsMgr.date2Timestamp(endDate);
        }

        if (startDate == null || startDate.equals("") || startDate.equals("null")) {
            //set start time to default, i.e., 1 months prior to today
            statsMgr.setCurrentTime(endTimestamp);
            startTimestamp = statsMgr.rollbackMonth(1);
        } else {
            startTimestamp = statsMgr.date2Timestamp(startDate);
        }

        if (startTimestamp > endTimestamp) {
            //set start date 1 months prior to end date
            statsMgr.setCurrentTime(endTimestamp);
            startTimestamp = statsMgr.rollbackMonth(1);
        }
        
        if (timeScale == null || timeScale.equals("") || timeScale.equals("null"))
            timeScale = ServerConstants.WEEKLY_TIMESCALE;

        //form the reporting period dates
        startDate = statsMgr.timestamp2date(startTimestamp);
        endDate = statsMgr.timestamp2date(endTimestamp);
        
        statsMgr.prepareUsageData();

        OutputStream out = response.getOutputStream();

        XYDataset dataset = createDataset(startTimestamp, endTimestamp);
        JFreeChart chart = createChart(dataset);

        //Send chart back as a PNG
        try {
            if (chart != null) {
                response.setContentType("image/png");
                ChartUtilities.writeChartAsPNG(out, chart, 500, 300);
            }
        } catch (IOException ioe) {
            System.err.println(ioe.toString());
        } finally {
            out.close();
        }
    }

    /**
     * Create chart's dataset.
     * @param startTimestamp Start date of the reporting period
     * @param endTimestamp End data of the reporting period.
     * @return The chart's dataset.
     */
    private XYDataset createDataset(long startTimestamp, long endTimestamp) {
        StatsManager statsManager = StatsManager.getInstance();
        //get the list of modules; used for the X-axis
        ArrayList<String> modules = statsManager.getModules();

        // create the dataset
        ArrayList<HashMap<String, Integer>> weeklyUses = timeScale.equals(ServerConstants.WEEKLY_TIMESCALE) ? statsManager.getWeeklyModuleUses(modules, startTimestamp, endTimestamp) :
            (timeScale.equals(ServerConstants.MONTHLY_TIMESCALE) ? statsManager.getMonthlyModuleUses(modules, startTimestamp, endTimestamp) : statsManager.getYearlyModuleUses(modules, startTimestamp, endTimestamp));
//TODO: get the number of users per month/year
        ArrayList<Integer> userCnts = null;
        if (timeScale.equals(ServerConstants.WEEKLY_TIMESCALE)) {
            userCnts = statsManager.getWeeklyUserCnts(startTimestamp, endTimestamp);
        }
		int numModules = modules.size();
        //add 1 for User count
		XYSeries[] series = new XYSeries[numModules+1];
		for (int i=0; i<numModules; i++) {
			series[i] = new XYSeries(modules.get(i));
		}
        series[series.length-1] = new XYSeries("Users");
		
		if (timeScale.equals(ServerConstants.WEEKLY_TIMESCALE)) {
			int startWeek = statsManager.getWeekOfYear(startTimestamp);
			int endWeek = statsManager.getWeekOfYear(endTimestamp);
			int startYear = statsManager.getYear(startTimestamp);
			int endYear = statsManager.getYear(endTimestamp);
	
			int year = startYear;
			int week = startWeek;
			int j = 0;
			while (year <= endYear) {
				int lastWeek = year==endYear ? endWeek : statsManager.lastWeekOfYear(year);
				//get the module usages for all the weeks in the year that fall into the reporting period
				while (week <= lastWeek) {
					HashMap<String, Integer> weekUses = weeklyUses.get(j);
					for (int k=0; k<numModules; k++) {
						series[k].add(week, weekUses.get(modules.get(k)));
					}
                    series[series.length-1].add(week, userCnts.get(j));
					
					week++; j++;
				}
	
				year++;
				week = 1;
                //Note: j has already been incremented
			}
		}

        XYSeriesCollection dataset = new XYSeriesCollection();
        for (int i=0; i<numModules+1; i++) {
            dataset.addSeries(series[i]);
        }

        return dataset;
    }

    /**
     * Create a module trends chart.
     *
     * @param dataset The chart's dataset.
     *
     * @return The module trends chart.
     */
    private JFreeChart createChart(XYDataset dataset) {
        String header = "qiWorkbench Module Trends";
        // create the chart
        String xaxisLabel = timeScale.equals(ServerConstants.WEEKLY_TIMESCALE) ? "Week" :
            (timeScale.equals(ServerConstants.MONTHLY_TIMESCALE) ? "Month" : "Year");
        JFreeChart chart = ChartFactory.createXYLineChart(
            header+"\n"+startDate+" to "+endDate, // chart title
            xaxisLabel,               // x axis label
            "Uses",                   // y axis label
            dataset,                  // data
            PlotOrientation.VERTICAL,
            true,                     // include legend
            true,                     // tooltips
            false                     // urls
        );

        chart.setBackgroundPaint(Color.white);

        // customize the plot
        XYPlot plot = (XYPlot)chart.getPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);

        XYLineAndShapeRenderer renderer
            = (XYLineAndShapeRenderer)plot.getRenderer();
        renderer.setBaseShapesVisible(true);
        renderer.setBaseShapesFilled(true);

        // change the axes units to integer
        NumberAxis rangeAxis = (NumberAxis)plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        NumberAxis domainAxis = (NumberAxis)plot.getDomainAxis();
        domainAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        return chart;
    }
}
