/**
 * Generate the qiWorkbench module histogram and output as a PNG to the HTTP response.
 */

package com.bhpb.qiworkbench.server.usageCharts;

import java.awt.Color;
import java.awt.Paint;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import com.bhpb.qiworkbench.server.ServerConstants;
import com.bhpb.qiworkbench.server.StatsManager;

public class GenModuleHistogram extends HttpServlet {
    public static Logger logger = Logger.getLogger(GenModuleHistogram.class.getName());
    
    /** Start date of reporting period: mm/dd/yyyy */
    String startDate = "";
    /** End date of reporting period: mm/dd/yyyy */
    String endDate = "";
    /** Time scale: weekly, monthly or yearly */
    String timeScale = "";

    /**
     * A custom renderer that returns a different color for each item in a
     * single series.
     */
    static class CustomRenderer extends BarRenderer {

        /** The colors. */
        private Paint[] colors;

        /**
         * Creates a new renderer.
         *
         * @param colors  the colors.
         */
        public CustomRenderer(Paint[] colors) {
            this.colors = colors;
        }

        /**
         * Returns the paint for an item.  Overrides the default behaviour
         * inherited from AbstractSeriesRenderer.
         *
         * @param row  the series.
         * @param column  the category.
         *
         * @return The item color.
         */
        public Paint getItemPaint(int row, int column) {
            return this.colors[column % this.colors.length];
        }
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }
    
    /**
     * Process a GET request.
     *
     * @param request  the request.
     * @param response  the response.
     *
     * @throws ServletException if there is a servlet related problem.
     * @throws IOException if there is an I/O problem.
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        startDate = request.getParameter("startDate");
        endDate = request.getParameter("endDate");
        timeScale = request.getParameter("scale");

        long startTimestamp, endTimestamp;
        StatsManager statsMgr = StatsManager.getInstance();

        if (endDate == null || endDate.equals("") || endDate.equals("null")) {
            //set end time to default, i.e., to today
            endTimestamp = System.currentTimeMillis();
        } else {
            endTimestamp = statsMgr.date2Timestamp(endDate);
        }

        if (startDate == null || startDate.equals("") || startDate.equals("null")) {
            //set start time to default, i.e., 1 months prior to today
            statsMgr.setCurrentTime(endTimestamp);
            startTimestamp = statsMgr.rollbackMonth(1);
        } else {
            startTimestamp = statsMgr.date2Timestamp(startDate);
        }

        if (startTimestamp > endTimestamp) {
            //set start date 1 months prior to end date
            statsMgr.setCurrentTime(endTimestamp);
            startTimestamp = statsMgr.rollbackMonth(1);
        }

        //form the reporting period dates
        startDate = statsMgr.timestamp2date(startTimestamp);
        endDate = statsMgr.timestamp2date(endTimestamp);
        
        if (timeScale == null || timeScale.equals("") || timeScale.equals("null"))
            timeScale = ServerConstants.WEEKLY_TIMESCALE;
logger.info("startDate="+startDate+", endDate="+endDate+", timeScale="+timeScale);
        statsMgr.prepareUsageData();
logger.info("prepared usage data");
        OutputStream out = response.getOutputStream();

        CategoryDataset dataset = createDataset(startTimestamp, endTimestamp);
logger.info("prepared dataset");
        //determine the number of unique users
        ArrayList<HashSet<String>> weekUsers = statsMgr.getWeeklyUsers(startTimestamp, endTimestamp);
        HashSet<String> uniqueUsers = new HashSet<String>();
        for (int i=0; i<weekUsers.size(); i++) {
            HashSet<String> users = weekUsers.get(i);
            Iterator<String> iter = users.iterator();
            while (iter.hasNext()) {
                uniqueUsers.add(iter.next());
            }
        }
logger.info("unique Users="+uniqueUsers.size());
        JFreeChart chart = createChart(dataset, uniqueUsers.size());

        //Send chart back as a PNG
        try {
            if (chart != null) {
                response.setContentType("image/png");
                ChartUtilities.writeChartAsPNG(out, chart, 500, 300);
            }
        } catch (IOException ioe) {
            System.err.println(ioe.toString());
        } finally {
            out.close();
        }
    }

    /**
     * Generate the chart's dataset.
     * @param startTimestamp Start date of the reporting period
     * @param endTimestamp End data of the reporting period.
     * @return The chart's dataset.
     */
    private CategoryDataset createDataset(long startTimestamp, long endTimestamp) {
        //get the list of modules; used for the X-axis
        ArrayList<String> modules = StatsManager.getInstance().getModules();
logger.info("modules # = "+modules.size());
        // create the dataset
        HashMap<String, Integer> usages = StatsManager.getInstance().getModuleUses(modules, startTimestamp, endTimestamp);
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (String module : modules) {
            Integer cnt = usages.get(module);
logger.info("module:"+module+", uses="+cnt);
            dataset.addValue(cnt==null ? 0 : cnt.intValue(), "Uses", module);
        }

        return dataset;
    }

    /**
     * Create a module histogram chart.
     *
     * @param dataset The chart's dataset.
     * @param uniqueusers The number of unique users over the reporting time period
     * @return The module histogram.
     */
    private JFreeChart createChart(CategoryDataset dataset, int uniqueUsers) {
        String header = "qiWorkbench Module Usage";
        // create the chart
        JFreeChart chart = ChartFactory.createBarChart(
            header+"\n"+startDate+" to "+endDate+"\n"+uniqueUsers+ " Users",       // chart title
            "qiWorkbench Module",             // domain axis label
            "Uses",                  // range axis label
            dataset,                  // data
            PlotOrientation.VERTICAL, // orientation
            false,                    // include legend
            true,                     // tooltips?
            false                     // URLs?
        );

        chart.setBackgroundPaint(Color.white);

        // get a reference to the plot for further customisation...
        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setRangeGridlinePaint(Color.white);

        // set the range axis to display integers only...
        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        rangeAxis.setUpperMargin(0.15);

        CategoryItemRenderer renderer = new CustomRenderer(
            new Paint[] {Color.red, Color.blue, Color.green,
                Color.yellow, Color.orange, Color.cyan,
                Color.magenta, Color.pink}
        );
        renderer.setBaseItemLabelGenerator(
                new StandardCategoryItemLabelGenerator());
        renderer.setSeriesItemLabelsVisible(0, Boolean.TRUE);
        plot.setRenderer(renderer);

        CategoryAxis domainAxis = plot.getDomainAxis();
        domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);

        return chart;
    }
}
