/**
 * Get information about the usage STATS server..
 */

package com.bhpb.qiworkbench.server.usageCharts;

import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.net.URLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.server.ServerConstants;
import com.bhpb.qiworkbench.server.StatsManager;
import com.bhpb.qiworkbench.server.util.ParseQiWbConfig;

public class UsageStats {
    public static Logger logger = Logger.getLogger(UsageStats.class.getName());

    private static UsageStats singleton = null;

    public static UsageStats getInstance() {
        if (singleton == null) {
            singleton = new UsageStats();
        }
        return singleton;
    }

    /**
     * Empty Constructor
     */
    private UsageStats() {}

    /**
     * Get the URL of the usage stats Webpage.
     *
     * @return URL of the stats Webpage on the STATS server.
     */
    public String getStatsURL() {
        //get the content of the qiWbconfig.xml file
        String line = "";
        String configInfo = "";
        String qiWbconfigFileName = System.getenv("QIWB_CONFIG");
        String errorMessage = "";

        if (qiWbconfigFileName == null) {
            errorMessage = "Unable to access workbench config file: the server's QIWB_CONFIG environment variable is not set";
        }
        else {
            BufferedReader in = null;
            try {
                in = new BufferedReader(new FileReader(qiWbconfigFileName));
                while ((line = in.readLine()) != null) {
                    configInfo = configInfo + line;
                }
            } catch (IOException ioe) {
                errorMessage = "IOException occurred while reading the QIWB_CONFIG file: " + ioe.getMessage();
            } finally {
                try {
                    if (in != null) in.close();
                } catch (IOException ioe) {
                    errorMessage = "IOException closing QIWB_CONFIG file";
                }
            }
        }

        if (!errorMessage.equals("")) {
            //TODO: form URL for error Webpage
        }

        //get the URL of the STATS server from the qiWbconfig.xml file
        try {
            ParseQiWbConfig.getInstance().setConfigFile(configInfo);
        } catch (Exception ex) {
            logger.warning("Unable to get stats server URL due to Exception while parsing QIWB_CONFIG file: "
                    + ex.getMessage());
            return "";
        }
        HashMap<String, String> statsAttrs = (HashMap)ParseQiWbConfig.getInstance().getStatsAttributes();
        String statsURL = statsAttrs.get(ServerConstants.STATS_URL_ATTR);
        if (statsURL == null) {
            errorMessage = "URL of STATS server not specified";
            //TODO: form URL for error Webpage
        }

        //form the URL of the usage stats Webpage
        return statsURL + ServerConstants.USAGE_STATS_WEBPAGE;
    }
	
    /**
     * Get the path of the usage stats directory.
     *
     * @return path of the usage stats directory on the STATS server. Empty string if error obtaining.
     */
    public String getStatsPath() {
        //get the content of the qiWbconfig.xml file
        String line = "";
        String configInfo = "";
        String qiWbconfigFileName = System.getenv("QIWB_CONFIG");
        String errorMessage = "";
		String statsPath = "";

        if (qiWbconfigFileName == null) {
            errorMessage = "Unable to access workbench config file: the server's QIWB_CONFIG environment variable is not set";
        }
        else {
            BufferedReader in = null;
            try {
                in = new BufferedReader(new FileReader(qiWbconfigFileName));
                while ((line = in.readLine()) != null) {
                    configInfo = configInfo + line;
                }
            } catch (IOException ioe) {
                errorMessage = "IOException occurred while reading the QIWB_CONFIG file: " + ioe.getMessage();
            } finally {
                try {
                    if (in != null) in.close();
                } catch (IOException ioe) {
                    errorMessage = "IOException closing QIWB_CONFIG file";
                }
            }
        }

        if (!errorMessage.equals("")) {
			//TODO: display errorMessage on error Webpage
            return "";
        }

        //get the path of the usage stats directory from the qiWbconfig.xml file
        ParseQiWbConfig.getInstance().setConfigFile(configInfo);
        HashMap<String, String> statsAttrs = (HashMap)ParseQiWbConfig.getInstance().getStatsAttributes();
        statsPath = statsAttrs.get(ServerConstants.STATS_PATH_ATTR);
        if (statsPath == null) {
            errorMessage = "Path of usage stats directory not specified";
			//TODO: display errorMessage on error Webpage
        }

        return statsPath;
    }
}
