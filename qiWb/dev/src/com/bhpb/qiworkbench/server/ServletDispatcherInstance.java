/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.server;

import com.bhpb.qiworkbench.api.IServerJobService;
import com.bhpb.qiworkbench.api.IServletDispatcher;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletDispatcherInstance implements IServletDispatcher {
    /** Get the line separator of the OS under which the Tomcat server executes. */
    public String getServerOSLineSeparator() {
        return ServletDispatcher.getServerOSLineSeparator();
    }

    /**
     * Check if job service active.
     *
     * @param service Service component.
     * @param try The number of times to try.
     * @return true if registered; otherwise, false
     */
    public boolean isJobServiceActive(IServerJobService jobService, int tries) {
        return ServletDispatcher.isJobServiceActive(jobService, tries);
    }

    /**
     * Acquire a remote job service from the pool. If there is none, create one.
     *
     * @return job service; null if cannot create one.
     */
    public IServerJobService acquireRemoteJobService() {
        return ServletDispatcher.acquireRemoteJobService();
    }

    /**
     * Release a remote job service to the pool.
     *
     * @param jobService Remote job service to release.
     */
    public void releaseRemoteJobService(IServerJobService jobService) {
        ServletDispatcher.releaseRemoteJobService(jobService);
    }
}