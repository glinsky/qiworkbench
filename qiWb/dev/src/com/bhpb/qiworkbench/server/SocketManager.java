/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006-2008  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.Selector;
import java.nio.channels.SelectionKey;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Server-side socket manager for server sockets used in binary IO with the
 * client.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class SocketManager {
    public static Logger logger = Logger.getLogger(SocketManager.class.getName());
    
    /** The maximum server socket channels that can be allocated to a client, i.e., in use simultaneously. */
    private static final int CLIENT_SOCKET_LIMIT = 30;
    
    /** Time in milliseconds after which a socket channel in the pool expires and is deleted from the pool */
    private static final long INACTIVE_THRESHOLD = 15 * 60 * 1000; // 15 minutes

    private static SocketManager singleton = null;
    
    /** Server-side socket pool */
    ArrayList<BoundIOSocket> socketPool;
    
    /** Count of server socket channels allocated to a client. Key is sessionID, value is the allocation count */
    HashMap<String, Integer> allocatedSockets;
    
    /** Assigned server socket channel */
    ServerSocketChannel serverSocketChannel = null;
    
    private SocketManager() {
        socketPool = new ArrayList<BoundIOSocket>();
        allocatedSockets = new HashMap<String, Integer>();
    }
    
    /**
     * Get the singleton instance of this class. If the Socket Manager doesn't
     * exist, create it.
     */
    public static SocketManager getInstance() {
        if (singleton == null) {
            singleton = new SocketManager();
        }
        return singleton;
    }
    
    /** 
     * Check if a client has reached the allocation limit for server sockets.
     * @param sessionID Client's session ID
     * @return true if allocation limit reached; otherwise, false
     */
    private synchronized boolean atSocketLimit(String sessionID) {
        Integer allocationCount = allocatedSockets.get(sessionID);
        if (allocationCount == null) return false;
         
        return (allocationCount.intValue() < CLIENT_SOCKET_LIMIT) ? false : true;
    }
    
    /**
     * Increment the socket allocation count for a client
     * @param sessionID Client's session ID
     * @return The incremented allocation count
     */
     private synchronized int incrSocketLimit(String sessionID) {
         Integer allocationCount = allocatedSockets.get(sessionID);
         if (allocationCount == null) {
             allocatedSockets.put(sessionID, new Integer(1));
             return 1;
         }
         
         int val = allocationCount.intValue();
         val++;
         allocatedSockets.put(sessionID, Integer.valueOf(val));
         
         return val;
     }
     
    /**
     * Decrement the socket allocation count for a client
     * @param sessionID Client's session ID
     * @return The decremented allocation count. Zero if none have been 
     * allocated or the allocation is zero before the decrement.
     */
     private synchronized int decrSocketLimit(String sessionID) {
         Integer allocationCount = allocatedSockets.get(sessionID);
         if (allocationCount == null) {
             allocatedSockets.put(sessionID, new Integer(0));
             return 0;
         }
         
         int val = allocationCount.intValue();
         if (val == 0) return 0;
         val--;
         allocatedSockets.put(sessionID, Integer.valueOf(val));
         
         return val;
     }
    
    /**
     * Get server-side channels for communicating with the client. Use an
     * available one in the socket pool, if any; otherwise, open a new one.
     * Increment the client's socket allocation count.
     * @param workbenchSessionID Workbench session ID that uniquely identifies
     * the client.
     * @return Either resused server-side channels or a newly opened server 
     * socket channel with no associated socket channel for communicating with 
     * the client; null if already reached allocation limit.
     */
    public BoundIOSocket acquireSocketChannel(String workbenchSessionID) {
        BoundIOSocket boundIOSocket;
        logger.info("Acquire server ServerSocketChannel. workbench sessionID="+workbenchSessionID);
        
        //check if client has reached the allocation limit.
        if (atSocketLimit(workbenchSessionID)) {
            return null;
        }
        
        incrSocketLimit(workbenchSessionID);
        int size = socketPool.size();
        //First check the pool for an available socket
        if (size != 0) {
            boundIOSocket = findReusableSocket(workbenchSessionID);
            //If there is no server socket channel that can be reused, open a
            //new one.
            if (boundIOSocket == null) {
                serverSocketChannel = openSocketChannel();
                logger.info("No reusable client SocketChannel pool. Open a new one on port "+serverSocketChannel.socket().getLocalPort());
                boundIOSocket = new BoundIOSocket(serverSocketChannel);
            } else {
                boundIOSocket.setLastUsage(System.currentTimeMillis());
                logger.info("Reusing server SocketSocketChannel on port "+serverSocketChannel.socket().getLocalPort());
            }
        } else {
            //Open a new socket
            serverSocketChannel = openSocketChannel();
            logger.info("Pool of server ServerSocketChannels empty. Open a new one on port "+serverSocketChannel.socket().getLocalPort());
            boundIOSocket = new BoundIOSocket(serverSocketChannel);
        }
        
        return boundIOSocket;
    }
    
    /**
     * Find a reusable server socket channel from the pool of available sockets.
     * If one is found, remove it from the pool.
     * @param workbenchSessionID Workbench session ID that uniquely identifies
     * the client
     * @return Reusable server socket channel, if any; otherwise, null.
     */
    private BoundIOSocket findReusableSocket(String workbenchSessionID) {
        BoundIOSocket boundIOSocket;
        //search the pool for an available server socket
        Iterator<BoundIOSocket> iter = socketPool.iterator();
        while (iter.hasNext()) {
            boundIOSocket = iter.next();
            if (boundIOSocket.getWorkbenchSessionID().equals(workbenchSessionID)) {
                //ignore sockets that are still in use by the client
                if (boundIOSocket.isSocketDirty()) continue;
                //remove the bounded server socket from the pool
                iter.remove();
                return boundIOSocket;
            }
        }
         
        return null;
    }
    
    /**
     * Open a new socket
     * @return New opened server channel
     */
    private ServerSocketChannel openSocketChannel() {
        try {
            //Allocate an unbounded server socket channel
            serverSocketChannel = ServerSocketChannel.open();
            //Set nonblocking mode for the listening socket
            serverSocketChannel.configureBlocking(false);
    
            //Get the associated ServerSocket
            ServerSocket serverSocket = serverSocketChannel.socket();
            serverSocket.setReuseAddress(true);
    
            //Set the port the server channel will listen to
            serverSocket.bind(null);
        } catch (IOException ioe) {
            serverSocketChannel = null;
        }

        return serverSocketChannel;
    }
     
    /**
     * Return a server-side socket to the socket pool for potential reuse. It
     * will be available for reuse when the client notifies the server it is
     * finished with the socket, i.e., has read all the data transmitted over
     * the socket by the server. Decrement the server's socket allocation count.
     * @param channel Server socket channel available for reuse.
     * @param selector Server socket channel's selector.
     * @param commChannel Communication socket channel between client-server
     * @param workbenchSessionID Workbench session ID that uniquely identifies
     * the client
     */
    public void releaseSocket(ServerSocketChannel serverSocketChannel, Selector selector, SocketChannel commChannel, String workbenchSessionID) {
        if (commChannel != null) {
            logger.info("Release server socket channel to pool: server port "+serverSocketChannel.socket().getLocalPort()+", communication port "+ commChannel.socket().getLocalPort()+", workbench sessionID="+workbenchSessionID);
        } else {
            logger.info("Release server socket channel to pool: server port "+serverSocketChannel.socket().getLocalPort()+", no communication port, workbench sessionID="+workbenchSessionID);
        }
        
        //decrement count before making socket available for reuse
        decrSocketLimit(workbenchSessionID);
		//Note: Constructor sets its dirty flag
        BoundIOSocket bios = new BoundIOSocket(serverSocketChannel, selector, commChannel, workbenchSessionID);
        socketPool.add(bios);
    }
    
    /**
     * Make a server-side socket in the socket pool, marked as potentially
     * available for reuse, as available for reuse. Called when receive
     * notification from the client it is finished with the socket.
     * @param port The socket's port
     */
     public void releaseSocket(int port) {
        BoundIOSocket boundIOSocket;
        //search the pool for the server socket. Unset its dirty flag so it is
        //available for reuse.
        Iterator<BoundIOSocket> iter = socketPool.iterator();
        while (iter.hasNext()) {
            boundIOSocket = iter.next();
            if (boundIOSocket.getServerPort() == port) {
                boundIOSocket.setDirtyFlag(false);
                logger.info("Release server socket channel for reuse on port "+port);
                break;
            }
        }
     }
      
    /**
     * Determine which server socket channels have been inactive too long. Close
     * them and delete them from the server's socket pool.
     * <p>
     * It suffices to simply delete them, for the dangling, matching socket
     * channel on the client side will eventually expire and be deleted from
     * the client's socket pool.
     */
    public void closeInactiveSockets() {
        //search the socket pool for a socket channel whose last usage exceeds
        //the timelimit threshold
        Iterator<BoundIOSocket> iter = socketPool.iterator();
        while (iter.hasNext()) {
            BoundIOSocket boundIOSocket = iter.next();
            long lastUsage = boundIOSocket.getLastUsage();
            long currentTime = System.currentTimeMillis();
            if (currentTime-lastUsage >= INACTIVE_THRESHOLD) {
                //mark channel as unavailable for allocation
                boundIOSocket.setUnavailable();
                
                //remove from allocated sockets list
                allocatedSockets.remove(boundIOSocket.getWorkbenchSessionID());
                
                //close the server socket channel
                closeSocket(boundIOSocket.getServerSocketChannel(), boundIOSocket.getServerChannelSelector(), boundIOSocket.getCommSocketChannel());
                
                Double inactiveTime = ((currentTime-lastUsage)/1000.0)/60.0;
                logger.info("closeInactiveSockets: Closing server channel on port #"+boundIOSocket.getServerPort()+". Inactive for "+inactiveTime+" minutes");
                //remove from the pool
                iter.remove();
            }
        }
    }
    
    /**
     * Close a server socket channel and its associated socket channel
     * @param serverChannel The server socket channel to be closed
     * @param sel Selector server socket is registered with
     * @return Empty string if socket successfully closed; otherwise, the reason for failure.
     */
    public String closeSocket(ServerSocketChannel serverChannel, 
            Selector sel, SocketChannel commChannel) {
        String errMsg = "";
        ServerSocket serverSocket = serverChannel.socket();
        int serverPort = serverSocket.getLocalPort();

        try {
            if (serverSocket != null && (serverSocket.isBound() || !serverSocket.isClosed())) {
                logger.info("Closing server socket on port #" + serverPort + "...");
                serverSocket.close();
                logger.info("Server socket closed.");
            } else {
                //do not log this, it may already be closed
                //errMsg += "Didn't close server socket: " + serverSocket;
            }

            if (sel != null && sel.isOpen()) {
                logger.info("Closing channel's selector on port #" + serverPort + "...");
                sel.close();
                logger.info("Selector closed.");
            } else {
                //do not log this, it may already be closed
                //errMsg += "; Didn't close selector: " + sel;
            }
            
            if (commChannel != null && commChannel.isOpen()) {
                logger.info("Closing communication channel on port #" + commChannel.socket().getLocalPort() + "...");
                commChannel.close();
                logger.info("communication channel closed.");
            } else {
                //do not log this, it may already be closed
                //errMsg += "; Didn't close channel: " + serverChannel;
            }

            if (serverChannel != null && serverChannel.isOpen()) {
                logger.info("Closing server channel on port #" + serverPort + "...");
                serverChannel.close();
                logger.info("Server channel closed.");
            } else {
                //do not log this, it may already be closed
                //errMsg += "; Didn't close server channel: " + serverChannel;
            }
            
            if (!errMsg.equals("")) {
                errMsg = "SEVERE: Could not release server socket. CAUSE: " + errMsg;
            }
        } catch (IOException ioe) {
            return "CAUSE2: " + ioe.getMessage();
        } finally {
            //put the port back in circulation
            logger.info("Releasing port #" + serverPort);
        }

        return errMsg;
    }
    
    /**
     * Information about the client-server sockets bound together during geoIO.
     */
    public class BoundIOSocket {
        /** session ID of client workbench */
        String workbenchSessionID = "";
        
        /** server socket channel (assigned by ServletDispatcher) */
        ServerSocketChannel serverSocketChannel;
        
        /** selector server socket channel is registered with */
        Selector registeredSelector;
        
        /** socket channel used by the server to transmit data */
        SocketChannel commSocketChannel;
        
        /** timestamp of last usage, i.e., server socket involved in geoIO */
        long lastUsage = 0L;
        
        /** Reused socket indicator */
        boolean reusedSocket = false;
        
        /** Unavailable for reuse indicator. Set when socket in the process of being deleted from the pool. */
        boolean unavailable = false;
        
        /** In use by the client and unavailable for reuse. Unset when notified by the client it has read all the data transmitted over the socket. false == reusable; true == in use by client*/
        boolean dirtyFlag = false;
        
        /**
         * Constructor used for a newly opened server socket channel.
         */
        public BoundIOSocket(ServerSocketChannel serverSocketChannel) {
            this.serverSocketChannel = serverSocketChannel;
            this.registeredSelector = null;
            this.commSocketChannel = null;
            this.workbenchSessionID = "";
            this.lastUsage = 0L;
            reusedSocket = false;
            dirtyFlag = true;
        }
        
        /**
         * Constructor used when the server is finished with the socket and it
		 * is added back to the pool..
         */
        public BoundIOSocket(ServerSocketChannel serverSocketChannel, Selector selector, SocketChannel commSocketChannel, String workbenchSessionID) {
            this.serverSocketChannel = serverSocketChannel;
            this.registeredSelector = selector;
            this.commSocketChannel = commSocketChannel;
            this.workbenchSessionID = workbenchSessionID;
            this.lastUsage = System.currentTimeMillis();
            reusedSocket = true;
			dirtyFlag = true;
        }
        
        public boolean isReusedSocket() {
            return reusedSocket;
        }
        
        public boolean isSocketUnavailable() {
            return unavailable;
        }
        
        public boolean isSocketDirty() {
            return dirtyFlag;
        }
        
        //GETTERS
        public String getWorkbenchSessionID() {
            return workbenchSessionID;
        }
        public int getServerPort() {
            return serverSocketChannel.socket().getLocalPort();
        }
        public ServerSocketChannel getServerSocketChannel() {
            return serverSocketChannel;
        }
        public SocketChannel getCommSocketChannel() {
            return commSocketChannel;
        }
        public Selector getServerChannelSelector() {
            return registeredSelector;
        }
        public long getLastUsage() {
            return lastUsage;
        }
        public boolean getDirtyFlag() {
            return dirtyFlag;
        }
        
        //SETTERS
        public void setWorkbenchSessionID(String workbenchSessionID) {
            this.workbenchSessionID = workbenchSessionID;
        }
        public void setServerSocketChannel(ServerSocketChannel channel) {
            this.serverSocketChannel = channel;
        }
        public void setCommSocketChannel(SocketChannel channel) {
            this.commSocketChannel = channel;
        }
        public void setServerChannelSelector(Selector selector) {
            this.registeredSelector = selector;
        }
        public void setLastUsage(long lastUsage) {
            this.lastUsage = lastUsage;
        }
        public void setUnavailable() {
            unavailable = true;
        }
        public void setDirtyFlag(boolean val) {
            dirtyFlag = val;
        }
    }
}
