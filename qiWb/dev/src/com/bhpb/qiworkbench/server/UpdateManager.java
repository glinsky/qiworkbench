//server
package com.bhpb.qiworkbench.server;

import java.util.ArrayList;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;

import com.bhpb.qiworkbench.updater.UpdateItem;
import com.bhpb.qiworkbench.updater.UpdateConstants;

/**
 * Server Side Update Manager
 * @author Marcus Vaal
 */
public class UpdateManager implements Runnable {

	private static Logger logger = Logger.getLogger(UpdateManager.class.getName());

	private static UpdateManager singleton = null;

	/**
	 * UpdateManager Constuctor
	 */
	public UpdateManager() {
	}

	/**
	 * Creates a singleton
	 * @return
	 */
	public static UpdateManager getInstance() {
		if(singleton == null){
			singleton = new UpdateManager();
			new Thread(singleton).start();
		}
		return singleton;
	}

	/**
	 * Initializes the server side UpdateManager
	 */
	public void init() {
		//Initialize Here if needed

		//QIWConstants.SYNC_LOCK.unlock();
	}

	/**
	 * Run method (needed for Runnable)
	 */
	public void run() {
  		//QIWConstants.SYNC_LOCK.lock();

		init();
	}

	/**
	 * Get available components from the server side tomcat
	 * @param serverArray ArrayList containing a servers information:	Cell 0 - Server URL 
	 * 																	Cell 1 - Components Folder
	 * @return List of components on the server side tomcat
	 */
	public ArrayList<UpdateItem> getAvailableComponents(ArrayList<String> serverArray) {
		logger.finest("Getting Available Components for Download from server");
		ArrayList<UpdateItem> updateItems = new ArrayList<UpdateItem>();
		String webapps = "/webapps";
		String compCachePath = System.getenv("CATALINA_HOME") + webapps + serverArray.get(1); 
		File compCacheDir = new File(compCachePath);
		//Check if server has a component cache
		if (!compCacheDir.isDirectory()) {
			logger.fine("Returning ArrayList of UpdateItems of size: " + updateItems.size());
			return updateItems;
		}
		File[] files = compCacheDir.listFiles();
		for (int i=0; i<files.length; i++) {
			if (!files[i].isDirectory()) {
				String fileName = files[i].getName();
				if (fileName.endsWith(".xml")) {
					try {
						DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
						DocumentBuilder parser = factory.newDocumentBuilder();
						Document xmlDocument = parser.parse(files[i]);
						NodeList componentNodeList = xmlDocument.getElementsByTagName(UpdateConstants.TAG_COMPONENT);
						Node node = componentNodeList.item(0);
						String filePath = "";
				        int idx = compCachePath.indexOf(webapps);
				        if(idx != -1) {
				        	//filePath = serverArray.get(0) + compCachePath.substring(idx+webapps.length(),(compCachePath.length()));
				        	filePath = serverArray.get(0) + serverArray.get(1);
				        }
						UpdateItem uI = new UpdateItem(node, files[i].lastModified(), filePath, fileName, files[i].length());
						updateItems.add(uI);
					} catch (IOException ioe) {
						logger.severe("Unable to access component xml: "+fileName+"; CAUSE:"+ioe.getMessage());
					} catch (SAXException saxe) {
						logger.severe("Unable to access component xml: "+fileName+"; CAUSE:"+saxe.getMessage());
					} catch (ParserConfigurationException pce) {
						logger.severe("Unable to access component xml: "+fileName+"; CAUSE:"+pce.getMessage());
					}
				}
			}
		}
		logger.fine("Returning ArrayList of UpdateItems of size: " + updateItems.size());
		return updateItems;
	}
}
