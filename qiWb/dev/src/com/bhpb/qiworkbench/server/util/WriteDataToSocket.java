/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench.server.util;

import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.server.SocketManager;

import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Logger;
import java.net.ServerSocket;

import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.Selector;
import java.nio.channels.SelectionKey;

import com.bhpb.qiworkbench.server.SocketManager.BoundIOSocket;

/**
 * Write byte data to a server-side communication socket channel which the 
 * client is connected to and from which the client will read the data. The 
 * communication channel may be one used before and is therefore established
 * or a new one which must be established, i.e., connected to the client-side
 * port assigned by the server.
 * <p>
 * It is the callee's responsibility to release the socket channel, regardless
 * if the write was successfull or not.
 *
 * @author Gil Hansen.
 * @version 1.0
 */
public class WriteDataToSocket {
    private static Logger logger = Logger.getLogger(WriteDataToSocket.class.getName());
    private static final int BUF_SIZE = 64 * 1024;
    /** Data to be written */
    byte[] data;
    /** ServerSocketChannel obtained by ServletDispatcher, used to detect incoming connection */
    final ServerSocketChannel serverSocketChannel;
    /** Socket associated with the ServerChannel */
    final ServerSocket serverSocket;
    /** Port assigned to the server socket channel */
    final int port;
    /** selector used to detect an OP_ACCEPT event on the serverSocketChannel */
    Selector sel = null;
    /** Channel associated with the OP_ACCEPT selector, used to send data */
    ServerSocketChannel server = null;
    /** Socket channel over which data will be sent.
     * Accepted channel between server and client 
     */
    SocketChannel commChannel = null;
    /** Status messages returned from write() */
    String errorMsg = "";
    
    /** Manager to get an available socket channel used to commuinicate with the server */
    SocketManager socketManager;
    
	/** Session ID for the client's workbench instance */
	String workbenchSessionID = "";

    /** Default constructor
     * @param boundIOSocket Wrapper for
     * <ul>
     * <li> serverSocketChannel Channel assigned by server to listen for incoming connections </li>
     * <li> selector Selector the server socket channel is registered with
     * <li> commChannel Communication socket channel between client-server </li>
     * <li> workbenchSessionID Workbench session ID that uniquely identifies the client. </li>
     * </ul>
     */
    public WriteDataToSocket(BoundIOSocket boundIOSocket) {
        this.serverSocketChannel = boundIOSocket.getServerSocketChannel();
        this.serverSocket = serverSocketChannel.socket();
        this.port = serverSocket.getLocalPort();
        this.sel = boundIOSocket.getServerChannelSelector();
        this.commChannel = boundIOSocket.getCommSocketChannel();
        this.workbenchSessionID = boundIOSocket.getWorkbenchSessionID();
        
        socketManager = SocketManager.getInstance();
    }
    
    /**
     * Release this instance's server socket for reuse.
     */
    public void releaseSocket() {
        socketManager.releaseSocket(serverSocketChannel, sel, commChannel, workbenchSessionID);
    }

    /**
     * Allocate the server channel. Create a readiness selector and save in class
     * instance variable. Create a server socket channel and listen for the client
     * to connect to the port.
     * @param port The port assigned by the server.
     * @return empty string if server channel established successfuly; otherwise, reason why it
     * failed.
     */
    public String establishServerSocket() {
        //Note: If server socket channel is being reused, the communication
        //channel and selector are already established.
        
        if (serverSocketChannel.isRegistered()) {
            logger.info("establishServerSocket: reused server socket channel is already established on port "+port);
            return "";
        }
        
        //if all available sockets are in use, wait up to 20 seconds for one to become available before returning an abnormal response
        final int WAIT_TIME = 1000; //milliseconds
        final int MAX_ATTEMPTS = QIWConstants.RESPONSE_TIMEOUT / WAIT_TIME;
        boolean successfulInit = false;

        for (int attempt = 0; (attempt < MAX_ATTEMPTS && successfulInit == false); attempt++) {
            try {
                //create a readiness selector for the server socket channel
                sel = Selector.open();

                //Register the server socket channel with the selector
                serverSocketChannel.register(sel, SelectionKey.OP_ACCEPT);
                successfulInit = true;
                logger.info("Registered server socket on port " + port);
            } catch (ClosedChannelException cce) {
                errorMsg = "WriteDataToSocket::establishServerSocket ClosedChannelException setting up server channel: " + cce.getMessage() + "; serverSocketChannel: " + serverSocketChannel;
                
                try {
                    Thread.sleep(WAIT_TIME);
                } catch (InterruptedException ie) {
                    logger.warning("Caught " + ie.getMessage() + " while sleeping 1000ms");
                }
            } catch (IOException ioe) {
                errorMsg = "WriteDataToSocket::establishServerSocket IOException setting up server channel: " + ioe.getMessage() + "; serverSocketChannel: " + serverSocketChannel;
                logger.warning(errorMsg);
                
                try {
                    Thread.sleep(WAIT_TIME);
                } catch (InterruptedException ie) {
                    logger.warning("Caught " + ie.getMessage() + " while sleeping 1000ms");
                }
            }
        }

        if (!successfulInit) {
            final String MAX_RETRY_MSG = "Failed to establish server socket after MAX_RETRIES";
            logger.warning(MAX_RETRY_MSG);
            logger.warning("Last error message: " + errorMsg);
            return errorMsg;
        }

        //Listen on port for the client to connect
        try {
            long startTime = System.currentTimeMillis();
            int n = sel.select(5*WAIT_TIME);
/*NOTE: 2nd call on select() never returns
            while (n == 0 && (System.currentTimeMillis() - startTime) < 300 * WAIT_TIME) {
                logger.info("Failed to select key(s) on port: " + port + " after " + WAIT_TIME + " ms, trying again...");
                n = sel.select(WAIT_TIME);
            }
logger.info("Exited while loop waiting for selector keys. n="+n);d
*/
            if (n == 0) {
                errorMsg = "WriteDataToSocket::establishServerSocket: No client connected yet" + "; serverSocketChannel: " + serverSocketChannel;
                return errorMsg;
            } else {
                logger.info("WriteDataToSocket: selected " + n + " keys on port: " + port + " after " + (System.currentTimeMillis() - startTime) + " ms.");
                
                //There is only one entry in the set of selected
                //keys so there is nothing to iterate over.
                Iterator iter = sel.selectedKeys().iterator();
                SelectionKey key = (SelectionKey) iter.next();
                //Remove it from the list to indicate that it is being processed
                iter.remove();

                //Check if a new connection coming in
                if (key.isAcceptable()) {
                    server = (ServerSocketChannel) key.channel();
                    //Block until a new connection is available
                    logger.info("Blocking until a connection is made by the client");
                    commChannel = server.accept();
                    logger.info("Acquired server communication channel on port "+commChannel.socket().getLocalPort()+" server channel on port "+port);
                } else {
                    logger.info("Key not acceptable: key=" + key + ", port=" + port);
                }

                return "";
            }
        } catch (IOException ioe) {
            errorMsg = "WriteDataToSocket::establishServerSocket: IO exception listening on port: " + ioe.getMessage() + "; serverSocketChannel: " + serverSocketChannel;
            return errorMsg;
        }
    }

    /**
     * Write byte data to an established socket channel the client is connected to.
     * <p> 
     * Note: It is up to the service to release the socket channel.
     *
     * @param data The byte data to be written to the socket.
     * @return empty string if write successful; otherwise, reason why write
     * failed.
     */
    public String write(byte[] data) {
        //Make sure communication channel was accepted
        if (commChannel != null) {
            logger.info("Writing " + data.length + " bytes of data to port #" + port);
            String opStatus = sendData(data, commChannel);
            logger.info("Write on port #" + port + " completed with opStatus: " + opStatus);
            return opStatus;
        } else {
            errorMsg = "WriteDataToSocket::write Channel not established; serverSocketChannel: " + serverSocketChannel;
            logger.severe(errorMsg);
            return errorMsg;
        }
    }

    /**
     * Send data to the client (over the established socket channel).
     *
     * @param data Byte data to be sent
     * @param channel The newly connected socket channel to send the data to
     * @return empty string if data sent successfully; otherwise, the reason
     * the send failed..
     */
    private String sendData(byte[] data, SocketChannel commChannel) {
        ByteBuffer buf = ByteBuffer.allocateDirect(BUF_SIZE);
        int bufCapacity = BUF_SIZE;

        int dataSize = data.length;

        //determine the number of writes, i.e., multiples the data is of
        //the byte buffer
        int numWrites = dataSize / bufCapacity + ((dataSize % bufCapacity) > 0 ? 1 : 0);

        int bytesWrote = 0;
        for (int i = 0; i < numWrites; i++) {
            buf.clear();

            int bytesToWrite = i != numWrites - 1 ? bufCapacity : dataSize % bufCapacity;

            //fill up byte buffer with next chunk
            buf.put(data, bytesWrote, bytesToWrite);
            bytesWrote += bytesToWrite;

            //write the byte buffer out over the communication channel
            //prepare to be drained
            buf.flip();

            try {
                //communication channel may not take it all at once
                while (commChannel.write(buf) > 0) {
                    Thread.yield();
                }
            } catch (IOException ioe) {
                errorMsg = "WriteDataToSocket::sendData: IO error writing to commChannel: " + ioe.getMessage() + "; serverSocketChannel: " + serverSocketChannel;
                logger.severe(errorMsg);
                return errorMsg;
            }
        }
        
        return "";
    }
}
