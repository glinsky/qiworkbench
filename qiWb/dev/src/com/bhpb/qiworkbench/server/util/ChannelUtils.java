/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench.server.util;

import java.io.IOException;
import java.net.ServerSocket;
import java.nio.channels.ServerSocketChannel;

/**
 * Server-side channel utilities.
 *
 * @author Woody Folsom
 * @version 1.1
 */
public class ChannelUtils {
    /**
     * Get a server socket channel.
     * <p>
     * Note: geoIO commands use the server's Socket Manager to get a new or
     * available server socket channel. Only the remote read commands for SEGY
     * and BHP-SU, used only by the bhpViewer, use this method.
     * @return Server socket channel.
     */
    static public ServerSocketChannel getChannel() throws IOException {

        //Allocate an unbounded server socket channel
        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        //Set nonblocking mode for the listening socket
        serverChannel.configureBlocking(false);

        //Get the associated ServerSocket
        ServerSocket serverSocket = serverChannel.socket();
        serverSocket.setReuseAddress(true);
        //serverSocket.setTcpNoDelay(true);
        //serverSocket.setKeepAlive(true);

        //Set the port the server channel will listen to
        serverSocket.bind(null);

        return serverChannel;
    }
}
