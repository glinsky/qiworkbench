/*
 * Parse the qiWorkbench's configuration XML file and save the available info.
 */
package com.bhpb.qiworkbench.server.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * @author Marcus Vaal
 * @author Gil Hansen
 * @version 1.1
 */
public class ParseQiWbConfig implements Runnable {
    private static Logger logger = Logger.getLogger(ParseQiWbConfig.class.getName());

    private static ParseQiWbConfig singleton = null;

    public static ParseQiWbConfig getInstance() {
        if(singleton == null){
            singleton = new ParseQiWbConfig();
            new Thread(singleton).start();
        }
        return singleton;
    }

    /**
     * Empty Constructor
     */
    private ParseQiWbConfig() {}

    /**
     * Reads and parses the configuration file and sets retrievable properties
     * @param configText
     */
    public void setConfigFile(String configText) {
        if(configText.equals("")) {
            return;
        }
        File temp = new File("configFile.xml");
        this.configText = configText;

        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(temp));
            out.write(configText);
            out.flush();

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = factory.newDocumentBuilder();
            Document xmlDocument = parser.parse(temp);
            NodeList componentNodeList = xmlDocument.getElementsByTagName("configuration");
            Node n = componentNodeList.item(0);
            setConfigProps(n);
            out.close();
        } catch (IOException ioe) {
            logger.severe("Unable to access component xml: " + temp.getName() + "; CAUSE:"+ioe.getMessage());
        } catch (SAXException saxe) {
            logger.severe("Unable to access component xml: " + temp.getName() + "; CAUSE:"+saxe.getMessage());
        } catch (ParserConfigurationException pce) {
            logger.severe("Unable to access component xml: " + temp.getName() + "; CAUSE:"+pce.getMessage());
        } finally {
            temp.delete();
        }
    }

    /**
     * Empty Initialization
     */
    private static void init() {
    }

    /**
     * Runs the singlton
     */
    public void run() {
        init();
    }

    public Map<String, String> getDistributionAttributes() {
        return distAttrs;
    }

    /**
     * Get the attributes stored in the PUC node
     * @return Map of attributes in PUC node
     */
    public Map<String, String> getPucAttributes() {
        return pucAttrs;
    }

    /**
     * Get the attributes stored in the STATS node
     * @return Map of attributes in STATS node
     */
    public Map<String, String> getStatsAttributes() {
        return statsAttrs;
    }

    /**
     * Get the attributes of the Update Center nodes
     * @return Maps of attributes in Update Center nodes
     */
    public ArrayList<Map<String, String>> getUpdateCentersAttributes() {
        return updateCenterAttrs;
    }

    /**
     * Returns the attributes of the Runtime Tomcat nodes
     * @return Maps of attributes  in Runtime Tomcat nodes
     */
    public ArrayList<Map<String, String>> getRuntimeTomcatAttributes() {
        return runtimeTomcatAttrs;
    }

    /**
     * Sets the returnable properties for the config file located in the deploy server
     * @param node properties node parsed from the config file
     */
    private void setConfigProps(Node node) {
        distAttrs = setDistAttrs(node);
        statsAttrs = setSTATSAttrs(node);
        pucAttrs = setPUCAttrs(node);
        updateCenterAttrs = setUpdateCentersAttrs(node);
        runtimeTomcatAttrs = setTomcatAttrs(node);
    }

    private Map<String, String> setDistAttrs(Node node) {
        Map<String, String> items = new HashMap<String, String>();
        Node n = getTagNameNode(node,"DISTRIBUTION");
        if (n != null) {
            items = getAttributes(n);
        } else {
            items = null;
        }
        return items;
    }

    /**
     * Returns the first node in the config file
     * @param node Root node
     * @param nodeName Name of the root node
     * @return First node in the config file
     */
    private Node getTagNameNode(Node node, String nodeName) {
        NodeList nodes = ((Element)node).getElementsByTagName(nodeName);
        Node n;
        if (nodes != null && nodes.getLength () > 0) {
            n = nodes.item(0); //Should only be one
        } else {
            //nodes not found
            n = null;
        }
        return n;
    }

    /**
     * Retrieves the attributes of a node
     * @param n Node the attributes will come from
     * @return Map of the attributes of the given node
     */
    private Map<String, String> getAttributes (Node n) {
        Map<String, String> items = new HashMap<String, String>();
        for (int i=0; i<n.getAttributes().getLength(); i++) {
            items.put(n.getAttributes().item(i).getNodeName(), n.getAttributes().item(i).getNodeValue());
        }
        return items;
    }

    /**
     * Sets the values of the PUC attributes
     * @param node PUC node
     * @return Map of the values contained in PUC node; empty list if no PUC node
     */
    private Map<String, String> setPUCAttrs(Node node) {
        Map<String, String> items = new HashMap<String, String>();
        Node n = getTagNameNode(node,"PUC");
        if (n != null) {
            items = getAttributes(n);
        }

        return items;
    }

    /**
     * Sets the values of the STATS attributes
     * @param node STATS node
     * @return Map of the values contained in STATS node; empty list if no STATS node
     */
    private Map<String, String> setSTATSAttrs(Node node) {
        Map<String, String> items = new HashMap<String, String>();
        Node n = getTagNameNode(node,"STATS");
        if (n != null) {
            items = getAttributes(n);
        }
        return items;
    }

    /**
     * Sets the values of the Update Center attributes
     * @param node Update Center Node
     * @return ArrayList of Maps cotained in the Update Centers Node; empty list
     * if no Update Center Node.
     */
    private ArrayList<Map<String, String>> setUpdateCentersAttrs(Node node) {
        ArrayList<Map<String, String>> allUpdateCenters = new ArrayList<Map<String, String>>();
        Node n = getTagNameNode(node, "UPDATE_CENTERS");
        if (n == null) return allUpdateCenters;
        NodeList updateCenters = n.getChildNodes();
        for (int i=0; i<updateCenters.getLength(); i++) {
            if (updateCenters.item(i).getNodeName().equals("UPDATE_CENTER")) {
                allUpdateCenters.add(getAttributes(updateCenters.item(i)));
            }
        }
        return allUpdateCenters;
    }

    /**
     * Sets the values of the Runtime Tomcats
     * @param node Runtime Tomcats node
     * @return ArrayList of Maps ontained in the Runtime Tomcats Node; empty list
     * if no Runtime Tomcats node
     */
    private ArrayList<Map<String, String>> setTomcatAttrs(Node node) {
        Node n = getTagNameNode(node, "RUNTIME_TOMCATS");
        if (n == null) return runtimeTomcatAttrs;
        NodeList updateCenters = n.getChildNodes();
        for (int i=0; i<updateCenters.getLength(); i++) {
            if (updateCenters.item(i).getNodeName().equals("RUNTIME_TOMCAT")) {
                runtimeTomcatAttrs.add(getAttributes(updateCenters.item(i)));
            }
        }
        return runtimeTomcatAttrs;
    }

    //Variable Declarations
    Map<String, String> pucAttrs = new HashMap<String, String>();
    Map<String, String> distAttrs = new HashMap<String, String>();
    ArrayList<Map<String, String>> updateCenterAttrs = new ArrayList<Map<String, String>>();
    ArrayList<Map<String, String>> runtimeTomcatAttrs = new ArrayList<Map<String, String>>();
    String configText = "";
    Map<String, String> statsAttrs = new HashMap<String, String>();
}
