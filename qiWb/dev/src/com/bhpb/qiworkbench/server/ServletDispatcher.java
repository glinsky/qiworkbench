/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.server;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.lang.NumberFormatException;
import java.net.URL;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.nio.ByteOrder;
import java.nio.channels.ServerSocketChannel;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thoughtworks.xstream.XStream;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IServerJobService;
import com.bhpb.qiworkbench.api.IServerMessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messaging.ServerMessagingManager;
import com.bhpb.qiworkbench.server.SocketManager;
import com.bhpb.qiworkbench.server.SocketManager.BoundIOSocket;
import com.bhpb.qiworkbench.server.multipart.FilePartition;
import com.bhpb.qiworkbench.server.multipart.MsgPartition;
import com.bhpb.qiworkbench.server.multipart.MultiPartitionParser;
import com.bhpb.qiworkbench.server.multipart.IPartition;
import com.bhpb.qiworkbench.server.services.GeoIOService;
import com.bhpb.qiworkbench.server.services.IOService;
import com.bhpb.qiworkbench.server.services.JobService;
import com.bhpb.qiworkbench.server.util.ChannelUtils;
import com.bhpb.qiworkbench.server.util.ComponentUtils;

import java.io.InputStream;
import java.io.InputStreamReader;

import com.thoughtworks.xstream.security.AnyTypePermission;       // JE FIELD                             

public class ServletDispatcher extends HttpServlet {
    private static Logger logger = Logger.getLogger(ServletDispatcher.class.getName());
    
    /** Time in milliseconds after which a check is made to close inactive client socket channels. */
    private static final long POLLING_THRESHOLD = 5 * 60 * 1000; // 5 minutes
    
    /** Last time the client sockets were polled for inactivity */
    private long lastPollTime = System.currentTimeMillis();

    //URL of this server. Obtained from the PING_SERVER_CMD.
    private static String serverURL = "";
    private static ArrayList<String> serverArray = new ArrayList<String>();
    
	private static final String XML_DIR = "xml";
	private static final String QIWB_CONFIG = "qiWbconfig.xml";


    /** Servlet Dispatcher's descriptor. Created by Message Dispatcher who
     *  sent it in a "Register-self" command. */
    private ComponentDescriptor myDesc = null;

    /** List of the server services (descriptors) */
    ArrayList<ComponentDescriptor> remoteServices = new ArrayList();

    /** Set of activated remote job services. key = jobID, value = job service */
    HashMap<String, IServerJobService> activeRemoteJobServices = new HashMap<String, IServerJobService>();

    /** Pool of remote inactive IO services */
    ArrayList<IOService> remoteIOServicePool = new ArrayList<IOService>();

    /** Pool of server-side inactive geoIO services */
    ArrayList<GeoIOService> remoteGeoIOServicePool = new ArrayList<GeoIOService>();

    /** Manifests of core components. Keyed on the name of the component's jar file */
    private HashMap<String, Manifest> coreManifests = new HashMap<String, Manifest>();

    /** Names of core component jar files.; Keyed on the core component's display name */
    private HashMap<String, String> coreJars = new HashMap<String, String>();
    //private LandmarkProjectHandler landmarkProjectHandler;

    /** Platform OS */
    String platformOS = "";

    public String getPlatformOS() {
        return platformOS;
    }

    /** File separator of the OS under which the Tomcat server executes */
    private static final String filesep = System.getProperty("file.separator");
    private static final String osname = System.getProperty("os.name");
    private static final String eol = System.getProperty("line.separator");
    
    /** Get the file separator of the OS under which the Tomcat server executes.
     * return Line separator
     */
    public static String getServerOSFileSeparator() {
        return filesep;
    }

    /** Get the line separator of the OS under which the Tomcat server executes. */
    public static String getServerOSLineSeparator() {
        return eol;
    }
    
    /** Manager to get an available socket channel used to communicate with the server */
    SocketManager socketManager =  SocketManager.getInstance();

    /** Configure logging:
     *  <ul>
     *    <li>Send logging information to .qiworkbench/logs/qiWorkbenchServer*.log in the user's home directory</li>
     *    <li>Log all messages of level INFO and higher unless a custom logging.properties file is present</li>
     *    <li>See the http://java.sun.com/j2se/1.4.2/docs/guide/util/logging/overview.html page for more information.</li>
     *    <li>Use a simple formatter, not the default XML formatter</li>
     *  </ul>
     *  <br>
     *  Note: To turn logging off, set the level to OFF. To log everything, set the level to ALL.
     */
    private static void configLogging() {
        //Make sure the directory where the server-side logs are stored exists.
        String homeDir = System.getProperty("user.home");
        String logDir = homeDir + File.separator + QIWConstants.QIWB_WORKING_DIR + File.separator + "logs";
        File logsPath = new File(logDir);
        if (!logsPath.exists()) logsPath.mkdirs();

        try {
            logger.config("Appending FileHandler for .qiworkbench/logs/qiWorkbenchServer%u_%g.log to root logger...");
            // log file in .qiworkbench/logs/ in user's home directory
            Handler fh = new FileHandler("%h/"+QIWConstants.QIWB_WORKING_DIR+"/logs/qiWorkbenchServer%u_%g.log", 500000, 3, false);
            // use simple formatter instead of default XML formatter
            fh.setFormatter(new SimpleFormatter());
            // append FileHandler to root logger
            Logger.getLogger("").addHandler(fh);
        } catch (IOException ioe) {
            String errorMsg = "Unable to append FileHandler for .qiworkbench/logs/qiWorkbenchServer%u_%g.log to root logger due to: "+ioe.getMessage();
            logger.warning(errorMsg);
        }
        logger.info("Using the configLogging() method to control logging levels has been disabled." +
            System.getProperty("line.separator") +
            "If you wish to change the logging level(s) from the default 'INFO', please use a logging.properties file located in your home directory." +
            System.getProperty("line.separator") +
            "See the http://java.sun.com/j2se/1.4.2/docs/guide/util/logging/overview.html page for more information.");
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
        // Set up logging configuration
        configLogging();
        logger.info("IN SERVLET DISPATCHER (init)");
        
        // generate the list of remote services (descriptors) and retain
        //   job service
        ComponentDescriptor remoteServiceDesc = new ComponentDescriptor();
        remoteServiceDesc.setCID(ComponentUtils.genBaseCID(JobService.class));
        remoteServiceDesc.setMsgHandler(null);
        remoteServiceDesc.setComponentKind(QIWConstants.REMOTE_SERVICE_COMP);
        remoteServiceDesc.setDisplayName(QIWConstants.REMOTE_JOB_SERVICE_NAME);
        remoteServices.add(remoteServiceDesc);
        logger.fine("Remote job service descriptor formed:"+remoteServiceDesc);
        //   remote reader service
        remoteServiceDesc = new ComponentDescriptor();
        remoteServiceDesc.setCID(ComponentUtils.genBaseCID(IOService.class));
        remoteServiceDesc.setMsgHandler(null);
        remoteServiceDesc.setComponentKind(QIWConstants.REMOTE_SERVICE_COMP);
        remoteServiceDesc.setDisplayName(QIWConstants.REMOTE_IO_SERVICE_NAME);
        remoteServices.add(remoteServiceDesc);
        logger.fine("Remote IO service descriptor formed:"+remoteServiceDesc);
        //   remote geoReader service
        remoteServiceDesc = new ComponentDescriptor();
        remoteServiceDesc.setCID(ComponentUtils.genBaseCID(GeoIOService.class));
        remoteServiceDesc.setMsgHandler(null);
        remoteServiceDesc.setComponentKind(QIWConstants.REMOTE_SERVICE_COMP);
        remoteServiceDesc.setDisplayName(QIWConstants.REMOTE_GEOIO_SERVICE_NAME);
        remoteServices.add(remoteServiceDesc);
        logger.fine("Remote GeoIO service descriptor formed:"+remoteServiceDesc);

        // Determine the platform OS. If the user's home directory starts
        // with '/', it is UNIX based; otherwise, Windows
        String homeDir = System.getenv("HOME"); // Unix platforms
        this.platformOS = (homeDir != null && homeDir.startsWith("/")) ? QIWConstants.UNIX_OS : QIWConstants.WINDOWS_OS;

        //Capture the name of the jar files in the lib directory and
        //their Manifests for the core components.
        ServletContext sc = getServletContext();
        Enumeration attrNames = sc.getAttributeNames();
        String jarClasspath = "";
        while (attrNames.hasMoreElements()) {
            String attrName = (String)attrNames.nextElement();
            if (attrName.endsWith("jsp_classpath")) {
                jarClasspath = (String)sc.getAttribute(attrName);
                break;
            }
        }
        if (!jarClasspath.equals("")) getCoreManifests(jarClasspath);
        logger.config("Starting the Servlet Dispatcher thread");
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain");
        // extract serialized message from request (in XML)
        String xmlRequest = "";
        FilePartition filePart = null;

        if (request.getMethod().equals("POST") && request.getContentType().startsWith("multipart/form-data")) {
            MultiPartitionParser parser = new MultiPartitionParser(request);
            IPartition part1 = parser.scanNextPartition();
            logger.fine("part1:: "+part1.toString());
            if (part1.isMsgPartition()) {
                xmlRequest = ((MsgPartition)part1).getMsg();
                logger.fine("Serialized (XML) message partition="+xmlRequest);
            } else {
                //if no message partition, return an abnormal message
            }

            IPartition part2 = parser.scanNextPartition();
            logger.fine("part2:: "+part2.toString());
            if (part2.isFilePartition()) {
                filePart = (FilePartition)part2;
            } else {
                //if no file partition, return an abnormal message
            }
        } else {
            xmlRequest = request.getParameter("requestMsg");
            logger.fine("Serialized (XML) request="+xmlRequest);
        }

        // deserialize XML into a message
        // Note: request message carries the request object along with its type
        XStream xstream = new XStream();

// JE FIELD added remove all xstream security
     xstream.addPermission(AnyTypePermission.ANY);

        xstream.alias("qiworkbenchMsg", QiWorkbenchMsg.class);
        IQiWorkbenchMsg requestMsg = (QiWorkbenchMsg)xstream.fromXML(xmlRequest);
        logger.fine("Request message="+requestMsg);
        
        //Time to check for inactive client sockets?
        long currentTime = System.currentTimeMillis();
        if (currentTime-lastPollTime > POLLING_THRESHOLD) {
            lastPollTime = currentTime;
            SocketManager.getInstance().closeInactiveSockets();
        }

        // TODO save the message so can match with the response

        // Process the command and form the response message
        // Note: the request message is a new object we can use as the response
        IQiWorkbenchMsg responseMsg = requestMsg;

        String cmd = requestMsg.getCommand();
        //This can not be put in if (myDesc == null) because once JavaWS is
        //loaded once, myDesc is set and it will not run.
        if (cmd.equals(QIWConstants.GET_AVAIL_COMPONENTS)) {
            serverArray = (ArrayList<String>)requestMsg.getContent();
            //updateItems = UpdateManager.getInstance(serverURL).getAvailableComponents();
            //UpdateManager manager = new UpdateManager(serverURL);
            responseMsg.setMsgKind(QIWConstants.DATA_MSG);
            responseMsg.setContentType(QIWConstants.ARRAYLIST_TYPE);
            //ArrayList availComp = manager.getAvailableComponents();
            //responseMsg.setContent(availComp);
            responseMsg.setContent(UpdateManager.getInstance().getAvailableComponents(serverArray));
            //UpdateManager updateManager = new UpdateManager(serverArray);
            //responseMsg.setContent(updateManager.getAvailableComponents());
            MsgUtils.swapCIDs(responseMsg);
            responseMsg.setStatusCode(MsgStatus.SC_OK);
        } else

      if (cmd.equals(QIWConstants.GET_SERVER_CONFIG_CMD)) {
          // Leave Tomcat's URL in the content, but save it. Some
          // remote commands will need it.
          String thisLine = "";
          String fileString = "";
          String qiWbconfigFileName = System.getenv("QIWB_CONFIG");
          boolean returnAbnormalResponse = false;
          BufferedReader in = null;          
          String errorMessage = "Unknown error";
		  String filePath = File.separator + XML_DIR + File.separator + QIWB_CONFIG;

          if (qiWbconfigFileName == null) {
			   URL url = this.getClass().getResource(filePath);
			   InputStream is = null;
			   try {
				   is = url.openStream();
			   } catch (IOException ioe) {
				   logger.severe("Cannot open qiwb config file: "+url);
				   return;
			   }
		
			   BufferedReader br = null;
			   StringBuffer fileLines = new StringBuffer();
			   try {
				   //FileInputStream fis = new FileInputStream(descFile);
				   br = new BufferedReader(new InputStreamReader(is));
				   String line = br.readLine();
				   // read config file line by line
				   while (line != null) {
					   fileLines.append(line);
					   line = br.readLine();
				   }
				   fileString = fileLines.toString();
				   
				   //hack - append the server's native byte order as if an xml element of format
				   //<nativeServerByteOrder>ByteOrder.nativeOrder</nativeServerByteOrder>
				   String closingElement = "</configuration>";
				   int closingElementIndex = fileString.lastIndexOf(closingElement);
		
				   StringBuffer sbuf = new StringBuffer(fileString.substring(0,closingElementIndex));
				   sbuf.append("<nativeServerByteOrder>");
				   sbuf.append(ByteOrder.nativeOrder().toString());
				   sbuf.append("</nativeServerByteOrder>");
				   sbuf.append(closingElement);
		
				   fileString = sbuf.toString();
			   } catch (IOException ioe) {
                   returnAbnormalResponse = true;
				   logger.severe("IO exception reading qiwb config file:"+ioe.getMessage());
			   } finally {
				   try {
					   if (br != null) br.close();
				   } catch (IOException e) {}
			   }
		  }
		  else {
             try {
                   in = new BufferedReader(new FileReader(qiWbconfigFileName));
                   while ((thisLine = in.readLine()) != null) { // while loop begins here
                       fileString = fileString + thisLine;
                   }

                   //hack - append the server's native byte order as if an xml element of format
                   //<nativeServerByteOrder>ByteOrder.nativeOrder</nativeServerByteOrder>
                   String closingElement = "</configuration>";
                   int closingElementIndex = fileString.lastIndexOf(closingElement);

                   StringBuffer sbuf = new StringBuffer(fileString.substring(0,closingElementIndex));
                   sbuf.append("<nativeServerByteOrder>");
                   sbuf.append(ByteOrder.nativeOrder().toString());
                   sbuf.append("</nativeServerByteOrder>");
                   sbuf.append(closingElement);

                   fileString = sbuf.toString();

               } catch (IOException ioe) {
                   returnAbnormalResponse = true;
                   errorMessage = "IOException occurred while parsing QIWB_CONFIG file: " + ioe.getMessage();
               } finally {
                   if (in != null) in.close();
               }
          }

          if (returnAbnormalResponse) {
              logger.warning("An error occurred while reading QIWB_CONFIG file: " + filePath + " and " +  qiWbconfigFileName + ", returning abnormal response");
              responseMsg = MsgUtils.genAbnormalMsg(requestMsg, MsgStatus.SC_INTERNAL_ERROR, errorMessage);
          } else {
              logger.info("Lines were successfully read from QIWB_CONFIG file: " + filePath + " or " +  qiWbconfigFileName );
              responseMsg.setContentType(QIWConstants.STRING_TYPE);
              responseMsg.setContent(fileString);
              responseMsg.setMsgKind(QIWConstants.DATA_MSG);
              MsgUtils.swapCIDs(responseMsg);
              responseMsg.setStatusCode(MsgStatus.SC_OK);
          }
      } else

        //check if receiving usage stats to record
        if (cmd.equals(QIWConstants.RECORD_STATS_CMD)) {
            ArrayList args = (ArrayList)requestMsg.getContent();
            boolean eventsCaptured = StatsManager.getInstance().recordRawEvents((String)args.get(0), (ArrayList)args.get(1));
            if (eventsCaptured) {
                responseMsg.setContentType("");
                responseMsg.setContent("");
                responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                MsgUtils.swapCIDs(responseMsg);
                responseMsg.setStatusCode(MsgStatus.SC_OK);
            } else {
                responseMsg = MsgUtils.genAbnormalMsg(requestMsg, MsgStatus.SC_INTERNAL_ERROR, "Cannot write usage events to a weekly file");
            }
        } else

        //check if receiving workbench module names to record
        if (cmd.equals(QIWConstants.RECORD_MODULES_CMD)) {
            ArrayList args = (ArrayList)requestMsg.getContent();
            boolean modulesCaptured = StatsManager.getInstance().recordModules((String)args.get(0), (ArrayList)args.get(1));
            if (modulesCaptured) {
                responseMsg.setContentType("");
                responseMsg.setContent("");
                responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                MsgUtils.swapCIDs(responseMsg);
                responseMsg.setStatusCode(MsgStatus.SC_OK);
            } else {
                responseMsg = MsgUtils.genAbnormalMsg(requestMsg, MsgStatus.SC_INTERNAL_ERROR, "Cannot write module names to a file");
            }
        } else

        // check if message contains my descriptor
        if (cmd.equals(QIWConstants.REGISTER_SELF_CMD)) {
            // fix for mulitple clients attempting to send the dispatcher different Descriptors
            if (getMyDesc() == null)
                setMyDesc((ComponentDescriptor)requestMsg.getContent());

            responseMsg.setContentType(QIWConstants.COMP_DESC_TYPE);
            // now, inform the client of the ServletDispatcher's actual ComponentDescriptor, even if it was set by another client

            responseMsg.setContent(getMyDesc());

            responseMsg.setMsgKind(QIWConstants.DATA_MSG);
            MsgUtils.swapCIDs(responseMsg);
            responseMsg.setStatusCode(MsgStatus.SC_OK);
        } else

        // check if pinging server
        if (cmd.equals(QIWConstants.PING_SERVER_CMD)) {
            // Leave Tomcat's URL in the content, but save it. Some
            // remote commands will need it.
            //TODO: THIS IS NOT A GOOD IDEA FOR IT DEPENDS ON THE LAST PING
            serverURL = (String)requestMsg.getContent();
            responseMsg.setMsgKind(QIWConstants.DATA_MSG);
            MsgUtils.swapCIDs(responseMsg);
        } else if (requestMsg.getConsumerCID().equals(myDesc.getCID())) {
            // message to be executed by Servlet Dispatcher
            if (cmd.equals(QIWConstants.GET_REMOTE_SERVICES_CMD)) {
                // get list of remote services
                responseMsg.setContentType(QIWConstants.ARRAYLIST_TYPE);
                responseMsg.setContent(remoteServices);
                responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                MsgUtils.swapCIDs(responseMsg);
                responseMsg.setStatusCode(MsgStatus.SC_OK);
            } else

            if (cmd.equals(QIWConstants.IS_COMPATIBLE_CLIENT_VERSION)) {
                logger.info("Processing command IS_COMPATIBLE_CLIENT_VERSION");
                responseMsg = new ClientVersionChecker().getReponse(requestMsg);
                responseMsg.setProducerCID(myDesc.getCID());
                responseMsg.setConsumerCID(requestMsg.getProducerCID());
                logger.info("Done processing command IS_COMPATIBLE_CLIENT_VERSION");
            } else

            if (cmd.equals(QIWConstants.GET_REMOTE_FILE_LIST_CMD) ||
                cmd.equals(QIWConstants.GET_REMOTE_DIR_FILE_LIST_CMD) ||
                cmd.equals(QIWConstants.CHECK_REMOTE_FILE_EXIST_CMD) ||
                cmd.equals(QIWConstants.REMOTE_FILE_READ_CMD) ||
                cmd.equals(QIWConstants.GET_UNMADE_QIPROJ_DESC_DIRS_CMD) ||
                cmd.equals(QIWConstants.CREATE_DIRECTORIES_CMD) ||
                cmd.equals(QIWConstants.REMOTE_FILE_WRITE_CMD)) {
                // Route the IO request to the IO service for processing
                // and wait for the response.
                IQiWorkbenchMsg resp = routeToRemoteIOService(requestMsg);
                responseMsg.setContentType(resp.getContentType());
                responseMsg.setContent(resp.getContent());
                responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                MsgUtils.swapCIDs(responseMsg);
                responseMsg.setStatusCode(resp.getStatusCode());
            } else

            if (cmd.equals(QIWConstants.GET_REMOTE_OS_INFO_CMD)) {
                responseMsg.setContentType(QIWConstants.ARRAYLIST_TYPE);
                ArrayList<String> list = new ArrayList<String>();
                list.add(osname);
                list.add(filesep);
                list.add(eol);
                responseMsg.setContent(list);
                responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                MsgUtils.swapCIDs(responseMsg);
                responseMsg.setStatusCode(MsgStatus.SC_OK);
            } else

            if (cmd.equals(QIWConstants.IS_REMOTE_DIRECTORY_CMD)) {
                String dirPath = (String)requestMsg.getContent();
                File dir = new File(dirPath);
                String isDir = dir.exists() && dir.isDirectory() ? "yes" : "no";
                responseMsg.setContentType(QIWConstants.STRING_TYPE);
                responseMsg.setContent(isDir);
                responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                MsgUtils.swapCIDs(responseMsg);
                responseMsg.setStatusCode(MsgStatus.SC_OK);
            } else

            if (cmd.equals(QIWConstants.CREATE_REMOTE_PROJECT_CMD)) {
                //Note: Have already checked if not a directory, bunt not
                //if it is a file
                ArrayList args = (ArrayList)requestMsg.getContent();
                String qiProject = (String)args.get(0);
                File dir = new File(qiProject);

                String created = "yes";
                if (dir.exists()) {
                    created = "File exists with the same name";
                } else {
                    //create the project directory
                    dir.mkdir();
                    //create the project subdirectories
                    ArrayList<String> subdirs = (ArrayList<String>)args.get(1);
                    for (int i=0; i<subdirs.size(); i++) {
                        dir = new File(qiProject+filesep+subdirs.get(i));
                        dir.mkdir();
                    }
                }

                responseMsg.setContentType(QIWConstants.STRING_TYPE);
                responseMsg.setContent(created);
                responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                MsgUtils.swapCIDs(responseMsg);
                responseMsg.setStatusCode(MsgStatus.SC_OK);
            } else

            if (cmd.equals(QIWConstants.REMOTE_BINARY_FILE_READ_CMD)) {
                // Route the IO request to the IO service for processing
                // and wait for the response.
                IQiWorkbenchMsg resp = routeToRemoteIOService(requestMsg);
                responseMsg.setContentType(resp.getContentType());
                responseMsg.setContent(resp.getContent());
                responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                MsgUtils.swapCIDs(responseMsg);
                responseMsg.setStatusCode(resp.getStatusCode());

            } else

            if (cmd.equals(QIWConstants.REMOTE_BINARY_FILE_WRITE_CMD)) {
                ArrayList fileItems = (ArrayList)requestMsg.getContent();
                String fileFormat = (String)fileItems.get(1);
                if (fileFormat.equals(QIWConstants.JPEG_FORMAT)) {
                    fileItems.add(filePart);
                }
                // Route the IO request to the IO service for processing
                // and wait for the response.
                IQiWorkbenchMsg resp = routeToRemoteIOService(requestMsg);
                responseMsg.setContentType(resp.getContentType());
                responseMsg.setContent(resp.getContent());
                responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                MsgUtils.swapCIDs(responseMsg);
                responseMsg.setStatusCode(resp.getStatusCode());
            } else

            if (cmd.equals(QIWConstants.REMOTE_READ_SEGY_DATA_CMD) ||
                cmd.equals(QIWConstants.REMOTE_READ_BHPSU_DATA_CMD)) {
                ArrayList params = (ArrayList)requestMsg.getContent();
logger.info("ServletDispatcher:REMOTE_READ_BHPSU_DATA_CMD: params="+params);            
                //Assign a server port for the client
                logger.info("Acquire server port for REMOTE_READ_*_DATA_CMD");

                //The last parameter is the workbench session which uniquely
                //identifies the client.
                String workbenchSessionID = (String)params.get(params.size()-1);
                BoundIOSocket boundIOSocket = socketManager.acquireSocketChannel(workbenchSessionID);
                //Check if exceeded number of server sockets that can be in use
                //simultaneously.
                if (boundIOSocket == null) {
                    //Wait for a server socket to be released. One will be released, but could be starved of getting one
                    int nAttempts = 0;
                    while (nAttempts < 100) {
                        try {
                            nAttempts++;
                            Thread.sleep(100);
                        } catch (InterruptedException ie) {
                            logger.warning("While waiting for ioService.init() to finish, caught: " + ie.getMessage());
                        }
                        boundIOSocket = socketManager.acquireSocketChannel(workbenchSessionID);
                        if (boundIOSocket != null) break;
                    }
                }
                //check if able to acquire a socket channel
                if (boundIOSocket == null) {
                    logger.warning("Cannot acquire a server socket; request not processed: "+requestMsg);
                    responseMsg = MsgUtils.genAbnormalMsg(requestMsg, MsgStatus.SC_INTERNAL_ERROR, "Cannot acquire a server socket. Could not process request: "+request);
                } else {
                    int port = boundIOSocket.getServerPort();
                    logger.info("Acquired server port " + port);
                    //Add bound socket as the first parameter
                    params.add(0, boundIOSocket);
    
                    //Send response back to DispatcherConnector with the URL
                    //of the server and the assigned port. The DispatcherConnector
                    //will pass the socket info back to the MessageDispatcher who will route
                    //it to a local client IO service that will read the requested data sent by the
                    //remote server IO service.
                    ArrayList socketInfo = new ArrayList();
                    socketInfo.add(serverURL);
                    socketInfo.add(Integer.toString(port));
                    //Note: This is a DATA_CMD_MSG response so MessageDispatcher
                    //can distinguish between it and the normal response.
                    logger.info("Creating response to read request informing client to use port #" + port);
                    IQiWorkbenchMsg resp = new QiWorkbenchMsg(requestMsg.getConsumerCID(),
                                   requestMsg.getProducerCID(), QIWConstants.DATA_CMD_MSG, requestMsg.getCommand(),
                                   requestMsg.getMsgID(), QIWConstants.ARRAYLIST_TYPE, socketInfo);
                    // serialize response message into XML
                    String serializedMsg = xstream.toXML(resp);
                    logger.finest("Serialized (XML) response="+serializedMsg);
    
                    // put serialized message in HTTP response and send
                    response.setStatus(HttpServletResponse.SC_OK);
                    logger.info("Writing response for read on port #" + port);
                    PrintWriter out = null;
                    try {
                        out = response.getWriter();
                        out.print(serializedMsg);
                        //commit the response
                        out.flush();
                        logger.info("Sent qiwb message response informing the client to use port #" + port);
                    } catch (IOException ioe) {
                        //could try again
                        String errorMsg = "IO: Cannot send response"+ioe.getMessage();
                        logger.severe(errorMsg);
                    } finally {
                        if (out != null) {
                            out.close();
                        }
                    }
                    logger.info("Response for read on port #" + port + " written.");
                    // Route the IO request to the IO service for processing
                    // and wait for the response.
                    logger.info("Routing request for read on port #" + port + " to remoteIOsvc.");
                    
                    //NOTE: The server socket channel is release for reuse via 
                    //WriteDataToSocket when the geoIO methods are finished using
                    //it or an IO error occurs.
                    
                    resp = routeToRemoteIOService(requestMsg);
                    responseMsg.setContentType(resp.getContentType());
                    responseMsg.setContent(resp.getContent());
                    responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                    MsgUtils.swapCIDs(responseMsg);
                    responseMsg.setStatusCode(resp.getStatusCode());
                    //??Can a 2nd response be sent to the DispatcherConnector??
                }
            } else

            if (cmd.equals(QIWConstants.READ_GEOFILE_CMD)) {
                ArrayList params = (ArrayList)requestMsg.getContent();
                //The last parameter is the workbench session which uniquely
                //identifies the client.
                String workbenchSessionID = (String)params.get(params.size()-1);
                
                //Acquire a server port for the client
                logger.info("Acquire server port for READ_GEOFILE_CMD");
                BoundIOSocket boundIOSocket = socketManager.acquireSocketChannel(workbenchSessionID);
                
                //Check if socket allocation limit has been reached
                if (boundIOSocket != null) {
                    //Replace the location preference with the channel to be used for socket IO
                    params.set(0, boundIOSocket);
         
                    int port = boundIOSocket.getServerPort();
                    logger.info("Acquired server port " + port);
    
                    //Send response back to DispatcherConnector with the URL
                    //of the server and the assigned port. The DispatcherConnector
                    //will pass the socket info back to the MessageDispatcher who will route
                    //it to a local IO service that will read the requested data sent by the
                    //remote IO service.
                    ArrayList socketInfo = new ArrayList<String>();
                    socketInfo.add(serverURL);
                    socketInfo.add(Integer.toString(port));
                    //Note: This is a DATA_CMD_MSG response so requester
                    //can distinguish between it and the normal response.
                    IQiWorkbenchMsg resp = new QiWorkbenchMsg(requestMsg.getConsumerCID(),
                                   requestMsg.getProducerCID(), QIWConstants.DATA_CMD_MSG, requestMsg.getCommand(),
                                   requestMsg.getMsgID(), QIWConstants.ARRAYLIST_TYPE, socketInfo);
                    // serialize response message into XML
                    String serializedMsg = xstream.toXML(resp);
                    logger.finest("Serialized (XML) response="+serializedMsg);
    
                    // put serialized message in HTTP response and send
                    response.setStatus(HttpServletResponse.SC_OK);
                    try {
                        PrintWriter out = response.getWriter();
                        out.print(serializedMsg);
                        //commit the response
                        out.flush();
                        out.close();
                    } catch (IOException ioe) {
                        //could try again
                        String errorMsg = "IO: Cannot send response"+ioe.getMessage();
                        logger.severe(errorMsg);
                    }
    
                    // Route the geoIO request to the server's geoIO service for processing
                    // and wait for the response.
                    resp = routeToRemoteGeoIOService(requestMsg);
                    
                    //NOTE: The server socket channel is release for reuse via 
                    //WriteDataToSocket when the geoIO methods are finished using
                    //it or an IO error occurs.
                    
                    responseMsg.setContentType(resp.getContentType());
                    responseMsg.setContent(resp.getContent());
                    responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                    MsgUtils.swapCIDs(responseMsg);
                    responseMsg.setStatusCode(resp.getStatusCode());
                } else {
                    responseMsg = MsgUtils.genAbnormalMsg(requestMsg, MsgStatus.SC_SOCKET_LIMIT_EXCEEDED, "Could not obtain an IO socket channel for performing\n geoIO. Try again later when an existing geoIO completes.");
                }
            } else
            
            if (cmd.equals(QIWConstants.WRITE_GEOFILE_CMD)) {
                //NOTE: The server socket channel is released for reuse via 
                //WriteDataToSocket when the geoIO methods are finished using
                //it or an IO error occurs.
                
                // Route the geoIO request to the server's geoIO service for processing
                // and wait for the response.
                IQiWorkbenchMsg resp = routeToRemoteGeoIOService(requestMsg);
                responseMsg.setContentType(resp.getContentType());
                responseMsg.setContent(resp.getContent());
                responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                MsgUtils.swapCIDs(responseMsg);
                responseMsg.setStatusCode(resp.getStatusCode());
            } else

            if (cmd.equals(QIWConstants.SUBMIT_JOB_CMD) ||
                cmd.equals(QIWConstants.CREATE_JOB_CMD)) {
                // acquire an inactive remote job service
                IServerJobService jobService = acquireRemoteJobService();

                if (jobService == null) {
                    logger.severe("Internal error: no remote job service");
                } else {
                    // create a job ID for the job service
                    String jobID = ComponentUtils.genJobID();
                    jobService.setJobID(jobID);
                    // add to the list of active job services
                    activeRemoteJobServices.put(jobID, jobService);

                    // Route the job request to the job service for processing
                    // and wait for the response.
                    IQiWorkbenchMsg resp = routeToRemoteJobService(requestMsg, jobService);

                    responseMsg.setContentType(resp.getContentType());
                    responseMsg.setContent(resp.getContent());
                    responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                    MsgUtils.swapCIDs(responseMsg);
                    responseMsg.setStatusCode(resp.getStatusCode());
                }
            } else

            if (cmd.equals(QIWConstants.WAIT_FOR_JOB_EXIT_CMD) ||
                cmd.equals(QIWConstants.EXECUTE_JOB_CMD) ||
                cmd.equals(QIWConstants.GET_JOB_STATUS_CMD) ||
                cmd.equals(QIWConstants.GET_JOB_OUTPUT_CMD) ||
                cmd.equals(QIWConstants.GET_JOB_ERROR_OUTPUT_CMD) ||
                cmd.equals(QIWConstants.SET_JOB_COMMAND_CMD) ||
                cmd.equals(QIWConstants.SET_JOB_WORKING_DIR_CMD) ||
                cmd.equals(QIWConstants.SET_JOB_ENV_VAR_CMD) ||
                cmd.equals(QIWConstants.KILL_JOB_CMD)) {

                String jobID = (String)requestMsg.getContent();

                IServerJobService jobService = (IServerJobService)activeRemoteJobServices.get(jobID);

                // Route the job request to the job service for processing
                // and wait for the response.
                IQiWorkbenchMsg resp = routeToRemoteJobService(requestMsg, jobService);

                responseMsg.setContentType(resp.getContentType());
                responseMsg.setContent(resp.getContent());
                responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                MsgUtils.swapCIDs(responseMsg);
                responseMsg.setStatusCode(resp.getStatusCode());
            } else

            if (cmd.equals(QIWConstants.RELEASE_JOB_CMD)) {
                String jobID = (String)requestMsg.getContent();
                // remove from list of active job services
                IServerJobService jobService = activeRemoteJobServices.remove(jobID);

                // Route the job request to the job service for processing
                // and wait for the response.
                IQiWorkbenchMsg resp = routeToRemoteJobService(requestMsg, jobService);

                // add inactive job service to pool
                releaseRemoteJobService(jobService);

                responseMsg.setContentType(resp.getContentType());
                responseMsg.setContent(resp.getContent());
                responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                MsgUtils.swapCIDs(responseMsg);
                responseMsg.setStatusCode(resp.getStatusCode());
            } else

            if (cmd.equals(QIWConstants.GET_CORE_MANIFESTS_CMD)) {
                // get Manifests of core components
                responseMsg.setContentType(QIWConstants.ARRAYLIST_TYPE);
                ArrayList<HashMap> maps = new ArrayList<HashMap>();
                maps.add(coreJars);
                maps.add(coreManifests);
                responseMsg.setContent(maps);
                responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                MsgUtils.swapCIDs(responseMsg);
                responseMsg.setStatusCode(MsgStatus.SC_OK);
            } else
            
            if (cmd.equals(QIWConstants.RELEASE_SERVER_SOCKET_CMD)) {
                String port = (String)requestMsg.getContent();
                int serverPort = 0;
                try {
                    serverPort = Integer.parseInt(port);
                } catch (NumberFormatException nfe) {
                }
                socketManager.releaseSocket(serverPort);
                
                responseMsg.setContentType(QIWConstants.STRING_TYPE);
                responseMsg.setContent("");
                responseMsg.setMsgKind(QIWConstants.DATA_MSG);
                MsgUtils.swapCIDs(responseMsg);
                responseMsg.setStatusCode(MsgStatus.SC_OK);
            }
        } else {
            logger.warning("message not processed:"+requestMsg);
            logger.info("myCid="+myDesc.getCID());
            responseMsg = MsgUtils.genAbnormalMsg(requestMsg, MsgStatus.SC_INTERNAL_ERROR, "Could not process request: "+request);
        }
        //logger.fine("Response message="+responseMsg);

        // serialize response message into XML
        String serializedMsg = xstream.toXML(responseMsg);
        logger.finest("Serialized (XML) response="+serializedMsg);

        // put serialized message in HTTP response and send
        response.setStatus(HttpServletResponse.SC_OK);
        try {
            PrintWriter out = response.getWriter();
            out.print(serializedMsg);
            //commit the response
            out.flush();
            out.close();
        } catch (IOException ioe) {
            //could try again
            String errorMsg = "IO: Cannot send response"+ioe.getMessage();
            logger.severe(errorMsg);
        }
    }

    /**
     * Check if IO service active.
     *
     * @param service Service component.
     * @param try The number of times to try.
     * @return true if registered; otherwise, false
     */
    private boolean isIOServiceActive(IOService ioService, int tries) {
        while (tries > 0) {
            if (ioService.isInitFinished() && ioService.isInitSuccessful()) return true;
            try {
                Thread.sleep(100);
            } catch (InterruptedException ie) {
            }
            tries--;
        }

        return false;
    }

    //TODO: Use the Jakarta Commons Pool http://jakarta.apache.org/commons/pool/
    /**
     * Acquire a remote IO service from the pool. If there is none, create one.
     *
     * @return IO service; null if cannot create one.
     */
    private IOService acquireRemoteIOservice() {
        int size = remoteIOServicePool.size();
        IOService ioService = null;

        if (size > 0) {
            // get an inactive IO service from the pool
            ioService = remoteIOServicePool.remove(size-1);
            //reset queue
            //ioServiceDesc.getMsgHandler().resetQueue();
            logger.info("reaquired remote IO service:"+ioService);
        } else {
            // create a new IO service
            IOService ioServiceInstance = new IOService();
            // get a CID for the new component instance
            String cid = ComponentUtils.genCID(com.bhpb.qiworkbench.server.services.IOService.class);
            // use the CID as the name of the thread
            Thread serviceThread = new Thread(ioServiceInstance, cid);
            serviceThread.start();
            long threadId = serviceThread.getId();
            logger.info("IO Service Thread-"+Long.toString(threadId)+" started; CID="+cid);

            //let the new IO thread run
            int nAttempts=0;
            while ((ioServiceInstance.isInitFinished() == false) && (nAttempts < 20)) {
                logger.info("ioService.init() is not finished, yielding...");
                try {
                    nAttempts++;
                    Thread.sleep(50);
                } catch (InterruptedException ie) {
                    logger.warning("While waiting for ioService.init() to finish, caught: " + ie.getMessage());
                }
            }

            if (nAttempts == 20) {
                logger.info("Finished 20 attempts of 50ms each waiting for ioService.init() to finish");
            }

            if (ioServiceInstance.isInitSuccessful() == false) {
                logger.warning("ioServiceInstance.isInitSuccessful() == false! AcquireRemoteIOService() returning null.");
                //partial bug fix for remoteIO - isIOServiceActive() cannot be called if ioService has not yet entered init()
            } else if (isIOServiceActive(ioServiceInstance, 3)) {
                //check if service active, i.e., its thread ran
                ioService = ioServiceInstance;
            } else {
                logger.warning("ioService was inactive after checking 3 times, AcquireRemoteIOService() returning null.");
            }
        }
        return ioService;
    }

    /**
     * Release a remote IO service to the pool.
     *
     * @param ioService Remote IO service to release.
     */
    private void releaseRemoteIOservice(IOService ioService) {
        remoteIOServicePool.add(ioService);
    }

    /**
     * Acquire a remote IO service and route the IO request to it for processing.
     * Wait for the IO request to be processed and the response returned.
     *
     * @param msg IO request.
     * @return Response to IO request.
     */
    private IQiWorkbenchMsg routeToRemoteIOService(IQiWorkbenchMsg msg) {
        // acquire a remote IO service (get from pool)
        IOService ioService = acquireRemoteIOservice();

        if (ioService == null) {
            logger.severe("Internal error: no remote IO service");
            //send an abnormal response back
            IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no remote IO service");
            return resp;
        }

        ServerMessagingManager messagingMgr = ioService.getMessagingMgr();
        // Send request to the IO service and wait for it to receive it.
        //ioService.getMsgHandler().enqueue(msg);
        messagingMgr.putMsgWait(msg);

        // Wait for the IO service to return the response
        IQiWorkbenchMsg resp = messagingMgr.getNextMsgWait();//ioService.getMsgHandler().dequeue();

        // release the remote IO service (return to the pool)
        releaseRemoteIOservice(ioService);

        return resp;
    }

    /**
     * Check if geoIO service active.
     *
     * @param service Service component.
     * @param try The number of times to try.
     * @return true if registered; otherwise, false
     */
    private boolean isGeoIOServiceActive(GeoIOService geoIoService, int tries) {
        while (tries > 0) {
            if (geoIoService.isInitFinished() && geoIoService.isInitSuccessful()) return true;
            try {
                Thread.sleep(100);
            } catch (InterruptedException ie) {
            }
            tries--;
        }

        return false;
    }

    /**
     * Acquire a remote geoIO service from the pool. If there is none, create one.
     *
     * @return geoIO service; null if cannot create one.
     */
    private GeoIOService acquireRemoteGeoIOservice() {
        int size = remoteGeoIOServicePool.size();
        if (size > 0) {
            // get an inactive IO service from the pool
            GeoIOService geoIoService = remoteGeoIOServicePool.remove(size-1);
            return geoIoService;
        } else {
            // create a new IO service
            com.bhpb.qiworkbench.server.services.GeoIOService geoIoServiceInstance = new com.bhpb.qiworkbench.server.services.GeoIOService();
            // get a CID for the new component instance
            String cid = ComponentUtils.genCID(com.bhpb.qiworkbench.server.services.GeoIOService.class);
            // use the CID as the name of the thread
            Thread serviceThread = new Thread(geoIoServiceInstance, cid);
            serviceThread.start();
            long threadId = serviceThread.getId();
            //let the new geoIO thread run
            Thread.yield();
            //check if service active, i.e., its thread ran
            if (isGeoIOServiceActive(geoIoServiceInstance, 3)) {
                return geoIoServiceInstance;
            }
        }
        return null;
    }

    /**
     * Release a remote geoIO service to the pool.
     *
     * @param geoIoService Remote geoIO service to release.
     */
    private void releaseRemoteGeoIOservice(GeoIOService geoIoService) {
        remoteGeoIOServicePool.add(geoIoService);
    }

    /**
     * Acquire a remote geoIO service and route the geoIO request to it for processing.
     * Wait for the geoIO request to be processed and the response returned.
     *
     * @param msg geoIO request.
     * @return Response to geoIO request.
     */
    private IQiWorkbenchMsg routeToRemoteGeoIOService(IQiWorkbenchMsg msg) {
        // acquire a remote geoIO service (get from pool)
        GeoIOService geoIoService = acquireRemoteGeoIOservice();

        if (geoIoService == null) {
            logger.severe("Internal error: no remote geoIO service");
            //send an abnormal response back
            IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no remote geoIO service");
            return resp;
        }

        ServerMessagingManager messagingMgr = geoIoService.getMessagingMgr();
        // Send request to the geoIO service and wait for it to receive it.
        //geoIoService.getMsgHandler().enqueue(msg);
        messagingMgr.putMsgWait(msg);

        // Wait for the geoIO service to return the response
        IQiWorkbenchMsg resp = messagingMgr.getNextMsgWait();//geoIoService.getMsgHandler().dequeue();

        // release the remote geoIO service (return to the pool)
        releaseRemoteGeoIOservice(geoIoService);

        return resp;
    }

    /**
     * Check if job service active.
     *
     * @param service Service component.
     * @param try The number of times to try.
     * @return true if registered; otherwise, false
     */
    static public boolean isJobServiceActive(IServerJobService jobService, int tries) {
        while (tries > 0) {
            if (jobService == null) {
                logger.warning("Cannot invoke jobService.getComponentDesc(), jobService is NULL!");
            } else {
                if (jobService.getComponentDesc() != null) {
                    logger.fine("isJobServiceActive == true");
                    return true;
                } else {
                    logger.info("JobService was not active, watiting 1/10 second.");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ie) {
                        logger.warning("While trying to get jobService component descriptor, caught InterruptedExeption: " + ie.getMessage());
                    }
                    tries--;
                }
            }
        }

        return false;
    }

    //TODO: Use the Jakarta Commons Pool http://jakarta.apache.org/commons/pool/
    /** Pool of remote inactive IO services */
    static ArrayList<IServerJobService> remoteJobServicePool = new ArrayList<IServerJobService>();

    /**
     * Acquire a remote job service from the pool. If there is none, create one.
     *
     * @return job service; null if cannot create one.
     */
    static synchronized public IServerJobService acquireRemoteJobService() {
        int size = remoteJobServicePool.size();
        IServerJobService jobService = null;
        if (size > 0) {
            // get an inactive job service from the pool
            jobService = remoteJobServicePool.remove(size-1);
        } else {
            // create a new job service
            IServerJobService jobServiceInstance = new JobService();
            // get a CID for the new component instance
            String cid = ComponentUtils.genCID(com.bhpb.qiworkbench.server.services.JobService.class);
            // use the CID as the name of the thread
            Thread serviceThread = new Thread(jobServiceInstance, cid);
            serviceThread.start();
            long threadId = serviceThread.getId();

            int nAttempts=0;
            while ((jobServiceInstance.isInitFinished() == false) && (nAttempts < 20)) {
                logger.info("jobService.init() is not finished, yielding...");
                try {
                    nAttempts++;
                    Thread.sleep(50);
                } catch (InterruptedException ie) {
                    logger.warning("While waiting for jobService.init() to finish, caught: " + ie.getMessage());
                }
            }

            if (nAttempts == 20) {
                logger.info("Finished 20 attempts of 50ms each waiting for jobService.init() to finish");
            }

            if (jobServiceInstance.isInitSuccessful() == false) {
                logger.warning("jobServiceInstance.isInitSuccessful() == false! AcquireRemoteJobService() returning null.");
                //partial bug fix for remoteIO - isJobServiceActive() cannot be called if jobService has not yet entered init()
            } else if (isJobServiceActive(jobServiceInstance, 3)) {
                //check if service active, i.e., its thread ran
                jobService = jobServiceInstance;
            } else {
                logger.warning("jobService was inactive after checking 3 times, AcquireRemoteJobService() returning null.");
            }
        }
        return jobService;
    }

    /**
     * Release a remote job service to the pool.
     *
     * @param jobService Remote job service to release.
     */
    static synchronized public void releaseRemoteJobService(IServerJobService jobService) {
        remoteJobServicePool.add(jobService);
    }

    /**
     * Route the job request to the specified job service for processing.
     * Wait for the job request to be processed and the response returned.
     *
     * @param msg Job request.
     * @param jobService Job service to use
     * @return Response to job request.
     */
    private IQiWorkbenchMsg routeToRemoteJobService(IQiWorkbenchMsg msg, IServerJobService jobService) {
        if (jobService == null) {
            logger.severe("Internal error: no remote job service");
            //send an abnormal response back
            IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no remote job service");
            return resp;
        }

        IServerMessagingManager messagingMgr = jobService.getMessagingMgr();
        // Send request to the job service and wait for it to receive it.
        //jobService.getMsgHandler().enqueue(msg);
        messagingMgr.putMsgWait(msg);

        // Wait for the job service to return the response
        IQiWorkbenchMsg resp = messagingMgr.getNextMsgWait(); //jobService.getMsgHandler().dequeue();
        return resp;
    }

    /**
     * Get the Manifests of core components in the lib dir. Ignore jar
     * files that are not core components (based in the Manifest).
     *
     * @param jarClasspath jsp-classpath attribute of Tomcat server. Contains full path of all jar files in WEB-INF/lib separated by ';'
     * Implicit returns:
     * <ul>
     * <li>Saves the Manifests in a class HashMap variable, coreManifests.
     * <li>Saves the name of the jar file in a class HashMap variable, coreJars.
     * </ul>
     */
    private void getCoreManifests(String jarClasspath) {
/* TODO: reference jars directly instead of through classpath
        String coreCompPath = "/WEB-INF/lib/";
        File coreCompDir = new File(coreCompPath);
        File[] files = coreCompDir.listFiles();
        for (int i=0; i<files.length; i++) {
            // lib dir contains only jar files, but just in case...
            if (!files[i].isDirectory()) {
                String fileName = files[i].getName();
                if (fileName.endsWith(".jar")) {
*/
        String sep = getPlatformOS().equals(QIWConstants.UNIX_OS) ? ":" : ";";
        StringTokenizer st = new StringTokenizer(jarClasspath, sep);
        //logger.finest("jarClasspath: " + jarClasspath);
        while (st.hasMoreTokens()) {
            String jarPath = st.nextToken();
            //skip jar files not partition of WEB-INF/lib
            if (jarPath.indexOf("WEB-INF/lib") == -1) continue;
            int idx = jarPath.lastIndexOf('/');
            String fileName = jarPath.substring(idx+1);
                    try {
                        // Get jar file's Manifest
//                        URL componentURL = new URL("file:///"+coreCompPath+fileName);
                        URL componentURL = new URL("file:"+jarPath);
                        URL jarURL = new URL("jar", "", componentURL + "!/");
//                        logger.info("jarURL: "+jarURL);
                        JarURLConnection jarConnection = (JarURLConnection)jarURL.openConnection();
                        Manifest manifest = jarConnection.getManifest();
                        //Ignore jar files that have no manifest; core components do
                        if (manifest == null) continue;

                        //Get display name
                        Attributes attrs = manifest.getAttributes(QIWConstants.COMP_ATTR_SECTION_NAME);
                        //Ignore manifests that are not for core components
                        if (attrs == null || attrs.isEmpty()) continue;
                        String displayName = attrs.getValue(QIWConstants.DISPLAY_NAME_ATTR);
//                        logger.info("core comp's displayName: "+displayName);
                        coreJars.put(displayName, fileName);
                        coreManifests.put(fileName, manifest);
                    } catch (MalformedURLException mue) {
                        logger.severe("Unable to locate jar; CAUSE:"+mue.getMessage());
                    } catch (IOException ioe) {
                        logger.severe("Unable to access component jar: "+fileName+"; CAUSE:"+ioe.getMessage());
                    }
                }
//            }
//        }
    }

    /**
     * Dump a multipart POST to the log file.
     */
    /* Method is not used and should be removed
    private void dumpMultipart(HttpServletRequest request) {
        if (request.getMethod().equals("POST") && request.getContentType().startsWith("multipart/form-data")) {
            logger.finest("START DUMP multipart POST:");

            try {
                ServletInputStream ins = null;
                try {
                    //int idx = request.getContentType().indexOf("boundary=");
                    ins = request.getInputStream();
                    byte[] buff = new byte[8192];
                    int length = 0;
                    String line = null;
                    boolean moreData = true;

                    int bytesRead;
                    while ((bytesRead = ins.readLine(buff, 0, buff.length)) != -1) {
                        line = new String (buff, 0, bytesRead, Charset.defaultCharset().displayName());
                        logger.finest(line);
                    }
                } catch (IOException ioe) {
                    logger.finest("IOException dumping mulipart POST: "+ioe.getMessage());
                } finally {
                    logger.finest("END DUMP multipart POST");
                    if (ins != null) {
                        ins.close();
                    }
                }
            } catch (IOException ioe) {
                logger.finest("IOException dumping mulipart POST: "+ioe.getMessage());
            }
        } else {
            logger.finest("Request not multi-partition: "+request);
        }
    }
    */
    
    private synchronized void setMyDesc(ComponentDescriptor componentDescriptor) {
        myDesc = componentDescriptor;
    }

    private synchronized ComponentDescriptor getMyDesc() {
        return myDesc;
    }
}
