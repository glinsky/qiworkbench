/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.server;

//import statements

/**
 * Server wide constants.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public final class ServerConstants {
    /**
     * Prevent object construction outside of this class.
     */
    private ServerConstants() {}

    /** Top level package */
    public static final String TOP_PACKAGE = "com.bhpb.qiworkbench.server";

    /**
     * Default path of directory where usage stats are stored.
     * Used when invoking qiUsageStats.jsp and the workbench has not
     * been launched; otherwise, the path is the one specified in the
     * qiWbconfig.xml file.
     */
    public static final String DEFAULT_STATS_DIR = "/app/qi/usageStats";

    //USAGE ACTIONS
    public static final String LAUNCHED_ACTION = "launched";
    public static final String QUIT_ACTION = "quit";

    //USAGE TIMESCALES
    public static final String WEEKLY_TIMESCALE = "Weekly";
    public static final String MONTHLY_TIMESCALE = "Monthly";
    public static final String YEARLY_TIMESCALE = "Yearly";

    //STATS SERVER
    public static final String USAGE_STATS_WEBPAGE = "/qiWorkbench/jsp/qiUsageStats.jsp";
    public static final String STATS_URL_ATTR = "statsurl";
    public static final String STATS_PATH_ATTR = "statspath";
}
