/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiworkbench.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.File;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 * This is a servlet class that dynamically generates JNLP file
 * We assume all the jars located in the lib subdirectory, if not spacified in request
 * 
 * @author Charlie Jiang
 * @version 1.0
 */
public class CreateJnlpServlet extends HttpServlet {
	static final long   serialVersionUID = 1l;
	static final String jarDir     = "lib";  //default jar location
	
	private static Logger logger = Logger.getLogger(CreateJnlpServlet.class.getName());

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	public void destroy() {
		super.destroy();
	}

	//process form GET event
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
	                                         throws ServletException, IOException {
		processIt(request,response);
	}

    //process form POST event
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
	                                          throws ServletException, IOException {
		processIt(request,response);
	}

	/**
     * Actual implementation of request prrocessing
     *
     * @param request
     * @param response
     * @throws IOException
     */
	protected void processIt(HttpServletRequest request, HttpServletResponse response) 
	                                          throws IOException{
		
		logger.info("processIt: create dynamic jnlp file ....");
		String saveSet   = request.getParameter("saveSet");
		
		if (!cfgFileExists(saveSet)) {
			goErrorPage(request, response);
			
		}else {
			generateJnlp(request, response);
		}
		  
		logger.info("processIt: create dynamic jnlp file sucessfully");
	}
	
	/**
     * Generate a dynamic error page (when saveset does not exist)
     *
     * @param request
     * @param response
     * @throws IOException
     */
	private void goErrorPage(HttpServletRequest request, HttpServletResponse response) 
	                                                    throws IOException{
		String saveSet   = request.getParameter("saveSet");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<title>Invalid Parameters</title>");
		out.println("<body>");
		out.println("<table border=1>");
		out.println("<tr><td>");
		out.println("<h3><font color=red>Invalid Parameters</font></h3>");
		out.println("</td></tr>");
		out.println("<tr><td>");
		out.println(" SaveSet: " + saveSet + " does not exist !!");
		out.println("</td></tr>");
		out.println("</body>");
		out.println("</html>");
		out.close();
	}
	
	/**
     * Generate a dynamic JNLP
     *
     * @param request
     * @param response
     * @throws IOException
     */
	private void generateJnlp(HttpServletRequest request, HttpServletResponse response) 
	                                                    throws IOException {
		
		String serverUrl = request.getParameter("serverUrl");
		String dataPath  = request.getParameter("dataPath");
		String saveSet   = request.getParameter("saveSet");
		
		String jardir    = request.getParameter("jarDir");
		if (jardir == null) jardir = jarDir;
		String jarPath   = serverUrl + "/" + jardir;
		String qijar     = getWorkbenchJar(request);
		response.setContentType("application/x-java-jnlp-file");
		PrintWriter out = response.getWriter();
		
		out.println("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
		out.println("<jnlp spec=\"1.0+\" codebase=\"" + serverUrl + "/\" >");

		out.println("\t<information>");
		out.println("\t\t<title>QI Workbench</title>");
		out.println("\t\t<vendor>BHP</vendor>");
		out.println("\t\t<description>Seismic Tookit for Quantitive Intepretation</description>");
		out.println("\t\t<offline-allowed/>");
		out.println("\t\t<icon href=\"" + serverUrl + "/images/QIW_icon.gif\" />");
		out.println("\t\t<icon href=\"" + serverUrl + "/images/QIW_splash.gif\" kind=\"splash\" />");
		out.println("\t\t<shortcut><desktop/></shortcut>");
		out.println("\t</information>");

		out.println("\t<resources>");
		out.println("\t\t<j2se version=\"1.5\"/>");
		out.println("\t\t<jar href=\"" + jarPath + "/" + qijar + "\"/>");
		out.println("\t\t<jar href=\"" + jarPath + "/jhall.jar\"/>");
		out.println("\t\t<jar href=\"" + jarPath + "/gw2Dlib.jar\"/>");
		out.println("\t\t<jar href=\"" + jarPath + "/xom-1.1.jar\"/>");
		out.println("\t\t<jar href=\"" + jarPath + "/gwSeismicLib.jar\"/>");
		out.println("\t\t<jar href=\"" + jarPath + "/qiwb_2Dviewer.jar\"/>");
		out.println("\t\t<jar href=\"" + jarPath + "/xstream-1.2.jar\"/>");
		out.println("\t\t<jar href=\"" + jarPath + "/xpp3-1.1.3.4.O.jar\"/>");
		out.println("\t\t<jar href=\"" + jarPath + "/commons-logging.jar\"/>");
		out.println("\t\t<jar href=\"" + jarPath + "/commons-codec-1.3.jar\"/>");
		out.println("\t\t<jar href=\"" + jarPath + "/swing-layout-1.0.jar\"/>");
		out.println("\t\t<jar href=\"" + jarPath + "/commons-httpclient-3.0.jar\"/>");
		out.println("\t\t<jar href=\"" + jarPath + "/client_filechooser_job_asciiIO.jar\"/>");
		
		out.println("\t\t<property name=\"saveSet\"   value=\""+saveSet+"\" />");
		out.println("\t\t<property name=\"dataPath\"  value=\""+dataPath +"\" />");
		
		int index = serverUrl.lastIndexOf("/");
		String server = serverUrl.substring(0, index);
		out.println("\t\t<property name=\"serverUrl\" value=\""+server+"\" />");
		out.println("\t</resources>");

		out.println("\t<security>");
		out.println("\t\t<all-permissions/>");
		out.println("\t</security>");
		out.println("\t<application-desc main-class=\"com.bhpb.qiworkbench.messageFramework.MessageDispatcher\" />");

		out.println("</jnlp>");
		
		out.close();
		
	}
	
	/**
     * if wkVesrion is not specified in the request, we assume the default version
     *
     * @param request
     */
	private String getWorkbenchJar(HttpServletRequest request) {
		return "qiWorkbench.jar";
	}
	
	/**
     * Make sure the save set exists
     *
     * @param request
     */
	private boolean cfgFileExists(String fileName) {
		File fd = new File(fileName);
		if (fd.exists() && fd.isFile()) {
			return true;
		}
		return false;
	}
}