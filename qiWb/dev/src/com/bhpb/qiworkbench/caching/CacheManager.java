/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006-2008  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.caching;

import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Logger;

import org.apache.jcs.JCS;
import org.apache.jcs.access.exception.CacheException;
import org.apache.jcs.engine.control.CompositeCacheManager;

import com.bhpb.qiworkbench.util.PropertyUtils;

/**
 * Cache Manager for POJOs. Used by the qiViewer to cache geoData Objects,
 * e.g, seismic traces and horizons.Supports the following capabilities:
 * <ul>
 * <li>Lookup a POJO in the cache.</li>
 * <li>Insert a POJO in the cache.</li>
 * <li>Purge part or all of the cache of POJOs.</li>
 * </ul>
 * The cache is implemented using JCS (Java Caching System), a Jakarta project.
 * POJOs are kept in a memory cache which overflows to a disk cache. POJOs
 * are removed from the cache using the LRU (Least Recently Used) expiration
 * algorithm.
 * <p>
 * POJOs may be grouped, e.g., a range of traces. A group may be inserted or purged
 * as a unit.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class CacheManager {
    public static Logger logger = Logger.getLogger(CacheManager.class.getName());

    private static CacheManager singleton = null;

    Caches regionCaches;

    CompositeCacheManager ccm = null;

    private CacheManager() {
        //Configure JCS
        try {
            Properties props = PropertyUtils.loadPropertiesFromFile(CacheManagerConstants.JCS_CONFIG_FILE);
            ccm = CompositeCacheManager.getUnconfiguredInstance();
            ccm.configure(props);
        } catch (IOException ioe) {
            logger.severe("Cannot configure JCS: "+ioe.getMessage());
        }

        //Create region caches
        String regionName = "";
        regionCaches = Caches.getInstance();
        try {
            regionName = CacheManagerConstants.TRACE_REGION_NAME;
            JCS regionCache = JCS.getInstance(regionName);
            regionCaches.putCache(regionName, regionCache);

            regionName = CacheManagerConstants.HORIZON_REGION_NAME;
            regionCache = JCS.getInstance(regionName);
            regionCaches.putCache(regionName, regionCache);

            regionName = CacheManagerConstants.WELL_LOG_REGION_NAME;
            regionCache = JCS.getInstance(regionName);
            regionCaches.putCache(regionName, regionCache);
        } catch (CacheException ce) {
            logger.severe("Cannot create region caches: "+ce.getMessage());
        }
    }

    /**
     * Get the singleton instance of this class. If the Cache Manager doesn't
     * exist, create it.
     */
    public static CacheManager getInstance() {
        if (singleton == null) {
            singleton = new CacheManager();
        }
        return singleton;
    }

    /**
     * Look up a POJO in a region cache.
     * @param pojoID Unique ID of the POJO data object. Used as a cache key.
     * @param regionName The name of a cache region (think name of hash table)
     * @return The POJO if it is in the cache; otherwise, null.
     */
    public Object lookup(String pojoID, String regionName) {
        JCS regionCache = (JCS)regionCaches.getCache(regionName);
        return regionCache.get(pojoID);
    }

    /**
     * Look up a set of POJOs in a region cache.
     * @param pojoIDs[] Set of unique IDs of the POJO data objects. Used as cache keys.
     * @param regionName The name of a cache region (think name of hash table)
     * @return The set of POJOs in the cache; null for those not in the cache..
     */
    public Object[] lookup(String[] pojoIDs, String regionName) {
        JCS regionCache = (JCS)regionCaches.getCache(regionName);
        Object[] pojos = new Object[pojoIDs.length];
        for (int i=0; i<pojoIDs.length; i++) {
            pojos[i] = regionCache.get(pojoIDs[i]);
        }
        return pojos;
    }

    /**
     * Reserve a set of POJOs in a region cache by incrementing a POJO's reference
     * count if it is in the cache.
     * @param pojoIDs[] Set of unique IDs of the POJO data objects. Used as cache keys.
     * @param regionName The name of a cache region (think name of hash table)
     * @return Set of reserved results. true if POJO in cache; otherwise, false.
     */
    public boolean[] reserve(String[] pojoIDs, String regionName) {
        JCS regionCache = (JCS)regionCaches.getCache(regionName);
        HashMap<String, Integer> refCnts = regionCaches.getCacheRefCnts(regionName);
        boolean[] reserved = new boolean[pojoIDs.length];

        for (int i=0; i<pojoIDs.length; i++) {
            Object pojo = regionCache.get(pojoIDs[i]);
            if (pojo == null) {
                reserved[i] = false;
            } else {
               int refCnt = refCnts.get(pojoIDs[i]).intValue();
               refCnts.put(pojoIDs[i], new Integer(++refCnt));
               reserved[i] = true;
            }
        }
        return reserved;
    }

    /**
     * Insert a POJO into the cache. If the POJO is already in the cache
     * under the specified ID, it is overridden.
     * @param pojoID Unique ID of the POJO data object. Used as a cache key.
     * @param regionName The name of a cache region (think name of hash table)
     * @param pojo POJO to be inserted
     * @return true if POJO inserted; otherwise, false.
     */
    public boolean insert(String pojoID, String regionName, Object pojo) {
        try {
           JCS regionCache = (JCS)regionCaches.getCache(regionName);

           HashMap<String, Integer> refCnts = regionCaches.getCacheRefCnts(regionName);
           if (regionCache.get(pojoID) == null) {
               regionCache.put(pojoID, pojo);
               refCnts.put(pojoID, new Integer(1));
           } else {
               regionCache.put(pojoID, pojo);
               int refCnt = refCnts.get(pojoID).intValue();
               refCnts.put(pojoID, new Integer(++refCnt));
           }

           return true;
       } catch (CacheException ce) {
           logger.severe("Couldn't insert POJO into "+regionName+" : "+ce.getMessage());
           return false;
       }
    }

    /**
     * Insert a set of POJOs into the cache. If the POJO is already in the cache
     * under the specified ID, it is overridden.
     * @param pojoIDs Set of unique ID of the POJO data objects. Used as a cache keys.
     * @param regionName The name of a cache region (think name of hash table)
     * @param pojos[] POJOs to be inserted
     * @return Set of insertion results. true if POJO inserted; otherwise, false.
     */
    public boolean[] insert(String[] pojoIDs, String regionName, Object[] pojos) {
        boolean[] inserted = new boolean[pojoIDs.length];
        for (int i=0; i<pojoIDs.length; i++) inserted[i] = false;
        JCS regionCache = (JCS)regionCaches.getCache(regionName);
        HashMap<String, Integer> refCnts = regionCaches.getCacheRefCnts(regionName);

        for (int j=0; j<pojoIDs.length; j++) {
            try {
                if (regionCache.get(pojoIDs[j]) == null) {
                    regionCache.put(pojoIDs[j], pojos[j]);
                    inserted[j] = true;
                    refCnts.put(pojoIDs[j], new Integer(1));
                } else {
                    regionCache.put(pojoIDs[j], pojos[j]);
                    inserted[j] = true;
                    int refCnt = refCnts.get(pojoIDs[j]).intValue();
                    refCnts.put(pojoIDs[j], new Integer(++refCnt));
                }
            } catch (CacheException ce) {
                logger.severe("Couldn't insert POJO["+j+"] into "+regionName+" : "+ce.getMessage());
                inserted[j] = false;
                return inserted;
            }
        }

        return inserted;
    }

    /**
     * Clear the entire cache, i.e., all the region caches.
     * @return true if all region caches cleared; otherwise, false
     */
    public boolean purge() {
        String regionName = "";
        try {
            regionName = CacheManagerConstants.TRACE_REGION_NAME;
            JCS regionCache = (JCS)regionCaches.getCache(regionName);
            regionCache.clear();
            HashMap<String, Integer> refCnts = regionCaches.getCacheRefCnts(regionName);
            refCnts.clear();

            regionName = CacheManagerConstants.HORIZON_REGION_NAME;
            regionCache = (JCS)regionCaches.getCache(regionName);
            regionCache.clear();
            refCnts = regionCaches.getCacheRefCnts(regionName);
            refCnts.clear();

            regionName = CacheManagerConstants.WELL_LOG_REGION_NAME;
            regionCache = (JCS)regionCaches.getCache(regionName);
            regionCache.clear();
            refCnts = regionCaches.getCacheRefCnts(regionName);
            refCnts.clear();

            return true;
        } catch (CacheException ce) {
            logger.severe("Couldn't clear entire cache. Failed on region "+regionName+" : "+ce.getMessage());
            return false;
        }
    }

    /**
     * Clear a region cache.
     * @param regionName The name of a cache region (think name of hash table)
     * @return true if region cache cleared; otherwise, false.
     */
    public boolean purge(String regionName) {
        try {
            JCS regionCache = (JCS)regionCaches.getCache(regionName);
            regionCache.clear();
            HashMap<String, Integer> refCnts = regionCaches.getCacheRefCnts(regionName);
            refCnts.clear();

            return true;
        } catch (CacheException ce) {
            logger.severe("Couldn't clear region cache "+regionName+" : "+ce.getMessage());
            return false;
        }
    }

    /**
     * Remove a POJO from a region cache, if it exists and its reference count is
     * zero.
     * @param pojoID Unique ID of the POJO data object. Used as a cache key.
     * @param regionName The name of a cache region (think name of hash table)
     * @return true if removed; otherwise, false
     */
    public boolean remove(String pojoID, String regionName) {
        try {
            JCS regionCache = (JCS)regionCaches.getCache(regionName);
            if (regionCache.get(pojoID) == null) {
                return true;
            }

            HashMap<String, Integer> refCnts = regionCaches.getCacheRefCnts(regionName);
            int refCnt = refCnts.get(pojoID).intValue();
            refCnt--;
            if (refCnt == 0) {
                regionCache.remove(pojoID);
                refCnts.remove(pojoID);
                return true;
            }

            refCnts.put(pojoID, new Integer(refCnt));
            return true;
        } catch (CacheException ce) {
            logger.severe("Couldn't remove POJO in region "+regionName+" : "+ce.getMessage());
            return false;
        }
    }

    /**
     * Remove a set of POJOs from a region cache. Physically remove it if the POJO exists and
     * its reference count is zero.
     * @param pojoIDs Set of unique IDs of the POJO data objects. Used as a cache keys.
     * @param regionName The name of a cache region (think name of hash table)
     * @return Set of removed indicators: true if removed; otherwise, false
     */
    public boolean[] remove(String[] pojoIDs, String regionName) {
        boolean[] removed = new boolean[pojoIDs.length];
        for (int i=0; i<pojoIDs.length; i++) removed[i] = false;

        JCS regionCache = (JCS)regionCaches.getCache(regionName);
        HashMap<String, Integer> refCnts = regionCaches.getCacheRefCnts(regionName);
        for (int j=0; j<pojoIDs.length; j++) {
            try {
                if (regionCache.get(pojoIDs[j]) == null) {
                    removed[j] = true;
                } else {
                    int refCnt = refCnts.get(pojoIDs[j]).intValue();
                    refCnt--;
                    if (refCnt == 0) {
                        regionCache.remove(pojoIDs[j]);
                        refCnts.remove(pojoIDs[j]);
                        removed[j] = true;
                    } else {
                        refCnts.put(pojoIDs[j], new Integer(refCnt));
                        removed[j] = true;
                    }
                }
            } catch (CacheException ce) {
                logger.severe("Couldn't remove POJO["+j+"] in region "+regionName+" : "+ce.getMessage());
                return removed;
            }
        }

        return removed;
    }

    /**
     * Remove numToFree POJOs from a region memory cache using the configured
     * expiration algorithm.
     * @param numToFree The number of POJOs to remove.
     * @param regionName The name of a cache region (think name of hash table)
     * @return The actual number removed.
     */
    public int freeMemoryCache(int numToFree, String regionName) {
        try {
            JCS regionCache = (JCS)regionCaches.getCache(regionName);
            return regionCache.freeMemoryElements(numToFree);
            //TODO: Determine which POJOs were removed and remove their reference count.
        } catch (CacheException ce) {
            logger.severe("Couldn't free memory cache in region "+regionName+" : "+ce.getMessage());
            return 0;
        }
    }

    /**
     * Get the number of POJOs in a region cache.
     * @param regionName The name of a cache region (think name of hash table)
     * @return
     */
    public int getCacheSize(String regionName) {
        return ccm.getCache(regionName).getSize();
    }
}
