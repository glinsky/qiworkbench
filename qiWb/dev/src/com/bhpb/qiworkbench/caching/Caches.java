/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006-2008  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.caching;

import java.util.HashMap;

/**
 * Region caches.
 */
public class Caches {
    //* Region caches */
    HashMap caches = new HashMap();
    //** Reference counts for POJOs in a region cache. */
    HashMap<String, HashMap<String, Integer>> refCnts = new HashMap<String, HashMap<String, Integer>>();

    private static Caches singleton = null;

    /**
     * Get the singleton instance of this class. If it doesn't exist, create it.
     */
    public static Caches getInstance() {
        if (singleton == null) {
            singleton = new Caches();
        }
        return singleton;
    }

    /**
     * Get a region cache.
     * @param regionName Name of the region cache.
     * @return The region cache if it has been created, otherwise, null.
     */
    public Object getCache(String regionName) {
        return caches.get(regionName);
    }

    /**
     * Get the reference counts for a region cache.
     */
    public HashMap<String, Integer> getCacheRefCnts(String regionName) {
        return refCnts.get(regionName);
    }

    /**
     * Save an instance of a region cache. Create a map of reference counts
     * for the POJOs in the region cache.
     * @param regionName Name of the region cache.
     */
    public void putCache(String regionName, Object cache) {
        caches.put(regionName, cache);
        refCnts.put(regionName, new HashMap<String, Integer>());
    }
}
