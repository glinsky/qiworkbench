/*
 * IqiMessageHandler.java
 *
 * Created on November 14, 2007, 1:52 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.qiworkbench;

import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;

/**
 * Interface for the majority of qiWorkbench components, which send and receive messages
 * and have state.  These are distinct from IqiWorkbenchComponents (services) which have init()
 * methods but not CIDs or processMsg() methods.
 */
public interface IqiMessageHandler extends IqiWorkbenchComponent {
    String getCID();
    void processMsg(IQiWorkbenchMsg msg);
}
