/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.messaging;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.api.IServerMessagingManager;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.server.util.ComponentUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.IMsgHandler;
import com.bhpb.qiworkbench.messageFramework.SynchronousMsgHandler;

/**
 * Messaging manager for a server component. This is the API inteface for the
 * developer of a server-side service. Basically these are high level
 * interfaces to functionality provided by the Servlet Dispatcher. It avoids
 * exposing the synchronized message passing between the dispatcher and the
 * service.
 * <p>
 * The server Messaging Manager carries messaging state for a server-side
 * service. Therefore, each service has its own instance.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class ServerMessagingManager implements IServerMessagingManager {
    private static Logger logger = Logger.getLogger(ServerMessagingManager.class.getName());

    /** Message handler for the service */
    private SynchronousMsgHandler msgHandler;

    public IMsgHandler getMsgHandler() {
        return msgHandler;
    }

    /** Service's component descriptor */
    private IComponentDescriptor myCompDesc = null;

    public IComponentDescriptor getComponentDesc() {
        return myCompDesc;
    }

    /**
     * Constructor. Form message handler and component descriptor for service.
     *
     * @param cid CID of service.
     */
    public ServerMessagingManager(String cid) {
        // create service's message handler
        msgHandler = new SynchronousMsgHandler();

        // create service's component descriptor
        myCompDesc = new ComponentDescriptor();
        myCompDesc.setCID(cid);
        myCompDesc.setMsgHandler(msgHandler);
        myCompDesc.setComponentKind(QIWConstants.REMOTE_SERVICE_COMP);
        myCompDesc.setDisplayName(ComponentUtils.genUniqueName(QIWConstants.REMOTE_IO_SERVICE_NAME));
        logger.fine("component's descriptor formed:"+myCompDesc.toString());
    }

    /**
     * Get next message from message queue. If the message queue is empty,
     * wait for a message to arrive.
     *
     * @return Next message in queue.
     */
    public IQiWorkbenchMsg getNextMsgWait() {
        // if queue is empty; blocks until message added to queue
        IQiWorkbenchMsg msg = msgHandler.dequeue();
        return msg;
    }

    /**
     * Put message on queue; wait for it to be received.
     *
     * @param msg Message.to queue
     */
    public void putMsgWait(IQiWorkbenchMsg msg) {
        msgHandler.enqueue(msg);
    }
}
