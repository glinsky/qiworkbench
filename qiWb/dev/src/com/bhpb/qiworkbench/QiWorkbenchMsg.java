/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench;


import java.io.Serializable;
import java.util.ConcurrentModificationException;
import java.util.HashMap;

import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Container for a message passed between components.
 * <p>
 * One should never transform a request message into a response message, but
 * create each as separate messages.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class QiWorkbenchMsg implements IQiWorkbenchMsg {
	private static final long serialVersionUID = 1L;

	/** Unique ID for the producer of a message. Used by the Message Dispatcher to direct a response */
    private String producerCID = "";

    /** Unique ID for the consumer of a message. Used by the Message
    Dispatcher to direct a request */
    private String consumerCID = "";

    /** Distinguishes a message and how it should be processed */
    private String msgKind = "";

    /** Command to be executed by the consumer of the message */
    private String command = QIWConstants.NULL_CMD;

    /** Unique ID for a message. Set by the producer of a request message and carried in each response message so the producer can distinguish responses from multiple requests, especially requests to the same consumer such as a service.
     */
    private String msgID = "";

    /** Data type of the message content */
    private String contentType = "";

    /** If the message is a command, the object is its argument(s).
    If the message is data, the object is the data. If the message is
    an abnormal response, the reason processing of the request failed. */
    private Object content;

    /** Processing status code of a request message. Since a message is a
    serializable object, status cannot keep as the int primitive type. */

    /** The processing status of a request. */
    private Integer status;

    /**
     * Used in achieving synchronization between matching request and response.
     * If the skip flag is true, then the response message will not be
     * dequeued within the main loop of the component's run method. Instead
     * the response will be claimed by calling the components Messaging
     * Manager's getMatchingResponseWait method.
     */
    private boolean skip = false;

    /** A dummy message.*/
    public static QiWorkbenchMsg dummyMsg = new QiWorkbenchMsg();

    /** Default constructor */
    public QiWorkbenchMsg() {
        producerCID = "";
        consumerCID = "";
        msgKind = QIWConstants.CMD_MSG;
        command = QIWConstants.NULL_CMD;
        msgID = "0";
        contentType = QIWConstants.STRING_TYPE;
        content = "";
        status = Integer.valueOf(MsgStatus.SC_UNKNOWN);
    };

    /** Constructor to set all attributes of a message. Status code set to UNKNOWN.
     * @param producerCID Producer's component ID
     * @param consumerCID Consumer's component ID
     * @param msgKind Kind of message
     * @param command Command to be executed by consumer
     * @param msgID Unique message ID. Used to distinquish responses
     * @param contentType Content's type
     * @param content Content object
     */
    public QiWorkbenchMsg(String producerCID, String consumerCID, String msgKind, String command, String msgID, String contentType, Object content) {
        this.producerCID = producerCID;
        this.consumerCID = consumerCID;
        this.msgKind = msgKind;
        this.command = command;
        this.msgID = msgID;
        this.contentType = contentType;
        this.content = content;
        this.status = Integer.valueOf(MsgStatus.SC_UNKNOWN);
    }

    /** Constructor to set all attributes of a message. Status code set to UNKNOWN.
     * @param producerCID Producer's component ID
     * @param consumerCID Consumer's component ID
     * @param msgKind Kind of message
     * @param command Command to be executed by consumer
     * @param msgID Unique message ID. Used to distinquish responses
     * @param contentType Content's type
     * @param skip flag to indicate that this message will not be dequeued from the component's main run method for processing message
     * @param content Content object
     */
    public QiWorkbenchMsg(String producerCID, String consumerCID, String msgKind, String command, String msgID, String contentType, Object content, boolean skip) {
        this.producerCID = producerCID;
        this.consumerCID = consumerCID;
        this.msgKind = msgKind;
        this.command = command;
        this.msgID = msgID;
        this.contentType = contentType;
        this.content = content;
        this.skip = skip;
        this.status = Integer.valueOf(MsgStatus.SC_UNKNOWN);
    }

    //Setters
    public String getProducerCID() {
        return producerCID;
    }
    public String getConsumerCID() {
        return consumerCID;
    }
    public String getMsgKind() {
        return msgKind;
    }
    public String getCommand() {
        return command;
    }
    public String getMsgID() {
        return msgID;
    }
    public String getContentType() {
        return contentType;
    }
    /** Up to the receipent to cast object to its content type */
    public Object getContent() {
        return content;
    }
    
    
    public int getStatusCode() {
        return status.intValue();
    }

    //Getters
    public void setProducerCID(String pid) {
        producerCID = pid;
    }
    public void setConsumerCID(String cid) {
        consumerCID = cid;
    }
    public void setMsgKind(String msgKind) {
        this.msgKind = msgKind;
    }
    public void setCommand(String cmd) {
        command = cmd;
    }
    public void setMsgID(String mid) {
        msgID = mid;
    }
    public void setContentType(String ct) {
        contentType = ct;
    }
    public void setContent(Object content) {
        this.content = content;
    }
    public void setStatusCode(int sc) {
        this.status = Integer.valueOf(sc);
    }

    /**
     * Check if the message is instructed to skip from
     * being collected by the consumer message process loop which will
     * normally claim any message sitting in the message queue.
     *
     * This is to ensure synchronization between sending a request and
     * receiving a corresponding response and to avoid race condition
     * within the same thread.
     *
     * The following should run as a pair so that the response message will not stay at consumer queue unclaimed.
     *
     * String msgID = messagingMgr.sendRequest(..,true); //skip is set to be true
     * QiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID..);
     *
     *
     * @return true if a DATA message and skip set to be true; otherwise, false.
     */
    public boolean skip(){
        //return msgKind.equals(QIWConstants.DATA_MSG) && skip;
        return skip;
    }
    public void setSkip(boolean skip){
        this.skip = skip;
    }
    /**
     * Check if the message status code indicates an abnormal condition occurred
     * during the processing of a request message that prevented it from being
     * successfully completed. An UNKNOWN status code is not considered
     * abnormal.
     *
     * @return true if a processing error occured; otherwise, false.
     */
    public boolean isAbnormalStatus() {
        int sc = status.intValue();
        if (sc == MsgStatus.SC_UNKNOWN || sc == MsgStatus.SC_OK) return false;

        return true;
    }

    /** Display attributes of message */
    public synchronized String toString() {
        StringBuffer buf  = new StringBuffer();
        buf.append(this.getClass().toString());
        buf.append(": \n Producer CID=");
        buf.append(producerCID);
        buf.append(", Consumer CID=");
        buf.append(consumerCID);
        buf.append(", MSG Kind=");
        buf.append(msgKind);
        buf.append(", Command=");
        buf.append(command);
        buf.append(", MSG ID=");
        buf.append(msgID);
        buf.append(", \n Content Type=");
        buf.append(contentType);
        buf.append(", Content=");
        if (content == null) buf.append("NULL_CONTENT");
        else buf.append(contentToString(content));
        buf.append(", Status Code=");
        buf.append(MsgUtils.getStatusName(status.intValue()));
        buf.append(", Skip looping=");
        buf.append(skip);
        return buf.toString();
    }
	
	private String contentToString(Object content) {
		try {
			return content.toString();
		} catch (ConcurrentModificationException cme) {
			return "Concurrent Mod of msg content that contains a Collection.";
		}
	}
    
    public Object getValueFromContentHashMap(Object key) {
        if (!getContentType().equals(QIWConstants.HASH_MAP_TYPE))
            throw new UnsupportedOperationException("Cannot get value from content for key: " + key.toString() + ", content is not a HashMap - it is a " + getContentType());
        
        Object value = ((HashMap)content).get(key);
        
        return value;
    }
}
