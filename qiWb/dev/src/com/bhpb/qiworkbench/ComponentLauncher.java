/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URLClassLoader;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.logging.Logger;

import java.security.CodeSource;

import com.bhpb.qiworkbench.client.util.ComponentUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import com.sun.deploy.security.TrustDecider;

/**
 * Dynamically load the qiComponent from the component cache and launch the
 * qiComponent. The component cache is housed in the user's home directory.
 * in the subdirectory .qiworkbench/qicomponents/
 */
public class ComponentLauncher {

    private static Logger logger = Logger.getLogger(ComponentLauncher.class.getName());
    private static ComponentLauncher singleton = null;
    /** Attributes (headers) in the JAR file's manifest */
    private Attributes attrs;
    /** JAR class loader */
    private URLClassLoader jarClassLoader = null;

    private ComponentLauncher() {
    }

    /**
     * Get the singleton instance of this class. If the class doesn't exist,
     * create it.
     *
     * @return Singleton instance of this class.
     */
    public static ComponentLauncher getInstance() {
        if (singleton == null) {
            singleton = new ComponentLauncher();
        }

        return singleton;
    }

    /**
     * Get the user's home directory.
     *
     * @return String URL of the user's home directory
     */
    public String getHomeURL() {
        return "file://///" + MessageDispatcher.getInstance().getUserHOME();
    }

    /**
     * Get the URL of the qiComponent in the component cache.
     *
     * @param jarName Name of the qiComponent's jar file
     * @return URL of the qiComponent in the component cache.
     */
    public URL getComponentURL(String jarName) throws MalformedURLException {
        URL componentUrl = new URL(getHomeURL() + File.separator + QIWConstants.QIWB_WORKING_DIR + File.separator + QIWConstants.COMP_CACHE_DIR + File.separator + jarName);
        return componentUrl;
    }

    /**
     * Get the class name of the main entry point. (Main-Class header in the
     * JAR file's manifest.)
     * @param jarUrl URL of the qiComponent's JAR file.
     * @return Class name of the main entry point
     */
    public String getMainClassName(URL jarUrl) throws IOException {
        JarURLConnection juc = (JarURLConnection) jarUrl.openConnection();
        attrs = juc.getMainAttributes();
        return attrs != null ? attrs.getValue(Attributes.Name.MAIN_CLASS) : null;
    }

    /**
     * Launch the qiComponent by invoking it main entry point class.
     * main will start the component thread whose initialization will register
     * itself with the Message Dispatcher.
     *
     * @param jarClassLoader Class loader to load plugin jar; null if a core component
     * @param entryPoint Name of the entry point class
     * @param input arguments to the main entry point
     * @param return CID of launched qiComponent
     */
    private String invokeClass(URLClassLoader jarClassLoader, String entryPointClass, String[] args) throws NoClassDefFoundError, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
        //TODO handle signed jar files and with different signatures
        Class c = jarClassLoader == null ? Class.forName(entryPointClass) : jarClassLoader.loadClass(entryPointClass);
        Method m = c.getMethod("main", new Class[]{args.getClass()});
        m.setAccessible(true);
        int mods = m.getModifiers();
        if (m.getReturnType() != void.class || !Modifier.isStatic(mods) ||
                !Modifier.isPublic(mods)) {
            throw new NoSuchMethodException("main");
        }
        String cid = ComponentUtils.genCID(entryPointClass);
        args[0] = cid;

        try {
            m.invoke(null, new Object[]{args});
        } catch (IllegalAccessException iae) {
            //Impossible since access checks disabled
            logger.severe("Access checks disabled, but received: " + iae.getMessage());
        }

        return cid;
    }

    public boolean checkPermissionForComponent(String jarName, Manifest mf, boolean isCoreComp) {
        if (isCoreComp) {
            return true;
        }
// JE FIELD hotwired this to always return true on permission because
//   there is a problem importing com.sun.javaws.security.SigningInfo from javaws.jar file
  return true;    // NOTE: HOTWIRED
/*        boolean permGrant = false;
        URL componentURL = null;
        try {
            componentURL = getComponentURL(jarName);
            logger.info("componentURL: " + componentURL);
            JarFile jarFile = new JarFile(componentURL.getFile());

            //Determine which Java WebStart is running, i.e., JDK 1.5's or 1.6's because
            //the name of a method in the SigningInfo class changed between versions.
            Class klass = Class.forName("com.sun.javaws.security.SigningInfo");
            Method m = null;
            Class urlClass = Class.forName("java.net.URL");
            Class jarFileClass = Class.forName("java.util.jar.JarFile");
            try {
                m = klass.getMethod("getCodeSource", new Class[]{urlClass, jarFileClass});
            } catch (NoSuchMethodException nsm1) {
                try {
                    m = klass.getMethod("getCodeSourceFromJarFile", new Class[]{urlClass, jarFileClass});
                } catch (NoSuchMethodException nsm2) {
                    //should never happed for it is either one or the other of the two methods
                    System.out.println("checkPermissionForComponent: SigningInfo method name changed");
                    return true;
                }
            }
            Object[] args = new Object[]{componentURL, jarFile};
            try {
                CodeSource cs = (CodeSource) m.invoke(null, args);
                permGrant = TrustDecider.isAllPermissionGranted(cs);
            } catch (IllegalAccessException iae) {
                //Impossible since access checks disabled
                logger.severe("Access checks disabled, but received: " + iae.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return permGrant;
*/    // END JE FIELD HOTWIRE
    }

    /**
     * Load and invoke a qiComponent.
     *
     * @param jarName Name of the qiComponent JAR file.
     * @param mf Manifest of the qiComponent.
     * @param isCoreComp true if a core component; false if an external component
     * @return "cid:<cid>" if qiComponent successfully loaded; otherwise, the cause
     * of the failure.
     */
    public String launchComponent(String jarName, Manifest mf, boolean isCoreComp, ClassLoader parent) {
        // Get the component's main class
        Attributes mainAttrs = mf.getMainAttributes();
        String entryPointClass = (String) mainAttrs.getValue(Attributes.Name.MAIN_CLASS);
        if (entryPointClass == null) {
            return "Component has no Main-Class attribute in its Manifest";
        }

        String compCID = "";

        Attributes attrs = mf.getAttributes(QIWConstants.COMP_ATTR_SECTION_NAME);
        String dependents = attrs.getValue(QIWConstants.DEPENDENT_JARS_ATTR);

        int count = 1;
        String jars[] = null;
        if (dependents != null && dependents.trim().length() > 0) {
            jars = dependents.split(",");
            if (jars == null) {
                jars = dependents.split(" ");
            }
            for (int ii = 0; jars != null && ii < jars.length; ii++) {
                count++;
            }
        }

        URL componentURL = null;
        // A core component has already been loaded by WebStart. A plugin
        // is in the component cache and needs to be loaded

        try {
            if (!isCoreComp) {
                // Get the URL of the JAR-bundled component
                componentURL = getComponentURL(jarName);
                URL[] urls = new URL[count];
                urls[0] = componentURL;
                if (urls.length > 1) {
                    for (int i = 1; i < count; i++) {
                        urls[i] = getComponentURL(jars[i - 1]);
                    }
                }
                logger.info("ComponentLauncher: componentURL:" + componentURL);
                ClassLoader cl = null;
                if (parent == null) {
                    cl = ComponentLauncher.class.getClassLoader();
                } else {
                    cl = parent;
                }

                jarClassLoader = new AllPermissionsClassLoader(urls, cl);
            } else {
                jarClassLoader = null;
            }

            //TODO: Get the input parameters from the manifest
            //Note: Cannot use args[0]; it is reserved for the CID
            String[] args = new String[1];
            compCID = invokeClass(jarClassLoader, entryPointClass, args);

        //TODO: Load the jar files it depends on; in the Manifest
        } catch (MalformedURLException mue) {
            return "Unable to launch component: component cache directory doesn't exist; CAUSE:" + mue.getMessage();
        } catch (NoClassDefFoundError ncdfe) {
            return "Unable to launch component: entry-point class:" + entryPointClass + " not defined; CAUSE:" + ncdfe.getMessage();
        } catch (ClassNotFoundException cnfe) {
            return "Unable to launch component: entry-point class:" + entryPointClass + " not found; CAUSE:" + cnfe.getMessage();
        } catch (NoSuchMethodException nsme) {
            return "Unable to launch component: no such entry-point method:" + entryPointClass + "; CAUSE:" + nsme.getMessage();
        } catch (InvocationTargetException ite) {
            return "Unable to launch component: exception invoking entry-point method:" + entryPointClass + "; CAUSE:" + ite.getMessage();
        }

        return "cid:" + compCID;   // component jar successfully loaded and invoked
    }
}
