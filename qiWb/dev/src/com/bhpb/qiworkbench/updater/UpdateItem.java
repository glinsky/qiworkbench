/*
 * UpdateItem.java
 *
 * Holds all the information given by an Update Descriptor
 */
package com.bhpb.qiworkbench.updater;

import java.util.Iterator;
import java.util.Set;
import java.util.HashMap;
import java.util.Map;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.StringTokenizer;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author  Marcus Vaal
 */
public class UpdateItem {

	/**
	 * Empty Update Item Constuctor
	 */
	public UpdateItem() {
	}

	//If checking component, an if (UpdateConstants.TAG_COMPONENT.equals(tagName)) should be done
	/**
	 * Update Item Constructor
	 * @param n Node of the update item
	 */
	public UpdateItem (Node n) {
		this.tagName = ((Element) n).getTagName();
		this.node = n;
		this.tagNameAttrs = getTagNameAttrs();
		this.manifestAttrs = getManifestAttrs();
		this.licenseAttrs = getLicenseAttrs();
	}

	/**
	 * Update Item Constructor
	 * @param n Node of the update item
	 * @param lastModified date the file was last modified
	 * @param filePath File path of the Update Item
	 * @param fileName File name of the Update Item
	 * @param size Size of the .jar file
	 */
	public UpdateItem(Node n, long lastModified, String filePath, String fileName, long size) {
		this.tagName = ((Element) n).getTagName();
		this.node = n;
		this.tagNameAttrs = getTagNameAttrs();
		this.manifestAttrs = getManifestAttrs();
		this.licenseAttrs = getLicenseAttrs();
		this.filePath = filePath;
		this.fileName = fileName;
		this.lastModified = lastModified;
		this.setSize(size);
	}

	/**
	 * Update Item Constuctor
	 * @param n Node of the update item
	 * @param filePath File path of the Update Item
	 * @param fileName File Name of the Update Item
	 */
	public UpdateItem(Node n, String filePath, String fileName) {
		this.tagName = ((Element) n).getTagName();
		this.node = n;
		this.tagNameAttrs = getTagNameAttrs();
		this.manifestAttrs = getManifestAttrs();
		this.licenseAttrs = getLicenseAttrs();
		this.filePath = filePath;
		this.fileName = fileName;
		File compFile = new File(filePath + fileName);
		this.lastModified = compFile.lastModified();
		compDescSize = compFile.length();

        //TODO: GET COMPONENT SIZE AND DEP JAR SIZE
		compSize = -1;
		jarSizeTotal = -1;
	}

	/**
	 * Gets the name of the tag
	 * @return String name of the tag
	 */
	public String getTagName() {
		return tagName;
	}

	/**
	 * Gets the Node of the Update Item
	 * @return Node of the Update item
	 */
	public Node getNode() {
		return node;
	}

	/**
	 * Gets a Map of Attributes associated with the Tag Name
	 * @return Map of Attributes associated with the Tag Name
	 */
	public Map<String, String> getTagNameAttributes() {
		return tagNameAttrs;
	}

	/**
	 * Gets a Map of Attributes associated with the Manifest of a Tag
	 * @return Map of Attributes associated with the Manifest of a Tag
	 */
	public Map<String, String> getManifestAttributes() {
		return manifestAttrs;
	}

	/**
	 * Gets a Map of Attributes associated with the License of a Tag
	 * @return Map of Attributes associated with the License of a Tag
	 */
	public Map<String, String> getLicenseAttributes() {
		return licenseAttrs;
	}

	/**
	 * Gets a Map of all the Attributes associated with a Tag Name (Tag, Manifest, and License Attributes)
	 * @return Map of all the Attributes associated with a Tag Name
	 */
	public Map<String, String> getAllAttributes() {
		Map<String, String> items = tagNameAttrs;
		items.putAll(manifestAttrs);
		items.putAll(licenseAttrs); //returns just "name", maybe should remove?
		return items;
	}

	/**
	 * Sets the last modified date of a file
	 * @param date long representation of the last modified date
	 */
	public void setLastModified(long date) {
		this.lastModified = date;
	}

	/**
	 * Gets the last modified date of a file
	 * @return long representation of the last modified date
	 */
	public long getLastModified() {
		return lastModified;
	}

	/**
	 * Sets the File information of an Update Item
	 * @param filePath file path of the update descriptor
	 * @param fileName file name of the update descriptor
	 */
	public void setFileInfo(String filePath, String fileName) {
		this.filePath = filePath;
		this.fileName = fileName;
	}

	/**
	 * Gets the File Path of the update descriptor
	 * @return String of the file path
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * Gets the file name of the update descriptor
	 * @return String of the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the size of the component being updated
	 * @param updateDescSize long of the size of the components being updated
	 */
	public void setSize(long updateDescSize) {
		compDescSize = updateDescSize;
		totalSize = totalSize + compDescSize;
		if((getManifestAttributes().get("QiWorkbench-Component-Component-Type")).equals("external plugin") || 
				(getManifestAttributes().get("QiWorkbench-Component-Component-Type")).equals("external viewer agent")) {
			setCompSize();
			setDepJarSize();
		}
	}

	/**
	 * Sets the size of the dependent jar files for the component being updated 
	 */
	private void setDepJarSize() {
		//String getJarPath = getTagNameAttributes().get("distribution");
		//int idx = getJarPath.lastIndexOf(getManifestAttributes().get("QiWorkbench-Component"));
		//if(idx != -1) {
			//Strips the the file path from the given jar Path
			//String filePath = getJarPath.substring(0,idx);
			//Get the list of dependent jar files
			String compDep = getManifestAttributes().get("QiWorkbench-Component-Component-Dependencies");
			if(compDep != null) {
				String sep = ",";
				StringTokenizer st = new StringTokenizer(compDep, sep);
				while (st.hasMoreTokens()) {
					String jarFile = st.nextToken();
					String updateURL = filePath + jarFile;

					long flen = -1;
					try {
						//Open connection to the jar file and get the length via getContentLength()
						URL url = new URL(updateURL);
						URLConnection distrConnection = url.openConnection();
						distrConnection.connect();
						//final BufferedInputStream bsrc = new BufferedInputStream(distrConnection.getInputStream());
						flen = distrConnection.getContentLength();
						if(flen == -1) {
							flen = 0;
						}
					} catch (IOException e) {
						flen = 0;
					}
					jarSizeTotal = jarSizeTotal + flen;
				}
			}
		//}
		totalSize = totalSize + jarSizeTotal;
	}

	/**
	 * Gets the size of the dependent jar files
	 * @return int of size of the dependent jar files
	 */
	public int getDepJarSize() {
		return (int)jarSizeTotal;
	}

	/**
	 * Get the total size of the component, descriptor, and dependent jars
	 * @return int of the total size
	 */
	public int getTotalSize() {
		return (int)totalSize;
	}

	/**
	 * Get the size of the update descriptor
	 * @return int of update descriptor size
	 */
	public int getCompDescSize() {
		return (int)compDescSize;
	}

	/**
	 * Get the size of the component 
	 * @return ing of component size
	 */
	public int getCompSize() {
		return (int)compSize;
	}

	/**
	 * Sets the size of the component
	 */
	private void setCompSize() {
		long flen = -1;
		try {
			URL url = new URL(filePath + getManifestAttributes().get("QiWorkbench-Component"));
			//URL url = new URL(getTagNameAttributes().get("distribution"));
			URLConnection distrConnection = url.openConnection();
			distrConnection.connect();
			flen = distrConnection.getContentLength();
			if (flen == -1) {
				flen = Long.parseLong(getTagNameAttributes().get("downloadsize"));
			}
		} catch (IOException e) {
		}
		compSize = flen;
		totalSize = totalSize + compSize;
	}

	/**
	 * Base function that gets the attributes of the tag name
	 * @return Map of the attributes of the tag name
	 */
	private Map<String, String> getTagNameAttrs() {
		return getAttributes(node);
	}

	/**
	 * Returns the manifest attributes of the component being downloaded
	 * @return map of the attributes of the component being downloaded
	 */
	private Map<String, String> getManifestAttrs() {
		Map<String, String> items = new HashMap<String, String>();
		Node n = getTagNameNode(UpdateConstants.TAG_MANIFEST);
		if(n != null) {
			items = getAttributes(n);
		} else {
			items = null;
		}
		return items;
	}

	/**
	 * Returns the license attributes of the component being downloaded 
	 * @return map the the license attributes
	 */
	private Map<String, String> getLicenseAttrs() {
		Map<String, String> items = new HashMap<String, String>();
		Node n = getTagNameNode(UpdateConstants.TAG_LICENSE);
		if(n != null) {
			items = getAttributes(n);
		} else {
			items = null;
		}
		return items;
	}

	/**
	 * Gets the node the the given tag name
	 * @param nodeName String of the Tag Name
	 * @return Node of the Tag Name
	 */
	private Node getTagNameNode(String nodeName) {
		NodeList nodes = ((Element)node).getElementsByTagName(nodeName);
		Node n;
		if (nodes != null && nodes.getLength () > 0) {
			n = nodes.item(0); //Should only be one
		} else {
			//nodes not found
			n = null;
		}
		return n;
	}

	/**
	 * Gets the attributes of a node
	 * @param n Node attributes are wanted by
	 * @return Map of the attributes
	 */
	private Map<String, String> getAttributes (Node n) {
		Map<String, String> items = new HashMap<String, String>();
		for(int i=0; i<n.getAttributes().getLength(); i++) {
			items.put(n.getAttributes().item(i).getNodeName(), n.getAttributes().item(i).getNodeValue());
		}
		return items;
    }

	/**
	 * Checks to see if two update items are equal
	 * @param obj Update Item that is being compaired to
	 * @return Returns true if they are equal, false otherwise
	 */
	public boolean equals(UpdateItem obj) {
		if((this.node).isEqualNode(obj.getNode())) {
			return true;
		}
		return false;
	}

	/**
	 * Converts the content of the Update Item to a string (overwrites the object.toString() method)
	 * @return String containing update item content
	 */
	public String toString() {
		String setString = "";
		if(node != null) {
			setString = "tagName:" + tagName + "; ";

			Set<String> keys = tagNameAttrs.keySet();
			Iterator<String> iter = keys.iterator();
			setString = setString + "tagName-Attributes:";
			while (iter.hasNext()) {
				String attrName = iter.next();
				String attrValue = tagNameAttrs.get(attrName);
				setString = setString + attrName + "=" + attrValue + " ";
			}

			keys = manifestAttrs.keySet();
			iter = keys.iterator();
			setString = setString + ";\nManifest-Attributes:";
			while (iter.hasNext()) {
				String attrName = iter.next();
				String attrValue = manifestAttrs.get(attrName);
				setString = setString + attrName + "=" + attrValue + " ";
			}

			keys = licenseAttrs.keySet();
			iter = keys.iterator();
			setString = setString + ";\nLicense-Attributes:";
			while (iter.hasNext()) {
				String attrName = iter.next();
				String attrValue = licenseAttrs.get(attrName);
				setString = setString + attrName + "=" + attrValue + " ";
			}
		}
		return setString;
	}
	
	//Variable Declarations
	private String tagName = "";
	private String filePath = "";
	private String fileName = "";
	private Node node;
	private long lastModified = -1;
	private long totalSize = 0;
	private long compDescSize = 0;
	private long compSize = 0;
	private long jarSizeTotal = 0;
	private Map<String, String> tagNameAttrs = new HashMap<String, String>();
	private Map<String, String> manifestAttrs = new HashMap<String, String>();
	private Map<String, String> licenseAttrs = new HashMap<String, String>();
}
