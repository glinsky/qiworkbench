package com.bhpb.qiworkbench.updater;

import java.text.SimpleDateFormat;
import java.io.File;

public class UpdateConstants {

	public static final String TIME_STAMP_ATTRIBUTE_NAME = "timestamp";
	public static final String TIME_STAMP_FORMAT = "yyyyMMdd-hhmm";
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat ("yyyyMMdd-kkmm");
	public static final SimpleDateFormat INTEGER_DATE_FORMAT = new SimpleDateFormat ("yyyyMMddkkmm");

	public static final String TAG_COMPONENT_UPDATES = "component_updates";
	public static final String TAG_COMPONENT = "component";
	public static final String TAG_MANIFEST = "manifest";
	public static final String TAG_LICENSE = "license";

	public static final String ATTR_COMP_DISTRIBUTION = "distribution";
	public static final String ATTR_COMP_DOWNLOADSIZE = "downloadsize";
	public static final String ATTR_COMP_NEEDSRESTART = "needsrestart";

	public static final String ATTR_MANIFEST_COMPONENT = "QiWorkbench-Componet";
	public static final String ATTR_MANIFEST_COMPONENT_NAME = "QiWorkbench-Componet-Name";

	public static String HOME_DIR = "home_dir";
	public static String QIWB_DIR = "qiwb_dir";
	public static String COMP_DIR = "comp_dir";

	public static final String UPDATE_PROP_LOC = UpdateUtilities.getCompCachePath() + "updatecenter.properties";
    public static final String TEMP_FOLDER = UpdateUtilities.getCompCachePath() + "temp" + File.separator;
	public static final String STATUS_FILE = UpdateUtilities.getCompCachePath() + "UpdateStatus.txt";
}
