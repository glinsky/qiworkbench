/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.updater;

import java.awt.CardLayout;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;
import com.bhpb.qiworkbench.client.UpdateManager;
import com.bhpb.qiworkbench.client.qiWbConfiguration;
import com.bhpb.qiworkbench.util.StreamConnector;
import java.net.HttpURLConnection;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.logging.Logger;

/**
 * UpdateWizard.java
 * 
 * Manual Update Dialog 
 *
 * Update Wizard that lets the user choose update centers and components they wish to update.
 * In order to retrieve the PUC and update centers, the deploy tomcat needs to put a qiWbConfig.xml file with the
 * information in it.  An environmental variable QIWB_CONFIG then needs to be set with the qiWbConfig.xml file class
 * path from the root directory.
 *
 * @author  Marcus Vaal
 * @author Woody Folsom woody.folsom@bhpbilliton.net
 */
public class UpdateWizard extends javax.swing.JDialog {
    private static Logger logger = Logger.getLogger(UpdateWizard.class.getName());
    
    public UpdateWizard(String myCID, String servletCID) {
        String proxyHost = "";
        String proxyPort = "";
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(UpdateConstants.UPDATE_PROP_LOC));
            String proxy = properties.getProperty("proxy", "true");
            proxyHost = properties.getProperty("proxyhost", proxyHost);
            proxyPort = properties.getProperty("proxyport", proxyPort);
            if(proxy != null && proxy.equals("true")) {
                Properties systemSettings = System.getProperties();
                systemSettings.put("http.proxyHost", proxyHost);
                systemSettings.put("http.proxyPort", proxyPort);
                System.setProperties(systemSettings);
            } else {
                Properties systemSettings = System.getProperties();
                systemSettings.remove("http.proxyHost");
                systemSettings.remove("http.proxyPort");
                System.setProperties(systemSettings);
            }
        } catch (IOException e) {
        }
       
        isFinished = false;
        this.myCID = myCID;
        this.servletCID = servletCID;
        sb.append("<font face=\"Arial\" size=3>");
        this.setModal(true);
        this.setTitle("Update Wizard");
        this.saveOriginalUCs();
        UpdateUtilities.addUpdateCenters();
        initComponents();
        serverListSelectionModel = availCompList.getSelectionModel();
        serverListSelectionModel.addListSelectionListener(new AvailListSelectionHandler());
        userListSelectionModel = userCompList.getSelectionModel();
        userListSelectionModel.addListSelectionListener(new UserListSelectionHandler());
        ((CardLayout)(mainPanel.getLayout())).show(mainPanel, String.valueOf(currentDisplayNum));
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(javax.swing.JDialog.DO_NOTHING_ON_CLOSE);
        this.setVisible(true);
    }
    
    private void saveOriginalUCs() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(UpdateConstants.UPDATE_PROP_LOC));
            if(properties.getProperty("manualupdatecenters") != null) {
                originalUpdateCenter = properties.getProperty("manualupdatecenters");
            } else {
                originalUpdateCenter = "";
            }
        } catch (IOException e) {
        }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        buttonPanel = new javax.swing.JPanel();
        backButton = new javax.swing.JButton();
        nextButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        proxyConfigButton = new javax.swing.JButton();
        mainPanel = new javax.swing.JPanel();
        selectComponentsPanel = new javax.swing.JPanel();
        availCompScrollPane = new javax.swing.JScrollPane();
        availCompListModel = new DefaultListModel();
        availCompList = new JList(availCompListModel);
        userCompScrollPane = new javax.swing.JScrollPane();
        userCompListModel = new DefaultListModel();
        userCompList = new JList(userCompListModel);
        addButton = new javax.swing.JButton();
        removeButton = new javax.swing.JButton();
        addAllButton = new javax.swing.JButton();
        removeAllButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        currentVersionTextField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        availableVersionTextField = new javax.swing.JTextField();
        downloadPanel = new javax.swing.JPanel();
        downloadLabel = new javax.swing.JLabel();
        ComponentProgressBar = new com.bhpb.qiworkbench.updater.LongProgressBar();
        TotalProgressBar = new com.bhpb.qiworkbench.updater.LongProgressBar();
        downloadScrollPane = new javax.swing.JScrollPane();
        downloadEditorPane = new javax.swing.JEditorPane();
        selectUpdateCenterPanel = new javax.swing.JPanel();
        updateCenterScrollPane = new javax.swing.JScrollPane();
        updateCenterTable = new javax.swing.JTable();
        selectCenterLabel = new javax.swing.JLabel();
        addUpdateCenterButton = new javax.swing.JButton();
        updateCenterTextField = new javax.swing.JTextField();
        selectAllButton = new javax.swing.JButton();
        deselectAllButton = new javax.swing.JButton();
        removeUpdateCentersButton = new javax.swing.JButton();
        componentFolderTextField = new javax.swing.JTextField();
        retrieveUpdatesPanel = new javax.swing.JPanel();
        progressLabel = new javax.swing.JLabel();
        checkingProgressBar = new javax.swing.JProgressBar();
        jSeparator1 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setResizable(false);
        setUndecorated(true);
        backButton.setText("< Back");
        backButton.setEnabled(false);
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        nextButton.setText("Next >");
        nextButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        proxyConfigButton.setText("Proxy Configuration...");
        proxyConfigButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                proxyConfigButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout buttonPanelLayout = new org.jdesktop.layout.GroupLayout(buttonPanel);
        buttonPanel.setLayout(buttonPanelLayout);
        buttonPanelLayout.setHorizontalGroup(
            buttonPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, buttonPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(proxyConfigButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 146, Short.MAX_VALUE)
                .add(backButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(nextButton)
                .add(25, 25, 25)
                .add(cancelButton)
                .addContainerGap())
        );

        buttonPanelLayout.linkSize(new java.awt.Component[] {backButton, cancelButton, nextButton}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        buttonPanelLayout.setVerticalGroup(
            buttonPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, buttonPanelLayout.createSequentialGroup()
                .addContainerGap(22, Short.MAX_VALUE)
                .add(buttonPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cancelButton)
                    .add(nextButton)
                    .add(backButton)
                    .add(proxyConfigButton))
                .addContainerGap())
        );

        mainPanel.setLayout(new java.awt.CardLayout());

        availCompScrollPane.setViewportView(availCompList);

        userCompScrollPane.setViewportView(userCompList);

        addButton.setText("Add >");
        addButton.setEnabled(false);
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        removeButton.setText("< Remove");
        removeButton.setEnabled(false);
        removeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeButtonActionPerformed(evt);
            }
        });

        addAllButton.setText("Add All >");
        addAllButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAllButtonActionPerformed(evt);
            }
        });

        removeAllButton.setText("< Remove All");
        removeAllButton.setEnabled(false);
        removeAllButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeAllButtonActionPerformed(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Components Available For Update");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Components Being Updated");

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Current Version");

        currentVersionTextField.setEditable(false);
        currentVersionTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Available Version");

        availableVersionTextField.setEditable(false);
        availableVersionTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        org.jdesktop.layout.GroupLayout selectComponentsPanelLayout = new org.jdesktop.layout.GroupLayout(selectComponentsPanel);
        selectComponentsPanel.setLayout(selectComponentsPanelLayout);
        selectComponentsPanelLayout.setHorizontalGroup(
            selectComponentsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(selectComponentsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(selectComponentsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(availCompScrollPane, 0, 0, Short.MAX_VALUE)
                    .add(jLabel2))
                .add(selectComponentsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(selectComponentsPanelLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(selectComponentsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(addAllButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(removeAllButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(removeButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(addButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jLabel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(currentVersionTextField)
                            .add(jLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(availableVersionTextField))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(userCompScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 217, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, selectComponentsPanelLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(jLabel3)
                        .add(34, 34, 34))))
        );

        selectComponentsPanelLayout.linkSize(new java.awt.Component[] {availCompScrollPane, userCompScrollPane}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        selectComponentsPanelLayout.linkSize(new java.awt.Component[] {addAllButton, addButton, currentVersionTextField, jLabel1, jLabel4, removeAllButton, removeButton}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        selectComponentsPanelLayout.setVerticalGroup(
            selectComponentsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(selectComponentsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(selectComponentsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel2)
                    .add(jLabel3))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(selectComponentsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, selectComponentsPanelLayout.createSequentialGroup()
                        .add(jLabel1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(currentVersionTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabel4)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(availableVersionTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 34, Short.MAX_VALUE)
                        .add(addButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(removeButton)
                        .add(14, 14, 14)
                        .add(addAllButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(removeAllButton)
                        .add(56, 56, 56))
                    .add(availCompScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE)
                    .add(userCompScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE)))
        );
        mainPanel.add(selectComponentsPanel, "3");

        downloadPanel.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                downloadPanelComponentShown(evt);
            }
        });

        downloadLabel.setText("Downloading...");

        ComponentProgressBar.setStringPainted(true);

        TotalProgressBar.setString("Total");
        TotalProgressBar.setStringPainted(true);

        downloadScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        sbar = downloadScrollPane.getVerticalScrollBar();
        downloadEditorPane.setContentType("text/html");
        downloadScrollPane.setViewportView(downloadEditorPane);

        org.jdesktop.layout.GroupLayout downloadPanelLayout = new org.jdesktop.layout.GroupLayout(downloadPanel);
        downloadPanel.setLayout(downloadPanelLayout);
        downloadPanelLayout.setHorizontalGroup(
            downloadPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, downloadPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(downloadPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, downloadScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 576, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, downloadLabel)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, TotalProgressBar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 576, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, ComponentProgressBar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 576, Short.MAX_VALUE))
                .addContainerGap())
        );
        downloadPanelLayout.setVerticalGroup(
            downloadPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(downloadPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(downloadLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(downloadScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(ComponentProgressBar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(TotalProgressBar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        mainPanel.add(downloadPanel, "4");

        updateCenterTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "", "Update Center", "Component Folder"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        //My Code
        this.createTable();
        updateCenterTable.getColumnModel().getColumn(0).setMaxWidth(25);
        updateCenterTable.getColumnModel().getColumn(0).setMinWidth(20);
        javax.swing.table.TableCellRenderer tcr = updateCenterTable.getDefaultRenderer(String.class);
        javax.swing.table.DefaultTableCellRenderer dtcr = (javax.swing.table.DefaultTableCellRenderer) tcr;
        dtcr.setHorizontalAlignment(javax.swing.table.DefaultTableCellRenderer.CENTER);
        updateCenterScrollPane.setViewportView(updateCenterTable);

        selectCenterLabel.setText("Select Update Center(s)");

        addUpdateCenterButton.setText("Add Update Center");
        addUpdateCenterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addUpdateCenterButtonActionPerformed(evt);
            }
        });

        updateCenterTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateCenterTextFieldActionPerformed(evt);
            }
        });

        selectAllButton.setText("Select All");
        selectAllButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectAllButtonActionPerformed(evt);
            }
        });

        deselectAllButton.setText("Deselect All");
        deselectAllButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deselectAllButtonActionPerformed(evt);
            }
        });

        removeUpdateCentersButton.setText("Remove Selected Update Center(s)");
        removeUpdateCentersButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeUpdateCentersButtonActionPerformed(evt);
            }
        });

        componentFolderTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                componentFolderTextFieldActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout selectUpdateCenterPanelLayout = new org.jdesktop.layout.GroupLayout(selectUpdateCenterPanel);
        selectUpdateCenterPanel.setLayout(selectUpdateCenterPanelLayout);
        selectUpdateCenterPanelLayout.setHorizontalGroup(
            selectUpdateCenterPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, selectUpdateCenterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(selectUpdateCenterPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, updateCenterScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 576, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, selectCenterLabel)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, selectUpdateCenterPanelLayout.createSequentialGroup()
                        .add(addUpdateCenterButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(updateCenterTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 208, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(componentFolderTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, selectUpdateCenterPanelLayout.createSequentialGroup()
                        .add(selectAllButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(deselectAllButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(removeUpdateCentersButton)))
                .addContainerGap())
        );

        selectUpdateCenterPanelLayout.linkSize(new java.awt.Component[] {deselectAllButton, selectAllButton}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        selectUpdateCenterPanelLayout.setVerticalGroup(
            selectUpdateCenterPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(selectUpdateCenterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(selectCenterLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(updateCenterScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 230, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(selectUpdateCenterPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(updateCenterTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, addUpdateCenterButton)
                    .add(componentFolderTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(selectUpdateCenterPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(selectAllButton)
                    .add(deselectAllButton)
                    .add(removeUpdateCentersButton))
                .addContainerGap())
        );

        selectUpdateCenterPanelLayout.linkSize(new java.awt.Component[] {componentFolderTextField, updateCenterTextField}, org.jdesktop.layout.GroupLayout.VERTICAL);

        mainPanel.add(selectUpdateCenterPanel, "1");

        retrieveUpdatesPanel.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                retrieveUpdatesPanelComponentShown(evt);
            }
        });

        progressLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        checkingProgressBar.setIndeterminate(true);

        org.jdesktop.layout.GroupLayout retrieveUpdatesPanelLayout = new org.jdesktop.layout.GroupLayout(retrieveUpdatesPanel);
        retrieveUpdatesPanel.setLayout(retrieveUpdatesPanelLayout);
        retrieveUpdatesPanelLayout.setHorizontalGroup(
            retrieveUpdatesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(retrieveUpdatesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(retrieveUpdatesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(retrieveUpdatesPanelLayout.createSequentialGroup()
                        .add(208, 208, 208)
                        .add(checkingProgressBar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(progressLabel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 576, Short.MAX_VALUE))
                .addContainerGap())
        );
        retrieveUpdatesPanelLayout.setVerticalGroup(
            retrieveUpdatesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(retrieveUpdatesPanelLayout.createSequentialGroup()
                .add(162, 162, 162)
                .add(progressLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(checkingProgressBar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(136, Short.MAX_VALUE))
        );
        mainPanel.add(retrieveUpdatesPanel, "2");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jSeparator1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
            .add(buttonPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(mainPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(mainPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 335, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(buttonPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(43, 43, 43))))
        );
        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void proxyConfigButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_proxyConfigButtonActionPerformed
        UpdateProxyConfig proxyDialog = new UpdateProxyConfig();
        proxyDialog.setLocation(this.getBounds().x +(this.getBounds().width/2) - (proxyDialog.getBounds().width/2),
            this.getBounds().y +(this.getBounds().height/2) - (proxyDialog.getBounds().height/2));
        proxyDialog.setVisible(true);
    }//GEN-LAST:event_proxyConfigButtonActionPerformed

    /**
     * Adds update center when 'ENTER' is pressed, resets the curser to the server field
     * @param evt
     */
    private void componentFolderTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_componentFolderTextFieldActionPerformed
        addUpdateCenter();
        updateCenterTextField.requestFocus();
    }//GEN-LAST:event_componentFolderTextFieldActionPerformed

    /**
     * Downloads and displays updating components
     * @param evt
     */
    private void downloadPanelComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_downloadPanelComponentShown
        
        new Thread() {
            public void run() {
                //Handle Downloads
                UpdateUtilities.createTempFolder();
                
                //createTempFolder();
                // This is a fix for PUCs which redirect jar file requests to another URL.
                
                try {
                    HttpURLConnection.setFollowRedirects(true);
                    //TODO refactor to make use of enhanced method in AutomaticUpdateDialog
                    newDownloadText("Enabled HttpURLConnection redirects (response code 3xx)<br/>"/*, "black", false*/);
                } catch (SecurityException se) {
                    newDownloadText("Security Manager does not allow following redirects - may be unable to download jars!<br/>"/*, "red", false*/);
                }
                
                downloadComponents();
                TotalProgressBar.setString("Finished Downloading");
                deleteComponents();
                moveComponents();
                deleteTempFolder();

                backButton.setEnabled(true);
                nextButton.setEnabled(true);
            }
        }.start();
    }//GEN-LAST:event_downloadPanelComponentShown

    /**
     * Retrieves updates for user components
     * @param evt
     */
    private void retrieveUpdatesPanelComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_retrieveUpdatesPanelComponentShown
        new Thread() {
            public void run() {
                resetSelectComponentsPanel();
                getAllDownloadableComponents();
                checkUpdates();
                if(updateItems.isEmpty()) {
                    addAllButton.setEnabled(false);
                } else {
                    addAllButton.setEnabled(true);
                }
                    
                nextButton.setText("Finish");
                backButton.setEnabled(true);
                nextButton.setEnabled(true);
                CardLayout cl = (CardLayout)(mainPanel.getLayout());
                String card = String.valueOf(currentDisplayNum+1);
                cl.show(mainPanel, card);
                currentDisplayNum++;
            }
        }.start();
    }//GEN-LAST:event_retrieveUpdatesPanelComponentShown

    /**
     * Resets the buttons and lists in the GUI of components selection
     */
    private void resetSelectComponentsPanel() {
        allAvailComps.clear();
        updateItems.clear();
        deleteItems.clear();
        availCompListModel.clear();
        userCompListModel.clear();
        userUpdateItems.clear();
    }
    
    /**
     * Removes all components from the user selected components list
     * @param evt
     */
    private void removeAllButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeAllButtonActionPerformed
        for(int i=0; i<userCompListModel.size(); i++) {
            availCompListModel.addElement(userCompListModel.getElementAt(i));
            updateItems.add(userUpdateItems.get(i));
        }
        for(int i=userCompListModel.size()-1; i>=0; i--) {
            userCompListModel.remove(i);
            userUpdateItems.remove(i);
        }
        addAllButton.setEnabled(true);
        removeAllButton.setEnabled(false);
        setNextButton();
    }//GEN-LAST:event_removeAllButtonActionPerformed

    /**
     * Adds all updateable components to the user selected components list
     * @param evt
     */
    private void addAllButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addAllButtonActionPerformed
        for(int i=0; i<availCompListModel.size(); i++) {
            userCompListModel.addElement(availCompListModel.getElementAt(i));
            userUpdateItems.add(updateItems.get(i));
        }
        for(int i=availCompListModel.size()-1; i>=0; i--) {
            availCompListModel.remove(i);
            updateItems.remove(i);
        }
        addAllButton.setEnabled(false);
        removeAllButton.setEnabled(true);
        setNextButton();
    }//GEN-LAST:event_addAllButtonActionPerformed

    /**
     * Removes the selected user update components from the user components list
     * @param evt
     */
    private void removeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeButtonActionPerformed
        int[] selectedComponents = userCompList.getSelectedIndices();
        for(int i=0; i<selectedComponents.length; i++) {
            availCompListModel.addElement(userCompListModel.getElementAt(selectedComponents[i]));
            updateItems.add(userUpdateItems.get(selectedComponents[i]));
        }
        for(int i=selectedComponents.length-1; i>=0; i--) {
            userCompListModel.remove(selectedComponents[i]);
            userUpdateItems.remove(selectedComponents[i]);
        }
        setNextButton();
    }//GEN-LAST:event_removeButtonActionPerformed

    /**
     * Adds the selected components from the update list to the user components list
     * @param evt
     */
    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        int[] selectedComponents = availCompList.getSelectedIndices();
        for(int i=0; i<selectedComponents.length; i++) {
            userCompListModel.addElement(availCompListModel.getElementAt(selectedComponents[i]));
            userUpdateItems.add(updateItems.get(selectedComponents[i]));
        }
        for(int i=selectedComponents.length-1; i>=0; i--) {
            availCompListModel.remove(selectedComponents[i]);
            updateItems.remove(selectedComponents[i]);
        }
        removeAllButton.setEnabled(true);
        setNextButton();
    }//GEN-LAST:event_addButtonActionPerformed

    /**
     * Action handler for the Back button
     * @param evt
     */
    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        //determine previous card to display
        CardLayout cl = (CardLayout)(mainPanel.getLayout());
        String card = String.valueOf(currentDisplayNum-1);
        if(currentDisplayNum == 4) { //Download Display
            //change next button text
            userCompListModel.clear();
            userUpdateItems.clear();
            allDownloadedCompNames.clear();
            //downloadTextArea.setText("");
            downloadEditorPane.setText("");
            setNextButton();
        }else if(currentDisplayNum == 3) { //Component selection
            //card = card + "_" +String.valueOf(currentDetailNum);
            nextButton.setText("Next >");
            //need to show first screen - disable back button
            card = String.valueOf(currentDisplayNum-2);
            backButton.setEnabled(false);
            proxyConfigButton.setEnabled(true);
            sb = new StringBuffer();
            sb.append("<font face=\"Arial\" size=3>");
            --currentDisplayNum;
        }else if(currentDisplayNum == 1) { //Update Center Selection
            //we are on first screen, nothing to do
        }
        //show card
        cl.show(mainPanel, card);
        //update display number for non-1 numbers
        currentDisplayNum = currentDisplayNum > 1?(--currentDisplayNum):1;
    }//GEN-LAST:event_backButtonActionPerformed

    /**
     * Action handler for the Next/Finish button
     * @param evt
     */
    private void nextButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextButtonActionPerformed
       
        boolean isDisposed = false;
        //determine next card to display
        CardLayout cl = (CardLayout)(mainPanel.getLayout());
        String card = String.valueOf(currentDisplayNum+1);
        if(currentDisplayNum == 1) {
            removeAllButton.setEnabled(false);
            nextButton.setEnabled(false);
            proxyConfigButton.setEnabled(false);
        } else if(currentDisplayNum == 3 && userUpdateItems.size() > 0) { //Component Selection w/ components selected
            //need to show last screen - update summary
            backButton.setEnabled(false);
            nextButton.setEnabled(false);
            cancelButton.setEnabled(false);
            nextButton.setText("Finish");
        } else if(currentDisplayNum == 4 || userUpdateItems.size() == 0) { //Download Display or no components selected
            isFinished = true;
            if(currentDisplayNum == 4) {
                UpdateUtilities.updateUpdateFiles(updateType, downloadEditorPane.getText());
            } else if(currentDisplayNum == 3) {
                UpdateUtilities.updateUpdateFiles(updateType, "<font face=\"Arial\" size=3><font color=green>No Components Updated</font>");
            }

            //dispose
            dispose();
            isDisposed = true;
        }
        //Doesnt show the next card after the dialog is disposed
        if(!isDisposed) {
            //show card
            cl.show(mainPanel, card);
            //update display number
            currentDisplayNum++;
        }
    }//GEN-LAST:event_nextButtonActionPerformed

    /**
     * Action handler for the Cancel button
     * @param evt
     */
    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(UpdateConstants.UPDATE_PROP_LOC));
            properties.setProperty("manualupdatecenters", originalUpdateCenter);
            properties.store(new FileOutputStream(UpdateConstants.UPDATE_PROP_LOC), null);
        } catch (IOException e) {
        }
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    /**
     * Action handler for removing an update center
     * @param evt
     */
    private void removeUpdateCentersButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeUpdateCentersButtonActionPerformed
        javax.swing.table.DefaultTableModel model = (javax.swing.table.DefaultTableModel)updateCenterTable.getModel();
        int i = 0;
        while(i<updateCenterTable.getRowCount()) {
            if((Boolean)updateCenterTable.getValueAt(i,0) == true) { //if row is marked true
                String updateCenter = (String)updateCenterTable.getValueAt(i,1);
                String compFolder = (String)updateCenterTable.getValueAt(i,2);
                Properties properties = new Properties();
                try {
                    properties.load(new FileInputStream(UpdateConstants.UPDATE_PROP_LOC));
                    String fullUpdateCenter = updateCenter + "|" + compFolder;
                    String allUCs = properties.getProperty("manualupdatecenters");
                    int idx = allUCs.indexOf(fullUpdateCenter);
                    if(idx != -1) {
                        allUCs = allUCs.substring(0,idx) + allUCs.substring(idx+fullUpdateCenter.length()+1, allUCs.length());
                        properties.setProperty("manualupdatecenters", allUCs);
                        properties.store(new FileOutputStream(UpdateConstants.UPDATE_PROP_LOC), null);
                    }
                } catch (IOException e) {
                }
                model.removeRow(i);
            } else {
                i++;
            }

        }
    }//GEN-LAST:event_removeUpdateCentersButtonActionPerformed

    /**
     * Action handler for deselect button, deselects all update center boxes
     * @param evt
     */
    private void deselectAllButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deselectAllButtonActionPerformed
        for(int i=0; i<updateCenterTable.getRowCount(); i++) {
            updateCenterTable.setValueAt(new Boolean("False"),i,0);
        }
    }//GEN-LAST:event_deselectAllButtonActionPerformed

    /**
     * Action handler for select all button, selects all update center boxes
     * @param evt
     */
    private void selectAllButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectAllButtonActionPerformed
        for(int i=0; i<updateCenterTable.getRowCount(); i++) {
            updateCenterTable.setValueAt(new Boolean("True"),i,0);
        }
    }//GEN-LAST:event_selectAllButtonActionPerformed

    /**
     * Adds an update center to the list of update centers if 'ENTER' is pressed
     * @param evt
     */
    private void updateCenterTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateCenterTextFieldActionPerformed
        addUpdateCenter();
    }//GEN-LAST:event_updateCenterTextFieldActionPerformed

    /**
     * Adds an update center to the list of update centers if add button is pressed
     * @param evt
     */
    private void addUpdateCenterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addUpdateCenterButtonActionPerformed
        addUpdateCenter();
    }//GEN-LAST:event_addUpdateCenterButtonActionPerformed
    
    /**
     * Adds the update center in the text fields to the update center list and saves it
     */
    private void addUpdateCenter() {
        String updateCenter = updateCenterTextField.getText();
        String componentFolder = componentFolderTextField.getText();
        javax.swing.table.DefaultTableModel model = (javax.swing.table.DefaultTableModel)updateCenterTable.getModel();
        if(!UpdateUtilities.checkUCExists("manualupdatecenters", updateCenter + "|" + componentFolder)) {
            Properties properties = new Properties();
            try {
                properties.load(new FileInputStream(UpdateConstants.UPDATE_PROP_LOC));
                String addNewUC = "";
                if(!updateCenter.equals("") && !componentFolder.equals("")) { //if fields are not empty
                    if(!isInTable(updateCenter, componentFolder)) {
                        model.addRow(new Object[] {new Boolean("True"), updateCenter, componentFolder });
                        addNewUC = properties.getProperty("manualupdatecenters") + updateCenter + "|" + componentFolder + ",";
                        properties.setProperty("manualupdatecenters", addNewUC);
                        properties.store(new FileOutputStream(UpdateConstants.UPDATE_PROP_LOC), null);
                    }
                }
            } catch (IOException e) {
                logger.warning("IOException: " + e.getMessage());
            }
        }
        updateCenterTextField.setText("");
        componentFolderTextField.setText("");
    }
    
    /**
     * Creates the Update Center Table
     */
    private void createTable() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(UpdateConstants.UPDATE_PROP_LOC));
            this.addToTable(properties.getProperty("PUC"));
            this.addToTable(properties.getProperty("updatecenters"));
            this.addToTable(properties.getProperty("manualupdatecenters"));

        } catch (IOException e) {
        }
    }
    
    /**
     * Adds update centers storied in a property to the selection table
     * @param UCproperty update center property name
     */
    private void addToTable(String UCproperty) {
        if(UCproperty == null) {
            logger.info("Property is null");
            return;
        }
        String sep1 = ",";
        StringTokenizer st1 = new StringTokenizer(UCproperty, sep1);
        while(st1.hasMoreTokens()) {
            String serverAndFolder = st1.nextToken();
            String sep2 = "|";
            StringTokenizer st2 = new StringTokenizer(serverAndFolder, sep2);
            if(st2.hasMoreTokens()) {
                String updateCenter = st2.nextToken();
                if(st2.hasMoreTokens()) {
                    String updateFolder = st2.nextToken();
                    if(!isInTable(updateCenter, updateFolder)) {
                        javax.swing.table.DefaultTableModel model = (javax.swing.table.DefaultTableModel)updateCenterTable.getModel();
                        model.addRow(new Object[] {new Boolean("True"), updateCenter, updateFolder });
                    }
                }
            }
        }
    }
    
    /**
     * Checks to see if an entered server is already in the table
     * @param updateCenter Update Centers server address
     * @param updateFolder UpdateCenters folder name
     * @return Returnstrue if passed in values are in the table, otherwise false
     */
    private boolean isInTable(String updateCenter, String updateFolder) {
        for(int i=0; i<updateCenterTable.getRowCount(); i++) {
            if(((String)updateCenterTable.getValueAt(i,1)).equals(updateCenter) && ((String)updateCenterTable.getValueAt(i,2)).equals(updateFolder)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Sets the next button to Next of Finish based on the number of user selected components
     */
    private void setNextButton() {
        if(userUpdateItems.size() == 0) {
            nextButton.setText("Finish");
        } else {
            nextButton.setText("Next >");
        }
    }
    
    /**
     * Get all available downloads from the update centers selected
     */
    private void getAllDownloadableComponents() {
        progressLabel.setText("Retrieving All Downloadable Components");
        //Check each row of the update center table
        for(int i=0; i<updateCenterTable.getRowCount(); i++) {
            //Check if the update center is selected
            if((Boolean)updateCenterTable.getValueAt(i,0)) {
                //Create an ArrayList that contains the server and component folder (info pulled from table)
                ArrayList<String> serverArray = new ArrayList<String>();
                serverArray.add((String)updateCenterTable.getValueAt(i,1));
                serverArray.add((String)updateCenterTable.getValueAt(i,2));
                //Get available components from the update center
                ArrayList<UpdateItem> checkArray = UpdateManager.getAllAvailComps(myCID, servletCID, serverArray);
                //Check if there are any repeats with the any of the previous update centers
                for(int j=allAvailComps.size()-1; j>=0; j--) {
                    for(int k=checkArray.size()-1; k>=0; k--) {
                        //Check if the two components have the same name
                        if((allAvailComps.get(j).getManifestAttributes().get("QiWorkbench-Component-Name")).equals(checkArray.get(k).getManifestAttributes().get("QiWorkbench-Component-Name"))) {
                            //Check if the update descriptors are equal
                            if(allAvailComps.get(j).equals(checkArray.get(k))) {
                                checkArray.remove(k);
                                continue;
                            }
                            //Assuming the version is seperated by '.'
                            String sep = ".";
                            StringTokenizer st1 = new StringTokenizer(allAvailComps.get(j).getManifestAttributes().get("QiWorkbench-Component-Specification-Version"), sep);
                            StringTokenizer st2 = new StringTokenizer(checkArray.get(k).getManifestAttributes().get("QiWorkbench-Component-Specification-Version"), sep);
                            //Checks each number in the version and add the most recent version to the available components
                            while (st1.hasMoreTokens() && st2.hasMoreTokens()) {
                                String availVersionNum = st1.nextToken();
                                String checkVersionNum = st2.nextToken();
                                if(Integer.parseInt(availVersionNum) < Integer.parseInt(checkVersionNum)) {
                                    allAvailComps.remove(j);
                                    break;
                                } else if(Integer.parseInt(availVersionNum) > Integer.parseInt(checkVersionNum)) {
                                    checkArray.remove(k);
                                    break;
                                } else {
                                    //do nothing, they are equal
                                }
                            }
                        } 
                    }
                }
                //Add the current update center components to the total components
                allAvailComps.addAll(checkArray);
            }
        }
    }
    
    /**
     * Check the available components on the update centers if user needs to update any of their own
     */
    private void checkUpdates() {
        progressLabel.setText("Checking Updates");
        ArrayList<ArrayList<UpdateItem>> updateChecker = UpdateManager.checkUpdates(allAvailComps);
        //Returns an ArrayList of ArrayList<UpdateItem>, cell 0-all components (new and updated), cell 1-updated components
        updateItems = updateChecker.get(0);
        deleteItems = updateChecker.get(1);
        versionItems = updateChecker.get(2);
        //Add the components that need update or download to the display list
        for(int i=0; i<updateItems.size(); i++) {
            availCompListModel.addElement(updateItems.get(i).getManifestAttributes().get("QiWorkbench-Component-Name"));
        }
    }
    
    /**
     * Download all components that need to be downloaded or updated
     */
    private void downloadComponents() {
                    
        //Sets the Total progress bar based on the components the user chose
        allCompsTotal = UpdateManager.getTotalSize(userUpdateItems);
        TotalProgressBar.setMaximum(allCompsTotal);
        totalDownloaded = 0;
        setProgressBar(0, 0);
        
        //Download all user selected components
        for(int i=0; i<userUpdateItems.size(); i++) {
            update(userUpdateItems.get(i));
        }

        doneDownloading = true;
    }
    
    /**
     * Downloads or Updates a component, update descriptor, and dependent JARs
     * @param updateItem component needing download or update
     */
    private void update(UpdateItem updateItem) {
        //Download the component
        boolean compFinishedDownload = downloadComponent(updateItem);
        //Make sure you download the JAR file before you download the descriptor or the dependent jars.
        //Or download the descriptor if it is a core component (core components wrapped in the workbench war)
        if(compFinishedDownload || isCoreComponent) {
            //Download the update descriptor
            downloadUpdateDescriptor(updateItem);
        }
        //Make sure you download the JAR file before you download the descriptor or the dependent jars.
        if(compFinishedDownload) {
            //Download the dependent JARs
            downloadDepJars(updateItem);
        }
    }
    
    /**
     * Downloads a component from its distibution site located in the manifest file
     * @param updateItem UpdateItem of Component to be downloaded
     * @return if download completes, returns true, otherwise returns false
     */
    private boolean downloadComponent(UpdateItem updateItem) {
        //Check to see if the component type is external and not core
        if((updateItem.getManifestAttributes().get("QiWorkbench-Component-Component-Type")).equals("external plugin") ||
                (updateItem.getManifestAttributes().get("QiWorkbench-Component-Component-Type")).equals("external viewer agent")) {
            //Component is not a Core component
            isCoreComponent = false;
            //Distribution URL of the component
            //String updateURL = updateItem.getTagNameAttributes().get("distribution");
            String updateURL = compBundles + updateItem.getManifestAttributes().get("QiWorkbench-Component");
            String destURL = UpdateConstants.TEMP_FOLDER + updateItem.getManifestAttributes().get("QiWorkbench-Component");
            //Used to help delete files after download
            allDownloadedCompNames.add(updateItem.getManifestAttributes().get("QiWorkbench-Component"));
            //Checks if the component is new or an update
            if(checkNewComponent(updateItem)) {
                newDownloadText("<font color=blue>Downloading New Component: " + updateItem.getManifestAttributes().get("QiWorkbench-Component") + "... </font>");
            } else {
                newDownloadText("<font color=purple>Updating: " + updateItem.getManifestAttributes().get("QiWorkbench-Component") + "... </font>");
            }
            
            //Download dependent JARs
            return copyToDirFromURL(updateURL, destURL, updateItem); 
        } else {//if((updateItem.getManifestAttributes().get("QiWorkbench-Component-Component-Type")).equals("core viewer agent")) { //Is core viewer agent the only other kind of component there can be?
            isCoreComponent = true;
        }
        return false;
    }
    
    /**
     * Downloads the update descriptor
     * @param updateItem component that has the Update Descriptor
     * @return if download completes, returns true, otherwise returns false
     */
    private boolean downloadUpdateDescriptor(UpdateItem updateItem) {
        if((updateItem.getManifestAttributes().get("QiWorkbench-Component-Component-Type")).equals("external plugin") ||
        		(updateItem.getManifestAttributes().get("QiWorkbench-Component-Component-Type")).equals("external viewer agent")) {
                //Check if the component has dependent components
        		String updateURL = compBundles + updateItem.getFileName();
                String destURL = UpdateConstants.TEMP_FOLDER + updateItem.getFileName();
                //Used to help delete files after download
                allDownloadedCompNames.add(updateItem.getFileName());
                
                if(isCoreComponent) {
                    newDownloadText("<font color=#0000A0>Downloading Update Descriptor: " + updateItem.getFileName() + "... </font>");
                } else {
                    //&nbsp; gives the tab look in the download display
                    newDownloadText("&nbsp;&nbsp;Downloading Update Descriptor: " + updateItem.getFileName() + "... ");
                }
                
                //downloadingComponent = updateItem.getFileName();
                return copyToDirFromURL(updateURL, destURL, updateItem);
            //}
    	}
    	return false;
    }
    
    /**
     * Downloads the dependent JAR files
     * @param updateItem component that has the dependent jars
     * @param if all downloads complete, return true, otherwise return false
     */
    public boolean downloadDepJars(UpdateItem updateItem) {
        boolean allDepJarsDownloaded = true;
        //Check to see if the component type is an external plugin and not a core
        if((updateItem.getManifestAttributes().get("QiWorkbench-Component-Component-Type")).equals("external plugin") ||
        		(updateItem.getManifestAttributes().get("QiWorkbench-Component-Component-Type")).equals("external viewer agent")) {
                //List of dependent JARs (found in the Manifest file) seperated by ','
                String compDep = updateItem.getManifestAttributes().get("QiWorkbench-Component-Component-Dependencies");
                //Check if the component has dependent components
                if(compDep != null) {
                    String sep = ",";
                    StringTokenizer st = new StringTokenizer(compDep, sep);
                    while (st.hasMoreTokens()) {
                        String jarFile = st.nextToken();
                        String updateURL = compBundles + jarFile;
                        String destURL = UpdateConstants.TEMP_FOLDER + jarFile;
                        //Used to help delete files after download
                        allDownloadedCompNames.add(jarFile);
                        //&nbsp; gives the tab look in the download display
                        newDownloadText("&nbsp;&nbsp;Downloading Dependent JAR: " + jarFile + "... ");
                        
                        //Download dependent JARs
                        if(!copyToDirFromURL(updateURL, destURL, updateItem)) {
                            allDepJarsDownloaded = false;
                        }
                    }
                }
                
                compDep = updateItem.getManifestAttributes().get("QiWorkbench-Component-JNI-Dependencies");
                //Check if the component has dependent components
                
                if(compDep != null) {
                    String sep = ",";
                    StringTokenizer st = new StringTokenizer(compDep, sep);
                    while (st.hasMoreTokens()) {
                        String jarFile = st.nextToken();
                        jarFile = jarFile.replace(" ", "");
                        String updateURL = compBundles + jarFile;
                        String destURL = UpdateConstants.TEMP_FOLDER + jarFile;
                        //Used to help delete files after download
                        allDownloadedCompNames.add(jarFile);
                        newDownloadText("&nbsp;&nbsp;Downloading Dependent JNI: " + jarFile + "... ");
                        
                        //Download dependent JNIs
                        if(!copyToDirFromURL(updateURL, destURL, updateItem)) {
                            allDepJarsDownloaded = false;
                        }
                    }
                }
            //}
        }
        return allDepJarsDownloaded;
    }
    
    /**
     * Copy a file from the UpdateCenter URL fileSource to the destination file fileDest.
     *
     * @param fileSource URL path of the source file
     * @param fileDest file path of the destiantion file
     * @param updateItem Component being downloaded
     * @param timeout numer of milleseconds to wait when establishing URLConnection
     *
     * @return true if the file was successfully downloaded and written to the filesystem, false otherwise
     */
    //TODO refactor as delegator target when filesource is a valid URL (throw exception if new URL(fileSource) is null or throws exception
    //TODO enable multi-threaded io by calling a Collection of StreamConnectors directly (delete method ioCopy)
    boolean copyToDirFromURL(String fileSource, String fileDest, UpdateItem updateItem) {
        try {
            URL url = new URL(fileSource);
            URLConnection distrConnection = url.openConnection();
            
            newDownloadText("Setting URL connection timeout to 10 seconds...<br />");
            distrConnection.setConnectTimeout(10000);
           
            final BufferedInputStream bsrc = new BufferedInputStream(distrConnection.getInputStream());
            BufferedOutputStream bdest = new BufferedOutputStream(new FileOutputStream(fileDest));
            
            int fileLength = distrConnection.getContentLength();
            
            //Sometimes distrConnection.getContentLength() doesnt work, attempt to get size from the UpdateDescriptor
            if (fileLength == -1) {
                    logger.fine("fileSource.getContentLength() returned -1, attempting to guess size of file from cache");
                if(fileSource.endsWith(".xml")) {
                    fileLength = (int)updateItem.getCompDescSize();
                } else if (fileSource.endsWith(".jar")) { 
                    //TODO: Currently sets size of dependent jars as the size of the component jar
                    fileLength = (int)updateItem.getCompSize();
                } else {
                    logger.fine("Unable to guess length of file " + fileSource + " because server returned contentLength -1 and extension is neither .xml nor .jar!");
                }
            }
            
            return ioCopy (bsrc, bdest, fileLength);
        } catch (IOException e) {
            newDownloadText("<font color=red>Download Failed: " + e.getMessage() + "</font><br>");
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * Copy a file from fileSource to the destination file fileDest.
     *
     * @param fileSource file path of the source file
     * @param fileDest file path of the destiantion file
     *
     * @return true if the file was successfully copied, otherwise false
     */
    //TODO refactor this method as a delegator target when the source is not a valid URL but may be a valid fileName
    //TODO enable multi-threaded io by calling a Collection of StreamConnectors directly (delete method ioCopy)
    boolean copyToDirFromFileName(String fileSource, String fileDest) {
        int fileLength = -1;
        componentDownloaded = 0; //TODO why is this setting an integer and returning a boolean? check and remove
        try {
            File sourceFile = new File(fileSource);
            File destFile = new File(fileDest);
            final BufferedInputStream bsrc = new BufferedInputStream(new FileInputStream(sourceFile));
            BufferedOutputStream bdest = new BufferedOutputStream(new FileOutputStream(destFile));
            
            fileLength = (int)sourceFile.length();

            return ioCopy (bsrc, bdest, fileLength);
        } catch (IOException e) {
            newDownloadText("<font color=red>Move Failed</font><br>");
            return false;
        }
    }
    
    /**
     * Deletes all components that have been downloaded
     */
    private void deleteComponents() {
        String deletePath = "";
        boolean success;
        //Deletes all files that have the same name as the downloaded components
        for(int i=0; i<allDownloadedCompNames.size(); i++) {
            deletePath = UpdateUtilities.getCompCachePath() + allDownloadedCompNames.get(i);
            success = (new File(deletePath)).delete();
            if (!success) {
                logger.warning("Failed to delete: " + allDownloadedCompNames.get(i));
            } else {
                logger.info("Deleted: " + allDownloadedCompNames.get(i));
            }
        }
        
        //Deletes all of the components older version if the component got updated
        for(int i=0; i<deleteItems.size(); i++) {
            deletePath = UpdateUtilities.getCompCachePath() + deleteItems.get(i).getManifestAttributes().get("QiWorkbench-Component");
            success = (new File(deletePath)).delete();
            if (!success) {
                logger.warning("Failed to delete: " + deleteItems.get(i).getManifestAttributes().get("QiWorkbench-Component"));
            } else {
                //Deletion successed
                logger.info("Deleted: " + deleteItems.get(i).getManifestAttributes().get("QiWorkbench-Component"));
            }
        }
    }
    
    /**
     * Moves the downloaded files from the temporary directory to the users component cache
     */
    private void moveComponents() {
        File tempDir = new File(UpdateConstants.TEMP_FOLDER);
        if(tempDir.isDirectory()){
            File[] childFiles = tempDir.listFiles();
            for(File child : childFiles){
                if(child.isFile()) {
                    newDownloadText("Moving " + child.getName() + "... ");
                    copyToDirFromFileName(child.getAbsolutePath(), UpdateUtilities.getCompCachePath()+child.getName());
                }
            }
        }
    }
    
    /**
     * Deletes the temporary directory
     */
    private void deleteTempFolder() {
        delete(new File(UpdateConstants.TEMP_FOLDER));
    }
    
    /**
     * Deletes file or directory and all the files within the directory
     * @param resource the file or directory
     */
    private void delete(File resource) { 
        if(resource.isDirectory()){
            File[] childFiles = resource.listFiles();
            for(File child : childFiles){
                delete(child);
            }
        }
        resource.delete();
    }
    
    /**
     * Sets the current value of both the total and component progress bars
     * @param modDL sets the value of the component progress bar
     * @param totalDL sets the value of the total progress bar
     */
    private void setProgressBar(long modDL, long totalDL) {
	ComponentProgressBar.setValue(modDL);
        TotalProgressBar.setValue(totalDL);
    }
    
    /**
     * Shows the component being downloaded in the component progress bar
     * @param setString String shown in the component progress bar
     */
    private void setDownloadedBar(String setString) {
        ComponentProgressBar.setString(setString + "%");
    }
    
    /**
     * Sets the text in download display
     * @param text String to be added to the display
     */
    private void newDownloadText(String text) {
        sb.append(text);
        downloadEditorPane.setText(sb.toString());
    }
    
    /**
     * Checks to see if an component being downloaded is a new component or being downloaded
     * @param updateItem UpdateItem of Component to be checked
     * @return returns false if the component is being updated, returns true if component is new
     */
    private boolean checkNewComponent(UpdateItem updateItem) {
        for(int i=0; i<deleteItems.size(); i++) {
            if((deleteItems.get(i).getManifestAttributes().get("QiWorkbench-Component-Name")).equals(updateItem.getManifestAttributes().get("QiWorkbench-Component-Name"))) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Implements and action listener for the available components list
     */
    class AvailListSelectionHandler implements ListSelectionListener {
        //Implements mehod valueChanged of interface ListSelectionListener
        public void valueChanged(ListSelectionEvent e) {
            ListSelectionModel lsm = (ListSelectionModel)e.getSource();

            if (lsm.isSelectionEmpty()) { //If nothing is selected
                currentVersionTextField.setText("");
                availableVersionTextField.setText("");
                addButton.setEnabled(false);
            } else { //Indexes are selected
                // Find out which indexes are selected.
                int minIndex = lsm.getMinSelectionIndex();
                int maxIndex = lsm.getMaxSelectionIndex();
                addButton.setEnabled(true);
                //Check if one index is selected
                if(minIndex == maxIndex) {
                    currentVersionTextField.setText("None");
                    //versionItems contains the components with different version numbers
                    for(int i=0; i<versionItems.size(); i++) {
                        //if the selected component has a different version number, display that, otherwise it displays null
                        if((versionItems.get(i).getManifestAttributes().get("QiWorkbench-Component-Name")).equals(updateItems.get(minIndex).getManifestAttributes().get("QiWorkbench-Component-Name"))) {
                            currentVersionTextField.setText(versionItems.get(i).getManifestAttributes().get("QiWorkbench-Component-Specification-Version"));
                            break;
                        }
                    }
                    availableVersionTextField.setText(updateItems.get(minIndex).getManifestAttributes().get("QiWorkbench-Component-Specification-Version"));
                } else {
                    currentVersionTextField.setText("");
                    availableVersionTextField.setText("");
                }
            }
            
            if(availCompList.getModel().getSize() == 0) {
                addAllButton.setEnabled(false);
            } else {
                addAllButton.setEnabled(true);
            }
        }
    }
    
    /**
     * Implements and action listener for the user selected components list
     */
    class UserListSelectionHandler implements ListSelectionListener {
        //Implements mehod valueChanged of interface ListSelectionListener
        public void valueChanged(ListSelectionEvent e) {
            ListSelectionModel lsm = (ListSelectionModel)e.getSource();

            if (lsm.isSelectionEmpty()) {
                removeButton.setEnabled(false);
            } else {
                removeButton.setEnabled(true);
            }
            
            if(userCompList.getModel().getSize() == 0) {
                removeAllButton.setEnabled(false);
            } else {
                removeAllButton.setEnabled(true);
            }
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.bhpb.qiworkbench.updater.LongProgressBar ComponentProgressBar;
    private com.bhpb.qiworkbench.updater.LongProgressBar TotalProgressBar;
    private javax.swing.JButton addAllButton;
    private javax.swing.JButton addButton;
    private javax.swing.JButton addUpdateCenterButton;
    private javax.swing.JList availCompList;
    private javax.swing.JScrollPane availCompScrollPane;
    private javax.swing.JTextField availableVersionTextField;
    private javax.swing.JButton backButton;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JProgressBar checkingProgressBar;
    private javax.swing.JTextField componentFolderTextField;
    private javax.swing.JTextField currentVersionTextField;
    private javax.swing.JButton deselectAllButton;
    private javax.swing.JEditorPane downloadEditorPane;
    private javax.swing.JLabel downloadLabel;
    private javax.swing.JPanel downloadPanel;
    private javax.swing.JScrollPane downloadScrollPane;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton nextButton;
    private javax.swing.JLabel progressLabel;
    private javax.swing.JButton proxyConfigButton;
    private javax.swing.JButton removeAllButton;
    private javax.swing.JButton removeButton;
    private javax.swing.JButton removeUpdateCentersButton;
    private javax.swing.JPanel retrieveUpdatesPanel;
    private javax.swing.JButton selectAllButton;
    private javax.swing.JLabel selectCenterLabel;
    private javax.swing.JPanel selectComponentsPanel;
    private javax.swing.JPanel selectUpdateCenterPanel;
    private javax.swing.JScrollPane updateCenterScrollPane;
    private javax.swing.JTable updateCenterTable;
    private javax.swing.JTextField updateCenterTextField;
    private javax.swing.JList userCompList;
    private javax.swing.JScrollPane userCompScrollPane;
    // End of variables declaration//GEN-END:variables
   
    private boolean doneDownloading = false;
    private boolean isCoreComponent = false;
    private boolean isFinished = false;
    
    private int currentDisplayNum = 1;
    //private int currentDetailNum = 1;
    private int totalDownloaded = 0;
    private int componentDownloaded = 0;
    private int allCompsTotal = 0;
    
    private ArrayList<UpdateItem> allAvailComps = new ArrayList<UpdateItem>();
    private ArrayList<UpdateItem> userUpdateItems = new ArrayList<UpdateItem>();
    private ArrayList<UpdateItem> updateItems = new ArrayList<UpdateItem>();
    private ArrayList<UpdateItem> deleteItems = new ArrayList<UpdateItem>();
    private ArrayList<UpdateItem> versionItems = new ArrayList<UpdateItem>();
    private DefaultListModel availCompListModel/*= new DefaultListModel()*/;
    private DefaultListModel userCompListModel/*= new DefaultListModel()*/;
    private ArrayList<String> allDownloadedCompNames = new ArrayList<String>();
    
    ListSelectionModel userListSelectionModel;
    ListSelectionModel serverListSelectionModel;

    private String servletCID = "";
    private String myCID = "";
    private String originalUpdateCenter = "";
    private String updateType = "Manual";
    private String compBundles = qiWbConfiguration.getInstance().getPucAttributes().get("pucurl") + qiWbConfiguration.getInstance().getPucAttributes().get("componentpath");
    
    private StringBuffer sb = new StringBuffer();
    private JScrollBar sbar;

    /**
     * Copies a file from one BufferedInputStream to another, indicating to the user when the inputstream is blocked
     *
     * @param in InputStream of the source file
     * @param out OutputStream of the destination file
     * @param flen the size of the file being copied
     */
    boolean ioCopy (BufferedInputStream in, BufferedOutputStream out, long fileLength) throws IOException {
        try {
            //TODO eventually this interface should display multiple download bars (perhaps only with 'show details'
            //and call methods that are synchronized on itself (the GUI)
            ComponentProgressBar.setMaximum(fileLength);
            //TODO fix this - not sure why multiple ioCopy method calls use the same class member counter
            //expl: this is because this method is only invoked serially
            componentDownloaded = 0;
        
            StreamConnector streamConnector = new StreamConnector(in, out, fileLength);
            Thread worker = new Thread(streamConnector);
            worker.start();
        
            do {
                long bytesCopied = streamConnector.getBytesReadSinceLastCheck();
                    
                componentDownloaded += bytesCopied;
                totalDownloaded += bytesCopied;
            
                updateDownloadStatusIndicators(componentDownloaded, totalDownloaded, fileLength);
            } while (streamConnector.isCopyCompleted() == false);
            if (streamConnector.isNormalTermination() == false) {
                logger.warning("An error occurred while downloading: " + streamConnector.getExceptionMessage());
            }
        } finally {
            if (in != null) in.close();
            if (out != null) out.close();
        }
        
        if(!doneDownloading) {
            newDownloadText("<font color=green>Download Successful</font><br>");
        } else {
            newDownloadText("<font color=green>Move Successful</font><br>");
        }
        return true;
    }
    
    private void updateDownloadStatusIndicators(long componentDownloaded, long totalDownloaded, long fileLength) {
        setProgressBar(componentDownloaded, totalDownloaded);
        
        if(fileLength > 0) { //TODO indicate to the user when no fileLength is available rather than doing nothing here
            double percent = ((double)componentDownloaded/(double)fileLength)*100;
            setDownloadedBar("" + (int)percent);
        }
    }
}