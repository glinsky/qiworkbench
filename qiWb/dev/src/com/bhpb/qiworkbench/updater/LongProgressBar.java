/*
 * LongProgressBar.java
 *
 * Created on September 17, 2007, 12:21 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.qiworkbench.updater;

import javax.swing.JProgressBar;

/**
 *
 * @author folsw9
 */
public class LongProgressBar extends JProgressBar{
    long maximum;
    long value;
    
    /** Creates a new instance of LongProgressBar */
    public LongProgressBar() {
        super();
        super.setMaximum(100); // represent the value as an integer percentage, since JProgressBar's setter does not accept a long.
    }
    
    /**
     * Sets the progress bar's current value (stored internally as a rounded integer percentage
     * of the maximum).
     */
    public void setValue(long value) {
        this.value = value;
        double percentMaximum = (new Long(value).doubleValue()) / (new Long(maximum).doubleValue());
        super.setMaximum(new Long(Math.round(percentMaximum)).intValue());
    }
    
    public void setMaximum(long maximum) {
        this.maximum = maximum;
    }
}
