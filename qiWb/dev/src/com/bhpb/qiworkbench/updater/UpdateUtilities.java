package com.bhpb.qiworkbench.updater;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.TimeZone;


import com.bhpb.qiworkbench.client.qiWbConfiguration;
import com.bhpb.qiworkbench.compAPI.CommonUtil;
import java.util.logging.Logger;

public class UpdateUtilities {

    private static Logger logger =
            Logger.getLogger(UpdateUtilities.class.toString());

    public static String getCompCachePath() {
        return CommonUtil.getAppDir(UpdateConstants.COMP_DIR) + File.separator;
    }

    public static String getDisplayText() {
        return displayText;
    }

    public static int getComponentDownloaded() {
        return componentDownloaded;
    }

    public static int getTotalDownloaded() {
        return totalDownloaded;
    }

    public static String getComponentPercent() {
        return componentPercent;
    }

    public static int getComponentLength() {
        return componentLength;
    }

    public static String getDownloadingComponent() {
        return downloadingComponent;
    }

    /**
     * Creates the properties file used by the update dialogs and status dialog
     * @return Returns true if file is created, other wise it returns false 
     */
    private static boolean createPropertiesFile() {
        boolean success = false;
        try {
            File propFile = new File(UpdateConstants.UPDATE_PROP_LOC);
            success = propFile.createNewFile();
            if (success) {
                BufferedWriter out = new BufferedWriter(new FileWriter(UpdateConstants.UPDATE_PROP_LOC));
                out.write("updatetype=\nlastupdate=\nPUC=\nupdatecenters=\nmanualupdatecenters=\nproxy=\nproxyhost=\nproxyport=");
                out.close();
            } else {
            }
        } catch (IOException e) {
            logger.info("IOException creating update.properties file");
        }
        return success;
    }

    /**
     * Checks if the an update center exists in the update properties filee
     * @param inputText Name of the update center
     * @return Returns true if file exists, otherwise false
     */
    public static boolean checkUCExists(String propertyType, String inputText) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(UpdateConstants.UPDATE_PROP_LOC));
            if (properties.getProperty(propertyType) != null) {
                String sep = ",";
                StringTokenizer st = new StringTokenizer(properties.getProperty(propertyType), sep);
                while (st.hasMoreTokens()) {
                    String updateCenter = st.nextToken();
                    if (updateCenter.equals(inputText)) {
                        return true;
                    }
                }
            }
        } catch (IOException e) {
        }
        return false;
    }

    /**
     * Adds the update centers from the config file on the deploy server into the update properties file
     */
    public static void addUpdateCenters() {
        createPropertiesFile();
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(UpdateConstants.UPDATE_PROP_LOC));

            Map<String, String> PUC = qiWbConfiguration.getInstance().getPucAttributes();
            if (PUC.size() > 0) { //If config file does not exist or PUC is not defined

                properties.setProperty("PUC", PUC.get("pucurl") + "|" + PUC.get("componentpath"));
                properties.store(new FileOutputStream(UpdateConstants.UPDATE_PROP_LOC), null);
            } else {
                properties.setProperty("PUC", "");
                properties.store(new FileOutputStream(UpdateConstants.UPDATE_PROP_LOC), null);
            }
            ArrayList<Map<String, String>> updateCenters = qiWbConfiguration.getInstance().getUpdateCentersAttributes();
            if (updateCenters.size() > 0) { //If config file does not exist or PUC or no update centers are defined

                for (int i = 0; i < updateCenters.size(); i++) {
                    if (!checkUCExists("updatecenters", updateCenters.get(i).get("updatecenter") + "|" + updateCenters.get(i).get("componentpath"))) {
                        String addNewUC = properties.getProperty("updatecenters") + updateCenters.get(i).get("updatecenter") + "|" + updateCenters.get(i).get("componentpath") + ",";
                        properties.setProperty("updatecenters", addNewUC);
                        properties.store(new FileOutputStream(UpdateConstants.UPDATE_PROP_LOC), null);
                    }
                }
            } else {
                properties.setProperty("updatecenters", "");
                properties.store(new FileOutputStream(UpdateConstants.UPDATE_PROP_LOC), null);
            }

        } catch (IOException ioe) {
            logger.info("IOException");
            ioe.printStackTrace();
        }
    }

    /**
     * Updates the update properties files and status file with information about the current update 
     */
    public static void updateUpdateFiles(String updateType, String fileText) {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        java.text.SimpleDateFormat date = new java.text.SimpleDateFormat("MMMMM dd, yyyy hh:mm:ss a");
        date.setTimeZone(TimeZone.getDefault());
        Properties properties = new Properties();
        //Write the type of update and the time the update took place
        try {
            properties.load(new FileInputStream(UpdateConstants.UPDATE_PROP_LOC));
            properties.setProperty("lastupdate", date.format(cal.getTime()));
            properties.setProperty("updatetype", updateType);
            properties.store(new FileOutputStream(UpdateConstants.UPDATE_PROP_LOC), null);
        } catch (IOException e) {
        }
        //Store the text for the updates that took place for the status dialog
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(UpdateConstants.STATUS_FILE));
            out.write(fileText);
            out.close();
        } catch (IOException e) {
            logger.info("IOException");
            e.printStackTrace();
        }
    }

    /**
     * Creates a temporary folder to store the downloaded components into
     * @return Returns true if the folder is created, otherwise false
     */
    public static boolean createTempFolder() {
        logger.info("Creating temp directory...");
        boolean success = (new File(UpdateConstants.TEMP_FOLDER)).mkdir();
        if (success) {
            logger.info("Directory: " + UpdateConstants.TEMP_FOLDER + " created");
        } else {
            logger.info("Directory: " + UpdateConstants.TEMP_FOLDER + " failed to be created");
        }
        return success;
    }
    public static String MESSAGE_DISPATCHER_CID = "";
    public static String SERVLET_DISP_DESC = "";
    private static String displayText = "";
    private static String componentPercent = "";
    private static String downloadingComponent = "";
    private static int componentDownloaded = 0;
    private static int totalDownloaded = 0;
    private static int componentLength = 0;
}
