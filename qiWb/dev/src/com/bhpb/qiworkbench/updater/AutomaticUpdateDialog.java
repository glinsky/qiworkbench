/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.updater;

import java.awt.CardLayout;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;
import com.bhpb.qiworkbench.client.UpdateManager;
import com.bhpb.qiworkbench.client.qiWbConfiguration;
import java.util.logging.Logger;

/**
 * @author  vaalm
 * @author Woody Folsom woody.folsom@bhpbilliton.net
 */
public class AutomaticUpdateDialog extends javax.swing.JDialog {
    private static final Logger logger = Logger.getLogger(AutomaticUpdateDialog.class.toString());
    
    public AutomaticUpdateDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }
    
    public AutomaticUpdateDialog(String myCID, String servletCID) {
    	String proxyHost = "";
        String proxyPort = "";
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(UpdateConstants.UPDATE_PROP_LOC));
            String proxy = properties.getProperty("proxy", "true");
            proxyHost = properties.getProperty("proxyhost", proxyHost);
            proxyPort = properties.getProperty("proxyport", proxyPort);
            if(proxy != null && proxy.equals("true")) {
                Properties systemSettings = System.getProperties();
                systemSettings.put("http.proxyHost", proxyHost);
                systemSettings.put("http.proxyPort", proxyPort);
                System.setProperties(systemSettings);
            } else {
                Properties systemSettings = System.getProperties();
                systemSettings.remove("http.proxyHost");
                systemSettings.remove("http.proxyPort");
                System.setProperties(systemSettings);
            }
        } catch (IOException e) {
        }
        
        this.myCID = myCID;
        this.servletCID = servletCID;
        this.setModal(true);
        sb.append("<font face=\"Arial\" size=3>");
        UpdateUtilities.addUpdateCenters();
        initComponents();
        this.setLocationRelativeTo(null);
        new Thread() {
            public void run() {
                while(!isVisible())
                {
                    logger.info("Automatic update component is not visible, sleeping 1 second.");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ie) {
                        logger.warning("Interrupted.");
                    }
                }
                autoUpdate();
            }
        }.start();
        this.setVisible(true);
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        mainPanel = new javax.swing.JPanel();
        retrieveUpdatePanel = new javax.swing.JPanel();
        checkingUpdatePanel = new javax.swing.JPanel();
        ProgressjLabel = new javax.swing.JLabel();
        checkingProgressBarPanel = new javax.swing.JPanel();
        checkingProgressBar = new javax.swing.JProgressBar();
        updatePanelSeparator = new javax.swing.JSeparator();
        downloadPanel = new javax.swing.JPanel();
        downloadTextAreaPanel = new javax.swing.JPanel();
        downloadTextAreaScrollPane = new javax.swing.JScrollPane();
        downloadTextArea = new javax.swing.JTextArea();
        downloadProgressBarPanel = new javax.swing.JPanel();
        ComponentProgressBar = new javax.swing.JProgressBar();
        TotalProgressBar = new javax.swing.JProgressBar();
        downloadPanelSeparator = new javax.swing.JSeparator();
        continueButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        mainPanel.setLayout(new java.awt.CardLayout());

        mainPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Updating..."));
        ProgressjLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ProgressjLabel.setText("Checking For Updates");

        org.jdesktop.layout.GroupLayout checkingUpdatePanelLayout = new org.jdesktop.layout.GroupLayout(checkingUpdatePanel);
        checkingUpdatePanel.setLayout(checkingUpdatePanelLayout);
        checkingUpdatePanelLayout.setHorizontalGroup(
            checkingUpdatePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, checkingUpdatePanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(ProgressjLabel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE)
                .addContainerGap())
        );
        checkingUpdatePanelLayout.setVerticalGroup(
            checkingUpdatePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(checkingUpdatePanelLayout.createSequentialGroup()
                .add(86, 86, 86)
                .add(ProgressjLabel)
                .addContainerGap(95, Short.MAX_VALUE))
        );

        checkingProgressBar.setIndeterminate(true);

        org.jdesktop.layout.GroupLayout checkingProgressBarPanelLayout = new org.jdesktop.layout.GroupLayout(checkingProgressBarPanel);
        checkingProgressBarPanel.setLayout(checkingProgressBarPanelLayout);
        checkingProgressBarPanelLayout.setHorizontalGroup(
            checkingProgressBarPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(checkingProgressBarPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(checkingProgressBar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE)
                .addContainerGap())
        );
        checkingProgressBarPanelLayout.setVerticalGroup(
            checkingProgressBarPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(checkingProgressBarPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(checkingProgressBar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout retrieveUpdatePanelLayout = new org.jdesktop.layout.GroupLayout(retrieveUpdatePanel);
        retrieveUpdatePanel.setLayout(retrieveUpdatePanelLayout);
        retrieveUpdatePanelLayout.setHorizontalGroup(
            retrieveUpdatePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, updatePanelSeparator, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 523, Short.MAX_VALUE)
            .add(checkingUpdatePanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, checkingProgressBarPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        retrieveUpdatePanelLayout.setVerticalGroup(
            retrieveUpdatePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(retrieveUpdatePanelLayout.createSequentialGroup()
                .add(checkingUpdatePanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(updatePanelSeparator, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(checkingProgressBarPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        mainPanel.add(retrieveUpdatePanel, "card1");

        downloadTextArea.setColumns(20);
        downloadTextArea.setRows(5);
        downloadTextAreaScrollPane.setViewportView(downloadTextArea);

        org.jdesktop.layout.GroupLayout downloadTextAreaPanelLayout = new org.jdesktop.layout.GroupLayout(downloadTextAreaPanel);
        downloadTextAreaPanel.setLayout(downloadTextAreaPanelLayout);
        downloadTextAreaPanelLayout.setHorizontalGroup(
            downloadTextAreaPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(downloadTextAreaScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 523, Short.MAX_VALUE)
        );
        downloadTextAreaPanelLayout.setVerticalGroup(
            downloadTextAreaPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(downloadTextAreaScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)
        );

        ComponentProgressBar.setString("");
        ComponentProgressBar.setStringPainted(true);

        TotalProgressBar.setString("TOTAL");
        TotalProgressBar.setStringPainted(true);

        org.jdesktop.layout.GroupLayout downloadProgressBarPanelLayout = new org.jdesktop.layout.GroupLayout(downloadProgressBarPanel);
        downloadProgressBarPanel.setLayout(downloadProgressBarPanelLayout);
        downloadProgressBarPanelLayout.setHorizontalGroup(
            downloadProgressBarPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(downloadProgressBarPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(downloadProgressBarPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, TotalProgressBar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, ComponentProgressBar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE))
                .addContainerGap())
        );
        downloadProgressBarPanelLayout.setVerticalGroup(
            downloadProgressBarPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(downloadProgressBarPanelLayout.createSequentialGroup()
                .add(ComponentProgressBar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(TotalProgressBar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout downloadPanelLayout = new org.jdesktop.layout.GroupLayout(downloadPanel);
        downloadPanel.setLayout(downloadPanelLayout);
        downloadPanelLayout.setHorizontalGroup(
            downloadPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, downloadProgressBarPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(downloadPanelSeparator, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 523, Short.MAX_VALUE)
            .add(downloadTextAreaPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        downloadPanelLayout.setVerticalGroup(
            downloadPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, downloadPanelLayout.createSequentialGroup()
                .add(downloadTextAreaPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(downloadPanelSeparator, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(downloadProgressBarPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
        mainPanel.add(downloadPanel, "card2");

        continueButton.setText("Cancel");
        continueButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                continueButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(mainPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 533, Short.MAX_VALUE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap(446, Short.MAX_VALUE)
                .add(continueButton)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(mainPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 277, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(continueButton)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void continueButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_continueButtonActionPerformed
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_continueButtonActionPerformed
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AutomaticUpdateDialog(new javax.swing.JFrame(), true).setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar ComponentProgressBar;
    private javax.swing.JLabel ProgressjLabel;
    private javax.swing.JProgressBar TotalProgressBar;
    private javax.swing.JProgressBar checkingProgressBar;
    private javax.swing.JPanel checkingProgressBarPanel;
    private javax.swing.JPanel checkingUpdatePanel;
    private javax.swing.JButton continueButton;
    private javax.swing.JPanel downloadPanel;
    private javax.swing.JSeparator downloadPanelSeparator;
    private javax.swing.JPanel downloadProgressBarPanel;
    private javax.swing.JTextArea downloadTextArea;
    private javax.swing.JPanel downloadTextAreaPanel;
    private javax.swing.JScrollPane downloadTextAreaScrollPane;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JPanel retrieveUpdatePanel;
    private javax.swing.JSeparator updatePanelSeparator;

    private void autoUpdate() {
        logger.info("Getting all downloadable components...");
        getAllDownloadableComponents();
        logger.info("Checking for updates...");
        checkUpdates();
        // Runs only if there are components that need updates, otherwise status dialog displays no updates
        if(updateItems.size() > 0) {
            logger.info("One or more update items exist, setting continue button enabled...");
            continueButton.setEnabled(false);
            logger.info("Creating temp folder");
            UpdateUtilities.createTempFolder();
            downloadComponents();
            deleteComponents();
            moveComponents();
            deleteTempFolder();
            continueButton.setText("Done");
            continueButton.setEnabled(true);
        } else {
            logger.info("No updates exist - disposing of AutomaticUpdateDialog.");
            sb.append("<font color=green>No Components Updated</font>");
            this.dispose();
        }
        UpdateUtilities.updateUpdateFiles(updateType, sb.toString());
    }
    
    /**
     * Gets available downloads from the PUC and secondary update centers found in the config file on the distrib server
     */
    private void getAllDownloadableComponents() {
        ProgressjLabel.setText("Getting All Available Components");
        allAvailComps = UpdateManager.getAllAvailComps(myCID, servletCID);
    }
    
    /**
     * Checks the available components with the components on the users computer to see if any need updating
     */
    private void checkUpdates() {
        ProgressjLabel.setText("Checking Updates");
        ArrayList<ArrayList<UpdateItem>> updateChecker = UpdateManager.checkUpdates(allAvailComps);
        updateItems = updateChecker.get(0);
        deleteItems = updateChecker.get(1);
       
    }
    
    /**
     * Starts the download process for each component that needs updating
     */
    private void downloadComponents() {
        CardLayout cl = (CardLayout)(mainPanel.getLayout());
        cl.show(mainPanel, "card2");
        TotalProgressBar.setMaximum(UpdateManager.getTotalSize(updateItems));
        for(int i=0; i<updateItems.size(); i++) {
                update(updateItems.get(i));
        }
        doneDownloading = true;
    }
    
    /**
     * Downloads the component, update descriptor, and dependent jars for each component that needs updating
     * @param updateItem UpdateItem of the component that needs updating
     */
    private void update(UpdateItem updateItem) {
        downloadComponent(updateItem);
        downloadUpdateDescriptor(updateItem);
        downloadDepJars(updateItem);
    }
    
    /**
     * Downloads the Components
     * @param updateItem UpdateItem of the component that needs updating
     */
    private void downloadComponent(UpdateItem updateItem) {
    	//Downloads the component only if it is an external component (update descriptors maybe
        if((updateItem.getManifestAttributes().get("QiWorkbench-Component-Component-Type")).equals("external plugin") ||
                (updateItem.getManifestAttributes().get("QiWorkbench-Component-Component-Type")).equals("external viewer agent")) {
		    isCoreComponent = false;
	        setComponentDownloaded(updateItem.getManifestAttributes().get("QiWorkbench-Component"));
	        String updateURL = compBundles + updateItem.getManifestAttributes().get("QiWorkbench-Component");
	        String destURL = UpdateConstants.TEMP_FOLDER + updateItem.getManifestAttributes().get("QiWorkbench-Component");
	        allDownloadedCompNames.add(updateItem.getManifestAttributes().get("QiWorkbench-Component"));
	        logger.info("Downloading: " + updateItem.getManifestAttributes().get("QiWorkbench-Component"));
		    if(checkNewComponent(updateItem)) {
	            newDownloadText("Downloading New Component: " + updateItem.getManifestAttributes().get("QiWorkbench-Component") + "... ", "blue", false);
	        } else {
	            newDownloadText("Updating: " + updateItem.getManifestAttributes().get("QiWorkbench-Component") + "... ", "purple", false);
	        }
	        copyToDirFromURL(updateURL, destURL, updateItem);
        } else {
            isCoreComponent = true;
        }
    }

    /**
     * Downloads the Update Descriptor of the component being updated
     * @param updateItem UpdateItem of the component that needs updating
     */
    private void downloadUpdateDescriptor(UpdateItem updateItem) {
        setComponentDownloaded(updateItem.getFileName());
        String updateURL = compBundles + updateItem.getFileName();
        String destURL = UpdateConstants.TEMP_FOLDER + updateItem.getFileName();
        allDownloadedCompNames.add(updateItem.getFileName());
        logger.info("Downloading: " + updateItem.getFileName());
        if(isCoreComponent) {
            newDownloadText("Downloading Update Descriptor: " + updateItem.getFileName() + "... ", "#0000A0", false);
        } else {
            newDownloadText("  Downloading Update Descriptor: " + updateItem.getFileName() + "... ", "black", true);
        }
        copyToDirFromURL(updateURL, destURL, updateItem);
    }
    
    /**
     * Downloads the dependent jars of the component being updated
     * @param updateItem UpdateItem of the component that needs updating
     */
    private void downloadDepJars(UpdateItem updateItem) {
        if((updateItem.getManifestAttributes().get("QiWorkbench-Component-Component-Type")).equals("external plugin") ||
                (updateItem.getManifestAttributes().get("QiWorkbench-Component-Component-Type")).equals("external viewer agent")) {
            String compDep = updateItem.getManifestAttributes().get("QiWorkbench-Component-Component-Dependencies");
            if(compDep != null) {
                String sep = ",";
                StringTokenizer st = new StringTokenizer(compDep, sep);
                while (st.hasMoreTokens()) {
                    String jarFile = st.nextToken();
                    jarFile = jarFile.replace(" ", "");
                    setComponentDownloaded(jarFile);
                    String updateURL = compBundles + jarFile;
                    String destURL = UpdateConstants.TEMP_FOLDER + jarFile;
                    allDownloadedCompNames.add(jarFile);
                    logger.info("Downloading: " + jarFile);
                    newDownloadText("  Downloading Dependent JAR: " + jarFile + "... ", "black", true);
                    copyToDirFromURL(updateURL, destURL, updateItem);
                }
            }
            
            compDep = updateItem.getManifestAttributes().get("QiWorkbench-Component-JNI-Dependencies");
            //Check if the component has dependent components
            
            if(compDep != null) {
                String sep = ",";
                StringTokenizer st = new StringTokenizer(compDep, sep);
                while (st.hasMoreTokens()) {
                    String jarFile = st.nextToken();
                    jarFile = jarFile.replace(" ", "");
                    String updateURL = compBundles + jarFile;
                    String destURL = UpdateConstants.TEMP_FOLDER + jarFile;
                    //Used to help delete files after download
                    allDownloadedCompNames.add(jarFile);
                    //&nbsp; gives the tab look in the download display
                    newDownloadText("  Downloading Dependent JAR: " + jarFile + "... ", "black", true);
                    
                    //Download dependent JARs
                    copyToDirFromURL(updateURL, destURL, updateItem);
                }
            }
        }
    }
    
    /**
     * Checks to make sure the component isn't already set to be updated
     * @param updateItem UpdateItem of the component that needs updating
     * @return Returns true if component is not already set to be updated, otherwise false
     */
    private boolean checkNewComponent(UpdateItem updateItem) {
        for(int i=0; i<deleteItems.size(); i++) {
            if((deleteItems.get(i).getManifestAttributes().get("QiWorkbench-Component-Name")).equals(updateItem.getManifestAttributes().get("QiWorkbench-Component-Name"))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Copies a URL file to a directory
     * @param fileSource Source of the incoming file
     * @param fileDest Destination of the file
     * @param updateItem UpdateItem of the component that needs updating
     */
    private void copyToDirFromURL(String fileSource, String fileDest, UpdateItem updateItem) {
        int flen = -1;
        componentDownloaded = 0;
        try {
            URL url = new URL(fileSource);
            URLConnection distrConnection = url.openConnection();
            final BufferedInputStream bsrc = new BufferedInputStream(distrConnection.getInputStream());
            BufferedOutputStream bdest = new BufferedOutputStream(new FileOutputStream(fileDest));
            flen = distrConnection.getContentLength();
            
            if (flen == -1) {
                String downloadFailedMessage = "Unable to download file: " + fileSource + " because getContentLength() returned: -1";
                logger.severe(downloadFailedMessage);
                newDownloadText(downloadFailedMessage, "red", false);
            } else {
                ioCopy (bsrc, bdest, flen);
            }
        } catch (IOException e) {
	    newDownloadText("Download Failed", "red", false);
        }
    }
    
    /**
     * Copies a File to a directory
     * @param fileSource Source of the incoming file
     * @param fileDest Destination of the file
     */
    private void copyToDirFromFileName(String fileSource, String fileDest) {
        int flen = -1;
        componentDownloaded = 0;
        try {
            File destFile = new File(fileDest);
            File sourceFile = new File(fileSource);
            final BufferedInputStream bsrc = new BufferedInputStream(new FileInputStream(sourceFile));
            BufferedOutputStream bdest = new BufferedOutputStream(new FileOutputStream(destFile));
            
            flen = new Long(sourceFile.length()).intValue();
            
            if (flen == -1) {
                String downloadFailedMessage = "Unable to download file: " + fileSource + " because length() returned: -1";
                logger.severe(downloadFailedMessage);
                newDownloadText(downloadFailedMessage, "red", false);
            } else {
                ioCopy (bsrc, bdest, flen);
            }

        } catch (IOException e) {
	    newDownloadText("Download, writing file or move failed", "red", false);
            e.printStackTrace();
        }
    }

    /**
     * Copies a file and updates the GUI with its progress 
     * @param in Input stream
     * @param out Output stream
     * @param flen Size of the downloading file
     * @throws IOException 
     */
    //TODO refactor this into a copier class and eliminate redundant code here and in AutomaticUpdateDialog
    //at which point synchronized can be removed or promoted to the calling method
    synchronized void ioCopy (BufferedInputStream in, BufferedOutputStream out, int flen) throws IOException {
        int c;
        final int COMP_UPDATE_BLOCKING_WAIT_PERIOD = 100; // wait 1/10 second if httpUrlConnection-backed input steram is blocked
        try {
            ComponentProgressBar.setMaximum(flen);
            /* TODO: Fix componentDownloaded in the calling method, which may accidentally be masked by class member.  
                     This method should probably return it.
            */
            //TODO: catch IOEXCEPTION here and prompt to retry
            //TODO: use logging properly here
            componentDownloaded = 0;
            //logger.finest("Component bytes downloaded = 0");
            while (componentDownloaded < flen) {
                //logger.finest("Downloaded " + componentDownloaded + " bytes of " + flen);
                int bytesAvailable = in.available();
                //logger.finest(bytesAvailable + " bytes available for download");
                if (bytesAvailable > 0) {
                    byte[] byteBuffer = new byte[bytesAvailable];
                    in.read(byteBuffer);
                    out.write(byteBuffer);
                    //TODO make sure these are properly using class members - any exception will likely break this
                    componentDownloaded += bytesAvailable;
                    totalDownloaded += bytesAvailable;
                    setProgressBar(componentDownloaded, totalDownloaded);
                } else {
                    //logger.finest("No bytes Available");
                    //logger.finest("Update descriptor or jar inputstream appears to be blocked... waiting 100 milleseconds...");
                    try {
                        Thread.sleep(COMP_UPDATE_BLOCKING_WAIT_PERIOD);
                    } catch (InterruptedException ie) {
                        Thread.interrupted();
                    }
                }
            }
        } finally {
            if (in != null) in.close();
            if (out != null) out.close();
        }
        
	if(!doneDownloading) {
            newDownloadText("Download Successful", "green", false);
        } else {
            newDownloadText("Move Successful", "green", false);
        }
    }
    
    /**
     * Deletes the components in the user cache directory that are being downloaded
     */
    private void deleteComponents() {
        //ProgressjLabel.setText("Deleting Old Components");
        ComponentProgressBar.setString("Deleting Old Components");
        String deletePath = "";
        boolean success;
        for(int i=0; i<allDownloadedCompNames.size(); i++) {
            deletePath = UpdateUtilities.getCompCachePath() + allDownloadedCompNames.get(i);
            File potentialStateCachedFile = new File(deletePath);
            if (potentialStateCachedFile.exists()) {
                success = potentialStateCachedFile.delete();
                if (!success) {
                    // Deletion failed
                    logger.warning("Failed to delete old cached file: " + allDownloadedCompNames.get(i));
                } else {
                    logger.info("Deleted old cached file: " + allDownloadedCompNames.get(i));
                }
            }
        }
        
        for(int i=0; i<deleteItems.size(); i++) {
            deletePath = UpdateUtilities.getCompCachePath() + deleteItems.get(i).getManifestAttributes().get("QiWorkbench-Component");
            File potentialStateCachedFile = new File(deletePath);
            if (potentialStateCachedFile.exists()) {
                success = (new File(deletePath)).delete();
                if (!success) {
                    // Deletion failed
                    logger.warning("Failed to delete old cached file: " + deleteItems.get(i).getManifestAttributes().get("QiWorkbench-Component"));
                } else {
                    logger.info("Deleted old cached file: " + deleteItems.get(i).getManifestAttributes().get("QiWorkbench-Component"));
                }
            }
        }
    }
    
    /**
     * Moves the components from the temporary folder into the user component cache directory 
     */
    private void moveComponents() {
        //ProgressjLabel.setText("Moving New Components");
        ComponentProgressBar.setString("Moving Components");
        File tempDir = new File(UpdateConstants.TEMP_FOLDER);
        if(tempDir.isDirectory()){
            File[] childFiles = tempDir.listFiles();
            for(File child : childFiles){
                if(child.isFile()) {
		    newDownloadText("Moving " + child.getName() + "... ", "black", false);
                    copyToDirFromFileName(child.getAbsolutePath(), UpdateUtilities.getCompCachePath()+child.getName());
                }
            }
        }
    }
    
    /**
     * Deletes the temporary folder and everything inside
     */
    private void deleteTempFolder() {
        delete(new File(UpdateConstants.TEMP_FOLDER));
    }

    /**
     * Deletes a folder and everything inside
     * @param resource File or directory that is to be deleted
     */
    private void delete(File resource) {
        if(resource.isDirectory()){
            File[] childFiles = resource.listFiles();
            for(File child : childFiles){
                delete(child);
            }
        }
        resource.delete();
    }

    /**
     * Sets the Component and Total Progress bar of the GUI
     * @param modDL How much of the component that has been downloaded
     * @param totalDL How much of the total amount has been downloaded
     */
    private void setProgressBar(int modDL, int totalDL) {
	ComponentProgressBar.setValue(modDL);
        TotalProgressBar.setValue(totalDL);
    }

    /**
     * Sets the text of the component download bar
     * @param modDL Name of the component being downloaded
     */
    private void setComponentDownloaded(String modDL) {
        ComponentProgressBar.setString(modDL);
    }
    
    private void newDownloadText(String text, String color, boolean tab) {
        //newDownloadText("<font color=green>Download Successful</font><br>");
        if(tab) {
            sb.append("<font color=" + color + ">&nbsp;&nbsp;" + text + "</font>");
        } else {
            sb.append("<font color=" + color + ">" + text + "</font>");
        }
        downloadTextArea.append(text);
        if(color.equals("red") || color.equals("green")) {
            sb.append("<br>");
            downloadTextArea.append("\n");
        }
    }
    
    private String myCID;
    private String servletCID;
    private String displayText;
    private String updateType = "Automatic";    
    private String compBundles = qiWbConfiguration.getInstance().getPucAttributes().get("pucurl") + qiWbConfiguration.getInstance().getPucAttributes().get("componentpath");
    private StringBuffer sb = new StringBuffer();
    private ArrayList<UpdateItem> updateItems = new ArrayList<UpdateItem>();
    private ArrayList<UpdateItem> deleteItems = new ArrayList<UpdateItem>();
    private ArrayList<UpdateItem> allAvailComps = new ArrayList<UpdateItem>();
    private ArrayList<String> allDownloadedCompNames = new ArrayList<String>();
    private boolean doneDownloading = false;
    private boolean isCoreComponent = false;
    private int totalDownloaded = 0;
    private int componentDownloaded = 0;
}