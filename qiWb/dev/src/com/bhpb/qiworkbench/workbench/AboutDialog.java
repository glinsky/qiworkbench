/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006-2009  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.workbench;

import com.bhpb.qiworkbench.util.PropertyUtils;
import java.lang.reflect.Method;
import java.util.Properties;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;
import javax.swing.event.HyperlinkEvent;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

/**
 * @author Marcus Vaal
 * @version 1.0
 */
public class AboutDialog extends javax.swing.JDialog{
    private static Logger logger = Logger.getLogger(AboutDialog.class.getName());

    /**
     * Create instance of an About Dialog
     */
    public AboutDialog() {
        initComponents();
    }

    /**
     * Initializes the components
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        ComponentIcons = new javax.swing.JPanel();
        qiWorkbench = new javax.swing.JButton();
        ButtonPanel = new javax.swing.JPanel();
        okButton = new javax.swing.JButton();
        compDetailsButton = new javax.swing.JButton();
        configDetailsButton = new javax.swing.JButton();
        LogoAndText = new javax.swing.JPanel();
        logoLabel = new javax.swing.JLabel();
        aboutScrollPane = new javax.swing.JScrollPane();
        aboutTextEditorPane = new javax.swing.JEditorPane();
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        java.net.URL imgURL2 = AboutDialog.class.getResource("QIW_icon.gif");
        qiWorkbench.setIcon(new javax.swing.ImageIcon(imgURL2));
        qiWorkbench.setToolTipText("iqworkbench.org");
        qiWorkbench.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                qiWorkbenchMouseClicked(evt);
            }
        });

        org.jdesktop.layout.GroupLayout ComponentIconsLayout = new org.jdesktop.layout.GroupLayout(ComponentIcons);
        ComponentIcons.setLayout(ComponentIconsLayout);
        ComponentIconsLayout.setHorizontalGroup(
            ComponentIconsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(ComponentIconsLayout.createSequentialGroup()
                .addContainerGap()
                .add(qiWorkbench, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 40, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(586, Short.MAX_VALUE))
        );
        ComponentIconsLayout.setVerticalGroup(
            ComponentIconsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(ComponentIconsLayout.createSequentialGroup()
                .addContainerGap()
                .add(qiWorkbench, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 40, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        compDetailsButton.setText("Component Details");
        compDetailsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                compDetailsButtonActionPerformed(evt);
            }
        });

        configDetailsButton.setText("Configuration Details");
        configDetailsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                configDetailsButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout ButtonPanelLayout = new org.jdesktop.layout.GroupLayout(ButtonPanel);
        ButtonPanel.setLayout(ButtonPanelLayout);
        ButtonPanelLayout.setHorizontalGroup(
            ButtonPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, ButtonPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(compDetailsButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(configDetailsButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 266, Short.MAX_VALUE)
                .add(okButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 74, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        ButtonPanelLayout.linkSize(new java.awt.Component[] {compDetailsButton, configDetailsButton}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        ButtonPanelLayout.setVerticalGroup(
            ButtonPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, ButtonPanelLayout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(ButtonPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(okButton)
                    .add(compDetailsButton)
                    .add(configDetailsButton))
                .addContainerGap())
        );

        LogoAndText.setBackground(new java.awt.Color(255, 255, 255));
        java.net.URL imgURL = AboutDialog.class.getResource("QIW_logo.gif");
        logoLabel.setIcon(new javax.swing.ImageIcon(imgURL));
        logoLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        aboutScrollPane.setBorder(null);
        aboutTextEditorPane.setEditable(false);
        aboutTextEditorPane.setContentType("text/html");

    try {
            props = PropertyUtils.loadProperties();
        } catch (IOException ioe) {
            logger.warning("IO exception reading version.properties file:" + ioe.getMessage());
        }

        versionProp = props.getProperty("project.version");
        if(versionProp == null) {
            versionProp = "";
        }

        buildProp = props.getProperty("project.build");
        if(buildProp == null) {
            buildProp = "";
        }

        aboutTextEditorPane.setText(AboutDialogText());

        aboutTextEditorPane.addHyperlinkListener(new javax.swing.event.HyperlinkListener() {
            public void hyperlinkUpdate(javax.swing.event.HyperlinkEvent evt) {
                aboutTextEditorPaneHyperlinkUpdate(evt);
            }
        });

        aboutScrollPane.setViewportView(aboutTextEditorPane);
        // position to top of text
        aboutTextEditorPane.setCaretPosition(0);

        org.jdesktop.layout.GroupLayout LogoAndTextLayout = new org.jdesktop.layout.GroupLayout(LogoAndText);
        LogoAndText.setLayout(LogoAndTextLayout);
        LogoAndTextLayout.setHorizontalGroup(
            LogoAndTextLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(LogoAndTextLayout.createSequentialGroup()
                .add(logoLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 192, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(aboutScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 440, Short.MAX_VALUE))
        );
        LogoAndTextLayout.setVerticalGroup(
            LogoAndTextLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(aboutScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
            .add(logoLabel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, ButtonPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(ComponentIcons, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(LogoAndText, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(LogoAndText, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(ComponentIcons, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(ButtonPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Returns text about the qiWorkbench
     * @return String contains text about qiWorkbench
     */
    private String AboutDialogText() {
        StringBuffer sb = new StringBuffer();
        sb.append("<font face=\"Arial\" size=2>");

        sb.append("qiWorkbench - an extensible platform for seismic interpretation<br><br>");

        sb.append("Version: " + versionProp + "<br>");
        sb.append("Build id: " + buildProp + "<br><br>");

        sb.append("Copyright (C) 2006-2009 BHP Billiton Petroleum<br>");
        sb.append("qiWorkbench comes with ABSOLUTELY NO WARRANTY.<br><br>");

        sb.append("This is free software, and you are welcome to redistribute it under certain conditions. Portions of qiWorkbench are licensed ");
        sb.append("under the <b><a href=\"http://www.gnu.org/licenses/gpl.txt\">GNU General Public License Version 2</a></b>. Other portions ");
        sb.append("are licensed<br>under the <b><a href=\"http://www.opensource.org/licenses/bsd-license.php\">BSD License</a></b>.  Please refer ");
        sb.append("to the headers of each source code file for more information.<br><br>");

        sb.append("PLEASE NOTE: One or more BHP Billiton patents or patent applications may cover this software or its use. BHP Billiton hereby grants a limited, personal, irrevocable, perpetual, royalty-free license under such patents, but only as needed for activities you are otherwise allowed to do pursuant to the terms of the relevant software license governing that portion of the software.  All other rights are expressly reserved.<br><br>");

        sb.append("To contact BHP Billiton about this software you can e-mail <b><a href=\"mailto:info@qiworkbench.org\">info@qiworkbench.org</a></b> ");
        sb.append("or visit <b><a href=\"http://www.qiworkbench.org/\">www.qiworkbench.org</a></b> to learn more.<br><br>");

        sb.append("This product includes software developed by the<br>");
        sb.append("Apache Software Foundation <b><a href=\"http://www.apache.org/\">http://www.apache.org/</a></b><br>");
        sb.append("Codehaus <b><a href=\"http://www.codehaus.org/\">http://www.codehaus.org/</a></b><br>");
        sb.append("Sun Microsystems, Inc. <b><a href=\"http://java.sun.com/\">http://java.sun.com/</a></b><br>");

        sb.append("</font>");
        return sb.toString();
    }


    /**
     * Action handler that changes curser when the mouse is over a hyperlink and opens a web browser when a hyperlink is clicked
     * @param evt event when a hyperlink is active
     */
    private void aboutTextEditorPaneHyperlinkUpdate(javax.swing.event.HyperlinkEvent evt) {//GEN-FIRST:event_aboutTextEditorPaneHyperlinkUpdate
        if (evt.getEventType() == HyperlinkEvent.EventType.ENTERED) {
            setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        } else if (evt.getEventType() == HyperlinkEvent.EventType.EXITED) {
            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        } else if(evt.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            StringTokenizer st = new StringTokenizer(evt.getDescription(), " ");
            if (st.hasMoreTokens()) {
                String s = st.nextToken();
                openWebBrowser(s);
            }
        }
    }//GEN-LAST:event_aboutTextEditorPaneHyperlinkUpdate

    /**
     * Launches the qiWorkbench website
     * @param evt event when qiWorkbench button is pressed
     */
    private void qiWorkbenchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_qiWorkbenchMouseClicked
        String url = "http://www.qiworkbench.org";
        openWebBrowser(url);
    }//GEN-LAST:event_qiWorkbenchMouseClicked

    /**
     * Launches a Configuration Detail Dialog
     * @param evt event when 'Configuration Details' button is pressed
     */
    private void configDetailsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_configDetailsButtonActionPerformed
        javax.swing.JDialog.setDefaultLookAndFeelDecorated(true);
        AboutConfigurationDialog configDialog = new AboutConfigurationDialog();
        configDialog.setTitle("Configuration Details");
        configDialog.setSize(550, 322);
        //configDialog.setModal(true);
        configDialog.setDefaultCloseOperation(javax.swing.JDialog.DISPOSE_ON_CLOSE);
        //configDialog.setLocation(200, 0);
    // Centers the Configuration Dialog in the middle of the About Dialog
        configDialog.setLocation(this.getBounds().x +(this.getBounds().width/2) - (configDialog.getBounds().width/2),
                            this.getBounds().y +(this.getBounds().height/2) - (configDialog.getBounds().height/2));
        configDialog.setVisible(true);

    }//GEN-LAST:event_configDetailsButtonActionPerformed

    /**
     * Launches a Component Detail Dialog
     * @param evt event when 'Component Details' button is pressed
     */
    private void compDetailsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_compDetailsButtonActionPerformed
        javax.swing.JDialog.setDefaultLookAndFeelDecorated(true);
        AboutComponentDialog compDialog = new AboutComponentDialog();
        compDialog.setTitle("About Components");
        compDialog.setSize(640, 322);
        //compDialog.setModal(true);
        compDialog.setDefaultCloseOperation(javax.swing.JDialog.DISPOSE_ON_CLOSE);
        //compDialog.setLocation(200, 50);
    // Centers the Component Dialog in the middle of the About Dialog
        compDialog.setLocation(this.getBounds().x +(this.getBounds().width/2) - (compDialog.getBounds().width/2),
                            this.getBounds().y +(this.getBounds().height/2) - (compDialog.getBounds().height/2));
        //Dimension compDimension = new Dimension(640, 322);
        //compDialog.setMaximumSize(compDimension);
        compDialog.setVisible(true);

    }//GEN-LAST:event_compDetailsButtonActionPerformed

    /**
     * Closes the About Dialog Window
     * @param evt event when 'OK' button is pressed
     */
    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_okButtonActionPerformed

    /**
     * Checks the users operating system and opens a web browser accordingly
     * @param url contains the url address of the website being accessed
     */
    private void openWebBrowser(String url){
        String osName = System.getProperty("os.name");
        try {
            if (osName.startsWith("Mac OS")) {
                Class fileMgr = Class.forName("com.apple.eio.FileManager");
                Method openURL = fileMgr.getDeclaredMethod("openURL", new Class[] {String.class});
                openURL.invoke(null, new Object[] {url});
            } else if (osName.startsWith("Windows"))
                Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + url);
            else { //assume Unix or Linux
                String[] browsers = { "firefox", "opera", "mozilla", "konqueror", "epiphany", "netscape" };
                String browser = null;
                for (int count = 0; count < browsers.length && browser == null; count++)
                    if (Runtime.getRuntime().exec( new String[] {"which", browsers[count]}).waitFor() == 0)
                        browser = browsers[count];
                if (browser == null)
                    throw new Exception("Could not find web browser");
                else Runtime.getRuntime().exec(new String[] {browser, url});
            }
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, errMsg + ":\n" + e.getLocalizedMessage());
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel ButtonPanel;
    private javax.swing.JPanel ComponentIcons;
    private javax.swing.JPanel LogoAndText;
    private javax.swing.JScrollPane aboutScrollPane;
    private javax.swing.JEditorPane aboutTextEditorPane;
    private javax.swing.JButton compDetailsButton;
    private javax.swing.JButton configDetailsButton;
    private javax.swing.JLabel logoLabel;
    private javax.swing.JButton okButton;
    private javax.swing.JButton qiWorkbench;
    // End of variables declaration//GEN-END:variables

    //My variable declarations
    private static final String errMsg = "Error attempting to launch web browser";
    Properties props = null;
    Class me = null;
    InputStream in = null;
    String versionProp = null;
    String buildProp = null;
}
