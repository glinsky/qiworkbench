/*
 * QiComponentBase.java
 *
 * Created on September 25, 2007, 9:52 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.qiworkbench.workbench;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author folsw9
 */
public abstract class QiComponentBase {
    private boolean initFinished = false;
    private boolean initSuccessful = false;
    private List<Exception> initExceptions = new ArrayList();
    
    /** Creates a new instance of QiComponentBase */
    public QiComponentBase() {
    }
    
    public boolean isInitFinished() {
        return initFinished;
    }
    
    public boolean isInitSuccessful() {
        return initSuccessful;
    }
    
    public List getInitExceptions() {
        return initExceptions;
    }
    
    protected void setInitFinished(boolean initFinished) {
        this.initFinished = initFinished;
    }
    
    protected void setInitSuccessful(boolean initSuccessful) {
        this.initSuccessful = initSuccessful;
    }
    
    protected void addInitException(Exception e) {
        initExceptions.add(e);
    }
}
