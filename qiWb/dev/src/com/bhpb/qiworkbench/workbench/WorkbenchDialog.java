/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.workbench;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.bhpb.qiworkbench.QiwbPreferences;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.client.qiWbConfiguration;
import com.bhpb.qiworkbench.client.util.PreferenceUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
import com.bhpb.qiworkbench.workbench.WorkbenchAction;
import com.bhpb.qiworkbench.workbench.WorkbenchMenuDescriptor;

/**
 * WorkbenchDialog is used for dialogs needed by GUI actions. In addition it
 * is used by MessageDispatcher init to get the Tomcat location
 *
 * @author Bob Miller
 * @author Gil Hansen
 * @version 1.1
 */
public class WorkbenchDialog extends JDialog implements ActionListener {
  private static Logger logger = Logger.getLogger(WorkbenchDialog.class.getName());

  JFrame frame;
  JTextField newTomcat;
  ArrayList<String> tomcatInfo;

  boolean rememberAsDefault = false;

  public boolean getDefaultIndicator() {
    return rememberAsDefault;
  }

  /**
   * Constructor for dialog to select a Tomcat server.
   * @param f Parent frame
   */
  public WorkbenchDialog(JFrame f) {
    super(f);
    frame = f;
    setDefaultLookAndFeelDecorated(true);
  }

  /**
   * Dialog to specify the URL of a Tomcat sever.
   *
   * @return List containing 1) the URL of the selected Tomcat server and
   * 2) the location of the server (used by the services).
   * @deprecated getTomcatInfo() method moved to ProjectSelectorDialog.
   */
  public ArrayList<String> getTomcatInfo() {
    tomcatInfo = new ArrayList<String>(2);
    // get a dialog window
    setTitle("Tomcat Server Selector");
    setModal(true);
    setDefaultLookAndFeelDecorated(true);
    setLayout(new BorderLayout());                  // d contains p1(North), p2(Center), and p4(south)
    JPanel p1 = new JPanel(new BorderLayout());     // p1 contains "Select Tomcat..."(North), "where QI..." (Center),and comboBox(South)
    JPanel p2 = new JPanel();                       // p2 contains "URL"(Left) and text for new entry(Right)
    JPanel p3 = new JPanel();                       // p3 contains checkbox
    JPanel p4 = new JPanel();                       // p4 contains p3(North) and button panel(South)
    p4.setLayout(new BoxLayout(p4, BoxLayout.X_AXIS));
    p1.add(new JLabel("Select or Enter the URL of a Tomcat server from"),BorderLayout.NORTH);
    p1.add(new JLabel("which your qiWorkbench projects are accessible."),BorderLayout.CENTER);

    // Note: The initial preference list is populated with the
    // allowed Tomcat servers keep in the config.properties file.
    // Thereafter, we display the preferred servers.

    // Get user's preferences
    String userHOME = MessageDispatcher.getInstance().getUserHOME();
    QiwbPreferences userPrefs = null;
    String prefFileDir = userHOME + File.separator + QIWConstants.QIWB_WORKING_DIR;
    if (PreferenceUtils.prefFileExists(prefFileDir)) {
        // get user's preferences saved in the preference file
        try {
            userPrefs = PreferenceUtils.readPrefs(prefFileDir);
        } catch (QiwIOException qioe) {
            //TODO notify user cannot read preference file
            logger.finest(qioe.getMessage());
        }
    } else {
        // NOTE: This case can never occur because if there is no
        // preference file, one is created by the Message Dispatcher
        // for the Server Selector dialog.

        // user has no preference file; create one
        userPrefs = new QiwbPreferences();
        try {
            PreferenceUtils.writePrefs(userPrefs, prefFileDir);
        } catch (QiwIOException qioe) {
            //TODO notify user cannot write preference file
            logger.finest(qioe.getMessage());
        }
    }

    /*ArrayList<String> tomcats = userPrefs.getServerURLs();

    String tomcatChoices[] = new String[tomcats.size()];
    for (int i=0; i<tomcats.size(); i++) {
        tomcatChoices[i] = tomcats.get(i);
    }*/
    String tomcatChoices[];
    ArrayList<Map<String, String>> tomcats = qiWbConfiguration.getInstance().getRuntimeTomcatAttributes();
    if(tomcats.size() > 0) {
        tomcatChoices = new String[tomcats.size()];
        for (int i=0; i<tomcats.size(); i++) {
            tomcatChoices[i] = tomcats.get(i).get("tomcaturl");
        }
    } else {
        ArrayList<String> tomcatPref = userPrefs.getServerURLs();
        tomcatChoices = new String[tomcatPref.size()];
        for (int i=0; i<tomcatPref.size(); i++) {
            tomcatChoices[i] = tomcatPref.get(i);
        }
    }

    JComboBox tomcatBox = new JComboBox(tomcatChoices);
    tomcatBox.setSelectedIndex(0);
    tomcatBox.setSelectedItem(tomcatChoices[0]);
    tomcatBox.setMaximumRowCount(5);
    tomcatBox.addActionListener(this);
    tomcatBox.setActionCommand("tomcatBox");
    p1.add(tomcatBox, BorderLayout.SOUTH);
    p2.add(new JLabel("URL: "));
    newTomcat = new JTextField("", 20);
    p2.add(newTomcat);

    // add the check box to remember server selection as the default
    JCheckBox remember = new JCheckBox("remember as default");
    remember.addActionListener(this);
    remember.setActionCommand("remember");
    p3.add(remember);

    JButton okButton = new JButton("OK");
    okButton.setPreferredSize(new Dimension(60, 30));
    okButton.setMaximumSize(new Dimension(60, 30));
    okButton.addActionListener(this);
    okButton.setActionCommand("OK");
    p4.add(p3, BorderLayout.NORTH);
    p4.add(okButton);
    add(p1, BorderLayout.NORTH);
    add(p2, BorderLayout.CENTER);
    add(p4, BorderLayout.SOUTH);
    tomcatInfo.add(0, (String)tomcatBox.getSelectedItem());
    tomcatInfo.add(1, QIWConstants.LOCAL_PREF);
    pack();
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    setLocation(400,400);
    setVisible(true);
    String hostname = System.getenv("HOSTNAME");    // Linux/Unix/Mac
    //TODO: hostname null on a Mac. Check URL for Mac
    if(hostname == null) hostname = System.getenv("COMPUTERNAME");  //PC
    if(WorkbenchData.verbose > 0)
      logger.info("Running on " + hostname);
    if(tomcatInfo.get(0).contains("localhost") || tomcatInfo.get(0).contains(hostname))
      tomcatInfo.set(1,QIWConstants.LOCAL_PREF);
    else
      tomcatInfo.set(1,QIWConstants.REMOTE_PREF);
    return tomcatInfo;
  }

  /**
   * Get user choice for Tomcat location from ComboBox or text entry.
   * Get the indicator to remember as the default Tomcat server.
   */
  public void actionPerformed(ActionEvent e) {
    String cmd = e.getActionCommand();
    if(cmd.equals("tomcatBox")) {
      JComboBox cb = (JComboBox)e.getSource();
      tomcatInfo.set(0,(String)cb.getSelectedItem());
    } else
    if(cmd.equals("OK")) {
      if(!newTomcat.getText().equals(""))
        tomcatInfo.set(0,newTomcat.getText());
      dispose();
    } else
    if (cmd.equals("remember")) {
      JCheckBox cb = (JCheckBox)e.getSource();
      rememberAsDefault = cb.isSelected() ? true : false;
    }
  }

  /**
   * Show dialog for how user wants to dispose of current desktop before starting a new one.
   * @param title String dialog title
   */
  public void saveQuitCloseDesktop(String title) {
    JOptionPane optionPane = new JOptionPane(
      "Save, Close (Save & Quit), or Quit " + WorkbenchManager.getInstance().getProject() + " Before Opening New Desktop, or Cancel Save?");
    String[] options = {"Save","Close","Quit","Cancel"};
    optionPane.setOptions(options);
    JDialog dialog = optionPane.createDialog(frame,title);
    dialog.setVisible(true);
    String response = optionPane.getValue().toString();
    if(response.equals(null) || response.equals("Cancel")) {
      return;
    }
    else if(response.equals("Save")) {
      int index = WorkbenchMenuDescriptor.getMenuDescriptorIndex("Desktop.Restore");
      if(index == -1) {
        WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                               "Error occured while saving desktop");

        return;
      }
      WorkbenchMenuDescriptor d = (WorkbenchMenuDescriptor)WorkbenchData.menuDescriptors.get(index);
      /*WorkbenchAction.clearMenu(d.getParentMenu(),d.getMenu(),WorkbenchData.savedDesktops);
      WorkbenchAction.updateContents(WorkbenchData.savedDesktops,WorkbenchData.project);
      WorkbenchAction.loadMenu(d.getParentMenu(),d.getMenu(),WorkbenchData.savedDesktops,
                                    d.getActionCommand(),d.getActionListener());*/
      WorkbenchAction.saveDesktop();
      WorkbenchAction.makeNewDesktop();
    }
    else if(response.equals("Close")) {
      WorkbenchAction.saveDesktop();
      WorkbenchAction.makeNewDesktop();
      WorkbenchAction.quitDesktop();
    }
    else if(response.equals("Quit")) {
      WorkbenchAction.makeNewDesktop();
      WorkbenchAction.quitDesktop();
    }
  }

  /**
   * renameDesktop shows dialog to allow user to rename current desktop
   *
  */
  public void renameDesktop() {
    String project = (String)JOptionPane.showInputDialog(frame,"Enter New Desktop Name");
    if(!project.equals(null))
      WorkbenchAction.renameDesktop(project);
  }

  /*
   * renameComponent shows dialog to allow user to rename an active plugin or viewer instance
   * @param String componentType "Plugin" or "Viewer"
   * @param String name  Name of instance to be renamed
   * @return String new name
   */
  public String renameComponent(String componentType, String name) {
    return (String)JOptionPane.showInputDialog(frame,"Enter New " + componentType + " Name","");
  }

  /*
   * renameError is used if name returned in renameComponent alreaday exists
   * @param String componentType "Plugin" or "Viewer"
   * @param String name  Name of instance to be renamed
   * @return String new name
   */
  public String renameError(String componentType, String name, String errorName) {
    return (String)JOptionPane.showInputDialog(frame,errorName + " Already Exists, Please use Another Name","");
  }

   /**
   * Show dialog to allow user to register a new plugin. For admin use only.
   *
  */
  public void registerPlugin() {
    String plugin = (String)JOptionPane.showInputDialog(frame,"Enter Name of New Plugin");
    WorkbenchAction.registerPlugin(plugin);
  }

  /**
   * Show dialog to allow user to update an exisiting plugin, for admin use only.
   * @param plugin component to update
   */
  public void updatePlugin(String plugin) {
    JOptionPane optionPane = new JOptionPane("Update " + plugin + "?");
    String[] options = {"OK","Cancel"};
    optionPane.setOptions(options);
    JDialog dialog = optionPane.createDialog(frame,"Update Plugin");
    dialog.setVisible(true);
    String response = optionPane.getValue().toString();
    if(response.equals("OK"))
      WorkbenchAction.updatePlugin(plugin);
  }

}
