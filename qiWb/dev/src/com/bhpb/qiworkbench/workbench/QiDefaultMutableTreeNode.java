/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.workbench;

import com.bhpb.qiworkbench.api.IComponentDescriptor;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;

public class QiDefaultMutableTreeNode extends DefaultMutableTreeNode implements Comparable{
    IComponentDescriptor desc;
    private List<String> nodePopupUITexts;
    boolean isClosed = false;
    public QiDefaultMutableTreeNode(Object userObject,IComponentDescriptor desc){
        super(userObject);
        this.desc = desc;
    }
    public void clearNodePopupUITexts(){
        nodePopupUITexts.clear();
    }
    public void setNodePopupUITexts(List<String> list){
        nodePopupUITexts = list;
    }
    public List<String> getNodePopupUITexts(){
        return nodePopupUITexts;
    }
    public int compareTo(Object node){
        return desc.compareTo(((QiDefaultMutableTreeNode)node).desc);
    }

    public void setClosed(boolean closed){
        isClosed = closed;
    }

    public boolean isClosedComponent(){
        if(this.isLeaf() && isClosed)
            return true;
        return false;
    }
    public IComponentDescriptor getComponentDescriptor(){
        return desc;
    }
}
