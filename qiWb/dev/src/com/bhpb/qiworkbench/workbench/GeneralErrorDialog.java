/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.workbench;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 * Dialog to display a general error message.
 *
 * @author  Marcus Vaal
 * @version 1.0
 */
public class GeneralErrorDialog extends javax.swing.JDialog {

    /**
     * Creates an instance of an Error Dialog
     */
    public GeneralErrorDialog() {
    	this.ErrorMessage = "An error has occurred.";
        initComponents();
    }
    /**
     * Creates an instance of an Error Dialog
     * @param paramErrorMessage error message to be displayed
     */
    public GeneralErrorDialog(String paramErrorMessage) {
        this.ErrorMessage = paramErrorMessage;
        initComponents();
    }
    
    /**
     * Initializes the components
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        errorTextPanel = new javax.swing.JPanel();
        errorScrollPane = new javax.swing.JScrollPane();
        errorTextPane = new javax.swing.JTextPane();
        buttonPanel = new javax.swing.JPanel();
        abortButton = new javax.swing.JButton();
        okButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        errorTextPane.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        errorTextPane.setEditable(false);
        errorTextPane.setFont(new java.awt.Font("Arial", 0, 10));
        StyledDocument doc = errorTextPane.getStyledDocument();
        MutableAttributeSet standard = new SimpleAttributeSet();
        StyleConstants.setFontFamily(standard, "Arial");
        StyleConstants.setFontSize(standard, 12);
        StyleConstants.setAlignment(standard, StyleConstants.ALIGN_CENTER);
        doc.setParagraphAttributes(0, 0, standard, true);
        errorTextPane.setText(ErrorMessage);
        errorScrollPane.setViewportView(errorTextPane);

        org.jdesktop.layout.GroupLayout errorTextPanelLayout = new org.jdesktop.layout.GroupLayout(errorTextPanel);
        errorTextPanel.setLayout(errorTextPanelLayout);
        errorTextPanelLayout.setHorizontalGroup(
            errorTextPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(errorTextPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(errorScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE)
                .addContainerGap())
        );
        errorTextPanelLayout.setVerticalGroup(
            errorTextPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(errorTextPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(errorScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE))
        );

        abortButton.setText("Abort");
	abortButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                abortButtonActionPerformed(evt);
            }
        });

        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout buttonPanelLayout = new org.jdesktop.layout.GroupLayout(buttonPanel);
        buttonPanel.setLayout(buttonPanelLayout);
        buttonPanelLayout.setHorizontalGroup(
            buttonPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, buttonPanelLayout.createSequentialGroup()
                .addContainerGap(149, Short.MAX_VALUE)
                .add(abortButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(okButton)
                .addContainerGap())
        );
        buttonPanelLayout.setVerticalGroup(
            buttonPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(buttonPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                .add(abortButton)
                .add(okButton))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(errorTextPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(buttonPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(errorTextPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(buttonPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Closes the Error Dialog Window
     * @param evt event when 'OK' button is pressed
     */
    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_okButtonActionPerformed

    /**
     * Closes the workbench
     * @param evt event when 'Abort' button is pressed
     */
    private void abortButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
	this.setVisible(false);
        this.dispose();
        System.exit(0);
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton abortButton;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JScrollPane errorScrollPane;
    private javax.swing.JTextPane errorTextPane;
    private javax.swing.JPanel errorTextPanel;
    private javax.swing.JButton okButton;
    // End of variables declaration//GEN-END:variables

    //My Variables
    private String ErrorMessage = null;
}
