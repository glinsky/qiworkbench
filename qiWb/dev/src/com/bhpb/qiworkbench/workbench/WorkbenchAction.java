/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench.workbench;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.ClientConstants;
import com.bhpb.qiworkbench.client.SocketManager;
import com.bhpb.qiworkbench.client.StatsManager;
import com.bhpb.qiworkbench.compAPI.IconResource;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Handle workbench GUI actions..
 * @author ahmilb
 *
 */
public class WorkbenchAction {

    public static Logger logger = Logger.getLogger(WorkbenchAction.class.getName());
    private static WorkbenchGUI workbenchGui = (WorkbenchGUI) WorkbenchManager.getInstance().getWorkbenchGUI();
    private static MessagingManager guiMessagingMgr = workbenchGui.getMsgMgr();
    private static IComponentDescriptor stMgrDesc = guiMessagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);

    /**
     * addComponent updates the GUI (menus and tree) with new component info
     * @param desc type of component, plugin, viewer, etc
     * @param parentDisplayName String display name of component's parent, e.g. "Amplitude Extraction"
     */
    public static void addComponent(IComponentDescriptor desc, String parentDisplayName) {
        String displayName = null;
        if (desc.getPreferredDisplayName().trim().length() > 0) {
            displayName = desc.getPreferredDisplayName().trim();
        } else {
            displayName = desc.getDisplayName();
        }
        String compKind = desc.getComponentKind();
        if (compKind.equals(QIWConstants.PLUGIN_AGENT_COMP) ||
                compKind.equals(QIWConstants.PLUGIN_PROJMGR_COMP)) {
            addComponentToGUI(parentDisplayName, displayName, desc, WorkbenchData.PLUGIN_INSTANCE_NODE_TYPE);
        } else if (compKind.equals(QIWConstants.VIEWER_AGENT_COMP)) {
            addComponentToGUI(parentDisplayName, displayName, desc, WorkbenchData.VIEWER_INSTANCE_NODE_TYPE);
        }
    }

    /**
     * removeComponent removes component from GUI
     */
    public static void removeComponent(IComponentDescriptor desc) {
        // update GUI
        String parentDisplayName = removeComponentFromGUI(desc);
        if (WorkbenchData.verbose > 0) {
            logger.info("Removing " + desc.getDisplayName() + " parent " + parentDisplayName);
        }
    }

    /**
     * removeComponent removes component from GUI
     * @param displayName String name of component to remove
     */
    public static void removeComponent(String componentKind, String displayName) {
        if (componentKind.equals(QIWConstants.PLUGIN_AGENT_COMP) ||
                componentKind.equals(QIWConstants.PLUGIN_PROJMGR_COMP)) {
            // update GUI
            String parentDisplayName = removeComponentFromGUI("Plugin", displayName);
            if (WorkbenchData.verbose > 0) {
                logger.info("Removing " + displayName + " from GUI");
            }
        } else if (componentKind.equals(QIWConstants.VIEWER_AGENT_COMP)) {
            // update GUI
            String parentDisplayName = removeComponentFromGUI("Viewer", displayName);
            if (WorkbenchData.verbose > 0) {
                logger.info("Removing " + displayName + " from GUI");
            }
        } else if (componentKind.equals(QIWConstants.LOCAL_SERVICE_COMP)) {
        } else if (componentKind.equals(QIWConstants.REMOTE_SERVICE_COMP)) {
        }
    }

    /**
     * deleteComponent sends request to MD to deactivate a component. Have response routed to WMMgr processMsg method
     */
    public static void deleteComponent(String type, String instance) {
        String command;
        String msgID = "";
        if (type.equals("Plugin")) {
            IComponentDescriptor desc = guiMessagingMgr.getComponentDescFromDisplayName(instance);
            msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.REMOVE_PLUGIN_CMD, desc);
        } else if (type.equals("Viewer")) {
            IComponentDescriptor desc = guiMessagingMgr.getComponentDescFromDisplayName(instance);
            msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.REMOVE_VIEWER_AGENT_CMD,
                    desc);
        }
        logger.info("Sending de-activate " + type + " " + instance);
    }

    /**
     * quitComponent sends request to MD to deactivate a component. Have response routed to WMMgr processMsg method
     */
    public static void quitComponent(IComponentDescriptor desc) {
        String command;
        String msgID = "";
        if (!WorkbenchStateManager.getInstance().componentExitsInSavedSet(desc)) {
            String compKind = desc.getComponentKind();
            if (compKind.equals(QIWConstants.PLUGIN_AGENT_COMP) ||
                    compKind.equals(QIWConstants.PLUGIN_PROJMGR_COMP)) {
                msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.DEACTIVATE_PLUGIN_CMD, desc);
            } else if (compKind.equals(QIWConstants.VIEWER_AGENT_COMP)) {
                msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.DEACTIVATE_VIEWER_AGENT_CMD,
                        desc);
            }
        } else {
            msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CLOSE_COMPONENT_GUI_CMD, desc);
        }
    }

    /**
     * quitComponent sends request to MD to deactivate a component. Have response routed to WMMgr processMsg method
     */
    public static void quitComponent(String type, String instance) {
        String command;
        String msgID = "";
        if (type.equals("Plugin")) {
            IComponentDescriptor desc = guiMessagingMgr.getComponentDescFromDisplayName(instance);
            msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.DEACTIVATE_PLUGIN_CMD, desc);
        } else if (type.equals("Viewer")) {
            IComponentDescriptor desc = guiMessagingMgr.getComponentDescFromDisplayName(instance);
            msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.DEACTIVATE_VIEWER_AGENT_CMD,
                    desc);
        }
        logger.info("Sending de-activate " + type + " " + instance);
    }

    public static void openComponentUI(IComponentDescriptor desc) {
        guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.OPEN_COMPONENT_GUI_CMD, stMgrDesc, QIWConstants.COMP_DESC_TYPE, desc);
        logger.info("Sending open component UI" + desc.getDisplayName());
        ArrayList<String> list = new ArrayList<String>();
        for (String s : workbenchGui.getComponentMenuItemTexts()) {
            list.add(s);
        }
        WorkbenchMenuDescriptor mDesc = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_QUIT_CMD);
        addMenuItemToMenu(mDesc, desc);
        mDesc = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_SAVE_CMD);
        addMenuItemToMenu(mDesc, desc);
        mDesc = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_SAVEAS_CMD);
        addMenuItemToMenu(mDesc, desc);
        mDesc = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_SAVE_QUIT_CMD);
        addMenuItemToMenu(mDesc, desc);
        //mDesc = workbenchGui.getMenuDescriptor("Component.Delete");
        //addMenuItemToMenu(mDesc,desc);
        mDesc = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_RENAME_CMD);
        addMenuItemToMenu(mDesc, desc);
        mDesc = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_OPEN_CMD);
        removeMenuItemFromMenu(mDesc, desc);
        list.remove("Open");

        updateTreeMenuItem(desc, list);
        if (mDesc.getContents().isEmpty()) {
            mDesc.getMenu().setEnabled(false);
        }
    }

    public static void updateTreeMenuItem(IComponentDescriptor desc, List<String> list) {
        WorkbenchTreeDescriptor t = WorkbenchTreeManager.getTreeDescriptorByCID(desc);
        if (t != null) {
            DefaultMutableTreeNode node = t.getNode();
            if (node instanceof QiDefaultMutableTreeNode) {
                ((QiDefaultMutableTreeNode) node).setNodePopupUITexts(list);
                ((QiDefaultMutableTreeNode) node).setClosed(false);

                ImageIcon icon = IconResource.getInstance().getImageIcon(IconResource.CLOSE_ICON);
                if (icon != null) {
                    QiTreeCellRenderer renderer = new QiTreeCellRenderer(icon);
                    //renderer.setLeafIcon(icon);
                    WorkbenchTreeManager.getComponentTree().setCellRenderer(renderer);
                }
            }
        }
    }

    /**
     * closeComponent sends request to MD to close a component. Have response routed to WMMgr processMsg method
     */
    public static void closeComponentGUI(final IComponentDescriptor desc) {
        //If an unsaved component, remove it from the workbench unless it is
        //the sole Project Manager in which case, give the user a warning.
        if (!WorkbenchStateManager.getInstance().componentExitsInSavedSet(desc)) {
            if (desc.getComponentKind().equals(QIWConstants.PLUGIN_PROJMGR_COMP) && WorkbenchManager.getInstance().getNumProjMgrs() == 1) {
                JOptionPane.showMessageDialog(workbenchGui, "Cannot quit sole Project Manager.", "Quit Warning", JOptionPane.WARNING_MESSAGE);
            } else {
                guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.REMOVE_COMPONENT_CMD, WorkbenchManager.getInstance().getDesc(), QIWConstants.COMP_DESC_TYPE, desc);
                WorkbenchStateManager.getInstance().removeNodeFromCurrentNodeMap(desc.getPreferredDisplayName());
            }
        } else {
            if (WorkbenchStateManager.getInstance().getComponentVisibleState(desc.getPreferredDisplayName()).equals("open")) {
                final String msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CLOSE_COMPONENT_GUI_CMD, desc);
                logger.info("Sending close component " + desc.getPreferredDisplayName());

                Runnable runnable = new Runnable() {

                    public void run() {
                        IQiWorkbenchMsg response = guiMessagingMgr.getMatchingResponseWait(msgID, 10000);
                        Component comp = null;
                        if (response != null && !response.isAbnormalStatus()) {
                            comp = (Component) response.getContent();
                            if (comp != null) {
                                WorkbenchStateManager.getInstance().setComponentVisibleStateClosed(desc.getPreferredDisplayName(), comp);
                            }
                        } else {
                            logger.warning("Messaging timed out or with abnormal status for getting the location and size of the component GUI.");
                        }
                    }
                };
                new Thread(runnable).start();
            }
            Runnable updateAComponent = new Runnable() {

                public void run() {
                    ArrayList<String> list = new ArrayList<String>();
                    for (String s : workbenchGui.getComponentMenuItemTexts()) {
                        list.add(s);
                    }
                    WorkbenchMenuDescriptor mDesc = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_OPEN_CMD);
                    addMenuItemToMenu(mDesc, desc);
                    mDesc = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_DELETE_CMD);
                    if (mDesc.getMenu().getItemCount() == 0) {
                        addMenuItemToMenu(mDesc, desc);
                    }
                    mDesc = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_SAVE_CMD);
                    removeMenuItemFromMenu(mDesc, desc);
                    list.remove("Save");
                    mDesc = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_SAVEAS_CMD);
                    removeMenuItemFromMenu(mDesc, desc);
                    list.remove("Save As");

                    mDesc = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_SAVE_QUIT_CMD);
                    removeMenuItemFromMenu(mDesc, desc);
                    list.remove("Save, Quit");

                    mDesc = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_QUIT_CMD);
                    removeMenuItemFromMenu(mDesc, desc);
                    list.remove("Quit");

                    mDesc = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_RENAME_CMD);
                    removeMenuItemFromMenu(mDesc, desc);
                    list.remove("Rename");
                    //list.add("Open");
                    //list.add("Delete");
                    WorkbenchTreeDescriptor t = WorkbenchTreeManager.getTreeDescriptorByCID(desc);
                    if (t != null) {
                        DefaultMutableTreeNode child = t.getNode();
                        if (child instanceof QiDefaultMutableTreeNode) {
                            ((QiDefaultMutableTreeNode) child).setClosed(true);
                            ((QiDefaultMutableTreeNode) child).setNodePopupUITexts(list);
                            ImageIcon icon = IconResource.getInstance().getImageIcon(IconResource.CLOSE_ICON);
                            if (icon != null) {
                                QiTreeCellRenderer renderer = new QiTreeCellRenderer(icon);
                                //renderer.setLeafIcon(icon);
                                WorkbenchTreeManager.getComponentTree().setCellRenderer(renderer);
                            }
                        }
                    }
                }
            };
            SwingUtilities.invokeLater(updateAComponent);
        }
    }

    /**
     * closeComponent sends request to MD to close a component. Have response routed to WMMgr processMsg method
     * @deprecated no longer in use
     */
    public static void restoreClosedComponentGUI(IComponentDescriptor desc) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("Open");
        list.add("Delete");
        WorkbenchMenuDescriptor mDesc = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_OPEN_CMD);
        addMenuItemToMenu(mDesc, desc);
        mDesc = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_DELETE_CMD);
        addMenuItemToMenu(mDesc, desc);
        WorkbenchTreeDescriptor t = WorkbenchTreeManager.getTreeDescriptorByCID(desc);
        if (t != null) {
            DefaultMutableTreeNode child = t.getNode();
            if (child instanceof QiDefaultMutableTreeNode) {
                ((QiDefaultMutableTreeNode) child).setClosed(true);
                ((QiDefaultMutableTreeNode) child).setNodePopupUITexts(list);
                ImageIcon icon = IconResource.getInstance().getImageIcon(IconResource.CLOSE_ICON);
                if (icon != null) {
                    QiTreeCellRenderer renderer = new QiTreeCellRenderer(icon);
                    //renderer.setLeafIcon(icon);
                    WorkbenchTreeManager.getComponentTree().setCellRenderer(renderer);
                }
            }
            //TODO needs to be modified if in use again
            WorkbenchStateManager.getInstance().setComponentVisibleStateClosed(desc.getPreferredDisplayName(), null);
        }

    }

    private static void addMenuItemToMenu(WorkbenchMenuDescriptor d, IComponentDescriptor desc) {
        if (desc.getPreferredDisplayName().trim().length() > 0) {
            desc.getPreferredDisplayName().trim();
        } else {
            desc.getDisplayName();
        }
        d.getMenu().setEnabled(true);
        QiMenuItem item = loadMenu2(d.getMenu(), desc, d.getActionCommand(), d.getActionListener());
        sortMenuItem(d, item);
    }

    /**
     * deleteComponent sends request to MD to deactivate a component. Have response routed to WMMgr processMsg method
     */
    public static void deleteComponent(IComponentDescriptor desc) {
        guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.REMOVE_COMPONENT_CMD,
                WorkbenchManager.getInstance().getDesc(), QIWConstants.COMP_DESC_TYPE, desc);
        logger.info("Sending delete " + desc.getDisplayName() + " to WorkbenchManager");
    }

    /**
     * makeNewDesktop sends request to messagingMgr to forward to dispatcher to start a new instance of WBMgr.
     *
     */
    public static void makeNewDesktop() {
        // TODO send message to load a new main and start new Workbench
        logger.info("Send message to load a new main and workbench");
    }

    /**
     * restoreDesktop sends request to state manager to forward to SaveRestore service to restore a previously saved instance of desktop
     * @param project
     */
    public static void restoreDesktop(String project) {
        logger.info("Restore saved desktop for " + project + " project");
        guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.RESTORE_DESKTOP_CMD, stMgrDesc);
    }

    /**
     * saveDesktop sends request to messagingMgr to forward to State Manager to save this instance of workbench
     *
     */
    public static void saveDesktop() {
        logger.info("Saving " + WorkbenchManager.getInstance().getProject() + " Desktop");
        WorkbenchStateManager.getInstance().setSaveAsRequested(false);
        guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SAVE_DESKTOP_CMD, stMgrDesc);
    }

    /**
     * saveDesktopAs sends request to messagingMgr to forward to State Manager to save this instance of workbench as
     * a different save set if  a save set is already in session.
     */
    public static void saveDesktopAs() {
        logger.info("Performing Saving As  " + WorkbenchManager.getInstance().getProject() + " Desktop");
        WorkbenchStateManager.getInstance().setSaveAsRequested(true);
        guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SAVE_DESKTOP_CMD, stMgrDesc);
    }

    public static void saveQuitDesktop() {
        logger.info("Saving " + WorkbenchManager.getInstance().getProject() + " Desktop");
        String msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SAVE_DESKTOP_THEN_QUIT_CMD, stMgrDesc);
    //QiWorkbenchMsg response = guiMessagingMgr.getMatchingResponseWait(msgID,10000);
    }

    /**
     * Quit workbench and dispose of the Workbench Manager
     *
     */
    public static void quitDesktop() {
        int status = JOptionPane.showConfirmDialog(workbenchGui,
                "Do you want to save the current workbench before quitting?",
                "Confirm to quit", JOptionPane.YES_NO_CANCEL_OPTION);
        if (status == JOptionPane.YES_OPTION) {
            //Close all sockets available for reuse
            SocketManager.getInstance().closeAllSockets();
            saveQuitDesktop();
        } else if (status == JOptionPane.NO_OPTION) {
            WorkbenchStateManager.getInstance().logQuitting();
            //Close all sockets available for reuse
            SocketManager.getInstance().closeAllSockets();
            System.exit(0);
        } else {
            return;
        }
    }

    /**
     * @param doPrompt if true, invoke quitDesktop() otherwise call System.exit()
     *
     */
    public static void quitDesktop(boolean doPrompt) {
        if (doPrompt) {
            quitDesktop();
        } else {
            //Close all sockets available for reuse
            SocketManager.getInstance().closeAllSockets();
            WorkbenchStateManager.getInstance().logQuitting();
            System.exit(0);
        }
    }

    /**
     * renameDesktop changes all applicable data items and GUI items to new name
     * @param project String new name
     */
    // TODO How does renamimg desktop affect which project directory to use, etc ???
    public static void renameDesktop(String project) {
        logger.warning("Renaming desktop currently disabled");
    }

    public static IComponentDescriptor newViewer(String viewer) {
        // Send an activate viewer message to the message dispatcher
        logger.info("newViewer1: Send message to activate an instance of " + viewer);
        String msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.ACTIVATE_VIEWER_AGENT_CMD,
                QIWConstants.STRING_TYPE, viewer);
        IQiWorkbenchMsg response = guiMessagingMgr.getMatchingResponseWait(msgID, 10000);
        if (response == null) {
            logger.info("Response to activating viewer " + viewer + " returning null due to timed out problem");
            return null;
        } else if (response.isAbnormalStatus()) {
            WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                    "Error occured while loading viewer");
            return null;
        } else {
            //reroute response to the Workbench Manager for processing
            // make a new msg with ARRAYLIST_TYPE content
            ArrayList<Object> responseList = new ArrayList<Object>(2);
            IComponentDescriptor cd = (IComponentDescriptor) response.getContent();
            responseList.add(0, (Object) response.getContent());
            responseList.add(1, (Object) viewer);
            QiWorkbenchMsg newMsg = new QiWorkbenchMsg(response.getProducerCID(), WorkbenchData.wbMgrCID, QIWConstants.DATA_MSG,
                    QIWConstants.ACTIVATE_VIEWER_AGENT_CMD, response.getMsgID(),
                    QIWConstants.ARRAYLIST_TYPE, responseList);
            guiMessagingMgr.routeMsg(newMsg);
            return cd;
        }
    }

    /**
     * @deprecated A project descriptor is obtained from the qiComponent's PM on demand.
     * Now the CID of the PM associated with a qiComponent is conveyed when it is launched.
     */
    public static IComponentDescriptor newViewer(String viewer, QiProjectDescriptor desc) {
        // Send an activate viewer message to the message dispatcher
        logger.info("newViewer2: Send message to activate an instance of " + viewer);
        String msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.ACTIVATE_VIEWER_AGENT_CMD,
                QIWConstants.STRING_TYPE, viewer);
        IQiWorkbenchMsg response = guiMessagingMgr.getMatchingResponseWait(msgID, 10000);
        if (response == null) {
            logger.info("Response to activating viewer " + viewer + " returning null due to timed out problem");
            return null;
        } else if (response.isAbnormalStatus()) {
            WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                    "Error occured while loading viewer");
            return null;
        } else {
            //reroute response to the Workbench Manager for processing
            // make a new msg with ARRAYLIST_TYPE content
            ArrayList responseList = new ArrayList();
            IComponentDescriptor cd = (IComponentDescriptor) response.getContent();
            responseList.add(0, response.getContent());
            responseList.add(1, viewer);
            responseList.add(2, desc);
            QiWorkbenchMsg newMsg = new QiWorkbenchMsg(response.getProducerCID(), WorkbenchData.wbMgrCID, QIWConstants.DATA_MSG,
                    QIWConstants.ACTIVATE_VIEWER_AGENT_CMD, response.getMsgID(),
                    QIWConstants.ARRAYLIST_TYPE, responseList);
            guiMessagingMgr.routeMsg(newMsg);
            return cd;
        }
    }

    public static IComponentDescriptor newPlugin(String plugin) {
        // Send an activate plugin message to the message dispatcher
        logger.info("newPlugin1: Send message to activate an instance of " + plugin);
        String msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.ACTIVATE_PLUGIN_CMD,
                QIWConstants.STRING_TYPE, plugin, true);
        IQiWorkbenchMsg response = guiMessagingMgr.getMatchingResponseWait(msgID);
        if (response == null) {
            logger.info("Response to activating plugin " + plugin + " returning null due to timed out problem");
            return null;
        } else if (response.isAbnormalStatus()) {
            WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                    "Error occured while loading plugin");
            return null;
        } else {
            //reroute response to the Workbench Manager for processing
            // make a new msg with ARRAYLIST_TYPE content
            ArrayList<Object> responseList = new ArrayList<Object>(2);
            IComponentDescriptor cd = (IComponentDescriptor) response.getContent();
            responseList.add(0, (Object) response.getContent());
            responseList.add(1, (Object) plugin);
            QiWorkbenchMsg newMsg = new QiWorkbenchMsg(response.getProducerCID(), WorkbenchData.wbMgrCID, QIWConstants.DATA_MSG,
                    QIWConstants.ACTIVATE_PLUGIN_CMD, response.getMsgID(),
                    QIWConstants.ARRAYLIST_TYPE, responseList);
            guiMessagingMgr.routeMsg(newMsg);

            return cd;
        }
    }

    /**
     * @deprecated A project descriptor is obtained from the qiComponent's PM on demand.
     * Now the CID of the PM associated with a qiComponent is conveyed when it is launched.
     */
    public static IComponentDescriptor newPlugin(String plugin, QiProjectDescriptor qiProjDesc) {
        // Send an activate plugin message to the message dispatcher
        logger.info("newPlugin2: Send message to activate an instance of " + plugin);
        String msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.ACTIVATE_PLUGIN_CMD,
                QIWConstants.STRING_TYPE, plugin);
        IQiWorkbenchMsg response = guiMessagingMgr.getMatchingResponseWait(msgID, 10000);
        if (response == null) {
            logger.info("Response to activating plugin " + plugin + " returning null due to timed out problem");
            return null;
        } else if (response.isAbnormalStatus()) {
            WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                    "Error occured while loading plugin");
            return null;
        } else {
            //reroute response to the Workbench Manager for processing
            // make a new msg with ARRAYLIST_TYPE content
            ArrayList<Object> responseList = new ArrayList<Object>(3);
            IComponentDescriptor cd = (IComponentDescriptor) response.getContent();
            responseList.add(0, response.getContent());
            responseList.add(1, plugin);
            responseList.add(2, qiProjDesc);
            QiWorkbenchMsg newMsg = new QiWorkbenchMsg(response.getProducerCID(), WorkbenchData.wbMgrCID, QIWConstants.DATA_MSG,
                    QIWConstants.ACTIVATE_PLUGIN_CMD, response.getMsgID(),
                    QIWConstants.ARRAYLIST_TYPE, responseList);
            guiMessagingMgr.routeMsg(newMsg);
            return cd;

        }
    }

    public static void checkComponentTrusted(String displayName) {
        String msgID = WorkbenchManager.getInstance().getMessagingManager().
                sendRequest(QIWConstants.CMD_MSG, QIWConstants.CHECK_COMPONENT_SECURITY_CMD,
                QIWConstants.STRING_TYPE, displayName);
    }
    
    public static IComponentDescriptor newComponent(String displayName) {
        String compType = WorkbenchManager.getInstance().getRegisteredComponentTypeByDisplayName(displayName);
        IComponentDescriptor cd = null;
        if (compType != null) {
            if (compType.equals(QIWConstants.COMPONENT_PLUGIN_TYPE)) {
                cd = newPlugin(displayName);
            } else if (compType.equals(QIWConstants.COMPONENT_VIEWER_TYPE)) {
                cd = newViewer(displayName);
            }
            if (!WorkbenchStateManager.getInstance().getCurrentNodeMap().containsKey(cd.getPreferredDisplayName())) {
                DecoratedNode node = new DecoratedNode(null, cd);
                node.setComponentClosed(false);
                WorkbenchStateManager.getInstance().setNodeToCurrentNodeMap(cd.getPreferredDisplayName(), node);
            }
        }
        return cd;
    }

    /**
     * @deprecated A project descriptor is obtained from the qiComponent's PM on demand.
     * Now the CID of the PM associated with a qiComponent is conveyed when it is launched.
     */
    public static IComponentDescriptor newComponent(String displayName, QiProjectDescriptor projDesc) {
        String compType = WorkbenchManager.getInstance().getRegisteredComponentTypeByDisplayName(displayName);
        IComponentDescriptor cd = null;
        if (compType != null) {
            if (compType.equals(QIWConstants.COMPONENT_PLUGIN_TYPE)) {
                cd = newPlugin(displayName, projDesc);
            } else if (compType.equals(QIWConstants.COMPONENT_VIEWER_TYPE)) {
                cd = newViewer(displayName, projDesc);
            }
            if (!WorkbenchStateManager.getInstance().getCurrentNodeMap().containsKey(cd.getPreferredDisplayName())) {
                DecoratedNode node = new DecoratedNode(null, cd);
                node.setComponentClosed(false);
                WorkbenchStateManager.getInstance().setNodeToCurrentNodeMap(cd.getPreferredDisplayName(), node);
            }
        }
        return cd;
    }

    public static void restorePlugin(String plugin, Node node) {
        // Send an activate plugin message to the message dispatcher
        logger.info("restorePlugin: Send message to activate an instance of " + plugin);
        String msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.ACTIVATE_PLUGIN_CMD,
                QIWConstants.STRING_TYPE, plugin);
        IQiWorkbenchMsg response = guiMessagingMgr.getMatchingResponseWait(msgID, 10000);
        if (response.isAbnormalStatus()) {
            WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                    "Error occured while restoring plugin");
            return;
        } else {
            //reroute response to the messaging manager for processing
            // make a new msg with ARRAYLIST_TYPE content
            ArrayList<Object> responseList = new ArrayList<Object>();
            responseList.add(0, (Object) response.getContent());
            responseList.add(1, (Object) plugin);
            responseList.add(2, node);

            QiWorkbenchMsg newMsg = new QiWorkbenchMsg(response.getProducerCID(), WorkbenchData.wbMgrCID, QIWConstants.DATA_MSG,
                    QIWConstants.ACTIVATE_PLUGIN_CMD, response.getMsgID(),
                    QIWConstants.ARRAYLIST_TYPE, responseList);
            guiMessagingMgr.routeMsg(newMsg);
        }
    }

    public static void restorePlugin() {
        String msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.RESTORE_COMP_CMD, stMgrDesc);
    }

    public static void savePlugin(String plugin) {
        //get state manager who will handle the state serialization
        //send a message to the component and request for state information returned as xml string
        String msgID = WorkbenchManager.getInstance().getMsgMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.SAVE_COMP_CMD, stMgrDesc, QIWConstants.STRING_TYPE, plugin);
    }

    public static void renameComponent1(IComponentDescriptor cd, String newName) {
        String oldName = cd.getPreferredDisplayName();
        if (cd != null) {
            ArrayList list = new ArrayList();
            //the 1st element is component descriptor which is to be renamed
            list.add(cd);
            //the 2nd element is the new name
            list.add(newName);
            String id = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SET_PREFERRED_DISPLAY_NAME_CMD, stMgrDesc,
                    QIWConstants.ARRAYLIST_TYPE, list);
            //QiWorkbenchMsg res = guiMessagingMgr.getMatchingResponseWait(id, 10000);
            //if(res != null && !res.isAbnormalStatus()){
            updateMenuItems(cd, newName);
            updateTreeNode(cd, newName);
        }
    }

    public static void saveQuitComponent(IComponentDescriptor cd) {
        //record usage event, quitting the qiWorkbench
        String event = StatsManager.getInstance().createStatsEvent(cd.getDisplayName(), ClientConstants.QUIT_ACTION);
        StatsManager.getInstance().logStatsEvent(event);

        WorkbenchManager.getInstance().getMsgMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.SAVE_COMP_THEN_QUIT_CMD, stMgrDesc, QIWConstants.COMP_DESC_TYPE, cd);
    }

    private static void updateMenuItem(WorkbenchMenuDescriptor d, IComponentDescriptor cd, String newName) {
        if (d == null || cd == null) {
            return;
        }
        String screenName = cd.getDisplayName();
        int idx = screenName.indexOf("#");
        if (idx != -1) {
            screenName = screenName.substring(0, idx);
        }
        ArrayList<JMenuItem> list = d.getContents();
        for (JMenuItem it : list) {
            if (it instanceof QiMenuItem) {
                QiMenuItem it2 = (QiMenuItem) it;
                if (it2.getComponentDescriptor().getCID().equals(cd.getCID())) {
                    it2.setText(screenName + ": " + newName);
                    break;
                }
            }
        }
        d.setMenuItems(list);
        sortMenuItem(d);
    }

    private static void updateTreeNode(IComponentDescriptor cd, String newName) {
        WorkbenchTreeDescriptor t = WorkbenchTreeManager.getTreeDescriptorByCID(cd);
        t.setDisplayName(newName);
        t.setScreenName("-" + newName);
        if (t != null) {
            DefaultMutableTreeNode child = t.getNode();
            DefaultMutableTreeNode parent = (DefaultMutableTreeNode) child.getParent();

            String parentDisplayName = (String) parent.getUserObject();
            //parentDisplayName = parentDisplayName.substring(parentDisplayName.indexOf(":")+2);
            WorkbenchTreeDescriptor pDesc = WorkbenchTreeManager.getTreeDescriptor(parentDisplayName);
            List<DefaultMutableTreeNode> children = pDesc.getChildren();

            for (DefaultMutableTreeNode n : children) {
                if (n instanceof QiDefaultMutableTreeNode) {
                    if (((QiDefaultMutableTreeNode) n).getComponentDescriptor().getCID().equals(cd.getCID())) {
                        n.setUserObject("-" + newName);
                        break;
                    }
                }
            }
            ArrayList<QiDefaultMutableTreeNode> list = new ArrayList<QiDefaultMutableTreeNode>();
            for (DefaultMutableTreeNode n : children) {
                if (n instanceof QiDefaultMutableTreeNode) {
                    list.add((QiDefaultMutableTreeNode) n);
                }
            }

            Collections.sort(list);
            for (DefaultMutableTreeNode n : list) {
                parent.remove((MutableTreeNode) n);
            }
            for (int i = 0; i < list.size(); i++) {
                WorkbenchTreeManager.getComponentTreeModel().insertNodeInto((DefaultMutableTreeNode) list.get(i), parent, i);
                WorkbenchTreeManager.getComponentTreeModel().nodeStructureChanged(parent);
            //parent.insert((MutableTreeNode)list.get(i),i);
            }
        }
    }

    private static void updateMenuItems(IComponentDescriptor cd, String newName) {
        WorkbenchGUI gui = (WorkbenchGUI) WorkbenchManager.getInstance().getWorkbenchGUI();
        WorkbenchMenuDescriptor d;
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_OPEN_CMD);
        updateMenuItem(d, cd, newName);
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_SAVE_CMD);
        updateMenuItem(d, cd, newName);

        // add displayName to componentType.SaveAs menu
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_SAVEAS_CMD);
        updateMenuItem(d, cd, newName);

        // add displayName to componentType.Save.Quit menu
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_SAVE_QUIT_CMD);
        updateMenuItem(d, cd, newName);

        // add displayName to componentType.Quit menu
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_QUIT_CMD);
        updateMenuItem(d, cd, newName);

        // add displayName to componentType.Quit menu
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_DELETE_CMD);
        updateMenuItem(d, cd, newName);

        // add displayName to componentType.Rename menu
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_RENAME_CMD);
        updateMenuItem(d, cd, newName);
    }

    public static void saveComponent(String displayName) {
        //  get state manager who will handle the state serialization
        //send a message to the component and request for state information returned as xml string
        String msgID = WorkbenchManager.getInstance().getMsgMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.SAVE_COMP_CMD, stMgrDesc, QIWConstants.STRING_TYPE, displayName);
    }

    public static void saveComponent(IComponentDescriptor desc) {
        if (desc == null) {
            return;
        //  get state manager who will handle the state serialization
        //send a message to the component and request for state information returned as xml string
        }
        WorkbenchManager.getInstance().getMsgMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.SAVE_COMP_CMD, stMgrDesc, QIWConstants.COMP_DESC_TYPE, desc);
    }

    //handle menu and tree menu item change when a component is saved from a new component
    public static void updateUIAfterSave(IComponentDescriptor cd) {
        if (cd == null) {
            return;
        }
        addMenuItemToUI(QIWMenuConstants.MENU_COMPONENT_DELETE_CMD, cd);
        String[] sArr = workbenchGui.getComponentMenuItemTexts();
        List<String> list = Arrays.asList(sArr);
        List<String> list2 = new ArrayList<String>();
        for (String s : list) {
            if (!s.equals("Open")) {
                list2.add(s);
            }
        }
        //list..remove("Open");
        updateTreeMenuItem(cd, list2);
    }

    public static void saveComponentAsClone(IComponentDescriptor desc) {
        //  get state manager who will handle the state serialization
        //send a message to the component and request for state information returned as xml string
        String msgID = WorkbenchManager.getInstance().getMsgMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.SAVE_COMP_AS_CLONE_CMD, stMgrDesc, QIWConstants.COMP_DESC_TYPE, desc);
    }

    public static void saveQuitComponent(String componentType, String componentDisplayName) {
        //record usage event, quitting the qiWorkbench
        String event = StatsManager.getInstance().createStatsEvent(componentDisplayName, ClientConstants.QUIT_ACTION);
        StatsManager.getInstance().logStatsEvent(event);

        //get state manager who will handle the state serialization
        //send a message to the component and request for state information returned as xml string
        String msgID = WorkbenchManager.getInstance().getMsgMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.SAVE_COMP_CMD, stMgrDesc, QIWConstants.STRING_TYPE, componentDisplayName);
        WorkbenchManager.getInstance().getMsgMgr().getMatchingResponseWait(msgID, 5000);
        quitComponent(componentType, componentDisplayName);
    }

    /**
     * registerPlugin sends request to register a new plugin and add it to the GUI
     * @param plugin String display name of new plugin
     */
    // TODO generalize this method to handle viewers also
    public static void registerPlugin(String plugin) {
        // TODO send plugin registration request to server
        logger.info("Register " + plugin);
    }

    /**
     * updatePlugin sends request to load new jar for plugin
     * @param plugin String display name of plugin to update
     */
    // TODO generalize this method to handle viewers also
    public static void updatePlugin(String plugin) {
        // TODO send load new plugin version request to server
        logger.info("Update " + plugin);
    }

    /**
     * renameComponent sends request to rename a plugin or viewer instance
     * @param instance String display name of component to rename
     */
    public static void renameComponent(String componentType, String instance) {
        logger.info("Rename component type " + componentType + ", instance " + instance);
        // get menu descriptor for this actionCommand
        WorkbenchMenuDescriptor d = WorkbenchMenuDescriptor.getMenuDescriptor(componentType + ".Rename");
        if (d == null) {
            WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                    "Error occured while renaming component");
            return;
        }
        WorkbenchDialog dialog = new WorkbenchDialog(d.getFrame());
        String newName = dialog.renameComponent(componentType, instance);
        if (newName == null || newName.length() == 0) {
            logger.info("Cancelling Re-Name");
        }
        while (true) {
            for (int i = 0; i < WorkbenchData.treeDescriptors.size(); i++) {
                if (WorkbenchData.treeDescriptors.get(i).getDisplayName().equals(newName)) {
                    newName = dialog.renameError(componentType, instance, newName);
                }
                if (newName == null || newName.length() == 0) {
                    logger.info("Cancelling Re-Name");
                    break;
                }
            }
            break;
        }
        if (newName != null && newName.length() > 0) {
            logger.info("Renaming " + componentType + " " + instance + " to " + newName);
            String parentDisplayName = removeComponentFromGUI(componentType, instance);
            // TODO use WorkbenchData.prefix and suffix instead
            parentDisplayName = parentDisplayName.substring(parentDisplayName.indexOf(":") + 2);
            logger.info("Add " + newName + " to " + parentDisplayName);
            // TODO get rid of hard-coded nodeType
            if (componentType.equals("Plugin")) {
                addComponentToGUI(componentType, parentDisplayName, newName, WorkbenchData.PLUGIN_INSTANCE_NODE_TYPE);
            } else if (componentType.equals("Viewer")) {
                addComponentToGUI(componentType, parentDisplayName, newName, WorkbenchData.VIEWER_INSTANCE_NODE_TYPE);
            } else {
                WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                        "Error occured while renaming component");
                return;
            }
            // send command to plugin to change its title
            ArrayList<String> renameComponent = new ArrayList<String>(2);
            renameComponent.add(0, instance);
            renameComponent.add(1, newName);
            String msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.RENAME_COMPONENT,
                    WorkbenchManager.getInstance().getDesc(), QIWConstants.ARRAYLIST_TYPE,
                    renameComponent);
        }
    }

    /**
     * Make all necessary changes to menus and tree when a new component is added
     * @param parentDisplayName is display name of parent node in tree
     * @param displayName is display name of new item
     * @param desc IComponentDescriptor of item being added to gui
     * @param nodeType type of node being added - plugin, viewer etc
     */
    public static void addComponentToGUI(String parentDisplayName, String displayName,
            IComponentDescriptor desc, String nodeType) {
        WorkbenchTreeManager.getComponentTree().repaint();

        WorkbenchMenuDescriptor d = null;
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_SAVE_CMD);
        if (d == null) {
            JOptionPane.showMessageDialog(null, "Error Adding New Component");
        //JMenuItem item = loadMenu(d.getMenu(),displayName,d.getActionCommand(),d.getActionListener());
        }
        QiMenuItem item = loadMenu2(d.getMenu(), desc, d.getActionCommand(), d.getActionListener());
        //d.getContents().add(item);
        sortMenuItem(d, item);

        //  add displayName to componentType.SaveAs menu
        d = null;
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_SAVEAS_CMD);
        if (d == null) {
            JOptionPane.showMessageDialog(null, "Error Adding New Component");
        }
        item = loadMenu2(d.getMenu(), desc, d.getActionCommand(), d.getActionListener());
        //d.getContents().add(item);
        sortMenuItem(d, item);

        // add displayName to componentType.Save.Quit menu
        d = null;
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_SAVE_QUIT_CMD);
        if (d == null) {
            JOptionPane.showMessageDialog(null, "Error Adding New Component");
        }
        item = loadMenu2(d.getMenu(), desc, d.getActionCommand(), d.getActionListener());
        //d.getContents().add(item);
        sortMenuItem(d, item);

        // add displayName to componentType.Quit menu

        d = null;

        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_QUIT_CMD);
        if (d == null) {
            JOptionPane.showMessageDialog(null, "Error Adding New Component");
        }
        item = loadMenu2(d.getMenu(), desc, d.getActionCommand(), d.getActionListener());
        //d.getContents().add(item);
        sortMenuItem(d, item);

        // add displayName to componentType.Quit menu

        boolean isExiting = WorkbenchStateManager.getInstance().componentExitsInSavedSet(desc);
        if (isExiting) {
            d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_DELETE_CMD);
            if (d == null) {
                JOptionPane.showMessageDialog(null, "Error Adding New Component");
            }
            item = loadMenu2(d.getMenu(), desc, d.getActionCommand(), d.getActionListener());
            //d.getContents().add(item);
            sortMenuItem(d, item);
        }



        // add displayName to componentType.Rename menu
        d = null;
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_RENAME_CMD);
        if (d == null) {
            JOptionPane.showMessageDialog(null, "Error Adding New Component");
        }
        item = loadMenu2(d.getMenu(), desc, d.getActionCommand(), d.getActionListener());
        //d.getContents().add(item);
        sortMenuItem(d, item);

        // update componentTree
        WorkbenchTreeDescriptor t = WorkbenchTreeManager.getTreeDescriptor(parentDisplayName);
        if (t == null) {
            JOptionPane.showMessageDialog(null, "Error Adding New Component");
        }
        DefaultMutableTreeNode n = t.getNode();
        QiDefaultMutableTreeNode childNode = new QiDefaultMutableTreeNode("-" + displayName, desc);
        ArrayList<String> list = new ArrayList<String>();
        for (String s : workbenchGui.getComponentMenuItemTexts()) {
            list.add(s);
        }
        list.remove("Open");
        if (!WorkbenchStateManager.getInstance().componentExitsInSavedSet(desc)) {
            list.remove("Delete");
        }
        childNode.setNodePopupUITexts(list);
        WorkbenchTreeDescriptor childDescriptor = new WorkbenchTreeDescriptor(displayName, "-" +
                displayName, childNode, nodeType, desc.getCID(),
                new ArrayList<DefaultMutableTreeNode>());
        WorkbenchTreeManager.addTreeDescriptor(childDescriptor);
        // TODO make sure parent node is expanded
        //WorkbenchData.projectTree.expandPath((TreePath)childNode.getPath());
        // add child node to tree descriptor and tree
        t.addChild(childNode);

        WorkbenchTreeManager.getComponentTreeModel().insertNodeInto(childNode, n, n.getChildCount());
        WorkbenchTreeManager.getComponentTreeModel().nodeStructureChanged(n);
        //sorting
        List<QiDefaultMutableTreeNode> lst = new ArrayList<QiDefaultMutableTreeNode>();
        for (DefaultMutableTreeNode nd : t.getChildren()) {
            if (nd instanceof QiDefaultMutableTreeNode) {
                lst.add((QiDefaultMutableTreeNode) nd);
            }
        }
        Collections.sort(lst);
        n.removeAllChildren();

        for (QiDefaultMutableTreeNode tn : lst) {
            WorkbenchTreeManager.getComponentTreeModel().insertNodeInto(tn, n, n.getChildCount());
            WorkbenchTreeManager.getComponentTreeModel().nodeStructureChanged(n);
        }

    }

    public static void addMenuItemToUI(String action, IComponentDescriptor desc) {
        if (desc == null) {
            return;
        }
        WorkbenchMenuDescriptor d = workbenchGui.getMenuDescriptor(action);
        if (d == null) {
            JOptionPane.showMessageDialog(workbenchGui, "Error Adding New Component to menu " + action);
        }
        QiMenuItem item = loadMenu2(d.getMenu(), desc, d.getActionCommand(), d.getActionListener());
        //d.getContents().add(item);
        sortMenuItem(d, item);
    }

    private static void sortMenuItem(WorkbenchMenuDescriptor d, QiMenuItem item) {
        if (d == null) {
            return;
        }
        d.getContents().add(item);
        //ArrayList<QiMenuItem> list = d.getContents();
        ArrayList<JMenuItem> lst = d.getContents();
        ArrayList<QiMenuItem> list = new ArrayList<QiMenuItem>();
        for (int i = 0; lst != null && i < lst.size(); i++) {
            if (lst.get(i) instanceof QiMenuItem) {
                list.add((QiMenuItem) lst.get(i));
            }
        }
        Collections.sort(list);
        ArrayList<JMenuItem> lst2 = new ArrayList<JMenuItem>();
        for (QiMenuItem it : list) {
            lst2.add(it);
        }
        d.setMenuItems(lst2);

        d.getMenu().removeAll();
        if (list != null && list.size() > 0) {
            String compKind = list.get(0).getComponentDescriptor().getComponentKind();
            for (int i = 0; i < list.size(); i++) {
                QiMenuItem itm = list.get(i);
                String temp = itm.getComponentDescriptor().getComponentKind();
                if (!compKind.equals(temp)) {
                    d.getMenu().addSeparator();
                    compKind = temp;
                }
                d.getMenu().add(itm);
            }
        }
    }

    private static void sortMenuItem(WorkbenchMenuDescriptor d) {
        if (d == null) {
            return;
        }
        ArrayList<JMenuItem> list1 = d.getContents();
        List<QiMenuItem> list = new ArrayList<QiMenuItem>();
        for (int i = 0; list1 != null && i < list1.size(); i++) {
            if (list1.get(i) instanceof QiMenuItem) {
                list.add((QiMenuItem) list1.get(i));
            }
        }
        Collections.sort(list);
        ArrayList<JMenuItem> list2 = new ArrayList<JMenuItem>(); //for backward compatibility

        for (QiMenuItem it : list) {
            list2.add(it);
        }
        d.setMenuItems(list2);
        d.getMenu().removeAll();
        if (list != null && list.size() > 0) {
            String compKind = list.get(0).getComponentDescriptor().getComponentKind();
            for (int i = 0; i < list.size(); i++) {
                QiMenuItem itm = list.get(i);
                String temp = itm.getComponentDescriptor().getComponentKind();
                if (!compKind.equals(temp)) {
                    d.getMenu().addSeparator();
                    compKind = temp;
                }
                d.getMenu().add(itm);
            }
        }
    }

    /**
     * Make all necessary changes to menus and tree when a new component is added
     * @param componentType is "Plugin", "Viewer", etc
     * @param parentDisplayName is display name of parent node in tree
     * @param displayName is display name of new item
     * @param nodeType is node type of new item
     */
    public static void addComponentToGUI(String componentType, String parentDisplayName, String displayName,
            String nodeType) {
        WorkbenchTreeManager.getComponentTree().repaint();
//        WorkbenchTreeManager.getDataTree().repaint();
        // add displayName to ComponentType.Save menu
        WorkbenchMenuDescriptor d = null;
        d = WorkbenchMenuDescriptor.getMenuDescriptor(componentType + ".Save");

        if (d == null) {
            WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                    "Error occured while adding component");
            return;
        } else {
            JMenuItem item = loadMenu(d.getMenu(), displayName, d.getActionCommand(), d.getActionListener());
            d.getContents().add(item);
            // add displayName to componentType.Save.Quit menu
            d = null;
            d = WorkbenchMenuDescriptor.getMenuDescriptor(componentType + ".Save.Quit");
            if (d == null) {
                WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                        "Error occured while adding component");
                return;
            } else {
                item = loadMenu(d.getMenu(), displayName, d.getActionCommand(), d.getActionListener());
                d.getContents().add(item);
                // add displayName to componentType.Quit menu
                d = null;
                d = WorkbenchMenuDescriptor.getMenuDescriptor(componentType + ".Quit");
                if (d == null) {
                    WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                            "Error occured while adding component");
                    return;
                } else {
                    item = loadMenu(d.getMenu(), displayName, d.getActionCommand(), d.getActionListener());
                    d.getContents().add(item);
                    // add displayName to componentType.Rename menu
                    d = null;
                    d = WorkbenchMenuDescriptor.getMenuDescriptor(componentType + ".Rename");
                    if (d == null) {
                        WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                "Error occured while adding component");
                        return;
                    } else {
                        item = loadMenu(d.getMenu(), displayName, d.getActionCommand(), d.getActionListener());
                        d.getContents().add(item);
                        // update componentTree
                        WorkbenchTreeDescriptor t = WorkbenchTreeDescriptor.getTreeDescriptor(parentDisplayName);
                        if (t == null) {
                            WorkbenchManager.getInstance().showInternalErrorDialog(
                                    Thread.currentThread().getStackTrace(),
                                    "Error occured while adding component");
                            return;
                        } else {
                            DefaultMutableTreeNode n = t.getNode();
                            DefaultMutableTreeNode childNode = new DefaultMutableTreeNode("-" + displayName);
                            WorkbenchTreeDescriptor childDescriptor = new WorkbenchTreeDescriptor(displayName, "-" + displayName, childNode, nodeType, "",
                                    new ArrayList<DefaultMutableTreeNode>(10));
                            WorkbenchData.treeDescriptors.add(childDescriptor);
                            logger.info("Added Tree Descriptor: displayName: " + displayName + " nodeType: " + nodeType);
                            // TODO make sure parent node is expanded
                            //WorkbenchData.projectTree.expandPath((TreePath)childNode.getPath());
                            // add child node to tree descriptor and tree
                            t.addChild(childNode);
                            WorkbenchTreeManager.getComponentTreeModel().insertNodeInto(childNode, n, n.getChildCount());
                            WorkbenchTreeManager.getComponentTreeModel().nodeStructureChanged(n);
                        }
                    }
                }
            }
        }
    }

    /**
     * Make all necessary changes to menus and tree when a component is removed.
     * @param componentType ComponentKind, e.g. plugin, viewer, etc
     * @param displayName String display name of parent
     */
    private static String removeComponentFromGUI(String componentType, String displayName) {
        String parentDisplayName = "";
        WorkbenchTreeManager.getComponentTree().repaint();
//        WorkbenchTreeManager.getDataTree().repaint();
        if (WorkbenchData.verbose > 0) {
            logger.info("Removing " + displayName + " from workbench");
        // remove from componentType.Save menu
        }
        WorkbenchMenuDescriptor d = WorkbenchMenuDescriptor.getMenuDescriptor(componentType + ".Save");
        if (d == null) {
            WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                    "Error occured while removing component");
        } else {
            JMenu m = d.getMenu();
            // find JMenuItem name in contents
            for (int i = 0; i < d.getContents().size(); i++) {
                if (d.getContents().get(i).getText().equals(displayName)) {
                    m.remove(d.getContents().get(i));
                    d.getContents().remove(i);
                }
            }
            // remove from componentType.Save.Quit menu
            d = WorkbenchMenuDescriptor.getMenuDescriptor(componentType + ".Save.Quit");
            if (d == null) {
                WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                        "Error occured while removing component");
            } else {
                m = d.getMenu();
                // find JMenuItem name in contents
                for (int i = 0; i < d.getContents().size(); i++) {
                    if (d.getContents().get(i).getText().equals(displayName)) {
                        m.remove(d.getContents().get(i));
                        d.getContents().remove(i);
                    }
                }
                // remove from componentType.Quit menu
                d = WorkbenchMenuDescriptor.getMenuDescriptor(componentType + ".Quit");
                if (d == null) {
                    WorkbenchManager.getInstance().showInternalErrorDialog(
                            Thread.currentThread().getStackTrace(),
                            "Error occured while removing component");
                } else {
                    m = d.getMenu();
                    // find JMenuItem name in contents
                    for (int i = 0; i < d.getContents().size(); i++) {
                        if (d.getContents().get(i).getText().equals(displayName)) {
                            m.remove(d.getContents().get(i));
                            d.getContents().remove(i);
                        }
                    }
                    // remove from componentType.Rename menu
                    d = WorkbenchMenuDescriptor.getMenuDescriptor(componentType + ".Rename");
                    if (d == null) {
                        WorkbenchManager.getInstance().showInternalErrorDialog(
                                Thread.currentThread().getStackTrace(),
                                "Error occured while removing component");
                    } else {
                        m = d.getMenu();
                        // find JMenuItem name in contents
                        for (int i = 0; i < d.getContents().size(); i++) {
                            if (d.getContents().get(i).getText().equals(displayName)) {
                                m.remove(d.getContents().get(i));
                                d.getContents().remove(i);
                            }
                        }
                        // remove node from componentTree
                        WorkbenchTreeDescriptor t = WorkbenchTreeDescriptor.getTreeDescriptor(displayName);
                        if (t == null) {
                            WorkbenchManager.getInstance().showInternalErrorDialog(
                                    Thread.currentThread().getStackTrace(),
                                    "Error occured while removing component");
                        } else {
                            DefaultMutableTreeNode child = t.getNode();
                            DefaultMutableTreeNode parent = (DefaultMutableTreeNode) child.getParent();
                            parentDisplayName = (String) parent.getUserObject();
                            t = WorkbenchTreeDescriptor.getTreeDescriptor((String) parent.getUserObject());
                            parent.remove((MutableTreeNode) child);
                            WorkbenchTreeManager.getComponentTreeModel().nodeStructureChanged(parent);
                            return parentDisplayName;
                        }
                    }
                }
            }
        }
        return parentDisplayName;
    }

    /**
     * removeMenuItemFromMenu makes all necessary changes to menus and tree when a component is removed.
     * @param d menuDescriptor of item being removedDescriptor of item being removed
     * @param desc Component
     */
    public static void removeMenuItemFromMenu(WorkbenchMenuDescriptor d, IComponentDescriptor desc) {
        if (d == null) {
            JOptionPane.showMessageDialog(null, "Error Removing Component");
            return;
        }

        JMenu m = d.getMenu();
        // find JMenuItem name in contents
        for (int i = 0; i < d.getContents().size(); i++) {
            if (d.getContents().get(i) instanceof QiMenuItem) {
                QiMenuItem item = (QiMenuItem) d.getContents().get(i);
                if (item.getComponentDescriptor().getCID().equals(desc.getCID())) {
                    m.remove(d.getContents().get(i));
                    d.getContents().remove(i);
                }
            }
        }
    }

    private static String removeComponentFromGUI(IComponentDescriptor desc) {
        WorkbenchTreeManager.getComponentTree().repaint();
        String pDisplayName = desc.getPreferredDisplayName();
        if (WorkbenchData.verbose > 0) {
            logger.info("Removing " + pDisplayName + " from workbench");
        }
        WorkbenchMenuDescriptor d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_OPEN_CMD);
        removeMenuItemFromMenu(d, desc);
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_SAVE_CMD);
        removeMenuItemFromMenu(d, desc);
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_SAVEAS_CMD);
        removeMenuItemFromMenu(d, desc);
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_SAVE_QUIT_CMD);
        removeMenuItemFromMenu(d, desc);
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_QUIT_CMD);
        removeMenuItemFromMenu(d, desc);
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_DELETE_CMD);
        removeMenuItemFromMenu(d, desc);
        d = workbenchGui.getMenuDescriptor(QIWMenuConstants.MENU_COMPONENT_RENAME_CMD);
        removeMenuItemFromMenu(d, desc);    // remove node from componentTree

        WorkbenchTreeDescriptor t = WorkbenchTreeManager.getTreeDescriptorByCID(desc);
        if (t == null) {
            JOptionPane.showMessageDialog(null, "Error Removing Component");
            return "UNKNOWN";
        } else {
            DefaultMutableTreeNode child = t.getNode();

            DefaultMutableTreeNode parent = (DefaultMutableTreeNode) child.getParent();
            String parentScreenName = (String) parent.getUserObject();
            t = WorkbenchTreeDescriptor.getTreeDescriptor(parentScreenName);
            parent.remove((MutableTreeNode) child);
            t.removeChild(child);

            WorkbenchTreeManager.getComponentTreeModel().nodeStructureChanged(parent);
            return parentScreenName;
        }
    }

    /**
     * loadMenu adds given subMenu to it's parent menu
     * @param parent is menu to which subMenu is added
     * @param displayName is menu to add
     * @param actionCommand is actionCommand for new item
     * @param actionListener for new item
     * @return item added
     */
    public static JMenuItem loadMenu(JMenu parent, String displayName, String actionCommand,
            ActionListener actionListener) {
        JMenuItem item = new JMenuItem(displayName);
        parent.add(item);
        item.setActionCommand(actionCommand + "." + displayName);
        item.addActionListener(actionListener);
        return item;
    }

    /**
     * loadMenu1 adds given subMenu to it's parent menu
     * @param parent is menu to which subMenu is added
     * @param displayName is menu to add
     * @param actionCommand is actionCommand for new item
     * @param actionListener is listener for new item
     * @return added item
     * @deprecated Never called.
     */
    public static QiMenuItem loadMenu1(JMenu parent, String displayName, String actionCommand, ActionListener actionListener) {
        QiMenuItem item = new QiMenuItem(displayName, null);
        parent.add(item);
        item.setActionCommand(actionCommand + "." + displayName);
        item.addActionListener(actionListener);
        return item;
    }

    /**
     * Add a qiComponent as a menu item to a qiWorkbench menu
     * @param parent Menu to which item is added
     * @param desc Component descriptor of the qiComponent to be added.
     * @param actionCommand actionCommand for new item
     * @param actionListener Listener for new item
     */
    public static QiMenuItem loadMenu2(JMenu parent, IComponentDescriptor desc, String actionCommand,
            ActionListener actionListener) {
        //get the type of qiComponent
        String screenName = desc.getDisplayName();
        int idx = screenName.indexOf("#");
        if (idx != -1) {
            screenName = screenName.substring(0, idx);
        }
        QiMenuItem item = new QiMenuItem(screenName + ": " + desc.getPreferredDisplayName(), desc);
        parent.add(item);
        item.setActionCommand(actionCommand + "." + desc.getPreferredDisplayName());
        item.addActionListener(actionListener);
        return item;
    }

    /**
     * @deprecated Call commented out.
     */
    private static Node buildXML(String dataSet, String dataTypeAndOrder) {
        String propertyMinMax = "";
        String propertyMin = "";
        String propertyMax = "";
        // property or event names if applicable
        String properties = WorkbenchData.properties.get(dataSet);
        String[] propertyNames = properties.split(" ");
        logger.info("properties: " + properties);
        logger.info("propertyNames: ");
        for (int i = 0; i < propertyNames.length; i++) {
            logger.info(propertyNames[i]);
        }
        logger.info(dataSet + " has " + propertyNames.length + " properties");
        if (!WorkbenchData.properties.containsKey(dataSet)) {
            WorkbenchManager.getInstance().showInternalErrorDialog(
                    Thread.currentThread().getStackTrace(),
                    "Error occured while starting viewer");
            return null;
        }
        // location of header limits file
        String seismicDir = WorkbenchData.seismicDir.get(dataSet);
        if (seismicDir == null) {
            WorkbenchManager.getInstance().showInternalErrorDialog(
                    Thread.currentThread().getStackTrace(),
                    "Error occured while starting viewer");
            return null;
        }
        // location of *.dat file
        String pathListDir = WorkbenchData.pathListDir.get(dataSet);
        // header limits
        int numberOfKeys;
        numberOfKeys = Integer.parseInt(WorkbenchData.numberOfKeys.get(dataSet));
        String[][] headerLimits = new String[numberOfKeys][5];
        logger.info(dataSet + " has " + numberOfKeys + " keys");
        headerLimits = getHeaderLimits(seismicDir + "/" + dataSet + "_0000.0001.HDR", numberOfKeys);
        // vertical key
        ArrayList<String> vkeyInfo = new ArrayList<String>(4);
        vkeyInfo = getVkeyInfo(dataSet, seismicDir);
        // property or event limits
        if (dataTypeAndOrder.equals("Event Cross-Sections") || dataTypeAndOrder.equals("Model Cross-Sections")) {
            propertyMinMax = WorkbenchData.propertyMinMax.get(dataSet);
            logger.info("propertyMinMax: " + propertyMinMax);
            if (propertyMinMax != null) {
                String fields[] = propertyMinMax.split(" ");
                propertyMin = fields[0];
                propertyMax = fields[1];
                for (int i = 0; i < fields.length; i++) {
                    logger.info(fields[i]);
                }
            } else {
                propertyMin = "0";
                propertyMax = "0";
            }
        }
        StringBuffer content = new StringBuffer();
        //BhpViewer part
        viewerXML(content, dataSet);
        //BhpWindow part
        windowXML(content, dataTypeAndOrder);
        //annotation
        annotationXML(content, dataSet, dataTypeAndOrder, headerLimits, numberOfKeys, propertyNames[0], vkeyInfo);
        // layer type
        layerTypeXML(content, dataTypeAndOrder);
        // BhpLayer part
        layerXML(content, dataSet, dataTypeAndOrder, propertyNames[0], pathListDir);
        //Parameter part
        parameterXML(content, dataSet, dataTypeAndOrder, propertyNames, headerLimits, vkeyInfo, numberOfKeys);
        // plot selector part
        selectorXML(content, dataTypeAndOrder, vkeyInfo, headerLimits, numberOfKeys, propertyMin, propertyMax);
        // plot scaling part
        scalingXML(content, dataTypeAndOrder, propertyMin, propertyMax);
        // plot agc part
        if (dataTypeAndOrder.equals("Seismic Cross-Sections")) {
            content.append("<PlotAGC applyAGC=\"false\" measureUnit=\"1\" windowLength=\"250.0\"/>\n");
        // rasterizer part
        }
        rasterizerXML(content, dataTypeAndOrder);
        // colormap part
        if (dataTypeAndOrder.contains("Model") || dataTypeAndOrder.contains("Maps")) {
            colormapXML(content);
        // ending part
        }
        endXML(content, dataTypeAndOrder);
        logger.info(content.toString());
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);
            DocumentBuilder dbuilder = factory.newDocumentBuilder();
            StringReader sr = new java.io.StringReader(content.toString());
            InputSource is = new InputSource(sr);
            Document doc = dbuilder.parse(is);
            Element root = doc.getDocumentElement();
            root.normalize();
            return root;
        } catch (Exception e) {
            WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                    "Could not read XML Stream");
        }
        return null;
    }

    private static void viewerXML(StringBuffer content, String dataSet) {
        content.append("<?xml version=\"1.0\" ?>\n");
        content.append("<BhpViewer ");
        content.append("title=\"" + dataSet + "\" ");
        content.append("highlightLS=\"" + "1" + "\" ");
        content.append("highlightLW=\"" + "6.0" + "\" ");
        content.append("highlightLC=\"" + "255 255 0 100" + "\" ");
        content.append("cursorLS=\"" + "1" + "\" ");
        content.append("cursorLW=\"" + "1" + "\" ");
        content.append("cursorLC=\"" + "255 0 0 255" + "\" ");
        content.append("cursorW=\"" + "10000.0" + "\" ");
        content.append("cursorH=\"" + "10000.0" + "\" ");
        content.append("version=\"" + "3.6.3.10" + "\" ");
        content.append("width=\"" + "972" + "\" ");
        content.append("height=\"" + "729" + "\">\n");
        content.append("    <MenuState ");
        content.append("showToolbar=\"" + "true" + "\" ");
        content.append("showStatusbar=\"" + "true" + "\"/>\n");
    }

    private static void windowXML(StringBuffer content, String dataTypeAndOrder) {
        if (dataTypeAndOrder.contains("Cross-Sections")) {
            content.append("<BhpWindow title=\"XSection Window\" width=\"775\" height=\"585\" ");
        } else {
            content.append("<BhpWindow title=\"Map Window\" width=\"350\" height=\"600\" ");
        }
        content.append("positionX=\"0\" positionY=\"0\" layerCounter=\"1\" ");
        if (dataTypeAndOrder.equals("Seismic Cross-Sections") || dataTypeAndOrder.equals("Model Cross-Sections") ||
                dataTypeAndOrder.equals("Model Maps")) {
            content.append("axisAssociate=\"1\" ");
        } else if (dataTypeAndOrder.equals("Event Cross-Sections") || dataTypeAndOrder.equals("Seismic Maps") ||
                dataTypeAndOrder.equals("Model Maps")) {
            content.append("axisAssociate=\"2\" ");
        } else if (dataTypeAndOrder.equals("Event Maps")) {
            content.append("axisAssociate=\"9\" ");
        }
        content.append(" numberOfLayers=\"1\" viewPositionX=\"0\" viewPositionY=\"0\" dividerLocation=\"50\" ");
        content.append("showHorizonGraph=\"false\" mouseTracking=\"true\" enablePlotTalk=\"true\" ");
        content.append("plotSynFlag=\"16777986\" plotLockRatio=\"false\" ");
        content.append("plotHScale=\"11.9\"");
        if (dataTypeAndOrder.equals("Seismic Cross-Sections")) {
            content.append(" plotVScale=\"0.11\" ");
        } else if (dataTypeAndOrder.equals("Model Cross-Sections")) {
            content.append(" plotVScale=\"1.3\" ");
        } else if (dataTypeAndOrder.equals("Event Cross-Sections")) {
            content.append(" plotVScale=\"4.0\" ");
        } else {
            content.append(" plotVScale=\"8.5\" ");
        }
        if (dataTypeAndOrder.contains("Cross-Sections")) {
            content.append("syncHScroll=\"true\" syncVScroll=\"true\" windowType=\"1\">");
        } else {
            content.append("syncHScroll=\"true\" syncVScroll=\"true\" windowType=\"2\">");
        }
        content.append("\n");
    }

    private static void annotationXML(StringBuffer content, String dataSet, String dataTypeAndOrder,
            String[][] headerLimits, int numberOfKeys, String propertyName,
            ArrayList<String> vkeyInfo) {
        content.append("<Annotation>\n");
        if (dataTypeAndOrder.contains("Cross-Sections")) {
            content.append("<AnnoTrace hSyncField=\"");
            content.append(headerLimits[0][0]);
            content.append("\" location=\"4\" >");
            content.append("\n");
            content.append("<AnnoTraceField name=\"");
            content.append(headerLimits[0][0]);
            content.append("\" step=\"10.0\" line=\"true\" angle=\"0.0\" gap=\"60.0\" limit=\"0.0\" flag=\"0\" />");
            content.append("\n");
            for (int i = 1; i < numberOfKeys; i++) {
                content.append("<AnnoTraceField name=\"");
                content.append(headerLimits[i][0]);
                content.append("\" step=\"10.0\" line=\"true\" angle=\"0.0\" gap=\"60.0\" limit=\"0.0\" flag=\"0\" />");
                content.append("\n");
            }
            content.append("</AnnoTrace>");
            content.append("\n");
            content.append("<AnnoSample location=\"1\" showGrid=\"false\" isAutomatic=\"true\" majorStep=\"100.0\" ");
            content.append("minorStep=\"10.0\"/>");
            content.append("<AnnoMisc titleLocation=\"0\" titleText=\"\" colorbarLocation=\"0\" leftLabel=\"\" ");
            content.append("rightLabel=\"\" topLabel=\"\" bottomLabel=\"\"/>");
            content.append("\n");
        } else {
            content.append("<AnnoGeneral hlocation=\"4\" hmajorStep=\"100.0\" hminorStep=\"10.0\" ");
            content.append("vlocation=\"1\" vmajorStep=\"100.0\" vminorStep=\"10.0\"/>\n");
            content.append("<AnnoMisc titleLocation=\"4\" titleText=\"");
            if (dataTypeAndOrder.contains("Seismic")) {
                content.append("Time/Depth: 0.0\" ");
            } else {
                content.append(propertyName + "\"");
            }
            content.append(" colorbarLocation=\"0\" leftLabel=\"");
            content.append(vkeyInfo.get(0) + "\" rightLabel=\"null\" topLabel=\"");
            content.append(headerLimits[1][0] + "\" bottomLabel=\"null\"/>\n");
        }
        content.append("</Annotation>\n");
    }

    private static void layerTypeXML(StringBuffer content, String dataTypeAndOrder) {
        if (dataTypeAndOrder.equals("Seismic Cross-Sections")) {
            content.append("<BhpSeismicLayer>\n");
        } else if (dataTypeAndOrder.equals("Event Cross-Sections")) {
            content.append("<BhpEventLayer refSeismicLayerId=\"-1\" eventLineStyle=\"1\" eventLineWidth=\"1.0\" " +
                    " eventLineColor=\"255 0 0 255\">\n");
        } else if (dataTypeAndOrder.equals("Model Cross-Sections")) {
            content.append("<BhpModelLayer>");
        } else if (dataTypeAndOrder.equals("Seismic Maps")) {
            content.append("<BhpMapLayer dataType=\"1\" modelLayerSync=\"false\" mapSynFlag=\"0\" " +
                    "reversedHDirection=\"false\" transposeImage=\"false\">\n");
        } else if (dataTypeAndOrder.equals("Event Maps")) {
            content.append("<BhpMapLayer dataType=\"3\" modelLayerSync=\"false\" mapSynFlag=\"0\" " +
                    "reversedHDirection=\"false\" transposeImage=\"false\">\n");
        } else if (dataTypeAndOrder.equals("Model Maps")) {
            content.append("<BhpMapLayer dataType=\"2\" modelLayerSync=\"false\" mapSynFlag=\"0\" " +
                    "reversedHDirection=\"false\" transposeImage=\"false\">\n");
        }
    }

    private static void layerXML(StringBuffer content, String dataSet, String dataTypeAndOrder,
            String propertyName, String pathListDir) {
        if (dataTypeAndOrder.equals("Seismic Cross-Sections")) {
            content.append("<BhpLayer idNumber=\"1\" dsType=\"BHP-SU\" name=\"");
            content.append(dataSet);
        } else if (dataTypeAndOrder.equals("Event Cross-Sections")) {
            content.append("<BhpLayer idNumber=\"2\" dsType=\"BHP-SU\" name=\"");
            content.append(propertyName);
        } else if (dataTypeAndOrder.equals("Model Cross-Sections")) {
            content.append("<BhpLayer idNumber=\"1\" dsType=\"BHP-SU\" name=\"");
            content.append(dataSet);
        } else if (dataTypeAndOrder.equals("Seismic Maps")) {
            content.append("<BhpLayer idNumber=\"2\" dsType=\"BHP-SU\" name=\"");
            content.append(dataSet);
        } else if (dataTypeAndOrder.equals("Event Maps")) {
            content.append("<BhpLayer idNumber=\"9\" dsType=\"BHP-SU\" name=\"");
            content.append(propertyName);
        } else if (dataTypeAndOrder.equals("Model Maps")) {
            content.append("<BhpLayer idNumber=\"1\" dsType=\"BHP-SU\" name=\"");
            content.append(propertyName);
        }
        content.append("\" dataName=\"");
        content.append(dataSet);
        content.append("\" pathlist=\"");
        content.append(pathListDir + "/" + dataSet + ".dat\"");
        if (dataTypeAndOrder.equals("Seismic Cross-Sections") || dataTypeAndOrder.equals("Seismic Maps")) {
            content.append(" enableTalk=\"true\" synFlag=\"0\" transparent=\"255\" property=\"null\" ");
        } else {
            content.append(" enableTalk=\"true\" synFlag=\"0\" transparent=\"255\" property=\"" +
                    propertyName + "\" ");
        }
        content.append(" propertySync=\"false\" scaleFactorH=\"1.0\" scaleFactorV=\"1.0\" reversedPolarity=\"false\" ");
        content.append("visible=\"true\" reversedSampleOrder=\"false\">\n");
    }

    private static void parameterXML(StringBuffer content, String dataSet, String dataTypeAndOrder,
            String[] propertyNames, String[][] headerLimits, ArrayList<String> vkeyInfo,
            int numberOfKeys) {
        if (dataTypeAndOrder.equals("Seismic Cross-Sections")) {
            content.append("<Parameter properties=\"\" attributeString=\"");
            content.append(dataSet);
            content.append(" is SEISMIC data, stored in CROSS-SECTION order\" content=\"");
        } else if (dataTypeAndOrder.equals("Event Cross-Sections") || dataTypeAndOrder.equals("Model Cross-Sections")) {
            content.append("<Parameter properties=\"");
            for (int i = 0; i < propertyNames.length; i++) {
                content.append(propertyNames[i] + " ");
            }
            content.append("\" attributeString=\"");
            content.append(dataSet);
            if (dataTypeAndOrder.equals("Event Cross-Sections")) {
                content.append(" is HORIZON data, stored in CROSS-SECTION order\" content=\"");
            } else {
                content.append(" is PROPERTY data, stored in CROSS-SECTION order\" content=\"");
            }
        } else if (dataTypeAndOrder.equals("Seismic Maps")) {
            content.append("<Parameter properties=\"\" attributeString=\"");
            content.append(dataSet);
            content.append(" is SEISMIC data, stored in MAP-VIEW order\" content=\"");
        } else if (dataTypeAndOrder.equals("Event Maps") || dataTypeAndOrder.equals("Model Maps")) {
            content.append("<Parameter properties=\"");
            for (int i = 0; i < propertyNames.length; i++) {
                content.append(propertyNames[i] + " ");
            }
            content.append("\" attributeString=\"");
            content.append(dataSet);
            if (dataTypeAndOrder.equals("Event Maps")) {
                content.append(" is HORIZON data, stored in MAP-VIEW order\" content=\"");
            } else {
                content.append(" is PROPERTY data, stored in MAP-VIEW order\" content=\"");
            }
        }
        content.append(headerLimits[0][0]);
        content.append("|");
        content.append(headerLimits[0][1]);
        content.append(" - ");
        content.append(headerLimits[0][2]);
        content.append("[");
        content.append(headerLimits[0][3]);
        content.append("] |");
        if (numberOfKeys == 1) {
            if (dataTypeAndOrder.contains("Seismic")) {
                content.append("*|1|1.0|0.0|false|false|0|");
            } else if (dataTypeAndOrder.contains("Event") || dataTypeAndOrder.contains("Model")) {
                content.append("*|1|1.0|0.0|false|false|");
            }
        } else {
            content.append(headerLimits[0][1]);
            if (dataTypeAndOrder.contains("Seismic")) {
                content.append("|1|1.0|0.0|false|false|0|");
            } else if (dataTypeAndOrder.contains("Event") || dataTypeAndOrder.contains("Model")) {
                content.append("|1|1.0|0.0|false|false|");
            }
            for (int i = 1; i < numberOfKeys - 1; i++) {
                content.append(headerLimits[i][0]);
                content.append("|");
                content.append(headerLimits[i][1]);
                content.append(" - ");
                content.append(headerLimits[i][2]);
                content.append("[");
                content.append(headerLimits[i][3]);
                content.append("] |");
                content.append(headerLimits[i][1]);
                if (dataTypeAndOrder.contains("Seismic")) {
                    content.append("|" + (i + 1) + "|1.0|0.0|false|false|0|");
                } else if (dataTypeAndOrder.contains("Event") || dataTypeAndOrder.contains("Model")) {
                    content.append("|" + (i + 1) + "|1.0|0.0|false|false|");
                }
            }
            content.append(headerLimits[numberOfKeys - 1][0]);
            content.append("|");
            content.append(headerLimits[numberOfKeys - 1][1]);
            content.append(" - ");
            content.append(headerLimits[numberOfKeys - 1][2]);
            content.append("[");
            content.append(headerLimits[numberOfKeys - 1][3]);
            content.append("] |*|");
            content.append(numberOfKeys);
            if (dataTypeAndOrder.contains("Seismic")) {
                content.append("|1.0|0.0|false|false|0|");
            } else if (dataTypeAndOrder.contains("Event") || dataTypeAndOrder.contains("Model")) {
                content.append("|1.0|0.0|false|false|");
            }
        }
        content.append(vkeyInfo.get(0));
        content.append("|");
        if (dataTypeAndOrder.equals("Seismic Cross-Sections") || dataTypeAndOrder.contains("Maps")) {
            content.append(vkeyInfo.get(1));
            content.append(" - ");
            content.append(vkeyInfo.get(2));
            content.append(" [");
            content.append(vkeyInfo.get(3));
            content.append("] |");
            content.append(vkeyInfo.get(1));
            content.append("-");
            content.append(vkeyInfo.get(2));
            content.append("[");
        } else if (dataTypeAndOrder.equals("Event Cross-Sections") ||
                dataTypeAndOrder.equals("Model Cross-Sections")) {
            if (vkeyInfo.get(1).contains(".")) {
                content.append(vkeyInfo.get(1).substring(0, vkeyInfo.get(1).indexOf(".")));
            } else {
                content.append(vkeyInfo.get(1));
            }
            content.append(" - ");
            if (vkeyInfo.get(2).contains(".")) {
                content.append(vkeyInfo.get(2).substring(0, vkeyInfo.get(2).indexOf(".")));
            } else {
                content.append(vkeyInfo.get(2));
            }
            content.append(" [");
            if (vkeyInfo.get(3).contains(".")) {
                content.append(vkeyInfo.get(3).substring(0, vkeyInfo.get(3).indexOf(".")));
            } else {
                content.append(vkeyInfo.get(3));
            }
            content.append("] |");
            if (vkeyInfo.get(1).contains(".")) {
                content.append(vkeyInfo.get(1).substring(0, vkeyInfo.get(1).indexOf(".")));
            } else {
                content.append(vkeyInfo.get(1));
            }
            content.append("-");
            if (vkeyInfo.get(2).contains(".")) {
                content.append(vkeyInfo.get(2).substring(0, vkeyInfo.get(2).indexOf(".")));
            } else {
                content.append(vkeyInfo.get(2));
            }
            content.append("[");
        }
        content.append(vkeyInfo.get(3));
        content.append("]|" + (numberOfKeys + 1) + "|");
        content.append(vkeyInfo.get(3));
        if (dataTypeAndOrder.contains("Seismic")) {
            content.append("|0.0|false|false|0|\" missingData=\"0\" />");
        } else if (dataTypeAndOrder.contains("Event") || dataTypeAndOrder.contains("Model")) {
            content.append("|0.0|false|false|\" missingData=\"0\" />");
        }
        content.append("\n");
        content.append("<Pipeline>\n");
    }

    private static void selectorXML(StringBuffer content, String dataTypeAndOrder, ArrayList<String> vkeyInfo,
            String[][] headerLimits, int numberOfKeys, String propertyMin, String propertyMax) {
        logger.info(" propertyMin: " + propertyMin + " propertyMax: " + propertyMax);
        content.append("<PlotDataSlector primaryKey=\"-1\" primaryKeyStart=\"1.0\" primaryKeyEnd=\"");
        int ensSize = Integer.parseInt(headerLimits[numberOfKeys - 1][2]) -
                Integer.parseInt(headerLimits[numberOfKeys - 1][1]) +
                Integer.parseInt(headerLimits[numberOfKeys - 1][3]);
        content.append(ensSize);
        content.append("\" primaryKeyStep=\"1.0\" applyGaps=\"false\" gapSize=\"5\" secondaryKey=\"-2\" ");
        content.append("useNewBhpio=\"true\" sampleValueStart=\"");
        if (dataTypeAndOrder.equals("Seismic Cross-Sections")) {
            content.append(vkeyInfo.get(1));
        } else if (dataTypeAndOrder.equals("Event Cross-Sections")) {
            content.append(propertyMin);
        } else if (dataTypeAndOrder.equals("Model Cross-Sections") || dataTypeAndOrder.contains("Maps")) {
            content.append("0.0");
        }
        content.append("\" sampleValueEnd=\"");
        if (dataTypeAndOrder.equals("Seismic Cross-Sections")) {
            content.append(vkeyInfo.get(2));
        } else if (dataTypeAndOrder.equals("Event Cross-Sections")) {
            content.append(propertyMax);
        } else if (dataTypeAndOrder.equals("Model Cross-Sections")) {
            content.append("0.0");
        } else if (dataTypeAndOrder.contains("Map")) {
            int k1, k2, k3;
            if (vkeyInfo.get(1).contains(".")) {
                k1 = Integer.parseInt(vkeyInfo.get(1).substring(0, vkeyInfo.get(1).indexOf(".")));
            } else {
                k1 = Integer.parseInt(vkeyInfo.get(1));
            }
            if (vkeyInfo.get(2).contains(".")) {
                k2 = Integer.parseInt(vkeyInfo.get(2).substring(0, vkeyInfo.get(2).indexOf(".")));
            } else {
                k2 = Integer.parseInt(vkeyInfo.get(2));
            }
            if (vkeyInfo.get(3).contains(".")) {
                k3 = Integer.parseInt(vkeyInfo.get(3).substring(0, vkeyInfo.get(3).indexOf(".")));
            } else {
                k3 = Integer.parseInt(vkeyInfo.get(3));
            }
            content.append(k2 - k1);
        }
        content.append("\" />\n");
    }

    private static void scalingXML(StringBuffer content, String dataTypeAndOrder, String propertyMin,
            String propertyMax) {
        if (dataTypeAndOrder.equals("Seismic Cross-Sections")) {
            content.append("<PlotScaling normType=\"4\" normScale=\"0.2\" interpolationType=\"1\"/>\n");
        } else if (dataTypeAndOrder.equals("Model Cross-Sections")) {
            content.append("<PlotScaling normType=\"6\" normLimitMin=\"");
            content.append(propertyMin + "\" normLimitMax=\"");
            content.append(propertyMax + "\" normScale=\"1.0\" interpolationType=\"0\"/>\n");
        } else if (dataTypeAndOrder.contains("Maps")) {
            content.append("<PlotScaling normType=\"0\" normScale=\"1.0\" interpolationType=\"2\"/>\n");
        }
    }

    private static void rasterizerXML(StringBuffer content, String dataTypeAndOrder) {
        if (dataTypeAndOrder.equals("Seismic Cross-Sections")) {
            content.append("<PlotRasterizer plotType=\"3\" clippingValue=\"4.0\" wiggleDecimation=\"5.0\" ");
            content.append("colorInterpStart=\"-1.0\" colorInterpEnd=\"1.0\">\n");
        } else if (dataTypeAndOrder.equals("Model Cross-Sections")) {
            content.append("<ModelRasterizer plotType=\"8\" clippingValue=\"1.0\" structuralInterp=\"true\" " +
                    "startDepth=\"0.0\" endDepth=\"0.0\" ");
            content.append("colorInterpStart=\"-1.0\" colorInterpEnd=\"1.0\">\n");
        } else if (dataTypeAndOrder.contains("Maps")) {
            content.append("<PlotRasterizer plotType=\"0\" clippingValue=\"4.0\" wiggleDecimation=\"1.0\" ");
            content.append("colorInterpStart=\"-1.0\" colorInterpEnd=\"1.0\">\n");
        }
    }

    private static void colormapXML(StringBuffer content) {
        content.append("<Colormap nColors=\"37\" rampSize=\"32\" backgroundIndex=\"32\" foregroundIndex=\"33\" ");
        content.append("positiveIndex=\"34\" negativeIndex=\"35\" hilightIndex=\"36\" backgroundColor=\"255 255 255 0\" ");
        content.append("foregroundColor=\"0 0 0 255\" positiveColor=\"0 0 0 255\" negativeColor=\"0 0 0 255\" ");
        content.append("hilightColor=\"255 0 0 255\" >\n");
        // int colormap
        content.append("<ColormapRampColor color=\"0 0 255 255\"/>\n");
        content.append("<ColormapRampColor color=\"0 29 218 255\"/>\n");
        content.append("<ColormapRampColor color=\"0 58 182 255\"/>\n");
        content.append("<ColormapRampColor color=\"0 87 145 255\"/>\n");
        content.append("<ColormapRampColor color=\"0 116 109 255\"/>\n");
        content.append("<ColormapRampColor color=\"0 145 72 255\"/>\n");
        content.append("<ColormapRampColor color=\"0 174 36 255\"/>\n");
        content.append("<ColormapRampColor color=\"0 204 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"36 211 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"72 218 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"109 225 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"145 233 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"182 240 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"218 247 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 255 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 240 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 225 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 211 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 196 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 182 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 167 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 153 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 137 5 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 122 10 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 107 15 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 91 20 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 76 25 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 61 30 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 45 35 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 30 40 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 15 45 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 0 51 255\"/>\n");
        // gw colormap
        /*content.append("<ColormapRampColor color=\"255 0 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 18 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 38 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 57 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 76 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 95 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 114 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 133 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 153 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 165 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 178 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 191 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 204 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 216 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 229 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 242 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"255 255 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"223 248 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"191 242 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"159 235 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"127 229 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"95 223 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"63 216 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"31 210 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"0 204 0 255\"/>\n");
        content.append("<ColormapRampColor color=\"0 182 36 255\"/>\n");
        content.append("<ColormapRampColor color=\"0 160 72 255\"/>\n");
        content.append("<ColormapRampColor color=\"0 116 145 255\"/>\n");
        content.append("<ColormapRampColor color=\"0 94 182 255\"/>\n");
        content.append("<ColormapRampColor color=\"0 72 218 255\"/>\n");
        content.append("<ColormapRampColor color=\"0 51 235 255\"/>\n");
        content.append("<ColormapRampColor color=\"0 30 255 255\"/>\n");*/
        content.append("</Colormap>\n");
    }

    private static void endXML(StringBuffer content, String dataTypeAndOrder) {
        if (dataTypeAndOrder.equals("Event Cross-Sections")) {
            content.append("</Pipeline>\n");
            content.append("</BhpLayer>\n");
            content.append("<BhpEventShapeSetting eventDrawLine=\"true\" eventDrawSymbol=\"false\" eventDrawAllLine=\"false\" " +
                    " eventSymbolStyle=\"3\" eventSymbolWidth=\"12.0\" eventSymbolHeight=\"12.0\" " +
                    " eventSymbolLineStyle=\"1\" eventSymbolFillStyle=\"1\" eventSymbolColor=\"0 0 0 255\">\n");
            content.append("</BhpEventShapeSetting>\n");
            content.append("</BhpEventLayer>\n");
        } else if (dataTypeAndOrder.equals("Seismic Cross-Sections") || dataTypeAndOrder.contains("Maps")) {
            content.append("</PlotRasterizer>\n");
            content.append("</Pipeline>\n");
            content.append("</BhpLayer>\n");
        } else if (dataTypeAndOrder.equals("Model Cross-Sections")) {
            content.append("</ModelRasterizer>\n");
            content.append("</Pipeline>\n");
            content.append("</BhpLayer>\n");
        }
        if (dataTypeAndOrder.equals("Seismic Cross-Sections")) {
            content.append("</BhpSeismicLayer>\n");
        } else if (dataTypeAndOrder.equals("Model Cross-Sections")) {
            content.append("</BhpModelLayer>\n");
        } else if (dataTypeAndOrder.contains("Map")) {
            content.append("</BhpMapLayer>\n");
        }
        content.append("</BhpWindow>\n");
        content.append("</BhpViewer>\n");
    }

    private static String[][] getHeaderLimits(String path, int numberOfKeys) {
        String[][] headerLimits = new String[numberOfKeys][5];
        String[] fields;
        ArrayList<String> temp = new ArrayList<String>();
        // call service to get header limits file
        ArrayList<String> params = new ArrayList<String>();
        params.add(guiMessagingMgr.getLocationPref());
        params.add(path);
        // tell agent to suspend message processing until IO services are done
        //.processMessages = false;
        String msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.FILE_READ_CMD,
                QIWConstants.ARRAYLIST_TYPE, params);
        IQiWorkbenchMsg response = guiMessagingMgr.getMatchingResponseWait(msgID, 10000);
        // restart message processing
        //.processMessages = true;
        if (response.isAbnormalStatus()) {
            WorkbenchManager.getInstance().showErrorDialog(QIWConstants.ERROR_DIALOG,
                    Thread.currentThread().getStackTrace(),
                    "Error occured while reading" + path,
                    new String[]{"File may have been deleted",
                        "Read permission may not be set"
                    },
                    new String[]{"Verify file exists and is readable",
                        "If yes, contact workbench support"
                    });
            return null;
        }
        temp = (ArrayList<String>) response.getContent();
        // parse temp into header limits arrays
        if (temp.size() < 5) {
            WorkbenchManager.getInstance().showErrorDialog(QIWConstants.ERROR_DIALOG,
                    Thread.currentThread().getStackTrace(),
                    "Error occured while reading" + path,
                    new String[]{path + " may be corrupted:"},
                    new String[]{"Contact workbench support"});
            return null;
        }
        for (int i = 0; i < numberOfKeys; i++) {
            for (int j = 0; j < 5; j++) {
                fields = temp.get(i * 5 + j).split("=");
                headerLimits[i][j] = fields[1].trim();
            }
        }
        return headerLimits;
    }

    protected static ArrayList<String> getVkeyInfo(String dataSet, String seismicDir) {
        ArrayList<String> vkeyInfo = new ArrayList<String>(4);
        ArrayList<String> params = new ArrayList<String>(2);
        ArrayList<String> temp = new ArrayList<String>();
        params.add(guiMessagingMgr.getLocationPref());
        params.add(seismicDir + "/" + dataSet + "_0000.HDR");
        String msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.FILE_READ_CMD, QIWConstants.ARRAYLIST_TYPE, params);
        IQiWorkbenchMsg response = guiMessagingMgr.getMatchingResponseWait(msgID, 10000);
        if (response.isAbnormalStatus()) {
            WorkbenchManager.getInstance().showErrorDialog(QIWConstants.ERROR_DIALOG,
                    Thread.currentThread().getStackTrace(),
                    "Error occured while reading" + seismicDir +
                    "/" + dataSet + "_0000.HDR",
                    new String[]{seismicDir + "/" + dataSet +
                        "_0000.HDR may be corrupted"
                    },
                    new String[]{"Contact workbench support"});
            return null;
        }
        temp = (ArrayList<String>) response.getContent();
        String[] fields;
        for (int i = 0; i < temp.size(); i++) {
            if (temp.get(i).startsWith("VKEY =")) {
                fields = temp.get(i).split("=");
                vkeyInfo.add(0, fields[1].trim());
            } else if (temp.get(i).startsWith("VMIN =")) {
                fields = temp.get(i).split("=");
                vkeyInfo.add(1, fields[1].trim());
            } else if (temp.get(i).startsWith("VMAX =")) {
                fields = temp.get(i).split("=");
                vkeyInfo.add(2, fields[1].trim());
            } else if (temp.get(i).startsWith("VINC =")) {
                fields = temp.get(i).split("=");
                vkeyInfo.add(3, fields[1].trim());
            }
        }
        logger.info("VKeyInfo: " + vkeyInfo);
        return vkeyInfo;
    }
    
    protected static void cascadeWindows() {
        workbenchGui.cascadeWindows();
    }
    
    protected static void tileWindows() {
        workbenchGui.tileWindows();
    }
}