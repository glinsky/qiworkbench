/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.workbench;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.help.CSH;
import javax.help.HelpBroker;
import javax.swing.filechooser.FileFilter;

import com.bhpb.qiworkbench.QiSpaceDescriptor;
import com.bhpb.qiworkbench.QiwbPreferences;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.client.util.GenericFileFilter;
import com.bhpb.qiworkbench.client.util.PreferenceUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;

/**
 * Dialog to edit the workbench session files associated with a project.
 * A workbench can be added, removed or designated as the default (if the
 * default qiProject associated with a default server is selected).
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class EditWorkbenchesDialog extends javax.swing.JDialog {
    private static Logger logger = Logger.getLogger(EditWorkbenchesDialog.class.getName());
    WorkbenchManager agent = null;
    String userHOME = "";
    QiwbPreferences userPrefs = null;
    ArrayList<String> validServers = null;
    String[] servers = new String[4];
    String selectedServer = "";
    ArrayList<QiSpaceDescriptor> validQispaces = null;
    String[] qispaces = new String[4];
    String selectedQispace = "";
    ArrayList<String> validWorkbenches = null;
    String[] workbenches = new String[4];
    String selectedWorkbench = "";
    boolean ignoreServerSelection = true;
    boolean ignoreQispaceSelection = true;
    boolean selectedDefaultServer = false;
    HelpBroker helpBroker;

    /** Creates new form EditWorkbenchesDialog */
    public EditWorkbenchesDialog(WorkbenchManager agent, HelpBroker helpBroker) {
        this.agent = agent;
        this.helpBroker = helpBroker;
        CSH.setHelpIDString(this, "prefs.saveset");

        initComponents();

        // Get user's preferences
        userHOME = MessageDispatcher.getInstance().getUserHOME();
        String prefFileDir = userHOME + File.separator + QIWConstants.QIWB_WORKING_DIR;
        if (PreferenceUtils.prefFileExists(prefFileDir)) {
            // get user's preferences saved in the preference file
            try {
                userPrefs = PreferenceUtils.readPrefs(prefFileDir);
            } catch (QiwIOException qioe) {
                //TODO notify user cannot read preference file
                logger.finest(qioe.getMessage());
            }
        } else {
            //NOTE: This should only happen if the user deleted their
            //preference file since one is created when the app is
            //launched (if there isn't one).
            // user has no preference file; create one
            userPrefs = new QiwbPreferences();
            try {
                PreferenceUtils.writePrefs(userPrefs, prefFileDir);
            } catch (QiwIOException qioe) {
                //TODO notify user cannot write preference file
                logger.finest(qioe.getMessage());
            }
        }

        // create the server combo list; mark default (if any)
        createServerList();
        // indicate no item selected
        serverComboBox.setSelectedIndex(-1);
        // ignore server selection events before user makes a selection
        ignoreServerSelection = false;
        // disable the default buttons
        asDefaultButton.setEnabled(false);
        noDefaultButton.setEnabled(false);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        stateFilePanel = new javax.swing.JPanel();
        selectedWorkbenchLabel = new javax.swing.JLabel();
        addWorkbenchButton = new javax.swing.JButton();
        workbenchLabel = new javax.swing.JLabel();
        instructionLabel1 = new javax.swing.JLabel();
        instructionLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        workbenchList = new javax.swing.JList();
        removeWorkbenchButton = new javax.swing.JButton();
        removeAllButton = new javax.swing.JButton();
        asDefaultButton = new javax.swing.JButton();
        noDefaultButton = new javax.swing.JButton();
        closeButton = new javax.swing.JButton();
        helpButton = new javax.swing.JButton();
        instructionLabel4 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        serverlabel = new javax.swing.JLabel();
        serverComboBox = new javax.swing.JComboBox();
        instructionsLabel3 = new javax.swing.JLabel();
        qispacePanel = new javax.swing.JPanel();
        serverlabel1 = new javax.swing.JLabel();
        qispaceComboBox = new javax.swing.JComboBox();
        browseButton = new javax.swing.JButton();

        setTitle("Edit Servers");
        setModal(true);
        stateFilePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 255)), "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(153, 84, 20)));
        selectedWorkbenchLabel.setText("Selected workbench:");

        addWorkbenchButton.setText("Add");
        addWorkbenchButton.setEnabled(false);
        addWorkbenchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addWorkbenchButtonActionPerformed(evt);
            }
        });

        //workbenchLabel.setText("<selected workbench>");
        workbenchLabel.setText("");

        org.jdesktop.layout.GroupLayout stateFilePanelLayout = new org.jdesktop.layout.GroupLayout(stateFilePanel);
        stateFilePanel.setLayout(stateFilePanelLayout);
        stateFilePanelLayout.setHorizontalGroup(
            stateFilePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(stateFilePanelLayout.createSequentialGroup()
                .add(selectedWorkbenchLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(workbenchLabel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 314, Short.MAX_VALUE))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, stateFilePanelLayout.createSequentialGroup()
                .addContainerGap(369, Short.MAX_VALUE)
                .add(addWorkbenchButton))
        );
        stateFilePanelLayout.setVerticalGroup(
            stateFilePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(stateFilePanelLayout.createSequentialGroup()
                .add(stateFilePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(selectedWorkbenchLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(workbenchLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(addWorkbenchButton))
        );

        instructionLabel1.setText("You can specify which workbench session files associated with a project to use.");
        instructionLabel1.setForeground(new java.awt.Color(102, 102, 255));

        instructionLabel2.setText("To add a new session file, first select an allowed runtime Tomcat server.");
        instructionLabel2.setForeground(new java.awt.Color(102, 102, 255));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 255)), "Workbenches", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(153, 84, 20)));
        jScrollPane1.setViewportView(workbenchList);

        removeWorkbenchButton.setText("Remove Workbench");
        removeWorkbenchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeWorkbenchButtonActionPerformed(evt);
            }
        });

        removeAllButton.setText("Remove All");
        removeAllButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeAllButtonActionPerformed(evt);
            }
        });

        asDefaultButton.setText("As Default");
        asDefaultButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                asDefaultButtonActionPerformed(evt);
            }
        });

        noDefaultButton.setText("No Default");
        noDefaultButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                noDefaultButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(removeWorkbenchButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(removeAllButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(asDefaultButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(noDefaultButton)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(removeWorkbenchButton)
                    .add(removeAllButton)
                    .add(asDefaultButton)
                    .add(noDefaultButton))
                .addContainerGap())
        );

        closeButton.setText("Close");
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });

        helpButton.setText("Help");
        helpButton.addActionListener(new CSH.DisplayHelpFromSource(helpBroker));
/*
        helpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                helpButtonActionPerformed(evt);
            }
        });
*/
        instructionLabel4.setText("Then browse to a project's workbench session file you want to add.");
        instructionLabel4.setForeground(new java.awt.Color(102, 102, 255));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 255))));
        serverlabel.setText("Server:");

        serverComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                serverComboBoxActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(serverlabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(serverComboBox, 0, 380, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                .add(serverlabel)
                .add(serverComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        instructionsLabel3.setText("Next select a valid qiProject accessible from the server.");
        instructionsLabel3.setForeground(new java.awt.Color(102, 102, 255));

        qispacePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        serverlabel1.setText("qiProject:");

        qispaceComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                qispaceComboBoxActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout qispacePanelLayout = new org.jdesktop.layout.GroupLayout(qispacePanel);
        qispacePanel.setLayout(qispacePanelLayout);
        qispacePanelLayout.setHorizontalGroup(
            qispacePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(qispacePanelLayout.createSequentialGroup()
                .add(serverlabel1)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(qispaceComboBox, 0, 378, Short.MAX_VALUE))
        );
        qispacePanelLayout.setVerticalGroup(
            qispacePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(qispacePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                .add(serverlabel1)
                .add(qispaceComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        browseButton.setText("Browse");
        browseButton.setEnabled(false);
        browseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .add(layout.createSequentialGroup()
                        .add(stateFilePanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .add(layout.createSequentialGroup()
                        .add(instructionLabel1)
                        .addContainerGap(66, Short.MAX_VALUE))
                    .add(instructionLabel2)
                    .add(layout.createSequentialGroup()
                        .add(instructionsLabel3)
                        .addContainerGap(188, Short.MAX_VALUE))
                    .add(layout.createSequentialGroup()
                        .add(instructionLabel4)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 79, Short.MAX_VALUE)
                        .add(browseButton)
                        .addContainerGap())
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(helpButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(closeButton)
                        .addContainerGap())
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(qispacePanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(instructionLabel1)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(instructionLabel2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(instructionsLabel3)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(qispacePanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(instructionLabel4)
                    .add(browseButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(stateFilePanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(helpButton)
                    .add(closeButton))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void qispaceComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_qispaceComboBoxActionPerformed
        // ignore event when list initialized
        if (ignoreQispaceSelection) return;

        selectedQispace = (String)qispaceComboBox.getSelectedItem();
        // nothing to do if no item has been selected
        if (selectedQispace == null) return;

        if (selectedQispace.endsWith(" (default)")) {
            selectedQispace = selectedQispace.substring(0, selectedQispace.length()-10);
            if (selectedDefaultServer) {
                // enable the default buttons
                asDefaultButton.setEnabled(true);
                noDefaultButton.setEnabled(true);
            }
        } else {
            // disable the default buttons. Can only set a default workbench
            // for a default qiSpace associated with a default server.
            asDefaultButton.setEnabled(false);
            noDefaultButton.setEnabled(false);
        }

        // get the workbenches associated with the project
        updateWorkbenchList(false);
        // enable the Browse button
        browseButton.setEnabled(true);
        // disable the Add button
        addWorkbenchButton.setEnabled(false);
        // clear the selected workbench
        workbenchLabel.setText("");
        selectedWorkbench = "";
    }//GEN-LAST:event_qispaceComboBoxActionPerformed

    private void serverComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_serverComboBoxActionPerformed
        // ignore event when list initialized
        if (ignoreServerSelection) return;

        selectedServer = (String)serverComboBox.getSelectedItem();
        // nothing to do if no item has been selected
        if (selectedServer == null) return;

        if (selectedServer.endsWith(" (default)")) {
            selectedServer = selectedServer.substring(0, selectedServer.length()-10);
            selectedDefaultServer = true;
        } else {
            selectedDefaultServer = false;
            // disable the default buttons. Can only select a default
            // qiSpace associated with a default server.
            asDefaultButton.setEnabled(false);
            noDefaultButton.setEnabled(false);
        }

        // get the qiSpaces associated with the server and
        // create the qiSpace combo list; mark default (if any)
        createQispaceList();
        // indicate no item selected
        qispaceComboBox.setSelectedIndex(-1);
        // ignore qiSpace selection events before user makes a selection
        ignoreQispaceSelection = false;
    }//GEN-LAST:event_serverComboBoxActionPerformed

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_closeButtonActionPerformed

    private void browseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseButtonActionPerformed
        // user pressed Browse button
        String selectedServer = (String)serverComboBox.getSelectedItem();
        if(selectedServer == null)
            return;
        String selectedQispace = (String)qispaceComboBox.getSelectedItem();
        if(selectedQispace == null)
            return;

        int ind = selectedQispace.indexOf("(default)");
        if(ind != -1){
            selectedQispace = selectedQispace.substring(0, ind).trim();
        }
        String[] cfgExtensions = new String[]{"cfg", "CFG"};
        FileFilter cfgFilter = (FileFilter) new GenericFileFilter(cfgExtensions, "Configuration File (*.cfg)");
        agent.browseDesktops(this,"Select a desktop",selectedQispace,cfgFilter,QIWConstants.FILE_CHOOSER_TYPE_OPEN,"",selectedServer);

        workbenchLabel.setText(selectedWorkbench);
        // enable Add button
        addWorkbenchButton.setEnabled(true);
    }//GEN-LAST:event_browseButtonActionPerformed

    public void setWorkbenchLabel(String label){
        workbenchLabel.setText(label);
        selectedWorkbench = label;
        addWorkbenchButton.setEnabled(true);
    }
    private void helpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_helpButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_helpButtonActionPerformed

    private void noDefaultButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_noDefaultButtonActionPerformed
        // NOTE: The No Default button is only enabled if the default
        // server and a default qiSpace were selected.
        // do nothing if no default workbench for the default qiSpace
        String defaultWorkbench = userPrefs.getDefaultDesktop(selectedServer, selectedQispace);
        if (defaultWorkbench.equals(QiwbPreferences.NO_DEFAULT)) return;

        userPrefs.setDefaultDesktop(selectedServer, selectedQispace, QiwbPreferences.NO_DEFAULT);
        updateWorkbenchList(true);
    }//GEN-LAST:event_noDefaultButtonActionPerformed

    private void asDefaultButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_asDefaultButtonActionPerformed
        // get selected list item
        int idx = workbenchList.getSelectedIndex();
        // nothing to do if no server selected
        if (idx == -1) return;

        String selectedWorkbench = workbenches[idx];
        if (selectedWorkbench.endsWith(" (default)")) {
            selectedWorkbench = selectedWorkbench.substring(0, selectedWorkbench.length()-10);
        }
        int status = userPrefs.setDefaultDesktop(selectedServer, selectedQispace, selectedWorkbench);
        // nothing to do if selected the default
        if (status == 0) return;

        updateWorkbenchList(true);
    }//GEN-LAST:event_asDefaultButtonActionPerformed

    private void removeAllButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeAllButtonActionPerformed
        userPrefs.removeAllDesktops(selectedServer, selectedQispace);
        updateWorkbenchList(true);
    }//GEN-LAST:event_removeAllButtonActionPerformed

    private void removeWorkbenchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeWorkbenchButtonActionPerformed
        // get selected list item
        int idx = workbenchList.getSelectedIndex();
        // nothing to do if no server selected
        if (idx == -1) return;

        //Note: if selected workbench is the default, there will be
        //no default.
        String selectedWorkbench = workbenches[idx];
        if (selectedWorkbench.endsWith(" (default)")) {
            selectedWorkbench = selectedWorkbench.substring(0, selectedWorkbench.length()-10);
        }
        userPrefs.removeDesktop(selectedServer, selectedQispace, selectedWorkbench);

        updateWorkbenchList(true);
    }//GEN-LAST:event_removeWorkbenchButtonActionPerformed

    private void addWorkbenchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addWorkbenchButtonActionPerformed
        // user pressed Add button
        // add to list

        if (!selectedWorkbench.equals("")) {
            int status = userPrefs.addDesktop(selectedServer, selectedQispace, selectedWorkbench);
            if (status == 1) {
                // Workbench added to the list; repaint JList
                updateWorkbenchList(true);
            }
        }
    }//GEN-LAST:event_addWorkbenchButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addWorkbenchButton;
    private javax.swing.JButton asDefaultButton;
    private javax.swing.JButton browseButton;
    private javax.swing.JButton closeButton;
    private javax.swing.JButton helpButton;
    private javax.swing.JLabel instructionLabel1;
    private javax.swing.JLabel instructionLabel2;
    private javax.swing.JLabel instructionLabel4;
    private javax.swing.JLabel instructionsLabel3;
    private javax.swing.JList workbenchList;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton noDefaultButton;
    private javax.swing.JComboBox qispaceComboBox;
    private javax.swing.JPanel qispacePanel;
    private javax.swing.JButton removeAllButton;
    private javax.swing.JButton removeWorkbenchButton;
    private javax.swing.JLabel selectedWorkbenchLabel;
    private javax.swing.JComboBox serverComboBox;
    private javax.swing.JLabel serverlabel;
    private javax.swing.JLabel serverlabel1;
    private javax.swing.JPanel stateFilePanel;
    private javax.swing.JLabel workbenchLabel;
    // End of variables declaration//GEN-END:variables

    /**
     * Create the server combo list, marking the default URL (if any)
     */
    private void createServerList() {
        serverComboBox.removeAllItems();

        // get list of allowed servers
        validServers = userPrefs.getServerURLs();
        if (servers.length != validServers.size()) {
            servers = new String[validServers.size()];
        }
        servers = validServers.toArray(servers);
        java.util.Arrays.sort(servers);

        // mark the default URL (if any)
        String defaultServer = userPrefs.getDefaultServer();
        if (!defaultServer.equals(QiwbPreferences.NO_DEFAULT)) {
            // find the default server in the list
            for (int i=0; i<servers.length; i++) {
                if (servers[i].equals(defaultServer)) {
                    servers[i] += " (default)";
                }
            }
        }

        for (int i=0; i<servers.length; i++) {
            serverComboBox.addItem(servers[i]);
        }
    }

    /**
     * Create the qiSpace combo list, marking the default qiSpace (if any)
     */
    private void createQispaceList() {
        qispaceComboBox.removeAllItems();

        // get list of allowed qiSpaces for the selected server
        validQispaces = userPrefs.getServerQispaces(selectedServer);
        if (qispaces.length != validQispaces.size()) {
            qispaces = new String[validQispaces.size()];
        }
        //qispaces = validQispaces.toArray(qispaces);
        for (int i=0; i<validQispaces.size(); i++) {
            qispaces[i] = validQispaces.get(i).getPath();
        }
        java.util.Arrays.sort(qispaces);

        // mark the default qiSpace (if any)
        String defaultQispace = userPrefs.getDefaultQispace(selectedServer);
        if (!defaultQispace.equals(QiwbPreferences.NO_DEFAULT)) {
            // find the default server in the list
            for (int i=0; i<qispaces.length; i++) {
                if (qispaces[i].equals(defaultQispace)) {
                    qispaces[i] += " (default)";
                }
            }
        }

        for (int i=0; i<qispaces.length; i++) {
            qispaceComboBox.addItem(qispaces[i]);
        }
    }

    /**
     * Update the workbench list, marking the default session file (if any)
     *
     * @param updatePrefs true if preference file should be rewritten
     */
    private void updateWorkbenchList(boolean updatePrefs) {
        // get list of allowed workbenches for the selected qiSpace
        validWorkbenches = userPrefs.getStateFiles(selectedServer, selectedQispace);
        if (workbenches.length != validWorkbenches.size()) {
            workbenches = new String[validWorkbenches.size()];
        }
        workbenches = validWorkbenches.toArray(workbenches);
        java.util.Arrays.sort(workbenches);
        workbenchList.setListData(workbenches);

        // mark the default state file (if any)
        String defaultWorkbench = userPrefs.getDefaultDesktop(selectedServer, selectedQispace);
        if (!defaultWorkbench.equals(QiwbPreferences.NO_DEFAULT)) {
            // find the default qiSpace in the list
            for (int i=0; i<workbenches.length; i++) {
                if (workbenches[i].equals(defaultWorkbench)) {
                    workbenches[i] += " (default)";
                }
            }
        }

        // update preferences
        if (updatePrefs) {
            // write out new preferences.
            String prefFileDir = userHOME + File.separator + QIWConstants.QIWB_WORKING_DIR;
            try {
                PreferenceUtils.writePrefs(userPrefs, prefFileDir);
            } catch (QiwIOException qioe) {
                //TODO notify user cannot write preference file
                logger.finest(qioe.getMessage());
            }
        }
    }
}
