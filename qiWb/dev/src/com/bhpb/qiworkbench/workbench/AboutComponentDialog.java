/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.workbench;

import java.util.ArrayList;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
/**
 * Dialog to display component details accessed from AboutDialog.java
 *
 * @author  Marcus Vaal
 * @version 1.0
 */
public class AboutComponentDialog extends javax.swing.JDialog {

    /**
     * Creates an instance of a Component Dialog
     */
    public AboutComponentDialog() {
    	initComponents();
    }

    /**
     * Initializes the components
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        String msgKind;
        String cmd = "";
        buttonPane = new javax.swing.JPanel();
        okButton = new javax.swing.JButton();
        moreDetailsButton = new javax.swing.JButton();
        componentTablePane = new javax.swing.JPanel();
        componentTableScrollPane = new javax.swing.JScrollPane();
        componentTable = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });

        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        moreDetailsButton.setText("More Details");
        moreDetailsButton.setEnabled(false);
        moreDetailsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moreDetailsButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout buttonPaneLayout = new org.jdesktop.layout.GroupLayout(buttonPane);
        buttonPane.setLayout(buttonPaneLayout);
        buttonPaneLayout.setHorizontalGroup(
            buttonPaneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(buttonPaneLayout.createSequentialGroup()
                .addContainerGap()
                .add(moreDetailsButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 413, Short.MAX_VALUE)
                .add(okButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 81, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        buttonPaneLayout.setVerticalGroup(
            buttonPaneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, buttonPaneLayout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(buttonPaneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(moreDetailsButton)
                    .add(okButton))
                .addContainerGap())
        );

        componentTableScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        componentTableScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        componentTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        componentTable.getTableHeader().setReorderingAllowed(false);

    Object [][] createdTable = createTable();

        componentTable.setModel(new javax.swing.table.DefaultTableModel( createdTable, new String [] {"Provider", "Component", "Version", "Component Id"})  {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });

        componentTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        componentTable.setFocusable(false);
        componentTable.getColumnModel().getColumn(0).setPreferredWidth(153);
        componentTable.getColumnModel().getColumn(1).setPreferredWidth(153);
        componentTable.getColumnModel().getColumn(2).setPreferredWidth(153);
        //componentTable.getColumnModel().getColumn(3).setPreferredWidth(this.getWidth() - 28 - 153 - 153 - 153);
        //612 across table, scrollbar is 28 pixels

        componentTable.getTableHeader().addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                componentTableMouseDragged(evt);
            }
        });

        componentTable.getTableHeader().addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                componentTableHeaderMouseReleased(evt);
            }
        });

        componentTable.getTableHeader().addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                componentTableHeaderMousePressed(evt);
            }
        });

        componentTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                componentTableMouseReleased(evt);
            }
        });

        componentTableScrollPane.setViewportView(componentTable);

        org.jdesktop.layout.GroupLayout componentTablePaneLayout = new org.jdesktop.layout.GroupLayout(componentTablePane);
        componentTablePane.setLayout(componentTablePaneLayout);
        componentTablePaneLayout.setHorizontalGroup(
            componentTablePaneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(componentTableScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 607, Short.MAX_VALUE)
        );
        componentTablePaneLayout.setVerticalGroup(
            componentTablePaneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(componentTableScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(buttonPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(componentTablePane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(componentTablePane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(buttonPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Change scroll policy and column width based on the size of the window and table when the window is being resized
     * @param evt event when dialog is resized
     */
    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        if(componentTable.getColumnModel().getColumn(3).getWidth() > 157 || this.getWidth() - getCurrentColumnWidth() > 0) {
            int setNewWidth = this.getWidth() - getCurrentColumnWidth() + componentTable.getColumnModel().getColumn(3).getWidth();
            componentTable.getColumnModel().getColumn(3).setPreferredWidth(setNewWidth);
        }
        if(getCurrentColumnWidth() < this.getWidth()) {
            componentTableScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        }
        else if(getCurrentColumnWidth() > this.getWidth()) {
            componentTableScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        }
    }//GEN-LAST:event_formComponentResized

    /**
     * When a table column is resized, and the table is greater than the window, add a horizontal scroll bar at the bottom,
     * if it is smaller, remove the scroll bar
     * @param evt event when table columns are being resized
     */
    private void componentTableMouseDragged(java.awt.event.MouseEvent evt) {
        if(getCurrentColumnWidth() > this.getWidth()) {
            componentTableScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        } else {
            componentTableScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        }
    }

    /**
     * Makes sure that autoResizing is off when changing the size of the columns
     * @param evt event when table columns are being resized
     */
    private void componentTableHeaderMousePressed(java.awt.event.MouseEvent evt) {
        componentTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
    }

    /**
     * If the table header is resized and is smaller than the window size, the last column is snapped back to fit the size of the window
     * @param evt event when table columns are being resized, but the table is smaller than the window
     */
    private void componentTableHeaderMouseReleased(java.awt.event.MouseEvent evt) {
    	// TODO Fix so that when a column is being resized, it resizes itself as it is being dragged rather than seeing the grey area and
	// snapping to the edge when the mouse is released
        if(getCurrentColumnWidth() < this.getWidth()) {
            int setNewWidth = this.getWidth() - getCurrentColumnWidth() + componentTable.getColumnModel().getColumn(3).getWidth();
            componentTable.getColumnModel().getColumn(3).setPreferredWidth(setNewWidth);
        }
        componentTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
    }

    /**
     * Returns the width of the columns including the width of the scroll bar
     *
     * @return int - integer representing the width of the first 4 columns and the width of the vertical scroll bar
     */
    private int getCurrentColumnWidth() {
        return componentTable.getColumnModel().getColumn(0).getWidth() +
                componentTable.getColumnModel().getColumn(1).getWidth() +
                componentTable.getColumnModel().getColumn(2).getWidth() +
                componentTable.getColumnModel().getColumn(3).getWidth() + 28;
    }

    /**
     * If the the table is clicked, checks if a row is highlighted, if highlighted activate the details button
     * @param evt event when table columns are being resized, but the table is smaller than the window
     */
    private void componentTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_componentTableMouseReleased
        if(componentTable.getSelectedRow() != -1) {
            moreDetailsButton.setEnabled(true);
        }
    }//GEN-LAST:event_componentTableMouseReleased

    /**
     * If the more details button is pressed, store the component ID
     * @param evt event when 'More Details' button is pressed
     */
    private void moreDetailsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moreDetailsButtonActionPerformed
	// TODO add handling code here:
        String componentIdValue = (String)componentTable.getValueAt(componentTable.getSelectedRow(),3);
    }//GEN-LAST:event_moreDetailsButtonActionPerformed

    /**
     * Closes the Component Dialog Window
     * @param evt event when 'OK' button is pressed
     */
    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_okButtonActionPerformed

    /**
     * Creates a table containing Provider, Component Name, Version of Component, and Component ID
     * @return Object[][] - table of size determined based on the number of components found
     */
    private Object [][] createTable() {
    ArrayList<ComponentDescriptor> allAvailableComponentsArray = MessageDispatcher.getInstance().getAllAvailableComponents();

    int tableHeight = allAvailableComponentsArray.size();
    // TODO dynamically create and change the number of rows in the table based on the size of the window
    // i.e., if the window is made larger, add more rows rather than showing grey area where rows do not exist, and if there
    // are blank rows, and the window is made smaller, delete the blank rows rather than have the table scroll the blank rows

    // If size is less than 14, creates a table that fills the size of the window
    if(tableHeight < 14) {
        tableHeight = 14;
    }
    // Create table object with 4 columns and the amount of rows needed
    Object [][] createComponentTable = new Object [tableHeight][4];
    for(int i = 0; i < allAvailableComponentsArray.size(); i++){
        for(int j = 0; j < 4; j++) {
            if(j == 0) { //column 1
                createComponentTable[i][j] = allAvailableComponentsArray.get(i).getProviderName();
            }
            else if(j == 1) { //column 2
                createComponentTable[i][j] = allAvailableComponentsArray.get(i).getDisplayName();
            }
            else if(j == 2){ //column 3
                createComponentTable[i][j] = allAvailableComponentsArray.get(i).getVersionNumber();
            }
            else { // j == 3, column 4
                createComponentTable[i][j] = allAvailableComponentsArray.get(i).getCID();
            }
        }
    }

    return createComponentTable;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel buttonPane;
    private javax.swing.JTable componentTable;
    private javax.swing.JPanel componentTablePane;
    private javax.swing.JScrollPane componentTableScrollPane;
    private javax.swing.JButton moreDetailsButton;
    private javax.swing.JButton okButton;
    // End of variables declaration//GEN-END:variables
}
