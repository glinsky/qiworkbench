/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench.workbench;

import com.bhpb.qiworkbench.compAPI.MDIDesktopPane;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.InputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.help.CSH;
import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.compAPI.IconResource;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;

/**
 * Set up the QI desktop.
 *
 * @author Bob Miller
 * @author Gil Hansen
 * @version 1.1
 *
 */
public class WorkbenchGUI extends JFrame implements IqiWorkbenchComponent {

    private static Logger logger = Logger.getLogger(WorkbenchGUI.class.getName());
    /** GUI CID */
    private String myCID = "";
    // GUI name
    public static final String WB_MGR_GUI_NAME = "Workbench Manager GUI";
    private WorkbenchManager agent;
    private JMenuBar menuBar;
    private int menuItemType = 0;
    private int menuType = 1;
    private MDIDesktopPane desktop;
    private JPanel workbenchTreePanel;
    // top-level menus
    private static final String[] menuNames = {"Workbench", "Components", "Service", "Window", "Help"};
    private static final int[] menuKeys = {KeyEvent.VK_W, KeyEvent.VK_C, KeyEvent.VK_S, KeyEvent.VK_I, KeyEvent.VK_H};
    // number of menuItems and menus for each top-level menu
    private String[] componentMenuItemTexts = {QIWMenuConstants.MENU_TEXT_OPEN, QIWMenuConstants.MENU_TEXT_SAVE,
        QIWMenuConstants.MENU_TEXT_SAVE_AS, QIWMenuConstants.MENU_TEXT_SAVE_QUIT, QIWMenuConstants.MENU_TEXT_QUIT,
        QIWMenuConstants.MENU_TEXT_DELETE, QIWMenuConstants.MENU_TEXT_RENAME
    };
    private List<WorkbenchMenuDescriptor> workbenchMenuDescriptors = new ArrayList<WorkbenchMenuDescriptor>();
    private List<WorkbenchMenuDescriptor> componentMenuDescriptors = new ArrayList<WorkbenchMenuDescriptor>();
    private List<WorkbenchMenuDescriptor> serviceMenuDescriptors = new ArrayList<WorkbenchMenuDescriptor>();
    private List<WorkbenchMenuDescriptor> windowMenuDescriptors = new ArrayList<WorkbenchMenuDescriptor>();
    private List<WorkbenchMenuDescriptor> helpMenuDescriptors = new ArrayList<WorkbenchMenuDescriptor>();
    private WorkbenchTreeManager workbenchTree;
    private boolean initFinished = false;
    private boolean initSuccessful = false;
    private List<Exception> initExceptions = new ArrayList();
    private javax.swing.JLabel statusBar = new javax.swing.JLabel();
    HelpBroker helpBroker;
    HelpSet helpSet;

    public boolean isInitFinished() {
        return initFinished;
    }

    public boolean isInitSuccessful() {
        return initSuccessful;
    }

    public List getInitExceptions() {
        return initExceptions;
    }

    /**
     * Get GUI CID
     * @return CID string
     */
    public String getCID() {
        return myCID;
    }

    // messaging manager for Workbench GUI
    private MessagingManager messagingMgr;

    public MessagingManager getMsgMgr() {
        return messagingMgr;
    }

    public String[] getComponentMenuItemTexts() {
        return componentMenuItemTexts;
    }

    public List<WorkbenchMenuDescriptor> getWorkbenchMenuDescriptors() {
        return workbenchMenuDescriptors;
    }

    public WorkbenchManager getAgent() {
        return agent;
    }

    public String getWorkbenchActionCommandByLabel(String label) {
        for (WorkbenchMenuDescriptor d : workbenchMenuDescriptors) {
            if (d.getLabel().equals(label)) {
                return d.getActionCommand();
            }
        }
        return null;
    }

    public String getComponentActionCommandByLabel(String label) {
        for (WorkbenchMenuDescriptor d : componentMenuDescriptors) {
            if (d.getLabel().equals(label)) {
                return d.getActionCommand();
            }
        }
        return null;
    }

    public List<WorkbenchMenuDescriptor> getComponentMenuDescriptors() {
        return componentMenuDescriptors;
    }

    public List<WorkbenchMenuDescriptor> getHelpMenuDescriptors() {
        return helpMenuDescriptors;
    }

    public int getMenuDescriptorIndex(String action) {
        for (int i = 0; i < workbenchMenuDescriptors.size(); i++) {
            WorkbenchMenuDescriptor wmd = workbenchMenuDescriptors.get(i);
            if (wmd.getActionCommand().equals(action)) {
                return i;
            }
        }
        for (int i = 0; i < componentMenuDescriptors.size(); i++) {
            WorkbenchMenuDescriptor wmd = componentMenuDescriptors.get(i);
            if (wmd.getActionCommand().equals(action)) {
                return i;
            }
        }
        for (int i = 0; i < windowMenuDescriptors.size(); i++) {
            WorkbenchMenuDescriptor wmd = windowMenuDescriptors.get(i);
            if (wmd.getActionCommand().equals(action)) {
                return i;
            }
        }
        for (int i = 0; i < helpMenuDescriptors.size(); i++) {
            WorkbenchMenuDescriptor wmd = helpMenuDescriptors.get(i);
            if (wmd.getActionCommand().equals(action)) {
                return i;
            }
        }
        return -1;
    }

    public WorkbenchMenuDescriptor getMenuDescriptor(String action) {
        for (int i = 0; i < workbenchMenuDescriptors.size(); i++) {
            WorkbenchMenuDescriptor wmd = workbenchMenuDescriptors.get(i);
            if (wmd.getActionCommand().equals(action)) {
                return wmd;
            }
        }
        for (int i = 0; i < componentMenuDescriptors.size(); i++) {
            WorkbenchMenuDescriptor wmd = componentMenuDescriptors.get(i);
            if (wmd.getActionCommand().equals(action)) {
                return wmd;
            }
        }
        for (int i = 0; i < windowMenuDescriptors.size(); i++) {
            WorkbenchMenuDescriptor wmd = windowMenuDescriptors.get(i);
            if (wmd.getActionCommand().equals(action)) {
                return wmd;
            }
        }
        for (int i = 0; i < helpMenuDescriptors.size(); i++) {
            WorkbenchMenuDescriptor wmd = helpMenuDescriptors.get(i);
            if (wmd.getActionCommand().equals(action)) {
                return wmd;
            }
        }

        return null;
    }

    /**
     * Initialize the plugin's GUI component:
     * <ul>
     * <li>Create its messaging manager</li>
     * <li>Create its CID</li>
     * <li>Register with the Message Dispatcher</li>
     * </ul>
     */
    public void init() {
        try {
            // following instruction executed under LOCK
            QIWConstants.SYNC_LOCK.lock();
            // save messaging mgr
            messagingMgr = new MessagingManager();
            myCID = messagingMgr.genCID(WorkbenchGUI.class);
            // register self with the Message Dispatcher
            messagingMgr.registerComponent(QIWConstants.WORKBENCH_GUI_COMP, WB_MGR_GUI_NAME, myCID);
            MessageDispatcher.getInstance().getComponentDesc(myCID);
            // notify the startup method initialization finished
            QIWConstants.SYNC_LOCK.unlock();
            initSuccessful = true;
        } catch (Exception e) {
            initSuccessful = false;
            initExceptions.add(e);
            logger.severe("Exception caught in RemoteWriterService.init(): " + e.getMessage());
        } finally {
            initFinished = true;
        }
    }

    /**
     * Constructor to create a menu bar and a JTree.
     */
    public WorkbenchGUI(WorkbenchManager agent) {
        // set up online Help
        try {
            ClassLoader cl = WorkbenchGUI.class.getClassLoader();
            String helpsetName = "com/bhpb/qiwbHelp/qiwb_helpset.hs";
            URL helpSetURL = HelpSet.findHelpSet(cl, helpsetName);
            helpSet = new HelpSet(cl, helpSetURL);
            helpBroker = helpSet.createHelpBroker();
        } catch (Exception e) {
            logger.warning("Error loading workbench online Help: " + e.getMessage());
        }

        init();

        // save WBMgr CID for message passing as needed
        WorkbenchData.wbMgrCID = agent.getCID();
        this.agent = agent;
        // save my CID
        WorkbenchData.wbMgrGUICID = myCID;

        if (WorkbenchData.verbose > 0) {
            logger.info("Starting WorkbenchManagerGUI with project " + agent.getProject());
        }
        String versionProp = "";
        Properties props = new Properties();
        Class me = WorkbenchGUI.class;
        InputStream in = me.getResourceAsStream("/version.properties");
        if (in == null) {
            String errorMessage = "The file /version.properties cannot be found as a resource of the qiWorkbench client.  The workbench cannot start.  Contact technical support.";
            logger.warning(errorMessage);
            JOptionPane.showConfirmDialog(this, errorMessage);
            throw new RuntimeException("Resource /version.properties cannot be found.");
        }
        try {
            props.load(in);
        } catch (IOException ioe) {
            //logger.warning("IO exception reading version.properties file:" + ioe.getMessage());
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
            }
        }
        versionProp = props.getProperty("project.version");
        setTitle("QI Workbench Version " + versionProp + " - qiProject:  " + agent.getQispace());
        setSize(WorkbenchData.DEFAULT_WORKBENCH_WIDTH, WorkbenchData.DEFAULT_WORKBENCH_HEIGHT);
        setLocation(WorkbenchData.DEFAULT_WORKBENCH_LOCATION_X, WorkbenchData.DEFAULT_WORKBENCH_LOCATION_Y);
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                WorkbenchAction.quitDesktop();
            }
        });


        //  set menu items
        setJMenuBar(createMenuBar());
        desktop = new MDIDesktopPane();
        desktop.setAutoscrolls(true);

        WorkbenchData.desktop = desktop;
        workbenchTree = new WorkbenchTreeManager(this, messagingMgr);
        workbenchTreePanel = new JPanel(new GridLayout(1, 1));
        JScrollPane scrollUpper = new JScrollPane(WorkbenchTreeManager.getComponentTree());
        workbenchTreePanel.add(scrollUpper);

        // left-right split pane
        JSplitPane sh = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, workbenchTreePanel, new JScrollPane(desktop));
        CSH.setHelpIDString(sh, "overview.concepts.whatisQiwb");
        sh.setOneTouchExpandable(true);
        getContentPane().add(sh, java.awt.BorderLayout.CENTER);
        getContentPane().add(statusBar, java.awt.BorderLayout.SOUTH);

        helpBroker.enableHelpKey(this.getRootPane(), "top", helpSet, "javax.help.SecondaryWindow", null);
    }

    public WorkbenchTreeManager getWorkbenchTreeManager() {
        return workbenchTree;
    }

    public JDesktopPane getDesktopPane() {
        return desktop;
    }

    public JPanel getWorkbenchTreePanel() {
        return workbenchTreePanel;
    }

    public void setStatusMessage(String message) {
        statusBar.setText(message);
    }

    /**
     * Build pull-down menus for workbench GUI.
     * Contents of menus are defined in WorkbenchData.
     * @return JMenuBar
     */
    @SuppressWarnings("unchecked")
    private JMenuBar createMenuBar() {

        ActionListener menuListener = new WorkbenchMenuListener(this);

        // menu Descriptors
        // each menu and menuitem has a menuDescriptor that is an ArrayList containing:
        // String               label           name of this menu or menuitem
        // int                  type            menuType(1) or menuItemType(0)
        // JMenu                thisMenu        JMenu pointer for this menu
        // String               actionCommand   action command associated with this menu item
        // ActionListener       actionListener  pointer to listener for this menu item
        // JFrame               frame           pointer to container holding this menu
        // ArrayList<JMenuItem> meunItems       pointers to JMenuItems under JMenu


        workbenchMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_NEW, menuItemType, null,
                QIWMenuConstants.MENU_DESKTOP_NEW_CMD, menuListener,
                this, null, false));

        workbenchMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_OPEN, menuItemType, null,
                QIWMenuConstants.MENU_DESKTOP_OPEN_CMD, menuListener,
                this, new ArrayList<JMenuItem>(10), true));

        workbenchMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_SAVE, menuItemType, null,
                QIWMenuConstants.MENU_DESKTOP_SAVE_CMD,
                menuListener, this, null, true));

        workbenchMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_SAVE_AS, menuItemType, null,
                QIWMenuConstants.MENU_DESKTOP_SAVEAS_CMD,
                menuListener, this, null, true));

        workbenchMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_SAVE_QUIT, menuItemType, null,
                QIWMenuConstants.MENU_DESKTOP_SAVE_QUIT_CMD, menuListener,
                this, null, true));

        workbenchMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_QUIT, menuItemType, null, QIWMenuConstants.MENU_DESKTOP_QUIT_CMD,
                menuListener, this, null, true));

        workbenchMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_RENAME, menuItemType, null,
                QIWMenuConstants.MENU_DESKTOP_RENAME_CMD, menuListener,
                this, null, false));

        workbenchMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_PREFERENCES, menuItemType, null,
                QIWMenuConstants.MENU_DESKTOP_PREFERENCES_CMD, menuListener,
                this, null, true));

        componentMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_NEW, menuType, null, QIWMenuConstants.MENU_COMPONENT_NEW_CMD,
                menuListener, this, new ArrayList<JMenuItem>(), true));

        componentMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_OPEN, menuType, null, QIWMenuConstants.MENU_COMPONENT_OPEN_CMD,
                menuListener, this,
                new ArrayList<JMenuItem>(), false));

        componentMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_SAVE, menuType, null, QIWMenuConstants.MENU_COMPONENT_SAVE_CMD,
                menuListener, this,
                new ArrayList<JMenuItem>(), true));

        componentMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_SAVE_AS, menuType, null, QIWMenuConstants.MENU_COMPONENT_SAVEAS_CMD,
                menuListener, this,
                new ArrayList<JMenuItem>(), true));

        componentMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_SAVE_QUIT, menuType, null,
                QIWMenuConstants.MENU_COMPONENT_SAVE_QUIT_CMD, menuListener,
                this, new ArrayList<JMenuItem>(), true));

        componentMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_QUIT, menuType, null, QIWMenuConstants.MENU_COMPONENT_QUIT_CMD,
                menuListener, this,
                new ArrayList<JMenuItem>(10), true));

        componentMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_DELETE, menuType, null, QIWMenuConstants.MENU_COMPONENT_DELETE_CMD,
                menuListener, this,
                new ArrayList<JMenuItem>(10), true));


        componentMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_RENAME, menuType, null, QIWMenuConstants.MENU_COMPONENT_RENAME_CMD,
                menuListener, this,
                new ArrayList<JMenuItem>(), true));

        componentMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_REGISTER, menuItemType, null,
                QIWMenuConstants.MENU_COMPONENT_REGISTER_CMD, menuListener,
                this, null, false));

        componentMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_UPDATE, menuType, null, QIWMenuConstants.MENU_COMPONENT_UPDATE_CMD,
                menuListener, this, new ArrayList<JMenuItem>(), true));

        componentMenuDescriptors.add(new WorkbenchMenuDescriptor(QIWMenuConstants.MENU_TEXT_COMPONENT_MANAGER, menuItemType, null, QIWMenuConstants.MENU_COMPONENT_MANAGER_CMD,
                menuListener, this, new ArrayList<JMenuItem>(), false));
        componentMenuDescriptors.add(new WorkbenchMenuDescriptor("iComponent Center", menuItemType, null, "Component.iComponent.Center",
                menuListener, this, new ArrayList<JMenuItem>(), false));

        serviceMenuDescriptors.add(new WorkbenchMenuDescriptor("Status", menuType, null, "Service.Status",
                menuListener, this,
                new ArrayList<JMenuItem>(10), false));

        serviceMenuDescriptors.add(new WorkbenchMenuDescriptor("Quit", menuType, null, "Service.Quit",
                menuListener, this,
                new ArrayList<JMenuItem>(10), false));

        windowMenuDescriptors.add(new WorkbenchMenuDescriptor("Cascade", menuItemType, null, "Window.Cascade",
                menuListener, this, null, true));

        windowMenuDescriptors.add(new WorkbenchMenuDescriptor("Tile", menuItemType, null, "Window.Tile",
                menuListener, this, null, true));

        WorkbenchMenuDescriptor wbmDesc = new WorkbenchMenuDescriptor("Help Contents", menuItemType, null, "Help.Open",
                menuListener, this, null, true);
        helpMenuDescriptors.add(wbmDesc);

        helpMenuDescriptors.add(new WorkbenchMenuDescriptor("Context-sensitive Help", menuItemType, null, "Help.CSH",
                menuListener, this, null, true));

        helpMenuDescriptors.add(new WorkbenchMenuDescriptor("About", menuItemType, null, "Help.About",
                menuListener, this, null, true));

        List[] menuDescriptors = {workbenchMenuDescriptors, componentMenuDescriptors, serviceMenuDescriptors, windowMenuDescriptors, helpMenuDescriptors};

        // build preset menuItems
        buildSubMenus();

        menuBar = new JMenuBar();
        int type;
        WorkbenchMenuDescriptor d;
        for (int i = 0; i < menuNames.length; i++) {
            // top-level menus are not menu descriptors, just Strings
            JMenu menu = new JMenu(menuNames[i]);
            if (menuNames[i].equals("Workbench")) {
                CSH.setHelpIDString(menu, "menus.workbench");
            } else if (menuNames[i].equals("Components")) {
                CSH.setHelpIDString(menu, "menus.qicomps");
            } else if (menuNames[i].equals("Service")) {
                CSH.setHelpIDString(menu, "menus.service");
            } else if (menuNames[i].equals("Window")) {
                CSH.setHelpIDString(menu, "menus.window");
            } else if (menuNames[i].equals("Help")) {
                CSH.setHelpIDString(menu, "menus.help");
            }
            menu.setMnemonic(menuKeys[i]);
            menuBar.add(menu);
            int index = 0;
            for (index = 0; index < menuDescriptors[i].size(); index++) {
                d = (WorkbenchMenuDescriptor) menuDescriptors[i].get(index);
                type = d.getType();
                if (i == 1 && (d.getLabel().equals(QIWMenuConstants.MENU_TEXT_REGISTER)) || d.getLabel().equals(QIWMenuConstants.MENU_TEXT_COMPONENT_MANAGER)) {
                    menu.addSeparator();
                }
                if (i == 0 && (d.getLabel().equals(QIWMenuConstants.MENU_TEXT_ANALYZE_DATA) || d.getLabel().equals(QIWMenuConstants.MENU_TEXT_PREFERENCES))) {
                    menu.addSeparator();
                }
                if (type == menuType) {
                    JMenu thisMenu = new JMenu(d.getLabel());
                    menu.add(thisMenu);
                    d.setMenu(thisMenu);
                    thisMenu.setEnabled(d.getEnabled());
                    int size = d.getContents().size();
                    for (int k = 0; k < size; k++) {
                        JMenuItem item = d.getContents().get(k);
                        thisMenu.add(item);
                        item.setActionCommand(d.getActionCommand() + "." + item.getText());
                        item.addActionListener(d.getActionListener());
                    }
                } else {
                    JMenuItem item = null;
                    if (d.getActionCommand().equals("Help.Open")) {
                        item = new JMenuItem(d.getLabel());
                        item.addActionListener(new CSH.DisplayHelpFromSource(helpBroker));
                        menu.add(item);
                    } else if (d.getActionCommand().equals("Help.CSH")) {
                        ImageIcon icon = IconResource.getInstance().getImageIcon(IconResource.CSH_ICON);
                        if (icon != null) {
                            item = new JMenuItem("Context-sensitive Help", icon);
                            item.addActionListener(new CSH.DisplayHelpAfterTracking(helpBroker));
                            menu.add(item);
                        }
                        menu.addSeparator();
                    } else {
                        item = new JMenuItem(d.getLabel());
                        item.addActionListener(d.getActionListener());
                        menu.add(item);
                    }
                    item.setActionCommand(d.getActionCommand());
                    item.setEnabled(d.getEnabled());
                }
            }
        }

        return menuBar;
    }

    // populate preset subMenus using WorkbenchData lists
    private void buildSubMenus() {
        // add registeredPlugins to Component.New
        WorkbenchMenuDescriptor d = getMenuDescriptor("Component.New");
        if (d == null) {
            JOptionPane.showMessageDialog(null, "Error Starting Workbench");
        }
        List<IComponentDescriptor> list = WorkbenchManager.getInstance().getRegisteredComponentList();

        ArrayList<String> modules = new ArrayList<String>();
        modules.add(QIWConstants.WORKBENCH_COMP);
        for (IComponentDescriptor cd : list) {
            modules.add(cd.getDisplayName());
        }

        //send the list to the STATS server via the MessageDispatcher
        agent.getMessagingManager().sendRequest(QIWConstants.CMD_MSG, QIWConstants.RECORD_MODULES_CMD, QIWConstants.ARRAYLIST_TYPE, modules);

        for (IComponentDescriptor cd : list) {
            QiMenuItem item = new QiMenuItem(cd.getDisplayName(), cd);

            String compKind = cd.getComponentKind();
            if (compKind.equals(QIWConstants.PLUGIN_AGENT_COMP) ||
                    compKind.equals(QIWConstants.PLUGIN_PROJMGR_COMP) ||
                    compKind.equals(QIWConstants.VIEWER_AGENT_COMP)) {
                item.setEnabled(true);
            } else {
                item.setEnabled(false);
            }
            d.getContents().add(item);
        // Currently only Amplitude Extraction is implemented, disable others
        }

        //Build Component Update Menu
        d = getMenuDescriptor("Component.Update");
        QiMenuItem item = new QiMenuItem("Update Components", null);
        item.setEnabled(true);
        d.getContents().add(item);
        item = new QiMenuItem("Status", null);
        item.setEnabled(true);
        d.getContents().add(item);
    }
    
    protected void cascadeWindows() {
        desktop.cascadeFrames();
    }
    
    protected void tileWindows() {
        desktop.tileFrames();
    }
}