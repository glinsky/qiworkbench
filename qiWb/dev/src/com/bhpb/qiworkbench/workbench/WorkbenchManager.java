/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench.workbench;

import java.awt.Component;
import java.awt.Dimension;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.swing.Icon;
import javax.swing.JDesktopPane;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;

import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiSpaceDescriptor;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.QiwbPreferences;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.util.ComponentUtils;
import com.bhpb.qiworkbench.compAPI.IconResource;
import com.bhpb.qiworkbench.client.util.ListSelectorDialog;
import com.bhpb.qiworkbench.client.util.PreferenceUtils;
import com.bhpb.qiworkbench.client.util.QiFileChooserDescriptor;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.ProgressUtil;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.DispatcherConnector;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
import java.util.logging.Level;

/**
 * Main class for qiWorkbench. There can only be 1 instance of this class.
 * Launches the Project Selector Dialog which then launches the workbench GUI.
 *
 * @author Bob Miller.
 * @author Gil Hansen
 * @version 1.3
 */
public class WorkbenchManager extends QiComponentBase implements IqiWorkbenchComponent, Runnable {
    public static Logger logger = Logger.getLogger(WorkbenchManager.class.getName());
	
    // WorkbenchManager CID
    private String myCID;
    // WorkbenchManager Component Descriptor
    private IComponentDescriptor myDesc;
    // Full path of project directory
    private String project;
    // Full path of qiSpace directory
    private String qispace;
    private static WorkbenchManager singleton = null;
    private int xCoord = 0;
    private int yCoord = 0;
    private int xyOffset = 30;

    // messaging manager for this class only
    private MessagingManager messagingMgr;
    private JDialog dialog = null;
    private ProjectAssocDialog pdialog = null;
    private Component workbenchGUI;
    private Map<String, List<IComponentDescriptor>> registeredIComponentDescriptors = null;
	
    /** List of project manager instances */
    ArrayList<IComponentDescriptor> projMgrInstances = new ArrayList<IComponentDescriptor>();
	
	/** Session ID for this workbench instance */
	String workbenchSessionID = "";
	
	/** Hostname of the client machine */
	String clientHostname = "";

    /**
     * Get WorkbenchManager CID
     * @return CID string
     */
    protected String getCID() {
        return myCID;
    }

    public MessagingManager getMsgMgr() {
        return messagingMgr;
    }

    public void setDialog(JDialog dialog) {
        this.dialog = dialog;
    }

    public MessagingManager getMessagingManager() {
        return messagingMgr;
    }

    protected IComponentDescriptor getDesc() {
        return myDesc;
    }

    public IComponentDescriptor getComponentDescriptor() {
        return myDesc;
    }
	
	public String getWorkbenchSessionID() {
		if (workbenchSessionID.equals("")) {
			workbenchSessionID = getQiwbSessionID();
		}
		return workbenchSessionID;
	}
	
	public String getClientHostname() {
		if (clientHostname.equals("")) {
			//side affect is to get client machine's hostname
			workbenchSessionID = getQiwbSessionID();
		}
		return clientHostname;
	}

    protected String getQispace() {
        return qispace;
    }

    /**
     * @deprecated Never called.
     */
    public String getProjectDir() {
        return project;
    }

    protected String getProject() {
        return project;
    }

    /**
     * Set qiSpace in the Workbench Manager. The qiSpace was selected in the
     * Project Selector dialog, so it is in the selected server.
     *
     * @param qispaceDir The full path of the qiSpace..
     * @param qispaceKind Kind of qiSpace: "W" (workspace of projects), "P" (project),
     * @param selectedServer The selected runtime Tomcat server
     * "" (qiSpace already exists in the preferences).
     */
    public void setQispace(String qispaceDir, String qispaceKind, String selectedServer) {
        this.qispace = qispaceDir;
        //TODO: Specify project: Get the project from the qiProjectManager. If there is
        // no active qiProjectManager, either assume the qiSpace is the project, or ask
        // the user to select a project within the qiSpace.
        //NOTE: Until there is a qiProjectManager, it is assumed the qiSpace is the project
        project = this.qispace;

        QiSpaceDescriptor qsDesc = null;
        if (qispaceKind.equals("")) {
            //qiSpace exists; get its descriptor (saved in the preferences)
            QiwbPreferences userPrefs = MessageDispatcher.getInstance().getUserPrefs();
            qsDesc = userPrefs.getQispaceDesc(selectedServer, qispaceDir);
            if (qsDesc == null) {
                logger.warning("Internal error getting selected qiSpace's descriptor in the preferences");
                showInternalErrorDialog(Thread.currentThread().getStackTrace(), "Failed to find selected qiSpace in preferences");
                return;
            }
        } else {
            qsDesc = new QiSpaceDescriptor(qispaceDir, qispaceKind);
        }

        // tell Message Dispatcher to save qiSpace
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SAVE_QISPACE_CMD, QIWConstants.QISPACE_TYPE, qsDesc);
    }

    public StackTraceElement[] getStack() {
        return Thread.currentThread().getStackTrace();
    }

    /** Empty constructor
     */
    private WorkbenchManager() {
    }

    /**
     * Get the singleton instance of this class. If the message dispatcher doesn't exist,
     * create it and start as a thread.
     */
    public static WorkbenchManager getInstance() {
        if (singleton == null) {
            singleton = new WorkbenchManager();
            new Thread(singleton).start();
        }
        return singleton;
    }

    /** Initialization gets critical items needed for message routing, using lock to make sure Id's
     * are unique and then starts the WorkbenchManagerGUI.
     */
    public void init() {
        try {
            messagingMgr = new MessagingManager();

            myCID = ComponentUtils.genCID(this);
            //modified as part of making WorkbenchManager to be singleton myCID = Thread.currentThread().getName();

            long threadId = Thread.currentThread().getId();

            //register self with the Message Dispatcher
            messagingMgr.registerComponent(QIWConstants.WORKBENCH_COMP, QIWConstants.WORKBENCH_MGR_NAME, myCID, true);
            myDesc = messagingMgr.getComponentDesc(myCID);

            //get list of local services
            String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_LOCAL_SERVICES_CMD);

            //get list of remote services
            msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_REMOTE_SERVICES_CMD);

            //get list of registered plugins
            msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_PLUGINS_CMD);

            //get list of pre-registered viewer agents
            msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_VIEWER_AGENTS_CMD);

            // notify the startup method initialization finished
            QIWConstants.SYNC_LOCK.unlock();

            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in WorkbenchManager.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

    /** Initialize the WorkbenchManager, then start processing messages it receives.
     */
    public void run() {
        QIWConstants.SYNC_LOCK.lock();

        javax.swing.JDialog.setDefaultLookAndFeelDecorated(true);

        // initialize the Workbench Manager who will release the lock when finished
        init();

        // debug - dump system properties and env variables
        if (WorkbenchData.verbose > 0) {
            printPropertiesAndEnvironment();
        }

        // There should be exactly 4 respnses from request sent in init()
        IQiWorkbenchMsg response = messagingMgr.getNextMsgWait();
        processMsg(response);
        response = messagingMgr.getNextMsgWait();
        processMsg(response);
        response = messagingMgr.getNextMsgWait();
        processMsg(response);
        response = messagingMgr.getNextMsgWait();
        processMsg(response);

        // show Tomcat location
        if (WorkbenchData.verbose > 0) {
            logger.info("Running tomcat from " + messagingMgr.getTomcatURL());
        }

        // get my component descriptor
        myDesc = messagingMgr.getComponentDesc(myCID);

        QiwbPreferences userPrefs = MessageDispatcher.getInstance().getUserPrefs();
        String defaultServer = userPrefs.getDefaultServer();
        String defaultQispace = null;
        if (defaultServer != null && defaultServer.trim().length() != 0) {
            defaultQispace = userPrefs.getDefaultQispace(defaultServer);
            if (defaultQispace != null && defaultQispace.trim().length() != 0) {
                //honor defaults if neverAskAgain flag is true
                if (userPrefs.getNeverAskAgain()) {
                    qispace = defaultQispace;
                    //defaultQispace is the project
                    //TODO: Project is the qiSpace until there is a qiProjectManager. Given each qiComponent must associated itself with a project, remove reliance on the project being set by the Workbench Manager. This means a qiComponent would set the project in the Workbench Manager which in turn sets it in the Messaging Manager.
                    project = defaultQispace;
                    MessageDispatcher.getInstance().setProject(defaultQispace);

                    String defaultDesktop = userPrefs.getDefaultDesktop(defaultServer, defaultQispace);
                    if (defaultDesktop != null && defaultDesktop.trim().length() != 0) {
                        IComponentDescriptor stMgrDesc = MessageDispatcher.getInstance().getComponentDescByName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
                        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.RESTORE_DEFAULT_DESKTOP_CMD, stMgrDesc, QIWConstants.STRING_TYPE, defaultDesktop);
                    } else {
                        launchWorkbench();
                    }
                }
            }
        }
        
        //force the determine of the workbench sessionID
        getWorkbenchSessionID();

        // process any messages received from other components
        while (true) {
            // Poll for a message every 1/2 second
            IQiWorkbenchMsg msg = messagingMgr.peekNextMsg();
            if (msg != null && !msg.skip()) {
                msg = messagingMgr.getNextMsgWait();

                //if (msg != null)
                processMsg(msg);
            } else {
                try {
                    Thread.sleep(500L);
                } catch (InterruptedException ie) {
                    showInternalErrorDialog(Thread.currentThread().getStackTrace(), "Thread.sleep failed");
                }
            }
        }
    }

    public void launchWorkbench() {
        Runnable heavyRunnable = new Runnable() {

            public void run() {
                String title = "Launching qiWorkbench";
                String text = "Please wait while system is launching the qiWorkbench tool....";
                Icon icon = IconResource.getInstance().getImageIcon(IconResource.LOGO_ICON_QI);
                com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(null, 100, true, 1000, title, text, icon);
                monitor.start("");
                WorkbenchGUI gui = new WorkbenchGUI(WorkbenchManager.getInstance());
                workbenchGUI = gui;
                WorkbenchData.gui = gui;

                gui.setVisible(true);
                if (gui.isVisible()) {

                    //if(gui != null){
                    //  WorkbenchStateManager.getInstance().setWorkbenchGUI(gui);
                    monitor.setCurrent(null, monitor.getTotal());
                }
            }
        };
        new Thread(heavyRunnable).start();
    }

    /**
     * Record active plugin.
     *
     * @param desc Component descriptor of the activated plugin.
     */
    public void addActivePlugin(IComponentDescriptor desc) {
        if (WorkbenchData.activePlugins.containsKey(desc.getCID())) {
            return;
        }
        WorkbenchData.activePlugins.put(desc.getCID(), desc);
    }

    /**
     * Record active services.
     *
     * @param desc Component descriptor of the activated service.
     */
    protected void addActiveService(IComponentDescriptor desc) {
        if (WorkbenchData.activeServices.containsKey(desc.getCID())) {
            return;
        }
        WorkbenchData.activeServices.put(desc.getCID(), desc);
    }

    /**
     * Record active viewers.
     *
     * @param desc Component descriptor of the activated viewer.
     */
    protected void addActiveViewer(IComponentDescriptor desc) {
        if (WorkbenchData.activeViewers.containsKey(desc.getCID())) {
            return;
        }
        WorkbenchData.activeViewers.put(desc.getCID(), desc);
    }

    /**
     * Get descriptor of an active component
     *
     * @param cid CID of the activated component.
     * @return Descriptor if component has been activated; otherwise, null
     */
    protected IComponentDescriptor getActivePlugin(String cid) {
        if (!WorkbenchData.activePlugins.containsKey(cid)) {
            return null;
        }
        return WorkbenchData.activePlugins.get(cid);
    }

    /**
     * Get descriptor of an active component
     *
     * @param cid CID of the activated component.
     * @return Descriptor if component has been activated; otherwise, null
     */
    protected IComponentDescriptor getActiveService(String cid) {
        if (!WorkbenchData.activeServices.containsKey(cid)) {
            return null;
        }
        return WorkbenchData.activeServices.get(cid);
    }

    /**
     * Get descriptor of an active component
     *
     * @param cid CID of the activated component.
     * @return Descriptor if component has been activated; otherwise, null
     */
    protected IComponentDescriptor getActiveViewer(String cid) {
        if (!WorkbenchData.activeViewers.containsKey(cid)) {
            return null;
        }
        return WorkbenchData.activeViewers.get(cid);
    }

    /**
     * Unrecord active component. If the component has not been activated, do nothing.
     *
     * @param cid CID of the activated component.
     */
    protected void removeActivePlugin(String cid) {
        if (!WorkbenchData.activePlugins.containsKey(cid)) {
            return;
        }
        WorkbenchData.activePlugins.remove(cid);
    }

    /**
     * Unrecord active component. If the component has not been activated, do nothing.
     *
     * @param cid CID of the activated component.
     */
    protected void removeActiveService(String cid) {
        if (!WorkbenchData.activeServices.containsKey(cid)) {
            return;
        }
        WorkbenchData.activeServices.remove(cid);
    }

    /**
     * Unrecord active component. If the component has not been activated, do nothing.
     *
     * @param cid CID of the activated component.
     */
    protected void removeActiveViewer(String cid) {
        if (!WorkbenchData.activeViewers.containsKey(cid)) {
            return;
        }
        WorkbenchData.activeViewers.remove(cid);
    }

    public void setWorkbenchGUI(WorkbenchGUI gui) {
        workbenchGUI = gui;
    }

    public Component getWorkbenchGUI() {
        return workbenchGUI;
    }

    /**
     * Get the number of project manager instances.
     * @return Number of project manager instances.
     */
    public int getNumProjMgrs() {
        getProjMgrInstances();
        return projMgrInstances.size();
    }

    /**
     * Get a list of descriptors for all project manager instances.
     */
    private void getProjMgrInstances() {
        projMgrInstances.clear();
        //get a list of all project manager instances
        if (!WorkbenchData.activePlugins.isEmpty()) {
            Set<String> keys = WorkbenchData.activePlugins.keySet();
            Iterator<String> iter = keys.iterator();
            while (iter.hasNext()) {
                String key = iter.next();
                IComponentDescriptor compDesc = WorkbenchData.activePlugins.get(key);
                if (compDesc.getComponentKind().equals(QIWConstants.PLUGIN_PROJMGR_COMP)) {
                    projMgrInstances.add(compDesc);
                }
            }
        }
    }

    /**
     * Find the request matching the response and process the response based on the request.
     * @param msg QiWorkbenchMsg
     */
    @SuppressWarnings("unchecked")
    public void processMsg(IQiWorkbenchMsg msg) {
        /** Request that matches the response */
        IQiWorkbenchMsg request = null;
        String msgKind = msg.getMsgKind();
        String cmd = "";

        // log message traffic
        logger.fine("WorkbenchManager::procssMsg: msg=" + msg);

        // check for a request
        if (msgKind.equals(QIWConstants.CMD_MSG)) {
            cmd = msg.getCommand();
            // add a component to active service components and update GUI
            if (cmd.equals(QIWConstants.ADD_COMPONENT_NODE_CMD)) {
                ArrayList<String> params = (ArrayList<String>) msg.getContent();
                String compKind = params.get(0);    // type of component
                String displayName = params.get(1); // display name of component
                String parentCID = params.get(2);   // CID of parent node
                logger.finest("Received add component command: " + params);
                if (WorkbenchData.verbose > 0) {
                    logger.info("Adding Component: compKind: " + compKind + " displayName " + displayName + " parentCID " + parentCID);
                }
                // Unsolicited add component commands apply to services only, plugins and viewers are done internally
                if (!compKind.equals(QIWConstants.LOCAL_SERVICE_COMP) && !compKind.equals(QIWConstants.REMOTE_SERVICE_COMP)) {
                    logger.info("Ignoring add component command for componentKind: " + compKind);
                    logger.finest("Ignoring add component command for componentKind: " + compKind);
                } else if (messagingMgr.getComponentDescFromDisplayName(displayName) != null) {
                    addActiveService(messagingMgr.getComponentDescFromDisplayName(displayName));
                    WorkbenchAction.addComponent(messagingMgr.getComponentDescFromDisplayName(displayName),
                            messagingMgr.getComponentDesc(parentCID).getDisplayName());
                }
            } // quit a component and close GUI
            else if (cmd.equals(QIWConstants.QUIT_COMPONENT_CMD)) {
                IComponentDescriptor desc = (IComponentDescriptor) msg.getContent();
                if (desc != null) {
                    WorkbenchAction.closeComponentGUI(desc);
                }
            } // remove component and update GUI
            else if (cmd.equals(QIWConstants.REMOVE_COMPONENT_NODE_CMD)) {
                String nodeCID = (String) msg.getContent();
                IComponentDescriptor desc = getActiveService(nodeCID);
                if (desc != null) {
                    removeActiveService(nodeCID);
                    WorkbenchAction.removeComponent(desc.getComponentKind(), desc.getDisplayName());
                }
            } // check for renameComponent
            else if (cmd.equals(QIWConstants.RENAME_COMPONENT)) {
                // msgContent is oldName, newName
                ArrayList<String> msgContent = new ArrayList<String>(2);
                msgContent = (ArrayList) msg.getContent();
                IComponentDescriptor desc = messagingMgr.getComponentDescFromDisplayName(msgContent.get(0));
                // TODO distinguish if Plugin or Viewer
                // remove old from active list
                removeActivePlugin(desc.getCID());
                // change displayName
                desc.setDisplayName(msgContent.get(1));
                // add back to list
                addActivePlugin(desc);
                // rename in component register and component registry
                messagingMgr.renameComponent(msgContent.get(0), msgContent.get(1));
                // send command to component
                if (WorkbenchData.verbose > 0) {
                    logger.info("WBMgr sending rename command - OLD: " + msgContent.get(0) + " NEW: " + msgContent.get(1));
                }
                //Tell the component to change its name
                messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.RENAME_COMPONENT, desc,
                        QIWConstants.ARRAYLIST_TYPE, msgContent);
            } // check for quitViewer
            else if (cmd.equals(QIWConstants.REMOVE_VIEWER_AGENT_CMD)) {
                IComponentDescriptor desc = messagingMgr.getComponentDescFromDisplayName((String) msg.getContent());
                // remove from active list
                removeActiveViewer(desc.getCID());
                // remove from GUI
                WorkbenchAction.removeComponent(desc.getComponentKind(), desc.getDisplayName());
                // doesn't matter where command came from, route it to viewer agent
                logger.finer("WBMgr sending quit to viewer " + msg.getContent() + " desc: " + desc);
                String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.REMOVE_VIEWER_AGENT_CMD, desc);
                IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgID, 1000);
                if (resp != null) {
                    if (msg.getProducerCID() != resp.getProducerCID()) {
                        if (resp.isAbnormalStatus()) {
                            resp.setConsumerCID(msg.getConsumerCID());
                            resp.setProducerCID(msg.getProducerCID());
                            messagingMgr.routeMsg(resp);
                        } else {
                            messagingMgr.sendResponse(msg, Object.class.getName(), resp.getContent());
                        }
                    }
                }
            } //check for quitPlugin
            else if (cmd.equals(QIWConstants.REMOVE_PLUGIN_CMD)) {
                IComponentDescriptor desc = messagingMgr.getComponentDescFromDisplayName((String) msg.getContent());
                // remove from active list
                removeActivePlugin(desc.getCID());
                // remove from GUI
                //WorkbenchAction.removeComponent(desc.getComponentKind(),desc.getDisplayName());
                WorkbenchAction.removeComponent(desc);

                // send command to plugin, even if it came from him
                logger.finer("WBMgr sending quit to plugin " + msg.getContent() + " desc: " + desc);
                //Tell the plugin to quit
                String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.REMOVE_PLUGIN_CMD, desc);
                IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgID, 1000);
                if (resp != null) {
                    if (msg.getProducerCID() != resp.getProducerCID()) {
                        if (resp.isAbnormalStatus()) {
                            resp.setConsumerCID(msg.getConsumerCID());
                            resp.setProducerCID(msg.getProducerCID());
                            messagingMgr.routeMsg(resp);
                        } else {
                            messagingMgr.sendResponse(msg, Object.class.getName(), resp.getContent());
                        }
                    }
                }
            } //check for quitPlugin
            else if (cmd.equals(QIWConstants.REMOVE_COMPONENT_CMD)) {
                IComponentDescriptor desc = (IComponentDescriptor) msg.getContent();
                String compKind = desc.getComponentKind();

                // remove from active list
                if (compKind.equals(QIWConstants.PLUGIN_AGENT_COMP) ||
                        compKind.equals(QIWConstants.PLUGIN_PROJMGR_COMP)) {
                    removeActivePlugin(desc.getCID());
                } else if (compKind.equals(QIWConstants.VIEWER_AGENT_COMP)) {
                    removeActiveViewer(desc.getCID());
                }

                // remove from GUI
                WorkbenchAction.removeComponent(desc);

                // send command to plugin, even if it came from him
                logger.finer("WBMgr sending quit to plugin " + msg.getContent() + " desc: " + desc);
                //Tell the plugin to quit
                String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.REMOVE_COMPONENT_CMD, desc);
                IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgID, 1000);
                if (resp != null) {
                    msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.REMOVE_COMPONENT_CMD,
                            WorkbenchStateManager.getInstance().getIComponentDescriptor(), QIWConstants.COMP_DESC_TYPE, desc);
                    if (msg.getProducerCID() != resp.getProducerCID()) {
                        if (resp.isAbnormalStatus()) {
                            resp.setConsumerCID(msg.getConsumerCID());
                            resp.setProducerCID(msg.getProducerCID());
                            messagingMgr.routeMsg(resp);
                        } else {
                            messagingMgr.sendResponse(msg, Object.class.getName(), resp.getContent());
                        }
                    }
                    //remove it from the data structure in the WorkbenchStateManager because it  no longer exists
                    WorkbenchStateManager.getInstance().removeNodeFromCurrentNodeMap(desc.getPreferredDisplayName());
                }
            } // check for quitViewer
            else if (cmd.equals(QIWConstants.DEACTIVATE_VIEWER_AGENT_CMD)) {
                IComponentDescriptor desc = messagingMgr.getComponentDescFromDisplayName((String) msg.getContent());
                // remove from active list
                removeActiveViewer(desc.getCID());
                // remove from GUI
                WorkbenchAction.removeComponent(desc.getComponentKind(), desc.getDisplayName());
                // doesn't matter where command came from, route it to viewer agent
                logger.finer("WBMgr sending deactivate to viewer " + msg.getContent() + " desc: " + desc);
                String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.DEACTIVATE_VIEWER_AGENT_CMD, desc);
                IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgID, 1000);
                if (resp != null) {
                    if (msg.getProducerCID() != resp.getProducerCID()) {
                        if (resp.isAbnormalStatus()) {
                            resp.setConsumerCID(msg.getConsumerCID());
                            resp.setProducerCID(msg.getProducerCID());
                            messagingMgr.routeMsg(resp);
                        } else {
                            messagingMgr.sendResponse(msg, Object.class.getName(), resp.getContent());
                        }
                    }
                }
            } //      check for quitPlugin
            else if (cmd.equals(QIWConstants.DEACTIVATE_PLUGIN_CMD)) {
                IComponentDescriptor desc = messagingMgr.getComponentDescFromDisplayName((String) msg.getContent());
                // remove from active list
                removeActivePlugin(desc.getCID());
                // remove from GUI
                WorkbenchAction.removeComponent(desc.getComponentKind(), desc.getDisplayName());

                // send command to plugin, even if it came from him
                logger.finer("WBMgr sending quit to plugin " + msg.getContent() + " desc: " + desc);
                //Tell the plugin to quit
                String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.DEACTIVATE_PLUGIN_CMD, desc);
                IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgID, 1000);
                if (resp != null) {
                    if (msg.getProducerCID() != resp.getProducerCID()) {
                        if (resp.isAbnormalStatus()) {
                            resp.setConsumerCID(msg.getConsumerCID());
                            resp.setProducerCID(msg.getProducerCID());
                            messagingMgr.routeMsg(resp);
                        } else {
                            messagingMgr.sendResponse(msg, Object.class.getName(), resp.getContent());
                        }
                    }
                }
            } else if (cmd.equals(QIWConstants.DEACTIVATE_COMPONENT_CMD)) {
                IComponentDescriptor desc = (IComponentDescriptor) msg.getContent();
                // remove from active list
                removeActivePlugin(desc.getCID());


                // send command to plugin, even if it came from him
                logger.info("WBMgr sending quit to component: " + desc);
                //Tell the component to quit
                String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.DEACTIVATE_COMPONENT_CMD, desc);
                IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgID, 10000);
                if (resp != null && !resp.isAbnormalStatus()) {
                    // remove from GUI
                    WorkbenchAction.removeComponent(desc);
                    messagingMgr.sendResponse(msg, Object.class.getName(), resp.getContent());
                } else {
                    logger.severe("response to DEACTIVATE_COMPONENT_CMD is null due to timed out or abnormal status");
                    return;
                }
            } else if (cmd.equals(QIWConstants.GET_WORKBENCH_GUI_CMD)) {
                messagingMgr.sendResponse(msg, WorkbenchGUI.class.getName(), workbenchGUI);
            } else if (cmd.equals(QIWConstants.RESTORE_COMP_CMD)) {
                messagingMgr.sendResponse(msg, WorkbenchGUI.class.getName(), workbenchGUI);
            } else {
                logger.warning("Request message not processed:" + msg);
            }
        } // check for a response
        else if (msgKind.equals(QIWConstants.DATA_MSG)) {
            request = messagingMgr.checkForMatchingRequest(msg);
            //if no matching request, then assume response rerouted from the GUI
            if (!request.getCommand().equals(QIWConstants.NULL_CMD)) {
                cmd = request.getCommand();
            } else {
                cmd = msg.getCommand();
            }
            if (cmd.equals(QIWConstants.NULL_CMD)) {
                return;
            } else if (cmd.equals(QIWConstants.CHECK_COMPONENT_SECURITY_CMD)) {
                ArrayList list = (ArrayList) msg.getContent();
                String compName = (String) list.get(0);
                String permission = (String) list.get(1);
                if (permission.equals("yes")) {
                    WorkbenchAction.newComponent(compName);
                }
            } // response from activate plugin that was re-routed by GUI listener
            else if (cmd.equals(QIWConstants.ACTIVATE_PLUGIN_CMD)) {
                if (!msg.isAbnormalStatus()) {
                    ArrayList<Object> responseList = new ArrayList<Object>();
                    responseList = (ArrayList) msg.getContent();
                    IComponentDescriptor pluginDesc = (IComponentDescriptor) responseList.get(0);
                    String displayName = pluginDesc.getDisplayName();
                    String parentDisplayName = (String) responseList.get(1);
                    logger.finest("Successfully activated plugin:" + displayName);
                    if (WorkbenchData.verbose > 0) {
                        logger.info("Successfully activated plugin instance of " + parentDisplayName);
                    }
                    //Record plugin as activated
                    addActivePlugin(pluginDesc);
                    WorkbenchAction.addComponent(pluginDesc, parentDisplayName);
                    responseList.set(0, QIWConstants.COMPONENT_INVOKE_NEW);
                    JDesktopPane desktop = ((WorkbenchGUI) workbenchGUI).getDesktopPane();
                    responseList.set(1, new java.awt.Point(xCoord, yCoord));
                    xCoord += xyOffset;
                    yCoord += xyOffset;

                    if (xCoord > desktop.getWidth() || yCoord > desktop.getHeight()) {
                        xCoord = 0;
                        yCoord = 0;
                    }
                    //responseList.remove(2);
                    //Have the plugin invoke an instance of itself
                    String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_SELF_CMD, pluginDesc, QIWConstants.ARRAYLIST_TYPE, responseList);
                    if (WorkbenchData.verbose > 0) {
                        printPropertiesAndEnvironment();
                    }
                } else {
                    showInternalErrorDialog(Thread.currentThread().getStackTrace(), "Error occured while loading plugin");
                    return;
                }
            } // response from activate viewer that was re-routed by GUI listener
            else if (cmd.equals(QIWConstants.ACTIVATE_VIEWER_AGENT_CMD)) {
                if (!msg.isAbnormalStatus()) {
                    ArrayList<Object> responseList = new ArrayList<Object>(2);
                    responseList = (ArrayList) msg.getContent();
                    IComponentDescriptor viewerDesc = (IComponentDescriptor) responseList.get(0);
                    String displayName = viewerDesc.getDisplayName();
                    String parentDisplayName = (String) responseList.get(1);
                    logger.finest("Successfully activated viewer:" + displayName);
                    if (WorkbenchData.verbose > 0) {
                        logger.info("Successfully activated viewer instance of " + parentDisplayName);
                    }
                    //Record viewer as activated
                    addActiveViewer(viewerDesc);
                    //WorkbenchAction.addComponent(QIWConstants.VIEWER_AGENT_COMP,viewerDesc.getDisplayName(),parentDisplayName);
                    WorkbenchAction.addComponent(viewerDesc, parentDisplayName);
                    //Have the viewer invoke an instance of itself
                    responseList.set(0, QIWConstants.COMPONENT_INVOKE_NEW);
                    JDesktopPane desktop = ((WorkbenchGUI) workbenchGUI).getDesktopPane();
                    responseList.set(1, new java.awt.Point(xCoord, yCoord));
                    xCoord += xyOffset;
                    yCoord += xyOffset;
                    if (xCoord > desktop.getWidth() || yCoord > desktop.getHeight()) {
                        xCoord = 0;
                        yCoord = 0;
                    }

                    String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_SELF_CMD, viewerDesc, QIWConstants.ARRAYLIST_TYPE, responseList);
                    if (WorkbenchData.verbose > 0) {
                        printPropertiesAndEnvironment();
                    }
                } else {
                    showInternalErrorDialog(Thread.currentThread().getStackTrace(), "Error occured while loading viewer");
                    return;
                }
            } //response sent by State Manager which reset content; effectively post processing, for
            //Workbench Manager already processed the activate request.
            else if (cmd.equals(QIWConstants.ACTIVATE_COMPONENT_CMD)) {
                if (!msg.isAbnormalStatus()) {
                    ArrayList responseList = (ArrayList) msg.getContent();
                    IComponentDescriptor compDesc = (IComponentDescriptor) responseList.get(0);
                    String displayName = compDesc.getDisplayName();
                    String preferredDisplayName = compDesc.getPreferredDisplayName();
                    String parentDisplayName = (String) responseList.get(1);
                    logger.finest("Successfully activated component: " + preferredDisplayName);
                    if (WorkbenchData.verbose > 0) {
                        logger.info("Successfully activated component instance of " + parentDisplayName);
                    }
                    //Record plugin as activated
                    String compKind = compDesc.getComponentKind();
                    if (compKind.equals(QIWConstants.PLUGIN_AGENT_COMP) ||
                            compKind.equals(QIWConstants.PLUGIN_PROJMGR_COMP)) {
                        addActivePlugin(compDesc);
                    } else if (compKind.equals(QIWConstants.VIEWER_AGENT_COMP)) {
                        addActiveViewer(compDesc);
                    }

                    WorkbenchAction.addComponent(compDesc, parentDisplayName);
                    //Have the plugin invoke an instance of itself
                    if (responseList.size() > 3) {
                        String visibleState = (String) responseList.get(3);
                        if (visibleState != null && visibleState.equals("Closed")) {
                            WorkbenchAction.closeComponentGUI(compDesc);
                        } else {
                            responseList.set(0, QIWConstants.COMPONENT_INVOKE_RESTORED);
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_SELF_CMD, compDesc, QIWConstants.ARRAYLIST_TYPE, responseList);
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(workbenchGUI, "Error Activating Plugin");
                }
            } // response from invoke self that was sent above
            else if (cmd.equals(QIWConstants.INVOKE_SELF_CMD)) {
                if (!msg.isAbnormalStatus()) {
                    // TODO check if instance a GUI; if not, nothing to do
                    // add component GUI to desktop and make visible
                    JInternalFrame compGUI = (JInternalFrame) msg.getContent();
                    if (compGUI == null) {
                        logger.warning("Cannot process INVOKE_SELF_CMD: specified contentType was " + msg.getContentType() + " but content was null");
                    } else {
                        synchronized (WorkbenchData.desktop) {
                            WorkbenchData.desktop.add(compGUI);
                        }
                        //Tile each qiComponent
                        //compGUI.setLocation(xCoord, yCoord);
                        //xCoord += xyOffset;
                        //yCoord += xyOffset;
                        JDesktopPane desktop = ((WorkbenchGUI) workbenchGUI).getDesktopPane();

                        Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
                        int ix = compGUI.getX();
                        int iy = compGUI.getY();

                        int relativeToScreenX = ix + (workbenchGUI.getWidth() - desktop.getWidth()) + workbenchGUI.getX();
                        int relativeToScreenY = iy + (workbenchGUI.getHeight() - desktop.getHeight()) + workbenchGUI.getY();
                        if (relativeToScreenX > screen.width) { //user can not see this component on screen
                            ix = ix - ((relativeToScreenX - screen.width)) - 50;
                            if (ix < 0) {
                                ix = 0;
                            }
                        }
                        if (relativeToScreenY > screen.height) { //user can not see this component on screen
                            iy = iy - ((relativeToScreenY - screen.height)) - 50;
                            if (iy < 0) {
                                iy = 0;
                            }
                        }

                        compGUI.setLocation(ix, iy);
                        compGUI.toFront();
                        compGUI.setVisible(true);
                    }
                } else {
                    showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                            "Error occured while activating component");
                    return;
                }
            } // response from get list of local services command
            else if (cmd.equals(QIWConstants.GET_LOCAL_SERVICES_CMD)) {
                if (!msg.isAbnormalStatus()) {
                    logger.finest("Got list of local services");
                    WorkbenchData.localServicesComponentDescriptors = (ArrayList) msg.getContent();
                    WorkbenchData.nLocalServices = WorkbenchData.localServicesComponentDescriptors.size();
                    if (WorkbenchData.verbose > 0) {
                        logger.info("Got " + WorkbenchData.nLocalServices + " local services");
                        for (int i = 0; i < WorkbenchData.nLocalServices; i++) {
                            logger.info(WorkbenchData.localServicesComponentDescriptors.get(i).getDisplayName());
                        }
                    }
                } else {
                    String cause = (String) msg.getContent();
                    logger.warning(cause);
                    // could not return list, so make list empty
                    WorkbenchData.localServicesComponentDescriptors = new ArrayList();
                }
            } // response from get list of remote services command
            else if (cmd.equals(QIWConstants.GET_REMOTE_SERVICES_CMD)) {
                if (!msg.isAbnormalStatus()) {
                    WorkbenchData.remoteServicesComponentDescriptors = (ArrayList) msg.getContent();
                    WorkbenchData.nRemoteServices = WorkbenchData.remoteServicesComponentDescriptors.size();
                    if (WorkbenchData.verbose > 0) {
                        logger.info("Got " + WorkbenchData.nRemoteServices + " remote services");
                        for (int i = 0; i < WorkbenchData.nRemoteServices; i++) {
                            logger.info(WorkbenchData.remoteServicesComponentDescriptors.get(i).getDisplayName());
                        }
                    }
                } else {
                    String cause = (String) msg.getContent();
                    logger.warning(cause);
                    // could not return list, so make list empty
                    WorkbenchData.remoteServicesComponentDescriptors = new ArrayList();
                }
            } // response from get list of registered plugins command
            else if (cmd.equals(QIWConstants.GET_PLUGINS_CMD)) {
                if (!msg.isAbnormalStatus()) {
                    logger.finest("Got list of registered plugins");
                    if (registeredIComponentDescriptors == null) {
                        registeredIComponentDescriptors = new HashMap<String, List<IComponentDescriptor>>();
                    }
                    registeredIComponentDescriptors.put(QIWConstants.COMPONENT_PLUGIN_TYPE, (ArrayList) msg.getContent());
                    WorkbenchData.registeredPluginsComponentDescriptors = (ArrayList) msg.getContent();
                    WorkbenchData.nAvailablePlugins = WorkbenchData.registeredPluginsComponentDescriptors.size();
                    if (WorkbenchData.verbose > 0) {
                        logger.info("Got " + WorkbenchData.nAvailablePlugins + " registered plugins");
                        for (int i = 0; i < WorkbenchData.nAvailablePlugins; i++) {
                            logger.info(WorkbenchData.registeredPluginsComponentDescriptors.get(i).getDisplayName());
                        }
                    }
                } else {
                    String cause = (String) msg.getContent();
                    logger.warning(cause);
                    // could not return list, so make list empty
                    WorkbenchData.registeredPluginsComponentDescriptors = new ArrayList();
                }
            } // response from get list of registered viewers command
            else if (cmd.equals(QIWConstants.GET_VIEWER_AGENTS_CMD)) {
                if (!msg.isAbnormalStatus()) {
                    logger.finest("Got list of pre-registered viewer agents");
                    if (registeredIComponentDescriptors == null) {
                        registeredIComponentDescriptors = new HashMap<String, List<IComponentDescriptor>>();
                    }
                    registeredIComponentDescriptors.put(QIWConstants.COMPONENT_VIEWER_TYPE, (ArrayList) msg.getContent());
                    WorkbenchData.registeredViewersComponentDescriptors = (ArrayList) msg.getContent();
                    WorkbenchData.nAvailableViewers = WorkbenchData.registeredViewersComponentDescriptors.size();
                    if (WorkbenchData.verbose > 0) {
                        logger.info("Got " + WorkbenchData.nAvailableViewers + " registered viewers");
                        for (int i = 0; i < WorkbenchData.nAvailableViewers; i++) {
                            logger.info(WorkbenchData.registeredViewersComponentDescriptors.get(i).getDisplayName());
                        }
                    }
                } else {
                    String cause = (String) msg.getContent();
                    logger.warning(cause);
                    // could not return list, so make list empty
                    WorkbenchData.registeredViewersComponentDescriptors = new ArrayList();
                }
            } else if (cmd.equals(QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD) ||
                    cmd.equals(QIWConstants.GET_RESULT_FROM_QI_FILE_CHOOSER_CMD)) {
                ArrayList list = (ArrayList) msg.getContent();
                if (((Integer) list.get(0)).intValue() == JFileChooser.APPROVE_OPTION) {
                    final Component dialog = (Component) list.get(5);
                    final String name = (String) list.get(1);
                    Runnable updateAComponent = new Runnable() {

                        public void run() {
                            if (dialog instanceof ProjectSelectorDialog) {
                                qispace = name;
                                ProjectSelectorDialog projectSelector = (ProjectSelectorDialog) dialog;
                                //Selection may be a qiSpace/qiProject or a saved session.
                                //The project selector will differentiate.
                                projectSelector.addSelection(name);
                            } else if (dialog instanceof EditQispacesDialog) {
                                EditQispacesDialog editQispace = (EditQispacesDialog) dialog;
                                editQispace.setStartDirText(name);
                                editQispace.setQispaceLabelText(name);
                            } else if (dialog instanceof EditWorkbenchesDialog) {
                                EditWorkbenchesDialog editQispace = (EditWorkbenchesDialog) dialog;
                                editQispace.setWorkbenchLabel(name);
                            } else if (dialog instanceof ProjectAssocDialog) {
                                ProjectAssocDialog editQiproject = (ProjectAssocDialog) dialog;
                                editQiproject.setProjectPath(name);
                            }
                        }
                    };
                    SwingUtilities.invokeLater(updateAComponent);
                }
            } else if (cmd.equals(QIWConstants.INVOKE_ERROR_DIALOG_SERVICE_CMD)) {
                return;
            } else {
                logger.warning("Response message not processed:" + msg);
            }
            return;
        }
    }

    /**
     * Associate a project manager with a qiComponent and send the component a
     * notification message with the PM's descriptor.
     * @param Component descriptor of qiComponent a PM is to be associated with.
     */
    public void associateProjMgr(IComponentDescriptor compDesc) {
//        if (WorkbenchStateManager.getInstance().isRestoringDesktop()) return;

        IComponentDescriptor projMgrDesc = null;

        getProjMgrInstances();
        if (projMgrInstances.isEmpty()) {
            //launch a PM
            projMgrDesc = WorkbenchAction.newComponent(QIWConstants.QI_PROJECT_MANAGER_NAME);
        } else if (projMgrInstances.size() == 1) {
            //There is just 1 PM, auto associate with it and inform the user
            projMgrDesc = projMgrInstances.get(0);
        } else {
            //Multiple PM instances. Have user select one.
            ArrayList<String> pmNames = new ArrayList<String>();
            HashMap<String, IComponentDescriptor> descMap = new HashMap<String, IComponentDescriptor>();
            for (int i = 0; i < projMgrInstances.size(); i++) {
                IComponentDescriptor desc = projMgrInstances.get(i);
                String pmName = CompDescUtils.getDescPreferredDisplayName(desc);
                pmNames.add(pmName);
                //map PM name to its descriptor
                descMap.put(pmName, desc);
            }
            String title = "Project Manager Selector";
            String insts = "Select a Project Manager instance for " + CompDescUtils.getDescPreferredDisplayName(compDesc) + ":";
            ListSelectorDialog pmSelector = new ListSelectorDialog(title, insts, pmNames);
            pmSelector.setVisible(true);
            //Dialog is modal so thread will hang until user selects a PM
            String pmSelected = pmSelector.getSelection();

            //Map selected pmName back to PM descriptor
            projMgrDesc = descMap.get(pmSelected);
        }
        //Notify the qiComponent of its associated PM
        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.NOTIFY_ASSOC_PROJMGR_CMD, compDesc, QIWConstants.COMP_DESC_TYPE, projMgrDesc);
    }

    /**
     * Browse for a qiSpace using a file chooser.
     */
    public void browseForQispace(Component parent, String title, String dir, FileFilter filter, String chooserType, String command, String serverUrl) {
        ArrayList list = new ArrayList();
        //1st element the parent GUI object
        list.add(parent);
        //2nd element is the dialog title
        list.add(title);
        //3rd element is a list that contains current directory to start with
        //and a flag (yes or no) indicating if a already remembered directory
        //should be used instead
        ArrayList lst = new ArrayList();
        lst.add(dir);
        lst.add("yes");
        list.add(lst);
        //4th element is the file filter
        list.add(filter);
        //5th element is the navigation flag
        list.add(true);
        //6th element is the producer component descriptor
        list.add(messagingMgr.getMyComponentDesc());
        //7th element is the type of file chooser either Open or Save
        list.add(chooserType);
        //8th element is the original qiworkbench message command or action that request for this service
        list.add(command);
        //9th element is the default pretext for the file name if user fails to pretext it. If no pretext is required then use empty String
        list.add("");
        //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
        list.add("");
        //11th element is the target tomcat url where the file chooser chooses
        list.add(serverUrl);
        doFileChooser(list);
    }

    /**
     * Browse for a project directory using a file chooser.
     */
    public void browseForQiProject(Component parent, String title, FileFilter filter, String chooserType, String command, String serverUrl) {
        ArrayList list = new ArrayList();
        //1st element the parent GUI object
        list.add(parent);
        //2nd element is the dialog title
        list.add(title);
        //3rd element is a list that contains current directory to start with
        //and a flag (yes or no) indicating if a already remembered directory
        //should be used instead
        ArrayList lst = new ArrayList();
        lst.add(messagingMgr.getQispaceDesc().getPath());
        lst.add("no");
        list.add(lst);
        //4th element is the file filter
        list.add(filter);
        //5th element is the navigation flag
        list.add(false);
        //6th element is the producer component descriptor
        list.add(messagingMgr.getMyComponentDesc());
        //7th element is the type of file chooser either Open or Save
        list.add(chooserType);
        //8th element is the original qiworkbench message command or action that request for this service
        list.add(command);
        //9th element is the default pretext for the file name if user fails to pretext it. If no pretext is required then use empty String
        list.add("");
        //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
        list.add("");
        //11th element is the target tomcat url where the file chooser chooses
        list.add(serverUrl);
        doFileChooser(list);
    }

    /**
     * Browse for a saved session using a file chooser.
     */
    public void browseForSession(Component parent, String title, String dir, FileFilter filter, String chooserType, String command, String serverUrl) {
        ArrayList list = new ArrayList();
        //1st element the parent GUI object
        list.add(parent);
        //2nd element is the dialog title
        list.add(title);
        //3rd element is a list that contains current directory to start with
        //and a flag (yes or no) indicating if a already remembered directory
        //should be used instead
        ArrayList lst = new ArrayList();
        lst.add(dir);
        lst.add("no");
        list.add(lst);
        //4th element is the file filter
        list.add(filter);
        //5th element is the navigation flag
        list.add(true);
        //6th element is the producer component descriptor
        list.add(messagingMgr.getMyComponentDesc());
        //7th element is the type of file chooser either Open or Save
        list.add(chooserType);
        //8th element is the original qiworkbench message command or action that request for this service
        list.add(command);
        //9th element is the default pretext for the file name if user fails to pretext it. If no pretext is required then use empty String
        list.add("");
        //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
        list.add("");
        //11th element is the target tomcat url where the file chooser chooses
        list.add(serverUrl);
        doFileChooser(list);
    }

    public String getRegisteredComponentTypeByDisplayName(String displayName) {
        for (String key : registeredIComponentDescriptors.keySet()) {
            List<IComponentDescriptor> list = registeredIComponentDescriptors.get(key);
            for (IComponentDescriptor cd : list) {
                if (displayName.startsWith(cd.getDisplayName())) {
                    return key;
                }
            }
        }
        return null;
    }

    public String getRegisteredComponentDisplayNameByDescriptor(IComponentDescriptor desc) {
        String displayName = desc.getDisplayName();
        int index = displayName.indexOf("#");
        if (index != -1) {
            displayName = displayName.substring(0, index);
        }
        for (String key : registeredIComponentDescriptors.keySet()) {
            List<IComponentDescriptor> list = registeredIComponentDescriptors.get(key);
            for (IComponentDescriptor cd : list) {
                //if(desc.getDisplayName().startsWith(cd.getDisplayName()))
                if (displayName.equals(cd.getDisplayName())) {
                    return cd.getDisplayName();
                }
            }
        }
        return null;
    }

    public Map<String, List<IComponentDescriptor>> getRegisteredComponentDescriptors() {
        return registeredIComponentDescriptors;
    }

    public List<IComponentDescriptor> getRegisteredComponentsByType(String type) {
        return registeredIComponentDescriptors.get(type);
    }

    public List<IComponentDescriptor> getRegisteredComponentList() {
        List<IComponentDescriptor> cdList = new ArrayList<IComponentDescriptor>();
        for (String key : registeredIComponentDescriptors.keySet()) {
            List<IComponentDescriptor> list = registeredIComponentDescriptors.get(key);
            for (IComponentDescriptor cd : list) {
                cdList.add(cd);
            }
        }
        Collections.sort(cdList);
        return cdList;
    }

    public void browseDesktops(Component parent, String title, String dir, FileFilter filter, String chooserType, String command, String serverUrl) {
        ArrayList list = new ArrayList();
        //1st element the parent GUI object
        list.add(parent);
        //2nd element is the dialog title
        list.add(title);
        //3rd element is a list that contains current directory to start with
        //and a flag (yes or no) indicating if a already remembered directory
        //should be used instead
        ArrayList lst = new ArrayList();
        lst.add(dir);
        lst.add("no");
        list.add(lst);
        //4th element is the file filter
        list.add(filter);
        //5th element is the navigation flag
        list.add(false);
        //6th element is the producer component descriptor
        list.add(messagingMgr.getMyComponentDesc());
        //7th element is the type of file chooser either Open or Save
        list.add(chooserType);
        //8th element is the original qiworkbench message command or action that request for this service
        list.add(command);
        //9th element is the default pretext for the file name if user fails to pretext it. If no pretext is required then use empty String
        list.add("");
        //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
        list.add(".cfg");
        //11th element is the target tomcat url where the file chooser chooses
        list.add(serverUrl);
        doFileChooser(list);
    }

    /**
     * Invoke old File Chooser service
     * @param list Service's parameters.
     */
    private void doFileChooser(ArrayList list) {
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD, true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, 10000);
        if (response == null) {
            logger.warning("Internal error occurring in getting file chooser service returning null");
            showInternalErrorDialog(Thread.currentThread().getStackTrace(), "Failed to start file chooser service");
            return;
        }

        if (response.isAbnormalStatus()) {
            logger.warning("Internal error occurring in getting file chooser service. Cause: " + response.getContent());
            showInternalErrorDialog(Thread.currentThread().getStackTrace(), "Failed to start file chooser service");
            return;
        } else {
            IComponentDescriptor cd = (IComponentDescriptor) response.getContent();

            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_FILE_CHOOSER_CMD, cd, QIWConstants.ARRAYLIST_TYPE, list);
            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD, cd);
            return;
        }
    }

    /**
     * Invoke new File Chooser Service
     * @param desc File Chooser descriptor containing service's parameters
     */
    public void callFileChooser(QiFileChooserDescriptor desc) {
        if (desc == null) {
            return;
        }
        IComponentDescriptor fileChooser = null;
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD, true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, 10000);
        if (response == null) {
            logger.warning("Respone to get file chooser service returning null due to timed out");
            return;
        } else if (MsgUtils.isResponseAbnormal(response)) {
            logger.warning("Internal error occurring in getting file chooser service. Cause: " + MsgUtils.getMsgContent(response));
            return;
        } else {
            fileChooser = (IComponentDescriptor) MsgUtils.getMsgContent(response);
        }

        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_QI_FILE_CHOOSER_CMD, fileChooser, QiFileChooserDescriptor.class.getName(), desc);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_RESULT_FROM_QI_FILE_CHOOSER_CMD, fileChooser);
        return;
    }

    /**
     * Add selected qiSpace to the preferences, if not already in preferences.
     *
     * @param qispace The qiSpace selected in the Project Selector dialog
     * @param qispaceKind Kind of qiSpace: "W" (workspace of projects), "P" (project)
     * @param selectedServer The runtime Tomcat server selected in the Project Selector dialog
     */
    public void addQispace(String qispace, String qispaceKind, String selectedServer) {
        //this.qispace = qispace;
        QiwbPreferences userPrefs = MessageDispatcher.getInstance().getUserPrefs();
//        String selectedServer = MessageDispatcher.getInstance().getTomcatURL();
        QiSpaceDescriptor qsDesc = new QiSpaceDescriptor(qispace, qispaceKind);
        int status = userPrefs.addQispace(selectedServer, qsDesc);
        //check if qiSpace added to list in preferences
        if (status > 0) {
            String home = MessageDispatcher.getInstance().getUserHOME();
            String prefFileDir = home + File.separator + QIWConstants.QIWB_WORKING_DIR;
            try {
                PreferenceUtils.writePrefs(userPrefs, prefFileDir);
            } catch (QiwIOException qioe) {
                showErrorDialog(QIWConstants.ERROR_DIALOG, Thread.currentThread().getStackTrace(),
                        "IO Error writing user preferences to " + home,
                        new String[]{"Write permission may not be set"},
                        new String[]{"Verify permissions and fix if necessary",
                            "If already writable, contact workbench support"});
            }
        }
    }

    /**
     * Set selected qiSpace as the default in the preferences. Also, set the 'never ask again' flag.
     *
     * @param selectedServer The default server selected in the Project Selector dialog
     * @param qispace The default qiSpace selected in the Project Selector dialog
     * @param neverAskAgainFlag true if the 'never ask again' in the Project Selector
     * dialog is selected; otherwise, false.
     */
    public void setDefaultQispace(String selectedServer, String qispace, boolean neverAskAgainFlag) {
        //this.qispace = qispace;
        QiwbPreferences userPrefs = MessageDispatcher.getInstance().getUserPrefs();

        int status2 = userPrefs.setNeverAskAgain(neverAskAgainFlag);
//        String selectedServer = MessageDispatcher.getInstance().getTomcatURL();
        int status = userPrefs.setDefaultQispace(selectedServer, qispace);
        //check if the default qiSpace was added to the preferences
        if (status > 0 || status2 > 0) {
            String home = MessageDispatcher.getInstance().getUserHOME();
            String prefFileDir = home + File.separator + QIWConstants.QIWB_WORKING_DIR;
            try {
                PreferenceUtils.writePrefs(userPrefs, prefFileDir);
            } catch (QiwIOException qioe) {
                showErrorDialog(QIWConstants.ERROR_DIALOG, Thread.currentThread().getStackTrace(),
                        "IO Error writing user preferences to " + home,
                        new String[]{"Write permission may not be set"},
                        new String[]{"Verify permissions and fix if necessary",
                            "If already writable, contact workbench support"});
            }
        }
    }

    /**
     * Invoke a Project Selector dialog so the user can select a runtime
     * server, qiProject and optionally a saved session.
     */
    public void selectServerAndProject() {
        // Have user select a runtime Tomcat server, then a qiSpace/qiProject
        // accessible from the server.
        // NOTE: Dialog will check if selected server is accessible.
        Component parent = (Component) new JFrame();
        ProjectSelectorDialog psDialog = new ProjectSelectorDialog(this, parent, false);
        javax.swing.JDialog.setDefaultLookAndFeelDecorated(true);
        psDialog.setLocation(200, 100);
        psDialog.setSize(700, 830);
        psDialog.setDefaultCloseOperation(javax.swing.JDialog.DISPOSE_ON_CLOSE);
        psDialog.setVisible();
        //Wait until user closes dialog

        ArrayList<String> tomcatInfo = psDialog.getTomcatInfo();
        String tomcatURL = tomcatInfo.get(0);
        String tomcatLoc = tomcatInfo.get(1);

        //NOTE: Setting the default qiSpace depends on already setting the
        //      default runtime Tomcat server. Order is important!
        boolean rememberAsDefault = psDialog.getServerDefaultIndicator();
        QiwbPreferences userPrefs = MessageDispatcher.getInstance().getUserPrefs();
        // add select server to list of preferences
        int status = userPrefs.addServer(tomcatURL);
        int status2 = 0;
        if (rememberAsDefault) {
            status2 = userPrefs.setDefaultServer(tomcatURL);
        }
        // and save the preferences to a file.
        try {
            // write out preferences if server add to the list
            // or server made the default
            if (status > 0 || status2 > 0) {
                String prefFileDir = MessageDispatcher.getInstance().getPrefFileDir();
                PreferenceUtils.writePrefs(userPrefs, prefFileDir);
            }
        } catch (QiwIOException qioe) {
            //TODO notify user cannot write preference file
            logger.finest(qioe.getMessage());
        }

        logger.config("Selected Tomcat: URL=" + tomcatURL + ", Location=" + tomcatLoc);
        logger.info("Setting runtime server:" + tomcatURL);

        if (MessageDispatcher.getInstance().resetRuntimeServer(tomcatURL) == false) {
            JOptionPane.showMessageDialog(psDialog, "Failed to set runtime Tomcat URL to: " + tomcatURL + System.getProperty("line.separator") + "The qiWorkbench will now close.", "Error Registering Runtime Tomcat URL", JOptionPane.ERROR_MESSAGE);
            //quit and do not prompt the user to save the desktop
            WorkbenchAction.quitDesktop(false);
        }
    }

    /**
     * If server is remote, get the name of its OS, and the OS's line and file
     * separators.
     * @return List of items: [0] OS name; [1] OS file separator; [2] OS line separator
     */
    public ArrayList<String> getRemoteOSInfo() {
        if (MessageDispatcher.getInstance().getLocationPref().equals(QIWConstants.LOCAL_PREF)) {
            return null;
        }
        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, MessageDispatcher.getInstance().getRuntimeServletDispDesc().getCID(), QIWConstants.CMD_MSG, QIWConstants.GET_REMOTE_OS_INFO_CMD, MsgUtils.genMsgID(), "", "");
        IQiWorkbenchMsg response = DispatcherConnector.getInstance().sendRequestMsg(request);
        return (ArrayList<String>) response.getContent();
    }

    /**
     * If server is remote, get the name of its OS, and the OS's line and file
     * separators. Mainly used by the Project Selector dialog when the user has
     * selected a server and location preference, and then must select a project
     * on that server.
     * @param locPref Location preference of runtime Tomcat server
     * @return List of items: [0] OS name; [1] OS file separator; [2] OS line separator
     */
    public ArrayList<String> getRemoteOSInfo(String locPref) {
        if (locPref.equals(QIWConstants.LOCAL_PREF)) {
            return null;
        }
        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, MessageDispatcher.getInstance().getRuntimeServletDispDesc().getCID(), QIWConstants.CMD_MSG, QIWConstants.GET_REMOTE_OS_INFO_CMD, MsgUtils.genMsgID(), "", "");
        IQiWorkbenchMsg response = DispatcherConnector.getInstance().sendRequestMsg(request);
        return (ArrayList<String>) response.getContent();
    }

    /**
     * Determines whether the directory is visible to the runtime server if remote,
     * or client is local
     *
     * @param locPref LOCAL_PREF or REMOTE_PREF
     * @param dir directory to search for, usign the server's correct file separator
     *
     * @return true if the directory exists and is visible to the runtime server
     */
    public boolean dirExists(String locPref, String dir) {
        if (locPref.equals(QIWConstants.LOCAL_PREF)) {
            File directory = new File(dir);
            return directory != null && directory.isDirectory();
        } else {
            IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID,
                    MessageDispatcher.getInstance().getRuntimeServletDispDesc().getCID(),
                    QIWConstants.CMD_MSG,
                    QIWConstants.IS_REMOTE_DIRECTORY_CMD,
                    MsgUtils.genMsgID(),
                    QIWConstants.STRING_TYPE,
                    dir);
            IQiWorkbenchMsg response = DispatcherConnector.getInstance().sendRequestMsg(request);
            String responseContent = response.getContent().toString();
            if ("yes".equals(responseContent)) {
                return true;
            } else if ("no".equals(responseContent)) {
                return false;
            } else {
                logger.warning("IS_REMOTE_DIRECTORY_CMD returned error: " + responseContent);
                return false;
            }
        }
    }

    /**
     * showInternalErrorDialog starts error service for showing internal errors, e.g. unexpected NULL etc
     * @param message one-line error message
     */
    void showInternalErrorDialog(StackTraceElement[] stack, String message) {
        showErrorDialog(QIWConstants.ERROR_DIALOG, stack, message, new String[]{"Internal workbench error"},
                new String[]{"Contact workbench support"});
    }

    /**
     * showErrorDialog passes caller's error or warning message to getErrorService
     * @param type QIWConstants.ERROR_DIALOG or QIWConstants.WARNING_DIALOG
     * @param message one-line message
     * @param causes list of possible causes
     * @param suggestions list of possible remedies
     */
    void showErrorDialog(String type, StackTraceElement[] stack, String message,
            String[] causes, String[] suggestions) {
        ArrayList list = new ArrayList(7);
        // 0: error dialog parent component
        list.add(WorkbenchData.gui);
        // 1 message type
        list.add(type);
        // 2: caller's display name
        list.add(WorkbenchManager.getInstance().getComponentDescriptor().getDisplayName());
        // 3: stack
        list.add(stack);
        // 4: message
        list.add(message);
        // 5: possible causes
        list.add(causes);
        // 6: suggested remedies
        list.add(suggestions);
        // send message to start error service
        WorkbenchManager.getInstance().getErrorService(list);
    }

    /**
     *
     * @param list to pass to error service
     */
    void getErrorService(ArrayList list) {
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_ERROR_DIALOG_SERVICE_CMD, true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, 10000);
        if (response == null || response.isAbnormalStatus()) {
            JOptionPane.showMessageDialog(null, "Error getting Error Service");
            return;
        }
        IComponentDescriptor errorServiceDesc = (IComponentDescriptor) response.getContent();
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.INVOKE_ERROR_DIALOG_SERVICE_CMD,
                errorServiceDesc, QIWConstants.ARRAYLIST_TYPE, list);
        response = messagingMgr.getMatchingResponseWait(msgID, 10000);
    }

    /**
     * printPropertiesAndEnvironment is for debugging
     *
     */
    private void printPropertiesAndEnvironment() {
        Level envLogLevel = Level.FINE;

        if (logger.isLoggable(envLogLevel)) {
            java.util.Properties props = System.getProperties();
            java.util.Enumeration e = props.propertyNames();

            logger.log(envLogLevel, "Output from System.getProperties()");
            while (e.hasMoreElements()) {
                String key = (String) e.nextElement();
                String value = props.getProperty(key);
                logger.log(envLogLevel, "Key: " + key + " Value: " + value);
            }
            java.util.Map<String, String> map = System.getenv();
            java.util.Set s = map.entrySet();
            java.util.Iterator it = s.iterator();
            logger.log(envLogLevel, "Output from System.getenv()");
            while (it.hasNext()) {
                Map.Entry aentry = (Map.Entry) it.next();
                String key = (String) aentry.getKey();
                String value = (String) aentry.getValue();
                logger.log(envLogLevel, "Key: " + key + " Value: " + value);
            }
        }
    }

    public boolean pingServer(String url) {
//       make sure Tomcat is alive
        IComponentDescriptor servletDispDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.SERVLET_DISPATCHER_NAME);
        QiWorkbenchMsg request = new QiWorkbenchMsg(myCID, servletDispDesc.getCID(), QIWConstants.CMD_MSG,
                QIWConstants.PING_SERVER_CMD, MsgUtils.genMsgID(),
                QIWConstants.STRING_TYPE, url);
        IQiWorkbenchMsg response = DispatcherConnector.getInstance().sendRequestMsg(request);
        if (response == null) {
            logger.severe("response to STRING_TYPE return null due to timed out");
            return false;
        }
        if (!response.isAbnormalStatus()) {
            logger.config("setTomcat: Established contact with " + url);
            return true;
        } else {
            logger.config("setTomcat: Failed to connect to " + url);
            //TODO: bring up selection dialog again.
            return false;
        }
    }

    public void registerStatsServer() {
        MessageDispatcher.getInstance().registerStatsServer();
    }
	
	/**
	 * Get the unique, system wide session ID for this workbench instance.
	 * The sessionID's form is <client hostname>_<timestamp>
	 * As a side affect, get the client machine's hostname.
	 * @return session ID of this workbench instance.
	 */
	private String getQiwbSessionID() {
		//NOTE: System.getenv("HOSTNAME") returns null (for a Mac)

		//Get the hostname of the client machine, the base of the session ID
        ArrayList<String> params = new ArrayList<String>();
        // make sure script is executable
        params.add(QIWConstants.LOCAL_SERVICE_PREF);
        params.add("bash");
        params.add("-c");
        params.add(" source ~/.bashrc; hostname ");
        params.add("true");//blocking
        // submit job for execution
        int status = 0;
        int errorStatus = 0;
        String errorContent = "";

        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.SUBMIT_JOB_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, 10000);
		
		if (response == null || MsgUtils.isResponseAbnormal(response)) {
			clientHostname = "UnknownHostname"+"."+Long.toString(System.currentTimeMillis());
			//Form the session ID for this instance of the workbench
			return clientHostname+"_"+Long.toString(System.currentTimeMillis());
		}
		
		String jobID = (String) MsgUtils.getMsgContent(response);
		
        //Wait until the command has finished
        params.clear();
        params.add(QIWConstants.LOCAL_SERVICE_PREF);
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        response = messagingMgr.getMatchingResponseWait(msgID, 10000);
        if (response == null || MsgUtils.isResponseAbnormal(response)) {
			clientHostname = "UnknownHostname"+"."+Long.toString(System.currentTimeMillis());
			//Form the session ID for this instance of the workbench
			return clientHostname+"_"+Long.toString(System.currentTimeMillis());
        }
		
        //get stdout
        params.clear();
        //params.add(QIWConstants.LOCAL_SERVICE_PREF);
        params.add(QIWConstants.LOCAL_SERVICE_PREF);
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_JOB_OUTPUT_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        response = messagingMgr.getMatchingResponseWait(msgID, 10000);

        if (response == null || MsgUtils.isResponseAbnormal(response)) {
 			clientHostname = "UnknownHostname"+"."+Long.toString(System.currentTimeMillis());
        } else {
            ArrayList<String> stdOut = (ArrayList<String>) MsgUtils.getMsgContent(response);
			clientHostname = stdOut.get(0);
        }

		//Form the session ID for this instance of the workbench
		return clientHostname+"_"+Long.toString(System.currentTimeMillis());
	}
}
