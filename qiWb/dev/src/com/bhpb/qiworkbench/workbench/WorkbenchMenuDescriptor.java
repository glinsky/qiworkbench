/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench.workbench;

import java.awt.event.ActionListener;
import java.util.ArrayList;

import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class WorkbenchMenuDescriptor {

    private static final Logger logger =
            Logger.getLogger(WorkbenchMenuDescriptor.class.getName());
    private String label;
    private int type;
    private JMenu thisMenu;
    private String actionCommand;
    private ActionListener actionListener;
    private JFrame frame;
    private ArrayList<JMenuItem> menuItems;
    private boolean enabled;


    // menu Descriptors
    // each menu and menuitem has a menuDescriptor that is an ArrayList containing:
    // String               label           name of this menu or menuitem
    // int                  type            menuType(1) or menuItemType(0)
    // JMenu                thisMenu        JMenu pointer for this menu
    // String               actionCommand   action command associated with this menu item
    // ActionListener       actionListener  pointer to listener for this menu item
    // JFrame               frame           pointer to container holding this menu
    // ArrayList<JMenuItem> menuItems       pointers to JMenuItems under JMenu
    // boolean              enabled         false if code not yet implemented, else true
    public WorkbenchMenuDescriptor(String label, int type, JMenu thisMenu, String actionCommand,
            ActionListener actionListener, JFrame frame, ArrayList<JMenuItem> menuItems, boolean enabled) {
        //ActionListener actionListener, JFrame frame, ArrayList<JMenuItem> menuItems, boolean enabled) {
        this.label = label;
        this.type = type;
        this.thisMenu = thisMenu;
        this.actionCommand = actionCommand;
        this.actionListener = actionListener;
        this.frame = frame;
        this.menuItems = menuItems;
        this.enabled = enabled;
    }

    // getters
    public void printDescriptor() {
        logger.info("LABEL: " + this.label + " TYPE: " + this.type);
        if (this.getType() == 1) {
            logger.info("NUM-ITEMS " + this.menuItems.size());
            for (int i = 0; i < this.menuItems.size(); i++) {
                logger.info(this.menuItems.get(i).getText());
            }
        }
    }

    public String getLabel() {
        return this.label;
    }

    public int getType() {
        return this.type;
    }

    public JMenu getMenu() {
        return this.thisMenu;
    }

    public String getActionCommand() {
        return this.actionCommand;
    }

    public ActionListener getActionListener() {
        return this.actionListener;
    }

    public JFrame getFrame() {
        return this.frame;
    }

    public ArrayList<JMenuItem> getContents() {
        return this.menuItems;
    }

    public boolean getEnabled() {
        return this.enabled;
    }

    public static int getMenuDescriptorIndex(String action) {
        for (int i = 0; i < WorkbenchData.menuDescriptors.size(); i++) {
            WorkbenchMenuDescriptor wmd = WorkbenchData.menuDescriptors.get(i);
            if (wmd.getActionCommand().equals(action)) {
                return i;
            }
        }
        return -1;
    }

    public static WorkbenchMenuDescriptor getMenuDescriptor(String action) {
        for (int i = 0; i < WorkbenchData.menuDescriptors.size(); i++) {
            WorkbenchMenuDescriptor wmd = WorkbenchData.menuDescriptors.get(i);
            if (wmd.getActionCommand().equals(action)) {
                return wmd;
            }
        }
        return null;
    }

    // setters
    public void setMenu(JMenu menu) {
        this.thisMenu = menu;
    }

    public void setMenuItems(ArrayList<JMenuItem> list) {
        this.menuItems = list;
    }
}