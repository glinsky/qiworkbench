/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.workbench;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.help.CSH;
import javax.help.HelpBroker;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.filechooser.FileFilter;

import com.bhpb.qiworkbench.QiSpaceDescriptor;
import com.bhpb.qiworkbench.QiwbPreferences;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.client.util.GenericFileFilter;
import com.bhpb.qiworkbench.client.util.PreferenceUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;

/**
 * Dialog to edit the qiProjects associated with a Tomcat servers.
 * A qiProject can be added, removed or designated as the default (if
 * the default server is selected).
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class EditQispacesDialog extends javax.swing.JDialog {
    private static Logger logger = Logger.getLogger(EditQispacesDialog.class.getName());

    String userHOME = "";
    String selectedServer = "";
    QiwbPreferences userPrefs = null;
    ArrayList<String> validServers = null;
    String[] servers = new String[4];
    ArrayList<QiSpaceDescriptor> validQispaces = null;
    //GUI's list of qiProjects
    String[] qispaces = new String[4];
    boolean ignoreSelection = true;
    String startDir = "";
    // the qiProject selected by browsing the start directory
    String selectedQispace = "";
    private WorkbenchManager agent = null;
    /** Indicates kind of qiSpace: "P" (project); "W" (workspace for projects) */
    private String qispaceKind = "P";
    HelpBroker helpBroker;

    /** Creates new form EditQispacesDialog */
    public EditQispacesDialog(WorkbenchManager agent, HelpBroker helpBroker) {
        this.agent = agent;
        this.helpBroker = helpBroker;
        CSH.setHelpIDString(this, "prefs.qispace");

        initComponents();

        //Group the radio buttons.
        ButtonGroup workspaceGroup = new ButtonGroup();
        workspaceGroup.add(projSpaceRadioButton);
        workspaceGroup.add(projRadioButton);

        // Get user's preferences
        userHOME = MessageDispatcher.getInstance().getUserHOME();
        String prefFileDir = userHOME + File.separator + QIWConstants.QIWB_WORKING_DIR;
        if (PreferenceUtils.prefFileExists(prefFileDir)) {
            // get user's preferences saved in the preference file
            try {
                userPrefs = PreferenceUtils.readPrefs(prefFileDir);
            } catch (QiwIOException qioe) {
                //TODO notify user cannot read preference file
                logger.finest(qioe.getMessage());
            }
        } else {
            //NOTE: This should only happen if the user deleted their
            //preference file since one is created when the app is
            //launched (if there isn't one).
            // user has no preference file; create one
            userPrefs = new QiwbPreferences();
            try {
                PreferenceUtils.writePrefs(userPrefs, prefFileDir);
            } catch (QiwIOException qioe) {
                //TODO notify user cannot write preference file
                logger.finest(qioe.getMessage());
            }
        }

        // create the server combo list; mark default (if any)
        createServerList();
        // indicate no item selected
        serverComboBox.setSelectedIndex(-1);
        // ignore selection events before user makes a selection
        ignoreSelection = false;
        // prevent user entering a start dir until server selected
        startDirTextField.setEnabled(false);
        // disable the default buttons
        asDefaultButton.setEnabled(false);
        noDefaultButton.setEnabled(false);
    }

    public void setStartDirText(String text){
        startDirTextField.setText(text);
    }
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        serverPanel = new javax.swing.JPanel();
        startDirTextField = new javax.swing.JTextField();
        startDirLabel = new javax.swing.JLabel();
        addQispaceButton = new javax.swing.JButton();
        browseButton = new javax.swing.JButton();
        selectedQispaceLabel1 = new javax.swing.JLabel();
        selectedQispaceLabel = new javax.swing.JLabel();
        instructionLabel1 = new javax.swing.JLabel();
        instructionLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        qispaceList = new javax.swing.JList();
        removeQispaceButton = new javax.swing.JButton();
        removeAllButton = new javax.swing.JButton();
        asDefaultButton = new javax.swing.JButton();
        noDefaultButton = new javax.swing.JButton();
        closeButton = new javax.swing.JButton();
        helpButton = new javax.swing.JButton();
        instructionLabel3 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        serverlabel = new javax.swing.JLabel();
        serverComboBox = new javax.swing.JComboBox();

        setTitle("Edit Servers");
        setModal(true);
        serverPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 255)), "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(153, 84, 20)));
        startDirTextField.addKeyListener(new StartDirTextFieldKeyListener());
        /*
        startDirTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startDirTextFieldActionPerformed(evt);
            }
        });
        */
        startDirLabel.setText("Start Directory:");

        addQispaceButton.setText("Add");
        addQispaceButton.setEnabled(false);
        addQispaceButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addQispaceButtonActionPerformed(evt);
            }
        });

        browseButton.setText("Browse");
        browseButton.setEnabled(false);
        browseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseButtonActionPerformed(evt);
            }
        });

        selectedQispaceLabel1.setText("Selected qiProject:");
        selectedQispaceLabel.setText("");

        projSpaceRadioButton = new javax.swing.JRadioButton();
        projSpaceRadioButton.setText("Contains project(s)");
        projSpaceRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        projSpaceRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        projSpaceRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JRadioButton rb = (JRadioButton)evt.getSource();
//                qispaceKind = rb.isSelected() ? QiSpaceDescriptor.WORKSPACE_KIND : QiSpaceDescriptor.PROJECT_KIND;
                qispaceKind = QiSpaceDescriptor.WORKSPACE_KIND;
            }
        });
        projSpaceRadioButton.setEnabled(false);

        projRadioButton = new javax.swing.JRadioButton();
        projRadioButton.setSelected(true);
        projRadioButton.setText("A project");
        projRadioButton.setBorder(javax.swing.BorderFactory
                .createEmptyBorder(0, 0, 0, 0));
        projRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        projRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JRadioButton rb = (JRadioButton)evt.getSource();
//                qispaceKind = rb.isSelected() ? QiSpaceDescriptor.PROJECT_KIND : QiSpaceDescriptor.WORKSPACE_KIND;
                qispaceKind = QiSpaceDescriptor.WORKSPACE_KIND;
            }
        });
        projRadioButton.setEnabled(false);

        org.jdesktop.layout.GroupLayout serverPanelLayout = new org.jdesktop.layout.GroupLayout(serverPanel);
        serverPanel.setLayout(serverPanelLayout);
        serverPanelLayout.setHorizontalGroup(
            serverPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(serverPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(serverPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(startDirTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE)
                    .add(startDirLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 146, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(serverPanelLayout.createSequentialGroup()
                        .add(selectedQispaceLabel1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(selectedQispaceLabel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, serverPanelLayout.createSequentialGroup()
//                        .add(projSpaceRadioButton)
//                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
//                        .add(projRadioButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 171, Short.MAX_VALUE)
                        .add(addQispaceButton))
/*
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, addQispaceButton)
*/
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, browseButton))
                .addContainerGap())
        );
        serverPanelLayout.setVerticalGroup(
            serverPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(serverPanelLayout.createSequentialGroup()
                .add(startDirLabel)
                .add(9, 9, 9)
                .add(startDirTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(browseButton)
                .add(9, 9, 9)
                .add(serverPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(selectedQispaceLabel1)
                    .add(selectedQispaceLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(serverPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(addQispaceButton))
//                    .add(projSpaceRadioButton)
//                    .add(projRadioButton))
                .addContainerGap())
        );

        instructionLabel1.setText("You can specify which qiProjects associated with a runtime Tomcat server to use.");
        instructionLabel1.setForeground(new java.awt.Color(102, 102, 255));

        instructionLabel2.setText("To add a new qiProject, first select an allowed server.");
        instructionLabel2.setForeground(new java.awt.Color(102, 102, 255));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 255)), "Valid qiProjects", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(153, 84, 20)));
        jScrollPane1.setViewportView(qispaceList);

        removeQispaceButton.setText("Remove qiProject");
        removeQispaceButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeQispaceButtonActionPerformed(evt);
            }
        });

        removeAllButton.setText("Remove All");
        removeAllButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeAllButtonActionPerformed(evt);
            }
        });

        asDefaultButton.setText("As Default");
        asDefaultButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                asDefaultButtonActionPerformed(evt);
            }
        });

        noDefaultButton.setText("No Default");
        noDefaultButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                noDefaultButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(removeQispaceButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(removeAllButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(asDefaultButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(noDefaultButton)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(removeQispaceButton)
                    .add(removeAllButton)
                    .add(asDefaultButton)
                    .add(noDefaultButton))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        closeButton.setText("Close");
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });

        helpButton.setText("Help");
        helpButton.addActionListener(new CSH.DisplayHelpFromSource(helpBroker));
/*
        helpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                helpButtonActionPerformed(evt);
            }
        });
*/
        instructionLabel3.setText("Then browse to a qiProject, accessible by the server, you want to add.");
        instructionLabel3.setForeground(new java.awt.Color(102, 102, 255));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 255))));
        serverlabel.setText("Server:");

        serverComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                serverComboBoxActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(serverlabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(serverComboBox, 0, 378, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                .add(serverlabel)
                .add(serverComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .add(layout.createSequentialGroup()
                        .add(serverPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .add(layout.createSequentialGroup()
                        .add(instructionLabel1)
                        .addContainerGap(102, Short.MAX_VALUE))
                    .add(instructionLabel2)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .add(layout.createSequentialGroup()
                        .add(instructionLabel3)
                        .addContainerGap(64, Short.MAX_VALUE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(helpButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(closeButton)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(instructionLabel1)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(instructionLabel2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(instructionLabel3)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(serverPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(15, 15, 15)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(helpButton)
                    .add(closeButton))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void serverComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_serverComboBoxActionPerformed
        // ignore event when list initialized
        if (ignoreSelection) return;

        selectedServer = (String)serverComboBox.getSelectedItem();
        int ind = selectedServer.indexOf("(default)");
        if(ind != -1) {
            //strip off "(default)"
            selectedServer = selectedServer.substring(0,ind).trim();
        }
        if(!agent.pingServer(selectedServer)){
          WorkbenchManager.getInstance().showErrorDialog(QIWConstants.ERROR_DIALOG,
                                             Thread.currentThread().getStackTrace(),
                                             "Failed to connect to " + selectedServer,
                                             new String[] {"Tomcat server down","Network issues"},
                                             new String[] {"Restart workbench using different Tomcat server"});
            return;
        }

        // nothing to do if no item has been selected
        if (selectedServer == null) return;

        if (ind != -1) {
            // enable the default buttons
            asDefaultButton.setEnabled(true);
            noDefaultButton.setEnabled(true);
        } else {
            // disable the default buttons. Can only set a default qiProject
            // for the default server.
            asDefaultButton.setEnabled(false);
            noDefaultButton.setEnabled(false);
        }

        // get the qiProjects associated with the server
        updateQispaceList(false);
        // allow user to enter a start dir
        startDirTextField.setEnabled(true);
        // clear the start dir
        startDirTextField.setText("");
        startDir = "";
        // enable the Browse button
        //browseButton.setEnabled(true);
        // disable the Add button
        addQispaceButton.setEnabled(false);
        // clear the selected qiProject
        selectedQispaceLabel.setText("");
        selectedQispace = "";
    }//GEN-LAST:event_serverComboBoxActionPerformed

    public void setQispaceLabelText(String text){
        selectedQispaceLabel.setText(text);
        addQispaceButton.setEnabled(true);
        projSpaceRadioButton.setEnabled(true);
        projRadioButton.setEnabled(true);
    }

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_closeButtonActionPerformed

    /**
     * @deprecated Call commented out.
     */
    private void startDirTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startDirTextFieldActionPerformed
        // user pressed ENTER key
        startDir = startDirTextField.getText();
        // nothing to do if no start dir specified
        if (startDir.equals("")) return;

        //TODO Call the fileChooserDialog to select a qiproject dir and set selectedQispace to the choice
        //TEST ONLY
        selectedQispace = "C:/qiProjects/maddog";
        selectedQispaceLabel.setText(selectedQispace);
        // enable Add button
        addQispaceButton.setEnabled(true);
    }//GEN-LAST:event_startDirTextFieldActionPerformed

    private void browseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseButtonActionPerformed
        // user pressed Browse button
        startDir = startDirTextField.getText();
        // nothing to do if no start dir specified
        if (startDir.equals("")) return;
        String[] extensions = new String[]{"*", "*.*"};
        FileFilter filter = (FileFilter) new GenericFileFilter(extensions, "Directory File (*)",true);
        agent.browseForQispace(this,"Select a qiProject", startDirTextField.getText(),filter, QIWConstants.FILE_CHOOSER_TYPE_OPEN, "", selectedServer);
        //TEST ONLY
       /// selectedQispace = "C:/qiProjects/atlantis";
       /// selectedQispaceLabel.setText(selectedQispace);
        // enable Add button
       /// addQispaceButton.setEnabled(true);
    }//GEN-LAST:event_browseButtonActionPerformed

    private void helpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_helpButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_helpButtonActionPerformed

    private void noDefaultButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_noDefaultButtonActionPerformed
        // NOTE: The No Default button is only enabled if the default
        // server was selected.
        // do nothing if no default qiProject for the default server
        String defaultQispace = userPrefs.getDefaultQispace(selectedServer);
        if (defaultQispace.equals(QiwbPreferences.NO_DEFAULT)) return;
        userPrefs.setDefaultQispace(selectedServer, QiwbPreferences.NO_DEFAULT);
        String defaultWorkbench = userPrefs.getDefaultDesktop(selectedServer, defaultQispace);
        if (!defaultWorkbench.equals(QiwbPreferences.NO_DEFAULT))
            userPrefs.setDefaultDesktop(selectedServer, defaultQispace, QiwbPreferences.NO_DEFAULT);
        updateQispaceList(true);
    }//GEN-LAST:event_noDefaultButtonActionPerformed

    private void asDefaultButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_asDefaultButtonActionPerformed
        // get selected list item
        int idx = qispaceList.getSelectedIndex();
        // nothing to do if no server selected
        if (idx == -1) return;

        String selectedQispace = qispaces[idx];
        if (selectedQispace.endsWith(" (default)")) {
            selectedQispace = selectedQispace.substring(0, selectedQispace.length()-10);
        }
        int status = userPrefs.setDefaultQispace(selectedServer, selectedQispace);
        // nothing to do if selected the default
        if (status == 0) return;

        updateQispaceList(true);
    }//GEN-LAST:event_asDefaultButtonActionPerformed

    private void removeAllButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeAllButtonActionPerformed
        userPrefs.removeAllQispaces(selectedServer);
        updateQispaceList(true);
    }//GEN-LAST:event_removeAllButtonActionPerformed

    private void removeQispaceButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeQispaceButtonActionPerformed
        // get selected list item
        int idx = qispaceList.getSelectedIndex();
        // nothing to do if no server selected
        if (idx == -1) return;

        //Note: if selected qiProject is the default, there will be
        //no default.
        String selectedQispace = qispaces[idx];
        if (selectedQispace.endsWith(" (default)")) {
            selectedQispace = selectedQispace.substring(0, selectedQispace.length()-10);
        }
        userPrefs.removeQispace(selectedServer, selectedQispace);

        updateQispaceList(true);
    }//GEN-LAST:event_removeQispaceButtonActionPerformed

    private void addQispaceButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addQispaceButtonActionPerformed
        // user pressed Add button
        // add to list
        selectedQispace = selectedQispaceLabel.getText();
        if (!selectedQispace.equals("")) {
            int status = userPrefs.addQispace(selectedServer, new QiSpaceDescriptor(selectedQispace, qispaceKind));
            if (status == 1) {
                // qiProject added to the list; repaint JList
                updateQispaceList(true);
            }
        }
    }//GEN-LAST:event_addQispaceButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addQispaceButton;
    private javax.swing.JButton asDefaultButton;
    private javax.swing.JButton browseButton;
    private javax.swing.JButton closeButton;
    private javax.swing.JTextField startDirTextField;
    private javax.swing.JButton helpButton;
    private javax.swing.JLabel instructionLabel1;
    private javax.swing.JLabel instructionLabel2;
    private javax.swing.JLabel instructionLabel3;
    private javax.swing.JList qispaceList;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton noDefaultButton;
    private javax.swing.JLabel selectedQispaceLabel;
    private javax.swing.JButton removeAllButton;
    private javax.swing.JButton removeQispaceButton;
    private javax.swing.JLabel selectedQispaceLabel1;
    private javax.swing.JComboBox serverComboBox;
    private javax.swing.JPanel serverPanel;
    private javax.swing.JLabel serverlabel;
    private javax.swing.JLabel startDirLabel;

    private javax.swing.JRadioButton projSpaceRadioButton;
    private javax.swing.JRadioButton projRadioButton;
    // End of variables declaration//GEN-END:variables

    /**
     * Create the server combo list, marking the default URL (if any)
     */
    private void createServerList() {
        serverComboBox.removeAllItems();

        // get list of allowed servers
        validServers = userPrefs.getServerURLs();
        if (servers.length != validServers.size()) {
            servers = new String[validServers.size()];
        }
        servers = validServers.toArray(servers);
        java.util.Arrays.sort(servers);

        // mark the default URL (if any)
        String defaultServer = userPrefs.getDefaultServer();
        if (!defaultServer.equals(QiwbPreferences.NO_DEFAULT)) {
            // find the default server in the list
            for (int i=0; i<servers.length; i++) {
                if (servers[i].equals(defaultServer)) {
                    servers[i] += " (default)";
                }
            }
        }

        for (int i=0; i<servers.length; i++) {
            serverComboBox.addItem(servers[i]);
        }
    }

    /**
     * Update the GUI's qiProject list, marking the default URL (if any)
     *
     * @param updatePrefs true if preference file should be rewritten
     */
    private void updateQispaceList(boolean updatePrefs) {
        // get list of allowed servers
        validQispaces = userPrefs.getServerQispaces(selectedServer);
        if (qispaces.length != validQispaces.size()) {
            qispaces = new String[validQispaces.size()];
        }
        //qispaces = validQispaces.toArray(qispaces);
        for (int i=0; i<validQispaces.size(); i++) {
            qispaces[i] = validQispaces.get(i).getPath();
        }
        java.util.Arrays.sort(qispaces);
        qispaceList.setListData(qispaces);

        // mark the default URL (if any)
        String defaultQispace = userPrefs.getDefaultQispace(selectedServer);
        if (!defaultQispace.equals(QiwbPreferences.NO_DEFAULT)) {
            // find the default qiProject in the list
            for (int i=0; i<qispaces.length; i++) {
                if (qispaces[i].equals(defaultQispace)) {
                    qispaces[i] += " (default)";
                }
            }
        }

        // update preferences
        if (updatePrefs) {
            // write out new preferences.
            String prefFileDir = userHOME + File.separator + QIWConstants.QIWB_WORKING_DIR;
            try {
                PreferenceUtils.writePrefs(userPrefs, prefFileDir);
            } catch (QiwIOException qioe) {
                //TODO notify user cannot write preference file
                logger.finest(qioe.getMessage());
            }
        }
    }

    private class StartDirTextFieldKeyListener extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER){
                if(browseButton.isEnabled()){
                    browseButton.doClick();
                }
            }
        }

        public void keyReleased(KeyEvent e) {

            if((e.getKeyCode() >= KeyEvent.VK_0 && e.getKeyCode() <= KeyEvent.VK_9)
                    || (e.getKeyCode() >= KeyEvent.VK_A && e.getKeyCode() <= KeyEvent.VK_Z)
                    || e.getKeyCode() == KeyEvent.VK_UNDERSCORE
                    || e.getKeyCode() == KeyEvent.VK_PERIOD
                    || e.getKeyCode() == KeyEvent.VK_BACK_SPACE
                    || e.getKeyCode() == KeyEvent.VK_SLASH
                    || e.getKeyCode() == KeyEvent.VK_DELETE){
                if (startDirTextField.getText() == null || startDirTextField.getText().trim().length() == 0){
                    browseButton.setEnabled(false);
                }else{
                    browseButton.setEnabled(true);
                }
            }

        }

    }

}
