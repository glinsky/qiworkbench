/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.workbench;

public class QIWMenuConstants {
    public static final String MENU_DESKTOP_NEW_CMD = "Desktop.New";
    public static final String MENU_DESKTOP_OPEN_CMD = "Desktop.Restore";
    public static final String MENU_DESKTOP_SAVE_CMD = "Desktop.Save";
    public static final String MENU_DESKTOP_SAVEAS_CMD = "Desktop.SaveAs";    
    public static final String MENU_DESKTOP_SAVE_QUIT_CMD = "Desktop.Save,Quit";
    public static final String MENU_DESKTOP_QUIT_CMD = "Desktop.Quit";
    public static final String MENU_DESKTOP_RENAME_CMD = "Desktop.Rename";
    public static final String MENU_DESKTOP_ANALYZE_DATA_CMD = "Desktop.AnalyzeData";    
    public static final String MENU_DESKTOP_PREFERENCES_CMD = "Desktop.Preferences";
    public static final String MENU_DESKTOP_RESET_MSG_QUEUE_CMD = "Desktop.ResetMsgQueue";
    public static final String MENU_COMPONENT_NEW_CMD = "Component.New";
    public static final String MENU_COMPONENT_OPEN_CMD = "Component.Open";
    public static final String MENU_COMPONENT_SAVE_CMD = "Component.Save";
    public static final String MENU_COMPONENT_SAVEAS_CMD = "Component.SaveAs";
    public static final String MENU_COMPONENT_SAVE_QUIT_CMD = "Component.Save.Quit";
    public static final String MENU_COMPONENT_QUIT_CMD = "Component.Quit";
    public static final String MENU_COMPONENT_DELETE_CMD = "Component.Delete";
    public static final String MENU_COMPONENT_RENAME_CMD = "Component.Rename";
    public static final String MENU_COMPONENT_REGISTER_CMD = "Component.Register";
    public static final String MENU_COMPONENT_UPDATE_CMD = "Component.Update";
    public static final String MENU_COMPONENT_UPDATE_UPDATE_CMD = "Component.Update.Update Components";
    public static final String MENU_COMPONENT_UPDATE_STATUS_CMD = "Component.Update.Status";
    public static final String MENU_COMPONENT_MANAGER_CMD = "Component.Manager";
    public static final String MENU_TEXT_NEW = "New";
    public static final String MENU_TEXT_OPEN = "Open";
    public static final String MENU_TEXT_SAVE = "Save";
    public static final String MENU_TEXT_SAVE_QUIT = "Save, Quit";
    public static final String MENU_TEXT_SAVE_AS = "Save As";
    public static final String MENU_TEXT_QUIT = "Quit";
    public static final String MENU_TEXT_RENAME = "Rename";
    public static final String MENU_TEXT_DELETE = "Delete";
    public static final String MENU_TEXT_UPDATE = "Update";
    public static final String MENU_TEXT_REGISTER = "Register";
    public static final String MENU_TEXT_ANALYZE_DATA = "Analyze Data";
    public static final String MENU_TEXT_PREFERENCES = "Preferences...";
    public static final String MENU_TEXT_RESET_MSG_QUEUE = "Reset Message Queue";
    public static final String MENU_TEXT_COMPONENT_MANAGER = "Component Manager";
}
