/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.workbench;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.tree.DefaultMutableTreeNode;

public class WorkbenchTreeDescriptor{

  private String displayName;
  private String screenName;
  private DefaultMutableTreeNode node;
  private String nodeType;
  private String cid;
  private List<DefaultMutableTreeNode> children;
  public static Logger logger = Logger.getLogger(WorkbenchTreeDescriptor.class.getName());

  public WorkbenchTreeDescriptor(String displayName, String screenName, DefaultMutableTreeNode node, String nodeType,
          String cid, List<DefaultMutableTreeNode> children) {
    this.displayName = displayName;
    this.screenName = screenName;
    this.node = node;
    this.nodeType = nodeType;
    this.cid = cid;
    this.children = children;
}

  // getters
  public String getDisplayName() {
    return displayName;
  }
  public String getScreenName() {
    return screenName;
  }
  public void setScreenName(String name){
      this.screenName = name;
  }
  public DefaultMutableTreeNode getNode() {
    return node;
  }
  public String getNodeType() {
    return nodeType;
  }
  public String getCID() {
    return cid;
  }

  public List<DefaultMutableTreeNode> getChildren() {
    return children;
  }
  public void setChildren(ArrayList<DefaultMutableTreeNode> children) {
      this.children = children;
  }
  public static int getTreeDescriptorIndex(String displayName) {
    int index = -1;
    for(int i=0; i<WorkbenchData.treeDescriptors.size(); i++) {
      WorkbenchTreeDescriptor wtd = (WorkbenchTreeDescriptor)WorkbenchData.treeDescriptors.get(i);
      if(wtd.getDisplayName().equals(displayName)) {
        index = i;
        break;
      }
    }
    return index;
  }

  public static WorkbenchTreeDescriptor getTreeDescriptor(String displayName) {
    for(int i=0; i<WorkbenchData.treeDescriptors.size(); i++) {
      WorkbenchTreeDescriptor wtd = WorkbenchData.treeDescriptors.get(i);
      if(wtd.getDisplayName().equals(displayName))
        return wtd;
    }
    return null;
  }

  public synchronized void removeChild(DefaultMutableTreeNode child){
      int index = -1;
      for(int i = 0; children != null && i < children.size(); i++){
          if(((String)child.getUserObject()).equals(((DefaultMutableTreeNode)children.get(i)).getUserObject()))
            index = i;
      }
      if(index != -1)
          children.remove(index);
      return;
  }
  // setters
  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }
  public void setNode(DefaultMutableTreeNode node) {
    this.node = node;
  }
  public void setNodeType(String nodeType) {
    this.nodeType = nodeType;
  }
  public void addChild(DefaultMutableTreeNode child) {
    this.children.add(child);
  }
}


