/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.workbench;

import com.bhpb.qiworkbench.api.IComponentDescriptor;
import java.awt.Dimension;
import java.awt.Point;

import nu.xom.converters.DOMConverter;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class DecoratedNode{
    private Node node;
    private IComponentDescriptor descriptor;
    private String oldDisplayName;
    private boolean componentClosed = false;
	private Point location;
    private Dimension dimension;
    
    public DecoratedNode(Node node, IComponentDescriptor descriptor){
        this.node = node;
        this.descriptor = descriptor;
        if(node == null)
            componentClosed = false;
    }

    public void setOldDisplayName(String name){
        oldDisplayName = name;
    }

    public String getOldDisplayName(){
        return oldDisplayName;
    }

    public void setLocation(Point point){
    	location = point;
    }

    public Point getLocation(){
    	return location;
    }


    public void setDimension(Dimension dimension){
    	this.dimension = dimension;
    }

    public Dimension getDimension(){
    	return dimension;
    }


    public boolean isNew(){
        return node == null;
    }

    public void setComponentClosed(boolean flag){
        componentClosed = flag;
    }

    public boolean getComponentClosed(){
        return componentClosed;
    }

    public Node getNode(){
        return node;
    }
    public void setNode(Node node){
        this.node = node;
    }
    public void setDescriptor(IComponentDescriptor descriptor){
        this.descriptor = descriptor;
    }
    public IComponentDescriptor getDescriptor(){
        return descriptor;
    }
    public String toString(){
        nu.xom.Element element = null;
        if(node != null)
            element = DOMConverter.convert((Element)node);
        return "\nComponent display name = " + (descriptor == null ? "null" : descriptor.getPreferredDisplayName())
        + ", component status = "   + (node == null ? "New" : "Existing")
        + ", Old Display Name = "   + (oldDisplayName == null ? "null" : oldDisplayName)
        + ", component visible status = "   + (componentClosed == true ? "closed" : "open")
        + ", component location: "   + (location == null ? "" : "(x=" + location.x + ",y=" + location.y + ")")
        + ", component dimension: "   + (dimension == null ? "" : "(height=" + dimension.height + ",width=" + dimension.width + ")");
        //+"\nNode contents = " + element == null ? "null" : element.toXML();
    }
}

