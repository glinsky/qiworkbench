/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench.workbench;

import com.bhpb.qiworkbench.api.IComponentDescriptor;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import com.bhpb.qiworkbench.client.SocketManager;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
import com.bhpb.qiworkbench.updater.UpdateWizard;
import com.bhpb.qiworkbench.updater.UpdateUtilities;
import com.bhpb.qiworkbench.updater.UpdateStatusDialog;

public class WorkbenchMenuListener implements ActionListener {

    private Component parent;
    public static Logger logger = Logger.getLogger(WorkbenchAction.class.getName());

    public WorkbenchMenuListener(Component parent) {
        this.parent = parent;
    }

    /**
     * Listener for all pull-down and popup menus
     */
    public void actionPerformed(ActionEvent e) {
        /* if action came from a context menu, actionCommand looks like "Popup.Plugin.New", and WorkbenchData.displayName contains
        the applicable tree node name. If action came from pulldown menu, actionCommand looks like "Plugin.New.displayName"
         */
        IComponentDescriptor cd = null;
        String action = e.getActionCommand();
        String action1 = e.getActionCommand();
        if (e.getSource() instanceof QiMenuItem) {
            cd = ((QiMenuItem) e.getSource()).getComponentDescriptor();
        }
        String displayName;

        if (WorkbenchData.verbose > 0) {
            logger.info("WorkbenchMenuListener - Action: " + action);
        // if action came from popup, get displayname from WorkbenchData, otherwise get it from ActionCommand
        }
        if (action.contains("Popup")) {
            displayName = WorkbenchData.displayName;
        } else {
            displayName = action.substring(action.lastIndexOf(".") + 1, action.length());
        }
        if (action1.indexOf(".") != -1) {
            action1 = action1.substring(0, action1.lastIndexOf("."));
        // Desktop.New is in pull-down only
        }
        if (parent != null && parent instanceof WorkbenchGUI) {
            ((WorkbenchGUI) parent).setStatusMessage("");
        }
        if (action.equals(QIWMenuConstants.MENU_DESKTOP_NEW_CMD)) {
            // get menu descriptor for this actionCommand
            WorkbenchMenuDescriptor d = WorkbenchMenuDescriptor.getMenuDescriptor(action);
            if (d == null) {
                logger.info("Internal Error: Cannot find menu descriptor for " + action);
            }
            WorkbenchDialog dialog = new WorkbenchDialog(d.getFrame());
            dialog.saveQuitCloseDesktop("Open New Desktop");
        } else if (action.contains(QIWMenuConstants.MENU_DESKTOP_OPEN_CMD)) {
            logger.info("CurrentNodeMap before Desktop.Restore " + WorkbenchStateManager.getInstance().getCurrentNodeMap());
            WorkbenchAction.restoreDesktop(displayName);

        } else if (action.equals(QIWMenuConstants.MENU_DESKTOP_SAVE_CMD)) {
            logger.info("CurrentNodeMap before Desktop.Save " + WorkbenchStateManager.getInstance().getCurrentNodeMap());
            WorkbenchAction.saveDesktop();
        } else if (action.equals(QIWMenuConstants.MENU_DESKTOP_SAVEAS_CMD)) {
            logger.info("CurrentNodeMap before Desktop.SaveAs " + WorkbenchStateManager.getInstance().getCurrentNodeMap());
            WorkbenchAction.saveDesktopAs();
        } else if (action.equals(QIWMenuConstants.MENU_DESKTOP_SAVE_QUIT_CMD)) {
            logger.info("CurrentNodeMap before Desktop.Save,Quit " + WorkbenchStateManager.getInstance().getCurrentNodeMap());
            WorkbenchAction.saveQuitDesktop();
        } else if (action.equals(QIWMenuConstants.MENU_DESKTOP_QUIT_CMD)) {
            if (System.getProperty("serverUrl") != null) {
                //Close all sockets available for reuse
                SocketManager.getInstance().closeInactiveSockets();
                System.exit(0);
            }
            WorkbenchAction.quitDesktop();
        } else if (action.contains(QIWMenuConstants.MENU_DESKTOP_RENAME_CMD)) {
            if (action.contains("Popup")) {
                action = "Desktop.Rename";
            }
            WorkbenchMenuDescriptor d = WorkbenchMenuDescriptor.getMenuDescriptor(action);
            if (d == null) {
                logger.info("Internal Error: Cannot find menu descriptor for " + action);
            }
            WorkbenchDialog dialog = new WorkbenchDialog(d.getFrame());
            dialog.renameDesktop();
        } else if (action.equals(QIWMenuConstants.MENU_DESKTOP_PREFERENCES_CMD)) {
            javax.swing.JDialog.setDefaultLookAndFeelDecorated(true);

            PreferencesDialog prefDialog = new PreferencesDialog(WorkbenchManager.getInstance());
            prefDialog.setTitle("Preferences");
            prefDialog.setSize(800, 310);
            prefDialog.setDefaultCloseOperation(javax.swing.JDialog.DISPOSE_ON_CLOSE);
            prefDialog.setLocation(parent.getBounds().x + (parent.getBounds().width / 2) - (prefDialog.getBounds().width / 2),
                    parent.getBounds().y + (parent.getBounds().height / 2) - (prefDialog.getBounds().height / 2));

            prefDialog.setVisible(true);

        } else if (action.equals(QIWMenuConstants.MENU_DESKTOP_RESET_MSG_QUEUE_CMD)) {
            MessageDispatcher.getInstance().displayMsgQueue();
            MessageDispatcher.getInstance().resetMsgQueue();
        } else if (action.equals(QIWMenuConstants.MENU_COMPONENT_REGISTER_CMD)) {
            // get menu descriptor for this actionCommand
            WorkbenchMenuDescriptor d = WorkbenchMenuDescriptor.getMenuDescriptor(action);
            if (d == null) {
                logger.info("Internal Error: Cannot find menu descriptor for " + action);
            }
            WorkbenchDialog dialog = new WorkbenchDialog(d.getFrame());
            dialog.registerPlugin();
        } else if (action.equals(QIWMenuConstants.MENU_COMPONENT_UPDATE_UPDATE_CMD)) {
            UpdateWizard wizard = new UpdateWizard(UpdateUtilities.MESSAGE_DISPATCHER_CID, UpdateUtilities.SERVLET_DISP_DESC);
        } else if (action.equals(QIWMenuConstants.MENU_COMPONENT_UPDATE_STATUS_CMD)) {
            new UpdateStatusDialog();
        } else if (action.contains(QIWMenuConstants.MENU_COMPONENT_NEW_CMD)) {
            logger.info("CurrentNodeMap before Component.New " + WorkbenchStateManager.getInstance().getCurrentNodeMap());
            WorkbenchAction.checkComponentTrusted(displayName);
        } else if (action1.contains(QIWMenuConstants.MENU_COMPONENT_OPEN_CMD)) {
            logger.info("CurrentNodeMap before Component.Open " + WorkbenchStateManager.getInstance().getCurrentNodeMap());
            WorkbenchAction.openComponentUI(cd);

        /*if(!action.contains("Popup"))
        WorkbenchData.displayName = action.substring(action.lastIndexOf(".")+1,action.length());
        WorkbenchAction.newPlugin(WorkbenchData.displayName);*/
        } else if (action1.equals(QIWMenuConstants.MENU_COMPONENT_SAVEAS_CMD)) {
            logger.info("CurrentNodeMap before Component.SaveAs " + WorkbenchStateManager.getInstance().getCurrentNodeMap());
            WorkbenchAction.saveComponentAsClone(cd);

        } else if (action1.equals(QIWMenuConstants.MENU_COMPONENT_SAVE_CMD)) {
            logger.info("CurrentNodeMap before Component.Save " + WorkbenchStateManager.getInstance().getCurrentNodeMap());
            WorkbenchAction.saveComponent(cd);
        } else if (action1.equals(QIWMenuConstants.MENU_COMPONENT_SAVE_QUIT_CMD)) {
            logger.info("CurrentNodeMap before Component.Save.Quit " + WorkbenchStateManager.getInstance().getCurrentNodeMap());
            WorkbenchAction.saveQuitComponent(cd);
        } else if (action.contains(QIWMenuConstants.MENU_COMPONENT_DELETE_CMD)) {
            logger.info("CurrentNodeMap before Component.Delete " + WorkbenchStateManager.getInstance().getCurrentNodeMap());
            int st = JOptionPane.showConfirmDialog(parent, "Are you sure you want to remove " + cd.getPreferredDisplayName() + " component from the current saved set?", "Delete confirmation", JOptionPane.YES_NO_OPTION);
            if (st == JOptionPane.YES_OPTION) {
                WorkbenchAction.deleteComponent(cd);
            }
        } else if (action.contains(QIWMenuConstants.MENU_COMPONENT_QUIT_CMD)) {
            logger.info("CurrentNodeMap before Component.Quit " + WorkbenchStateManager.getInstance().getCurrentNodeMap());
            int n = JOptionPane.showConfirmDialog(
                    parent, "Are you sure you want to quit " + cd.getPreferredDisplayName() + " without saving?",
                    "Confirm to quit",
                    JOptionPane.YES_NO_OPTION);
            if (n == JOptionPane.YES_OPTION) {
                WorkbenchAction.closeComponentGUI(cd);
            }
        } else if (action.contains(QIWMenuConstants.MENU_COMPONENT_RENAME_CMD)) {
            logger.info("CurrentNodeMap before Component.Rename " + WorkbenchStateManager.getInstance().getCurrentNodeMap());
            if (cd != null) {
                String displayname = cd.getPreferredDisplayName().trim();
                String s = "";
                while ((s != null) && (s.trim().length() == 0)) {
                    s = (String) JOptionPane.showInputDialog(
                            parent,
                            "Enter the new display name.",
                            "Rename",
                            JOptionPane.PLAIN_MESSAGE,
                            null,
                            null,
                            displayname);
                    logger.info("s = " + s);
                    if (s != null && (s.trim().length() > 0)) {
                        displayname = s;
                        if (WorkbenchStateManager.getInstance().displayNameExist(s)) {
                            JOptionPane.showMessageDialog(parent, "Name entered already exists. Please try another one.");
                            s = "";
                        } else {
                            WorkbenchAction.renameComponent1(cd, s);
                        }
                    }
                }
            }
        } else if (action.contains("Plugin.New")) {
            logger.info("Plugin.New: " + displayName);
            WorkbenchAction.newPlugin(displayName);
        } else if (action.contains("Plugin.Restore")) {
            WorkbenchAction.restorePlugin();
        } else if (action.equals("Plugin.Save" + "." + displayName) || action.equals("Viewer.Save" + "." + displayName)) {
            WorkbenchAction.savePlugin(displayName);
        } else if (action.equals("Plugin.Save.Quit" + "." + displayName) || action.equals("Viewer.Save.Quit" + "." + displayName)) {
            String componentType = "";
            if (action.startsWith("Plugin")) {
                componentType = "Plugin";
            } else if (action.startsWith("Viewer")) {
                componentType = "Viewer";
            }
            WorkbenchAction.saveQuitComponent(componentType, displayName);
        } else if (action.contains("Plugin.Rename") || action.contains("Viewer.Rename")) {
            String componentType;
            if (action.contains("Plugin")) {
                componentType = "Plugin";
            } else {
                componentType = "Viewer";
            }
            WorkbenchAction.renameComponent(componentType, displayName);
        } // quit Plugin or Viewer
        else if (action.contains("Plugin.Quit") || action.contains("Viewer.Quit")) {
            String componentType;
            if (action.contains("Plugin")) {
                componentType = "Plugin";
            } else {
                componentType = "Viewer";
            }
            logger.info("Quitting Type: " + componentType);
            WorkbenchAction.quitComponent(componentType, displayName);
        } //      delete Plugin or Viewer
        else if (action.contains("Plugin.Delete") || action.contains("Viewer.Delete")) {
            String componentType;
            if (action.contains("Plugin")) {
                componentType = "Plugin";
            } else {
                componentType = "Viewer";
            }
            logger.info("Quitting Type: " + componentType);
            WorkbenchAction.deleteComponent(componentType, displayName);
        } else if (action.startsWith("Window")) {
            if (action.equals("Window.Cascade")) {
                WorkbenchAction.cascadeWindows();
            } else if (action.equals("Window.Tile")) {
                WorkbenchAction.tileWindows();
            } else {
                throw new UnsupportedOperationException("Unable to process unknown action: " + action);
            }
        } else if (action.contains("Viewer.New")) {
            WorkbenchAction.newViewer(displayName);
        } else if (action.equals("Help.About")) {
            //  WorkbenchAction.helpAbout();
            javax.swing.JDialog.setDefaultLookAndFeelDecorated(true);
            AboutDialog aboDialog = new AboutDialog();
            aboDialog.setTitle("About");
            aboDialog.setSize(750, 410);
            //aboDialog.setModal(true);
            aboDialog.setDefaultCloseOperation(javax.swing.JDialog.DISPOSE_ON_CLOSE);
            //aboDialog.setLocation(200, 200);
            aboDialog.setLocation(parent.getBounds().x + (parent.getBounds().width / 2) - (aboDialog.getBounds().width / 2),
                    parent.getBounds().y + (parent.getBounds().height / 2) - (aboDialog.getBounds().height / 2));
            aboDialog.setVisible(true);
        }
    }
}