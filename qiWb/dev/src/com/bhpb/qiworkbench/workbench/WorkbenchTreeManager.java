/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.workbench;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.JobManager;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * WorkbenchTreeManager sets up projectTree and fields mouse events.
 * Currently no code exists behind popups.
 *
 * @author Bob Miller
 * @version 1.0
 */
public class WorkbenchTreeManager {
  public static Logger logger = Logger.getLogger(WorkbenchTreeManager.class.getName());

  private MessagingManager guiMessagingMgr;

  private JPopupMenu desktopPopup;
  private JPopupMenu pluginPopup;
  private JPopupMenu activePluginPopup;
  private JPopupMenu suspendedPluginPopup;
  private JPopupMenu viewerPopup;
  private JPopupMenu activeViewerPopup;
  private JPopupMenu suspendedViewerPopup;
  private JPopupMenu activeServicePopup;
  private JPopupMenu suspendedServicePopup;
  private int nTreeNodes;
  private String[] components;
  private String[] nodeType;

  // Popup menu items
  private static final int nDesktopPopupItems = 5;
  private static final String[] desktopPopupItems = {"Open","Save","Save, Quit","Quit","Rename"};
    // items pertaining to plugin/viewer display names
  private static final int nPluginPopupItems = 1;
  private static final String[] pluginPopupItems = {"New"};
  private static final String[] componentKindPopupItems = {"New"};
  //public static final int nActivePluginPopupItems = 5;
  private static final int nActivePluginPopupItems = 5;
  // ActivePluginPopupItems and SuspendedPluginPopupItems apply to plugin instances
  //public static final String[] activePluginPopupItems = {"Save","Save,Quit","Quit","Rename","View"};
  //private static final String[] activePluginPopupItems = {"Save","Save,Quit","Quit","Rename"};
  private static final String[] activePluginPopupItems = {"Save","Save, Quit","Quit","Delete","Rename"};
  private static final int nSuspendedPluginPopupItems = 3;
  private static final String[] suspendedPluginPopupItems = {"New","Restore","Rename"};
  private static final int nServicePopupItems = 2;
  private static final String[] servicePopupItems = {"Status","Quit"};

  private DefaultMutableTreeNode seismicXSect;
  private DefaultMutableTreeNode seismicMap;
  private DefaultMutableTreeNode eventXSect;
  private DefaultMutableTreeNode eventMap;
  private DefaultMutableTreeNode modelXSect;
  private DefaultMutableTreeNode modelMap;
  private JPopupMenu seismicXSectPopup;
  private static JTree componentTree;
  private static JTree dataTree;
  private static DefaultTreeModel componentTreeModel;
  private static DefaultTreeModel dataTreeModel;

  private static List<WorkbenchTreeDescriptor> treeDescriptors;
  private String jobID = "";
  private ArrayList<String> params = new ArrayList<String>();
  private ArrayList<String> stdout = new ArrayList<String>();
  private ArrayList<String> stderr = new ArrayList<String>();
  private int status = 0;
  private int errorStatus;
  private String errorContent;

  private StringBuffer properties;
  private StringBuffer propertyMinMax;
  private Component parent;

  private JobManager jobManager;

  public static JTree getComponentTree() {
    return componentTree;
  }

  /**
   * @deprecated Data managed by qiProjectManager
   */
  public static JTree getDataTree() {
    return dataTree;
  }

  public static DefaultTreeModel getComponentTreeModel() {
    return componentTreeModel;
  }

  public static void addTreeDescriptor(WorkbenchTreeDescriptor d){
      treeDescriptors.add(d);
  }

  public static WorkbenchTreeDescriptor getTreeDescriptorByCID(IComponentDescriptor cd){
      for(WorkbenchTreeDescriptor desc : treeDescriptors){
          if(desc.getCID().equals(cd.getCID()))
              return desc;
      }
      return null;
  }

  /**
   * @deprecated Data managed by qiProjectManager
   */
  public static DefaultTreeModel getDataTreeModel() {
    return dataTreeModel;
  }

/**
 * Build component tree and popup menus.
 *
 * @param parent Parent frame of component tree, i.e, workbench GUI
 * @param guiMessagingMgr Messaging manager of workbench GUI
 */
  protected WorkbenchTreeManager(Component parent, MessagingManager guiMessagingMgr) {
    this.guiMessagingMgr = guiMessagingMgr;
    // component tree
    this.parent = parent;
    componentTree = new JTree();
    componentTree.setToggleClickCount(1000); //Easy way to stop folder expantion, could implement TreeWillExpandListener instead

    //makeNewComponentTree();
//  Enable tool tips.
    ToolTipManager.sharedInstance().registerComponent(componentTree);
    QiDefaultMutableTreeNode componentRootNode = new QiDefaultMutableTreeNode(WorkbenchManager.getInstance().getQispace(),null);
    List<String> lst = Arrays.asList(desktopPopupItems);
    componentRootNode.setNodePopupUITexts(lst);
    componentTreeModel = new DefaultTreeModel(componentRootNode);
    createTreeNodes(componentRootNode,((WorkbenchGUI)parent).getAgent().getRegisteredComponentDescriptors());
    componentTree.setModel(componentTreeModel);
    componentTree.setDragEnabled(true);
    componentTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    makePopupMenus();

    // data tree
    // deprecated; Data managed by qiProjectManager
/*
    dataTree = new JTree();
    DefaultMutableTreeNode dataRootNode = new DefaultMutableTreeNode(WorkbenchManager.getInstance().getProject());
    dataTreeModel = new DefaultTreeModel(dataRootNode);
    makeDataModel(dataRootNode);

    QiwbPreferences userPrefs = MessageDispatcher.getInstance().getUserPrefs();
    if (userPrefs.getPreanalyzeDataOnStartup() == true){
        analayTreeData();
    }
    loadDataTree();
*/
        /*
        dataTree.setModel(dataTreeModel);
        dataTree.setDragEnabled(true);
        dataTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        makePopup();
        */
  }

  /**
   * @deprecated Data managed by qiProjectManager
   */
  public int analayTreeData(){
      int status;
      String locPref = guiMessagingMgr.getLocationPref();
      if (locPref.equals(QIWConstants.LOCAL_SERVICE_PREF)) {
          String hostname = System.getenv("COMPUTERNAME");  //PC
          if (hostname != null) {
              //running on Windows box
              status = populateDataModelWindows(WorkbenchManager.getInstance().getQispace());
          } else {
              //running on Linux/Unix/Mac
              int status1 = writeScanDataScript();
              if (status1 == 0) {
                  status = populateDataModelUNIX(WorkbenchManager.getInstance().getQispace());
              } else
                  status = -1;
          }
      } else {
          status = -1;
          //TODO: Determine if remote server on a Windows or Linux/Unix/Mac box and execute the appropriate remote script to analyze project files
      }

      return status;
  }

  /**
   * @deprecated Data managed by qiProjectManager
   */
  public void loadDataTree(){
      Runnable updateAComponent = new Runnable() {
            public void run() {
                dataTree.setModel(dataTreeModel);
                dataTree.setDragEnabled(true);
                dataTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
                dataTreeModel.reload();
                makePopup();
            }
          };
          SwingUtilities.invokeLater(updateAComponent);
  }

  /**
   * @deprecated Data managed by qiProjectManager
   */
  private void makePopup() {
    JMenuItem item;
    MouseListener dataListener = new DataMouseHandler();

    ActionListener menuListener = new WorkbenchMenuListener(parent);
    dataTree.addMouseListener(dataListener);
    // Seismic XSect popup items
    seismicXSectPopup = new JPopupMenu();
    item = new JMenuItem("View with BHP 2D");
    seismicXSectPopup.add(item);
    item.addActionListener(menuListener);
    item.setActionCommand("Popup.View2D");
  }

  /**
   * @deprecated Data managed by qiProjectManager
   */
  private int writeScanDataScript() {
    params.clear();
    params.add(QIWConstants.LOCAL_SERVICE_PREF);
    params.add(guiMessagingMgr.getUserHOME() + "/.qiworkbench.scanData.sh");
    for(int i=0; i<WorkbenchData.scanDataScript.length; i++)
      params.add(WorkbenchData.scanDataScript[i]);
    String msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.FILE_WRITE_CMD,
                                                             QIWConstants.ARRAYLIST_TYPE,params);
    IQiWorkbenchMsg response = guiMessagingMgr.getMatchingResponseWait(msgID,10000);
    if(response == null || response.isAbnormalStatus()) {
      WorkbenchManager.getInstance().showErrorDialog(QIWConstants.ERROR_DIALOG,
                                                     Thread.currentThread().getStackTrace(),
                                                      "Error occured while writing " +
                                                          guiMessagingMgr.getUserHOME() +
                                                          "/.qiworkbench.scanData.sh",
                                                       new String[] {"File writing permission not set"},
                                                       new String[] {"Make file writable",
                                                      "If already writable, contact workbench support"});
      return 1;
    }
    return 0;
  }

  /**
   * @deprecated Data managed by qiProjectManager. Therefore, it never uses
   * this classes jobManager variable.
   */
  private int populateDataModelUNIX(String project) {
    // run script to get file type and order for each project dataset
    //make sure it is executable
    params.clear();
    params.add(QIWConstants.LOCAL_SERVICE_PREF);
    params.add("sh");
    params.add("-c");
    params.add("chmod +x " + guiMessagingMgr.getUserHOME() + "/.qiworkbench.scanData.sh");
//    jobManager = WorkbenchManager.getInstance().getWorkbenchJobManager();
//so will compile    jobManager = new JobManager(guiMessagingMgr);
    status = jobManager.submitJobWait(params);
    if (status != 0) {
      WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                             "Error occured while opening project " + project);
      return status;
    }
    // execute script
    params.clear();
    params.add(guiMessagingMgr.getLocationPref());
    params.add("sh");
    params.add("-c");
    // TODO Find a better location to store script
    params.add(guiMessagingMgr.getUserHOME() + "/.qiworkbench.scanData.sh " + project);
    status = jobManager.submitJobWait(params);
    if (status != 0) {
      WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                             "Error occured while opening project " + project);
      return status;
    }

    // get stdout
    stdout = jobManager.getStdOut();

    // parse stdout
    String[] fields;
    for(int i=0; i<stdout.size(); i++) {
      // process lines that have TYPE and ORDER
      if(stdout.get(i).contains("TYPE") && stdout.get(i).contains("ORDER")) {
        fields = stdout.get(i).split(" ");
        String fileName = fields[5].trim();
        String headerFile = fields[2].trim() + "/" + fileName + "_0000.HDR";
        int dataType = Integer.parseInt(fields[8].trim());
        int dataOrder = Integer.parseInt(fields[11].trim());
        WorkbenchData.seismicDir.put(fileName,fields[2].trim());
        WorkbenchData.pathListDir.put(fileName,fields[14].trim());
        WorkbenchData.numberOfKeys.put(fileName,fields[17].trim());
        properties = new StringBuffer();
        if(fields.length > 18) {
          for(int j=18; j<fields.length; j+=3) {
            if(fields[j].equals("PROPERTY") || fields[j].equals("HORIZON")) {
              properties.append(fields[j+2].trim());
              properties.append(" ");
            }
          }
        }

        WorkbenchData.properties.put(fileName,properties.toString());
        propertyMinMax = new StringBuffer();
        for(int j=0; j<fields.length; j++) {
          if(fields[j].equals("PROP_MIN")) {
            propertyMinMax.append(fields[j+2]);
            propertyMinMax.append(" ");
            break;
          }
        }

        for(int j=0; j<fields.length; j++) {
          if(fields[j].equals("PROP_MAX")) {
            propertyMinMax.append(fields[j+2]);
            break;
          }
        }

        if (propertyMinMax.length() > 0)
          WorkbenchData.propertyMinMax.put(fileName,propertyMinMax.toString());
        if(dataType == 1 && dataOrder == 1)
          seismicXSect.add(new DefaultMutableTreeNode(fileName));
        else if(dataType == 1 && dataOrder == 2)
          seismicMap.add(new DefaultMutableTreeNode(fileName));
        else if(dataType == 2 && dataOrder == 1)
          eventXSect.add(new DefaultMutableTreeNode(fileName));
        else if(dataType == 2 && dataOrder == 2)
          eventMap.add(new DefaultMutableTreeNode(fileName));
        else if(dataType == 3 && dataOrder == 1)
          modelXSect.add(new DefaultMutableTreeNode(fileName));
        else if(dataType == 3 && dataOrder == 2)
          modelMap.add(new DefaultMutableTreeNode(fileName));
        if(WorkbenchData.verbose > 0)
          logger.info("fileName: " + fileName + " seismicDir: " + WorkbenchData.seismicDir.get(fileName) +
                             " pathListDir: " + WorkbenchData.pathListDir.get(fileName) +
                             " numberOfkeys: " + WorkbenchData.numberOfKeys.get(fileName) +
                             " properties: " + WorkbenchData.properties.get(fileName) +
                             " propertyMinMax: " + WorkbenchData.propertyMinMax.get(fileName));
      } else {
        logger.info("Skipping " + stdout.get(i));
        continue;
      }
    }
    return 0;
  }

  /**
   * @deprecated Data managed by qiProjectManager
   */
  private Integer populateDataModelWindows(String project) {
    // get all the *.dat filenames in project/int_datasets and project/datasets
    ArrayList<String> files = new ArrayList<String>();
    ArrayList<String> dataSets = new ArrayList<String>();
    ArrayList<String> pathList = new ArrayList<String>();
    ArrayList<String> temp = new ArrayList<String>();
    String name = "";
    String headerFile = "";
    files.clear();
    files = getDatFiles(project + "/int_datasets");
    // for files that end in .dat , save dataSet, pathListDir
    for(int i=0; i<files.size(); i++) {
      if(WorkbenchData.verbose > 0)
        logger.info("Checking " + files.get(i));
      if(files.get(i).endsWith(".dat")) {
        name = files.get(i).substring(0,files.get(i).lastIndexOf(".dat"));
        // skip "default"
        if(!name.equals("default")) {
          dataSets.add(name);
          WorkbenchData.pathListDir.put(name,project + "/int_datasets");
        }
        else
          if(WorkbenchData.verbose > 0)
            logger.info("Skipping " + files.get(i));
      }
      else
        if(WorkbenchData.verbose > 0)
          logger.info("Skipping " + files.get(i));
    }
    // repeat for /datasets directory
    files.clear();
    files = getDatFiles(project + "/datasets");
    for(int i=0; i<files.size(); i++) {
      if(WorkbenchData.verbose > 0)
        logger.info("Checking " + files.get(i));
      if(files.get(i).endsWith(".dat")) {
        name = files.get(i).substring(0,files.get(i).lastIndexOf(".dat"));
        // skip "default"
        if(!name.equals("default")) {
          dataSets.add(name);
          WorkbenchData.pathListDir.put(name,project + "/datasets");
        }
        else
          if(WorkbenchData.verbose > 0)
            logger.info("Skipping " + files.get(i));
      }
      else
        if(WorkbenchData.verbose > 0)
          logger.info("Skipping " + files.get(i));
    }
    if(WorkbenchData.verbose > 0)
      logger.info("Found " + WorkbenchData.pathListDir.size() + " datasets in " + project);
/* GJH : 8/6/06 Having no data sets when start up is irrelevant. Besides, same check now made in populateDataModelUNIX
    if(WorkbenchData.pathListDir.size() == 0) {
      WorkbenchManager.getInstance().showErrorDialog(QIWConstants.ERROR_DIALOG,
                                     Thread.currentThread().getStackTrace(),
                                     "Error occured while opening project " + project,
                                     new String[] {"No datasets found"},
                                     new String[] {"Try another project",
                                     "If current project has valid datasets, contact workbench support"});
      return;
    }
*/
    for(int i=0; i<dataSets.size(); i++)
      if(WorkbenchData.verbose > 0)
        logger.info(dataSets.get(i) + " " + WorkbenchData.pathListDir.get(dataSets.get(i)));
    // first record of each .dat file points to seismic directory
    for(int i=0; i<dataSets.size(); i++) {
      params.clear();
      params.add(guiMessagingMgr.getLocationPref());
      params.add(WorkbenchData.pathListDir.get(dataSets.get(i)) + "/" + dataSets.get(i) + ".dat");
      String msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.FILE_READ_CMD,
                                                               QIWConstants.ARRAYLIST_TYPE,params);
      IQiWorkbenchMsg response = guiMessagingMgr.getMatchingResponseWait(msgID,10000);

      if (response == null || response.isAbnormalStatus()) {
        WorkbenchManager.getInstance().showErrorDialog(QIWConstants.ERROR_DIALOG,
                                       Thread.currentThread().getStackTrace(),
                                       "Error reading" + WorkbenchData.pathListDir.get(dataSets.get(i)) +
                                         "/" + dataSets.get(i) + ".dat",
                                       new String[] {"Read permission not set","File is empty"},
                                       new String[] {"Check permissions and content",
                                                     "If file is valid, contact workbench support"});
        continue;
      }
      pathList = (ArrayList<String>)response.getContent();
      WorkbenchData.seismicDir.put(dataSets.get(i),pathList.get(0));
    }
    // get data type and order from each header file
    for(int i=0; i<WorkbenchData.seismicDir.size(); i++) {
      headerFile = WorkbenchData.seismicDir.get(dataSets.get(i)) + "/" + dataSets.get(i)  + "_0000.HDR";
      params.clear();
      params.add(guiMessagingMgr.getLocationPref());
      params.add(headerFile);
      if(WorkbenchData.verbose > 0)
        logger.info("Reading " + headerFile + " fileName= " + dataSets.get(i));
      String msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.FILE_READ_CMD,
                                                               QIWConstants.ARRAYLIST_TYPE,params);
      IQiWorkbenchMsg response = guiMessagingMgr.getMatchingResponseWait(msgID,10000);
      if (response == null || response.isAbnormalStatus()) {
        WorkbenchManager.getInstance().showErrorDialog(QIWConstants.WARNING_DIALOG,
                                       Thread.currentThread().getStackTrace(),
                                       "Error trying to read " + headerFile,
                                       new String[] {"Read permission not set","File is missing"},
                                       new String[] {"Check permissions and content",
                                       "If file is valid, contact workbench support"});
        continue;
      }
      temp = (ArrayList<String>)response.getContent();
      properties = new StringBuffer();
      propertyMinMax = new StringBuffer();
      int dataType = -1;
      int dataOrder = -1;
      int propertyMin = 0;
      int propertyMax = 0;
      String[] fields;
      for(int j=0; j<temp.size(); j++) {
        if(temp.get(j).startsWith("TYPE =")) {
          fields = temp.get(j).split("=");
          dataType = Integer.parseInt(fields[1].trim());
        }
        else if(temp.get(j).startsWith("ORDER =")) {
          fields = temp.get(j).split("=");
          dataOrder = Integer.parseInt(fields[1].trim());
        }
        if(temp.get(j).startsWith("PROPERTY") || temp.get(j).startsWith("HORIZON")) {
          fields = temp.get(j).split("=");
          properties.append(fields[1].trim());
          properties.append(" ");
        }
        if(temp.get(j).startsWith("NKEYS")) {
          fields = temp.get(j).split("=");
          WorkbenchData.numberOfKeys.put(dataSets.get(i),fields[1].trim());
        }
        if(temp.get(j).startsWith("PROP_MIN") && propertyMin == 0) {
          propertyMin = 1;
          fields = temp.get(j).split("=");
          propertyMinMax.append(fields[1].trim());
          propertyMinMax.append(" ");
        }
        if(temp.get(j).startsWith("PROP_MAX") && propertyMax == 0) {
          propertyMax = 1;
          fields = temp.get(j).split("=");
          propertyMinMax.append(fields[1].trim());
        }
      }
      if(dataType == 1 && dataOrder == 1)
        seismicXSect.add(new DefaultMutableTreeNode(dataSets.get(i)));
      else if(dataType == 1 && dataOrder == 2)
        seismicMap.add(new DefaultMutableTreeNode(dataSets.get(i)));
      else if(dataType == 2 && dataOrder == 1)
        eventXSect.add(new DefaultMutableTreeNode(dataSets.get(i)));
      else if(dataType == 2 && dataOrder == 2)
        eventMap.add(new DefaultMutableTreeNode(dataSets.get(i)));
      else if(dataType == 3 && dataOrder == 1)
        modelXSect.add(new DefaultMutableTreeNode(dataSets.get(i)));
      else if(dataType == 3 && dataOrder == 2)
        modelMap.add(new DefaultMutableTreeNode(dataSets.get(i)));
      WorkbenchData.properties.put(dataSets.get(i),properties.toString());
      WorkbenchData.propertyMinMax.put(dataSets.get(i),propertyMinMax.toString());
    }
    if(WorkbenchData.verbose > 0)
      for(int i=0; i<WorkbenchData.seismicDir.size(); i++)
        logger.info("dataSet: " + dataSets.get(i) +
                           " pathListDir: " + WorkbenchData.pathListDir.get(dataSets.get(i)) +
                           " seismicDir: " + WorkbenchData.seismicDir.get(dataSets.get(i)) +
                           " numberOfKeys: " + WorkbenchData.numberOfKeys.get(dataSets.get(i)) +
                           " properties: " + WorkbenchData.properties.get(dataSets.get(i)) +
                           " propertyMinMax: " + WorkbenchData.propertyMinMax.get(dataSets.get(i)));
    return 0;

  }

  /**
   * @deprecated Data managed by qiProjectManager
   */
  private ArrayList<String> getDatFiles(String dir) {
    ArrayList<String> files = new ArrayList<String>();
    params.clear();
    params.add(guiMessagingMgr.getLocationPref());
    params.add(dir);
    String msgID = guiMessagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_FILE_LIST_CMD,
                                                             QIWConstants.ARRAYLIST_TYPE,params);
    IQiWorkbenchMsg response = guiMessagingMgr.getMatchingResponseWait(msgID,10000);
    if(response != null && !response.isAbnormalStatus())
      files = (ArrayList<String>)response.getContent();
    else
        logger.severe("Response to GET_FILE_LIST_CMD either returning null due to timed out or responding with abnormal message");
    return files;
  }

  private void makeNewComponentTree() {
    nTreeNodes = WorkbenchData.nAvailablePlugins + WorkbenchData.nAvailableViewers;
    components = new String[nTreeNodes];
    nodeType = new String[nTreeNodes];
    for(int i=0; i<WorkbenchData.nAvailablePlugins; i++) {
      components[i] = WorkbenchData.registeredPluginsComponentDescriptors.get(i).getDisplayName();
      nodeType[i] = WorkbenchData.PLUGIN_NODE_TYPE;
    }
    for(int i=WorkbenchData.nAvailablePlugins; i<nTreeNodes; i++) {
      components[i] = WorkbenchData.registeredViewersComponentDescriptors.
                                     get(i - WorkbenchData.nAvailablePlugins).getDisplayName();
      nodeType[i] = WorkbenchData.VIEWER_NODE_TYPE;
    }
  }

/**
 * Populate JTree nodes, using data from projectTree in WorkbenchData.
 * @param top DefaultMutableTreeNode
 * @param projectTree String array
 * @param type integer array of node types: 0=root, 1=plugin name 2=viewer type, 3=plugin instance, 4=viewer instance, 5=service
 * @deprecated Never called.
 */
  @SuppressWarnings("unchecked")
  private void createTreeNodes(DefaultMutableTreeNode top, String projectTree[], String type[]) {
    // TODO retrieve tree nodes for 'project'
    String pluginName = "";
    String pluginInstanceName = "";
    String serviceName = "";
    String cid = "";
    DefaultMutableTreeNode pluginNode = null;
    DefaultMutableTreeNode pluginInstanceNode = null;
    DefaultMutableTreeNode serviceNode = null;
    if (!type[0].equals(WorkbenchData.PLUGIN_NODE_TYPE)  &&
       !type[0].equals(WorkbenchData.VIEWER_NODE_TYPE))
      WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                   "First node must be PLUGIN_NODE_TYPE or VIEWER_NODE_TYPE");
    WorkbenchData.treeDescriptors  = new ArrayList<WorkbenchTreeDescriptor>(nTreeNodes);
    WorkbenchTreeDescriptor t;
    for(int i=0; i<nTreeNodes; i++) {
      String screenName = "";
      if(type[i].equals(WorkbenchData.PLUGIN_NODE_TYPE) || type[i].equals(WorkbenchData.VIEWER_NODE_TYPE)) {
        if(type[i].equals(WorkbenchData.PLUGIN_NODE_TYPE)) {
          screenName = "-plugin: " + projectTree[i];
          pluginNode = new DefaultMutableTreeNode(screenName);
        }
        else if(type[i].equals(WorkbenchData.VIEWER_NODE_TYPE)) {
          screenName = "-viewer: " + projectTree[i];
          pluginNode = new DefaultMutableTreeNode(screenName);
        }
        pluginName = projectTree[i];
        top.add(pluginNode);
        //  get CID
        if(type[i].equals(WorkbenchData.PLUGIN_NODE_TYPE)) {
          cid = "";
          for(int j=0; j<WorkbenchData.nAvailablePlugins; j++) {
            if(WorkbenchData.registeredPluginsComponentDescriptors.get(j).getDisplayName().equals(projectTree[i])) {
              cid = WorkbenchData.registeredPluginsComponentDescriptors.get(j).getCID();
              break;
            }
          }
          if (cid.equals("")) {
            WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                                   "Error starting workbench");
            return;
          }
        }
        else if (type[i].equals(WorkbenchData.VIEWER_NODE_TYPE)){
          cid = "";
          for(int j=0; j<WorkbenchData.nAvailableViewers; j++) {
            if(WorkbenchData.registeredViewersComponentDescriptors.get(j).getDisplayName().equals(projectTree[i])) {
              cid = WorkbenchData.registeredViewersComponentDescriptors.get(j).getCID();
              break;
            }
          }
          if (cid.equals("")) {
            WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                                   "Error starting workbench");
            return;
          }
        }
        WorkbenchData.treeDescriptors.add(new WorkbenchTreeDescriptor(projectTree[i],screenName,pluginNode,type[i],cid,
                                          new ArrayList<DefaultMutableTreeNode>(10)));
      }
      else if(type[i].equals(WorkbenchData.PLUGIN_INSTANCE_NODE_TYPE) || type[i].equals(WorkbenchData.VIEWER_INSTANCE_NODE_TYPE)) {
        if(pluginNode != null) {
          pluginInstanceName = projectTree[i];
          screenName = "-" + pluginInstanceName;
          pluginInstanceNode = new DefaultMutableTreeNode("-" + projectTree[i]);
          pluginNode.add(pluginInstanceNode);
          WorkbenchData.treeDescriptors.add(new WorkbenchTreeDescriptor(projectTree[i],screenName,pluginInstanceNode,type[i],
                                            "",new ArrayList<DefaultMutableTreeNode>(10)));
          t = WorkbenchTreeDescriptor.getTreeDescriptor(pluginName);
          t.addChild(pluginInstanceNode);
          // add item to activePlugins or activeViewers
          /*if(type[i] == 3)
            WorkbenchData.activePlugins.add(projectTree[i]);
          else
            WorkbenchData.activeViewers.add(projectTree[i]);*/
        }
        else {
          WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                                 "Error starting workbench");
          return;
        }
      }
      else if(type[i].equals(WorkbenchData.ACTIVE_SERVICE_NODE_TYPE) || type[i].equals(WorkbenchData.SUSPENDED_SERVICE_NODE_TYPE)) {
        if(pluginInstanceNode != null) {
          serviceName = projectTree[i];
          screenName = "-service: " + serviceName;
          serviceNode = new DefaultMutableTreeNode("-service: " + projectTree[i]);
          pluginInstanceNode.add(serviceNode);
          WorkbenchData.treeDescriptors.add(new WorkbenchTreeDescriptor(projectTree[i],screenName,serviceNode,type[i],"",null));
          t = WorkbenchTreeDescriptor.getTreeDescriptor(pluginInstanceName);
          t.addChild(serviceNode);
          // add item to activeServices or suspendedServices
          /*if(type[i] == 5)
            WorkbenchData.activeServices.add(projectTree[i]);*/
        }
        else {
          WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                                 "Error starting workbench");
          return;
        }
      }
    }
    if(WorkbenchData.verbose > 1) {
      for(int i=0; i<nTreeNodes; i++) {
        t = (WorkbenchTreeDescriptor)WorkbenchData.treeDescriptors.get(i);
        logger.info("Display Name: " + t.getDisplayName() + " Screen Name: " + t.getScreenName() + " CID: " + t.getCID() +
                           " Node Type: " + t.getNodeType() + "Children: " + t.getChildren());
      }
    }
  }

  /**
   * Populate JTree nodes, using data from projectTree in WorkbenchData.
   * @param top DefaultMutableTreeNode
   * @param map IComponentDescriptor map
   */
    @SuppressWarnings("unchecked")
    private void createTreeNodes(DefaultMutableTreeNode top, Map<String,List<IComponentDescriptor>> map) {
        if(map == null || map.isEmpty())
            return;
      // TODO retrieve tree nodes for 'project'
      String cid = "";
      QiDefaultMutableTreeNode compNode = null;
      WorkbenchData.treeDescriptors  = new ArrayList<WorkbenchTreeDescriptor>();

      Map<IComponentDescriptor,String> registedComponentMap = new HashMap<IComponentDescriptor,String>();
      List<IComponentDescriptor> registedPluginList = map.get(QIWConstants.COMPONENT_PLUGIN_TYPE);
      for(IComponentDescriptor desc : registedPluginList){
          registedComponentMap.put(desc, WorkbenchData.PLUGIN_NODE_TYPE);
      }

      List<IComponentDescriptor> registedViewerList = map.get(QIWConstants.COMPONENT_VIEWER_TYPE);
      for(IComponentDescriptor desc : registedViewerList){
          registedComponentMap.put(desc, WorkbenchData.VIEWER_NODE_TYPE);
      }

      //Sorts Map
      Map<IComponentDescriptor,String> sortedComponentMap = new TreeMap(registedComponentMap);

      if(treeDescriptors == null)
          treeDescriptors = new ArrayList<WorkbenchTreeDescriptor>();

      List popNameList = Arrays.asList(componentKindPopupItems);
      Iterator iter = sortedComponentMap.keySet().iterator();
      while(iter.hasNext()) //or your for as well
      {
          Object key = iter.next();
          Object value = sortedComponentMap.get(key);
          String screenName = "";
          screenName = ((IComponentDescriptor)key).getDisplayName();
          compNode = new QiDefaultMutableTreeNode(screenName,(IComponentDescriptor)key);
          compNode.setNodePopupUITexts(popNameList);
          top.add(compNode);
          treeDescriptors.add(new WorkbenchTreeDescriptor(((IComponentDescriptor)key).getDisplayName(),screenName,compNode,(String)value,cid,
                  new ArrayList<DefaultMutableTreeNode>()));
      }

      /*List<IComponentDescriptor> registedPluginList = map.get(QIWConstants.COMPONENT_PLUGIN_TYPE);
      Object[] pluginList = registedPluginList.toArray();
      java.util.Arrays.sort(pluginList);
      registedPluginList.clear();
      for (int i = 0; i < pluginList.length; i++)
          registedPluginList.add((IComponentDescriptor)pluginList[i]);

      if(treeDescriptors == null)
          treeDescriptors = new ArrayList<WorkbenchTreeDescriptor>();

      List popNameList = Arrays.asList(componentKindPopupItems);
      for(IComponentDescriptor desc : registedPluginList){
        String screenName = "";
            //screenName = "-plugin: " + desc.getDisplayName();
            screenName = desc.getDisplayName();
            compNode = new QiDefaultMutableTreeNode(screenName,desc);
            compNode.setNodePopupUITexts(popNameList);
            top.add(compNode);
            treeDescriptors.add(new WorkbenchTreeDescriptor(desc.getDisplayName(),screenName,compNode,WorkbenchData.PLUGIN_NODE_TYPE,cid,
                    new ArrayList<DefaultMutableTreeNode>()));
      }

      List<IComponentDescriptor> registedViewerList = map.get(QIWConstants.COMPONENT_VIEWER_TYPE);
      Object[] viewerList = registedViewerList.toArray();
      java.util.Arrays.sort(viewerList);
      registedViewerList.clear();
      for (int i = 0; i < viewerList.length; i++)
          registedViewerList.add((IComponentDescriptor)viewerList[i]);

      if(treeDescriptors == null)
          treeDescriptors = new ArrayList<WorkbenchTreeDescriptor>();
      for(IComponentDescriptor desc : registedViewerList){
        String screenName = "";
        //screenName = "-viewer: " + desc.getDisplayName();
        screenName = desc.getDisplayName();
        compNode = new QiDefaultMutableTreeNode(screenName,desc);
        compNode.setNodePopupUITexts(popNameList);
        top.add(compNode);
        treeDescriptors.add(new WorkbenchTreeDescriptor(desc.getDisplayName(),screenName,compNode,WorkbenchData.VIEWER_NODE_TYPE,cid,
                new ArrayList<DefaultMutableTreeNode>()));
      }*/
      WorkbenchData.treeDescriptors = (ArrayList<WorkbenchTreeDescriptor>)treeDescriptors;
    }

/**
 * Make context menus for each projectTree node type using data from WorkbenchData.
 *
 */
  public void makePopupMenus() {
    JMenuItem item;
    MouseListener treeListener = new MouseHandler1();
      ActionListener menuListener = new WorkbenchMenuListener(parent);
    // Desktop popup items
    desktopPopup = new JPopupMenu("Desktop Actions");
    for(int i=0; i<nDesktopPopupItems; i++) {
      item = new JMenuItem(desktopPopupItems[i]);
      desktopPopup.add(item);
      item.addActionListener(menuListener);
      item.setActionCommand("Popup.Desktop." + desktopPopupItems[i]);
      // Desktop.Rename not implemented
      if(desktopPopupItems[i].equals("Rename"))
        item.setEnabled(false);
    }
    componentTree.addMouseListener(treeListener);

    //  plugin popup items
    pluginPopup = new JPopupMenu("Plugin Actions");
    for(int i=0; i<nPluginPopupItems; i++) {
      item = new JMenuItem(pluginPopupItems[i]);
      pluginPopup.add(item);
      item.addActionListener(menuListener);
      item.setActionCommand("Popup.Plugin." + pluginPopupItems[i]);
    }

    // Active plugin instance popup items
    activePluginPopup = new JPopupMenu("Active Plugin Actions");
    for(int i=0; i<nActivePluginPopupItems; i++) {
      item = new JMenuItem(activePluginPopupItems[i]);
      activePluginPopup.add(item);
      item.addActionListener(menuListener);
      item.setActionCommand("Popup.Plugin." + activePluginPopupItems[i]);
    }

    // Suspended plugin instance popup items
    suspendedPluginPopup = new JPopupMenu("Suspended Plugin Actions");
    for(int i=0; i<nSuspendedPluginPopupItems; i++) {
      item = new JMenuItem(suspendedPluginPopupItems[i]);
      suspendedPluginPopup.add(item);
      item.addActionListener(menuListener);
      item.setActionCommand("Popup.Plugin." + suspendedPluginPopupItems[i]);
     }

     // viewer popups are the same except for names referenced by MouseHandler and names passed to WorkbenchMenuListener
     //  viewer popup items
     viewerPopup = new JPopupMenu("Viewer Actions");
     for(int i=0; i<nPluginPopupItems; i++) {
       item = new JMenuItem(pluginPopupItems[i]);
       viewerPopup.add(item);
       item.addActionListener(menuListener);
       item.setActionCommand("Popup.Viewer" + "." + pluginPopupItems[i]);
     }

    // Active plugin instance popup items
    activeViewerPopup = new JPopupMenu("Active Viewer Actions");
    for(int i=0; i<nActivePluginPopupItems; i++) {
      item = new JMenuItem(activePluginPopupItems[i]);
      activeViewerPopup.add(item);
      item.addActionListener(menuListener);
      item.setActionCommand("Popup.Viewer." + activePluginPopupItems[i]);
    }

    // Suspended viewer instance popup items
    suspendedViewerPopup = new JPopupMenu("Suspended Viewer Actions");
    for(int i=0; i<nSuspendedPluginPopupItems; i++) {
      item = new JMenuItem(suspendedPluginPopupItems[i]);
      suspendedViewerPopup.add(item);
      item.addActionListener(menuListener);
      item.setActionCommand("Popup.Viewer." + suspendedPluginPopupItems[i]);
    }

    // Active service popup items
    activeServicePopup = new JPopupMenu("Active Service Actions");
    for(int i=0; i<nServicePopupItems; i++) {
      item = new JMenuItem(servicePopupItems[i]);
      activeServicePopup.add(item);
      item.addActionListener(menuListener);
      item.setActionCommand("Popup.Service." + servicePopupItems[i]);
      if(servicePopupItems[i].equals("Status") || servicePopupItems[i].equals("Quit"))
        item.setEnabled(false);
    }

    // Suspended service popup items
    suspendedServicePopup = new JPopupMenu("Suspended Service Actions");
    for(int i=0; i<nServicePopupItems; i++) {
      item = new JMenuItem(servicePopupItems[i]);
      suspendedServicePopup.add(item);
      item.addActionListener(menuListener);
      item.setActionCommand("Popup.Service." + servicePopupItems[i]);
    }
  }


  /**
   * Make context menus for each projectTree node type using data from WorkbenchData.
   *
   */
    public void makePopupMenus1() {
      JMenuItem item;
      MouseListener treeListener = new MouseHandler1();
        ActionListener menuListener = new WorkbenchMenuListener(parent);
      // Desktop popup items
      desktopPopup = new JPopupMenu("Desktop Actions");
      for(int i = 0; i < desktopPopupItems.length; i++) {
        item = new JMenuItem(desktopPopupItems[i]);
        desktopPopup.add(item);
        item.addActionListener(menuListener);
        item.setActionCommand("Popup.Desktop." + desktopPopupItems[i]);
        // Desktop.Rename not implemented
        if(desktopPopupItems[i].equals("Rename"))
          item.setEnabled(false);
      }
      componentTree.addMouseListener(treeListener);

      //  plugin popup items
      pluginPopup = new JPopupMenu("Plugin Actions");
      for(int i=0; i<nPluginPopupItems; i++) {
        item = new JMenuItem(pluginPopupItems[i]);
        pluginPopup.add(item);
        item.addActionListener(menuListener);
        item.setActionCommand("Popup.Plugin." + pluginPopupItems[i]);
      }

      // Active plugin instance popup items
      activePluginPopup = new JPopupMenu("Active Plugin Actions");
      for(int i=0; i<nActivePluginPopupItems; i++) {
        item = new JMenuItem(activePluginPopupItems[i]);
        activePluginPopup.add(item);
        item.addActionListener(menuListener);
        item.setActionCommand("Popup.Plugin." + activePluginPopupItems[i]);
      }

      // Suspended plugin instance popup items
      suspendedPluginPopup = new JPopupMenu("Suspended Plugin Actions");
      for(int i=0; i<nSuspendedPluginPopupItems; i++) {
        item = new JMenuItem(suspendedPluginPopupItems[i]);
        suspendedPluginPopup.add(item);
        item.addActionListener(menuListener);
        item.setActionCommand("Popup.Plugin." + suspendedPluginPopupItems[i]);
       }

       // viewer popups are the same except for names referenced by MouseHandler and names passed to WorkbenchMenuListener
       //  viewer popup items
       viewerPopup = new JPopupMenu("Viewer Actions");
       for(int i=0; i<nPluginPopupItems; i++) {
         item = new JMenuItem(pluginPopupItems[i]);
         viewerPopup.add(item);
         item.addActionListener(menuListener);
         item.setActionCommand("Popup.Viewer" + "." + pluginPopupItems[i]);
       }

      // Active plugin instance popup items
      activeViewerPopup = new JPopupMenu("Active Viewer Actions");
      for(int i=0; i<nActivePluginPopupItems; i++) {
        item = new JMenuItem(activePluginPopupItems[i]);
        activeViewerPopup.add(item);
        item.addActionListener(menuListener);
        item.setActionCommand("Popup.Viewer." + activePluginPopupItems[i]);
      }

      // Suspended viewer instance popup items
      suspendedViewerPopup = new JPopupMenu("Suspended Viewer Actions");
      for(int i=0; i<nSuspendedPluginPopupItems; i++) {
        item = new JMenuItem(suspendedPluginPopupItems[i]);
        suspendedViewerPopup.add(item);
        item.addActionListener(menuListener);
        item.setActionCommand("Popup.Viewer." + suspendedPluginPopupItems[i]);
      }

      // Active service popup items
      activeServicePopup = new JPopupMenu("Active Service Actions");
      for(int i=0; i<nServicePopupItems; i++) {
        item = new JMenuItem(servicePopupItems[i]);
        activeServicePopup.add(item);
        item.addActionListener(menuListener);
        item.setActionCommand("Popup.Service." + servicePopupItems[i]);
        if(servicePopupItems[i].equals("Status") || servicePopupItems[i].equals("Quit"))
          item.setEnabled(false);
      }

      // Suspended service popup items
      suspendedServicePopup = new JPopupMenu("Suspended Service Actions");
      for(int i=0; i<nServicePopupItems; i++) {
        item = new JMenuItem(servicePopupItems[i]);
        suspendedServicePopup.add(item);
        item.addActionListener(menuListener);
        item.setActionCommand("Popup.Service." + servicePopupItems[i]);
      }
    }

  /**
   * @deprecated Data managed by qiProjectManager
   */
  protected void makeDataModel(DefaultMutableTreeNode root) {
    seismicXSect = new DefaultMutableTreeNode("Seismic Cross-Sections");
    root.add(seismicXSect);
    seismicMap = new DefaultMutableTreeNode("Seismic Maps");
    root.add(seismicMap);
    eventXSect = new DefaultMutableTreeNode("Event Cross-Sections");
    root.add(eventXSect);
    eventMap = new DefaultMutableTreeNode("Event Maps");
    root.add(eventMap);
    modelXSect = new DefaultMutableTreeNode("Model Cross-Sections");
    root.add(modelXSect);
    modelMap = new DefaultMutableTreeNode("Model Maps");
    root.add(modelMap);
  }


  public static int getTreeDescriptorIndex(String displayName) {
    int index = -1;
    for(int i=0; i<treeDescriptors.size(); i++) {
      WorkbenchTreeDescriptor wtd = (WorkbenchTreeDescriptor)treeDescriptors.get(i);
      if(wtd.getDisplayName().equals(displayName)) {
        index = i;
        break;
      }
    }
    return index;
  }

  public static WorkbenchTreeDescriptor getTreeDescriptor(String displayName) {
    for(int i=0; i<treeDescriptors.size(); i++) {
      WorkbenchTreeDescriptor wtd = treeDescriptors.get(i);
      if(wtd.getDisplayName().equals(displayName))
        return wtd;
    }
    return null;
  }

  /**
   * Handle JTree mouse events.
    * it displays the proper popup based on the node type at which MB3 is pressed
   *
  */
  public class MouseHandler extends MouseAdapter implements ActionListener{
    MouseHandler () {
    }
    public void actionPerformed(ActionEvent e){

    }

    public void mouseReleased(MouseEvent e) {
      String name = "";
      String nodeType = "";

      if(e.isPopupTrigger() || e.getButton() == MouseEvent.BUTTON3) {
        int x = e.getX();
        int y = e.getY();
        TreePath path = componentTree.getPathForLocation(x,y);
        if (path == null)
          return;
        componentTree.scrollPathToVisible(path);
        componentTree.setSelectionPath(path);
        int nPathElements = path.getPathCount();
        if(nPathElements == 1) {
          desktopPopup.show(e.getComponent(),x,y);
          return;
        }
        else {
          DefaultMutableTreeNode node = (DefaultMutableTreeNode)path.getLastPathComponent();
          name = (String)node.getUserObject();
          WorkbenchData.displayName = parseName(name,nPathElements);
          WorkbenchTreeDescriptor t = WorkbenchTreeDescriptor.getTreeDescriptor(WorkbenchData.displayName);
          if (t == null) {
            WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                                   "Error starting workbench");
            return;
          }
          nodeType = t.getNodeType();
        }
        if(nPathElements == 2 && nodeType.equals(WorkbenchData.PLUGIN_NODE_TYPE)) {
          // Currently, only Amplitude Extraction is implemented
          if(WorkbenchData.displayName.equals("AmpExt"))
            pluginPopup.show(e.getComponent(),x,y);
        }
        else if(nPathElements == 2 && nodeType.equals(WorkbenchData.VIEWER_NODE_TYPE)) {
          // Currently only 2Dviewer is implemented
          if(WorkbenchData.displayName.equals("bhpViewer"))
            viewerPopup.show(e.getComponent(),x,y);
        }
        else if(nPathElements == 3 && nodeType.equals(WorkbenchData.PLUGIN_INSTANCE_NODE_TYPE))
          activePluginPopup.show(e.getComponent(),x,y);
        else if(nPathElements == 3 && nodeType.equals(WorkbenchData.VIEWER_INSTANCE_NODE_TYPE))
          activeViewerPopup.show(e.getComponent(),x,y);
        else if(nPathElements == 4 && nodeType.equals(WorkbenchData.ACTIVE_SERVICE_NODE_TYPE))
          activeServicePopup.show(e.getComponent(),x,y);
        else if(nPathElements == 4 && nodeType.equals(WorkbenchData.SUSPENDED_SERVICE_NODE_TYPE))
          suspendedServicePopup.show(e.getComponent(),x,y);
        else
          WorkbenchManager.getInstance().showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                                 "UNKNOWN tree node");
      }
    }
    private String parseName(String name, int n) {
      /* if n=2 --> plugin name or viewer name, e.g. -plugin: Amp Ext
       * if n=3 --> instance name, e.g. -plugin1
       * if n=4 --> service name, e.g. -service: filereader
      */
      String displayName = "";
      if(n == 2 || n == 4)
        displayName = name.substring(name.lastIndexOf(":")+2,name.length());
      else if(n == 3)
        displayName = name.substring(name.lastIndexOf("-")+1,name.length());
      return displayName;
    }
  }

  /**
   * Handle JTree mouse events.
   * It displays the proper popup based on the node type at which MB3 is pressed
   *
  */
  class MouseHandler1 extends MouseAdapter{
    MouseHandler1 () {}

    public void mouseReleased(MouseEvent e) {
      String name = "";
      int x = e.getX();
      int y = e.getY();
      TreePath path = componentTree.getPathForLocation(x,y);
      if (path == null){
          return;
      }
      ActionListener menuListener = new WorkbenchMenuListener(parent);
      componentTree.scrollPathToVisible(path);
      componentTree.setSelectionPath(path);
      DefaultMutableTreeNode node = (DefaultMutableTreeNode)path.getLastPathComponent();
      if(SwingUtilities.isLeftMouseButton(e) && node.isLeaf() ){ //left-click on active component level
          if(node instanceof QiDefaultMutableTreeNode) {
                IComponentDescriptor cd = ((QiDefaultMutableTreeNode)node).getComponentDescriptor();
                JInternalFrame[] iframes = ((WorkbenchGUI)parent).getDesktopPane().getAllFrames();
                for(int i = 0; i < iframes.length; i++){
                    String sname = ((QiDefaultMutableTreeNode)node).getComponentDescriptor().getPreferredDisplayName();
                    if(iframes[i].getTitle().contains(sname)){
                        String [] temp = iframes[i].getTitle().split(" +");
                        if(temp != null && temp.length > 1 && temp[1].equals(sname)){
                        try{
                            iframes[i].setSelected(true);
                        }catch(PropertyVetoException pve){
                            pve.printStackTrace();
                        }
                        }
                    }
                }
          }
      }

      //if(e.getClickCount() == 2 && !node.isRoot()/* && node.isLeaf() */){ //left-click on active component level

      if(e.getClickCount() == 2 && node.isLeaf() && node.getLevel()== 2){ //double click to open an closed component
          logger.info("DOUBLE CLICK");
          if(node instanceof QiDefaultMutableTreeNode) {
                IComponentDescriptor cd = ((QiDefaultMutableTreeNode)node).getComponentDescriptor();
                if(!WorkbenchStateManager.getInstance().componentOpen(cd)){
                    WorkbenchAction.openComponentUI(cd);
                }
          }
      }

      //if(e.getClickCount() == 2 && !node.isRoot()/* && node.isLeaf() */){ //left-click on active component level
      if(e.getClickCount() == 2 && !node.isRoot() && node.getLevel() == 1){ //double click on activate a new component
          logger.info("DOUBLE CLICK");
          if(node instanceof QiDefaultMutableTreeNode) {
                IComponentDescriptor cd = ((QiDefaultMutableTreeNode)node).getComponentDescriptor();
                if(!WorkbenchStateManager.getInstance().componentOpen(cd)){
                    WorkbenchAction.checkComponentTrusted(((QiDefaultMutableTreeNode)node).getComponentDescriptor().getPreferredDisplayName());
                }
          }
      }
      if( e.isPopupTrigger() || SwingUtilities.isRightMouseButton(e) ){ //right-click
        JPopupMenu popup = null;
        if(node instanceof QiDefaultMutableTreeNode) {
            List<String> list = ((QiDefaultMutableTreeNode)node).getNodePopupUITexts();
            IComponentDescriptor cd = ((QiDefaultMutableTreeNode)node).getComponentDescriptor();
            name = (String)node.getUserObject();
            if(node.isRoot())   //desktop level
                popup = new JPopupMenu("Desktop Actions");
            else if(node.isLeaf()) //active component level
                popup = new JPopupMenu("Active Component Actions");
            else //component type level
                popup = new JPopupMenu("Component Actions");

            for(String s : list){
                QiMenuItem item = new QiMenuItem(s,cd);
                item.addActionListener(menuListener);
                popup.add(item);
                if(node.isRoot()){ //workbench
                    String action = ((WorkbenchGUI)parent).getWorkbenchActionCommandByLabel(s);
                    item.setActionCommand(action);
                } else if(node.isLeaf()) { //component
                    String action = ((WorkbenchGUI)parent).getComponentActionCommandByLabel(s);
                    String displayName = "";
                    String nameTemp = ((QiDefaultMutableTreeNode)node).getComponentDescriptor().getPreferredDisplayName().trim();
                    if(nameTemp.length() > 0)
                        displayName = nameTemp;
                    else
                        displayName = ((QiDefaultMutableTreeNode)node).getComponentDescriptor().getDisplayName();
                    item.setActionCommand(action + "." + displayName);
                } else if(node.getLevel() == 1) {//component kind
                    String action = ((WorkbenchGUI)parent).getComponentActionCommandByLabel(s);
                    String displayName = "";
                    displayName = ((QiDefaultMutableTreeNode)node).getComponentDescriptor().getDisplayName();
                    item.setActionCommand(action + "." + displayName);
                }
            }
            popup.show(e.getComponent(),x,y);
        }
      }
    }
  }

  public class DataMouseHandler extends MouseAdapter {
    DataMouseHandler () {
    }
    public void mouseReleased(MouseEvent e) {
      if(e.isPopupTrigger() || e.getButton() == MouseEvent.BUTTON3) {
        int x = e.getX();
        int y = e.getY();
        TreePath path = dataTree.getPathForLocation(x,y);
        if (path == null)
          return;
        dataTree.scrollPathToVisible(path);
        dataTree.setSelectionPath(path);
        int nPathElements = path.getPathCount();
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)path.getLastPathComponent();
        if(nPathElements == 3) {
          WorkbenchData.displayName = (String)node.getUserObject();
          WorkbenchData.dataTypeAndOrder = node.getParent().toString();
          seismicXSectPopup.show(e.getComponent(),x,y);
        }
      }
    }
  }
}
