/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006-2007  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.workbench;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Logger;

import javax.help.CSH;
import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.KeyStroke;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;

import com.bhpb.qiworkbench.QiwbPreferences;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.QiSpaceDescriptor;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.util.GenericFileFilter;
import com.bhpb.qiworkbench.compAPI.CommonUtil;
import com.bhpb.qiworkbench.compAPI.IconResource;
import com.bhpb.qiworkbench.client.qiWbConfiguration;
import com.bhpb.qiworkbench.client.util.PreferenceUtils;
import com.bhpb.qiworkbench.client.util.QiFileChooserDescriptor;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.ProgressUtil;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.DispatcherConnector;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;

/**
 * Dialog to first select a runtime Tomcat server, then a qiProject
 * accessible by the selected server and finally, optionally select a saved
 * session. If a saved session is selected, a qiWorkbench will be opened
 * and restored to the saved state; otherwise, it will be launched devoid
 * of any components.
 * @author Gil Hansen
 * @version 1.0
 */
public class ProjectSelectorDialog extends javax.swing.JDialog {
    private static Logger logger = Logger.getLogger(ProjectSelectorDialog.class.getName());

    /** Indicates kind of qiSpace: "P" (project); "W" (workspace for projects) */
    private String qispaceKind = QiSpaceDescriptor.PROJECT_KIND;

    //TODO: share the project subdirectories across components so one common
    //      place to access
    // Directories of data groups defined in qiProjectManager's project-defn.xml
    public static final String DATASETS_DIR = "datasets";
    public static final String HORIZONS_DIR = "horizons";
    public static final String SCRIPTS_DIR = "scripts";
    public static final String SEGY_DIR = "segy";
    public static final String SEISMIC_DIR = "seismic";
    public static final String SESSIONS_DIR = "sessions";
    public static final String TEMP_DIR = "tmp";
    public static final String WELLS_DIR = "wells";
    public static final String WORKBENCHES_DIR = "workbenches";
    public static final String XML_DIR = "xml";

    Component parentGUI;

    WorkbenchManager agent = null;

    //Differentiates which file choose is called, i.e., for a project
    //or for a saved session.
    boolean selectProject = true;
    //Differentiates whether selecting or creating a project
    boolean createProject = false;

    HelpBroker helpBroker;
    HelpSet helpSet;

    /** Creates new form ProjectSelectorDialog */
    public ProjectSelectorDialog(WorkbenchManager agent, java.awt.Component parent, boolean modal) {
        super(JOptionPane.getFrameForComponent(parent), modal);
        this.agent = agent;
        this.parentGUI = parent;

        // set up online Help
        try {
            ClassLoader cl = WorkbenchGUI.class.getClassLoader();
            String helpsetName = "com/bhpb/qiwbHelp/qiwb_helpset.hs";
            URL helpSetURL = HelpSet.findHelpSet(cl, helpsetName);
            helpSet = new HelpSet(cl, helpSetURL);
            helpBroker = helpSet.createHelpBroker();
        } catch (Exception e) {
            logger.warning("Error loading project selector online Help: " + e.getMessage());
        }
        CSH.setHelpIDString(this, "selector.dialog");

        initComponents();

        this.setTitle("Project Selector");

        rememberDefaultServerCheckBox.setEnabled(false);
        rememberDefaultServerCheckBox.setSelected(false);
        rememberDefaultProjectCheckBox.setEnabled(false);
        rememberDefaultProjectCheckBox.setSelected(false);
        rememberDefaultSessionCheckBox.setEnabled(false);
        rememberDefaultSessionCheckBox.setSelected(false);
        neverAskAgainCheckBox.setEnabled(false);
        neverAskAgainCheckBox.setSelected(false);

        browseProjectButton.setEnabled(false);
        addServerUrlButton.setEnabled(true);

        //make OK button the default
        this.getRootPane().setDefaultButton(okButton);
        this.getRootPane().getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "OK");
        this.getRootPane().getActionMap().put("OK", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                okButtonActionPerformed(e);
            }
        });

        okButton.setEnabled(false);

        //Group the server location radio buttons.
        ButtonGroup locationGroup = new ButtonGroup();
        locationGroup.add(localServerRadioButton);
        locationGroup.add(remoteServerRadioButton);
        //Fix for qiWorkbench Issue #27
        //The default for a selected/entered server is 'local', which the
        //user can always change.
        localServerRadioButton.setSelected(true);
        createProjectButton.setEnabled(true);

        //Populate the list of available runtime Tomcat servers. Servers
        //in the user's preferences take precedence. If there are none,
        //use the servers in the qiWorkbench config file.
        serversListModel = new DefaultListModel();
        QiwbPreferences userPrefs = MessageDispatcher.getInstance().getUserPrefs();
        ArrayList<String> tomcatPref = userPrefs.getServerURLs();
        if (tomcatPref.size() > 0) {
            for (int i=0; i<tomcatPref.size(); i++) {
                serversListModel.addElement(tomcatPref.get(i));
            }
        } else {
            ArrayList<Map<String, String>> tomcats = qiWbConfiguration.getInstance().getRuntimeTomcatAttributes();
            if (tomcats.size() > 0) {
                for (int i=0; i<tomcats.size(); i++) {
                    String tomcatServerAddr = tomcats.get(i).get("tomcaturl");
                    serversListModel.addElement(tomcatServerAddr);
                    if (!userPrefs.getServerURLs().contains(tomcatServerAddr)) {
                        userPrefs.addServer(tomcatServerAddr);
                    }
                }
                writePrefs(userPrefs); // store the modifications to the initial, invalid empty file.
            }
        }

        serversList.setModel(serversListModel);
        serversList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting() && isServerSelected()) {
					String selectedServer = getSelectedServer();
                    if (isServerAccessible(selectedServer)) {
                        selectServer(selectedServer);
                    } else {
                        //not an accessible server
                        rememberDefaultServerCheckBox.setEnabled(false);
                        rememberDefaultServerCheckBox.setSelected(false);
                        rememberDefaultProjectCheckBox.setEnabled(false);
                        rememberDefaultProjectCheckBox.setSelected(false);
                        rememberDefaultSessionCheckBox.setEnabled(false);
                        rememberDefaultSessionCheckBox.setSelected(false);
                        neverAskAgainCheckBox.setEnabled(false);
                        neverAskAgainCheckBox.setSelected(false);
                    }
                }
            }
        });

        //If there is a default server, select it in the list
        String defaultServer = userPrefs.getDefaultServer();
        boolean selectedDefaultServer = false;
        if (!defaultServer.equals("")) {
            //Search list model for server and select
            for (int i=0; i<serversListModel.size(); i++) {
                String server = (String)serversListModel.getElementAt(i);
                if (server.equals(defaultServer)) {
                    serversList.setSelectedIndex(i);
                    selectedDefaultServer = true;
                }
            }

            if (selectedDefaultServer && isServerAccessible(defaultServer)) {
                selectServer(defaultServer);
            }
        }

        projectsList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting() && isProjectSelected()) {
					String selectedProject = getSelectedProject();
                    if (projectExists(selectedProject)) {
                        if (rememberDefaultServerCheckBox.isSelected()) {
                            rememberDefaultProjectCheckBox.setEnabled(true);
                            rememberDefaultProjectCheckBox.setSelected(false);
                            rememberDefaultSessionCheckBox.setEnabled(false);
                            rememberDefaultSessionCheckBox.setSelected(false);
                        }
    
                        //If the selected project is the default for the selected default server,
                        //set its Remember As Default checkbox.
                        String selectedServer = getSelectedServer();
                        QiwbPreferences userPrefs = MessageDispatcher.getInstance().getUserPrefs();
                        String defaultServer = userPrefs.getDefaultServer();
                        boolean selectedDefaultServer = defaultServer.equals(selectedServer) ? true : false;
                        if (selectedDefaultServer && rememberDefaultServerCheckBox.isSelected()) {
                            String defaultProject = userPrefs.getDefaultQispace(selectedServer);
                            if (defaultProject.equals(selectedProject)) {
                                rememberDefaultProjectCheckBox.setSelected(true);
                            }
                        }
    
                        //Populate the sessions list with those associated with the selected project
                        ArrayList<String> sessionList = userPrefs.getStateFiles(selectedServer, selectedProject);
                        sessionsListModel = new DefaultListModel();
                        for (int i=0; sessionList != null && i < sessionList.size(); i++)
                            sessionsListModel.addElement(sessionList.get(i));
                        sessionsList.setModel(sessionsListModel);
    
                        browseSessionButton.setEnabled(true);
    
                        okButton.setEnabled(true);
                    }
                }
            }
        });

        sessionsList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (isSessionSelected()) {
                    if (rememberDefaultServerCheckBox.isSelected() &&
                        rememberDefaultProjectCheckBox.isSelected()) {
                            rememberDefaultSessionCheckBox.setEnabled(true);
                            rememberDefaultSessionCheckBox.setSelected(false);
                    }
                }
            }
        });
        pack();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        dialogScrollPane = new javax.swing.JScrollPane();
        dialogPanel = new javax.swing.JPanel();
        selectIntro1Label = new javax.swing.JLabel();
        selectIntro2Label = new javax.swing.JLabel();
        selectServerPanel = new javax.swing.JPanel();
        selectServerLabel = new javax.swing.JLabel();
        serverListScrollPane = new javax.swing.JScrollPane();
        serversList = new javax.swing.JList();
        localServerRadioButton = new javax.swing.JRadioButton();
        remoteServerRadioButton = new javax.swing.JRadioButton();
        rememberDefaultServerCheckBox = new javax.swing.JCheckBox();
        addServerUrlButton = new javax.swing.JButton();
        selectIntro3Label = new javax.swing.JLabel();
//        selectIntro4Label = new javax.swing.JLabel();
        selectProjectPanel = new javax.swing.JPanel();
        projectListScrollPane = new javax.swing.JScrollPane();
        projectsList = new javax.swing.JList();
        rememberDefaultProjectCheckBox = new javax.swing.JCheckBox();
        selectProjectLabel = new javax.swing.JLabel();
        browseProjectButton = new javax.swing.JButton();
        createProjectButton = new javax.swing.JButton();
        selectIntro5Label = new javax.swing.JLabel();
        selectSessionPanel = new javax.swing.JPanel();
        sessionListScrollPane = new javax.swing.JScrollPane();
        sessionsList = new javax.swing.JList();
        rememberDefaultSessionCheckBox = new javax.swing.JCheckBox();
        selectSessionLabel = new javax.swing.JLabel();
        browseSessionButton = new javax.swing.JButton();
        neverAskAgainCheckBox = new javax.swing.JCheckBox();
        helpButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        okButton = new javax.swing.JButton();

        setTitle("Project Selector");
        setModal(true);
        selectIntro1Label.setForeground(new java.awt.Color(102, 102, 255));
        selectIntro1Label.setText("You must select a qiProject accessible from a runtime Tomcat server.");

        selectIntro2Label.setForeground(new java.awt.Color(102, 102, 255));
        selectIntro2Label.setText("First, select an allowed, accessible runtime Tomcat server.");

        selectServerPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 255)), "Select Runtime Tomcat Server", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(153, 84, 20)));
        selectServerLabel.setForeground(new java.awt.Color(153, 0, 153));
        selectServerLabel.setText("Select or enter the URL of a server from which your qiProjects are accessible.");

        serversList.setToolTipText("<html>Select a runtime Tomcat server from the list<br>of those available at your site. A site default <br>server may already be selected. Afterwards,<br>specify if is either local or remote relative<br>to your machine. Then select the checkbox<br>if you want the server to be your default<br>the next time you launch the qiWorkbench.</html>");

        serverListScrollPane.setViewportView(serversList);

        localServerRadioButton.setText("Local");
        localServerRadioButton.setToolTipText("<html>A runtime Tomcat server is local relative<br>to your machine if your machine sees<br>the same file systems as the server.</html>");
        localServerRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        localServerRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        localServerRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                localServerRadioButtonActionPerformed(evt);
            }
        });

        remoteServerRadioButton.setText("Remote");
        remoteServerRadioButton.setToolTipText("<html>A runtime Tomcat server is remote relative to<br>your machine if your machine does NOT see<br>the same file systems as the server. <br><br>File access will be slower than for a local<br>server.</html>");
        remoteServerRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        remoteServerRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        remoteServerRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remoteServerRadioButtonActionPerformed(evt);
            }
        });

        rememberDefaultServerCheckBox.setText("remember as default");
        rememberDefaultServerCheckBox.setToolTipText("<html>Select if the selected server is to be<br>the default server the next time you<br>launch the qiWorkbench.</html>");
        rememberDefaultServerCheckBox.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        rememberDefaultServerCheckBox.setMargin(new java.awt.Insets(0, 0, 0, 0));
        rememberDefaultServerCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rememberDefaultServerCheckBoxActionPerformed(evt);
            }
        });

        addServerUrlButton.setText("Add");
        addServerUrlButton.setToolTipText("Enter the URL of a new runtime Tomcat server.");
        addServerUrlButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addServerUrlButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout selectServerPanelLayout = new org.jdesktop.layout.GroupLayout(selectServerPanel);
        selectServerPanel.setLayout(selectServerPanelLayout);
        selectServerPanelLayout.setHorizontalGroup(
            selectServerPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(selectServerPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(selectServerPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(selectServerPanelLayout.createSequentialGroup()
                        .add(localServerRadioButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(remoteServerRadioButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 340, Short.MAX_VALUE)
                        .add(rememberDefaultServerCheckBox))
                    .add(serverListScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE)
                    .add(selectServerLabel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE)
                    .add(addServerUrlButton))
                .addContainerGap())
        );
        selectServerPanelLayout.setVerticalGroup(
            selectServerPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(selectServerPanelLayout.createSequentialGroup()
                .add(selectServerLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(serverListScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 98, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(addServerUrlButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(selectServerPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(localServerRadioButton)
                    .add(remoteServerRadioButton)
                    .add(rememberDefaultServerCheckBox)))
        );

        selectIntro3Label.setForeground(new java.awt.Color(102, 102, 255));
        selectIntro3Label.setText("Second, select a qiProject accessible by the server.");

//        selectIntro4Label.setForeground(new java.awt.Color(102, 102, 255));
//        selectIntro4Label.setText("you must select a qiProject in the qiProjectManager associated with each qiComponent.");

        selectProjectPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 255)), "Select qiProject", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(153, 84, 20)));
        projectsList.setToolTipText("<html>Select a qiProject from the list<br>of those availabe on the selected server.<br>If the list is empty or doesn't contain the<br>entry you want, browse to it. Afterwards,<br>select the checkbox if you want the <br>qiProject to be the default the<br>next time you launch the qiWorkbench.</html>");

        projectListScrollPane.setViewportView(projectsList);

        rememberDefaultProjectCheckBox.setText("remember as default");
        rememberDefaultProjectCheckBox.setToolTipText("<html>Select if the selected qiProject<br>\n is to be the default the next time you<br>launch the qiWorkbench.</html>");
        rememberDefaultProjectCheckBox.setActionCommand("rememberSelection1");
        rememberDefaultProjectCheckBox.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        rememberDefaultProjectCheckBox.setMargin(new java.awt.Insets(0, 0, 0, 0));
        rememberDefaultProjectCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rememberDefaultProjectCheckBoxActionPerformed(evt);
            }
        });

        selectProjectLabel.setForeground(new java.awt.Color(153, 0, 153));
        selectProjectLabel.setText("Select or enter a qiProject from those associated with the selected server.");

        browseProjectButton.setText("Add");
        browseProjectButton.setToolTipText("<html>Add an existing qiProject.</html>");
        browseProjectButton.setEnabled(false);
        browseProjectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseProjectButtonActionPerformed(evt);
            }
        });

        createProjectButton.setText("Create");
        createProjectButton.setToolTipText("<html>Create a new qiProject populating it with empty,<br> default subdirectories for housing project data.</html>");
        createProjectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createProjectButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout selectProjectPanelLayout = new org.jdesktop.layout.GroupLayout(selectProjectPanel);
        selectProjectPanel.setLayout(selectProjectPanelLayout);
        selectProjectPanelLayout.setHorizontalGroup(
            selectProjectPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(selectProjectPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(selectProjectPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(projectListScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 585, Short.MAX_VALUE)
                    .add(selectProjectLabel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 585, Short.MAX_VALUE)
                    .add(selectProjectPanelLayout.createSequentialGroup()
                        .add(browseProjectButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(createProjectButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 346, Short.MAX_VALUE)
                        .add(rememberDefaultProjectCheckBox)))
                .addContainerGap())
        );
        selectProjectPanelLayout.setVerticalGroup(
            selectProjectPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(selectProjectPanelLayout.createSequentialGroup()
                .add(selectProjectLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(projectListScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(selectProjectPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(browseProjectButton)
                    .add(rememberDefaultProjectCheckBox)
                    .add(createProjectButton))
                .addContainerGap())
        );

        selectIntro5Label.setForeground(new java.awt.Color(102, 102, 255));
        selectIntro5Label.setText("Finally, optionally select a qiWorbkench session previously saved in your selected qiProject.");

        selectSessionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 255)), "Select saved session", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(153, 84, 20)));
        sessionsList.setToolTipText("<html>Select a saved session  from the list<br>of those availabe in the selected project.<br>If the list is empty or doesn't contain the<br>entry you want, browse to it. Afterwards,<br>select the checkbox if you want the <br>saved session to be the default the<br>next time you launch the qiWorkbench.</html>");

        sessionListScrollPane.setViewportView(sessionsList);

        rememberDefaultSessionCheckBox.setText("remember as default");
        rememberDefaultSessionCheckBox.setToolTipText("<html>Select if the selected saved session is<br>\n  to be the default the next time you<br>launch the qiWorkbench.</html>");
        rememberDefaultSessionCheckBox.setActionCommand("rememberSelection1");
        rememberDefaultSessionCheckBox.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        rememberDefaultSessionCheckBox.setMargin(new java.awt.Insets(0, 0, 0, 0));
        rememberDefaultSessionCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rememberDefaultSessionCheckBoxActionPerformed(evt);
            }
        });

        selectSessionLabel.setForeground(new java.awt.Color(153, 0, 153));
        selectSessionLabel.setText("Select or enter a saved session from those associated with the selected qiProject.");

        browseSessionButton.setText("Add");
        browseSessionButton.setToolTipText("<html>Add an existing saved session  in the selected qiProject</html>");
        browseSessionButton.setEnabled(false);
        browseSessionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseSessionButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout selectSessionPanelLayout = new org.jdesktop.layout.GroupLayout(selectSessionPanel);
        selectSessionPanel.setLayout(selectSessionPanelLayout);
        selectSessionPanelLayout.setHorizontalGroup(
            selectSessionPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(selectSessionPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(selectSessionPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(selectSessionLabel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 561, Short.MAX_VALUE)
                    .add(selectSessionPanelLayout.createSequentialGroup()
                        .add(browseSessionButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 391, Short.MAX_VALUE)
                        .add(rememberDefaultSessionCheckBox))
                    .add(sessionListScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 561, Short.MAX_VALUE))
                .addContainerGap())
        );
        selectSessionPanelLayout.setVerticalGroup(
            selectSessionPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(selectSessionPanelLayout.createSequentialGroup()
                .add(selectSessionLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(sessionListScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(selectSessionPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(browseSessionButton)
                    .add(rememberDefaultSessionCheckBox))
                .addContainerGap())
        );

        neverAskAgainCheckBox.setText("never ask again");
        neverAskAgainCheckBox.setToolTipText("<html>If a default server and project are specified, don't<br>show this dialog. To change the defaults, select<br>File -> Preferences in the qiWorkbench.</html>");
        neverAskAgainCheckBox.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        neverAskAgainCheckBox.setMargin(new java.awt.Insets(0, 0, 0, 0));
        neverAskAgainCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                neverAskAgainCheckBoxActionPerformed(evt);
            }
        });

        helpButton.setText("Help");
        helpButton.addActionListener(new CSH.DisplayHelpFromSource(helpBroker));
/*
        helpButton.setToolTipText("<html>Get online helf for this dialog.</html>");
        helpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                helpButtonActionPerformed(evt);
            }
        });
*/
        cancelButton.setText("Cancel");
        cancelButton.setToolTipText("<html>Cancel launching the qiWorkbench</html>");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        okButton.setText("OK");
        okButton.setToolTipText("<html>OK is enabled if you have selected a server<br>and a qiProject. End the dialog and<br>launch the qiWorkbench.</html>");
        okButton.setEnabled(false);
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout dialogPanelLayout = new org.jdesktop.layout.GroupLayout(dialogPanel);
        dialogPanel.setLayout(dialogPanelLayout);
        dialogPanelLayout.setHorizontalGroup(
            dialogPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, dialogPanelLayout.createSequentialGroup()
                .add(10, 10, 10)
                .add(selectIntro1Label, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 599, Short.MAX_VALUE))
            .add(dialogPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(selectIntro2Label, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 589, Short.MAX_VALUE)
                .addContainerGap())
            .add(dialogPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(selectServerPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .add(dialogPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(selectIntro3Label, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 591, Short.MAX_VALUE)
                .add(8, 8, 8))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, dialogPanelLayout.createSequentialGroup()
                .add(8, 8, 8)
                .add(selectProjectPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .add(dialogPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(selectIntro5Label, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 589, Short.MAX_VALUE)
                .addContainerGap())
            .add(dialogPanelLayout.createSequentialGroup()
                .add(8, 8, 8)
                .add(selectSessionPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .add(dialogPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(neverAskAgainCheckBox)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 319, Short.MAX_VALUE)
                .add(okButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cancelButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(helpButton)
                .addContainerGap())
/*
            .add(dialogPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(selectIntro4Label, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 591, Short.MAX_VALUE)
                .add(8, 8, 8))
*/
        );
        dialogPanelLayout.setVerticalGroup(
            dialogPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(dialogPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(selectIntro1Label)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(selectIntro2Label)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(selectServerPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(selectIntro3Label)
//                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
//                .add(selectIntro4Label)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(selectProjectPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(selectIntro5Label)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(selectSessionPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(dialogPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(neverAskAgainCheckBox)
                    .add(dialogPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(helpButton)
                        .add(cancelButton)
                        .add(okButton)))
                .addContainerGap())
        );
        dialogScrollPane.setViewportView(dialogPanel);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, dialogScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 611, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(dialogScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Adds the URL of a new runtime Tomcat server to the list
     * of available servers (if not already on the list) and
     * select it. The server has already been checked for
     * accessibility. Also, add the server to the user's
     * preferences.
     * @param serverURL URL of new runtime Tomcat server
     */
    public void addServer(String serverURL){
        //Check if server already on the list. If not add it.
        //Regardless, select it.
        int idx = -1;
        int selectedIdx = -1;
        for (int i=0; i<serversListModel.size(); i++) {
            String server = (String)serversListModel.getElementAt(i);
            if (server.equals(serverURL)) {
                idx = i;
                selectedIdx = i;
                break;
            }
        }
        //If not on list, add it.
        if (idx == -1) {
            serversListModel.addElement(serverURL);
            serversList.setModel(serversListModel);
            selectedIdx = serversListModel.size()-1;
        }

        if (isServerAccessible(serverURL)) {
            serversList.setSelectedIndex(selectedIdx);
            selectServer(serverURL);
        } else {
            //nothing selected
            rememberDefaultServerCheckBox.setEnabled(false);
            rememberDefaultServerCheckBox.setSelected(false);
            rememberDefaultProjectCheckBox.setEnabled(false);
            rememberDefaultProjectCheckBox.setSelected(false);
            rememberDefaultSessionCheckBox.setEnabled(false);
            rememberDefaultSessionCheckBox.setSelected(false);
            neverAskAgainCheckBox.setEnabled(false);
            neverAskAgainCheckBox.setSelected(false);
        }

        //make added item visible (even though it may not be selected)
        JScrollBar sbar = serverListScrollPane.getVerticalScrollBar();
        sbar.setValue(sbar.getMaximum());

        //add server to preferences
        QiwbPreferences userPrefs = MessageDispatcher.getInstance().getUserPrefs();
        userPrefs.addServer(serverURL);
    }

    /**
     * Add file chooser selection.
     * @param selection The full path of either a qiProject or saved session.
     */
    public void addSelection(String selection) {
        if (selectProject) {
            if (createProject) createQiProject(selection);
            else addQispace(selection);
        } else addSession(selection);
    }

    /**
     * Add the qiProject selected in the file chooser to the list
     * of available qiProjects (if not already on the list) and
     * select it.
     * @param qiProject full path of qiProject selected in the file chooser.
     */
    public void addQispace(String qiProject) {
        //Check if qiProject already on the list. If not add it.
        //Regardless, select it.
        int idx = -1;
        for (int i=0; i<projectsListModel.size(); i++) {
            String qiSpace = (String)projectsListModel.getElementAt(i);
            if (qiSpace.equals(qiProject)) {
                idx = i;
                projectsList.setSelectedIndex(i);
                break;
            }
        }
        //If not on list, add it. Also, add it to the user's preferences.
        if (idx == -1) {
            //add to display list
            projectsListModel.addElement(qiProject);
            projectsList.setModel(projectsListModel);
            projectsList.setSelectedIndex(projectsListModel.size()-1);

            //add specified project to user's preferences for the selected server
            String selectedServer = getSelectedServer();
            agent.addQispace(qiProject, qispaceKind, selectedServer);
        }

        //make added item visible
        JScrollBar sbar = projectListScrollPane.getVerticalScrollBar();
        sbar.setValue(sbar.getMaximum());

        if (rememberDefaultServerCheckBox.isSelected()) {
            rememberDefaultProjectCheckBox.setEnabled(true);
            rememberDefaultProjectCheckBox.setSelected(false);
            rememberDefaultSessionCheckBox.setEnabled(false);
            rememberDefaultSessionCheckBox.setSelected(false);
        }

        browseSessionButton.setEnabled(true);
        okButton.setEnabled(true);
    }

    /**
     * Create a new qiProject within a qiSpace and populate it with
     * default subdirectories used to house the project's data.
     * Give an error if the project already exists; otherwise, create
     * it and add it to the list of available qiProject and select it.
     * @param qiProject Full path of qiProject directory
     */
    public void createQiProject(String qiProject) {
        //check if qiProject already exists as a file or directory
        boolean isLocalServer = localServerRadioButton.isSelected() ? true : false;
        boolean dirExists = false;
        File dir = new File(qiProject);
        if (isLocalServer) {
            if (dir.exists()) {
                dirExists = true;
                if (dir.isDirectory())
                    JOptionPane.showMessageDialog(this, "Cannot create a qiProject that already exists.",
                        "Create Error", JOptionPane.ERROR_MESSAGE);
                else
                    JOptionPane.showMessageDialog(this, "Cannot create a qiProject with the same name as a file.",
                        "Create Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            String msgId = agent.getMessagingManager().sendRequest(QIWConstants.CMD_MSG, QIWConstants.IS_REMOTE_DIRECTORY_CMD, QIWConstants.STRING_TYPE, qiProject, true);
            IQiWorkbenchMsg response = agent.getMessagingManager().getMatchingResponseWait(msgId,5000);

            if (response != null && !response.isAbnormalStatus()) {
                String isDir = (String)response.getContent();
                if (isDir.equals("yes")) {
                    dirExists = true;
                    JOptionPane.showMessageDialog(this, "Cannot create a qiProject that already exists.",
                        "Create Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this, (String)response.getContent(),
                    "Create Error", JOptionPane.ERROR_MESSAGE);
            }

            //TODO: add command IS_REMOTE_FILE_CMD
        }

        //create qiProject and its subdirectories
        boolean dirCreated = true;
        if (!dirExists) {
            //create list of default subdirectories
            ArrayList<String> subdirs = new ArrayList<String>();
            subdirs.add(DATASETS_DIR);
            subdirs.add(HORIZONS_DIR);
            subdirs.add(SCRIPTS_DIR);
            subdirs.add(SEGY_DIR);
            subdirs.add(SEISMIC_DIR);
            subdirs.add(SESSIONS_DIR);
            subdirs.add(TEMP_DIR);
            subdirs.add(WELLS_DIR);
            subdirs.add(WORKBENCHES_DIR);
            subdirs.add(XML_DIR);

            String filesep = File.separator;

            if (isLocalServer) {
                //create the project directory
                dir.mkdir();
                //create the project subdirectories
                for (int i=0; i<subdirs.size(); i++) {
                    dir = new File(qiProject+filesep+subdirs.get(i));
                    dir.mkdir();
                }
            } else {
                ArrayList params = new ArrayList();
                params.add(qiProject);
                params.add(subdirs);
                String msgId = agent.getMessagingManager().sendRequest(QIWConstants.CMD_MSG, QIWConstants.CREATE_REMOTE_PROJECT_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);
                IQiWorkbenchMsg response = agent.getMessagingManager().getMatchingResponseWait(msgId,5000);

                if (response != null && !response.isAbnormalStatus()) {
                    String created = (String)response.getContent();
                    if (!created.equals("yes")) {
                        dirCreated = false;
                        JOptionPane.showMessageDialog(this, created, "Create Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(this, (String)response.getContent(), "Create Error", JOptionPane.ERROR_MESSAGE);
                }
            }

            if (dirCreated) addQispace(qiProject);
        }
    }

    /**
     * Add the save state file selected in the file chooser to the list
     * of available sessions (if not already on the list) and
     * select it.
     * @param session Path of session file selected in the file chooser.
     */
    public void addSession(String savedSession){
        //Check if session already on the list. If not add it.
        //Regardless, select it.
        int idx = -1;
        for (int i=0; i<sessionsListModel.size(); i++) {
            String session = (String)sessionsListModel.getElementAt(i);
            if (session.equals(savedSession)) {
                idx = i;
                sessionsList.setSelectedIndex(i);
                break;
            }
        }
        //If not on list, add it.
        if (idx == -1) {
            sessionsListModel.addElement(savedSession);
            sessionsList.setModel(sessionsListModel);
            sessionsList.setSelectedIndex(sessionsListModel.size()-1);

            //add specified session to user's preferences for the selected server and project
            String selectedServer = getSelectedServer();
            String qispaceDir = getSelectedProject();
            String selectedSession = getSelectedSession();
            QiwbPreferences userPrefs = MessageDispatcher.getInstance().getUserPrefs();
            userPrefs.addDesktop(selectedServer, qispaceDir, selectedSession);
            writePrefs(userPrefs);
        }

        //make added item visible
        JScrollBar sbar = sessionListScrollPane.getVerticalScrollBar();
        sbar.setValue(sbar.getMaximum());
    }

    private void createProjectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createProjectButtonActionPerformed
        //Specify which of the two file choosers is being invoked.
        selectProject = true;
        createProject = true;

        String[] extensions = new String[]{"*", "*.*"};
        FileFilter filter = (FileFilter) new GenericFileFilter(extensions, "Directory (*)",true);

        String selectedServer = getSelectedServer();
        String tomcatLoc = localServerRadioButton.isSelected() ? QIWConstants.LOCAL_PREF : QIWConstants.REMOTE_PREF;
        MessageDispatcher.getInstance().setTomcatURL(selectedServer);
        //NOTE: Server location preference is needed by the file chooser which it gets
        //from the Message Dispatcher. The OS info of the server is also needed if it
        //is remote.

        //TODO these methods should probably be combined in a synchronized block
        //otherwise an unroutable message may be dispatched while the dispatcher has a new 'tomcatLoc'
        //but an old runtimeServletDispDesc
        MessageDispatcher.getInstance().setLocationPref(tomcatLoc);
        if (MessageDispatcher.getInstance().resetRuntimeServer(selectedServer) == false)
            JOptionPane.showMessageDialog(this, "Failed to set runtime Tomcat URL to: " + selectedServer + ".", "Error Registering Runtime Tomcat URL",  JOptionPane.ERROR_MESSAGE);
        else {
            ArrayList<String> remoteOSInfoList = agent.getRemoteOSInfo(tomcatLoc);
            MessageDispatcher.getInstance().setRemoteOSInfoList(remoteOSInfoList);

            //Start in either the qiSpace home directory or the user's home dir
            String userid = CommonUtil.getUserID();
            String qiSpaceBaseDir = qiWbConfiguration.getInstance().getBaseDirsAttributes().get("qiSpace_home");
            String startDir = getValidStartDir(qiSpaceBaseDir, userid, MessageDispatcher.getInstance().getServerOSFileSeparator(), tomcatLoc);

            QiFileChooserDescriptor desc = new QiFileChooserDescriptor();
            desc.setParentGUI(this);
            desc.setTitle("Create a qiProject");
            desc.setHomeDirectory(startDir);
            desc.setDirectoryRememberedEnabled(false);
            desc.setFileFilter(filter);
            desc.setNavigationUpwardEnabled(false);
            desc.setMultiSelectionEnabled(false);
            desc.setetProducerComponentDescriptor(agent.getComponentDescriptor());
            desc.setDialogType(javax.swing.JFileChooser.OPEN_DIALOG);
            desc.setMessageCommand("");
            desc.setServerUrl(selectedServer);
            desc.setDirectoryOnlyEnabled(true);
            desc.setCreateDirectory(true);
            agent.callFileChooser(desc);
        }
    }//GEN-LAST:event_createProjectButtonActionPerformed

    private String getValidStartDir(String qiSpaceBaseDir, String userid, String serverOSFileSeparator, String tomcatLoc) {
        String startDir;
        if (qiSpaceBaseDir != null && !"".equals(qiSpaceBaseDir)) {
            startDir = qiSpaceBaseDir + serverOSFileSeparator + userid;

            if (agent.dirExists(tomcatLoc, startDir)) {
                return startDir;
            }

            startDir = qiSpaceBaseDir;
            if (agent.dirExists(tomcatLoc, startDir)) {
                return startDir;
            }

            return "/"; //always a valid *nix root dir
        } else { //server is misconfigured, no qiSpaceBaseDir is defined
            return "/"; //always a valid *nix root dir
        }
    }

    private void neverAskAgainCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_neverAskAgainCheckBoxActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_neverAskAgainCheckBoxActionPerformed

    private void addServerUrlButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addServerUrlButtonActionPerformed
        ServerUrlSelectorDialog susDialog = new ServerUrlSelectorDialog(agent, this, true);
        javax.swing.JDialog.setDefaultLookAndFeelDecorated(true);
        susDialog.setLocation(220, 120);
        susDialog.setSize(580, 160);
        susDialog.setDefaultCloseOperation(javax.swing.JDialog.DISPOSE_ON_CLOSE);
        susDialog.setVisible(true);
    }//GEN-LAST:event_addServerUrlButtonActionPerformed

    private void localServerRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_localServerRadioButtonActionPerformed
        okButton.setEnabled(selectedProjectExists());
    }//GEN-LAST:event_localServerRadioButtonActionPerformed

    private void remoteServerRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remoteServerRadioButtonActionPerformed
        okButton.setEnabled(selectedProjectExists());
    }//GEN-LAST:event_remoteServerRadioButtonActionPerformed

    private void rememberDefaultServerCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rememberDefaultServerCheckBoxActionPerformed
        if (rememberDefaultServerCheckBox.isSelected()) {
            String selectedProject = getSelectedProject();
            rememberDefaultProjectCheckBox.setEnabled(isProjectSelected());
            rememberDefaultProjectCheckBox.setSelected(false);
        } else {
            rememberDefaultProjectCheckBox.setEnabled(false);
            rememberDefaultProjectCheckBox.setSelected(false);
            neverAskAgainCheckBox.setEnabled(false);
            neverAskAgainCheckBox.setSelected(false);
        }
        rememberDefaultSessionCheckBox.setEnabled(false);
        rememberDefaultSessionCheckBox.setSelected(false);
    }//GEN-LAST:event_rememberDefaultServerCheckBoxActionPerformed

    private void rememberDefaultProjectCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rememberDefaultProjectCheckBoxActionPerformed
        if (rememberDefaultProjectCheckBox.isSelected()) {
            String selectedSession = getSelectedSession();
            rememberDefaultSessionCheckBox.setEnabled(isSessionSelected());
            rememberDefaultSessionCheckBox.setSelected(false);
            neverAskAgainCheckBox.setEnabled(true);
            neverAskAgainCheckBox.setSelected(false);
        } else {
            rememberDefaultSessionCheckBox.setEnabled(false);
            rememberDefaultSessionCheckBox.setSelected(false);
            neverAskAgainCheckBox.setEnabled(false);
            neverAskAgainCheckBox.setSelected(false);
        }
    }//GEN-LAST:event_rememberDefaultProjectCheckBoxActionPerformed

    private void rememberDefaultSessionCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rememberDefaultSessionCheckBoxActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_rememberDefaultSessionCheckBoxActionPerformed

    private void helpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_helpButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_helpButtonActionPerformed

    private void browseProjectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseProjectButtonActionPerformed
        //Specify which of the two file choosers is being invoked.
        selectProject = true;
        createProject = false;

        String[] extensions = new String[]{"*", "*.*"};
        FileFilter filter = (FileFilter) new GenericFileFilter(extensions, "Directory (*)",true);

        String selectedServer = getSelectedServer();
        String tomcatLoc = localServerRadioButton.isSelected() ? QIWConstants.LOCAL_PREF : QIWConstants.REMOTE_PREF;
        MessageDispatcher.getInstance().setTomcatURL(selectedServer);
        //NOTE: Server location preference is needed by the file chooser which it gets
        //from the Message Dispatcher. The OS info of the server is also needed if it
        //is remote.

        //TODO these methods should probably be combined in a synchronized block
        //otherwise an unroutable message may be dispatched while the dispatcher has a new 'tomcatLoc'
        //but an old runtimeServletDispDesc
        MessageDispatcher.getInstance().setLocationPref(tomcatLoc);
        if (MessageDispatcher.getInstance().resetRuntimeServer(selectedServer) == false)
            JOptionPane.showMessageDialog(this, "Failed to set runtime Tomcat URL to: " + selectedServer + ".", "Error Registering Runtime Tomcat URL",  JOptionPane.ERROR_MESSAGE);
        else {
            ArrayList<String> remoteOSInfoList = agent.getRemoteOSInfo(tomcatLoc);
            MessageDispatcher.getInstance().setRemoteOSInfoList(remoteOSInfoList);

            //Start in either the qiSpace home directory or the user's home dir
            String userid = CommonUtil.getUserID();
            String qiSpaceBaseDir = qiWbConfiguration.getInstance().getBaseDirsAttributes().get("qiSpace_home");
            String startDir = getValidStartDir(qiSpaceBaseDir, userid, MessageDispatcher.getInstance().getServerOSFileSeparator(), tomcatLoc);
            
            agent.browseForQispace(this, "Select a qiProject", startDir, filter, QIWConstants.FILE_CHOOSER_TYPE_OPEN, "", selectedServer);
        }
    }//GEN-LAST:event_browseProjectButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.setVisible(false);
        dispose();
        System.exit(0);
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void browseSessionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseSessionButtonActionPerformed
        //Specify which of the two file choosers is being invoked.
        selectProject = false;
        createProject = false;

        String[] extensions = new String[]{".cfg", ".CFG"};
        FileFilter filter = (FileFilter) new GenericFileFilter(extensions, "Saved Session File (*.cfg)", false);
        String selectedServer = getSelectedServer();
        String tomcatLoc = localServerRadioButton.isSelected() ? QIWConstants.LOCAL_PREF : QIWConstants.REMOTE_PREF;
        MessageDispatcher.getInstance().setTomcatURL(selectedServer);
        //NOTE: Server location preference is needed by the file chooser which it gets
        //from the Message Dispatcher. The OS info of the server is also needed if it
        //is remote.

        //TODO these methods should probably be combined in a synchronized block
        //otherwise an unroutable message may be dispatched while the dispatcher has a new 'tomcatLoc'
        //but an old runtimeServletDispDesc
        //TODO refactor this duplicate code - the following 3 lines were previously added to browseProjectButtonActionPerformed
        //and also had to be added here to fix the same bug (invoking the runtime server would fail if the client had not
        //previously been launched on that system or MessageDispatcher.getInstance().resetRuntimeServer() was called
        //to force the ServletDispatcher to accept an initial CID)
        MessageDispatcher.getInstance().setLocationPref(tomcatLoc);
        if (MessageDispatcher.getInstance().resetRuntimeServer(selectedServer) == false)
            JOptionPane.showMessageDialog(this, "Failed to set runime Tomcat URL to: " + selectedServer + ".", "Error Registering Runtime Tomcat URL",  JOptionPane.ERROR_MESSAGE);
        else {
            ArrayList<String> remoteOSInfoList = agent.getRemoteOSInfo(tomcatLoc);
            MessageDispatcher.getInstance().setRemoteOSInfoList(remoteOSInfoList);

            String startDir = getSelectedProject();
            agent.browseForSession(this, "Select a saved session", startDir, filter, QIWConstants.FILE_CHOOSER_TYPE_OPEN, "", selectedServer);
        }
    }//GEN-LAST:event_browseSessionButtonActionPerformed

    /** Called only if OK button enabled, i.e., a server and project selected/entered */
    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        boolean rememberProjectAsDefault = getProjectDefaultIndicator();

        String qispaceDir = getSelectedProject();
        String selectedServer = getSelectedServer();

        //Convey selected server and its location to the Message Dispatcher
        //NOTE: Must be done before calling agent's setQispace() below.
        String tomcatLoc = localServerRadioButton.isSelected() ? QIWConstants.LOCAL_PREF : QIWConstants.REMOTE_PREF;
        MessageDispatcher.getInstance().setTomcatURL(selectedServer);
        MessageDispatcher.getInstance().setLocationPref(tomcatLoc);

        //Add qiProject to preferences. Do before set default qiProject in prefs
        //Note:QibwPreferences.addQispace() will check if already on the list
        agent.addQispace(qispaceDir, qispaceKind, selectedServer);

        if (rememberProjectAsDefault){
            agent.setDefaultQispace(selectedServer, qispaceDir, getNeverAskAgainIndicator());
        }

        //Save qiProject in the Workbench Manager
        agent.setQispace(qispaceDir, qispaceKind, selectedServer);

        //Add the selected saved session to the preferences and set it as the
        //default if so specified.
        QiwbPreferences userPrefs = MessageDispatcher.getInstance().getUserPrefs();
        String selectedSession = getSelectedSession();
        if (isSessionSelected()) {
            int status = userPrefs.addDesktop(selectedServer, qispaceDir, selectedSession);
            int status2 = 0;
            if (getSessionDefaultIndicator()) {
                status2 = userPrefs.setDefaultDesktop(selectedServer, qispaceDir, selectedSession);
            }
            if (status > 0 || status2 > 0) {
                writePrefs(userPrefs);
            }
        }

        //Update the Remember As Default checkboxes. A selected server, project or session may be set
        //as the default in the preferences, but may no longer be so indicated.
        boolean prefsUpdated = false;
        if (selectedServer != null && selectedServer.equals(userPrefs.getDefaultServer()) && !getServerDefaultIndicator()) {
            prefsUpdated = true;
            userPrefs.setDefaultServer(QiwbPreferences.NO_DEFAULT);
        }
        if (qispaceDir != null && qispaceDir.equals(userPrefs.getDefaultQispace(selectedServer)) && !getProjectDefaultIndicator()) {
            prefsUpdated = true;
            userPrefs.setDefaultQispace(selectedServer, QiwbPreferences.NO_DEFAULT);
        }
        if (selectedSession != null && selectedSession.equals(userPrefs.getDefaultDesktop(selectedServer, qispaceDir)) && !getSessionDefaultIndicator()) {
            prefsUpdated = true;
            userPrefs.setDefaultDesktop(selectedServer, qispaceDir, QiwbPreferences.NO_DEFAULT);
        }

        if (prefsUpdated) writePrefs(userPrefs);

        //If a saved session is selected, open a qiWorkbench with the saved state
        if (isSessionSelected()) {
            //NOte: he "Restore Default State" command which works for any saved
            //      state file, not just the default saved desktop.
            IComponentDescriptor stMgrDesc = MessageDispatcher.getInstance().getComponentDescByName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
            String msgID = agent.getMessagingManager().sendRequest(QIWConstants.CMD_MSG,QIWConstants.RESTORE_DEFAULT_DESKTOP_CMD,stMgrDesc,QIWConstants.STRING_TYPE,selectedSession);
            this.setVisible(false);
            this.dispose();
        } else {
            launchWorkbench();
        }
    }//GEN-LAST:event_okButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton browseProjectButton;
    private javax.swing.JButton browseSessionButton;
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton createProjectButton;
    private javax.swing.JPanel dialogPanel;
    private javax.swing.JScrollPane dialogScrollPane;
    private javax.swing.JButton helpButton;
    private javax.swing.JRadioButton localServerRadioButton;
    private javax.swing.JCheckBox neverAskAgainCheckBox;
    private javax.swing.JButton addServerUrlButton;
    private javax.swing.JButton okButton;
    private javax.swing.JScrollPane projectListScrollPane;
    private javax.swing.JList projectsList;
    private javax.swing.JCheckBox rememberDefaultProjectCheckBox;
    private javax.swing.JCheckBox rememberDefaultServerCheckBox;
    private javax.swing.JCheckBox rememberDefaultSessionCheckBox;
    private javax.swing.JRadioButton remoteServerRadioButton;
    private javax.swing.JLabel selectIntro1Label;
    private javax.swing.JLabel selectIntro2Label;
    private javax.swing.JLabel selectIntro3Label;
//    private javax.swing.JLabel selectIntro4Label;
    private javax.swing.JLabel selectIntro5Label;
    private javax.swing.JLabel selectProjectLabel;
    private javax.swing.JPanel selectProjectPanel;
    private javax.swing.JLabel selectServerLabel;
    private javax.swing.JPanel selectServerPanel;
    private javax.swing.JLabel selectSessionLabel;
    private javax.swing.JPanel selectSessionPanel;
    private javax.swing.JScrollPane serverListScrollPane;
    private javax.swing.JList serversList;
    private javax.swing.JScrollPane sessionListScrollPane;
    private javax.swing.JList sessionsList;
    // End of variables declaration//GEN-END:variables
    private DefaultListModel projectsListModel;
    private DefaultListModel serversListModel;
    private DefaultListModel sessionsListModel;

    /**
     * Write out the updated user's preferences.
     * @userPrefs Modified user preferences.
     */
     private void writePrefs(QiwbPreferences userPrefs) {
        String home = MessageDispatcher.getInstance().getUserHOME();
        String prefFileDir = home + File.separator + QIWConstants.QIWB_WORKING_DIR;
        try {
            PreferenceUtils.writePrefs(userPrefs, prefFileDir);
        } catch (QiwIOException qioe) {
            JOptionPane.showMessageDialog(this, "Unable to save updated preferences.",
                "IO Error", JOptionPane.ERROR_MESSAGE);
        }
     }

    /**
     * Get info about the selected runtime Tomcat server.
     *
     * @return List containing 1) the URL of the selected Tomcat server and
     * 2) the location (local or remote) of the server.
     */
    public ArrayList<String> getTomcatInfo() {
        ArrayList<String> tomcatInfo = new ArrayList<String>(2);

        tomcatInfo.add(getSelectedServer());
        tomcatInfo.add(localServerRadioButton.isSelected() ? QIWConstants.LOCAL_PREF : QIWConstants.REMOTE_PREF);

        return tomcatInfo;
    }

    private String getSelectedServer() {
        return (serversListModel.size() > 0) ? (String)serversList.getSelectedValue() : "";
    }

    private String getSelectedProject() {
        return (projectsListModel != null && projectsListModel.size() > 0) ? (String)projectsList.getSelectedValue() : "";
    }

    private String getSelectedSession() {
        return (sessionsListModel.size() > 0) ? (String)sessionsList.getSelectedValue() : "";
    }

    private boolean isServerSelected() {
        String selectedServer = getSelectedServer();
        return selectedServer != null && !selectedServer.equals("");
    }

    private boolean isProjectSelected() {
        String selectedProject = getSelectedProject();
        return selectedProject != null && !selectedProject.equals("");
    }

    private boolean isSessionSelected() {
        String selectedSession = getSelectedSession();
        return selectedSession != null && !selectedSession.equals("");
    }

    public boolean getServerDefaultIndicator() {
        return rememberDefaultServerCheckBox.isSelected() ? true : false;
    }

    public boolean getProjectDefaultIndicator() {
        return rememberDefaultProjectCheckBox.isSelected() ? true : false;
    }

    public boolean getSessionDefaultIndicator() {
        return rememberDefaultSessionCheckBox.isSelected() ? true : false;
    }

    public boolean getNeverAskAgainIndicator() {
        return neverAskAgainCheckBox.isSelected() ? true : false;
    }

    private void selectServer(String selectedServer) {
        //Populate the projects list with those associated with the selected server
        QiwbPreferences userPrefs = MessageDispatcher.getInstance().getUserPrefs();
        ArrayList<QiSpaceDescriptor> projectList = userPrefs.getServerQispaces(selectedServer);
        projectsListModel = new DefaultListModel();
        for (int i=0; projectList != null && i < projectList.size(); i++)
            projectsListModel.addElement(projectList.get(i).getPath());
        projectsList.setModel(projectsListModel);

        browseProjectButton.setEnabled(true);
//        createProjectButton.setEnabled(remoteServerRadioButton.isSelected() ? false : true);

        //If the selected server is a default, then check if it has a default
        //project, select it in the list
        boolean selectedDefaultProject = false;
        String defaultProject = "";
        String defaultServer = userPrefs.getDefaultServer();
        boolean selectedDefaultServer = defaultServer.equals(selectedServer) ? true : false;
        if (selectedDefaultServer) {
            defaultProject = userPrefs.getDefaultQispace(selectedServer);
            if (!defaultProject.equals("")) {
                //Search list model for project and select
                for (int i=0; i<projectsListModel.size(); i++) {
                    String project = (String)projectsListModel.getElementAt(i);
                    if (project.equals(defaultProject)) {
                        projectsList.setSelectedIndex(i);
                        selectedDefaultProject = true;
                    }
                }
            }
        }

        browseSessionButton.setEnabled(selectedDefaultProject);
        okButton.setEnabled(selectedDefaultProject);

        rememberDefaultServerCheckBox.setEnabled(true);
        rememberDefaultServerCheckBox.setSelected(selectedDefaultServer);

        rememberDefaultProjectCheckBox.setEnabled(selectedDefaultServer && isProjectSelected());
        rememberDefaultProjectCheckBox.setSelected(selectedDefaultProject);

        //If a project is selected as the default, then populate the sessions
        //list with those associated with the selected project. If one of the
        //sessions is the default, select it.
        boolean selectedDefaultSession = false;
        if (selectedDefaultProject) {
            //Populate the sessions list with those associated with the selected project
            ArrayList<String> sessionList = userPrefs.getStateFiles(selectedServer, defaultProject);
            sessionsListModel = new DefaultListModel();
            for (int i=0; sessionList != null && i < sessionList.size(); i++)
                sessionsListModel.addElement(sessionList.get(i));
            sessionsList.setModel(sessionsListModel);

            String defaultSession = userPrefs.getDefaultDesktop(selectedServer, defaultProject);
            if (!defaultSession.equals("")) {
                //Search list model for default session and select
                for (int i=0; i<sessionsListModel.size(); i++) {
                    String session = (String)sessionsListModel.getElementAt(i);
                    if (session.equals(defaultSession)) {
                        sessionsList.setSelectedIndex(i);
                        selectedDefaultSession = true;
                    }
                }
            }
        } else {
            //Clear the sessions list
            sessionsListModel = new DefaultListModel();
            sessionsList.setModel(sessionsListModel);
        }

        rememberDefaultSessionCheckBox.setEnabled(selectedDefaultProject && isSessionSelected());
        rememberDefaultSessionCheckBox.setSelected(selectedDefaultSession);

        neverAskAgainCheckBox.setEnabled(selectedDefaultProject);
        neverAskAgainCheckBox.setSelected(false);
    }

    private void launchWorkbench() {
        final Dialog comp = this;
        Runnable heavyRunnable = new Runnable() {
            public void run(){
                String title = "Launching qiWorkbench";
                String text = "Please wait while system is launching the qiWorkbench tool....";
                Icon icon = IconResource.getInstance().getImageIcon(IconResource.LOGO_ICON_QI);
                com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(comp,100,true,500,title,text,icon);
                monitor.start("");
                WorkbenchGUI gui = new WorkbenchGUI(agent);
                if (monitor.isCancel()) {
                    if (gui != null && gui.isVisible()) {
                        gui.setVisible(false);
                    }
                    return;
                    //monitor.setCurrent(null,monitor.getTotal());
                }
                agent.setWorkbenchGUI(gui);
                WorkbenchData.gui = gui;
                gui.setVisible(true);
                if (gui.isVisible()) {
                    monitor.setCurrent(null,monitor.getTotal());
                    comp.setVisible(false);
                    comp.dispose();
                }
            }
        };
        new Thread(heavyRunnable).start();
    }
    
    /**
     * Check if selected server is accessible. If not, warn the user and tell them
     * to pick one that is accessible.
     * @param selectedServer The selected runtime Tomcat server.
     * @return true if selected server accessible; otherwise, false.
     */
    private boolean isServerAccessible(String selectedServer) {
        IQiWorkbenchMsg request = null;
        IQiWorkbenchMsg response = null;

        // check if selected server is accessible
        request = new QiWorkbenchMsg(agent.getCID(), MessageDispatcher.getInstance().getRuntimeServletDispDesc().getCID(), QIWConstants.CMD_MSG,
                         QIWConstants.PING_SERVER_CMD, MsgUtils.genMsgID(),
                         QIWConstants.STRING_TYPE, selectedServer);
        response = DispatcherConnector.getInstance().sendRequestMsg(request);

        if (!response.isAbnormalStatus()) {
            logger.config("isServerAccessible: Established contact with " + selectedServer);
            return true;
        } else {
            logger.warning("isServerAccessible: Failed to connect to default" + selectedServer);

            JOptionPane.showMessageDialog(this, selectedServer +" inaccessible. Select another.", "Selected Server Access Error", JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }
    
    /**
     * Check if there is a selected project and if so, if it exists. If it 
     * doesn't exists, it cannot be remembered as the default project.
     * @return true if there is a selected project and it exists; otherwise,
     * false
     */
     private boolean selectedProjectExists() {
         boolean projectExists = true;
         
         //If there is a selected project and it does does not exist, it cannot 
         // be remembered as the default project
         if (isProjectSelected() && !projectExists(getSelectedProject())) {
             rememberDefaultProjectCheckBox.setSelected(false);
             projectExists = false;
         }
        
         return projectExists;
     }
    
    /**
     * Check if selected project exists.
     * @param selectedProject Project selected from list of available projects
     * @return true if selected project exists; otherwise, false
     */
     private boolean projectExists(String selectedProject) {
        //check if qiProject already exists as a file or directory
        boolean isLocalServer = localServerRadioButton.isSelected() ? true : false;
        boolean projectExists = true;
        
        File dir = new File(selectedProject);
        if (isLocalServer) {
            if (!dir.exists()) {
                projectExists = false;
                JOptionPane.showMessageDialog(this, selectedProject+"\ndoes not exist. Select another.",
                        "qiProject Selection Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            String msgId = agent.getMessagingManager().sendRequest(QIWConstants.CMD_MSG, QIWConstants.IS_REMOTE_DIRECTORY_CMD, QIWConstants.STRING_TYPE, selectedProject, true);
            IQiWorkbenchMsg response = agent.getMessagingManager().getMatchingResponseWait(msgId,5000);

            if (response != null && !response.isAbnormalStatus()) {
                String isDir = (String)response.getContent();
                if (!isDir.equals("yes")) {
                    projectExists = false;
                    JOptionPane.showMessageDialog(this, "<html>"+selectedProject+"<br>does not exist. Select another.</html>",
                        "qiProject Selection Error", JOptionPane.ERROR_MESSAGE);
                }
            } else if (response != null) {
                projectExists = false;
                JOptionPane.showMessageDialog(this, (String)response.getContent(),
                    "qiProject Selection Error", JOptionPane.ERROR_MESSAGE);
            } else {
                projectExists = false;
                JOptionPane.showMessageDialog(this, "Unable to get response to IS_REMOTE_DIR_CMD after 5 seconds - check server or try again.",
                    "qiProject Selection Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if (!projectExists) okButton.setEnabled(false);
        
        return projectExists;
    }
    
    /**
     * Make dialog visible and check if the selected project, if any, exists
     */
     public void setVisible() {
         Runnable existRunnable = new Runnable() {
             public void run() {
                 if (isServerSelected()) selectedProjectExists();
             }
         };
         new Thread(existRunnable).start();

         this.setVisible(true);
     }
}
