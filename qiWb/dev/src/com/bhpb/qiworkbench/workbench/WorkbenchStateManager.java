/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.workbench;

import com.bhpb.qiworkbench.ComponentDescriptor;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.io.File;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.bhpb.qiworkbench.IqiMessageHandler;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiSpaceDescriptor;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.QiwbPreferences;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.ClientConstants;
import com.bhpb.qiworkbench.client.SocketManager;
import com.bhpb.qiworkbench.client.StatsManager;
import com.bhpb.qiworkbench.client.util.ComponentUtils;
import com.bhpb.qiworkbench.client.util.ErrorDialogUtils;
import com.bhpb.qiworkbench.client.util.GenericFileFilter;
import com.bhpb.qiworkbench.compAPI.IconResource;
import com.bhpb.qiworkbench.client.util.PreferenceUtils;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.ProgressMonitor;
import com.bhpb.qiworkbench.compAPI.ProgressUtil;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiSpaceDescUtils;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;

/**
 * All state serialization request messages are sent to the workbench state manager for processing.
 * <p>
 * A singleton since there is only one and so there is only one message handler
 * the other components can communicate with.
 *
 * @author Woody Folsom woody.folsom@bhpbilliton.net
 * @version 1.0
 */

public class WorkbenchStateManager extends QiComponentBase implements IqiWorkbenchComponent, Runnable{
    private static Logger logger = Logger.getLogger(WorkbenchStateManager.class.getName());
    private MessagingManager messagingMgr;
    private static WorkbenchStateManager singleton = null;
    /** CID for component instance. Generated before the thread is started and carried as the thread's name. */
    private String myCID = "";
    private String projectRoot = "";
    private WorkbenchGUI workbenchGUI = null;
    private IComponentDescriptor wbMgrDesc = null;
    private boolean wait = true;
    private String currentDirectory = "";
    private List<IComponentDescriptor> saveList = null;
    private Element element = null;
    private Element root = null;
    private Map<String,DecoratedNode> currentNodeMap = new HashMap<String,DecoratedNode>();
    private String restoredFilePath;
    private boolean notAskAgainForSaveDesktop = false;
    private boolean saveAsRequested = false;
//  This delegate exists to add debugging code to a proxy of this class
    private IqiMessageHandler processorDelegate;
    private WorkbenchStateManager(){ }

    /**
     * Add or update the state data structure which is keyed by component's preferred display name.
     * The value is DecoratedNode which contains XML element node and component descriptor
     * @return
     */
    public void setNodeToCurrentNodeMap(String name, DecoratedNode node){
        //if(!currentNodeMap.containsKey(name))
        currentNodeMap.put(name,node);
        //return;
    }

    public void setSaveAsRequested(boolean flag){
        saveAsRequested = flag;
    }

    public boolean componentOpen(IComponentDescriptor cd){
        if(cd == null)
            return false;
        if(!currentNodeMap.containsKey(cd.getPreferredDisplayName()))
            return false;
        for(String s : currentNodeMap.keySet()){
            if(s.equals(cd.getPreferredDisplayName())){
                if(currentNodeMap.get(s).getComponentClosed() == true)
                    return false;
                else
                    return true;
            }
        }
        return false;
    }

    /**
     * Set the component's visibility status to "Closed" within the data structure. This is generally
     * applied to the existing component where the user request to quit.
     * @return
     */
    public void setComponentVisibleStateClosed(String key, Component comp){
        if(currentNodeMap.containsKey(key)){
            currentNodeMap.get(key).setComponentClosed(true);
            if(comp != null){
                currentNodeMap.get(key).setLocation(comp.getLocation());
                currentNodeMap.get(key).setDimension(comp.getSize());
            }
        }
        return;
    }

    public String getComponentVisibleState(String key){
        if(currentNodeMap.containsKey(key)){
            if(currentNodeMap.get(key).getComponentClosed())
                return "closed";
            else
                return "open";
        }
        return "unknown";
    }

    private void setComponentNode(String key, Node node){
        if(currentNodeMap.containsKey(key))
            currentNodeMap.get(key).setNode(node);
        return;
    }

    /**
     * Set the component's visibility status to "Open" within the data structure. This is generally
     * applied to the existing component where the user request to open a component which was quit.
     * @return
     */
    public void setComponentVisibleStateOpen(String key){
        if(currentNodeMap.containsKey(key))
            currentNodeMap.get(key).setComponentClosed(false);
        return;
    }

    /**
     * Remove the key and its value from the data structure.
     * @return
     */
    public void removeNodeFromCurrentNodeMap(String key){
        if(currentNodeMap.containsKey(key))
            currentNodeMap.remove(key);
        logger.info("currentNodeMap " + currentNodeMap);
        return;
    }

    public void setWorkbenchGUI(WorkbenchGUI workbenchGUI){
        this.workbenchGUI = workbenchGUI;
    }

    /**
     * Reset the key in the data structure. This is needed when the rename command for a
     * component is requested because the preferredDisplayName of a component is a primary key
     * for the data structure which contains component status information
     * @return
     */
    public void resetKeyFromCurrentNodeMap(String oldkey, String newkey){
        logger.info("oldkey= " + oldkey + " newkey= " + newkey);
        if(currentNodeMap.containsKey(oldkey)  && !currentNodeMap.containsKey(newkey)){
            DecoratedNode node = currentNodeMap.get(oldkey);
            node.setOldDisplayName(oldkey);

            IComponentDescriptor cd = node.getDescriptor();
            cd.setPreferredDisplayName(newkey);
            node.setDescriptor(cd);
            currentNodeMap.remove(oldkey);
            currentNodeMap.put(newkey,node);
        }
        return;
    }

    /**
     * Get the state manager internal data structure
     * @return
     */
    public Map<String,DecoratedNode> getCurrentNodeMap(){
        return currentNodeMap;
    }

    /**
     * Generate state information of the Workbench level currently only storing what existing
     * components are closed
     * @return true or false
     */
    private String getWorkbenchStates(){
        if(currentNodeMap.isEmpty())
            return "";
        logger.info("currentNodeMap " + currentNodeMap);
        List<String> openList = new ArrayList<String>();
        List<String> closeList = new ArrayList<String>();
        for(String key : currentNodeMap.keySet()){
            if(!currentNodeMap.get(key).isNew()){
                if(currentNodeMap.get(key).getComponentClosed()  == true)
                    closeList.add(key);
                else
                    openList.add(key);
            }
        }
        //if(closeList.size() == 0)
        //  return null;
        String sOpened = "";
        String sClosed = "";

        for(int i = 0; i < closeList.size(); i++){
            sClosed += closeList.get(i);
            if(i < closeList.size() - 1)
                sClosed += "~";
        }
        for(int i = 0; i < openList.size(); i++){
            sOpened += openList.get(i);
            if(i < openList.size() - 1)
                sOpened += "~";
        }
        StringBuffer content = new StringBuffer();
        //content.append("<workbench componentOpen=\"" + sOpened + "\" componentClosed=\"" + sClosed + "\">\n");
        content.append("<workbench componentClosed=\"" + sClosed + "\">\n");
        content.append("</workbench>\n");
        return content.toString();
    }

    private Node renameNode(IComponentDescriptor desc, String newName){
        Node node = getNodeByIComponentDescriptor(desc);
        if(node != null){
            ((Element)node).setAttribute("preferredDisplayName",newName);
            return root;
        }
        return null;
    }

    /**
     * Check to see a gived component is existing in the saved set by matching preferredDisplayName
     * @return true or false
     */
    public boolean componentExitsInSavedSet(IComponentDescriptor cd) {
        if(root == null){
            return false;
        }
        NodeList children = root.getChildNodes();
        for(int i = 0; i < children.getLength(); i++){
            Node child = children.item(i);
            if(child.getNodeType() == Node.ELEMENT_NODE){
                String nodeName = child.getNodeName();
                if(nodeName.equals("component")){
                    if(((Element)child).getAttribute("preferredDisplayName").equals(cd.getPreferredDisplayName())){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Get the singleton instance of this class. If the message dispatcher doesn't exist,
     * create it and start as a thread.
     */
    public static WorkbenchStateManager getInstance() {
        if (singleton == null) {
            singleton = new WorkbenchStateManager();
            new Thread(singleton).start();
        }
        return singleton;
    }

    /**
     * Initialize the component:
     * <ul>
     * <li>Create its messaging manager</li>
     * <li>Register with the Message Dispatcher</li>
     * </ul>
     */
    public void init() {
        try {
            QIWConstants.SYNC_LOCK.lock();
            messagingMgr = new MessagingManager();
            myCID = ComponentUtils.genCID(this);
            messagingMgr.registerComponent(QIWConstants.WORKBENCH_COMP, QIWConstants.WORKBENCH_STATE_MANAGER_NAME, myCID,true);

            QIWConstants.SYNC_LOCK.unlock();
            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in WorkbenchStateManager.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

    public void run(){
        //initialize the state manager
        init();
        while(true) {
            IQiWorkbenchMsg msg = messagingMgr.peekNextMsg();
            //if a response message with skip flag set to true, it will not be processed here
            //instead it will be claimed by calling messingMgr.getMatchingResponseWait
            //if(msg != null && !msg.skip()){
            //    msg = messagingMgr.getNextMsgWait();
            //    processMsg(msg);
            //}
            if (msg != null) {
                // If it is a response with a timed-out request, dequeue it
                if (msg.getMsgKind().equals(QIWConstants.DATA_MSG) && msg.skip()) {
                    if (messagingMgr.getMatchingOutstandingRequest(msg) == null) {
                        msg = messagingMgr.getNextMsg();
                        logger.warning("Consuming response message which lacks a matching outstanding request. Response msg: " + msg);
                    }
                } else { // it is a request or asynchronous response, process it
	                msg = messagingMgr.getNextMsgWait();
	                if (processorDelegate != null) {
	                    logger.info("Processor delegate non-null, delegating processing through IqiMessageHandler interface " + processorDelegate);
	                    processorDelegate.processMsg(msg);
	                }
	                else {
	                    logger.info("Processor delegate is null, processing my own message");
	                    try{
	                    	processMsg(msg);
	                    }catch(Exception e){
	                    	logger.info(e.toString());
	                    	e.printStackTrace();
	                    }
	                }
                }
            }
        }
    }

    /**
     * Process the request/response message
     *
     * @param msg The message to be processed.
     */
    public void processMsg(IQiWorkbenchMsg msg) {
        /** Request that matches the response */
        IQiWorkbenchMsg request = null, response = null;

        //log message traffic
        logger.fine("WorkbenchStateManager::procssMsg: msg="+msg.toString());

        //get component descriptor of WorkbenchManager
        if(wbMgrDesc == null)
            wbMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_MGR_NAME);

        //Start the file chooser in the qiSpace. There is no notion of a project
        //at the workbench level. Each component may be associated with a different
        //project. Therefore, the user must select where they want to house a saveset file.
        //Note: The qiSpace may be a project.
        QiSpaceDescriptor qiSpaceDesc = messagingMgr.getQispaceDesc();
        String qiSpacePath = QiSpaceDescUtils.getQiSpacePath(qiSpaceDesc);
        currentDirectory = qiSpacePath;

        String msgId = "";
        if(workbenchGUI == null){
            //send a message to workbench manager to get the workbench GUI object will be needed by FileChooserDialog
            msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_WORKBENCH_GUI_CMD,wbMgrDesc);
            ////response = messagingMgr.getNextMsgWait();
            response = messagingMgr.getMatchingResponseWait(msgId,1000);
            if (response == null || response.isAbnormalStatus()) {
                logger.info("Error is getting workbench GUI: " + response.getContent());
                JOptionPane.showMessageDialog(workbenchGUI, "Error in getting workbench GUI.", "QI Workbench",
                        JOptionPane.WARNING_MESSAGE);
                return;
            }
            workbenchGUI = (WorkbenchGUI)response.getContent();
        }

        if (messagingMgr.isResponseMsg(msg)) {
            request = messagingMgr.checkForMatchingRequest(msg);
            String cmd = request.getCommand();
            // check if from the message dispatcher
            if (messagingMgr.isResponseFromMsgDispatcher(msg)) {
            } else

            if (msg.getCommand().equals(QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD)){
                ArrayList list = (ArrayList)msg.getContent();
                if(((Integer)list.get(0)).intValue() == JFileChooser.APPROVE_OPTION){
                    QiwbPreferences userPrefs = MessageDispatcher.getInstance().getUserPrefs();
                    String selectedServer = MessageDispatcher.getInstance().getTomcatURL();
                    String selectedProject = messagingMgr.getProject();
                    int status1 = userPrefs.addDesktop(selectedServer,selectedProject,(String)list.get(1));
                    int status2 = -1;
                    if(list.size() == 5){
                        boolean defaultDesktop = ((Boolean)list.get(4)).booleanValue();
                        if(defaultDesktop){
                            status2 = userPrefs.setDefaultDesktop(selectedServer,selectedProject,(String)list.get(1));
                        }
                    }
                    if(status1 > 0 || status2 > 0){
                        String home = MessageDispatcher.getInstance().getUserHOME();
                        String prefFileDir = home + File.separator + QIWConstants.QIWB_WORKING_DIR;
                        try {
                            PreferenceUtils.writePrefs(userPrefs, prefFileDir);
                        } catch (QiwIOException qioe) {
                            //TODO notify user cannot write preference file
                            logger.warning(qioe.getMessage());
                        }
                    }
                    //restoredFilePath = (String)list.get(1);
                    String selectedFilePath = (String)list.get(1);
                    if (((String)list.get(2)).equals(QIWConstants.FILE_CHOOSER_TYPE_OPEN)){
                        //Node rootTemp = generateXMLRoot(restoredFilePath);
                        Node rootTemp = generateXMLRoot(selectedFilePath);
                        if(rootTemp == null){
                            messagingMgr.resetMsgQueue();
                            return;
                        }

                        if(!rootTemp.getNodeName().equals("desktop")){
                            JOptionPane.showMessageDialog(workbenchGUI,"The selected file " + selectedFilePath +
                                    " does not have a compatible format required by the qiWorkbench tool.",
                                    "Incompatible File Format",JOptionPane.WARNING_MESSAGE);
                            return;
                        }

                        root = (Element)rootTemp;
                        //String returnedPath ="";
                        Map<String,DecoratedNode> tempMap = new HashMap<String,DecoratedNode>();
                        if(restoreDesktop(selectedFilePath,tempMap)){
                            restoredFilePath = selectedFilePath;
                            currentNodeMap = tempMap;
                        }
                        //restore(root,restoredFilePath);
                    } else

                    if(((String)list.get(2)).equals(QIWConstants.FILE_CHOOSER_TYPE_SAVE)){
                        String command = (String)list.get(3);
                        boolean okToRestore = false;
                        if(command.equals(QIWConstants.SAVE_DESKTOP_CMD) || command.equals(QIWConstants.RESTORE_DESKTOP_CMD) || command.equals(QIWConstants.SAVE_DESKTOP_THEN_QUIT_CMD)){
                            if(!doSaveDesktop1(selectedFilePath,command))
                                return;
                            //restoredFilePath = selectedFilePath;
                            //setWorkbenchTitle(selectedFilePath);
                            /*if(command.equals(QIWConstants.SAVE_DESKTOP_CMD) || command.equals(QIWConstants.SAVE_DESKTOP_THEN_QUIT_CMD)){
                                String message = "The desktop has been successfully saved to " + selectedFilePath;
                                JOptionPane.showMessageDialog(workbenchGUI, message, "QI Workbench",
                                        JOptionPane.INFORMATION_MESSAGE);
                            }
                            */
                            if(command.equals(QIWConstants.SAVE_DESKTOP_THEN_QUIT_CMD)) {
                                //Close all sockets available for reuse
                                SocketManager.getInstance().closeAllSockets();
                                System.exit(0);
                            }
                            if(command.equals(QIWConstants.RESTORE_DESKTOP_CMD)){
                                if(deactivateComponents())
                                	okToRestore = true;
                            }
                        }else{
                            IComponentDescriptor compDesc = saveList.get(0);
                            //check to see if this component is an unsaved new component
                            boolean isExisting1 = componentExitsInSavedSet(compDesc);
                            if(command.equals(QIWConstants.SAVE_COMP_AS_CLONE_CMD)){
                                if(!doSaveComponentAsClone(selectedFilePath,compDesc)){
                                    logger.info("SaveAs ACTION FOR " + compDesc.getPreferredDisplayName() + " NOT SUCCESSFUL!!!!");
                                    String message = "Component " + compDesc.getPreferredDisplayName() + " was not successfully saved as a clone. Please check the log or seek assistance from workbench support.";
                                    workbenchGUI.setStatusMessage(message);
                                    JOptionPane.showMessageDialog(workbenchGUI,message,
                                            "Save As Failed",JOptionPane.WARNING_MESSAGE);
                                    return;
                                }else{
                                    logger.info("SaveAs action for " + compDesc.getPreferredDisplayName() + " successful.");
                                    workbenchGUI.setStatusMessage("Component " + compDesc.getPreferredDisplayName() + " was successfully saved as a clone.");
                                }
                            }else if(!saveComponentList(saveList,(String)list.get(1),command)){
                                logger.info("ACTION " + command + " NOT SUCCESSFUL!!!!");
                                String message = "Component " + compDesc.getPreferredDisplayName() + " was not successfully saved. Please check the log or seek assistance from workbench support.";
                                workbenchGUI.setStatusMessage(message);
                                JOptionPane.showMessageDialog(workbenchGUI,message,
                                        "Save Failed",JOptionPane.WARNING_MESSAGE);
                                return;
                            }
                            //check to see if this component was successfully saved
                            boolean isExisting2 = componentExitsInSavedSet(compDesc);
                            if(!isExisting1 && isExisting2){ //newly saved component
                                setWorkbenchTitle(selectedFilePath);
                                if(command.equals(QIWConstants.SAVE_COMP_CMD)){
                                    WorkbenchAction.updateUIAfterSave(compDesc);
                                }else if(command.equals(QIWConstants.SAVE_COMP_THEN_QUIT_CMD))
                                    WorkbenchAction.closeComponentGUI(compDesc);
                                Node node = getNodeByIComponentDescriptor(compDesc);
                                DecoratedNode deNode = new DecoratedNode(node,compDesc);
                                setNodeToCurrentNodeMap(compDesc.getPreferredDisplayName(),deNode);
                                logger.info("currentNodeMap " + currentNodeMap);
                            }
                        }
                        root = generateXMLRoot(restoredFilePath);
                        if(command.equals(QIWConstants.RESTORE_DESKTOP_CMD)){
                            if(okToRestore){
                                callFileChooser(workbenchGUI,QIWConstants.FILE_CHOOSER_TYPE_OPEN,currentDirectory,QIWConstants.FILE_CHOOSER_TYPE_OPEN,command);
                                return;
                            }
                        }
                    }

                }else{
                    logger.info("File Chooser canceled by the user");

                }
                return;
            }
        } else

            //Check if a request. If so, process and send back a response
            if (messagingMgr.isRequestMsg(msg)) {
                String cmd = msg.getCommand();
                if(cmd.equals(QIWConstants.SAVE_DESKTOP_CMD)){
                    List<IComponentDescriptor> list = getOpenedComponentList();

                    saveList = list;
                    String title = "Save Desktop";

                    if(root != null && !saveAsRequested){
                        if(notAskAgainForSaveDesktop == true)
                            doSaveDesktop1(restoredFilePath,cmd);
                        else{
                            ConfirmSaveDialog confirm = new ConfirmSaveDialog(workbenchGUI, true);
                            confirm.setVisible(true);
                            int returnStatus = confirm.getReturnStatus();
                            notAskAgainForSaveDesktop = confirm.getNotAskAgain();
                            if(returnStatus == ConfirmSaveDialog.RET_YES){
                                doSaveDesktop1(restoredFilePath,cmd);
                            }else
                                callFileChooser(workbenchGUI,title,currentDirectory,QIWConstants.FILE_CHOOSER_TYPE_SAVE,msg.getCommand());
                        }
                    }else{
                        callFileChooser(workbenchGUI,title,currentDirectory,QIWConstants.FILE_CHOOSER_TYPE_SAVE,msg.getCommand());
                    }
                    return;
                } else
                    //Save the desktop state then quit the workbench tool when the action is completed
                if(cmd.equals(QIWConstants.SAVE_DESKTOP_THEN_QUIT_CMD)){
                    List<IComponentDescriptor> list = getOpenedComponentList();

                    saveList = list;
                    String title = "Save Desktop";

                    //log usage event: quitting workbench and all components
                    logQuitting();

                    if(root != null){
                        if(doSaveDesktop1(restoredFilePath,cmd)) {
                            //Close all sockets available for reuse
                            SocketManager.getInstance().closeAllSockets();
                            System.exit(0);
                        }
                    }else{
                        callFileChooser(workbenchGUI,title,currentDirectory,QIWConstants.FILE_CHOOSER_TYPE_SAVE,msg.getCommand());
                    }
                    return;
                } else
                //Rename a component
                if(cmd.equals(QIWConstants.SET_PREFERRED_DISPLAY_NAME_CMD)){
                    ArrayList list = (ArrayList)msg.getContent();
                    IComponentDescriptor desc = (IComponentDescriptor)list.get(0);
                    IComponentDescriptor descClone = (IComponentDescriptor)desc.clone();
                    String newName = (String)list.get(1);
                    String id = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SET_PREFERRED_DISPLAY_NAME_CMD,desc,
                            QIWConstants.STRING_TYPE,newName);
                    logger.info("component descriptor to be renamed " + desc);
                    IQiWorkbenchMsg res = messagingMgr.getMatchingResponseWait(id);
                    if(res != null && !res.isAbnormalStatus()){
                        if(componentExitsInSavedSet(descClone)){
                            Node n = renameNode(descClone,newName);
                            if(n != null){
                                root = (Element)n;
                                nu.xom.Element rootElement = nu.xom.converters.DOMConverter.convert(root);
                                String desktopXML = rootElement.toXML();
                                if(save(restoredFilePath,desktopXML)){
                                    resetKeyFromCurrentNodeMap(descClone.getPreferredDisplayName(),newName);
                                    messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,"Preferred Display name has been suscessfully reset.");
                                }else {
                                    IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error in persisting rename");
                                    messagingMgr.sendResponse(resp,QIWConstants.STRING_TYPE,resp.getContent());
                                    JOptionPane.showMessageDialog(workbenchGUI,(String)resp.getContent());
                                }
                            }else{
                                IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error can find the component in the saved set");
                                messagingMgr.sendResponse(resp,QIWConstants.STRING_TYPE,resp.getContent());
                            }
                            // removeNodeFromCurrentNodeMap(descClone.getPreferredDisplayName());

                        }else{
                            resetKeyFromCurrentNodeMap(descClone.getPreferredDisplayName(),newName);
                            messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,"Preferred Display name has been suscessfully reset.");
                        }
                    } else {
                        IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error in renaming the component");
                        messagingMgr.sendResponse(resp,QIWConstants.STRING_TYPE,resp.getContent());
                    }
                    return;
                } else
                if(cmd.equals(QIWConstants.SAVE_WORKBENCH_STATES_CMD)) {
                    String workbench = getWorkbenchStates();
                    logger.info("getWorkbenchStates =" + workbench);
                    if(workbench != null && restoredFilePath != null){
                        root = generateXMLRoot(restoredFilePath);
                        nu.xom.Element rootElement = nu.xom.converters.DOMConverter.convert(root);
                        Element n = buildElement(workbench);
                        nu.xom.Element myElement = nu.xom.converters.DOMConverter.convert(n);
                        nu.xom.Elements children = rootElement.getChildElements();
                        boolean success = false;
                        for(int i = 0; i < children.size(); i++){
                            nu.xom.Element child = children.get(i);
                            String nodeName = child.getLocalName();
                            if(nodeName.equals("workbench")){
                                rootElement.removeChild(child);
                                rootElement.insertChild(myElement,i);
                                success = true;
                            }
                        }
                        if(success == false)
                            rootElement.appendChild(myElement);

                        String desktopXML = rootElement.toXML();
                        if(save(restoredFilePath,desktopXML)){
                            messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,desktopXML);
                        }else {
                            messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,"");
                        }
                    }else
                        messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,"");
                    return;
                } else
                if(cmd.equals(QIWConstants.SAVE_COMP_THEN_QUIT_CMD)){
                    IComponentDescriptor descForSave = (IComponentDescriptor)msg.getContent();
                    logger.info("component descriptor to be saved " + descForSave);
                    List<IComponentDescriptor> list = new ArrayList<IComponentDescriptor>();
                    list.add(descForSave);
                    saveList = list;
                    String title = "Save qiComponent";
                    if(getExistingComponentNodeCount() > 0 || restoredFilePath != null){
                        logger.info("currentNodeMap before saveComponent " + currentNodeMap);
                        if(doSaveComponent(descForSave)){
                            WorkbenchAction.closeComponentGUI(descForSave);
                            logger.info("currentNodeMap after saveComponent " + currentNodeMap);
                            String message = "Component " + descForSave.getPreferredDisplayName() + " was successfully saved into " + restoredFilePath;
                            //JOptionPane.showMessageDialog(workbenchGUI, message, title, JOptionPane.INFORMATION_MESSAGE);
                            workbenchGUI.setStatusMessage(message);
                        }else{
                            logger.info("SAVING " + descForSave.getPreferredDisplayName() + " NOT SUCCESSFUL!!!!");
                            String message = "Component " + descForSave.getPreferredDisplayName() + " was not successfully saved. Please check the log or seek assistance from workbench support.";
                            //JOptionPane.showMessageDialog(workbenchGUI, message, title, JOptionPane.WARNING_MESSAGE);
                            workbenchGUI.setStatusMessage(message);
                            return;
                        }
                    }else{
                        callFileChooser(workbenchGUI,title,currentDirectory,QIWConstants.FILE_CHOOSER_TYPE_SAVE,msg.getCommand());
                    }
                    return;
                } else
                if(cmd.equals(QIWConstants.SAVE_COMP_CMD)){
                    IComponentDescriptor descForSave = (IComponentDescriptor)msg.getContent();
                    logger.info("component descriptor to be saved " + descForSave);
                    List<IComponentDescriptor> list = new ArrayList<IComponentDescriptor>();
                    list.add(descForSave);
                    saveList = list;
                    String returnPath = "";
                    String title = "Save qiComponent";

                    if(getExistingComponentNodeCount() > 0 || restoredFilePath != null){
                        boolean isNew = false;
                        if(!componentExitsInSavedSet(descForSave))
                            isNew = true;
                        if(doSaveComponent(descForSave)){
                            if(isNew)
                                WorkbenchAction.updateUIAfterSave(descForSave);
                            String message = "Component " + descForSave.getPreferredDisplayName() + " was successfully saved into " + restoredFilePath;
                            JOptionPane.showMessageDialog(workbenchGUI, message, title, JOptionPane.INFORMATION_MESSAGE);
                            workbenchGUI.setStatusMessage(message);
                        }else{
                            logger.warning("SAVING " + descForSave.getPreferredDisplayName() + " NOT SUCCESSFUL!!!!");
                            String message = "Component " + descForSave.getPreferredDisplayName() + " was not successfully saved. Please check the log or seek assistance from workbench support.";
                            JOptionPane.showMessageDialog(workbenchGUI, message, "QI Workbench",
                                    JOptionPane.WARNING_MESSAGE);
                            workbenchGUI.setStatusMessage(message);
                            return;
                        }
                    }else{
                        callFileChooser(workbenchGUI,title,currentDirectory,QIWConstants.FILE_CHOOSER_TYPE_SAVE,msg.getCommand());
                    }
                    return;
                } else
                if(cmd.equals(QIWConstants.SAVE_COMP_AS_CLONE_CMD)){
                    IComponentDescriptor desc = (IComponentDescriptor)msg.getContent();

                    if(getExistingComponentNodeCount() > 0 || restoredFilePath != null)
                        if(!doSaveComponentAsClone(restoredFilePath,desc)){
                            logger.info("SaveAs ACTION FOR " + desc.getPreferredDisplayName() + " NOT SUCCESSFUL!!!!");
                            workbenchGUI.setStatusMessage("Component " + desc.getPreferredDisplayName() + " was not successfully saved as a clone. Please check the log or seek assistance from workbench support.");
                            return;
                        }else
                            workbenchGUI.setStatusMessage("Component " + desc.getPreferredDisplayName() + " was saved as a clone successfully.");
                    else{
                        logger.info("component descriptor to be saved as " + desc);
                        List<IComponentDescriptor> list = new ArrayList<IComponentDescriptor>();
                        list.add(desc);
                        saveList = list;
                        String title = "Save qiComponent";
                        callFileChooser(workbenchGUI,title,currentDirectory,QIWConstants.FILE_CHOOSER_TYPE_SAVE,msg.getCommand());
                    }

                    return;
                } else
                if(cmd.equals(QIWConstants.RESTORE_DESKTOP_CMD)){
                    List<IComponentDescriptor> list = getOpenedComponentList();
                    List<IComponentDescriptor> closedList = getClosedComponentList();
                    logger.info("before performing desktop restore there are " + (list != null ? list.size() : 0) + " open component(s) and there are " + (closedList != null ? closedList.size() : 0) + " closed component(s).");
                    boolean ok = false;

                    if(list != null){
                            int action = JOptionPane.showConfirmDialog(workbenchGUI,"Do you want to save workbench before restoring the desktop?","Save confirmation",JOptionPane.YES_NO_OPTION);
                            if(action == JOptionPane.YES_OPTION ){
                                saveList = list;
                                String title = "Save Desktop";
                                if(root != null){
                                    if(doSaveDesktop1(restoredFilePath,cmd)){
                                        if(deactivateComponents()){
                                            currentNodeMap.clear();
                                            ok = true;
                                        }
                                    }
                                }else{
                                    //LTL if(deactivateComponents()){
                                        //currentNodeMap.clear();
                                        callFileChooser(workbenchGUI,title,currentDirectory,QIWConstants.FILE_CHOOSER_TYPE_SAVE,msg.getCommand());
                                    //LTL}
                                }
                            } else { //user answer No
                                if(deactivateComponents()){
                                    currentNodeMap.clear();
                                    ok = true;
                                }
                            }
                    }else
                        ok = true;
                    if(ok == true){
                        callFileChooser(workbenchGUI,"Open A Saved Set",currentDirectory,QIWConstants.FILE_CHOOSER_TYPE_OPEN,msg.getCommand());
                    }
                    return;
                } else
                if(cmd.equals(QIWConstants.OPEN_COMPONENT_GUI_CMD)){
                    IComponentDescriptor cd = (IComponentDescriptor)msg.getContent();
                    Node reopenedNode = null;
                    String dispName= cd.getPreferredDisplayName();
                    DecoratedNode getCurNode = currentNodeMap.get(dispName);
                    reopenedNode = getCurNode.getNode();
                    if(reopenedNode != null){
                        logger.info("reopenedNode.getNodeName() " + reopenedNode.getNodeName());
                        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.RESTORE_COMP_CMD,cd,
                                Node.class.getName(),reopenedNode);
                        IQiWorkbenchMsg res = messagingMgr.getMatchingResponseWait(msgID);
                        if(res != null && !res.isAbnormalStatus()){
                            final JInternalFrame comp = (JInternalFrame)res.getContent();
                            comp.setLocation(getCurNode.getLocation());
                            comp.setSize(getCurNode.getDimension().getSize());
                            Runnable updateAComponent = new Runnable() {
                                public void run() {
                                    //TODO eliminate synchronization on this non-final field 'desktop'
                                    synchronized(WorkbenchData.desktop) {
                                        WorkbenchData.desktop.add(comp);
                                    }
                                    comp.setVisible(true);
                                }
                            };
                            SwingUtilities.invokeLater(updateAComponent);
                            setComponentVisibleStateOpen(cd.getPreferredDisplayName());
                            logger.info("currentNodeMap after setComponentVisibleStateOpen " + currentNodeMap);
                        }else if(res == null)
                            logger.info("time out returning null response");
                        else {
                            logger.info("abnormal response returned");
                        }
                    }
                    return;
                } else
                if(cmd.equals(QIWConstants.RESTORE_DEFAULT_DESKTOP_CMD)){
                    String selectedFilePath = (String)msg.getContent();
                    
                    String status = checkFileExist(selectedFilePath);
                    if(!status.equals("yes")){
                    	String errorText = "Can not find " + selectedFilePath + " in the " + messagingMgr.getLocationPref() + " environment.";
                    	JOptionPane.showMessageDialog(workbenchGUI,errorText,
                            "File Not Found",JOptionPane.WARNING_MESSAGE);
                    	WorkbenchManager.getInstance().launchWorkbench();
                    	return;
                    }
                    
                    Element rootTemp = generateXMLRoot(selectedFilePath);
                    if(rootTemp == null){
                    	JOptionPane.showMessageDialog(workbenchGUI,"Failed to restore " + selectedFilePath,
                                "Restoration Failure",JOptionPane.WARNING_MESSAGE);
                    	WorkbenchManager.getInstance().launchWorkbench();
                    	return;
                    }
                    if(rootTemp != null && !rootTemp.getNodeName().equals("desktop")){
                        JOptionPane.showMessageDialog(workbenchGUI,"The default desktop " + selectedFilePath +
                                " does not have a compatible format required by the qiWorkbench tool. Please modify your preferences and reset the default workbench.",
                                "Incompatible File Format",JOptionPane.WARNING_MESSAGE);
                        //WorkbenchGUI gui = new WorkbenchGUI(WorkbenchManager.getInstance());
                        WorkbenchManager.getInstance().launchWorkbench();
                        //gui.setVisible(true);

                        return;
                    }

                    //restoredFilePath = selectedFilePath;
                    root = rootTemp;
                    restoreWorkbench(selectedFilePath);
                    return;
                } else
                if(cmd.equals(QIWConstants.REMOVE_COMPONENT_CMD)){
                    IComponentDescriptor desc = (IComponentDescriptor)msg.getContent();
                    if(componentExitsInSavedSet(desc)){
                        if(!removeComponentFromSavedSet(desc))
                            JOptionPane.showMessageDialog(workbenchGUI,"Error in deleting component " + desc.getPreferredDisplayName() + " from the saved set");
                    }
                    return;
                }
            }

        logger.warning("IO message not processed:"+msg.toString());
    }

      private void restoreWorkbench(String path){
          final String fpath = path;
          Runnable heavyRunnable = new Runnable(){
            public void run(){
                String title = "Launching qiWorkbench";
                String text = "Please wait while system is launching the qiWorkbench tool....";
                Icon icon = IconResource.getInstance().getImageIcon(IconResource.LOGO_ICON_QI);
                com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(null,100,true,500,title,text,icon);
                monitor.start("");
                WorkbenchGUI gui = new WorkbenchGUI(WorkbenchManager.getInstance());
                if(monitor.isCancel()){
                    System.exit(0);
                }
                workbenchGUI = gui;
                WorkbenchManager.getInstance().setWorkbenchGUI(gui);
                WorkbenchData.gui = gui;
                /*
                if(workbenchGUI != null && restore(root,restoredFilePath))
                    workbenchGUI.setVisible(true);
                if(workbenchGUI.isVisible()){
                      monitor.setCurrent(null,monitor.getTotal());
                 }
                 */
                if(workbenchGUI != null) {
                    //monitor.setCurrent(null,monitor.getTotal());
                    monitor.setCurrent(null,0);
                    monitor.setIndeterminate(false);
                    monitor.setText("Please wait while the system is restoring components...");
                    //monitor.start("Start restoring components.");
                    //monitor = ProgressUtil.createModalProgressMonitor(null,100,false,0,title,text,icon);
                    Map<String,DecoratedNode> tempMap = new HashMap<String,DecoratedNode>();
                    if(restoreDesktop(fpath,monitor,tempMap)){
                        restoredFilePath = fpath;
                        currentNodeMap = tempMap;
                    }else{
                        monitor.setCurrent(null,monitor.getTotal());
                        JOptionPane.showMessageDialog(workbenchGUI,"Problem in restoring the default workbench " + fpath +
                                ".  Please contact workbench support.",
                                "Problem in restoring workbench",JOptionPane.WARNING_MESSAGE);
                        root = null;
                    }
                    //workbenchGUI.setVisible(true);

                }
            }
          };
          new Thread(heavyRunnable).start();
      }

      private void launchWorkbench(){
          Runnable heavyRunnable = new Runnable(){
            public void run(){
                String title = "Launching qiWorkbench";
                String text = "Please wait while system is launching the qiWorkbench tool....";
                Icon icon = IconResource.getInstance().getImageIcon(IconResource.LOGO_ICON_QI);
                com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(null,100,true,500,title,text,icon);
                monitor.start("");
                WorkbenchGUI gui = new WorkbenchGUI(WorkbenchManager.getInstance());
                workbenchGUI = gui;
                WorkbenchManager.getInstance().setWorkbenchGUI(gui);
                WorkbenchData.gui = gui;
                Map<String,DecoratedNode> tempMap = new HashMap<String,DecoratedNode>();
                if(workbenchGUI != null && restoreDesktop(restoredFilePath,tempMap)){
                    workbenchGUI.setVisible(true);
                    currentNodeMap = tempMap;
                }
                if(workbenchGUI.isVisible()){
                      monitor.setCurrent(null,monitor.getTotal());
                 }

            }
          };
          new Thread(heavyRunnable).start();
      }
    private boolean removeComponentFromSavedSet(IComponentDescriptor cd){
        if(cd == null)
            return false;
        if(root == null)
            return false;
        Node node = getNodeByIComponentDescriptor(cd);
        root.removeChild(node);
        String xml = getSaveDesktopXML();
        if(save(restoredFilePath,xml)){
            removeNodeFromCurrentNodeMap(cd.getPreferredDisplayName());
            return true;
        }
        return false;
    }
    /**
     * Get the cid of this thread component
     * @return String
     */
    public String getCID() {
        return myCID;
    }

    /**
     * Get the component descriptor of this thread component
     * @return IComponentDescriptor
     */
    public IComponentDescriptor getIComponentDescriptor() {
        return messagingMgr.getMyComponentDesc();
    }

    /**
     * Set the current directory where the file chooser starts with
     * @param dir
     * @return
     */
    public void setCurrentDirectory(String dir){
        currentDirectory = dir;
    }

    /**
     * Get the current directory name
     * @return String
     */
    public String getCurrentDirectory(){
        return currentDirectory;
    }

    /**
     * Deactivate all the components in the data structure regardless of its status.
     * <p>
     * Note: A component's status could be New (Open) or Existing (Open or Closed)
     */
    private boolean deactivateComponents(){
        boolean ok = true;
        for (String key : currentNodeMap.keySet()){
            IComponentDescriptor cd = currentNodeMap.get(key).getDescriptor();
            //record usage event, quitting the qiComponent
            String event = StatsManager.getInstance().createStatsEvent(cd.getDisplayName(), ClientConstants.QUIT_ACTION);
            StatsManager.getInstance().logStatsEvent(event);

            //send message to deactivate each component whether it is a new, an existing, an open, or a closed component
            messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.DEACTIVATE_COMPONENT_CMD,
                    wbMgrDesc,QIWConstants.COMP_DESC_TYPE,cd);

            IQiWorkbenchMsg response = messagingMgr.getNextMsgWait();
            //QiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);

            if(response == null || response.isAbnormalStatus()){
                logger.warning("Error is removing active component: " + response.getContent());
                JOptionPane.showMessageDialog(workbenchGUI, "Error in deactivating the component " + cd.getPreferredDisplayName(), "QI Workbench",
                        JOptionPane.WARNING_MESSAGE);
                ok = false;
                break;
            }

        }
        return ok;
    }

    /**
     * Set wait flag which will make the thread wait or not wait until consumer notify
     * @param wait
     * @return
     */
    public void setWait(boolean wait){
        this.wait = wait;
    }

    /**
     * @return FileFilter
     */
    private FileFilter getFileFilter() {
        String[] cfgExtensions = new String[]{".cfg", ".CFG"};
        FileFilter cfgFilter = (FileFilter) new GenericFileFilter(cfgExtensions, "Configuration File (*.cfg)");
        return cfgFilter;
    }

    private void callFileChooser(Component parent,String title,String dir,String chooserType, String command){
        ArrayList list = new ArrayList();
        //1st element the parent GUI object
        list.add(parent);
        //2nd element is the dialog title
        list.add(title);
        //3rd element is a list that contains current directory to start with
        //and a flag (yes or no) indicating if a already remembered directory
        //should be used instead
        ArrayList lst = new ArrayList();
        lst.add(dir);
        lst.add("yes");
        list.add(lst);
        //4th element is the file filter
        list.add(getFileFilter());
        //5th element is the navigation flag
        list.add(false);
        //6th element is the producer component descriptor
        list.add(messagingMgr.getMyComponentDesc());
        //7th element is the type of file chooser either Open or Save
        list.add(chooserType);
        //8th element is the original qiworkbench message command or action that request for this service
        list.add(command);
        //9th element is the default pretext for the file name if user fails to pretext it. If no pretext is required then use empty String
        String userName = System.getProperty("user.name");
        list.add(userName);
        //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
        list.add(".cfg");
        //11th element is the target tomcat url where the file chooser chooses
        list.add(messagingMgr.getTomcatURL());
        openFileChooser(list);
    }

    /**
     * Generate the xml tree root node by a XML source file
     * @param path the file path
     * @return Element
     */
    private Element generateXMLRoot(String selectedPath) {
        if(selectedPath == null)
            return null;

        ArrayList<String> params = new ArrayList<String>();

        //Send a file read message to retrieve the ascii XML string
        //the first element of list is location preferenec
        params.add(messagingMgr.getLocationPref());
        //the second element is to add file path and name user just selected
        params.add(selectedPath);

        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.FILE_READ_CMD, QIWConstants.ARRAYLIST_TYPE, params,true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId);
        if (resp == null){
            JOptionPane.showMessageDialog(workbenchGUI, "Error in reading " + selectedPath + " due to timed out problem in retrieving message." + selectedPath, "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            return null;
        }else if(resp.isAbnormalStatus()) {
            logger.finest("IO error:"+(String)resp.getContent());
            JOptionPane.showMessageDialog(workbenchGUI, "Error in reading " + selectedPath + "Cause: " + (String)resp.getContent(), "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            return null;
        }

        ArrayList<String> strList = (ArrayList<String>)resp.getContent();
        String xml = "";
        for(String s : strList){
            xml += s;
        }
        return buildElement(xml);
    }

    
    /**
     * Validate the XML string making sure it is well formed
     * @param XML String
     * @return boolean
     */
    private boolean validateXmlString(String xml){
        InputStream xmlInStream = null;
        Element rt;
        try {
            xmlInStream = new ByteArrayInputStream(xml.getBytes());
            rt = getXMLTree(xmlInStream);
            try{
                xmlInStream.close();
            }catch(IOException e){
                e.printStackTrace();
                return false;
            }
        }catch(Exception e){
            e.printStackTrace();
            if(xmlInStream != null){
                try{
                    xmlInStream.close();
                }catch(IOException io){
                    io.printStackTrace();
                }
            }
            return false;
        }
        return true;
    }

    
    /**
     * Convert the XML string into the XML element object
     * @param XML String
     * @return Element
     */
    private Element buildElement(String xml){
        InputStream xmlInStream = null;
        Element rt;
        try {
            xmlInStream = new ByteArrayInputStream(xml.getBytes());
            rt = getXMLTree(xmlInStream);
            try{
                xmlInStream.close();
            }catch(IOException e){
                e.printStackTrace();
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(workbenchGUI, "IO Error caught: " + e.getMessage(), "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            if(xmlInStream != null){
                try{
                    xmlInStream.close();
                }catch(IOException io){
                    io.printStackTrace();
                }
            }
            return null;
        }
        return rt;
    }


    private List<String> getClosedComponentNameList(){
        if(root == null)
            return null;
        NodeList children = root.getChildNodes();
        for(int i = 0; i < children.getLength(); i++){
            Node child = children.item(i);
            if(child.getNodeType() == Node.ELEMENT_NODE){
                String nodeName = child.getNodeName();
                if(nodeName.equals("workbench")){
                    String str = ((Element)child).getAttribute("componentClosed");
                    if(str != null && str.trim().length() > 0){
                        String [] sArr = str.split("~");
                        return Arrays.asList(sArr);
                    }
                }
            }
        }
        return null;
    }
    private void setWorkbenchTitle(String workbench){
        String title = workbenchGUI.getTitle();
        int ind = -1;
        if(title != null)
         ind = title.indexOf("Saved Set:");
        if(ind != -1)
            title = title.substring(0, ind);
        if(title == null)
            title = "";
        workbenchGUI.setTitle(title + "\t\t Saved Set: " + workbench);
    }

    /**
     * Check if restoring desktop.
     * @return true if restoring desktop; otherwise, false.
     */

    /**
     * Handle state restoration including interfacing with user for the XML file to be restored.
     * @param  filePath the file path choosen by the user
     * @return true if desktop successful retored; otherwise, false
     */
    private boolean restoreDesktop(String filePath, Map<String,DecoratedNode> currentNodeMap1){
        String height=null, width = null;
        String x = null, y = null;
        int ix = WorkbenchData.DEFAULT_WORKBENCH_LOCATION_X, iy = WorkbenchData.DEFAULT_WORKBENCH_LOCATION_Y;
        int iwidth = WorkbenchData.DEFAULT_WORKBENCH_WIDTH, iheight = WorkbenchData.DEFAULT_WORKBENCH_HEIGHT;
        int pmsRestored = 0;

        //currentNodeMap.clear();
        height = root.getAttribute("height");
        width = root.getAttribute("width");
        x = root.getAttribute("x");
        y = root.getAttribute("y");
        try{
            if(height != null){
                iheight = Integer.valueOf(height.trim()).intValue();
            }
            if(width != null){
                iwidth = Integer.valueOf(width.trim()).intValue();
            }
            if(x != null){
                ix = Integer.valueOf(x.trim()).intValue();
            }
            if(y != null){
                iy = Integer.valueOf(y.trim()).intValue();
            }
        }catch(NumberFormatException e){
            e.printStackTrace();
        }

        Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        if(iy < 0 || iy > screen.height)
            iy = 0;
        if(ix < 0 && Math.abs(ix) > iwidth)
            ix = 0;
        if(ix > 0 && ix > screen.width)
            ix = 0;
        workbenchGUI.setLocation(ix,iy);
        workbenchGUI.setSize(iwidth,iheight);

        setWorkbenchTitle(filePath);
        workbenchGUI.validate();
        workbenchGUI.repaint();

        List<String> list = getClosedComponentNameList();
        logger.info("getClosedComponentNameList()=" + list);
        NodeList children = root.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                String nodeName = child.getNodeName();
                String compx = "0";
                String compy = "0";
                String compHeight = "800";
                String compWidth = "800";
                if (nodeName.equals("component")) {
                    String kind = ((Element)child).getAttribute("componentKind");
                    String type = ((Element)child).getAttribute("componentType");
                    String preferredDisplayName = ((Element)child).getAttribute("preferredDisplayName");
                    NodeList childs = child.getChildNodes();
                    for (int j = 0; j < childs.getLength(); j++) {
                        Node ch = childs.item(j);
                        if (ch != null && ch.getNodeType() == Node.ELEMENT_NODE) {
                            compx = ((Element)ch).getAttribute("x");
                            compy = ((Element)ch).getAttribute("y");
                            compHeight = ((Element)ch).getAttribute("height");
                            compWidth = ((Element)ch).getAttribute("width");
                        }
                    }

                    String msgID = "";
                    boolean isClosed = false;
                    if (list != null && list.indexOf(preferredDisplayName) != -1) {
                        isClosed = true;
                    }
                    logger.info(preferredDisplayName + " isClosed " + isClosed);
                    msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.ACTIVATE_COMPONENT_CMD,
                            Node.class.getName(),child,true);
                    IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID);
                    if (response == null) {
                        JOptionPane.showMessageDialog(workbenchGUI, "Error in activating component " + kind + " Cause: no message returned.","QI Workbench",
                                JOptionPane.WARNING_MESSAGE);
                        return false;
                    }
                    if (response.isAbnormalStatus()) {
                        JOptionPane.showMessageDialog(workbenchGUI,
                                "Error in activating component " + kind + " Cause: " + response.getContent(),"QI Workbench",
                                JOptionPane.WARNING_MESSAGE);
                        return false;
                    }

                    //Validate response content and get ComponentDescriptor for activated qiComponent
                    IComponentDescriptor compDesc;

                    if (response.getContentType().compareTo(ComponentDescriptor.class.getName()) != 0) {
                        JOptionPane.showMessageDialog(workbenchGUI, "Error in activating component " + kind +
                                " Cause: response to ACTIVATE_COMPONENT_CMD was received by the content type was not ComponentDescriptor",
                                "QI Workbench",
                                JOptionPane.WARNING_MESSAGE);
                        return false;
                    } else {
                        compDesc = (IComponentDescriptor) response.getContent();
                    }

                    // Begin QIWB-54 bugfix #1 (see note below)
                    // : qiSpace is empty String after restore if qiProjectManager was saved in closed state)
                    //
                    //Unique among the qiComponents, the qiProjectManager's state _must_ be restored
                    //when it is activated, because other qiComponents will query it for parameters such as 'qiSpace'
                    //even if the qiProjectManager's GUI is closed.
                    //Therefore, if the activated qiComponent is a qiProjectManager agent, restoer it before proceeding
                    //
                    //NOTE: This fix is copy pasted below because there are two restoreDesktop() methods, which differ
                    //only in that the second method also takes a ProgerssMonitor parameter
                    if ("qiProjectManager".equals(type)) {
                        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.RESTORE_COMP_CMD,compDesc,
                                Node.class.getName(),child, true);
                        response = messagingMgr.getMatchingResponseWait(msgID);
                        if (response == null) {
                            JOptionPane.showMessageDialog(workbenchGUI, "Error in restoring component " + kind + " Cause: no message returned.","QI Workbench",
                                JOptionPane.WARNING_MESSAGE);
                            return false;
                        }
                        if (response.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(workbenchGUI, "Error in restoring component " + kind + " Cause: " + response.getContent(),"QI Workbench",
                                JOptionPane.WARNING_MESSAGE);
                            return false;
                        }
                    }
                    //// end QIWB-54 bugfix #1

                    compDesc.setPreferredDisplayName(preferredDisplayName);
                    if (compDesc.getComponentKind().equals(QIWConstants.PLUGIN_PROJMGR_COMP)) ++pmsRestored;

                    DecoratedNode dNode = new DecoratedNode(child,compDesc);
                    dNode.setDimension(new Dimension(Integer.valueOf(compWidth).intValue(),Integer.valueOf(compHeight).intValue()));
                    dNode.setLocation(new Point(Integer.valueOf(compx).intValue(),Integer.valueOf(compy).intValue()));
                    if(isClosed)
                        dNode.setComponentClosed(true);
                    else
                        dNode.setComponentClosed(false);

                    currentNodeMap1.put(compDesc.getPreferredDisplayName(),dNode);

                    //logger.info("currentNodeMap after put " + currentNodeMap1);

                    ArrayList responseList = new ArrayList();
                    //the first element is plugin component to be restored (already activated)
                    responseList.add(0,compDesc);
                    //the second element is the type of the plugin referencing WorkbenchData.registeredPluginsIComponentDescriptors
                    responseList.add(1,type);
                    //the third element is the xml tree node representing the component
                    responseList.add(2,child);
                    //the fourth element is the component visible state stored in the workbench node
                    if(isClosed)
                        responseList.add(3,"Closed");
                    else
                        responseList.add(3,"Open");
                    QiWorkbenchMsg newMsg = null;
                    newMsg = new QiWorkbenchMsg(response.getProducerCID(),wbMgrDesc.getCID(),QIWConstants.DATA_MSG,
                            QIWConstants.ACTIVATE_COMPONENT_CMD,response.getMsgID(),
                            QIWConstants.ARRAYLIST_TYPE, responseList);
                    messagingMgr.routeMsg(newMsg);
                }
            }
        }

        //Tell all PMs to notify the qiComponents of their existance so they can conditionally associate with the PM.
        //Send the message to the Message Dispatcher who will broadcast it to each PM.
//        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.NOTIFY_PROJMGR_EXISTS_CMD);

        //If no PM restored, then restoring an old saveset. Create a PM and have it
        //associate itself with all restored qiComponents.
        if (pmsRestored == 0) {
            IComponentDescriptor projMgrDesc = WorkbenchAction.newComponent(QIWConstants.QI_PROJECT_MANAGER_NAME);
            //Notify all qiComponents of its associated PM
            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.NOTIFY_ASSOC_PROJMGR_CMD, QIWConstants.COMP_DESC_TYPE, projMgrDesc);
        }

        return true;
    }

    /**
     * Handle state restoration including interfacing with user for the XML file to be restored.
     * @param  filePath the file path choosen by the user
     * @param  monitor Progress monitor checking for component restoring progress.
     * @return true if desktop successful restored; otherwise, false
     */
    private boolean restoreDesktop(String filePath,ProgressMonitor monitor, Map<String,DecoratedNode> currentNodeMap1) {
    	if(root == null)
    		return false;
        String height=null, width = null;
        String x = null, y = null;
        int ix = WorkbenchData.DEFAULT_WORKBENCH_LOCATION_X, iy = WorkbenchData.DEFAULT_WORKBENCH_LOCATION_Y;
        int iwidth = WorkbenchData.DEFAULT_WORKBENCH_WIDTH, iheight = WorkbenchData.DEFAULT_WORKBENCH_HEIGHT;
        int pmsRestored = 0;
        currentNodeMap1.clear();

        height = root.getAttribute("height");
        width = root.getAttribute("width");
        x = root.getAttribute("x");
        y = root.getAttribute("y");

        try {
            if(height != null){
                iheight = Integer.valueOf(height.trim()).intValue();
            }
            if(width != null){
                iwidth = Integer.valueOf(width.trim()).intValue();
            }
            if(x != null){
                ix = Integer.valueOf(x.trim()).intValue();
            }
            if(y != null){
                iy = Integer.valueOf(y.trim()).intValue();
            }
        }catch(NumberFormatException e){
            e.printStackTrace();
        }
        Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        if(iy < 0 || iy > screen.height)
            iy = 0;
        if(ix < 0 && Math.abs(ix) > iwidth)
            ix = 0;
        if(ix > 0 && ix > screen.width)
            ix = 0;

        try{
            final int fix = ix, fiy = iy;
            final int fiwidth = iwidth, fiheight = iheight;
            final String f_filePath = filePath;
            Runnable updateAComponent = new Runnable() {
                public void run() {
                    workbenchGUI.setVisible(true);
                    workbenchGUI.setLocation(fix,fiy);
                    workbenchGUI.setSize(fiwidth,fiheight);
                    setWorkbenchTitle(f_filePath);
                    workbenchGUI.validate();
                    workbenchGUI.repaint();

                }
            };
            SwingUtilities.invokeLater(updateAComponent);


            List<String> list = getClosedComponentNameList();
            logger.info("getClosedComponentNameList()=" + list);

            int numOfComponents = 0;
            NodeList children = root.getChildNodes();
            for(int i = 0; i < children.getLength(); i++){
                Node child = children.item(i);
                if(child.getNodeType() == Node.ELEMENT_NODE){
                    String nodeName = child.getNodeName();
                    if(nodeName.equals("component")){
                        numOfComponents++;
                    }
                }
            }

            int ind = 0;
            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                if(child.getNodeType() == Node.ELEMENT_NODE){
                    String nodeName = child.getNodeName();
                    String compx = "0";
                    String compy = "0";
                    String compHeight = "800";
                    String compWidth = "800";
                    if(nodeName.equals("component")){
                        ind++;
                        String kind = ((Element)child).getAttribute("componentKind");
                        String type = ((Element)child).getAttribute("componentType");
                        String preferredDisplayName = ((Element)child).getAttribute("preferredDisplayName");
                        NodeList childs = child.getChildNodes();
                        for (int j = 0; j < childs.getLength(); j++) {
                            Node ch = childs.item(j);
                            if (ch != null && ch.getNodeType() == Node.ELEMENT_NODE) {
                                compx = ((Element)ch).getAttribute("x");
                                compy = ((Element)ch).getAttribute("y");
                                compHeight = ((Element)ch).getAttribute("height");
                                compWidth = ((Element)ch).getAttribute("width");
                            }
                        }

                        String msgID = "";
                        boolean isClosed = false;
                        if (list != null && list.indexOf(preferredDisplayName) != -1) {
                            isClosed = true;
                        }
                        logger.info(preferredDisplayName + " isClosed " + isClosed);
                        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.ACTIVATE_COMPONENT_CMD,
                                Node.class.getName(),child,true);
                        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID);
                        if (response == null) {
                            JOptionPane.showMessageDialog(workbenchGUI, "Error in restoring component " + kind + " Cause: no message returned.","QI Workbench",
                                    JOptionPane.WARNING_MESSAGE);
                            return false;
                        }
                        if (response.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(workbenchGUI, "Error in restoring component " + kind + " Cause: " + response.getContent(),"QI Workbench",
                                    JOptionPane.WARNING_MESSAGE);
                            return false;
                        }

                        //Validate response content and get ComponentDescriptor for activated qiComponent
                    IComponentDescriptor compDesc;

                    if (response.getContentType().compareTo(ComponentDescriptor.class.getName()) != 0) {
                        JOptionPane.showMessageDialog(workbenchGUI, "Error in activating component " + kind +
                                " Cause: response to ACTIVATE_COMPONENT_CMD was received by the content type was not ComponentDescriptor",
                                "QI Workbench",
                                JOptionPane.WARNING_MESSAGE);
                        return false;
                    } else {
                        compDesc = (IComponentDescriptor) response.getContent();
                    }

                    //// begin QIWB-54 bugfix #2(qiSpace is empty String after restore if qiProjectManager was saved in closed state)
                    //Unique among the qiComponents, the qiProjectManager's state _must_ be restored
                    //when it is activated, because other qiComponents will query it for parameters such as 'qiSpace'
                    //even if the qiProjectManager's GUI is closed.
                    //Therefore, if the activated qiComponent is a qiProjectManager agent, restoer it before proceeding.
                    if ("qiProjectManager".equals(type)) {
                        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.RESTORE_COMP_CMD,compDesc,
                                Node.class.getName(),child, true);
                        response = messagingMgr.getMatchingResponseWait(msgID);
                        if (response == null) {
                            JOptionPane.showMessageDialog(workbenchGUI, "Error in restoring component " + kind + " Cause: no message returned.","QI Workbench",
                                JOptionPane.WARNING_MESSAGE);
                            return false;
                        }
                        if (response.isAbnormalStatus()) {
                            JOptionPane.showMessageDialog(workbenchGUI, "Error in restoring component " + kind + " Cause: " + response.getContent(),"QI Workbench",
                                JOptionPane.WARNING_MESSAGE);
                            return false;
                        }
                    }
                    //// end QIWB-54 bugfix #2
                        compDesc.setPreferredDisplayName(preferredDisplayName);
                        if (compDesc.getComponentKind().equals(QIWConstants.PLUGIN_PROJMGR_COMP)) ++pmsRestored;

                        DecoratedNode dNode = new DecoratedNode(child,compDesc);
                        dNode.setDimension(new Dimension(Integer.valueOf(compWidth).intValue(),Integer.valueOf(compHeight).intValue()));
                        dNode.setLocation(new Point(Integer.valueOf(compx).intValue(),Integer.valueOf(compy).intValue()));
                        if(isClosed)
                            dNode.setComponentClosed(true);
                        else
                            dNode.setComponentClosed(false);
                        currentNodeMap1.put(compDesc.getPreferredDisplayName(),dNode);

                        //logger.info("currentNodeMap after put " + currentNodeMap);
                        ArrayList responseList = new ArrayList();
                        //the first element is plugin component asked to be restored (already activated)
                        responseList.add(0,compDesc);
                        //the second element is the type of the plugin referencing WorkbenchData.registeredPluginsIComponentDescriptors
                        responseList.add(1,type);
                        //the third element is the xml tree node representing the component
                        responseList.add(2,child);
                        //the fourth element is the component visible state stored in the workbench node
                        if(isClosed)
                            responseList.add(3,"Closed");
                        else
                            responseList.add(3,"Open");
                        QiWorkbenchMsg newMsg = null;
                        newMsg = new QiWorkbenchMsg(response.getProducerCID(),wbMgrDesc.getCID(),QIWConstants.DATA_MSG,
                                QIWConstants.ACTIVATE_COMPONENT_CMD,response.getMsgID(),
                                QIWConstants.ARRAYLIST_TYPE,responseList);
                        messagingMgr.routeMsg(newMsg);

                        float f = (float)ind/(float)numOfComponents;
                        monitor.setCurrent("Restoring component " + compDesc.getPreferredDisplayName(),(int)(f*100));
                    }
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }

        //Tell all PMs to notify the qiComponents of their existance so they can conditionally associate with the PM.
        //Send the message to the Message Dispatcher who will broadcast it to each PM
//        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.NOTIFY_PROJMGR_EXISTS_CMD);

        //If no PM restored, then restoring an old saveset. Create a PM and have it
        //associate itself with all restored qiComponents.
        if (pmsRestored == 0) {
            IComponentDescriptor projMgrDesc = WorkbenchAction.newComponent(QIWConstants.QI_PROJECT_MANAGER_NAME);
            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.NOTIFY_ASSOC_PROJMGR_CMD, QIWConstants.COMP_DESC_TYPE, projMgrDesc);
        }
        return true;
    }

    private String getComponentVisibleState(Element root, Element element){
        NodeList children = root.getChildNodes();
        for(int i = 0; i < children.getLength(); i++){
            Node child = children.item(i);
            if(child.getNodeType() == Node.ELEMENT_NODE){
                String nodeName = child.getNodeName();
                if(nodeName.equals("workbench")){
                    String s = ((Element)child).getAttribute("componentClosed");
                    if(s != null){
                        String [] sArr = s.split("~");
                        for(int ii = 0; sArr != null && ii < sArr.length; ii++){
                            if(element.getAttribute("preferredDisplayName").trim().equals(sArr[ii])){
                                return "Closed";
                            }
                        }
                        return "Open";
                    }
                }
            }
        }
        return "Open";
    }

    //Used by Save As A Clone for restoring individual component element
    private IComponentDescriptor restoreComponent(Element element){
        if(element == null)
            return null;

        IComponentDescriptor compDesc = null;
        if (element.getNodeType() == Node.ELEMENT_NODE) {
            String nodeName = element.getNodeName();
            if (nodeName.equals("component")) {
                String kind = element.getAttribute("componentKind");
                String type = element.getAttribute("componentType");
                String preferredDisplayName = element.getAttribute("preferredDisplayName");
                String msgID = "";

                msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.ACTIVATE_COMPONENT_CMD,
                        Node.class.getName(),element);

                IQiWorkbenchMsg res = messagingMgr.getMatchingResponseWait(msgID);
                if (res == null) {
                    JOptionPane.showMessageDialog(workbenchGUI, "Error in restoring component " + kind + " Cause: no message returned.","QI Workbench",
                            JOptionPane.WARNING_MESSAGE);
                    return null;
                }
                if (res.isAbnormalStatus()) {
                    JOptionPane.showMessageDialog(workbenchGUI, "Error in restoring component " + kind + " Cause: " + res.getContent(),"QI Workbench",
                            JOptionPane.WARNING_MESSAGE);
                    return null;
                }

                //logger.info("response = " + res);
                compDesc = (IComponentDescriptor)res.getContent();
                compDesc.setPreferredDisplayName(preferredDisplayName);

                DecoratedNode dNode = new DecoratedNode(element,compDesc);
                setNodeToCurrentNodeMap(compDesc.getPreferredDisplayName(),dNode);
                logger.info("currentNodeMap after  setNodeToCurrentNodeMap " + currentNodeMap);

                ArrayList responseList = new ArrayList();
                //the first element is plugin component asked to be restored (already activated)
                responseList.add(0,compDesc);
                //the second element is the type of the plugin referencing WorkbenchData.registeredPluginsIComponentDescriptors
                responseList.add(1,type);
                //the third element is the xml tree node representing the component
                responseList.add(2,element);
                //the fourth element
                responseList.add(3,"Open");
                QiWorkbenchMsg newMsg = null;
                newMsg = new QiWorkbenchMsg(res.getProducerCID(),wbMgrDesc.getCID(),QIWConstants.DATA_MSG,
                        QIWConstants.ACTIVATE_COMPONENT_CMD,res.getMsgID(),
                        QIWConstants.ARRAYLIST_TYPE,responseList);
                messagingMgr.routeMsg(newMsg);
            }
        }
        return compDesc;
    }


    private synchronized boolean doSaveComponent(IComponentDescriptor cd){
        if(cd == null)
            return false;
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_CMD,cd,true);
        ////QiWorkbenchMsg resp = messagingMgr.getNextMsgWait();
        //IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);
        //IQiWorkbenchMsg resp = getResponseAfterTimeAdjustment(msgId, 10000, messagingMgr);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId, 60000, true);
        if (resp == null){
            logger.warning("Response to getting the state from component " + cd.getPreferredDisplayName() + " returning null due to time running out.");
            JOptionPane.showMessageDialog(workbenchGUI, "The action of getting the state from component " + cd.getPreferredDisplayName() + " is running out of time.", "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            return false;
        }else if(resp.isAbnormalStatus()){
            logger.info("Error is getting xml state information from component: " + resp.getContent());
            JOptionPane.showMessageDialog(workbenchGUI, "There is an error in getting the state from component " + cd.getPreferredDisplayName() + ". Cause: " + resp != null ? resp.getContent() : "", "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            return false;
        }

        String xml = (String)resp.getContent();
        if(!validateXmlString(xml)){
        	JOptionPane.showMessageDialog(workbenchGUI, "XML Validation Error found in the state of the component " + cd.getPreferredDisplayName(), "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
           	return false;
        }
        //try{
        nu.xom.Element rootElement = nu.xom.converters.DOMConverter.convert(root);

        Element saveElement = buildElement(xml);
        nu.xom.Element myElement = nu.xom.converters.DOMConverter.convert(saveElement);
        String desktopXML = rootElement.toXML();

        if(currentNodeMap.containsKey(cd.getPreferredDisplayName())){
            DecoratedNode dn = currentNodeMap.get(cd.getPreferredDisplayName());
            String oldName = dn.getOldDisplayName();

            String name;
            if(oldName != null)
                name = oldName;
            else
                name = cd.getPreferredDisplayName();
            if(!dn.isNew()){
                nu.xom.Elements children = rootElement.getChildElements();
                for(int i = 0; i < children.size(); i++){
                    nu.xom.Element child = children.get(i);
                    String nodeName = child.getLocalName();
                    if(nodeName.equals("component")){
                        if(child.getAttributeValue("preferredDisplayName").trim().equals(name)){
                            rootElement.removeChild(child);
                            rootElement.insertChild(myElement,i);
                        }
                    }
                }

                desktopXML = rootElement.toXML();

                if(save(restoredFilePath,desktopXML)){
                    //refresh current node list on the current desktop
                    DecoratedNode dNode = new DecoratedNode(saveElement,cd);
                    dNode.setOldDisplayName(null);
                    setNodeToCurrentNodeMap(cd.getPreferredDisplayName(),dNode);

                    //rebuild the xml root node

                    root = generateXMLRoot(restoredFilePath);
                    return true;
                }
                //}
            } else { //the newly activated component to be saved into the saved set
                rootElement.insertChild(myElement,getExistingComponentNodeCount());
                //desktopXML = desktopXML.substring(0,desktopXML.indexOf("</desktop>")) + xml + "</desktop>";
                desktopXML = rootElement.toXML();
                if(save(restoredFilePath,desktopXML)){
                    //refresh current node list on the current desktop
                    setComponentNode(cd.getPreferredDisplayName(),saveElement);
                    logger.info("currentNodeMap after save and setComponentNode " + currentNodeMap);

                    DecoratedNode dNode = new DecoratedNode(saveElement,cd);

                    //rebuild the xml root node
                    root = generateXMLRoot(restoredFilePath);
                    return true;
                }
            }
        }
        return false;
        //}catch(Exception e){
        //  e.printStackTrace();
        //  return false;
        //}
    }

    private synchronized boolean doSaveComponentAsClone(String path, IComponentDescriptor cd){

        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_AS_CLONE_CMD,cd);
        ////QiWorkbenchMsg resp = messagingMgr.getNextMsgWait();
        //IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);
        //IQiWorkbenchMsg resp = getResponseAfterTimeAdjustment(msgId, 10000, messagingMgr);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId, 60000, true);
        if (resp == null){
            logger.warning("Response to getting the state from component " + cd.getPreferredDisplayName() + " returning null due to time running out.");
            JOptionPane.showMessageDialog(workbenchGUI, "The action of getting the state from component " + cd.getPreferredDisplayName() + " is running out of time.", "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            return false;
        }else if(resp.isAbnormalStatus()) {
            logger.info("Error is getting xml state information from component: " + resp.getContent());
            JOptionPane.showMessageDialog(workbenchGUI, "Error in saving component " + cd.getPreferredDisplayName() + " cause: " + resp != null ? resp.getContent() : "", "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            return false;
        }

        String xml = (String)resp.getContent();
        if(!validateXmlString(xml)){
        	JOptionPane.showMessageDialog(workbenchGUI, "XML Validation Error found in the state of the component " + cd.getPreferredDisplayName(), "QI Workbench",
                JOptionPane.WARNING_MESSAGE);

        	return false;
        }
        String  desktopXML = "";
        Element saveElement = buildElement(xml);
        //try{
        if (root != null){//empty saved set
            nu.xom.Element rootElement = nu.xom.converters.DOMConverter.convert(root);
            //saveElement.setAttribute("preferredDisplayName","CopyOf" + cd.getPreferredDisplayName());
            nu.xom.Element myElement = nu.xom.converters.DOMConverter.convert(saveElement);
            rootElement.appendChild(myElement);
            desktopXML = rootElement.toXML();
        } else {
            desktopXML = "<?xml version=\"1.0\" ?>\n";
            desktopXML += "<desktop height=\"" + workbenchGUI.getHeight() + "\" ";
            desktopXML += "width=\"" + workbenchGUI.getWidth() + "\" ";
            desktopXML += "x=\"" + workbenchGUI.getX() + "\" ";
            desktopXML += "y=\"" + workbenchGUI.getY() + "\">\n ";
            desktopXML += xml;
            desktopXML += "</desktop>\n";
        }

        if (save(path, desktopXML)) {
            restoredFilePath = path;
            //refresh current node list on the current desktop
            root = generateXMLRoot(path);
            IComponentDescriptor desc = restoreComponent(saveElement);
            return true;
        }
        return false;
    }

    private void reportSaveAction(){
    	String title = "Save Action Report";
    	String message = "Save set: " + restoredFilePath + "\n\n";
    	message += componentsSucceedToGetStates.size() + " component(s) saved:\n";
    	StringBuffer buf = new StringBuffer();
    	for(IComponentDescriptor comp : componentsSucceedToGetStates){
    		buf.append("\t");
    		buf.append(comp.getPreferredDisplayName());
    		buf.append("\n");
    	}
    	message += buf;
    	message += "\n\n";
    	
    	if(!componentsFailedToGetStates.isEmpty()){
    		message += componentsFailedToGetStates.keySet().size() + " component(s) not saved:\n";
    		StringBuffer buf1 = new StringBuffer();
    	
        	Enumeration<Object> it = componentsFailedToGetStates.keys();
        	while(it.hasMoreElements()){
        		Object key = it.nextElement();
        		buf1.append("\t");
        		buf1.append(key);
        		buf1.append("\n");
        	}
        	message += buf1;
    	}
    	JOptionPane.showMessageDialog(workbenchGUI, message, title, JOptionPane.INFORMATION_MESSAGE);
    }
    
    private String saveStatus = "";
    private boolean doSaveDesktop1(String filePath,String cmd){

        final String fFilePath = filePath;
        final String command = cmd;
        Runnable heavyRunnable = new Runnable(){
            public void run(){
                String title = "Saving qiWorkbench states";
                String text = "Please wait while system is saving qiWorkbench states....";
                Icon icon = IconResource.getInstance().getImageIcon(IconResource.LOGO_ICON_QI);
                com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(workbenchGUI,100,false,500,title,text,icon);
                monitor.start("");

                if(monitor.isCancel()){
                	saveStatus = "failure";
                    return;
                }
                monitor.setCurrent("Gathering the workbench states ...",0);
                String xml = getSaveDesktopXML();
                
                if(!componentsFailedToGetStates.isEmpty()){
                	int mode = (root == null) ? StateMessageDialog.NEW_SAVE_SET_MODE : StateMessageDialog.EXISTING_SAVE_SET_MODE;
                	StateMessageDialog dialog = new StateMessageDialog(workbenchGUI,messagingMgr,true,componentsFailedToGetStates,fFilePath,mode);
                	dialog.setVisible(true);
                	if(dialog.getUserAction() == StateMessageDialog.CANCEL_SAVE){
                		saveStatus = "failure";
                		monitor.setCurrent(null,monitor.getTotal());
                		return;
                	}else if(dialog.getUserAction() == StateMessageDialog.SAVE_ALTERNATIVE){
                		String newfilePath = dialog.getAlternativeSaveSetPath();
                		monitor.setCurrent("Saving the workbench states ...",50);
                        if(save(newfilePath,xml)){
                            //root = buildElement(xml);
                            root = generateXMLRoot(newfilePath);
                            resetComponentNodeMap();
                            monitor.setCurrent(null,monitor.getTotal());
                            restoredFilePath = newfilePath;
                            setWorkbenchTitle(newfilePath);
                            saveStatus = "success";
                            reportSaveAction();
                        }else
                        	saveStatus = "failure";
                	}else if(dialog.getUserAction() == StateMessageDialog.OVERWRITE){
                		if(save(fFilePath,xml)){
                            //root = buildElement(xml);
                            root = generateXMLRoot(fFilePath);
                            resetComponentNodeMap();
                            monitor.setCurrent(null,monitor.getTotal());
                            restoredFilePath = fFilePath;
                            setWorkbenchTitle(fFilePath);
                            saveStatus = "success";
                            reportSaveAction();
                        }else{
                        	saveStatus = "failure";
                        }
                	}
                }else if(xml != null){
                    monitor.setCurrent("Saving the workbench states ...",50);
                    if(save(fFilePath,xml)){
                        //root = buildElement(xml);
                        root = generateXMLRoot(fFilePath);
                        resetComponentNodeMap();
                        logger.info("CurrentNodeMap after doSaveDesktop1 and resetComponetNodeMap " + currentNodeMap);
                        monitor.setCurrent(null,monitor.getTotal());
                        restoredFilePath = fFilePath;
                        setWorkbenchTitle(fFilePath);
                        reportSaveAction();
                        saveStatus = "success";
                    }else{
                        if(command.equals(QIWConstants.SAVE_DESKTOP_THEN_QUIT_CMD)){
                            int status = JOptionPane.showConfirmDialog(null,
                            "Problem in saving workbench information. Do you want to exit anyway?",
                            "Confirm to quit",JOptionPane.YES_NO_OPTION);
                            if(status == JOptionPane.YES_OPTION) {
                                //Close all sockets available for reuse
                                SocketManager.getInstance().closeAllSockets();
                                System.exit(0);
                            }
                        }
                        saveStatus = "failure";
                    }
                }else{
                    saveStatus = "failure";
                    //messagingMgr.resetMsgQueue();
                    JOptionPane.showMessageDialog(workbenchGUI, "Error in saving workbench states. Please try again or contact workbench support.", "QI Workbench",
                            JOptionPane.WARNING_MESSAGE);
                    monitor.setCurrent(null,monitor.getTotal());
                }
            }
        };
        saveStatus = "";
        Thread thread = new Thread(heavyRunnable);
        thread.start();
        while(thread.isAlive()){
        	try{
        		Thread.sleep(500);
        	}catch(InterruptedException e){}
        }
        logger.info("saveStatus = " + saveStatus);
        if(saveStatus.equals("success"))
          return true;
        else
          return false;
    }

    private void resetComponentNodeMap(){
        for(String key : currentNodeMap.keySet()){
            DecoratedNode dNode = currentNodeMap.get(key);
            IComponentDescriptor cd = dNode.getDescriptor();
            Node node = getNodeByIComponentDescriptor(cd);
            if(node != null){
                if(dNode.isNew())
                    WorkbenchAction.updateUIAfterSave(cd);
                setComponentNode(cd.getPreferredDisplayName(),node);

            }//else{
            //    logger.info("~~~~~~~~~~something really wrong somewhere ~~~~~~~~~~~~~~~~~~~~~~");
            //
            //}
        }
    }
    // a data storage containing names(key) of components and the values regarding
    // the reason why the state of the component can not be obtained
    private Properties componentsFailedToGetStates = new Properties();
    private List<IComponentDescriptor> componentsSucceedToGetStates = new ArrayList<IComponentDescriptor>();
    private String getSaveDesktopXML(){
    	try{
	    	componentsFailedToGetStates.clear();
	    	componentsSucceedToGetStates.clear();
	        String xml = "<?xml version=\"1.0\" ?>\n";
	        xml += "<desktop height=\"" + workbenchGUI.getHeight() + "\" ";
	        xml += "width=\"" + workbenchGUI.getWidth() + "\" ";
	        xml += "x=\"" + workbenchGUI.getX() + "\" ";
	        xml += "y=\"" + workbenchGUI.getY() + "\">\n ";
	        for(String key : currentNodeMap.keySet()){
	            if(currentNodeMap.get(key).getComponentClosed() == true){
	                Node node = getNodeByIComponentDescriptor(currentNodeMap.get(key).getDescriptor());
	                if(node != null){
	                    String sXML = nu.xom.converters.DOMConverter.convert((Element)node).toXML();
	                    xml += sXML;
	                }else{
	                    logger.warning("Error: Unexpected data integrity error in workbench state data structure. Please contact workbench support for further assistance.");
	                    String componentName = currentNodeMap.get(key).getDescriptor().getPreferredDisplayName();
	                    logger.info("Error: " + componentName + " is in closed state but getNodeByIComponentDescriptor(" + componentName + ") return null.");
	                    componentsFailedToGetStates.put(componentName, "Servere Data Integrity Error");
	                    //return null;
	                }
	            }else{
	                IComponentDescriptor cd = currentNodeMap.get(key).getDescriptor();
	                String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_CMD,cd,true);
	                
	/*                
	        		int WAIT_TIME = 10000;
	        		boolean proceed = true;
	        		IQiWorkbenchMsg response = null;
	        		BooleanSettings setting = new BooleanSettings();
	        		while(proceed){
//	        		wait and try again
	        			response = null;
	        	        for (long t=(WAIT_TIME<1000 ? WAIT_TIME : 1000); t <= WAIT_TIME; t += 1000) {
	        	        	response = messagingMgr.getMatchingResponse(msgId);
	        	        	if(response != null)
	        	        		break;
	        	            try {
	        	                Thread.currentThread().sleep(1000);
	        	            } catch (InterruptedException ie) {}
	        	        }
	        			
	        			if(response == null) {
        					logger.info("Asking user to proceed or not");
        					logger.info("Show confirm dialog flag (will not show if user enables not ask again)=" + setting.getValue());
        					String text = "The system may need more time to finish the task. Do you want to proceed?";
        					int n = showConfirmDialog(workbenchGUI, text, "Confirm to continue with task?", JOptionPane.YES_NO_OPTION, setting);
        		            if(n != JOptionPane.YES_OPTION){
        						proceed = false;
        						messagingMgr.removeMatchingRequest(msgId);  //remove outstanding request
        						continue;
        					}
	        		        //user yes to proceed
	        		         WAIT_TIME*=10;
	        		        logger.info("Continue to get response by increasing wait time to " + WAIT_TIME);
	        			}else{
	        				proceed = false;
	        				componentsFailedToGetStates.put(cd.getPreferredDisplayName(), "Time Out");
	        			}
	        		
	        			try{
	        				Thread.sleep(5000);
	        			}catch(InterruptedException ie){}
	        		}
*/
	                //IQiWorkbenchMsg response = null;
	                //IQiWorkbenchMsg response = getResponseAfterTimeAdjustment(msgId, 10000, messagingMgr);
	                IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId, 60000, true);
	        		if(response == null){
        				componentsFailedToGetStates.put(cd.getPreferredDisplayName(), "Time Out");

	                
	                
	                
	                
	                
	                ////IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);
	                ////if (resp == null){
	                    logger.warning("Response time out while getting the state from component name = " + cd.getPreferredDisplayName());
	                    //JOptionPane.showMessageDialog(workbenchGUI, "The action of getting the state from component " + cd.getPreferredDisplayName() + " is running out of time.", "QI Workbench",JOptionPane.WARNING_MESSAGE);
	                    //componentsFailedToGetStates.put(cd.getPreferredDisplayName(), "Time Out");
	                    //return null;
	                    //continue;
	                }else if(response.isAbnormalStatus()) {
	                    logger.info("Error in getting xml state information from component: " + response.getContent());
	                    //JOptionPane.showMessageDialog(workbenchGUI, "There is an error in getting the state from component " + cd.getPreferredDisplayName() + ". Cause: " + resp != null ? resp.getContent() : "", "QI Workbench",
	                    //        JOptionPane.WARNING_MESSAGE);
	                    componentsFailedToGetStates.put(cd.getPreferredDisplayName(), response.getContent());
	                    //return null;
	                }else{
	                	String xmlTemp = (String)response.getContent();
	                	if(validateXmlString(xmlTemp)){
	                		xml += xmlTemp;
	                		componentsSucceedToGetStates.add(cd);
	                	}else
	                		componentsFailedToGetStates.put(cd.getPreferredDisplayName(), "Validation error in XML states.");
	                }
	            }
	        }
	        xml += getWorkbenchStates();
	        xml += "</desktop>\n";
	        return xml;
    	}catch(Exception e){
    		e.printStackTrace();
    		return null;
    	}
    }

    private synchronized boolean saveDesktop(List<IComponentDescriptor> list){
        if(list == null || list.size() == 0)
            return false;
        String xml = "<?xml version=\"1.0\" ?>\n";
        xml += "<desktop height=\"" + workbenchGUI.getHeight() + "\" ";
        xml += "width=\"" + workbenchGUI.getWidth() + "\" ";
        xml += "x=\"" + workbenchGUI.getX() + "\" ";
        xml += "y=\"" + workbenchGUI.getY() + "\">\n ";


        for(IComponentDescriptor cd : list){
            String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_CMD,cd,true);
            //IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);
            //IQiWorkbenchMsg resp = getResponseAfterTimeAdjustment(msgId, 10000, messagingMgr);
            IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId, 60000, true);
            if (resp == null){
                logger.warning("Response to getting the state from component " + cd.getPreferredDisplayName() + " returning null due to time running out.");
                JOptionPane.showMessageDialog(workbenchGUI, "The action of getting the state from component " + cd.getPreferredDisplayName() + " is running out of time.", "QI Workbench",
                        JOptionPane.WARNING_MESSAGE);
                return false;
            }else if (resp.isAbnormalStatus()) {
                logger.info("Error is getting xml state information from component: " + resp.getContent());
                JOptionPane.showMessageDialog(workbenchGUI, "Error in saving component " + cd.getPreferredDisplayName() + " cause: " + resp != null ? resp.getContent() : "", "QI Workbench",
                        JOptionPane.WARNING_MESSAGE);
                return false;
            }
            String xmlTemp = (String)resp.getContent();
            Element elem = buildElement(xmlTemp);
            xml += xmlTemp;
        }
        String workbench = getWorkbenchStates();
        if(workbench != null)
            xml += workbench;
        xml += "</desktop>\n";
        if(save(restoredFilePath,xml)){
            //root = buildElement(xml);
            root = generateXMLRoot(restoredFilePath);
            return true;
        }else
            return false;
    }

    private List<IComponentDescriptor> getOpenedComponentList(){
        if(currentNodeMap.isEmpty())
            return null;
        List<IComponentDescriptor> list = new ArrayList<IComponentDescriptor>();
        for(String key : currentNodeMap.keySet()){
            if(currentNodeMap.get(key).getComponentClosed() == false){
                list.add(currentNodeMap.get(key).getDescriptor());
            }
        }
        return list;
    }

    private List<IComponentDescriptor> getClosedComponentList(){
        if(currentNodeMap.isEmpty())
            return null;
        List<IComponentDescriptor> list = new ArrayList<IComponentDescriptor>();
        for(String key : currentNodeMap.keySet()){
            if(currentNodeMap.get(key).getComponentClosed() == true){
                list.add(currentNodeMap.get(key).getDescriptor());
            }
        }
        return list;
    }

    /**
     * @param list of components to be saved
     * @param path where the user chooses to save
     * @param cmd the message command either save component or save the desktop
     * @return if successful return true else false
     */
    private boolean saveComponentList(List<IComponentDescriptor> list, String selectedPath, String cmd){
        ArrayList<String> lst1 = new ArrayList<String>();
        String xml = "<?xml version=\"1.0\" ?>\n";
        //if(cmd.equals(QIWConstants.SAVE_DESKTOP_CMD)){
            xml += "<desktop height=\"" + workbenchGUI.getHeight() + "\" ";
            xml += "width=\"" + workbenchGUI.getWidth() + "\" ";
            xml += "x=\"" + workbenchGUI.getX() + "\" ";
            xml += "y=\"" + workbenchGUI.getY() + "\">\n ";
        //}else{
        //  xml += "<desktop>\n";
        //}
        for(IComponentDescriptor cd : list){
            String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_CMD,cd,true);
            ////QiWorkbenchMsg resp = messagingMgr.getNextMsgWait();
            IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);
            if (resp == null){
                logger.info("Response to saving component " + cd.getPreferredDisplayName() + " returning null due to timed out.");
                JOptionPane.showMessageDialog(workbenchGUI, "Response to saving component " + cd.getPreferredDisplayName() + " returning null due to timed out.", "QI Workbench",
                        JOptionPane.WARNING_MESSAGE);
                return false;
            }else if(resp.isAbnormalStatus()){
                logger.info("Error is getting xml state information from component: " + resp.getContent());
                JOptionPane.showMessageDialog(workbenchGUI, "Error in saving component " + cd.getPreferredDisplayName() + " cause: " + resp != null ? resp.getContent() : "", "QI Workbench",
                        JOptionPane.WARNING_MESSAGE);
                return false;
            }
            String xmlTemp = (String)resp.getContent();
            if(validateXmlString(xmlTemp)){
            	xml += xmlTemp;
            }else{
            	JOptionPane.showMessageDialog(workbenchGUI, "XML Validation Error found in the state of the component " + cd.getPreferredDisplayName(), "QI Workbench",
                        JOptionPane.WARNING_MESSAGE);
            	return false;
            }
        }
        /*
         String workbench = getWorkbenchStates();
         if(workbench != null)
         xml += workbench;
         */
        xml += "</desktop>\n";  //the third element is to add the xml content returned from the component


        //String strtmp = resp.toString().length() > 501  ?  resp.toString().substring(0,500) : resp.toString();
        logger.info("returned xml state information = " + xml);

        //first element of list is location preferenec
        boolean isLocal = messagingMgr.getLocationPref().equals(QIWConstants.LOCAL_SERVICE_PREF) == true;
        if(isLocal)
            lst1.add(QIWConstants.LOCAL_SERVICE_PREF);
        else
            lst1.add(QIWConstants.REMOTE_SERVICE_PREF);

        //logger.info("chooser's selected path = " + chooser.getPath() + "  and name = " + chooser.getName());
        logger.info("chooser's selected path = " + selectedPath);

        //the second element is to add file path and name user just selected
        //lst1.add(chooser.getPath() + "/" + chooser.getName());
        lst1.add(selectedPath);
        //the 3rd element is to add xml string returned from the components
        lst1.add(xml);

        //send a message to message dispatcher for persist the state information into selected file path within preferred environment (local or remote)
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.FILE_WRITE_CMD,QIWConstants.ARRAYLIST_TYPE,lst1,true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);
        ////QiWorkbenchMsg resp = messagingMgr.getNextMsgWait();
        if (resp == null || resp.isAbnormalStatus()) {
            logger.info("IO error in requesting to save state :"+ resp != null ? (String)resp.getContent() : "");
            //JOptionPane.showMessageDialog(workbenchGUI, "Error in writing file " + chooser.getPath() + "/" + chooser.getName() + " cause: " + resp != null ? (String)resp.getContent() : "", "QI Workbench",
            JOptionPane.showMessageDialog(workbenchGUI, "Error in writing file " + selectedPath + " cause: " + resp != null ? (String)resp.getContent() : "", "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            //chooser.setVisible(false);
            return false;
        }
        String message = "";
        root = buildElement(xml);
        for(IComponentDescriptor cd : list){
            if(currentNodeMap.containsKey(cd.getPreferredDisplayName())){
                Node node = getNodeByIComponentDescriptor(cd);
                if(node != null)
                    currentNodeMap.get(cd.getPreferredDisplayName()).setNode(node);
            }
        }
        if(cmd.equals(QIWConstants.SAVE_DESKTOP_CMD))
            message = "The desktop has been successfully saved to " + selectedPath;
        else
            message = "The component has been successfully saved to " + selectedPath;
        restoredFilePath = selectedPath;

        JOptionPane.showMessageDialog(workbenchGUI, message, "QI Workbench",
                JOptionPane.INFORMATION_MESSAGE);
        return true;
    }


    private Node getNodeByIComponentDescriptor(IComponentDescriptor cd){
        if(root != null){
            NodeList children = root.getChildNodes();
            for(int i = 0; i < children.getLength(); i++){
                Node child = children.item(i);
                if(child.getNodeType() == Node.ELEMENT_NODE){
                    String nodeName = child.getNodeName();
                    if(nodeName.equals("component")){
                        if(((Element)child).getAttribute("preferredDisplayName").trim().equals(cd.getPreferredDisplayName()))
                            return child;
                    }
                }
            }
        }
        return null;
    }

    private boolean save(String filePath, String xml){
        if(xml == null || filePath == null || filePath.trim().length() == 0)
            return false;
        
        //first element of list is location preferenec
        boolean isLocal = messagingMgr.getLocationPref().equals(QIWConstants.LOCAL_SERVICE_PREF) == true;
        ArrayList lst1 = new ArrayList();
        if(isLocal)
            lst1.add(QIWConstants.LOCAL_SERVICE_PREF);
        else
            lst1.add(QIWConstants.REMOTE_SERVICE_PREF);


        logger.info("chooser's selected path = " + filePath);

        //the second element is to add file path and name user just selected
        //lst1.add(chooser.getPath() + "/" + chooser.getName());
        lst1.add(filePath);
        //the 3rd element is to add xml string returned from the components
        lst1.add(xml);

        //send a message to message dispatcher for persist the state information into selected file path within preferred environment (local or remote)
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.FILE_WRITE_CMD,QIWConstants.ARRAYLIST_TYPE,lst1,true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,30000);
        ////QiWorkbenchMsg resp = messagingMgr.getNextMsgWait();
        if(resp == null){
            logger.info("Response to writing states to " + filePath + " returning null due to timed out.");
            JOptionPane.showMessageDialog(workbenchGUI, "Response to writing states to " + filePath + " returning null due to timed out.", "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            return false;
        }else if ( resp.isAbnormalStatus()) {
            logger.info("IO error in requesting to save state :"+ resp != null ? (String)resp.getContent() : "");
            //JOptionPane.showMessageDialog(workbenchGUI, "Error in writing file " + chooser.getPath() + "/" + chooser.getName() + " cause: " + resp != null ? (String)resp.getContent() : "", "QI Workbench",
            JOptionPane.showMessageDialog(workbenchGUI, "Error in writing file " + filePath + " cause: " + resp != null ? (String)resp.getContent() : "", "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            //chooser.setVisible(false);
            return false;
        }
        return true;
    }

    private void openFileChooser(ArrayList list){
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID);
        if(response == null){
            logger.warning("Internal error occurring in getting file chooser service returning null");
            ErrorDialogUtils.showInternalErrorDialog(Thread.currentThread().getStackTrace(), "Failed to start file chooser service");
            return;
        }
        if(response.isAbnormalStatus()) {
            logger.warning("Internal error occurring in getting file chooser service. Cause: " + response.getContent());
            ErrorDialogUtils.showInternalErrorDialog(Thread.currentThread().getStackTrace(), "Failed to start file chooser service");
            return;
        }else{
            IComponentDescriptor cd = (IComponentDescriptor)response.getContent();

            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_FILE_CHOOSER_CMD,cd,QIWConstants.ARRAYLIST_TYPE,list);
            messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD,cd);
            return;
        }
    }


    /**
     * Get the display name of the component's parent.
     * @param cid Component ID
     * @return component type display name
     * @deprecated Use getRegisteredComponentDisplayNameByDescriptor() in messaging manager
     */
    public static String getRegisteredComponentParentDisplayName(String cid){
        logger.info("cid passed " + cid);
        for(IComponentDescriptor cd : WorkbenchData.registeredPluginsComponentDescriptors){
            logger.info("cid " + cd.getCID() + " displayName " + cd.getDisplayName());
            if(cid.substring(cid.lastIndexOf(".")+1).startsWith(cd.getCID().substring(cd.getCID().lastIndexOf(".")+1)))
                return cd.getDisplayName();
        }
        for(IComponentDescriptor cd : WorkbenchData.registeredViewersComponentDescriptors){
            logger.info("cid " + cd.getCID() + " displayName " + cd.getDisplayName());
            if(cid.substring(cid.lastIndexOf(".")+1).startsWith(cd.getCID().substring(cd.getCID().lastIndexOf(".")+1)))
                return cd.getDisplayName();
        }
        return "";
    }


    /**
     * @param cid
     * @return component type display name
     */
    public static String getRegisteredViewerParentDisplayName(String cid){
        for(IComponentDescriptor cd : WorkbenchData.registeredViewersComponentDescriptors){
            logger.info("cid " + cd.getCID() + " displayName " + cd.getDisplayName());
            if(cid.substring(cid.lastIndexOf(".")+1).startsWith(cd.getCID().substring(cd.getCID().lastIndexOf(".")+1)))
                return cd.getDisplayName();
        }
        return "";
    }

    private String matchRegisteredCompTypeDispName(String displayName){
        for(IComponentDescriptor cd : WorkbenchData.registeredPluginsComponentDescriptors){
            if(displayName.startsWith(cd.getDisplayName()))
                return cd.getDisplayName();
        }
        for(IComponentDescriptor cd : WorkbenchData.registeredViewersComponentDescriptors){
            if(displayName.startsWith(cd.getDisplayName()))
                return cd.getDisplayName();
        }
        return "";
    }

    /**
     * @param ins
     * @return
     * @throws FactoryConfigurationError
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    private static Element getXMLTree(InputStream ins) throws FactoryConfigurationError, ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        DocumentBuilder dbuilder = factory.newDocumentBuilder();
        Document doc = dbuilder.parse(ins);
        Element rt = doc.getDocumentElement();
        rt.normalize();
        return rt;
    }

    /**
     * Will be replaced by the messaginMgr.getProject();
     * @return The project root directory.
     * @deprecated There is no notion of a project at the workbench level, Each
     * qiComponent kmay be associated with a different project.
     */
    public String getProjectRoot(){
        return projectRoot;
    }

    /**
     * Get the number of the components stored in the current saved set
     * @return integer.
     */
    public int getExistingComponentNodeCount(){
        int count = 0;
        if(root != null){
            NodeList children = root.getChildNodes();
            for(int i = 0; i < children.getLength(); i++){
                Node child = children.item(i);
                if(child.getNodeType() == Node.ELEMENT_NODE){
                    String nodeName = child.getNodeName();
                    if(nodeName.equals("component")){
                        count++;
                    }
                }
            }
        }
        return count;
    }

    /**
     * Check to see if the preferred display name is already in use
     * @param name
     * @return true or false.
     */
    public boolean displayNameExist (String name){
        for(String key : currentNodeMap.keySet()){
            if(key.equals(name))
                return true;
        }
        return false;
    }
    /**
     * Get the my instance of MessagingManager
     * @return the instance of MessagingManager.
     *
     */
    public MessagingManager getMessagingManager(){
        return messagingMgr;
    }

    /**
     * Record quitting all qiComponents. Called when quitting the workbench.
     */
    public void logQuitting() {
        //if there is an existing usage log file, send it to the
        //StATS server for processing.
        if (StatsManager.getInstance().isStatsLog()) {
            StatsManager.getInstance().transmitStats(myCID);
        }
		
        //record quitting the qiWorkbench
        String event = StatsManager.getInstance().createStatsEvent(QIWConstants.WORKBENCH_COMP, ClientConstants.QUIT_ACTION);
        StatsManager.getInstance().logStatsEvent(event);

        List<IComponentDescriptor> list = getOpenedComponentList();
        //record usage event, quitting the qiComponent
        if (saveList != null) {
            for (int i=0; i<saveList.size(); i++){
                IComponentDescriptor cd = saveList.get(i);
                event = StatsManager.getInstance().createStatsEvent(cd.getDisplayName(), ClientConstants.QUIT_ACTION);
                StatsManager.getInstance().logStatsEvent(event);
            }
        }
        List<IComponentDescriptor>closedList = getClosedComponentList();
        if (closedList != null) {
            for (int i=0; i<closedList.size(); i++){
                IComponentDescriptor cd = closedList.get(i);
                event = StatsManager.getInstance().createStatsEvent(cd.getDisplayName(), ClientConstants.QUIT_ACTION);
                StatsManager.getInstance().logStatsEvent(event);
            }
        }
    }

	/**
	 * Check to see if a given directory exists in the file system
	 * @param filePath file path to be checked
	 * @return yes, no, or error
	 */
    public String checkFileExist(String filePath){
    	ArrayList params = new ArrayList();
    	params.add(messagingMgr.getLocationPref());
    	params.add(filePath);
   		String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.CHECK_FILE_EXIST_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
   		IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);
   		String temp = "";
   		if(resp != null && !MsgUtils.isResponseAbnormal(resp)){
   			temp = (String)resp.getContent();
   		}else{
   			temp = "error";
   		}
   		return temp;
    }

    
    /**
     * @param args
     */
    public static void main(String[] args) {
        WorkbenchStateManager stateManager = new WorkbenchStateManager();
        // get a CID for the new component instance
        String cid = ComponentUtils.genCID(WorkbenchStateManager.class);
        // use the CID as the name of the thread
        Thread serviceThread = new Thread(stateManager, cid);
        serviceThread.start();
        long threadId = serviceThread.getId();
        logger.info("State Manager Thread-"+Long.toString(threadId)+" started");

        // When the services's init() is finished, it will release the lock
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();    }
    
}
