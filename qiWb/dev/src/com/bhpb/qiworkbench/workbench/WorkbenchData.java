/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.workbench;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JDesktopPane;

import com.bhpb.qiworkbench.api.IComponentDescriptor;

/**
 * WorkbenchData contains constants and shared data used by all classes.
 * @author Bob Miller
 * @version 1.0
 *
 */
public class WorkbenchData {

  /**
  ** Prevent object construction outside of this class.
  */
  private WorkbenchData(){}

  // Items below are populated in Workbenchmanager init with lock set

  // WorkbenchManager CID, passed to GUI in constructor
  public static String wbMgrCID = "";
  // WorkbenchManager GUI CID
  public static String wbMgrGUICID = "";

  // number of each type of component - used to populate initial tree and menus
  public static int nAvailablePlugins;
  public static int nAvailableViewers;
  public static int nLocalServices;
  public static int nRemoteServices;
  /** List of the server services (descriptors); obtained from the Servlet Dispatcher. */
  public static ArrayList<IComponentDescriptor> remoteServicesComponentDescriptors = null;
  /** List of the client services (descriptors); obtained from the Message Dispatcher. */
  public static ArrayList<IComponentDescriptor> localServicesComponentDescriptors = null;
  /** List of registered plugins (descriptors); obtained from the Servlet Dispatcher. */
  public static ArrayList<IComponentDescriptor> registeredPluginsComponentDescriptors = null;
  /** List of pre-registered viewer agents (descriptors); obtained from the Servlet Dispatcher. */
  public static ArrayList<IComponentDescriptor> registeredViewersComponentDescriptors = null;

  /** Active and saved components (plugins, viewers, client services. Keyed on CID. */
  public static HashMap<String, IComponentDescriptor> activePlugins = new HashMap<String, IComponentDescriptor>();
  public static HashMap<String, IComponentDescriptor> savedPlugins = new HashMap<String, IComponentDescriptor>();
  public static HashMap<String, IComponentDescriptor> activeViewers = new HashMap<String, IComponentDescriptor>();
  public static HashMap<String, IComponentDescriptor> savedViewers = new HashMap<String, IComponentDescriptor>();
  public static HashMap<String, IComponentDescriptor> activeServices = new HashMap<String, IComponentDescriptor>();

  // dataset names and corresponding seismic directories
  public static HashMap<String,String> seismicDir = new HashMap<String,String>();
  // dataset names and corresponding pathlist directories, i.e. "int_datasets", "datasets", etc
  public static HashMap<String,String> pathListDir = new HashMap<String,String>();
  //dataset names and corresponding number of header keys
  public static HashMap<String,String> numberOfKeys = new HashMap<String,String>();
  //dataset names and corresponding properties for model data, or horizons for event data, or null for seismic data
  public static HashMap<String,String> properties = new HashMap<String,String>();
  //dataset names and corresponding property/horizon min/max values
  public static HashMap<String,String> propertyMinMax = new HashMap<String,String>();

  // arraylist of menu descriptors
  public static ArrayList<WorkbenchMenuDescriptor> menuDescriptors;
  // arraylist of tree descriptors
  public static ArrayList<WorkbenchTreeDescriptor> treeDescriptors;
  public static int DEFAULT_WORKBENCH_WIDTH = 1200;
  public static int DEFAULT_WORKBENCH_HEIGHT = 800;
  public static int DEFAULT_WORKBENCH_LOCATION_X = 100;
  public static int DEFAULT_WORKBENCH_LOCATION_Y = 100;  
  // information about current project
  public static WorkbenchGUI gui;
  public static JDesktopPane desktop;
  // Tree node types
  public static final String ROOT_NODE_TYPE = "rootNodeType";
  public static final String PLUGIN_NODE_TYPE = "pluginNodeType";
  public static final String VIEWER_NODE_TYPE = "viewerNodeType";
  public static final String PLUGIN_INSTANCE_NODE_TYPE = "pluginInstanceNodeType";
  public static final String VIEWER_INSTANCE_NODE_TYPE = "viewerInstanceNodeType";
  public static final String ACTIVE_SERVICE_NODE_TYPE = "activeServiceNodeType";
  public static final String SUSPENDED_SERVICE_NODE_TYPE = "suspendedServiceNodeType";
  // component types
  public static final String PLUGIN_COMPONENT_TYPE = "pluginComponentType";
  public static final String VIEWER_COMPONENT_TYPE = "viewerComponentType";
  public static final int newProject = 1;     // flag for new project
  public static final int restoreProject = 0; // flag to restore project
  public static int projectType;              // new or restore
  public static String displayName;           // displayName of currently selected tree node
  public static String dataTypeAndOrder;      // "Seismic Cross-Sections", "Seismic Maps", "Event Cross-Sections", "Event Maps",
                                              // "Model Cross-Sections", or "Model Maps"

  public static final int verbose = 1;           // 0=silent, 1=some, 2=all
  // scanData script used at startup to get dataset info
  public static final String[] scanDataScript = {
    "#!/bin/sh ",
    "project=$1 ",
    "for dir in [ ${project}/int_datasets ${project}/datasets ] ",
    "do ",
    "  if [ -d ${dir} ] ",
    "  then ",
    "    dat=`ls -1 ${dir}/*.dat` ",
    "    for file in ${dat} ",
    "    do ",
    "      seismicDir=`head -1 ${file}` ",
    "      fileName=`basename ${file} .dat` ",
    "      pathListDir=`dirname ${file}` ",
    "      if [ -f ${seismicDir}/${fileName}_0000.HDR ] ",
    "      then ",
    "        item1=\"seismicDir = \"${seismicDir} ",
    "        item2=\"fileName = \"${fileName} ",
    "        item3=`grep 'TYPE =' ${seismicDir}/${fileName}_0000.HDR` ",
    "        item4=`grep 'ORDER =' ${seismicDir}/${fileName}_0000.HDR` ",
    "        item5=\"pathListDir = \"${pathListDir} ",
    "        item6=`grep 'NKEYS =' ${seismicDir}/${fileName}_0000.HDR` ",
    "        item7=`grep 'PROPERTY =' ${seismicDir}/${fileName}_0000.HDR` ",
    "        item8=`grep 'HORIZON =' ${seismicDir}/${fileName}_0000.HDR` ",
    "        item9=`grep 'PROP_MIN =' ${seismicDir}/${fileName}_0000.HDR` ",
    "        item10=`grep 'PROP_MAX =' ${seismicDir}/${fileName}_0000.HDR` ",
    "        record=\"${item1} ${item2} ${item3} ${item4} ${item5} ${item6} ${item7} ${item8} ${item9} ${item10}\" ",
    "        echo ${record} ",
    "      fi ",
    "    done ",
    "  fi ",
    "done "
  };
}
