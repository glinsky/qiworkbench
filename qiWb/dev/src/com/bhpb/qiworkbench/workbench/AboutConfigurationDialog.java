/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.workbench;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.util.Date;
import java.util.Locale;
import java.util.ArrayList;

import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
import com.bhpb.qiworkbench.QiwbPreferences;
import com.bhpb.qiworkbench.client.util.PreferenceUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.QiwIOException;

/**
 * Dialog to display configuration details accessed from AboutDialog.java
 *
 * @author  Marcus Vaal
 * @version 1.0
 */
public class AboutConfigurationDialog extends javax.swing.JDialog {

    /**
     * Creates an instance of a Configuration Dialog
     */
    public AboutConfigurationDialog() {
        initComponents();
    }

    /**
     * Initializes the components
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        configurationPanel = new javax.swing.JPanel();
        configurationScrollPane = new javax.swing.JScrollPane();
        configurationTextArea = new javax.swing.JTextArea();
        buttonPanel = new javax.swing.JPanel();
        viewErrorLogButton = new javax.swing.JButton();
        copyToClipboardButton = new javax.swing.JButton();
        closeButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        configurationTextArea.setColumns(20);
        configurationTextArea.setEditable(false);
        configurationTextArea.setFont(new java.awt.Font("Courier New", 0, 12));
        configurationTextArea.setRows(5);

        String tomcatServer = MessageDispatcher.getInstance().getTomcatURL();
    String userHOME = MessageDispatcher.getInstance().getUserHOME();
    String preferenceFile = "";
    // NOTE: When the preferenceFile is created, the workbench stores it in the home directory.
    // If the preference file is not in the home directory, it assumes there is no preference file.
    String prefFileDir = userHOME + File.separator + QIWConstants.QIWB_WORKING_DIR;
    if(PreferenceUtils.prefFileExists(prefFileDir)) {
        preferenceFile = prefFileDir;
    }
    else {
        preferenceFile = "No Preference File.";
    }

    // TODO locate and add in qiComponent directory
        configurationTextArea.setText(
            "*** Date: " + new Date() + "\n\n" +
            "*** System Properties: \n" +
            "Operating System: " + System.getProperty("os.name") + "\n" +
            "Java: " + System.getProperty("java.version") + "\n" +
            "VM: " + System.getProperty("java.vm.name") + " " + System.getProperty("java.vm.version") + "\n" +
            "Java Home: " + System.getProperty("java.home") + "\n" +
            "System Locale: " + Locale.getDefault() + "\n\n" +
            "*** Directories:" + "\n" +
            "Home dir: " + System.getProperty("user.home") + "\n" +
            //"Component Dir: " + "\n" +
            "Preference File: " + preferenceFile + "\n" +
            "Project Dir: " + WorkbenchManager.getInstance().getProject() + "\n" +
            "Tomcat: " + tomcatServer
        );

        configurationScrollPane.setViewportView(configurationTextArea);

        org.jdesktop.layout.GroupLayout configurationPanelLayout = new org.jdesktop.layout.GroupLayout(configurationPanel);
        configurationPanel.setLayout(configurationPanelLayout);
        configurationPanelLayout.setHorizontalGroup(
            configurationPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(configurationScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE)
        );
        configurationPanelLayout.setVerticalGroup(
            configurationPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(configurationScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
        );

        viewErrorLogButton.setText("View Error Log");
        viewErrorLogButton.setEnabled(false);

        copyToClipboardButton.setText("Copy to Clipboard");
        copyToClipboardButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyToClipboardButtonActionPerformed(evt);
            }
        });

        closeButton.setText("Close");
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout buttonPanelLayout = new org.jdesktop.layout.GroupLayout(buttonPanel);
        buttonPanel.setLayout(buttonPanelLayout);
        buttonPanelLayout.setHorizontalGroup(
            buttonPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(buttonPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(viewErrorLogButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(copyToClipboardButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 239, Short.MAX_VALUE)
                .add(closeButton)
                .addContainerGap())
        );
        buttonPanelLayout.setVerticalGroup(
            buttonPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, buttonPanelLayout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(buttonPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(viewErrorLogButton)
                    .add(copyToClipboardButton)
                    .add(closeButton))
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(configurationPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(buttonPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(configurationPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(buttonPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Copies the configuration details to the clipboard
     * @param evt event when 'Copy To Clipboard' button is pressed
     */
    private void copyToClipboardButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyToClipboardButtonActionPerformed
// TODO add your handling code here:
        StringSelection configCopy = new StringSelection(configurationTextArea.getText());
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(configCopy, null);
    }//GEN-LAST:event_copyToClipboardButtonActionPerformed

    /**
     * Closes the Configuration Dialog Window
     * @param evt event when 'Close' button is pressed
     */
    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
// TODO add your handling code here:
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_closeButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton closeButton;
    private javax.swing.JPanel configurationPanel;
    private javax.swing.JScrollPane configurationScrollPane;
    private javax.swing.JTextArea configurationTextArea;
    private javax.swing.JButton copyToClipboardButton;
    private javax.swing.JButton viewErrorLogButton;
    // End of variables declaration//GEN-END:variables

}
