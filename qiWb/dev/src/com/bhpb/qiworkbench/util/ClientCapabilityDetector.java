/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2007-2008  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench.util;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;

/**
 * Execute a system-specific command to determine the maximum amount of available physical memory
 * and then launch the 
 */
public class ClientCapabilityDetector {

    public static enum PLATFORM {

        WINDOWS, MAC, LINUX, UNKNOWN
    };
    public static final int DEFAULT_DELAY = 100; // wait 100ms between checking to see if commandLineJob has new lines available

    public static final int DEFAULT_ITERATIONS = 100; // wait a maximum of 100 times (10 seconds total)

    public static final String[] LINUX_CMD = {"top", "-b", "-n", "1"};
    public static final String[] WINDOWS_CMD = {"systeminfo"};
    public static final String[] MAC_CMD = {"sysctl", "hw.memsize"};
    public static final String DEFAULT_MAX_HEAP_SIZE = "512m";
    //This detector may fail if the max available/physical ram in KB or MB 
    //cannot be represented by a long, for some platform(s)
    //As this is up to 9,223,372,036,854,775,807 Megabytes (Windows), I doubt this issue will arise any time soon.  
    private static final long MAX_32_BIT_HEAP = 2147483648L;
    private static final long MAX_32_BIT_HEAP_IN_KB = 2097152;
    private static final long MAX_32_BIT_HEAP_IN_MB = 2048;
    private static final long WINDOWS_1GB_HEAP_LIMIT = 1024;
    private static final int CONSOLE_DISPLAY_TIME = 10000;

    /**
     *  Appends the system property indicated by paramName to the url StringBuffer
     *  in the format "&paramName=System.getProperty(paramname)" if the 
     *  property is set, otherwise, the StringBuffer is not modified.
     *
     *  The value of the 'paramName' property is escaped by the escapeURIcomponent method.
     */
    private static void addParam(StringBuffer url, String paramName) {
        String paramValue = System.getProperty(paramName);

        if (paramValue == null || "".equals(paramValue)) {
            return;
        } else {
            url.append("&" + paramName + "=" + escapeURIcomponent(paramValue));
        }
    }

    private static String escapeURIcomponent(String comp) {
        return comp.replaceAll(":", "%3A");
    }

    /**
     * Based on the OS, return maximum available heap size, converted to a format usable by the Java Webstart JNLP attribute 'max-heap-size'
     * @param osName is one of "Linux", "Windows XP" or "Darwin"
     * @param physicalMemoryAmount is the unformatted string parsed from the os-specific command line job output
     *
     * @return the max heap size or the empty string if inputMemResult was null, empty or invalid
     */
    protected static String getHeapStringInJVMformat(String physicalMemoryAmountString, PLATFORM platform) {

        if (physicalMemoryAmountString == null) {
            throw new IllegalArgumentException("Cannot format null or empty physical amount string for JVM argument -Xmx use");
        }

        if ("".equals(physicalMemoryAmountString)) {
            System.out.println("Physical memory size parsed from system-specific command was the empty string, qiWorkbench will launch with the default maxheapsize=" + DEFAULT_MAX_HEAP_SIZE);
            return DEFAULT_MAX_HEAP_SIZE;
        }

        switch (platform) {
            case LINUX:
                long physicalMemoryAmountInKB = new Long(physicalMemoryAmountString.trim()).longValue();
                if (physicalMemoryAmountInKB > MAX_32_BIT_HEAP_IN_KB) {
                    physicalMemoryAmountInKB = MAX_32_BIT_HEAP_IN_KB;
                }
                return new Long(physicalMemoryAmountInKB).toString() + "k";
            case WINDOWS:
                long physicalMemoryAmountInMB = new Long(physicalMemoryAmountString.trim()).longValue();

                if (physicalMemoryAmountInMB > MAX_32_BIT_HEAP_IN_MB) {
                    physicalMemoryAmountInMB = MAX_32_BIT_HEAP_IN_MB;
                }

                //fix for failure to init JVM on some Windows XP machines with 2.5 GB ram, if
                //detected limit is > WINDOWS_1GB_HEAP_LIMIT, clamp it
                if (physicalMemoryAmountInMB > WINDOWS_1GB_HEAP_LIMIT) {
                    physicalMemoryAmountInMB = WINDOWS_1GB_HEAP_LIMIT;
                }

                if (physicalMemoryAmountInMB < 512) {
                    physicalMemoryAmountInMB = 512;
                }
                return new Long(physicalMemoryAmountInMB).toString() + "m";
            case MAC:
                long physicalMemoryAmount = new Long(physicalMemoryAmountString.trim()).longValue();
                if (physicalMemoryAmount > MAX_32_BIT_HEAP) {
                    physicalMemoryAmount = MAX_32_BIT_HEAP;
                }
                return new Long(physicalMemoryAmount).toString();
            case UNKNOWN:
                System.out.println("PLATFORM is " + PLATFORM.UNKNOWN + ", using default maxheapsize=" + DEFAULT_MAX_HEAP_SIZE);
                return "";
            default:
                throw new RuntimeException("Unable to format Heap string for invalid platform: " + platform + ". Perhaps a new value has been added to enumerated type PLATFORM.");
        }
    }

    /**
     * Returns the max heap size for operatingSystemName in the format "&maxheapsize=<valid Java Heap Size>"
     * See Java Webstart JNLP documentation for more information.
     */
    private static String getMaxHeapForOS() {
        String memResult = "1024m";

        try {
            String javaOsNameProperty = System.getProperty("os.name");
            PLATFORM platform = getPlatformByOsName(javaOsNameProperty);
            CommandLineJob commandLineJob = null;
            switch (platform) {
                case WINDOWS:
                    commandLineJob = new CommandLineJobImpl(WINDOWS_CMD);
                    memResult = parsePhysicalMemoryFromStdout(commandLineJob, platform);
                    break;
                case LINUX:
                    commandLineJob = new CommandLineJobImpl(LINUX_CMD);
                    memResult = parsePhysicalMemoryFromStdout(commandLineJob, platform);
                    break;
                case MAC:
                    commandLineJob = new CommandLineJobImpl(MAC_CMD);
                    memResult = parsePhysicalMemoryFromStdout(commandLineJob, platform);
                    break;
                case UNKNOWN:
                    System.out.println("Unknown OS type detected, using default max heap size: 1024m");
                    break;
                default:
                    //This should never happen because PLATFORM.UNKNOWN is the default case for getPlatformByOsName
                    throw new RuntimeException("Unable to determine how to get heap for platform: " + platform + ". Perhaps a new value has been added to enumerated type PLATFORM");
            }

            memResult = getHeapStringInJVMformat(memResult, platform);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("Exception caught: " + e.getMessage());
        }

        if (memResult != null) {
            return "&maxheapsize=" + memResult;
        } else {
            return "";
        }
    }

    /**
     * Return the max-heap-size for the client Operating System
     * For Windows XP or Linux, this is the lesser of 2 GB or the physical RAM
     * If the Operating System is not one of these, the max-heap-size is 1024 MB
     */
    private static String getMaxHeapSize() {
        try {
            System.out.println("Calculating max heap size...");
            return getMaxHeapForOS();
        } catch (Exception e) {
            System.out.println("Caught Exception: " + e);
            return "";
        }
    }
    /*
     * Given a particular OS, one of
     * Windows XP
     * Windows Vista
     * Mac OSX or Mac OS X
     * Linux
     *
     * return the overall platform to which it belongs
     * Windows
     * Mac
     * Linux
     *
     * @ param osName a valid string representation of a particular os
     * @ return PLATFORM representing the overall type of the operating system (LINUX, MAC or WINDOWS)
     *
     * @ throws IllegalArgumentException if osName is null
     */
    protected static PLATFORM getPlatformByOsName(String osName) {
        if (osName == null) {
            throw new IllegalArgumentException("OS name may not be null");
        }
        if ("Linux".equalsIgnoreCase(osName)) {
            return PLATFORM.LINUX;
        } else if (("Windows XP").equalsIgnoreCase(osName)) {
            return PLATFORM.WINDOWS;
        } else if (("Windows Vista").equalsIgnoreCase(osName)) {
            return PLATFORM.WINDOWS;
        //NOTE: "Mac OSX" is for Tiger, "Mac OS X" is for Leopard
        } else if (("Mac OSX").equalsIgnoreCase(osName) || ("Mac OS X").equalsIgnoreCase(osName)) {
            return PLATFORM.MAC;
        } else {
            return PLATFORM.UNKNOWN;
        }
    }
    
    /**
     * Return the URL for the qiWorkbench jnlp, minus the value for maxheapsize.
     */
    private static String getURL() {
        //this is passed in as a system property from the invoker JNLP
        StringBuffer url = new StringBuffer(System.getProperty("codebaseBuffer") + "qiw_httpinvoker_jnlp.jsp");

        // Even if this jsp was invoked without an install parameter and hence the install property is not set,
        // the jsp called by this application requires it, so set it if available, otherwise automatic updates (install == true)
        // will be disabled in the workbench.
        String installFlag = System.getProperty("install");
        if ((installFlag != null) && installFlag.equalsIgnoreCase("true")) {
            installFlag = "true";
        } else {
            installFlag = "false";
        }
        url.append("?install=" + installFlag);

        //now pass along the runtimeURL, project and session parameters as URL params if set
        addParam(url, "runtimeURL");

        addParam(url, "serverLoc");

        addParam(url, "project");

        addParam(url, "session");

        System.out.println("url: " + url.toString());

        return url.toString();
    }

    public static void main(String[] args) {
        System.out.println("Initializing ClientCapabilityDetector...");

        String[] processCommand = {"javaws", "-wait", getURL() + getMaxHeapSize()};

        CommandLineJob commandLineJob = new CommandLineJobImpl(processCommand);
        System.out.println("Executing command: " + commandLineJob.getCommandAndArguments());
        System.out.println("Waiting for command to complete... ");
        try {
            commandLineJob.start();
        } catch (Exception ex) {
            System.out.println("Caught: " + ex.getMessage());
            ex.printStackTrace();
        }

        System.out.println("Waiting for javaws process to terminate...");
        System.out.println(commandLineJob.getProcInfo());
        System.out.println("Closing in " + CONSOLE_DISPLAY_TIME / 1000 + " seconds...");

        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < CONSOLE_DISPLAY_TIME) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ie) {
                System.out.println("Caught: " + ie.getMessage());
            }
        }

        System.exit(0);
    }
    
    /**
     * Run the commandLineJob and parse the available physical memory from the stdout stream.
     *
     * @param commandLineJob CommandLineJob configured to run a command to determine physical memory size.
     * @param platform Enumerated type grouping a known OS into one of three general categories to aid in parsing the commandLineJob output.
     * @throws IllegalArgumentException if commandLineJob is null or platform is not one of LINUX, WINDOWS, MAC or UNKNOWN.
     *
     * @return The amount of available physical memory in megabytes or the empty string if the commandLineJob
     * does not produce parsable output for the given platform.
     */
    protected static String parsePhysicalMemoryFromStdout(CommandLineJob commandLineJob, PLATFORM platform) throws IOException {
        final double MAX_PORTION_OF_PHYSICAL_RAM = 0.90;

        if (commandLineJob == null) {
            throw new IllegalArgumentException("Cannot run null CommandLineJob");
        }
        String memResult = null;

        System.out.println("Executing command: " + commandLineJob.getCommandAndArguments());

        commandLineJob.start();
        try {
            if (commandLineJob.waitFor(DEFAULT_DELAY, DEFAULT_ITERATIONS) == false) {
                System.out.println("Warning: command-line process failed to start after 10 seconds - aborting.");
                return "";
            }
        } catch (InterruptedException ie) {
            System.out.println("Interrupted while waiting for command-line process to complete.  Sufficient output for parsing may not be available.");
        }

        String[] lines = null;
        String line = null;
        int parsedInt = 0;
        boolean parsableDataAvailable = false;

        switch (platform) {
            case LINUX:
                String LinuxRegex = "^Mem:";
                lines = commandLineJob.getLinesMatching(CommandLineJob.STREAM.STDOUT, LinuxRegex);
                parsableDataAvailable = lines.length > 0;
                if (parsableDataAvailable) {
                    line = lines[0];
                    memResult = line.substring(6, 13);
                    try {
                        if (memResult == null) {
                            System.out.println("Command output line #8 was null, using default max heap size");
                            memResult = "";
                        } else if ("".equals(memResult)) {
                            System.out.println("Command output line #8 was empty, using default max heap size.");
                        } else {
                            parsedInt = new Integer(memResult.trim()).intValue();
                            memResult = new Integer(new Double(MAX_PORTION_OF_PHYSICAL_RAM * parsedInt).intValue()).toString();
                        }
                    } catch (NumberFormatException nfe) {
                        System.out.println("Unable to parse integer value from indices 6 to 13 of line: " + line);
                        System.out.println("NumberFormatException: " + nfe.getMessage());
                        System.out.println("Using default max heap size.");
                        memResult = "";
                    }
                } else {
                    System.out.println("Warning: Insufficient data available for parsing memory capability - fewer than 9 lines of output from system command");
                }
                break;
            case WINDOWS:
                String WindowsRegex = "^Available Physical Memory:";
                lines = commandLineJob.getLinesMatching(CommandLineJob.STREAM.STDOUT, WindowsRegex);
                parsableDataAvailable = lines.length > 0;
                if (parsableDataAvailable) {
                    line = lines[0];

                    //TODO add accessor to get the actual length of the match, rather than relying on it being 1 less than the length of the regex
                    //as in this special case
                    memResult = line.substring(WindowsRegex.length() - 1, line.length() - 3);
                    memResult = memResult.trim();
                    NumberFormat nfILocal = NumberFormat.getIntegerInstance();
                    try {
                        Number n = nfILocal.parse(memResult);
                        memResult = new Integer(new Double(MAX_PORTION_OF_PHYSICAL_RAM * n.intValue()).intValue()).toString(); // strip locale-dependent formatting (e.g. commas) and return size in mb
                    } catch (ParseException pe) {
                        memResult = "";
                    }
                } else {
                    System.out.println("Warning: Insufficient data available for parsing memory capability - no line of output from system command starts with 'Available Physical Memory: '");
                }
                break;
            case MAC:
                lines = commandLineJob.getLinesMatching(CommandLineJob.STREAM.STDOUT, "^hw.memsize");
                parsableDataAvailable = lines.length > 0;
                if (parsableDataAvailable) {
                    System.out.println("Parsing line: " + lines[0]);
                    String[] tokens = lines[0].split(":");
                    if (tokens.length < 2) {
                        System.out.println("Line does not contain at least 2 tokens delimited by space, unable to parse memory size.");
                        memResult = "";
                    } else {
                        try {
                            long memResultInt = Long.parseLong(tokens[1].trim());
                            System.out.println("Physical memory size in bytes: " + memResultInt);
                            memResult = new Long(new Double(MAX_PORTION_OF_PHYSICAL_RAM * memResultInt).longValue()).toString();
                        } catch (Exception ex) {
                            System.out.println("Caught exception while parsing long from: " + tokens[1] + ": " +ex.getMessage());
                        }
                    }
                } else {
                    System.out.println("Warning: Insufficient data available for parsing memory capability - no line of output from system command starts with 'PhysMem:'");
                }
                break;
            case UNKNOWN:
                System.out.println("Warning: Cannot execute command-line process for unknown OS - JVM default max heap size will be used.");
                memResult = "";
                break;
            default:
                throw new RuntimeException("Unable to parse Physical Memory string from stdout for invalid platform: " + platform + ". Perhaps a new value has been added to enumerated type PLATFORM.");
        }

        return memResult != null ? memResult : "";
    }
}