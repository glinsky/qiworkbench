/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench.util;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringValidator {

    public enum TYPE {

        DOUBLE, INTEGER, FILENAME
    };

    public enum CONSTRAINT {

        POSITIVE
    };
    private static final String INVALID_FLOATING_POINT_NUMBER = "not a valid floating-point number";
    private static final String INVALID_INTEGER = "not a valid integer";
    private static final String NOT_A_POSITIVE_NUMBER = "not a positive number";
    private static final String INVALID_QIWB_FILENAME = "not a valid qiWb filename";
    private static final String VALID_FLOATING_POINT_NUMBER = "a valid floating-point number";
    private static final String VALID_QIWB_FILENAME = "a valid qiWb filename";
    //QIWB filename regex goes here
    private static final String INVALID_FILENAME_REGEX = "[a-zA-Z0-9_\\s-.$]*";
    private static final String NO_CONSTRAINTS_ALLOWED = "not permitted to be constrained";

    public static ValidatorResult validate(String value, TYPE type, CONSTRAINT... constraints) {
        switch (type) {
            case INTEGER:
                return isValidInteger(value, constraints);
            case DOUBLE:
                return isValidDouble(value, constraints);
            case FILENAME:
                return isValidFileName(value, constraints);
            default:
                throw new IllegalArgumentException("Cannot valid unknown type: " + type);
        }
    }

    public static ValidatorResult validate(NumberFormat format, String value, TYPE type, CONSTRAINT... constraints) {
        switch (type) {
            case INTEGER:
                return isValidInteger(value, constraints);
            case DOUBLE:
                return isValidDouble(format, value, constraints);
            case FILENAME:
                return isValidFileName(value, constraints);
            default:
                throw new IllegalArgumentException("Cannot valid unknown type: " + type);
        }
    }

    private static ValidatorResult isValidFileName(String someString, CONSTRAINT... constraints) {
        //if constraints are specified, an invalid result is created
        for (CONSTRAINT constraint : constraints) {
            switch (constraint) {
                default:
                    return ValidatorResult.createInvalidResult(someString, TYPE.FILENAME.toString(), NO_CONSTRAINTS_ALLOWED);
            }
        }
        //if the string contains a match for the invalid filename regex, an invalid result is returned
        Pattern pattern = Pattern.compile(INVALID_FILENAME_REGEX);
        Matcher matcher = pattern.matcher("someString");
        if (!matcher.matches()) {
            return ValidatorResult.createInvalidResult(someString, TYPE.FILENAME.toString(), INVALID_QIWB_FILENAME);
        } else {
            //valid result is only returned if no constraints are specified and the string does not 
            //contain a match for the invalid filename regex
            return ValidatorResult.createValidResult(someString, VALID_QIWB_FILENAME);
        }
    }

    private static ValidatorResult isValidDouble(String doubleString, CONSTRAINT... constraints) {
        try {
            double doubleVal = Double.valueOf(doubleString);
            return checkConstraints(doubleVal, constraints);
        } catch (NumberFormatException nfe) {
            return ValidatorResult.createInvalidResult(doubleString, TYPE.DOUBLE.toString(), INVALID_FLOATING_POINT_NUMBER);
        }
    }

    private static ValidatorResult isValidDouble(NumberFormat format, String doubleString, CONSTRAINT... constraints) {
        try {
            double doubleVal = format.parse(doubleString).doubleValue();
            return checkConstraints(doubleVal, constraints);
        } catch (NumberFormatException nfe) {
            return ValidatorResult.createInvalidResult(doubleString, TYPE.DOUBLE.toString(), INVALID_FLOATING_POINT_NUMBER);
        } catch (ParseException pe) {
            return ValidatorResult.createInvalidResult(doubleString, TYPE.DOUBLE.toString(), pe.getMessage());
        }
    }

    private static ValidatorResult isValidInteger(String intString, CONSTRAINT... constraints) {
        try {
            int intVal = Integer.valueOf(intString);
            return checkConstraints(intVal, constraints);
        } catch (NumberFormatException nfe) {
            return ValidatorResult.createInvalidResult(intString, TYPE.INTEGER.toString(), INVALID_INTEGER);
        }
    }

    private static ValidatorResult checkConstraints(double value, CONSTRAINT... constraints) {
        for (CONSTRAINT constraint : constraints) {
            switch (constraint) {
                case POSITIVE:
                    if (value <= 0.0) {
                        return ValidatorResult.createInvalidResult(
                                Double.toString(value),
                                TYPE.DOUBLE.toString(),
                                NOT_A_POSITIVE_NUMBER);
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Invalid constraint for floating-point number: " + constraint);
            }
        }
        return ValidatorResult.createValidResult(Double.toString(value), VALID_FLOATING_POINT_NUMBER);
    }

    private static ValidatorResult checkConstraints(int value, CONSTRAINT... constraints) {
        for (CONSTRAINT constraint : constraints) {
            switch (constraint) {
                case POSITIVE:
                    if (value <= 0) {
                        return ValidatorResult.createInvalidResult(
                                Integer.toString(value),
                                TYPE.INTEGER.toString(),
                                NOT_A_POSITIVE_NUMBER);
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Invalid constraint for integer: " + constraint);
            }
        }
        return ValidatorResult.createValidResult(Double.toString(value), VALID_FLOATING_POINT_NUMBER);
    }
}