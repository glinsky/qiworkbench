/*
 * PropertyUtils.java
 *
 * Created on October 1, 2007, 1:30 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.qiworkbench.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Logger;

/**
 *
 * @author folsw9
 */
public class PropertyUtils {
    private static Logger logger = Logger.getLogger(PropertyUtils.class.getName());
    private static final String DEFAULT_VERSION_PROPERTIES_FILENAME = "/version.properties";
    
    /** Creates a new instance of PropertyUtils */
    public PropertyUtils() {
    }
 
    /**
     * Load Properties from resource "/version.properties".
     *
     * @return Properties loaded from fileName
     *
     * @throws IOException if fileName does not exist or cannot be opened.
     */
    public static Properties loadProperties() throws IOException {
        //throw new UnsupportedOperationException("Something bad happen!!!!!");
        return loadPropertiesFromFile(DEFAULT_VERSION_PROPERTIES_FILENAME);
    }
    
    /**
     * Load Properties from a flat file.
     *
     * @param fileName String representing the file from which to import properties.
     * @return Properties loaded from fileName
     *
     * @throws IOException if fileName does not exist or cannot be opened.
     */
    public static Properties loadPropertiesFromFile(String fileName) throws IOException {
        Properties props = new Properties();
        
        //This method had a bug which is replicated elsewhere in the qiWb as well.
        //While it handles an IOException, it never checks for null InputStream, i.e.,
        //whether the file actually exists.  Because of this, it threw a NPE and
        //does not generate a useful error message.
        if (fileName == null) {
            String errorMsg = "Cannot load properties from null fileName";
            logger.warning(errorMsg);
            throw new IllegalArgumentException(errorMsg);
        } else {
            logger.fine("Attempting to load non-nullproperties file: " + fileName + " as a resource.");
            
            InputStream in = PropertyUtils.class.getResourceAsStream(fileName);
            if (in == null) {
                String errorMsg = "Cannot open file for reading because it does not exist: " + fileName;
                logger.warning(errorMsg);
                logger.warning("Valid resources:");
                
                Enumeration<URL> resourceEnum = PropertyUtils.class.getClassLoader().getResources(fileName);
                while(resourceEnum.hasMoreElements()) {
                    URL resourceURL = resourceEnum.nextElement();
                    logger.warning(resourceURL.toString());
                }
                throw new IOException(errorMsg);
            } else {
            try {
                logger.fine("InputStream in is NOT null.");
                props.load(in);
            } finally {
                try {
                    if (in != null) in.close();
                } catch (IOException e) {
                    logger.warning("Exception closing properties file: " + fileName);
                }
            }
            }
        }
        return props;
    }
}