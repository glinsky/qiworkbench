/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2007-2008  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiworkbench.util;

/**
 * Formats the exception message, relevant class name and line number as a 1-line String.
 */
public class ExceptionMessageFormatter {
    private ExceptionMessageFormatter() {
    }
 
   /**
    * Formats the exception message, relevant class name and line number as a 1-line String.
    * If the specified Exception is null, a String indicating this error condition is returned.
    * If no class containing the package 'com.bhpb' is found in the stack trace, the first
    * line of the stack trace is used in the formatted message.
    * @param ex Exception for which a formatted message String will be returned
    * @return String representing the relevant Exception information in a concise single-line format
    */
    public static String getFormattedMessage(Exception ex) {
        if (ex == null) {
            return "Unable to format message of null Exception";
        } else {
            return ex.getClass().getName() + ": " + ex.getMessage() + " at " + getRelevantStackTraceElement(ex, "com.bhpb").toString(); 
        }
    }
   
   /**
    * Formats the exception message, relevant class name and line number as a 1-line String.
    * If the specified Exception is null, a String indicating this error condition is returned.
    * If no class containing the package 'com.bhpb' is found in the stack trace, the first
    * line of the stack trace is used in the formatted message.
    * @param ex Exception for which a formatted message String will be returned
    * @return String representing the relevant Exception information in a concise single-line format
    */
    public static String getShortFormattedMessage(Exception ex) {
        if (ex == null) {
            return "Unable to format message of null Exception";
        } else {
            return ex.getMessage() + " at " + getRelevantStackTraceElement(ex, "com.bhpb").toString(); 
        }
    }
    
    /**
     * Finds the StackTraceElement of the exception which contains the specified package name.
     * If no match is found, the first element of the stack trace is returned.
     */
    private static StackTraceElement getRelevantStackTraceElement(Exception ex, String packageNameToMatch) {
        StackTraceElement[] stElements = ex.getStackTrace();
        int elementMatchingPkgName = 0; // if no suitable element can be found, return the 
        for (int i=0; i<stElements.length; i++)
            if  (stElements[i].getClassName().contains(packageNameToMatch)) {
                elementMatchingPkgName = i;
                break;
            }
        return stElements[elementMatchingPkgName];
    }
}