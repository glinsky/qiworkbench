/*
 * StreamToStringListConverter.java
 *
 * Created on September 12, 2007, 12:51 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.qiworkbench.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author folsw9
 */
public class StreamToStringListConverter implements Runnable {
    BufferedReader reader = null;
    List<String> textList = null;
    List<Exception> exceptions = new ArrayList<Exception>();
    
    /** Creates a new instance of StreamToStringListConverter */
    public StreamToStringListConverter(BufferedReader reader, List<String> textList) {
        if (reader == null) throw new IllegalArgumentException("Reader must not be null");
        if (textList == null) throw new IllegalArgumentException("textList must not be null");
        
        this.reader = reader;
        this.textList = textList;
    }
    
    /**
     * @return true if and only if the stream reading process has finished
     */
    public boolean isFinished() {
        return reader == null;
    }
    
    /**
     * Read from the buffered reader until the stream is consumed or n lines are captured.
     * If an IOExceptions are caught while reading from the BufferedReader, they are stored in a List
     * and available via getNumExceptions() and getException(int).
     */
    public void run() {
        String line; int cnt = 0;
        try {
            while ((line = reader.readLine()) != null && cnt<30) {
                synchronized (textList) {
                    textList.add(line);
					cnt++;
                }
            }
        } catch (IOException ioe) {
            exceptions.add(ioe);
        } finally {
            try {
                reader.close();
            } catch (IOException ioe) {
                exceptions.add(ioe);
            }
            reader = null;
        }
    }
    
    public int getNumExceptions() {
        return exceptions.size();
    }
    
    public Exception getException(int index) {
        return exceptions.get(index);
    }
}
