/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Empty the standard output stream or the standard error output stream of a
 * spawned job process.
 * <p>Note: According to the Process JavaDocs, failure to immediately digest
 * these streams may cause the spawned job process to hang or deadlock because
 * of limited buffer size for standard input and output streams. It is also
 * highly suggested these streams be buffered. Using a line Reader is not
 * sufficient.
 *
 * @author Gil Hansen
 * @version 1.0
 */

public class JobStreamDigester extends Thread {
    private static Logger logger = Logger.getLogger(JobStreamDigester.class.getName());

    /** JobStreamDigester's input stream which is the spawned job process's
     * output. */
    private InputStream ins;

    private StringBuffer streamOutput;

    public static final int bufSize = 1024;

    /**
     * Constructor.
     *
     * @param ins JobStreamDigester's input stream which is the spawned job
     * process's output.
     */
     public JobStreamDigester(InputStream ins, StringBuffer streamOutput) {
         this.ins = ins;
         this.streamOutput = streamOutput;
     }

    /**
     * Read standard out or standard error of the spawned job process.
     * Accumulate the buffers by converting each byte vector to a string.
     * When EOF is reach, convert the strings back to byte vectors and pipe
     * to the thread requesting the output.
     *
     * 100ms wait time added by Woody Folsom to reduce cpu usage when no data is ready
     */
     @Override
    public void run() {
        try {
            //Read all of the job process's output, i.e., until read EOF
            BufferedInputStream bis = new BufferedInputStream(ins);
            byte[] buf = new byte[bufSize];
            int bufLen = bis.read(buf, 0, bufSize);
            //Note: will get EOF when job process terminates
            while(bufLen > -1) {
                if (bufLen > 0) { 
                    streamOutput.append(new String(buf, 0, bufLen));
                } else {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ie) {
                        logger.warning("Interrupted while waiting 100ms for JobStreamDigester stream input");
                    }
                }
                bufLen = bis.read(buf, 0, bufSize);
            }
        } catch (IOException ioe) {
            //just send the error message so an abnormal response message can be formed
            streamOutput.setLength(0);
            String errMsg = "IO error: exception reading job's output:"+ioe.getMessage();
            streamOutput.append(errMsg);
            logger.warning(errMsg);
        } finally {
            try {
                if (ins != null) ins.close();
            } catch (IOException e) {
                logger.warning("IOException while closing input stream (stdout/stderr): " + e.getMessage());
            }
        }
    }
}
