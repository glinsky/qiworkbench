/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.messageFramework;

import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Message handler for case when communication is strictly between one producer (Message
 * Dispatcher) and one consumer (agent, plugin, service).
 * <p>
 * Each consumer has its own blocking message queue and methods for managing the queue.
 * <p>
 * The queue is unbounded so there is no wait when a message is added to the queue. If
 * the consumer tries to get an item from an empty queue, it will wait until there is one.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class BlockingMsgHandler implements IMsgHandler {
    private static Logger logger = Logger.getLogger(BlockingMsgHandler.class.getName());

    /** Message queue */
    LinkedBlockingQueue2<IQiWorkbenchMsg> msgQueue = new LinkedBlockingQueue2<IQiWorkbenchMsg>();

    /** Add message to queue. Consumer waiting on an empty queue will wake up.
     * A NULL message is logged as a severe internal error and is ignored.
     * @param msg Message to be queued
     */
    public synchronized void enqueue(IQiWorkbenchMsg msg) {
        if (msg == null) {
            logger.warning("Internal error: enqueuing a null message; ignored");
            return;
        }
        try {
            msgQueue.put(msg);
            logger.finest("msg: "+msg.toString());
        } catch (InterruptedException ie) {
            logger.warning("While putting msg in msgQueue, caught: " + ie.getMessage());
        }
    }

    /** Get the next message sent for processing. Wait for one if queue empty.
     * @return Next message.
     */
    public IQiWorkbenchMsg dequeue() {
        IQiWorkbenchMsg msg = null;
        try {
            // wait if the queue is empty
            msg = msgQueue.take();
            logger.finest("msg: "+msg.toString());
        } catch (InterruptedException ie) {
        }

        return msg;
    }

    /** Check if message queue is empty
     * @return true if empty; otherwise, false
     */
    public boolean isQueueEmpty() {
        return msgQueue.isEmpty();
    }

    /**
     * Reset the queue, i.e., clear it of any elements.
     *
     */
    public void resetQueue() {
        msgQueue.clear();
    }

    public IQiWorkbenchMsg peek(){
        return msgQueue.peek();
    }

    public IQiWorkbenchMsg peek2(){
        return msgQueue.peek2();
    }

    /**
     * Find response that matches specified message ID.
     *
     * @param msgID ID of response message search for.
     * @return Response with matching message ID; null if none.
     */
     public synchronized IQiWorkbenchMsg findResponse(String msgID) {
        IQiWorkbenchMsg msg = msgQueue.peek();

        if(msg != null && msg.getMsgKind().equals(QIWConstants.DATA_MSG) && msg.getMsgID().equals(msgID)) {
            try {
                return msgQueue.take();
            } catch (InterruptedException ie) {
                logger.warning("Caught InterruptedException in findResponse " + ie);
            }
        }

        // Response is not on top of the queue. Search for it.
        Object[] msgs = msgQueue.toArray();
        //logger.info("findResponse: msgQueue:"+msgQueue.toString());
        for (int i=0; i<msgs.length; i++) {
            msg = (IQiWorkbenchMsg)msgs[i];
            if (msg == null) continue;
            if (msg.getMsgKind().equals(QIWConstants.DATA_MSG)) {
                if (msg.getMsgID().equals(msgID)) {
                    //remove message from the queue (last item accessed)
                    boolean removed = msgQueue.remove(msg);
                    return msg;
                }
            }
        }

        return null;
    }

    public IQiWorkbenchMsg findResponseFoo(String msgID) {
      logger.info("find msg with ID="+msgID);
      IQiWorkbenchMsg msg = msgQueue.peek();
      logger.info("peeked msg:"+msg);
      if(msg != null && msg.getMsgKind().equals(QIWConstants.DATA_MSG) && msg.getMsgID().equals(msgID)) {
        try {
          msg = msgQueue.take();
        }
        catch (InterruptedException ie) {
            logger.warning("Caught InterruptedException in findResponse " + ie);
        }
      }
      if(msg != null)
        logger.info("findResponse returning " + msg);
      else
        logger.info("findResponse returning null");
      return msg;
    }

    public void displayMsgQueue(){
       	logger.info("Contents of MsgQueue:");
       	if(msgQueue.isEmpty())
       		return;
       	Object[] msgs = msgQueue.toArray();
           for (int i=0; i<msgs.length; i++) {
           	IQiWorkbenchMsg msg = (IQiWorkbenchMsg)msgs[i];
               if (msg == null) 
               	continue;
              	logger.info(msg.toString());
           }
       }
}
