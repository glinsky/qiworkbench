/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench.messageFramework;

import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.httpclient.params.HttpMethodParams;

import com.thoughtworks.xstream.XStream;

import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.util.ExceptionMessageFormatter;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpURL;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

import com.thoughtworks.xstream.security.AnyTypePermission;       // JE FIELD                             

/**
 * Communication interface between the Message Dispatcher and the Servlet
 * Dispatcher. Uses <a href="http://xstream.codehaus.org">XStream</a> to serialize
 * objects being transmitted to/from XML. Uses
 * <a href="http://jakarta.apache.org/commons/httpclient/">Jakarta Commons HTTPClient</a>
 * to communicate with the Web server via HTTP.
 * <p>
 * A singleton since there is only one and so there is only one dispatcher
 * connector the message dispatcher can communicate with.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class DispatcherConnector {

    public static final int MAX_REDIRECTS = 5;
    private static Logger logger = Logger.getLogger(DispatcherConnector.class.getName());
    private static DispatcherConnector singleton = null;

    private DispatcherConnector() {
    }
    /** URL of the Tomcat server */
    String tomcatURL = "";

    /**
     * Get the singleton instance of this class. If the dispatcher connector doesn't exist,
     * create it.
     */
    public static DispatcherConnector getInstance() {
        if (singleton == null) {
            singleton = new DispatcherConnector();
        }

        return singleton;
    }

    /**
     * Prepares a request message for transmission via HTTP to the Servlet Dispatcher
     * who processes it. Process the returned response, forms a response message and
     * sends it to the producer of the request, i.e., puts the response message
     * on the producer's message queue.  SUBMIT_JOB_CMD requests block until the
     * run-time server responds.  All other message types result in an Abnormal response
     * if the client does not receive a response to the http message post within
     * the default QIWConstants.RESPONSE_TIMEOUT limit (5 seconds).
     *
     * @param requestMsg Message to be processed by the Servlet Dispatcher.
     * @return Message containing results returned by the Servlet Dispatcher;
     * null if message not delivered or an error in its processing.
     */
    public IQiWorkbenchMsg sendRequestMsg(IQiWorkbenchMsg requestMsg) {
        logger.fine("request=" + requestMsg);

        String baseURL = "";

        return sendRequestMsg(requestMsg, baseURL, QIWConstants.RESPONSE_TIMEOUT);
    }

    /**
     * Prepares a request message for transmission via HTTP to the Servlet Dispatcher
     * who processes it. Process the returned response, forms a response message and
     * sends it to the producer of the request, i.e., puts the response message
     * on the producer's message queue.  
     *
     * @param requestMsg Message to be processed by the Servlet Dispatcher.
     * @param timeOut number of ms to wait for response to request message.
     * 
     * @return Message containing results returned by the Servlet Dispatcher;
     * null if message not delivered or an error in its processing.
     * 
     */
    public IQiWorkbenchMsg sendRequestMsg(IQiWorkbenchMsg requestMsg, int timeOut) {
        logger.fine("request=" + requestMsg);

        String baseURL = "";

        return sendRequestMsg(requestMsg, baseURL, timeOut);
    }

    /**
     * Prepares a request message for transmission via HTTP to the Servlet Dispatcher
     * who processes it. Process the returned response, forms a response message and
     * sends it to the producer of the request, i.e., puts the response message
     * on the producer's message queue.
     *
     * @param requestMsg Message to be processed by the Servlet Dispatcher.
     * @param baseURL URL of the servlet dispatcher to consume the request, if not inferred from
     *  or given by the request's command.  This is necessary to register a componentDescriptor
     *  for the deploymentserver, yet retain a message format compatible with qiWorkbench 1.1
     *
     * @return Message containing results returned by the Servlet Dispatcher;
     * null if message not delivered or an error in its processing.
     */
    public IQiWorkbenchMsg sendRequestMsg(IQiWorkbenchMsg requestMsg, String baseURL) {
        return sendRequestMsg(requestMsg, baseURL, QIWConstants.RESPONSE_TIMEOUT);
    }

    /**
     * Prepares a request message for transmission via HTTP to the Servlet Dispatcher
     * who processes it. Process the returned response, forms a response message and
     * sends it to the producer of the request, i.e., puts the response message
     * on the producer's message queue.
     *
     * @param requestMsg Message to be processed by the Servlet Dispatcher.
     * @param baseURL URL of the servlet dispatcher to consume the request, if not inferred from
     *  or given by the request's command.  This is necessary to register a componentDescriptor
     *  for the deploymentserver, yet retain a message format compatible with qiWorkbench 1.1
     * @param timeOut number of ms to wait for response to request message.
     *
     * @return Message containing results returned by the Servlet Dispatcher;
     * null if message not delivered or an error in its processing.
     */
    public synchronized IQiWorkbenchMsg sendRequestMsg(IQiWorkbenchMsg requestMsg, String baseURL, int timeOut) {
        String url = "";

        String cmd = requestMsg.getCommand();
        // Special case: if pinging server command, use the specified URL
        // NOTE: The location of Tomcat must be the VERY FIRST thing determined!
        // Update: Core Manifests (and Jars) are now fetched from the deployServerURL
        // passed in as a Java system property
        // If REGISTER_SELF_CMD, verify that the URL is not "", which is a special
        // case indicating the runtime tomcat server
        if (cmd.equals(QIWConstants.PING_SERVER_CMD) ||
                cmd.equals(QIWConstants.GET_CORE_MANIFESTS_CMD)) {
            baseURL = (String) requestMsg.getContent();
            url = baseURL + "/" + QIWConstants.APP_ROOT + "/ServletDispatcher";
        } else if (cmd.equals(QIWConstants.REGISTER_SELF_CMD) &&
                ("".equals(baseURL) == false)) {
            url = baseURL + "/" + QIWConstants.APP_ROOT + "/ServletDispatcher";
        } else if ((cmd.equals(QIWConstants.RECORD_STATS_CMD) ||
                cmd.equals(QIWConstants.RECORD_MODULES_CMD)) && ("".equals(baseURL) == false)) {
            url = baseURL + "/" + QIWConstants.APP_ROOT + "/ServletDispatcher";
        } else {
            //TODO server down if tomcat URL the empty string
            url = tomcatURL + "/" + QIWConstants.APP_ROOT + "/ServletDispatcher";
        }

        // Check for binary IO commands
        BufferedImage bfImage = null;
        if (cmd.equals(QIWConstants.REMOTE_BINARY_FILE_WRITE_CMD)) {
            ArrayList params = (ArrayList) requestMsg.getContent();
            String fileFormat = (String) params.get(1);

            if (fileFormat.equals(QIWConstants.JPEG_FORMAT)) {
                //remove the binary data; it is not serializable
                bfImage = (BufferedImage) params.remove(2);
            }
        }

        // serialize request message into XML
        XStream xstream = new XStream();

    // JE FIELD added remove all xstream security
    xstream.addPermission(AnyTypePermission.ANY);

        xstream.alias("qiworkbenchMsg", QiWorkbenchMsg.class);
        String serializedMsg = xstream.toXML(requestMsg);
        HttpConnectionManager cm = new MultiThreadedHttpConnectionManager();

        HttpClient client = new HttpClient(); //cm);
        logger.finest("POST URL=" + url);
        PostMethod post = new PostMethod(url);
        // auto-retry 3 times
        post.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
                new DefaultHttpMethodRetryHandler(3, false));

        //set the response timeout
        //fix for socket connection exception when converting ~2gb su file using qiDataConverter w/ remoteIO
        if (requestMsg.getCommand().equals(QIWConstants.SUBMIT_JOB_CMD)) {
            //if this is a server side job, it takes an indefinite amount of time
            post.getParams().setSoTimeout(0);
        } else {
            //otherwise, require that the server return a response in < 5 seconds
            post.getParams().setSoTimeout(timeOut);
        }

        String errorMsg = "";
        IQiWorkbenchMsg responseMsg;

        // send the HTTP message
        if (cmd.equals(QIWConstants.REMOTE_BINARY_FILE_WRITE_CMD) &&
                bfImage != null) {
            ArrayList params = (ArrayList) requestMsg.getContent();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                //Get image and add to file part.
                ImageIO.write(bfImage, "jpeg", baos);
                byte[] image = baos.toByteArray();
                FilePart fp = new FilePart("binaryData", new ByteArrayPartSource((String) params.get(0), image));
                Part[] parts = {new StringPart("requestMsg", serializedMsg), fp};
                post.setRequestEntity(new MultipartRequestEntity(parts, post.getParams()));
            } catch (IOException ioe) {
                errorMsg = "IO exception accessing binary data:" + ioe.getMessage();
                logger.severe(errorMsg);
                responseMsg = MsgUtils.genAbnormalMsg(requestMsg, MsgStatus.SC_EXCEPTION, errorMsg);
                return responseMsg;
            }
        } else {
            NameValuePair[] data = {
                new NameValuePair("requestMsg", serializedMsg)
            };
            post.setRequestBody(data);
        }
        logger.finest("serialized request:" + serializedMsg);

        String xmlResponse = null;

        try {
            //TODO Asynchoronous messages are not supported, i.e., a HTTP request
            //always has a HTTP response. Therefore, components have to poll for
            //service results that take a long time, like a cluster job.
            int statusCode = client.executeMethod(post);

            if (statusCode != HttpStatus.SC_OK) {
                int msgStatus = MsgStatus.SC_EXCEPTION;

                if (statusCode == HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                    //try a second time
                    logger.info("Internal server error, trying post 2nd time");
                    statusCode = client.executeMethod(post);
                } else if (isRedirect(statusCode)) {
                    logger.info("Message redirected due to Http Status: " + statusCode + ", attempting to redirect up to " + MAX_REDIRECTS + " times");

                    //Fix for inability to connect to runtime tomcat server via a redirected URL
                    for (int redirectAttempt = 0; (redirectAttempt < MAX_REDIRECTS) && (isRedirect(statusCode)); redirectAttempt++) {
                        logger.info("Attempting redirect #" + redirectAttempt);
                        Header locationHeader = post.getResponseHeader("location");
                        String redirectLocation = locationHeader.getValue();
                        post.setURI(new HttpURL(redirectLocation));
                        logger.info("Posting request");
                        statusCode = client.executeMethod(post);
                        logger.info("Got status code from post method");
                    }

                    if (statusCode != HttpStatus.SC_OK) {
                        errorMsg = "Maximum number of allowed redirects exceeded - message not routed";
                        logger.severe(errorMsg);
                        logger.info("Returning abnormal message response");
                        responseMsg = MsgUtils.genAbnormalMsg(requestMsg, msgStatus, errorMsg);
                        return responseMsg;
                    }
                } else {
                    errorMsg = "Server error: Request messsage not processed: " + post.getStatusLine();
                    logger.severe(errorMsg);

                    if (statusCode == HttpStatus.SC_REQUEST_TIMEOUT ||
                            statusCode == HttpStatus.SC_REQUEST_TOO_LONG) {
                        msgStatus = MsgStatus.SC_RESPONSE_TIMEOUT;
                    }

                    responseMsg = MsgUtils.genAbnormalMsg(requestMsg, msgStatus, errorMsg);
                    return responseMsg;
                }
            }

            // retrieve server response
            // Fix for QIWB-29.  Now uses getResponseBodyAsStream for to eliminate verbose console warnings.
            // This should also improve the message-passing speed.
            //byte[] xmlBody = post.getResponseBody();
            InputStream responseStream = post.getResponseBodyAsStream();
            StringBuffer sbuf = new StringBuffer();
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(responseStream));
                int BUF_SIZE = 1024;
                char[] cbuf = new char[BUF_SIZE];
                int charsRead = 0;
                do {
                    charsRead = in.read(cbuf, 0, BUF_SIZE);
                    if (charsRead == -1) { //read() always returns as least 1 unless end of stream (-1)
                        break;
                    } else {
                        sbuf.append(cbuf, 0, charsRead);
                    }
                } while (charsRead != -1);
            } finally {
                if (responseStream != null) {
                    responseStream.close();
                }
            }
            xmlResponse = sbuf.toString();

            // deserialize XML into a response message
            // Note: response message carries the response object along with its type
            logger.finest("Deserializing xmlResponse...");
            responseMsg = (IQiWorkbenchMsg) xstream.fromXML(xmlResponse);
            logger.fine("response message:" + responseMsg);

            // remember URL of Tomcat server.
            if ((cmd.equals(QIWConstants.PING_SERVER_CMD) || cmd.equals(QIWConstants.GET_AVAIL_COMPONENTS)) && ("".equals(baseURL) == false)) {
                tomcatURL = baseURL;
            }
            logger.finest("Returning non-abnormal message response");
            return responseMsg;
        } catch (IllegalArgumentException e) {
            errorMsg = "Severe: IllegalArgumentException: Fatal violation: " + e.getMessage();
            logger.severe(errorMsg);
            logger.info("Returning abnormal message response");
            responseMsg = MsgUtils.genAbnormalMsg(requestMsg, MsgStatus.SC_SERVER_DOES_NOT_EXIST, errorMsg);
            return responseMsg;
        } catch (HttpException e) {
            errorMsg = "Severe: HttpClient: Fatal protocol violation: " + e.getMessage();
            logger.severe(errorMsg);
            logger.info("Returning abnormal message response");
            responseMsg = MsgUtils.genAbnormalMsg(requestMsg, MsgStatus.SC_EXCEPTION, errorMsg);
            return responseMsg;
        } catch (IOException e) {
            //Updated error message to show url of attempted HTTP connection
            errorMsg = "Unable to connect to " + url + " due to: " + ExceptionMessageFormatter.getShortFormattedMessage(e);

            //Per Gil 1/21/2009, removed MessageBox showing a user-friendly error message
            //which read "Unable to connect to run-time server.  Please contact technical support.
            //This message was requested for Mike Glinsky's 2008 class and is no longer
            //useful as it primarily appears in a confusing context when the STATS server is down.

            logger.severe(errorMsg);
            int msgStatus = MsgStatus.SC_EXCEPTION;
            int idx = QIWConstants.HTTP_CONNECTION_REFUSED.indexOf(e.getMessage());
            if (idx != -1) {
                msgStatus = MsgStatus.SC_SERVER_DOWN;
            }
            logger.info("Returning abnormal message response");
            responseMsg = MsgUtils.genAbnormalMsg(requestMsg, msgStatus, errorMsg);
            return responseMsg;
        } catch (Exception e) {
            logger.info("A runtime exception has occurred in DispatcherConnector.sendRequestMsg() : " + e.getMessage());
            e.printStackTrace();
            logger.info("Exception was caused by the serialized request: ");

            if (serializedMsg != null) {
                logger.info(serializedMsg);
            } else {
                logger.info("Serialized message is null/was not assigned");
            }

            logger.info("...which generated the xml response: ");

            if (xmlResponse != null) {
                logger.info(xmlResponse);
            } else {
                logger.info("XML response is null/was not assigned");
            }

            responseMsg = MsgUtils.genAbnormalMsg(requestMsg, MsgStatus.SC_EXCEPTION, "sendMsg() failed due to exception: " + e.getMessage());
            return responseMsg;
        } finally {
            logger.finest("Releasing connection...");
            post.releaseConnection();
            logger.finest("Connection released.");
        }
    }

    private boolean isRedirect(int statusCode) {
        if ((HttpStatus.SC_MOVED_PERMANENTLY == statusCode) ||
                (HttpStatus.SC_MOVED_TEMPORARILY == statusCode) ||
                (HttpStatus.SC_SEE_OTHER == statusCode) ||
                (HttpStatus.SC_TEMPORARY_REDIRECT == statusCode)) {
            return true;
        }
        return false;
    }
}
