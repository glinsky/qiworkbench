/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.messageFramework;

import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import java.util.concurrent.SynchronousQueue;
import java.util.logging.Logger;

/**
 * Message handler for case when communication is syncrhonous between two
 * components, In particular, an IO server waits for an IO request from the
 * Servlet Dispatcher and the Servlet Dispatcher waits for the IO response from
 * the IO service to the Servlet Dispatcher.
 * <p>
 * When a message is added to the queue, the sender waits until the message is
 * received, i.e., removed by the receiver. When a message is removed from the
 * queue, the receiver waits until a message is added to the queue.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class SynchronousMsgHandler implements IMsgHandler {
    private static Logger logger = Logger.getLogger(SynchronousMsgHandler.class.getName());

    /** Message queue */
    SynchronousQueue<IQiWorkbenchMsg> msgQueue = new SynchronousQueue<IQiWorkbenchMsg>(true);

    /** Add message to queue. Consumer waiting for message will wake up.
     * A NULL message is logged as a severe internal error and is ignored.
     * @param msg Message to be queued
     */
    public void enqueue(IQiWorkbenchMsg msg) {
        if (msg == null) {
            logger.warning("Internal error: enqueuing a null message; ignored");
            return;
        }
        logger.finest("enqueue msg: "+msg.toString());
        try {
            msgQueue.put(msg);
        } catch (InterruptedException ie) {
            logger.warning("While putting msg in msgQueue, caught: " + ie.getMessage());
        }
    }

    /** Get the message sent for processing. Wait for one if queue empty.
     * @return Next message.
     */
    public IQiWorkbenchMsg dequeue() {
        IQiWorkbenchMsg msg = null;
        try {
            // wait if the queue is empty
            msg = msgQueue.take();
            logger.finest("dequeue msg: "+msg.toString());
        } catch (InterruptedException ie) {
            // should never happen
        }

        return msg;
    }

    /** Check if message queue is empty
     * @return always returns true
     */
    public boolean isQueueEmpty() {
        return msgQueue.isEmpty();
    }

    /**
     * Reset the queue, i.e., does nothing.
     *
     */
    public void resetQueue() {
        msgQueue.clear();
    }
}
