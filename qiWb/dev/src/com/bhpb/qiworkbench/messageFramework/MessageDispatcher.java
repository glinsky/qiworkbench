/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench.messageFramework;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.jnlp.DownloadService;
import javax.jnlp.DownloadServiceListener;
import javax.jnlp.ServiceManager;
import javax.jnlp.UnavailableServiceException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.w3c.dom.Node;
import org.w3c.dom.Element;

import com.bhpb.geoio.filesystems.FileSystemConstants;
import com.bhpb.qiworkbench.AllPermissionsClassLoader;
import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.ComponentLauncher;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.QiSpaceDescriptor;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.QiwbPreferences;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.ClientConstants;
import com.bhpb.qiworkbench.client.qiWbConfiguration;
import com.bhpb.qiworkbench.client.SocketManager;
import com.bhpb.qiworkbench.client.StatsManager;
import com.bhpb.qiworkbench.client.services.IOService;
import com.bhpb.qiworkbench.client.services.JobService;
import com.bhpb.qiworkbench.client.util.ComponentUtils;
import com.bhpb.qiworkbench.client.util.ErrorService;
import com.bhpb.qiworkbench.client.util.FileChooserService;
import com.bhpb.qiworkbench.client.util.PreferenceUtils;
import com.bhpb.qiworkbench.compAPI.CommonUtil;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.workbench.DecoratedNode;
import com.bhpb.qiworkbench.workbench.QiComponentBase;
import com.bhpb.qiworkbench.workbench.WorkbenchAction;
import com.bhpb.qiworkbench.workbench.WorkbenchManager;
import com.bhpb.qiworkbench.workbench.WorkbenchStateManager;
import com.bhpb.qiworkbench.updater.AutomaticUpdateDialog;
import com.bhpb.qiworkbench.updater.UpdateItem;
import com.bhpb.qiworkbench.updater.UpdateUtilities;
import com.bhpb.qiworkbench.util.PropertyUtils;

import com.thoughtworks.xstream.io.StreamException;
import java.io.FileInputStream;
import java.util.logging.LogManager;
import javax.swing.UIManager;
import javax.swing.plaf.metal.MetalLookAndFeel;

import javax.xml.parsers.ParserConfigurationException;    // JE FIELD added

/**
 * All request messages are sent to the message dispatcher for processing.
 * <p>
 * A singleton since there is only one and so there is only one message handler
 * the other components can communicate with.
 *
 * @author Gil Hansen
 * @version 1.1
 */
public class MessageDispatcher extends QiComponentBase implements IqiWorkbenchComponent, Runnable {
    private static Logger logger = Logger.getLogger(MessageDispatcher.class.getName());
    
    /** Time in milliseconds after which a check is made to close inactive client socket channels. */
    private static final long POLLING_THRESHOLD = 5 * 60 * 1000; // 5 minutes
    
    /** Minimum time in milliseconds to wait for a qiComponent being launched to register itself with the system. */
    private static final long REGISTRATION_WAIT = 200;
    
    private static MessageDispatcher singleton = null;
    private boolean addServiceTreeNode = false;
    
    /** Last time the client sockets were polled for inactivity */
    private long lastPollTime = System.currentTimeMillis();

    private MessageDispatcher() {
    }

    /**
     * Get the singleton instance of this class. If the message dispatcher doesn't exist,
     * create it and start as a thread.
     */
    public static MessageDispatcher getInstance() {
        if (singleton == null) {
            singleton = new MessageDispatcher();
            new Thread(singleton).start();
        }
        return singleton;
    }
    /** Message handler for dispatcher */
    private ConcurrentMsgHandler msgHandler;
    /** Message queue. Used in monitoring an empty queue. */
    private AbstractQueue msgQueue;
    /** Descriptor passed to other components so they can communicate with the message dispatcher */
    private IComponentDescriptor myDesc = null;
    /** Runtime Servlet Dispatcher's descriptor */
    private IComponentDescriptor runtimeServletDispDesc = null;
    /** Deployment Servlet Dispatcher's descriptor (used only for GET_CORE_MANIFESTS_CMD)*/
    private IComponentDescriptor deployServletDispDesc = null;
    /** STATS Servlet Dispatcher's descriptor */
    private IComponentDescriptor statsServletDispDesc = null;
    /** List of the server services (descriptors); obtained from the Servlet Dispatcher. */
    ArrayList<IComponentDescriptor> remoteServices = null;
    /** List of the client services (descriptors); generated by the Message Dispatcher. */
    ArrayList<IComponentDescriptor> localServices = null;
    /** List of available plugins (descriptors) - core and external. */
    ArrayList<IComponentDescriptor> availablePlugins = null;
    /** List of available viewer agents (descriptors) - core and external */
    ArrayList<IComponentDescriptor> availableViewers = null;
    ArrayList<UpdateItem> updateItems = null;

    //TODO: Use the Jakarta Commons Pool http://jakarta.apache.org/commons/pool/
    /** Pool of local inactive IO services */
    ArrayList<IComponentDescriptor> localIOServicePool = new ArrayList<IComponentDescriptor>();
    /** Pool of client-side inactive geoIO services */
    ArrayList<IComponentDescriptor> localGeoIOServicePool = new ArrayList<IComponentDescriptor>();
    /** Pool of local inactive job services */
    ArrayList<JobService> localJobServicePool = new ArrayList<JobService>();
    /** Pool of file chooser services */
    ArrayList<FileChooserService> fileChooserServicePool = new ArrayList<FileChooserService>();
    /** Pool of error services */
    ArrayList<ErrorService> errorServicePool = new ArrayList<ErrorService>();
    /** Set of activated local job services. key = jobID, value = job service */
    HashMap<String, JobService> activeLocalJobServices = new HashMap<String, JobService>();

    //** Set of registered qiProjectManagers*/
    //HashMap<String, IqiWorkbenchComponent> activeQiProjectManagers = new HashMap<String, IqiWorkbenchComponent>();
    /** Workbench Manager's descriptor. Set when it registers itself */
    IComponentDescriptor workbenchMgrDesc = null;
    private Properties props = null;
    
    /** Unique, system wide ID (CID) */
    private String myCID = "";
    public String getCID() {
        return myCID;
    }

    // Tomcat url
    private String tomcatURL;

    // Tomcat location (local or remote)
    private String tomcatLoc;
    /** qiSpace - set by SAVE_QISPACE_CMD at startup or by "Rename Desktop".*/
    private QiSpaceDescriptor qispace;
    /** project path - derived from the qiSpace until there is a qiProjectManager */
    private String qiProject = "";
    /** List of requests to other components, e.g., services */
    private RequestList outstandingRequests = new RequestList();
    /** Components who registered themselves. Keyed on CID. */
    private HashMap<String, IComponentDescriptor> componentRegister = new HashMap<String, IComponentDescriptor>();
    /** Components who registered themselves. Keyed on display name. */
    private HashMap<String, IComponentDescriptor> componentRegistry = new HashMap<String, IComponentDescriptor>();
    /**
     * List of remote OS attributes:
     * [0] OS name
     * [1] OS file separator
     * [2] OS line separator
     */
    static private ArrayList<String> remoteOSInfoList = null;
    /**
     * URL of the STATS server. Obtained from the qiWbconfig.xml file
     * Note: The config file is always read from the deployment Tomcat server.
     * The URL is the empty string if 1) The deployment server is down or the
     * config file does not contain a STATS node.
     */
    String statsURL = "";
    /**
     * Path of the directory where the stats files are stores. Obtained from the
     * qiWbconfig.xml file.
     */
    String statsDirPath = "";
    private AllPermissionsClassLoader jniClassLoader = null;

    public IComponentDescriptor getRuntimeServletDispDesc() {
        return runtimeServletDispDesc;
    }

    public IComponentDescriptor getStatsServletDispDesc() {
        return statsServletDispDesc;
    }

    public String getStatsURL() {
        return statsURL;
    }

    public String getStatsDirPath() {
        return statsDirPath;
    }

    /*
    public HashMap<String,IqiWorkbenchComponent> getRegistedQiProjectManagers(){
    return activeQiProjectManagers;
    }
     */
    /**
     * Get the name of the OS under which the runtime Tomcat server executes.
     * return Name of server's OS
     */
    public String getServerOSName() {
        if (getLocationPref().equals(QIWConstants.LOCAL_PREF)) {
            return platformOS;
        } else {
            return remoteOSInfoList.get(0);
        }
    }

    /**
     * Get the file separator of the OS under which the runtime Tomcat server executes.
     * return File separator of server's OS
     */
    public String getServerOSFileSeparator() {
        if (getLocationPref().equals(QIWConstants.LOCAL_PREF)) {
            return File.separator;
        } else {
            return remoteOSInfoList.get(1);
        }
    }

    /**
     * Get the line separator of the OS under which the runtime Tomcat server executes.
     * @return Line separator of server's OS
     */
    public String getServerOSLineSeparator() {
        if (getLocationPref().equals(QIWConstants.LOCAL_PREF)) {
            return System.getProperty("line.separator");
        } else {
            return remoteOSInfoList.get(2);
        }
    }
    /** Platform OS */
    String platformOS = "";

    public String getPlatformOS() {
        return platformOS;
    }
    /** User's HOME directory on the client/local computer */
    String userHOME = "";

    public String getUserHOME() {
        return userHOME;
    }
    /** Directory containing the preference file */
    String prefFileDir = "";

    public String getPrefFileDir() {
        return prefFileDir;
    }
    /** true if there is a default server, it is accessible, there is
    a default qiSpace/qiProject and neverAskAgain is true; otherwise, false. */
    static boolean validDefaultServerAndProject = false;
    /** Manifests of core components. Keyed on the name of the component's jar file */
    private HashMap<String, Manifest> coreManifests = new HashMap<String, Manifest>();
    /** Names of core component jar files.; Keyed on the core component's display name */
    private HashMap<String, String> coreJars = new HashMap<String, String>();
    /** Manifests of plugin components, i.e., those in the component cache. Keyed on the name of the component's jar file */
    private HashMap<String, Manifest> externalManifests = new HashMap<String, Manifest>();
    private HashMap<String, HashMap<String, Manifest>> externalDependentManifests = new HashMap<String, HashMap<String, Manifest>>();
    /** Names of plugin jar files.; Keyed on the plugin's display name */
    private HashMap<String, String> pluginJars = new HashMap<String, String>();
    /** User's preferences */
    private QiwbPreferences userPrefs = null;

    /**
     * Get the user's preferences.
     *
     * @return User's preferences
     */
    public QiwbPreferences getUserPrefs() {
        return userPrefs;
    }

    public void resetMsgQueue() {
        msgHandler.resetQueue();
    }

    public void displayMsgQueue() {
//        logger.info("Contents of MsgQueue:");
        if (msgQueue.isEmpty()) {
            return;
        }
        Object[] msgs = msgQueue.toArray();
        for (int i = 0; i < msgs.length; i++) {
            IQiWorkbenchMsg msg = (IQiWorkbenchMsg) msgs[i];
            if (msg == null) {
                continue;
            }
            logger.info(msg.toString());
        }
    }

    /**
     * Acquire a file chooser service from the pool. If there is none, create one.
     *
     * @return Component descriptor of an fil chooser service; null if cannot create one.
     */
    private FileChooserService acquireFileChooserService() {
        int size = fileChooserServicePool.size();
        if (size > 0) {
            // get an inactive file chooser service from the pool
            FileChooserService fileChooserService = fileChooserServicePool.remove(size - 1);
            //reset queue
//            logger.info("reaquired file chooser service: "+ fileChooserService.getCID());
            return fileChooserService;
        } else {
            // create a new file chooser service which will register itself
            com.bhpb.qiworkbench.client.util.FileChooserService fcServiceInstance = new com.bhpb.qiworkbench.client.util.FileChooserService();
            // get a CID for the new component instance
            String cid = ComponentUtils.genCID(com.bhpb.qiworkbench.client.util.FileChooserService.class);
            // use the CID as the name of the thread
            Thread serviceThread = new Thread(fcServiceInstance, cid);
            serviceThread.start();
            long threadId = serviceThread.getId();
//            logger.info("File Chooser Service Thread-"+Long.toString(threadId)+" started; CID="+cid);
            if (isRegistered(cid, 3)) {
                fileChooserServicePool.add(fcServiceInstance);
                return fcServiceInstance;
            }
        }
        return null;
    }

    /**
     * Acquire error dialog service from the pool. If there is none, create one.
     *
     * @return Component descriptor of error dialog service; null if cannot create one.
     */
    private ErrorService acquireErrorDialogService() {
        int size = errorServicePool.size();
        if (size > 0) {
            // get an inactive error dialog service from the pool
            ErrorService errorService = errorServicePool.remove(size - 1);
            //reset queue
//            logger.info("reaquired error dialog service: "+ errorService.getCID());
            return errorService;
        } else {
            // create a new error dialog service which will register itself
            com.bhpb.qiworkbench.client.util.ErrorService errServiceInstance =
                    new com.bhpb.qiworkbench.client.util.ErrorService();
            // get a CID for the new component instance
            String cid = ComponentUtils.genCID(com.bhpb.qiworkbench.client.util.ErrorService.class);
            // use the CID as the name of the thread
            Thread serviceThread = new Thread(errServiceInstance, cid);
            serviceThread.start();
            long threadId = serviceThread.getId();
//            logger.info("ErrorDialogService Thread-"+Long.toString(threadId)+" started; CID="+cid);
            if (isRegistered(cid, 3)) {
                errorServicePool.add(errServiceInstance);
                return errServiceInstance;
            }
        }
        return null;
    }

    /**
     * Acquire a local IO service from the pool. If there is none, create one.
     *
     * @return Component descriptor of an IO service; null if cannot create one.
     */
    private IComponentDescriptor acquireLocalIOservice() {
        int size = localIOServicePool.size();
        if (size > 0) {
            // get an inactive IO service from the pool
            IComponentDescriptor ioServiceDesc = localIOServicePool.remove(size - 1);
            //reset queue
            //ioServiceDesc.getMsgHandler().resetQueue();
//            logger.info("reaquired IO service:"+ioServiceDesc);
            return ioServiceDesc;
        } else {
            // create a new IO service which will register itself
            com.bhpb.qiworkbench.client.services.IOService ioServiceInstance = new com.bhpb.qiworkbench.client.services.IOService();
            // get a CID for the new component instance
            String cid = ComponentUtils.genCID(com.bhpb.qiworkbench.client.services.IOService.class);
            // use the CID as the name of the thread
            Thread serviceThread = new Thread(ioServiceInstance, cid);
            serviceThread.start();
            long threadId = serviceThread.getId();
//            logger.info("IO Service Thread-"+Long.toString(threadId)+" started; CID="+cid);
            if (isRegistered(cid, 3)) {
                return getComponentDesc(cid);
            }
        }
        return null;
    }

    /**
     * Release a local IO service to the pool.
     *
     * @param ioServiceCID CID of local ioService.
     */
    private void releaseLocalIOservice(String ioServiceCID) {
        IComponentDescriptor desc = getComponentDesc(ioServiceCID);
        localIOServicePool.add(desc);
    }

    /**
     * Acquire a local geoIO service from the pool. If there is none, create one.
     *
     * @return Component descriptor of an geoIO service; null if cannot create one.
     */
    private IComponentDescriptor acquireLocalGeoIOservice() {
        logger.info("Entering acquireLocalGeoIOservice()");
        int size = localGeoIOServicePool.size();
        logger.info("Pool size is: " + size);
        if (size > 0) {
            // get an inactive geoIO service from the pool
            logger.info("localGeoIOServicePool.size() == 0, getting a service from the pool...");
            IComponentDescriptor geoIoServiceDesc = localGeoIOServicePool.remove(size - 1);
            //reset queue
            //geoIoServiceDesc.getMsgHandler().resetQueue();
            //            logger.info("reaquired geoIO service:"+geoIoServiceDesc);
            if (geoIoServiceDesc == null) {
                logger.warning("Drew null geoIoServiceDesc from the pool!");
            }
            return geoIoServiceDesc;
        } else {
            // create a new geoIO service which will register itself
            com.bhpb.qiworkbench.client.services.GeoIOService geoIoServiceInstance = new com.bhpb.qiworkbench.client.services.GeoIOService();
            logger.info("geoIoServiceInstance created.");
            // get a CID for the new component instance
            String cid = ComponentUtils.genCID(com.bhpb.qiworkbench.client.services.GeoIOService.class);
            // use the CID as the name of the thread
            Thread serviceThread = new Thread(geoIoServiceInstance, cid);
            logger.info("Starting service...");
            serviceThread.start();
            long threadId = serviceThread.getId();
            logger.info("geoIO Service Thread-" + Long.toString(threadId) + " started; CID=" + cid);
            if (isRegistered(cid, 3)) {
                return getComponentDesc(cid);
            } else {
                logger.warning("isRegistered(" + cid + ",3) failed.");
            }
        }
        logger.warning("Existing acquireLocalGeoIOService with a null IComponentDescriptor.");
        return null;
    }

    /**
     * Release a local geoIO service to the pool.
     *
     * @param geoIoServiceCID CID of local geoIoService.
     */
    private void releaseLocalGeoIOservice(String geoIoServiceCID) {
        IComponentDescriptor desc = getComponentDesc(geoIoServiceCID);
        localGeoIOServicePool.add(desc);
    }

    /**
     * Acquire a local job service from the pool. If there is none, create one.
     *
     * @return Job service instance; null if cannot create one.
     */
    private JobService acquireLocalJobService() {
        int size = localJobServicePool.size();
        if (size > 0) {
            // get an inactive job service from the pool
            JobService jobService = localJobServicePool.remove(size - 1);
//            logger.info("reaquired job service:"+jobService.getCID());
            return jobService;
        } else {
            // create a new job service which will register itself
            com.bhpb.qiworkbench.client.services.JobService jobServiceInstance = new com.bhpb.qiworkbench.client.services.JobService();
            // get a CID for the new component instance
            String cid = ComponentUtils.genCID(com.bhpb.qiworkbench.client.services.JobService.class);
            // use the CID as the name of the thread
            Thread serviceThread = new Thread(jobServiceInstance, cid);
            serviceThread.start();
            long threadId = serviceThread.getId();
//            logger.info("Job Service Thread-"+Long.toString(threadId)+" started; CID="+cid);
            // give new job service a chance to register itself so its CID is
            if (isRegistered(cid, 3)) {
                return jobServiceInstance;
            }
        }
        return null;
    }

    /**
     * Release a local job service to the pool. This only happens on demand,
     * i.e., by a releaseJob command. There is no way to determine when a
     * component using the service no longer requires the service. It does
     * this by sending the releaseJob command.
     *
     * @param jobService Local job Service.
     */
    private void releaseLocalJobService(JobService jobService) {
        localJobServicePool.add(jobService);
    }

    /**
     * Analyze and set preference properly
     * @ return true if the qiwbpreferences.xml file was successfully read, otherwise false
     */
    private boolean checkPreference() {
        // set the location of the preference file
        userHOME = CommonUtil.getAppDir(CommonUtil.HOME_DIR);
        prefFileDir = CommonUtil.getAppDir(CommonUtil.QIWB_DIR);
        String serverUrl = System.getProperty("serverUrl");
        String dataPath = System.getProperty("dataPath");
        String saveSet = System.getProperty("saveSet");
        //!!! ASSUMES datapath is a project
        String qiSpaceKind = "P";
        if (serverUrl != null) {
            if (dataPath == null || saveSet == null) {
                serverUrl = null; //ignore it

            }
        }

        //get user's preferences saved in the pref file, create one if non-existent
        if (PreferenceUtils.prefFileExists(prefFileDir)) {
            try {
                //TODO use Gil's custom xml reader within readPrefs
                //for now, do not handle any QiwIOExcpetion wrapping an XStream Exception
                //eventually, these QiwIOExceptions will be caught and the user asked to choose
                //between exiting the application and instantiating the preferences from default settings
                userPrefs = PreferenceUtils.readPrefs(prefFileDir);
                if (serverUrl != null) {
                    userPrefs.addServerQispaceDesktop(serverUrl, dataPath, qiSpaceKind, saveSet, true);
                }
            } catch (QiwIOException qioe) {
                logger.severe(qioe.getMessage());
                return false;
            } catch (StreamException se) {
                String errMsg = "Caught StreamException while parsing qiwbPreferences.xml: " + se.getMessage();
                logger.warning(errMsg);
                addInitException(new Exception(errMsg,se));
                return false;
            }
        } else {
            //fix for QIWB-35 - The config.properties flat file has been removed.
            //If the user has no qiwbPreferences, an empty one will be created,
            //and the qiwbConfiguration will parse the response to GET_SERVER_CONFIG,
            //adding these servers and empty lists of qiSpaces to the qiwbPreferences file.
            userPrefs = new QiwbPreferences();
            if (serverUrl != null) {
                userPrefs.addServerQispaceDesktop(serverUrl, dataPath, qiSpaceKind, saveSet, true);
            }
        }
        return true;
    }

    /**
     * Initialize the Message Dispatcher thread.
     * <ul>
     *   <li>Create its own component descriptor; includes CID.</li>
     *   <li>Create component descriptor for Servlet Dispatcher and send it to it in a Register-self command. Register the Servlet Dispatcher.</li>
     *   <li>Generate descriptors for all locals services and save in a list. Register each service.</li>
     *   <li>For any commands whose argument is a component's display name, replace with its descriptor. Replace the base CID in the descriptor with a unique, full CID.</li>
     *   <li>Record the name of the plugin jar files in the component cache and Manifest of each.</li>
     * </ul>
     * Note: Services, plugins and viewer agents register themselves with the
     * Message Dispatcher when an instance is activated.
     *
     */
    public void init() {
        try {
            if (checkPreference() == false) {
                //initFinished is not set to true here - this should prevent other threads from attempting to
                //send a Message until the application shuts down
                setInitSuccessful(false);
            } else {
                long threadId = Thread.currentThread().getId();

                // create dispatcher's message handler
                msgHandler = new ConcurrentMsgHandler();

                // extract message queue used by message handler
                msgQueue = msgHandler.getMsgQueue();

                // generate unique, system wide ID (CID)
                myCID = ComponentUtils.genCID(this);

                // create descriptor for self
                myDesc = new ComponentDescriptor();
                myDesc.setCID(myCID);
                myDesc.setMsgHandler(msgHandler);
                myDesc.setComponentKind(QIWConstants.FRAMEWORK_COMP);
                myDesc.setDisplayName(QIWConstants.MSG_DISPATCHER_NAME);

                // ONLY CREATE the servletDispatcher descriptors. registration is now deferred until after REGISTER_SELF is issued to the servlet
                // This allows the same URL to be used for the runtime and deployment Tomcat servers, wihout pre-registering 2 distinct servletDispatcher Descriptors,
                // which would collide on the single key QIWConstants.SERV_DISPATCHER_NAME if component registration were still performed here.
                runtimeServletDispDesc = createDispatcher("Runtime server servlet dispatcher");
                deployServletDispDesc = createDispatcher("Deployment server servlet dispatcher");
                statsServletDispDesc = createDispatcher("Stats server servlet dispatcher");

                IQiWorkbenchMsg request = null;
                IQiWorkbenchMsg response = null;

                //Note: Property set in qiw_httpinvoker_jnlp.jsp
                String deploymentURL = System.getProperty("deployServerURL");

                request = new QiWorkbenchMsg(myCID, deployServletDispDesc.getCID(), QIWConstants.CMD_MSG,
                        QIWConstants.PING_SERVER_CMD, MsgUtils.genMsgID(),
                        QIWConstants.STRING_TYPE, deploymentURL);
                response = DispatcherConnector.getInstance().sendRequestMsg(request);
                boolean pingSuccessful = false;
                boolean getConfigSuccessful = false;

                if (!response.isAbnormalStatus()) {
                    logger.info("Established contact with Deployment Tomcat");
                    pingSuccessful = true;

                    //Note: qiWbconfig.xml obtained from the deployment server since command routed to the
                    //Tomcat server for which the last PING was issued.
                    request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG,
                            QIWConstants.GET_SERVER_CONFIG_CMD, MsgUtils.genMsgID(), "", "");
                    response = DispatcherConnector.getInstance().sendRequestMsg(request);

                    if (!response.isAbnormalStatus()) {
                        getConfigSuccessful = true;
                      try {
                        qiWbConfiguration.getInstance().setConfigFile((String) response.getContent());
                      } catch (Exception e) {
                        System.out.println("MY EXCEPTION HACK: " + e.getMessage());
                      }
                        HashMap<String, String> statsAttrs = (HashMap) qiWbConfiguration.getInstance().getStatsAttributes();
                        statsURL = statsAttrs.get("statsurl");
                        if (statsURL == null) {
                            statsURL = "";
                        }
                        StatsManager.getInstance().setStatsURL(statsURL);
                        statsDirPath = statsAttrs.get("statspath");
                        if (statsDirPath == null) {
                            statsDirPath = "";
                        }
                        if ("".equals(statsDirPath)) {
                            logger.warning("statsDirPath is empty - STATS element may not be properly defined in the server's $QIWB_CONFIG file.");
                        }
                        StatsManager.getInstance().setStatsDirPath(statsDirPath);

                        //if there is an existing usage log file, send it to the
                        //STATS server for processing.
                        if (StatsManager.getInstance().isStatsLog()) {
                            StatsManager.getInstance().transmitStats(myCID);
                        }

                        String event = StatsManager.getInstance().createStatsEvent(QIWConstants.WORKBENCH_COMP, ClientConstants.LAUNCHED_ACTION);
                        StatsManager.getInstance().logStatsEvent(event);
                    } else {
                        String errorMessage = "Failed to retrieve configuration: " + System.getProperty("line.separator");

                        if ((response.getContentType().equals(QIWConstants.STRING_TYPE)) && (response.getContent() != null)) {
                            errorMessage += response.getContent().toString();
                        } else {
                            errorMessage += "reason unknown";
                        }
                        logger.severe(errorMessage);
                        JOptionPane.showMessageDialog(new JFrame(), errorMessage, "Default Server Access Error", JOptionPane.WARNING_MESSAGE);
                    }
                } else {
                    String errorMessage = "Failed to connect to deployment server: " + System.getProperty("line.separator");
                    if ((response.getContentType().equals(QIWConstants.STRING_TYPE)) && (response.getContent() != null)) {
                        errorMessage += response.getContent().toString();
                    } else {
                        errorMessage += "reason unknown";
                    }
                    JOptionPane.showMessageDialog(new JFrame(), errorMessage, "Default Server Access Error", JOptionPane.WARNING_MESSAGE);
                }

                if (pingSuccessful && getConfigSuccessful) {
                    UpdateUtilities.MESSAGE_DISPATCHER_CID = myCID;
                    UpdateUtilities.SERVLET_DISP_DESC = runtimeServletDispDesc.getCID();

                    if (System.getProperty("install") != null && System.getProperty("install").equals("true")) {
                        //UpdatingDialog dialog = new UpdatingDialog(myCID,servletDispDesc.getCID());
                        AutomaticUpdateDialog dialog = new AutomaticUpdateDialog(myCID, runtimeServletDispDesc.getCID());
                    }

                    //Select the default runtime Tomcat server and if it is accessible, the default project,
                    //if specified in the user's preferences. Otherwise, let the user select (at the end of
                    //main() after the Workbench Manager has been launched.
                    logger.finest("Checking runtimeURL, project and sessions properties to determine whether to bypass ProjectSelectionDialog...");
                    String runtimeURL = System.getProperty("runtimeURL");
                    String project = System.getProperty("project");
                    String session = System.getProperty("session");

                    if (runtimeURL != null && project != null && session != null) {
                        logger.info("All 3 parameters runtimeURL, project and session are non-null, attempting to set default session...");
                        selectSession(runtimeURL, project, session);
                    } else {
                        logger.info("One or more of the parameters runtimeURL, project and session are NULL, invoking ProjectSelectorDialog...");
                        selectDefaultServerAndProject();
                    }

                    // Determine the platform OS. If the user's home directory starts
                    // with '/', it is UNIX based; otherwise, Windows
                    this.platformOS = this.userHOME.startsWith("/") ? QIWConstants.UNIX_OS : QIWConstants.WINDOWS_OS;

                    runtimeServletDispDesc = registerDescriptor(runtimeServletDispDesc, "");
                    deployServletDispDesc = registerDescriptor(deployServletDispDesc, deploymentURL);

                    // The deferred registration of the runtime servlet Dispatcher now occurs here.
                    // This is in case another client has already registered a servletdispatcher
                    // CID on the server and the initial descriptor created by this client was overridden.
                    registerComponent(runtimeServletDispDesc);

                    // Get the Manifests of the core components. Used to activate
                    // a core component, for the Manifest contains the main class.
                    // NOTE: Cannot get the Manifests of the core components until
                    //       AFTER the Servlet Dispatcher has registered itself.
                    // UPDATE: Core Jars are now always retrieved from the deployment
                    // server, NOT the runtime server
                    // This will prevent fetching old jars, and possibly putting
                    // components on the "new" menu more than once, if the runtime
                    // server is out of date

                    request = new QiWorkbenchMsg(myCID, deployServletDispDesc.getCID(), QIWConstants.CMD_MSG, QIWConstants.GET_CORE_MANIFESTS_CMD, MsgUtils.genMsgID(), "", deploymentURL);
                    response = DispatcherConnector.getInstance().sendRequestMsg(request);
                    if (!response.isAbnormalStatus()) {
                        logger.config("Retrieved the core Manifests from the deployment server: " + deploymentURL);
                        ArrayList maps = (ArrayList) response.getContent();
                        coreJars = (HashMap) maps.get(0);
                        coreManifests = (HashMap) maps.get(1);
                    } else {
                        logger.config("Failed to get the Manifests of the core components from Tomcat");
                    //TODO: System issue. No core components can be launched.
                    }

                    //Generate the list of local services (descriptors) and retain
                    localServices = new ArrayList();
                    IComponentDescriptor localServiceDesc = new ComponentDescriptor();
                    // local IO service
                    localServiceDesc.setCID(ComponentUtils.genBaseCID(IOService.class));
                    localServiceDesc.setMsgHandler(null);
                    localServiceDesc.setComponentKind(QIWConstants.LOCAL_SERVICE_COMP);
                    localServiceDesc.setDisplayName(QIWConstants.LOCAL_IO_SERVICE_NAME);
                    localServices.add(localServiceDesc);
                    logger.fine("Local IO service descriptor formed:" + localServiceDesc);

                    // local job service
                    localServiceDesc = new ComponentDescriptor();
                    localServiceDesc.setCID(ComponentUtils.genBaseCID(JobService.class));
                    localServiceDesc.setMsgHandler(null);
                    localServiceDesc.setComponentKind(QIWConstants.LOCAL_SERVICE_COMP);
                    localServiceDesc.setDisplayName(QIWConstants.LOCAL_JOB_SERVICE_NAME);
                    localServices.add(localServiceDesc);
                    logger.fine("Local job service descriptor formed:" + localServiceDesc);

                    //Capture the name of the JAR files in the component cache and
                    //their Manifests.
                    getPluginManifests();

                    acquireFileChooserService();
                    WorkbenchStateManager.getInstance();
                    // notify the startup method initialization finished
                    QIWConstants.SYNC_LOCK.unlock();

                    // get the list of available viewers and plugins, core and external. It
                    // is assumed the external components do not change during a session, i.e.,
                    // an external component is not added or deleted during the session.
                    availablePlugins = getAvailableComponents("plugin");
                    availableViewers = getAvailableComponents("viewer");
                    super.setInitSuccessful(true);
                } else {
                    super.setInitSuccessful(false);
                    logger.severe("Failed to ping or get configuration from runtime server - MessageDispatcher init unsuccessful.  Application will now close");
                }
            }
//        } catch (Exception e) {
//            super.setInitSuccessful(false);
//            super.addInitException(e);
//            logger.severe("Exception caught in MessageDispatcher.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

    /**
     * If the user's preferences indicate both a default runtime Tomcat server
     * and a default qiSpace/qiProject, and the server is accessible, then select
     * them; otherwise, let the user select via the selector dialog (invoked by the
     * Workbench manager).
     */
    private void selectDefaultServerAndProject() {
        validDefaultServerAndProject = false;
        //Check if a default runtime Tomcat server is specified and available
        String defaultServer = userPrefs.getDefaultServer();
        if (!defaultServer.equals("")) {
            IQiWorkbenchMsg request = null;
            IQiWorkbenchMsg response = null;

            // use the default server
            tomcatURL = defaultServer;
            tomcatLoc = QiwbPreferences.getServerLoc(defaultServer);
            // make sure server is accessible
            request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG,
                    QIWConstants.PING_SERVER_CMD, MsgUtils.genMsgID(),
                    QIWConstants.STRING_TYPE, tomcatURL);
            response = DispatcherConnector.getInstance().sendRequestMsg(request);

            if (!response.isAbnormalStatus()) {
                logger.config("selectDefaultServerAndProject: Established contact with " + tomcatURL);

                //Check if a default qiSpace/qiProject is specified
                String defaultQispace = userPrefs.getDefaultQispace(defaultServer);
                if (!defaultQispace.equals("") && userPrefs.getNeverAskAgain()) {
                    validDefaultServerAndProject = true;

                    //qispace = defaultQispace;
                    //Note: qiSpace metadata will be found since default server found
                    qispace = userPrefs.getQispaceDesc(defaultServer, defaultQispace);
                    //TODO: Specify project. There may be no active qiProjectManager
                    //NOTE: Until there is a qiProjectManager, it is assumed the qiSpace is the project
                    qiProject = qispace.getPath();
                }
            } else {
                logger.warning("selectDefaultServerAndProject: Failed to connect to default" + tomcatURL);

                JOptionPane.showMessageDialog(new JFrame(), "Default runtime Tomcat server not accessible: " + tomcatURL, "Default Server Access Error", JOptionPane.WARNING_MESSAGE);
                validDefaultServerAndProject = false;
            }
        }
    }

    /**
     * If the Java system properties for runtimeURL, project and session have all
     *  been set, these settings will be checked and used to launch the workbench
     *  if valid. (validDefaultServerAndProject will be set to true).
     *
     *  All parameters must be non-null, or else this method should not be called.
     */
    private void selectSession(String runtimeURL, String project, String session) {

        validDefaultServerAndProject = false;

        if (runtimeURL == null || project == null || session == null) {
            throw new IllegalArgumentException("MessageDispatcher.selectSession() cannot take any null parameters, but runtimeURL, project and/or session were null.");
        }
        if (!runtimeURL.equals("")) {
            IQiWorkbenchMsg request = null;
            IQiWorkbenchMsg response = null;

            // use the default server
            tomcatURL = runtimeURL;
            tomcatLoc = userPrefs.getServerLoc(runtimeURL);

            logger.info("Checking for runtime Tomcat server accessibility: " + runtimeURL);
            // make sure server is accessible
            request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG,
                    QIWConstants.PING_SERVER_CMD, MsgUtils.genMsgID(),
                    QIWConstants.STRING_TYPE, tomcatURL);
            response = DispatcherConnector.getInstance().sendRequestMsg(request);

            if (!response.isAbnormalStatus()) {
                logger.finest("Connected to: " + runtimeURL);

                QiwbPreferences userPrefs = MessageDispatcher.getInstance().getUserPrefs();

                logger.info("Adding project, session to userPrefs: " + project + ", " + session);

                //TODO fix this hack - the slash should be a call to
                //getServerOSFileSeparator(), but this cannot be called until MessageDispatcher.init() is finished
                //workaround - parse the project and session parameters, looking for '/' or '\'
                String projectAndSession = project + "/" + session;

                userPrefs.setDefaultServer(runtimeURL);

                int status = userPrefs.addDesktop(runtimeURL, project, projectAndSession);
                logger.info("userPrefs.addDesktop returned status code: " + status);

                int status2 = userPrefs.setDefaultDesktop(runtimeURL, project, projectAndSession);
                logger.info("userPrefs.setDefaultDesktop returned status code: " + status2);

                qispace = userPrefs.getQispaceDesc(runtimeURL, project);
                qiProject = qispace.getPath();

                userPrefs.setDefaultQispace(runtimeURL, project);
                userPrefs.setNeverAskAgain(true);

                validDefaultServerAndProject = true;
            } else {
                logger.warning("selectDefaultServerAndProject: Failed to connect to default" + tomcatURL);

                JOptionPane.showMessageDialog(new JFrame(), "Default runtime Tomcat server not accessible: " + tomcatURL, "Default Server Access Error", JOptionPane.WARNING_MESSAGE);
                validDefaultServerAndProject = false;
            }
        }
    }

    /**
     * Get information about the OS of the remote server.
     *
     * @return List of OS information, namely, [0] name of OS, [1] file separator.
     */
    public ArrayList<String> getRemoteOSInfoList() {
        return remoteOSInfoList;
    }

    /**
     * Set info about the OS of the remote server. Mainly called from the
     * Project Selector dialog once the user selected a server and its location
     * preference and then must select a project on that server.
     * @param OS information of the remote server
     */
    public void setRemoteOSInfoList(ArrayList<String> remoteOSInfoList) {
        this.remoteOSInfoList = remoteOSInfoList;
    }

    /** Initialize the dispatcher, then start processing messages it receives.
     */
    public void run() {
        QIWConstants.SYNC_LOCK.lock();
        // initialize the dispatcher who will release the lock when finished

        init();
        if (isInitSuccessful()) {
            // process any request messages received from other components
            while (true) {
                // Note: do not dequeue until through processing; otherwise,
                // will wait for a message to be queued
                // if queue is empty wait for a message
                IQiWorkbenchMsg msg = msgHandler.dequeue();
                processMsg(msg);
            }
        }
    }

    /**
     * Register a component so the message dispatcher can communicate with it. In exchange,
     * return the message dispatcher's descriptor so the registrant can communicate with the
     * message dispatcher.
     * <p>
     * The registrant's descriptor is kept in a hashmap keyed on its CID.
     *
     * @param desc Component descriptor of the registrant.
     * @return Component descriptor of the message dispatcher or null if the component is already registered.
     */
    synchronized public IComponentDescriptor registerComponent(IComponentDescriptor desc) {
        // check if component is already registered
        if (componentRegister.containsKey(desc.getCID())) {
            return null;
        }
        componentRegister.put(desc.getCID(), desc);

        if (componentRegistry.containsKey(desc.getDisplayName())) {
            return null;
        }
        componentRegistry.put(desc.getDisplayName(), desc);

        // Save the descriptor of the Workbench Manager so don't have to look
        // it up. There can only be one. Ignore the #n at the end of the name.
        if (desc.getDisplayName().indexOf(QIWConstants.WORKBENCH_MGR_NAME) != -1) {
            workbenchMgrDesc = desc;
        }
        logger.config("registered:" + desc);

        return myDesc;
    }

    /**
     * Unregister a registered component. Usually performed when the component
    is deactivated.
     *
     * @param desc Descriptor of the registered component.
     */
    synchronized public void unregisterComponent(IComponentDescriptor desc) {
        logger.config("unregistering:" + desc);
        componentRegister.remove(desc.getCID());

        String name = desc.getDisplayName();
        componentRegistry.remove(name);

        if (name.equals(QIWConstants.WORKBENCH_MGR_NAME)) {
            workbenchMgrDesc = null;
        }
    }

    /*public Vector getComponentsFromPUC(String PUC) {
    
    }*/
    /**
     * Get the descriptor for a registered component based on its CID.
     *
     * @param cid CID of component
     * @return Component's descriptor if registered; otherwise, null.
     */
    public IComponentDescriptor getComponentDesc(String cid) {
        // check if Message Dispatcher's CID
        if (cid.equals(myCID)) {
            return myDesc;
        }
        if (!componentRegister.containsKey(cid)) {
            return null;
        }
        return componentRegister.get(cid);
    }

    /**
     * Get the descriptor for a registered component based on its display name.
     *
     * @param displayName  Display Name of component
     * @return Component's descriptor if registered; otherwise, null.
     */
    public IComponentDescriptor getComponentDescByName(String displayName) {
        if (!componentRegistry.containsKey(displayName)) {
            return null;
        }
        for (String key : componentRegistry.keySet()) {
            if (key.endsWith(displayName)) {
                return componentRegistry.get(key);
            }
        }
        return componentRegistry.get(displayName);
    }

    /**
     * Get the CID of a registered component based on its display name
     *
     * @param displayName Display name of component.
     * @return CID if component registered; otherwise, blank.
     */
    public String getCidByName(String displayName) {
        if (!componentRegistry.containsKey(displayName)) {
            return "";
        }
        for (String key : componentRegistry.keySet()) {
            if (key.endsWith(displayName)) {
                return componentRegistry.get(key).getCID();
            }
        }
        return componentRegistry.get(displayName).getCID();
    }

    /**
     * Check if a qiComponent registered. Repeatedly check for the component
     * may not have established itself yet with the system, i.e., it is in the
     * process of being launched, but not yet registered.
     *
     * @param compCID CID of component.
     * @param tries The number of times to try.
     * @return true if registered; otherwise, false
     */
    private boolean isRegistered(String compCID, int tries) {
        while (tries > 0) {
            if (componentRegister.containsKey(compCID)) {
                return true;
            }
            try {
                //progressively wait less and less on each try
                Thread.sleep(REGISTRATION_WAIT*tries);
            } catch (InterruptedException ie) {
            }
            tries--;
        }

        return false;
    }

    /**
     * Map the display name to its descriptor.
     *
     * @param displayName Display name of component.
     * @return component's descriptor if found; otherwise, null
     */
    private IComponentDescriptor mapName2Descriptor(String displayName) {
        // search the list of remote services
        for (IComponentDescriptor desc : remoteServices) {
            if (desc.getDisplayName().equals(displayName)) {
                return desc;
            }
        }

        // search the list of local services
        for (IComponentDescriptor desc : localServices) {
            if (desc.getDisplayName().equals(displayName)) {
                return desc;
            }
        }

        // search the list of available plugins
        for (IComponentDescriptor desc : availablePlugins) {
            if (desc.getDisplayName().equals(displayName)) {
                return desc;
            }
        }

        // search the list of available viewer agents
        for (IComponentDescriptor desc : availableViewers) {
            if (desc.getDisplayName().equals(displayName)) {
                return desc;
            }
        }

        return null;
    }

    /**
     * Route the message to the designated component
     *
     * @param msg Message to be routed.
     * @param cid CID of component receiving the message
     */
    private void routeMsg(IQiWorkbenchMsg msg, String cid) {
        IComponentDescriptor desc = getComponentDesc(cid);
        if (desc == null) {
            String errorMsg = "Internal error: Component not registered so cannot send message:" + msg + "; CID=" + cid;
            //log internal error
            logger.severe(errorMsg);
            return;
        }
        desc.getMsgHandler().enqueue(msg);
    }

    /**
     * Route the message to its consumer.
     *
     * @param msg Message to be routed.
     */
    private void routeMsg(IQiWorkbenchMsg msg) {
        IComponentDescriptor desc = getComponentDesc(msg.getConsumerCID());
        if (desc == null) {
            String errorMsg = "Internal error: Component not registered so cannot send message:" + msg + "; CID=" + msg.getConsumerCID();
            //log internal error
            logger.severe(errorMsg);
            return;
        }
        desc.getMsgHandler().enqueue(msg);
    }

    /**
     * Route the IO request to the specified IO service. A new request
     * from the Message Dispatcher is sent to the IO service. The response
     * will be sent to the dispatcher who will route it on to the requester.
     *
     * @param msg IO request.
     * @param ioService Component Descriptorof the IO Service.
     */
    private void routeToLocalIOService(IQiWorkbenchMsg msg, IComponentDescriptor ioService) {
        //remember the original IO request

        logger.finest("routeToLocalIOService: remember request: " + msg);
        outstandingRequests.addRequest(msg);
        String ioServiceCID = ioService.getCID();

        //make a new request and send to IO service
        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, ioServiceCID, QIWConstants.CMD_MSG, msg.getCommand(), msg.getMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
        routeMsg(request, ioServiceCID);
    }

    /**
     * Route the geoIO request to the specified geoIO service. A new request
     * from the Message Dispatcher is sent to the geoIO service. The response
     * will be sent to the dispatcher who will route it on to the requester.
     *
     * @param msg geoIO request.
     * @param geoIoService Component Descriptor of the geoIO Service.
     */
    private void routeToLocalGeoIOService(IQiWorkbenchMsg msg, IComponentDescriptor geoIoService) {
        //remember the original geoIO request

        logger.finest("routeToLocalGeoIOService: remember request: " + msg);
        outstandingRequests.addRequest(msg);
        String geoIoServiceCID = geoIoService.getCID();

        //make a new request and send to geoIO service
        IQiWorkbenchMsg request = new QiWorkbenchMsg(msg.getProducerCID(), geoIoServiceCID, QIWConstants.CMD_MSG, msg.getCommand(), msg.getMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
        routeMsg(request, geoIoServiceCID);
    }

    /**
     * Route job request to the specified local job service. A new request
     * from the Message Dispatcher is sent to the job service. The response
     * will be sent to the dispatcher who will route it on to the requester.
     *
     * @param msg Job request.
     * @param jobService The job service to process the request.
     */
    private void routeToLocalJobService(IQiWorkbenchMsg msg, JobService jobService) {
        //remember the original job request
        outstandingRequests.addRequest(msg);

        String jobServiceCID = jobService.getCID();

        //make a new request and send to job service
        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, jobServiceCID, QIWConstants.CMD_MSG, msg.getCommand(), msg.getMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
        routeMsg(request, jobServiceCID);
    }

    /**
     * Process the message. Route the response or another subcommand.
     * <p>
     * If the message is targeted to the Message Dispatcher, execute the
     * command and send a reponse back.
     * <p>
     * If the message is targeted to the Servlet Dispatcher, send the command
     * via Dispatcher Connector and send the response back.
     * <p>
     * Otherwise, process the message based on the message type:
     * <ul>
     *   <li>CMD - message is a command to be sent to its designated consumer</li>
     *   <li>CMD_ROUTE - message is a command to be routed to other agents as determined by the Message Dispatcher's routing matrix</li>
     *   <li>DATA - message is information to be returned to its designated producer</li>
     *   <li>DATA_CMD - message is a command to be treated as data, not to be executed</li>
     * </ul>
     *
     * @param msg Message to be routed for processing
     * @see com.bhpb.qiworkbench.ComponentDescriptor ComponentDescriptor
     * @see com.bhpb.qiworkbench.server.ServletDispatcher ServletDispatcher
     */
    public void processMsg(IQiWorkbenchMsg msg) {
        //Time to check for inactive client sockets?
        long currentTime = System.currentTimeMillis();
        if (currentTime-lastPollTime > POLLING_THRESHOLD) {
            lastPollTime = currentTime;
            SocketManager.getInstance().closeInactiveSockets();
        }
        
        //log message traffic
        logger.fine("Processing message:" + msg);
        String msgKind = msg.getMsgKind();

        IQiWorkbenchMsg response = null;
        String cmd = "";
        IComponentDescriptor desc = null;
        String errorMsg = "";

        //ALWAYS PROCESS MESSAGES TARGETED TO THE DISPATCHER FIRST!
        // message targeted to the message dispatcher for processing?
        if (msg.getConsumerCID().equals(myCID)) {
            cmd = msg.getCommand();

            if (msgKind.equals(QIWConstants.CMD_MSG)) {
                if (cmd.equals(QIWConstants.REGISTER_SELF_CMD)) {
                    IQiWorkbenchMsg request = msg;
                    request.setProducerCID(myCID);
                    request.setConsumerCID(runtimeServletDispDesc.getCID());
                    request.setContentType(ComponentDescriptor.class.getName());
                    request.setContent(runtimeServletDispDesc);
                    response = DispatcherConnector.getInstance().sendRequestMsg(request);
                    if (!response.isAbnormalStatus()) {
                        // consume response
                        logger.config("Sent Servlet Dispatcher its descriptor: response=" + response);
                    } else {
                        //TODO Take action based on the cause of the abnormal status. For
                        //example, if the server is down, notify the user and suspend
                        //processing. Usually a message is sent to the Workbench Manager
                        //who takes the appropriate action.
                        String cause = (String) response.getContent();
                        logger.severe("Unable to send Servlet Dispatcher its descriptor. CAUSE:" + cause);
                    }
                } else if (cmd.equals(QIWConstants.RETURN_FILE_CHOOSER_SERVICE_CMD)) {
                    FileChooserService fileChooser = (FileChooserService) msg.getContent();
                    fileChooserServicePool.add(fileChooser);
                    return;
                } else if (cmd.equals(QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD)) {
                    FileChooserService fileChooserService = acquireFileChooserService();
                    IQiWorkbenchMsg resp = null;
                    if (fileChooserService == null) {
                        logger.severe("Internal error: no file chooser service");
                        //send an abnormal response back
                        resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no file chooser service");
                        routeMsg(resp);
                    //return;
                    } else {
                        resp = msg;

                        resp.setMsgKind(QIWConstants.DATA_MSG);
                        //resp.setContentType(FileChooserService.class.getName());
                        resp.setContentType(QIWConstants.COMP_DESC_TYPE);
                        //resp.setContent(fileChooserService);
                        resp.setContent(fileChooserService.getMessagingMgr().getMyComponentDesc());
                        MsgUtils.swapCIDs(resp);
                        routeMsg(resp);
                    }

                    return;
                } else if (cmd.equals(QIWConstants.RETURN_ERROR_DIALOG_SERVICE_CMD)) {
                    ErrorService errorService = (ErrorService) msg.getContent();
                    errorServicePool.add(errorService);
                    return;
                } else if (cmd.equals(QIWConstants.GET_ERROR_DIALOG_SERVICE_CMD)) {
                    ErrorService errorService = acquireErrorDialogService();
                    IQiWorkbenchMsg resp = null;
                    if (errorService == null) {
                        logger.severe("Internal error: no error dialog service");
                        //send an abnormal response back
                        resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR,
                                "Internal error: no error dialog service");
                        routeMsg(resp);
                    } else {
                        resp = msg;
                        resp.setMsgKind(QIWConstants.DATA_MSG);
                        resp.setContentType(QIWConstants.COMP_DESC_TYPE);
                        resp.setContent(errorService.getMessagingMgr().getMyComponentDesc());
                        MsgUtils.swapCIDs(resp);
                        routeMsg(resp);
                    }

                    return;
                } else if (cmd.equals(QIWConstants.GET_COMP_DESC_CMD)) {
                    response = msg;
                    String compName = (String) msg.getContent();
                    IComponentDescriptor compDesc = getComponentDescByName(compName);
                    response.setMsgKind(QIWConstants.DATA_MSG);
                    response.setContent(compDesc);
                    MsgUtils.swapCIDs(response);
                    routeMsg(response);
                    return;
                } else //?'Activate plugin' or 'Activate viewer' command?
                if (cmd.equals(QIWConstants.ACTIVATE_PLUGIN_CMD) ||
                        cmd.equals(QIWConstants.ACTIVATE_VIEWER_AGENT_CMD) ||
                        cmd.equals(QIWConstants.ACTIVATE_COMPONENT_CMD)) {

                    String displayName = "";
                    String preferredDisplayName = "";

                    if (msg.getContentType().equals(QIWConstants.STRING_TYPE)) //activate a brand new component
                    {
                        displayName = (String) msg.getContent();
                    } else if (msg.getContentType().equals(Node.class.getName())) {// restore a component from the saveset

                        displayName = ((Element) msg.getContent()).getAttribute("componentType");
                        preferredDisplayName = ((Element) msg.getContent()).getAttribute("preferredDisplayName");
                    } else if (msg.getContentType().equals(QIWConstants.ARRAYLIST_TYPE)) {
                        goNestedPlugin(msg);
                        return;
                    }

                    //Get the component's Manifest which contains the name
                    //of its main class and the display name.
                    Manifest mf = null;
                    boolean isCoreComp = true;
                    String jarName = coreJars.get(displayName);
                    if (jarName != null) {
                        mf = coreManifests.get(jarName);
                    } else {
                        isCoreComp = false;
                        jarName = pluginJars.get(displayName);
                        if (jarName != null) {
                            mf = externalManifests.get(jarName);
                        }
                    }

                    boolean launched = true;
                    IComponentDescriptor compDesc = null;
                    if (mf == null) {
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Cannot find Manifest for component " + displayName);
                        launched = false;
                    } else {
                        //Load the component, if necessary, and start its
                        //thread which will register the component with
                        //the Message Dispatcher.
                        String status = ComponentLauncher.getInstance().launchComponent(jarName, mf, isCoreComp, jniClassLoader);

                        // check if component successfully launched, i.e., "cid:..." returned
                        if (!status.toLowerCase().startsWith("cid:")) {
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, status);
                            launched = false;
                        } else {
                            //Send a response that the component was activated. Returned
                            //content is the descriptor of the component
                            //Note: Race condition: component may not have registered
                            //itself yet, so check first.

                            int idx = status.indexOf(":") + 1;
                            String cid = status.substring(idx);
                            if (isRegistered(cid, 3)) {
                                compDesc = getComponentDesc(cid);

                                //record usage event
                                String event = StatsManager.getInstance().createStatsEvent(compDesc.getDisplayName(), ClientConstants.LAUNCHED_ACTION);
                                StatsManager.getInstance().logStatsEvent(event);

                                response = new QiWorkbenchMsg(cid, msg.getProducerCID(), QIWConstants.DATA_MSG, cmd, msg.getMsgID(), QIWConstants.COMP_DESC_TYPE, compDesc, msg.skip());
                                response.setStatusCode(MsgStatus.SC_OK);
                            }
                        }
                    }

                    //If the qiComponent was successfully launched, check if it requires a project manager.
                    //NOTE: ACTIVATE_COMPONENT_CMD may be sent by the State
                    //Manager, which means the component is being restored, or
                    //when a new component is launched. In the former case, do
                    //not auto associate the component with a PM.
                    //NOTE: If restoring a component, the message's content type
                    //will be a DOM Node; otherwise, a String.
                    boolean restoring = cmd.equals(QIWConstants.ACTIVATE_COMPONENT_CMD) &&
                            !msg.getContentType().equals(QIWConstants.STRING_TYPE);
                    if (launched && !restoring) {
                        Attributes attrs = mf.getAttributes(QIWConstants.COMP_ATTR_SECTION_NAME);
                        String pmRequired = attrs.getValue(QIWConstants.PROJECT_MANAGER_ATTR);
                        if (pmRequired != null && pmRequired.equals("Yes")) {
                            //Associate with a PM, but first send response to producer of request
                            routeMsg(response, msg.getProducerCID());
                            final IComponentDescriptor cd = compDesc;

                            Runnable runnable = new Runnable() {

                                public void run() {
                                    WorkbenchManager.getInstance().associateProjMgr(cd);
                                }
                            };
                            new Thread(runnable).start();

                            return;
                        }
                    }
                } else //?'Check if a qiComponent is trusted' command?
                if (cmd.equals(QIWConstants.CHECK_COMPONENT_SECURITY_CMD)) {
                    String displayName = "";

                    if (msg.getContentType().equals(QIWConstants.STRING_TYPE)) {
                        //activate a brand new component
                        displayName = (String) msg.getContent();
                        Manifest mf = null;
                        boolean isCoreComp = true;
                        String jarName = coreJars.get(displayName);
                        if (jarName != null) {
                            mf = coreManifests.get(jarName);
                        } else {
                            isCoreComp = false;
                            jarName = pluginJars.get(displayName);
                            if (jarName != null) {
                                mf = externalManifests.get(jarName);
                            }
                        }
                        if (mf == null) {
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Cannot find Manifest for component " + displayName);
                        } else {
                            //Load the component, if necessary, and start its
                            //thread which will register the component with
                            //the Message Dispatcher.
                            Attributes attrs = mf.getAttributes(QIWConstants.COMP_ATTR_SECTION_NAME);
                            int jniCount = 0;
                            String[] jniJars = null;
                            if (attrs != null) {
                                String dependents = attrs.getValue(QIWConstants.DEPENDENT_JNI_JARS_ATTR);
                                if (dependents != null && dependents.trim().length() > 0) {
                                    jniJars = dependents.split(",");
                                    if (jniJars == null) {
                                        jniJars = dependents.split(" ");
                                    }
                                    if (jniJars != null) {
                                        jniCount = jniJars.length;
                                    }
                                }
                            }
                            List<String> jarList = new ArrayList<String>();
                            jarList.add(jarName);
                            for (int i = 0; jniJars != null && i < jniCount; i++) {
                                String jarPath = getUserHOME() + File.separator + QIWConstants.QIWB_WORKING_DIR + File.separator + QIWConstants.COMP_CACHE_DIR + File.separator + jniJars[i];
                                File jarFile = new File(jarPath);
                                if (jarFile.exists()) {
                                    jarList.add(jniJars[i]);
                                }
                            }

                            boolean granted = false;
                            for (int i = 0; i < jarList.size(); i++) {
                                //boolean granted = ComponentLauncher.getInstance().checkPermissionForComponent(jarName, mf, isCoreComp);
                                logger.info("about to check permission for " + jarList.get(i));
                                granted = ComponentLauncher.getInstance().checkPermissionForComponent(jarList.get(i), mf, isCoreComp);
                                if (!granted) {
                                    logger.info(jarList.get(i) + " not granted");
                                    break;
                                }
                            }
                            //logger.info("Component " + displayName + " granted " + granted);
                            ArrayList list = new ArrayList();
                            list.add(0, displayName);
                            //list.add(1,spaceName);
                            //list.add(2,projectName);
                            if (granted) {
                                list.add(1, "yes");
                            } else {
                                list.add(1, "no");
                            }
                            response = new QiWorkbenchMsg(myCID, msg.getProducerCID(), QIWConstants.DATA_MSG, cmd, msg.getMsgID(), QIWConstants.ARRAYLIST_TYPE, list, msg.skip());
                            response.setStatusCode(MsgStatus.SC_OK);
                        }
                    }
                } else //IO commands: START
                //NOTE: For each IO command, the user must specify whether the IO
                //is local or remote. This preference is passed as the first
                //parameter to the command and remove when the command is changed
                //to a local or remote command.
                if (cmd.equals(QIWConstants.FILE_READ_CMD)) {
                    ArrayList<String> params = (ArrayList<String>) msg.getContent();
                    String locPref = params.get(0);     //local or remote IO

                    String filePath = params.get(1);    //path of file to read
                    //remove the 1st parameter

                    msg.setContentType(QIWConstants.STRING_TYPE);
                    msg.setContent(filePath);
                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);
                    String ioCmd = isLocalPref ? QIWConstants.LOCAL_FILE_READ_CMD : QIWConstants.REMOTE_FILE_READ_CMD;
                    msg.setCommand(ioCmd);

                    // notify the Workbench Manager to add a service node to
                    // the component tree under the requester's node
                    if (addServiceTreeNode) {
                        if (isLocalPref) {
                            addComponentNode(QIWConstants.LOCAL_SERVICE_COMP, QIWConstants.LOCAL_READ_FILE, msg.getProducerCID());
                        } else {
                            addComponentNode(QIWConstants.REMOTE_SERVICE_COMP, QIWConstants.REMOTE_READ_FILE, msg.getProducerCID());
                        }
                    }
                    // Note; no response message required since the request is a notification

                    //TODO: If Tomcat is local and the preference is remote IO,
                    //perform local IO which is more efficient.
                    if (isLocalPref) {
                        // acquire a local IO service (get from pool)
                        IComponentDescriptor ioService = acquireLocalIOservice();

                        if (ioService == null) {
                            logger.severe("Internal error: no local IO service");
                            //send an abnormal response back
                            IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no local IO service");
                            routeMsg(resp);
                            return;
                        }

                        routeToLocalIOService(msg, ioService);
                        return;
                    } else {
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to read remote file. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not read file
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }
                } else if (cmd.equals(QIWConstants.FILE_WRITE_CMD)) {
                    ArrayList<String> params = (ArrayList<String>) msg.getContent();
                    //remove the 1st parameter (location of IO)
                    String locPref = params.remove(0);     //local or remote IO

                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);
                    String ioCmd = isLocalPref ? QIWConstants.LOCAL_FILE_WRITE_CMD : QIWConstants.REMOTE_FILE_WRITE_CMD;
                    msg.setCommand(ioCmd);

                    // notify the Workbench Manager to add a service node to
                    // the component tree under the requester's node
                    if (addServiceTreeNode) {
                        if (isLocalPref) {
                            addComponentNode(QIWConstants.LOCAL_SERVICE_COMP, QIWConstants.LOCAL_WRITE_FILE, msg.getProducerCID());
                        } else {
                            addComponentNode(QIWConstants.REMOTE_SERVICE_COMP, QIWConstants.REMOTE_WRITE_FILE, msg.getProducerCID());
                        }
                    }
                    // Note; no response message required since the request is a notification

                    //TODO: If Tomcat is local and the preference is remote IO,
                    //perform local IO which is more efficient.
                    if (isLocalPref) {
                        // acquire a local IO service (get from pool)
                        IComponentDescriptor ioService = acquireLocalIOservice();

                        if (ioService == null) {
                            logger.severe("Internal error: no local IO service");
                            //send an abnormal response back
                            IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no local IO service");
                            routeMsg(resp);
                            return;
                        }

                        routeToLocalIOService(msg, ioService);
                        return;
                    } else {
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to write remote file. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not write file
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }
                } else if (cmd.equals(QIWConstants.BINARY_FILE_READ_CMD)) {
                    ArrayList<String> params = (ArrayList<String>) msg.getContent();
                    String locPref = params.get(0);     //local or remote IO

                    //String filePath = params.get(1);    //path of file to read
                    //remove the 1st parameter
                    //msg.setContentType(QIWConstants.STRING_TYPE);
                    msg.setContentType(QIWConstants.ARRAYLIST_TYPE);
                    //msg.setContent(filePath);
                    params.remove(0);
                    msg.setContent(params);

                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);
                    String ioCmd = isLocalPref ? QIWConstants.LOCAL_BINARY_FILE_READ_CMD : QIWConstants.REMOTE_BINARY_FILE_READ_CMD;
                    msg.setCommand(ioCmd);
                    //ArrayList<String> params = (ArrayList<String>)msg.getContent();
                    //String locPref = params.get(0);     //local or remote IO
                    //String filePath = params.get(1);    //path of file to read
                    //remove the 1st parameter
                    //msg.setContentType(QIWConstants.STRING_TYPE);
                    //msg.setContent(filePath);
                    //boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);
                    //String ioCmd = isLocalPref ? QIWConstants.LOCAL_BINARY_FILE_READ_CMD : QIWConstants.REMOTE_FILE_READ_CMD;
                    //msg.setCommand(ioCmd);

                    // notify the Workbench Manager to add a service node to
                    // the component tree under the requester's node
                    if (addServiceTreeNode) {
                        if (isLocalPref) {
                            addComponentNode(QIWConstants.LOCAL_SERVICE_COMP, QIWConstants.LOCAL_READ_BINARY_FILE, msg.getProducerCID());
                        } else {
                            addComponentNode(QIWConstants.REMOTE_SERVICE_COMP, QIWConstants.REMOTE_READ_BINARY_FILE, msg.getProducerCID());
                        }
                    }
                    // Note; no response message required since the request is a notification

                    //TODO: If Tomcat is local and the preference is remote IO,
                    //perform local IO which is more efficient.
                    if (isLocalPref) {
                        // acquire a local IO service (get from pool)
                        IComponentDescriptor ioService = acquireLocalIOservice();

                        if (ioService == null) {
                            logger.severe("Internal error: no local IO service");
                            //send an abnormal response back
                            IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no local IO service");
                            routeMsg(resp);
                            return;
                        }

                        routeToLocalIOService(msg, ioService);
                        return;
                    } else {
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to read remote binary file. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not read file
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }
                } else if (cmd.equals(QIWConstants.BINARY_FILE_WRITE_CMD)) {
                    ArrayList params = (ArrayList) msg.getContent();
                    //remove the 1st parameter (location of IO)
                    String locPref = (String) params.remove(0);     //local or remote IO

                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);
                    String ioCmd = isLocalPref ? QIWConstants.LOCAL_BINARY_FILE_WRITE_CMD : QIWConstants.REMOTE_BINARY_FILE_WRITE_CMD;
                    msg.setCommand(ioCmd);

                    // notify the Workbench Manager to add a service node to
                    // the component tree under the requester's node
                    if (addServiceTreeNode) {
                        if (isLocalPref) {
                            addComponentNode(QIWConstants.LOCAL_SERVICE_COMP, QIWConstants.LOCAL_WRITE_BINARY_FILE, msg.getProducerCID());
                        } else {
                            addComponentNode(QIWConstants.REMOTE_SERVICE_COMP, QIWConstants.REMOTE_WRITE_BINARY_FILE, msg.getProducerCID());
                        }
                    }
                    // Note; no response message required since the request is a notification

                    //TODO: If Tomcat is local and the preference is remote IO,
                    //perform local IO which is more efficient.
                    if (isLocalPref) {
                        // acquire a local IO service (get from pool)
                        IComponentDescriptor ioService = acquireLocalIOservice();

                        if (ioService == null) {
                            logger.severe("Internal error: no local IO service");
                            //send an abnormal response back
                            IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no local IO service");
                            routeMsg(resp);
                            return;
                        }

                        routeToLocalIOService(msg, ioService);
                        return;
                    } else {
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to write binary data to remote file. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not write binary file
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }
                } else if (cmd.equals(QIWConstants.READ_SEGY_DATA_CMD) ||
                        cmd.equals(QIWConstants.READ_BHPSU_DATA_CMD)) {
                    boolean isSegy = cmd.equals(QIWConstants.READ_SEGY_DATA_CMD);
                    ArrayList<String> params = (ArrayList<String>) msg.getContent();
                    String locPref = params.get(0);     //local or remote IO

                    //remove the 1st parameter
                    params.remove(0);
                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);
                    String ioCmd = "";
                    if (isSegy) {
                        ioCmd = isLocalPref ? QIWConstants.LOCAL_READ_SEGY_DATA_CMD : QIWConstants.REMOTE_READ_SEGY_DATA_CMD;
                    } else {
                        ioCmd = isLocalPref ? QIWConstants.LOCAL_READ_BHPSU_DATA_CMD : QIWConstants.REMOTE_READ_BHPSU_DATA_CMD;
                    }
                    msg.setCommand(ioCmd);
                    
                    if (!isLocalPref) {
                        //Add the workbench session ID as the last parameter
                        params.add(WorkbenchManager.getInstance().getWorkbenchSessionID());
                    }

                    // notify the Workbench Manager to add a service node to
                    // the component tree under the requester's node
                    if (addServiceTreeNode) {
                        if (isLocalPref) {
                            if (isSegy) {
                                addComponentNode(QIWConstants.LOCAL_SERVICE_COMP, QIWConstants.LOCAL_READ_SEGY_DATA, msg.getProducerCID());
                            } else {
                                addComponentNode(QIWConstants.LOCAL_SERVICE_COMP, QIWConstants.LOCAL_READ_BHPSU_DATA, msg.getProducerCID());
                            }
                        } else {
                            if (isSegy) {
                                addComponentNode(QIWConstants.REMOTE_SERVICE_COMP, QIWConstants.REMOTE_READ_SEGY_DATA, msg.getProducerCID());
                            } else {
                                addComponentNode(QIWConstants.REMOTE_SERVICE_COMP, QIWConstants.REMOTE_READ_BHPSU_DATA, msg.getProducerCID());
                            }
                        }
                    }
                    // Note; no response message required since the request is a notification

                    if (isLocalPref) {
                        //TODO: implement local IO service
                        // acquire a local IO service (get from pool)
                        IComponentDescriptor ioService = acquireLocalIOservice();

                        if (ioService == null) {
                            logger.severe("Internal error: no local IO service");
                            //send an abnormal response back
                            IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no local IO service");
                            routeMsg(resp);
                            return;
                        }

                        routeToLocalIOService(msg, ioService);
                        return;
                    } else {
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent());

                        response = DispatcherConnector.getInstance().sendRequestMsg(request);

                        if (!response.isAbnormalStatus()) {
                            //Check if the response contains the server's URL and
                            //assigned port
                            if (response.getMsgKind().equals(QIWConstants.DATA_CMD_MSG)) {
                                // acquire a local IO service (get from pool) to handle the
                                // receiving end of the socket channel the server is writing
                                // the requested SEGY/BHP-SU data to.
                                IComponentDescriptor ioService = acquireLocalIOservice();

                                if (ioService == null) {
                                    logger.severe("Internal error: no local IO service");
                                    //send an abnormal response back
                                    IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no local IO service");
                                    routeMsg(resp);
                                    return;
                                }

                                //Replace server URL returned in the response with the URL selected in the Server
                                //Selector dialog, because the returned URL is http://localhost:8080.
                                ArrayList<String> content = (ArrayList<String>) response.getContent();
                                content.set(0, getTomcatURL());

                                //make the IO service request appear to come from the
                                //original requester.
                                response.setConsumerCID(myCID);
                                response.setProducerCID(msg.getProducerCID());
                                response.setMsgID(msg.getMsgID());
                                response.setSkip(msg.skip());
                                //make response into a request
                                response.setMsgKind(QIWConstants.CMD_MSG);
                                //local IO service will handle receiving end of the socket channel
                                routeToLocalIOService(response, ioService);
                                return;
                            } else {
                                response.setConsumerCID(msg.getProducerCID());
                                response.setProducerCID(myCID);
                                response.setMsgID(msg.getMsgID());
                            }
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = isSegy ? "Severe: Unable to read remote SEGY file. CAUSE:" + cause
                                    : "Severe: Unable to read remote BHP-SU file set. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not read file
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }
                } else if (cmd.equals(QIWConstants.GET_DIR_FILE_LIST_CMD)) {
                    ArrayList<String> params = (ArrayList<String>) msg.getContent();
                    String locPref = params.get(0);    //local or remote IO

                    String dirPath = params.get(1);    //path of directory
                    //remove the 1st parameter

                    msg.setContentType(QIWConstants.STRING_TYPE);
                    msg.setContent(dirPath);
                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);
                    String ioCmd = isLocalPref ? QIWConstants.GET_LOCAL_DIR_FILE_LIST_CMD : QIWConstants.GET_REMOTE_DIR_FILE_LIST_CMD;
                    msg.setCommand(ioCmd);

                    //TODO: If Tomcat is local and the preference is remote IO,
                    //perform local IO which is more efficient.
                    if (isLocalPref) {
                        // acquire a local IO service (get from pool)
                        IComponentDescriptor ioService = acquireLocalIOservice();

                        if (ioService == null) {
                            logger.severe("Internal error: no local IO service");
                            //send an abnormal response back
                            IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no local IO service");
                            routeMsg(resp);
                            return;
                        }

                        routeToLocalIOService(msg, ioService);
                        return;
                    } else {
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to get remote file list. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not get file list
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }
                } else if (cmd.equals(QIWConstants.CHECK_FILE_EXIST_CMD) || cmd.equals(QIWConstants.DELETE_LOCAL_FILE_CMD)) {
                    ArrayList<String> params = (ArrayList<String>) msg.getContent();
                    String locPref = params.get(0);    //local or remote IO

                    String dirPath = params.get(1);    //path of directory
                    //remove the 1st parameter

                    msg.setContentType(QIWConstants.STRING_TYPE);
                    msg.setContent(dirPath);
                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);
                    String ioCmd = isLocalPref ? QIWConstants.CHECK_LOCAL_FILE_EXIST_CMD : QIWConstants.CHECK_REMOTE_FILE_EXIST_CMD;
                    msg.setCommand(ioCmd);

                    //TODO: If Tomcat is local and the preference is remote IO,
                    //perform local IO which is more efficient.
                    if (isLocalPref) {
                        // acquire a local IO service (get from pool)
                        IComponentDescriptor ioService = acquireLocalIOservice();

                        if (ioService == null) {
                            logger.severe("Internal error: no local IO service");
                            //send an abnormal response back
                            IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no local IO service");
                            routeMsg(resp);
                            return;
                        }

                        routeToLocalIOService(msg, ioService);
                        return;
                    } else {
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to check the existence of remote file " + msg.getContent() + " CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not get file list
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }
                } else if (cmd.equals(QIWConstants.COPY_FILE_CMD) || cmd.equals(QIWConstants.MOVE_FILE_CMD)) {
                    ArrayList<String> params = (ArrayList<String>) msg.getContent();
                    String locPref = params.get(0);    //local or remote IO

                    String inFilePath = params.get(1);    //path of directory

                    String outFilePath = params.get(2);    //path of directory
                    //remove the 1st parameter

                    msg.setContentType(QIWConstants.ARRAYLIST_TYPE);
                    params.remove(0);
                    msg.setContent(params);
                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);
                    String ioCmd = "";
                    if (cmd.equals(QIWConstants.COPY_FILE_CMD)) {
                        ioCmd = isLocalPref ? QIWConstants.COPY_LOCAL_FILE_CMD : QIWConstants.COPY_REMOTE_FILE_CMD;
                    } else if (cmd.equals(QIWConstants.MOVE_FILE_CMD)) {
                        ioCmd = isLocalPref ? QIWConstants.MOVE_LOCAL_FILE_CMD : QIWConstants.MOVE_REMOTE_FILE_CMD;
                    }
                    msg.setCommand(ioCmd);

                    //TODO: If Tomcat is local and the preference is remote IO,
                    //perform local IO which is more efficient.
                    if (isLocalPref) {
                        // acquire a local IO service (get from pool)
                        IComponentDescriptor ioService = acquireLocalIOservice();

                        if (ioService == null) {
                            logger.severe("Internal error: no local IO service");
                            //send an abnormal response back
                            IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no local IO service");
                            routeMsg(resp);
                            return;
                        }

                        routeToLocalIOService(msg, ioService);
                        return;
                    } else {
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to check the existence of remote file " + msg.getContent() + " CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not get file list
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }
                } else if (cmd.equals(QIWConstants.CREATE_DIRECTORY_CMD)) {
                    ArrayList<String> params = (ArrayList<String>) msg.getContent();
                    String locPref = params.get(0);    //local or remote IO

                    String dirPath = params.get(1);    //path of directory
                    //remove the 1st parameter

                    msg.setContentType(QIWConstants.STRING_TYPE);
                    msg.setContent(dirPath);
                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);
                    String ioCmd = isLocalPref ? QIWConstants.CREATE_LOCAL_DIRECTORY_CMD : QIWConstants.CREATE_REMOTE_DIRECTORY_CMD;
                    msg.setCommand(ioCmd);

                    //TODO: If Tomcat is local and the preference is remote IO,
                    //perform local IO which is more efficient.
                    if (isLocalPref) {
                        // acquire a local IO service (get from pool)
                        IComponentDescriptor ioService = acquireLocalIOservice();

                        if (ioService == null) {
                            logger.severe("Internal error: no local IO service");
                            //send an abnormal response back
                            IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no local IO service");
                            routeMsg(resp);
                            return;
                        }

                        routeToLocalIOService(msg, ioService);
                        return;
                    } else {
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to creat a remote directory " + msg.getContent() + ". CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not get file list
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }
                } else if (cmd.equals(QIWConstants.GET_FILE_LIST_CMD)) {
                    ArrayList<String> params = (ArrayList<String>) msg.getContent();
                    String locPref = params.get(0);    //local or remote IO

                    String dirPath = params.get(1);    //path of directory
                    //remove the 1st parameter

                    msg.setContentType(QIWConstants.STRING_TYPE);
                    msg.setContent(dirPath);
                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);
                    String ioCmd = isLocalPref ? QIWConstants.GET_LOCAL_FILE_LIST_CMD : QIWConstants.GET_REMOTE_FILE_LIST_CMD;
                    msg.setCommand(ioCmd);

                    // notify the Workbench Manager to add a service node to
                    // the component tree under the requester's node
                    if (addServiceTreeNode) {
                        if (isLocalPref) {
                            addComponentNode(QIWConstants.LOCAL_SERVICE_COMP, QIWConstants.LOCAL_GET_FILE_LIST, msg.getProducerCID());
                        } else {
                            addComponentNode(QIWConstants.REMOTE_SERVICE_COMP, QIWConstants.REMOTE_GET_FILE_LIST, msg.getProducerCID());
                        }
                    }
                    // Note; no response message required since the request is a notification

                    //TODO: If Tomcat is local and the preference is remote IO,
                    //perform local IO which is more efficient.
                    if (isLocalPref) {
                        // acquire a local IO service (get from pool)
                        IComponentDescriptor ioService = acquireLocalIOservice();

                        if (ioService == null) {
                            logger.severe("Internal error: no local IO service");
                            //send an abnormal response back
                            IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no local IO service");
                            routeMsg(resp);
                            return;
                        }

                        routeToLocalIOService(msg, ioService);
                        return;
                    } else {
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to get remote file list. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not get file list
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }

                } else if (cmd.equals(QIWConstants.GET_UNMADE_QIPROJ_DESC_DIRS_CMD)) {
                    ArrayList params = (ArrayList) msg.getContent();
                    String locPref = (String) params.get(0);     //local or remote IO

                    QiProjectDescriptor qiProjDesc = (QiProjectDescriptor) params.get(1);    //QiProjectDescriptor
                    //remove the 1st parameter

                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);

                    //TODO: If Tomcat is local and the preference is remote IO,
                    //perform local IO which is more efficient.
                    if (isLocalPref) {
                        // acquire a local IO service (get from pool)
                        List<File> list = getUnMadeLocalDirs(qiProjDesc, getServerOSFileSeparator());
                        List<String> listString = new ArrayList<String>();
                        for (int i = 0; list != null && i < list.size(); i++) {
                            listString.add(list.get(i).getAbsolutePath());
                        }
                        IQiWorkbenchMsg resp = new QiWorkbenchMsg(myCID, msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(), msg.getMsgID(), QIWConstants.ARRAYLIST_TYPE, listString, msg.skip());
                        routeMsg(resp);
                        return;
                    } else {
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), QiProjectDescriptor.class.getName(), qiProjDesc, msg.skip());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to read remote file. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not read file
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }
                } else if (cmd.equals(QIWConstants.CREATE_DIRECTORIES_CMD)) {
                    ArrayList params = (ArrayList) msg.getContent();
                    String locPref = (String) params.get(0);     //local or remote IO

                    List<String> dirs = (List<String>) params.get(1);    //QiProjectDescriptor
                    //remove the 1st parameter

                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);

                    //TODO: If Tomcat is local and the preference is remote IO,
                    //perform local IO which is more efficient.
                    if (isLocalPref) {
                        // acquire a local IO service (get from pool)
                        IQiWorkbenchMsg resp;
                        try {
                            boolean pass = true;
                            for (int i = 0; dirs != null && i < dirs.size(); i++) {
                                if (!new File(dirs.get(i)).mkdirs()) {
                                    pass = false;
                                    break;
                                }
                            }
                            if (pass) {
                                resp = new QiWorkbenchMsg(myCID,
                                        msg.getProducerCID(), QIWConstants.DATA_MSG, msg.getCommand(),
                                        msg.getMsgID(), QIWConstants.STRING_TYPE, "success", msg.skip());
                            } else {
                                resp = new QiWorkbenchMsg(myCID,
                                        msg.getProducerCID(), QIWConstants.STRING_TYPE, msg.getCommand(),
                                        msg.getMsgID(), Boolean.class.getName(), "failure", msg.skip());
                            }
                        } catch (Exception qioe) {
                            //send an abnormal response back
                            resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: " + qioe.getMessage());
                        }
                        routeMsg(resp);
                        return;
                    } else {
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, dirs, msg.skip());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to read remote file. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not read file
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }

                } else if (cmd.equals(QIWConstants.IS_REMOTE_DIRECTORY_CMD)) {
                    IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
                    response = DispatcherConnector.getInstance().sendRequestMsg(request);
                    if (!response.isAbnormalStatus()) {
                        response.setConsumerCID(msg.getProducerCID());
                        response.setProducerCID(myCID);
                        response.setMsgID(msg.getMsgID());
                    } else {
                        String cause = (String) response.getContent();
                        errorMsg = "Severe: Unable to determine if a directory. CAUSE:" + cause;
                        logger.severe(errorMsg);
                        //notify requester could not determine if path is a directory
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                    }
                //Note: response will be routed at the end of if-then-else chain
                } else if (cmd.equals(QIWConstants.CREATE_REMOTE_PROJECT_CMD)) {
                    IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
                    response = DispatcherConnector.getInstance().sendRequestMsg(request);
                    if (!response.isAbnormalStatus()) {
                        response.setConsumerCID(msg.getProducerCID());
                        response.setProducerCID(myCID);
                        response.setMsgID(msg.getMsgID());
                    } else {
                        String cause = (String) response.getContent();
                        errorMsg = "Unable to create remote qiProject. CAUSE:" + cause;
                        logger.severe(errorMsg);
                        //notify requester could not create the project
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                    }
                //Note: response will be routed at the end of if-then-else chain
                } else if (cmd.equals(QIWConstants.READ_GEOFILE_CMD)) {
                    ArrayList params = (ArrayList) msg.getContent();
                    String locPref = (String) params.get(0);     //local or remote IO
                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);
                    
                    //Add the workbench session ID as the last parameter
                    params.add(WorkbenchManager.getInstance().getWorkbenchSessionID());

                    // notify the Workbench Manager to add a service node to
                    // the component tree under the requester's node
                    if (addServiceTreeNode) {
                        if (isLocalPref) {
                            addComponentNode(QIWConstants.LOCAL_SERVICE_COMP, QIWConstants.READ_GEOFILE_CMD, msg.getProducerCID());
                        } else {
                            addComponentNode(QIWConstants.REMOTE_SERVICE_COMP, QIWConstants.READ_GEOFILE_CMD, msg.getProducerCID());
                        }
                    }
                    // Note; no response message required since the request is a notification

                    // acquire a client-side geoIO service (get from pool)
                    IComponentDescriptor geoIoService = acquireLocalGeoIOservice();
                    if (geoIoService == null) {
                        logger.severe("Internal error: no local geoIO service");
                        //send an abnormal response back
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: unable to obtain a client-side geoIO service");
                        routeMsg(response);
                        return;
                    }

                    //Route the request to the client-side geoIO Service for processing.
                    // Note: The client-side geoIO Service will process the request.
                    // It will either send back to the requester an object containing the
                    // requested geoData, e.g., GeoFileMetadata, or a GeoIoResult.
                    routeToLocalGeoIOService(msg, geoIoService);
                } else if (cmd.equals(QIWConstants.WRITE_GEOFILE_CMD)) {
                    ArrayList params = (ArrayList) msg.getContent();
                    String locPref = (String) params.get(0);     //local or remote IO
                    String geoFormat = (String)params.get(1);
                    String geoDataType = (String)params.get(3);

                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);
                    
                    //Add the workbench session ID as the last parameter
                    params.add(WorkbenchManager.getInstance().getWorkbenchSessionID());

                    // notify the Workbench Manager to add a service node to
                    // the component tree under the requester's node
                    if (addServiceTreeNode) {
                        if (isLocalPref) {
                            addComponentNode(QIWConstants.LOCAL_SERVICE_COMP, QIWConstants.WRITE_GEOFILE_CMD, msg.getProducerCID());
                        } else {
                            addComponentNode(QIWConstants.REMOTE_SERVICE_COMP, QIWConstants.WRITE_GEOFILE_CMD, msg.getProducerCID());
                        }
                    }
                    // Note; no response message required since the request is a notification
                    
                    if (geoFormat.equals(FileSystemConstants.BHP_SU_FORMAT) &&
                        geoDataType.equals(FileSystemConstants.HORIZON_DATA)) {
                        //Note: Writing out horizon handled by server-side
                        //GeoIOService.
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Unable to write horizon. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not write the horizon
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }
                    else {
                        // acquire a client-side geoIO service (get from pool)
                        IComponentDescriptor geoIoService = acquireLocalGeoIOservice();
                        if (geoIoService == null) {
                            logger.severe("Internal error: no local geoIO service");
                            //send an abnormal response back
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: unable to obtain a client-side geoIO service");
                            routeMsg(response);
                            return;
                        }
                        //Note: The client-side geoIO Service will send a response back
                        //to the client requesting the service. The response will contain
                        //the CID of the service and a reference handle to its instance.
                        //The CID is for when the client send a message to the MessageDispatcher
                        //requesting the service be released. The reference handle is so
                        //the client can call service IO methods.
                        routeToLocalGeoIOService(msg, geoIoService);
    
                        //If the IO preference is remote, obtain a server-side geoIO
                        //service and pair it with the client-side geoIO service. Save
                        //the CID of the server-side service in the client-side service.
        /* Note: client-side geoIO Service gets server-side geoIO Service directly from the Servlet Dispatcher. ONCE CODE TESTED, REMOVE THIS COMMENTED OUT CODE.
                        if (!isLocalPref) {
                        ArrayList<String> args = (ArrayList<String>)msg.getContent();
                        args.set(0, geoIoService.getCID());
                        args.add(QIWConstants.REMOTE_WRITE_FILE);
                        //Note: The request is on behalf of the client-side service
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, QIWConstants.GET_REMOTE_GEOIO_SERVICE_CMD, MsgUtils.genMsgID(), msg.getContentType(), args ,msg.skip());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        //route the response to the client-side service
                        if (!response.isAbnormalStatus()) {
                        response.setConsumerCID(geoIoService.getCID());
                        response.setProducerCID(myCID);
                        response.setMsgID(msg.getMsgID());
                        } else {
                        String cause = (String)response.getContent();
                        errorMsg = "Internal error: unable to obtain a server-side geoIO service. CAUSE:"+cause;
                        logger.severe(errorMsg);
                        //notify requester could not obtain a server-side service
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                        //Note: response will be routed at the end of if-then-else chain
                        }
                         */
                    }
                } else if (cmd.equals(QIWConstants.RELEASE_GEOIO_SERVICE_CMD)) {
                    IQiWorkbenchMsg request = checkForMatchingRequest(msg);

                    // release client-side geoIO service (return to pool)
                    ArrayList<String> params = (ArrayList<String>) msg.getContent();
                    String geoIoServiceCID = params.get(0);
                    releaseLocalGeoIOservice(geoIoServiceCID);

                    //TODO: release paired server-side geoIO Service
                    geoIoServiceCID = params.get(1);

                    return;
                } else //IO commands: END
                //Job commands: START
                if (cmd.equals(QIWConstants.SUBMIT_JOB_CMD)) {
                    ArrayList<String> params = (ArrayList<String>) msg.getContent();
                    //remove the 1st parameter (location of job processor)
                    String locPref = params.remove(0);
                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);

                    // notify the Workbench Manager to add a service node to
                    // the component tree under the requester's node
                    if (addServiceTreeNode) {
                        if (isLocalPref) {
                            addComponentNode(QIWConstants.LOCAL_SERVICE_COMP, QIWConstants.LOCAL_JOB_SERVICE_NAME, msg.getProducerCID());
                        } else {
                            addComponentNode(QIWConstants.REMOTE_SERVICE_COMP, QIWConstants.REMOTE_JOB_SERVICE_NAME, msg.getProducerCID());
                        }
                    }
                    // Note; no response message required since the request is a notification

                    //TODO: If Tomcat is local and the preference is remote job,
                    //perform local job which is more efficient.
                    if (isLocalPref) {
                        // acquire an inactive local job service
                        JobService jobService = acquireLocalJobService();

                        if (jobService == null) {
                            logger.severe("Internal error: no local job service");
                            //send an abnormal response back
                            IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no local job service");
                            routeMsg(resp);
                            return;
                        }

                        // create a job ID for the job service
                        String jobID = ComponentUtils.genJobID();
                        jobService.setJobID(jobID);
                        // add to the list of active job services
                        activeLocalJobServices.put(jobID, jobService);

                        routeToLocalJobService(msg, jobService);

                        return;
                    } else {
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to submit job. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not submit job
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }
                } else if (cmd.equals(QIWConstants.CREATE_JOB_CMD)) {
                    String locPref = (String) msg.getContent();
                    //remove the location of job processor
                    msg.setContentType("");
                    msg.setContent("");
                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);

                    // notify the Workbench Manager to add a service node to
                    // the component tree under the requester's node
                    if (addServiceTreeNode) {
                        if (isLocalPref) {
                            addComponentNode(QIWConstants.LOCAL_SERVICE_COMP, QIWConstants.LOCAL_JOB_SERVICE_NAME, msg.getProducerCID());
                        } else {
                            addComponentNode(QIWConstants.REMOTE_SERVICE_COMP, QIWConstants.REMOTE_JOB_SERVICE_NAME, msg.getProducerCID());
                        }
                    }
                    // Note; no response message required since the request is a notification

                    //TODO: If Tomcat is local and the preference is remote job,
                    //perform local job which is more efficient.
                    if (isLocalPref) {
                        // acquire an inactive local job service
                        JobService jobService = acquireLocalJobService();

                        if (jobService == null) {
                            logger.severe("Internal error: no local job service");
                            //send an abnormal response back
                            IQiWorkbenchMsg resp = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: no local job service");
                            routeMsg(resp);
                            return;
                        }

                        // create a job ID for the job service
                        String jobID = ComponentUtils.genJobID();
                        jobService.setJobID(jobID);

                        // add to the list of active job services
                        activeLocalJobServices.put(jobID, jobService);

                        //Note: No reason to pass message to the job service
                        //for the request has been satisfied when acquiring
                        //a local job service.

                        // send a normal response back with the job ID
                        response = new QiWorkbenchMsg(myCID, msg.getProducerCID(), QIWConstants.DATA_MSG, cmd, msg.getMsgID(), QIWConstants.STRING_TYPE, jobID);
                        response.setStatusCode(MsgStatus.SC_OK);
                    } else {
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to create job. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not create job
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }
                } else if (cmd.equals(QIWConstants.WAIT_FOR_JOB_EXIT_CMD) ||
                        cmd.equals(QIWConstants.GET_JOB_STATUS_CMD) ||
                        cmd.equals(QIWConstants.GET_JOB_OUTPUT_CMD) ||
                        cmd.equals(QIWConstants.GET_JOB_ERROR_OUTPUT_CMD) ||
                        cmd.equals(QIWConstants.RELEASE_JOB_CMD) ||
                        cmd.equals(QIWConstants.EXECUTE_JOB_CMD) ||
                        cmd.equals(QIWConstants.KILL_JOB_CMD)) {
                    ArrayList<String> params = (ArrayList<String>) msg.getContent();
                    String locPref = params.get(0);  //local or remote job

                    String jobID = params.get(1);
                    //remove the 1st parameter
                    msg.setContentType(QIWConstants.STRING_TYPE);
                    msg.setContent(jobID);
                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);

                    //TODO: If Tomcat is local and the preference is remote job,
                    //perform local job which is more efficient.
                    if (isLocalPref) {
                        JobService jobService = (JobService) activeLocalJobServices.get(jobID);

                        routeToLocalJobService(msg, jobService);
                        return;
                    } else {
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent(), msg.skip());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to access job. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not access job
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }
                } else if (cmd.equals(QIWConstants.SET_JOB_COMMAND_CMD) ||
                        cmd.equals(QIWConstants.SET_JOB_WORKING_DIR_CMD) ||
                        cmd.equals(QIWConstants.SET_JOB_ENV_VAR_CMD)) {
                    ArrayList<String> params = (ArrayList<String>) msg.getContent();
                    String locPref = params.remove(0); //local or remote job

                    String jobID = params.get(0);    //job ID

                    boolean isLocalPref = locPref.equals(QIWConstants.LOCAL_SERVICE_PREF);

                    //TODO: If Tomcat is local and the preference is remote job,
                    //perform local job which is more efficient.
                    if (isLocalPref) {
                        JobService jobService = (JobService) activeLocalJobServices.get(jobID);

                        routeToLocalJobService(msg, jobService);
                        return;
                    } else {
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent());
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to config job. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not configure job
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    //Note: response will be routed at the end of if-then-else chain
                    }
                } else //Job commands: END
                if (cmd.equals(QIWConstants.GET_CID_CMD)) {
                    // get the CID of a registered component based on its name
                    String compName = (String) msg.getContent();
                    String compCID = getCidByName(compName);
                    if (compCID.equals("")) {
                        //couldn't find CID; component not registered
                        errorMsg = "Internal Error: Component not registered (active). name:" + compName;
                        logger.severe(errorMsg);
                        //notify requester could not get CID
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, errorMsg);
                    } else {
                        // send a response back
                        response = new QiWorkbenchMsg(myCID, msg.getProducerCID(), QIWConstants.DATA_MSG, "", msg.getMsgID(), QIWConstants.STRING_TYPE, compCID);
                        response.setStatusCode(MsgStatus.SC_OK);
                    }
                } else if (cmd.equals(QIWConstants.GET_USER_PREFERENCE_CMD)) {
                    // Get user's preferences
                    if (PreferenceUtils.prefFileExists(prefFileDir)) {
                        // get user's preferences saved in the preference file
                        try {
                            userPrefs = PreferenceUtils.readPrefs(prefFileDir);
                            // send a response back
                            response = new QiWorkbenchMsg(myCID, msg.getProducerCID(), QIWConstants.DATA_MSG, "", msg.getMsgID(), QiwbPreferences.class.getName(), userPrefs, msg.skip());
                            response.setStatusCode(MsgStatus.SC_OK);
                        } catch (QiwIOException qioe) {
                            //TODO notify user cannot read preference file
                            logger.finest(qioe.getMessage());
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, qioe.getMessage());
                        }
                    }
                } else if (cmd.equals(QIWConstants.GET_LOCAL_SERVICES_CMD)) {
                    // already have the list since generated by the Message
                    // Dispatcher during initialization
                    response = new QiWorkbenchMsg(myCID, msg.getProducerCID(), QIWConstants.DATA_MSG, cmd, msg.getMsgID(), QIWConstants.ARRAYLIST_TYPE, localServices);
                    response.setStatusCode(MsgStatus.SC_OK);
                } else if (cmd.equals(QIWConstants.GET_REMOTE_SERVICES_CMD)) {
                    // list static, so check if already have list
                    if (remoteServices != null) {
                        response = new QiWorkbenchMsg(myCID, msg.getProducerCID(), QIWConstants.DATA_MSG, cmd, msg.getMsgID(), QIWConstants.ARRAYLIST_TYPE, remoteServices);
                        response.setStatusCode(MsgStatus.SC_OK);
                    } else {
                        // get the list of remote services (descriptors) and retain
                        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(), QIWConstants.CMD_MSG, QIWConstants.GET_REMOTE_SERVICES_CMD, MsgUtils.genMsgID(), "", "");
                        response = DispatcherConnector.getInstance().sendRequestMsg(request);
                        if (!response.isAbnormalStatus()) {
                            remoteServices = (ArrayList<IComponentDescriptor>) response.getContent();
                            response.setConsumerCID(msg.getProducerCID());
                            response.setProducerCID(myCID);
                            response.setMsgID(msg.getMsgID());
                            logger.config("Obtained list of remote services");
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to get list of remote services. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not get list
                            MsgUtils.swapCIDs(response);
                            response.setContent(cause);
                        }
                    }
                } else if (cmd.equals(QIWConstants.GET_PLUGINS_CMD)) {
                    response = new QiWorkbenchMsg(myCID, msg.getProducerCID(), QIWConstants.DATA_MSG, cmd, msg.getMsgID(), QIWConstants.ARRAYLIST_TYPE, availablePlugins);
                    response.setStatusCode(MsgStatus.SC_OK);
                    logger.config("Obtained list of available plugins");
                } else if (cmd.equals(QIWConstants.GET_VIEWER_AGENTS_CMD)) {
                    response = new QiWorkbenchMsg(myCID, msg.getProducerCID(), QIWConstants.DATA_MSG, cmd, msg.getMsgID(), QIWConstants.ARRAYLIST_TYPE, availableViewers);
                    response.setStatusCode(MsgStatus.SC_OK);
                    logger.config("Obtained list of available viewer agents");
                } else // Save qiSpace Command
                if (cmd.equals(QIWConstants.SAVE_QISPACE_CMD)) {
                    QiwbPreferences userPrefs = getUserPrefs();
                    String selectedServer = getTomcatURL();
                    String defaultQispace = userPrefs.getDefaultQispace(selectedServer);
                    QiSpaceDescriptor qsDesc = (QiSpaceDescriptor) msg.getContent();
                    if ((defaultQispace != null) && !defaultQispace.equals(qsDesc.getPath())) {
                        userPrefs.addQispace(selectedServer, qsDesc);
                    }
                    try {
                        PreferenceUtils.writePrefs(userPrefs, prefFileDir);
                    } catch (QiwIOException qioe) {
                        qioe.printStackTrace();
                    }

                    //TODO: Project is the qiSpace until there is a qiProjectManager
                    qispace = qsDesc;
                    qiProject = qsDesc.getPath();
                } else if (cmd.equals(QIWConstants.PING_CMD)) {
                    // send a response back
                    response = new QiWorkbenchMsg(myCID, msg.getProducerCID(), QIWConstants.DATA_MSG, "", msg.getMsgID(), QIWConstants.STRING_TYPE, "Reply from Message Dispatcher: ping acknowledged");
                } else if (cmd.equals(QIWConstants.NOTIFY_PROJ_INFO_CHANGED_CMD)) {
                    //broadcast notification to all non-PM qiComponents
                    ArrayList params = (ArrayList) msg.getContent();

                    Set<String> keys = componentRegister.keySet();
                    Iterator<String> iter = keys.iterator();
                    while (iter.hasNext()) {
                        String compCid = iter.next();
                        IComponentDescriptor compDesc = componentRegister.get(compCid);
                        String compKind = compDesc.getComponentKind();
                        boolean notifyComp = compKind.equals(QIWConstants.COMPONENT_PLUGIN_TYPE) ||
                                compKind.equals(QIWConstants.COMPONENT_VIEWER_TYPE) ||
                                compKind.equals(QIWConstants.VIEWER_AGENT_COMP) ||
                                compKind.equals(QIWConstants.PLUGIN_AGENT_COMP);
                        if (notifyComp) {
                            IQiWorkbenchMsg request = new QiWorkbenchMsg(msg.getProducerCID(), compCid, QIWConstants.CMD_MSG, QIWConstants.NOTIFY_PROJ_INFO_CHANGED_CMD, MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params);
                            routeMsg(request);
                        }
                    }
                } else if (cmd.equals(QIWConstants.GET_ASSOC_PROJMGR_CMD)) {
                    //broadcast command to all PMs.
                    Set<String> keys = componentRegister.keySet();
                    Iterator<String> iter = keys.iterator();
                    while (iter.hasNext()) {
                        String compCid = iter.next();
                        IComponentDescriptor compDesc = componentRegister.get(compCid);
                        String compKind = compDesc.getComponentKind();
                        if (compKind.equals(QIWConstants.PLUGIN_PROJMGR_COMP)) {
                            //pass along the message ID so response will match request
                            IQiWorkbenchMsg request = new QiWorkbenchMsg(msg.getProducerCID(), compCid, QIWConstants.CMD_MSG, QIWConstants.GET_ASSOC_PROJMGR_CMD, MsgUtils.getMsgID(msg), QIWConstants.STRING_TYPE, msg.getContent());
                            routeMsg(request);
                        }
                    }
                } else if (cmd.equals(QIWConstants.NOTIFY_ASSOC_PROJMGR_CMD)) {
                    //broadcast command to all qiComponents.
                    IComponentDescriptor pmDesc = (IComponentDescriptor) msg.getContent();

                    Set<String> keys = componentRegister.keySet();
                    Iterator<String> iter = keys.iterator();
                    while (iter.hasNext()) {
                        String compCid = iter.next();
                        IComponentDescriptor compDesc = componentRegister.get(compCid);
                        String compKind = compDesc.getComponentKind();
                        boolean notifyComp = compKind.equals(QIWConstants.COMPONENT_PLUGIN_TYPE) ||
                                compKind.equals(QIWConstants.COMPONENT_VIEWER_TYPE) ||
                                compKind.equals(QIWConstants.VIEWER_AGENT_COMP) ||
                                compKind.equals(QIWConstants.PLUGIN_AGENT_COMP);
                        if (notifyComp) {
                            IQiWorkbenchMsg request = new QiWorkbenchMsg(msg.getProducerCID(), compCid, QIWConstants.CMD_MSG, QIWConstants.NOTIFY_ASSOC_PROJMGR_CMD, MsgUtils.genMsgID(), QIWConstants.COMP_DESC_TYPE, pmDesc);
                            routeMsg(request);
                        }
                    }
                } else if (cmd.equals(QIWConstants.RECORD_MODULES_CMD)) {
                    //route to the STATS server for processing
                    ArrayList<String> modules = (ArrayList<String>) msg.getContent();
                    ArrayList params = new ArrayList();
                    params.add(statsDirPath);
                    params.add(modules);
                    msg.setContent(params);
                    msg.setProducerCID(myCID);
                    msg.setConsumerCID(statsServletDispDesc.getCID());
                    response = DispatcherConnector.getInstance().sendRequestMsg(msg, statsURL);

                    //If response is normal, delete the stats log file
                    if (response.isAbnormalStatus()) {
                        logger.warning("WARNING: List of module names not saved. CAUSE: " + response.getContent());
                    }
                } else {    // command not recognized

                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Internal error: Message Dispatcher does not recognize command:" + cmd);
                }

                // send response message to producer of request
                routeMsg(response, msg.getProducerCID());
                return;
            } else // RESPONSES
            if (msgKind.equals(QIWConstants.DATA_MSG)) {
                if (cmd.equals(QIWConstants.GET_LOCAL_FILE_LIST_CMD) ||
                        cmd.equals(QIWConstants.GET_LOCAL_DIR_FILE_LIST_CMD) ||
                        cmd.equals(QIWConstants.CHECK_LOCAL_FILE_EXIST_CMD) ||
                        cmd.equals(QIWConstants.CREATE_LOCAL_DIRECTORY_CMD) ||
                        cmd.equals(QIWConstants.LOCAL_FILE_READ_CMD) ||
                        cmd.equals(QIWConstants.LOCAL_FILE_WRITE_CMD) ||
                        cmd.equals(QIWConstants.COPY_LOCAL_FILE_CMD) ||
                        cmd.equals(QIWConstants.MOVE_LOCAL_FILE_CMD) ||
                        cmd.equals(QIWConstants.LOCAL_BINARY_FILE_READ_CMD) ||
                        cmd.equals(QIWConstants.LOCAL_BINARY_FILE_WRITE_CMD) ||
                        cmd.equals(QIWConstants.LOCAL_READ_SEGY_DATA_CMD) ||
                        cmd.equals(QIWConstants.REMOTE_READ_SEGY_DATA_CMD) ||
                        cmd.equals(QIWConstants.REMOTE_READ_BHPSU_DATA_CMD)) {

                    //NOTE: REMOTE_READ_SEGY_DATA_CMD and REMOTE_READ_BHPSU_DATA_CMD
                    //handled by a local IO service which handles the receiving end
                    //of a socket channel

                    IQiWorkbenchMsg request = checkForMatchingRequest(msg);

                    // release local IO service (return to pool)
                    String ioServiceCID = msg.getProducerCID();
                    releaseLocalIOservice(ioServiceCID);

                    // notify the Workbench Manager to remove the service node
                    // from the component tree
                    if (addServiceTreeNode) {
                        removeComponentNode(ioServiceCID);
                    // Note; no response message required since the request is a notification

                    // route the response from the IO service to the requester.
                    }
                    msg.setConsumerCID(request.getProducerCID());
                    routeMsg(msg);
                    return;
                } else if (cmd.equals(QIWConstants.GET_REMOTE_FILE_LIST_CMD) ||
                        cmd.equals(QIWConstants.GET_REMOTE_DIR_FILE_LIST_CMD) ||
                        cmd.equals(QIWConstants.CHECK_REMOTE_FILE_EXIST_CMD) ||
                        cmd.equals(QIWConstants.CREATE_REMOTE_DIRECTORY_CMD) ||
                        cmd.equals(QIWConstants.REMOTE_FILE_READ_CMD) ||
                        //cmd.equals(QIWConstants.GET_LANDMARK_3D_PROJECTS_CMD) ||
                        cmd.equals(QIWConstants.REMOTE_FILE_WRITE_CMD) ||
                        cmd.equals(QIWConstants.REMOTE_BINARY_FILE_READ_CMD) ||
                        cmd.equals(QIWConstants.REMOTE_BINARY_FILE_WRITE_CMD)) {

                    IQiWorkbenchMsg request = checkForMatchingRequest(msg);

                    String ioServiceCID = msg.getProducerCID();

                    // notify the Workbench Manager to remove the service node
                    // from the component tree
                    if (addServiceTreeNode) {
                        removeComponentNode(ioServiceCID);
                    // Note; no response message required since the request is a notification

                    // route the response from the IO service to the requester.
                    }
                    msg.setConsumerCID(request.getProducerCID());
                    routeMsg(msg);
                    return;
                } else if (cmd.equals(QIWConstants.SUBMIT_JOB_CMD) ||
                        cmd.equals(QIWConstants.WAIT_FOR_JOB_EXIT_CMD) ||
                        cmd.equals(QIWConstants.CREATE_JOB_CMD) ||
                        cmd.equals(QIWConstants.EXECUTE_JOB_CMD) ||
                        cmd.equals(QIWConstants.GET_JOB_STATUS_CMD) ||
                        cmd.equals(QIWConstants.GET_JOB_OUTPUT_CMD) ||
                        cmd.equals(QIWConstants.GET_JOB_ERROR_OUTPUT_CMD) ||
                        cmd.equals(QIWConstants.SET_JOB_COMMAND_CMD) ||
                        cmd.equals(QIWConstants.SET_JOB_WORKING_DIR_CMD) ||
                        cmd.equals(QIWConstants.SET_JOB_ENV_VAR_CMD) ||
                        cmd.equals(QIWConstants.KILL_JOB_CMD)) {

                    IQiWorkbenchMsg request = checkForMatchingRequest(msg);

                    // route the response from the job service to the requester.
                    msg.setConsumerCID(request.getProducerCID());
                    routeMsg(msg);
                    return;
                } else if (cmd.equals(QIWConstants.RELEASE_JOB_CMD)) {
                    ArrayList<String> params = (ArrayList<String>) msg.getContent();
                    String locPref = params.get(0);
                    String jobServiceCID = params.get(1);
                    String jobID = params.get(2);

                    if (locPref.equals(QIWConstants.LOCAL_SERVICE_PREF)) {
                        // remove from list of active job services
                        JobService jobService = activeLocalJobServices.remove(jobID);
                        // add inactive job service to pool
                        releaseLocalJobService(jobService);
                    }
                    //Note: The Servlet Dispatcher manages its active job
                    //services and pool.

                    // notify the Workbench Manager to remove the service node
                    // from the component tree
                    if (addServiceTreeNode) {
                        removeComponentNode(jobServiceCID);
                    // Note; no response message required since the request is a notification
                    }
                    IQiWorkbenchMsg request = checkForMatchingRequest(msg);
                    // route the response from the job service to the requester.
                    msg.setConsumerCID(request.getProducerCID());
                    routeMsg(msg);
                    return;
                } else if (cmd.equals(QIWConstants.READ_GEOFILE_CMD) ||
                        cmd.equals(QIWConstants.WRITE_GEOFILE_CMD)) {
                    IQiWorkbenchMsg request = checkForMatchingRequest(msg);

                    // release local geoIO service (return to pool)
                    String geoIOServiceCID = msg.getProducerCID();
                    releaseLocalGeoIOservice(geoIOServiceCID);

                    // notify the Workbench Manager to remove the service node
                    // from the component tree
                    if (addServiceTreeNode) {
                        removeComponentNode(geoIOServiceCID);
                    // Note; no response message required since the request is a notification

                    // route the response from the geoIO service to the requester.
                    }
                    msg.setConsumerCID(request.getProducerCID());
                    routeMsg(msg);
                    return;
                } else if (cmd.equals(QIWConstants.RELEASE_GEOIO_SERVICE_CMD)) {
                    IQiWorkbenchMsg request = checkForMatchingRequest(msg);

                    // release client-side geoIO service (return to pool)
                    ArrayList<String> params = (ArrayList<String>) msg.getContent();
                    String geoIoServiceCID = params.get(0);
                    releaseLocalGeoIOservice(geoIoServiceCID);

                    // route the response from the geoIO service to the requester.
                    msg.setConsumerCID(request.getProducerCID());
                    routeMsg(msg);
                    return;
                }
            }
        //END OF RESPONSES
        } else // message targeted to the runtime servlet dispatcher for processing?
        if (msg.getConsumerCID().equals(runtimeServletDispDesc.getCID())) {
            if (msgKind.equals(QIWConstants.CMD_MSG)) {
                cmd = msg.getCommand();
                // TODO check for specific commands and form response

                // send response message to producer of request
                routeMsg(response, msg.getProducerCID());
            }
            return;
        } else if (msgKind.equals(QIWConstants.CMD_MSG)) {
            // send message to its designated consumer
            routeMsg(msg);
            return;
        } else if (msgKind.equals(QIWConstants.CMD_ROUTE_MSG)) {
            // message is a command to be routed to other agents as determined by the routing matrix
        } else if (msgKind.equals(QIWConstants.DATA_MSG)) {
            // message is information to be returned to its designated producer
            cmd = msg.getCommand();
        } else if (msgKind.equals(QIWConstants.DATA_CMD_MSG)) {
            // message is a command to be treated as data, not to be executed
        }

        logger.warning("message not processed:" + msg);
    }

    /**
     * Request component be added to the component tree. Send request to the
     * Workbench Manager.
     *
     * @param compKind Kind of component.
     * @param displayName Display name of component
     * @param parentCID CID of parent node
     */
    private void addComponentNode(String compKind, String displayName, String parentCID) {
        ArrayList<String> params = new ArrayList<String>();
        params.add(compKind);    // type of component

        params.add(displayName); // display name of component

        params.add(parentCID);   // CID of parent node

        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, workbenchMgrDesc.getCID(), QIWConstants.CMD_MSG, QIWConstants.ADD_COMPONENT_NODE_CMD, MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params);
        routeMsg(request);
    }

    /**
     * Request component be removed from the component tree. Send request to the
     * Workbench Manager.
     *
     * @param nodeCID CID of the node to be removed.
     */
    private void removeComponentNode(String nodeCID) {
        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, workbenchMgrDesc.getCID(), QIWConstants.CMD_MSG, QIWConstants.REMOVE_COMPONENT_NODE_CMD, MsgUtils.genMsgID(), QIWConstants.STRING_TYPE, nodeCID);
        routeMsg(request);
    }

    /**
     * Check for a request matching the response. If found, remove from the
     * list of outstanding request.
     *
     * @param response message
     * @return Original request message if a match; otherwise, a NULL message.
     */
    private IQiWorkbenchMsg checkForMatchingRequest(IQiWorkbenchMsg response) {
        IQiWorkbenchMsg req = outstandingRequests.findMatchingRequest(response);
        if (req == null) {
            logger.severe("Internal error: cannot find matching request for response:" + response.toString());
            //create dummy message so processing can continue
            return QiWorkbenchMsg.dummyMsg;
        } else {
            outstandingRequests.removeMatchineRequest(response);
            return req;
        }
    }

    /**
     * Return tomcat location - if tomcatLoc is equal to QIWConstants.LOCAL_PREF
     * or QIWConstants.REMOTE_PREF, then the relevant QIWConstants String is returned.
     *
     * If tomcatLoc is null or the empty string, QIWConstants.LOCAL_PREF is used unless
     * tomcatURL starts with one of the following strings
     * <ul>
     *   <li>http://localhost</li>
     *   <li>http://127.0.0.1</li>
     *   <li>http://_local_hostname_</li>
     * </ul>
     * @return "localLoc" or "remoteLoc"
     */
    //TODO add check for hostname to tomcatLocs which can return localhost
    public String getLocationPref() {
        if (QIWConstants.LOCAL_PREF.equals(tomcatLoc)) {
            logger.info("tomcatLoc is LOCAL_PREF, using local service(s)");
            return QIWConstants.LOCAL_PREF;
        } else if (QIWConstants.REMOTE_PREF.equals(tomcatLoc)) {
            logger.info("tomcatLoc is REMOTE_PREF, using remote service(s)");
            return QIWConstants.REMOTE_PREF;
        } else if ((tomcatURL != null) && (tomcatURL.equals("") == false)) {
            logger.info("tomcatURL is neither null nor the empty string. attempting to choose local or remote services based on tomcatURL");
            String inferredTomcatLoc = QiwbPreferences.getServerLoc(tomcatURL);
            if (inferredTomcatLoc.equals(QIWConstants.LOCAL_PREF)) {
                logger.info("tomcatURL is " + tomcatURL + ", attempting to use local services");
                return QIWConstants.LOCAL_PREF;
            } else if (inferredTomcatLoc.equals(QIWConstants.LOCAL_PREF)) {
                logger.warning("tomcatURL is" + tomcatURL + ",  attempting to use remote services");
                return QIWConstants.REMOTE_PREF;
            } else {
                logger.warning("failed to determine default service location for tomcatURL: " + tomcatURL + ", defaulting to local services");
                return QIWConstants.LOCAL_PREF;
            }
        } else {
            logger.warning("tomcatLoc is neither LOCAL_PREF nor REMOTE_PREF and tomcatURL is null or the empty string - getLocationPref() is defaulting to local services");
            return QIWConstants.LOCAL_PREF;
        }
    }

    public void setLocationPref(String loc) {
        tomcatLoc = loc;
    }

    /**
     * Return tomcat URL
     */
    public String getTomcatURL() {
        return tomcatURL;
    }

    public void setTomcatURL(String url) {
        tomcatURL = url;
    }

    /**
     * Get project path.
     * @return Path of project
     */
    //Note: Called by the Messaging Manager's getProject().
    public String getProject() {
        return qiProject;
    }

    /**
     * Set project path
     * @param project Path of the qiProject.
     */
    public void setProject(String qiProject) {
        this.qiProject = qiProject;
    }

    /**
     * Get qiSpace metadata
     * @return qiSpace descriptor
     */
    public QiSpaceDescriptor getQispaceDesc() {
        return qispace;
    }

    /**
     * Get path.of qiSpace.
     * @return Path of qiSpace
     */
    public String getQispacePath() {
        return (qispace == null) ? "" : qispace.getPath();
    }

    /**
     * Set qiSpace path
     * @param qiSpace Path of qiSpace.
     * @deprecated Never called. qispace set by SAVE_QISPACE_CMD
     */
    public void setQispace(String qispace) {
//        this.qispace = qispace;
        //TODO: Specify project. There may be no active qiProjectManager
        //NOTE: Until there is a qiProjectManager, it is assumed the qiSpace is the project
//        this.project = qispace;
    }

    /*
     * Rename component in register and registry
     * @param String oldName
     * @param String newName
     */
    public void renameComponent(String oldName, String newName) {
        logger.info("MD: Attempting rename from " + oldName + " to " + newName);
        // Verify descriptor is in both lists
        if (!componentRegistry.containsKey(oldName)) {
            logger.warning("Internal Error: Cannot find " + oldName + " in componentRegistry");
            return;
        } else if (!componentRegister.containsKey(componentRegistry.get(oldName).getCID())) {
            logger.warning("Internal Error: Cannot find " + componentRegistry.get(oldName).getCID() + " in componentRegister");
            return;
        }

        // get component's descriptor
        IComponentDescriptor desc = componentRegistry.get(oldName);
        // Verify newName not already in lists
        if (componentRegistry.containsKey(newName)) {
            logger.warning("Internal Error: " + newName + " already in Registry");
            return;
        }
        // Remove oldName from Registry and remove CID from Register
        componentRegistry.remove(oldName);
        componentRegister.remove(desc.getCID());
        // Update display name in descriptor and add to lists
        desc.setDisplayName(newName);
        componentRegistry.put(newName, desc);
        componentRegister.put(desc.getCID(), desc);
    }

    /**
     * Get the Manifests of qiComponents in the component cache.
     * Used to activate a qiComponent, for the Manifest contains the main class.
     * Implicit returns:
     * <ul>
     * <li>Saves the Manifests in a class HashMap variable, externalManifests.
     * <li>Saves the name of the jar file in a class HashMap variable, pluginJars.
     * </ul>
     */
    private void getPluginManifests() {
        String compCachePath = getUserHOME() + File.separator + QIWConstants.QIWB_WORKING_DIR + File.separator + QIWConstants.COMP_CACHE_DIR + File.separator;
        File compCacheDir = new File(compCachePath);
        //Check if user has a component cache
        if (!compCacheDir.isDirectory()) {
            return;
        }
        File[] files = compCacheDir.listFiles();
        for (int i = 0; i < files.length; i++) {
            //TODO: handle components in a directory; for now, ignore dir
            if (!files[i].isDirectory()) {
                String fileName = files[i].getName();
                if (fileName.endsWith(".jar")) {
                    try {
                        // Get jar file's Manifest
                        String prefix = getPlatformOS().equals(QIWConstants.UNIX_OS) ? "file:" : "file:///";
                        URL componentURL = new URL(prefix + compCachePath + fileName);
                        URL jarURL = new URL("jar", "", componentURL + "!/");
//                        logger.info("jarURL: "+jarURL);
                        JarURLConnection jarConnection = (JarURLConnection) jarURL.openConnection();
                        Manifest manifest = jarConnection.getManifest();
                        externalManifests.put(fileName, manifest);

                        //Get display name
                        Attributes attrs = manifest.getAttributes(QIWConstants.COMP_ATTR_SECTION_NAME);

                        if (attrs != null) {
                            String dependents = attrs.getValue(QIWConstants.DEPENDENT_JNI_JARS_ATTR);
                            loadJNIJars(dependents);
                            String displayName = attrs.getValue(QIWConstants.DISPLAY_NAME_ATTR);
                            pluginJars.put(displayName, fileName);
                        }
                    } catch (MalformedURLException mue) {
                        logger.severe("Unable to locate jar; CAUSE:" + mue.getMessage());
                    } catch (IOException ioe) {
                        logger.severe("Unable to access component jar: " + fileName + "; CAUSE:" + ioe.getMessage());
                    }
                }
            }
        }
    }
    private List<String> jniJarList = new ArrayList<String>();

    private void loadJNIJars(String dependents) {
        if (dependents == null || dependents.trim().length() == 0) {
            return;
        }
        logger.fine("jni dependents= " + dependents);
        String compCachePath = getUserHOME() + File.separator + QIWConstants.QIWB_WORKING_DIR + File.separator + QIWConstants.COMP_CACHE_DIR + File.separator;
        String jars[] = dependents.split(",");
        if (jars == null) {
            jars = dependents.split(" ");
        }
        if (jars != null && jars.length > 0) {
            List<String> list = new ArrayList<String>();
            for (int i = 0; i < jars.length; i++) {
                if (!jniJarList.contains(jars[i])) {
                    list.add(jars[i]);
                }
            }
            URL[] urls = new URL[list.size()];
            int j = 0;
            for (String s : list) {
                try {
                    logger.info("class loading " + "jar:file://///" + compCachePath + File.separator + s + "!/");
                    urls[j++] = new URL("jar:file://///" + compCachePath + File.separator + s + "!/");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
            jniClassLoader = new AllPermissionsClassLoader(urls, MessageDispatcher.class.getClassLoader());
        }
    }

    private void downloadJars(String dependents) {
        if (dependents == null || dependents.trim().length() == 0) {
            return;
        }
        DownloadService ds;
        try {
            ds = (DownloadService) ServiceManager.lookup("javax.jnlp.DownloadService");
        } catch (UnavailableServiceException e) {
            e.printStackTrace();
            ds = null;
            return;
        }
        String compCachePath = getUserHOME() + File.separator + QIWConstants.QIWB_WORKING_DIR + File.separator + QIWConstants.COMP_CACHE_DIR + File.separator;
        String jars[] = dependents.split(",");
        if (jars == null) {
            jars = dependents.split(" ");
        }
        if (jars != null && jars.length > 0) {
            URL[] urls = new URL[jars.length];

            for (int j = 0; j < jars.length; j++) {
                try {
                    logger.info("jar:file://///" + compCachePath + File.separator + jars[j] + "!/");
                    urls[j] = new URL("jar:file://///" + compCachePath + File.separator + jars[j] + "!/");
                    // determine if a particular resource is cached
                    boolean cached = ds.isResourceCached(urls[j], "1.0");
                    logger.info("cached = " + cached);
                    // remove the resource from the cache
                    if (cached) {
                        ds.removeResource(urls[j], "1.0");
                    }
                    // reload the resource into the cache
                    DownloadServiceListener dsl = ds.getDefaultProgressWindow();
                    ds.loadResource(urls[j], "1.0", dsl);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Combines core and external plugins and viewers into one ArrayList.
     * Used in AboutComponentDialog to access components and create table.
     *
     * @return List of component discriptors of core and external components of both core and external
     */
    public ArrayList getAllAvailableComponents() {
        ArrayList<IComponentDescriptor> plugins = getAvailableComponents("plugin");
        ArrayList<IComponentDescriptor> viewers = getAvailableComponents("viewer");
        int arraySize = plugins.size() + viewers.size();
        ArrayList<IComponentDescriptor> combo = new ArrayList();
        for (int i = 0; i < arraySize; i++) {
            if (i < plugins.size()) {
                combo.add(plugins.get(i));
            } else {
                combo.add(viewers.get(i - plugins.size()));
            }
        }
        return combo;
    }

    /**
     * Get the list of avalable components - plugin or viewer - core
     * and external. Extract the information needed to form a component's
     * partial component descriptor from the component's manifest. The list
     * is used by the Workbenvch manager to populate its menus.
     *
     * @param compKind Kind of component - plugin or viewer
     * @return List of component descriptors of core and external components
     */
    private ArrayList getAvailableComponents(String compKind) {
        ArrayList<IComponentDescriptor> comps = new ArrayList<IComponentDescriptor>();
        HashMap<String, Manifest> compManifests = null;

        // Search both sets of manifests - core and external - for
        // components of the specified kind.
        for (int k = 0; k < 2; k++) {
            if (k == 0) {
                compManifests = externalManifests;
            }
            if (k == 1) {
                compManifests = coreManifests;
            }
            for (String key : compManifests.keySet()) {
                Manifest mf = compManifests.get(key);

                Attributes attrs = mf.getAttributes(QIWConstants.COMP_ATTR_SECTION_NAME);
                if (attrs != null) {
                    String componentType = attrs.getValue(QIWConstants.COMPONENT_TYPE_ATTR);
                    if (componentType != null && componentType.contains(compKind)) {
                        // get component's display name
                        String displayName = attrs.getValue(QIWConstants.DISPLAY_NAME_ATTR);
                        //get component's provider
                        String providerName = attrs.getValue(QIWConstants.PROVIDER_ATTR);
                        //get component's version
                        String versionNumber = attrs.getValue(QIWConstants.VERSION_ATTR);
                        // Get the component's main class
                        Attributes mainAttrs = mf.getMainAttributes();
                        String entryPointClass = (String) mainAttrs.getValue(Attributes.Name.MAIN_CLASS);
                        if (entryPointClass == null) {
                            entryPointClass = "";
                        // form partially complete component descriptor
                        }
                        IComponentDescriptor compDesc = new ComponentDescriptor();
                        // set CID to base CID
                        compDesc.setCID(entryPointClass);
                        compDesc.setMsgHandler(null);
                        compDesc.setComponentKind(QIWConstants.PLUGIN_AGENT_COMP);
                        compDesc.setDisplayName(displayName);
                        compDesc.setProviderName(providerName);
                        compDesc.setVersionNumber(versionNumber);
                        comps.add(compDesc);
                        logger.fine("System init: " + displayName + " " + compKind + "  descriptor formed:" + compDesc);
                    }
                }
            }
        }
        return comps;
    }

    private void goNestedPlugin(IQiWorkbenchMsg msg) {
        IQiWorkbenchMsg response = null;
        List<String> strList = (List<String>) msg.getContent();
        String displayName = strList.get(0);
        Manifest mf = null;
        String jarName = pluginJars.get(displayName);
        if (jarName != null) {
            mf = externalManifests.get(jarName);
        }
        if (mf == null) {
            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR,
                    "Cannot find Manifest for " + displayName);
        } else {
            String status = ComponentLauncher.getInstance().launchComponent(jarName, mf, false, jniClassLoader);
            if (!status.toLowerCase().startsWith("cid:")) {
                response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, status);
            } else {
                int idx = status.indexOf(":") + 1;
                String cid = status.substring(idx);
                if (isRegistered(cid, 3)) {
                    //record usage event
                    IComponentDescriptor compDesc = getComponentDesc(cid);
                    String event = StatsManager.getInstance().createStatsEvent(compDesc.getDisplayName(), ClientConstants.LAUNCHED_ACTION);
                    StatsManager.getInstance().logStatsEvent(event);
                    //updateNode(cid, displayName);
                    response = new QiWorkbenchMsg(msg.getProducerCID(), cid, QIWConstants.DATA_MSG,
                            msg.getCommand(), msg.getMsgID(),
                            QIWConstants.ARRAYLIST_TYPE, msg.getContent(), msg.skip());
                    response.setStatusCode(MsgStatus.SC_OK);
                }
            }
        }

        routeMsg(response);
    }

    /**
     * This function will be called when you activate a plugin from inside a plugin
     * Example is XmlEditor which has two plugins (nested plugins)
     * This function directlty updates the GUI nodes
     *
     * @return
     */
    private void updateNode(String cid, String displayName) {
        IComponentDescriptor cd = getComponentDesc(cid);
        String pName = cd.getPreferredDisplayName();
        WorkbenchManager.getInstance().addActivePlugin(cd);
        WorkbenchAction.addComponent(cd, displayName);
        if (!WorkbenchStateManager.getInstance().getCurrentNodeMap().containsKey(pName)) {
            DecoratedNode node = new DecoratedNode(null, cd);
            node.setComponentClosed(false);
            WorkbenchStateManager.getInstance().setNodeToCurrentNodeMap(pName, node);
        }
    }

    /** Configure logging:
     *  <ul>
     *    <li>Send logging information to .qiworkbench/logs/qiWorkbench*.log in the user's home directory</li>
     *    <li>Log all messages of level INFO and higher unless a custom logging.properties file is present</li>
     *    <li>See the http://java.sun.com/j2se/1.4.2/docs/guide/util/logging/overview.html page for more information.</li>
     *    <li>Use a simple formatter, not the default XML formatter</li>
     *  </ul>
     *  <br>
     *  Note: To turn logging off, set the level to OFF. To log everything, set the level to ALL.
     */
    public static void configLogging() {
        // Make sure the directory where the client-side logs are stored exists.
        // This code is for maintaining backward compatibility.

        String homeDir = System.getProperty("user.home");
        String logDir = homeDir + File.separator + QIWConstants.QIWB_WORKING_DIR + File.separator + "logs";
        File logsPath = new File(logDir);
        if (!logsPath.exists()) {
            logsPath.mkdirs();
        }
        FileInputStream configInputStream = null;
        try {
            File customLoggingConfig = new File(homeDir + File.separator + "logging.properties");
            if (customLoggingConfig.exists()) {
                LogManager lm = LogManager.getLogManager();
                logger.info("Reading logging configuration from: " + customLoggingConfig.getPath());
                configInputStream = new FileInputStream(customLoggingConfig);
                lm.readConfiguration(configInputStream);
                logger.info("Done reading logging configuration");
            } else {
                logger.info("File: " + customLoggingConfig.getPath() + " does not exist...");
                logger.info("No logging.properties file found in client home directory, proceeding with default logging configuration...");
                logger.config("Appending FileHandler for .qiworkbench/logs/qiWorkbench%u_%g.log to root logger...");
                // log file in .qiworkbench/logs/ in user's home directory
                Handler fh = new FileHandler("%h/" + QIWConstants.QIWB_WORKING_DIR + "/logs/qiWorkbench%u_%g.log", 500000, 3, false);
                // use simple formatter instead of default XML formatter
                fh.setFormatter(new SimpleFormatter());
                // append FileHandler to root logger
                Logger.getLogger("").addHandler(fh);
            }

        } catch (IOException ioe) {
            String errorMsg = "Unable to append FileHandler for .qiworkbench/logs/qiWorkbench%u_%g.log to root logger due to: " + ioe.getMessage();
            logger.warning(errorMsg);
        } finally {
            if (configInputStream != null) {
                try {
                    configInputStream.close();
                } catch (IOException ioe) {
                    logger.warning("Caught while closing configInputStream: " + ioe.getMessage());
                }
            }
        }
        logger.info("Using the configLogging() method to control logging levels has been disabled." +
                System.getProperty("line.separator") +
                "If you wish to change the logging level(s) from the default 'INFO', please use a logging.properties file located in your home directory." +
                System.getProperty("line.separator") +
                "See the http://java.sun.com/j2se/1.4.2/docs/guide/util/logging/overview.html page for more information.");
    }

    private List<File> getUnMadeLocalDirs(QiProjectDescriptor projDesc, String filesep) {
        if (projDesc == null) {
            return null;
        }
        List<File> dirs = new ArrayList<File>();
        String root = QiProjectDescUtils.getProjectPath(projDesc);
        File rootDir = new File(root);
        if (!rootDir.exists()) {
            return null;
        }
        File aDir = new File(rootDir + filesep + projDesc.getDatasetsReloc());
        if (!aDir.exists()) {
            dirs.add(aDir);
        }
        aDir = new File(rootDir + filesep + projDesc.getHorizonsReloc());
        if (!aDir.exists()) {
            dirs.add(aDir);
        }
        aDir = new File(rootDir + filesep + projDesc.getSavesetsReloc());
        if (!aDir.exists()) {
            dirs.add(aDir);
        }
        aDir = new File(rootDir + filesep + projDesc.getScriptsReloc());
        if (!aDir.exists()) {
            dirs.add(aDir);
        }
        aDir = new File(rootDir + filesep + projDesc.getWellsReloc());
        if (!aDir.exists()) {
            dirs.add(aDir);
        }

        aDir = new File(rootDir + filesep + projDesc.getSegyReloc());
        if (!aDir.exists()) {
            dirs.add(aDir);
        }

        String tmpDir = projDesc.getTempReloc();
        if (tmpDir.startsWith(filesep + "tmp")) {
            aDir = new File(tmpDir);
            if (!aDir.exists()) {
                dirs.add(aDir);
            }
        } else {
            aDir = new File(rootDir + filesep + projDesc.getTempReloc());
            if (!aDir.exists()) {
                dirs.add(aDir);
            }
        }

        List<String> list = projDesc.getSeismicDirs();
        for (int i = 0; list != null && i < list.size(); i++) {
            aDir = new File(rootDir + filesep + list.get(i));
            if (!aDir.exists()) {
                dirs.add(aDir);
            }
        }
        return dirs;
    }

    /**
     * Start up the Message Dispatcher. When it's initialization is finished,
     * start up a Workbench Manager. Also, configure logging for client-side
     * code.
     *
     * <p>
     * NOTE: Each thread's init() must finish before the next thread is
     * started. This is accompished by monitoring the SYNC_LOCK object.
     */
    public static void main(String[] args) {
        // Set up logging configuration
        configLogging();

        // set the look & feel - fix for colorless buttons in OSX
        try {
            UIManager.setLookAndFeel(new MetalLookAndFeel());
        } catch (Exception e) {
            logger.warning("Failed to set lookAndFeel to MetalLookAndFeel - some GUI elements may not be presented as designed, e.g. colored JButtons may appear colorless in Mac OSX.");
        }

        // If the qiComponent cache is located in the old location, move it
        // to the new location. This code is for maintaining backward compatibility.
        String homeDir = System.getProperty("user.home");
        String oldCacheDir = homeDir + File.separator + "qiworkbench" + File.separator + "components";
        String newCacheDir = homeDir + File.separator + QIWConstants.QIWB_WORKING_DIR + File.separator + QIWConstants.COMP_CACHE_DIR;
        File oldCachePath = new File(oldCacheDir);
        File newCachePath = new File(newCacheDir);
        if (!newCachePath.exists()) {
            // move all the JAR files to the new location
            if (oldCachePath.exists()) {
                oldCachePath.renameTo(newCachePath);
            } else {
                newCachePath.mkdirs();
            }
        }

        // If the qiwbPreferences.xml file is located in the old location, move it
        // to the new location. This code is for maintaining backward compatibility.
        String oldDir = homeDir;
        String newDir = homeDir + File.separator + QIWConstants.QIWB_WORKING_DIR;
        File oldPath = new File(oldDir, QIWConstants.PREF_FILE_NAME);
        File newPath = new File(newDir, QIWConstants.PREF_FILE_NAME);
        if (!newPath.exists()) {
            if (oldPath.exists()) {
                oldPath.renameTo(newPath);
            }
        }

        logger.config("Starting the Message Dispatcher thread");

        MessageDispatcher dispatcher = getInstance();

        //Note: Creation of the message dispatcher starts it as a thread. The
        //first thing it's run method does is lock the SYNC_LOCK. However,on
        //a multiprocessor machine, it is possible execution of the thread
        //thread is interrupted before the lock is secured in which case the
        //Workbench Manager thread is created below. The manager will access
        //the user's preferences before they are obtained by the dispatcher.

        //Make sure the dispatcher's init method has been executed before creating
        //the manager thread.
        while (!dispatcher.isInitFinished()) {
            //allow the dispatcher to run
            Thread.yield();
        }

        if (dispatcher.isInitSuccessful() == false) {
            logger.severe("Initialization was not completed successfully, prompting user to exit");
            String errorMessage = "\nReason: An unspecified MessageDispatcher initialization error occurred.";

            List initExceptions = dispatcher.getInitExceptions();
            if (initExceptions.size() > 0) {
                if (initExceptions.size() == 1) {
                    errorMessage = "\nReason: ";
                } else {
                    errorMessage = "\nReasons: ";
                }
                Iterator iter = initExceptions.iterator();

                while (iter.hasNext()) {
                    Exception exception = (Exception) iter.next();
                    if (exception instanceof NullPointerException) {
                        errorMessage += "A NullPointerException occurred at ";
                        StackTraceElement element = exception.getStackTrace()[0];
                        errorMessage += getUnqualifiedName(element.getClassName()) + "." + element.getMethodName() + " line #" + element.getLineNumber();
                    } else {
                        errorMessage += exception.getMessage();
                    }
                    if (iter.hasNext()) {
                        errorMessage += "\n";
                    }
                }
            }
            JOptionPane.showMessageDialog(new JFrame(), "QiWorkbench could not be initialized, the application will now close.  " + errorMessage, "Initialization error", JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        } else {
            //Start the workbench manager. If there is a default server and project
            //which are to be honored, then getting its instance will start its
            //thread and launch the workbench; otherwise, just get its instance.
            WorkbenchManager manager = WorkbenchManager.getInstance();
            if (!validDefaultServerAndProject) {
                manager.selectServerAndProject();

            //Capture the OS name, file separator and line separator if the server is remote.
            }
            remoteOSInfoList = manager.getRemoteOSInfo();

            //It is now safe to register the STATS server which may be the same
            //as the selected runtime Tomcat server, which was registered at the
            //time of selection via MessageDispatcher's resetRuntimeServer()
            //The WorkbenchManager will call the MessageDispatcher to do the
            //registration; otherwise, doing it here requires static variables
            //and methods to be introduced.
            manager.registerStatsServer();
        }
    }

    /**
     * Main-like method for functional testing of MessageDispatcher (and thereby serveletDispatcher)
     * Unsuccessful init of MessageDispatcher has been changed to return a boolean rather than calling System.exit()
     * which would abort the JUnit Test.
     *
     * @return true if isInitSuccessful() == true, otherwise false
     */
    public static boolean mainNoGUI() {
        // Set up logging configuration
        configLogging();

        // If the qiComponent cache is located in the old location, move it
        // to the new location. This code is for maintaining backward compatibility.
        String homeDir = System.getProperty("user.home");
        String oldCacheDir = homeDir + File.separator + "qiworkbench" + File.separator + "components";
        String newCacheDir = homeDir + File.separator + QIWConstants.QIWB_WORKING_DIR + File.separator + QIWConstants.COMP_CACHE_DIR;
        File oldCachePath = new File(oldCacheDir);
        File newCachePath = new File(newCacheDir);
        if (!newCachePath.exists()) {
            // move all the JAR files to the new location
            if (oldCachePath.exists()) {
                oldCachePath.renameTo(newCachePath);
            } else {
                newCachePath.mkdirs();
            }
        }

        // If the qiwbPreferences.xml file is located in the old location, move it
        // to the new location. This code is for maintaining backward compatibility.
        String oldDir = homeDir;
        String newDir = homeDir + File.separator + QIWConstants.QIWB_WORKING_DIR;
        File oldPath = new File(oldDir, QIWConstants.PREF_FILE_NAME);
        File newPath = new File(newDir, QIWConstants.PREF_FILE_NAME);
        if (!newPath.exists()) {
            if (oldPath.exists()) {
                oldPath.renameTo(newPath);
            }
        }

        logger.config("Starting the Message Dispatcher thread");

        MessageDispatcher dispatcher = getInstance();

        //Note: Creation of the message dispatcher starts it as a thread. The
        //first thing it's run method does is lock the SYNC_LOCK. However,on
        //a multiprocessor machine, it is possible execution of the thread
        //thread is interrupted before the lock is secured in which case the
        //Workbench Manager thread is created below. The manager will access
        //the user's preferences before they are obtained by the dispatcher.

        //Make sure the dispatcher's init method has been executed before creating
        //the manager thread.
        while (!dispatcher.isInitFinished()) {
            //allow the dispatcher to run
            Thread.yield();
        }

        if (dispatcher.isInitSuccessful()) {
            try {
                WorkbenchManager manager = WorkbenchManager.getInstance();
                return true;
            } catch (Exception ex) {
                logger.warning("While getting WorkbenchManager instance, caught: " + ex.getMessage());
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Register different ServletDispatchers (runtime, deployment, secondary, stats, etc)
     * @param consumerDesc Proposed component descriptor of the ServletDispatcher
     * @param servletDispatcherURL URL of the machine running the ServletDispatcher
     * @return Component descriptor of the ServletDispatcher.
     */
    private IComponentDescriptor registerDescriptor(IComponentDescriptor consumerDesc, String servletDispatcherURL) {
        // send descriptor to ServletDisp
        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, consumerDesc.getCID(), QIWConstants.CMD_MSG, QIWConstants.REGISTER_SELF_CMD, MsgUtils.genMsgID(), "", servletDispatcherURL);
        request.setContentType(ComponentDescriptor.class.getName());
        request.setContent(consumerDesc);
        /*request.setURL(servletDispatcherURL);*/

        IQiWorkbenchMsg response = DispatcherConnector.getInstance().sendRequestMsg(request, servletDispatcherURL);
        if (!response.isAbnormalStatus()) {
            // consume response
            logger.config("Sent Servlet Dispatcher its descriptor: response=" + response);
            return (ComponentDescriptor) response.getContent();
        } else {
            //TODO Take action based on the cause of the abnormal status. For
            //example, if the server is down, notify the user and suspend
            //processing. Usually a message is sent to the Workbench Manager
            //who takes the appropriate action.

            //If registering the STATS ServletDispatcher and it is unaccessible,
            //for example, down or user deploying from a machine not internal to
            //the BHP intranet, then indicate this fact by returning the same
            //descriptor
            if (consumerDesc.getCID().equals(statsServletDispDesc.getCID())) {
                return consumerDesc;
            }
            String cause = (String) response.getContent();
            String criticalDescriptorRegistrationErrorText = "Unable to set ComponentDescriptor for " + servletDispatcherURL + ". CAUSE:" + cause;
            logger.severe(criticalDescriptorRegistrationErrorText);
            throw new RuntimeException(criticalDescriptorRegistrationErrorText);
        }
    }

    /**
     * Create a componentDescriptor for a servletDispatcher.  This method is now distinct from registration
     * to avoid creating a runtime and deployment servletDispatchers with identical keys (QIWConstants.SERVLET_DISPATCHER_NAME).
     *
     * @param dispInfo String describing the purpose or location of the servlet dispatcher.  This parameter is used only for logging, if logging is enabled.
     * @return ComponentDescriptor configured for a servlet dispatcher.
     *
     */
    private IComponentDescriptor createDispatcher(String dispInfo) {
        IComponentDescriptor desc = new ComponentDescriptor();

        //TODO ServletDispatcher not a part of the client JAR file.
        desc.setCID(ComponentUtils.genFullCID(QIWConstants.SERVLET_DISPATCHER_CLASS));

        // Note: Servlet Dispatcher does not have a msgHandler
        desc.setMsgHandler(null);
        desc.setComponentKind(QIWConstants.FRAMEWORK_COMP);
        desc.setDisplayName(QIWConstants.SERVLET_DISPATCHER_NAME);
        logger.fine(dispInfo + " descriptor formed:" + desc);

        return desc;
    }

    /**
     * Reset the dispatcher's runtimeServletDispatcherDescriptor.  This is needed
     * when running remotely and accessing a runtime server different than localhost
     * as well as deployment URL.
     */
    public boolean resetRuntimeServer(String url) {
        try {
            runtimeServletDispDesc = registerDescriptor(runtimeServletDispDesc, url);
            logger.info("resetRuntimeServer: " + url);
            logger.info("runtimeServletDispDesc=" + runtimeServletDispDesc);
            return isServerCompatibleWithClient(url);
        } catch (RuntimeException re) {
            logger.warning("Unable to register descriptor for runtime tomcat server at: " + url + " due to: " + re.getMessage());
            return false;
        }
    }

    /**
     * @param runtimetomcatServerURL string representation of URL at which runtime tomcat servletDispatcher is found.
     * The message is sent to this URL regardless of the MessageDispatcher's actual own runtime or deployment server URLs.
     *
     * @return true if the server accepts the client's project.version and project.version as compatible.
     */
    public boolean isServerCompatibleWithClient(String runtimeTomcatServerURL) {
        String versionPropertiesFileName = "/version.properties";
        try {
            if (props == null) {
                props = PropertyUtils.loadPropertiesFromFile(versionPropertiesFileName);
            }
        } catch (IOException ioe) {
            logger.warning("isServerCompatibleWithClient check has failed because properties cannot be loaded from " + versionPropertiesFileName);
            return false;
        }

        String clientVersion = props.getProperty("project.version");
        String clientBuild = props.getProperty("project.build");

        HashMap hashMap = new HashMap();
        hashMap.put("project.version", clientVersion);
        hashMap.put("project.build", clientBuild);

        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, runtimeServletDispDesc.getCID(),
                QIWConstants.CMD_MSG, QIWConstants.IS_COMPATIBLE_CLIENT_VERSION, MsgUtils.genMsgID(), "",
                runtimeTomcatServerURL);

        request.setContentType(QIWConstants.HASH_MAP_TYPE);
        request.setContent(hashMap);

        IQiWorkbenchMsg response = DispatcherConnector.getInstance().sendRequestMsg(request, runtimeTomcatServerURL);

        if (response.getStatusCode() != MsgStatus.SC_OK) {
            JOptionPane.showMessageDialog(new JFrame(), "Client & server are not compatible: server version" + System.getProperty("line.separator") + "could not be determined due to abnormal message response.", "Client & Server Not Compatible", JOptionPane.WARNING_MESSAGE);
            return false;
        } else {
            String contentType = response.getContentType();
            if (QIWConstants.HASH_MAP_TYPE.compareTo(contentType) == 0) {
                Object contentObject = response.getContent();
                if (!(contentObject instanceof HashMap)) {
                    throw new RuntimeException("Could not parse response to client-server compatibility check - response content type is HASH_MAP_TYPE but actual content is instance of: " + contentObject.getClass().toString());
                } else {
                    HashMap contentHashMap = (HashMap) contentObject;
                    Boolean isCompatible = (Boolean) contentHashMap.get("compatible");
                    if (isCompatible.equals(true)) {
                        return true;
                    } else {
                        String reason = contentHashMap.get("reason").toString();
                        JOptionPane.showMessageDialog(new JFrame(), "Client & server are not compatible: " + reason, "Client & Server Not Compatible", JOptionPane.WARNING_MESSAGE);
                        return false;
                    }
                }
            } else {
                throw new RuntimeException("Client-server compatibility check response content type does not equal the expected value " + QIWConstants.HASH_MAP_TYPE);
            }
        }
    }

    public static String getUnqualifiedName(String fullyQualifiedName) {
        int lastDotIndex = fullyQualifiedName.lastIndexOf(".");
        if (lastDotIndex == -1) {
            return fullyQualifiedName;
        } else //strip everything prior to and including the final '.' of the fully-qualified classname
        {
            return fullyQualifiedName.substring(lastDotIndex + 1);
        }
    }

    /**
     * Register the STATS server which may be the same as the selected runtime
     * Tomcat server which will have been registered at this point.;
     */
    public void registerStatsServer() {
        statsServletDispDesc = registerDescriptor(statsServletDispDesc, statsURL);
    }
}
