/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.messageFramework;

import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import java.util.ArrayList;

import java.util.logging.Logger;

/**
 * List of outstanding request messages in the order they were sent by the component.
 */

public class RequestList {
    /** List of requests waiting for a response. Maintaining a list keeps the messages
     *  in the order they were sent. When the component gets a response, it can search the list
     *  for the command with the matching message ID. Based on the command sent, the viewer
     *  can determine how to process the response.
     * <p>
     *  It is the responsibility of the component to remove the request from the list.
     */
    private ArrayList<IQiWorkbenchMsg> requests = new ArrayList<IQiWorkbenchMsg>();
    private static Logger logger = Logger.getLogger(RequestList.class.getName());
    /**
     * Add request to the list.
     *
     * @param request Messge sent to another component
     */
    public void addRequest(IQiWorkbenchMsg request) {
        requests.add(request);
    }

    /**
     * Get the request.
     *
     * @param idx Index of request; >=0
     */
    public IQiWorkbenchMsg getRequest(int idx) {
        return requests.get(idx);
    }

    /**
     * Remove the request from the list
     *
     * @param idx Index of request; >=0
     * @return The request that was removed.
     */
    public IQiWorkbenchMsg removeRequest(int idx) {
        return requests.remove(idx);
    }

    /**
     * Find and remove the matching request from list. Test is made on the message ID.
     *
     * @param response Message taken from message queue for processing.
     * @return True if matchine request removed; otherwise, false
     */
    public boolean removeMatchineRequest(IQiWorkbenchMsg response) {
        String rMsgID = response.getMsgID();
        return removeMatchingRequest(rMsgID);
    }

    public IQiWorkbenchMsg findMatchingRequest(String rMsgID) {
        for (int i=0; i<requests.size(); i++) {
            IQiWorkbenchMsg msg = requests.get(i);
            if (msg.getMsgID().equals(rMsgID)) {
                return msg;
            }
        }
        return null;
    }

    public boolean removeMatchingRequest(String rMsgID) {
        for (int i=0; i<requests.size(); i++) {
            IQiWorkbenchMsg msg = requests.get(i);
            if (msg.getMsgID().equals(rMsgID)) {
                requests.remove(i);
                return true;
            }
        }
        return false;    
    }
    
    /**
     * Find the request matching the resonse. Test is made on the message ID.
     *
     * @param response Message taken from message queue for processing.
     * @return Matching request. If no match, null
     */
    public IQiWorkbenchMsg findMatchingRequest(IQiWorkbenchMsg response) {
        String rMsgID = response.getMsgID();
        //logger.info("# of outstanding requests: " + requests.size());
        for (int i=0; i<requests.size(); i++) {
            IQiWorkbenchMsg msg = requests.get(i);
            if (msg.getMsgID().equals(rMsgID)) 
                return msg;
            //else
                //logger.info(msg.getMsgID()+ " did not match " + rMsgID);
        }

        return null;
    }

    /**
     * Find the index of the request matching the resonse. Test is made on the message ID.
     *
     * @param response Message taken from message queue for processing.
     * @return Index of matching request. If no match, -1
     */
    public int findMatchingRequestIndex(IQiWorkbenchMsg response) {
        String rMsgID = response.getMsgID();
        for (int i=0; i<requests.size(); i++) {
            IQiWorkbenchMsg msg = requests.get(i);
            if (msg.getMsgID().equals(rMsgID)) return i;
        }

        return -1;
    }

    /** Dump the list of outstanding requests.*/
    public String toString() {
        StringBuffer sbuf = new StringBuffer();
        sbuf.append("Outstanding Requests:");
        for (int i=0; i<requests.size(); i++) {
            IQiWorkbenchMsg msg = requests.get(i);
            sbuf.append("\nRequest["+(i+1)+"]:\n");
            sbuf.append(msg.toString());
        }

        return sbuf.toString();
    }
}
