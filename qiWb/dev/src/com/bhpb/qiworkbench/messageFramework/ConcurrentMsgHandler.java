/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.messageFramework;

import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import java.util.Map;
import java.util.HashMap;
import java.util.AbstractQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;
import java.util.logging.Logger;

/**
 * Message handler for case when communication is between multiple producers (agent,
 * plugin, service) and one consumer (Message Dispatcher).
 * <p>
 * The consumer has its own concurrent message queue and methods for managing the queue.
 * <p>
 * The queue is unbounded so there is no wait when a message is added to the queue. It
 * is up to the consumer to wait on an empty queue.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class ConcurrentMsgHandler implements IMsgHandler {
    private static Logger logger = Logger.getLogger(ConcurrentMsgHandler.class.getName());

    /** empty queue lock */
    private Semaphore available = new Semaphore(0);

    /** Message queue */
    ConcurrentLinkedQueue<IQiWorkbenchMsg> msgQueue = new ConcurrentLinkedQueue<IQiWorkbenchMsg>();

    /** Add message to queue and notify consumer. A NULL message is logged as
     *  a severe internal error and is ignored.
     * @param msg Message to be queued
     */
    public synchronized void enqueue(IQiWorkbenchMsg msg) {
        if (msg == null) {
            logger.warning("Internal error: enqueuing a null message; ignored");
            return;
        }
        logger.finest("msg: "+msg.toString());
        msgQueue.add(msg);
        available.release();
    }

    /** Get the next message sent for processing. Wait for one if queue empty.
     * @return Next message.
     */
    public IQiWorkbenchMsg dequeue() {
        try {
            available.acquire();
        } catch (InterruptedException ie) {
        }

        IQiWorkbenchMsg msg = msgQueue.poll();
        logger.finest("msg: "+msg.toString());
        return msg;
    }

    /** Get handler's message queue. Used by component to wait on an empty queue
     * @return Handler's message queue.
     */
    public AbstractQueue getMsgQueue() {
        return msgQueue;
    }

    /** Check if message queue is empty
     * @return true if empty; otherwise, false
     */
    public boolean isQueueEmpty() {
        return msgQueue.isEmpty();
    }

    /**
     * Reset the queue, i.e., clear it of any elements.
     *
     */
    public void resetQueue() {
        msgQueue.clear();
    }
    
    
    
    /**
     * There is a situation that the mesage is not for processing, but just
     * records the state for retrieving later (caching the sate of each plugins)
     * By doing this, we can access one plugin's state from other
     */
    Map<String, Map<String, Object>> cachedStates = new HashMap<String, Map<String, Object>>();
    
    /** getState
     *  @param key  which plugin state
     *  @param name state name 
     */
    public Object getState(String key, String name) {
    	Object retVal = null;
    	Map<String, Object> mapVars = cachedStates.get(key);
    	if (mapVars != null) {
    		retVal = mapVars.get(name);
    	}
    	return  retVal;
    }
    
    /** saveState only save a empty string at the moment 
     *  @param key  which plugin state
     *  @param name state name 
     */
    public synchronized void saveState(String key, String name, Object cid) {
    	Map<String, Object> mapVars = cachedStates.get(key);
    	if (mapVars == null) {
    		mapVars = new HashMap<String, Object>();
    		cachedStates.put(key, mapVars);
    	}
    	mapVars.put(name, cid);
    }
    
    /** saveState only save a empty string at the moment 
     *  @param key  which plugin state
     *  @param name state name 
     */
    public synchronized void updateState(String key, String oldName, String newName, Object cid) {
    	Map<String, Object> mapVars = cachedStates.get(key);
    	if (mapVars == null) {
    		mapVars = new HashMap<String, Object>();
    		cachedStates.put(key, mapVars);
    	}
    	mapVars.remove(oldName);
    	mapVars.put(newName, cid);
    }
    
    /** removeState
     *  @param key  which plugin state
     *  @param name state name 
     */
    public synchronized void removeState(String key, String name) {
    	Map<String, Object> mapVars = cachedStates.get(key);
    	if (mapVars != null) {
    		mapVars.remove(name);
    	}
    }
}
