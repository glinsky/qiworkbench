/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench;

import java.io.Serializable;

/**
 * Container for a file chooser's requester attributes. Registered with a
 * qiComponent's agent so the agent can perform the file chooser's action
 * after the user makes their selection. Contains the instance of the requester
 * who will be called upon (by the agent) to perform the chooser's action
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class FileChooserDescriptor implements Serializable {
//  static final long serialVersionUID = 0;

    /** Constructor
     * @param msgID ID of the file chooser request
     * @param requesterType Data type of the requester
     * @param requesterInstance Instance of the requester
     */
    public FileChooserDescriptor(String msgID, String requesterType, Object requesterInstance) {
        this.msgID = msgID;
        this.requesterType = requesterType;
        this.requesterInstance = requesterInstance;
    }

    /** Unique ID for response containing user's selection. */
    private String msgID = "";

    /** Data type of the requester */
    private String requesterType = "";

    /** Instance of requester of file chooser. */
    private Object requesterInstance;

    //Getters
    public String getMsgID() {
        return msgID;
    }
    public String getRequesterType() {
        return requesterType;
    }
    /** Up to the qiComponent agent to cast object to its content type */
    public Object getRequesterInstance() {
        return requesterInstance;
    }

    //Setters
    public void setMsgID(String msgID) {
        this.msgID = msgID;
    }
    public void setRequesterType(String requesterType) {
        this.requesterType = requesterType;
    }
    public void setRequesterInstance(Object requesterInstance) {
        this.requesterInstance = requesterInstance;
    }

    /** Display attributes of file chooser descriptor */
    public String toString() {
        StringBuffer buf  = new StringBuffer();
        buf.append(this.getClass().toString());
        buf.append(": \n Selection response ID=");
        buf.append(msgID);
        buf.append(", Requester Type=");
        buf.append(requesterType);
        buf.append(", Requester Instance=");
        buf.append(requesterInstance.toString());
        return buf.toString();
    }
}
