/*
 * ServerJobService.java
 *
 * Created on February 28, 2008, 3:54 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.qiworkbench.api;

import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.messageFramework.IMsgHandler;

/**
 *
 * @author folsw9
 */
public interface IServerJobService extends IqiWorkbenchComponent, Runnable {
    String getCID();

    IComponentDescriptor getComponentDesc();

    String getJobID();

    IServerMessagingManager getMessagingMgr();

    IMsgHandler getMsgHandler();

    /**
     * Initialize the job service. Create its messaging manager which
     * creates it message handler and component descriptor. Also, create
     * the service's job adapter.
     */
    void init();

    /**
     * Process a job request.
     * 
     * @param msg Request message
     * @return Response message; abnormal response if job error
     */
    IQiWorkbenchMsg processMsg(IQiWorkbenchMsg msg);

    /**
     * Initialize the service, then process received job requests.
     */
    void run();

    void setJobID(String jobID);
    
}
