/*
 * IServletDispatcher.java
 *
 * Created on February 28, 2008, 5:05 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.qiworkbench.api;

/**
 *
 * @author folsw9
 */
public interface IServletDispatcher {
    /**
     * Acquire a remote job service from the pool. If there is none, create one.
     * 
     * @return job service; null if cannot create one.
     */
    IServerJobService acquireRemoteJobService();

    /**
     * Get the line separator of the OS under which the Tomcat server executes.
     */
    String getServerOSLineSeparator();

    /**
     * Check if job service active.
     * 
     * @param service Service component.
     * @param try The number of times to try.
     * @return true if registered; otherwise, false
     */
    boolean isJobServiceActive(IServerJobService jobService, int tries);

    /**
     * Release a remote job service to the pool.
     * 
     * @param jobService Remote job service to release.
     */
    void releaseRemoteJobService(IServerJobService jobService);
    
}
