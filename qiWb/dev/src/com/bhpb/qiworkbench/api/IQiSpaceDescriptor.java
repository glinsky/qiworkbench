/*
 * NewInterface.java
 *
 * Created on February 25, 2008, 3:07 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.qiworkbench.api;

import java.io.Serializable;

/**
 *
 * @author folsw9
 */
public interface IQiSpaceDescriptor extends Serializable {
    /** qiSpace is a project */
    String PROJECT_KIND = "P";

    /** qiSpace is a workspace of projects */
    String WORKSPACE_KIND = "W";

    String getKind();

    String getPath();

    void setKind(String kind);

    void setPath(String path);

    /**
     * Display attributes of qiSpace 
     */
    String toString();
    
}
