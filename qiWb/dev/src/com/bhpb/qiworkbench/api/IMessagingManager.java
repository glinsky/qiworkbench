/*
 * NewInterface.java
 *
 * Created on February 25, 2008, 3:02 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.qiworkbench.api;

import java.util.ArrayList;

import javax.swing.JDesktopPane;

/**
 *
 * @author folsw9
 */
public interface IMessagingManager {
    /**
     *  Add a message to a qiComponent's message queue.
     * 
     * @param targetDesc  Consumer of message.
     * @param msg Message
     */
    void addMessageTo(IComponentDescriptor targetDesc, IQiWorkbenchMsg msg);

    /**
     * Check for a request matching the response. If found, remove from the
     * list of outstanding requests.
     * 
     * @param response message
     * @return Original request message if a match; otherwise, a NULL message.
     */
    IQiWorkbenchMsg checkForMatchingRequest(IQiWorkbenchMsg response);

    void displayMsgQueue();

    /**
     * Generate a component's CID given its class.
     * 
     * @param compClass Class of component
     * @return Component's CID
     */
    String genCID(Class compClass);

    /**
     * Get a registered component's descriptor given its CID.
     * 
     * @param cid CID of the component
     * @return Component's descriptor if registered; otherwise, null
     */
    IComponentDescriptor getComponentDesc(String cid);

    IComponentDescriptor getComponentDescFromDisplayName(String displayName);

    /**
     * Get job service preference, LOCAL or REMOTE
     * 
     * @return QIWConstants.LOCAL_SERVICE_PREF or QIWConstants.REMOTE_SERVICE_PREF
     */
    String getLocationPref();

    IQiWorkbenchMsg getMatchingOutstandingRequest(IQiWorkbenchMsg response);

    /**
     * Get response from message queue with the matching message ID. If none,
     * return null.
     * 
     * @param msgID Message ID waiting on for a response
     * @return Response message in queue with matching message ID; null if none.
     */
    IQiWorkbenchMsg getMatchingResponse(String msgID);

    /**
     * Get response from message queue with the matching message ID. If there
     * is none, wait for up to QIWConstants.RESPONSE_TIMEOUT (5 minutes) before
     * returning null.
     * 
     * @param msgID Message ID waiting on for a response
     *
     * @return Response message in queue with matching message ID; null if
     * response doesn't arrive in specified amount of time.
     */
    IQiWorkbenchMsg getMatchingResponseWait(String msgID);
    
    /**
     * Get response from message queue with the matching message ID. If there
     * is none, wait for the matching response to arrive up to the specified
     * duration of time (in milliseconds) expires.
     * 
     * @param msgID Message ID waiting on for a response
     * @param duration Amount of time in milliseconds to wait for a matching response.
     * @return Response message in queue with matching message ID; null if
     * response doesn't arrive in specified amount of time.
     */
    IQiWorkbenchMsg getMatchingResponseWait(String msgID, long duration);

    /**
     * Get the component descriptor for this registered component, i.e.,
     * the component represented by this messaging manager.
     * 
     * @return Component's descriptor if registered; otherwise, null
     */
    IComponentDescriptor getMyComponentDesc();

    /**
     * Get next message from message queue. If the message queue is empty,
     * return immediately with null.
     * 
     * @return Next message in queue; null if queue is empty.
     */
    IQiWorkbenchMsg getNextMsg();

    /**
     * Get next message from message queue. If the message queue is empty,
     * wait for a message to arrive.
     * 
     * @return Next message in queue.
     */
    IQiWorkbenchMsg getNextMsgWait();

    String getProject();

    IQiSpaceDescriptor getQispaceDesc();

    /**
     * Get the component parent display name information
     * 
     * @return Display name of the parent qiComponent
     */
    String getRegisteredComponentDisplayNameByDescriptor(IComponentDescriptor desc);

    /**
     * Get information about the OS of the remote server.
     * 
     * @return List of OS information, namely, name of OS, file separator.
     */
    ArrayList<String> getRemoteOSInfo();

    String getServerOSFileSeparator();

    String getServerOSLineSeparator();

    /**
     *  Get the state of a qiComponent.
     * 
     * @param key Which qiComponent
     * @param name State name
     * @return qiComponent's state.
     */
    Object getState(String key, String name);

    /**
     * Get URL where tomcat lives
     * 
     * @return Tomcat URL from Msg Dispatcher
     */
    String getTomcatURL();

    String getUserHOME();

    /**
     * Check if the message queue is empty.
     * 
     * @return true f message queue empty; otherwise, false.
     */
    boolean isMsgQueueEmpty();

    /**
     * Check if message is a request (command) message.
     * 
     * @param msg QiWorkbench message
     * @return true if a request (command) message; otherwise, false
     */
    boolean isRequestMsg(IQiWorkbenchMsg msg);

    /**
     * Check if response message is from the Message Dispatcher
     * 
     * @param msg QiWorkbench response message
     * @return true if from dispatcher; otherwise, false
     */
    boolean isResponseFromMsgDispatcher(IQiWorkbenchMsg msg);

    /**
     * Check if response message is from the requester of the message.
     * 
     * @param msg QiWorkbench response message
     * @param requesterDesc Descriptor of the producer.
     * @return true if from dispatcher; otherwise, false
     */
    boolean isResponseFromRequester(IQiWorkbenchMsg msg, IComponentDescriptor requesterDesc);

    /**
     * Check if message is a response message.
     * 
     * @param msg QiWorkbench message
     * @return true if a response message; otherwise, false
     */
    boolean isResponseMsg(IQiWorkbenchMsg msg);

    /**
     * Check if message is a trace (command data) message.
     * 
     * @param msg QiWorkbench message
     * @return true if a trace message; otherwise, false
     */
    boolean isTraceMsg(IQiWorkbenchMsg msg);

    /**
     *  Load variables from file if necessary (deserilizing)
     * 
     * @param key Which qiComponent
     * @param name State name
     * @param fromFile File containing variables.
     * @param xStream XML deserializer
     * @return 
     */
    Object loadFromFile(String key, String name, String fromFile, com.thoughtworks.xstream.XStream xStream);

    /**
     * Peek for next message from message queue. Wait until there is a message
     * on the queue.
     * 
     * @return Next message in queue for peeking, the message will remain in the queue; null if queue is empty.
     */
    IQiWorkbenchMsg peekNextMsg();

    /**
     * Register component with the Message Dispatcher. Form message handler, CID,
     * descriptor.The register and save the Message Dispatcher's message handler
     * so can communicate with it.
     * 
     * @param compKind Kind of component.
     * @param compName Display name of the component.
     * @param compCID Unique, system wide ID (CID) for the component.
     */
    void registerComponent(String compKind, String compName, String compCID);

    /**
     * Register component with the Message Dispatcher. Form message handler, CID,
     * descriptor.The register and save the Message Dispatcher's message handler
     * so can communicate with it.
     * 
     * @param compKind Kind of component.
     * @param compName Display name of the component.
     * @param compCID Unique, system wide ID (CID) for the component.
     * @param isSystemComp if the registered component is a system wide component.
     */
    void registerComponent(String compKind, String compName, String compCID, boolean isSystemComp);

    /**
     *  Remove the state of a qiComponent
     * 
     * @param key  Which qiComponent
     * @param name State name
     */
    void removeState(String key, String name);

    void renameComponent(String oldName, String newName);

    /**
     * Reset the message queue for the component.
     */
    void resetMsgQueue();

    /**
     * Route a message to the consumer. Used to route an abnormal message.
     * 
     * @param msg Message to be routed.
     */
    void routeMsg(IQiWorkbenchMsg msg);

    /**
     *  Save the state of a qiComponent. Only save a empty string at the moment.
     * 
     * @param key  Which qiComponent
     * @param name State name
     */
    void saveState(String key, String name);

    /**
     *  Save the state of a qiComponent.
     * 
     * @param key  Which qiComponent
     * @param name State name
     * @param val State
     */
    void saveState(String key, String name, Object val);

    /**
     *  Save an serialized object (in XML) to a file.
     * 
     * @param toFile File to save serialized object to.
     * @param xStream XML serializer
     * @param val Serialized object (in XML)
     */
    void saveToFile(String toFile, com.thoughtworks.xstream.XStream xStream, Object val);

    /**
     * Send a request to the Servlet Dispatcher for processing.
     * 
     * @param msg Request message to be sent
     * @return Response message
     */
    IQiWorkbenchMsg sendRemoteRequest(IQiWorkbenchMsg request);

    /**
     * Send a request with no content to the message dispatcher.
     * 
     * @param routing Kind of command.
     * @param command Command to be executed by the message dispatcher.
     * @return ID of request message.
     */
    String sendRequest(String routing, String command);

    /**
     * Send a request with no content to the designated consumer.
     * 
     * @param routing Kind of command.
     * @param command Command to be executed by the target consumer.
     * @param targetDesc Descriptor of the consumer.
     * @return ID of request message.
     */
    String sendRequest(String routing, String command, IComponentDescriptor targetDesc);

    /**
     * Send a request with content to the designated consumer.
     * 
     * @param routing Kind of command.
     * @param command Command to be executed by the target consumer.
     * @param targetDesc Descriptor of the consumer.
     * @param contentType Content's type
     * @param content Content object
     * @return ID of request message.
     */
    String sendRequest(String routing, String command, IComponentDescriptor targetDesc, String contentType, Object content);

    /**
     * Send a request with content to the designated consumer.
     * 
     * @param routing Kind of command.
     * @param command Command to be executed by the target consumer.
     * @param targetDesc Descriptor of the consumer.
     * @param contentType Content's type
     * @param content Content object
     * @param skip flag determining if the message should skip from component's main processMsg method mainly used by response type message.
     * @return ID of request message.
     */
    String sendRequest(String routing, String command, IComponentDescriptor targetDesc, String contentType, Object content, boolean skip);

    /**
     * Send a request with no content to the designated consumer.
     * 
     * @param routing Kind of command.
     * @param command Command to be executed by the target consumer.
     * @param targetDesc Descriptor of the consumer.
     * @param skip flag determining if the message should skip from component's main processMsg method mainly used by response type message.
     * @return ID of request message.
     */
    String sendRequest(String routing, String command, IComponentDescriptor targetDesc, boolean skip);

    /**
     * Send a request with content to the message dispatcher.
     * 
     * @param routing Kind of command.
     * @param command Command to be executed by the message dispatcher.
     * @param contentType Content's type
     * @param content Content object
     * @return ID of request message.
     */
    String sendRequest(String routing, String command, String contentType, Object content);

    /**
     * Send a request with content to the message dispatcher.
     * 
     * @param routing Kind of command.
     * @param command Command to be executed by the message dispatcher.
     * @param contentType Content's type
     * @param content Content object
     * @param skip flag determining if the message should skip from component's main processMsg method mainly used by response type message.
     * @return ID of request message.
     */
    String sendRequest(String routing, String command, String contentType, Object content, boolean skip);

    /**
     * Send a request with no content to the message dispatcher.
     * 
     * @param routing Kind of command.
     * @param command Command to be executed by the message dispatcher.
     * @param skip flag determining if the message should skip from component's main processMsg method mainly used by response type message.
     * @return ID of request message.
     */
    String sendRequest(String routing, String command, boolean skip);

    /**
     * Send a normal response with content back to the original requester,
     * 
     * @param request Original request message
     * @param contentType Content's type
     * @param content Content object
     */
    void sendResponse(IQiWorkbenchMsg request, String contentType, Object content);
    /**
     * Request workbench desktop container
     * @return Workbench Desktop Pane
     */
    JDesktopPane getWorkbenchDesktopPane();

    void unregisterComponent(IComponentDescriptor compDesc);

    /**
     *  Update the state of a qiComponent.
     * 
     * @param key  Which qiComponent
     * @param name State name
     * @param val New state
     */
    void updateState(String key, String oldName, String newName, Object val);
    
}
