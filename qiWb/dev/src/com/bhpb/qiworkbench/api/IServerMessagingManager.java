/*
 * IServerMessagingManager.java
 *
 * Created on February 28, 2008, 5:27 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.qiworkbench.api;

import com.bhpb.qiworkbench.messageFramework.IMsgHandler;

/**
 *
 * @author folsw9
 */
public interface IServerMessagingManager {
    IComponentDescriptor getComponentDesc();

    IMsgHandler getMsgHandler();

    /**
     * Get next message from message queue. If the message queue is empty,
     * wait for a message to arrive.
     * 
     * @return Next message in queue.
     */
    IQiWorkbenchMsg getNextMsgWait();

    /**
     * Put message on queue; wait for it to be received.
     * 
     * @param msg Message.to queue
     */
    void putMsgWait(IQiWorkbenchMsg msg);
    
}
