/*
 * IComponentDescriptor.java
 *
 * Created on February 25, 2008, 3:06 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.qiworkbench.api;

import com.bhpb.qiworkbench.messageFramework.IMsgHandler;
import java.io.Serializable;

/**
 *
 * @author folsw9
 */
public interface IComponentDescriptor extends Cloneable, Comparable<IComponentDescriptor>, Serializable {
    Object clone();

    int compareTo(IComponentDescriptor desc);

    String getCID();

    String getComponentKind();

    String getDisplayName();

    IMsgHandler getMsgHandler();

    String getPreferredDisplayName();

    String getProviderName();

    String getVersionNumber();

    void setCID(String cid);

    void setComponentKind(String compKind);

    void setDisplayName(String displayName);

    void setMsgHandler(IMsgHandler msgHandler);

    void setPreferredDisplayName(String displayName);

    void setProviderName(String providerName);

    void setVersionNumber(String versionNumber);

    /**
     * Display attributes of component 
     */
    String toString();
    
}
