package com.bhpb.qiworkbench.api;

import com.bhpb.qiworkbench.api.DataSystemConstants.FieldKind;
import com.bhpb.qiworkbench.api.DataSystemConstants.FloatFormat;
import com.bhpb.qiworkbench.api.DataSystemConstants.RecordType;
import java.nio.ByteOrder;

/**
 * Interface for most trace-related accessors.  This class is the primary location
 * of format IDataObject methods.
 * 
 * @author folsw9
 */
public interface ITrace {

    /**
     * Calculate the size of the fields in the data record.
     * @return Size of the fields in the data record.
     */
    int calcRecordSize();

    /**
     * Create an empty data record with a given capacity.
     * @param capacity Capacity of the data record created.
     */
    void createDataRecord(int capacity);

    /**
     * Get boolean value of a scalar field by generic name
     * @param field Generic name of field
     */
    boolean getBoolean(String fieldName);

    /**
     * Get boolean value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     */
    boolean getBoolean(String fieldName, int idx);

    /**
     * Get byte value of a scalar field by generic name
     * @param field Generic name of field
     * @return Field value as a byte.
     */
    byte getByte(String fieldName) throws GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Get byte value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @return Field value as a byte.
     */
    byte getByte(String fieldName, int idx) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException;

    ByteOrder getByteOrder();

    /**
     * Get char value of a scalar field by generic name.
     * Assumes encoding is 1 byte.
     * @param field Generic name of field
     * @return Field value as a single byte char
     */
    char getChar(String fieldName) throws GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Get char value of a vector field by generic name.
     * Assumes encoding is 1 byte.
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @return Field value as a single byte char
     */
    char getChar(String fieldName, int idx) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Extract the data record from its containing byte buffer.
     */
    byte[] getDataRecord();

    /**
     * Get this DO's unique ID
     * @return DO's unique ID
     */
    String getDoID();

    /**
     * Get double value of a scalar field by generic name
     * @param field Generic name of field
     * @return Field value as a double in IEEE format
     */
    double getDouble(String fieldName) throws GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Get double value a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @return Field value as a double in IEEE format
     */
    double getDouble(String fieldName, int idx) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException;

    FieldKind getFieldKind();

    /**
     * Get float value of a scalar field by generic name
     * @param field Generic name of field
     * @return Field value as a float in IEEE format
     */
    float getFloat(String fieldName) throws GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Get float value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @return Field value as a float in IEEE format
     */
    float getFloat(String fieldName, int idx) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException;

    FloatFormat getFloatFormat();

    /**
     * Get vector as floats
     * @return Vector of values as floats in IEEE format; null if DO isn't
     * a binary vector field.
     */
    float[] getFloatVector() throws GeoIndexOutOfBoundsException;

    /**
     * Get int value of a scalar field by generic name
     * @param field Generic name of field
     * @return Field value as an int.
     */
    int getInt(String fieldName) throws GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Get int value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @return Field value as an int.
     */
    int getInt(String fieldName, int idx) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Get long value of a scalar field by generic name
     * @param field Generic name of field
     * @return Field value as a long.
     */
    long getLong(String fieldName) throws GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Get long value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @return Field value as a long.
     */
    long getLong(String fieldName, int idx) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException;

    int getRecordSize();

    RecordType getRecordType();

    /**
     * Get short value of a scalar field by generic name
     * @param field Generic name of field
     * @return Field value as a short
     */
    short getShort(String fieldName) throws GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Get short value of a vector field by generic name
     * @param field Generic name of record
     * @param idx Index of vector element accessing.
     * @return Field value as a short
     */
    short getShort(String fieldName, int idx) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Get String value of a scalar field by generic name
     * @param field Generic name of field
     */
    String getString(String fieldName);

    /**
     * Get String value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     */
    String getString(String fieldName, int dix);

    /**
     * Check if thie DO is empty, i.e., contains no data
     * @reutrn true if empty; otherwise, false.
     */
    boolean isEmpty();

    /**
     * Set boolean value of a scalar field by generic name
     * @param field Generic name of field
     * @param val boolean value
     */
    void setBoolean(String fieldName, boolean val);

    /**
     * Set boolean value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val boolean value
     */
    void setBoolean(String fieldName, int idx, boolean val) throws GeoIndexOutOfBoundsException;

    /**
     * Set byte value of a scalar field by generic name
     * @param field Generic name of field
     * @param val byte value
     */
    void setByte(String fieldName, byte val) throws GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Set byte value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val byte value
     */
    void setByte(String fieldName, int idx, byte val) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Set char value of a scalar field by generic name.
     * Assumes encoding is 1 byte.
     * @param field Generic name of field
     * @param val char value
     */
    void setChar(String fieldName, byte val) throws GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Set char value of a vector field by generic name.
     * Assumes encoding is 1 byte.
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val char value
     */
    void setChar(String fieldName, int idx, byte val) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Create the containing byte buffer for a data record
     */
    void setDataRecord(byte[] drec);

    /**
     * Set the DO's unique ID
     * @param DO's unique ID
     */
    void setDoID(String pojoID);

    /**
     * Set double value of a scalar field by generic name
     * @param field Generic name of field
     * @param val double value
     */
    void setDouble(String fieldName, double val) throws GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Set double value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val double value
     */
    void setDouble(String fieldName, int idx, double val) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException;

    void setFieldKind(FieldKind fieldKind);

    /**
     * Set float value of a scalar field by generic name
     * @param field Generic name of field
     * @param val float value
     */
    void setFloat(String fieldName, float val) throws GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Set float value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val float value
     */
    void setFloat(String fieldName, int idx, float val) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException;

    void setFloatFormat(FloatFormat floatFormat);

    /**
     * Set int value of a scalar field by generic name
     * @param field Generic name of field
     * @param val Boolean value
     */
    void setInt(String fieldName, int val) throws GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Set int value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val Boolean value
     */
    void setInt(String fieldName, int idx, int val) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Set long value of a scalar field by generic name
     * @param field Generic name of field
     * @param val long value
     */
    void setLong(String fieldName, long val) throws GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Set long value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val long value
     */
    void setLong(String fieldName, int idx, long val) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException;

    void setRecordSize(int recordSize);

    void setRecordType(RecordType recordType);

    /**
     * Set short value of a scalar field by generic name
     * @param field Generic name of field
     * @param val short value
     */
    void setShort(String fieldName, short val) throws GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Set short value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val short value
     */
    void setShort(String fieldName, int idx, short val) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException;

    /**
     * Set String value of a scalar field by generic name
     * @param field Generic name of field
     * @param val String value
     */
    void setString(String fieldName, String val);

    /**
     * Set String value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val String value
     */
    void setString(String fieldName, int idx, String val) throws GeoIndexOutOfBoundsException;
}
