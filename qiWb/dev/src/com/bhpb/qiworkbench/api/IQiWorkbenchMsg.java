/*
 * IQiWorkbenchMsg.java
 *
 * Created on February 25, 2008, 3:05 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.qiworkbench.api;

import java.io.Serializable;

/**
 *
 * @author folsw9
 */
public interface IQiWorkbenchMsg extends Serializable {
    String getCommand();

    String getConsumerCID();

    /**
     * Up to the receipent to cast object to its content type 
     */
    Object getContent();

    String getContentType();

    String getMsgID();

    String getMsgKind();

    String getProducerCID();

    int getStatusCode();

    Object getValueFromContentHashMap(Object key);

    /**
     * Check if the message status code indicates an abnormal condition occurred
     * during the processing of a request message that prevented it from being
     * successfully completed. An UNKNOWN status code is not considered
     * abnormal.
     * 
     * @return true if a processing error occured; otherwise, false.
     */
    boolean isAbnormalStatus();

    void setCommand(String cmd);

    void setConsumerCID(String cid);

    void setContent(Object content);

    void setContentType(String ct);

    void setMsgID(String mid);

    void setMsgKind(String msgKind);

    void setProducerCID(String pid);

    void setSkip(boolean skip);

    void setStatusCode(int sc);

    /**
     * Check if the message is instructed to skip from
     * being collected by the consumer message process loop which will
     * normally claim any message sitting in the message queue.
     * 
     * This is to ensure synchronization between sending a request and
     * receiving a corresponding response and to avoid race condition
     * within the same thread.
     * 
     * The following should run as a pair so that the response message will not stay at consumer queue unclaimed.
     * 
     * String msgID = messagingMgr.sendRequest(..,true); //skip is set to be true
     * QiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID..);
     * 
     * 
     * @return true if a DATA message and skip set to be true; otherwise, false.
     */
    boolean skip();

    /**
     * Display attributes of message 
     */
    String toString();
    
}
