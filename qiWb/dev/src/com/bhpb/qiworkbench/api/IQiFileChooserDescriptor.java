/*
 * IQiFileChooserDescriptor.java
 *
 * Created on February 26, 2008, 9:11 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.qiworkbench.api;

import java.awt.Component;
import java.util.List;
import java.util.Map;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author folsw9
 */
public interface IQiFileChooserDescriptor {
    Map getAdditionalProperties();

    int getDialogType();

    String getDirectoryRemembered();

    FileFilter getFileFilter();

    int getFileSelectionMode();

    String getHomeDirectory();

    String getMessageCommand();

    Component getParentGUI();

    String getPreText();

    IComponentDescriptor getProducerComponentDescriptor();

    int getReturnCode();

    List<String> getSelectedFileNameList();

    String getSelectedFilePathBase();

    String getServerUrl();

    String getTitle();

    boolean isDirectoryOnlyEnabled();

    boolean isDirectoryRememberedEnabled();

    boolean isDirectorySelectionEnabled();

    boolean isFileSelectionEnabled();

    boolean isFilesDirectories();

    boolean isMultiSelectionEnabled();

    boolean isNavigationUpwardEnabled();

    void setAdditionalProperties(Map properties);

    void setDialogType(int type);

    void setDirectoryOnlyEnabled(boolean bool);

    void setDirectoryRemembered(String dir);

    void setDirectoryRememberedEnabled(boolean enabled);

    void setFileFilter(FileFilter filter);

    /**
     * Sets the JFileChooser to allow the user to just select files, just select directories, or 
     * 		select both files and directories. The default is JFilesChooser.FILES_ONLY.
     */
    void setFileSelectionMode(int mode);

    void setHomeDirectory(String home);

    void setMessageCommand(String command);

    void setMultiSelectionEnabled(boolean enabled);

    void setNavigationUpwardEnabled(boolean enabled);

    void setParentGUI(Component gui);

    void setPreText(String text);

    void setReturnCode(int code);

    void setSelectedFileNameList(List<String> list);

    void setSelectedFilePathBase(String dir);

    void setServerUrl(String url);

    void setTitle(String title);

    void setetProducerComponentDescriptor(IComponentDescriptor desc);   
}