/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.api;

/**
 * DataSystem constants used within the geoIO library
 *
 * @author Gil Hansen
 * @version 1.0
 */
public final class DataSystemConstants {
    /**
     * Prevent object construction outside of this class.
     */
    private DataSystemConstants(){}

    /** Endian byte orderings. Java's default is Big Endian. */
    //DataObject now uses java.nio.ByteOrder
    //public enum Endian {BIG_ENDIAN, LITTLE_ENDIAN};

    /** Floating-point formats. Java's default is IEEE. */
    public enum FloatFormat {IEEE_FORMAT, IBM_FORMAT};

    /** Type of data record */
    public enum RecordType {BINARY, BINARY_BLOCK, TEXT}

    /** Kind of field (in a data record) */
    public enum FieldKind {SCALAR, VECTOR}

    /** Character encodings */
    public enum CharEncoding {ASCII, EBCDIC, UNICODE}

    /**
     * Implementation type. The Java implementation type is mapped to this
     * enumeration when read in the XML self-description for a data record
     * field.
     */
    public enum ImplType {NOTYPE, ARRAY, BOOLEAN, BYTE, SHORT, CHAR, INT, LONG, FLOAT, DOUBLE, STRING};

    //JAVA IMPLEMENTATION TYPES
    static public final String ARRAY_JAVA = "java.lang.Array";
    static public final String BOOLEAN_JAVA = "java.lang.Boolean";
    static public final String BYTE_JAVA = "java.lang.Byte";
    static public final String CHAR_JAVA = "java.lang.Char";
    static public final String SHORT_JAVA = "java.lang.Short";
    static public final String INT_JAVA = "java.lang.Integer";
    static public final String LONG_JAVA = "java.lang.Long";
    static public final String FLOAT_JAVA = "java.lang.Float";
    static public final String DOUBLE_JAVA = "java.lang.Double";
    static public final String STRING_JAVA = "java.lang.String";

    //Character encodings.
    static public final String ASCII_ENCODING = "ASCII";
    static public final String EBCDIC_ENCODING = "EBCDIC";
    static public final String UNICODE_ENCODING = "UNICODE";
    static public final String UNDEF_ENCODING = "UNDEF";

    //Endianess
    static public final String BIG_ENDIAN = "Big Endian";
    static public final String LITTLE_ENDIAN = "Little Endian";
    static public final String UNDEF_ENDIAN = "UNDEF";

    //Floating formats
    static public final String IBM_FLOAT_FORMAT = "IBM";
    static public final String IEEE_FLOAT_FORMAT = "IEEE";
    static public final String UNDEF_FLOAT_FORMAT = "UNDEF";

    //GEOPHYSICAL FORMATS
    static public final String SEGY_REV0_FORMAT = "SEGY_rev0";
    static public final String SEGY_REV1_FORMAT = "SEGY_rev1";
    static public final String BHP_SU_FORMAT = "BHP_SU";
    static public final String SU_FORMAT = "SU";

    //SEGY REV0 DATA RECORDS
    static public final String SEGY_REV0_ASCII_HEADER_ID = "SEGYrev0_ASCIIHeader";
    static public final String SEGY_REV0_BINARY_HEADER_ID = "SEGYrev0_BinaryHeader";
    static public final String SEGY_REV0_TRACE_BLOCK_ID = "SEGYrev0_TraceBlock";

    //URIs FOR SEGY REV0 FORMAT XML DEFINITIONS
	static public final String SEGY_REV0_URI_BASE = "/com/bhpb/geoio/datasystems/xmldesc/SEGYrev0/";
    static public final String SEGY_REV0_FORMAT_URI = SEGY_REV0_URI_BASE+"SEGYrev0_Format.xml";
	
    //BHP-S DATA RECORDS
    static public final String BHP_SU_TRACE_BLOCK_ID = "BhpSU_TraceBlock";

    //URIs FOR BHP-SU FORMAT XML DEFINITIONS
	static public final String BHP_SU_URI_BASE = "/com/bhpb/geoio/datasystems/xmldesc/BhpSU/";
    static public final String BHP_SU_FORMAT_URI = BHP_SU_URI_BASE+"BhpSU_Format.xml";
}
