/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.client.services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.QiwIOException;

/**
 * Write a text (ASCI) file.
 */
public class LocalWriteFile {
    private static Logger logger = Logger.getLogger(LocalWriteFile.class.getName());

    /**
     * Write a text file. If the file exists, it will be overwritten, i.e., the
     * lines to be written will not be appended.
     *
     * @param listItems First item in list the path of the file. The remaining
     * items the lines of the file.
     * @return IO status.
     * @throws QiwIOException If the file path is null or empty, or there is
     * an IO exception while writing the file.
     */
    public void writeFile(ArrayList<String> listItems) throws QiwIOException {
        String filePath = listItems.get(0);
        if (filePath == null || filePath.length() == 0) throw new QiwIOException("invalid file path="+filePath);

        File f = new File(filePath);
        BufferedWriter bw = null;

        try {
            FileOutputStream fos = new FileOutputStream(f);
            bw = new BufferedWriter(new OutputStreamWriter(fos));
            //write out the file line by line
            for (int i = 1; i < listItems.size(); i++) {
                String line = listItems.get(i);
                bw.write(line);
                bw.newLine();   //write the line separator
            }
        } catch (IOException ioe) {
            if(ioe instanceof FileNotFoundException && ioe.getMessage().contains("Permission denied"))
                throw new QiwIOException("Insufficient privilege to write.");
            else
            throw new QiwIOException("IO exception writing file:"+ioe.getMessage());
        } finally {
            try {
                if (bw != null) bw.close();
            } catch (IOException e) {}
        }
    }
}
