/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.client.services;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.util.ComponentUtils;
import com.bhpb.qiworkbench.client.util.ReadDataFromSocket;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.workbench.QiComponentBase;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Local IO service, a core component of qiWorkbench. It is started as a thread
 * by the Messenger Dispatcher and waits for IO commands. Local IO means the
 * files are accessible from the user's machine. This is in contrast to remote
 * IO which means the file is accessible from the machine running the Tomcat
 * server. By accessible we mean files on the machine or in a cross-mounted
 * file system. When Tomcat is running on the user's machine, local and remote
 * IO are the same.
 */
public class IOService extends QiComponentBase implements IqiWorkbenchComponent, Runnable {
    private static final Logger logger = Logger.getLogger(IOService.class.getName());

    private static final String TEMP_FILE_PREFIX = "BHP_";
    private static final String TEMP_FILE_SUFFIX = ".sutmp";
    private static final String NOMINAL_STATUS = "";
    
    /** Messaging manager for service */
    private MessagingManager messagingMgr;

    /** CID for component instance. Generated before the thread is started and carried as the thread's name. */
    private String myCID = "";

    public String getCID() {
        return myCID;
    }

    /**
     * Initialize the service component:
     * <ul>
     * <li>Create its messaging manager</li>
     * <li>Register with the Message Dispatcher</li>
     * </ul>
     */
    public void init() {
        try {
            QIWConstants.SYNC_LOCK.lock();

            messagingMgr = new MessagingManager();

            myCID = Thread.currentThread().getName();

            // register self with the Message Dispatcher
            messagingMgr.registerComponent(QIWConstants.LOCAL_SERVICE_COMP, QIWConstants.LOCAL_IO_SERVICE_NAME, myCID);

            // notify the startup method initialization finished
            QIWConstants.SYNC_LOCK.unlock();
            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in IOService.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

    /** Initialize the service, then process received IO requests.
     */
    public void run() {
        // initialize the IO service
        init();

        // Process IO requests made on behalf of the Message Dispatcher
        // for another component. The dispatcher will route the response back
        // to the component. The dispatcher must be involved for it pools the
        // IO threads.
        while(true) {
            IQiWorkbenchMsg msg = messagingMgr.getNextMsgWait();
            processMsg(msg);
        }
    }

    /**
     * Process the request/response message
     *
     * @param msg The message to be processed.
     */
    public void processMsg(IQiWorkbenchMsg msg) {
        /** Request that matches the response */
        IQiWorkbenchMsg request = null, response = null;

        //log message traffic
        // JE FIELD added for debugging
        System.out.println("client IOService::processMsg: msg="+msg.toString());
        logger.fine("client IOService::processMsg: msg="+msg.toString());
        // Check if a response. If so, process it based on the matching request
        // and consume the response
        if (messagingMgr.isResponseMsg(msg)) {
            //TODO: what is this block of code intended to do?  if nothing, remove it
            //or at most log a warning that the IOService received a response
            //and whether or not it matched a request, and if so, what type.
            request = messagingMgr.checkForMatchingRequest(msg);

            // check if from the message dispatcher
            if (messagingMgr.isResponseFromMsgDispatcher(msg)) {
                String cmd = request.getCommand();
                if (cmd.equals(QIWConstants.NULL_CMD)) {
                    return;
                }
            }
        } else if (messagingMgr.isRequestMsg(msg)) { //Check if a request. If so, process and send back a response
            String cmd = msg.getCommand();
            if (cmd.equals(QIWConstants.CREATE_LOCAL_DIRECTORY_CMD)) {
                String dirPath = (String)msg.getContent();
                try {
                    File dir = new File(dirPath);
                    if(dir.mkdirs()) {
                        messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, "success");
                    } else {
                        messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, "failure");
                    }
                } catch(Exception e) {
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+ e.getMessage());
                    messagingMgr.routeMsg(response);
                }
            } else if (cmd.equals(QIWConstants.CHECK_LOCAL_FILE_EXIST_CMD)) {
                String filePath = (String)msg.getContent();
                try {
                    File file = new File(filePath);
                    if(file.exists()) {
                        messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, "yes");
                    } else {
                        messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, "no");
                    }
                } catch(Exception e){
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+ e.getMessage());
                    messagingMgr.routeMsg(response);
                }
            } else if (cmd.equals(QIWConstants.MOVE_LOCAL_FILE_CMD)) {
            	List<String> list = (List<String>)msg.getContent();
                try {
                	String s0 = list.get(0);
                	String s1 = list.get(1);
                	boolean enforce = false;
                	if(list.size() > 2){
                		String s = list.get(2);
                		if(s.equals("-f"))
                			enforce = true;
                	}
                    String status = copyFile(s0, s1, enforce);
                    if(status.equals("success"))
                    	deleteFile(s0);
                    messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, "success");
                } catch(Exception e){
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+ e.getMessage());
                    messagingMgr.routeMsg(response);
                }
            } else if (cmd.equals(QIWConstants.DELETE_LOCAL_FILE_CMD)) {
            	String filePath = (String)msg.getContent();
                try {
                    File infile = new File(filePath);
                    infile.delete();
                    messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, "success");
                } catch(Exception e){
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+ e.getMessage());
                    messagingMgr.routeMsg(response);
                }
            } else if (cmd.equals(QIWConstants.COPY_LOCAL_FILE_CMD)) {
            	List<String> list = (List<String>)msg.getContent();
                try {
                	String source_name = list.get(0);
                	String dest_name = list.get(1);
                	String flag = null;
                	if(list.size() > 2)
                		flag = list.get(2);
                	boolean enforce = false;
                	if(flag != null && flag.equals("-f"))
                		enforce = true;
                	String status = copyFile(source_name, dest_name, enforce);
                	messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, status);
                	return;
       /*         	File source_file = new File(source_name);
                    File destination_file = new File(dest_name);
                    FileInputStream source = null;
                    FileOutputStream destination = null;
                    byte[] buffer;
                    int bytes_read;
                    
                    try {
                        // First make sure the specified source file 
                        // exists, is a file, and is readable.
                        if (!source_file.exists() || !source_file.isFile()){
                        	messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, source_name + " file does not exist.");
                        	return;
                        }
                        if (!source_file.canRead()){
                        	messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, source_name + " file is unreadable.");
                        	return;
                        }
                        
                        // If the destination exists, make sure it is a writeable file
                        // and ask before overwriting it.  If the destination doesn't
                        // exist, make sure the directory exists and is writeable.
                        if (destination_file.exists()){
                        	if(flag != null && flag.equals("-f")) {
	                            if(destination_file.isFile()) {
	                                if (!destination_file.canWrite()){
	                                	messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, destination_file + " file is unwriteable.");
	                                	return;
	                                }
	                            }else{
	                            	messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, destination_file + " file is not a file.");
	                            	return;
	                            }
                        	}else{
                        		messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, destination_file + " copy file cancelled. to enforce copy use -f parameter.");
                        		return;
                        	}
                        } else {
                        	File parentdir = parent(destination_file);
                            if (!parentdir.exists()){
                            	messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, "destination directory " + destination_file.getParent() + " doesn't exist");
                            	return;
                            }
                            if (!parentdir.canWrite()){
                            	messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, "destination directory " + destination_file.getParent() + " is unwriteable");
                            	return;
                            }
                        }
                    	// If we've gotten this far, then everything is okay; we can
                    	// copy the file.
                    	source = new FileInputStream(source_file);
                    	destination = new FileOutputStream(destination_file);
                    	buffer = new byte[1024];
                    	while(true) {
                    		bytes_read = source.read(buffer);
                    		if (bytes_read == -1) break;
                    		destination.write(buffer, 0, bytes_read);
                    	}
                    	messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, destination_file.getAbsolutePath() + " is created.");
                    }
                    // No matter what happens, always close any streams we've opened.
                    finally {
                        if (source != null) 
                            try { source.close(); } catch (IOException e) { ; }
                        if (destination != null) 
                            try { destination.close(); } catch (IOException e) { ; }
                    }
                    */
                } catch(Exception e){
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+ e.getMessage());
                    messagingMgr.routeMsg(response);
                }                
            } else if (cmd.equals(QIWConstants.GET_LOCAL_DIR_FILE_LIST_CMD)) {
                String dirPath = (String)msg.getContent();
                ArrayList files = null;
                try {
                    files = new LocalGetFileList().getDirectoryFileList(dirPath);
                } catch (QiwIOException qioe) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qioe.getMessage());
                    messagingMgr.routeMsg(response);
                }
                //send a normal response back
                messagingMgr.sendResponse(msg, QIWConstants.ARRAYLIST_TYPE, files);
            } else if (cmd.equals(QIWConstants.GET_LOCAL_FILE_LIST_CMD)) {
                String dirPath = (String)msg.getContent();
                ArrayList files = null;
                try {
                    files = new LocalGetFileList().getFileList(dirPath);
                } catch (QiwIOException qioe) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qioe.getMessage());
                    messagingMgr.routeMsg(response);
                }
                // send a normal response back
                messagingMgr.sendResponse(msg, QIWConstants.ARRAYLIST_TYPE, files);
            } else if (cmd.equals(QIWConstants.LOCAL_FILE_READ_CMD)) {
                String filePath = (String)msg.getContent();
                ArrayList fileLines = null;
                try {
                    fileLines = new LocalReadFile().readFile(filePath);
                } catch (QiwIOException qioe) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qioe.getMessage());
                    messagingMgr.routeMsg(response);
                }
                //Send a normal response back
                messagingMgr.sendResponse(msg, QIWConstants.ARRAYLIST_TYPE, fileLines);
            } else if (cmd.equals(QIWConstants.LOCAL_FILE_WRITE_CMD)) {
                ArrayList<String> fileItems = (ArrayList<String>)msg.getContent();
                try {
                    new LocalWriteFile().writeFile(fileItems);
                } catch (QiwIOException qioe) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qioe.getMessage());
                    messagingMgr.routeMsg(response);
                }
                //Send an acknowledge response back indicating file successfully
                //written.
                messagingMgr.sendResponse(msg, "", "");
            } else if (cmd.equals(QIWConstants.LOCAL_BINARY_FILE_READ_CMD)) {
				//String filePath = (String)msg.getContent();
				ArrayList params = (ArrayList)msg.getContent();
				String filePath = (String)params.get(0);
				Long lOffset = (Long)params.get(1);
				Long lLen = (Long)params.get(2);
				long offset = lOffset.longValue();
				long len = lLen.longValue();
				byte[] bs = new byte[0];
				try {
					bs = new LocalReadBinaryFile().readFile(filePath,offset,len);
				} catch (QiwIOException qioe) {
					//send an abnormal response back
					response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qioe.getMessage());
					messagingMgr.routeMsg(response);
					return;
				}
				//Send a normal response back
				messagingMgr.sendResponse(msg, QIWConstants.ARRAYLIST_TYPE, bs);
            } else if (cmd.equals(QIWConstants.LOCAL_BINARY_FILE_WRITE_CMD)) {
                ArrayList fileItems = (ArrayList)msg.getContent();
                try {
                    new LocalWriteBinaryFile().writeBinaryFile(fileItems);
                } catch (QiwIOException qioe) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qioe.getMessage());
                    messagingMgr.routeMsg(response);
                }
                //Send an acknowledge response back indicating file successfully
                //written.
                messagingMgr.sendResponse(msg, "", "");
            } else if (cmd.equals(QIWConstants.REMOTE_READ_SEGY_DATA_CMD) ||   //Read the SEGY/BHP-SU byte data from the socket channel it is being written to by
                cmd.equals(QIWConstants.REMOTE_READ_BHPSU_DATA_CMD)) {  //the remote IO service.

                //Form SocketChannel and connect to the server
                //get the URL and port
                ArrayList<String> socketInfo = (ArrayList<String>)msg.getContent();
                String serverURL = socketInfo.get(0);
                int port = Integer.parseInt(socketInfo.get(1));
                
                //Give server time to set up the port
                //TODO: is this necessary?
                logger.info("IOService:ReadDataFromSocket serverURL= " + serverURL + " port="+port);
                try {
                    Thread.sleep(300);
                } catch (InterruptedException ie) {
                    logger.warning("While waiting 300ms in IOService:ReadDataFromSocket for server to set up port, caught: " + ie.getMessage());
                }
                
				//Set up a temp file to hold the data
                File tempFile = null;
                FileOutputStream tempOutputStream = null;
                String opStatus = "";
                try {
                    tempFile = File.createTempFile(TEMP_FILE_PREFIX, TEMP_FILE_SUFFIX);
                    tempOutputStream = new FileOutputStream(tempFile);
                } catch (IOException ioe) {
                    opStatus = "Unable to open output stream for temp file due to: " + ioe.getMessage();
                }
                
                if (tempOutputStream == null) {
                    opStatus = "Unable to open outputStream for file: " + tempFile.getName() + "due to unknown non-exception error";
                }
				
                //Read data into a tempfile and return the name of the temp file
                //in the response if the read is successful. Try connection
                //up to 3 times.
                if (isStatusNominal(opStatus)) {
logger.info("IOService:ReadDataFromSocket temp file="+tempFile.getAbsolutePath());
                    try {
						ReadDataFromSocket reader = new ReadDataFromSocket(serverURL, port);
						
						//set up the socket channel for reading a set of trace blocks at a time
						String status = reader.setupSocketChannel();
						if (!status.equals("")) {
							//send an abnormal response back
							response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Error: Could not set up socket channel: " + status);
							messagingMgr.routeMsg(response);
							return;
						}
                        opStatus = reader.read(tempOutputStream);
                        //Release the socket channel for reuse
                        reader.teardownSocketChannel();
                        if (!isStatusNominal(opStatus)) {
                            if (opStatus.indexOf("Connection refused") != -1) {
                                logger.info("IOService: Connection refused. Trying to read data from socket a 2nd time");
                                //Give server time to set up the port
                                try {
                                    Thread.sleep(300);
                                } catch (InterruptedException ie) {
                                    logger.warning("While waiting 300ms in IOService:ReadDataFromSocket for server to set up port, caught: " + ie.getMessage());
                                }
                                //try getting data a 2nd time-
                                reader = new ReadDataFromSocket(serverURL, port);
                                //set up the socket channel for reading a set of trace blocks at a time
                                status = reader.setupSocketChannel();
                                if (!status.equals("")) {
                                    //send an abnormal response back
                                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Error: Could not set up socket channel: " + status);
                                    messagingMgr.routeMsg(response);
                                    return;
                                }
                                opStatus = reader.read(tempOutputStream);
								//Release the socket channel for reuse
								reader.teardownSocketChannel();
                                if (!isStatusNominal(opStatus)) {
                                    if (opStatus.indexOf("Connection refused") != -1) {
                                        //Give server time to set up the port
                                        try{
                                            Thread.sleep(300);
                                        } catch (InterruptedException ie) {
                                            logger.warning("While waiting 300ms in IOService:ReadDataFromSocket for server to set up port, caught: " + ie.getMessage());
                                        }
                                        logger.info("IOService: Connection refused. Trying to read data from socket a 3rd time");
                                        //try getting data a 3rd time
                                        reader = new ReadDataFromSocket(serverURL, port);
										//set up the socket channel for reading a set of trace blocks at a time
										status = reader.setupSocketChannel();
										if (!status.equals("")) {
											//send an abnormal response back
											response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Error: Could not set up socket channel: " + status);
											messagingMgr.routeMsg(response);
											return;
										}
                                        opStatus = reader.read(tempOutputStream);
										//Release the socket channel for reuse
										reader.teardownSocketChannel();
                                        if (!isStatusNominal(opStatus)) {
                                            //form an abnormal response
                                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_SOCKET_CHANNEL_ERROR, opStatus);
                                        }
                                    } else {
                                        //form an abnormal response
                                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_SOCKET_CHANNEL_ERROR, opStatus);
                                    }
                                }
                            } else {
                                //form an abnormal response
                                response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_SOCKET_CHANNEL_ERROR, opStatus);
                            }
                        }
                        if (isStatusNominal(opStatus)) {
                            messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, tempFile.getCanonicalFile().toString());
                        } else if (response == null) {
                            logger.warning("Socket read status was: " + opStatus + " after 1-3 attempts, but no response was generated.  Creating SC_SOCKET_CHANNEL_ERROR response now.");
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_SOCKET_CHANNEL_ERROR, opStatus);
                        }
                    } catch (IOException ioe) {
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, ioe.getMessage());
                    } finally {
                        if (tempOutputStream != null) {
                            try {
                                tempOutputStream.close();
                            } catch (IOException ioe ){
                                logger.warning("While attempting to close tempOutputStream, caught: " + ioe.getMessage());
                            }
                        }
                    }
                }
            } else { // fallthrough case: command not handled because it did not match any known IOService command
                logger.warning("IO message not processed:"+msg.toString());
                response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Command '" + request.getCommand() + "' not handled because it did not match any known IOService command name.");
            }
            //Send a normal response back
            if (response != null) {
                messagingMgr.routeMsg(response);
            } else {
                logger.warning("Unable to send null response from IOService:processMsg()");
            }
        }
    }

    /** Launch the IO service:
     *  <ul>
     *  <li>Start up the service thread which will initialize the service.</li>
     *  </ul>
     * <p>
     * NOTE: Each thread's init() must finish before the next thread is
     * started. This is accompished by monitoring the SYNC_LOCK object.
     */
    public static void main(String[] args) {
        IOService ioServiceInstance = new IOService();
        // get a CID for the new component instance
        String cid = ComponentUtils.genCID(IOService.class);
        // use the CID as the name of the thread
        Thread serviceThread = new Thread(ioServiceInstance, cid);
        serviceThread.start();
        long threadId = serviceThread.getId();
        logger.info("Local IO Service Thread-"+Long.toString(threadId)+" started");

        // When the services's init() is finished, it will release the lock
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();
    }
    
    /**
     * Returns true if and only if the status string is empty.
     */
    private boolean isStatusNominal(String status) {
        return NOMINAL_STATUS.equals(status);
    }
    
//  File.getParent() can return null when the file is specified without
    // a directory or is in the root directory.  
    // This method handles those cases.
    private static File parent(File f) {
        String dirname = f.getParent();
        if (dirname == null) {
            if (f.isAbsolute()) return new File(File.separator);
            else return new File(System.getProperty("user.dir"));
        }
        return new File(dirname);
    }
    
    private static void deleteFile(String filePath) throws IOException{
        File infile = new File(filePath);
        infile.delete();
    }
    
    private static String copyFile(String source_name, String dest_name, boolean enforce) throws Exception{
    	File source_file = new File(source_name);
        File destination_file = new File(dest_name);
        FileInputStream source = null;
        FileOutputStream destination = null;
        byte[] buffer;
        int bytes_read;
        
        try {
            // First make sure the specified source file 
            // exists, is a file, and is readable.
            if (!source_file.exists() || !source_file.isFile()){
            	return source_name + " file does not exist.";
            }
            if (!source_file.canRead()){
            	return  source_name + " file is unreadable.";
            }
            
            // If the destination exists, make sure it is a writeable file
            // and ask before overwriting it.  If the destination doesn't
            // exist, make sure the directory exists and is writeable.
            if (destination_file.exists()){
            	if(enforce) {
                    if(destination_file.isFile()) {
                        if (!destination_file.canWrite()){
                        	return destination_file + " file is unwriteable.";
                        }
                    }else{
                    	return  destination_file + " file is not a file.";
                    }
            	}else{
            		return "copy file " + source_file + " to " + destination_file + " cancelled. to enforce copy use -f parameter.";
            	}
            } else {
            	File parentdir = parent(destination_file);
                if (!parentdir.exists()){
                	return "destination directory " + destination_file.getParent() + " doesn't exist";
                }
                if (!parentdir.canWrite()){
                	return  "destination directory " + destination_file.getParent() + " is unwriteable";
                }
            }
        	// If we've gotten this far, then everything is okay; we can
        	// copy the file.
        	source = new FileInputStream(source_file);
        	destination = new FileOutputStream(destination_file);
        	buffer = new byte[1024];
        	while(true) {
        		bytes_read = source.read(buffer);
        		if (bytes_read == -1) break;
        		destination.write(buffer, 0, bytes_read);
        	}
        	//return destination_file.getAbsolutePath() + " is created.";
        	return "success";
        }        
        // No matter what happens, always close any streams we've opened.
        finally {
            if (source != null) 
                try { source.close(); } catch (IOException e) { ; }
            if (destination != null) 
                try { destination.close(); } catch (IOException e) { ; }
        }
    }
}
