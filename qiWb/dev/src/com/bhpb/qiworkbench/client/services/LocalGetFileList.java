/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.client.services;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.QiwIOException;

/**
 * Get a list of files in a directory.
 */
public class LocalGetFileList {
    private static Logger logger = Logger.getLogger(LocalGetFileList.class.getName());

    /**
     * Get the list of files in the specified directory. Do not recurse on the
     * directory. Note: Filtering of files is done by the requester.
     *
     * @param dirPath Path of the directory containing the files
     * @return List of files in the directory. The list is empty if there are none.
     * @throws QiwIOException If the directory path does not exist or the
     * path is not a directory.
     */
    public ArrayList<String> getFileList(String dirPath) throws QiwIOException {
        File dir = new File(dirPath);

        if (!dir.exists()) throw new QiwIOException("specified directory does not exist; path="+dirPath);

        if (!dir.isDirectory()) throw new QiwIOException("specified directory is not a directory; path="+dirPath);

        ArrayList<String> fileList = new ArrayList<String>();
        File[] files = dir.listFiles();
        logger.info("LocalGetFileList - DIR: " + dir + " NFILES: " + files.length);
        for (int i=0; i<files.length; i++) {
            // ignore subdirectories
            if (!files[i].isDirectory()) fileList.add(files[i].getName());
        }

        return fileList;
    }

    /**
     * Get the list of directories and files in the specified directory. Do not recurse on the
     * directory. Note: Filtering of files is done by the requester.
     *
     * @param dirPath Path of the directory containing the files
     * @return List of lists. First list is the list of directories
     * in the directory; empty if there are none. Second list is the list
     * of files in the directory; empty if there are none.
     * @throws QiwIOException If the directory path does not exist or the
     * path is not a directory.
     */
    public ArrayList<ArrayList> getDirectoryFileList(String dirPath) throws QiwIOException {
        File dir = new File(dirPath);

        if (!dir.exists()) throw new QiwIOException("specified directory does not exist; path="+dirPath);

        if (!dir.isDirectory()) throw new QiwIOException("specified directory is not a directory; path="+dirPath);
        File [] files = dir.listFiles();

        ArrayList<ArrayList> list = new ArrayList<ArrayList>();
        ArrayList<String> dirList = new ArrayList<String>();
        ArrayList<String> fileList = new ArrayList<String>();
        if (files.length > 0) {
            int dirCount = 0;
            int fileCount = 0;
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory())
                    dirCount++;
                else fileCount++;
            }

            String[] dirNames = new String[dirCount];
            String[] fileNames = new String[fileCount];
            int f = 0, d = 0;
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory())
                    dirNames[d++] = files[i].getName();
                else fileNames[f++] = files[i].getName();

            }
            java.util.Arrays.sort(dirNames);
            java.util.Arrays.sort(fileNames);
            for(int i = 0; i < dirNames.length; i++)
                dirList.add(dirNames[i]);
            for(int i = 0; i < fileNames.length; i++)
                fileList.add(fileNames[i]);
        }

        list.add(dirList);
        list.add(fileList);

        return list;
    }
}
