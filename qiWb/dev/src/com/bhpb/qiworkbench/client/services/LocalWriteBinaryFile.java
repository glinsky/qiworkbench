/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.client.services;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Write binary data to a file.
 */
public class LocalWriteBinaryFile {
    private static Logger logger = Logger.getLogger(LocalWriteBinaryFile.class.getName());

    /**
     * Write a binary file. If the file exists, it will be overwritten, i.e., the
     * binary data to be written will not be appended.
     *
     * @param listItems
     * <ul>
     * <li>[0] Path of file to write to</li>
     * <li>[1] file's format</li>
     * <li>[2] Binary data; type indicated by file's format</li>
     * </ul>
     * @return IO status.
     * @throws QiwIOException If the file path is null or empty, or there is
     * an IO exception while writing the file.
     */
    public void writeBinaryFile(ArrayList listItems) throws QiwIOException {
        String filePath = (String)listItems.get(0);
        if (filePath == null || filePath.length() == 0) throw new QiwIOException("invalid file path="+filePath);
        
        String fileFormat = (String)listItems.get(1);

        try {
            if (fileFormat.equals(QIWConstants.JPEG_FORMAT)) {
                BufferedImage bfImage = (BufferedImage)listItems.get(2);
                File bf = new File(filePath);
                ImageIO.write(bfImage, "jpeg", bf);
            }else if(fileFormat.equals(QIWConstants.TEXT_FORMAT)) {
                String text = (String)listItems.get(2);
                BufferedWriter output = new BufferedWriter(new FileWriter(filePath));
                output.write(text,0,text.length());
                output.flush();
            }
        } catch (IOException ioe) {
            if(ioe instanceof FileNotFoundException && ioe.getMessage().contains("Permission denied"))
                throw new QiwIOException("Insufficient privilege to write.");
            else
            throw new QiwIOException("IO exception writing binary file:"+ioe.getMessage());
        }
    }
}
