/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2007-2008, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
 */
package com.bhpb.qiworkbench.client.services;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.api.DataSystemConstants;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.util.ComponentUtils;
import com.bhpb.qiworkbench.client.util.ReadDataFromSocket;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.DispatcherConnector;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.workbench.QiComponentBase;
import com.bhpb.qiworkbench.util.ExceptionMessageFormatter;

import com.bhpb.bhpsuio.BhpSuIO;
import com.bhpb.geoio.datasystems.BhpSuDataObject;
import com.bhpb.geoio.datasystems.BhpSuHorizonDataObject;
import com.bhpb.geoio.datasystems.SeismicConstants.SampleFormat;
import com.bhpb.geoio.filesystems.FileSystemConstants;
import com.bhpb.geoio.filesystems.geofileio.GeoReadData;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.metadata.ModelFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.qiworkbench.client.qiWbConfiguration;
import java.nio.ByteOrder;

/**
 * Client-side geoIO service, a component of qiWorkbench. It is started as a thread
 * by the Messenger Dispatcher and waits for IO commands. Local geoIO means the
 * files are accessible from the user's machine. This is in contrast to remote
 * geoIO which means the file is accessible from the machine running the Tomcat
 * server. By accessible we mean files on the machine or in a cross-mounted
 * file system. When Tomcat is running on the user's machine, local and remote
 * geoIO are the same.
 * <p>
 * A geoIO Service follows the Object Adaptor Pattern, i.e., it wraps an instance of
 * a geoIO class that performs the actual IO and provides methods that call methods
 * in the instance of the wrapped class.
 */
public class GeoIOService extends QiComponentBase implements IqiWorkbenchComponent, Runnable {

    private static Logger logger = Logger.getLogger(GeoIOService.class.getName());
    //The number of trace blocks to read and process at once.
    private static final int TRACE_BLOCKS_PER_READ = 1000;

//TODO: Get the size of the trace header from the BHP-SU metadata.
    /** Size of a trace header block in bytes. */
    public static final int TRACE_HEADER_SIZE = 240;
    private static final int BUF_SIZE = 64 * 1024;
    /** Messaging manager for service */
    private MessagingManager messagingMgr;
    /** CID of this client-side service instance. Generated before the thread is started and carried as the thread's name. */
    private String myCID = "";

    public String getCID() {
        return myCID;
    }
    /** CID of paired server-side geoIO service instance. */
    private String remoteServiceCID = "";
    /** CID of Servlet Dispatcher */
    String servletDispCID = "";

    /**
     * Initialize the service component:
     * <ul>
     * <li>Create its messaging manager</li>
     * <li>Register with the Message Dispatcher</li>
     * </ul>
     */
    public void init() {
        try {
            QIWConstants.SYNC_LOCK.lock();

            messagingMgr = new MessagingManager();

            myCID = Thread.currentThread().getName();

            //register self with the Message Dispatcher
            messagingMgr.registerComponent(QIWConstants.LOCAL_SERVICE_COMP, QIWConstants.LOCAL_IO_SERVICE_NAME, myCID);

            //get Servlet Dispatcher's descriptor
            servletDispCID = MessageDispatcher.getInstance().getRuntimeServletDispDesc().getCID();

            //notify the startup method initialization finished
            QIWConstants.SYNC_LOCK.unlock();
            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in IOService.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

    /** Initialize the service, then process received IO requests.
     */
    public void run() {
        // initialize the IO service
        init();

        // Process geoIO requests made on behalf of the Message Dispatcher
        // for another component. The dispatcher will route the response back
        // to the component. The dispatcher must be involved for it pools the
        // geoIO services.
        while (true) {
            IQiWorkbenchMsg msg = messagingMgr.getNextMsgWait();
            try {
                processMsg(msg);
            } catch (Exception e) {
                logger.warning("Caught: " + e.getMessage());
            }
        }
    }

    /**
     * Process the request/response message
     *
     * @param msg The message to be processed.
     */
    public void processMsg(IQiWorkbenchMsg msg) {
        /** Request that matches the response */
        IQiWorkbenchMsg request = null, response = null;
        String errorMsg = "";

        //log message traffic
        logger.fine("client IOService::processMsg: msg=" + msg.toString());
        // Check if a response. If so, process it based on the matching request
        // and consume the response
        if (messagingMgr.isResponseMsg(msg)) {
            request = messagingMgr.checkForMatchingRequest(msg);

            // check if from the message dispatcher
            if (messagingMgr.isResponseFromMsgDispatcher(msg)) {
                String cmd = request.getCommand();

                if (cmd.equals(QIWConstants.NULL_CMD)) {
                    return;
                }
            }
        } else if (messagingMgr.isRequestMsg(msg)) { //Check if a request. If so, process and send back a response

            GeoReadData geoRecords = null; //moved declaration here so that it can be used to relay error condition to caller if instantiated

            try {
                String cmd = msg.getCommand();

                if (cmd.equals(QIWConstants.READ_GEOFILE_CMD)) {
                    ArrayList params = (ArrayList) msg.getContent();
                    //extract command parameters
                    this.ioPref = (String) params.get(0);
                    this.geoFormat = (String) params.get(1);
                    this.geoDataType = (String) params.get(3);

                    this.readService = true;

                    if (geoFormat.equals(FileSystemConstants.BHP_SU_FORMAT) &&
                            geoDataType.equals(FileSystemConstants.FILE_METADATA_DATA)) {
                        this.geoFile = (String) params.get(2);

                        BhpSuIO bhpsuIO = new BhpSuIO();
                        ArrayList args = new ArrayList();
                        args.add(ioPref);
                        args.add(geoFile);
                        //Note: BhpSuIO will handle the remote case too
                        GeoFileMetadata metadata = bhpsuIO.getFileMetadata(args, messagingMgr);
                        if (metadata != null && metadata.isValidMetadata()) {
                            args.clear();
                            args.add(geoFormat);
                            args.add(geoDataType);
                            args.add(metadata);
                            messagingMgr.sendResponse(msg, QIWConstants.ARRAYLIST_TYPE, args);
                        } else if (metadata == null) {
                            //send an abnormal response back
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Error: Could not get the metadata for unknown reason.");
                            messagingMgr.routeMsg(response);
                            return;    
                        } else { // non-null inValidMetada
                            //send an abnormal response back
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Error: Could not get the metadata for the file. CAUSE: " + metadata.getErrMsg());
                            messagingMgr.routeMsg(response);
                            return;
                        }
                    } else if (geoFormat.equals(FileSystemConstants.BHP_SU_FORMAT) &&
                            geoDataType.equals(FileSystemConstants.TRACE_SUMMARY_DATA)) {
                        this.properties = (DatasetProperties) params.get(2);

                        BhpSuIO bhpsuIO = new BhpSuIO();
                        ArrayList args = new ArrayList();
                        args.add(ioPref);
                        args.add(properties);
                        //Note: BhpSuIO will handle the remote case too
                        GeoFileDataSummary summary = bhpsuIO.getDataSummary(args, messagingMgr);
                        if (summary != null) {
                            args.clear();
                            args.add(geoFormat);
                            args.add(geoDataType);
                            args.add(summary);
                            messagingMgr.sendResponse(msg, QIWConstants.ARRAYLIST_TYPE, args);
                        } else {
                            //send an abnormal response back
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Error: Could not get the trace summary for the file " + geoFile);
                            messagingMgr.routeMsg(response);
                            return;
                        }
                    } else if (geoFormat.equals(FileSystemConstants.BHP_SU_FORMAT) &&
                            geoDataType.equals(FileSystemConstants.TRACE_DATA)) {
                        //set up socket IO for reading the geoData
                        //Note: geoIO socket IO is always performed by the runtime Tomcat server
                        //server regardless if it is remote or local.Socket IO is just as efficient
                        //for both cases.
                        request = new QiWorkbenchMsg(myCID, servletDispCID, QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent());

                        response = DispatcherConnector.getInstance().sendRequestMsg(request);

                        if (!response.isAbnormalStatus()) {
                            //Check if the response contains the server's assigned port.
                            //Note: The server's URL is obtained from the preferences
                            if (response.getMsgKind().equals(QIWConstants.DATA_CMD_MSG)) {
                                //Container for holding geoData records as they are read
                                geoRecords = new GeoReadData();

                                ArrayList<String> content = (ArrayList<String>) response.getContent();
                                int port = Integer.parseInt(content.get(1));
                                //Ignore the server URL in the response. Use that in the user's preferences.
                                String serverURL = MessageDispatcher.getInstance().getTomcatURL();

                                //Send a GeoReadData object back to the requester that will
                                //be filled with DataObjects by this service as it receives
                                //and processes the binary byte data from the server.

                                ArrayList args = new ArrayList();
                                args.add(geoFormat);
                                args.add(geoDataType);
                                args.add(geoRecords);
                                messagingMgr.sendResponse(msg, QIWConstants.ARRAYLIST_TYPE, args);

                                //handle the receiving end of the socket channel
                                //Give server time to set up the port
                                //TODO: is this necessary?
                                try {
                                    Thread.sleep(300);
                                } catch (InterruptedException ie) {
                                    logger.warning("While waiting 300ms in geoIOService for server to set up port, caught: " + ie.getMessage());
                                }

                                int start = Integer.parseInt((String) params.get(4));
                                int num = Integer.parseInt((String) params.get(5)) - start;
                                this.properties = (DatasetProperties) params.get(2);
                                GeoFileDataSummary summary = properties.getSummary();
                                //TODO: assumes there are a fixed number of samples per trace record
                                int len = summary.getSampleFormatLength();

                                int sampleSize = summary.getSamplesPerTrace() * len;
                                int traceBlockSize = TRACE_HEADER_SIZE + sampleSize;

                                logger.info("geoIOService:ReadDataFromSocket port=" + port + ", serverURL=" + serverURL);
                                ReadDataFromSocket reader = new ReadDataFromSocket(serverURL, port);

                                //set up the socket channel for reading a set of trace blocks at a time
                                String status = reader.setupSocketChannel();
                                if (!status.equals("")) {
                                    //send an abnormal response back
                                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Error: Could not set up socket channel: " + status);
                                    messagingMgr.routeMsg(response);
                                    return;
                                }

                                byte[] bytes = new byte[BUF_SIZE];
                                ByteBuffer buf = ByteBuffer.wrap(bytes);
                                buf.clear();
                                //allocate space to hold a fixed number of trace blocks
                                byte[] data = new byte[traceBlockSize * TRACE_BLOCKS_PER_READ];

                                int blocksRead = 0;
                                int blocksUnread = num;
                                while (blocksRead < num) {
                                    //Read a buffer at a time from the socket
                                    //until the set of trace blocks is read
                                    int insertOffset = 0;
                                    int bytesRead = reader.read(buf);
                                    while (bytesRead != -1) {
                                        //append bytes read to the data
                                        try {
                                            buf.flip();
                                            buf.get(data, insertOffset, bytesRead);
                                            buf.clear();
                                        } catch (BufferUnderflowException bue) {
                                            //Release the socket channel for reuse
                                            reader.teardownSocketChannel();
                                            //send an abnormal response back
                                            errorMsg = "Tried to get " + bytesRead + " from ByteBuffer but got: " + bue.getMessage();
                                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                                            messagingMgr.routeMsg(response);
                                            return;
                                        }

                                        insertOffset += bytesRead;

                                        if (insertOffset > data.length) {
                                            //Release the socket channel for reuse
                                            reader.teardownSocketChannel();
                                            throw new RuntimeException("Buffer overflow of 1000-trace data buffer after reading " + insertOffset + " bytes");
                                        } else if (insertOffset == data.length) {
                                            break; // parse this block of up to 1000 traces and continue reading from socket if necessary
                                        }
                                        //If appending from the standard 64k buffer
                                        //after the next read would overflow the 1000-trace
                                        //data array, create a smaller buffer.
                                        if (insertOffset + BUF_SIZE >= data.length) {
                                            bytes = new byte[data.length - insertOffset];
                                            buf = ByteBuffer.wrap(bytes);
                                            buf.clear();
                                        }
                                        //read the next buffer
                                        bytesRead = reader.read(buf);
                                    }

                                    //Process trace blocks read, i.e., Wrap each
                                    //trace block in a data object and add to
                                    //the result set
                                    int blocksToProcess = (blocksUnread < TRACE_BLOCKS_PER_READ) ? blocksUnread : TRACE_BLOCKS_PER_READ;
                                    processTraceBlocks(data, traceBlockSize, blocksToProcess, geoRecords);

                                    //Get the next set of trace blocks
                                    blocksRead += blocksToProcess;
                                    blocksUnread -= blocksToProcess;
                                }

                                //Finished reading traces. Set indicator in result set.
                                logger.info("Finished adding all seismic BhpSuDataObjects to geoRecords.");
                                geoRecords.setOpFinished(true);
                                geoRecords.setStatus(FileSystemConstants.GeoIoOpStatus.SC_OK);

                                //Release the socket channel for reuse
                                reader.teardownSocketChannel();
                                //Send an acknowledge response back indicating data
                                //successfully transmitted via socket.
                                /* Setting indicator in result set suffices. */
                                return;
                            } else {
                                response.setConsumerCID(msg.getProducerCID());
                                response.setProducerCID(myCID);
                                response.setMsgID(msg.getMsgID());
                            }
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to establish socket IO with server. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not read geoFile data
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }

                        messagingMgr.routeMsg(response);
                        return;
                    } else if (geoFormat.equals(FileSystemConstants.BHP_SU_FORMAT) &&
                            geoDataType.equals(FileSystemConstants.HORIZON_DATA)) {
                        //set up socket IO for reading the geoData
                        //Note: geoIO socket IO is always performed by the runtime Tomcat server
                        //server regardless if it is remote or local.Socket IO is just as efficient
                        //for both cases.
                        request = new QiWorkbenchMsg(myCID, servletDispCID, QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent());

                        response = DispatcherConnector.getInstance().sendRequestMsg(request);

                        if (!response.isAbnormalStatus()) {
                            //Check if the response contains the server's assigned port.
                            //Note: The server's URL is obtained from the preferences
                            if (response.getMsgKind().equals(QIWConstants.DATA_CMD_MSG)) {
                                //Container for holding geoData horizon record when it is read
                                geoRecords = new GeoReadData();

                                ArrayList<String> content = (ArrayList<String>) response.getContent();
                                int port = Integer.parseInt(content.get(1));
                                //Ignore the server URL in the response. Use that in the user's preferences.
                                String serverURL = MessageDispatcher.getInstance().getTomcatURL();

                                //Send a GeoReadData object back to the requester that will
                                //be filled with the horizon DataObject by this service as it receives
                                //and processes the binary byte data from the server.

                                ArrayList args = new ArrayList();
                                args.add(geoFormat);
                                args.add(geoDataType);
                                args.add(geoRecords);
                                messagingMgr.sendResponse(msg, QIWConstants.ARRAYLIST_TYPE, args);

                                //handle the receiving end of the socket channel
                                //Give server time to set up the port
                                //TODO: is this necessary?
                                try {
                                    Thread.sleep(300);
                                } catch (InterruptedException ie) {
                                    logger.warning("While waiting 300ms in geoIOService for server to set up port, caught: " + ie.getMessage());
                                }

                                this.properties = (DatasetProperties) params.get(2);
                                GeoFileDataSummary summary = properties.getSummary();
                                GeoFileMetadata metadata = this.properties.getMetadata();

                                logger.info("geoIOService:ReadDataFromSocket port=" + port + ", serverURL=" + serverURL);
                                ReadDataFromSocket reader = new ReadDataFromSocket(serverURL, port);

                                //set up the socket channel for reading a buffer at a time
                                String status = reader.setupSocketChannel();
                                if (!status.equals("")) {
                                    //send an abnormal response back
                                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Error: Could not set up socket channel: " + status);
                                    messagingMgr.routeMsg(response);
                                    return;
                                }

                                //Read horizon data the same regardless if it is in XSec of Map view
                                int start = Integer.parseInt((String) params.get(5));
                                int len = summary.getSampleFormatLength();

                                int sampleSize = summary.getSamplesPerTrace() * len;
                                int traceBlockSize = TRACE_HEADER_SIZE + sampleSize;
                                //Bugfix - get number of traces from summary.
                                int numTraces = summary.getNumberOfTraces() - start;
                                int length = traceBlockSize * numTraces;

                                byte[] bytes = new byte[BUF_SIZE];
                                ByteBuffer buf = ByteBuffer.wrap(bytes);
                                buf.clear();
                                //allocate space to hold a fixed number of trace blocks
                                byte[] data = new byte[traceBlockSize * TRACE_BLOCKS_PER_READ];

                                int blocksRead = 0;
                                int blocksUnread = numTraces;
                                while (blocksRead < numTraces) {
                                    //Read a buffer at a time from the socket
                                    //until the set of trace blocks is read
                                    int insertOffset = 0;
                                    int bytesRead = reader.read(buf);
                                    while (bytesRead != -1) {
                                        //append bytes read to the data
                                        try {
                                            buf.flip();
                                            buf.get(data, insertOffset, bytesRead);
                                            buf.clear();
                                        } catch (BufferUnderflowException bue) {
                                            //Release the socket channel for reuse
                                            reader.teardownSocketChannel();
                                            //send an abnormal response back
                                            errorMsg = "Tried to get " + bytesRead + " from ByteBuffer but got: " + bue.getMessage();
                                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                                            messagingMgr.routeMsg(response);
                                            return;
                                        }
                                        insertOffset += bytesRead;

                                        if (insertOffset > data.length) {
                                            //Release the socket channel for reuse
                                            reader.teardownSocketChannel();
                                            throw new RuntimeException("Buffer overflow of 1000-trace data buffer after reading " + insertOffset + " bytes");
                                        } else if (insertOffset == data.length) {
                                            break; // parse this block of up to 1000 traces and continue reading from socket if necessary
                                        }
                                        //If appending from the standard 64k buffer
                                        //after the next read would overflow the 1000-trace
                                        //data array, create a smaller buffer.
                                        if (insertOffset + BUF_SIZE >= data.length) {
                                            bytes = new byte[data.length - insertOffset];
                                            buf = ByteBuffer.wrap(bytes);
                                            buf.clear();
                                        }
                                        //read the next buffer
                                        bytesRead = reader.read(buf);
                                    }

                                    //Process trace blocks read, i.e., Wrap each
                                    //trace block in a data object and add to
                                    //the result set
                                    int blocksToProcess = (blocksUnread < TRACE_BLOCKS_PER_READ) ? blocksUnread : TRACE_BLOCKS_PER_READ;
                                    processHorizonTraceBlocks(data, traceBlockSize, blocksToProcess, geoRecords, (String)params.get(4));

                                    //Get the next set of trace blocks
                                    blocksRead += blocksToProcess;
                                    blocksUnread -= blocksToProcess;
                                }

                                //Finished reading traces. Set indicator in result set.
                                logger.info("Finished adding all horizon BhpSuDataObjects to geoRecords.");
                                geoRecords.setOpFinished(true);
                                geoRecords.setStatus(FileSystemConstants.GeoIoOpStatus.SC_OK);
                                //end of handling a transposed horizon

                                //Release the socket channel for reuse
                                reader.teardownSocketChannel();
                                //Send an acknowledge response back indicating data
                                //successfully transmitted via socket.
                                /* Setting indicator in result set suffices. */
                                return;
                            } else {
                                response.setConsumerCID(msg.getProducerCID());
                                response.setProducerCID(myCID);
                                response.setMsgID(msg.getMsgID());
                            }
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to establish socket IO with server. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not read geoFile data
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }

                        messagingMgr.routeMsg(response);
                        return;
                    } else if (geoFormat.equals(FileSystemConstants.BHP_SU_FORMAT) &&
                            geoDataType.equals(FileSystemConstants.MODEL_DATA)) {
                        //set up socket IO for reading the geoData
                        //Note: geoIO socket IO is always performed by the runtime Tomcat server
                        //server regardless if it is remote or local.Socket IO is just as efficient
                        //for both cases.
                        request = new QiWorkbenchMsg(myCID, servletDispCID, QIWConstants.CMD_MSG, msg.getCommand(), MsgUtils.genMsgID(), msg.getContentType(), msg.getContent());

                        response = DispatcherConnector.getInstance().sendRequestMsg(request);

                        if (!response.isAbnormalStatus()) {
                            //Check if the response contains the server's assigned port.
                            //Note: The server's URL is obtained from the preferences
                            if (response.getMsgKind().equals(QIWConstants.DATA_CMD_MSG)) {
                                //Container for holding geoData records as they are read
                                geoRecords = new GeoReadData();

                                ArrayList<String> content = (ArrayList<String>) response.getContent();
                                int port = Integer.parseInt(content.get(1));
                                //Ignore the server URL in the response. Use that in the user's preferences.
                                String serverURL = MessageDispatcher.getInstance().getTomcatURL();

                                //Send a GeoReadData object back to the requester that will
                                //be filled with DataObjects by this service as it receives
                                //and processes the binary byte data from the server.

                                ArrayList args = new ArrayList();
                                args.add(geoFormat);
                                args.add(geoDataType);
                                args.add(geoRecords);
                                messagingMgr.sendResponse(msg, QIWConstants.ARRAYLIST_TYPE, args);

                                //handle the receiving end of the socket channel
                                //Give server time to set up the port
                                //TODO: is this necessary?
                                try {
                                    Thread.sleep(300);
                                } catch (InterruptedException ie) {
                                    logger.warning("While waiting 300ms in geoIOService for server to set up port, caught: " + ie.getMessage());
                                }

                                int start = Integer.parseInt((String) params.get(4));
                                ArrayList<String> modelProps = (ArrayList<String>) params.get(6);
                                this.properties = (DatasetProperties) params.get(2);
                                GeoFileDataSummary summary = properties.getSummary();
                                //Alternatively: num is (String)params.get(5)
                                int num = summary.getNumberOfTraces() - start;
                                ModelFileMetadata metadata = (ModelFileMetadata) this.properties.getMetadata();

                                int numLayers = metadata.getNumLayers();
                                int numModelProps = modelProps.size();
                                if (numModelProps == 0) {
                                    numModelProps = metadata.getNumProps();
                                //A model trace contains for each model property numLayers
                                //values + the tops for each layer + bottom of last layer
                                }
                                int samplesPerTrace = summary.getSamplesPerTrace();
                                int len = summary.getSampleFormatLength();
                                int sampleSize = samplesPerTrace * len;
                                int traceBlockSize = TRACE_HEADER_SIZE + sampleSize;

                                logger.info("geoIOService:ReadDataFromSocket port=" + port + ", serverURL=" + serverURL);
                                ReadDataFromSocket reader = new ReadDataFromSocket(serverURL, port);

                                //set up the socket channel for reading a buffer at a time
                                String status = reader.setupSocketChannel();
                                if (!status.equals("")) {
                                    //send an abnormal response back
                                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Error: Could not set up socket channel: " + status);
                                    messagingMgr.routeMsg(response);
                                    return;
                                }

                                byte[] bytes = new byte[BUF_SIZE];
                                ByteBuffer buf = ByteBuffer.wrap(bytes);
                                buf.clear();
                                //allocate space to hold a fixed number of trace blocks
                                byte[] data = new byte[traceBlockSize * TRACE_BLOCKS_PER_READ];

                                int blocksRead = 0;
                                int blocksUnread = num;
                                while (blocksRead < num) {
                                    //Read a buffer at a time from the socket
                                    //until the set of trace blocks is read
                                    int insertOffset = 0;
                                    int bytesRead = reader.read(buf);
                                    while (bytesRead != -1) {
                                        //append bytes read to the data
                                        try {
                                            buf.flip();
                                            buf.get(data, insertOffset, bytesRead);
                                            buf.clear();
                                        } catch (BufferUnderflowException bue) {
                                            //Release the socket channel for reuse
                                            reader.teardownSocketChannel();
                                            //send an abnormal response back
                                            errorMsg = "Tried to get " + bytesRead + " from ByteBuffer but got: " + bue.getMessage();
                                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                                            messagingMgr.routeMsg(response);
                                            return;
                                        }
                                        insertOffset += bytesRead;

                                        if (insertOffset > data.length) {
                                            //Release the socket channel for reuse
                                            reader.teardownSocketChannel();
                                            throw new RuntimeException("Buffer overflow of 1000-trace data buffer after reading " + insertOffset + " bytes");
                                        } else if (insertOffset == data.length) {
                                            break; // parse this block of up to 1000 traces and continue reading from socket if necessary
                                        }
                                        //If appending from the standard 64k buffer
                                        //after the next read would overflow the 1000-trace
                                        //data array, create a smaller buffer.
                                        if (insertOffset + BUF_SIZE >= data.length) {
                                            bytes = new byte[data.length - insertOffset];
                                            buf = ByteBuffer.wrap(bytes);
                                            buf.clear();
                                        }
                                        //read the next buffer
                                        bytesRead = reader.read(buf);
                                    }

                                    //Process trace blocks read, i.e., Wrap each
                                    //trace block in a data object and add to
                                    //the result set
                                    int blocksToProcess = (blocksUnread < TRACE_BLOCKS_PER_READ) ? blocksUnread : TRACE_BLOCKS_PER_READ;
                                    processTraceBlocks(data, traceBlockSize, blocksToProcess, geoRecords);

                                    //Get the next set of trace blocks
                                    blocksRead += blocksToProcess;
                                    blocksUnread -= blocksToProcess;
                                }

                                //Finished reading model traces. Set indicator in result set.
                                logger.info("Finished adding all model BhpSuDataObjects to geoRecords.");
                                geoRecords.setOpFinished(true);
                                geoRecords.setStatus(FileSystemConstants.GeoIoOpStatus.SC_OK);

                                //Release the socket channel for reuse
                                reader.teardownSocketChannel();
                                //Send an acknowledge response back indicating data
                                //successfully transmitted via socket.
                                /* Setting indicator in result set suffices. */
                                return;
                            } else {
                                response.setConsumerCID(msg.getProducerCID());
                                response.setProducerCID(myCID);
                                response.setMsgID(msg.getMsgID());
                            }
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to establish socket IO with server. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not read geoFile data
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }

                        messagingMgr.routeMsg(response);
                        return;
                    }
                } else if (cmd.equals(QIWConstants.WRITE_GEOFILE_CMD)) {
                    ArrayList params = (ArrayList) msg.getContent();

                    //WHF - This is a fix for NPE when the caller gives an insufficient number of parameters
                    if (params.size() < 3) {
                        //send an abnormal response back
                        errorMsg = "Unable to process command: " + QIWConstants.WRITE_GEOFILE_CMD + " because the request contains an insufficient number of parameters (3 are required)";
                        response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        messagingMgr.routeMsg(response);
                        return;
                    }

                    this.ioPref = (String) params.get(0);
                    this.geoFormat = (String) params.get(1);
                    this.geoFile = (String) params.get(2);
                    this.geoDataType = (String) params.get(3);
                    //get(4), the specification of the data records to be written

                    this.readService = false;

                } else if (cmd.equals(QIWConstants.RELEASE_GEOIO_SERVICE_CMD)) {
                    //if remote geoIO, release the paired server-side geoIO Service
                    if (!this.ioPref.equals(QIWConstants.LOCAL_SERVICE_PREF)) {
                        IQiWorkbenchMsg req = new QiWorkbenchMsg(myCID, servletDispCID, QIWConstants.CMD_MSG, QIWConstants.RELEASE_REMOTE_GEOIO_SERVICE_CMD, MsgUtils.genMsgID(), QIWConstants.STRING_TYPE, remoteServiceCID, msg.skip());
                        response = DispatcherConnector.getInstance().sendRequestMsg(req);
                        if (!response.isAbnormalStatus()) {
                            //Send a normal response back
                            messagingMgr.sendResponse(msg, "", "");
                        } else {
                            String cause = (String) response.getContent();
                            errorMsg = "Severe: Unable to release remote geoIO service. CAUSE:" + cause;
                            logger.severe(errorMsg);
                            //notify requester could not get geoIO Service
                            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                        }
                    }

                    //reset internal state for next usage
                    resetState();

                    messagingMgr.routeMsg(response);
                    return;
                } else {
                    errorMsg = "Unable to process geoIO message '" + msg.toString() + "' because the command " + cmd + " is not recognized.";
                    logger.warning(errorMsg);
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                    messagingMgr.routeMsg(response);
                }
            } catch (NullPointerException npe) {
                errorMsg = "Unable to process geoIO request '" + msg.getCommand() + "' because of " + ExceptionMessageFormatter.getFormattedMessage(npe);
                logger.warning(errorMsg);
                response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                messagingMgr.routeMsg(response);
            } catch (Exception ex) {
                errorMsg = "Unable to process geoIO request '" + msg.getCommand() + "' because of " + ExceptionMessageFormatter.getFormattedMessage(ex);
                logger.warning(errorMsg);
                if (geoRecords != null) {
                    geoRecords.setErrorMsg(errorMsg);
                    geoRecords.setStatus(FileSystemConstants.GeoIoOpStatus.SC_ERROR);
                    geoRecords.setOpFinished(true);
                } else {
                    logger.warning("Sending synchronous message response back to GeoIOService invoker, if skip == true on the message, this may block the caller's message queue indefinitely!");
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
                    messagingMgr.routeMsg(response);
                }
            }
        } else {
            errorMsg = "Unable to process geoIO message '" + msg.toString() + "' because it is neither a request nor response.";
            logger.warning(errorMsg);
            response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, errorMsg);
            messagingMgr.routeMsg(response);
        }
    }

    /**
     * Process a set of seismic/model traces block in a buffer, namely, wrap each trace block
     * in a data object and add the data object to the result set.
     * @param data Buffer containing trace blocks
     * @param traceBlockSize Size of a trace block in bytes
     * @param numBlocks Number of trace blocks in the buffer
     * @param geoRecords Results set to append trace blocks to
     */
    private void processTraceBlocks(byte[] data, int traceBlockSize, int numBlocks, GeoReadData geoRecords) {
        GeoFileMetadata metadata = this.properties.getMetadata();
        //DataSystemConstants.Endian endianess = metadata.getEndianFormat();
        String filePath = metadata.getGeoFilePath();
        GeoFileDataSummary summary = properties.getSummary();
        SampleFormat sampleFormat = summary.getSampleFormat();
        //Note: BHP-SU data is in IEEE format, BIG_ENDIAN
        DataSystemConstants.FloatFormat floatFormat = DataSystemConstants.FloatFormat.IEEE_FORMAT;

        //Wrap each trace block in a data object and add to the result set
        int traceOffset = 0;

        Runtime rt = Runtime.getRuntime();
        for (int i = 0; i < numBlocks; i++) {
            byte[] traceBlock;
            traceBlock = new byte[traceBlockSize];

            System.arraycopy(data, traceOffset, traceBlock, 0, traceBlockSize);

            //geoIO DataObjects are _always_ in server's byteOrder
            ByteOrder serverByteOrder =
                    qiWbConfiguration.getInstance().getServerByteOrder();
            BhpSuDataObject bhpsuDO = new BhpSuDataObject(serverByteOrder, floatFormat, DataSystemConstants.RecordType.BINARY_BLOCK, DataSystemConstants.FieldKind.VECTOR, filePath + ":" + i);

            bhpsuDO.setDataRecord(traceBlock);
            bhpsuDO.setHeaderSize(TRACE_HEADER_SIZE);
            //add data object to the result set
            geoRecords.addRecord(bhpsuDO);

            traceOffset += traceBlockSize;
        }
    }
    
    /**
     * Process a set of horizon trace blocks in a buffer, namely, wrap each trace block
     * in a horizon data object and add the data object to the result set.
     * @param data Buffer containing trace blocks
     * @param traceBlockSize Size of a trace block in bytes
     * @param numBlocks Number of trace blocks in the buffer
     * @param geoRecords Results set to append trace blocks to
     * @param horizonName Name of the horizon
     */
    private void processHorizonTraceBlocks(byte[] data, int traceBlockSize, int numBlocks, GeoReadData geoRecords, String horizonName) {
        GeoFileMetadata metadata = this.properties.getMetadata();
        //DataSystemConstants.Endian endianess = metadata.getEndianFormat();
        String filePath = metadata.getGeoFilePath();
        GeoFileDataSummary summary = properties.getSummary();
        SampleFormat sampleFormat = summary.getSampleFormat();
        //Note: BHP-SU data is in IEEE format, BIG_ENDIAN
        DataSystemConstants.FloatFormat floatFormat = DataSystemConstants.FloatFormat.IEEE_FORMAT;

        //Wrap each trace block in a data object and add to the result set
        int traceOffset = 0;

        Runtime rt = Runtime.getRuntime();
        for (int i = 0; i < numBlocks; i++) {
            byte[] traceBlock;
            traceBlock = new byte[traceBlockSize];

            System.arraycopy(data, traceOffset, traceBlock, 0, traceBlockSize);

            //geoIO DataObjects are _always_ in server's byteOrder
            ByteOrder serverByteOrder =
                    qiWbConfiguration.getInstance().getServerByteOrder();
            BhpSuHorizonDataObject bhpsuDO = new BhpSuHorizonDataObject(serverByteOrder, floatFormat, DataSystemConstants.RecordType.BINARY_BLOCK, DataSystemConstants.FieldKind.VECTOR, filePath + ":" + i);
            
            bhpsuDO.setHorizonName(horizonName);
            bhpsuDO.setDataRecord(traceBlock);
            bhpsuDO.setHeaderSize(TRACE_HEADER_SIZE);
            //add data object to the result set
            geoRecords.addRecord(bhpsuDO);

            traceOffset += traceBlockSize;
        }
    }
    
    /** Format of the geoFile */
    private String geoFormat = "";
    /** Full pathname of the geoFile */
    private String geoFile = "";
    /** Type of geoData */
    private String geoDataType = "";
    /** IO preference, i.e., local or remote. Default is local. */
    private String ioPref = QIWConstants.LOCAL_SERVICE_PREF;
    /** Properties of the geoFile; includes it metadata */
    private DatasetProperties properties = null;
    /** true if this geoIO Service is a read service; otherwise, false. */
    private boolean readService;

    //GETTERS
    /**
     * Check if this geoIO service is a write service
     */
    public boolean isWriteService() {
        return !readService;
    }

    /**
     * Check if this geoIO service is a read service
     */
    public boolean isReadService() {
        return readService;
    }

    //SETTERS
    public void setGeoFormat(String geoFormat) {
        this.geoFormat = geoFormat;
    }

    public void setGeoDataType(String geoDataType) {
        this.geoDataType = geoDataType;
    }

    public void setGeoFile(String pathname) {
        this.geoFile = pathname;
    }

    public void setIoPref(String ioPref) {
        this.ioPref = ioPref;
    }

    /**
     * Save the CID of the paired server-side geoIO Service.
     * @maram remoteServiceCID CID of the paired server-side geoIO Service.
     */
    public void setRemoteServiceCID(String remoteServiceCID) {
        this.remoteServiceCID = remoteServiceCID;
    }

    /**
     * Reset internal state for next usage.
     */
    private void resetState() {
        //Note: Keep CID and ServletDispatcher's CID for they won't change.
        this.geoFormat = "";
        this.geoDataType = "";
        this.geoFile = "";
        this.ioPref = QIWConstants.LOCAL_SERVICE_PREF;
        this.remoteServiceCID = "";
    }

    //GEOIO OPERATIONS
    /** server-side geoIO Actions */
    ArrayList actions = new ArrayList();
    /** action parameters */
    ArrayList actionParams = new ArrayList();
    /** geoIO action result */
    GeoReadData actionResult = null;
    /** action error message */
    String actionErrorMsg = "";

    /**
     * Seed a geoIO Read with the data records to be read.
     * @param recordIDs List of data record IDs
     * @return Action result whose content is the return value(s) or an error exception if the op could not be performed.
     */
    /* DEPRECATED: use method in GeoIOData
    public GeoReadData seed(ArrayList<String> recordIDs) {
    if (!isReadService()) {
    actionResult = new GeoReadData();
    actionErrorMsg = "Warning: Invalid geoIO write action. CAUSE: Cannot seed a geoIO write operation";
    actionResult.setAbnormResult(actionErrorMsg);
    } else

    if (this.ioPref.equals(QIWConstants.LOCAL_SERVICE_PREF)) {
    //perform action locally
    actionResult = ((ReadGeoFile)geoFileIO).seed(recordIDs);
    } else { //perform action(s) remotely
    actions.clear(); actionParams.clear();

    //specify the CID of the server-side geoIO Service
    actions.add(remoteServiceCID);

    //set action
    actions.add("action="+ControlConstants.SEED_ACTION);

    //set action parameters
    actionParams.add(recordIDs);
    actions.add(actionParams);

    actionResult = performGeoIOActions(actions);
    }

    //Note: This is a direct call; no sending the response using message passing.
    return actionResult;
    }
     */
    /**
     * Check if there are more data records to read or if all the data records
     * have been written.
     * @return Action result containing "true" if there are no more data records
     * to read or write; otherwise "false". Or, an error exception if unable to
     * make the check.
     */
    /* DEPRECATED: use method in GeoIOData
    public GeoReadData hasMoreRecords() {
    if (isReadService()) {
    if (this.ioPref.equals(QIWConstants.LOCAL_SERVICE_PREF)) {
    //perform action locally
    actionResult = ((ReadGeoFile)geoFileIO).hasMoreRecords();
    } else { //perform action(s) remotely
    actions.clear(); actionParams.clear();

    //specify the CID of the server-side geoIO Service
    actions.add(remoteServiceCID);

    //set action
    actions.add("action="+ControlConstants.MORE_RECORDS_ACTION);

    actionResult = performGeoIOActions(actions);
    }
    } else {
    if (this.ioPref.equals(QIWConstants.LOCAL_SERVICE_PREF)) {
    //perform action locally
    actionResult = ((WriteGeoFile)geoFileIO).hasMoreRecords();
    } else { //perform action(s) remotely
    actions.clear(); actionParams.clear();

    //specify the CID of the server-side geoIO Service
    actions.add(remoteServiceCID);

    //set action
    actions.add("action="+ControlConstants.MORE_RECORDS_ACTION);

    actionResult = performGeoIOActions(actions);
    }
    }

    return actionResult;
    }
     */
    /**
     * Read the next data record.
     * @return Action result containing the next data record to be read.
     * An error message if all data records have been read or there is an IO error
     * reading the data record or this is a write service.
     */
    /* DEPRECATED: use method in GeoIOData
    public GeoReadData nextRecord() {
    if (isReadService()) {
    if (this.ioPref.equals(QIWConstants.LOCAL_SERVICE_PREF)) {
    //perform action locally
    actionResult = ((ReadGeoFile)geoFileIO).nextRecord();
    } else { //perform action(s) remotely
    actions.clear(); actionParams.clear();

    //specify the CID of the server-side geoIO Service
    actions.add(remoteServiceCID);

    //set action
    actions.add("action="+ControlConstants.NEXT_RECORD_ACTION);

    actionResult = performGeoIOActions(actions);
    }
    } else {
    actionResult = new GeoReadData();
    actionErrorMsg = "Warning: Invalid geoIO write action. CAUSE: Cannot get the nextRecord to write.";
    actionResult.setAbnormResult(actionErrorMsg);
    }

    return actionResult;
    }
     */
    /**
     * Have the server-side geoIO Service perform the geoIO action(s)
     * @param actions List of actions
     * @return Action result which contains an error exception if the action(s) could not be performed.
     */
    /* DEPRECATED:
    private GeoReadData performGeoIOActions(ArrayList actions) {
    IQiWorkbenchMsg req = new IQiWorkbenchMsg(myCID, servletDispCID, QIWConstants.CMD_MSG, QIWConstants.PERFORM_GEOIO_OP_CMD, MsgUtils.genMsgID(), QIWConstants.STRING_TYPE, remoteServiceCID, false);
    GeoReadData rslt = new GeoReadData();

    IQiWorkbenchMsg actionResp = DispatcherConnector.getInstance().sendRequestMsg(req);
    if (actionResp.isAbnormalStatus()) {
    String cause = (String)actionResp.getContent();
    actionErrorMsg = "Severe: geoIO service unable to perform actions. CAUSE:"+cause;
    logger.severe(actionErrorMsg);
    rslt.setAbnormResult(actionErrorMsg);
    } else {
    //extract result from the response
    rslt.setNormResult((ArrayList)actionResp.getContent());
    }

    return rslt;
    }
     */
    /** Launch the geoIO service:
     *  <ul>
     *  <li>Start up the service thread which will initialize the service.</li>
     *  </ul>
     * <p>
     * NOTE: Each thread's init() must finish before the next thread is
     * started. This is accompished by monitoring the SYNC_LOCK object.
     */
    public static void main(String[] args) {
        IOService ioServiceInstance = new IOService();
        // get a CID for the new component instance
        String cid = ComponentUtils.genCID(IOService.class);
        // use the CID as the name of the thread
        Thread serviceThread = new Thread(ioServiceInstance, cid);
        serviceThread.start();
        long threadId = serviceThread.getId();
        logger.info("Local IO Service Thread-" + Long.toString(threadId) + " started");

        // When the services's init() is finished, it will release the lock
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();
    }
}
