/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.client.services;

import java.util.ArrayList;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.JobAdapter;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.QiwJobException;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.util.ComponentUtils;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.workbench.QiComponentBase;

/**
 * Local Job service, a core component of qiWorkbench. It is started as a thread
 * by the Messenger Dispatcher and waits for job commands.
 * <p>
 * The order of job commands is:
 * <ul>
 *   <li>Issue a Submit Job or Execute Job. This will execute the job, spawn two
 *       parallel tasks to capture the job's standard out and err, and wait for
 *       the job to terminate.</li>
 *   <li>When the requester gets the response containing the job ID, issue a
 *       WaitForJobTermination even though the job has already terminated. The
 *       response will contain the terminated job's status code. Process it.</li>
 *   <li>Issue a command to get the job's standard out. Process the response.</li>
 *   <li>Issue a command to get the job's standard err. Process the response.</li>
 * </ul>
 * <p>If another command is executed before the standard out and err are
 *    requested, they will be lost.
 */
public class JobService extends QiComponentBase implements IqiWorkbenchComponent, Runnable {
    private static Logger logger = Logger.getLogger(JobService.class.getName());

    /** Messaging manager for service */
    private MessagingManager messagingMgr;

    /** CID for component instance. Generated before the thread is started and carried as the thread's name. */
    private String myCID = "";

    public String getCID() {
        return myCID;
    }

    /** Job ID associated with this job service. */
    private String jobID = "";

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }
    public String getJobID() {
        return jobID;
    }

    /** Job adapter for this job service */
    JobAdapter jobAdapter = null;

    /**
     * Initialize the service component:
     * <ul>
     * <li>Create its messaging manager</li>
     * <li>Register with the Message Dispatcher</li>
     * </ul>
     */
    public void init() {
        try {
            QIWConstants.SYNC_LOCK.lock();

            messagingMgr = new MessagingManager();

            myCID = Thread.currentThread().getName();

            // register self with the Message Dispatcher
            messagingMgr.registerComponent(QIWConstants.LOCAL_SERVICE_COMP, QIWConstants.LOCAL_IO_SERVICE_NAME, myCID);

            // create job adapter instance
            jobAdapter = new JobAdapter();

            // notify the startup method initialization finished
            QIWConstants.SYNC_LOCK.unlock();
            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in JobService.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

    /** Initialize the service, then process received job requests.
     */
    public void run() {
        // initialize the job service
        init();

        // Process job requests made on behalf of the Message Dispatcher
        // for another component. The dispatcher will route the response back
        // to the component. The dispatcher must be involved for it pools the
        // job threads.
        while(true) {
            IQiWorkbenchMsg msg = messagingMgr.getNextMsgWait();
            processMsg(msg);
        }
    }

    /**
     * For a response, find the request matching the response and process the
     * response based on the request. Process a job request.
     */
    public void processMsg(IQiWorkbenchMsg msg) {
        /** Request that matches the response */
        IQiWorkbenchMsg request = null, response = null;

        //log message traffic
        logger.fine("client JobService::procssMsg: msg="+msg.toString());

        // Check if a response. If so, process and consume response
        if (messagingMgr.isResponseMsg(msg)) {
            request = messagingMgr.checkForMatchingRequest(msg);

            // check if from the message dispatcher
            if (messagingMgr.isResponseFromMsgDispatcher(msg)) {
                String cmd = request.getCommand();

                if (cmd.equals(QIWConstants.NULL_CMD)) return;
            }
            // TODO other possible responses...
        } else

        //Check if a request. If so, process and send back a response
        if (messagingMgr.isRequestMsg(msg)) {
            String cmd = msg.getCommand();

            if (cmd.equals(QIWConstants.SUBMIT_JOB_CMD)) {
                ArrayList<String> jobCmd = (ArrayList<String>)msg.getContent();
                String doBlockParam = jobCmd.get(jobCmd.size() - 1);
                try {
                    boolean doBlock;
                    if ("true".equalsIgnoreCase(doBlockParam) || "false".equalsIgnoreCase(doBlockParam)) {
                        doBlock = Boolean.valueOf(doBlockParam);
                        jobCmd.remove(jobCmd.size() - 1);
                    } else {
                        logger.warning("The final parameter of SUBMIT_JOB_CMD must be 'true' or 'false' but was not specified, defaulting to 'true'.");
                        doBlock = true;
                    }
                    jobAdapter.executeCommand(jobCmd, doBlock);
                } catch (QiwJobException qje) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qje.getMessage());
                    messagingMgr.routeMsg(response);
                    return;
                }
                // send a normal response back to the producer of the request
                messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, jobID);
                return;
            } else

            if (cmd.equals(QIWConstants.EXECUTE_JOB_CMD)) {
                // the command must have already been set by setJobCommand
                try {
                    //This command is not used, so setting doBlock=true is harmless.
                    jobAdapter.executeCommand(true);
                } catch (QiwJobException qje) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qje.getMessage());
                    messagingMgr.routeMsg(response);
                    return;
                }
                // send a normal response back
                messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, jobID);
                return;
            } else

            if (cmd.equals(QIWConstants.RELEASE_JOB_CMD)) {
                jobAdapter.reset();
                String releasedJobID = jobID;
                jobID = "";
                ArrayList<String> params = new ArrayList<String>();
                params.add(QIWConstants.LOCAL_SERVICE_PREF);
                params.add(myCID);
                params.add(releasedJobID);

                // send a normal response back
                messagingMgr.sendResponse(msg, QIWConstants.ARRAYLIST_TYPE, params);
                return;
            } else

            if (cmd.equals(QIWConstants.WAIT_FOR_JOB_EXIT_CMD)) {
                int status = jobAdapter.waitForTermination();
                //Send a normal response back
                messagingMgr.sendResponse(msg, QIWConstants.INTEGER_TYPE, new Integer(status));
                return;
            } else

            if (cmd.equals(QIWConstants.GET_JOB_STATUS_CMD)) {
                int status = jobAdapter.jobStatus();
                //Send a normal response back
                messagingMgr.sendResponse(msg, QIWConstants.INTEGER_TYPE, new Integer(status));
                return;
            } else

            if (cmd.equals(QIWConstants.GET_JOB_OUTPUT_CMD)) {
                ArrayList<String> output;
                try {
                    output = jobAdapter.getJobOutput();
                } catch (QiwIOException qioe) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qioe.getMessage());
                    messagingMgr.routeMsg(response);
                    return;
                }
                //Send a normal response back
                messagingMgr.sendResponse(msg, QIWConstants.ARRAYLIST_TYPE, output);
                return;
            } else

            if (cmd.equals(QIWConstants.GET_JOB_ERROR_OUTPUT_CMD)) {
                ArrayList<String> errorOutput;
                try {
                    errorOutput = jobAdapter.getJobErrors();
                } catch (QiwIOException qioe) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qioe.getMessage());
                    messagingMgr.routeMsg(response);
                    return;
                }
                //Send a normal response back
                messagingMgr.sendResponse(msg, QIWConstants.ARRAYLIST_TYPE, errorOutput);
                return;
            } else

            if (cmd.equals(QIWConstants.SET_JOB_COMMAND_CMD)) {
                ArrayList<String> jobCmd = (ArrayList<String>)msg.getContent();
                //1st item is the job ID
                jobCmd.remove(0);
                try {
                    jobAdapter.resetCommand(jobCmd);
                } catch (QiwJobException qje) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qje.getMessage());
                    messagingMgr.routeMsg(response);
                    return;
                }
                //Send a normal response back
                messagingMgr.sendResponse(msg, "", "");
                return;
            } else

            if (cmd.equals(QIWConstants.SET_JOB_WORKING_DIR_CMD)) {
                ArrayList<String> items = (ArrayList<String>)msg.getContent();
                //1st item is the job ID
                String dirPath = (String)items.get(1);
                jobAdapter.setWorkingDirectory(dirPath);
                //Send a normal response back
                messagingMgr.sendResponse(msg, "", "");
                return;
            } else

            if (cmd.equals(QIWConstants.SET_JOB_ENV_VAR_CMD)) {
                ArrayList<String> item = (ArrayList<String>)msg.getContent();
                //1st item is the job ID
                String var = (String)item.get(1);
                String val = (String)item.get(2);
                try {
                    jobAdapter.addEnvVar(var, val);
                } catch (QiwJobException qje) {
                    //send an abnormal response back
                    response = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_EXCEPTION, "Warning: "+qje.getMessage());
                    messagingMgr.routeMsg(response);
                    return;
                }
                //Send a normal response back
                messagingMgr.sendResponse(msg, "", "");
                return;
            } else

            if (cmd.equals(QIWConstants.KILL_JOB_CMD)) {
                jobAdapter.killJob();

                //Send a normal response back
                messagingMgr.sendResponse(msg, "", "");
                return;
            }
        }

        logger.warning("Job message not processed:"+msg.toString());
    }
    /** Launch the job service:
     *  <ul>
     *  <li>Start up the service thread which will initialize the service.</li>
     *  </ul>
     * <p>
     * NOTE: Each thread's init() must finish before the next thread is
     * started. This is accompished by monitoring the SYNC_LOCK object.
     */
    public static void main(String[] args) {
        JobService jobServiceInstance = new JobService();
        // get a CID for the new component instance
        String cid = ComponentUtils.genCID(JobService.class);
        // use the CID as the name of the thread
        Thread serviceThread = new Thread(jobServiceInstance, cid);
        serviceThread.start();
        long threadId = serviceThread.getId();
        logger.info("Job Service Thread-"+Long.toString(threadId)+" started");
        // When the services's init() is finished, it will release the lock

        // wait until the plugin's init() has finished
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();
    }
}
