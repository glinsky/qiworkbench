/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiworkbench.client;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.bhpb.qiworkbench.updater.UpdateItem;
import com.bhpb.qiworkbench.updater.UpdateUtilities;
import com.bhpb.qiworkbench.updater.UpdateConstants;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.DispatcherConnector;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MsgUtils;

public class UpdateManager {

    private static Logger logger = Logger.getLogger(UpdateManager.class.getName());

    /**
     * Checks components for updates
     * @param allAvailComps
     * @return ArrayList:	Cell 0 - updateComps - List of components that need updates,
     * 						Cell 1 - deleteComps - List of components that have an older version needing to be deleted,
     * 						Cell 2 - versionComps - Used for GUI to display difference in version number
     */
    public static ArrayList<ArrayList<UpdateItem>> checkUpdates(ArrayList<UpdateItem> allAvailComps) {
        logger.finest("Checking for Updates");
        ArrayList<ArrayList<UpdateItem>> returnArray = new ArrayList<ArrayList<UpdateItem>>();
        ArrayList<UpdateItem> updateComps = new ArrayList<UpdateItem>();
        ArrayList<UpdateItem> clientComps = getClientComponents();
        ArrayList<UpdateItem> deleteComps = new ArrayList<UpdateItem>();
        ArrayList<UpdateItem> versionComps = new ArrayList<UpdateItem>();
        //Determines whether the current component is to be downloaded
        boolean downloadComponent;

        // Read in the qiWorkbench version number
        Properties workbenchProperties = new Properties();
        Class updateManagerClass = UpdateManager.class;
        InputStream propertyInput = updateManagerClass.getResourceAsStream("/version.properties");
        try {
            workbenchProperties.load(propertyInput);
        } catch (IOException ioe) {
            logger.finest("IOException loading version.properties");
        } finally {
            try {
                if (propertyInput != null) {
                    propertyInput.close();
                }
            } catch (IOException e) {
                logger.finest("IOException closing version.properties");
            }
        }
        String versionProp = workbenchProperties.getProperty("project.version");

        //Begin Checking available components to user components
        for (int i = 0; i < allAvailComps.size(); i++) {
            downloadComponent = true;
            try {
                //Compare Workbench version dependencies
                if (!isVersionCompatible(allAvailComps.get(i).getManifestAttributes().get("QiWorkbench-Component-Workbench-Version-Dependencies"), versionProp)) {
                    logger.info(allAvailComps.get(i).getManifestAttributes().get("QiWorkbench-Component-Name") + " version incompatable with current qiWorkbench version");
                    continue;
                } //Compare Java version dependencies
                else if (System.getProperty("java.version").compareTo(allAvailComps.get(i).getManifestAttributes().get("QiWorkbench-Component-Java-Dependencies")) < 0) {
                    logger.info(allAvailComps.get(i).getManifestAttributes().get("QiWorkbench-Component-Name") + " Java version incompatable with currently installed Java version");
                    continue;
                }
                //Compare available components to current user components
                for (int j = 0; j < clientComps.size(); j++) {
                    //If the client and server update descriptors are identical...
                    if ((allAvailComps.get(i)).equals(clientComps.get(j))) {
                        //Then the binary bundle is downloaded only if dependencies are
                        //missing on the client.
                        if (isMissingDependency(clientComps.get(j))) {
                            logger.info(allAvailComps.get(i).getManifestAttributes().get("QiWorkbench-Component-Name") 
                                    + " is up to date but one or more JAR dependencies is missing.  Updating binary bundle.");
                            break;
                        }
                        //Otherwise, a message is logged and the binary bundle will not be downloaded.
                        else {
                            logger.info(allAvailComps.get(i).getManifestAttributes().get("QiWorkbench-Component-Name")
                                    +" the same, no update needed");
                            clientComps.remove(j);
                            downloadComponent = false;
                            break;
                        }
                    //Otherwise, the client and server update descriptors and filenames
                    //are evaluated to determine whether a download is necessary.
                    } else if ((allAvailComps.get(i).getManifestAttributes().get("QiWorkbench-Component-Name")).equals(
                            clientComps.get(j).getManifestAttributes().get("QiWorkbench-Component-Name"))) {
                        //First, check for missing dependencies
                        if (isMissingDependency(clientComps.get(j))) {
                            logger.info(allAvailComps.get(i).getManifestAttributes().get("QiWorkbench-Component-Name")
                                    + " is up to date but one or more JAR dependencies is missing.  Updating binary bundle.");
                            break;
                        }
                        //Next, skip binary bundle download if the client update descriptor is the same or newer
                        else if ((clientComps.get(j).getTagNameAttributes().get("timestamp") != null && allAvailComps.get(i).getTagNameAttributes().get("timestamp") != null) &&
                                allAvailComps.get(i).getTagNameAttributes().get("timestamp").compareTo(clientComps.get(j).getTagNameAttributes().get("timestamp")) <= 0) {
                            logger.info(allAvailComps.get(i).getManifestAttributes().get("QiWorkbench-Component-Name") + " timestamp same or higher, no update needed");
                            clientComps.remove(j);
                            downloadComponent = false;
                            break;
                        }
                        //Finally, download the new version and delete the old one if the filename has changed, indicating a newer component version.
                        //(As opposed to a newer build of the same version)
                        else if (!((allAvailComps.get(i).getManifestAttributes().get("QiWorkbench-Component")).equals(
                                clientComps.get(j).getManifestAttributes().get("QiWorkbench-Component")))) {
                            logger.info(allAvailComps.get(i).getManifestAttributes().get("QiWorkbench-Component-Name") + " the same, with different file names");
                        } else if ((clientComps.get(j).getTagNameAttributes().get("timestamp") != null && allAvailComps.get(i).getTagNameAttributes().get("timestamp") != null) &&
                                allAvailComps.get(i).getTagNameAttributes().get("timestamp").compareTo(clientComps.get(j).getTagNameAttributes().get("timestamp")) > 0) {
                            logger.info(allAvailComps.get(i).getManifestAttributes().get("QiWorkbench-Component-Name") + " timestamp differs, newer component available");
                        } else if (clientComps.get(j).getTagNameAttributes().get("timestamp") == null || allAvailComps.get(i).getTagNameAttributes().get("timestamp") == null) {
                            logger.info("A timestamp doesnt exist in one of the Update Descriptors");
                        }
                        versionComps.add(clientComps.get(j));
                        deleteComps.add(clientComps.get(j));
                    }
                }
                //If the component is to be downloaded, add it to the updateComps list
                if (downloadComponent) {
                    logger.info(allAvailComps.get(i).getManifestAttributes().get("QiWorkbench-Component-Name") + " needs update");
                    updateComps.add(allAvailComps.get(i));
                }
            } catch (NullPointerException npe) {
                logger.warning("A null pointer exception occurred while analyzing  '" + allAvailComps.get(i).getFileName() + "': ");
            }
        }
        returnArray.add(updateComps);
        returnArray.add(deleteComps);
        returnArray.add(versionComps);
        return returnArray;
    }

    /**
     * Gets the total size of all the jar files being updated
     * @param updateComps List of components that are to be updated
     * @return Integer of the approx. size in bytes
     */
    public static int getTotalSize(ArrayList<UpdateItem> updateComps) {
        long totalSize = 0;
        for (int i = 0; i < updateComps.size(); i++) {
            totalSize = totalSize + updateComps.get(i).getTotalSize();
        }
        return (int) totalSize;
    }

    /**
     * Get all available components from all update centers (used for Auto Update)
     * @param myCID CID passed in from the MessageDispatcher
     * @param servletCID servletCID passed in from the MessageDispatcher
     * @return Array of components that need updates from all the update centers
     */
    public static ArrayList<UpdateItem> getAllAvailComps(String myCID, String servletCID) {
        ArrayList<UpdateItem> totalArray = new ArrayList<UpdateItem>();
        ArrayList<UpdateItem> centerArray = new ArrayList<UpdateItem>();
        ArrayList<String> updateCenterArray = new ArrayList<String>();
        String pucurl = qiWbConfiguration.getInstance().getPucAttributes().get("pucurl");
        String componentpath = qiWbConfiguration.getInstance().getPucAttributes().get("componentpath");

        if (pucurl != null && componentpath != null) {
            logger.info("Attempting to get all available components from pucurl: " + pucurl + ", componentpath: " + componentpath);
        } else {
            if (pucurl == null || "".equals(pucurl)) {
                logger.warning("PucAttr 'pucurl' is null or empty, update will not succeed");
            }
            if (componentpath == null) {
                logger.warning("PucAttr 'componentpath' is null or empty, update will not succeed");
            }
        }

        updateCenterArray.add(pucurl);
        updateCenterArray.add(componentpath);

        totalArray = getAllAvailComps(myCID, servletCID, updateCenterArray);
        ArrayList<Map<String, String>> updateCenters = qiWbConfiguration.getInstance().getUpdateCentersAttributes();
        for (int i = 0; i < updateCenters.size(); i++) {
            updateCenterArray.clear();
            updateCenterArray.add(updateCenters.get(i).get("updatecenter"));
            updateCenterArray.add(updateCenters.get(i).get("componentpath"));
            centerArray = getAllAvailComps(myCID, servletCID, updateCenterArray);

            for (int j = totalArray.size() - 1; j >= 0; j--) {
                for (int k = centerArray.size() - 1; k >= 0; k--) {
                    //Check if the two components have the same name
                    if ((totalArray.get(j).getManifestAttributes().get("QiWorkbench-Component-Name")).equals(centerArray.get(k).getManifestAttributes().get("QiWorkbench-Component-Name"))) {
                        //Check if the update descriptors are equal
                        if (totalArray.get(j).equals(centerArray.get(k))) {
                            centerArray.remove(k);
                            continue;
                        }
                        //Assuming the version is seperated by '.'
                        String sep = ".";
                        StringTokenizer st1 = new StringTokenizer(totalArray.get(j).getManifestAttributes().get("QiWorkbench-Component-Specification-Version"), sep);
                        StringTokenizer st2 = new StringTokenizer(centerArray.get(k).getManifestAttributes().get("QiWorkbench-Component-Specification-Version"), sep);
                        //Checks each number in the version and add the most recent version to the available components
                        while (st1.hasMoreTokens() && st2.hasMoreTokens()) {
                            String availVersionNum = st1.nextToken();
                            String checkVersionNum = st2.nextToken();
                            if (Integer.parseInt(availVersionNum) < Integer.parseInt(checkVersionNum)) {
                                totalArray.remove(j);
                                break;
                            } else if (Integer.parseInt(availVersionNum) > Integer.parseInt(checkVersionNum)) {
                                centerArray.remove(k);
                                break;
                            } else {
                                //do nothing, they are equal
                            }
                        }
                    }
                }
            }
            //Add the current update center components to the total components
            totalArray.addAll(centerArray);
        }
        return totalArray;
    }

    /**
     * Get all available components from all update centers (used for Manual Update)
     * @param myCID CID passed in from the MessageDispatcher
     * @param servletCID servletCID passed in from the MessageDispatcher
     * @param server ArrayList containing a servers information:	Cell 0 - Server URL
     * 																Cell 1 - Components Folder
     * @return Array of components that need updates from all the update centers
     */
    public static ArrayList<UpdateItem> getAllAvailComps(String myCID, String servletCID, ArrayList<String> server) {
        ArrayList<UpdateItem> allAvailComps = new ArrayList<UpdateItem>();
        boolean serverOK = false;
        // compose the message
        IQiWorkbenchMsg request = new QiWorkbenchMsg(myCID, servletCID, QIWConstants.CMD_MSG,
                QIWConstants.PING_SERVER_CMD, MsgUtils.genMsgID(),
                QIWConstants.STRING_TYPE, server.get(0));
        logger.info("Request for PING_SERVER_CMD formulated...");
        IQiWorkbenchMsg response = DispatcherConnector.getInstance().sendRequestMsg(request, 30000);
        logger.info("Response for PING_SERVER_CMD received.");
        if (!response.isAbnormalStatus()) {
            logger.finest("setTomcat: Established contact with " + server.get(0));
            serverOK = true;
        } else {
            logger.finest("setTomcat: Failed to connect to " + server.get(0));
        }

        if (serverOK) {
            request = new QiWorkbenchMsg(myCID, servletCID, QIWConstants.CMD_MSG,
                    QIWConstants.GET_AVAIL_COMPONENTS, MsgUtils.genMsgID(),
                    QIWConstants.ARRAYLIST_TYPE, server);
            response = DispatcherConnector.getInstance().sendRequestMsg(request, 30000);

            if (!response.isAbnormalStatus()) {
                logger.finest("Response Normal, returning new array");
                allAvailComps = (ArrayList<UpdateItem>) response.getContent();
            } else {
                logger.finest("Response Abnormal, returning empty array");
            //Return empty
            }
        }

        return allAvailComps;
    }

    /**
     * Gets the components currently on the users computer before update
     * @return ArrayList of components
     */
    public static ArrayList<UpdateItem> getClientComponents() {
        ArrayList<UpdateItem> updateItems = new ArrayList<UpdateItem>();
        try {
            //Get directory for file path
            File compCacheDir = new File(UpdateUtilities.getCompCachePath());
            if (!compCacheDir.isDirectory()) {
                return null;
            }
            //Array of all files and directories in the user Component Cache Path
            File[] files = compCacheDir.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (!files[i].isDirectory()) {
                    String fileName = files[i].getName();
                    if (fileName.endsWith(".xml")) {
                        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                        DocumentBuilder parser = factory.newDocumentBuilder();
                        Document xmlDocument = parser.parse(files[i]);
                        NodeList componentNodeList = xmlDocument.getElementsByTagName(UpdateConstants.TAG_COMPONENT);
                        Node n = componentNodeList.item(0);
                        UpdateItem uI = new UpdateItem(n, UpdateUtilities.getCompCachePath(), fileName);
                        uI.setLastModified(files[i].lastModified());
                        updateItems.add(uI);
                    }
                }
            }
        } catch (IOException e) {
            logger.finest("IOException in getting Client Components: " + e.getMessage());
        } catch (SAXException s) {
            logger.finest("SAXException in getting Client Components" + s.getMessage());
        } catch (ParserConfigurationException pce) {
            logger.finest("ParserConfigurationException in getting Client Components" + pce.getMessage());
        }
        return updateItems;
    }

    static boolean isMissingDependency(UpdateItem updateItem) {
        String depJarsAttrib = updateItem.getManifestAttributes().get("QiWorkbench-Component-Component-Dependencies");
        //updateItem has no JAR dependencies
        if (depJarsAttrib == null || "".equals(depJarsAttrib)) {
            return false;
        }
        String[] depJars = depJarsAttrib.split("[,]");
        File compCacheDir = new File(UpdateUtilities.getCompCachePath());
        for (String depJar : depJars) {
            File depJarFile = new File(compCacheDir + File.separator + depJar);
            if (!depJarFile.exists()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determine whether the client's required qiWorkbench version is compatible this qiWorkbench.
     *  
     * This is done by comparing each field of the component's required version number with the matching field of the
     * qiWorkbench version number.
     * 
     * If the qiWorkbench version has fewer fields than the required version number, missing fields are considered to be "0",
     * so that qiWb v. 1.3 is "1.3.0.0" when compared with required version "1.3.0.1".  
     * 
     * If the required version number has fewer fields than the actual qiWb version, the addition fields found in the actual
     * version number are not considered.  In other words, the qiComponent may express compatibility with a major qiWorkbench version without
     * requiring a specific minor version or point release.
     *
     * If the required version field is ever less than
     * the matching field of the actual qiWb version, the component is compatible.  For example, the following required and actual versions are compatible:
     *
     * Note that any valid Integer may constitute a version number field. 
     *
     * <ul>
     *   <li>1.2.3, 1.3.0 - required version is lower</li>
     *   <li>4.5.6, 4.5.6 - exact version match</li>
     *   <li>7.0, 7.0.2 - required major and minor version match, specific point release not required</li>
     *   <li>8.9.0, 8.9 - exact version match with qiWb version 8.9, inferred point release 0 </li>
     * </ul>
     *
     * The following required and actual versions are NOT compatible
     * <ul>
     *   <li>1.2.3, 1.2.2 - required version is higher</li>
     *   <li>4.5.6, 4.5 - required version is not compatible with qiWb version 4.5, inferred point release 0</li>
     * </ul>
     */
    static boolean isVersionCompatible(String requiredWorkbenchVersion, String actualWorkbenchVersion) {
        if ((requiredWorkbenchVersion == null) || "".equals(requiredWorkbenchVersion)) {
            throw new IllegalArgumentException("Unable to determine compatibility of UpdateManager's qiComponent with the running qiWorkbench becasue requiredWorkbenchVersion is null or empty.");
        }

        if ((actualWorkbenchVersion == null) || "".equals(actualWorkbenchVersion)) {
            throw new IllegalArgumentException("Unable to determine compatibility of UpdateManager's qiComponent with the running qiWorkbench becasue actualWorkbenchVersion is null or empty.");
        }

        String[] reqVerFields = requiredWorkbenchVersion.split("[.]");
        String[] actualVerFields = actualWorkbenchVersion.split("[.]");

        for (int verFieldIndex = 0; verFieldIndex < reqVerFields.length; verFieldIndex++) {

            int reqVerField = Integer.parseInt(reqVerFields[verFieldIndex]);
            int actualVerField = getVerField(actualVerFields, verFieldIndex);

            //if required version field is greater than actual qiWb version field, component is not compatible
            if (reqVerField > actualVerField) {
                return false;
            } else if (reqVerField < actualVerField) { // if required version field is less than current version field, component is compatible
                return true;
            } // otherwise version field exactly matches, proceed to the next field of required version number, if any
        }
        return true;
    }

    private static int getVerField(String[] verFields, int index) {
        if (index > verFields.length - 1) {
            return 0;
        } else {
            return Integer.parseInt(verFields[index]);
        }
    }
}