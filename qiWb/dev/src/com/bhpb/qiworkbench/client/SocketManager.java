/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006-2008  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.client;

import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.workbench.WorkbenchManager;

import java.io.IOException;
import java.net.Socket;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SocketChannel;
import java.nio.channels.Selector;
import java.nio.channels.SelectionKey;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Logger;

/**
 * Client-side socket manager for client sockets used in binary IO with the 
 * runtime server.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class SocketManager {
    public static Logger logger = Logger.getLogger(SocketManager.class.getName());
    
    private static final String NOMINAL_STATUS = "";
    
    /** Time in milliseconds after which a socket channel in the pool expires and is deleted from the pool */
    private static final long INACTIVE_THRESHOLD = 15 * 60 * 1000; // 15 minutes

    private static SocketManager singleton = null;
    
    /** Client-side socket pool. */
    ArrayList<BoundIOSocket> socketPool;
    
    /** Assigned client socket channel */
    SocketChannel clientSocketChannel = null;
	
	String workbenchSessionID = "";
    
    private SocketManager() {
		//get the unique, system wide sessionID for this workbench instance
		workbenchSessionID = WorkbenchManager.getInstance().getWorkbenchSessionID();
		
        socketPool = new ArrayList<BoundIOSocket>();
    }
    
    /**
     * Get the singleton instance of this class. If the Socket Manager doesn't
     * exist, create it.
     */
    public static SocketManager getInstance() {
        if (singleton == null) {
            singleton = new SocketManager();
        }
        return singleton;
    }
    
    /**
     * Get a client-side socket channel for communicating with the server. Use 
     * an available one in the socket pool, if any; otherwise, open a new one.
     * <p>
     * Note: It is the responsibility of the caller to connect the socket with
     * the server's socket prior to performing the IO if a new one is acquired;
     * otherwise, a reused one is already open and connected.
     * @param serverURL URL of the runtime server
     * @param serverPort Port of the server-side socket channel
     * @return Client-side socket channel. Open is a new one, open and connected
     * if a reused one.
     */
    public BoundIOSocket acquireSocketChannel(String serverURL, int serverPort) {
        BoundIOSocket boundIOSocket;
        logger.info("acquire client SocketChannel. serverURL="+serverURL+" serverPort="+serverPort);
        int size = socketPool.size();
        //First check the pool for an available socket that can be reused
        if (size != 0) {
            boundIOSocket = findReusableSocket(serverURL, serverPort);
            //If there is no client socket channel that can be reused, open a
            //new one.
            if (boundIOSocket == null) {
                clientSocketChannel = openSocketChannel();
                //Note: Since socket channel is not connected to the server, its
                //port is 0.
                logger.info("No reusable client SocketChannel in pool; open a new one");
                boundIOSocket = new BoundIOSocket(clientSocketChannel, null, serverURL, serverPort);
            } else {
                boundIOSocket.setLastUsage(System.currentTimeMillis());
                logger.info("Reusing client SocketChannel on port "+clientSocketChannel.socket().getLocalPort());
            }
        } else {
            //Open a new socket
            clientSocketChannel = openSocketChannel();
            logger.info("Pool of client SocketChannels is empty; open a new one");
            boundIOSocket = new BoundIOSocket(clientSocketChannel, null, serverURL, serverPort);
        }
        
        return boundIOSocket;
    }
    
    /**
     * Find a reusable client socket channel from the pool of available sockets.
     * If one is found, remove it from the pool.
     * @param serverURL URL of the runtime server
     * @param serverPort Port of the server-side socket channel
     * @return Reusable client socket channel, if any; otherwise, null.
     */
    private BoundIOSocket findReusableSocket(String serverURL, int serverPort) {
        BoundIOSocket boundIOSocket;
        //search the pool for a client socket bound to the server port
        Iterator<BoundIOSocket> iter = socketPool.iterator();
        while (iter.hasNext()) {
            boundIOSocket = iter.next();
            //ignore a socket in the process of being deleted from the pool
            if (boundIOSocket.isSocketUnavailable()) continue;
            if (boundIOSocket.getServerURL().equals(serverURL) &&
            boundIOSocket.getServerPort() == serverPort) {
                //remove the bounded client socket from the pool
                iter.remove();
                return boundIOSocket;
            }
        }
         
        return null;
    }
    
    /**
     * Open a new socket channel.
     * @return Newly opened socket channel if one can be obtained; otherwise, 
     * null.
     */
    private SocketChannel openSocketChannel() {
        try {
            //Create a nonblocking socket channel for the specified host name and port
            clientSocketChannel = SocketChannel.open();
    
            //set nonblocking mode for the socket channel
            clientSocketChannel.configureBlocking(false);
            //allow the socket to be bound even though a previous connection is in a timeout state
            clientSocketChannel.socket().setReuseAddress(true);
        } catch (IOException ioe) {
            clientSocketChannel = null;
        }
        
        return clientSocketChannel;
    }
     
    /**
     * Return a client-side socket channel to the socket channel pool for reuse.
     * @param clientSocketChannel Client socket channel available for reuse.
     * @param selector Client socket channel's selector.
     * @param serverURL URL of the server
     * @param serverPort Port of the server-side socket channel
     */
    public void releaseSocket(SocketChannel clientSocketChannel, Selector selector, String serverURL, int serverPort) {
        logger.info("Release client socket channel to pool: clientPort="+clientSocketChannel.socket().getLocalPort()+", bound to server port "+clientSocketChannel.socket().getPort()+", serverURL = "+serverURL+", serverPort="+serverPort);
        socketPool.add(new BoundIOSocket(clientSocketChannel, selector, serverURL, serverPort));
    }
    
    /**
     * Close all client socket channels in the pool and delete them. Called when
     * terminating a workbench session.
     * <p>
     * It suffices to simply delete them, for the dangling, matching socket
     * channel on the server side will eventually expire and be deleted from
     * the server's socket pool.
     */
     
    public void closeAllSockets() {
        //search the socket pool for a socket channel whose last usage exceeds
        //the timelimit threshold
        Iterator<BoundIOSocket> iter = socketPool.iterator();
        while (iter.hasNext()) {
            BoundIOSocket boundIOSocket = iter.next();
            //mark channel as unavailable for allocation
            boundIOSocket.setUnavailable();
            
            //close the server socket channel
            closeSocket(boundIOSocket.getClientSocketChannel(), boundIOSocket.getClientChannelSelector());
            
            Double inactiveTime = ((System.currentTimeMillis()-boundIOSocket.getLastUsage())/1000.0)/60.0;
            logger.info("closeALLSockets: Closing client channel on port #"+boundIOSocket.getServerPort()+"\nBound to server URL "+boundIOSocket.getServerURL()+", server port #"+boundIOSocket.getServerPort()+".\nInactive for "+inactiveTime+" minutes");
            
            //remove from the pool
            iter.remove();
        }
    }
      
    /**
     * Determine which client socket channels have been inactive too long. Close
     * them and delete them from the client's socket pool.
     * <p>
     * It suffices to simply delete them, for the dangling, matching socket
     * channel on the server side will eventually expire and be deleted from
     * the server's socket pool.
     */
     
    public void closeInactiveSockets() {
        //search the socket pool for a socket channel whose last usage exceeds
        //the timelimit threshold
        Iterator<BoundIOSocket> iter = socketPool.iterator();
        while (iter.hasNext()) {
            BoundIOSocket boundIOSocket = iter.next();
            long lastUsage = boundIOSocket.getLastUsage();
            long currentTime = System.currentTimeMillis();
            if (currentTime-lastUsage >= INACTIVE_THRESHOLD) {
                //mark channel as unavailable for allocation
                boundIOSocket.setUnavailable();
                
                //close the server socket channel
                closeSocket(boundIOSocket.getClientSocketChannel(), boundIOSocket.getClientChannelSelector());
                
                Double inactiveTime = ((currentTime-lastUsage)/1000.0)/60.0;
                logger.info("closeInactiveSockets: Closing client channel on port #"+boundIOSocket.getServerPort()+"\nBound to server URL "+boundIOSocket.getServerURL()+", server port #"+boundIOSocket.getServerPort()+".\nInactive for "+inactiveTime+" minutes");
                
                //remove from the pool
                iter.remove();
            }
        }
    }
    
    /**
     * Close a client socket channel and its associated socket.
     * @param socketChannel The socket channel to be closed
     * @param selector The selector registered with the socket channel
     * @return Empty string if socket successfully closed; otherwise, the reason for failure.
     */
    public String closeSocket(SocketChannel socketChannel, Selector selector) {
        Socket assocSocket = (socketChannel != null) ? socketChannel.socket() : null;
        
        int port = (socketChannel != null) ? socketChannel.socket().getLocalPort() : -1;
        
        String errmsg = "Cannot close socket channel. CAUSE: ";
         
        try {
            if (assocSocket != null && (assocSocket.isBound() || !assocSocket.isClosed())) {
                assocSocket.close();
            } else {
                if (assocSocket == null) {
                    logger.warning("assocSocket == null");
                } else {
                    if (!assocSocket.isBound()) {
                        logger.warning("assocSocket.isBound() == false");
                    }
                    if (!assocSocket.isClosed()) {
                        logger.warning("assocSocket.isClosed() == false");
                    }
                }
                errmsg += "Associated socket is null or else is neither bound, connected nor open.";
                logger.warning(errmsg);
                return errmsg;
            }

            if (selector != null && selector.isOpen()) {
                logger.info("Closing selector on port #" + port + "...");
                selector.close();
                logger.info("Selector closed.");
            } else {
                if (selector == null) {
                    logger.warning("sel == null");
                } else {
                    if (!selector.isOpen()) {
                        logger.warning("sel.isOpen() == false");
                    }
                }
                errmsg += "Registered selector is null or not open.";
                logger.warning("Unable to close selector on port #" + port + "because registered selector is null or not open.");
                return errmsg;
            }
            
            if (socketChannel != null && socketChannel.isOpen()) {
                socketChannel.close();
            } else {
                if (socketChannel == null) {
                    logger.warning("socketChannel == null");
                } else {
                    if (!socketChannel.isOpen()) {
                        logger.warning("socketChannel.isOpen() == false");
                    }
                }
                errmsg += "Socket channel is null or not open.";
                logger.warning(errmsg);
                return errmsg;
            }

            return "";
        } catch (IOException ioe) {
            logger.warning("Caught exception while attempting to close socket: " + ioe.getMessage());
            return errmsg + ioe.getMessage();
        }
    }
    
    /**
     * Information about a client-server socket bound together during geoIO.
     */
    public class BoundIOSocket {
        /** URL of server */
        String serverURL = "";
        
        /** port of server channel */
        int serverPort;
        
        /** client socket channel */
        SocketChannel clientSocketChannel;
        
        /** selector associated with registered client socket channel */
        Selector registeredSelector;
        
        /** timestamp of last usage, i.e., client socket involved in geoIO */
        long lastUsage = 0L;
        
        /** Unavailable for reuse indicator. Set when socket in the process of being deleted from the pool. */
        boolean unavailable = false;
        
        public BoundIOSocket(SocketChannel clientSocketChannel, Selector selector, String serverURL, int serverPort) {
            this.clientSocketChannel = clientSocketChannel;
            this.registeredSelector = selector;
            this.serverURL = serverURL;
            this.serverPort = serverPort;
            this.lastUsage = System.currentTimeMillis();
        }
        
        public boolean isSocketUnavailable() {
            return unavailable;
        }
        
        //GETTERS
        public String getServerURL() {
            return serverURL;
        }
        public int getServerPort() {
            return serverPort;
        }
        public SocketChannel getClientSocketChannel() {
            return clientSocketChannel;
        }
        public Selector getClientChannelSelector() {
            return registeredSelector;
        }
        public long getLastUsage() {
            return lastUsage;
        }
        
        //SETTERS
        public void setServerURL(String serverURL) {
            this.serverURL = serverURL;
        }
        public void setServerPort(int serverPort) {
            this.serverPort = serverPort;
        }
        public void setClientSocketChannel(SocketChannel channel) {
            this.clientSocketChannel = channel;
        }
        public void setClientChannelSelector(Selector selector) {
            this.registeredSelector = selector;
        }
        public void setLastUsage(long lastUsage) {
            this.lastUsage = lastUsage;
        }
        public void setUnavailable() {
            unavailable = true;
        }
    }
}
