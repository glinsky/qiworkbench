/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006-2008  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.InterruptedException;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.QiwbPreferences;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.DispatcherConnector;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
import com.bhpb.qiworkbench.workbench.WorkbenchManager;

/**
 * Client-side Stats Manager for statistical events. Used to log usage events and transmit logged
 * usage events to the statistical server specified in the qiWbconfig.xml file.
 * There is a server-side stats mananger that gathers stats from all end-users
 * and processes them in preparation for the generation and display of the stats
 * charts.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class StatsManager {
    public static Logger logger = Logger.getLogger(StatsManager.class.getName());

    private static StatsManager singleton = null;

    /** item separator in stats event */
    String statsep = " ";

    /** URL of the STATS server. */
    String statsURL = "";

    /** Path of the directory housing the stats files */
    String statsDirPath = "";

    /** Path of the stats log file where stats events are recorded during a workbench sessionl */
    String statsLogPath = "";
    File logFile = null;

    private StatsManager() {
        String filesep = File.separator;
        //Set the path of the stats log file
        String userHOME = MessageDispatcher.getInstance().getUserHOME();
        String logsDir = userHOME + filesep + QIWConstants.QIWB_WORKING_DIR + filesep + "logs";
        statsLogPath = logsDir + filesep + QIWConstants.USAGE_LOG_FILE;
        logFile = new File(statsLogPath);
    }

    /**
     * Get the singleton instance of this class. If the Stats Manager doesn't
     * exist, create it.
     */
    public static StatsManager getInstance() {
        if (singleton == null) {
            singleton = new StatsManager();
        }
        return singleton;
    }

    /**
     * Set the URL of the STATS server
     * @param URL of the STATS server
     */
    public void setStatsURL(String statsURL) {
        this.statsURL = statsURL;
    }

    /**
     * Set the path of the directory where the stats files are stored
     * @param Path of the stats directory
     */
    public void setStatsDirPath(String statsDirPath) {
        this.statsDirPath = statsDirPath;
    }

    /**
     * Create a stats log event.
     * @param module qiWorkbench module log event associated with
     * @param action Event action
     * @return Stats event.
     */
     public String createStatsEvent(String module, String action) {
         String statsEvent = "";
         //Add timestamp
         statsEvent += Long.toString(System.currentTimeMillis());
         statsEvent += statsep + module + statsep + action;
         statsEvent += statsep + QiwbPreferences.getHostName();

         return statsEvent;
     }

    /**
     * Log a stats event.
     * @param statsEvent A statistical event to be logged.
     */
    public void logStatsEvent(String statsEvent) {
        //Append the stats event to the usage log file
        BufferedWriter bw = null;

        try {
            FileOutputStream fos = new FileOutputStream(logFile, true);
            bw = new BufferedWriter(new OutputStreamWriter(fos));
            bw.write(statsEvent);
            bw.newLine();   //write the line separator
        } catch (IOException ioe) {
            if (ioe instanceof FileNotFoundException && ioe.getMessage().contains("Permission denied"))
                logger.warning("WARNING: Insufficient privilege to write usage log file.");
            else
                logger.warning("WARNING: IO exception writing usage log file:"+ioe.getMessage());
        } finally {
            try {
                if (bw != null) bw.close();
            } catch (IOException e) {}
        }
    }

    /**
     * Check if the stats log file exists
     * @return true if the stats log file exists; otherwise, false
     */
     public boolean isStatsLog() {
         return logFile.isFile() ? true : false;
     }

    /**
     * Get the stats events from the stats log file
     * @return List of usage events
     */
    private ArrayList<String> getStatEvents() {
        ArrayList<String> events = new ArrayList<String>();

        BufferedReader br = null;
        try {
            FileInputStream fis = new FileInputStream(logFile);
            br = new BufferedReader(new InputStreamReader(fis));
            String line = br.readLine();
            // read file line by line
            while (line != null) {
                events.add(line);
                line = br.readLine();
            }
        } catch (IOException ioe) {
            logger.warning("IO exception reading usage log:"+ioe.getMessage());
        } finally {
            try {
                if (br != null) br.close();
            } catch (IOException e) {}
        }

        return events;
    }

    /**
     * Transmit the stats log to the STATS server.
     */
    public void transmitStats(String requesterCID) {
        //check if the STATS server is down or not accessible
        if (statsURL.equals("")) return;

        //Get the stats events from the stats log file
        ArrayList<String> events = getStatEvents();

        //Send the events to the STATS server
        ArrayList params = new ArrayList();
        params.add(statsDirPath);
        params.add(events);

        IQiWorkbenchMsg request = new QiWorkbenchMsg(requesterCID, MessageDispatcher.getInstance().getStatsServletDispDesc().getCID(), QIWConstants.CMD_MSG, QIWConstants.RECORD_STATS_CMD, MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params);
        IQiWorkbenchMsg response = DispatcherConnector.getInstance().sendRequestMsg(request, statsURL);

        //If response is normal, delete the stats log file
        if (!response.isAbnormalStatus()) logFile.delete();

    }
}
