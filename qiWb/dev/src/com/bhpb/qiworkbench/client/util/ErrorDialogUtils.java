/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.client.util;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.workbench.WorkbenchManager;

/**
 * Error dialog utilities
 * @author Gil Hansen
 *
 */
public class ErrorDialogUtils {
    /**
     * Show a general error dialog.
     * @param type QIWConstants.ERROR_DIALOG or QIWConstants.WARNING_DIALOG
     * @param stack stack trace
     * @param message one-line error message
     * @param causes list of possible causes
     * @param suggestions list of suggested remedies
     */
    public static void showErrorDialog(String type, StackTraceElement[] stack, String message, String[] causes, String[] suggestions) {
      ArrayList list = new ArrayList(7);
      // 0 error dialog parent component
      list.add(WorkbenchManager.getInstance().getWorkbenchGUI());
      // 1 message type
      list.add(type);
      // 2 caller's display name
      list.add(WorkbenchManager.getInstance().getComponentDescriptor().getDisplayName());
      // 3 stack trace
      list.add(stack);
      // 4 message
      list.add(message);
      // 5 possible causes
      list.add(causes);
      // 6 suggested remedies
      list.add(suggestions);
      // send message to start error service
      getErrorService(list);
    }

    /**
     * Start error service for showing internal errors, for example unexpected NULL etc
     * @param stack StackTrace
     * @param message one-line error message
     */
    public static void showInternalErrorDialog(StackTraceElement[] stack, String message) {
        showErrorDialog(QIWConstants.ERROR_DIALOG,stack,message,
                        new String[] {"Internal viewer error"},
                        new String[] {"Contact workbench support"});
    }

    /**
     * Get an error dialog service.
     * @param list ArrayList of arguments
     */
    private static void getErrorService(ArrayList list) {
        MessagingManager messagingMgr = WorkbenchManager.getInstance().getMessagingManager();

        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                                                QIWConstants.GET_ERROR_DIALOG_SERVICE_CMD,true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
        if (response == null || response.isAbnormalStatus()) {
            JOptionPane.showMessageDialog(null,"Error getting Error Service");
            return;
        }

        ComponentDescriptor errorServiceDesc = (ComponentDescriptor)response.getContent();
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_ERROR_DIALOG_SERVICE_CMD, errorServiceDesc, QIWConstants.ARRAYLIST_TYPE, list, true);
        response = messagingMgr.getMatchingResponseWait(msgID,10000);
    }
}
