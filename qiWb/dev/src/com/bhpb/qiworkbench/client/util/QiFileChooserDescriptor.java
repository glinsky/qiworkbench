package com.bhpb.qiworkbench.client.util;

import com.bhpb.qiworkbench.api.IQiFileChooserDescriptor;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.api.IComponentDescriptor;

public class QiFileChooserDescriptor implements IQiFileChooserDescriptor {

    private Component parentGUI;
    //if false; navigation upward beyond home directory will not be allowed
    private boolean navigationUpwardEnabled = true;
    //directory where the file chooser gets started
    private String homeDirectory;
    private String directoryRemembered;
    private boolean directoryRememberedEnabled = false;
    private String title = "";
    private String preText = "";
    private String messageCommand = "";
    private boolean multiSelectionEnabled = false;
    private boolean directoryOnly = false;
    private IComponentDescriptor producerComponentDescriptor;
    //Type value indicating that the JFileChooser supports an "Open" file operation.
    private int fileSelectionMode = JFileChooser.FILES_AND_DIRECTORIES;
    private FileFilter fileFilter;
    private String serverUrl;
    private int dialogType = JFileChooser.OPEN_DIALOG;
    //base name (full directory name) of the file path
    private String selectedFilePathBase = "";
    private List<String> selectedFileNameList = new ArrayList<String>();
    //additional properties this object may contain
    private Map additionalProperties;
    //possible return codes JFileChooser.APPROVE_OPTION, JFileChooser.CANCEL_OPTION
    private int returnCode = JFileChooser.CANCEL_OPTION;
    private boolean createDirectory = false;


    /** Sets the JFileChooser to allow the user to just select files, just select directories, or
        select both files and directories. The default is JFilesChooser.FILES_ONLY.
    */
    public void setFileSelectionMode(int mode){
        fileSelectionMode = mode;
    }

    public int getFileSelectionMode(){
        return fileSelectionMode;
    }

    public void setAdditionalProperties(Map properties){
        this.additionalProperties = properties;
    }

    public Map getAdditionalProperties(){
        return additionalProperties;
    }

    public void setReturnCode(int code){
        returnCode = code;
    }

    public int getReturnCode(){
        return returnCode;
    }

    public void setDirectoryOnlyEnabled(boolean bool){
        directoryOnly = bool;
    }

    public boolean isDirectoryOnlyEnabled(){
        return directoryOnly;
    }

    public void setCreateDirectory(boolean bool) {
        createDirectory = bool;
    }

    public boolean isCreateDirectoryEnabled() {
        return createDirectory;
    }

    public int getDialogType(){
        return dialogType;
    }

    public void setDialogType(int type){
        if(this.dialogType == type) {
            return;
        }

        if(!(type == JFileChooser.OPEN_DIALOG || type == JFileChooser.SAVE_DIALOG)) {
            throw new IllegalArgumentException("Incorrect Dialog Type: " + type);
        }

        dialogType = type;
    }

    public boolean isFileSelectionEnabled(){
        return fileSelectionMode == JFileChooser.FILES_ONLY ? true : false;
    }

    public boolean isDirectorySelectionEnabled(){
        return fileSelectionMode == JFileChooser.DIRECTORIES_ONLY ? true : false;
    }


    public boolean isFilesDirectories(){
        return fileSelectionMode == JFileChooser.FILES_AND_DIRECTORIES ? true : false;
    }

    public void setParentGUI(Component gui){
        parentGUI = gui;
    }

    public Component getParentGUI(){
        return parentGUI;
    }

    public void setMultiSelectionEnabled(boolean enabled){
        multiSelectionEnabled = enabled;
    }

    public boolean isMultiSelectionEnabled(){
        return multiSelectionEnabled;
    }

    public boolean isDirectoryRememberedEnabled(){
        return directoryRememberedEnabled;
    }

    public void setDirectoryRememberedEnabled(boolean enabled){
        directoryRememberedEnabled = enabled;
    }

    public void setNavigationUpwardEnabled(boolean enabled){
        navigationUpwardEnabled = enabled;
    }

    public boolean isNavigationUpwardEnabled(){
        return navigationUpwardEnabled;
    }

    public void setServerUrl(String url){
        serverUrl = url;
    }

    public String getServerUrl(){
        return serverUrl;
    }

    public void setHomeDirectory(String home){
        homeDirectory = home;
    }

    public String getHomeDirectory(){
        return homeDirectory;
    }

    public void setMessageCommand(String command){
        messageCommand = command;
    }

    public String getMessageCommand(){
        return messageCommand;
    }

    public void setDirectoryRemembered(String dir){
        directoryRemembered = dir;
    }

    public String getDirectoryRemembered(){
        return directoryRemembered;
    }

    public void setSelectedFilePathBase(String dir){
        selectedFilePathBase = dir;
    }

    public String getSelectedFilePathBase(){
        return selectedFilePathBase;
    }

    public List<String> getSelectedFileNameList(){
        return selectedFileNameList;
    }

    public void setSelectedFileNameList(List<String> list){
        selectedFileNameList = list;
    }

    public FileFilter getFileFilter(){
        return fileFilter;
    }

    public void setFileFilter(FileFilter filter){
        fileFilter = filter;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    public void setPreText(String text){
        this.preText = text;
    }

    public String getPreText(){
        return preText;
    }

    public IComponentDescriptor getProducerComponentDescriptor(){
        return producerComponentDescriptor;
    }

    public void setetProducerComponentDescriptor(IComponentDescriptor desc){
        producerComponentDescriptor = desc;
    }
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
    }

}
