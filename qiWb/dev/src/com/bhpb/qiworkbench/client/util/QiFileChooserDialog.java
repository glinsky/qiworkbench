/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */

package com.bhpb.qiworkbench.client.util;

import com.bhpb.qiworkbench.compAPI.IconResource;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;

import com.bhpb.qiworkbench.compAPI.QiSpaceDescUtils;


/**
 * Dialog for selecting a file/dir. Uses qiWorkbench IO services to get the
 * contents of a directory. The contents may be filtered. The accessiblity
 * of the directory depends on the location of the Tomcat server. That is,
 * the directory must be accessible from the server.
 * <p>This file chooser does not use Java's JFileChooser, because it needs
 * to be able to use the remote IO service. This dialog is also used when
 * Tomcat is local so the UI is consistent regardless. In this case, the
 * local IO service is used.
 * <p>FileChooserDialog is NOT functionally equivalent to JFileChooser. It
 * does allow one to go up a directory, restrict content to just
 * directories, specify file extensions in the filter and select multiple files, but it does not
 * allow one to create a new directory.
 * <p>The dialog presents first sorted directories followed by sorted files
 * within the specified directory.
 *
 * @author Gil Hansen, LT Li
 * @version 1.0
 */

public class QiFileChooserDialog extends JDialog {

    private static final int _GENERAL_FILE = 2;
    private static final int _DIRECTORY = 3;
    private boolean _enableDirectorySelection = false;

    public static Logger logger = Logger.getLogger(QiFileChooserDialog.class.getName());

    private JList _list;
    private DefaultListModel _listModel;

    private JTextField _pathField;

    private JTextField _nameField;

    private JComboBox _filterCombo;
    private JButton _executeButton;
    private JButton _executeButton1;
    private JButton _upButton;
    private String _homeDirectory;
    private int _returnValue;
    //owner name of the current directory
    private String directoryOwner;
    private String requestMsgCommand;
    private Vector _filters;
    private Component parent;
    private ImageIcon _fileIcon = null;
    private ImageIcon _dirIcon = null;
    private ImageIcon _homeIcon = null;
    private ImageIcon _upIcon = null;
    private FileChooserService agent  = null;
    private boolean naviagateUpward = true;
    private String chooserType = null;
    private String defaultPreText = "";
    private String defaultExtension = "";
    private String serverUrl = "";
    private boolean multiSelectionEnabled = false;
    private boolean rememberAsDefault = false;
    private QiFileChooserDescriptor fileChooserDescriptor = new QiFileChooserDescriptor();
    private JCheckBox remember = null;

    public void setUpwardNavigation(boolean naviagateUpward) {
        this.naviagateUpward = naviagateUpward;
    }

    
    /**
     * Constructor which allows a modal FileChooserDialog with a Dialog parent.
     * 
     * @param parentDlg
     * @param agent
     * @param modal
     */
    public QiFileChooserDialog(Dialog parentDlg, FileChooserService agent, boolean modal) {
        super(parentDlg, modal);
        initGUI((Component)parentDlg, agent);
    }
    
    /**
     * Constructor which allows a modal FileChooserDialog with a Frame parent.
     * 
     * @param parentDlg
     * @param agent
     * @param modal
     */
    public QiFileChooserDialog(Frame parentFrame, FileChooserService agent, boolean modal) {
        super(parentFrame, modal);
        initGUI((Component)parentFrame, agent);
    }   
    
    /**
     * Constructor.
     *
     * @param messageManager Message Manager for the component GUI using this dialog.
     */
    private void initGUI(Component parentGui, FileChooserService agent) {
        this.agent = agent;
        if(agent != null)
            fileChooserDescriptor = agent.getQiFileChooserDescriptor();
        this.setTitle(fileChooserDescriptor.getTitle());
        this.parent = fileChooserDescriptor.getParentGUI();
        this.setLocationRelativeTo(parent);
        //this.setModal(true);

        multiSelectionEnabled = fileChooserDescriptor.isMultiSelectionEnabled();
        _returnValue = JFileChooser.CANCEL_OPTION;
        _homeDirectory = fileChooserDescriptor.getHomeDirectory();
        naviagateUpward = fileChooserDescriptor.isNavigationUpwardEnabled();
        serverUrl = fileChooserDescriptor.getServerUrl();
        if(fileChooserDescriptor.getDialogType() == JFileChooser.OPEN_DIALOG)
            chooserType = "Open";
        else if(fileChooserDescriptor.getDialogType() == JFileChooser.SAVE_DIALOG)
            chooserType = "Save";
        defaultPreText = fileChooserDescriptor.getPreText();
        _filters = new Vector();
        String[] allextensions = {};
        _filters.addElement(new GenericFileFilter(allextensions, "All Files"));

        try {
            _fileIcon = IconResource.getInstance().getImageIcon(IconResource.GENERALFILE_ICON);
            _dirIcon = IconResource.getInstance().getImageIcon(IconResource.DIRECTORY_ICON);
            _homeIcon = IconResource.getInstance().getImageIcon(IconResource.HOME_ICON);
            _upIcon = IconResource.getInstance().getImageIcon(IconResource.UP_ICON);
        }
        catch (Exception ex) {
            logger.info("FileChooserDialog.constructor failed to load images");
            logger.info("    " + ex.toString());
        }
        setupGUI();
    }

    public void enableDirectorySelection(boolean enable) {
        _enableDirectorySelection = enable;
    }

    public void setMultiSelectionEnabled(boolean enabled){
        multiSelectionEnabled = enabled;
    }

    public boolean isMultiSelectionEnabled(){
        return multiSelectionEnabled;
    }

    public void addChoosableFileFilter(FileFilter filter) {
        if (filter!=null && !_filters.contains(filter)) {
            _filters.addElement(filter);
            _filterCombo.setSelectedItem(filter);
        }
    }

    public void setChoosableFileFilter(FileFilter filter, int index) {
        if (filter!=null && !_filters.contains(filter)) {
            _filters.setElementAt(filter,index);
            _filterCombo.setSelectedItem(filter);
        }
    }

    public void setSelectedFile(String fname) {
        try {
            //String path = fname.substring(0, fname.lastIndexOf("/"));
            String path = fname.substring(0, fname.lastIndexOf(File.separator));
            //String name = fname.substring(fname.lastIndexOf("/")+1);
            String name = fname.substring(fname.lastIndexOf(File.separator)+1);
            setPathFieldText(path);
            updateList();
            ListObject lobj = null;
            for (int i=0; i<_listModel.size(); i++) {
                lobj = (ListObject) _listModel.getElementAt(i);
                if (lobj.getType() != _DIRECTORY &&
                        lobj.toString().equals(name)) {
                    _list.setSelectedIndex(i);
                    _nameField.setText(_list.getSelectedValue().toString());
                    break;
                }
            }
            logger.info("SSS " + fname + " : " + _list.getSelectedValue().toString());
        }
        catch (Exception ex) {
            logger.info("RemoteFileChooser.setSelectedFile fails for file " + fname);
            ex.printStackTrace();
        }
    }

    public boolean getDefaultIndicator() {
        return rememberAsDefault;
    }

    public String getPath() { return _pathField.getText(); }
    public String getName() { return _nameField.getText(); }
    public void setDirectoryOwner(String owner){
        this.directoryOwner = owner;
    }

    public void setServerUrl(String url){
        this.serverUrl = url;
    }
    /** Get the full pathname of the selected file
     *
     * @return Full pathname of the selected file
     */
    public String getSelectedFileAbsolutePathname() {
        ArrayList<String> list = agent.getRemoteOSInfo();
        if(list != null)
            return _pathField.getText() + (String)list.get(1) + _nameField.getText();
        else
            return _pathField.getText() + File.separator + _nameField.getText();
    }

    /** Get the full pathname of the selected directory
     *
     * @return Full pathname of the selected directory
     */
    public String getSelectedDirAbsolutePathname() {
        return _pathField.getText();
    }

    private ArrayList<ArrayList> list = null;
    public void setDirFileList(ArrayList<ArrayList> list) {
        this.list = list;
    }

    public void updateList() {
        final QiFileChooserDialog dialog = this;
        Thread worker = new Thread() {
            public void run() {
                String pathString = _pathField.getText();
                synchronized(agent){
                    list = agent.getDirFileList(pathString,serverUrl);
                }

                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        updateList1();
                    }
                });
            }
        };
        worker.start();
    }


    public void updateList1() {
        //String pathString = _pathField.getText();
        _listModel.removeAllElements();
        if(list == null){
            logger.warning("Dir and File list is null");
            return;
        }
        try {

            ArrayList<String> dirList = list.get(0);  //the first element is directory list
            ArrayList<String> fileList = list.get(1);  //the second element is file list
            GenericFileFilter filter = (GenericFileFilter) _filterCombo.getSelectedItem();
            if(filter.acceptDirectoryOnly() || fileChooserDescriptor.isDirectoryOnlyEnabled()){
                for(String s : dirList)
                    _listModel.addElement(new ListObject(_DIRECTORY,s));
            } else {
                for(String s : dirList)
                    _listModel.addElement(new ListObject(_DIRECTORY,s));
                for(String s : fileList) {

                    if (_enableDirectorySelection) continue;
                    if (filter != null) {
                        if (filter.acceptFilename(s)){
                            _listModel.addElement(new ListObject(_GENERAL_FILE,s));
                        }
                    }
                    else
                        _listModel.addElement(new ListObject(_GENERAL_FILE, s));
                }
            }

            if (_nameField.getText()!=null && _nameField.getText().length()>0) {
                ListObject lobj = null;

                String oldText = _nameField.getText();

                boolean containsOldText = false;
                for (int i=0; i<_listModel.size(); i++) {
                    lobj = (ListObject) _listModel.getElementAt(i);
                    if (lobj.getType() != _DIRECTORY &&
                            lobj.toString().equals(oldText)) {
                        containsOldText = true;
                        //_list.setSelectedIndex(i);
                        break;
                    }
                }
                if (!containsOldText) _nameField.setText("");
            }
            else  _nameField.setText("");
        }
        catch (Exception e) {
            logger.warning("FileChooserDialog.updateList Exception " + e.toString());
            e.printStackTrace();
        }

    }

    private boolean fileExistsInList(String name) {
        Enumeration enumElements = _listModel.elements();
        ListObject obj = null;
        while(enumElements.hasMoreElements()) {
            obj = (ListObject) enumElements.nextElement();
            if (obj.getType() == _GENERAL_FILE && obj.toString().equals(name))
                return true;
        }
        return false;
    }

    private ListObject getListObjectByName(String name){
        Enumeration enumElements = _listModel.elements();
        ListObject obj = null;
        while(enumElements.hasMoreElements()) {
            obj = (ListObject) enumElements.nextElement();
            if (obj.toString().equals(name))
                return obj;
        }
        return null;
    }

    private boolean directoryExistsInList(String name) {
        Enumeration enumElements = _listModel.elements();
        ListObject obj = null;
        while(enumElements.hasMoreElements()) {
            obj = (ListObject) enumElements.nextElement();
            if (obj.getType() == _DIRECTORY && obj.toString().equals(name))
                return true;
        }
        return false;
    }

    public int showOpenDialog() {
        _executeButton.setText(" Open ");
        GenericFileFilter filter = (GenericFileFilter) _filterCombo.getSelectedItem();
        if(filter.acceptDirectoryOnly() || fileChooserDescriptor.isDirectoryOnlyEnabled())
            _executeButton1.setVisible(true);
        this.updateList();
        this.setVisible(true);
        return _returnValue;
    }

    public int showSaveDialog() {
        _executeButton.setText("  Save ");
        this.updateList();
        this.setVisible(true);
        return _returnValue;
    }

    public void hideDialog() {
        agent.returnFileChooserService();
        super.setVisible(false);
    }

    public void showMessage(String message){
        JOptionPane.showMessageDialog(this, message, "QI Workbench",
                JOptionPane.INFORMATION_MESSAGE);
    }

    public void setPathFieldText(String text) {
        agent.setOwnerCurrentDirectory(directoryOwner,text);
        text = text.trim();
        _pathField.setText(text);
        _upButton.setEnabled(true);
    }


    public void setChooserType(String type){
        this.chooserType = type;
    }

    public void setRequestMsgCommand(String cmd){
        this.requestMsgCommand = cmd;
    }

    public void setDefaultPreText(String text){
        this.defaultPreText = text;
    }

    public void setDefaultExtension(String extension){
        this.defaultExtension = extension;
    }

    public int showConfirmBox(String message){
        return JOptionPane.showConfirmDialog(this,message,"File overwriting confirmation",JOptionPane.YES_NO_OPTION);
    }

    public int getReturnValue(){
        return _returnValue;
    }

    public int approveSelection() {
        return approveSelection(_pathField.getText());
    }
    private int approveSelection(String filename) {
        int re = JFileChooser.CANCEL_OPTION;
        GenericFileFilter filter = (GenericFileFilter) _filterCombo.getSelectedItem();
        if(filter.acceptDirectoryOnly() || fileChooserDescriptor.isDirectoryOnlyEnabled()){
            if (directoryExistsInList(filename)) re = JFileChooser.APPROVE_OPTION;
            else{
                String message = "Can not find the " + filename + ". Please try again.";
                re = JFileChooser.CANCEL_OPTION;
            }
            return re;
        }else
            if (filter.acceptFilename(filename)) re = JFileChooser.APPROVE_OPTION;
        if (_executeButton.getText().indexOf("Open") > -1) {
            // must opan an existing file
            if (!fileExistsInList(filename)) re = JFileChooser.CANCEL_OPTION;
        }
        else {
            // saving an inexisting file is always okay
            if (!fileExistsInList(filename)) re = JFileChooser.APPROVE_OPTION;
            else{
                String message = filename + " already exists. Do you want to replace it?";
                int action = showConfirmBox(message);
                if(action == JOptionPane.YES_OPTION )
                    re = JFileChooser.APPROVE_OPTION;
                else if(action == JOptionPane.CANCEL_OPTION)
                    re = JFileChooser.CANCEL_OPTION;
                else if(action == JOptionPane.NO_OPTION)
                    re = JOptionPane.NO_OPTION;
            }

        }
        return re;
    }

    private void setupGUI() {
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                _returnValue = JFileChooser.CANCEL_OPTION;
            }
        });

        JPanel extraButtonPanel = new JPanel(new FlowLayout());
        _upButton = new JButton(_upIcon);
        _upButton.setPreferredSize(new Dimension(25, 25));
        JButton hmButton = new JButton(_homeIcon);
        hmButton.setPreferredSize(new Dimension(25, 25));
        _upButton.addActionListener(new UpListener());
        hmButton.addActionListener(new HomeListener());
        extraButtonPanel.add(_upButton);
        extraButtonPanel.add(hmButton);

        JPanel p1 = new JPanel(new BorderLayout());
        p1.add(new JLabel("  Look in:    "), BorderLayout.WEST);
        _pathField = new JTextField("");
        _pathField.setEditable(true);
        p1.add(_pathField, BorderLayout.CENTER);
        p1.add(extraButtonPanel, BorderLayout.EAST);

        _pathField.addKeyListener(new MyPathKeyListener());

        _pathField.setText(_homeDirectory);

        JPanel p2 = new JPanel(new BorderLayout());
        _listModel = new DefaultListModel();
        _list = new JList(_listModel);
        _list.setCellRenderer(new MyCellRenderer());
        if(multiSelectionEnabled){
            _list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            _list.addMouseListener(new MyListMouseAdapter2());
        }else{
            _list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            _list.addMouseListener(new MyListMouseAdapter());
        }

        _list.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (_list.getSelectedValue() != null){
                    _nameField.setText(_list.getSelectedValue().toString());
                    Object selectedObj = _list.getSelectedValue();
                    if (selectedObj instanceof ListObject) {
                        ListObject listObj = (ListObject) selectedObj;
                        int type = listObj.getType();
                        if ( type != _DIRECTORY && agent.getDefaultIndicator()) {
                            if(remember != null)
                                remember.setEnabled(true);
                        }
                    }

                }
            }
        });

        p2.add(new JScrollPane(_list), BorderLayout.CENTER);

        //JPanel p3 = new JPanel(new FlowLayout());
        _executeButton = new JButton("  Open ");
        if(multiSelectionEnabled)
            _executeButton.addActionListener(new ExecuteMultipleListener());
        else
            _executeButton.addActionListener(new ExecuteListener());
        _executeButton1 = new JButton("  OK  ");
        _executeButton1.setVisible(false);
        _executeButton1.addActionListener(new ExecuteListener1());
        JButton b3 = new JButton("Cancel");
        b3.addActionListener(new CancelListener());
        //p3.add(b1);
        //p3.add(b3);

        JPanel p4 = new JPanel(new BorderLayout(10, 10));
        p4.add(new JLabel("  File name:"), BorderLayout.WEST);
        _nameField = new JTextField("");
        _nameField.addKeyListener(new MyNameFieldKeyListener());
        /*
        _nameField.addFocusListener(new FocusListener(){
            public void focusGained(FocusEvent e){
                if(_nameField.getText().trim().length() == 0)
                    remember.setEnabled(false);
            }
            public void focusLost(FocusEvent e) {
                if(_nameField.getText().trim().length() != 0)
                    remember.setEnabled(true);
              }
        });
         */
        p4.add(_nameField, BorderLayout.CENTER);
        JPanel p41 = new JPanel(new BorderLayout(5, 0));
        p41.add(_executeButton, BorderLayout.CENTER);
        p41.add(_executeButton1, BorderLayout.EAST);
        //p4.add(_executeButton, BorderLayout.EAST);
        p4.add(p41, BorderLayout.EAST);
        JPanel p5 = new JPanel(new BorderLayout(10, 10));
        p5.add(new JLabel("  Filter type:"), BorderLayout.WEST);
        //String[] types = {"All Files(*.*)"};
        //p5.add(new JComboBox(types));
        _filterCombo = new JComboBox(_filters);
        p5.add(_filterCombo);
        p5.add(b3, BorderLayout.EAST);

        if(fileChooserDescriptor.isDirectoryOnlyEnabled())
            setChoosableFileFilter(fileChooserDescriptor.getFileFilter(),0);
        else
            addChoosableFileFilter(fileChooserDescriptor.getFileFilter());

        JPanel panel = new JPanel(new BorderLayout(10, 10));
        panel.add(p2, BorderLayout.CENTER);
        panel.add(p1, BorderLayout.NORTH);

        Box panel2 = new Box(BoxLayout.Y_AXIS);
        panel2.add(Box.createVerticalStrut(10));
        panel2.add(p4);
        panel2.add(Box.createVerticalStrut(10));
        panel2.add(p5);
        panel2.add(Box.createVerticalStrut(10));
        if(agent.getDefaultIndicator()){
            JPanel p6 = new JPanel(new BorderLayout(10, 10));
            //add the check box to remember server selection as the default
            remember = new JCheckBox("remember as default");
            remember.addActionListener(new RememberListener());
            remember.setActionCommand("remember");
            remember.setEnabled(false);
            p6.add(remember, BorderLayout.WEST);
            panel2.add(p6);
            panel2.add(Box.createVerticalStrut(10));
        }

        this.getContentPane().add(panel, BorderLayout.CENTER);
        this.getContentPane().add(panel2, BorderLayout.SOUTH);

        this.pack();
        this.setSize(500, 350);
    }

    private class MyListMouseAdapter extends MouseAdapter {
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e) == false) return;

            if (e.getClickCount() < 2) return;
            if(remember != null)
                remember.setEnabled(false);
            JList source = (JList) e.getSource();
            int index = source.locationToIndex(e.getPoint());
            source.setSelectedIndex(index);

            Object selectedObj = source.getSelectedValue();
            if (selectedObj instanceof ListObject) {
                ListObject listObj = (ListObject) selectedObj;
                int type = listObj.getType();
                if ( type == _DIRECTORY) {
                    String oldPath = _pathField.getText();
                    ArrayList<String> list = agent.getRemoteOSInfo();
                    if(list != null){
                        //String osName = (String)list.get(0);
                        //if(osName.contains("Linux") || osName.contains("Unix")){
                            //if(System.getProperty("os.name").contains("Windows")){
                        if (!oldPath.endsWith((String)list.get(1))) oldPath = oldPath + (String)list.get(1);
                        //}
                        //}
                    } else
                        if (!oldPath.endsWith(File.separator)) oldPath = oldPath + File.separator;

                    setPathFieldText(oldPath + listObj.toString());
                    updateList();
                }
                else {
                    _nameField.setText((String)selectedObj.toString());
                    _returnValue = approveSelection(_nameField.getText());
                    if(_returnValue == JOptionPane.NO_OPTION)
                        return;
                    
                    fileChooserDescriptor.setSelectedFilePathBase(_pathField.getText().trim());
                    List<String> list = new ArrayList<String>();
                    list.add(_nameField.getText().trim());
                    fileChooserDescriptor.setSelectedFileNameList(list);
                    fileChooserDescriptor.setReturnCode(_returnValue);
                    agent.setQiFileChooserDescriptor(fileChooserDescriptor);
                    agent.notifyQiProducer();
                    hideDialog();
                }
            }
        }
    }

    //for multiSelectionOnly
    private class MyListMouseAdapter2 extends MouseAdapter {
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e) == false) return;

            //if (e.getClickCount() < 2) return;
            if(remember != null)
                remember.setEnabled(false);
            JList source = (JList) e.getSource();

            String listText = "";
            Object[] selectedObjs = source.getSelectedValues();
            for(int i = 0; i < selectedObjs.length; i++){
                if (selectedObjs[i] instanceof ListObject) {
                    ListObject listObj = (ListObject) selectedObjs[i];
                    int type = listObj.getType();
                    if ( type == _DIRECTORY) {
                        if (e.getClickCount() == 2){
                            //source.setSsetSelectedIndex(-1);
                            String oldPath = _pathField.getText();
                            ArrayList<String> list = agent.getRemoteOSInfo();
                            if(list != null){
                                if (!oldPath.endsWith((String)list.get(1))) oldPath = oldPath + (String)list.get(1);
                            } else
                                if (!oldPath.endsWith(File.separator)) oldPath = oldPath + File.separator;

                            setPathFieldText(oldPath + listObj.toString());
                            updateList();
                        }
                        continue;
                    } else {
                        int ret = approveSelection(((ListObject)selectedObjs[i])._name);
                        if(ret != JFileChooser.APPROVE_OPTION)
                            continue;
                        //listText += "\"" + (String)selectedObj.toString() + "\" ";
                        listText += (String)selectedObjs[i].toString();
                        if(i < selectedObjs.length - 1)
                            listText += ",";
                    }
                }
                _nameField.setText(listText);
                //agent.setReturnCode(_returnValue);
                //agent.setSelectedFilePath(getSelectedFileAbsolutePathname());
                //agent.setChooserType(chooserType);
                //agent.setRequestMsgCommand(requestMsgCommand);
                //agent.setDefaultIndicator(rememberAsDefault);
                //agent.notifyProducer();
                //hideDialog();

            }
        }
    }

    private class ExecuteListener1 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(list != null){
                GenericFileFilter filter = (GenericFileFilter) _filterCombo.getSelectedItem();
                if (filter.acceptDirectoryOnly() || fileChooserDescriptor.isDirectoryOnlyEnabled()) {
                    String nameFieldText = _nameField.getText();
                    if (nameFieldText.length() > 0) {
                        if (!fileChooserDescriptor.isCreateDirectoryEnabled() && !directoryExistsInList(nameFieldText)) {
                            showMessage("Cannot find the directory "+nameFieldText+" in " + _pathField.getText());
                            return;
                        }
                        agent.setReturnCode(JFileChooser.APPROVE_OPTION);
                        agent.setSelectedFilePath(getSelectedFileAbsolutePathname());
                        agent.setChooserType(chooserType);
                        agent.setDefaultIndicator(rememberAsDefault);
                        agent.setRequestMsgCommand(requestMsgCommand);
                        hideDialog();
                        agent.notifyProducer();
                    } else {
                        String message = "File name field is empty.";
                        showMessage(message);
                    }
                    return;
                }
            }
        }
    }

    private class ExecuteListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(list == null){
                return;
            }
            if (_enableDirectorySelection) {
                _returnValue = JFileChooser.APPROVE_OPTION;
                hideDialog();
                return;
            }

            GenericFileFilter filter = (GenericFileFilter) _filterCombo.getSelectedItem();

            Object selectedObj = _list.getSelectedValue();
            if (selectedObj == null) {
                String nameFieldText = _nameField.getText().trim();
                if(fileChooserDescriptor.getDialogType() == JFileChooser.OPEN_DIALOG){
                    if(nameFieldText.length() == 0){
                        String message = "File name field is empty.";
                        showMessage(message);
                        return;
                    }else{
                        if(getListObjectByName(nameFieldText) == null){
                            String message = nameFieldText + " does not exist in the " + _pathField.getText().trim();
                            showMessage(message);
                            return;
                        }
                    }
                }
                if (nameFieldText.length() > 0) {
                    if(filter.acceptDirectoryOnly() || fileChooserDescriptor.isDirectoryOnlyEnabled()){
                        if(!directoryExistsInList(nameFieldText)){
                            showMessage(nameFieldText + " does not exist in " + _pathField.getText());
                            return;
                        }else{
                            String oldPath = _pathField.getText();
                            ArrayList<String> list = agent.getRemoteOSInfo();
                            if(list != null){
                                if (!oldPath.endsWith((String)list.get(1))) oldPath = oldPath + (String)list.get(1);
                            }else
                                if (!oldPath.endsWith(File.separator)) oldPath = oldPath + File.separator;
                            setPathFieldText(oldPath + nameFieldText);
                            updateList();
                            return;
                        }
                    }else{
                        ListObject o = getListObjectByName(nameFieldText);
                        if(o != null && o.getType() == _DIRECTORY){
                            String oldPath = _pathField.getText();
                            ArrayList<String> list = agent.getRemoteOSInfo();
                            if(list != null){
                                if (!oldPath.endsWith((String)list.get(1))) oldPath = oldPath + (String)list.get(1);
                            }else
                                if (!oldPath.endsWith(File.separator)) oldPath = oldPath + File.separator;
                            setPathFieldText(oldPath + nameFieldText);
                            updateList();
                            return;
                        }
                        if(!nameFieldText.startsWith(defaultPreText))
                            nameFieldText = defaultPreText + "_" + nameFieldText;
                        if(nameFieldText.length() != 0 && !nameFieldText.endsWith(defaultExtension)  && !nameFieldText.endsWith(defaultExtension.toUpperCase())){
                            nameFieldText += defaultExtension;
                            _nameField.setText(nameFieldText);
                            repaint();
                        }
                        if (nameFieldText.length() > 0) {
                            _returnValue = approveSelection(nameFieldText);
                            if(_returnValue == JOptionPane.NO_OPTION)
                                return;
                            fileChooserDescriptor.setSelectedFilePathBase(_pathField.getText().trim());
                            List<String> list = new ArrayList<String>();
                            list.add(_nameField.getText().trim());
                            fileChooserDescriptor.setSelectedFileNameList(list);
                            fileChooserDescriptor.setReturnCode(_returnValue);
                            agent.setQiFileChooserDescriptor(fileChooserDescriptor);
                            agent.notifyQiProducer();
                            hideDialog();
                        }else{
                            String message = "File name field is empty.";
                            showMessage(message);
                        }
                    }
                }else{
                    String message = "File name field is empty.";
                    showMessage(message);
                }

            }
            else if (selectedObj instanceof ListObject) {
                ListObject listObj = (ListObject) selectedObj;
                int type = listObj.getType();
                if ( type == _DIRECTORY) {
                    String oldPath = _pathField.getText();
                    ArrayList<String> list = agent.getRemoteOSInfo();
                    if(list != null){
                        if (!oldPath.endsWith((String)list.get(1))) oldPath = oldPath + (String)list.get(1);
                    }else
                        if (!oldPath.endsWith(File.separator)) oldPath = oldPath + File.separator;
                    setPathFieldText(oldPath + listObj.toString());
                    updateList();
                }
                else {
                    String nameFieldText = _nameField.getText().trim();
                    if(!nameFieldText.equals(listObj._name)){
                        if(fileChooserDescriptor.getDialogType() == JFileChooser.OPEN_DIALOG && !fileExistsInList(nameFieldText)){
                            String message = "File name does not exist in the " + _pathField.getText().trim();
                            showMessage(message);
                            return;
                        }
                    }
                    if (nameFieldText.length() > 0) {
                        _returnValue = approveSelection(nameFieldText);
                        if(_returnValue == JOptionPane.NO_OPTION)
                            return;
                        fileChooserDescriptor.setSelectedFilePathBase(_pathField.getText().trim());
                        List<String> list = new ArrayList<String>();
                        list.add(_nameField.getText().trim());
                        fileChooserDescriptor.setSelectedFileNameList(list);
                        fileChooserDescriptor.setReturnCode(_returnValue);
                        agent.setQiFileChooserDescriptor(fileChooserDescriptor);
                        agent.notifyQiProducer();
                        hideDialog();
                    }else{
                        String message = "File name field is empty.";
                        showMessage(message);
                    }
                }
            }
            //agent.notifyProducer();
        }
    }

    private class ExecuteMultipleListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(list == null){
                return;
            }

            GenericFileFilter filter = (GenericFileFilter) _filterCombo.getSelectedItem();

            Object [] selectedObjs = _list.getSelectedValues();
            if (selectedObjs == null || selectedObjs.length == 0){
                if(_nameField.getText().trim().length() == 0){
                    String message = "File name field is empty.";
                    showMessage(message);
                    return;
                }else{
                    String nameFieldText = _nameField.getText().trim();
                    if(filter.acceptDirectoryOnly() || fileChooserDescriptor.isDirectoryOnlyEnabled()){
                        if(!directoryExistsInList(nameFieldText)){
                            String message = nameFieldText + " does not exist in the " + _pathField.getText().trim();
                            showMessage(message);
                            return;
                        }
                    }else{
                        if(!fileExistsInList(nameFieldText)){
                            String message = nameFieldText + " does not exist in the " + _pathField.getText().trim();
                            showMessage(message);
                            return;
                        }
                    }
                }
            }

            if(selectedObjs.length == 1){
                ListObject listObj = (ListObject) selectedObjs[0];
                int type = listObj.getType();
                if ( type == _DIRECTORY) {
                    String oldPath = _pathField.getText();
                    ArrayList<String> list = agent.getRemoteOSInfo();
                    if(list != null){
                        if (!oldPath.endsWith((String)list.get(1)))
                            oldPath = oldPath + (String)list.get(1);
                    }else if (!oldPath.endsWith(File.separator))
                        oldPath = oldPath + File.separator;
                    setPathFieldText(oldPath + listObj.toString());
                    updateList();
                    return;
                }else {
                    String nameFieldText = _nameField.getText().trim();
                    if(!nameFieldText.equals(listObj._name)){
                        if(!fileExistsInList(nameFieldText)){
                            String message = nameFieldText + " does not exist in the " + _pathField.getText().trim();
                            showMessage(message);
                            return;
                        }
                    }
                    _returnValue = JFileChooser.APPROVE_OPTION;
                    fileChooserDescriptor.setSelectedFilePathBase(_pathField.getText().trim());
                    List<String> list = new ArrayList<String>();
                    list.add(nameFieldText);
                    fileChooserDescriptor.setSelectedFileNameList(list);
                    fileChooserDescriptor.setReturnCode(_returnValue);
                    agent.setQiFileChooserDescriptor(fileChooserDescriptor);
                    agent.notifyQiProducer();
                    hideDialog();
                    return;
                }
            }
            List<String> list = new ArrayList<String>();
            for(int i = 0; i < selectedObjs.length; i++){
                ListObject lo = (ListObject)selectedObjs[i];
                int ret = approveSelection(lo._name);
                if(ret != JFileChooser.APPROVE_OPTION)
                    continue;
                list.add(lo._name);
            }
            _returnValue = JFileChooser.APPROVE_OPTION;
            fileChooserDescriptor.setSelectedFilePathBase(_pathField.getText().trim());
            fileChooserDescriptor.setSelectedFileNameList(list);
            fileChooserDescriptor.setReturnCode(_returnValue);
            agent.setQiFileChooserDescriptor(fileChooserDescriptor);
            agent.notifyQiProducer();
            hideDialog();
        }
    }

    private class CancelListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            _returnValue = JFileChooser.CANCEL_OPTION;
            fileChooserDescriptor.setReturnCode(_returnValue);
            agent.setQiFileChooserDescriptor(fileChooserDescriptor);
            agent.notifyQiProducer();
            hideDialog();
        }
    }

    private class RememberListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String cmd = e.getActionCommand();
            if (cmd.equals("remember")) {
                JCheckBox cb = (JCheckBox)e.getSource();
                rememberAsDefault = cb.isSelected() ? true : false;
            }
        }
    }

    private class UpListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String curDir = _pathField.getText();

            if(!naviagateUpward && curDir.equals(QiSpaceDescUtils.getQiSpacePath(agent.getMessagingMgr().getQispaceDesc())))
                return;
            //if(!naviagateUpward && curDir.equals(_homeDirectory))
            //    return;
            String upDir = curDir;

            ArrayList<String> list = agent.getRemoteOSInfo();
            String filesep = "";
            if(list != null)
                filesep = (String)list.get(1);
            else
                filesep = File.separator;
            if (curDir != null && curDir.lastIndexOf(filesep) >= 0)
                upDir = curDir.substring(0, curDir.lastIndexOf(filesep));
            if (upDir.length() == 0) {
                upDir = File.separator.equals("/") ? "/" : "C:\\";
            }
            setPathFieldText(upDir);
            updateList();
            if(remember != null)
                remember.setEnabled(false);
        }
    }

    public void setHomeDirectory(String dir){
        _homeDirectory = dir;
    }
    private class HomeListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            setPathFieldText(_homeDirectory);
            //setPathFieldText(agent.getMessagingMgr().getProject());
            updateList();
            if(remember != null)
                remember.setEnabled(false);
        }
    }

    private class ListObject {
        private int _type;
        private String _name;

        public ListObject(int type, String name) {
            _type = type;
            _name = name;
        }

        public int getType() { return _type; }

        public String toString() {
            return _name;
        }

        public ImageIcon getSymbolIcon() {
            if (_type == _DIRECTORY) return _dirIcon;
            else return _fileIcon;
        }
    }

    private class MyCellRenderer extends JLabel implements ListCellRenderer {
        public Component getListCellRendererComponent(JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {
            if (value == null) return this;
            String s = value.toString();
            setOpaque(true);
            setText(s);
            if (value instanceof ListObject)
                setIcon(((ListObject)value).getSymbolIcon());
            if (isSelected) setBackground(list.getSelectionBackground());
            else setBackground(list.getBackground());
            return this;
        }
    }

    private class MyPathKeyListener extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER){
                if(naviagateUpward == false){
                    String projectHome = agent.getMessagingMgr().getProject();
                    if(!_pathField.getText().startsWith(projectHome)){
                        JOptionPane.showMessageDialog(parent,
                                "Error in the Look in field: directory entered " +  _pathField.getText() + " is not on or above the project home directory (" + projectHome +").",
                                "QI Workbench", JOptionPane.WARNING_MESSAGE);
                        return;
                    }
                }
                updateList();
            }
        }
    }

    private class MyNameFieldKeyListener extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER){
                _executeButton.doClick();
                if(true)
                    return;
                String nameFieldText = _nameField.getText();
                if (nameFieldText != null && nameFieldText.trim().length() != 0) {
                    nameFieldText = nameFieldText.trim();
                    GenericFileFilter filter = (GenericFileFilter) _filterCombo.getSelectedItem();
                    if(filter.acceptDirectoryOnly() || fileChooserDescriptor.isDirectoryOnlyEnabled()){
                        if(!directoryExistsInList(nameFieldText)){
                            showMessage("Can not find this directory name from " + _pathField.getText());
                            return;
                        }else{
                            _executeButton.doClick();
                            return;
                        }
                    }else{
                        if(!nameFieldText.startsWith(defaultPreText))
                            nameFieldText = defaultPreText + "_" + nameFieldText;
                        if(nameFieldText.length() != 0 && !nameFieldText.endsWith(defaultExtension)  && !nameFieldText.endsWith(defaultExtension.toUpperCase())){
                            nameFieldText += defaultExtension;
                            _nameField.setText(nameFieldText);
                            repaint();
                        }
                        if (nameFieldText.length() > 0) {
                            _returnValue = approveSelection(nameFieldText);
                            if(_returnValue == JOptionPane.NO_OPTION)
                                return;
                            fileChooserDescriptor.setSelectedFilePathBase(_pathField.getText().trim());
                            List<String> list = new ArrayList<String>();
                            list.add(_nameField.getText().trim());
                            fileChooserDescriptor.setSelectedFileNameList(list);
                            fileChooserDescriptor.setReturnCode(_returnValue);
                            agent.setQiFileChooserDescriptor(fileChooserDescriptor);
                            agent.notifyQiProducer();
                            hideDialog();
                        }else{
                            String message = "File name field is empty.";
                            showMessage(message);
                        }
                    }
                }else{
                    String message = "File name field is empty.";
                    showMessage(message);
                }
            }
        }

        public void keyReleased(KeyEvent e) {

            if((e.getKeyCode() >= KeyEvent.VK_0 && e.getKeyCode() <= KeyEvent.VK_9)
                    || (e.getKeyCode() >= KeyEvent.VK_A && e.getKeyCode() <= KeyEvent.VK_Z)
                    || e.getKeyCode() == KeyEvent.VK_UNDERSCORE
                    || e.getKeyCode() == KeyEvent.VK_PERIOD
                    || e.getKeyCode() == KeyEvent.VK_BACK_SPACE
                    || e.getKeyCode() == KeyEvent.VK_DELETE){
                String nameFieldText = _nameField.getText();
                if(remember != null){
                    if (nameFieldText == null || nameFieldText.trim().length() == 0){
                        remember.setEnabled(false);
                    }else
                        remember.setEnabled(true);
                }
            }

        }
    }

}
