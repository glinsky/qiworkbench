/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.client.util;

import java.awt.Component;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.SwingUtilities;

import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.workbench.QiComponentBase;

/**
 * ErrorService is a core component of qiWorkbench. It is started as a thread
 * by the MessageDispatcher and waits for service requests. It calls ErrorDialog
 * to display error or warning messages that originated from any workbench component.
 * <p>
 * To use ErrorService:
 * <ul>
 *   <li>Issue request GET_ERROR_DIALOG_SERVICE_CMD to MessageDispather with a component
 *       descriptor of ErrorService returned from the service pool.
 *   <li>Issue a request INVOKE_ERROR_DIALOG_SERVICE_CMD to ErrorService to set the dialog visible.
 *   <li>Issue a request RETURN_ERROR_DIALOG_SERVICE_CMD to ErrorService to return the service to
 *   <li> the pool of available services.
 * </ul>
 * @author Bob Miller
 * @version 1.0
 */

public class ErrorService extends QiComponentBase implements IqiWorkbenchComponent, Runnable {

    // error dialog
    private ErrorDialog errorDialog = null;

    // copy of invoke message
    private IQiWorkbenchMsg invokeErrorDialogServiceMsg;

    // java logger
    private static final Logger logger = Logger.getLogger(ErrorService.class.getName());

    /** Messaging manager for service */
    private MessagingManager messagingMgr;

    // CID for component instance. Generated before the thread is started and carried as the thread's name
    private String myCID = "";

    /**
     * getCID returns the component ID for ErrorService
     * @return String CID
     */
    public String getCID() {
        return myCID;
    }

    /**
     * Initialize the service component:
     * <ul>
     * <li>Create its messaging manager</li>
     * <li>Register with the Message Dispatcher</li>
     * </ul>
     */
    public void init() {
        try {
            // lock out others while initializing
            // initialization: get MessagingManager and CID, register with MessageDispatcher
            QIWConstants.SYNC_LOCK.lock();

            messagingMgr = new MessagingManager();

            myCID = Thread.currentThread().getName();
            // register self with the Message Dispatcher
            messagingMgr.registerComponent(QIWConstants.ERROR_DIALOG_SERVICE_COMP,
                                       QIWConstants.ERROR_DIALOG_SERVICE_NAME,myCID);

            // notify the startup method initialization finished
            QIWConstants.SYNC_LOCK.unlock();
            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in ErrorService.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

    /** Initialize the service, then process received job requests.
     */
    public void run() {
        // initialize the job service
        init();

        // Process requests made on behalf of the Message Dispatcher
        // for another component. The dispatcher will route the response back
        // to the component. The dispatcher must be involved for it pools the
        // threads.
        while(true) {
            IQiWorkbenchMsg msg = messagingMgr.peekNextMsg();
            if(msg != null){
              msg = messagingMgr.getNextMsgWait();
                  processMsg(msg);
            }
        }
    }


    /**
     * Get the MessagingManager to handle the message passing.
     */
    public MessagingManager getMessagingMgr(){
        return messagingMgr;
    }

    /**
     * For a response, find the request matching the response and process the
     * response based on the request. Process a job request.
     */
    public void processMsg(IQiWorkbenchMsg msg) {
        /** Request that matches the response */
        IQiWorkbenchMsg request = null, response = null;

        //log message traffic
        logger.fine("client ErrorService::procssMsg: msg="+msg.toString());

        // Check if a response. If so, process and consume response
        if (messagingMgr.isResponseMsg(msg)) {
            request = messagingMgr.checkForMatchingRequest(msg);
            String cmd = request.getCommand();
            // check if from the message dispatcher
            if (messagingMgr.isResponseFromMsgDispatcher(msg)) {
                if (cmd.equals(QIWConstants.NULL_CMD))
                    return;
                else
                    logger.warning("Workbench Error Service message not processed:"+msg.toString());
            }
        }
        // message is a request
        else if (messagingMgr.isRequestMsg(msg)) {
            String cmd = msg.getCommand();
            if(cmd.equals(QIWConstants.INVOKE_ERROR_DIALOG_SERVICE_CMD)) {
              invokeErrorDialogServiceMsg = msg;
                ArrayList list = (ArrayList)msg.getContent();
                // save contents
                final Component parentGUI = (Component)list.get(0);
                final String messageType = (String)list.get(1);
                final String displayName = (String)list.get(2);
                final StackTraceElement[] stack = (StackTraceElement[])list.get(3);
                final String message = (String)list.get(4);
                final String[] causes = (String[])list.get(5);
                final String[] suggestions = (String[])list.get(6);

                final ErrorService service = this;
                Runnable updateAComponent = new Runnable() {
                    public void run() {
                        errorDialog = new ErrorDialog(parentGUI,messageType,
                                                                        displayName,stack,message,causes,
                                                                        suggestions,service);
                        int xx,yy;
                        if(parentGUI instanceof JInternalFrame){
                            Component comp = parentGUI.getParent();
                            //WorkbenchGUI is a kind of JFrame
                            while(comp != null && !(comp instanceof JFrame)){
                              comp = comp.getParent();
                              logger.info("awt component name = " + comp.getClass().getName());
                            }

                            if(comp != null){
                                xx = comp.getBounds().x + comp.getBounds().width/2;
                                yy = comp.getBounds().y + comp.getBounds().height/2;
                            } else {
                                xx = parentGUI.getBounds().x + parentGUI.getBounds().width/2;
                                yy = parentGUI.getBounds().y + parentGUI.getBounds().height/2;
                            }
                        } else{
                            xx = parentGUI.getBounds().x + parentGUI.getBounds().width/2;
                            yy = parentGUI.getBounds().y + parentGUI.getBounds().height/2;
                        }

                        final int x = xx - errorDialog.getBounds().width/2;
                        final int y = yy - errorDialog.getBounds().height/2;
                    }
                };
                SwingUtilities.invokeLater(updateAComponent);
                return;
            }
            else
                logger.warning("Error Service message not processed:"+msg.toString());
        }
    }


    /**
     * Notify the producer that error dialog has been dismissed
     */
    public void notifyProducer(){
        messagingMgr.sendResponse(invokeErrorDialogServiceMsg,QIWConstants.STRING_TYPE,"Ok");
        returnErrorService();
    }

    /**
     * Send a request to MessageDispatcher to return the current error service back to pool.
     */
    public void returnErrorService() {
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.RETURN_ERROR_DIALOG_SERVICE_CMD,
                                 ErrorService.class.getName(),this);
    }

    /** Launch the  error service:
     *  <ul>
     *  <li>Start up the service thread which will initialize the service.</li>
     *  </ul>
     * <p>
     * NOTE: Each thread's init() must finish before the next thread is
     * started. This is accompished by monitoring the SYNC_LOCK object.
     */
    public static void main(String[] args) {
        ErrorService ErrorServiceInstance = new ErrorService();
        // get a CID for the new component instance
        String cid = ComponentUtils.genCID(ErrorService.class);
        // use the CID as the name of the thread
        Thread serviceThread = new Thread(ErrorServiceInstance,cid);
        serviceThread.start();
        long threadId = serviceThread.getId();
        logger.info(" Error Service Thread-"+Long.toString(threadId)+" started");
        // When the services's init() is finished, it will release the lock

        // wait until the service's init() has finished
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();
    }

}
