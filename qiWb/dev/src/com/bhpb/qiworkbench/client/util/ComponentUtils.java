/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.client.util;

import java.lang.Class;
import java.util.Map;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.workbench.WorkbenchStateManager;

/**
 * Component utilities.
 *
 * @author Gil Hansen.
 * @version 1.0
 */
public class ComponentUtils {
    private static Logger logger = Logger.getLogger(ComponentUtils.class.getName());

    static long compVersion = 1, nameID = 1, jobID = 1;;
    /**
     * Generate a unique component ID (CID). The CID is the component's fully qualified name, i.e.,
     * package.class. A unique number is appended because there may be multiple versions
     * of a component running simultaneously.
     * <p>
     * For example, the CID of the message dispatcher is
     * com.bhpb.qiworkbench.messageFramework.MessageDispatcher#2
     *
     * @param comp qiWorkbench component that implements IqiWorkbenchComponent.
     * @return Unique CID
     */
    static public String genCID(IqiWorkbenchComponent comp) {
        String compName = comp.getClass().getName();

        return compName+"#"+compVersion++;
    }

    /**
     * Generate a base CID, i.e., one with no #n appended.
     *
     * @param comp qiWorkbench component that implements IqiWorkbenchComponent.
     * @return Unique CID
     */
    static public String genBaseCID(IqiWorkbenchComponent comp) {
        return comp.getClass().getName();
    }

    /**
     * Generate a unique component ID (CID) based on the component's class.
     *
     * @param klass Class that implements IqiWorkbenchComponent
     * @return Unique CID
     */
    static public String genCID(Class klass) {
        Class[] interfaces = klass.getInterfaces();
        if (interfaces[0].getName().indexOf("IqiWorkbenchComponent") == -1) {
            //log internal error
            logger.severe("Internal error::genCID(Class): Class not an instance of IqiWorkbenchComponent; class="+klass.getName());
        }
        return klass.getName()+"#"+compVersion++;
    }

    /**
     * Generate a base CID based on the component's class.
     *
     * @param klass Class that implements IqiWorkbenchComponent
     * @return Unique CID
     */
    static public String genBaseCID(Class klass) {
        Class[] interfaces = klass.getInterfaces();
        if (interfaces[0].getName().indexOf("IqiWorkbenchComponent") == -1) {
            //log internal error
            logger.severe("Internal error::genBaseCID(Class): Class not an instance of IqiWorkbenchComponent; class="+klass.getName());
        }
        return klass.getName();
    }

    /**
     * Generate a unique component ID (CID) based on the component's class name.
     * Mainly used when a component is activated, e.g., a plugin, and the class
     * name is carried in the manifest.
     * @param className Class name of a component that implements IqiWorkbenchComponent
     * @return Unique CID
     */
    static public String genCID(String className) {
        return className+"#"+compVersion++;
    }

    /**
     * Generate a full, unique CID given a base CID, i.e., append #n.
     * If the CID already contains #n, return the original argument.
     *
     * @param baseCID Base CID
     * @return Full, unique CID
     */
    static public String genFullCID(String baseCID) {
        if (baseCID.indexOf('#') != -1) return baseCID;
        return baseCID+"#"+compVersion++;
    }

    /**
     * Generate a unique display name given a base display name, i.e.,
     * append #n. If the name already contains #n, return the original argument.
     *
     * @param baseName Base display name
     * @return Unique display name
     */
    static public String genUniqueName(String baseName) {
        String name = "";
        if (baseName.indexOf('#') != -1)
            return baseName;
        Map map = WorkbenchStateManager.getInstance().getCurrentNodeMap();
        name = baseName+"#"+nameID++;
        while(map.containsKey(name)){
            logger.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Very rare situation caughted!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            name = baseName+"#"+nameID++;
        }
        return name;
    }

    /**
     * Generate a unique job ID.
     *
     * @return Unique job ID.
     */
    static public String genJobID() {
        return "jobProcess#"+jobID++;
    }
}
