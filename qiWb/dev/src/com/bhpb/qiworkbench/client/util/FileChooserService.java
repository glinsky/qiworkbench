/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.client.util;

import com.bhpb.qiworkbench.ComponentDescriptor;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.QiwbPreferences;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.DispatcherConnector;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
import com.bhpb.qiworkbench.workbench.QiComponentBase;
import java.awt.Dialog;
import java.awt.Frame;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * File Chooser service, a core component of qiWorkbench. It is started as a thread
 * by the MessageDispatcher and waits for service requests.
 * <p>
 * To use FileChooserService:
 * <ul>
 *   <li>Issue a request GET_FILE_CHOOSER_SERVICE_CMD to MessageDispather with a component descriptor of FileChooserService returned
 *       from the service pool.
 *   <li>Issue a request OPEN_FILE_CHOOSER_CMD to the FileChooserService component descriptor to lauch FileChooserDialog GUI component.
 * </ul>
 * @author L.T. Li
 * @version 1.0
 */

public class FileChooserService extends QiComponentBase implements IqiWorkbenchComponent, Runnable {
    private static Logger logger = Logger.getLogger(FileChooserService.class.getName());

    private static HashMap<String,String> currentDirMap = new HashMap<String,String>();

    /** Messaging manager for service */
    private MessagingManager messagingMgr;

    /** CID for component instance. Generated before the thread is started and carried as the thread's name. */
    private String myCID = "";

    //private boolean pleaseWait = false;
    private ArrayList<ArrayList> dirFileList = null;
    private FileChooserDialog fileChooserDialog = null;
    private QiFileChooserDialog fileChooserDialog2 = null;
    //returnCode: either JFileChooser.APPROVE_OPTION or JFileChooser.CANCEL_OPTION
    private int returnCode;
    private String selectedFilePath;
    private String chooserType;
    private String requestMsgCommand;
    private Component parentGUI;
    private IComponentDescriptor producerComponentDescriptor;
    //"remember as default" for some message commands such as RESTORE_DESKTOP_CMD but only available
    //if user wish to set a certain file name as a default file for a certain message command
    private boolean defaultIndicator = false;
    private ArrayList<String> remoteOSInfoList = null;

    private QiFileChooserDescriptor qiFileChooserDescriptor = new QiFileChooserDescriptor();

    private boolean isLocal = true;

    public String getCID() {
        return myCID;
    }

    public void setOwnerCurrentDirectory(String owner, String dir){
        currentDirMap.put(owner,dir);
    }

    public String getOwnerCurrentDirectory(String owner){
        return currentDirMap.get(owner);
    }

    public void setReturnCode(int code){
        this.returnCode = code;
    }

    public void setSelectedFilePath(String path){
        this.selectedFilePath = path;
    }

    public void setChooserType(String type){
        this.chooserType = type;
    }

    public void setRequestMsgCommand(String cmd){
        this.requestMsgCommand = cmd;
    }

    /**
     * Get information about the OS of the remote server.
     *
     * @return List of OS information, namely, name of OS, file separator.
     */
    public ArrayList<String> getRemoteOSInfo(){
        return remoteOSInfoList;
    }

    /** Runtime Tomcat server's OS */
    String serverOS = "";

    /** Runtime To mcat Server's root directory */
    String serverRootDir = "";

    /**
     * Initialize the service component:
     * <ul>
     * <li>Create its messaging manager</li>
     * <li>Register with the Message Dispatcher</li>
     * </ul>
     */
    public void init() {
        try {
            QIWConstants.SYNC_LOCK.lock();

            messagingMgr = new MessagingManager();

            myCID = Thread.currentThread().getName();
            // register self with the Message Dispatcher
            messagingMgr.registerComponent(QIWConstants.FILE_CHOOSER_SERVICE_COMP, QIWConstants.FILE_CHOOSER_SERVICE_NAME, myCID);

            if (remoteOSInfoList == null) {
                logger.info("In FileChooserService.init... remoteOSInfoList is null, invoking messagingMgr.getRemoteOSInfo()...");
                remoteOSInfoList = messagingMgr.getRemoteOSInfo();
                isLocal = messagingMgr.getLocationPref().equals(QIWConstants.LOCAL_SERVICE_PREF);
            
                // Determine the root directory of the runtime Tomcat server's OS
                if (isLocal) {
                    logger.info("FileChooserService is local, determining OS type...");
                    String homeDir = System.getenv("HOME"); // Unix latforms
                    logger.info("System.getenv(\"HOME\") = " + homeDir);
                    this.serverOS = (homeDir != null && homeDir.startsWith("/")) ? QIWConstants.UNIX_OS : QIWConstants.WINDOWS_OS;
                    if (this.serverOS == QIWConstants.UNIX_OS)
                        logger.info("OS type is UNIX");
                    else
                        logger.info("OS type is Windows");
                    serverRootDir = serverOS.equals(QIWConstants.UNIX_OS) ? "/" : "C:\\";
                
                } else {    //remote server
                    //The OS root directory is determine by the OS file separator
                    logger.info("FileChooserService is remote");
                    serverRootDir = (remoteOSInfoList != null && remoteOSInfoList.get(1).equals("/")) ? "/" : "C:\\";
                }
                logger.info("serverRootDir = " + serverRootDir);
            }
            
            // notify the startup method initialization finished
            QIWConstants.SYNC_LOCK.unlock();
            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in FileChooserService.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

    /** Initialize the service, then process received job requests.
     */
    public void run() {
        // initialize the job service
        init();

        // Process requests made on behalf of the Message Dispatcher
        // for another component. The dispatcher will route the response back
        // to the component. The dispatcher must be involved for it pools the
        // threads.
        while(true) {
            //while(true){
            //if(pleaseWait == false)
            //      break;
            //}
            IQiWorkbenchMsg msg = messagingMgr.peekNextMsg();
            //if a data message with skip flag set to true, it will not be processed here
            //instead it will be claimed by calling messingMgr.getMatchingResponseWait
            if(msg != null && !msg.skip()){
              msg = messagingMgr.getNextMsgWait();
              //if(msg != null)
                  processMsg(msg);
            }
        }
    }


    /**
     * Get the MessagingManager to handle the message passing.
     */
    public MessagingManager getMessagingMgr(){
        return messagingMgr;
    }

    public String getRequestMsgCommand(){
        return requestMsgCommand;
    }

    public boolean getDefaultIndicator(){
        if(requestMsgCommand != null){
            if(requestMsgCommand.equals(QIWConstants.RESTORE_DESKTOP_CMD)
                || requestMsgCommand.equals(QIWConstants.SAVE_COMP_CMD)
                || requestMsgCommand.equals(QIWConstants.SAVE_COMP_AS_CLONE_CMD)
                || requestMsgCommand.equals(QIWConstants.SAVE_COMP_THEN_QUIT_CMD)
                || requestMsgCommand.equals(QIWConstants.SAVE_DESKTOP_THEN_QUIT_CMD)
                || requestMsgCommand.equals(QIWConstants.SAVE_DESKTOP_CMD)){
                String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_USER_PREFERENCE_CMD,true);
                IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,5000);
                if (response == null || response.isAbnormalStatus()) {
                    if(response != null)
                        logger.severe((String)response.getContent());
                    else
                        logger.info("Response to get user preference returning null due to timed out problem. Please try again.");
                    return false;
                } else {
                    QiwbPreferences userPrefs = (QiwbPreferences)response.getContent();
                    String server = userPrefs.getDefaultServer();
                    if( server != null && server.trim().length() > 0){
                        String qispace = userPrefs.getDefaultQispace(server);
                        if (qispace != null && qispace.trim().length() > 0){
                            return true;
                        } else
                            return false;
                    } else
                        return false;
                }
                //|| requestMsgCommand.equals(QIWConstants.OPEN_SESSION_ACTION_BY_2D_VIEWER))
            }else
                return false;
        }
        return defaultIndicator;
    }

    public void setDefaultIndicator(boolean isDefault){
        defaultIndicator = isDefault;
    }

    private IQiWorkbenchMsg msgOfRequestForSelectedFile;

    /**
     * For a response, find the request matching the response and process the
     * response based on the request. Process a job request.
     */
    public void processMsg(IQiWorkbenchMsg msg) {

        /** Request that matches the response */
        IQiWorkbenchMsg request = null, response = null;

        //log message traffic
        logger.fine("client FileChooserService::procssMsg: msg="+msg.toString());

        // Check if a response. If so, process and consume response
        if (messagingMgr.isResponseMsg(msg)) {
            request = messagingMgr.checkForMatchingRequest(msg);
            String cmd = request.getCommand();
            // check if from the message dispatcher
            if (messagingMgr.isResponseFromMsgDispatcher(msg)) {
                if (cmd.equals(QIWConstants.NULL_CMD)) return;
            } else if (cmd.equals(QIWConstants.GET_LOCAL_DIR_FILE_LIST_CMD)){
                if(msg.isAbnormalStatus()){
                    JOptionPane.showMessageDialog(fileChooserDialog, "Error in getting local directory and file list. Cause: " + (String)msg.getContent(), "QI Workbench",
                            JOptionPane.WARNING_MESSAGE);
                    return;
                } else
                dirFileList = (ArrayList<ArrayList>)msg.getContent();
            } else if (cmd.equals(QIWConstants.GET_REMOTE_DIR_FILE_LIST_CMD)){
                if (msg.isAbnormalStatus()) {
                    JOptionPane.showMessageDialog(fileChooserDialog, "Error in getting remote directory and file list. Cause: " + (String)msg.getContent(), "QI Workbench",
                            JOptionPane.WARNING_MESSAGE);
                    return;
                } else
                    dirFileList = (ArrayList<ArrayList>)msg.getContent();
            }
            // TODO other possible responses...
        } else

        //Check if a request. If so, process and send back a response
        if (messagingMgr.isRequestMsg(msg)) {
            String cmd = msg.getCommand();
            //if (cmd.equals(QIWConstants.FIND_DIRECTORY_CMD)) {
            //
            //} else
            //
            if (cmd.equals(QIWConstants.INVOKE_FILE_CHOOSER_CMD)) {
                ArrayList list = (ArrayList)msg.getContent();
                //final Component parentGUI = (Component)list.get(0);
                parentGUI = (Component)list.get(0);
                final String title = (String)list.get(1);

                ArrayList lstTemp = (ArrayList)list.get(2);
                final String directory = (String)lstTemp.get(0);
                String useRemembered = (String)lstTemp.get(1);
                producerComponentDescriptor = (ComponentDescriptor)list.get(5);
                //final String owner = producerComponentDescriptor.getComponentKind();
                String sTemp = getCurrentDirOwner();

                final String owner = sTemp;
                //final String owner = producerComponentDescriptor.getDisplayName();

                final GenericFileFilter filter = (GenericFileFilter)list.get(3);

                String dirTemp = "";
                //If selecting just a directory means the request came from ProjectSelectorDialog.
                //Then there is no need to remember the directory previously accessed
                if (useRemembered.equals("no")) {
                //if(filter.acceptDirectoryOnly()){
                    if (directory != null && directory.trim().length() > 0)
                        dirTemp = directory;
                    else {
                        dirTemp = isLocal ? messagingMgr.getUserHOME() : serverRootDir;
                    }
                } else {
                    logger.info("currentDirMap in FileChooserService " + currentDirMap);
                    dirTemp = currentDirMap.get(owner);
                    if (dirTemp == null || dirTemp.trim().length() == 0) {
                        if (directory == null || directory.trim().length() == 0) {
                            dirTemp = isLocal ? messagingMgr.getUserHOME() : serverRootDir;
                        } else
                            dirTemp = directory;
                    }
                }

                /*
                if(directory != null && directory.trim().length() != 0)
                    dirTemp = directory;
                else{
                    dirTemp = currentDirMap.get(owner);
                    if(dirTemp == null || dirTemp.trim().length() == 0)
                        dirTemp = messagingMgr.getProject();
                }
                */

                final String dir = dirTemp;
                final String serverUrl =  (String)list.get(10);
                final ArrayList lst = getDirFileList(dir, serverUrl);
                if (lst == null) return;

                final Boolean nav = (Boolean)list.get(4);
                final String type = (String)list.get(6);
                final String command =  (String)list.get(7);
                requestMsgCommand = command;
                final String pretext =  (String)list.get(8);
                final String extension =  (String)list.get(9);

                final FileChooserService service = this;
                Runnable updateAComponent = new Runnable() {
                    public void run() {
                        
                        Component parentComponentRoot = SwingUtilities.getRoot(parentGUI);
                        //Bugfix for qiWorkbench lockup with modal but not-on-top FileChooserDialog
                        if (parentComponentRoot instanceof Dialog) {
                            fileChooserDialog = new FileChooserDialog((Dialog)parentComponentRoot, service, true);
                        } else if (parentComponentRoot instanceof Frame) {
                            fileChooserDialog = new FileChooserDialog((Frame)parentComponentRoot, service, true);
                        } else {
                            fileChooserDialog = new FileChooserDialog(parentGUI, service);
                        }
                        
                        fileChooserDialog.setDirectoryOwner(owner);
                        fileChooserDialog.setPathFieldText(dir);
                        fileChooserDialog.setHomeDirectory(dir);
                        if (filter.acceptDirectoryOnly())
                            fileChooserDialog.setChoosableFileFilter(filter,0);
                        else
                            fileChooserDialog.addChoosableFileFilter(filter);
                        fileChooserDialog.setUpwardNavigation(nav.booleanValue());
                        fileChooserDialog.setTitle(title);
                        fileChooserDialog.setDirFileList(lst);
                        fileChooserDialog.setChooserType(type);
                        fileChooserDialog.setRequestMsgCommand(command);
                        fileChooserDialog.setDefaultPreText(pretext);
                        fileChooserDialog.setDefaultExtension(extension);
                        fileChooserDialog.setServerUrl(serverUrl);
                        int xx,yy;
                        if (parentGUI instanceof JInternalFrame) {
                            Component comp = parentGUI.getParent();
                            //WorkbenchGUI is a kind of JFrame
                            while(comp != null && !(comp instanceof JFrame)){
                              comp = comp.getParent();
                              logger.info("awt component name = " + comp.getClass().getName());
                            }

                            if(comp != null){
                                xx = comp.getBounds().x + comp.getBounds().width/2;
                                yy = comp.getBounds().y + comp.getBounds().height/2;
                            } else {
                                xx = parentGUI.getBounds().x + parentGUI.getBounds().width/2;
                                yy = parentGUI.getBounds().y + parentGUI.getBounds().height/2;
                            }
                        } else{
                            xx = parentGUI.getBounds().x + parentGUI.getBounds().width/2;
                            yy = parentGUI.getBounds().y + parentGUI.getBounds().height/2;
                        }

                        final int x = xx - fileChooserDialog.getBounds().width/2;
                        final int y = yy - fileChooserDialog.getBounds().height/2;
                        fileChooserDialog.setLocation(x,y);
                        if (type.equals(QIWConstants.FILE_CHOOSER_TYPE_SAVE))
                            fileChooserDialog.showSaveDialog();
                        else if (type.equals(QIWConstants.FILE_CHOOSER_TYPE_OPEN))
                            fileChooserDialog.showOpenDialog();
                    }
                };
                SwingUtilities.invokeLater(updateAComponent);
                return;
            } else if (cmd.equals(QIWConstants.INVOKE_QI_FILE_CHOOSER_CMD)) {
                QiFileChooserDescriptor desc = (QiFileChooserDescriptor)msg.getContent();
                if(desc != null)
                	qiFileChooserDescriptor = desc;
                //final Component parentGUI = (Component)list.get(0);
                parentGUI = qiFileChooserDescriptor.getParentGUI();
                final String title = qiFileChooserDescriptor.getTitle();
                //final String directory = qiFileChooserDescriptor.getDirectoryRemembered();
                //directory where the chooser starts
                final String directory = qiFileChooserDescriptor.getHomeDirectory();
                boolean remembered = qiFileChooserDescriptor.isDirectoryRememberedEnabled();
                producerComponentDescriptor = qiFileChooserDescriptor.getProducerComponentDescriptor();
                //final String owner = producerComponentDescriptor.getComponentKind();
                String sTemp = getCurrentDirOwner();

                final String owner = sTemp;
                //final String owner = producerComponentDescriptor.getDisplayName();

                final GenericFileFilter filter = (GenericFileFilter)qiFileChooserDescriptor.getFileFilter();

                String dirTemp = "";
                //if selecting just a directory means the request came from ProjectSelectorDialog.
                //Then there is no need to remember the directory previously accessed
                if (!remembered) {
                //if(filter.acceptDirectoryOnly()){
                    if(directory != null && directory.trim().length() > 0)
                        dirTemp = directory;
                    else
                        dirTemp = messagingMgr.getUserHOME();
                } else {
                    logger.info("currentDirMap in FileChooserService " + currentDirMap);
                    dirTemp = currentDirMap.get(owner);
                    if (dirTemp == null || dirTemp.trim().length() == 0){
                        if(directory == null || directory.trim().length() == 0)
                            dirTemp = messagingMgr.getUserHOME();
                        else
                            dirTemp = directory;
                    }
                }

                final String dir = dirTemp;
                final String serverUrl =  qiFileChooserDescriptor.getServerUrl();
                final ArrayList lst = getDirFileList(dir,serverUrl);

                if(lst == null){
                    return;
                }

                String type = "";

                //final String command =  (String)list.get(7);
                //requestMsgCommand = command;
                //final String extension =  (String)list.get(9);
                final FileChooserService service = this;
                Runnable updateAComponent = new Runnable() {
                    public void run() {
                    	Component parentComponentRoot = SwingUtilities.getRoot(parentGUI);
                        //Bugfix for qiWorkbench lockup with modal but not-on-top FileChooserDialog
                        if (parentComponentRoot instanceof Dialog) {
                        	fileChooserDialog2 = new QiFileChooserDialog((Dialog)parentComponentRoot, service, true);
                        } else if (parentComponentRoot instanceof Frame) {
                        	fileChooserDialog2 = new QiFileChooserDialog((Frame)parentComponentRoot, service, true);
                        } 
                        fileChooserDialog2.setDirectoryOwner(owner);
                        fileChooserDialog2.setPathFieldText(dir);
                        //fileChooserDialog2.setHomeDirectory(dir);

                        fileChooserDialog2.setDirFileList(lst);
                        //fileChooserDialog.setRequestMsgCommand(command);
                        //fileChooserDialog.setDefaultExtension(extension);
                        int xx,yy;
                        if(parentGUI instanceof JInternalFrame){
                            Component comp = parentGUI.getParent();
                            //WorkbenchGUI is a kind of JFrame
                            while(comp != null && !(comp instanceof JFrame)){
                              comp = comp.getParent();
                              logger.info("awt component name = " + comp.getClass().getName());
                            }

                            if(comp != null){
                                xx = comp.getBounds().x + comp.getBounds().width/2;
                                yy = comp.getBounds().y + comp.getBounds().height/2;
                            } else {
                                xx = parentGUI.getBounds().x + parentGUI.getBounds().width/2;
                                yy = parentGUI.getBounds().y + parentGUI.getBounds().height/2;
                            }
                        } else{
                            xx = parentGUI.getBounds().x + parentGUI.getBounds().width/2;
                            yy = parentGUI.getBounds().y + parentGUI.getBounds().height/2;
                        }

                        final int x = xx - fileChooserDialog2.getBounds().width/2;
                        final int y = yy - fileChooserDialog2.getBounds().height/2;
                        fileChooserDialog2.setLocation(x,y);
                        if(qiFileChooserDescriptor.getDialogType() == javax.swing.JFileChooser.SAVE_DIALOG)
                            fileChooserDialog2.showSaveDialog();
                        else if(qiFileChooserDescriptor.getDialogType() == javax.swing.JFileChooser.OPEN_DIALOG)
                            fileChooserDialog2.showOpenDialog();
                    }
                };
                SwingUtilities.invokeLater(updateAComponent);
                return;
            } else if(cmd.equals(QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD) || cmd.equals(QIWConstants.GET_RESULT_FROM_QI_FILE_CHOOSER_CMD)) {
                msgOfRequestForSelectedFile = msg;
                return;
            }
        }
        logger.warning("File Chooser Service message not processed:"+msg.toString());
    }


    public QiFileChooserDescriptor getQiFileChooserDescriptor(){
    	return qiFileChooserDescriptor;
    }

    private String getCurrentDirOwner(){
        if(producerComponentDescriptor == null)
            return "";
        int ind = producerComponentDescriptor.getDisplayName().indexOf("#");

        String sTemp ="";
        if(ind == -1)
            sTemp = producerComponentDescriptor.getDisplayName();
        else
            sTemp = producerComponentDescriptor.getDisplayName().substring(0,ind);
        return sTemp;
    }

    public void notifyQiProducer(){
    	messagingMgr.sendResponse(msgOfRequestForSelectedFile,QiFileChooserDescriptor.class.getName(),qiFileChooserDescriptor);
    }

    /**
     * Notify the producer not to wait anymore because the user has made his descision so the producer can start receiving response
     */
    public void notifyProducer(){
        ArrayList list = new ArrayList();
        list.add(returnCode);
        list.add(selectedFilePath);
        list.add(chooserType);
        list.add(requestMsgCommand);
        list.add(defaultIndicator);
        list.add(parentGUI);
        messagingMgr.sendResponse(msgOfRequestForSelectedFile,QIWConstants.ARRAYLIST_TYPE,list);
    }

    public void setQiFileChooserDescriptor(QiFileChooserDescriptor desc){
    	qiFileChooserDescriptor = desc;
    }
    /**
     * Send a request to MessageDispatcher to return the current file chooser service back to pool.
     */
    public void returnFileChooserService(){
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.RETURN_FILE_CHOOSER_SERVICE_CMD,FileChooserService.class.getName(),this);
    }

    /**
     * Get the list of files and directories in the specified directory.
     * @param path The path of the directory
     * @return a list of directories and files
     */
    public ArrayList<ArrayList> getDirFileList(String path) {
        IQiWorkbenchMsg request = null;
        IQiWorkbenchMsg response = null;

        if (isLocal) {
            ArrayList<String> list = new ArrayList<String>();
            list.add(QIWConstants.LOCAL_SERVICE_PREF);
            list.add(path);

            String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_DIR_FILE_LIST_CMD,QIWConstants.ARRAYLIST_TYPE, list, true);
            response = messagingMgr.getMatchingResponseWait(msgId,5000);
            //response = messagingMgr.getNextMsgWait();
        } else {
            request = new QiWorkbenchMsg(messagingMgr.getMyComponentDesc().getCID(),
            messagingMgr.getComponentDescFromDisplayName(QIWConstants.SERVLET_DISPATCHER_NAME).getCID(),
                QIWConstants.CMD_MSG,
                QIWConstants.GET_REMOTE_DIR_FILE_LIST_CMD,
                MsgUtils.genMsgID(),
                QIWConstants.STRING_TYPE, path);
            response = DispatcherConnector.getInstance().sendRequestMsg(request);
        }

        ArrayList<ArrayList> list = null;

        if (response == null) {
            logger.warning("Returning null response. Check to see if this is a race condition");
            return dirFileList;
        }

        if (response.isAbnormalStatus()) {
            logger.warning("Error in running GET_DIR_FILE_LIST_CMD command. caused by " + (String)response.getContent());
            currentDirMap.remove(getCurrentDirOwner());
            logger.info("currentDirMap " + currentDirMap);
            JOptionPane.showMessageDialog(null, "Error in getting dirctory and file list from " + path + ". Cause: " + (String)response.getContent(), "QI Workbench",
                        JOptionPane.WARNING_MESSAGE);

            return null;
        }
        list = (ArrayList<ArrayList>)response.getContent();
        return list;
    }

    /**
     * Get the list of files and directories in the specified directory on the specified server.
     * @param path The path of the directory
     * @param serverUrl URL of the runtime Tomcat server
     * @return a list of directories and files
     */
    public ArrayList<ArrayList> getDirFileList(String path, String serverUrl){
        IQiWorkbenchMsg request = null;
        IQiWorkbenchMsg response = null;

        if (isLocal) {
            ArrayList<String> lst = new ArrayList<String>();
            lst.add(QIWConstants.LOCAL_SERVICE_PREF);
            lst.add(path);

            String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_DIR_FILE_LIST_CMD, QIWConstants.ARRAYLIST_TYPE, lst, true);
            response = messagingMgr.getMatchingResponseWait(msgId,5000);
        } else {
            messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.REGISTER_SELF_CMD);
            request = new QiWorkbenchMsg(messagingMgr.getMyComponentDesc().getCID(),
                messagingMgr.getComponentDescFromDisplayName(QIWConstants.SERVLET_DISPATCHER_NAME).getCID(),
                QIWConstants.CMD_MSG,
                QIWConstants.GET_REMOTE_DIR_FILE_LIST_CMD,
                MsgUtils.genMsgID(),
                QIWConstants.STRING_TYPE, path);
            response = DispatcherConnector.getInstance().sendRequestMsg(request, serverUrl);
        }

        ArrayList<ArrayList> list = null;

        if (response == null) {
            logger.warning("Returning null response. Check to see if this is a race condition");
            JOptionPane.showMessageDialog(null, "Probelm occurs in getting dirctory and file list from " + path + " under " + serverUrl + " possibly due to race condition. Please try again.", "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            MessageDispatcher.getInstance().resetMsgQueue();
            messagingMgr.resetMsgQueue();
            return dirFileList;
        }

        if (response.isAbnormalStatus()) {
            logger.warning("Error in running GET_DIR_FILE_LIST_CMD command. from " + path + " under " + serverUrl + ". Cause: " + (String)response.getContent());
            currentDirMap.remove(getCurrentDirOwner());
            JOptionPane.showMessageDialog(null, "Error in getting dirctory and file list from " + path + " under " + serverUrl + ". Cause: " + (String)response.getContent(), "QI Workbench",
                        JOptionPane.WARNING_MESSAGE);
            return null;
        }

        list = (ArrayList<ArrayList>)response.getContent();

        return list;
    }

    /** Launch the file choooser service:
     *  <ul>
     *  <li>Start up the service thread which will initialize the service.</li>
     *  </ul>
     * <p>
     * NOTE: Each thread's init() must finish before the next thread is
     * started. This is accompished by monitoring the SYNC_LOCK object.
     */
    public static void main(String[] args) {
        FileChooserService fileServiceInstance = new FileChooserService();
        // get a CID for the new component instance
        String cid = ComponentUtils.genCID(FileChooserService.class);
        // use the CID as the name of the thread
        Thread serviceThread = new Thread(fileServiceInstance, cid);
        serviceThread.start();
        long threadId = serviceThread.getId();
        logger.info("File Chooser Service Thread-"+Long.toString(threadId)+" started");
        // When the services's init() is finished, it will release the lock

        // wait until the plugin's init() has finished
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();
    }

}
