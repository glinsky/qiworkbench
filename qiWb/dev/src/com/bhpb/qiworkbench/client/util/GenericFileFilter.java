/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.qiworkbench.client.util;

import javax.swing.filechooser.*;
import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 *  A general purpose file filter. Usable by any JFileChooser
 *
 * @author     Ruby Varghese
 * @created    May 12, 2003
 * @version    1.0
 * @author Gil Hansen
 * @version 1.1
 */
public class GenericFileFilter extends javax.swing.filechooser.FileFilter implements FileFilter {
    private static Logger logger = Logger.getLogger(GenericFileFilter.class.getName());
    private String[] extensions;
    private String description;
    private boolean directoryOnly = false;

    /**
     *  Constructor for the GenericFileFilter object
     *
     * @param extensions List of file extensions to filter on.
     * @param description Description of the filter.
     */
    public GenericFileFilter(String[] extensions, String description) {
        this.extensions = extensions;
        this.description = description;

        cleanExtensions ();
    }

    public GenericFileFilter(String[] extensions, String description, boolean directoryOnly) {
        this.extensions = extensions;
        this.description = description;
        this.directoryOnly = directoryOnly;
        cleanExtensions ();
    }

    private void cleanExtensions () {
        //FIXME - this is temporary until all the generic file filters in their code get fixed
        for (int i=0;i<extensions.length;i++) {
            if (extensions[i].charAt(0) == '.' && extensions[i].length() > 2) {
                extensions[i] = extensions[i].substring(1);
            }
        }
    }

    /**
     * Gets the description attribute of the MyFileFilter object
     *
     * @return The description value
     */
    public String getDescription() {
        //    return nice description;
        return (this.description);
    }
     public String getNiceDescription() {
        //    return nice description;
        return (this.description + " (" + join(extensions, ", ") + ")");
    }

    private String join (String[] array, String c) {
        StringBuffer buf = new StringBuffer();
        for (int i=0; i < array.length; i++) {
            if (i != 0) buf.append (c);
            buf.append (array[i]);
        }
       return buf.toString();
    }

    public String toString() {
        return this.description;
    }

    public boolean acceptDirectoryOnly() {
        return directoryOnly;
    }

    public boolean acceptFilename(String filename) {
//        return matchesExtensionOf(filename, Arrays.asList(extensions));
        return matchesExtensionOf(filename);
    }

    /**
     * Check if file passes the filters.
     *
     * @param file File to check
     * @return true if file matches the extensions or is a directory; otherwise, false
     */
    public boolean accept(File file) {
//        return accept(file, Arrays.asList(extensions)) ;
        if (file.isDirectory()) return true;
        return file.isFile() && file.canRead() && matchesExtensionOf(file.getAbsolutePath());
    }

    /**
     *  Check whether a file matches an extension.
     *
     * @param file File to check.
     * @param ext File extension matching against.
     * @return true if file matchs extension; otherwise, false.
     * @deprecated Use matchesExtensionOf(filename) instead, for or extensions implicit input parameters.
     */
    private boolean matchesExtensionOf(String filename, java.util.List ext) {
        boolean matches = false;

        // Pick off extension from file
        String extension = getExtension(filename);
        // Compare extension with all accepted extensions
        matches = ext.contains(extension);
        return matches;
    }

    /**
     *  Check whether a file matches one of the extensions.
     *
     * @param filepath Full path of file to check.
     * @return true if file matches an extension; otherwise, false.
     */
    private boolean matchesExtensionOf(String filePath) {
        for (int i=0; i<extensions.length; i++) {
            String suffix = extensions[i];
            if (filePath.endsWith(suffix)) return true;
        }

        return false;
    }

    private String getExtension(String s) {
        int i = s.lastIndexOf('.');
        return (i>0 && i<s.length()-1) ? s.substring(i+1).toLowerCase().trim() : "";
    }

    /**
     * Check whether a file matches an extension.
     *
     * @param file File to check.
     * @param ext File extension matching against.
     * @return true if file matches extension or is a directory; otherwise, false.
     * @deprecated Use accept() instead, for extensions implicit input parameters.
     */
    private boolean accept(File file, java.util.List ext) {
        if (file.isDirectory()) return true;
        return file.isFile() && file.canRead() && matchesExtensionOf(file.getAbsolutePath(), ext);
    }
}
