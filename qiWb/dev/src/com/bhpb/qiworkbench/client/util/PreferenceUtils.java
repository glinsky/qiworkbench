/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.client.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.thoughtworks.xstream.XStream;

import com.bhpb.qiworkbench.QiwbPreferences;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.thoughtworks.xstream.converters.ConversionException;

import com.thoughtworks.xstream.security.AnyTypePermission;       // JE FIELD

/**
 * Utilities to read and write the preference file, qiwbPreferences.xml
 *
 * @author Gil Hansen.
 * @author Woody Folsom
 *
 * @version 1.0
 */
public class PreferenceUtils {
     private static Logger logger = Logger.getLogger(PreferenceUtils.class.getName());

    /**
     * Check if preference file exists.
     * Uses filename from QIWConstants.PREF_FILE_NAME
     * @param prefFileDir Directory containing preference file. Normally,
     * .qiworkbench under the user's home directory.
     *
     * @return true if preferences file exists; otherwise, false
     */
    static public boolean prefFileExists(String prefFileDir) {
        return prefFileExists(prefFileDir, QIWConstants.PREF_FILE_NAME);
    }

    /**
     * Check if preference file exists.
     *
     * @param prefFileDir Directory containing preference file. Normally,
     * .qiworkbench under the user's home directory.
     * @param prefFileName name of file containing qiWorkbench preferences
     *
     * @return true if preferences file exists; otherwise, false
     */
    static public boolean prefFileExists(String prefFileDir, String prefFileName) {
        File prefFile = new File (prefFileDir + File.separator + prefFileName);
        return prefFile.exists() && prefFile.isFile();
    }
    
    /**
     * Write serialized preference to preference file. File fill be
     * overwritten if it already exists.
     *
     * @param userPrefs The current set of preference values.
     * @param prefFileDir Directory to save the preference file.
     *
     */
    static public void writePrefs(QiwbPreferences userPrefs, String prefFileDir) throws QiwIOException {
        logger.fine(userPrefs.toString());

        // serialize preferences into XML
        XStream xstream = new XStream();
        
    // JE FIELD added remove all xstream security
    xstream.addPermission(AnyTypePermission.ANY);

        xstream.alias("userPreferences", QiwbPreferences.class);
        String serializedPrefs = xstream.toXML(userPrefs);
        logger.finest("serialized preferences:"+serializedPrefs);

        // write serialized XML to the preference file.
        File prefFile = new File (prefFileDir + File.separator + QIWConstants.PREF_FILE_NAME);
        BufferedWriter bw = null;

        try {
            FileOutputStream fos = new FileOutputStream(prefFile);
            bw = new BufferedWriter(new OutputStreamWriter(fos));
            //write out the serialized XML
            bw.write(serializedPrefs);
        } catch (IOException ioe) {
            if(ioe instanceof FileNotFoundException && ioe.getMessage().contains("Permission denied"))
                throw new QiwIOException("Insufficient privilege to write preference file.");
            else
            throw new QiwIOException("IO exception writing preference file:"+ioe.getMessage());
        } finally {
            try {
                if (bw != null) bw.close();
            } catch (IOException e) {}
        }
    }

    /**
     * Read serialized preference file and make an instance.
     * <p>
     * NOTE: Always create a new instance of the preferences for the user
     * may have edited them using Help | Preferences. The preference file
     * will contain the latest values.
     *
     * @param prefFileDir Directory containing the preference file.
     * @param prefFileName name of preference file, excluding path
     *
     * @return QiwbPreferences instance.
     */
    static public QiwbPreferences readPrefs(String prefFileDir, String prefFileName) throws QiwIOException {
         // read the serialized XML
        String prefFilePath = prefFileDir + File.separator + prefFileName;
        File prefFile = new File(prefFilePath);

        if (!prefFile.exists()) throw new QiwIOException("preference file does not exist; path="+prefFilePath);

        if (!prefFile.isFile()) throw new QiwIOException("preference file is not a file; path="+prefFilePath);

        BufferedReader br = null;
        ArrayList<String> fileLines = new ArrayList<String>();
        try {
            FileInputStream fis = new FileInputStream(prefFile);
            br = new BufferedReader(new InputStreamReader(fis));
            String line = br.readLine();
            // read file line by line
            while (line != null) {
                fileLines.add(line);
                line = br.readLine();
            }
        } catch (IOException ioe) {
            throw new QiwIOException("IO exception reading preference file:"+ioe.getMessage());
        } finally {
            try {
                if (br != null) br.close();
            } catch (IOException e) {}
        }

        // Combine list elements into a string
        String serializedPrefs = "";
        for (int i=0; i<fileLines.size(); i++) {
            serializedPrefs += fileLines.get(i);
        }
        logger.finest("serialized preference file:"+serializedPrefs);

        // deserialize XML into a message
        XStream xstream = new XStream();
        
    // JE FIELD added remove all xstream security
    xstream.addPermission(AnyTypePermission.ANY);

        xstream.alias("userPreferences", QiwbPreferences.class);
        
        QiwbPreferences userPrefs = null;
        
        try {
            userPrefs = (QiwbPreferences)xstream.fromXML(serializedPrefs);
            logger.fine(userPrefs.toString());
        } catch (com.thoughtworks.xstream.mapper.CannotResolveClassException crce) {// todo is there a superclass that could be caught instead?
            throw new QiwIOException("Exception while processing qiwbpreferences.xml.  File may have been saved from incompatible version of qiWorkbench. " + crce.getMessage());
/* JE FIELD commented out to avoid a problem with package "com.thoughtworks.xstream.alias does not exist"

        } catch (com.thoughtworks.xstream.alias.CannotResolveClassException crce) {// todo is there a superclass that could be caught instead?
            throw new QiwIOException("Exception while processing qiwbpreferences.xml.  File may have been saved from incompatible version of qiWorkbench. " + crce.getMessage());
* END JE FIELD COMMENT */
        } catch (ConversionException ce) {// todo is there a superclass that could be caught instead?
            throw new QiwIOException("Exception while processing qiwbpreferences.xml.  File may have been saved from incompatible version of qiWorkbench. " + ce.getMessage());
        }
        // This should never be possible unless xstream returns a null object without throwing an exception
        if (userPrefs == null)
            throw new QiwIOException("Unable to retrieve workbench preferences from file: " + prefFileDir + File.separator + prefFileName);
        
        return userPrefs;       
    }
    
    /**
     * Read serialized preference file and make an instance.
     * Gets filename from QIWConstants.PREF_FILE_NAME
     * <p>
     * NOTE: Always create a new instance of the preferences for the user
     * may have edited them using Help | Preferences. The preference file
     * will contain the latest values.
     *
     * @param prefFileDir Directory containing the preference file.
     * @return QiwbPreferences instance.
     */
    static public QiwbPreferences readPrefs(String prefFileDir) throws QiwIOException {
        return readPrefs(prefFileDir, QIWConstants.PREF_FILE_NAME);
    }
}
