/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiworkbench.client.util;

import java.nio.ByteBuffer;

import java.util.logging.Logger;

/**
 * Client-side channel utilities.
 *
 * @author Gil Hansen.
 * @version 1.0
 */
public class ChannelUtils {
    private static Logger logger = Logger.getLogger(ComponentUtils.class.getName());

    /**
     * Extend the capacity of a byte buffer. Creates a new byte buffer whose
     * capacity is the capacity of the old byte buffer + size, and whose
     * content is the content of the old byte buffer.
     * @param oldbuf Old byte buffer
     * @param size Capacity increase (in bytes)
     * @return New byte buffer whose content is a copy of the old byte buffer's content and whose capacity is the capacity of the old byte buffer + size.
     */
    static public ByteBuffer extendByteBuffer(ByteBuffer oldbuf, int size) {
        int oldCapacity = oldbuf.capacity();
        int newCapacity = oldCapacity + size;
        int oldPosition = oldbuf.position();

        //new buffer's position will be zero, its limit will be its capacity, and its mark will be undefined
        ByteBuffer newbuf = ByteBuffer.allocate(newCapacity);

        oldbuf.position(0);
        oldbuf.limit(oldPosition);
        //copy contents of old byte buffer into the new byte buffer
        newbuf.put(oldbuf);

        return newbuf;
    }
}
