/*
 *  Parse the configuration XML file and save the available information.
 */
package com.bhpb.qiworkbench.client;

import com.bhpb.qiworkbench.compAPI.XmlUtils;
import java.io.IOException;
import java.io.StringReader;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.InputSource;

/**
 * Parse the qiWorkbench configuration file and extract the properties for later
 * access.
 * @author Marcus Vaal
 * @author Gil Hansen
 * @version 1.2
 */
public class qiWbConfiguration implements Runnable {

    private static Logger logger = Logger.getLogger(qiWbConfiguration.class.getName());
    static final String DISTRIBUTION_NODE = "DISTRIBUTION";
    static final String PUC_NODE = "PUC";
    static final String STATS_NODE = "STATS";
    static final String UPDATE_CENTERS_NODE = "UPDATE_CENTER";
    static final String UPDATE_CENTER_NODE = "UPDATE_CENTERS";
    static final String RUNTIME_TOMCATS_NODE = "RUNTIME_TOMCATS";
    static final String RUNTIME_TOMCAT_NODE = "RUNTIME_TOMCAT";
    static final String BASE_DIRS_NODE = "BASE_DIRS";
    private static qiWbConfiguration singleton = null;
    private ByteOrder serverByteOrder = null;

    public static qiWbConfiguration getInstance() {
        if (singleton == null) {
            singleton = new qiWbConfiguration();
            new Thread(singleton).start();
        }
        return singleton;
    }

    /**
     * Empty Constructor
     */
    private qiWbConfiguration() {
    }

    /**
     * Reads and parses the configuration file and sets retrievable properties
     * @param configText
     */
    public void setConfigFile(String configText) throws ParserConfigurationException,
            SAXException, IOException {
        if (configText == null || configText.equals("")) {
            logger.severe("Unable to parse configText: text null or empty String.");
            return;
        }

        logger.info("Parsing qiWbconfig: " + configText);

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = factory.newDocumentBuilder();
            StringReader reader = new StringReader(configText);
            InputSource inputSource = new InputSource(reader);
            Document xmlDocument = parser.parse(inputSource);
            NodeList componentNodeList = xmlDocument.getElementsByTagName("configuration");
            Node n = componentNodeList.item(0);
            getConfigProps(n);
        } catch (ParserConfigurationException pce) {
            throw new ParserConfigurationException("Parser configuration exception setting up to parse qiWbconfig: " + pce.getMessage());
        } catch (SAXException se) {
            throw new SAXException("SAXException parsing qiWbconfig text: " + se.getMessage(), se);
        } catch (IOException ioe) {
            throw new IOException("IOException reading qiWbconfig text: " + ioe.getMessage());
        }
    }

    /**
     * Empty Initialization
     */
    private static void init() {
    }

    /**
     * Runs the singlton
     */
    public void run() {
        init();
    }

    /**
     * Get the attributes stored in the DISTRIBUTION node
     * @return Map of attributes in DISTRIBUTION node
     */
    public Map<String, String> getDistributionAttributes() {
        return distAttrs;
    }

    /**
     * Get the attributes stored in the PUC node
     * @return Map of attributes in PUC node
     */
    public Map<String, String> getPucAttributes() {
        return pucAttrs;
    }

    /**
     * Get the attributes stored in the STATS node
     * @return Map of attributes in STATS node
     */
    public Map<String, String> getStatsAttributes() {
        return statsAttrs;
    }

    /**
     * Get the attributes of the UPDATE_CENTERS node
     * @return Maps of attributes in Update Center node
     */
    public ArrayList<Map<String, String>> getUpdateCentersAttributes() {
        return updateCenterAttrs;
    }

    /**
     * Get the attributes of the RUNTIME_TOMCATS node
     * @return Maps of attributes  in Runtime Tomcat node
     */
    public ArrayList<Map<String, String>> getRuntimeTomcatAttributes() {
        return runtimeTomcatAttrs;
    }

    public ByteOrder getServerByteOrder() {
        return serverByteOrder;
    }

    /**
     * Get the attributes of the BASE_DIRS nodes
     * @return Maps of attributes in BASE_DIRS node
     */
    public Map<String, String> getBaseDirsAttributes() {
        return basedirsAttrs;
    }

    /**
     * Parse the qiWorkbench configuration attributes in the configuration file
     * located on the deployment server
     * @param node Root node in the XML config file
     */
    private void getConfigProps(Node node) {
        distAttrs = getDistAttrs(node);
        statsAttrs = getStatsAttrs(node);
        pucAttrs = getPUCAttrs(node);
        updateCenterAttrs = getUpdateCentersAttrs(node);
        runtimeTomcatAttrs = getTomcatAttrs(node);
        basedirsAttrs = getBaseDirsAttrs(node);
        serverByteOrder = getServerByteOrder(node);
    }

    /**
     * Returns the first node in the config file
     * @param node Root node
     * @param nodeName Name of the root node
     * @return First node in the config file
     */
    private Node getTagNameNode(Node node, String nodeName) {
        NodeList nodes = ((Element) node).getElementsByTagName(nodeName);
        Node n;
        if (nodes != null && nodes.getLength() > 0) {
            n = nodes.item(0); //Should only be one
        } else {
            //nodes not found
            n = null;
        }
        return n;
    }

    /**
     * Retrieves the attributes of a node
     * @param n Node containing the attributes
     * @return Map of the attributes of the given node
     */
    private Map<String, String> getAttributes(Node n) {
        Map<String, String> attrs = new HashMap<String, String>();
        for (int i = 0; i < n.getAttributes().getLength(); i++) {
            attrs.put(n.getAttributes().item(i).getNodeName(), n.getAttributes().item(i).getNodeValue());
        }
        return attrs;
    }

    /**
     * Gets the values of the DISTRIBUTION attributes
     * @param node DISTRIBUTION node
     * @return Map of the values contained in DISTRIBUTION node; empty list if no DISTRIBUTION node
     */
    private Map<String, String> getDistAttrs(Node node) {
        Map<String, String> attrs = new HashMap<String, String>();
        Node n = getTagNameNode(node, DISTRIBUTION_NODE);
        if (n != null) {
            attrs = getAttributes(n);
        }
        return attrs;
    }

    /**
     * Gets the values of the PUC attributes
     * @param node PUC node
     * @return Map of the values contained in PUC node; empty list if no PUC node
     */
    private Map<String, String> getPUCAttrs(Node node) {
        Map<String, String> attrs = new HashMap<String, String>();
        Node n = getTagNameNode(node, PUC_NODE);
        if (n != null) {
            attrs = getAttributes(n);
        }

        return attrs;
    }

    /**
     * Gets the values of the STATS attributes
     * @param node STATS node
     * @return Map of the values contained in STATS node; empty list if no STATS node
     */
    private Map<String, String> getStatsAttrs(Node node) {
        Map<String, String> attrs = new HashMap<String, String>();
        Node n = getTagNameNode(node, STATS_NODE);
        if (n != null) {
            attrs = getAttributes(n);
        }
        return attrs;
    }

    /**
     * Gets the values of the Update Center attributes
     * @param node Update Center Node
     * @return ArrayList of Maps cotained in the Update Centers Node; empty list
     * if no Update Center Node.
     */
    private ArrayList<Map<String, String>> getUpdateCentersAttrs(Node node) {
        ArrayList<Map<String, String>> allUpdateCenters = new ArrayList<Map<String, String>>();
        Node n = getTagNameNode(node, UPDATE_CENTERS_NODE);
        if (n == null) {
            return allUpdateCenters;
        }
        NodeList updateCenters = n.getChildNodes();
        for (int i = 0; i < updateCenters.getLength(); i++) {
            if (updateCenters.item(i).getNodeName().equals(UPDATE_CENTER_NODE)) {
                allUpdateCenters.add(getAttributes(updateCenters.item(i)));
            }
        }
        return allUpdateCenters;
    }

    /**
     * Gets the values of the Runtime Tomcats
     * @param node Runtime Tomcats node
     * @return ArrayList of Maps ontained in the Runtime Tomcats Node; empty list
     * if no Runtime Tomcats node
     */
    private ArrayList<Map<String, String>> getTomcatAttrs(Node node) {
        ArrayList<Map<String, String>> allRuntimeServers = new ArrayList<Map<String, String>>();
        Node n = getTagNameNode(node, RUNTIME_TOMCATS_NODE);
        if (n == null) {
            return allRuntimeServers;
        }
        NodeList runtimeServers = n.getChildNodes();
        for (int i = 0; i < runtimeServers.getLength(); i++) {
            if (runtimeServers.item(i).getNodeName().equals(RUNTIME_TOMCAT_NODE)) {
                allRuntimeServers.add(getAttributes(runtimeServers.item(i)));
            }
        }
        return allRuntimeServers;
    }

    /**
     * Gets the values of the BASE_DIRS attributes
     * @param node BASE_DIRS node
     * @return Map of the values contained in BASE_DIRS node; empty list if no BASE_DIRS node
     */
    private Map<String, String> getBaseDirsAttrs(Node node) {
        Map<String, String> attrs = new HashMap<String, String>();
        Node n = getTagNameNode(node, BASE_DIRS_NODE);
        if (n != null) {
            attrs = getAttributes(n);
        }
        return attrs;
    }

    private ByteOrder getServerByteOrder(Node node) {
        Node serverByteOrderNode = XmlUtils.getChild(node, "nativeServerByteOrder");
        String serverBOstring = serverByteOrderNode.getTextContent();

        if (ByteOrder.LITTLE_ENDIAN.toString().equals(serverBOstring)) {
            return ByteOrder.LITTLE_ENDIAN;
        } else if (ByteOrder.BIG_ENDIAN.toString().equals(serverBOstring)) {
            return ByteOrder.BIG_ENDIAN;
        } else {
            throw new IllegalArgumentException("Unable to parse invalid ByteOrder String: " + serverBOstring);
        }
    }
    //Variable Declarations
    Map<String, String> pucAttrs;
    Map<String, String> distAttrs;
    ArrayList<Map<String, String>> updateCenterAttrs;
    ArrayList<Map<String, String>> runtimeTomcatAttrs;
    Map<String, String> statsAttrs;
    Map<String, String> basedirsAttrs;
}
