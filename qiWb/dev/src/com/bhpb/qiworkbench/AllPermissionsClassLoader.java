/*
 * AllPermissionsClassLoader.java
 *
 * Created on August 7, 2006, 1:43 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author vaalm
 */
package com.bhpb.qiworkbench;

import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLStreamHandlerFactory;
import java.security.AllPermission;
import java.security.CodeSource;
import java.security.PermissionCollection;
import java.security.Permissions;
import java.util.logging.Logger;
 
public class AllPermissionsClassLoader extends URLClassLoader {
    private static final Logger logger = Logger.getLogger(AllPermissionsClassLoader.class.getName());
    public AllPermissionsClassLoader (URL[] urls) {
        super(urls);
    }
 
    public AllPermissionsClassLoader (URL[] urls, ClassLoader parent) {
        super(urls, parent);
        StringBuffer sbuf = new StringBuffer("Constructing AllPermissionsClassLoader for the following URLS: ");

        for (int i = 0; i < urls.length -1;i++) {
            sbuf.append(urls[i] + ", ");
        }

        if (urls.length > 0) {
            sbuf.append(urls[urls.length-1]);
        }

        logger.info(sbuf.toString());
    }
 
    public AllPermissionsClassLoader (URL[] urls, ClassLoader parent, URLStreamHandlerFactory factory) {
        super(urls, parent, factory);
    }
 
    protected PermissionCollection getPermissions (CodeSource codesource) {
        Permissions permissions = new Permissions();
        permissions.add(new AllPermission());
        return permissions;
    }
}

