package com.bhpb.model.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Title:        Config <br><br>
 * Description:  this class is used for configurable GUI<br><br>
 * 
 * @version 1.0
 */
public class Config implements Serializable {
	    private static final long serialVersionUID = 1L;
	    
	    List<NameComp> components = new ArrayList<NameComp>();
		
		public Config() {}

		public List<NameComp> getComponents() {
			return components;
		}

		public void setComponents(List<NameComp> components) {
			this.components = components;
		}
		
		public boolean addComponent(NameComp comp) {
			boolean ret = true;
			int size = components.size();
			String name = comp.getCompName();
			for (int i = 0; i < size; i++) {
				NameComp temp = components.get(i);
				if (temp.getCompName().equals(name)) {
					ret = false;
					break;
				}
			}
			if (ret) components.add(comp);
			
			return ret;
		}
		
		public boolean removeComponent(String name) {
			boolean ret = false;
			int size = components.size();
			for (int i = 0; i < size; i++) {
				NameComp temp = components.get(i);
				if (temp.getClass().equals(name)) {
					ret = true;
					components.remove(i);
					break;
				}
			}
			
			return ret;
		}
		
		public NameComp getComponent(String name) {
			NameComp ret = null;
			int size = components.size();
			for (int i = 0; i < size; i++) {
				NameComp temp = components.get(i);
				if (temp.getCompName().equals(name)) {
					ret = temp;
					break;
				}
			}
			
			return ret;
		}
		
		public Config defaultModel() {
			NameComp lite = new NameComp("lite");
			NameValues liteParam = new NameValues("params");
			NameValue  v1 = new NameValue("p_info", "info", "1");
			
			NameComp full = new NameComp("full");
			components.add(lite);
			components.add(full);
			return this;
		}
		
}