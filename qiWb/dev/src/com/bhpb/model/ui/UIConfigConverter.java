package com.bhpb.model.ui;

import java.util.List;

import com.bhpb.model.IConverter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class UIConfigConverter implements IConverter {
	 
	 public boolean canConvert(Class clazz) {
         return clazz.equals(Config.class);
     }

     public void marshal(Object value, HierarchicalStreamWriter writer,
                                       MarshallingContext context) {
    	 
    	 Config pModel = (Config)value; 
    	 writeModel(pModel, writer, context); 

     }
     
  
     private void writeModel(Config config, HierarchicalStreamWriter writer,
             MarshallingContext context) {
    	 
    	 List<NameComp> components = config.getComponents();
    	 int size = components.size();
    	 for (int i = 0; i < size; i++) {
    		 NameComp comp = components.get(i);
    		 String cname = comp.getCompName();
    		 writer.startNode("comp");
    		 writer.addAttribute("name", cname);
    		 List<NameValues> list = comp.getElements();
    		 int cnt = list.size();
    		 for (int j = 0; j < cnt; j++) {
    			 NameValues nvals = list.get(j);
    			 String tabname = nvals.getName();
    			 writeList(tabname, nvals.getNameList(), writer);
    			 
    		 }
    		 writer.endNode();
    	 }
    	
     }
     
     private void writeList(String tabName, List<NameValue> list, HierarchicalStreamWriter writer) {
    	 
    	 NameValue temp = null;
    	 writer.startNode("ele");
    	 
    	 writer.addAttribute("name", tabName);
    	 for (int i = 0; i < list.size(); i++) {  		 
    		 temp = list.get(i); 
    		 writer.startNode("panel");
    		 String attr = temp.getId();
    		 writer.addAttribute("id", attr);
    		 attr = temp.getDisplayName();
    		 writer.addAttribute("displayName", attr);
    		 attr = temp.getValue();
    		 writer.addAttribute("value", attr);
    		 writer.endNode();
    	 }
    	 writer.endNode();
    	 
     }

     
     public Object unmarshal(HierarchicalStreamReader reader,
                                    UnmarshallingContext context) {
    	 
    	 Config pModel = new Config(); 
    	 readModel(pModel, reader, context); 
         return pModel;
     }
	
     private void readModel(Config pModel,  HierarchicalStreamReader reader, 
                                            UnmarshallingContext context) {
    	  
    	 while (reader.hasMoreChildren()) {
    	    readComponent(pModel, reader, context);
         }
     }
     
     private void readComponent(Config pModel, HierarchicalStreamReader reader,
                                               UnmarshallingContext context) {
    	 NameComp     comp  = null;
    	 reader.moveDown();
    	 comp = new NameComp();
    	 String name = reader.getAttribute("name");
    	 comp.setCompName(name);
    	 while (reader.hasMoreChildren()) {
    		reader.moveDown();
    		NameValues vals = new NameValues();
    		String eleName = reader.getAttribute("name");
    		vals.setName(eleName);
    		while (reader.hasMoreChildren()) {
    			reader.moveDown();
    			String id    = reader.getAttribute("id");
    			String dname = reader.getAttribute("displayName");
    			String value = reader.getAttribute("value");
    			NameValue val = new NameValue(id, dname, value);
    			reader.moveUp();
    			vals.addNameValue(val);
    		}
    		reader.moveUp();
    		comp.addElement(vals);
    	}
    	 
    	reader.moveUp();
        pModel.addComponent(comp);
     }
}
