/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.model.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Title:        NameValues <br><br>
 * Description:  this class is used for configurable GUI<br><br>
 * 
 * @version 1.0
 */
public class NameValues implements Serializable {
	    private static final long serialVersionUID = 1L;
	    
	    String name;
	    List<NameValue>  nameList  = new ArrayList<NameValue>();
		
		public NameValues() {}
		public NameValues(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public List<NameValue> getNameList() {
			return nameList;
		}

		public void setNameList(List<NameValue> nameList) {
			this.nameList = nameList;
		}
		
		public boolean addNameValue(NameValue nameVal) {
			boolean ret = true;
			int size = nameList.size();
			String id = nameVal.getId();
			for (int i = 0; i < size; i++) {
				NameValue temp = nameList.get(i);
				if (temp.getId().equals(id)) {
					ret = false;
					break;
				}
			}
			if (ret) nameList.add(nameVal);
			
			return ret;
		}
		
		public NameValue getNameValue(String id) {
			NameValue ret = null;
			int size = nameList.size();
			for (int i = 0; i < size; i++) {
				NameValue temp = nameList.get(i);
				if (temp.getId().equals(id)) {
					ret = temp;
					break;
				}
			}
			
			return ret;
		}
}