/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.model.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Title:        NameComp <br><br>
 * Description:  this class is used for configurable GUI<br><br>
 * 
 * @version 1.0
 */
public class NameComp implements Serializable {
	    private static final long serialVersionUID = 1L;
	    
	    String compName;
	    List<NameValues>  elements  = new ArrayList<NameValues>();
		
		public NameComp() {}
		public NameComp(String compName) {
			this.compName = compName;
		}

		public String getCompName() {
			return compName;
		}

		public void setCompName(String compName) {
			this.compName = compName;
		}

		public List<NameValues> getElements() {
			return elements;
		}

		public void setElements(List<NameValues> elements) {
			this.elements = elements;
		}
		
		public boolean addElement(NameValues ele) {
			boolean ret = true;
			int size = elements.size();
			String name = ele.getName();
			for (int i = 0; i < size; i++) {
				NameValues temp = elements.get(i);
				if (temp.getName().equals(name)) {
					ret = false;
					break;
				}
			}
			if (ret) elements.add(ele);
			
			return ret;
		}
		
		public boolean removeElement(String name) {
			boolean ret = false;
			int size = elements.size();
			for (int i = 0; i < size; i++) {
				NameValues temp = elements.get(i);
				if (temp.getName().equals(name)) {
					ret = true;
					elements.remove(i);
					break;
				}
			}
			
			return ret;
		}
		
		public NameValues getElement(String name) {
			NameValues ret = null;
			int size = elements.size();
			for (int i = 0; i < size; i++) {
				NameValues temp = elements.get(i);
				if (temp.getName().equals(name)) {
					ret = temp;
					break;
				}
			}
			
			return ret;
		}
		
}