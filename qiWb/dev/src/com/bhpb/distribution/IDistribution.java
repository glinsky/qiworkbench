/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.distribution;

import java.io.IOException;
import java.io.File;
import java.util.List;

/**
 * A common interface that should be implemented by each component
 * Note: each component may hava variations of distribution file structure.
 * The external component must implement it accordingly
 *
 * @author Charlie Jiang
 * @version 1.0
 */
public interface IDistribution {
	
	public String getName();
	
	public void   bhpZip(String name, boolean zipBinary);
	public void   bhpUnZip(String name, String fullName);
	
	public String getDefaultDir();
	public void   makeNamedDir(String name);
	public String getDirByName(String type);
	
	public void   copyBinary() throws IOException;
	
	public void   copyFile(String source, String dest) throws IOException;
	public void   copyFile(File source, String dest) throws IOException;
	public void   copyFiles(String sourceDir, String destDir) throws IOException;
	public void   copyFiles(List<String> files, String type) throws IOException;
	
	public void   sendByEmail();
	public void   pushToServer();
	
	public String getProperty(String type);	
	public void setProperty(String type, String name);	
}
