<?xml version='1.0' encoding='ISO-8859-1' ?>
<!DOCTYPE helpset
  PUBLIC "-//Sun Microsystems Inc.//DTD JavaHelp HelpSet Version 2.0//EN"
         "http://java.sun.com/products/javahelp/helpset_2_0.dtd">

<helpset version="2.0">

  <!-- title -->
  <title>qiWorkbench - Help</title>

  <!-- maps -->
  <maps>
     <homeID>top</homeID>
     <mapref location="qiwb_map.jhm"/>
  </maps>

  <!-- views -->
  <view>
    <name>TOC</name>
    <label>TOC</label>
    <type>javax.help.TOCView</type>
    <data>qiwb_toc.xml</data>
  </view>

  <view>
    <name>Index</name>
    <label>Index</label>
    <type>javax.help.IndexView</type>
    <data>qiwb_index.xml</data>
  </view>

  <view>
    <name>Search</name>
    <label>Search</label>
    <type>javax.help.SearchView</type>
    <data engine="com.sun.java.help.search.DefaultSearchEngine">
      JavaHelpSearch
    </data>
  </view>

  <view>
    <name>Favorites</name>
    <label>Favorites</label>
    <type>javax.help.FavoritesView</type>
  </view>

  <presentation default="true" displayviewimages="false">
     <name>main window</name>
     <size width="700" height="600" />
     <location x="200" y="200" />
     <title>qiWorkbench - Online Help</title>
     <image>toplevelfolder</image>
     <toolbar>
       <helpaction>javax.help.BackAction</helpaction>
       <helpaction>javax.help.ForwardAction</helpaction>
       <helpaction>javax.help.SeparatorAction</helpaction>
       <helpaction>javax.help.HomeAction</helpaction>
       <helpaction>javax.help.ReloadAction</helpaction>
       <helpaction>javax.help.SeparatorAction</helpaction>
       <helpaction>javax.help.PrintAction</helpaction>
       <helpaction>javax.help.PrintSetupAction</helpaction>
     </toolbar>
  </presentation>
  
  <presentation displayviews=false>
     <name>blankQiwb</name>
     <size width="860" height="680" />
     <location x="200" y="200" />
     <title>qiWorkbench screenshot</title>
  </presentation>
  <presentation displayviews=false>
     <name>mainWebpage</name>
     <size width="670" height="1040" />
     <location x="100" y="0" />
     <title>qiWorkbench Webpage</title>
  </presentation>
  
  <presentation displayviews=false>
     <name>qiwbAmpExt</name>
     <size width="870" height="860" />
     <location x="200" y="150" />
     <title>Amplitude Extraction screenshot</title>
  </presentation>
  
  <presentation displayviews=false>
     <name>prefsDialog</name>
     <size width="600" height="360" />
     <location x="200" y="150" />
     <title>Preferences Dialog screenshot</title>
  </presentation>
  <presentation displayviews=false>
     <name>prefsServerMgr</name>
     <size width="520" height="490" />
     <location x="200" y="150" />
     <title>Server Manager screenshot</title>
  </presentation>
  <presentation displayviews=false>
     <name>prefsProjectMgr</name>
     <size width="570" height="635" />
     <location x="200" y="150" />
     <title>qiSpace Manager screenshot</title>
  </presentation>
  <presentation displayviews=false>
     <name>prefsSavesetMgr</name>
     <size width="600" height="605" />
     <location x="200" y="150" />
     <title>Workbench Manager screenshot</title>
  </presentation>
  
  <presentation displayviews=false>
     <name>selectorDialog</name>
     <size width="735" height="835" />
     <location x="100" y="50" />
     <title>Project Selector Dialog screenshot</title>
  </presentation>
  <presentation displayviews=false>
     <name>selectorServer</name>
     <size width="735" height="300" />
     <location x="200" y="150" />
     <title>Select Runtime Server screenshot</title>
  </presentation>
  <presentation displayviews=false>
     <name>selectorProject</name>
     <size width="735" height="300" />
     <location x="200" y="150" />
     <title>Select qiSpace/qiProject screenshot</title>
  </presentation>
  <presentation displayviews=false>
     <name>selectorSaveset</name>
     <size width="735" height="310" />
     <location x="200" y="150" />
     <title>Select Saveset screenshot</title>
  </presentation>
</helpset>
