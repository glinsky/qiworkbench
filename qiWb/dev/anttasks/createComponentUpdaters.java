package anttasks;

import java.util.ArrayList;

import java.util.Properties;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.jar.Manifest;
import java.util.jar.Attributes;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.util.jar.JarFile;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

public class createComponentUpdaters {
	
	public static void main(String[] args) {
		createUD();
	}
	
	private static void createUD() {
    	String timeStamp = "";
    	String distribution = "";
    	int downloadsize = -1;
    	String needsRestart = "true";
    	String license = ""; //Not Yet Implemented
    	String codenamebase = "";
    	String releaseDate = "";
    	String homePage = ""; //Not Yet Implemented
    	String moduleAuthor = "";
    	String qiwbComp = "";
    	String qiwbCompName = "";
    	String qiwbCompSpecVer = "";
    	String qiwbCompProvider = "";
    	String qiwbCompCompType = "";
    	String qiwbCompShortDisc = "";
    	String qiwbCompLongDisc = ""; //Not Yet Implemented
    	String qiwbCompCompDep = "";
    	String qiwbCompPackDep = ""; //Not Yet Implemented
    	String qiwbCompJNIDep = "";
    	String qiwbCompJavaDep = "";
    	String qiwbCompIDEDep = ""; //Not Yet Implemented
    	String qiwbCompTargetPlatform = "";
    	String licenseName = ""; //Not Yet Implemented
    	String licenseDesc = "";
	String qiwbCompWbDep = "";
    	String currentFilePath = null;
    	String cacheFilePath = null;
    	
    	Properties props = new Properties();
    	try {
    		props.load(new FileInputStream("./update.properties"));
    	} catch (IOException e) {
    	}
    	
        qiwbComp = props.getProperty("component.name");
    	timeStamp = props.getProperty("build.number");
    	qiwbCompJavaDep = props.getProperty("jre.required");
    	
        //This is a fix for the bug that caused the Update Descriptor (XML file) to lack the attribute QiWorkbench-Component-Java-Dependencies if
        //the update.properties file did not contain the 'jre.required' property
        if (qiwbCompJavaDep == null)
            qiwbCompJavaDep = "1.5.0";
            
        try {
            BufferedReader in = new BufferedReader(new FileReader("./README_LICENSE"));
            String str;
            while ((str = in.readLine()) != null) {
                licenseDesc = licenseDesc + tab + tab + str + "\n";
            }
            in.close();
        } catch (IOException e) {
        }
        
        try {
        	File currentJAR = new File("./dist/" + qiwbComp);
        	File manifestFile = new File("./src/META-INF/MANIFEST.MF");
	        InputStream FIS = new FileInputStream(manifestFile);
	        Manifest manifest = new Manifest(FIS);
	        FIS.close();
	        Attributes mainAttrs = manifest.getMainAttributes();
	        codenamebase = (String)mainAttrs.getValue(Attributes.Name.MAIN_CLASS);
	        Attributes attrs = manifest.getAttributes("attributes");
	        qiwbCompProvider = attrs.getValue("Provider");
	        qiwbCompSpecVer = attrs.getValue("Version");
	        qiwbCompCompType = attrs.getValue("Component-Type");
	        qiwbCompName = attrs.getValue("Display-Name");
	        qiwbCompShortDisc = attrs.getValue("Description");
	        qiwbCompCompDep = attrs.getValue("Dependent-Jars");
	        qiwbCompJNIDep = attrs.getValue("Dependent-JNI-Jars");
	        qiwbCompTargetPlatform = attrs.getValue("Target-Platform");
	        distribution = attrs.getValue("Download-URL");
		qiwbCompWbDep = attrs.getValue("Dependent-Workbench-Version");
	        
	        currentFilePath = "." + File.separator + "dist" + File.separator + qiwbCompName + "Update.xml";
	        cacheFilePath = getCompCachePath() + qiwbCompName + "Update.xml";
	        
            BufferedWriter out = new BufferedWriter(new FileWriter(currentFilePath));
            out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"); out.newLine();
            //out.write("<!DOCTYPE component PUBLIC \"-//qiWorkbench//DTD Update Component Info 1.0//EN\" \"http://localhost:8080/DTD/autoupdate-info-1_0.dtd\">"); out.newLine();
            //out.write("<component distribution=\"" + distribution + "\" downloadsize=\"" + currentJAR.length() + "\" ");
	    out.write("<component ");
	    if(timeStamp != null && !timeStamp.equals("")) out.write("timestamp=\"" + timeStamp + "\" ");
	    out.write("downloadsize=\"" + currentJAR.length() + "\" ");
            if(needsRestart != null && !needsRestart.equals("")) out.write("needsrestart=\"" + needsRestart + "\" ");
            if(license != null && !license.equals("")) out.write("license=\"" + license + "\" ");
            if(codenamebase != null && !codenamebase.equals("")) out.write("codenamebase=\"" + codenamebase + "\"");
            if(releaseDate != null && !releaseDate.equals("")) out.write(" releasedate=\"" + releaseDate + "\"");
            if(homePage != null && !homePage.equals("")) out.write(" homepage=\"" + homePage + "\"");
            if(moduleAuthor != null && !moduleAuthor.equals("")) out.write(" moduleauthor=\"" + moduleAuthor + "\"");
            out.write(">"); out.newLine();
            //End of component attributes
            //Manifest attributes
            out.write(tab + "<manifest ");
	    if(qiwbComp != null && !qiwbComp.equals("")) out.write("QiWorkbench-Component=\"" + qiwbComp + "\"");
            if(qiwbCompName != null && !qiwbCompName.equals("")) out.write(" QiWorkbench-Component-Name=\"" + qiwbCompName + "\"");
            if(qiwbCompSpecVer != null && !qiwbCompSpecVer.equals("")) out.write(" QiWorkbench-Component-Specification-Version=\"" + qiwbCompSpecVer + "\"");
	    if(qiwbCompWbDep != null && !qiwbCompWbDep.equals("")) out.write(" QiWorkbench-Component-Workbench-Version-Dependencies=\"" + qiwbCompWbDep + "\"");
            if(qiwbCompProvider != null && !qiwbCompProvider.equals("")) out.write(" QiWorkbench-Component-Provider=\"" + qiwbCompProvider + "\"");
            if(qiwbCompCompType != null && !qiwbCompCompType.equals("")) out.write(" QiWorkbench-Component-Component-Type=\"" + qiwbCompCompType + "\"");
            if(qiwbCompShortDisc != null && !qiwbCompShortDisc.equals("")) out.write(" QiWorkbench-Component-Short-Description=\"" + qiwbCompShortDisc + "\"");
            if(qiwbCompLongDisc != null && !qiwbCompLongDisc.equals("")) out.write(" QiWorkbench-Component-Long-Description=\"" + qiwbCompLongDisc + "\"");
            if(qiwbCompCompDep != null && !qiwbCompCompDep.equals("")) out.write(" QiWorkbench-Component-Component-Dependencies=\"" + qiwbCompCompDep + "\"");
            if(qiwbCompPackDep != null && !qiwbCompPackDep.equals("")) out.write(" QiWorkbench-Component-Package-Dependencies=\"" + qiwbCompPackDep + "\"");
            if(qiwbCompJNIDep != null && !qiwbCompJNIDep.equals("")) out.write(" QiWorkbench-Component-JNI-Dependencies=\"" + qiwbCompJNIDep + "\"");
            if(qiwbCompJavaDep != null && !qiwbCompJavaDep.equals("")) out.write(" QiWorkbench-Component-Java-Dependencies=\"" + qiwbCompJavaDep/*System.getProperty( "java.version" )*/ + "\"");
            if(qiwbCompIDEDep != null && !qiwbCompIDEDep.equals("")) out.write(" QiWorkbench-Component-IDE-Dependencies=\"" + qiwbCompIDEDep + "\"");
            if(qiwbCompTargetPlatform != null && !qiwbCompTargetPlatform.equals("")) out.write(" QiWorkbench-Component-Target-Platform=\"" + qiwbCompTargetPlatform + "\"");
            out.write("/>");
            out.newLine();
            out.write(tab + "<license name=\"" + licenseName + "\">");
            out.newLine();
            out.write(tab +tab + "<![CDATA[\n" + licenseDesc + tab + "]]>");
            out.newLine();
            out.write(tab + "</license>");
            out.newLine();
            out.write("</component>");
            out.newLine();
            out.close();
            //copy(currentFilePath,cacheFilePath);
        } catch (IOException e) {
        }
	}
	
	//NOTE: the following must be changed if the Cache Path of the workbench changes
	private static String getCompCachePath() {
		String userHOME = getAppDir(HOME_DIR);
		return userHOME+File.separator+".qiworkbench"+File.separator+"qicomponents"+File.separator;
	}
	
	//NOTE: the following must be changed if the Cache Path of the workbench changes
	private static String getAppDir(String name) {
		String retDir = "";
		String homeDir = System.getenv("HOME"); // Unix platforms
		if (homeDir == null || homeDir.equals("")) {
			homeDir = System.getenv("USERPROFILE"); // Windows platforms
		}
		if (homeDir != null) {
			// If the end-user is on a Windows platform and their HOME
			// directory is on the network, force their home directory to
			// be C:\Documents and Settings\%USERNAME% so qiwbPreferences.xml
			// will be written.
			// NOTE: On Unix/Linux, $HOME may be on th network, but this is
			// not an issue because it is NSF mounted.
			if (homeDir.startsWith("/") | homeDir.startsWith("C:")) {
				// do nothing, homeZDir id fine
			} else {
				String username = System.getenv("USERNAME");
				homeDir = "C:\\Documents and Settings\\" + username;
			}
		}

		if (HOME_DIR.equals(name)) {
			retDir = homeDir;
		} else if (QIWB_DIR.equals(name)) {
			retDir = homeDir + File.separator + ".qiworkbench";
		} else if (COMP_DIR.equals(name)) {
			retDir = homeDir + File.separator + ".qiworkbench"
					+ File.separator + "qicomponents";
		}
		return retDir;
	}
	
	private static void copy(String src, String dst) throws IOException {
		 BufferedReader in = new BufferedReader(new FileReader(src));
		 BufferedWriter out = new BufferedWriter(new FileWriter(dst));
         String str;
         while ((str = in.readLine()) != null) {
           	 out.write(str); out.newLine();
         }
         out.close();
         in.close();
	}
	
	private static String HOME_DIR = "home_dir";
	private static String QIWB_DIR = "qiwb_dir";
	private static String COMP_DIR = "comp_dir";
	private static String tab = "   ";
}
