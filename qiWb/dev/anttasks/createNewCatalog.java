package anttasks;

import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.File;
import java.util.Properties;
import java.io.IOException;

public class createNewCatalog {
	
	public static void main(String[] args) {
		String timeStamp = "";
		try {
			Properties props = new Properties();
    		props.load(new FileInputStream("./src/version.properties"));
	    	timeStamp = props.getProperty("project.build");
		} catch (IOException e){
		}		
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter("./UpdateCatalog.xml"));
            out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"); out.newLine();
            out.write("<!DOCTYPE component PUBLIC \"-//qiWorkbench//DTD Update Catalog 1.0//EN\" \"http://localhost:8080/DTD/autoupdate-catalog-1_0.dtd\">"); out.newLine();
			out.write("<component_updates timestamp=\"" + timeStamp + "\">"); out.newLine();
			out.write(getUpdateDescriptors());
			out.write("</component_updates>");
			out.close();
		} catch (IOException e){
		}
	} 
	
	private static String getUpdateDescriptors() {
		String updateDescriptor = "";
		String compCachePath = getCompCachePath();
		int j;
		File compCacheDir = new File(compCachePath);
		if (!compCacheDir.isDirectory()) return "";
		File[] files = compCacheDir.listFiles();
		for (int i=0; i<files.length; i++) {
			j = 1;
			if (!files[i].isDirectory()) {
				String fileName = files[i].getName();
				if (fileName.endsWith(".xml")) {
					try {
			            BufferedReader in = new BufferedReader(new FileReader(compCachePath+fileName));
			            String str;
			            while ((str = in.readLine()) != null) {
			            	if( j > 2 ) {
			            		updateDescriptor = updateDescriptor + tab + str + "\n";
			            	}
			            	else {
			            		j++;
			            	}
			            }
			            in.close();
					} catch (IOException ioe) {
					}
				}
			}
		}
		return updateDescriptor;
	}
	
	//NOTE: the following must be changed if the Cache Path of the workbench changes
	private static String getCompCachePath() {
		userHOME = getAppDir(HOME_DIR);
		return userHOME+File.separator+".qiworkbench"+File.separator+"qicomponents"+File.separator;
	}
	
	//NOTE: the following must be changed if the Cache Path of the workbench changes
	private static String getAppDir(String name) {
		String retDir = "";
		String homeDir = System.getenv("HOME"); // Unix platforms
		if (homeDir == null || homeDir.equals("")) {
			homeDir = System.getenv("USERPROFILE"); // Windows platforms
		}
		if (homeDir != null) {
			if (homeDir.startsWith("/") | homeDir.startsWith("C:")) {
				// do nothing, homeZDir id fine
			} else {
				String username = System.getenv("USERNAME");
				homeDir = "C:\\Documents and Settings\\" + username;
			}
		}

		if (HOME_DIR.equals(name)) {
			retDir = homeDir;
		} else if (QIWB_DIR.equals(name)) {
			retDir = homeDir + File.separator + ".qiworkbench";
		} else if (COMP_DIR.equals(name)) {
			retDir = homeDir + File.separator + ".qiworkbench"
					+ File.separator + "qicomponents";
		}
		return retDir;
	}
	
	private static String HOME_DIR = "home_dir";
	private static String QIWB_DIR = "qiwb_dir";
	private static String COMP_DIR = "comp_dir";
	private static String userHOME = "";
	
	private static String tab = "   ";
}