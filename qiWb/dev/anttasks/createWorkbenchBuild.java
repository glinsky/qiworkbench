/*
###########################################################################
# qiWorkbench - an extensible platform for seismic interpretation
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package anttasks;

import java.util.Properties;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.jar.Manifest;
import java.util.jar.Attributes;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.JarURLConnection;
import java.net.MalformedURLException;

/**
 *
 * @author Marcus Vaal
 * @version 1.0
 */
public class createWorkbenchBuild {

    /**
     * @param args
     */
    public static void main(String[] args) {
        String[] ids = TimeZone.getAvailableIDs(-6 * 60 * 60 * 1000);
         // if no ids were returned, something is wrong. get out.
         if (ids.length == 0)
             System.exit(0);

         // create a Pacific Standard Time time zone
         SimpleTimeZone pdt = new SimpleTimeZone(-6 * 60 * 60 * 1000, ids[0]);

         // set up rules for daylight savings time
         pdt.setStartRule(Calendar.APRIL, 1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
         pdt.setEndRule(Calendar.OCTOBER, -1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);

         // create a GregorianCalendar with the Pacific Daylight time zone
         // and the current date and time
         Calendar calendar = new GregorianCalendar(pdt);
         Date trialTime = new Date();
         calendar.setTime(trialTime);

         String currentYear = "" + calendar.get(Calendar.YEAR);
         String currentMonth = addZero(calendar.get(Calendar.MONTH) + 1);
         String currentDate = addZero(calendar.get(Calendar.DATE));
         String currentHour = addZero(calendar.get(Calendar.HOUR_OF_DAY));
         String currentMinute = addZero(calendar.get(Calendar.MINUTE));

         buildNumber = currentYear + currentMonth +
            currentDate + "-" + currentHour + currentMinute;

        try {
            BufferedWriter out = new BufferedWriter(new FileWriter("./src/version.properties"));
            out.write("project.version=" + getVersion());
            out.newLine();
            out.write("project.build=" + getBuild());
            out.newLine();
            out.write("project.provider=" + getProvider());
            out.close();
        } catch (IOException e) {
        }
    }

    //Adds a 0 if the parameter's length is less than 2
    private static String addZero(int calendarParam) {
        String calendarReturn = "" + calendarParam;
        if(calendarReturn.length() < 2)
             calendarReturn = "0" + calendarReturn;
        return calendarReturn;
    }

    //Finds and returns the version number out of the qiWorkbench manifest file
    /*private static String getNewVersion() throws IOException, MalformedURLException{
        String versionNumber = "";
        File manifestFile = new File("./web/META-INF/MANIFEST.MF");
        InputStream FIS = new FileInputStream(manifestFile);
        Manifest manifest = new Manifest(FIS);
        FIS.close();
        Attributes attrs = manifest.getAttributes("attributes");
        versionNumber = attrs.getValue("Version");
        return versionNumber;
    }*/

    public static String getVersion() throws IOException{
        return getProps("Version");
    }

    public static String getProvider() throws IOException{
        return getProps("Provider");
    }

    public static String getBuild() {
        return buildNumber;
    }

    private static String getProps(String propParam) throws IOException, MalformedURLException{
        File manifestFile = new File("./web/META-INF/MANIFEST.MF");
	if(!manifestFile.isFile()) {
		manifestFile = new File("./src/META-INF/MANIFEST.MF");
	}
        InputStream FIS = new FileInputStream(manifestFile);
        Manifest manifest = new Manifest(FIS);
        FIS.close();
        Attributes attrs = manifest.getAttributes("attributes");
        //versionNumber = attrs.getValue("Version");
        //providerName = attrs.getValue("Provider");
        return attrs.getValue(propParam);
    }

    private static String versionNumber = null;
    private static String providerName = null;
    private static String buildNumber = null;
}
