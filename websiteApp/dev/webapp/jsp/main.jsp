<%@ include file="/jsp/include.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

  <title>qiWorkbench&trade; &gt; Home</title>

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

  <meta name="description" content="qiWorkbench is an open source, web based, working environment." />

  <meta name="keywords" content="qiWorkbench, qi Workbench, open source, web based, environment, qiComponent, workbench" />

  <script type="text/javascript" src="scripts/navigation.js"></script>
  <script type="text/javascript" src="scripts/validation.js"></script>
  
  <style type="text/css" media="all">@import url(css/home.css);</style>

</head>

<body onload="mbSet('menu', 'mb'); document.getElementById('menu').style.display = 'block';" id="bodyhome">

<div id="centre">
    <div id="header">
<%@ include file="shared/menus/commonmenus.html"%>
    </div>

    <div id="container">
        <div id="containera">
            <br/>
            <img src="images/qiWorkbenchLogo.gif" alt="qiWorkbench logo" height="124" width="542" />

            <br/>

            <blockquote>
              <p>Welcome to the site for the qiWorkbench&trade;. It is an
            open source, web based, working environment that you can take with you
            whereever you go. It is meant to be your qi (pronounced "chi" in
            Chinese) -- your life force.</p>
            </blockquote>

            <!-- Same font as <p>, but without the leading space. -->
            <dl>The content of this workbench is contained in its plug-in
            components called qiComponents&trade;. These qiComponents can
            communicate with each other, thereby allowing a rich behavior of 
            the qiComponent community on the qiWorkbench. It was originally designed by BHP Billiton to implement its integrated workflows to 
            process, analyze and view seismic data. Its flexibility and 
            generality, though, allow it to be anyone's working environment.
            </dl>

            <p>The qiWorkbench has been developed in Java and is available under a 
            <a href="dev/licenses/qiWorkbench/qiwb_license.html">GPL v2</a>
            license. The interface of the 
            qiComponents to the qiWorkbench as well
            as some example qiComponents are made available under an academic <a href="http://www.opensource.org/licenses/bsd-license.php">BSD</a>
            license, meant to isolate and protect commercial qiComponents (for
            profit) that we encourage to be developed.</p>

            <p>This website is made available for the development of 3rd Party open
            source qiComponents. It is a full featured development site including
            a Wiki, Forum and Subversion source code maintenance facilities.</p>

            <div id="columna">
                <div id="part">
                    <h3>Corporate Partners</h3>

                    <ul>
                      <li><a target="_blank" href="http://www.bhpbilliton.com/">BHP
                    Billiton</a></li>

                      <li><a target="_blank" href="http://www.g-w-systems.com/">G&amp;W
                    Systems</a></li>

                      <li><a target="_blank" href="http://www.int.com/" class="style1">Interactive Network
                    Technologies</a></li>

                    </ul>
                </div>

                <h3>Academic Partners</h3>

                <ul>
                  <li><a target="_blank" href="http://www.mines.edu/">Colorado
                School of Mines</a></li>
                  <li><a target="_blank" href="http://www.dpr.csiro.au/">CSIRO
                Petroleum</a></li>
                  <li><a target="_blank" href="http://engineering.ucsb.edu/">UC Santa Barbara Engineering</a></li>
                </ul>
            </div>

            <div id="columnb">
                <div id="news">
                    <h3>News</h3>
                    <dl>
                      <dt class="date">1 July 2006</dt>
                      <dd>qiworkbench.org domain registered and online</dd>
                      <dt></dt>
                      <dd></dd>
                      <dt class="date">27 September 2006</dt>
                      <dd>First official release of the qiWorkbench</dd>
                      <dt></dt>
                      <dd></dd>
                      <dt class="date">8 March 2007</dt>
                      <dd>Production release of qiWorkbench v1.1</dd>
                      <dt></dt>
                      <dd></dd>
                      <dt class="date">5 July 2007</dt>
                      <dd>Release of Numerical Turbidite Simulation codes</dd>
                      <dt></dt>
                      <dd></dd>
                      <dt class="date">15 August 2007</dt>
                      <dd>Production release of qiWorkbench v1.2</dd>
                      <dt></dt>
                      <dd></dd>
                      <dt class="date">11 December 2007</dt>
                      <dd>Production release of qiWorkbench v1.3</dd>
                      <dt></dt>
                      <dd></dd>
                      <dt class="date">7 March 2008</dt>
                      <dd>Production release of qiWorkbench v1.3.1</dd>
                      <dt></dt>
                      <dd></dd>
                      <dt class="date">24 April 2009</dt>
                      <dd>Production release of qiWorkbench v1.4</dd>
                      <dt></dt>
                      <dd></dd>
                      <dt class="date">16 November 2011</dt>
                      <dd>Release of qiWorkbench v1.5 @ qiWorkbench.biz and source moved to BitBucket</dd>
                    </dl>
                </div>
            </div>
        </div>

        <div id="columnc">
            <div id="oursites">
                <h4>Related Sites</h4>

                <form id="siteLaunch" action="">
                  <fieldset>
                  <select onchange="if(this.options[this.selectedIndex].value != '0') { window.open(this.options[this.selectedIndex].value); }">
                  <option value="0" selected="selected">Select one</option>
                  <option value="http://www.bhpbilliton.com">BHP Billiton</option>
                  <option value="http://www.cwp.mines.edu/cwpcodes/BHP_SU/">BHP SU</option>
                  <option value="http://www.mines.edu">Colorado School of Mines</option>
                  <option value="http://www.dpr.csiro.au/">CSIRO Petroleum</option>
                  <option value="http://www.petroleum.csiro.au/ourcapabilities/petroleumgeoengineering/mathematicalmodelling/projects/deliveryanopensourceseismicinversionsoftware/">DELIVERY</option>
                  <option value="http://www.g-w-systems.com/">G&amp;W Systems</option>
                  <option value="http://www.int.com/">Interactive Network Technologies</option>
                  <option value="http://www.qitech.biz">Michael Glinsky's Homepage</option>
                  <option value="http://www.cwp.mines.edu/cwpcodes/">Seismic Un*x</option>
                  <option value="http://engineering.ucsb.edu/">UC Santa Barbara Engineering</option>
                  </select>

                  </fieldset>
                </form>
            </div>
        </div>

        <div id="tm">
            &nbsp;&nbsp;&nbsp;&nbsp;
            <img border="1" src="http://www.opensource.org/trademarks/opensource/web/opensource-55x48.png"  alt="Open Source (OSI) Logo" width="55" height="48"/>
            &nbsp;&nbsp;
            <img border="1" src="http://www.opensource.org/trademarks/osi-certified/web/osi-certified-60x50.png"  alt="Open Source (OSI) Trademark" width="60" height="50"/>
        </div>
    </div>

<%@ include file="shared/menus/footer.html"%>

</div>

</body>
</html>
