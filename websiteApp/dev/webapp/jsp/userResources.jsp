<%@ include file="/jsp/include.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

  <title>qiWorkbench&trade; &gt; User Install/Update</title>

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

  <meta name="description" content="qiWorkbench is an open source, web based, working environment." />

  <meta name="keywords" content="qiWorkbench, qi Workbench, open source, web based, environment, qiComponents, workbench" />

  <script type="text/javascript" src="scripts/navigation.js"></script>
  <script type="text/javascript" src="scripts/validation.js"></script>
  <style type="text/css" media="all">
@import url(css/home.css);
  </style>
</head>

<body onload="mbSet('menu', 'mb'); document.getElementById('menu').style.display = 'block';" id="divhome">

<div id="centre">
    <div id="header">
<%@ include file="shared/menus/commonmenus.html"%>
    </div>
    
    <div id="container">
        <br/>
        <img src="images/qiWorkbenchLogo.gif" alt="qiWorkbench logo" height="124" width="542" />

        <div id="containera">
            <font size="+1">User Install / Update</font>
            
            <p>Starting with the 1.3 production release, the recommended way to install the qiWorkbench and its qiComponents&trade; on end-user machines is from a Primary Update Center (PUC). Someone has to set up the PUC and deploy the workbench to it; an IT department should do this. The PUC is referred to as a deployment server. Details on how to set up a PUC can be found in the document <a href="docs/HowToSetupUpdateCenters.pdf">How to set up a qiWorkbench Deployment Update Center</a>.</p>
            
            <p>The workbench and its components execute on the end-user's machine. Project data files must be accessible from a runtime Tomcat server which may be the PUC, the user's machine or a different machine on a network. When the workbench is launched, the user must select a runtime Tomcat server. Details on how to set up a runtime Tomcat server can be found in the document <a href="docs/HowToSetupRuntimeServer.pdf.pdf">How to set up a qiWorkbench Runtime Server</a>.</p>
            
            <p>Detaled installation instructions can be found in the <a href="docs/ReadMe.txt">ReadMe</a>.</p>
            
            <br/>
            <font size="+1"><b>Auto Install & Update</b></font>
            
            <p>There are two ways to automatically update qiComponents in the user's home directory, .qiworkbench/components/ subdirectory a.k.a. the <em>component cache</em>. The first is performed when launching the qiWorkbench from the main Webpage of a PUC using the "with auto component updates" link. If there is a newer version of the qiWorkbench, it will be downloaded, installed and launched. If the qiComponents on the PUC are new or newer than those installed on the user's machine, they will be downloaded and installed. The public PUC's URL is <a href="http://app.qiworkbench.biz">http://app.qiworkbench.biz</a>.
            </p>
            
            <p>The second method is by selecting the "Update Components" menu item in the qiWorkbench after it is launched. In the "Select Update Center(s) dialog, the user specifies which Update Centers to use. In the next dialog, the user selects which qiComponents available from the Update Centers to update.
            </p>
            
            <p>If a user has never used the qiWorkbench before, the PUC serves as a bootstrapping mechanism. The qiWorkbench will be downloaded, installed on their machine and launched. Then the qiWorkbench will create the component cache in the user's home directory. Finally, the qiWorkbench will populate the component cache with all qiComponents available on the PUC.
            </p>
            
            <p>Users should obtain and read the <a href="docs/ReleaseNotes.pdf">Release Notes</a> and "Getting Started" which is available  on the <a href="userdocs.jsp">User Documentation</a> webpage.</p>
            
            <p>This public release contains the following program modules:<p>
            
            <br/>
              <table border="0" cellpadding="2" cellspacing="2" width="700">
                <tbody>
                  <tr>
                    <th scope="col" width="180">&nbsp;</th>
                    <th scope="col" align="center" width="100"><font size="+1">License</font></th>
                    <th scope="col" align="center" width="320"><font size="+1">Description</font></th>
                  </tr>

                  <tr valign="top">
                    <th scope="row" align="left">qiWorkbench</th>
                    <td align="center">
                      <a target="_blank" href="dev/licenses/qiWorkbench/qiwb_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                    </td>
                    <td align="left"><font size="-1">An extensible platform for seismic interpretation</font></td>
                  </tr>
                  
                  <tr valign="top">
                    <th scope="row" align="left">qiwbBuilder</th>
                    <td align="center">
                      <a target="_blank" href="dev/licenses/qiwbBuilder/builder_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                    </td>
                    <td align="left"><font size="-1">Master ANT build scripts for the qiWorkbench and all its qiComponents and libraries</font></td>
                  </tr>
                  
                  <tr>
                    <td colspan="3">&nbsp;</td>
                  </tr> 
                  
                  <tr>
                    <td colspan="3"><hr/></td>
                  </tr>
                  
                  <tr>
                    <td colspan="3">&nbsp;</td>
                  </tr> 
                 
                  <tr>
                    <th scope="row" align="left"><i>qiComponents</i></th>
                    <td></td>
                    <td></td>
                  </tr>
                  
                  <tr valign="top">
                    <th scope="row" align="left">Amplitude Extraction</th>
                    <td align="center">
                      <a target="_blank" href="dev/licenses/ampextract/ampe_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                    </td>
                    <td align="left"><font size="-1">Horizon-based Secant amplitude and area extraction</font></td>
                  </tr>

                  <tr valign="top">
                    <th scope="row" align="left">bhpViewer (2D)</th>
                    <td align="center">
                      <a target="_blank" href="dev/licenses/bhpViewer/bhpv_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                    </td>
                    <td align="left"><font size="-1">A 2D seismic viewer</font></td>
                  </tr>
                  
                  <tr valign="top">
                    <th scope="row" align="left">qiDataConverter</th>
                    <td align="center">
                      <a target="_blank" href="dev/licenses/qiDataConverter/dataconvert_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                    </td>
                    <td align="left"><font size="-1">Convert seismic/horizon data between BHP-SU and other data formats such as SEGY, Landmark, SU</font></td>
                  </tr>
                  
                  <tr valign="top">
                    <th scope="row" align="left">qiProjectManager</th>
                    <td align="center">
                      <a target="_blank" href="dev/licenses/qiProjectManager/projmgr_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                    </td>
                    <td align="left"><font size="-1">Manage a project's data files and their location</font></td>
                  </tr>
                  
                  <tr valign="top">
                    <th scope="row" align="left">qiViewer (2D)</th>
                    <td align="center">
                      <a target="_blank" href="dev/licenses/qiViewer/qiViewer_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                    </td>
                    <td align="left"><font size="-1">A programmatically controllable 2D seismic viewer that is functionally equivalent to the bhpViewer</font></td>
                  </tr>
                  
                  <tr valign="top">
                    <th scope="row" align="left">XML Editor</th>
                    <td align="center">
                      <a target="_blank" href="dev/licenses/xmlEditor/xmle_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                    </td>
                    <td align="left"><font size="-1">An editor to create and edit an arbitrary XML file or a XML file based on a schema</font></td>
                  </tr>

                </tbody>
              </table>
        </div>
    </div>
    
<%@ include file="shared/menus/footer.html"%>

</div>

</body>
</html>
