<%@ include file="/jsp/include.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

  <title>qiWorkbench&trade; &gt; User Download/Update</title>

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

  <meta name="description" content="qiWorkbench is an open source, web based, working environment." />

  <meta name="keywords" content="qiWorkbench, qi Workbench, open source, web based, environment, qiComponents, workbench" />

  <script type="text/javascript" src="scripts/navigation.js"></script>
  <script type="text/javascript" src="scripts/validation.js"></script>
  <style type="text/css" media="all">
@import url(css/home.css);
  </style>
</head>

<body onload="mbSet('menu', 'mb'); document.getElementById('menu').style.display = 'block';" id="divhome">

<div id="centre">
    <div id="header">
<%@ include file="shared/menus/commonmenus.html"%>
    </div>
    
    <div id="container">
        <br/>
        <img src="images/qiWorkbenchLogo.gif" alt="qiWorkbench logo" height="124" width="542" />

        <div id="containera">
            <font size="+1">User Download / Update</font>
            
            <p>There are 3 versions of qiTurbSim, a numerical turbidite simulator: </p>
            <ul>
            <li>2d_multigrain: This code simulates 2-d turbidity currents with multiple grain sizes.  It includes a substrate model that can track the sediment deposits that result from an arbitrary number of successive flows.  Complex topography is treated with a spatially varying gravity vector.  It includes a LES turbulence model.</li>
            <li>3d_multigrain: This code simulates 3-d turbidity currents.  It allows for one grain size.  Complex topography is treated with a spatially varying gravity vector.   It includes a LES turbulence model.</li>
            <li>gvg_2d: This code is currently under active development and is experimental in nature.  It is a 2d gravity current simulation with variable geometry.  It will treat complex evolving topography in a spatially and temporally accurate manner.</li>
            </ul>
            
            <p>The theory behind this work is contained in these references:</p>
            <ul>
            <li>High-Resolution Numerical Simulations of Resuspending Gravity Currents: Conditions for Self-Sustainment, with F. Blanchette, M. Strauss, B. Kneller and M. Glinsky, J. Geophys. Res. C: Oceans 110, C12 (2005). (<a href="docs/qiTurbSim/Blanchette_et_al_2005.pdf"><img align="absbottom" src="images/pdf-icon-small.gif" height="26" width="29" /></a>)</li>
            <li>Evaluation of a Simplified Approach for Simulating Gravity Currents over Slopes of Varying Angles, with F. Blanchette, V. Piche and M. Strauss, Comp. Fluids 35, 492 (2006). (<a href="docs/qiTurbSim/Blanchette_et_al_2006.pdf"><img align="absbottom" src="images/pdf-icon-small.gif" height="26" width="29" /></a>)</li>
            </ul>

            <p>Before you obtain a version of qiTurbSim, you should read its license by selecting the applications's Text icon (<img align="bottom" src="images/text.gif" width="16" height="16"/>).</p>
            
            <br/>
            <font size="+1"><b>Manual Download & Build</b></font>
            
            <p>To make the applications at your site, download each application and follow the instructions in the README file which states how to build the application, the name of the executable file produced and where it is created, how to run the application and what its inputs and outputs are. [NOTE: These README files are in the process of being created.]<p>
            
            <br/>
              <table border="0" cellpadding="2" cellspacing="2" width="700">
                <tbody>
                  <tr>
                    <th scope="col" width="180">&nbsp;</th>
                    <th scope="col" align="center" width="100"><font size="+1">License</font></th>
                    <th scope="col" align="center" width="100"><font size="+1">Download</font></th>
                    <th scope="col" align="center" width="320"><font size="+1">Description</font></th>
                  </tr>

                  <tr valign="top">
                    <th scope="row" align="left">2d_multigrain</th>
                    <td align="center">
                      <a target="_blank" href="dev/licenses/qiTurbSim/2Dmultigrain_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                    </td>
                    <td align="center"><a href="downloads/qiTurbSim/2d_multigrain.tar.gz"><img src="images/zip-icon.gif" height="35" width="39" /></a></td>
                    <td align="left"><font size="-1">A turbidity current flow and deposition simulator in two dimensions; incorporates a substrate erosion and resuspension model.</font></td>
                  </tr>
                  
                  <tr valign="top">
                    <th scope="row" align="left">3dturbidite</th>
                    <td align="center">
                      <a target="_blank" href="dev/licenses/qiTurbSim/3Dturbidite_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                    </td>
                    <td align="center"><a href="downloads/qiTurbSim/3dturbidite.tar.gz"><img src="images/zip-icon.gif" height="35" width="39" /></a></td>
                    <td align="left"><font size="-1">A turbidity current flow and deposition simulator in three dimensions; not functionally complete. Employs a LES (large eddy simulation) model and a variable substrate geometry using the variable gravity vector approach.</font></td>
                  </tr>
                  
                  <tr valign="top">
                    <th scope="row" align="left">gvg_2d</th>
                    <td align="center">
                      <a target="_blank" href="dev/licenses/qiTurbSim/gvg2D_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                    </td>
                    <td align="center"><a href="downloads/qiTurbSim/gvg_2d.tar.gz"><img src="images/zip-icon.gif" height="35" width="39" /></a></td>
                    <td align="left"><font size="-1">A new turbidity current flow and deposition simulator that uses a masking approach to represent an arbitrary substrate boundary. To be evolved into a fully functional 3D simulator..</font></td>
                  </tr>

                </tbody>
              </table>
        </div>
    </div>
    
<%@ include file="shared/menus/footer.html"%>

</div>

</body>
</html>
