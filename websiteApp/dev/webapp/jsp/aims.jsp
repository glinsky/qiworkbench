<%@ include file="/jsp/include.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<!-- web update by Daniel Cooke of hypermean.com -->
  <title>qiWorkbench&trade; &gt; Aims</title>


  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

  <meta name="description" content="qiWorkbench is an open source, web based, working environment." />

  <meta name="keywords" content="qiWorkbench, qi Workbench, open source, web based, environment, qiComponents, workbench" />

  <script type="text/javascript" src="scripts/navigation.js"></script>
  <script type="text/javascript" src="scripts/validation.js"></script>
  <style type="text/css" media="all">
@import url(css/home.css);
  </style>
</head>


<body onload="mbSet('menu', 'mb'); document.getElementById('menu').style.display = 'block';" id="divhome">

<div id="centre">
    <div id="header">
<%@ include file="shared/menus/commonmenus.html"%>
    </div>
    
    <div id="container">
        <br/>
        <img src="images/qiWorkbenchLogo.gif" alt="qiWorkbench logo" height="124" width="542" />

        <div id="containera">
            <font size="+1">Project Aims</font>

            <p>The qiWorkbench&trade; was developed to provide a flexible, extensible platform for implementing integrated workflows to process, analyze and view seismic data. It provides the underpinings of an Operating System through its message passing framework, job services and IO services. </p>

            <p>The workbench can be extended by adding external components. Such components can interact via command messages with other active components thereby allowing a rich behavior of the qiComponent&trade; community on the qiWorkbench, and build upon core IO and job services. Thus, a 2D viewer component could request a 3D viewer component synchronize its cursor with the 2D viewer. Or, a monitoring component could record request messages as they flow through system that could be played back later. The command set may be extended to accommodate new forms of interaction.</p>

            <p>The qiWorkbench provides an API for developing qiComponents that is meant to encourage the development of components by 3rd parties. The API represents a high level interface to the messaging framework and services and is covered by an academic BSD license thereby isolating component code from workbench code covered by the reciprocal GPL 2 license. The API therefore allows commercial components to be developed and be governed by their own license and license control. For example, a vendor could develop a superior component for performing IO on data in a particular seismic format and make it available to the community.</p>

            <p>A basic requirement was to make the qiWorkbench platform independent and Web based. Therefore, it was coded entirely in Java. The former quarantees consistent behavior on Windows, Mac and Unix based computers. The latter allows end-users the ability to use the workbench wherever they go and have internet access.</p>

            <p>In time the qiWorkbench is envisioned to provide a rich set of qiComponents that will give analysts in the industry access to quality open-source tools for performing their workflows. By providing services and components that handle data in different formats, analysts will be able to reduce the time it takes to perform their analysis. </p>

            <p>Being an open source project, it is envisioned that other devlopers will contribute to the code base thereby making the qiWorkbench a better product suited to the needs of the community.</p>

        </div>
    </div>
    
<%@ include file="shared/menus/footer.html"%>

</div>

</body>
</html>
