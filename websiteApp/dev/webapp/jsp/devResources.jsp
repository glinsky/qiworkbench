<%@ include file="/jsp/include.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

  <title>qiWorkbench&trade; &gt; Developer Resources/Download</title>

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

  <meta name="description" content="qiWorkbench is an open source, web based, working environment." />

  <meta name="keywords" content="qiWorkbench, qi Workbench, open source, web based, environment, qiComponents, workbench" />

  <script type="text/javascript" src="scripts/navigation.js"></script>
  <script type="text/javascript" src="scripts/validation.js"></script>
  <style type="text/css" media="all">
@import url(css/home.css);
  </style>
</head>


<body onload="mbSet('menu', 'mb'); document.getElementById('menu').style.display = 'block';" id="divhome">

<div id="centre">
    <div id="header">
<%@ include file="shared/menus/commonmenus.html"%>
    </div>
    
    <div id="container">
        <br/>
        <img src="images/qiWorkbenchLogo.gif" alt="qiWorkbench logo" height="124" width="542" />

        <div id="containera">
            <font size="+1">Developer Resources / Download</font>
            <p>There are a number of resources available for each workbench
            module:</p>

            <table style="width: 515px; height: 117px;" border="0" cellpadding="2" cellspacing="2">
              <tbody>
                <tr>
                  <td>&nbsp;</td>
                  <td align="center"><img src="images/sq0.png" /></td>
                  <td align="left"><font size="-1">
                  &nbsp;Visit the module's portion of the qiWorkbench&trade; Wiki <font color="#ff0000">(currently not set up)</font></font>
                  </td>
                </tr>

                <tr>
                  <td>&nbsp;</td>
                  <td align="center"><img src="images/sq0.png" /></td>
                  <td align="left"><font size="-1">
                  &nbsp;Download the source code and/or binary bundle for a qiWorkbench module</font>
                  </td>
                </tr>

                <tr>
                  <td>&nbsp;</td>
                  <td align="center"><img src="images/sq0.png" /></td>
                  <td align="left"><font size="-1">
                  &nbsp;View the license of a qiWorkbench module</font>
                  </td>
                </tr>

                <tr>
                  <td>&nbsp;</td>
                  <td align="center"><img src="images/sq0.png" /></td>
                  <td align="left"><font size="-1">
                  &nbsp;Become involved in the development of the qiWorkbench </font>
                  </td>
                </tr>

              </tbody>
            </table>

            <p>
            To access a module's resource, simply click on the appropriate icon in the module's row.</p>
            
            <p>If you want to build all qiWorkbench modules from scratch, then download their source code files and the qiwbBuilder and follow the instructions in the "qiWorkbench Build Guide" available separately on the <a href="devdocs.jsp">Developer Documentation</a> webpage. The build process is contained in the qiwbBuilder module. It contains ANT scripts for building all the modules in the proper order.</p>

            <p>If you want to develop a qiComponent&trade;, then download the qiComponent API and follow the instructions in the "qiComponent Development Guide" available separately on the <a href="devdocs.jsp">Developer Documentation</a> webpage.</p>

            <p>If you are a certified qiWorkbench developer, then you will need access to the <a href="https://bitbucket.org/glinsky/qiworkbench">BitBucket source code repositories</a> and download the developer's documentation found on the <a href="devdocs.jsp">Developer Documentation</a> webpage. To gain access to the BitBucket source code repositories and become a certified qiWorkbench developer, send your request in e-mail to <a href="mailto:admin@qiworkbench.biz">admin@qiworkbench.biz</a>.  (The source has just recently been moved from subversion to BitBucket and Mercurial.  You can download the source without getting permission.  You will need permission to be able to push your changes up to the BitBucket repository.)</p>

            <p>Before you obtain the source code of any workbench module, you
            should reads its license by selecting the module's Text icon (<img src="images/text.gif" height="16" width="16" />).</p>
            
            <p>Numerous qiWorkbench JAR files and each qiComponent JAR file must be signed. The ANT build files use a self-signing certificate contained in the supplied keystore, local_qi.keystore. Download <a href="downloads/local_qi.keystore">local_qi.keystore</a> and place it in the qiWorkbench's dependency directory specified by the QIDEP_DIR environment variable. Follow the instructions contained in the <a href="docs/Build_Guide.pdf">qiWorkbench Build Guide</a>. The Guide also states how to update the certificate if it has expired. If you use your own digital certificate, replace the supplied keystore with yours and modify the build files accordingly. </p>
            <br/>

            <table style="width: 700px;" border="0" cellpadding="4" cellspacing="4">
              <tbody>
                <tr valign="top">
                  <th scope="col">&nbsp; </th>
                  <th scope="col" valign="middle" align="center">
                    Wiki
                  </th>
                  <th scope="col" align="center">
                    Download<br/>Current Release<br/>of Source Code
                  </th>
                  <th scope="col" align="center">
                    Download<br/>Binary Bundle
                  </th>                  
                  <th scope="col" valign="middle" align="center">
                    License
                  </th>
                  <th scope="col" align="center">
                    Submit modifications /<br/>join development<br/>community
                  </th>
                </tr>

                <tr valign="top">
                  <th scope="row" align="left">qiWorkbench </th>
                  <td align="center">
                    <img src="images/green_arrow_small.gif" height="14" width="14" />
                  </td>
                  <td align="center"><a href="https://bitbucket.org/glinsky/qiworkbench">See bitBucket</a></td>
                  <td align="center">
                    <a href="downloads/qiWorkbench.war"><img src="images/javalogo.gif" height="44" width="26" /></a>
                  </td>
                  <td align="center">
                    <a target="_blank" href="dev/licenses/qiWorkbench/qiwb_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                  </td>
                  <td align="center">
                    <a href="mailto:admin@qiworkbench.biz">
                        <img src="images/green_arrow_small.gif" height="14" width="14" /></a>
                  </td>
                </tr>
                
                <tr valign="top">
                  <th scope="row" align="left">all qiComponents </th>
                  <td align="center">
                    <img src="images/green_arrow_small.gif" height="14" width="14" />
                  </td>
                  <td align="center"><a href="https://bitbucket.org/glinsky/qiworkbench">See bitBucket</a>
                  </td>
                  <td align="center"><a href="downloads/qiComponents.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a></td>
                  <td align="center">
                    <a target="_blank" href="dev/licenses/qiwbBuilder/builder_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                  </td>
                  <td align="center">
                    <a href="mailto:admin@qiworkbench.biz">
                        <img src="images/green_arrow_small.gif" height="14" width="14" /></a>
                  </td>
                </tr>
                
                <tr>
                  <td colspan="3">&nbsp;</td>
                </tr> 
                  
                <tr>
                  <td colspan="3"><hr/></td>
                </tr>
                  
                <tr>
                  <td colspan="3">&nbsp;</td>
                </tr>
                
                <tr valign="top">
                  <th scope="row" align="left">qiComponent API, Common </th>
                  <td align="center">
                    <img src="images/green_arrow_small.gif" height="14" width="14" />
                  </td>
                  <td align="center"><font size="-1">Part of<br/>qiWorkbench  source</font></td>
                  <td align="center">
                    <a href="downloads/qiwbCompAPI.jar"><img src="images/javalogo.gif" height="44" width="26" /></a> <b>,</b> <a href="downloads/qiwbCompCommon.jar"><img src="images/javalogo.gif" height="44" width="26" /></a>
                  </td>
                  <td align="center">
                    <a target="_blank" href="dev/licenses/qiwbAPI/qiwbAPI_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                  </td>
                  <td align="center">
                    <a href="mailto:admin@qiworkbench.biz">
                        <img src="images/green_arrow_small.gif" height="14" width="14" /></a>
                  </td>
                </tr>
                
                <tr valign="top">
                  <th scope="row" align="left">Amplitude Extraction </th>
                  <td align="center">
                    <img src="images/green_arrow_small.gif" height="14" width="14" />
                  </td>
                  <td align="center"><a href="downloads/ampExt-1.4_src.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a></td>
                  <td align="center">
                    <a href="downloads/ampExt-1.4.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a>
                  </td>
                  <td align="center">
                    <a target="_blank" href="dev/licenses/ampextract/ampe_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                  </td>
                  <td align="center">
                    <a href="mailto:admin@qiworkbench.biz">
                        <img src="images/green_arrow_small.gif" height="14" width="14" /></a>
                  </td>
                </tr>

                <tr valign="top">
                  <th scope="row" align="left">bhpViewer (2D) </th>
                  <td align="center">
                    <img src="images/green_arrow_small.gif" height="14" width="14" />
                  </td>
                  <td align="center">
                    <a href="downloads/bhpViewer-1.4_src.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a>
                  </td>
                  <td align="center">
                    <a href="downloads/bhpViewer-1.4.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a>
                  </td>
                  <td align="center">
                    <a target="_blank" href="dev/licenses/bhpViewer/bhpv_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                  </td>
                  <td align="center">
                    <a href="mailto:admin@qiworkbench.biz">
                        <img src="images/green_arrow_small.gif" height="14" width="14" /></a>
                  </td>
                </tr>

                <tr valign="top">
                  <th scope="row" align="left">gwToolkits:<br/> 2Dlib, SeismicLib </th>
                  <td align="center">
                    <img src="images/green_arrow_small.gif" height="14" width="14" />
                  </td>
                  <td align="center">
                    <a href="downloads/gw2Dlib-1.4_src.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a><b>,</b> <a href="downloads/gwSeismicLib-1.4_src.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a>
                  </td>
                  <td align="center">
                    <a href="downloads/gw2Dlib.jar"><img src="images/javalogo.gif" height="44" width="26" /></a> <b>,</b> <a href="downloads/gwSeismicLib.jar"><img src="images/javalogo.gif" height="44" width="26" /></a>
                  </td>
                  <td align="center">
                    <a target="_blank" href="dev/licenses/gwToolkits/gwLibs_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                  </td>
                  <td align="center">
                    <a href="mailto:admin@qiworkbench.biz">
                        <img src="images/green_arrow_small.gif" height="14" width="14" /></a>
                  </td>
                </tr>
                
                <tr valign="top">
                  <th scope="row" align="left">geoIOToolkits:<br/> bhpsuIOlib, geoIOlib </th>
                  <td align="center">
                    <img src="images/green_arrow_small.gif" height="14" width="14" />
                  </td>
                  <td align="center">
                    <a href="downloads/bhpsuIOlib-1.4_src.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a><b>,</b> <a href="downloads/geoIOlib-1.4_src.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a>
                  </td>
                  <td align="center">
                    <a href="downloads/bhpsuIOlib.jar"><img src="images/javalogo.gif" height="44" width="26" /></a> <b>,</b> <a href="downloads/geoIOlib.jar"><img src="images/javalogo.gif" height="44" width="26" /></a>
                  </td>
                  <td align="center">
                    <a target="_blank" href="dev/licenses/geoToolkits/geoLibs_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                  </td>
                  <td align="center">
                    <a href="mailto:admin@qiworkbench.biz">
                        <img src="images/green_arrow_small.gif" height="14" width="14" /></a>
                  </td>
                </tr>
                
                <tr valign="top">
                  <th scope="row" align="left">qiDataConverter </th>
                  <td align="center">
                    <img src="images/green_arrow_small.gif" height="14" width="14" />
                  </td>
                  <td align="center"><a href="downloads/qiDataConverter-1.4_src.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a></td>
                  <td align="center">
                    <a href="downloads/qiDataConverter-1.4.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a></a>
                  </td>
                  <td align="center">
                    <a target="_blank" href="dev/licenses/qiDataConverter/dataconvert_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                  </td>
                  <td align="center">
                    <a href="mailto:admin@qiworkbench.biz">
                        <img src="images/green_arrow_small.gif" height="14" width="14" /></a>
                  </td>
                </tr>
                
                <tr valign="top">
                  <th scope="row" align="left">qiProjectManager </th>
                  <td align="center">
                    <img src="images/green_arrow_small.gif" height="14" width="14" />
                  </td>
                  <td align="center"><a href="downloads/qiProjectManager-1.4_src.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a></td>
                  <td align="center">
                    <a href="downloads/qiProjectManager-1.4.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a></a>
                  </td>
                  <td align="center">
                    <a target="_blank" href="dev/licenses/qiProjectManager/projmgr_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                  </td>
                  <td align="center">
                    <a href="mailto:admin@qiworkbench.biz">
                        <img src="images/green_arrow_small.gif" height="14" width="14" /></a>
                  </td>
                </tr>
                
                <tr valign="top">
                  <th scope="row" align="left">qiViewer (2D), geoGraphicsLib </th>
                  <td align="center">
                    <img src="images/green_arrow_small.gif" height="14" width="14" />
                  </td>
                  <td align="center">
                    <a href="downloads/qiViewer-1.4_src.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a><b>,</b> <a href="downloads/geoGraphicsLib-1.4_src.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a>
                  </td>
                  <td align="center">
                    <a href="downloads/qiViewer-1.4.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a> <b>,</b> <a href="downloads/geoIOlib-1.4.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a>
                  </td>
                  <td align="center">
                    <a target="_blank" href="dev/licenses/qiViewer/qiViewer_license.html"><img src="images/text.gif" height="16" width="16" /></a> <b>,</b> <a target="_blank" href="dev/licenses/qiViewer/geoGraphics_license.html"><img src="images/text.gif" height="16" width="16" />
                  </td>
                  <td align="center">
                    <a href="mailto:admin@qiworkbench.biz">
                        <img src="images/green_arrow_small.gif" height="14" width="14" /></a>
                  </td>
                </tr>
                
                <tr valign="top">
                  <th scope="row" align="left">XML Editor </th>
                  <td align="center">
                    <img src="images/green_arrow_small.gif" height="14" width="14" />
                  </td>
                  <td align="center"><a href="downloads/xmlEditor-1.4_src.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a></td>
                  <td align="center">
                    <a href="downloads/xmlEditor-1.4.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a>
                  </td>
                  <td align="center">
                    <a target="_blank" href="dev/licenses/xmlEditor/xmle_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                  </td>
                  <td align="center">
                    <a href="mailto:admin@qiworkbench.biz">
                        <img src="images/green_arrow_small.gif" height="14" width="14" /></a>
                  </td>
                </tr>
                
                <tr>
                  <td colspan="3">&nbsp;</td>
                </tr> 
                  
                <tr>
                  <td colspan="3"><hr/></td>
                </tr>
                  
                <tr>
                  <td colspan="3">&nbsp;</td>
                </tr>
                
                <tr valign="top">
                  <th scope="row" align="left">qiwbBuilder </th>
                  <td align="center">
                    <img src="images/green_arrow_small.gif" height="14" width="14" />
                  </td>
                  <td align="center"><a href="downloads/qiwbBuilder-1.4_src.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a></td>
                  <td align="center">
                  </td>
                  <td align="center">
                    <a target="_blank" href="dev/licenses/qiwbBuilder/builder_license.html"><img src="images/text.gif" height="16" width="16" /></a>
                  </td>
                  <td align="center">
                    <a href="mailto:admin@qiworkbench.biz">
                        <img src="images/green_arrow_small.gif" height="14" width="14" /></a>
                  </td>
                </tr>
                
                
              </tbody>
            </table>

        </div>
    </div>
    
<%@ include file="shared/menus/footer.html"%>

</div>

</body>
</html>
