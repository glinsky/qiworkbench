<%@ include file="/jsp/include.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

  <title>qiWorkbench&trade; &gt; User Docs</title>


  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

  <meta name="description" content="qiWorkbench is an open source, web based, working environment." />

  <meta name="keywords" content="qiWorkbench, qi Workbench, open source, web based, environment, qiComponents, workbench" />

  <script type="text/javascript" src="scripts/navigation.js"></script>
  <script type="text/javascript" src="scripts/validation.js"></script>
  <style type="text/css" media="all">
@import url(css/home.css);
  </style>
</head>


<body onload="mbSet('menu', 'mb'); document.getElementById('menu').style.display = 'block';" id="divhome">

<div id="centre">
    <div id="header">
<%@ include file="shared/menus/commonmenus.html"%>
    </div>
    
    <div id="container">
        <br/>
        <img src="images/qiWorkbenchLogo.gif" alt="qiWorkbench logo" height="124" width="542" />

        <div id="containera">
            <font size="+1">User Documentation</font>

            <p>&nbsp; </p>
            <table border="0" cellpadding="2" cellspacing="2" width="600">
                <tbody>
                  <tr valign="top">
                    <th scope="col" align="center" width="180"><font size="+1">Document</font></th>
                    <th scope="col" align="center" width="100">
                      <font size="+1">Download</font>
                    </th>
                    <th scope="col" align="center" width="320">
                      <font size="+1">Description</font>
                    </th>
                  </tr>

                  <tr valign="top">
                    <th scope="row" align="left">
                      Getting Started
                    </th>
                    <td align="center">
                      <a href="docs/Getting_Started.pdf"><img src="images/pdf-icon.gif" height="35" width="39" /></a>
                    </td>
                    <td><font size="-1">How to get started using the qiWorkbeanch&trade;.</font>
                    </td>
                  </tr>

                  <tr valign="top">
                    <th scope="row" align="left">qiWorkbench Overview
                    </th>
                    <td align="center">
                      <a href="docs/qiWorkbench_Overview_v2.pdf"><img src="images/pdf-icon.gif" height="35" width="39" /></a>
                    </td>
                    <td><font size="-1">An overview presentation on the qiWorkbench.</font>
                    </td>
                  </tr>
                  
                  <tr valign="top">
                    <th scope="row" align="left">Bug/Enhancement Form
                    </th>
                    <td align="center">
                      <a href="docs/BugReportForm.pdf"><img src="images/pdf-icon.gif" height="35" width="39" /></a>
                    </td>
                    <td><font size="-1">A standard form for submitting a bug or enhancement.</font>
                    </td>
                  </tr>

                </tbody>
              </table>
              
        </div>
    </div>

<%@ include file="shared/menus/footer.html"%>

</div>

</body>
</html>
