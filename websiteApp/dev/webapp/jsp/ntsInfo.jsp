<%@ include file="/jsp/include.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

  <title>qiTurbSim&trade; &gt; General Information</title>

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

  <meta name="description" content="qiTurbSim is an open source, turbidity current flow and deposition simulator." />

  <meta name="keywords" content="qiWorkbench, qi Workbench, open source, turbidite, simulator" />

  <script type="text/javascript" src="scripts/navigation.js"></script>
  <script type="text/javascript" src="scripts/validation.js"></script>
  <style type="text/css" media="all">
@import url(css/home.css);
  </style>
</head>

<body onload="mbSet('menu', 'mb'); document.getElementById('menu').style.display = 'block';" id="divhome">

<div id="centre">
    <div id="header">
<%@ include file="shared/menus/commonmenus.html"%>
    </div>
    
    <div id="container">
        <br/>
        <img src="images/qiWorkbenchLogo.gif" alt="qiWorkbench logo" height="124" width="542" />

        <div id="containera">
            <font size="+1">qiTurbSim General Information</font>

            <p>qiTurbSim performs numerical simulation of turbidity currents for hydrocarbon reservoir modeling.</p>
            
            <p>Prediction of deep marine depositional systems (where turbidity currents are a dominant process of sediment deposition) in the subsurface remains more of an art than a science, given their extreme complexity and the virtually unique nature of each system. Direct observation of modern processes remains limited, and experimental simulation is extremely valuable but time-consuming, physically difficult, and suffers from scaling problems. Numerical simulation can potentially provide matchless quantitative predictions of deposit geometries and properties in the subsurface, at a tiny fraction of the cost of drilling.</p>

            <p>qiTurbSim has been developed by Eckart Meiburg, from the Department of Mechanical and Environmental Engineering at University of California, Santa Barbara, Ben Kneller, of the Department of Geology and Petroleum Geology at the University of Aberdeen, and others, and funded by BHP Billiton Petroleum. Eckart Meiburg has been intimately involved with the creation and development of the model that formed the basis of this project. Ben Kneller is a sedimentologist with extensive experience of deep marine depositional systems, and over fifteen years of experience of working with the oil and gas business.</p>

            <p>Although there are a number of available numerical simulations of sediment gravity flow processes, the extreme complexity of natural systems leaves the accuracy of their predictions open to question. Realism necessitates a comprehension of the geological reality at the large scale, sediment transport processes at the intermediate scale, and an ability to capture the physical complexity of the flows at the small scale.</p>

            <p>The direct numerical simulation of turbidity currents by Necker et al (2002) removed many of the assumptions necessary in cruder simulations, but was restricted to the modeling of relatively simple lock-exchange systems - which it does, in three dimensions, with remarkable realism. The capabilities of the original model have been expanded to include situations of greater geological relevance, in particular the use of multiple grain-sizes, deep ambient fluid, mulitple flows, and complex bottom topography.</p>
        </div>
        
        <div id="columnc">
            <p><b>If you would like to submit modifications to the qiTurbSim codebase or join the development community, send email to <a href="mailto:admin@qiworkbench.biz">here</a>.</b></p>
            <br>
        </div>
    </div>

<%@ include file="shared/menus/footer.html"%>

</div>

</body>
</html>
