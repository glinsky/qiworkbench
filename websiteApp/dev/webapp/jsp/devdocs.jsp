<%@ include file="/jsp/include.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

  <title>qiWorkbench&trade; &gt; Developer Docs</title>


  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

  <meta name="description" content="qiWorkbench is an open source, web based, working environment." />

  <meta name="keywords" content="qiWorkbench, qi Workbench, open source, web based, environment, qiComponents, workbench" />

  <script type="text/javascript" src="scripts/navigation.js"></script>
  <script type="text/javascript" src="scripts/validation.js"></script>
  <style type="text/css" media="all">
@import url(css/home.css);
  </style>
</head>


<body onload="mbSet('menu', 'mb'); document.getElementById('menu').style.display = 'block';" id="divhome">

<div id="centre">
    <div id="header">
<%@ include file="shared/menus/commonmenus.html"%>
    </div>
    
    <div id="container">
        <br/>
        <img src="images/qiWorkbenchLogo.gif" alt="qiWorkbench logo" height="124" width="542" />

        <div id="containera">
            <font size="+1">Developer Documentation</font>

            <p>&nbsp; </p>
            <table border="0" cellpadding="2" cellspacing="2" width="600">
                <tbody>
                  <tr valign="top">
                    <th scope="col" align="center" width="200"><font size="+1">Document</font></th>
                    <th scope="col" align="center" width="100">
                      <font size="+1">Download</font>
                    </th>
                    <th scope="col" align="center" width="300">
                      <font size="+1">Description</font>
                    </th>
                  </tr>
                  
                  <tr valign="top">
                    <th scope="row" align="left">
                    ReadMe
                    </th>
                    <td align="center">
                      <a href="docs/ReadMe.txt"><img src="images/text.gif" height="16" width="16" /></a>
                    </td>
                    <td><font size="-1">Instructions on how to set up the deployment and execution environment for the workbench and its components. READ THIS FIRST.</font>
                    </td>
                  </tr>
                  
                  <tr valign="top">
                    <th scope="row" align="left">
                      Update Center Setup Guide
                    </th>
                    <td align="center">
                      <a href="docs/HowToSetupUpdateCenters.pdf"><img src="images/pdf-icon.gif" height="35" width="39" /></a>
                    </td>
                    <td><font size="-1">Instructions on how to set up a deployment update center for installing new and newer qiComponents.</font>
                    </td>
                  </tr>
                  
                  <tr valign="top">
                    <th scope="row" align="left">
                      Runtime Server Setup Guide
                    </th>
                    <td align="center">
                      <a href="docs/HowToSetupRuntimeServer.pdf"><img src="images/pdf-icon.gif" height="35" width="39" /></a>
                    </td>
                    <td><font size="-1">Instructions on how to set up a runtime Tomcat server which has access to qiProjects and their data files.</font>
                    </td>
                  </tr>

                  <tr valign="top">
                    <th scope="row" align="left">
                    qiWorkbench Javadocs
                    </th>
                    <td align="center">
                      <a href="downloads/qiWorkbench-1.4-apidoc.zip"><img src="images/zip-icon.gif" height="35" width="39" /></a>
                    </td>
                    <td><font size="-1">The API Javadocs for the entire qiWorkbench&trade;. Includes the Javadocs for the qiComponent&trade; API. Unzip into a folder and start at index.html.</font>
                    </td>
                  </tr>

                  <tr valign="top">
                    <th scope="row" align="left">
                      qiWorkbench Build Guide
                    </th>
                    <td align="center">
                      <a href="docs/Build_Guide.pdf"><img src="images/pdf-icon.gif" height="35" width="39" /></a>
                    </td>
                    <td><font size="-1">Instructions on how to build the qiWorkbench from the code base.</font>
                    </td>
                  </tr>

                  <tr valign="top">
                    <th scope="row" align="left">
                      qiWorkbench Configuration
                    </th>
                    <td align="center">
                      <a href="docs/Configuration.pdf"><img src="images/pdf-icon.gif" height="35" width="39" /></a>
                    </td>
                    <td><font size="-1">Instructions on how to configure the qiWorkbench.</font>
                    </td>
                  </tr>

                  <tr valign="top">
                    <th scope="row" align="left">
                      qiComponent Development Guide
                    </th>
                    <td align="center">
                      <a href="docs/qiComponent_Dev_Guide.pdf"><img src="images/pdf-icon.gif" height="35" width="39" /></a>
                    </td>
                    <td><font size="-1">Instructions on how to develop a qiComponent.</font>
                    </td>
                  </tr>

                  <tr valign="top">
                    <th scope="row" align="left">
                      openSeismic Architecture Design Note
                    </th>
                    <td align="center">
                      <a href="docs/openSeismic_DN.pdf"><img src="images/pdf-icon.gif" height="35" width="39" /></a>
                    </td>
                    <td><font size="-1">Orignal architecture design note for the qiWorkbench.</font>
                    </td>
                  </tr>

                  <tr valign="top">
                    <th scope="row" align="left">
                      openSeismic Supplement Design Note
                    </th>
                    <td align="center">
                      <a href="docs/openSeismic_Supplement.pdf"><img src="images/pdf-icon.gif" height="35" width="39" /></a>
                    </td>
                    <td><font size="-1">Updates to the architecture design note.</font>
                    </td>
                  </tr>

                  <tr valign="top">
                    <th scope="row" align="left">
                      Using Subversion
                    </th>
                    <td align="center">
                      <a href="docs/Using_Subversion.pdf"><img src="images/pdf-icon.gif" height="35" width="39" /></a>
                    </td>
                    <td><font size="-1">Instructions on accessing the Subversion source code repositories for the qiWorkbench, the qiComponents, and the libraries.</font>
                    </td>
                  </tr>
                </tbody>
              </table>

        </div>
    </div>

<%@ include file="shared/menus/footer.html"%>

</div>

</body>
</html>
