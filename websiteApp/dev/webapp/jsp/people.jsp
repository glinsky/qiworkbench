<%@ include file="/jsp/include.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<!-- web update by Daniel Cooke of hypermean.com -->
  <title>qiWorkbench&trade; &gt; People</title>


  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

  <meta name="description" content="qiWorkbench is an open source, web based, working environment." />

  <meta name="keywords" content="qiWorkbench, qi Workbench, open source, web based, environment, qiComponents, workbench" />

  <script type="text/javascript" src="scripts/navigation.js"></script>
  <script type="text/javascript" src="scripts/validation.js"></script>
  <style type="text/css" media="all">
@import url(css/home.css);
  </style>
</head>

<body onload="mbSet('menu', 'mb'); document.getElementById('menu').style.display = 'block';" id="divhome">

<div id="centre">
    <div id="header">
<%@ include file="shared/menus/commonmenus.html"%>
    </div>
    
    <div id="container">
        <br/>
        <img src="images/qiWorkbenchLogo.gif" alt="qiWorkbench logo" height="124" width="542" />

        <div id="containera">
            <font size="+1">Biographies</font>

            <p>The qiWorkbench&trade; is the vision of Michael Glinsky, leader of the Quantitative Interpretation Group at BHP Billiton Petroleum. The head architect and lead developer of the workbench is Gil Hansen.</p>

            <p><strong>Michael Glinsky</strong> received a B.S. degree in physics from Case Western Reserve University in 1983 and a Ph.D. degree in physics from the University of California, San Diego in 1991. His doctoral research on magnetized pure electron plasmas was recognized by the American Physical Society as the outstanding thesis in the United States (1993 Simon Ramo Award). Before enrolling in graduate school as a National Science Foundation Graduate Fellow, he worked as a geophysicist for Shell Oil Company. After graduate school, he worked as a Department of Energy Distinguished Postdoctoral Research Fellow at Lawrence Livermore National Laboratory for 5 years. More recently he worked for three years at the Shell E&P Technology Co. doing research on Bayesian AVO and 4D inversion. He is currently the Section Leader of Quantitative Interpretation for BHP Billiton Petroleum. He has published over 25 papers in the scientific literature on subjects as varied as plasma physics, signal processing for oil exploration, x-ray diagnostics, application of exterior calculus to theoretical mechanics, and laser biological tissue interactions. He recently received the top Australian scientific medal for his research on petroleum reservoir characterization.</p>

            <p><strong>Gil Hansen </strong> received a B.S. degree in Physics and M.S. degree in Engineering from Case Western Reserve University and a Ph.D. degree in Computer Science from Carnegie-Mellon University. After graduate school, he taught Computer Science at the University of Houston, Vanderbilt University and the University of Florida. Afterwards, he developed compilers for Texas Instruments and Convex Computer Corp. While at Convex, he managed, designed, and developed three major supercomputer products leveraging advanced research � vectorizing Fortran and C compilers, an interactive performance analysis tool, and a symbolic debugger for optimized code. After being involved in the R&D of a data-parallel compiler for High Performance Fortran at Rice University, he became a Java Consultant developing applications in the insurance, telecom, data storage, marketing and transportation industries. He joined BHP Billiton Petroleum as a QI Development Specialist where he has focused on providing tools to support interpreter�s workflow.</p>
        </div>
    </div>
    
<%@ include file="shared/menus/footer.html"%>

</div>

</body>
</html>
