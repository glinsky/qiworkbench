<%@ include file="/jsp/include.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<!-- web update by Daniel Cooke of hypermean.com -->
  <title>qiWorkbench&trade; &gt; Components</title>


  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

  <meta name="description" content="qiWorkbench is an open source, web based, working environment." />

  <meta name="keywords" content="qiWorkbench, qi Workbench, open source, web based, environment, qiComponents, workbench" />

  <script type="text/javascript" src="scripts/navigation.js"></script>
  <script type="text/javascript" src="scripts/validation.js"></script>
  <style type="text/css" media="all">
@import url(css/home.css);
  </style>
</head>

<body onload="mbSet('menu', 'mb'); document.getElementById('menu').style.display = 'block';" id="divhome">

<div id="centre">
    <div id="header">
<%@ include file="shared/menus/commonmenus.html"%>
    </div>
    
    <div id="container">
        <br/>
        <img src="images/qiWorkbenchLogo.gif" alt="qiWorkbench logo" height="124" width="542" />

        <div id="containera">
            <font size="+1">qiComponent&trade; Management</font>

            <p>Select those workbench modules you want to download or update and then click the "Update/Download Selected" button. You will be routed to Webpages that will step you through the process. If you are obtaining a commercial qiComponent, you will have to agree to the vendor's license terms and go through a payment step.</p>

            <p>Before you obtain a workbench module, you should read its  license by selecting the module's Text icon (<img src="images/text.gif" width="16" height="16"/>).</p>

            <form name="addComponent" id="addComponent" method="post" action="">
              <table border="0" cellpadding="2" cellspacing="2" width="400">
                <caption> <strong><br />
                </strong></caption> <tbody>
                  <tr>
                    <th scope="col">&nbsp;</th>
                    <th scope="col">&nbsp;</th>
                    <th scope="col">
                    <div align="center">License</div>
                    </th>
                  </tr>

                  <tr>
                    <th scope="row"><input name="checkbox" value="checkbox" type="checkbox" /> </th>
                    <th scope="row">qiWorkbench&trade;</th>
                    <td>
                    <div align="center"><a href="dev/licenses/qiWorkbench/qiwb_license.html"><img src="images/text.gif" height="16" width="16" /></a></div>
                    </td>
                  </tr>

                  <tr>
                    <th scope="row"><input name="checkbox" value="checkbox" type="checkbox" /></th>
                    <th scope="row">bhpViewer (2D) </th>
                    <td>
                    <div align="center"><a href="dev/licenses/bhpViewer/bhpv_license.html"><img src="images/text.gif" height="16" width="16" /></a></div>
                    </td>
                  </tr>

                  <tr>
                    <th scope="row"><input name="checkbox" value="checkbox" type="checkbox" /></th>
                    <th scope="row">gw3DViewer</th>
                    <td>
                    <div align="center"><a href="dev/licenses/gw3DViewer/gw3d_license.html"><img src="images/text.gif" height="16" width="16" /></a></div>
                    </td>
                  </tr>

                  <tr>
                    <th scope="row"><input name="checkbox" value="checkbox" type="checkbox" /></th>
                    <th scope="row">gwWellViewer</th>
                    <td>
                    <div align="center"><a href="dev/licenses/gwWellViewer/gwwell_license.html"><img src="images/text.gif" height="16" width="16" /></a></div>
                    </td>
                  </tr>

                  <tr>
                    <th scope="row"><input name="checkbox" value="checkbox" type="checkbox" /></th>
                    <th scope="row">amplitudeExtraction </th>
                    <td>
                    <div align="center"><a href="dev/licenses/ampextract/ampe_license.html"><img src="images/text.gif" height="16" width="16" /></a></div>
                    </td>
                  </tr>

                  <tr>
                    <th scope="row"><input name="checkbox" value="checkbox" type="checkbox" /></th>
                    <th scope="row">xmlEditor </th>
                    <td>
                    <div align="center"><a href="dev/licenses/xmlEditor/xmle_license.html"><img src="images/text.gif" height="16" width="16" /></a></div>
                    </td>
                  </tr>

                  <tr>
                    <th scope="row"><input name="checkbox" value="checkbox" type="checkbox" /></th>
                    <th scope="row">gwToolkit</th>
                    <td>
                    <div align="center"><a href="dev/licenses/gwToolkits/gwLibs_license.html"><img src="images/text.gif" height="16" width="16" /></a></div>
                    </td>
                  </tr>

                  <tr>
                    <th scope="row"><input name="checkbox" value="checkbox" type="checkbox" /></th>
                    <th scope="row">qiWorkbench Interface </th>
                    <td>
                    <div align="center"><a href="dev/licenses/qiWorkbench/qiwb_license.html"><img src="images/text.gif" height="16" width="16" /></a></div>
                    </td>
                  </tr>

                </tbody>
              </table>

              <div align="left">
              <br/>
              <input name="Submit" value="Update / Download Selected" type="submit" /></div>

            </form>
        </div>
    </div>
    
<%@ include file="shared/menus/footer.html"%>

</div>

</body>
</html>
