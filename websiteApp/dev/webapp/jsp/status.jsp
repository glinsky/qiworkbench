<%@ include file="/jsp/include.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

  <title>qiWorkbench&trade; &gt; Status</title>

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

  <meta name="description" content="qiWorkbench is an open source, web based, working environment." />

  <meta name="keywords" content="qiWorkbench, qi Workbench, open source, web based, environment, qiComponents, workbench" />

  <script type="text/javascript" src="scripts/navigation.js"></script>
  <script type="text/javascript" src="scripts/validation.js"></script>
  <style type="text/css" media="all">
@import url(css/home.css);
  </style>
</head>

<body onload="mbSet('menu', 'mb'); document.getElementById('menu').style.display = 'block';" id="divhome">

<div id="centre">
    <div id="header">
<%@ include file="shared/menus/commonmenus.html"%>
    </div>
    
    <div id="container">
        <br/>
        <img src="images/qiWorkbenchLogo.gif" alt="qiWorkbench logo" height="124" width="542" />

        <div id="containera">
            <font size="+1">Project Status</font>

            <p>The following key features are in place:</p>
            <div id="indent">
                <ul>
                    <li>Message framework and Messaging Manager</li>
                    <li>Dynamically load and launch qiComponents&trade; plus any dependent JAR files</li>
                    <li>Save and restore state of the workbench and individual components</li>
                    <li>Core components</li>
                    <div id="indent">
                        <ul>
                          <li> IO services: read and write ASCII and binary files, locally and
                        remotely</li>

                          <li>Job services: execute a program or script, locally or
                        remotely</li>

                          <li>Convenience services: file chooser dialog, error dialog</li>
                        </ul>
                    </div>

                    <li>External components</li>
                    <div id="indent">
                        <ul>
                          <li>Amplitude Extraction - horizon-based Secant amplitude and area extraction</li>
                          
                          <li>bhpViewer - a 2D seismic viewer that can read and write local and remote geophysical data files</li>
                          
                          <li>qiDataConverter - converts seismic/horizon data between BHP-SU and other data formats such as SEGY, Landmark, SU</li>
                          
                          <li>qiProjectManager - manages a qiProject's data files and their location. Includes a metadata editor for specifying the path of directories housing project data such as seismic, horizons, savesets and generated scripts.</li>

                          <li>xmlEditor - an editor to create and edit an arbitrary XML file or a XML file based on a schema.</li>
                        </ul>
                    </div>
                    
                    <li>qiwbBuilder - Master ANT build scripts for the qiWorkbench and all its qiComponents and libraries. </li>
                </ul>
            </div>

            <p>External components can be developed using the component API. See the <a href="docs/qiComponent_Dev_Guide.pdf">Guide to qiComponent Development</a>.</p>

            <p>The following capabilities are under development and will appear in a future release:</p>

            <div id="indent">
                <ul>
                    <li>Geophysical IO services - generic local and remote services to read and write geophysical files in different formats.</li>
                    <li>qiViewer component - a 2D seismic viewer equivalent to the bhpViewer, but with a new rastorization engine, IO based on the new geoIO services, a well log layer and command controllable.</li>
                </ul>
            </div>
            
            <p><em>Note: Local and remote refers to the accessibility of project data files from the end-user&rsquo;s machine using IO services executing on the runtime Tomcat server required to run the qiWorkbench. Local means file access is as if the files existed on the user&rsquo;s machine; otherwise, access is remote. </em></p>            
        </div>
    </div>

<%@ include file="shared/menus/footer.html"%>

</div>

</body>
</html>
