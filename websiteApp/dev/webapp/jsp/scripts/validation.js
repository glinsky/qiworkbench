// Verify function to perform basic check before
// a form is posted. This function checks for required fields and/or
// if fields are numeric
//

var errors="";

function verify(f)
{
    var msg="";
    var empty_fields="";

    for(var i = 0; i < f.length; i++)
    {
        var e = f.elements[i];
        var fieldName = e.desc ? e.desc : e.name;

        if(e.required=="true")
        {
            // check if field is empty
            if(isEmpty(e))
            {
                empty_fields += "\n      * " + fieldName;
                continue;
            }
            else if(e.numeric=="true" && !isNumeric(e))
            {
                errors += "\n - The " + fieldName + " field must be a number.\n";
                continue;
            }
            else if(e.email=="true" && !isValidEmail(e))
            {
                errors += "\n - The " + fieldName + " field contains an invalid e-mail address.\n";
                continue;
            }
            else if(e.maxlen)
            {
                var len = parseInt(e.maxlen);
                var value = e.value;

                if(value.length > len)
                {
                    errors += "\n - The " + fieldName + " field cannot exceed " + len + " characters. It currently contains " + value.length + " characters.\n";
                    continue;
                }
            }
        }
        else if(e.required=="false" && !isEmpty(e)) //do not validate optional fields if blank
        {
            if(e.numeric=="true" && !isNumeric(e))
            {
                errors += "\n - The " + fieldName + " field must be a number.\n";
                continue;
            }
            else if(e.email=="true" && !isValidEmail(e))
            {
                errors += "\n - The " + fieldName + " field contains an invalid e-mail address.\n";
                continue;
            }
            else if(e.maxlen)
            {
                var len = parseInt(e.maxlen);
                var value = e.value;

                if(value.length > len)
                {
                    errors += "\n - The " + fieldName + " field cannot exceed " + len + " characters. It currently contains " + value.length + " characters.\n";
                    continue;
                }
            }
        }

        // check if field is zip. Looking for XXXXX-XXXX
        // check if field is phone number. Looking for XXX-XXX-XXXX.
        // check if field is quantity. Spec lists XXXX as the format.

    } //end for

    // if no errors then return true
    if (!empty_fields && !errors) return true;

    msg  = "\n";
    msg += "The form was not submitted for the following reason(s)\n";
    msg += "Please correct the following and re-submit.\n";
    msg += "\n";

    if (empty_fields)
    {
        msg += " - The following required fields are empty:" + empty_fields + "\n";
    }

    msg += errors;
    errors="";
    alert(msg);
    return false;
}

//
// Check if the supplied field is empty
//
function isEmpty(field)
{
    if((field.type == "text") || (field.type == "file") || (field.type == "password") || (field.type == "hidden") || (field.type == "textarea"))
    {
        return (trim(field.value) == "" || field.value == null);
    }
    else if((field.type == "select-one") || (field.type == "select-multiple"))
    {
        return field.options[field.options.selectedIndex].value == "";
    }
    else
    {
        return false;
    }
}

//
// Check if the supplied field contains a numeric value
//
function isNumeric(field)
{
    return !isNaN( parseFloat(field.value) );
}

//
// Check for valid email
//
function isValidEmail(field)
{
    // grab the email address from the form.
    var email = new String(field.value);
    var retval = 0;  // store value returned by String.indexOf.
    var count = 0;   // count of the number of '@' characters.
    var index = 0;   // index from which to start seach

    // look for multiple '@' characters in the address
    while ((retval = email.indexOf("@", index)) != -1)
    {
        count++;
        index = retval + 1;
    }

    // report error if invalid number of '@' characters
    if (count == 1)
    {
        // look for at least 1 dot after the '@' character
        count = 0;
        while ((retval = email.indexOf(".", index)) != -1)
        {
            count++;
            index = retval + 1;
        }
        if (count == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
}

//
// Returns a copy of a string without leading spaces
//
function lTrim(str)
{
    var whitespace = new String(" \t\n\r");
    var s = new String(str);
    var count=0;

    while( count < s.length && whitespace.indexOf(s.charAt(count)) != -1 )
    {
        count++;
    }

    return s.substring(count, s.length);
}

//
// Returns a copy of a string without trailing spaces
//
function rTrim(str)
{
    var whitespace = new String(" \t\n\r");
    var s = new String(str);
    var count=s.length-1;

    while( count >= 0 && whitespace.indexOf(s.charAt(count)) != -1 )
    {
        count--;
    }

    return s.substring(0, count+1);
}

//
// Returns a copy of a string without leading or trailing spaces
//
function trim(str)
{
   return rTrim(lTrim(str));
}

//
// Will show or hide an object(span, div, etc..) given its id.
//
function setVisible(id, state)
{
    var element;
    var visible = 'visible';
    var hidden = 'hidden';

    //determine the object based on browser
    if(document.getElementById) // standards
    {
        element = document.getElementById(id).style;
    }
    else if(document.all) // IE4
    {
        element = document.all[id].style;
    }
    else if(document.layers) //N4
    {
        element = document.layers[id];
        visible = 'show';
        hidden = 'hide';
    }

    //hide or show the element
    element.visibility = state ? visible : hidden ;
}

//
// Will set a fields required status amd description.
// For use in Netscape...
//
function setRequired(field, desc)
{
    field.required = "true";
    field.desc = desc;
}


//provides the functionality to check or clear all checkboxes on a form
//uised for selecting or clearing all search collections
function checkAll(fields, value)
{
    for(var i=0; i<fields.length; i++)
    {
        fields[i].checked = value;
    }
}

//validates simple search
function validateSearch(form)
{
    if(trim(form.query.value)=="")
    {
        errors += " - Please enter a search query.";
    }

    return verify(form);
}


//validates advanced search
function validateAdvancedSearch(form)
{

    var collectionsChecked = false;

    if(trim(form.allwords.value)=="" && trim(form.phrase.value)=="" && trim(form.anywords.value)=="" && trim(form.exclude.value)=="")
    {
        errors += " - Please enter a search query.";
    }

    for(var i=0; i<form.elements.length; i++)
    {
        if(form.elements[i].type=="checkbox")
        {
            if(form.elements[i].checked) collectionsChecked = true;
        }
    }

    if(!collectionsChecked)
    {
        errors += "\n - You must choose at least one site to search.";
    }

    return verify(form);
}

//validates feedback form
function validateFeedback(form)
{

    form.name.required='true';
    form.name.desc='Name';
    form.email.required='true';
    form.email.desc='Email Address';
    form.confirmEmail.required='true';
    form.confirmEmail.desc='Confirm Email Address';
    form.comments.required='true';
    form.comments.desc='Comments';

    if(form.email.value != form.confirmEmail.value)
    {
        errors += "\n - The confirm email address supplied is incorrect.";
    }

    return verify(form);
}
