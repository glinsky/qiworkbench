/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.rasterizer;

import java.awt.Point;
import java.awt.Rectangle;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

public class RasterizerContextTest {
    //Point2Ds are equivalent if the X and Y values are both
    //within 0.00001 of one another.

    private static final double EPSILON = 0.00001;

    @Test
    @Ignore("Disabled pending test rewrite-RasterizerContext now requires a reference Layer")
    public void testOrigin() {
        throw new UnsupportedOperationException("Disabled pending test rewrite-RasterizerContext now requires a reference Layer");
        /*
        RasterizerContext context = new RasterizerContext(1000, 1000, 1000, 1000);

        Point viewOrigin = new Point(0, 0);
        Point worldOrigin = new Point(0, 0);

        //initially, the world and view origins are equivalent
        assertTrue(equals(worldOrigin, context.getViewCoordinate(worldOrigin)));
        assertTrue(equals(viewOrigin, context.getModelCoordinate(viewOrigin)));
        assertFalse(equals(context.getModelCoordinate(viewOrigin), new Point(1, 1)));
        assertFalse(equals(context.getViewCoordinate(worldOrigin), new Point(1, 1)));
        */
    }

    @Test
    @Ignore("Disabled pending test rewrite-RasterizerContext now requires a reference Layer")
    public void testScaling() {
        throw new UnsupportedOperationException("Disabled pending test rewrite-RasterizerContext now requires a reference Layer");
        /*
        RasterizerContext context = new RasterizerContext(1000, 1000, 1000, 1000);

        assertTrue(equals(1.0, context.getScaleX()));
        context.zoomIn();
        assertTrue(equals(2.0, context.getScaleX()));
        //9 more zoomIns to max zoomlevel of 10, or 1024x
        context.zoomIn();
        context.zoomIn();
        context.zoomIn();
        context.zoomIn();
        context.zoomIn();
        context.zoomIn();
        context.zoomIn();
        context.zoomIn();
        context.zoomIn();
        assertTrue(equals(1024.0, context.getScaleX()));
        //verify Y scale has also changed
        assertTrue(equals(1024.0, context.getScaleY()));
        //attempt 11th zoomin, which should have no effect
        context.zoomIn();
        assertTrue(equals(1024.0, context.getScaleX()));
        //finally, zoom out twice
        context.zoomOut();
        context.zoomOut();
        assertTrue(equals(256.0, context.getScaleY()));
        //now reset scale
        context.zoomReset();
        assertTrue(equals(1.0, context.getScaleX()));
        assertTrue(equals(1.0, context.getScaleY()));
        //and test zooming Out
        context.zoomOut();
        context.zoomOut();
        context.zoomOut();
        context.zoomOut();
        context.zoomOut();
        context.zoomOut();
        context.zoomOut();
        context.zoomOut();
        context.zoomOut();
        context.zoomOut();
        assertTrue(equals(Math.pow(2, -10), context.getScaleX()));
        //again, 11th zoom has no effect
        context.zoomOut();
        assertTrue(equals(Math.pow(2, -10), context.getScaleX()));
        */
    }

    @Test
    @Ignore("Disabled pending test rewrite-RasterizerContext now requires a reference Layer")
    public void testRectangleZooming() {
        throw new UnsupportedOperationException("Disabled pending test rewrite-RasterizerContext now requires a reference Layer");
        /*
        //RasterizerContext context = new RasterizerContext(1024, 1024, 1024, 1024);
        Rectangle rect = new Rectangle(100, 100, 512, 256);
        context.zoomRect(rect, false);

        Point expectedOriginMC = new Point(200, 400);

        Point actualOriginMC = context.getModelCoordinate(new Point(0, 0));
        assertNotNull(actualOriginMC);
        assertTrue(equals(expectedOriginMC, actualOriginMC));
        
        Point expectedCornerMC = new Point(712,656);
        Point actualCornerMC = context.getModelCoordinate(new Point(1024,1024));
        assertTrue(equals(expectedCornerMC, actualCornerMC));
        
        //expected X scale is now 2x
        double expectedXscale = 2.0;
        double actualXscale = context.getScaleX();
        assertTrue(equals(expectedXscale, actualXscale));
        //expected Y scale is now 4x
        double expectedYscale = 4.0;
        double actualYscale = context.getScaleY();
        assertTrue(equals(expectedYscale, actualYscale));
        
        //now zoomIn, the new origin should still be 100,100 in model coords,
        //but the new maxX, maxY is 356, 228

        context.zoomIn();

        expectedOriginMC = new Point(425,813);
        assertTrue(equals(expectedOriginMC,context.getModelCoordinate(new Point(100,100))));
        expectedCornerMC = new Point(489,829);
        assertTrue(equals(expectedCornerMC,context.getModelCoordinate(new Point(356,228))));
        */
    }

    @Test
    @Ignore("Disabled pending test rewrite-RasterizerContext now requires a reference Layer")
    public void testZoomAll() {
        throw new UnsupportedOperationException("Disabled pending test rewrite-RasterizerContext now requires a reference Layer");
        /*
        RasterizerContext context = new RasterizerContext(1024, 1024, 1024, 1024);
        context.zoomAll(false);
        
        assertTrue(equals(0.25, context.getScaleX()));
        assertTrue(equals(0.5, context.getScaleY()));
        
        //check that the origin was reset to 0,0 and that view point 100,100 is now model point 400,200 (1/4 scale by 1/2 scale)
        Point expectedOrigin = new Point(0,0);
        assertTrue(equals(expectedOrigin, context.getModelCoordinate(new Point(0,0))));
        Point expectedPoint = new Point(400,200);
        assertTrue(equals(expectedPoint, context.getModelCoordinate(new Point(100,100))));
                
        //Now, repeat the above exercise after creating a new context and selecting a rectangle
        */
    }
    
    private boolean equals(Point arg1, Point arg2) {
        if (Math.abs(arg1.getX() - arg2.getX()) < EPSILON &&
                Math.abs(arg1.getY() - arg2.getY()) < EPSILON) {
            return true;
        } else {
            System.out.println("Warning - expected Point("+arg1.getX()+","+arg1.getY()+") does not equal actual Point("
                    +arg2.getX()+","+arg2.getY()+")");
            return false;
        }
    }

    private boolean equals(double arg1, double arg2) {
        return Math.abs(arg1 - arg2) < EPSILON;
    }
}