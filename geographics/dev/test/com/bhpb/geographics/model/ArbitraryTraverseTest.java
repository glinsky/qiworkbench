/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.model;

import org.junit.Test;
import com.bhpb.geographics.util.LayerFactory;
import com.bhpb.geoio.datasystems.ArbTravKeyRange;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import java.awt.geom.Point2D;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.*;

public class ArbitraryTraverseTest {
    private int[] testPointX = {0, 100, 50, 75, 200};
    private int[] testPointY = {0, 0, 50, 100, 150};

    private int[] testHeaderValX = {0, 0, 0, 0, 0};
    private int[] testHeaderValY = {0, 0, 25, 50, 75};

    @Test
    public void testConstructorWithValidLayer2keys() {
        Layer testSeismicTranspose = LayerFactory.createSeismicLayer(GeoDataOrder.MAP_VIEW_ORDER, 0, 3, 8080);
        System.out.println("ORIENTATION: " + testSeismicTranspose.getOrientation());
        ArbitraryTraverse arbTrav = new ArbitraryTraverse(testSeismicTranspose);
        assertNotNull(arbTrav);

        assertEquals("cdp", arbTrav.getHorizontalKeyName());
        assertEquals("ep", arbTrav.getVerticalKeyName());
    }

    @Test (expected = IllegalArgumentException.class)
    public void testConstructorWithInvalidLayerOrientation() {
        Layer testSeismicXsec = LayerFactory.createSeismicLayer(GeoDataOrder.CROSS_SECTION_ORDER, 0, 2, 8080);
        ArbitraryTraverse arbTrav = new ArbitraryTraverse(testSeismicXsec);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testConstructorWithInvalidNumKeys_2() {
        Layer testSeismicXsec = LayerFactory.createSeismicLayer(GeoDataOrder.MAP_VIEW_ORDER, 0, 2, 8080);
        ArbitraryTraverse arbTrav = new ArbitraryTraverse(testSeismicXsec);
    }

    @Test
    public void testConstructorWithValidLayer4keys() {
        Layer testSeismicTranspose = LayerFactory.createSeismicLayer(GeoDataOrder.MAP_VIEW_ORDER, 0, 4, 8080);
        System.out.println("ORIENTATION: " + testSeismicTranspose.getOrientation());
        ArbitraryTraverse arbTrav = new ArbitraryTraverse(testSeismicTranspose);
        assertNotNull(arbTrav);

        assertEquals("cdp", arbTrav.getHorizontalKeyName());
        assertEquals("ep", arbTrav.getVerticalKeyName());
    }

    private void addTestPoints(ArbitraryTraverse arbTrav, Layer layer) {
        for (int i = 0; i < testPointX.length; i++) {
            arbTrav.addPoint(ViewTarget.createTargetProps(layer, testPointX[i], testPointY[i]));
        }
    }

    @Test
    public void addAndClearPoints() {
        Layer testSeismicTranspose = LayerFactory.createSeismicLayer(GeoDataOrder.MAP_VIEW_ORDER, 0, 3, 8080);
        System.out.println("ORIENTATION: " + testSeismicTranspose.getOrientation());
        ArbitraryTraverse arbTrav = new ArbitraryTraverse(testSeismicTranspose);
        assertNotNull(arbTrav);
        assertFalse(arbTrav.isDefined());

        addTestPoints(arbTrav, testSeismicTranspose);
        
        assertTrue(arbTrav.isDefined());

        List<Point2D> targetPoints = arbTrav.getPoints();

        assertEquals(5, targetPoints.size());

        for (int i = 0; i < targetPoints.size(); i++) {
            System.out.println("targetPoint[" + i + "] x: " +
                    (int)targetPoints.get(i).getX() + " y: " +
                    (int)targetPoints.get(i).getY());
        }

        for (int i = 0; i < targetPoints.size(); i++) {
            assertEquals(testPointX[i], (int)targetPoints.get(i).getX());
            assertEquals(testPointY[i]+1, (int)targetPoints.get(i).getY());
        }

        arbTrav.clearPoints();
        targetPoints = arbTrav.getPoints();
        assertEquals(0, targetPoints.size());
    }

    @Test
    public void getHeaderKeyMap() {
        Layer testSeismicTranspose = LayerFactory.createSeismicLayer(GeoDataOrder.MAP_VIEW_ORDER, 0, 3, 8080);
        System.out.println("ORIENTATION: " + testSeismicTranspose.getOrientation());
        ArbitraryTraverse arbTrav = new ArbitraryTraverse(testSeismicTranspose);
        assertNotNull(arbTrav);
        assertFalse(arbTrav.isDefined());

        addTestPoints(arbTrav, testSeismicTranspose);

        assertTrue(arbTrav.isDefined());

        Map<String, ArbTravKeyRange> hkMap = arbTrav.getHeaderKeyMap();
        assertEquals(2, hkMap.size());

        //now test that the arb trav associated w/ horizontal key has the correct values
        verifyArbTravValues(hkMap, "cdp", testHeaderValX);
        //finally, verify the vertical arbtrav values
        verifyArbTravValues(hkMap, "ep", testHeaderValY);
    }

    private void verifyArbTravValues(Map<String, ArbTravKeyRange> hkMap, String keyName, int[] testValArray) {
        ArbTravKeyRange keyRange = hkMap.get(keyName);
        assertEquals(testValArray.length, keyRange.getArbTravSize());

        System.out.println(keyName + " scriptStr: " + keyRange.toString());

        for (int i = 0; i < testValArray.length; i++) {
            assertEquals(testValArray[i], keyRange.getArbTravVal(i));
        }
    }
}