/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import com.bhpb.geoio.datasystems.DataSystemConstants.GeoKeyType;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.datasystems.UnlimitedKeyRange;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class KeyRangeSerializerTest {
    private static final Logger logger = Logger.getLogger(KeyRangeSerializerTest.class.toString());
    
    private static final double MIN = 100.0;
    private static final double MAX = 1000.0;
    private static final double INCR = 25.0;
    private static final GeoKeyType KEY_TYPE 
            = GeoKeyType.REGULAR_TYPE;
    
    private DiscreteKeyRange fullKeyRange;
    
    @Before
    public void setUp() { //FullKeyRange = 100 - 1000 [25]
        fullKeyRange = new DiscreteKeyRange(MIN, MAX, INCR, KEY_TYPE);
    }
    
    @After
    public void tearDown() {
        // do nothing
    }
    
    @Test
    public void testWildCard() {
        KeyRangeParseResult result = KeyRangeSerializer.parseRange("*",fullKeyRange);
        assertNoErrors(result);
        assertTrue(result.getKeyRange() instanceof UnlimitedKeyRange);
        assertEquals(result.getKeyRange().getFullKeyRange(), new DiscreteKeyRange(100,1000,25,fullKeyRange.getKeyType()));        
    }
    
    @Test
    public void testSingleValue() {
        KeyRangeParseResult result = KeyRangeSerializer.parseRange("200",fullKeyRange);
        assertNoErrors(result);
        assertEquals((DiscreteKeyRange)result.getKeyRange(), new DiscreteKeyRange(200,200,fullKeyRange.getIncr(),fullKeyRange.getKeyType()));
    }
    
    @Test
    public void testTwoValues() {
        KeyRangeParseResult result = KeyRangeSerializer.parseRange("200 - 400",fullKeyRange);
        assertNoErrors(result);
        assertEquals((DiscreteKeyRange)result.getKeyRange(), new DiscreteKeyRange(200,400,fullKeyRange.getIncr(),fullKeyRange.getKeyType()));        
    }
    
    @Test
    public void testThreeValues() {
        KeyRangeParseResult result = KeyRangeSerializer.parseRange("200 - 400 [50]",fullKeyRange);
        assertNoErrors(result);
        assertEquals((DiscreteKeyRange)result.getKeyRange(), new DiscreteKeyRange(200,400,50,fullKeyRange.getKeyType()));                
    }
    
    @Test
    public void testMoreThanThreeValues() {
        KeyRangeParseResult result = KeyRangeSerializer.parseRange("200 - 400 [50] 1234",fullKeyRange);
        assertHasErrors(result);
    }
    
    @Test
    public void testEmptyString() {
        KeyRangeParseResult result = KeyRangeSerializer.parseRange("",fullKeyRange);
        assertHasErrors(result);        
    }
    
    @Test (expected = NullPointerException.class)
    public void testNullString() {
        KeyRangeParseResult result = KeyRangeSerializer.parseRange(null,fullKeyRange);
        assertHasErrors(result);        
    }
    
    @Test
    public void testMaxTooLarge() {
        KeyRangeParseResult result = KeyRangeSerializer.parseRange("100 - 1001 [25]",fullKeyRange);
        assertHasErrors(result);                
    }
    
    @Test
    public void testMinGreaterThanMaxInvalid() {
        KeyRangeParseResult result = KeyRangeSerializer.parseRange("1000 - 100 [25]",fullKeyRange);
        assertHasErrors(result);        
    }

    @Test
    //per bhpViewer min > max is valid if incr is negative
    public void testMinGreaterThanMaxValid() {
        KeyRangeParseResult result = KeyRangeSerializer.parseRange("1000 - 100 [-25]",fullKeyRange);
        assertNoErrors(result);
    }
    
    @Test
    public void testBadIncrValue() {        
        KeyRangeParseResult result = KeyRangeSerializer.parseRange("100 - 1000 [26]",fullKeyRange);
        assertHasErrors(result);        
    }
    
    @Test
    public void testUnrecognizedDelimiters() {
        KeyRangeParseResult result = KeyRangeSerializer.parseRange("100 = 1000 [25]",fullKeyRange);
        assertHasErrors(result);        
    }
    
    @Test
    public void testDelimitersOutOfOrder() {
        KeyRangeParseResult result = KeyRangeSerializer.parseRange("100 - 1000 ]25[",fullKeyRange);
        assertHasErrors(result);                
    }
    
    @Test
    public void testRangeWithFloats() {
        KeyRangeParseResult result = KeyRangeSerializer.parseRange("100.0 - 1000.0 [25.0]",fullKeyRange);
        assertNoErrors(result);
    }

    @Test
    /**
     * Incorrect FLOAT regex resulted in 100.000 matching tokens FLOAT, FLOAT with values 100.0 and 00.
     * The correct match should have been a single FLOAT with value 100.000.
     */
    public void testMultipleDecimalPlaces() {
        KeyRangeParseResult result = KeyRangeSerializer.parseRange("100.000 - 1000.000 [25.0]",fullKeyRange);
        assertNoErrors(result);
    }

    private void assertHasErrors(KeyRangeParseResult result) {
        if (!result.hasErrors()) {
            String errMsg = "Errors were expected but KeyRangeParseResult.hasErrors() == false.";
            logger.info(errMsg);
            fail(errMsg);
        }
    }
    
    private void assertNoErrors(KeyRangeParseResult result) {
        if (result.hasErrors()) {
            String errMsg = "KeyRangeParseResult had errors when none were expected.";
            logger.info("Parser errors: " + result.getParserErrors());
            logger.info("IntParse errors: " + result.getIntParseErrors());
            logger.info("Compatibility errors: " + result.getCompatibilityErrors());
            logger.info(errMsg);
            fail(errMsg);
        }
    }
    
    private void assertEquals(DiscreteKeyRange chosenRange, DiscreteKeyRange expectedRange) {
        assertTrue("chosen KeyRange.min " + chosenRange.getMin() + " did not equal expected KeyRange.min " + expectedRange.getMin(),
                chosenRange.getMin() == expectedRange.getMin());
        assertTrue("chosen KeyRange.max " + chosenRange.getMax() + " did not equal expected KeyRange.max " + expectedRange.getMax(),
                chosenRange.getMax() == expectedRange.getMax());
        assertTrue("chosen KeyRange.incr " + chosenRange.getIncr() + " did not equal expected KeyRange.incr " + expectedRange.getIncr(),
                chosenRange.getIncr() == expectedRange.getIncr());
        assertTrue("chosen KeyRange.keyType " + chosenRange.getKeyType() + " did not equal expected KeyRange.keyType " + expectedRange.getKeyType(),
                chosenRange.getKeyType() == expectedRange.getKeyType());
    }
}