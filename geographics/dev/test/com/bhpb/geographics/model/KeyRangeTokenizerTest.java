/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import junit.framework.TestCase;

/**
 *
 * @author folsw9
 */
public class KeyRangeTokenizerTest extends TestCase {
    KeyRangeTokenizer tokenizer = null;
    
    /** Creates a new instance of KeyRangeTokenizerTest */
    public KeyRangeTokenizerTest() {
    }
    
    @Override
    public void setUp() {
        tokenizer = new KeyRangeTokenizer();
    }
    
    @Override
    public void tearDown() {
        
    }
    
    public void testEmptyStringMatch() {
        KeyRangeToken[] tokens = tokenizer.getTokens("");
        assertTrue(tokens.length == 0);
    }
    
    public void testValidMatch() {
        KeyRangeToken[] tokens = tokenizer.getTokens("1 - 100 [50]");
        //printTokens(tokens);
        assertTrue("tokens.length != 9, lokens.length == " + tokens.length, tokens.length == 9);
        assertTrue("".equals(tokenizer.getParserErrors(tokens)));
    }
    
    public void testInvalidMatch() {
        KeyRangeToken[] tokens = tokenizer.getTokens("1 - 100 [50}");
        //printTokens(tokens);
        assertTrue(tokens.length == 9);
        assertFalse("Unknown character '}' at position 11".equals(tokenizer.getParserErrors(tokens)));
    }
    
    public void testAlphabetMatch() {
        KeyRangeToken[] tokens = tokenizer.getTokens("abcdefg");
        //printTokens(tokens);
        assertTrue(tokens.length == 7);
        //assertFalse("Unknown character '}' at position 11".equals(tokenizer.getParserErrors(tokens)));
        printTokens(tokens);
        System.out.println(KeyRangeTokenizer.getParserErrors(tokens));
    }
        
    private void printTokens(KeyRangeToken[] tokens) {
        
        for (int tIndex = 0; tIndex < tokens.length; tIndex++) {
            KeyRangeToken token = tokens[tIndex];
            String tokenString = token.toString();
            if ("".equals(tokenString)) {
                tokenString = token.getToken();
            }
            System.out.println("token " + tIndex + ": " + tokenString);
        }
    }
}