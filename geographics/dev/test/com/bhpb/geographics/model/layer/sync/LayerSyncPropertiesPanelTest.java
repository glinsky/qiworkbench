/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.sync;

import com.bhpb.geographics.controller.layer.sync.SyncPropertiesController;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties.SYNC_FLAG;
import com.bhpb.geographics.ui.layer.sync.LayerSyncPropertiesPanel;
import com.bhpb.geographics.ui.layer.sync.LayerSyncPropertiesPanelFactory;
import com.bhpb.geographics.util.FestUtils;
import javax.swing.JDialog;
import junit.framework.TestCase;
import org.fest.swing.core.BasicRobot;
import org.fest.swing.core.Robot;
import org.fest.swing.core.Settings;
import org.fest.swing.fixture.ContainerFixture;
import org.fest.swing.fixture.DialogFixture;

/**
 *
 * @author folsw9
 */
public class LayerSyncPropertiesPanelTest extends TestCase {
    private Robot robot;
     
    @Override
    public void setUp() {
        robot = BasicRobot.robotWithNewAwtHierarchy();
        new Settings().delayBetweenEvents(500);    
    }
    
    public void testMVCinit() throws InterruptedException {
        LayerSyncProperties model = new SeismicXsecSyncProperties();
        LayerSyncPropertiesPanel view = LayerSyncPropertiesPanelFactory.createSeismicXsecSyncPanel();
        JDialog dlg = new JDialog();
        dlg.setName("testDlg");
        dlg.getContentPane().add(view);
        SyncPropertiesController controller = new SyncPropertiesController(model, view);
        
        new Thread(controller).run();
        
        dlg.pack();
        dlg.setVisible(true);
        
        Thread.sleep(2000);
        DialogFixture dlgFixture = FestUtils.getDialog(robot, "testDlg");
        ContainerFixture panelFixture = dlgFixture.panel("LayerSyncPropertiesPanel").panel("CentralPanel").panel("CheckBoxPanel");
        boolean colorInterpCheckBoxEnabled = panelFixture.checkBox(SYNC_FLAG.COLOR_INTERP.toString()).target.isEnabled();
        boolean visibilityCheckBoxEnabled = panelFixture.checkBox(SYNC_FLAG.VISIBILITY.toString()).target.isEnabled();
       
        assertFalse(colorInterpCheckBoxEnabled);
        assertTrue(visibilityCheckBoxEnabled);
    }
}