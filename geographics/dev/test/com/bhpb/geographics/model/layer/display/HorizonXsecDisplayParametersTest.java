/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.display;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters.LINE_STYLE;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters.COLOR;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters.SYMBOL_STYLE;
import junit.framework.TestCase;

public class HorizonXsecDisplayParametersTest extends TestCase {

    private String layerName = "p1";
    private Boolean drawLine = true;
    private LINE_STYLE lineStyle = LINE_STYLE.SOLID;
    private Integer lineWidth = 1;
    private COLOR lineColor = COLOR.BLACK;
    private Boolean drawSymbol = Boolean.TRUE;
    private SYMBOL_STYLE symbolStyle = SYMBOL_STYLE.STAR;
    private Double symbolWidth = 12.0;
    private Double symbolHeight = 12.0;
    private COLOR symbolColor = COLOR.BLACK;
    private HorizonXsecDisplayParameters ldParams;

    public HorizonXsecDisplayParametersTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        ldParams = new HorizonXsecDisplayParameters();
        ldParams.setLayerName(layerName);
        ldParams.setDrawLine(drawLine);
        ldParams.setLineStyle(lineStyle);
        ldParams.setLineWidth(lineWidth);
        ldParams.setLineColor(lineColor);
        ldParams.setDrawSymbol(drawSymbol);
        ldParams.setSymbolStyle(symbolStyle);
        ldParams.setSymbolHeight(symbolHeight);
        ldParams.setSymbolWidth(symbolWidth);
        ldParams.setSymbolColor(symbolColor);
    }

    @Override
    protected void tearDown() throws Exception {
    }

    public void testAccessors() {
        assertEquals(layerName, ldParams.getLayerName());
        assertTrue(drawLine == ldParams.getDrawLine());
        assertEquals(lineStyle, ldParams.getLineStyle());
        assertTrue(lineWidth == ldParams.getLineWidth());
        assertEquals(HorizonXsecDisplayParameters.getColor(lineColor), ldParams.getLineColor());
        assertTrue(drawSymbol == ldParams.getDrawSymbol());
        assertEquals(symbolStyle, ldParams.getSymbolStyle());
        assertTrue(symbolHeight == ldParams.getSymbolHeight());
        assertTrue(symbolWidth == ldParams.getSymbolWidth());
        assertEquals(HorizonXsecDisplayParameters.getColor(symbolColor), ldParams.getSymbolColor());
    }
    
    public void testPropertyAccessorGettersAndSetters() throws PropertyAccessorException {
        ldParams = new HorizonXsecDisplayParameters();
        ldParams.setProperty("LayerName", layerName);
        ldParams.setProperty("DrawLine", FieldProcessor.BOOLEAN_TRUE_STRING);
        ldParams.setProperty("LineStyle",LINE_STYLE.SOLID.toString());
        ldParams.setProperty("LineWidth", lineWidth.toString());
        ldParams.setProperty("LineColor", lineColor.toString());
        ldParams.setProperty("DrawSymbol", FieldProcessor.BOOLEAN_FALSE_STRING);
        ldParams.setProperty("SymbolStyle", symbolStyle.toString());
        ldParams.setProperty("SymbolHeight", symbolHeight.toString());
        ldParams.setProperty("SymbolWidth", symbolWidth.toString());
        ldParams.setProperty("SymbolColor", symbolColor.toString());
        
        assertEquals(layerName, ldParams.getProperty("LayerName"));
        assertEquals(FieldProcessor.BOOLEAN_TRUE_STRING, ldParams.getProperty("DrawLine"));
        assertEquals(lineStyle.toString(), ldParams.getProperty("LineStyle"));
        assertEquals(lineWidth.toString(), ldParams.getProperty("LineWidth"));
        assertEquals(HorizonXsecDisplayParameters.getColor(lineColor).toString(), ldParams.getProperty("LineColor"));
        assertEquals(FieldProcessor.BOOLEAN_FALSE_STRING, ldParams.getProperty("DrawSymbol"));
        assertEquals(symbolStyle.toString(), ldParams.getProperty("SymbolStyle"));
        assertEquals(symbolWidth.toString(), ldParams.getProperty("SymbolWidth"));
        assertEquals(symbolHeight.toString(), ldParams.getProperty("SymbolHeight"));
        assertEquals(HorizonXsecDisplayParameters.getColor(symbolColor).toString(), ldParams.getProperty("SymbolColor"));
    }
}