/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.display;

import com.bhpb.geographics.util.ColorMap;
import junit.framework.TestCase;

public class MapDisplayParametersTest extends TestCase {

    private MapDisplayParameters ldParams;
    
    private final String layerName = "gathers";
    private final String polarity = "Normal";
    private final double horizontalScale = 1.0;
    private final double verticalScale = 1.0;
    private final double opacity = 1.0;
    private final boolean epReversed = false;
    private final boolean cdpReversed = false;
    private final boolean transpose = false;
    private final double colorMapMin=0.1277;
    private final double colorMapMax=0.258;

    public MapDisplayParametersTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        ldParams = new MapDisplayParameters(ColorMap.DEFAULT_COLORMAP);

        ldParams.setLayerName(layerName);
        ldParams.getBasicLayerProperties().setPolarity(polarity);
        ldParams.getBasicLayerProperties().setHorizontalScale(horizontalScale);
        ldParams.getBasicLayerProperties().setVerticalScale(verticalScale);
        ldParams.getBasicLayerProperties().setOpacity(opacity);
        
        ldParams.getDisplayAttributes().setEpReversed(epReversed);
        ldParams.getDisplayAttributes().setCdpReversed(cdpReversed);
        ldParams.getDisplayAttributes().setTransposed(transpose);
        
        ldParams.getColorAttributes().setMin(colorMapMin);
        ldParams.getColorAttributes().setMax(colorMapMax);
    }

    @Override
    protected void tearDown() throws Exception {
    }

    public void testAccessors() {
        assertTrue(layerName.equals(ldParams.getLayerName()));
        assertTrue(polarity.equals(ldParams.getBasicLayerProperties().getPolarity()));
        assertEquals(horizontalScale, ldParams.getBasicLayerProperties().getHorizontalScale());
        assertEquals(verticalScale, ldParams.getBasicLayerProperties().getVerticalScale());
        assertEquals(opacity, ldParams.getBasicLayerProperties().getOpacity());
        
        assertTrue(epReversed == ldParams.getDisplayAttributes().getEpReversed());
        assertTrue(cdpReversed == ldParams.getDisplayAttributes().getCdpReversed());
        assertTrue(transpose == ldParams.getDisplayAttributes().isTransposed());
        
        assertTrue(colorMapMin == ldParams.getColorAttributes().getMin());
        assertTrue(colorMapMax == ldParams.getColorAttributes().getMax());
    }
}