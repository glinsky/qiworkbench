/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */

package com.bhpb.geographics.model.layer.display;

import junit.framework.TestCase;

public class LocalSubsetPropertiesFactoryTest extends TestCase {
    final String pkField = "TRACE_ID";
    final int pkStartValue = 1;
    final int pkEndValue=3215;
    final int pkStepValue=1;
    final boolean applyGaps=true;
    final int gapSize=5;
    
    final String skValue="NONE";
    final double skStartValue = Double.NEGATIVE_INFINITY;
    final double skEndValue = Double.POSITIVE_INFINITY;
    final double skStepValue = Double.NaN;
    
    final int startTime = 0;
    final int endTime = 500;
    
    public LocalSubsetPropertiesFactoryTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
    }

    @Override
    protected void tearDown() throws Exception {
    }

    public void testCreateLocalSubsetProperties() {
        LocalSubsetProperties lsProps = LocalSubsetPropertiesFactory.createLocalSubsetPropertiesTestData(3215);
        
        assertTrue(pkField.equals(lsProps.getPrimaryKeyField()));
        
        assertEquals(pkStartValue, lsProps.getPrimaryKeyStartValue());
        assertEquals(pkEndValue, lsProps.getPrimaryKeyEndValue());
        assertEquals(pkStepValue, lsProps.getPrimaryKeyStepValue());
        assertEquals(applyGaps, lsProps.doApplyGaps());
        assertEquals(gapSize, lsProps.getGapSize());
        
        assertEquals(skValue, lsProps.getSecondaryKeyValue());
        assertEquals(skStartValue, lsProps.getSecondaryKeyStartValue());
        assertEquals(skEndValue, lsProps.getSecondaryKeyEndValue());
        assertEquals(skStepValue, lsProps.getSecondaryKeyStepValue());
    }
    
}
