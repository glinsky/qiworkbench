/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.display;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geographics.model.layer.shared.Normalization.NORMALIZATION_TYPE;
import junit.framework.TestCase;

public class ModelXsecDisplayParametersTest extends TestCase {

    private ModelXsecDisplayParameters ldParams;
    private final String layerName = "gathers";
    private final String polarity = "Normal";
    private final double horizontalScale = 1.0;
    private final double verticalScale = 1.0;
    private final double opacity = 1.0;
    private final double normalizationScale = 0.2;
    private final NORMALIZATION_TYPE normalizationType = NORMALIZATION_TYPE.RMS;
    private final double normalizationMinValue = -0.005;
    private final double normalizationMaxValue = 0.004;
    private final Boolean rasterizeWiggleTraces = false;
    private final Boolean rasterizePositiveFill = false;
    private final Boolean rasterizeNegativeFill = false;
    private final Boolean rasterizeVariableDensity = false;
    private final Boolean rasterizeInterpolatedDensity = false;
    private final int clippingFactor = 4;
    private final int decimationSpacing = 5;
    private final ColorMap colorMap = new ColorMap(ColorMap.generateDefaultColorMap());
    private final boolean doStructuralInterpolation = true;
    private final double startTime = 2338.1543;
    private final double endTime = 2722.1406;

    public ModelXsecDisplayParametersTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
    }

    @Override
    protected void tearDown() throws Exception {
    }

    public void testAccessors() {
        ldParams = new ModelXsecDisplayParameters();

        ldParams.setLayerName(layerName);
        ldParams.getBasicLayerProperties().setPolarity(polarity);
        ldParams.getBasicLayerProperties().setHorizontalScale(horizontalScale);
        ldParams.getBasicLayerProperties().setVerticalScale(verticalScale);
        ldParams.getBasicLayerProperties().setOpacity(opacity);
        ldParams.getNormalizationProperties().setNormalizationScale(normalizationScale);
        ldParams.getNormalizationProperties().setNormalizationType(normalizationType);
        ldParams.getNormalizationProperties().setNormalizationMinValue(normalizationMinValue);
        ldParams.getNormalizationProperties().setNormalizationMaxValue(normalizationMaxValue);
        ldParams.getRasterizationTypeModel().setRasterizeWiggleTraces(rasterizeWiggleTraces);
        ldParams.getRasterizationTypeModel().setRasterizePositiveFill(rasterizePositiveFill);
        ldParams.getRasterizationTypeModel().setRasterizeNegativeFill(rasterizeNegativeFill);
        ldParams.getRasterizationTypeModel().setRasterizeVariableDensity(rasterizeVariableDensity);
        ldParams.getRasterizationTypeModel().setRasterizeInterpolatedDensity(rasterizeInterpolatedDensity);
        ldParams.getCulling().setClippingFactor(clippingFactor);
        ldParams.getCulling().setDecimationSpacing(decimationSpacing);
        ldParams.getModelVerticalRange().setStartTime(startTime);
        ldParams.getModelVerticalRange().setEndTime(endTime);
        ldParams.getModelVerticalRange().setStructuralInterpolationEnabled(doStructuralInterpolation);
        ldParams.setColorMap(colorMap);

        assertTrue(layerName.equals(ldParams.getLayerName()));
        assertTrue(polarity.equals(ldParams.getBasicLayerProperties().getPolarity()));
        assertEquals(horizontalScale, ldParams.getBasicLayerProperties().getHorizontalScale());
        assertEquals(verticalScale, ldParams.getBasicLayerProperties().getVerticalScale());
        assertEquals(opacity, ldParams.getBasicLayerProperties().getOpacity());
        assertEquals(normalizationScale, ldParams.getNormalizationProperties().getNormalizationScale());
        assertEquals(normalizationMinValue, ldParams.getNormalizationProperties().getNormalizationMinValue());
        assertEquals(normalizationMaxValue, ldParams.getNormalizationProperties().getNormalizationMaxValue());

        assertTrue(doStructuralInterpolation == ldParams.getModelVerticalRange().isStructuralInterpolationEnabled());
        assertTrue(startTime == ldParams.getModelVerticalRange().getStartTime());
        assertTrue(endTime == ldParams.getModelVerticalRange().getEndTime());

        assertEquals(rasterizeWiggleTraces, ldParams.getRasterizationTypeModel().doRasterizeWiggleTraces());
        assertEquals(rasterizePositiveFill, ldParams.getRasterizationTypeModel().doRasterizeNegativeFill());
        assertEquals(rasterizeNegativeFill, ldParams.getRasterizationTypeModel().doRasterizeNegativeFill());
        assertEquals(rasterizeVariableDensity, ldParams.getRasterizationTypeModel().doRasterizeVariableDensity());
        assertEquals(rasterizeInterpolatedDensity, ldParams.getRasterizationTypeModel().doRasterizeInterpolatedDensity());
        assertTrue(clippingFactor == ldParams.getCulling().getClippingFactor());
        assertTrue(decimationSpacing == ldParams.getCulling().getDecimationSpacing());
        assertTrue(colorMap.equals(ldParams.getColorMap()));
    }

    public void testPropertyGettersAndSetters() throws PropertyAccessorException {
        ldParams = new ModelXsecDisplayParameters();

        ldParams.setProperty("LayerName", layerName);
        ldParams.setProperty("Layer.Polarity", polarity);
        ldParams.setProperty("Layer.HorizontalScaleMultiplier",Double.toString(horizontalScale));
        ldParams.setProperty("Layer.VerticalScaleMultiplier", Double.toString(verticalScale));
        ldParams.setProperty("Layer.Opacity", Double.toString(opacity));
        
        ldParams.setProperty("Normalization.Scale", Double.toString(normalizationScale));
        ldParams.setProperty("Normalization.Type", normalizationType.toString());
        ldParams.setProperty("Normalization.Min_Value", Double.toString(normalizationMinValue));
        ldParams.setProperty("Normalization.Max_Value", Double.toString(normalizationMaxValue));
        
        ldParams.setProperty("RasterizingType.Wiggle_Trace", FieldProcessor.valueOf(rasterizeWiggleTraces));
        ldParams.setProperty("RasterizingType.Positive_Fill", FieldProcessor.valueOf(rasterizePositiveFill));
        ldParams.setProperty("RasterizingType.Negative_Fill", FieldProcessor.valueOf(rasterizeNegativeFill));
        ldParams.setProperty("RasterizingType.Variable_Density", FieldProcessor.valueOf(rasterizeVariableDensity));
        ldParams.setProperty("RasterizingType.Interpolated_Density", FieldProcessor.valueOf(rasterizeInterpolatedDensity));
        
        ldParams.setProperty("Culling.ClippingFactor", Integer.toString(clippingFactor));
        ldParams.setProperty("Culling.DecimationSpacing", Integer.toString(decimationSpacing));
        
        ldParams.setProperty("ModelVerticalRange.StartTime", Double.toString(startTime));
        ldParams.setProperty("ModelVerticalRange.EndTime", Double.toString(endTime));
        ldParams.setProperty("ModelVerticalRange.StructuralInterpolation", FieldProcessor.BOOLEAN_TRUE_STRING);
        
        assertTrue(layerName.equals(ldParams.getLayerName()));
        assertTrue(polarity.equals(ldParams.getBasicLayerProperties().getPolarity()));
        assertEquals(horizontalScale, ldParams.getBasicLayerProperties().getHorizontalScale());
        assertEquals(verticalScale, ldParams.getBasicLayerProperties().getVerticalScale());
        assertEquals(opacity, ldParams.getBasicLayerProperties().getOpacity());
        assertEquals(normalizationScale, ldParams.getNormalizationProperties().getNormalizationScale());
        assertEquals(normalizationMinValue, ldParams.getNormalizationProperties().getNormalizationMinValue());
        assertEquals(normalizationMaxValue, ldParams.getNormalizationProperties().getNormalizationMaxValue());

        assertTrue(doStructuralInterpolation == ldParams.getModelVerticalRange().isStructuralInterpolationEnabled());
        assertTrue(startTime == ldParams.getModelVerticalRange().getStartTime());
        assertTrue(endTime == ldParams.getModelVerticalRange().getEndTime());

        assertEquals(rasterizeWiggleTraces, ldParams.getRasterizationTypeModel().doRasterizeWiggleTraces());
        assertEquals(rasterizePositiveFill, ldParams.getRasterizationTypeModel().doRasterizePositiveFill());
        assertEquals(rasterizeNegativeFill, ldParams.getRasterizationTypeModel().doRasterizeNegativeFill());
        assertEquals(rasterizeVariableDensity, ldParams.getRasterizationTypeModel().doRasterizeVariableDensity());
        assertEquals(rasterizeInterpolatedDensity, ldParams.getRasterizationTypeModel().doRasterizeInterpolatedDensity());
        assertTrue(clippingFactor == ldParams.getCulling().getClippingFactor());
        assertTrue(decimationSpacing == ldParams.getCulling().getDecimationSpacing());

        ldParams.setProperty("RasterizingType.*", FieldProcessor.BOOLEAN_FALSE_STRING);
        assertEquals(Boolean.FALSE, ldParams.getRasterizationTypeModel().doRasterizeWiggleTraces());
        assertEquals(Boolean.FALSE, ldParams.getRasterizationTypeModel().doRasterizePositiveFill());
        assertEquals(Boolean.FALSE, ldParams.getRasterizationTypeModel().doRasterizeNegativeFill());
        assertEquals(Boolean.FALSE, ldParams.getRasterizationTypeModel().doRasterizeVariableDensity());
        assertEquals(Boolean.FALSE, ldParams.getRasterizationTypeModel().doRasterizeInterpolatedDensity());

        ldParams.setProperty("RasterizingType.*", FieldProcessor.BOOLEAN_TRUE_STRING);
        assertEquals(Boolean.TRUE, ldParams.getRasterizationTypeModel().doRasterizeWiggleTraces());
        assertEquals(Boolean.TRUE, ldParams.getRasterizationTypeModel().doRasterizePositiveFill());
        assertEquals(Boolean.TRUE, ldParams.getRasterizationTypeModel().doRasterizeNegativeFill());
        assertEquals(Boolean.TRUE, ldParams.getRasterizationTypeModel().doRasterizeVariableDensity());
        assertEquals(Boolean.TRUE, ldParams.getRasterizationTypeModel().doRasterizeInterpolatedDensity());
    }
}