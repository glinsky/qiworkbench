/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.window;

import com.bhpb.geographics.util.DatasetPropertiesFactory;
import com.bhpb.geographics.util.ScaleUtil;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import junit.framework.TestCase;

public class ScalePropertiesTest extends TestCase {
    private final double EXPECTED_SEISMIC_DEFAULT_HSCALE = 19.291338582677167;
    private final double EXPECTED_SEISMIC_XSEC_DEFAULT_VSCALE = 0.003239795918367347;
    private final double EPSILON = 0.0000001;
    
    @Override
    public void setUp() {
        
    }
    
    @Override
    public void tearDown() {
        
    }
    
    public void testDefaultValues() {
        WindowScaleProperties scale = new WindowScaleProperties(DatasetPropertiesFactory.createTestSeismicProperties(GeoDataOrder.CROSS_SECTION_ORDER), new ScaleUtil(98));
        assertTrue(Math.abs(scale.getHorizontalScale() - EXPECTED_SEISMIC_DEFAULT_HSCALE) < EPSILON);
        assertEquals(scale.getHorizontalScaleUnits(), WindowScaleProperties.UNITS.TRACES_PER_CM);
        assertTrue(Math.abs(scale.getVerticalScale() - EXPECTED_SEISMIC_XSEC_DEFAULT_VSCALE) < EPSILON);
        assertEquals(scale.getVerticalScaleUnits(), WindowScaleProperties.UNITS.CMS_PER_UNIT);
        assertFalse(scale.isAspectRatioLocked());
        assertFalse(scale.isSettingAspectRatioLockEnabled());
    }
    
    public void testGettersAndSetters() {
        WindowScaleProperties scale = new WindowScaleProperties(DatasetPropertiesFactory.createTestSeismicProperties(GeoDataOrder.CROSS_SECTION_ORDER), new ScaleUtil(98));
        scale.setHorizontalScale(1.234);
        scale.setVerticalScale(5.678);
        scale.setAspectRatioLocked(true);
        scale.setAspectRatioLockingEnabled(true);
        
        assertEquals(scale.getHorizontalScale(), 1.234);
        assertEquals(scale.getVerticalScale(), 5.678);
        assertTrue(scale.isAspectRatioLocked());
        assertTrue(scale.isSettingAspectRatioLockEnabled());
    }
}