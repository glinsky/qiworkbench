/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.window;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import junit.framework.TestCase;

public class WindowSyncPropertiesTest extends TestCase {

    @Override
    public void setUp() {
    }

    @Override
    public void tearDown() {
    }

    public void testDefaultValues() {
        WindowSyncProperties syncProps = new WindowSyncProperties();
        for (WindowSyncProperties.SYNC_FLAG flag : WindowSyncProperties.SYNC_FLAG.values()) {
            assertTrue(syncProps.isEnabled(flag));
        }
    }

    public void testAccessors() {
        WindowSyncProperties syncProps = new WindowSyncProperties();
        for (WindowSyncProperties.SYNC_FLAG flag : WindowSyncProperties.SYNC_FLAG.values()) {
            syncProps.setEnabled(flag, false);
        }

        for (WindowSyncProperties.SYNC_FLAG flag : WindowSyncProperties.SYNC_FLAG.values()) {
            assertFalse(syncProps.isEnabled(flag));
        }
    }
    
    public void testPropertyGettersAndSetters() throws PropertyAccessorException {
        PropertyAccessor syncProps = new WindowSyncProperties();
        
        //first, test the default value is 'off' (that is, false)
        assertTrue(FieldProcessor.BOOLEAN_TRUE_STRING.equals(syncProps.getProperty(WindowSyncProperties.FIELD.BROADCAST.toString())));
        //now set it to on (true)
        syncProps.setProperty(WindowSyncProperties.FIELD.BROADCAST.toString(), FieldProcessor.BOOLEAN_FALSE_STRING);
        assertTrue(FieldProcessor.BOOLEAN_FALSE_STRING.equals(syncProps.getProperty(WindowSyncProperties.FIELD.BROADCAST.toString())));
        
        //finally, try to set and get an invalid qualified field name and verify that exceptions are thrown
        try {
            syncProps.getProperty("INVALID NAME");
            fail("getProperty('INVALID_NAME') did not result in a PropertyAccessorException being thrown.");
        } catch (PropertyAccessorException pae) {
            System.out.println("Success - getProperty('INVALID_NAME') caused a PropertyAccessorException to be thrown");
        } catch (Exception e) {
            fail("getProperty('INVALID_NAME') did not result in a PropertyAccessorException being thrown.");
        }
        
        try {
            syncProps.setProperty("INVALID NAME", "test value");
            fail("getProperty('INVALID_NAME') did not result in a PropertyAccessorException being thrown.");
        } catch (PropertyAccessorException pae) {
            System.out.println("Success - getProperty('INVALID_NAME') caused a PropertyAccessorException to be thrown");
        } catch (Exception e) {
            fail("getProperty('INVALID_NAME') did not result in a PropertyAccessorException being thrown.");
        }
    }
}