/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.window;

import com.bhpb.geographics.model.Layer;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;

public class WindowPropertiesTest extends TestCase {
    private List<Layer> layerList = new ArrayList<Layer>();
    
    @Override
    public void setUp() {
        //Empty list must be used - see note below regarding why the current
        //test data is now invalid with regard to initializing (CrossSection) AnnotationProperties.
        //layerList.add(LayerFactory.createSeismicLayer(ORIENTATION.XSEC, 0));
    }
    
    @Override
    public void tearDown() {
        
    }
    
    public void testConstructor() {
        assertTrue("LayerList must be empty or else test DataObjects must be corrected to implement ITraceHeader and return at least one header field, 'ep'", 
                layerList.size() == 0);
        WindowProperties windowProps = new WindowProperties(GeoDataOrder.CROSS_SECTION_ORDER);
        assertNotNull(windowProps.getScaleProperties());
        assertNotNull(windowProps.getAnnotationProperties());
        assertNotNull(windowProps.getSyncProperties());
    }
}
