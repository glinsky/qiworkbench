/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.window;

import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.util.LayerFactory;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import junit.framework.TestCase;

public class MapAnnotationPropertiesTest extends TestCase {
    @Override
    public void setUp() {
        
    }
    
    @Override
    public void tearDown() {
        
    }
    
    public void testDefaultValues() {
        MapAnnotationProperties aProps = new MapAnnotationProperties();
        assertTrue(CrossSectionAnnotationProperties.NO_ANNOTATION.equals(aProps.getAnnotatedLayerName()));
        assertEquals(CrossSectionAnnotationProperties.LABEL_LOCATION.NONE, aProps.getTitleLocation());
        assertTrue("".equals(aProps.getTitle()));
        
        assertTrue("".equals(aProps.getLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.LEFT)));
        assertTrue("".equals(aProps.getLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.RIGHT)));
        assertTrue("".equals(aProps.getLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.TOP)));
        assertTrue("".equals(aProps.getLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.BOTTOM)));
        
        assertEquals(CrossSectionAnnotationProperties.LABEL_LOCATION.NONE, aProps.getColorBarLocation());
        
        //assertEquals(0, aProps.getAvailableKeys().length);
        //assertEquals(0, aProps.getSelectedKeys().length);
    }
    
    public void testAccessors() {
        MapAnnotationProperties aProps = new MapAnnotationProperties();
        
        //set values...
        final String LEFTLABELTEXT="left-test";
        final String RIGHTLABELTEXT="right-test";
        final String TOPLABELTEXT="top-test";
        
        aProps.setLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.LEFT, LEFTLABELTEXT);
        aProps.setLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.RIGHT, RIGHTLABELTEXT);
        aProps.setLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.TOP, TOPLABELTEXT);
        //done setting values
        
        //test accessors
        String annotatedLayerName = aProps.getAnnotatedLayerName();
        //System.out.println("Annotated layer name from aProps.getAnnotatedLayerName(): " + annotatedLayerName);
        assertTrue(CrossSectionAnnotationProperties.NO_ANNOTATION.equals(annotatedLayerName));
        
        //now add a seismic layer and verify that the annotated layer name changes from "No Annotation"
        Layer layer = LayerFactory.createSeismicLayer(GeoDataOrder.MAP_VIEW_ORDER,0,3,8080);

        WindowModel windowModel = new WindowModel("Test WindowModel", GeoDataOrder.MAP_VIEW_ORDER);
        windowModel.add(0, layer);

        aProps.setWindowModel(windowModel);

        aProps.addLayer(layer);
        annotatedLayerName = aProps.getAnnotatedLayerName();
        //System.out.println("Annotated layer name from aProps.getAnnotatedLayerName(): " + annotatedLayerName);
        assertTrue("S 1 : gathers".equals(annotatedLayerName));
        
        assertEquals(CrossSectionAnnotationProperties.LABEL_LOCATION.NONE, aProps.getTitleLocation());
        assertTrue("".equals(aProps.getTitle()));
        
        assertTrue(LEFTLABELTEXT.equals(aProps.getLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.LEFT)));
        assertTrue(RIGHTLABELTEXT.equals(aProps.getLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.RIGHT)));
        assertTrue(TOPLABELTEXT.equals(aProps.getLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.TOP)));
        assertTrue("".equals(aProps.getLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.BOTTOM)));
        
        assertEquals(CrossSectionAnnotationProperties.LABEL_LOCATION.NONE, aProps.getColorBarLocation());
        
        //assertEquals(3, aProps.getAvailableKeys().length);
        //assertEquals(0, aProps.getSelectedKeys().length);
        //done testing accessors
        
        //now move a layer from available to selected list
        //aProps.setKeySelected("ep");
        //and test that it is so
        //assertEquals(2, aProps.getAvailableKeys().length);
        //assertEquals(1, aProps.getSelectedKeys().length);
    }
}