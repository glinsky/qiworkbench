/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.window;

import com.bhpb.geographics.model.window.CrossSectionAnnotationProperties.STEP_TYPE;
import junit.framework.TestCase;

public class CrossSectionAnnotationPropertiesTest extends TestCase {

    @Override
    public void setUp() {
        
    }
    
    @Override
    public void tearDown() {
        
    }
    
    public void testDefaultValues() {
        CrossSectionAnnotationProperties aProps = new CrossSectionAnnotationProperties();
        assertTrue(CrossSectionAnnotationProperties.NO_ANNOTATION.equals(aProps.getAnnotatedLayerName()));
        assertEquals(CrossSectionAnnotationProperties.LABEL_LOCATION.NONE, aProps.getTitleLocation());
        assertTrue("".equals(aProps.getTitle()));
        
        assertTrue("".equals(aProps.getLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.LEFT)));
        assertTrue("".equals(aProps.getLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.RIGHT)));
        assertTrue("".equals(aProps.getLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.TOP)));
        assertTrue("".equals(aProps.getLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.BOTTOM)));
        
        assertEquals(CrossSectionAnnotationProperties.LABEL_LOCATION.NONE, aProps.getColorBarLocation());
        
        assertEquals(0, aProps.getAvailableKeys().length);
        assertEquals(0, aProps.getSelectedKeys().length);
        
        assertEquals(CrossSectionAnnotationProperties.LABEL_LOCATION.TOP, aProps.getHorizontalAnnotationLocation());
        assertEquals("", aProps.getSynchronizationKey());
        
        assertEquals(CrossSectionAnnotationProperties.STEP_TYPE.AUTOMATIC, aProps.getStepType());
        assertEquals(CrossSectionAnnotationProperties.LABEL_LOCATION.LEFT, aProps.getVerticalAnnotationLocation());
        assertEquals(CrossSectionAnnotationProperties.LABEL_LOCATION.NONE, aProps.getColorBarLocation());
        
        assertEquals(CrossSectionAnnotationProperties.MAJOR_STEP_DEFAULT, aProps.getMajorStep());
        assertEquals(CrossSectionAnnotationProperties.MINOR_STEP_DEFAULT, aProps.getMinorStep());
    }
    
    public void testAccessors() {
        CrossSectionAnnotationProperties aProps = new CrossSectionAnnotationProperties();
        
        final String LEFTLABELTEXT="left-test";
        final String RIGHTLABELTEXT="right-test";
        final String TOPLABELTEXT="top-test";
        
        aProps.setLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.LEFT, LEFTLABELTEXT);
        aProps.setLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.RIGHT, RIGHTLABELTEXT);
        aProps.setLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.TOP, TOPLABELTEXT);
        
        aProps.setStepType(STEP_TYPE.USER_DEFINED);
        
        final double MAJORSTEPSIZE = 1.23;
        final double MINORSTEPSIZE = 4.56;
        aProps.setMajorStep(MAJORSTEPSIZE);
        aProps.setMinorStep(MINORSTEPSIZE);
        
        assertTrue(CrossSectionAnnotationProperties.NO_ANNOTATION.equals(aProps.getAnnotatedLayerName()));
        assertEquals(CrossSectionAnnotationProperties.LABEL_LOCATION.NONE, aProps.getTitleLocation());
        assertTrue("".equals(aProps.getTitle()));
        
        assertTrue(LEFTLABELTEXT.equals(aProps.getLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.LEFT)));
        assertTrue(RIGHTLABELTEXT.equals(aProps.getLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.RIGHT)));
        assertTrue(TOPLABELTEXT.equals(aProps.getLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.TOP)));
        assertTrue("".equals(aProps.getLabel(CrossSectionAnnotationProperties.LABEL_LOCATION.BOTTOM)));
        
        assertEquals(CrossSectionAnnotationProperties.LABEL_LOCATION.NONE, aProps.getColorBarLocation());
        
        assertEquals(0, aProps.getAvailableKeys().length);
        assertEquals(0, aProps.getSelectedKeys().length);
        
        assertEquals(CrossSectionAnnotationProperties.LABEL_LOCATION.TOP, aProps.getHorizontalAnnotationLocation());
        assertEquals("", aProps.getSynchronizationKey());
        
        assertEquals(CrossSectionAnnotationProperties.STEP_TYPE.USER_DEFINED, aProps.getStepType());
        assertEquals(CrossSectionAnnotationProperties.LABEL_LOCATION.LEFT, aProps.getVerticalAnnotationLocation());
        assertEquals(CrossSectionAnnotationProperties.LABEL_LOCATION.NONE, aProps.getColorBarLocation());
        
        assertEquals(MAJORSTEPSIZE, aProps.getMajorStep());
        assertEquals(MINORSTEPSIZE, aProps.getMinorStep());
    }
}