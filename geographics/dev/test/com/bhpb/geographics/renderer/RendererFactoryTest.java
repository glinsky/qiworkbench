/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.renderer;

import junit.framework.TestCase;

public class RendererFactoryTest extends TestCase {
    @Override
    public void setUp() {

    }

    @Override
    public void tearDown() {

    }
    
    public void testCursorRendererCreation() {
        float width = 100; 
        float height = 100;
        float x = 50;
        float y = 50; 
        float xOrigin = 25; 
        float yOrigin = 50;
            
        Renderer cRenderer = RendererFactory.createCursorRenderer(width, 
            height, 
            x, 
            y, 
            xOrigin, 
            yOrigin);
        
        assertNotNull(cRenderer);    
    }
}