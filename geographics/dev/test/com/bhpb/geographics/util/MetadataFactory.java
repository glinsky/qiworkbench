/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import com.bhpb.geoio.datasystems.DataSystemConstants.GeoKeyType;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.filesystems.FileSystemConstants;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Factor for creation of various MetadataFactory instances used to test the
 * associated controller and view functionality.
 */
public class MetadataFactory {
    private static final String[] TEST_XSEC_KEYNAMES = { "ep",
        "cdp", "offset", "tracl" };
    private static final String[] TEST_MAP_KEYNAMES = { "tracl",
        "cdp", "ep", "offset" };

    private static final DiscreteKeyRange[] TEST_XSEC_KEYRANGES = {
        new DiscreteKeyRange(3015.0, 3016.0, 1.0, GeoKeyType.REGULAR_TYPE),
        new DiscreteKeyRange(1350.0, 1450.0, 1.0, GeoKeyType.REGULAR_TYPE),
        new DiscreteKeyRange(50.0, 3950.0, 100.0, GeoKeyType.REGULAR_TYPE),
        new DiscreteKeyRange(0, 8000.0, 4.0, GeoKeyType.REGULAR_TYPE)
    };

    private static final DiscreteKeyRange[] TEST_MAP_KEYRANGES = {
        new DiscreteKeyRange(0.0, 210.0, 10.0, GeoKeyType.REGULAR_TYPE),
        new DiscreteKeyRange(1.0, 201.0, 1.0, GeoKeyType.REGULAR_TYPE),
        new DiscreteKeyRange(1.0, 151.0, 1.0, GeoKeyType.REGULAR_TYPE),
        new DiscreteKeyRange(50.0, 3950.0, 100.0, GeoKeyType.REGULAR_TYPE)
    };

    public MetadataFactory() {
    }
    
    /**
     * Convenience method to create test SeismicMetadata with 4 header keys,
     * including 1 vertical header key.
     * 
     * @param orientation
     * 
     * @return
     */
    public static SeismicFileMetadata createTestSeismicMetadata(GeoDataOrder orientation) {
        return createTestSeismicMetadata(orientation, 4);
    }

    public static SeismicFileMetadata createTestSeismicMetadata(GeoDataOrder orientation, int maxKeys) {
        String geoFilePath = "/scratch/qiProjects/demo///datasets/gathers.dat";
        String geoFileName = "gathers";
        FileSystemConstants.GeoFileType geoFileType = FileSystemConstants.GeoFileType.SEISMIC_GEOFILE;
        FileSystemConstants.GeoDataOrder geoDataOrder;
        
        switch (orientation) {
            case CROSS_SECTION_ORDER:
                geoDataOrder = FileSystemConstants.GeoDataOrder.CROSS_SECTION_ORDER;
                break;
            case MAP_VIEW_ORDER :
                geoDataOrder = FileSystemConstants.GeoDataOrder.MAP_VIEW_ORDER;
                break;
            default :
                throw new IllegalArgumentException("Unable to create metadate for Layer with unknown orientation: " + orientation);
        }
        
        ArrayList<String> headerKeys = getTestHeaderKeys(orientation, maxKeys);
        
        HashMap<String, DiscreteKeyRange> keyRangeMap = getTestKeyRanges(orientation,maxKeys);
                
        SeismicFileMetadata metadata = new SeismicFileMetadata(geoFilePath, 
                geoFileName, 
                geoFileType, 
                geoDataOrder,
                headerKeys, 
                keyRangeMap,
                "dat",
                new ArrayList<String>());
        
        return metadata;
    }

    private static ArrayList<String> getTestHeaderKeys(GeoDataOrder orientation, int maxKeys) {
        ArrayList<String> headerKeys = new ArrayList<String>();

        String[] testKeyNames = getTestKeyNames(orientation);

        for (int i = 0; i < Math.min(maxKeys,4); i++) {
            headerKeys.add(testKeyNames[i]);
        }

        return headerKeys;
    }

    private static HashMap<String, DiscreteKeyRange> getTestKeyRanges(GeoDataOrder orientation,
            int maxKeys) {
        HashMap<String, DiscreteKeyRange> keyRangeMap =
            new HashMap<String, DiscreteKeyRange>();

        String[] testKeyNames = getTestKeyNames(orientation);

        DiscreteKeyRange[] keyRangeArray;

        if (orientation == GeoDataOrder.MAP_VIEW_ORDER) {
            keyRangeArray = TEST_MAP_KEYRANGES;
        } else {
            keyRangeArray = TEST_XSEC_KEYRANGES;
        }

        for (int i = 0; i < Math.min(maxKeys,4); i++) {
            keyRangeMap.put(testKeyNames[i], keyRangeArray[i]);
        }

        return keyRangeMap;
    }

    private static String[] getTestKeyNames(GeoDataOrder orientation) {
        switch (orientation) {
            case MAP_VIEW_ORDER :
                return TEST_MAP_KEYNAMES;
            case CROSS_SECTION_ORDER :
                return TEST_XSEC_KEYNAMES;
            default :
                throw new IllegalArgumentException("Unknown orientation: " + orientation);
        }
    }
    /**
     * Creates test seismic file metadata with 4 header keys and horizons.
     * 
     * @return
     */
    public static SeismicFileMetadata createTestSeismicMetadataWithHorizons() {
        String geoFilePath = "/scratch/qiProjects/demo///datasets/gathers.dat";
        String geoFileName = "gathers";
        FileSystemConstants.GeoFileType geoFileType = FileSystemConstants.GeoFileType.SEISMIC_GEOFILE;
        FileSystemConstants.GeoDataOrder geoDataOrder = FileSystemConstants.GeoDataOrder.CROSS_SECTION_ORDER;
        
        ArrayList<String> headerKeys = new ArrayList<String>();
        headerKeys.add("ep");
        headerKeys.add("cdp");
        headerKeys.add("offset");
        headerKeys.add("tracl");
        
        HashMap<String, DiscreteKeyRange> keyRangeMap = new HashMap<String, DiscreteKeyRange>();
        keyRangeMap.put("ep", new DiscreteKeyRange(3015.0, 3016.0, 1.0, GeoKeyType.REGULAR_TYPE));
        keyRangeMap.put("cdp", new DiscreteKeyRange(1350.0, 1450.0, 1.0, GeoKeyType.REGULAR_TYPE));
        keyRangeMap.put("offset", new DiscreteKeyRange(50.0, 3950.0, 100.0, GeoKeyType.REGULAR_TYPE));
        keyRangeMap.put("tracl", new DiscreteKeyRange(0, 8000.0, 4.0, GeoKeyType.REGULAR_TYPE));
                
        SeismicFileMetadata metadata = new SeismicFileMetadata(geoFilePath, 
                geoFileName, 
                geoFileType, 
                geoDataOrder,
                headerKeys, 
                keyRangeMap,
                "dat",
                createTestHorizons(1));
        
        return metadata;
    }
    
    private static ArrayList<String> createTestHorizons(int nHorizons) {
        ArrayList<String> hlist = new ArrayList<String>(nHorizons);
        for (int i=0;i<nHorizons;i++) {
            hlist.add("STUB_HORIZON_"+Integer.valueOf(i).toString());
        }
        return hlist;
    }
}