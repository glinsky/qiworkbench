/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import junit.framework.TestCase;

public class DecimalFormatterTest extends TestCase {

    private final double[] validTestScales = {1000.0, 100.0, 10.0, 1.0, 0.1, 0.01, 0.001, 0.0001, 3.14};
    private final double[] invalidTestScales = {-3.14};
    private final int[] expectedNumDecimalPlaces = {0, 0, 0, 0, 1, 2, 3, 4, 0};
    private DecimalFormatter formatter;

    @Override
    public void setUp() {
        formatter = DecimalFormatter.getInstance(10);
    }

    public void testConstructors() {
        formatter = DecimalFormatter.getInstance(0);
        formatter = DecimalFormatter.getInstance(5);

        try {
            formatter = DecimalFormatter.getInstance(-1);
            fail("Did not catch expected IllegalArgumentException");
        } catch (IllegalArgumentException iae) {
            System.out.println("Caught expected IllegalArgumentException invoking new AnnotationNumberFormatter(-1)");
        }

        try {
            formatter = DecimalFormatter.getInstance(11);
            fail("Did not catch expected IllegalArgumentException");
        } catch (IllegalArgumentException iae) {
            System.out.println("Caught expected IllegalArgumentException invoking new AnnotationNumberFormatter(11)");
        }
    }

    public void testGetNumDecimalPlaces() {
        for (int i = 0; i < validTestScales.length; i++) {
            assertEquals(
                    "Expected and calculated number of decimal places differed for valid test scale: " + validTestScales[i],
                    expectedNumDecimalPlaces[i],
                    formatter.getNumDecimalPlaces(validTestScales[i]));
        }
    }

    public void testGetFormattedString_Protected() {
        //no decimal places, round down
        String formattedString = formatter.getFormattedString(1.234, 0);
        assertEquals("Formatted string was not equal to the expected value",
                "1",
                formattedString);

        //no decimal places, round up
        formattedString = formatter.getFormattedString(1.500, 0);
        assertEquals("Formatted string was not equal to the expected value",
                "2",
                formattedString);

        //decimal places, round down
        formattedString = formatter.getFormattedString(12.341, 2);
        assertEquals("Formatted string was not equal to the expected value",
                "12.34",
                formattedString);

        //decimal places, round down (b/c of DecimalFormatter ROUND_HALF_EVEN)
        formattedString = formatter.getFormattedString(12.345, 2);
        assertEquals("Formatted string was not equal to the expected value",
                "12.34",
                formattedString);

        //decimal places, round up (b/c of DecimalFormatter ROUND_HALF_EVEN)
        formattedString = formatter.getFormattedString(12.355, 2);
        assertEquals("Formatted string was not equal to the expected value",
                "12.36",
                formattedString);
        
        //decimal places, omit trailing zeroes
        formattedString = formatter.getFormattedString(45, 3);
        assertEquals("Formatted string was not equal to the expected value",
                "45",
                formattedString);
    }

    public void testGetFormattedString_Public() {
        //scale > 1.0, no decimal places
        String formattedString = formatter.getFormattedString(123.456, 800.0);
        assertEquals("Formatted string was not equal to the expected value",
                "123",
                formattedString);

        //scale == 1.0, no decimal places
        formattedString = formatter.getFormattedString(123.456, 1.0);
        assertEquals("Formatted string was not equal to the expected value",
                "123",
                formattedString);

        // 0.1 <= scale < 1.0, 1 decimal place
        formattedString = formatter.getFormattedString(123.456, 0.99);
        assertEquals("Formatted string was not equal to the expected value",
                "123.5",
                formattedString);

        // 0.01 <= scale < 0.1, 2 decimal place
        formattedString = formatter.getFormattedString(123.456, 0.099);
        assertEquals("Formatted string was not equal to the expected value",
                "123.46",
                formattedString);

        // smallest possible scale, 10 decimal places
        formattedString = formatter.getFormattedString(1.1111111111, Double.MIN_VALUE);
        assertEquals("Formatted string was not equal to the expected value",
                "1.1111111111",
                formattedString);

        //now try invalid scales and verify Exceptions
        for (int i = 0; i < invalidTestScales.length; i++) {
            try {
                formatter.getFormattedString(12.345, invalidTestScales[i]);
                fail("Did not catch expected IllegalArgumentException invoking getNumDecimalPlaces(" + invalidTestScales[i] + ").");
            } catch (IllegalArgumentException iae) {
                System.out.println("Caught expected IllegalArgumentException invoking getNumDecimalPlaces(" + invalidTestScales[i] + ").");
            }
        }
    }
}