/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import java.awt.Color;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import junit.framework.TestCase;

public class ColorMapEditorTest extends TestCase {
    private final String property1 = "foreground";
    private final String property2 = "background";
    private final String property3 = "densityGradient";
    
    private Color[] colorPalette1 = {new Color(255,0,0)};
    private Color[] colorPalette2 = {new Color(0,255,0)};
    private Color[] colorPalette3 = {
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0),
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0),
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0),
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0),
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0),
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0),
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0),
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0),
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0)};
    
    protected static final String baseDir = System.getProperty("basedir");
    protected static final String baseTestFilePath = baseDir 
            + File.separator + "test" + File.separator + "data" 
            + File.separator;
    
    @Override
    public void setUp() {
        
    }
    
    @Override
    public void tearDown() {
        
    }
    
    public void testInit() throws InterruptedException {
        ColorMapEditor cmEditor = new ColorMapEditor(0,0,new ColorMap(getTestColorMap()));
        cmEditor.setVisible(true);
    }
    
    private Map<String, List<Color>> getTestColorMap() {
        Map<String, List<Color>> testColorMap = new HashMap<String, List<Color>>();
        
        testColorMap.put(property1, Arrays.asList(colorPalette1));
        testColorMap.put(property2, Arrays.asList(colorPalette2));
        testColorMap.put(property3, Arrays.asList(colorPalette3));
        
        return testColorMap;
    }
}