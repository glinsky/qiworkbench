/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import com.bhpb.geographics.model.window.WindowScaleProperties;
import junit.framework.TestCase;

public class ScaleUtilTest extends TestCase {
    //verify accuracy to 4 decimal places
    public static final double EPSILON = 0.0001;
    
    private static final ScaleUtil scaleUtil = new ScaleUtil(98.0);

    public void testGetPixelsPerInch() {
        double calcPPI = scaleUtil.getPixelsPerInch();
        double refPPI = 98.0;
        assertTrue(Math.abs(refPPI - calcPPI) < EPSILON);
    }

    public void testGetPixelsPerUnit_horizontal() {
        double refHorizPixPerUnit = 2.000035102656904;
        double calcHorizPixPerUnit = scaleUtil.calcNumPixels(19.291, WindowScaleProperties.UNITS.TRACES_PER_CM);
        //System.out.println("Expected vs actual pixelsPerUnit (vertical): " + refHorizPixPerUnit + " vs. " + calcHorizPixPerUnit);
        assertTrue(Math.abs(refHorizPixPerUnit - calcHorizPixPerUnit) < EPSILON);
    }

    public void testGetPixelsPerUnit_vertical() {
        double refVertPixPerUnit = 0.08333858267717069;
        double calcVertPixPerUnit = scaleUtil.calcNumPixels(0.00216, WindowScaleProperties.UNITS.CMS_PER_UNIT);
        //System.out.println("Expected vs actual pixelsPerUnit (vertical): " + refVertPixPerUnit + " vs. " + calcVertPixPerUnit);
        assertTrue(Math.abs(refVertPixPerUnit - calcVertPixPerUnit) < EPSILON);
    }

    public void testNearRunsumVert() {
        double calcVertPixPerUnit = scaleUtil.calcNumPixels(0.001372, WindowScaleProperties.UNITS.CMS_PER_UNIT);
        double vPix = calcVertPixPerUnit * 7800;
        //ref. image is 413 pixels tall at this scale
        //System.out.println("7800 samples should be 413 pixels tall at 0.001372 cms per unit and 98 pixels per inch.  Calculated height: " + Math.round(vPix) + " pixels.");
        assertTrue(Math.abs(Math.round(vPix) - 413) < EPSILON);
    }

    public void testNearRunsumHoriz() {
        double calcHorizPixPerUnit = scaleUtil.calcNumPixels(7.477, WindowScaleProperties.UNITS.TRACES_PER_CM);
        double hPix = calcHorizPixPerUnit * 122;
        //ref. image is 630 pixels wide at this scale
        //System.out.println("122 trace should be 630 pixels wide at 7.477 traces per cm and 98 pixels per inch.  Calculated width: " + Math.round(hPix) + " pixels.");
        assertTrue(Math.abs(Math.round(hPix) - 630) < EPSILON);
    }
}
