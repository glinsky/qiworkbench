/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.util;

import com.bhpb.geoio.datasystems.SeismicConstants.SampleFormat;
import com.bhpb.geoio.datasystems.SeismicConstants.SampleUnit;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;

public class SummaryFactory {
    /**
     * Creates a GeoFileDataSummary properties
     * <ul>
     *   <li>min: 0.0</li>
     *   <li>max: 0.0</li>
     *   <li>rms: 0.0</li>
     *   <li>avg: 0.0</li>
     *   <li>fixedSamplesPerTrace: true</li>
     *   <li>numberOfTraces: 8080</li>
     *   <li>samplesPerTrace: 2001</li>
     * </ul>
     *
     * @param metadata
     *
     * @return
     */
    public static GeoFileDataSummary createTestSummary(GeoFileMetadata metadata) {
        GeoFileDataSummary summary = new GeoFileDataSummary();
        
        summary.setDataStats(0.0, 0.0, 0.0, 0.0);
        summary.setFixedSamplesPerTrace(true);
        summary.setNumberOfTraces(8080);
        summary.setSampleFormat(SampleFormat.IEEE_FLOAT4_FORMAT);
        summary.setSampleRate(4.0);
        summary.setSampleStart(0.0);
        summary.setSampleUnits(SampleUnit.TIME_UNIT);
        summary.setSamplesPerTrace(2001);

        return summary;
    }    
}
