/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import com.bhpb.geographics.controller.layer.LayerSyncGroup;
import com.bhpb.geographics.model.HorizonHighlightSettings;
import com.bhpb.geographics.model.HorizonLayer;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.PickingSettings;
import com.bhpb.geographics.model.SeismicLayer;
import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParametersFactory;
import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.LocalSubsetProperties;
import com.bhpb.geographics.model.layer.display.LocalSubsetPropertiesFactory;
import com.bhpb.geographics.model.layer.sync.SeismicXsecSyncProperties;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geoio.datasystems.BhpSuDataObject;
import com.bhpb.geoio.datasystems.BhpSuHorizonDataObject;
import com.bhpb.geoio.datasystems.GeoIO;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.util.GeoIOAdapter;
import com.bhpb.qiworkbench.api.DataSystemConstants;

public class LayerFactory {

    public static Layer createSeismicLayer(GeoDataOrder orientation, int zOrder) {
        return createSeismicLayer(orientation, zOrder, 4, 1);
    }

    public static Layer createSeismicLayer(GeoDataOrder orientation, int zOrder, int maxKeys,
            int nDataObjects) {

        DatasetProperties dsProperties = DatasetPropertiesFactory.createTestSeismicProperties(orientation, maxKeys);
        SeismicXsecDisplayParameters layerDisplayParams = SeismicXsecDisplayParametersFactory.createLayerDisplayParameters(dsProperties);
        LocalSubsetProperties subsetProps = LocalSubsetPropertiesFactory.createLocalSubsetPropertiesTestData(8080);
        LayerSyncProperties syncProps = new SeismicXsecSyncProperties();

        BhpSuDataObject[] wiggles = new BhpSuDataObject[nDataObjects];
        for (int i = 0; i < nDataObjects; i++) {
            wiggles[i] = DataObjectFactory.createBhpSuDataObject(dsProperties, i);
        }

        GeoIOAdapter ioAdapter = null;
        WindowProperties winProps = new WindowProperties(orientation);
        return new SeismicLayer(dsProperties,
                layerDisplayParams,
                subsetProps,
                syncProps,
                wiggles,
                ioAdapter,
                orientation == GeoDataOrder.MAP_VIEW_ORDER,
                new LayerSyncGroup(),
                new WindowModel("Test Seismic Window #", orientation));
    }

    public static Layer createHorizonLayer(GeoDataOrder orientation, int zOrder) {

        DatasetProperties dsProperties = DatasetPropertiesFactory.createTestSeismicProperties(orientation);
        SeismicXsecDisplayParameters layerDisplayParams = SeismicXsecDisplayParametersFactory.createLayerDisplayParameters(dsProperties);
        LocalSubsetProperties subsetProps = LocalSubsetPropertiesFactory.createLocalSubsetPropertiesTestData(8080);
        LayerSyncProperties syncProps = new SeismicXsecSyncProperties();
        GeoIO.getInstance().initFormatIO(DataSystemConstants.BHP_SU_FORMAT);
        BhpSuHorizonDataObject[] wiggles = new BhpSuHorizonDataObject[1];
        wiggles[0] = DataObjectFactory.createBhpSuHorizonDataObject();
        GeoIOAdapter ioAdapter = null;
        WindowProperties winProps = new WindowProperties(orientation);

        PickingSettings pickingSettings = new PickingSettings();
        HorizonHighlightSettings highlightSettings = new HorizonHighlightSettings();

        return new HorizonLayer(dsProperties,
                layerDisplayParams,
                subsetProps,
                syncProps,
                wiggles,
                ioAdapter,
                pickingSettings,
                highlightSettings,
                orientation == GeoDataOrder.MAP_VIEW_ORDER,
                new LayerSyncGroup(),
                new WindowModel("Test Horizon Window #", orientation));
    }
}