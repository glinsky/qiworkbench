/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import com.bhpb.geoio.datasystems.BhpSuDataObject;
import com.bhpb.geoio.datasystems.BhpSuHorizonDataObject;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.datasystems.GeoIO;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.qiworkbench.api.DataSystemConstants;
import com.bhpb.qiworkbench.api.DataSystemConstants.FieldKind;
import com.bhpb.qiworkbench.api.DataSystemConstants.FloatFormat;
import com.bhpb.qiworkbench.api.DataSystemConstants.RecordType;
import java.nio.ByteOrder;

public class DataObjectFactory {
    public static final int TRACE_HEADER_SIZE = 240;

    private static boolean geoIOinitialized = false;

    public static DataObject createDataObject() {
        initGeoIO();

        ByteOrder  byteOrder = ByteOrder.LITTLE_ENDIAN;
        FloatFormat floatFormat =FloatFormat.IEEE_FORMAT;
        RecordType recordType = RecordType.BINARY;
        FieldKind fieldKind = FieldKind.VECTOR;
        String pojoID = "TEST";
   
        DataObject dataObject = new DataObject(byteOrder,
                 floatFormat, 
                 recordType, 
                 fieldKind, 
                pojoID);
        
        dataObject.setDataRecord(new byte[0]); // why is this null and not 0-length?
        return dataObject;
    }

    public static BhpSuDataObject createBhpSuDataObject(DatasetProperties dsProps,
            int traceIndex) {
        initGeoIO();

        ByteOrder  byteOrder = ByteOrder.LITTLE_ENDIAN;
        FloatFormat floatFormat =FloatFormat.IEEE_FORMAT;
        RecordType recordType = RecordType.BINARY;
        FieldKind fieldKind = FieldKind.VECTOR;
        String pojoID = "TEST";

        BhpSuDataObject dataObject = new BhpSuDataObject(byteOrder,
                 floatFormat,
                 recordType,
                 fieldKind,
                pojoID);

        GeoFileDataSummary summary = dsProps.getSummary();

        dataObject.setDataRecord(new byte[summary.getSamplesPerTrace() * 4 + TRACE_HEADER_SIZE]); // why is this null and not 0-length?
        dataObject.setHeaderSize(TRACE_HEADER_SIZE);

        return dataObject;
    }

    public static BhpSuHorizonDataObject createBhpSuHorizonDataObject() {
        initGeoIO();

        ByteOrder  byteOrder = ByteOrder.LITTLE_ENDIAN;
        FloatFormat floatFormat =FloatFormat.IEEE_FORMAT;
        RecordType recordType = RecordType.BINARY;
        FieldKind fieldKind = FieldKind.VECTOR;
        String pojoID = "TEST";

        BhpSuHorizonDataObject dataObject = new BhpSuHorizonDataObject(byteOrder,
                 floatFormat,
                 recordType,
                 fieldKind,
                pojoID);

        dataObject.setDataRecord(new byte[0]); // why is this null and not 0-length?
        return dataObject;
    }

    private static synchronized void initGeoIO() {
        if (!geoIOinitialized) {
            GeoIO.getInstance().initFormatIO(DataSystemConstants.BHP_SU_FORMAT);
            geoIOinitialized = true;
        }
    }
}