/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import junit.framework.TestCase;

public class ColorMapTest extends TestCase {
        private final String property1 = ColorMap.FOREGROUND;
    private final String property2 = ColorMap.BACKGROUND;
    private final String property3 = ColorMap.RAMP;
    
    private Color[] colorPalette1 = {new Color(255,0,0)};
    private Color[] colorPalette2 = {new Color(0,255,0)};
    private Color[] colorPalette3 = {
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0),
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0),
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0),
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0),
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0),
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0),
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0),
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0),
        new Color(0,0,255), new Color(255,255,255), new Color(0,0,0)};
    
    protected static final String baseDir = System.getProperty("basedir");
    protected static final String baseTestFilePath = baseDir 
            + File.separator + "test" + File.separator + "data" 
            + File.separator;
    
    private Map<String, List<Color>> getTestColorMap() {
        Map<String, List<Color>> testColorMap = new HashMap<String, List<Color>>();
        
        testColorMap.put(property1, Arrays.asList(colorPalette1));
        testColorMap.put(property2, Arrays.asList(colorPalette2));
        testColorMap.put(property3, Arrays.asList(colorPalette3));
        
        return testColorMap;
    }
    
    public void testWriteToFile() throws Exception {
        String actualFileName = baseTestFilePath + "ActualColorMap.cmp";
        String expectedFileName = baseTestFilePath + "ExpectedColorMap.cmp";
        
        ColorMap testMap = new ColorMap(getTestColorMap());
        
        File actualFile = new File(actualFileName);
        File expectedFile = new File(expectedFileName);
        
        testMap.saveToFile(actualFile);
        assertEqualContent(actualFile, expectedFile);
    }
    
    public void testReadFromFile() throws Exception {
        String fileName = baseTestFilePath + "ExpectedColorMap.cmp";
        
        File file = new File(fileName);
        ColorMap testMapFromFile = new ColorMap(file);
        ColorMap testMap2 = new ColorMap(getTestColorMap());
                
        assertTrue("Color maps from file and test case are not equal", testMapFromFile.equals(testMap2));
    }
    
    public void assertEqualContent(File file1, File file2) throws FileNotFoundException, IOException {
        if (file1.isDirectory() || file2.isDirectory()) {
            throw new UnsupportedOperationException("This method cannot be used to compare a directory to a file or another directory");
        }
        assertEquals("The files do not have equal length", file1.length(), file2.length());
        
        LineNumberReader reader1 = new LineNumberReader(new FileReader(file1));
        LineNumberReader reader2 = new LineNumberReader(new FileReader(file2));
        try {
            String line1 = null;
            String line2 = null;
            do {
                int lineNumber = reader1.getLineNumber();
            
                line1 = reader1.readLine();
                line2 = reader2.readLine();
            
                if (line1 != null && line2 != null) {
                    String errorMessage = "File contents not equal - String mismatch at line #" + lineNumber;
                    assertTrue(errorMessage, line1.equals(line2));
                }
            } while (line1 != null && line2 != null);
            if (line1 != null) {
                fail("File #1 contains more lines than file #2");
            }
            if (line2 != null) {
                fail("File #2 contains more lines than file #1");
            }
        } finally {
            reader1.close();
            reader2.close();
        }
    }
    
    public void testGenState(){
    	ColorMap testMap = new ColorMap(getTestColorMap());
    	String states = testMap.genState();
    	System.out.println("states=" + states);
    	Document doc = null;
    	try{
    		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    		doc = dbf.newDocumentBuilder().parse(new InputSource(new StringReader(states)));
        } catch (SAXException sxe) {
            Exception x = sxe;
            if (sxe.getException() != null)
                x = sxe.getException();
            sxe.printStackTrace();
        } catch (ParserConfigurationException pce) {
            // Parser with specified options can't be built
        	pce.printStackTrace();
        } catch (IOException ioe) {
        	ioe.printStackTrace();
        } catch (Exception e) {
        	e.printStackTrace();
        }
    	assertTrue(doc != null);
    	Node node = doc.getFirstChild();
    	System.out.println("node.getNodeName()=" + node.getNodeName());
    	ColorMap cMap= ColorMap.restoreState(node);
    	assertTrue(testMap.equals(cMap));
    }
    
    public void testEquals() {
        String propertyName="test";
        List<Color> list1 = new ArrayList<Color>(1);
        list1.add(Color.WHITE);
        List<Color> list2 = new ArrayList<Color>(1);
        list2.add(Color.WHITE);
        List<Color> list3 = new ArrayList<Color>(1);
        list3.add(Color.BLACK);
        
        Map<String,List<Color>> map1 = new HashMap<String, List<Color>>(1);
        map1.put(propertyName, list1);
        
        Map<String,List<Color>> map2 = new HashMap<String, List<Color>>(1);
        map2.put(propertyName, list2);
        
        Map<String,List<Color>> map3 = new HashMap<String, List<Color>>(1);
        map3.put(propertyName, list3);
        
        ColorMap cm1 = new ColorMap(map1);
        ColorMap cm2 = new ColorMap(map2);
        ColorMap cm3 = new ColorMap(map3);
        
        
        assertTrue(cm1.equals(cm2));
        assertFalse(cm1.equals(cm3));
    }
    
    /**
     * Test of minimal requirement of hashCode(), that is,
     * that equal objects must have identical hashCodes.
     */
    public void testHashCode() {
        String propertyName="test";
        List<Color> list1 = new ArrayList<Color>(1);
        list1.add(Color.WHITE);
        List<Color> list2 = new ArrayList<Color>(1);
        list2.add(Color.WHITE);
        
        Map<String, List<Color>> map1 = new HashMap<String, List<Color>>(1);
        map1.put(propertyName, list1);
        
        Map<String, List<Color>> map2 = new HashMap<String, List<Color>>(1);
        map2.put(propertyName, list2);
        
        ColorMap cm1 = new ColorMap(map1);
        ColorMap cm2 = new ColorMap(map2);
        
        assertEquals(cm1.hashCode(), cm2.hashCode());
    }
}