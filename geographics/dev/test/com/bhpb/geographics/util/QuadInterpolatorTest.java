/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class QuadInterpolatorTest {

    float[] values;
    Interpolator interpolator;

    @Before
    public void setUp() {

        interpolator = new QuadraticInterpolator();

        values = new float[5];
        values[0] = 0.0f; //t=0
        values[1] = 227.04f; //t=5
        values[2] = 362.78f; //t=10
        values[3] = 517.35f; //t=15
        values[4] = 732.3423f;
    }

    @Test
    public void testInterp() {
        float[] interpolatedvalues = interpolator.interpolate(values, 4);

        /*
        System.out.println();
        for (int i = 0; i < interpolatedvalues.length; i++) {
            System.out.println(i + ". " + interpolatedvalues[i]);
        }
        */
        assertTrue(interpolatedvalues[0] == values[0]);
        assertTrue(Math.abs(interpolatedvalues[11] - 392.1876) < 0.01);
        assertTrue(interpolatedvalues[interpolatedvalues.length - 1] ==
                values[values.length - 1]);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInterpIllegalArg() {
        float[] interpolatedvalues = interpolator.interpolate(values, 0.5);
    }
}
