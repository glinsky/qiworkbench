import groovy.util.GroovyTestCase
import com.bhpb.geographics.util.DecimalFormatter

class GroovyTest extends GroovyTestCase {
  private DecimalFormatter formatter
	
  static void main(String[] args) {}

  void testSimpleJavaPackage() {
    formatter = new DecimalFormatter(2)
    String formattedString = "1.24"
    assertEquals("value should be " + formattedString, formattedString, formatter.getFormattedString(1.239,true))
  }	
}
