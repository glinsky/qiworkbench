/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.io;

import com.bhpb.geographics.util.ColorMapEditorAgent;
import com.bhpb.geographics.util.ColorMapEditorAgent.FILECHOOSER_ACTION;
import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.FileChooserDescriptor;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.util.GenericFileFilter;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import java.awt.Component;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Implementation borrowed from qiViewer GeoIODirector.  Convenience methods to encapsulate message syntax.
 */
public class IODirector {

    private static final Logger logger =
            Logger.getLogger(IODirector.class.toString());
    private ColorMapEditorAgent agent;
    private HashMap<String, FileChooserDescriptor> fileChooserRegistry = new HashMap<String, FileChooserDescriptor>();

    public IODirector(ColorMapEditorAgent agent) {
        this.agent = agent;
    }

    /**
     * Invoke a file chooser for reading BHP-SU.  Adapted from <code>ComponentStateUtils.callFileChooser()</code>.
     * This method returns nothing, rather, the FileChooserService it invokes may
     * send an OPEN_WINDOW command to the ViewerAgent, depending on the
     * the user's interaction with the FileChooser GUI.
     *
     * @param parent qiComponent GUI taking the import/export action.
     * @param dir Directory to start browsing from.
     * @param action Action to take, namely, import or export.
     * @param extension Only files with matching extensions will be displayed.
     * @param description Description of the extension (format) of visible files.
     */
    public void callFileChooser(Component parent,
            String dir,
            FILECHOOSER_ACTION action,
            String extension,
            String description,
            String fileChooserTitle) {
        ArrayList<Object> list = new ArrayList<Object>();
        //true is Export, false if Import.
        boolean isExport;

        switch (action) {
            case OPEN_COLORMAP:
                isExport = false;
                break;
            case SAVE_COLORMAP:
                isExport = true;
                break;
            default:
                throw new UnsupportedOperationException("Unsupported action: " + action);
        }

        //1st element the parent GUI object
        list.add(parent);

        //2nd element is the dialog title
        list.add(fileChooserTitle);

        //3rd element is a list that contains current directory to start with
        //and a flag (yes or no) indicating if an already remembered directory
        //should be used instead
        ArrayList<String> lst = new ArrayList<String>();
        lst.add(dir);
        lst.add("no");
        list.add(lst);

        //4th element is the file filter
        list.add(createFileFilter(extension, description));

        //5th element is the navigation flag
        list.add(false);

        //6th element is the producer's/requester's component descriptor
        list.add(agent.getComponentDescriptor());

        //7th element is the type of file chooser: either Open or Save
        String chooserType = isExport ? QIWConstants.FILE_CHOOSER_TYPE_SAVE : QIWConstants.FILE_CHOOSER_TYPE_OPEN;
        list.add(chooserType);

        //8th element is the action request for this service: either Import or Export
        list.add(action.toString());

        //9th element is the default pretext for the file name if user fails to pretext it. If no pretext is required then use empty String
        String userName = ""; //System.getProperty("user.name");

        list.add(userName);

        //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
        if ((extension != null) && (!("".equals(extension)))) {
            list.add(extension);
        } else {
            list.add("");
        }

        //11th element is the target Tomcat URL where the file chooser chooses
        list.add(agent.getMessagingManager().getTomcatURL());

        //Second parameter seems to become part of the FileChooserDescriptor
        //but is never used otherwise.
        showFileChooser(list, "DUMMY CLASS NAME", this);
    }

    static private FileFilter createFileFilter(String extension, String description) {
        String[] xmlExtensions = new String[]{extension.toLowerCase(), extension.toUpperCase()};
        FileFilter xmlFilter = (FileFilter) new GenericFileFilter(xmlExtensions, description + " (*." + xmlExtensions[0] + ", " + xmlExtensions[1] + ")");
        return xmlFilter;
    }

    /**
     * Register the request for a file chooser's result so the agent can invoke 
     * the file chooser action. Usually performed when the request is sent.
     * <p>
     * The file chooser's descriptor is kept in a hashmap keyed on the ID of the 
     * response.
     *
     * @param desc File chooser descriptor for the requester.
     * @return Empty string if successfully registerd or null if the file chooser 
     * request is already registered.
     */
    synchronized public String registerFileChooserRequest(FileChooserDescriptor desc) {
        // check if response is already registered
        if (fileChooserRegistry.containsKey(desc.getMsgID())) {
            return null;
        }

        fileChooserRegistry.put(desc.getMsgID(), desc);

        logger.config("registered:" + desc);

        return "";
    }

    /*
     * Get a file chooser service, invoke a file chooser, request the user's
     * selection and register the request so the selector action is processed
     * by the requester.
     * @param list File chooser input parameters
     * @param requesterType Data type of the requester
     * @param requesterInstance Instance of the requester
     */
    public void showFileChooser(ArrayList paramList, String requesterType, Object requesterInstance) {
        IMessagingManager msgMgr = agent.getMessagingManager();

        String msgID = msgMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD, true);
        IQiWorkbenchMsg response = msgMgr.getMatchingResponseWait(msgID, 10000);

        if (response == null) {
            logger.warning("Internal error occurring in getting file chooser service returning null");
        } else if (response.isAbnormalStatus()) {
            logger.warning("Internal error occurring in getting file chooser service. Cause: " + response.getContent());
        } else {
            ComponentDescriptor cd = (ComponentDescriptor) response.getContent();

            msgMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_FILE_CHOOSER_CMD, cd, QIWConstants.ARRAYLIST_TYPE, paramList);

            msgID = msgMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD, cd);

            FileChooserDescriptor desc = new FileChooserDescriptor(msgID, requesterType, requesterInstance);
            registerFileChooserRequest(desc);
        }
    }
}