/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.listener;

import com.bhpb.geographics.ui.AbstractGeoPlot;
import java.util.logging.Logger;

public class ScrollDispatchWorker implements Runnable {

    private static final Logger logger =
            Logger.getLogger(ScrollDispatchWorker.class.getName());
    private static final long MIN_WAIT_TIME = 100L;
    private final AbstractGeoPlot geoPlot;
    private long lastScrollTime = 0L;
    private final ScrollPaneAdjustmentListener listener;

    public ScrollDispatchWorker(ScrollPaneAdjustmentListener listener, AbstractGeoPlot geoPlot) {
        this.listener = listener;
        this.geoPlot = geoPlot;
    }

    public void run() {
        while (!Thread.interrupted()) {
            synchronized (listener) {
                Long currentTime = System.currentTimeMillis();
                if (listener.doHorizontalScroll &&
                        (listener.immediateHorizontalScroll || currentTime - lastScrollTime >= MIN_WAIT_TIME)) //scroll action initiated by Swing event listener can cause broadcast
                {
                    geoPlot.broadcastScroll();
                    //geoPlot.redraw(true, false);
                    listener.doHorizontalScroll = false;
                    lastScrollTime = currentTime;
                }

                if (listener.doVerticalScroll &&
                        (listener.immediateVerticalScroll || currentTime - lastScrollTime >= MIN_WAIT_TIME)) {
                    geoPlot.broadcastScroll();
                    //geoPlot.redraw(true, false);
                    listener.doVerticalScroll = false;
                    lastScrollTime = currentTime;
                }
                try {
                    listener.wait(MIN_WAIT_TIME);
                } catch (InterruptedException ie) {
                    logger.warning("Interrupted while waiting for other threads to get lock on listener.");
                }
            }
        }
    }
}