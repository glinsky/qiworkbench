/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.listener;

import com.bhpb.geographics.ui.AbstractGeoPlot;
import java.awt.Adjustable;
import java.awt.Rectangle;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.logging.Logger;

public class ScrollPaneAdjustmentListener implements AdjustmentListener {

    private static final Logger logger =
            Logger.getLogger(ScrollPaneAdjustmentListener.class.toString());
    private AbstractGeoPlot geoPlot;
    private Thread scrollDispatchThread;

    //These are package protected to be used by ScrollDispatchWorker
    //access to them must be externally synchronized
    boolean doHorizontalScroll = false;
    boolean doVerticalScroll = false;
    boolean immediateHorizontalScroll = false;
    boolean immediateVerticalScroll = false;
    int horizontalScrollValue = 0;
    int verticalScrollValue = 0;
    
    public ScrollPaneAdjustmentListener(AbstractGeoPlot geoPlot) {
        this.geoPlot = geoPlot;
        scrollDispatchThread = new Thread(new ScrollDispatchWorker(this, geoPlot));
        scrollDispatchThread.start();
    }

    public void adjustmentValueChanged(AdjustmentEvent ae) {
        //if the dispatch thread is interrupted or not alive, start a new one
        if (scrollDispatchThread.isInterrupted() || !scrollDispatchThread.isAlive()) {
            scrollDispatchThread = new Thread(new ScrollDispatchWorker(this, geoPlot),"Win-" + geoPlot.getWindowID() + "-ScrollDispatchWorker");
            scrollDispatchThread.start();
        }

        Rectangle visRect = geoPlot.getVisibleRect();

        if (visRect.getHeight() == 0 || visRect.getWidth() == 0) {
            return;
        } else {
            Adjustable adjustable = ae.getAdjustable();

            if (ae.getValueIsAdjusting()) {
                setScrollDispatchParams(false, adjustable.getOrientation(), adjustable.getValue());
            } else {
                setScrollDispatchParams(true, adjustable.getOrientation(), adjustable.getValue());
            }
        }
    }

    private synchronized void setScrollDispatchParams(boolean doImmediate, int orientation, int value) {
        switch (orientation) {
            case Adjustable.HORIZONTAL :
                doHorizontalScroll = true;
                horizontalScrollValue = value;
                immediateHorizontalScroll = doImmediate;
                break;
            case Adjustable.VERTICAL :
                doVerticalScroll = true;
                verticalScrollValue = value;
                immediateVerticalScroll = doImmediate;
                break;
            default :
                logger.warning("Unable to set scroll dispatch params for unknown orientation: " + orientation);
        }
    }
}