/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics;

import com.bhpb.geographics.model.HorizonLayer;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.ModelLayer;
import com.bhpb.geographics.model.SeismicLayer;
import com.bhpb.geographics.rasterizer.LayeredRenderableImageProducer;
import com.bhpb.geographics.ui.SparseBufferedImage;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderContext;
import java.awt.image.renderable.RenderableImage;
import java.util.Vector;
import java.util.logging.Logger;

public class GeoImage implements RenderableImageLayer {
    private static final Logger logger = Logger.getLogger(GeoImage.class.toString());
    private static final long serialVersionUID = -857293711767875573L;
    
    private Layer layer;
    private LayeredRenderableImageProducer rasterizer;
    
    public GeoImage(SeismicLayer layer) {
        this.layer = layer;
    }
    
    public GeoImage(ModelLayer layer) {
        this.layer = layer;
    }
    
    public GeoImage(HorizonLayer layer) {
        this.layer = layer;
    }
    
    public Vector<RenderableImage> getSources() {
        return null;
    }
    
    public Object getProperty(String name) {
        logger.info("Undefined property: " + name + " requested.");
        return java.awt.Image.UndefinedProperty;
    }

    public String[] getPropertyNames() {
        return new String[0];
    }
    
    public boolean isDynamic() {
        return false;
    }

    public float getWidth() {
        return (float)layer.getWidth();
    }
  
    public float getHeight() {
        return (float)layer.getHeight();
    }
    
    public float getMinX() {
        return 0.0f;
    }
  
    public float getMinY() {
        return 0.0f;
    }

    public RenderedImage createScaledRendering(int w, int h, RenderingHints hints) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
  
    public SparseBufferedImage createDefaultRendering(Rectangle visRect) {
        return rasterizer.rasterize(visRect);
    }
  
    public RenderedImage createRendering(RenderContext renderContext) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    
    public void setRasterizer(LayeredRenderableImageProducer rasterizer) {
        this.rasterizer = rasterizer;
    }
    
    public int getZorder() {
        return layer.getZorder();
    }
}