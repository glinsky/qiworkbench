/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geographics.controller.AbstractEditorPanelController.SELECTION;
import com.bhpb.geographics.model.GeoGraphicsPropertyGroup;
import com.bhpb.geographics.model.layer.display.LayerDisplayParameters;
import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;

public class LayerProperties extends TransactionResult<Object> implements PropertyAccessor, GeoGraphicsPropertyGroup {

    public enum FIELD {

        LayerDisplayProperties, DatasetProperties
    };
    private DatasetProperties dsProperties = null;
    private LayerDisplayProperties ldProperties = null;
    private FieldProcessor<FIELD> processor = new FieldProcessor<FIELD>(FIELD.values(), LayerProperties.class);

    public static LayerProperties createUserCancelledResult() {
        return new LayerProperties(SELECTION.CANCEL);
    }

    public static LayerProperties createUserAcceptedResult(DatasetProperties dsProperties,
            LayerDisplayProperties ldProperties) {
        return new LayerProperties(SELECTION.OK, dsProperties, ldProperties);
    }

    public static LayerProperties createUserAppliedResult(DatasetProperties dsProperties,
            LayerDisplayProperties ldProperties) {
        return new LayerProperties(SELECTION.APPLY, dsProperties, ldProperties);
    }

    public static LayerProperties createErrorResult(String errorMsg) {
        return new LayerProperties(errorMsg);
    }

    /**
     * Construct a FileChooserResult with null DatasetProperties and userAccepted == false
     */
    private LayerProperties(SELECTION selection) {
        super(null, selection);
    }

    /**
     * Construct a FileChooserResult with null DatasetProperties and userAccepted == false,
     *  selection == NONE and an error messaging indicating that an error occurred.
     */
    private LayerProperties(String errorMsg) {
        super(null, SELECTION.NONE, errorMsg);
    }

    /**
     * Construct a FileChooserResult with specified DatasetProperties and userAccepted == true
     */
    private LayerProperties(SELECTION selection, DatasetProperties dsProperties, LayerDisplayProperties ldProperties) {
        super(null, selection);
        this.dsProperties = dsProperties;
        this.ldProperties = ldProperties;
        this.selection = selection;
    }

    public DatasetProperties getDatasetProperties() {
        return dsProperties;
    }

    public String getProperty(String qualifiedField) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        switch (field) {
            case LayerDisplayProperties:
                return ldProperties.getProperty(remainder);
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public LayerDisplayParameters getLayerDisplayParameters() {
        return ldProperties.getLayerDisplayParameters();
    }

    public LayerDisplayProperties getLayerDisplayProperties() {
        return ldProperties;
    }
	
	public void setLayerDisplayProperties(LayerDisplayProperties ldProperties) {
		this.ldProperties = ldProperties;
	}

    @Override
    public Object getResult() {
        throw new UnsupportedOperationException("This method cannot be invoked on LayerProperties - call getDatasetProperties() or getLayerDisplayParameters instead.");
    }

    public void setProperty(String qualifiedField, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        switch (field) {
            case LayerDisplayProperties:
                ldProperties.setProperty(remainder, value);
                break;
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }
}