/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller;


import com.bhpb.geographics.model.HorizonHighlightSettings;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters.LINE_STYLE;
import com.bhpb.geographics.ui.HorizonHighlightPanel;
import java.awt.Color;
import java.util.logging.Logger;

public class HorizonHighlightSettingsController extends AbstractEditorPanelController<HorizonHighlightSettings,HorizonHighlightPanel> {

    private static final Logger logger =
            Logger.getLogger(HorizonHighlightSettingsController.class.toString());

    public HorizonHighlightSettingsController(HorizonHighlightSettings model, HorizonHighlightPanel view) {
        super(model, view);
        view.setController(this);
    }

    public void run() {
        view.setLineColor(model.getLineColor());
        view.setLineStyle(model.getLineStyle());
        view.setLineWidth(model.getLineWidth());
        view.setColorOpacity(model.getColorOpacityAsPercent());
    }
    
    public void setLineStyle(LINE_STYLE lineStyle){
        if (lineStyle == null) {
            return;
        }
        try {
            //model.setLineStyle(LINE_STYLE.valueOf(lineStyle));
            model.setLineStyle(lineStyle);
        } catch (IllegalArgumentException iae) {
            logger.warning(iae.getMessage());
        }
    }
    
    public void setLineWidth(int width){
        model.setLineWidth(width);
    }
    
    public void setColorOpacity(int opacity){
        model.setColorOpacityPercent(opacity);
    }

    public void setLineColor(Color color) {
        model.setLineColor(color);
    }
}