/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller;

import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.ui.layer.LayerPropertiesDialog;
import com.bhpb.geographics.ui.layer.PropertiesEditorFactory;
import java.util.logging.Logger;
import javax.swing.JFrame;

public class LayerPropsEditorWorker implements Runnable {
    private static final Logger logger =
            Logger.getLogger(LayerPropsEditorWorker.class.getName());

    private JFrame frame;
    private Layer layer;
    private LayerPropertiesDialog dlg;

    public LayerPropsEditorWorker(JFrame frame, Layer layer) {
        this.layer = layer;
        this.frame = frame;
    }

    public void run() {
        dlg = PropertiesEditorFactory.createLayerPropertiesDialog(frame, layer);
        dlg.setVisible(true);
        dlg.pack();
        //LayerProperties result = dlg.getTransactionResult();
        //layer.processEditorResult(result);
        LayerProperties result;
        do {
            result = dlg.getTransactionResult();
            if (result == null) {
                logger.warning("null TransactionResult");
            } else if (result.isAccepted() || result.isApplied()) {
                //process the result if the user clicked Ok or Apply
                layer.processEditorResult(result);
            } else if (result.isErrorCondition()) {
                logger.warning("Editor transaction resulted in error: " + result.getErrorMsg());
            } else if (!result.isCancelled()) {
                logger.warning("Invalid TransactionResult: status is not Accepted, Applied, Canceled or Error.");
            }
        //wait for another result if the user clicked Apply
        } while (result != null && result.isApplied());
    }
    
    /**
     * 
     */
    public void bringDialogToFront() {
        dlg.toFront();
    }
}