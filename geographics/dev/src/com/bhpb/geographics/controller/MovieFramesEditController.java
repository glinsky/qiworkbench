/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller;


import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.bhpb.geographics.model.MovieFramesEditModel;
import com.bhpb.geographics.ui.MovieFramesEditPanel;

public class MovieFramesEditController extends AbstractEditorPanelController<MovieFramesEditModel,MovieFramesEditPanel> {

    private static final Logger logger =
            Logger.getLogger(MovieFramesEditController.class.toString());

    private List<Image> images = new ArrayList<Image>();
    public MovieFramesEditController(MovieFramesEditModel model, MovieFramesEditPanel view) {
        super(model, view);
        view.setController(this);
    }

    public void run() {
        view.setFrameList(model.getFrames());
    }
    
    public List<Image> getImages(){
    	return images;
    }
    
    public void setFrameList(List<Object> frames){
        if (frames == null) {
            return;
        }
        try {
            model.setFrames(frames);
            int[] order = new int[frames.size()];
            for (int i = 0; i < order.length; i++) {
                order[i] = Integer.parseInt(frames.get(i).toString().substring(
                        6));
            }
            List<Image> newImages = new ArrayList<Image>();
            for (int i = 0 ; i< order.length; i++) {
                newImages.add(images.get(order[i]));
            }
            images.clear();
            images = newImages;
        } catch (IllegalArgumentException iae) {
            logger.warning(iae.getMessage());
        }
    }
    
}