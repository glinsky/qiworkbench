/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller;

import com.bhpb.geographics.ui.layer.dataset.DatasetPropertiesPanel;
import com.bhpb.geographics.ui.layer.dataset.VerticalRangePanel;
import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.datasystems.UnlimitedKeyRange;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.qiworkbench.util.StringValidator;
import com.bhpb.qiworkbench.util.StringValidator.TYPE;
import com.bhpb.qiworkbench.util.ValidatorResult;
import java.util.List;

/**
 * Marker superclass for Seismic, Horizon and DatasetPropertiesController classes.
 * 
 * @param M sublcass of DatasetProperties
 * @param V subclass of JPanel
 */
public abstract class DatasetPropertiesController<M extends DatasetProperties, V extends DatasetPropertiesPanel>
        extends AbstractEditorPanelController<M, V> {

    public DatasetPropertiesController(M model, V view) {
        super(model, view);
    }

    private String getVertKeyName() {
        List<String> keys = model.getMetadata().getKeys();
        return keys.get(keys.size() - 1);
    }

    private DiscreteKeyRange getVertKeyChosenRange() {
        String vertKey = getVertKeyName();
        return (DiscreteKeyRange) model.getKeyChosenRange(vertKey);
    }

    public void setVerticalRangeMin(String vertRangeMinStr) {
        ValidatorResult result = StringValidator.validate(vertRangeMinStr, TYPE.DOUBLE);

        if (!result.isValid()) {
            view.showValidationFailure(result.getReason());
            return;
        }

        double vertRangeMin = Double.valueOf(vertRangeMinStr);
        String vertKey = getVertKeyName();
        AbstractKeyRange oldVertKeyRange = model.getKeyChosenRange(vertKey);
        DiscreteKeyRange fullKeyRange = model.getMetadata().getKeyRange(vertKey);
        DiscreteKeyRange asDiscrete = getChosenRangeAsDiscrete(oldVertKeyRange);

        DiscreteKeyRange newVertKeyRange = new DiscreteKeyRange(vertRangeMin,
                asDiscrete.getMax(),
                asDiscrete.getIncr(),
                asDiscrete.getKeyType(),
                fullKeyRange);
        model.setKeyChosenRange(vertKey, newVertKeyRange);
    }

    public void setVerticalRangeMax(String vertRangeMaxStr) {
        ValidatorResult result = StringValidator.validate(vertRangeMaxStr, TYPE.DOUBLE);

        if (!result.isValid()) {
            view.showValidationFailure(result.getReason());
            return;
        }

        double vertRangeMax = Double.valueOf(vertRangeMaxStr);
        String vertKey = getVertKeyName();
        AbstractKeyRange oldVertKeyRange = model.getKeyChosenRange(vertKey);
        DiscreteKeyRange asDiscrete = getChosenRangeAsDiscrete(oldVertKeyRange);
        DiscreteKeyRange fullKeyRange = model.getMetadata().getKeyRange(vertKey);
        DiscreteKeyRange newVertKeyRange = new DiscreteKeyRange(asDiscrete.getMin(),
                vertRangeMax,
                asDiscrete.getIncr(),
                asDiscrete.getKeyType(),
                fullKeyRange);
        model.setKeyChosenRange(vertKey, newVertKeyRange);
    }

    public void setVerticalRangeIncr(String vertRangeIncrStr) {
        ValidatorResult result = StringValidator.validate(vertRangeIncrStr, TYPE.DOUBLE);

        if (!result.isValid()) {
            view.showValidationFailure(result.getReason());
            return;
        }

        double vertRangeIncr = Double.valueOf(vertRangeIncrStr);
        String vertKey = getVertKeyName();
        AbstractKeyRange oldVertKeyRange = model.getKeyChosenRange(vertKey);
        DiscreteKeyRange asDiscrete = getChosenRangeAsDiscrete(oldVertKeyRange);
        DiscreteKeyRange fullKeyRange = model.getMetadata().getKeyRange(vertKey);
        DiscreteKeyRange newVertKeyRange = new DiscreteKeyRange(asDiscrete.getMin(),
                asDiscrete.getMax(),
                vertRangeIncr,
                asDiscrete.getKeyType(),
                fullKeyRange);
        model.setKeyChosenRange(vertKey, newVertKeyRange);
    }

    public double getVerticalRangeIncr() {
        return getVertKeyChosenRange().getIncr();
    }

    public double getVerticalRangeMax() {
        return getVertKeyChosenRange().getMax();
    }

    public double getVerticalRangeMin() {
        return getVertKeyChosenRange().getMin();
    }

    public DiscreteKeyRange getChosenRangeAsDiscrete(AbstractKeyRange chosenRange) {
        if (chosenRange instanceof DiscreteKeyRange) {
            return (DiscreteKeyRange) chosenRange;
        } else if (chosenRange instanceof UnlimitedKeyRange) {
            return chosenRange.getFullKeyRange();
        } else {
            throw new IllegalArgumentException("ChosenRange must be an instanceof Discrete or UnlimitedKeyRange, but was: " + chosenRange.getClass().getName());
        }
    }

    protected void initVerticalRangePanel(GeoFileMetadata metadata) {
        //The full vertical range is the range of the last key of the metadata.
        List<String> keys = metadata.getKeys();
        int nKeys = metadata.getNumKeys();

        DiscreteKeyRange fullVerticalRange = metadata.getKeyRange(keys.get(nKeys - 1));

        VerticalRangePanel vkPanel = view.getVerticalRangePanel();
        if (vkPanel != null) {
            vkPanel.setVerticalKeyName(keys.get(nKeys - 1));
            vkPanel.setFullVertRange(fullVerticalRange.getMin() + " - " + fullVerticalRange.getMax() + " [" + fullVerticalRange.getIncr() + "]");

            vkPanel.setVertRangeMin(fullVerticalRange.getMin());
            vkPanel.setVertRangeMax(fullVerticalRange.getMax());
            vkPanel.setVertRangeStep(fullVerticalRange.getIncr());

            vkPanel.setVertRangeSync(false);
        }
    }
}