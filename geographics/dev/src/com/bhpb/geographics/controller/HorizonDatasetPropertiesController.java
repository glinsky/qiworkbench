/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller;

import com.bhpb.geographics.model.DatasetPropertiesTableModel;
import com.bhpb.geographics.ui.layer.dataset.HorizonDatasetPropertiesPanel;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import com.bhpb.geoio.util.GeoIOAdapter;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class HorizonDatasetPropertiesController 
        extends DatasetPropertiesController<HorizonProperties, HorizonDatasetPropertiesPanel> {

    private static final Logger logger = Logger.getLogger(HorizonDatasetPropertiesController.class.getName());
    final GeoIOAdapter geoIOadapter;

    public HorizonDatasetPropertiesController(HorizonProperties model, HorizonDatasetPropertiesPanel view,
            GeoIOAdapter geoIOadapter,
            float[] mutableHorizon) {
        super(model,view);
        this.geoIOadapter = geoIOadapter;
        view.setController(this);
        if (mutableHorizon != null) {
            view.setHorizonValues(mutableHorizon);
        }
    }

    public void run() {
        SeismicFileMetadata metadata = (SeismicFileMetadata) model.getMetadata();
        getView().setFileName(metadata.getGeoFilePath());
        getView().showFileDescription(metadata);

        view.setTableModel(new DatasetPropertiesTableModel(model, view));
        ArrayList<String> horizons = metadata.getHorizons();
        if (horizons.size() > 0) {
            view.setHorizons(horizons);
            view.setSelectedHorizon(model.getSelectedHorizon());
        } else {
            //display warning
            logger.warning("HorizonProperties do not contain any horizons, property editing must be cancelled.");
        }
        //only transpose horizons have configurable vertical range
        if (metadata.getGeoDataOrder() == GeoDataOrder.MAP_VIEW_ORDER) {
            view.setVerticalRangePanelEnabled(true);
            initVerticalRangePanel(metadata);
        } else {
            view.setVerticalRangePanelEnabled(false);
        }
    }

    public void setSelectedHorizons(ArrayList<String> selectedHorizonNames) {
        if (selectedHorizonNames.size() == 0) {
            logger.warning("At least one horizon must be selected.");
        }
        StringBuilder sbuilder = new StringBuilder(selectedHorizonNames.get(0));

        for (int i = 1; i < selectedHorizonNames.size(); i++) {
            sbuilder.append(";");
            sbuilder.append(selectedHorizonNames.get(i));
        }

        model.setSelectedHorizon(sbuilder.toString());
    }

    @Override
    public void setSelection(SELECTION selection) {
        if (view.applyChosenRanges()) {
            super.setSelection(selection);
        } else {
            JOptionPane.showMessageDialog(null, "Header key chosen range validation failed.");
        }
    }
}