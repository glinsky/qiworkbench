/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller;

import com.bhpb.geographics.model.PickingSettings;
import com.bhpb.geographics.model.PickingSettings.PICKING_MODE;
import com.bhpb.geographics.model.PickingSettings.SNAPPING_MODE;
import com.bhpb.geographics.ui.PickingSettingsPanel;
import com.bhpb.qiworkbench.util.StringValidator;
import com.bhpb.qiworkbench.util.StringValidator.TYPE;
import com.bhpb.qiworkbench.util.ValidatorResult;
import java.util.logging.Logger;

public class PickingSettingsController extends AbstractEditorPanelController<PickingSettings, PickingSettingsPanel> {

    private static final Logger logger =
            Logger.getLogger(PickingSettingsController.class.toString());

    public PickingSettingsController(PickingSettings model, PickingSettingsPanel view) {
        super(model, view);
        view.setController(this);
    }

    public void run() {
        view.setPickingMode(model.getPickingMode());
        view.setSnappingMode(model.getSnappingMode());
        view.setSnapLimit(model.getSnapLimit());
    }

    public int getSnapLimit() {
        return model.getSnapLimit();
    }
    
    /**
     * Sets the PickingSettings' PICKING_MODE.  If the String is null
     * or not a valid PICKING_MODE String, no action occurs.
     * 
     * @param pickingModeStr String representation of a PICKING_MODE value.
     */
    public void setPickingMode(String pickingModeStr) {
        if (pickingModeStr == null) {
            return;
        }
        try {
            model.setPickingMode(PICKING_MODE.valueOf(pickingModeStr));
        } catch (IllegalArgumentException iae) {
            logger.warning(iae.getMessage());
        }
    }

    public void setSnapLimit(String snapLimitStr) {
        ValidatorResult validatorResult = StringValidator.validate(snapLimitStr, TYPE.INTEGER);
        if (validatorResult.isValid()) {
            model.setSnapLimit(Integer.valueOf(snapLimitStr));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    /**
     * Sets the PickingSettings' SNAPPING_MODE.  If the String is null
     * or not a valid SNAPPING_MODE String, no action occurs.
     * 
     * @param snappingModeStr String representation of a PICKING_MODE value.
     */
    public void setSnappingMode(String snappingModeStr) {
        if (snappingModeStr == null) {
            return;
        }
        try {
            model.setSnappingMode(SNAPPING_MODE.valueOf(snappingModeStr));
        } catch (IllegalArgumentException iae) {
            logger.warning(iae.getMessage());
        }
    }
}