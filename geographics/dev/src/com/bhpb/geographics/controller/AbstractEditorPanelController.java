/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller;

import java.util.logging.Logger;
import javax.swing.JComponent;

/**
 * Abstract class extended by controllers which are transactional editors.
 * Requires only that concrete subclasses implement the rollback()
 * and getRollbackFailureReason() methods.
 */
public abstract class AbstractEditorPanelController<MODEL, VIEW extends JComponent> 
        implements Runnable {

    private static final Logger logger =
            Logger.getLogger(AbstractEditorPanelController.class.toString());

    public enum SELECTION {

        OK, CANCEL, NONE, APPLY
    }

    protected  MODEL model;
    protected  VIEW view  ;
    protected String rollbackFailureReason = null;
    private SELECTION selection = SELECTION.NONE;

    public AbstractEditorPanelController(MODEL model, VIEW view) {
        this.model = model;
        this.view = view;
    }

    public MODEL getModel() {
        return model;
    }
    
    public String getRollbackFailureReason() {
        return rollbackFailureReason;
    }

    /**
     * Return the DIALOG_SELECTION representing the principle result of the user's
     * interaction with the view, e.g. "OK", "CANCEL".
     */
    public SELECTION getSelection() {
        return selection;
    }

    public VIEW getView() {
        return view;
    }
    
    public boolean rollback() {
        rollbackFailureReason = "Rollback not yet implemented";
        return false;
    }
    
    public void setSelection(SELECTION selection) {
        this.selection = selection;
    }
    
    /**
     * Attempts to wait indefinitely until the value of selection is not NONE,
     * checking every 1/4 second.  Returns if interrupted while the Thread is asleep,
     * or when the value of selection is not NULL.
     */
    public void waitForSelection() {
        try {
            while (selection.equals(SELECTION.NONE)) {
                Thread.sleep(250);
            }
        } catch (InterruptedException ie) {
            logger.warning("Interrupted while waiting for selection, returning.  Selection may be NONE.");
        }
    }
}