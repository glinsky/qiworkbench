/*
###########################################################################
# This program module Copyright (C) 2009 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller;

import com.bhpb.geographics.controller.AbstractEditorPanelController.SELECTION;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class EditorDialogListener extends WindowAdapter {
    private final List<AbstractEditorPanelController> controllers;

    public EditorDialogListener(List<AbstractEditorPanelController> controllers) {
         this.controllers = controllers;
    }

    @Override
    /**
     * Iterates over all controllers used by the EditorDialog and, if the window
     * was closed before all controllers were set to OK/APPLY/CANCEL, then
     * this Listener sets all controllers to cancel.  Bug fix for lockup when
     * user closes the EditorDialog by clicking the 'x', which did not set controller
     * selections and caused the EditorDialog invoker to continue waiting for a selection.
     */
    public void windowClosed(WindowEvent e) {
        for (AbstractEditorPanelController controller : controllers) {
            if (controller.getSelection() == SELECTION.NONE) {
                controller.setSelection(SELECTION.CANCEL);
            }
        }
    }
}