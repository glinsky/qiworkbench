/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller;

import com.bhpb.geographics.controller.AbstractEditorPanelController.SELECTION;
import com.bhpb.geographics.ui.layer.EditorDialog;
import java.util.logging.Logger;
import javax.swing.JComponent;

public class EditorWorker<T> implements Runnable {
    private static final Logger logger =
            Logger.getLogger(EditorWorker.class.getName());

    private final EditorResultProcessor<T> processor;
    private final AbstractEditorPanelController<T,JComponent> editor;
    private final EditorDialog dlg;
    
    public EditorWorker(EditorResultProcessor<T> processor, AbstractEditorPanelController<T, JComponent> editor,
            EditorDialog dlg) {
        this.processor = processor;
        this.editor = editor;
        this.dlg = dlg;
    }

    public void run() {
        dlg.setVisible(true);
        dlg.pack();
        TransactionResult<T> result;
        do {
            result = getTransactionResult();
            if (result == null) {
                logger.warning("null TransactionResult");
            } else if (result.isAccepted() || result.isApplied()) {
                //process the result if the user clicked Ok or Apply
                processor.processEditorResult(result.getResult());
            } else if (result.isErrorCondition()) {
                logger.warning("Editor transaction resulted in error: " + result.getErrorMsg());
            } else if (!result.isCancelled()) {
                logger.warning("Invalid TransactionResult: status is not Accepted, Applied, Canceled or Error.");
            }
        //wait for another result if the user clicked Apply
        } while (result != null && result.isApplied());
    }
    
    /**
     *  Brings this editor's GUI to the front.
     */
    public void bringDialogToFront() {
        dlg.toFront();
    }

     /**
      * Gets the transaction result.  As a side-effect, sets the status of the editor
      * to SELECTION.NONE, allowing another choice after Apply has been selected.
      *
      * @return TransactionResult<T> wrapping the model object
      */
     public TransactionResult<T> getTransactionResult() {
        editor.waitForSelection();
        SELECTION selection = editor.getSelection();
        editor.setSelection(SELECTION.NONE);
            TransactionResult<T> result;
            switch (selection) {
                case OK :
                case APPLY :
                case CANCEL :
                    result = new TransactionResult<T>(editor.model,selection);
                    break;
                default :
                    result = new TransactionResult<T>(editor.model,selection,"No user selection was available after waitForSelection.  The controller was interrupted while waiting or an error occurred.");
                    break;
            }
            return result;
        }
}