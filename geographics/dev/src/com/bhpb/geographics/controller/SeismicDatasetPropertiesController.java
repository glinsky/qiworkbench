/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.controller;

import com.bhpb.geographics.model.DatasetPropertiesTableModel;
import com.bhpb.geographics.ui.layer.dataset.SeismicDatasetPropertiesPanel;
import com.bhpb.geoio.filesystems.FileSystemConstants.MissingTraceOption;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.properties.SeismicProperties;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class SeismicDatasetPropertiesController 
        extends DatasetPropertiesController<SeismicProperties, SeismicDatasetPropertiesPanel> {
    private static final Logger logger =
            Logger.getLogger(SeismicDatasetPropertiesController.class.toString());
    
    public SeismicDatasetPropertiesController(SeismicProperties model, SeismicDatasetPropertiesPanel view) {
        super(model,view);
        view.setController(this);
    }

    public void run() {
        GeoFileMetadata metadata = model.getMetadata();
        
        getView().setFileName(metadata.getGeoFilePath());
        getView().showFileDescription(metadata);

        super.initVerticalRangePanel(metadata);

        view.setTableModel(new DatasetPropertiesTableModel(model,getView()));

        view.setDeadTraceOption(model.getMissingTraceOption());
    }
    
    public void setMissingTraceOption(MissingTraceOption mto) {
        model.setMissingTraceOption(mto);
        logger.info("MTO set to " + mto.toString());
    }
        
    @Override
    public void setSelection(SELECTION selection) {
        if (view.applyChosenRanges()) {
            super.setSelection(selection);
        } else {
            JOptionPane.showMessageDialog(null, "Validation failed: chosen ranges must contain exactly 2 or 0 Arbitrary Traverses");
        }
    }
}