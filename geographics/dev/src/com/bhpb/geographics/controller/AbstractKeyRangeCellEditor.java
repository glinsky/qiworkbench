/*
###########################################################################
# This program module Copyright (C) 2009 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller;

import com.bhpb.geographics.model.KeyRangeParseResult;
import com.bhpb.geographics.model.KeyRangeSerializer;
import com.bhpb.geoio.datasystems.ArbTravKeyRange;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.datasystems.UnlimitedKeyRange;
import java.awt.Component;
import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

public class AbstractKeyRangeCellEditor extends DefaultCellEditor implements TableCellEditor {
    DiscreteKeyRange fullKeyRange;
    
    public AbstractKeyRangeCellEditor() {
        super(new JTextField());
        setClickCountToStart(1);
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int rowIndex, int vColIndex) {
    
        String valueAsText = value.toString();
        
        if (value instanceof DiscreteKeyRange) {
            DiscreteKeyRange asDiscrete = (DiscreteKeyRange) value;
            fullKeyRange = asDiscrete.getFullKeyRange();
            valueAsText = KeyRangeSerializer.getKeyRangeAsLiteralString(asDiscrete);
        } else if (value instanceof ArbTravKeyRange) {
            ArbTravKeyRange asArbTrav = (ArbTravKeyRange) value;
            fullKeyRange = asArbTrav.getFullKeyRange();
            valueAsText = KeyRangeSerializer.getKeyRangeAsLiteralString(asArbTrav);
        } else if (value instanceof UnlimitedKeyRange) {
            UnlimitedKeyRange asUnlimited = (UnlimitedKeyRange) value;
            fullKeyRange = asUnlimited.getFullKeyRange();
            valueAsText = asUnlimited.toScriptString();
        }

        JTextField asTextField = (JTextField) super.getComponent();
        asTextField.setText(valueAsText);

        return asTextField;
    }

    @Override
    public Object getCellEditorValue() {
        String aValue = ((JTextField)super.getComponent()).getText();
        KeyRangeParseResult result = KeyRangeSerializer.parseRange(aValue, fullKeyRange);
        return result;
    }
}