/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */

package com.bhpb.geographics.controller.layer.subset;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.model.layer.display.LocalSubsetProperties;
import com.bhpb.geographics.ui.layer.subset.LocalSubsetPropertiesPanel;
import com.bhpb.qiworkbench.util.StringValidator;
import com.bhpb.qiworkbench.util.StringValidator.TYPE;
import com.bhpb.qiworkbench.util.ValidatorResult;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.logging.Logger;

public class LocalSubsetPropertiesController 
        extends AbstractEditorPanelController<LocalSubsetProperties, LocalSubsetPropertiesPanel> implements Runnable {

    private static final Logger logger =
            Logger.getLogger(LocalSubsetPropertiesController.class.getName());
    
    private NumberFormat formatter = DecimalFormat.getInstance();

    public LocalSubsetPropertiesController(LocalSubsetProperties model, LocalSubsetPropertiesPanel view) {
        super(model, view);
        view.setController(this);
    }

    public boolean doApplyGaps() {
        return model.doApplyGaps();
    }

    public String getEndTime() {
        return formatter.format(model.getEndTime());
    }

    public String getGapSize() {
        return Integer.toString(model.getGapSize());
    }
    
    public String getPkEnd() {
        return Integer.toString(model.getPrimaryKeyEndValue());
    }

    public String getPkStart() {
        return Integer.toString(model.getPrimaryKeyStartValue());
    }

    public String getPkStep() {
        return Integer.toString(model.getPrimaryKeyStepValue());
    }

    public String getSkEnd() {
        return formatter.format(model.getSecondaryKeyEndValue());
    }

    public String getSkStart() {
        return formatter.format(model.getSecondaryKeyStartValue());
    }

    public String getSkStep() {
        Double skStep = model.getSecondaryKeyStepValue();
        if (Double.isNaN(skStep)) {
            return "";
        } else {
            return formatter.format(model.getSecondaryKeyStepValue());
        }
    }

    public String getStartTime() {
        return formatter.format(model.getStartTime());
    }

    public void run() {
        if (model == null) {
            return;
        }
        //primary key attributes
        view.setSelectedPk(model.getPrimaryKeyField());
        view.setPkStart(model.getPrimaryKeyStartValue());
        view.setPkEnd(model.getPrimaryKeyEndValue());
        view.setPkStep(model.getPrimaryKeyStepValue());
        view.setApplyGapsSelected(model.doApplyGaps());
        view.setGapSize(model.getGapSize());

        //secondary key attributes
        view.setSelectedSk(model.getSecondaryKeyValue());
        view.setSkStart(model.getSecondaryKeyStartValue());
        view.setSkEnd(model.getSecondaryKeyEndValue());
        view.setSkStep(model.getSecondaryKeyStepValue());

        //time/depth range
        view.setStartTime(model.getStartTime());
        view.setEndTime(model.getEndTime());
    }

    public void setGapSize(String gapSize) {
        ValidatorResult validatorResult = StringValidator.validate(gapSize, TYPE.INTEGER);
        if (validatorResult.isValid()) {
            model.setGapSize(Integer.valueOf(gapSize));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }
    public void setPkStart(String pkStart) {
        ValidatorResult validatorResult = StringValidator.validate(pkStart, TYPE.INTEGER);
        if (validatorResult.isValid()) {
            model.setPkStart(Integer.valueOf(pkStart));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setPkEnd(String pkEnd) {
        ValidatorResult validatorResult = StringValidator.validate(pkEnd, TYPE.INTEGER);
        if (validatorResult.isValid()) {
            model.setPkEnd(Integer.valueOf(pkEnd));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setPkStep(String pkStep) {
        ValidatorResult validatorResult = StringValidator.validate(pkStep, TYPE.INTEGER);
        if (validatorResult.isValid()) {
            model.setPkStep(Integer.valueOf(pkStep));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setApplyGapsEnabled(boolean enabled) {
        model.setApplyGapsEnabled(enabled);
    }

    public void setSkStart(String skStart) {
        ValidatorResult validatorResult = StringValidator.validate(formatter,skStart, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            try {
                model.setSkStart(formatter.parse(skStart).doubleValue());
            } catch (ParseException pe) {
                logger.warning(pe.getMessage());
            }
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setSkEnd(String skEnd) {
        ValidatorResult validatorResult = StringValidator.validate(formatter,skEnd, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            try {
                model.setSkEnd(formatter.parse(skEnd).doubleValue());
            } catch (ParseException pe) {
                logger.warning(pe.getMessage());
            }
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setSkStep(String skStep) {
        if("".equals(skStep)) {
            model.setSkStep(Double.NaN);
            return;
        }

        ValidatorResult validatorResult = StringValidator.validate(skStep, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            model.setSkStep(Double.valueOf(skStep));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setStartTime(String startTime) {
        ValidatorResult validatorResult = StringValidator.validate(startTime, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            model.setStartTime(Double.valueOf(startTime));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setEndTime(String endTime) {
        ValidatorResult validatorResult = StringValidator.validate(endTime, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            model.setEndTime(Double.valueOf(endTime));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }
}