/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller.layer.shared;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.model.layer.shared.ModelVerticalRange;
import com.bhpb.geographics.ui.layer.shared.ModelVerticalRangePanel;
import com.bhpb.qiworkbench.util.StringValidator;
import com.bhpb.qiworkbench.util.StringValidator.TYPE;
import com.bhpb.qiworkbench.util.ValidatorResult;

public class ModelVerticalRangeController extends AbstractEditorPanelController<ModelVerticalRange, ModelVerticalRangePanel> {

    public ModelVerticalRangeController(ModelVerticalRange model, ModelVerticalRangePanel view) {
        super(model, view);
    }

    public void run() {
        view.setController(this);
        view.setStartTime(model.getStartTime());
        view.setEndTime(model.getEndTime());
        view.setStructuralInterpolationEnabled(model.isStructuralInterpolationEnabled());
    }

    public double getEndTime() {
        return model.getEndTime();
    }

    public double getStartTime() {
        return model.getStartTime();
    }

    public void setEndTime(String endTimeString) {
        ValidatorResult validatorResult = StringValidator.validate(endTimeString, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            model.setEndTime(Double.valueOf(endTimeString));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setStartTime(String startTimeString) {
        ValidatorResult validatorResult = StringValidator.validate(startTimeString, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            model.setStartTime(Double.valueOf(startTimeString));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setStructuralInterpolationEnabled(boolean enabled) {
        model.setStructuralInterpolationEnabled(enabled);
    }
}