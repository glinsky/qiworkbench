/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller.layer;

import com.bhpb.geographics.controller.LayerDisplayParametersController;
import com.bhpb.geographics.controller.layer.shared.ColorAttributesController;
import com.bhpb.geographics.controller.layer.shared.DisplayAttributeController;
import com.bhpb.geographics.controller.layer.display.LayerPropertiesController;
import com.bhpb.geographics.model.layer.display.MapDisplayParameters;
import com.bhpb.geographics.ui.layer.display.MapDisplayParametersPanel;

public class MapDisplayParametersController
        extends LayerDisplayParametersController<MapDisplayParameters, MapDisplayParametersPanel> {

    public MapDisplayParametersController(MapDisplayParameters model, MapDisplayParametersPanel view) {
        super(model, view);
    }

    public void run() {
        if (model == null) {
            return;
        }
        //Here the LayerDisplayParameters base class is used as the model which contains
        //the layerName field.  This field cannot be located with visually adjacent properties
        //in the BasicLayerProperties class because its other fields are not used by the
        //HorizonXsecDisplayParameters model.        
        view.getLayerInfoPanel().setModel(model);
        //set the layerName after setting the model or else the actionhandler,
        //which had to be intialized by the Matisse init code, will throw a NullPointerException
        view.getLayerInfoPanel().setLayerName(model.getLayerName());

        LayerPropertiesController layerInfoPanelController =
                new LayerPropertiesController(model.getBasicLayerProperties(), view.getLayerInfoPanel());
        DisplayAttributeController displayAttributeController =
                new DisplayAttributeController(model.getDisplayAttributes(), view.getDisplayAttributesPanel());
        ColorAttributesController colorAttributesController =
                new ColorAttributesController(model.getColorAttributes(), view.getColorAttributesPanel());

        new Thread(layerInfoPanelController).start();
        new Thread(displayAttributeController).start();
        new Thread(colorAttributesController).start();
    }
}