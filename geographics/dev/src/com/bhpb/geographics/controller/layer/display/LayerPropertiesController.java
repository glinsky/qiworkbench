/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.controller.layer.display;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.model.layer.shared.BasicLayerProperties;
import com.bhpb.geographics.ui.layer.shared.LayerInfoPanel;
import com.bhpb.qiworkbench.util.StringValidator;
import com.bhpb.qiworkbench.util.StringValidator.TYPE;
import com.bhpb.qiworkbench.util.ValidatorResult;

public class LayerPropertiesController extends AbstractEditorPanelController<BasicLayerProperties, LayerInfoPanel> {

    public LayerPropertiesController(BasicLayerProperties model, LayerInfoPanel view) {
        super(model, view);
    }

    public void run() {
        view.setController(this);

        view.setHorizontalScale(model.getHorizontalScale());
        view.setVerticalScale(model.getVerticalScale());
        view.setOpacity(model.getOpacity());
        view.setPolarity(model.getPolarity());
    }

    public double getHscale() {
        return model.getHorizontalScale();
    }

    public double getOpacity() {
        return model.getOpacity();
    }

    public double getVscale() {
        return model.getVerticalScale();
    }

    public void setHscale(String hScale) {
        ValidatorResult validatorResult = StringValidator.validate(hScale, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            model.setHorizontalScale(Double.valueOf(hScale));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setOpacity(double opacity) {
        if (0.0 > opacity || 1.0 < opacity) {
            view.showValidationFailure("Validation failure: " + Double.toString(opacity) + " is not in the range [0 .. 1.0]");
        } else {
            model.setOpacity(opacity);
        }

    }

    public void setVscale(String vScale) {
        ValidatorResult validatorResult = StringValidator.validate(vScale, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            model.setVerticalScale(Double.valueOf(vScale));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setPolarity(String polarity) {
        model.setPolarity(polarity);
    }
}