/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller.layer.shared;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.model.layer.shared.Normalization;
import com.bhpb.geographics.model.layer.shared.Normalization.NORMALIZATION_TYPE;
import com.bhpb.geographics.ui.layer.shared.NormalizationPanel;
import com.bhpb.qiworkbench.util.StringValidator;
import com.bhpb.qiworkbench.util.StringValidator.TYPE;
import com.bhpb.qiworkbench.util.ValidatorResult;
import java.util.logging.Logger;

public class NormalizationController extends AbstractEditorPanelController<Normalization, NormalizationPanel> {
    //superclass logger not hidden because it is private

    private static final Logger logger = Logger.getLogger(NormalizationController.class.toString());

    public NormalizationController(Normalization model, NormalizationPanel view) {
        super(model, view);
        view.setController(this);
    }

    public void run() {
        view.setNormScale(model.getNormalizationScale());
        view.setNormalizationType(model.getNormalizationType().toString());
        view.setNormMin(model.getNormalizationMinValue());
        view.setNormMax(model.getNormalizationMaxValue());
    }

    public double getNormMax() {
        return model.getNormalizationMaxValue();
    }

    public double getNormMin() {
        return model.getNormalizationMinValue();
    }

    public double getNormScale() {
        return model.getNormalizationScale();
    }

    public String getNormType() {
        return model.getNormalizationType().toString();
    }

    public void setNormMax(String maxString) {
        ValidatorResult validatorResult = StringValidator.validate(maxString, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            model.setNormalizationMaxValue(Double.valueOf(maxString));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setNormMin(String minString) {
        ValidatorResult validatorResult = StringValidator.validate(minString, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            model.setNormalizationMinValue(Double.valueOf(minString));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setNormScale(String scaleString) {
        ValidatorResult validatorResult = StringValidator.validate(scaleString, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            model.setNormalizationScale(Double.valueOf(scaleString));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setNormalizationType(String normTypeString) {
        NORMALIZATION_TYPE normType;
        try {
            normType = NORMALIZATION_TYPE.valueOf(normTypeString);
            model.setNormalizationType(normType);
        } catch (IllegalArgumentException iae) {
            logger.warning(iae.getMessage() + ", normalization type unchanged");
            normType = model.getNormalizationType();
        }
        switch (normType) {
            case LIMITS:
                view.setNormMinEnabled(true);
                view.setNormMaxEnabled(true);
                break;
            default:
                view.setNormMinEnabled(false);
                view.setNormMaxEnabled(false);
        }
    }
}