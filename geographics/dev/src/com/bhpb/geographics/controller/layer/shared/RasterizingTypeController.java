/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller.layer.shared;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.model.layer.shared.RasterizationTypeModel;
import com.bhpb.geographics.ui.layer.shared.RasterizingTypePanel;

public class RasterizingTypeController extends AbstractEditorPanelController<RasterizationTypeModel, RasterizingTypePanel> {

    private CullingController cullingController = null;

    /**
     * Constructs a new RasterizingTypeController.
     * 
     * @param model
     * @param rasterTypePanel
     * @param cullingController
     */
    public RasterizingTypeController(RasterizationTypeModel model,
            RasterizingTypePanel rasterTypePanel,
            CullingController cullingController) {
        super(model, rasterTypePanel);
        this.cullingController = cullingController;
        rasterTypePanel.setController(this);
    }

    public boolean isInterpolatedDensityEnabled() {
        return model.doRasterizeInterpolatedDensity();
    }

    public boolean isVariableDensityEnabled() {
        return model.doRasterizeVariableDensity();
    }

    public boolean isWiggleTraceEnabled() {
        return model.doRasterizeWiggleTraces();
    }

    public boolean isPosFillEnabled() {
        return model.doRasterizePositiveFill();
    }

    public boolean isNegFillEnabled() {
        return model.doRasterizeNegativeFill();
    }

    public void run() {
        boolean rasterizeVarDensity = model.doRasterizeVariableDensity();
        boolean rasterizeInterpDensity = model.doRasterizeInterpolatedDensity();

        if (rasterizeVarDensity && rasterizeInterpDensity) {
            throw new IllegalArgumentException("Variable and interpolated density rendering may not be enabled at the same time.");
        }

        view.setRasterizeWiggleTrace(model.doRasterizeWiggleTraces());
        view.setRasterizePosFill(model.doRasterizePositiveFill());
        view.setRasterizeNegFill(model.doRasterizeNegativeFill());
        view.setRasterizeVariableDensity(rasterizeVarDensity);
        view.setRasterizeInterpolatedDensity(rasterizeInterpDensity);
    }

    public void setRasterizeWiggleTraces(boolean enabled) {
        model.setRasterizeWiggleTraces(enabled);
        processNewSettings();
    }

    private boolean wiggleRasterizationEnabled() {
        return model.doRasterizeNegativeFill() ||
                model.doRasterizePositiveFill() ||
                model.doRasterizeWiggleTraces();
    }

    private boolean allRasterTypesDisabled() {
        return !(model.doRasterizeInterpolatedDensity() ||
                model.doRasterizeNegativeFill() ||
                model.doRasterizePositiveFill() ||
                model.doRasterizeVariableDensity() ||
                model.doRasterizeWiggleTraces());
    }

    public void setRasterizePositiveFill(boolean enabled) {
        model.setRasterizePositiveFill(enabled);
        processNewSettings();
    }

    public void setRasterizeNegativeFill(boolean enabled) {
        model.setRasterizeNegativeFill(enabled);
        processNewSettings();
    }

    /**
     * Sets the flag for rendering in variable density mode.  If enabled
     * is true, then interpolated density rendering is disabled.
     *
     * @param enabled
     */
    public void setRasterizeVariableDensity(boolean enabled) {
        model.setRasterizeVariableDensity(enabled);
        if (enabled) {
            model.setRasterizeInterpolatedDensity(false);
        }
        processNewSettings();
    }

    /**
     * Sets the flag for rendering in interpolated density mode.  If enabled
     * is true, then variable density rendering is disabled.
     *
     * @param enabled
     */
    public void setRasterizeInterpolatedDensity(boolean enabled) {
        model.setRasterizeInterpolatedDensity(enabled);
        if (enabled) {
            model.setRasterizeVariableDensity(false);
        }
        processNewSettings();
    }

    /**
     * If all rasterization types are disabed, wiggle and posfill are re-enabled.
     * The culling controller is enabled or disabled depending on whether any of wiggle,
     * posfill or negfill are enabled.
     */
    private void processNewSettings() {
        if (allRasterTypesDisabled()) {
            model.setRasterizeWiggleTraces(true);
            model.setRasterizePositiveFill(true);
        }
        cullingController.setEnabled(wiggleRasterizationEnabled());
    }
}