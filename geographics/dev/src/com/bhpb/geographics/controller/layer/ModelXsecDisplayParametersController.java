/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.controller.layer;

import com.bhpb.geographics.controller.LayerDisplayParametersController;
import com.bhpb.geographics.controller.layer.shared.CullingController;
import com.bhpb.geographics.controller.layer.display.LayerPropertiesController;
import com.bhpb.geographics.controller.layer.shared.ModelVerticalRangeController;
import com.bhpb.geographics.controller.layer.shared.NormalizationController;
import com.bhpb.geographics.controller.layer.shared.RasterizingTypeController;
import com.bhpb.geographics.controller.layer.display.XsecColorMapController;
import com.bhpb.geographics.model.layer.display.ModelXsecDisplayParameters;
import com.bhpb.geographics.ui.layer.display.ModelXsecDisplayParametersPanel;

public class ModelXsecDisplayParametersController
        extends LayerDisplayParametersController<ModelXsecDisplayParameters, ModelXsecDisplayParametersPanel> {

    public ModelXsecDisplayParametersController(ModelXsecDisplayParameters model, ModelXsecDisplayParametersPanel view) {
        super(model, view);
    }

    public void run() {
        if (model == null) {
            return;
        }
        //Here the LayerDisplayParameters base class is used as the model which contains
        //the layerName field.  This field cannot be located with visually adjacent properties
        //in the BasicLayerProperties class because its other fields are not used by the
        //HorizonXsecDisplayParameters model.        
        view.getLayerInfoPanel().setModel(model);
        //set the layerName after setting the model or else the actionhandler,
        //which had to be intialized by the Matisse init code, will throw a NullPointerException
        view.getLayerInfoPanel().setLayerName(model.getLayerName());

        LayerPropertiesController layerInfoPanelController =
                new LayerPropertiesController(model.getBasicLayerProperties(), view.getLayerInfoPanel());
        NormalizationController normalizationController =
                new NormalizationController(model.getNormalizationProperties(), view.getNormalizationPanel());
        ModelVerticalRangeController modelVerticalRangeController =
                new ModelVerticalRangeController(model.getModelVerticalRange(), view.getModelVerticalRangePanel());
        CullingController cullingController =
                new CullingController(model.getCulling(), view.getCullingPanel());
        RasterizingTypeController rasterizingTypeController =
                new RasterizingTypeController(model.getRasterizationTypeModel(), view.getRasterizingTypePanel(),
                cullingController);
        
        //not necessary to start the colorMapController - its Runnable is event-driven
        new XsecColorMapController(model.getColorMapModel(), view.getXsecColorMapPanel());

        new Thread(layerInfoPanelController).start();
        new Thread(normalizationController).start();
        new Thread(modelVerticalRangeController).start();
        new Thread(rasterizingTypeController).start();
        new Thread(cullingController).start();
    }
}