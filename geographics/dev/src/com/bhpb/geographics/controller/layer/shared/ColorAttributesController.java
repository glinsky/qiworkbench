/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller.layer.shared;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.model.layer.shared.ColorAttributes;
import com.bhpb.geographics.ui.layer.shared.ColorAttributesPanel;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.qiworkbench.util.StringValidator;
import com.bhpb.qiworkbench.util.StringValidator.TYPE;
import com.bhpb.qiworkbench.util.ValidatorResult;
import java.util.logging.Logger;

public class ColorAttributesController extends AbstractEditorPanelController<ColorAttributes, ColorAttributesPanel> {

    private Thread workerThread;
    private Logger logger =
            Logger.getLogger(ColorAttributesController.class.toString());

    public ColorAttributesController(ColorAttributes model, ColorAttributesPanel view) {
        super(model, view);
    }

    public void run() {
        view.setController(this);
        view.setMin(model.getMin());
        view.setMax(model.getMax());
    }

    public void setColorMap(ColorMap colorMap){
        model.setColorMap(colorMap);
    }
    
    public synchronized void editColorMap() {
        if (workerThread == null || !workerThread.isAlive()) {
            workerThread = new Thread(new ColorMapEditorWorker(model.getColorMapModel(), view));
            workerThread.start();
        } else {
            logger.info("ColorMap editor is already running.");
        }
    }
    
    public double getMax() {
        return model.getMax();
    }
    
    public double getMin() {
        return model.getMin();
    }
    
    public void setMax(String maxString) {
        ValidatorResult validatorResult = StringValidator.validate(maxString, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            model.setMax(Double.valueOf(maxString));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }
    
    public void setMin(String minString) {
        ValidatorResult validatorResult = StringValidator.validate(minString, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            model.setMin(Double.valueOf(minString));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }   
    }
}