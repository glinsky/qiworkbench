/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller.layer;

import com.bhpb.geographics.controller.LayerDisplayParametersController;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters;
import com.bhpb.geographics.ui.layer.display.HorizonXsecDisplayParametersPanel;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters.LINE_STYLE;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters.SYMBOL_STYLE;
import com.bhpb.qiworkbench.util.StringValidator;
import com.bhpb.qiworkbench.util.StringValidator.TYPE;
import com.bhpb.qiworkbench.util.ValidatorResult;
import java.awt.Color;

public class HorizonXsecDisplayParametersController
        extends LayerDisplayParametersController<HorizonXsecDisplayParameters, HorizonXsecDisplayParametersPanel> {

    public HorizonXsecDisplayParametersController(HorizonXsecDisplayParameters model, HorizonXsecDisplayParametersPanel view) {
        super(model, view);
    }

    public void run() {
        if (model == null) {
            return;
        }
        //Here the LayerDisplayParameters base class is used as the model which contains
        //the layerName field.  This field cannot be located with visually adjacent properties
        //in the BasicLayerProperties class because its other fields are not used by the
        //HorizonXsecDisplayParameters model.        
        view.setController(this);
        //set the layerName after setting the model or else the actionhandler,
        //which had to be intialized by the Matisse init code, will throw a NullPointerException
        view.setLayerName(model.getLayerName());

        view.setDrawLineEnabled(model.getDrawLine());
        view.setPointConnectionEnabled(model.getConnectNonContiguousPoints());
        view.setLineStyle(model.getLineStyle());
        view.setLineWidth(model.getLineWidth());
        view.setLineColor(model.getLineColor());
        view.setDrawSymbol(model.getDrawSymbol());
        view.setSolidFillEnabled(model.getSolidFill());
        view.setSymbolStyle(model.getSymbolStyle().toString());
        view.setSymbolWidth(model.getSymbolWidth());
        view.setSymbolHeight(model.getSymbolHeight());
        view.setSymbolColor(model.getSymbolColor());
    }

    public void setLayerName(String newName) {
        model.setLayerName(newName);
    }

    public Color getLineColor() {
        return model.getLineColor();
    }

    public Color getSymbolColor() {
        return model.getSymbolColor();
    }

    public void setLineStyle(LINE_STYLE lineStyle) {
        model.setLineStyle(lineStyle);
    }

    public double getSymbolHeight() {
        return model.getSymbolHeight();
    }

    public double getSymbolWidth() {
        return model.getSymbolWidth();
    }

    public void setDrawLineEnabled(boolean enabled) {
        model.setDrawLine(enabled);
    }
    
    public void setDrawSymbol(boolean enabled) {
        model.setDrawSymbol(enabled);
    }

    public void setLineColor(Color color) {
        model.setLineColor(color);
    }
    
    public void setPointConnectionEnabled(boolean enabled) {
        model.setConnectNonContiguousPoints(enabled);
    }
    
    public void setSolidFill(boolean enabled) {
        model.setSolidFill(enabled);
    }
    
    public void setSymbolHeight(String height) {
        ValidatorResult validatorResult = StringValidator.validate(height, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            model.setSymbolHeight(Double.valueOf(height));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setSymbolWidth(String width) {
        ValidatorResult validatorResult = StringValidator.validate(width, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            model.setSymbolWidth(Double.valueOf(width));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setSymbolStyle(String symbol) {
        try {
            SYMBOL_STYLE symbolStyle = SYMBOL_STYLE.valueOf(symbol.toUpperCase());
            model.setSymbolStyle(symbolStyle);
        } catch (Exception ex) {
            view.showValidationFailure("Unable to set symbol to unrecognized name: " + symbol);
        }
    }

    public void setSymbolColor(Color color) {
        model.setSymbolColor(color);
    }
}