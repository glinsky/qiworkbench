/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller.layer.shared;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.model.layer.shared.Culling;
import com.bhpb.geographics.ui.layer.shared.CullingPanel;
import com.bhpb.qiworkbench.util.StringValidator;
import com.bhpb.qiworkbench.util.StringValidator.CONSTRAINT;
import com.bhpb.qiworkbench.util.StringValidator.TYPE;
import com.bhpb.qiworkbench.util.ValidatorResult;

public class CullingController extends AbstractEditorPanelController<Culling, CullingPanel> {

    public CullingController(Culling model, CullingPanel view) {
        super(model, view);
    }

    public void run() {
        view.setController(this);
        view.setClippingFactor(model.getClippingFactor());
        view.setDecimationSpacing(model.getDecimationSpacing());
    }

    public int getClippingFactor() {
        return model.getClippingFactor();
    }

    public int getDecimationSpacing() {
        return model.getDecimationSpacing();
    }

    public void setClippingFactor(String clippingFactorString) {
        ValidatorResult validatorResult = StringValidator.validate(clippingFactorString, TYPE.INTEGER, CONSTRAINT.POSITIVE);
        if (validatorResult.isValid()) {
            model.setClippingFactor(Integer.valueOf(clippingFactorString));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setDecimationSpacing(String decimationSpacingString) {
        ValidatorResult validatorResult = StringValidator.validate(decimationSpacingString, TYPE.INTEGER, CONSTRAINT.POSITIVE);
        if (validatorResult.isValid()) {
            model.setDecimationSpacing(Integer.valueOf(decimationSpacingString));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    /**
     * Enables or disables the CullingPanel labels and text fields.
     */
    public void setEnabled(boolean enabled) {
        view.setEnabled(enabled);
    }
}