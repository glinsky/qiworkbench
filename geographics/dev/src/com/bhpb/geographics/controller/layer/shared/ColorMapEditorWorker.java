/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller.layer.shared;

import com.bhpb.geographics.model.layer.shared.ColorMapModel;
import com.bhpb.geographics.util.ColorMapEditor;
import java.awt.Component;
import java.util.logging.Logger;

public class ColorMapEditorWorker implements Runnable {
    private ColorMapModel colorMapModel;
    private Component viewParent;
    
    public Logger logger =
            Logger.getLogger(ColorMapEditorWorker.class.toString());
    
    public ColorMapEditorWorker(ColorMapModel colorMapModel, Component viewParent) {
        this.colorMapModel = colorMapModel;
        this.viewParent = viewParent;
    }
    
    public void run() {
        ColorMapEditor cmEditor = new ColorMapEditor(viewParent.getX(), viewParent.getY(), colorMapModel.getColorMap());
        ColorMapEditor.SELECTION sel = ColorMapEditor.SELECTION.NONE;
        cmEditor.setAlwaysOnTop(true);
        cmEditor.setVisible(true);

        do {
            sel = cmEditor.getSelection();
            while (sel == ColorMapEditor.SELECTION.NONE) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ie) {
                    logger.warning("Interrupted while sleeping 500ms for user to make selection from ColorMapEditor: " + ie.getMessage());
                } finally {
                    sel = cmEditor.getSelection();
                }
                if (sel == ColorMapEditor.SELECTION.OK ||
                        sel == ColorMapEditor.SELECTION.APPLY) {
                    colorMapModel.setColorMap(cmEditor.getColorMap());
                    if (sel == ColorMapEditor.SELECTION.APPLY) {
                        cmEditor.resetSelection();
                    }
                }
            }
        } while (sel != ColorMapEditor.SELECTION.OK &&
                sel != ColorMapEditor.SELECTION.CANCEL);
    }
}
