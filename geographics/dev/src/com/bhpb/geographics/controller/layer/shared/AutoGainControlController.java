/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */

package com.bhpb.geographics.controller.layer.shared;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.model.layer.shared.AutoGainControl;
import com.bhpb.geographics.model.layer.shared.AutoGainControl.UNITS;
import com.bhpb.geographics.ui.layer.shared.AutoGainControlPanel;
import com.bhpb.qiworkbench.util.StringValidator;
import com.bhpb.qiworkbench.util.StringValidator.CONSTRAINT;
import com.bhpb.qiworkbench.util.StringValidator.TYPE;
import com.bhpb.qiworkbench.util.ValidatorResult;
import java.util.logging.Logger;

public class AutoGainControlController extends AbstractEditorPanelController<AutoGainControl, AutoGainControlPanel> {
    private static final Logger logger =
            Logger.getLogger(AutoGainControlController.class.getName());

    public AutoGainControlController(AutoGainControl model, AutoGainControlPanel view) {
        super(model,view);
    }
    
    public void run() {
        view.setController(this);
        view.setWindowLength(model.getWindowLength());
        view.setApply(model.doApplyAutoGainControl());
        view.setUnits(model.getUnits().toString());
    }
    
    public int getWindowLength() {
        return model.getWindowLength();
    }
    
    public boolean isEnabled() {
        return model.isEnabled();
    }
    
    public void setWindowLength(String winLengthString) {
        ValidatorResult validatorResult = StringValidator.validate(winLengthString, TYPE.INTEGER, CONSTRAINT.POSITIVE);
        if (validatorResult.isValid()) {
            model.setWindowLength(Integer.valueOf(winLengthString));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }
    
    public void setApplyEnabled(boolean enabled) {
        model.setEnabled(enabled);
    }

    public void setUnits(String unitsString) {
        try {
            UNITS units = UNITS.valueOf(unitsString);
            model.setUnits(units);
        } catch (Exception ex) {
            logger.warning("Unable to set AGC units due to invalid UNITS String: "+ unitsString);
        }
    }
}