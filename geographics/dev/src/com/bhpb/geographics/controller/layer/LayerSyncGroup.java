/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */

package com.bhpb.geographics.controller.layer;

import com.bhpb.geographics.ui.event.ArbTravEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * A group of Layers which broadcast and receive events
 * configurable within LayerSyncProperties for each Layer individually.
 * 
 */
public class LayerSyncGroup {
    private static final Logger logger =
            Logger.getLogger(LayerSyncGroup.class.getName());
    
    private List<LayerSyncListener> listeners = new ArrayList<LayerSyncListener>();
    
    public void addListener(LayerSyncListener listener) {
        if (listeners.contains(listener)) {
            logger.warning("LayerSyncGroup windows list already contains window: "
                    + listener.getLayerID() + ", no action taken");
        } else {
            listeners.add(listener);
        }
    }
    
    public void broadcast(ArbTravEvent ate) {
        for (LayerSyncListener listener : listeners) {
            if (listener != ate.getSourceLayer()) {
                listener.handleArbitraryTraverse(ate);
            }
        }
    }

    public void removeListener(LayerSyncListener listener) {
        if (!listeners.contains(listener)) {
            logger.warning("LayerSyncGroup windows does not contain window: "
                    + listener.getLayerID() + ", no action taken");
        } else {
            listeners.remove(listener);
        }
    }
}