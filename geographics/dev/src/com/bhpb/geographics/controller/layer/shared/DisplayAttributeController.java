/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller.layer.shared;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.model.layer.shared.DisplayAttributes;
import com.bhpb.geographics.ui.layer.shared.DisplayAttributesPanel;

public class DisplayAttributeController extends AbstractEditorPanelController<DisplayAttributes, DisplayAttributesPanel> {

    public DisplayAttributeController(DisplayAttributes model, DisplayAttributesPanel view) {
        super(model, view);
    }

    public void run() {
        view.setController(this);
        view.setEpReversed(model.getEpReversed());
        view.setCdpReversed(model.getCdpReversed());
        view.setTransposed(model.isTransposed());
    }

    public void setCdpReversed(boolean enabled) {
        model.setCdpReversed(enabled);
    }

    public void setEpReversed(boolean enabled) {
        model.setEpReversed(enabled);
    }
    
    public void setTranspose(boolean enabled) {
        model.setTransposed(enabled);
    }
}