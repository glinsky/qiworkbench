/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller.window;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.model.window.WindowAnnotationProperties;
import com.bhpb.geographics.model.window.WindowAnnotationProperties.LABEL_LOCATION;
import com.bhpb.geographics.ui.window.AbstractAnnotationPropertiesPanel;
import com.bhpb.geographics.ui.window.MiscAnnotationPropertiesPanel;
import java.util.logging.Logger;

/**
 * 
 * @author folsw9
 * @param <M>
 * @param <V>
 */
public abstract class AbstractAnnotationPropertiesController<M extends WindowAnnotationProperties, V extends AbstractAnnotationPropertiesPanel>
        extends AbstractEditorPanelController<M, V> {

    private static final Logger logger =
            Logger.getLogger(AbstractAnnotationPropertiesController.class.toString());
    private MiscAnnotationPropertiesPanel miscPanel;

    /**
     * 
     * @param model
     * @param view
     */
    public AbstractAnnotationPropertiesController(M model, V view) {
        super(model, view);
        miscPanel = view.getMiscPanel();
    }

    /**
     * Sets the initial state of the selected annotation and misc. panel.
     * Override and call super.run() to extend in a subclass.
     */
    public void run() {
        view.setAnnotatedLayerNames(model.getLayerNames().toArray(new String[0]));
        view.setAnnotatedLayer(model.getAnnotatedLayerName());

        updateMiscPanel();
    }

    public void setAnnotatedLayer(String layerName) {
        model.setAnnotatedLayerName(layerName);
        updateMiscPanel();
    }

    public void setColorBarLocation(String location) {
        LABEL_LOCATION locEnum = LABEL_LOCATION.NONE;

        try {
            locEnum = LABEL_LOCATION.valueOf(location);
        } catch (Exception ex) {
            logger.warning(ex.getMessage());
            return;
        }

        model.setColorBarLocation(locEnum);
    }

    public void setTitleLocation(String location) {
        LABEL_LOCATION locEnum = LABEL_LOCATION.NONE;

        try {
            locEnum = LABEL_LOCATION.valueOf(location);
        } catch (Exception ex) {
            logger.warning(ex.getMessage());
            return;
        }

        model.setTitleLocation(locEnum);
    }

    private void updateMiscPanel() {
        miscPanel.setTitleLocation(model.getTitleLocation().toString());
        miscPanel.setTitle(model.getTitle());
        miscPanel.setLeftLabel(model.getLabel(LABEL_LOCATION.LEFT));
        miscPanel.setRightLabel(model.getLabel(LABEL_LOCATION.RIGHT));
        miscPanel.setTopLabel(model.getLabel(LABEL_LOCATION.TOP));
        miscPanel.setBottomLabel(model.getLabel(LABEL_LOCATION.BOTTOM));
        miscPanel.setColorBarLocation(model.getColorBarLocation().toString());
    }
}