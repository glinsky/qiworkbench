/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller.window;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.model.window.WindowScaleProperties;
import com.bhpb.geographics.model.window.WindowScaleProperties.UNITS;
import com.bhpb.geographics.ui.window.ScalePropertiesPanel;
import com.bhpb.geographics.util.ScaleUtil;
import com.bhpb.qiworkbench.util.StringValidator;
import com.bhpb.qiworkbench.util.StringValidator.TYPE;
import com.bhpb.qiworkbench.util.ValidatorResult;
import javax.swing.JComponent;

/**
 * Controls editing of the ViewerWindow's WindowProperties' ScaleProperties.
 * 
 * @author Woody Folsom
 * @version 1.0
 */
public class ScalePropertiesController<T extends Object, U extends JComponent> extends AbstractEditorPanelController<WindowScaleProperties, ScalePropertiesPanel> {

    public ScalePropertiesController(WindowScaleProperties model, ScalePropertiesPanel view) {
        super(model, view);
    }

    public void run() {
        view.setController(this);

        UNITS hu = model.getHorizontalScaleUnits();
        UNITS vu = model.getVerticalScaleUnits();

        view.setHscaleUnits(ScaleUtil.toString(hu));
        view.setVscaleUnits(ScaleUtil.toString(vu));

        view.setHscale(model.getHorizontalScale());
        view.setVscale(model.getVerticalScale());
    }

    public double getHscale() {
        return model.getHorizontalScale();
    }

    public double getVscale() {
        return model.getVerticalScale();
    }

    public void setHscale(String hScale) {
        ValidatorResult validatorResult = StringValidator.validate(hScale, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            model.setHorizontalScale(Double.valueOf(hScale));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setVscale(String vScale) {
        ValidatorResult validatorResult = StringValidator.validate(vScale, TYPE.DOUBLE);
        if (validatorResult.isValid()) {
            model.setVerticalScale(Double.valueOf(vScale));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }
}