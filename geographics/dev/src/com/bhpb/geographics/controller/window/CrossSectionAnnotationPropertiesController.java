/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller.window;

import com.bhpb.geographics.controller.EditorResultProcessor;
import com.bhpb.geographics.controller.EditorWorker;
import com.bhpb.geographics.model.window.AnnotationProperties;
import com.bhpb.geographics.model.window.CrossSectionAnnotationProperties;
import com.bhpb.geographics.model.window.CrossSectionAnnotationProperties.STEP_TYPE;
import com.bhpb.geographics.model.window.WindowAnnotationProperties.LABEL_LOCATION;
import com.bhpb.geographics.ui.layer.EditorDialog;
import com.bhpb.geographics.ui.window.AnnotationPropertiesPanel;
import com.bhpb.geographics.ui.window.CrossSectionAnnotationPropertiesPanel;
import com.bhpb.geographics.ui.window.HorizontalAnnotationPropertiesPanel;
import com.bhpb.geographics.ui.window.VerticalAnnotationPropertiesPanel;
import com.bhpb.qiworkbench.util.StringValidator;
import com.bhpb.qiworkbench.util.StringValidator.CONSTRAINT;
import com.bhpb.qiworkbench.util.StringValidator.TYPE;
import com.bhpb.qiworkbench.util.ValidatorResult;
import java.util.logging.Logger;

public class CrossSectionAnnotationPropertiesController
        extends AbstractAnnotationPropertiesController<CrossSectionAnnotationProperties, CrossSectionAnnotationPropertiesPanel>
        implements EditorResultProcessor<AnnotationProperties>{
    
    private static final Logger logger =
            Logger.getLogger(CrossSectionAnnotationProperties.class.getName());

    private HorizontalAnnotationPropertiesPanel hPanel;
    private VerticalAnnotationPropertiesPanel vPanel;

    public CrossSectionAnnotationPropertiesController(CrossSectionAnnotationProperties model,
            CrossSectionAnnotationPropertiesPanel view) {
        super(model, view);
        view.setController(this);
        hPanel = view.getHorizontalAnnotationPropertiesPanel();
        vPanel = view.getVerticalAnnotationPropertiesPanel();
    }

    @Override
    public void run() {
        super.run();
        
        updateHorizontalPanel();
        updateVerticalPanel();

        view.setLayerSelectionEnabled(true);
    }

    public String[] getAvailableKeys() {
        return model.getAvailableKeys();
    }
    
    public String[] getSelectedKeys() {
        return model.getSelectedKeys();
    }

    public String getSynchronizationKey() {
        return model.getSynchronizationKey();
    }
    
    public void makeKeyAvailable(String key) {
        //This may appear redundant, but properly isolates the GUI from
        //the logic of whether or not the attempted selection is valid,
        //and what the resulting model state should be.
        model.setKeyAvailable(key);
        //updateHorizontalPanel();
    }

    public void selectKey(String key) {
        //This may appear redundant, but properly isolates the GUI from
        //the logic of whether or not the attempted selection is valid,
        //and what the resulting model state should be.
        model.setKeySelected(key);
        //updateHorizontalPanel();
    }

    public void makeKeysAvailable(String[] keys) {
        //This may appear redundant, but properly isolates the GUI from
        //the logic of whether or not the attempted selection is valid,
        //and what the resulting model state should be.
        for(String key : keys){
            model.setKeyAvailable(key);
        }
        //updateHorizontalPanel();
    }
        
    public void selectKeys(String[] keys) {
        //This may appear redundant, but properly isolates the GUI from
        //the logic of whether or not the attempted selection is valid,
        //and what the resulting model state should be.
        for(String key : keys){
            model.setKeySelected(key);
        }
        //updateHorizontalPanel();
    }

    public void setSynchronizationKey(String syncKey) {
        model.setSynchronizationKey(syncKey);
        //updateHorizontalPanel();
    }

    public void moveSelectedKeyUp(String keyName){
        model.moveSelectedKeyUp(keyName);
        hPanel.setSelectedKeys(model.getSelectedKeys());
    }
    
    public void moveSelectedKeyDown(String keyName){
        model.moveSelectedKeyDown(keyName);
        hPanel.setSelectedKeys(model.getSelectedKeys());
    }
    
    @Override
    public void setAnnotatedLayer(String layerName) {
        super.setAnnotatedLayer(layerName);
        updateHorizontalPanel();
    }
    
    private void updateHorizontalPanel() {
        hPanel.setAvailableKeys(model.getAvailableKeys());
        hPanel.setSelectedKeys(model.getSelectedKeys());
        hPanel.setSynchronizableKeys(model.getSelectedKeys());
        hPanel.setSynchronizationKey(model.getSynchronizationKey());
    }

    private void updateVerticalPanel() {
        STEP_TYPE stepType = model.getStepType();
        switch (stepType) {
            case AUTOMATIC :
                vPanel.setStepType(STEP_TYPE.AUTOMATIC);
                break;
            case USER_DEFINED :
                vPanel.setStepType(STEP_TYPE.USER_DEFINED);
                break;
            default :
                logger.warning("Unknown vertical annotation step type: " + stepType);
        }

        vPanel.setMajorStep(model.getMajorStep());
        vPanel.setMinorStep(model.getMinorStep());
        vPanel.setLocation(model.getVerticalAnnotationLocation().toString());
    }

    public void setVerticalAnnotationLocation(String annotationLocation) {
        try {
            model.setVerticalAnnotationLocation(LABEL_LOCATION.valueOf(annotationLocation));
        } catch (Exception ex) {
            logger.warning("Unable to set vertical annotation location to '" + annotationLocation + "' due to: " + ex.getMessage());
        }
    }

    public void setMajorStep(String majorStepStr) {
        ValidatorResult validatorResult = StringValidator.validate(majorStepStr, TYPE.DOUBLE, CONSTRAINT.POSITIVE);
        if (validatorResult.isValid()) {
            model.setMajorStep(Double.valueOf(majorStepStr));
        } else {
            vPanel.showValidationFailure(validatorResult.getReason());
        }
    }

    public void setMinorStep(String minorStepStr) {
        ValidatorResult validatorResult = StringValidator.validate(minorStepStr, TYPE.DOUBLE, CONSTRAINT.POSITIVE);
        if (validatorResult.isValid()) {
            model.setMinorStep(Double.valueOf(minorStepStr));
        } else {
            vPanel.showValidationFailure(validatorResult.getReason());
        }
    }

    public Double getMajorStep() {
        return model.getMajorStep();
    }

    public Double getMinorStep() {
        return model.getMinorStep();
    }

    public void setStepType(STEP_TYPE stepType) {
        model.setStepType(stepType);
    }

    public void editAnnotationProperties(String keyName) {
        AnnotationProperties annoProps = model.getAnnotationProperties(model.getAnnotatedLayerName(), keyName);
        AnnotationProperties annoPropsCopy = new AnnotationProperties(annoProps);

        AnnotationPropertiesPanel annoPropsPanel = new AnnotationPropertiesPanel();
        AnnotationPropertiesController controller = new AnnotationPropertiesController(annoPropsCopy, annoPropsPanel);
        EditorDialog dlg = new EditorDialog(
                //(JFrame) SwingUtilities.getRoot(view),
                null,
                annoPropsPanel, controller);

        EditorWorker<AnnotationProperties> worker = new EditorWorker<AnnotationProperties>(
                (EditorResultProcessor) this,
                controller,
                dlg);

        new Thread(worker).start();
    }

    public void processEditorResult(AnnotationProperties annoProps) {
        model.setAnnotationProperties(annoProps.getLayerName(), annoProps.getKeyName(), annoProps);
    }
}