/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.controller.window;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.model.window.WindowSyncProperties;
import com.bhpb.geographics.model.window.WindowSyncProperties.SYNC_FLAG;
import com.bhpb.geographics.ui.window.SyncPropertiesPanel;
import java.util.logging.Logger;
import javax.swing.JComponent;

/**
 * Controls editing of the ViewerWindow's WindowProperties' ScaleProperties.
 * 
 * @author Woody Folsom
 * @version 1.0
 */
public class SyncPropertiesController<T extends Object, U extends JComponent> 
        extends AbstractEditorPanelController <WindowSyncProperties, SyncPropertiesPanel> {
    
    private static final transient Logger logger =
            Logger.getLogger(SyncPropertiesController.class.getName());
    
    public SyncPropertiesController(WindowSyncProperties model, SyncPropertiesPanel view) {
        super(model,view);
    }
    
    public void run() {
        view.setController(this);
        for (SYNC_FLAG flag : model.getSyncFlagSet()) {
            view.setSelected(flag.toString(), model.isEnabled(flag));
        }
        if (!model.isEnabled(SYNC_FLAG.SCROLL_POS_LISTENING)) {
            view.setScrollCheckBoxesEnabled(false);
        }
    }
    
    public void setEnabled(String syncFlagName, boolean enabled) {
        try {
            SYNC_FLAG syncFlag = SYNC_FLAG.valueOf(syncFlagName);
            model.setEnabled(syncFlag, enabled);
            if (syncFlag == SYNC_FLAG.SCROLL_POS_LISTENING) {
                view.setScrollCheckBoxesEnabled(enabled);
            }
        } catch (IllegalArgumentException iae) {
            logger.warning("Illegal SYNC_FLAG name: " + syncFlagName + ", no action taken");
        }
        
    }
}