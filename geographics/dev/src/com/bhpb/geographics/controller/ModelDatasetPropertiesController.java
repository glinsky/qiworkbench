/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.controller;

import com.bhpb.geographics.model.DatasetPropertiesTableModel;
import com.bhpb.geographics.ui.layer.dataset.ModelDatasetPropertiesPanel;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.metadata.ModelFileMetadata;
import com.bhpb.geoio.filesystems.properties.ModelProperties;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * Controller class for ModelProperties model and ModelDatasetPropertiesPanel view.
 * 
 */
public class ModelDatasetPropertiesController
        extends DatasetPropertiesController<ModelProperties, ModelDatasetPropertiesPanel> {

    private static final Logger logger =
            Logger.getLogger(ModelDatasetPropertiesController.class.toString());
    private final boolean mapOrderDataset;
    private Integer selectedLayer;
    private String selectedProperty;

    /**
     * Constructs a ModelDatasetPropertiesController
     * 
     * @param model ModelDatasetProperties to be edited
     * @param view ModelDatasetPropertiesPanel GUI
     */
    public ModelDatasetPropertiesController(ModelProperties model, ModelDatasetPropertiesPanel view) {
        super(model, view);
        mapOrderDataset = model.getMetadata().getGeoDataOrder() == GeoDataOrder.MAP_VIEW_ORDER;
        view.setController(this);
    }

    /**
     * Get the string representation of the selected layer for appending to the
     * ModelDatasetProperties.  If there is no selected layer, the empty
     * String is returned. If the Dataset is in CROSS_SECTION order, then
     * the empty string is also returned.
     * 
     * @return String representation of the selectd layer ":<integer>"
     */
    private String getSelectedLayer() {
        if (mapOrderDataset) {
            if (selectedLayer == null) {
                return "";
            } else {
                return ":" + selectedLayer.toString();
            }
        } else {
            return ""; //only MAP_VIEW_ORDER models have layers

        }
    }

    /**
     * Gets the Integer value of the subString following the first ':' character.
     * If no such delimiter exists, or the substring following it is not a valid
     * Integer, null is returned.
     * 
     * @param selectedProperties String of the form 'propertyName' or 'propertyName:layerNumber'
     * 
     * @return the Integer value of the String after the first ':', or null if 
     * no such Integer exists
     */
    private Integer getIntegerAfterSeparator(String selectedProperties) {
        String[] tokens = selectedProperties.split("[:]");
        if (tokens.length < 2) {
            return null;
        }
        Integer integerValue = null;
        try {
            integerValue = Integer.valueOf(tokens[1]);
        } catch (NumberFormatException nfe) {
            logger.info("selectedProperties: " + selectedProperties + " contains a ':' character which is not followed by a valid Integer");
        }
        
        return integerValue;
    }
    
    /**
     * Gets the subsString of seletedProperties prior to the first ':' character,
     * if any.
     * 
     * @param selectedProperties
     * @return
     */
    private String getStringBeforeSeparator(String selectedProperties) {
        String[] tokens = selectedProperties.split("[:]");
        return tokens[0];
    }
    
    /**
     * Initializes the view to the state of the model, logging a warning 
     * regarding the current workaround to set the selectedProperty to a valid
     * state for MAP_VIEW_ORDER GeoFileMetadata.  Throws a RuntimeException
     * if the GeoFileMetadata.isModelFile() method returns false.
     */
    public void run() {
        GeoFileMetadata geoFileMetadata = model.getMetadata();

        if (geoFileMetadata.isModelFile() == false) {
            throw new RuntimeException("Invalid ModelProperties: ModelProperties.getMetadata().isModelFile() == false.");
        }

        ModelFileMetadata metadata = (ModelFileMetadata) geoFileMetadata;
        String selectedProperties = model.getSelectedProperties();
        selectedProperty = getStringBeforeSeparator(selectedProperties);
        
        view.setFileName(metadata.getGeoFilePath());
        getView().showFileDescription(metadata);
        view.setTableModel(new DatasetPropertiesTableModel(getModel(), getView()));
        view.setProperties(metadata.getProperties().toArray(new String[0]));
        view.setNumLayers(metadata.getNumLayers());
        view.setLayerListVisible(true);
        view.setSelectedProperty(selectedProperty);
        
        if (mapOrderDataset) {
            selectedLayer = getIntegerAfterSeparator(selectedProperties); 
            
            if (selectedLayer != null) {
                view.setSelectedLayer(selectedLayer);
            } else {
                logger.warning("ModelDatasetProperties does not have a valid initial selected Layer field, setting it here.  See GEOIO-7 for more information");
                setSelectedLayer(1);
            }
            
            logger.info("Initial value of selectedProperties is now: " + selectedProperty + getSelectedLayer());
            //Model datasets only support configurable vertical range when in map order
            view.setVerticalRangePanelEnabled(true);
            initVerticalRangePanel(metadata);
        } else {
            view.setNumLayers(0);
            view.setLayerListVisible(false);
            //Model datasets only support configurable vertical range when in map order
            view.setVerticalRangePanelEnabled(false);
        }
    }

    /**
     * Sets the selected layer as requested if the dataset is in MAP_VIEW_ORDER.
     * Otherwise, no action is taken.
     * 
     * @param selectedLayer the 1-based index of the layer to be selected
     */
    public void setSelectedLayer(Integer selectedLayer) {
        if (mapOrderDataset) {
            this.selectedLayer = selectedLayer;
            updateModelSelectedProperty();
        } else {
            return; //only MAP_VIEW_ORDER models have layers

        }
    }

    private void updateModelSelectedProperty() {
        model.setSelectedProperty(selectedProperty + getSelectedLayer());
    }

    public void setSelectedProperty(String selectedProperty) {
        this.selectedProperty = selectedProperty;
        updateModelSelectedProperty();
    }

    @Override
    public void setSelection(SELECTION selection) {
        if (view.applyChosenRanges()) {
            super.setSelection(selection);
        } else {
            JOptionPane.showMessageDialog(null, "Header key chosen range validation failed.");
        }
    }
}