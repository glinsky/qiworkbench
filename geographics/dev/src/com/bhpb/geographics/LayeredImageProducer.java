/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics;

import com.bhpb.geographics.ui.SparseBufferedImage;
import java.awt.Rectangle;

/**
 * Modeled after JRE 1.5 ImageProducer, but sends
 * data to a LayeredImageConsumer instead.
 * 
 * @author woody
 */
public interface LayeredImageProducer {

    /**
     * Return the color associated with the property name.  Implementation should
     * return a non-null Color for ColorMap.HIGHLIGHT because this Color is used
     * for the cursor and other plot decorations.
     * 
     * @param propertyName String
     * 
     * @return Color associated with the given property name, if any, otherwise null.
     */
    //Color getColor(String propertyName);

    /**
     * Rasterize the portion of the image within the context's visible rectangle,
     * using the context's scale and the current LayerDisplayParameters of the
     * GeoRasterizer.
     * 
     * @return SparseBufferedImage containing a BufferedImage and origin point
     */
    SparseBufferedImage rasterize(Rectangle visRect);

    public void addConsumer(LayeredImageConsumer lic);

    public boolean isConsumer(LayeredImageConsumer lic);

    public void removeConsumer(LayeredImageConsumer lic);
}