/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.renderer;

import java.awt.Shape;
import java.awt.geom.GeneralPath;

public class CursorRenderer implements Renderer {
    
    private float width;
    private float height;
    private float x;
    private float y;
    private float xOrigin;
    private float yOrigin;
            
    public CursorRenderer(float width, float height, float x, float y, float xOrigin, float yOrigin) {
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.xOrigin = xOrigin;
        this.yOrigin = yOrigin;                
    }
    
    public Shape render() {
        GeneralPath cursor;
        
        if (x < 0 || y < 0) {
            cursor = null; // crosshair is outside of visible rectangle somehow
        } else if (x >= width || y >= height) {
            cursor = null; // crosshair is outside of visible rectange somehow
        } else {
            cursor = new GeneralPath();
            cursor.moveTo(xOrigin + x, yOrigin);
            cursor.lineTo(xOrigin + x, yOrigin + height - 1f);
            cursor.moveTo(xOrigin, yOrigin + y);
            cursor.lineTo(xOrigin + width -1f, yOrigin + y);
        }
        
        return cursor;
    }
}