/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters.LINE_STYLE;
import com.bhpb.geographics.util.XStreamUtil;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.thoughtworks.xstream.XStream;
import java.awt.Color;
import java.io.StringReader;
import org.w3c.dom.Node;

public class HorizonHighlightSettings implements GeoGraphicsPropertyGroup {

    private LINE_STYLE lineStyle = LINE_STYLE.SOLID;
    private int lineWidth = 6;
    private int colorOpacity = 50;
    private Color lineColor = Color.YELLOW;

    public HorizonHighlightSettings() {
    }

    public HorizonHighlightSettings(HorizonHighlightSettings aHorizonHighlightSettings) {
        this.lineStyle = aHorizonHighlightSettings.lineStyle;
        this.lineWidth = aHorizonHighlightSettings.lineWidth;
        this.colorOpacity = aHorizonHighlightSettings.colorOpacity;
        this.lineColor = aHorizonHighlightSettings.lineColor;
    }

    public int getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(int width) {
        lineWidth = width;
    }

    /**
     * Converst the internal reprsentation of opacity as an integer percentage
     * value to a 8-bit unsigned byte value.
     * 
     * @return color opacity in the range [0..255]
     */
    public int getColorOpacityAsByte() {
        float opacity = colorOpacity;
        float byteScaledOpacity = (opacity * 255.0f / 100.0f);
        return Math.round(byteScaledOpacity);
    }

    public int getColorOpacityAsPercent() {
        return colorOpacity;
    }

    /**
     * Sets color opacity as a percentage of full opaqueness.
     *
     * @param opacity integer in the range [0..100]
     */
    public void setColorOpacityPercent(int opacity) {
        if (colorOpacity < 0 || colorOpacity > 100) {
            throw new IllegalArgumentException("Color opacity cannot be set to a value < 0 or > 100: " + opacity);
        }
        colorOpacity = opacity;
    }

    public Color getLineColor() {
        return lineColor;
    }

    public void setLineColor(Color color) {
        this.lineColor = color;
    }

    public void setLineStyle(LINE_STYLE lineStyle) {
        this.lineStyle = lineStyle;
    }

    public LINE_STYLE getLineStyle() {
        return lineStyle;
    }

    public static String getXmlAlias() {
        return "horizonHighlight";
    }

    /**
     * Deserializes a HorizonHighlightSettings.
     * 
     * @param node w3c.dom.node created from the XML String serialization of PickingSettings
     * 
     * @return the deserialized PickingSettings object
     */
    public static HorizonHighlightSettings restoreState(Node node) {
        XStream xStream = XStreamUtil.getInstance();
        String xml = ElementAttributeReader.nodeToXMLString(node);

        HorizonHighlightSettings settings = (HorizonHighlightSettings) xStream.fromXML(new StringReader(xml));

        return settings;
    }

    /**
     * Serializes the state of this <code>HorizonHighlightSettings</code> as a {@link String}
     * representation of an XML element.
     */
    public String saveState() {
        XStream xStream = XStreamUtil.getInstance();
        return xStream.toXML(this);
    }
}