/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */

package com.bhpb.geographics.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KeyRangeTokenizer {    
    static final private String FLOAT_PATTERN = "(\\-)?((\\d)+(\\.(\\d)+)?)";
    static final private String WHITESPACE_PATTERN = "[\\s]+";
    static final private String DASH_PATTERN = "\\-";
    static final private String LEFT_BRACKET_PATTERN = "\\[";
    static final private String RIGHT_BRACKET_PATTERN = "\\]";
    static final private String WILDCARD_PATTERN = "\\*";
    
    static final Map<KeyRangeToken.TYPE, Pattern> patternMap = new HashMap<KeyRangeToken.TYPE, Pattern>();
    
    public KeyRangeTokenizer() {
        for (KeyRangeToken.TYPE tokenType : KeyRangeToken.TYPE.values()) {
            
            switch (tokenType) {
                case FLOAT :
                    patternMap.put(KeyRangeToken.TYPE.FLOAT, Pattern.compile(FLOAT_PATTERN));
                    break;
                case WHITESPACE :
                    patternMap.put(KeyRangeToken.TYPE.WHITESPACE, Pattern.compile(WHITESPACE_PATTERN));
                    break;
                case DASH : 
                    patternMap.put(KeyRangeToken.TYPE.DASH, Pattern.compile(DASH_PATTERN));
                    break;
                case LEFTBRACKET :
                    patternMap.put(KeyRangeToken.TYPE.LEFTBRACKET, Pattern.compile(LEFT_BRACKET_PATTERN));
                    break;
                case RIGHTBRACKET :
                    patternMap.put(KeyRangeToken.TYPE.RIGHTBRACKET, Pattern.compile(RIGHT_BRACKET_PATTERN));
                    break;
                case WILDCARD :
                    patternMap.put(KeyRangeToken.TYPE.WILDCARD, Pattern.compile(WILDCARD_PATTERN));
                    break;
                case DELIMITER :
                    //do nothing - a delimiter is any single character that does not match a pattern
                    break;
                default :
                    throw new IllegalArgumentException("Unable to get pattern for unknown token type: " + tokenType);
            } 
        }
    }
    
    protected static Pattern getPattern(KeyRangeToken.TYPE tokenType) {
        return patternMap.get(tokenType);
    }
    
    public KeyRangeToken[] getTokens(String keyRange, KeyRangeToken.TYPE tokenType) {
        KeyRangeToken[] tokens = getTokens(keyRange);
        return getTokenSubset(tokens, tokenType);
    }
    
    public KeyRangeToken[] getTokenSubset(KeyRangeToken[] tokens, KeyRangeToken.TYPE tokenType) {
        List<KeyRangeToken> tokenSubset = new LinkedList<KeyRangeToken>();
        for (KeyRangeToken token : tokens) {
            if (token.getType() == tokenType) {
                tokenSubset.add(token);
            }
        }
        return tokenSubset.toArray(new KeyRangeToken[0]);
    }

    /**
     * Returns an array of KeyRangeTokens corresponding to regexes matched in the order
     * in which they appear in the KeyRangeToken.TYPE enum.  Parsing of the DASH token
     * is only attempted after one float has been read but two float have not.  Othwerwise,
     * the '-' character must be part of a float token or it will be parsed as a DELIMITER.
     *
     * The final attempt to match a token starting at a given character position, which always succeeds,
     * is KeyRangeToken.TYPE.DELIMITER, which indicates any single character which is not part of
     * any other KeyRangeToken.TYPE.
     *
     * @param keyRange
     *
     * @return array of KeyRangeTokens parsed from keyRange String
     */
    public KeyRangeToken[] getTokens(String keyRange) {
        if (keyRange == null) {
            return new KeyRangeToken[0];
        }
        
        int currentCharacterPos = 0;
        String remainingKeyRange = keyRange;
        List<KeyRangeToken> tokens = new LinkedList<KeyRangeToken>();
        
        String match = "";
        
        KeyRangeToken.TYPE[] tokenTypes = {
            KeyRangeToken.TYPE.FLOAT,
            KeyRangeToken.TYPE.FLOAT.WHITESPACE,
            KeyRangeToken.TYPE.FLOAT.DASH,
            KeyRangeToken.TYPE.FLOAT.LEFTBRACKET,
            KeyRangeToken.TYPE.FLOAT.RIGHTBRACKET,
            KeyRangeToken.TYPE.FLOAT.WILDCARD,
            KeyRangeToken.TYPE.FLOAT.DELIMITER
        };
        
        //loop until the entire keyRange string has been consumed
        int nFloatsMatched = 0;
        while (currentCharacterPos < keyRange.length()) {
            boolean matchSuccessful = false;
            
            for (int ttIndex = 0; (ttIndex < tokenTypes.length) && (!matchSuccessful); ttIndex++) {
                KeyRangeToken.TYPE tokenType = tokenTypes[ttIndex];
                if (tokenType == KeyRangeToken.TYPE.DELIMITER) {
                    tokens.add(new KeyRangeToken(keyRange.charAt(currentCharacterPos), currentCharacterPos));
                    currentCharacterPos++;
                    matchSuccessful = true; // do not attempt to match a delimiter, by definition it does not match a pattern
                } else {
                    Pattern pattern = patternMap.get(tokenType);
                    
                    if (pattern == null) {
                        throw new IllegalArgumentException("Unable to get pattern for unknown token type: " + tokenType);
                    }
                
                    Matcher matcher = pattern.matcher(remainingKeyRange);
                    if (matcher.find()) {
                        if (matcher.start() == 0) { // token is only taken if it starts at the beginning of the remaining string
                            match = matcher.group();
                            tokens.add(new KeyRangeToken(tokenType, match, currentCharacterPos));
                            currentCharacterPos += match.length();
                            matchSuccessful = true;
                            if (tokenType == KeyRangeToken.TYPE.FLOAT) {
                                nFloatsMatched++;
                            }
                            //Switch the precedence of FLOAT and DASH if 1 float has been read
                            //and switch them back when 2 floats have been read.
                            if (nFloatsMatched == 1) {
                                tokenTypes[0] = KeyRangeToken.TYPE.DASH;
                                tokenTypes[2] = KeyRangeToken.TYPE.FLOAT;
                            } else if (nFloatsMatched == 2) {
                                tokenTypes[0] = KeyRangeToken.TYPE.FLOAT;
                                tokenTypes[2] = KeyRangeToken.TYPE.DASH;
                            }
                        }
                    }
                }
            }
            if (currentCharacterPos < keyRange.length()) {
                remainingKeyRange = keyRange.substring(currentCharacterPos);
            }
        }
        
        return tokens.toArray(new KeyRangeToken[0]);
    }
    
    /**
     * Return a String consisting of the concatenated values off all parser errors
     * contained within the array of tokens.  Each line of the parse error
     * String is separated from the next by a comma and newline.
     *
     * Currently, the only tokens which are considered parser errors are unknown
     * characters, which become tokens of type KeyRangeToken.TYPE.DELIMITER.
     *
     * @param tokens KeyRangeToken[] which is evaluated for parser errors
     * @return String containing all parser errors
     */
    public static String getParserErrors(KeyRangeToken[] tokens) {
        List<String> parserErrorList = new LinkedList<String>();
        
        for (KeyRangeToken token : tokens) {
            if (token.getType() == KeyRangeToken.TYPE.DELIMITER) {
                parserErrorList.add(token.toString());
            }
        }
        
        int nErrors = parserErrorList.size();
        
        StringBuilder sb = new StringBuilder("");
        
        if (nErrors > 0) {
            sb.append(nErrors + " parser errors occurred:\n");
            for (int i = 0; i < nErrors; i ++) {
                sb.append( (i+1) + ": " + parserErrorList.get(i));
                if (i < nErrors - 1) {
                    sb.append(",\n");
                }
            }
        }
        return sb.toString();
    }
}