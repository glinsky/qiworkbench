/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import com.bhpb.geographics.controller.layer.LayerSyncGroup;
import com.bhpb.geographics.model.layer.display.LayerDisplayParameters;
import com.bhpb.geographics.model.layer.display.LocalSubsetProperties;
import com.bhpb.geographics.model.layer.display.LocalSubsetPropertiesFactory;
import com.bhpb.geographics.model.layer.display.MapDisplayParameters;
import com.bhpb.geographics.model.layer.display.MapDisplayParametersFactory;
import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParametersFactory;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties;
import com.bhpb.geographics.model.layer.sync.MapSyncProperties;
import com.bhpb.geographics.model.layer.sync.SeismicXsecSyncProperties;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.SeismicProperties;
import com.bhpb.geoio.util.GeoIOAdapter;
import java.util.List;
import javax.swing.ImageIcon;

/**
 * SeismicLayer - contains all data and context information necessary to generate an
 * image of a Seismic Layer.  This should possibly be merged into the LayerPanel
 * class.
 * 
 * SeismicLayer is final because otherwise calling 
 * getClass().getResource() could be unsafe.
 * 
 * @author Woody Folsom
 */
final public class SeismicLayer extends Layer {

    private static final long serialVersionUID = -5185596599264981605L;
    private static final String SEISMIC_LAYER_ICON = "/icons/SeismicLayer.gif";

    public SeismicLayer(DatasetProperties dsProperties,
            LayerDisplayParameters layerDisplayParams,
            LocalSubsetProperties subsetProps,
            LayerSyncProperties syncProps,
            DataObject[] dataObjects,
            GeoIOAdapter ioAdapter,
            String layerID,
            String layerName,
            boolean allowArbTrav,
            ArbitraryTraverse arbTrav,
            LayerSyncGroup layerSyncGroup,
            WindowModel windowModel) { //minimum number of pixels between rendered traces
        super(dsProperties,
                layerDisplayParams,
                subsetProps,
                syncProps,
                ioAdapter,
                "S",
                dataObjects,
                layerID,
                layerName,
                allowArbTrav,
                arbTrav,
                layerSyncGroup,
                windowModel);
    }

    public SeismicLayer(DatasetProperties dsProperties,
            LayerDisplayParameters layerDisplayParams,
            LocalSubsetProperties subsetProps,
            LayerSyncProperties syncProps,
            DataObject[] dataObjects,
            GeoIOAdapter ioAdapter,
            boolean allowArbTrav,
            LayerSyncGroup layerSyncGroup,
            WindowModel windowModel) { //minimum number of pixels between rendered traces
        super(dsProperties,
                layerDisplayParams,
                subsetProps,
                syncProps,
                ioAdapter,
                "S",
                dataObjects,
                allowArbTrav,
                layerSyncGroup,
                windowModel);
    }

    /**
     * Creates a new SeismicLayer which is associated with the given WindowModel.
     *
     * @param aSeismicLayer
     * @param aWindowModel
     */
    public static SeismicLayer createSeismicLayer(SeismicLayer aSeismicLayer, WindowModel aWindowModel) {
        DatasetProperties dsProps = new SeismicProperties((SeismicProperties)aSeismicLayer.getDatasetProperties());
        LayerDisplayParameters layerDisplayParams;
        LayerSyncProperties layerSyncProps;

        GeoDataOrder orientation = aSeismicLayer.getDatasetProperties().getMetadata().getGeoDataOrder();

        switch (orientation) {
            case CROSS_SECTION_ORDER :
                layerDisplayParams = SeismicXsecDisplayParametersFactory.createLayerDisplayParameters(
                        (SeismicXsecDisplayParameters)aSeismicLayer.getDisplayParameters());
                layerSyncProps = new SeismicXsecSyncProperties((SeismicXsecSyncProperties)aSeismicLayer.getSyncProperties());
                break;
            case MAP_VIEW_ORDER :
                layerDisplayParams = MapDisplayParametersFactory.createLayerDisplayParameters(
                        (MapDisplayParameters)aSeismicLayer.getDisplayParameters());
                layerSyncProps = new MapSyncProperties((MapSyncProperties)aSeismicLayer.getSyncProperties());
                break;
            default :
                throw new IllegalArgumentException("Unable to create new SeismicLayer with unrecognized orientation: " + orientation);
        }

        LocalSubsetProperties localSubsetProps = LocalSubsetPropertiesFactory.createLocalSubsetProperties(aSeismicLayer.getLocalSubsetProperties());

        return new SeismicLayer(
                dsProps,
                layerDisplayParams,
                localSubsetProps,
                layerSyncProps,
                aSeismicLayer.getDataObjects(),
                aSeismicLayer.ioAdapter,
                aSeismicLayer.arbTravEnabled,
                aSeismicLayer.layerSyncGroup,
                aWindowModel
            );
    }

    public double getVerticalStepSize() {
        List<String> keys = getDatasetProperties().getMetadata().getKeys();
        int nKeys = keys.size();
        return getDatasetProperties().getKeyIncrement(keys.get(nKeys - 1));
    }

    public static String getXmlAlias() {
        return "seismicLayer";
    }

    public double getAmplitudeMax() {
        switch (getOrientation()) {
            case MAP_VIEW_ORDER:
                return ((MapDisplayParameters) getDisplayParameters()).getColorAttributes().getMax();
            case CROSS_SECTION_ORDER:
            default:
                return getDatasetProperties().getSummary().getMaxAmplitude();
        }
    }

    public double getAmplitudeMin() {
        switch (getOrientation()) {
            case MAP_VIEW_ORDER:
                return ((MapDisplayParameters) getDisplayParameters()).getColorAttributes().getMin();
            case CROSS_SECTION_ORDER:
            default:
                return getDatasetProperties().getSummary().getMinAmplitude();
        }
    }

    public ImageIcon getImageIcon() {
        return new ImageIcon(getClass().getResource(SEISMIC_LAYER_ICON));
    }

    public ColorMap getColorMap() {
        GeoDataOrder dataOrder = getOrientation();
        switch (dataOrder) {
            case CROSS_SECTION_ORDER :
                return ((SeismicXsecDisplayParameters)getDisplayParameters()).getColorMap();
            case MAP_VIEW_ORDER :
                return ((MapDisplayParameters)getDisplayParameters()).getColorAttributes().getColorMap();
            default :
                throw new UnsupportedOperationException("Cannot get ColorMap for SeismicLayer with unknown orientation: " + dataOrder);
        }
    }
}