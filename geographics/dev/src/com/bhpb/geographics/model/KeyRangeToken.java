/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */

package com.bhpb.geographics.model;

public class KeyRangeToken {
    public enum TYPE { FLOAT, WHITESPACE, DASH, LEFTBRACKET, RIGHTBRACKET, WILDCARD, DELIMITER  };
    
    private final TYPE tokenType;
    private String token;
    private int pos;
    
    public KeyRangeToken(TYPE tokenType, String token, int pos) {
        this.tokenType = tokenType;
        this.token = token;
        this.pos = pos;        
    }
    
    public KeyRangeToken(char delimiter, int pos) {
        this.tokenType = TYPE.DELIMITER;
        this.token = String.valueOf(delimiter);
        this.pos = pos;
    }
    
    public TYPE getType() {
        return tokenType;
    }
    
    public String getToken() {
        return token;
    }
    
    @Override
    public String toString() {
        String tokenString;
        switch (tokenType) {
            case DELIMITER :
                tokenString = "Unknown delimiter '" + token + "' at position " + (pos + 1);
                break;
            default :
                tokenString = "";
                break;
        }
        return tokenString;
    }
}