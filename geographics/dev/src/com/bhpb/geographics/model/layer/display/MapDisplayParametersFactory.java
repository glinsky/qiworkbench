/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.display;

import com.bhpb.geographics.model.layer.shared.BasicLayerProperties;
import com.bhpb.geographics.model.layer.shared.ColorAttributes;
import com.bhpb.geographics.model.layer.shared.DisplayAttributes;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;

public class MapDisplayParametersFactory {

    public static MapDisplayParameters createLayerDisplayParameters(DatasetProperties dsProperties) {
        return createLayerDisplayParameters(dsProperties, new ColorMap(ColorMap.generateDefaultColorMap()));
    }

    public static MapDisplayParameters createLayerDisplayParameters(DatasetProperties dsProperties, ColorMap colorMap) {
        MapDisplayParameters ldParams = new MapDisplayParameters(colorMap);
        ldParams.setLayerName(dsProperties.getMetadata().getGeoFileName());
        ldParams.getColorAttributes().setMin(dsProperties.getSummary().getMinAmplitude());
        ldParams.getColorAttributes().setMax(dsProperties.getSummary().getMaxAmplitude());
        
        return ldParams;
    }

    /**
     * MapDisplayParams copy method, using by rasterizers to avoid exposing internal
     * state via the setDisplayParameters setter.
     *
     * @param aMapDisplayParams
     *
     * @return MapDisplayParameters copied from aMapDisplayParams
     */
    public static MapDisplayParameters createLayerDisplayParameters(MapDisplayParameters aMapDisplayParams) {
        //initialize a new set of MapDisplayParameters using a copy of the colorMap of aMapDisplayParams
        MapDisplayParameters ldParams = new MapDisplayParameters(new ColorMap(aMapDisplayParams.getColorAttributes().getColorMap()));

        ldParams.setLayerName(aMapDisplayParams.getLayerName());

        BasicLayerProperties aBasicProps = aMapDisplayParams.getBasicLayerProperties();
        BasicLayerProperties basicProps = ldParams.getBasicLayerProperties();
        basicProps.setHorizontalScale(aBasicProps.getHorizontalScale());
        basicProps.setVerticalScale(aBasicProps.getVerticalScale());
        basicProps.setOpacity(aBasicProps.getOpacity());
        basicProps.setPolarity(aBasicProps.getPolarity());

        DisplayAttributes aDisplayAttribs = aMapDisplayParams.getDisplayAttributes();
        DisplayAttributes displayAttributes = ldParams.getDisplayAttributes();
        displayAttributes.setEpReversed(aDisplayAttribs.getEpReversed());
        displayAttributes.setCdpReversed(aDisplayAttribs.getCdpReversed());
        displayAttributes.setTransposed(aDisplayAttribs.isTransposed());

        ColorAttributes aColorAttribs = aMapDisplayParams.getColorAttributes();
        ColorAttributes colorAttribs = ldParams.getColorAttributes();
        colorAttribs.setMin(aColorAttribs.getMin());
        colorAttribs.setMax(aColorAttribs.getMax());

        return ldParams;
    }
}