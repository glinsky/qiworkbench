/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.display;

import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;

public class HorizonXsecDisplayParametersFactory {

    public static HorizonXsecDisplayParameters createLayerDisplayParameters(DatasetProperties dsProperties) {
        return createLayerDisplayParameters(dsProperties, new ColorMap(ColorMap.generateDefaultColorMap()));
    }

    public static HorizonXsecDisplayParameters createLayerDisplayParameters(DatasetProperties dsProperties, ColorMap colorMap) {
        HorizonXsecDisplayParameters ldParams = new HorizonXsecDisplayParameters();
        String layerName = ((HorizonProperties)dsProperties).getSelectedHorizon();
        ldParams.setLayerName(layerName);
        return ldParams;
    }

    public static HorizonXsecDisplayParameters createLayerDisplayParameters(HorizonXsecDisplayParameters aDisplayParameters) {
        if (aDisplayParameters == null) {
            return null;
        }
        
        HorizonXsecDisplayParameters displayParams = new HorizonXsecDisplayParameters();

        displayParams.setConnectNonContiguousPoints(aDisplayParameters.getConnectNonContiguousPoints());
        displayParams.setDrawLine(aDisplayParameters.getDrawLine());
        displayParams.setDrawSymbol(aDisplayParameters.getDrawSymbol());
        displayParams.setLineColor(aDisplayParameters.getLineColor());
        displayParams.setLineStyle(aDisplayParameters.getLineStyle());
        displayParams.setLineWidth(aDisplayParameters.getLineWidth());
        displayParams.setSolidFill(aDisplayParameters.getSolidFill());
        displayParams.setSymbolColor(aDisplayParameters.getSymbolColor());
        displayParams.setSymbolHeight(aDisplayParameters.getSymbolHeight());
        displayParams.setSymbolWidth(aDisplayParameters.getSymbolWidth());

        //bugfix: also copy layerName, which is the name of the horizon by default
        displayParams.layerName = aDisplayParameters.layerName;

        return displayParams;
    }
}