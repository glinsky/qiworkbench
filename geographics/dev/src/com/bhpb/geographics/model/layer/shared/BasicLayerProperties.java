/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.model.layer.shared;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geoio.util.ElementAttributeReader;
import java.io.Serializable;
import java.util.logging.Logger;
import org.w3c.dom.Node;

public class BasicLayerProperties implements Serializable, PropertyAccessor {

    public enum FIELD {
        HorizontalScaleMultiplier, Opacity, VerticalScaleMultiplier, Polarity
    }
    
    private static final Logger logger =
            Logger.getLogger(BasicLayerProperties.class.toString());
    private static final long serialVersionUID = 8362269802664839908L;
    public static final String NORMAL_POLARITY = "Normal";
    public static final String REVERSED_POLARITY = "Reversed";
    
    private static transient final FieldProcessor<FIELD> processor = new FieldProcessor<FIELD>(FIELD.values(), BasicLayerProperties.class);
    private Double horizontalScale = 1.0;
    private Double opacity = 1.0;
    private Double verticalScale = 1.0;
    private String polarity = NORMAL_POLARITY;
    
    /**
     * Package-protected constructor to enforce factory usage
     */
    public BasicLayerProperties() {
    }
    
    public BasicLayerProperties(Node node) {
        try {
            setProperty(FIELD.HorizontalScaleMultiplier.toString(), ElementAttributeReader.getString(node, FIELD.HorizontalScaleMultiplier.toString()));
            setProperty(FIELD.Opacity.toString(), ElementAttributeReader.getString(node, FIELD.Opacity.toString()));
            setProperty(FIELD.VerticalScaleMultiplier.toString(), ElementAttributeReader.getString(node, FIELD.VerticalScaleMultiplier.toString()));
            setProperty(FIELD.Polarity.toString(), ElementAttributeReader.getString(node, FIELD.Polarity.toString()));
        } catch (PropertyAccessorException pae) {
            String errMsg = pae.getMessage();
            throw new RuntimeException(errMsg);
        }        
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BasicLayerProperties other = (BasicLayerProperties) obj;
        if (this.horizontalScale != other.horizontalScale && (this.horizontalScale == null || !this.horizontalScale.equals(other.horizontalScale))) {
            return false;
        }
        if (this.opacity != other.opacity && (this.opacity == null || !this.opacity.equals(other.opacity))) {
            return false;
        }
        if (this.verticalScale != other.verticalScale && (this.verticalScale == null || !this.verticalScale.equals(other.verticalScale))) {
            return false;
        }
        if ((this.polarity == null) ? (other.polarity != null) : !this.polarity.equals(other.polarity)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (this.horizontalScale != null ? this.horizontalScale.hashCode() : 0);
        hash = 97 * hash + (this.opacity != null ? this.opacity.hashCode() : 0);
        hash = 97 * hash + (this.verticalScale != null ? this.verticalScale.hashCode() : 0);
        hash = 97 * hash + (this.polarity != null ? this.polarity.hashCode() : 0);
        return hash;
    }

    public double getHorizontalScale() {
        return horizontalScale;
    }

    public double getVerticalScale() {
        return verticalScale;
    }

    public double getOpacity() {
        return opacity;
    }
    
    public String getPolarity() {
        return polarity;
    }

    public String getProperty(String qualifiedField) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        switch (field) {
            case HorizontalScaleMultiplier:
                return horizontalScale.toString();
            case Opacity:
                return opacity.toString();
            case VerticalScaleMultiplier:
                return verticalScale.toString();
            case Polarity:
                return polarity;
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public static String getXmlAlias() {
        return "basicLayerProperties";
    }
    
    /**
     * Serializes this BasicLayerProperties object as XML, converting each FIELD to an XML attribute.
     * 
     * @return the BasicLayerProperties state serialized as XML.
     */
    public String saveState() {
        StringBuffer sbuf = new StringBuffer();

        sbuf.append("<" + getXmlAlias() + " ");
        try {
            //append the FIELDs as XML attributes
            sbuf.append(FIELD.Polarity + "=\""+ getProperty(FIELD.Polarity.toString()) +"\" ");
            sbuf.append(FIELD.HorizontalScaleMultiplier + "=\""+ getProperty(FIELD.HorizontalScaleMultiplier.toString()) +"\" ");
            sbuf.append(FIELD.VerticalScaleMultiplier + "=\""+ getProperty(FIELD.VerticalScaleMultiplier.toString()) +"\" ");
            sbuf.append(FIELD.Opacity + "=\""+ getProperty(FIELD.Opacity.toString()) +"\" ");
        } catch (PropertyAccessorException pae) {
            logger.warning("Unexpected PropertyAccesorException while saving state: " + pae.getMessage());
            throw new RuntimeException(pae);
        }
        
        sbuf.append(" />\n");

        return sbuf.toString();
    }
    
    public void setPolarity(String polarity) {
        this.polarity = polarity;
    }

    public void setHorizontalScale(double horizontalScale) {
        this.horizontalScale = horizontalScale;
    }

    public void setVerticalScale(double verticalScale) {
        this.verticalScale = verticalScale;
    }

    public void setOpacity(double opacity) {
        this.opacity = opacity;
    }

    public void setProperty(String qualifiedField, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        try {
            switch (field) {
                case HorizontalScaleMultiplier:
                    horizontalScale = Double.valueOf(value);
                    break;
                case Opacity:
                    opacity = Double.valueOf(value);
                    break;
                case VerticalScaleMultiplier:
                    verticalScale = Double.valueOf(value);
                    break;
                case Polarity:
                    polarity = value;
                    break;
                default:
                    throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
            }
        } catch (Exception ex) {
            throw new PropertyAccessorException(ex);
        }
    }
}