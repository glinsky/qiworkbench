/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.sync;

import com.bhpb.geographics.util.XStreamUtil;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.thoughtworks.xstream.XStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;

/**
 * Horizon Xsec Sync Properties, consisting of mutable flags
 * <ul>
 * <li>SYNC_FLAG.VISIBILTY</li>
 * <li>SYNC_FLAG.OPACITY</li>
 * <li>SYNC_FLAG.TIME_DEPTH_RANGE</li>
 * <li>SYNC_FLAG.EVENT_ATTRIBUTE</li>
 * </ul>
 * 
 */
public class HorizonXsecSyncProperties extends LayerSyncProperties {

    public HorizonXsecSyncProperties() {
        super(getAllProperties(),
                new ArrayList<SYNC_FLAG>(),
                HorizonXsecSyncProperties.getMutableProperties(),
                getVisibleProperties());
    }

    public HorizonXsecSyncProperties(HorizonXsecSyncProperties that) {
        super(that);
    }

    private static List<SYNC_FLAG> getMutableProperties() {
        List<SYNC_FLAG> mutableProps = getAllProperties();
        return mutableProps;
    }

    private static List<SYNC_FLAG> getVisibleProperties() {
        List<SYNC_FLAG> visibleProps = getAllProperties();
        return visibleProps;
    }

    private static List<SYNC_FLAG> getAllProperties() {
        List<SYNC_FLAG> allProps = new ArrayList<SYNC_FLAG>();

        allProps.add(SYNC_FLAG.VISIBILITY);
        allProps.add(SYNC_FLAG.OPACITY);
        allProps.add(SYNC_FLAG.TIME_DEPTH_RANGE);
        allProps.add(SYNC_FLAG.EVENT_ATTRIBUTE);

        return allProps;
    }

    public static String getXmlAlias() {
        return "horizonXsecSyncProperties";
    }

    public static HorizonXsecSyncProperties restoreState(Node node) {
        XStream xStream = XStreamUtil.getInstance();
        String xml = ElementAttributeReader.nodeToXMLString(node);

        //xStream will deserialize the only non-transient field, enabledMap
        HorizonXsecSyncProperties hxsProps = (HorizonXsecSyncProperties) xStream.fromXML(new StringReader(xml));

        hxsProps.setAllProperties(getAllProperties());
        hxsProps.setMutableProperties(getMutableProperties());
        hxsProps.setVisibleProperties(getVisibleProperties());

        return hxsProps;
    }

    public LayerSyncProperties inheritFrom(LayerSyncProperties ancestorSyncProps) {
        return this;
    }
}