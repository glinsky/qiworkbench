/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.display;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geographics.model.layer.shared.BasicLayerProperties;
import com.bhpb.geographics.model.layer.shared.ColorAttributes;
import com.bhpb.geographics.model.layer.shared.DisplayAttributes;
import com.bhpb.geographics.model.layer.shared.InterpolationSettings;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geoio.util.ElementAttributeReader;
import java.awt.Color;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MapDisplayParameters extends LayerDisplayParameters {

    private static final long serialVersionUID = 249417505073939187L;
    private static transient final FieldProcessor<FIELD> processor = new FieldProcessor<FIELD>(FIELD.values(), MapDisplayParameters.class);
    private DisplayAttributes displayAttributes = new DisplayAttributes();
    private ColorAttributes colorAttribs = new ColorAttributes();

    public enum FIELD {

        LayerName, Layer, DisplayAttribute, ColorAttributes, Scale
    }

    public MapDisplayParameters(ColorMap colorMap) {
        this.colorAttribs.setColorMap(colorMap);
    }

    public MapDisplayParameters(Node node) {
        layerName = ElementAttributeReader.getString(node, "layerName");
        setHidden(ElementAttributeReader.getBoolean(node, "hidden"));

        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            String childName = child.getNodeName();
            if (childName.equals("#text")) {
                continue;
            } else if (childName.equals(BasicLayerProperties.getXmlAlias())) {
                basicProps = new BasicLayerProperties(child);
            } else if (childName.equals(DisplayAttributes.getXmlAlias())) {
                displayAttributes = new DisplayAttributes(child);
            } else if (childName.equals(ColorAttributes.getXmlAlias())) {
                colorAttribs = new ColorAttributes(child);
            } else if (childName.equals(InterpolationSettings.getXmlAlias())) {
                interpolationSettings = new InterpolationSettings(child);
            } else {
                throw new RuntimeException("Unknown child of " + getXmlAlias() + ": " + childName);
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MapDisplayParameters other = (MapDisplayParameters) obj;
        if (this.basicProps != other.basicProps && (this.basicProps == null || !this.basicProps.equals(other.basicProps))) {
            return false;
        }
        if (this.displayAttributes != other.displayAttributes && (this.displayAttributes == null || !this.displayAttributes.equals(other.displayAttributes))) {
            return false;
        }
        if (this.colorAttribs != other.colorAttribs && (this.colorAttribs == null || !this.colorAttribs.equals(other.colorAttribs))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.basicProps != null ? this.basicProps.hashCode() : 0);
        hash = 41 * hash + (this.displayAttributes != null ? this.displayAttributes.hashCode() : 0);
        hash = 41 * hash + (this.colorAttribs != null ? this.colorAttribs.hashCode() : 0);
        return hash;
    }

    public ColorAttributes getColorAttributes() {
        return colorAttribs;
    }

    public DisplayAttributes getDisplayAttributes() {
        return displayAttributes;
    }

    public String getProperty(String qualifiedFieldName) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedFieldName);
        String remainder = processor.getFieldRemainder(qualifiedFieldName);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        switch (field) {
            case LayerName:
                return layerName;
            case Layer:
            case Scale:
                return basicProps.getProperty(remainder);
            case DisplayAttribute:
                return displayAttributes.getProperty(remainder);
            case ColorAttributes:
                return colorAttribs.getProperty(remainder);
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public static String getXmlAlias() {
        return "mapDisplayParameters";
    }

    public String saveState() {
        StringBuffer content = new StringBuffer();

        content.append("<" + getXmlAlias() + " ");
        content.append("layerName=\"" + layerName + "\" ");
        content.append("hidden=\"" + isHidden() + "\" ");
        content.append(">\n");

        content.append(basicProps.saveState());
        content.append(displayAttributes.saveState());
        content.append(colorAttribs.saveState());

        content.append("</" + getXmlAlias() + ">\n");

        return content.toString();
    }

    public void setProperty(String qualifiedFieldname, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedFieldname);
        String remainder = processor.getFieldRemainder(qualifiedFieldname);

        //if ("".equals(remainder) == false) {
        //    throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        //}

        try {
            switch (field) {
                case LayerName:
                    layerName = value;
                    break;
                case Layer:
                case Scale:
                    basicProps.setProperty(remainder, value);
                    break;
                case DisplayAttribute:
                    displayAttributes.setProperty(remainder, value);
                    break;
                case ColorAttributes:
                    colorAttribs.setProperty(remainder, value);
                    break;
                default:
                    throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
            }
        } catch (Exception ex) {
            throw new PropertyAccessorException(ex);
        }
    }

    public Color getBackgroundColor() {
        return colorAttribs.getColorMap().getBackgroundColor();
    }

    /**
     * Returns a copy of this MapDisplayParameters, with all properties except
     * name, colormap min and colormap max set to match the corresponding fields of ancestor,
     * if aLayerDisplayParameters is also a MapDisplayParameters.  If ancestor
     * is of any other type, then this MapDisplayParameters is returned instead*
     *
     * *if the ancestor is not a HorizonXsecLayerDisplayParameters, then the ColorMap _is_ inherited
     *
     * @param ancestor LayerDisplayParameters used for inheritance
     * @throws IllegalArgumentException if ancestor is not an intance of SeismicXsec, ModelXsec, HorizonXsec or MapDisplayParameters
     *
     * @return LayerDisplayParameters inherited from ancestor
     */
    public LayerDisplayParameters inheritFrom(LayerDisplayParameters ancestor) {
        if (ancestor == null) {
            return this;
        } else
        //inherit nothing from Horizon xsec display params
        if (ancestor instanceof HorizonXsecDisplayParameters) {
            return this;
        } //inherit only ColorMap from SeismicXsec or Map display params
        else if (ancestor instanceof SeismicXsecDisplayParameters) {
            SeismicXsecDisplayParameters seismicXsecDisplayParams = (SeismicXsecDisplayParameters) ancestor;
            colorAttribs.setColorMap(seismicXsecDisplayParams.getColorMap());
            return this;
        } else if (ancestor instanceof ModelXsecDisplayParameters) {
            ModelXsecDisplayParameters modelXsecDisplayParams = (ModelXsecDisplayParameters) ancestor;
            colorAttribs.setColorMap(modelXsecDisplayParams.getColorMap());
            return this;
        } else if (ancestor instanceof MapDisplayParameters) {
            MapDisplayParameters ldParamsCopy =
                    MapDisplayParametersFactory.createLayerDisplayParameters(this);
            MapDisplayParameters mapDisplayParams = (MapDisplayParameters) ancestor;

            DisplayAttributes aDisplayAttribs = mapDisplayParams.getDisplayAttributes();
            DisplayAttributes displayAttribs = ldParamsCopy.getDisplayAttributes();

            displayAttribs.setEpReversed(aDisplayAttribs.getEpReversed());
            displayAttribs.setCdpReversed(aDisplayAttribs.getCdpReversed());
            displayAttribs.setTransposed(aDisplayAttribs.isTransposed());

            ColorAttributes colorAttrs = ldParamsCopy.getColorAttributes();
            ColorAttributes aColorAttribs = mapDisplayParams.getColorAttributes();

            colorAttrs.setColorMap(aColorAttribs.getColorMap());

            return ldParamsCopy;
        } else {
            throw new IllegalArgumentException("Cannot inherit from unknown LayerDisplayParameters subclass: " +
                    ancestor.getClass().getName());
        }
    }

    public void setColorMap(ColorMap colorMap) {
        colorAttribs.setColorMap(colorMap);
    }
}