/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.shared;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geoio.util.ElementAttributeReader;
import java.io.Serializable;
import java.util.logging.Logger;
import org.w3c.dom.Node;

public class RasterizationTypeModel implements Serializable, PropertyAccessor {
    private static final long serialVersionUID = -1733905923155717418L;

    private static transient final Logger logger =
            Logger.getLogger(RasterizationTypeModel.class.toString());
    private static transient final FieldProcessor<FIELD> processor =
            new FieldProcessor<FIELD>(FIELD.values(), RasterizationTypeModel.class);
    private Boolean rasterizeInterpolatedDensity;
    private Boolean rasterizeNegativeFill;
    private Boolean rasterizePositiveFill;
    private Boolean rasterizeVariableDensity;
    private Boolean rasterizeWiggleTraces;

    public enum FIELD {

        WILDCARD, Interpolated_Density, Negative_Fill, Positive_Fill,
        Variable_Density, Wiggle_Trace
    }

    public RasterizationTypeModel() {
    }

    public RasterizationTypeModel(Node node) {
        try {
            setProperty(FIELD.Interpolated_Density.toString(), ElementAttributeReader.getString(node, FIELD.Interpolated_Density.toString()));
            setProperty(FIELD.Negative_Fill.toString(), ElementAttributeReader.getString(node, FIELD.Negative_Fill.toString()));
            setProperty(FIELD.Positive_Fill.toString(), ElementAttributeReader.getString(node, FIELD.Positive_Fill.toString()));
            setProperty(FIELD.Variable_Density.toString(), ElementAttributeReader.getString(node, FIELD.Variable_Density.toString()));
            setProperty(FIELD.Wiggle_Trace.toString(), ElementAttributeReader.getString(node, FIELD.Wiggle_Trace.toString()));
        } catch (PropertyAccessorException pae) {
            String errMsg = pae.getMessage();
            throw new RuntimeException(errMsg);
        }
    }

    public Boolean doRasterizeInterpolatedDensity() {
        return rasterizeInterpolatedDensity;
    }

    public Boolean doRasterizeNegativeFill() {
        return rasterizeNegativeFill;
    }

    public Boolean doRasterizePositiveFill() {
        return rasterizePositiveFill;
    }

    public Boolean doRasterizeVariableDensity() {
        return rasterizeVariableDensity;
    }

    public Boolean doRasterizeWiggleTraces() {
        return rasterizeWiggleTraces;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RasterizationTypeModel other = (RasterizationTypeModel) obj;
        if (this.rasterizeInterpolatedDensity != other.rasterizeInterpolatedDensity && (this.rasterizeInterpolatedDensity == null || !this.rasterizeInterpolatedDensity.equals(other.rasterizeInterpolatedDensity))) {
            return false;
        }
        if (this.rasterizeNegativeFill != other.rasterizeNegativeFill && (this.rasterizeNegativeFill == null || !this.rasterizeNegativeFill.equals(other.rasterizeNegativeFill))) {
            return false;
        }
        if (this.rasterizePositiveFill != other.rasterizePositiveFill && (this.rasterizePositiveFill == null || !this.rasterizePositiveFill.equals(other.rasterizePositiveFill))) {
            return false;
        }
        if (this.rasterizeVariableDensity != other.rasterizeVariableDensity && (this.rasterizeVariableDensity == null || !this.rasterizeVariableDensity.equals(other.rasterizeVariableDensity))) {
            return false;
        }
        if (this.rasterizeWiggleTraces != other.rasterizeWiggleTraces && (this.rasterizeWiggleTraces == null || !this.rasterizeWiggleTraces.equals(other.rasterizeWiggleTraces))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + (this.rasterizeInterpolatedDensity != null ? this.rasterizeInterpolatedDensity.hashCode() : 0);
        hash = 29 * hash + (this.rasterizeNegativeFill != null ? this.rasterizeNegativeFill.hashCode() : 0);
        hash = 29 * hash + (this.rasterizePositiveFill != null ? this.rasterizePositiveFill.hashCode() : 0);
        hash = 29 * hash + (this.rasterizeVariableDensity != null ? this.rasterizeVariableDensity.hashCode() : 0);
        hash = 29 * hash + (this.rasterizeWiggleTraces != null ? this.rasterizeWiggleTraces.hashCode() : 0);
        return hash;
    }

    public void setRasterizeInterpolatedDensity(boolean enabled) {
        rasterizeInterpolatedDensity = enabled;
    }

    public void setRasterizeNegativeFill(boolean enabled) {
        rasterizeNegativeFill = enabled;
    }

    public void setRasterizePositiveFill(boolean enabled) {
        rasterizePositiveFill = enabled;
    }

    public void setRasterizeVariableDensity(boolean enabled) {
        rasterizeVariableDensity = enabled;
    }

    public void setRasterizeWiggleTraces(boolean enabled) {
        rasterizeWiggleTraces = enabled;
    }

    /**
     * Serializes this RasterizationTypeModel object as XML, converting each FIELD to an XML attribute.
     * 
     * @return the RasterizationTypeModel state serialized as XML.
     */
    public String saveState() {
        StringBuffer sbuf = new StringBuffer();

        sbuf.append("<" + getXmlAlias() + " ");
        try {
            //append the FIELDs as XML attributes
            sbuf.append(FIELD.Wiggle_Trace + "=\"" + getProperty(FIELD.Wiggle_Trace.toString()) + "\" ");
            sbuf.append(FIELD.Positive_Fill + "=\"" + getProperty(FIELD.Positive_Fill.toString()) + "\" ");
            sbuf.append(FIELD.Negative_Fill + "=\"" + getProperty(FIELD.Negative_Fill.toString()) + "\" ");
            sbuf.append(FIELD.Variable_Density + "=\"" + getProperty(FIELD.Variable_Density.toString()) + "\" ");
            sbuf.append(FIELD.Interpolated_Density + "=\"" + getProperty(FIELD.Interpolated_Density.toString()) + "\" ");
        } catch (PropertyAccessorException pae) {
            logger.warning("Unexpected PropertyAccesorException while saving state: " + pae.getMessage());
            throw new RuntimeException(pae);
        }

        sbuf.append(" />\n");

        return sbuf.toString();
    }

    public String getProperty(String qualifiedField) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        switch (field) {
            case Interpolated_Density:
                return FieldProcessor.valueOf(rasterizeInterpolatedDensity);
            case Negative_Fill:
                return FieldProcessor.valueOf(rasterizeNegativeFill);
            case Positive_Fill:
                return FieldProcessor.valueOf(rasterizePositiveFill);
            case Variable_Density:
                return FieldProcessor.valueOf(rasterizeVariableDensity);
            case Wiggle_Trace:
                return FieldProcessor.valueOf(rasterizeWiggleTraces);
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public static String getXmlAlias() {
        return "rasterizationProperties";
    }

    public void setProperty(String qualifiedField, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        try {
            switch (field) {
                case Interpolated_Density:
                    setRasterizeInterpolatedDensity(FieldProcessor.booleanValue(value));
                    break;
                case Negative_Fill:
                    setRasterizeNegativeFill(FieldProcessor.booleanValue(value));
                    break;
                case Positive_Fill:
                    setRasterizePositiveFill(FieldProcessor.booleanValue(value));
                    break;
                case Variable_Density:
                    setRasterizeVariableDensity(FieldProcessor.booleanValue(value));
                    break;
                case Wiggle_Trace:
                    setRasterizeWiggleTraces(FieldProcessor.booleanValue(value));
                    break;
                case WILDCARD:
                    boolean parsedValue = FieldProcessor.booleanValue(value);
                    setRasterizeInterpolatedDensity(parsedValue);
                    setRasterizeNegativeFill(parsedValue);
                    setRasterizePositiveFill(parsedValue);
                    setRasterizeVariableDensity(parsedValue);
                    setRasterizeWiggleTraces(parsedValue);
                    break;
                default:
                    throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
            }
        } catch (Exception ex) {
            throw new PropertyAccessorException(ex);
        }
    }
}