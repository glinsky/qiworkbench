/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.display;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geographics.util.XStreamUtil;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.thoughtworks.xstream.XStream;
import java.io.Serializable;
import java.io.StringReader;
import org.w3c.dom.Node;

public class LocalSubsetProperties implements Serializable, PropertyAccessor {

    private static transient final FieldProcessor<FIELD> processor = new FieldProcessor<FIELD>(FIELD.values(), LocalSubsetProperties.class);
    private static final long serialVersionUID = 8837210857879379192L;

    enum FIELD {

        pkField, pkStartValue, pkEndValue, pkStepValue,
        applyGaps, gapSize, skValue, skStartValue, skEndValue, skStepValue,
        startTime, endTime
    }
    private Boolean applyGaps;
    private Double endTime;
    private Double skStartValue;
    private Double skEndValue;
    private Double skStepValue;
    private Double startTime;
    private Integer gapSize;
    private Integer pkStartValue;
    private Integer pkEndValue;
    private Integer pkStepValue;
    private String pkField;
    private String skValue;

    /**
     * Package-protected to enforce factory use.
     */
    LocalSubsetProperties() {
    }

    /**
     * Package-protected to enforce factory use.
     */
    LocalSubsetProperties(LocalSubsetProperties that) {
        this.applyGaps = that.applyGaps;
        this.endTime = that.endTime;
        this.skStartValue = that.skStartValue;
        this.skEndValue = that.skEndValue;
        this.skStepValue = that.skStepValue;
        this.startTime = that.startTime;
        this.gapSize = that.gapSize;
        this.pkStartValue = that.pkStartValue;
        this.pkEndValue = that.pkEndValue;
        this.pkStepValue = that.pkStepValue;
        this.pkField = that.pkField;
        this.skValue = that.skValue;
    }

    public String getProperty(String qualifiedField) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        switch (field) {
            case pkField:
                return pkField;
            case pkStartValue:
                return pkStartValue.toString();
            case pkEndValue:
                return pkEndValue.toString();
            case pkStepValue:
                return pkStepValue.toString();
            case applyGaps:
                return applyGaps.toString();
            case gapSize:
                return gapSize.toString();
            case skValue:
                return skValue;
            case skStartValue:
                return skStartValue.toString();
            case skEndValue:
                return skEndValue.toString();
            case skStepValue:
                if (skStepValue != null) {
                    return skStepValue.toString();
                } else {
                    return "";
                }
            case startTime:
                return startTime.toString();
            case endTime:
                return endTime.toString();
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public static LocalSubsetProperties restoreState(Node node) {
        XStream xStream = XStreamUtil.getInstance();
        String xml = ElementAttributeReader.nodeToXMLString(node);
        return (LocalSubsetProperties) xStream.fromXML(new StringReader(xml));
    }

    public String saveState() {
        StringBuffer content = new StringBuffer();

        XStream xStream = XStreamUtil.getInstance();
        content.append(xStream.toXML(this));
        content.append("\n");

        return content.toString();
    }

    public void setPrimaryKey(String pkField, int pkStartValue, int pkEndValue,
            int pkStepValue, boolean applyGaps, int gapSize) {
        this.pkField = pkField;
        this.pkStartValue = pkStartValue;
        this.pkEndValue = pkEndValue;
        this.pkStepValue = pkStepValue;
        this.applyGaps = applyGaps;
        this.gapSize = gapSize;
    }

    public void setSecondaryKey(String skValue, double skStartValue,
            double skEndValue, double skStepValue) {
        this.skValue = skValue;
        this.skStartValue = skStartValue;
        this.skEndValue = skEndValue;
        this.skStepValue = skStepValue;
    }

    public String getPrimaryKeyField() {
        return pkField;
    }

    public int getPrimaryKeyStartValue() {
        return pkStartValue.intValue();
    }

    public int getPrimaryKeyEndValue() {
        return pkEndValue.intValue();
    }

    public int getPrimaryKeyStepValue() {
        return pkStepValue.intValue();
    }

    public boolean doApplyGaps() {
        return applyGaps;
    }

    public int getGapSize() {
        return gapSize;
    }

    /**
     * This accessor is named ...Value rather than ...Field as in the Primary Key
     * accessor because the bhpViewer UI gives these JComboBoxes different
     * labels, and this model was designed based on that user interface.
     */
    public String getSecondaryKeyValue() {
        return skValue;
    }

    public static String getXmlAlias() {
        return "localSubsetProperties";
    }

    public double getSecondaryKeyStartValue() {
        return skStartValue.doubleValue();
    }

    public double getSecondaryKeyEndValue() {
        return skEndValue.doubleValue();
    }

    public double getSecondaryKeyStepValue() {
        return skStepValue.doubleValue();
    }

    public void setApplyGapsEnabled(boolean enabled) {
        this.applyGaps = enabled;
    }

    public void setGapSize(int gapSize) {
        this.gapSize = gapSize;
    }

    public void setProperty(String qualifiedField, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        try {
            switch (field) {
                case pkField:
                    pkField = value;
                    break;
                case pkStartValue:
                    pkStartValue = Integer.valueOf(value);
                    break;
                case pkEndValue:
                    pkEndValue = Integer.valueOf(value);
                    break;
                case pkStepValue:
                    pkStepValue = Integer.valueOf(value);
                    break;
                case applyGaps:
                    applyGaps = Boolean.valueOf(value);
                    break;
                case gapSize:
                    gapSize = Integer.valueOf(value);
                    break;
                case skValue:
                    skValue = value;
                    break;
                case skStartValue:
                    skStartValue = Double.valueOf(value);
                    break;
                case skEndValue:
                    skEndValue = Double.valueOf(value);
                    break;
                case skStepValue:
                    if ("".equals(value)) {
                        skStepValue = null;
                    } else {
                        skStepValue = Double.valueOf(value);
                    }
                    break;
                case startTime:
                    startTime = Double.valueOf(value);
                    break;
                case endTime:
                    endTime = Double.valueOf(value);
                    break;
                default:
                    throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
            }
        } catch (Exception ex) {
            throw new PropertyAccessorException(ex);
        }
    }

    public double getStartTime() {
        return startTime;
    }

    public void setStartTime(double startTime) {
        this.startTime = startTime;
    }

    public double getEndTime() {
        return endTime;
    }

    public void setEndTime(double endTime) {
        this.endTime = endTime;
    }

    public void setPkStart(int pkStart) {
        this.pkStartValue = pkStart;
    }

    public void setPkStep(int pkStep) {
        this.pkStepValue = pkStep;
    }

    public void setPkEnd(int pkStart) {
        this.pkStartValue = pkStart;
    }

    public void setSkEnd(double skEnd) {
        this.skEndValue = skEnd;
    }

    public void setSkStart(double skStart) {
        this.skStartValue = skStart;
    }

    public void setSkStep(double skStep) {
        this.skStepValue = skStep;
    }
}