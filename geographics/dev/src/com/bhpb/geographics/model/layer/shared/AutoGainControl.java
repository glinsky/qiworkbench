/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.model.layer.shared;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geoio.util.ElementAttributeReader;
import java.io.Serializable;
import java.util.logging.Logger;
import org.w3c.dom.Node;

public class AutoGainControl implements Serializable, PropertyAccessor {

    private static transient final FieldProcessor<FIELD> processor = new FieldProcessor<FIELD>(FIELD.values(), AutoGainControl.class);
    private static transient final Logger logger =
            Logger.getLogger(AutoGainControl.class.toString());
    private static final long serialVersionUID = 6184493403506452147L;    
    
    public enum FIELD {

        Apply, WindowLength, Units
    }

    public enum UNITS {
        Sample, Time
    }

    private Boolean applyAGC = Boolean.FALSE;
    private Integer windowLength = 250;
    private UNITS units = UNITS.Sample;

    public AutoGainControl() {
    }

    public AutoGainControl(Node node) {
        try {
            setProperty(FIELD.Apply.toString(), ElementAttributeReader.getString(node, FIELD.Apply.toString()));
            setProperty(FIELD.WindowLength.toString(), ElementAttributeReader.getString(node, FIELD.WindowLength.toString()));
            setProperty(FIELD.Units.toString(), ElementAttributeReader.getString(node, FIELD.Units.toString()));
        } catch (PropertyAccessorException pae) {
            String errMsg = pae.getMessage();
            throw new RuntimeException(errMsg);
        }
    }

    public Boolean doApplyAutoGainControl() {
        return applyAGC;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AutoGainControl other = (AutoGainControl) obj;
        if (this.applyAGC != other.applyAGC && (this.applyAGC == null || !this.applyAGC.equals(other.applyAGC))) {
            return false;
        }
        if (this.windowLength != other.windowLength && (this.windowLength == null || !this.windowLength.equals(other.windowLength))) {
            return false;
        }
        if (this.units != other.units) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.applyAGC != null ? this.applyAGC.hashCode() : 0);
        hash = 47 * hash + (this.windowLength != null ? this.windowLength.hashCode() : 0);
        hash = 47 * hash + (this.units != null ? this.units.hashCode() : 0);
        return hash;
    }

    public String getProperty(String qualifiedField) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        switch (field) {
            case Apply:
                return FieldProcessor.valueOf(applyAGC);
            case WindowLength:
                return windowLength.toString();
            case Units:
                return units.toString();
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public UNITS getUnits() {
        return units;
    }

    public int getWindowLength() {
        return windowLength;
    }

    public static String getXmlAlias() {
        return "autoGainControl";
    }

    public boolean isEnabled() {
        return applyAGC;
    }

    /**
     * Serializes this AutoGainControl object as XML, converting each FIELD to an XML attribute.
     * 
     * @return the AutoGainControl state serialized as XML.
     */
    public String saveState() {
        StringBuffer sbuf = new StringBuffer();

        sbuf.append("<" + getXmlAlias() + " ");
        try {
            //append the FIELDs as XML attributes
            sbuf.append(FIELD.Apply + "=\"" + getProperty(FIELD.Apply.toString()) + "\" ");
            sbuf.append(FIELD.WindowLength + "=\"" + getProperty(FIELD.WindowLength.toString()) + "\" ");
            sbuf.append(FIELD.Units + "=\"" + getProperty(FIELD.Units.toString()) + "\" ");
        } catch (PropertyAccessorException pae) {
            logger.warning("Unexpected PropertyAccesorException while saving state: " + pae.getMessage());
            throw new RuntimeException(pae);
        }

        sbuf.append(" />\n");

        return sbuf.toString();
    }

    public void setEnabled(boolean enabled) {
        applyAGC = enabled;
    }

    public void setProperty(String qualifiedField, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        switch (field) {
            case Apply:
                applyAGC = FieldProcessor.booleanValue(value);
                break;
            case WindowLength:
                windowLength = Integer.valueOf(value);
                break;
            case Units:
                units = UNITS.valueOf(value);
                break;
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public void setUnits(UNITS units) {
        this.units = units;
    }

    public void setWindowLength(int value) {
        windowLength = value;
    }
}