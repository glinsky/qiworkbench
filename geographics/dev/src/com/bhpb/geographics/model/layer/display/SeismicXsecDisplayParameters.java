/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.display;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geographics.model.layer.shared.AutoGainControl;
import com.bhpb.geographics.model.layer.shared.BasicLayerProperties;
import com.bhpb.geographics.model.layer.shared.ColorMapModel;
import com.bhpb.geographics.model.layer.shared.Culling;
import com.bhpb.geographics.model.layer.shared.InterpolationSettings;
import com.bhpb.geographics.model.layer.shared.Normalization;
import com.bhpb.geographics.model.layer.shared.RasterizationTypeModel;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geoio.util.ElementAttributeReader;
import java.awt.Color;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Display parameters specific to Seismic Cross-section layers.
 * 
 */
public class SeismicXsecDisplayParameters extends LayerDisplayParameters {

    private static transient final FieldProcessor<FIELD> processor = new FieldProcessor<FIELD>(FIELD.values(), SeismicXsecDisplayParameters.class);
    private static final long serialVersionUID = 2870315817593464051L;

    /**
     * Fields reflect GUI Component groups, with the exception of Culling, which
     * wraps 'Clipping_Factor' and 'Decimation_Spacing' for reuse by ModelXsecDisplayParameters.
     */
    public enum FIELD {

        AutoGainControl, RasterizingType, ClippingFactor, DecimationSpacing, Normalization,
        LayerName, Layer, Scale, Interpolation
    }
    private AutoGainControl autoGainControl = new AutoGainControl();
    private ColorMapModel colorMapModel = new ColorMapModel(ColorMap.DEFAULT_COLORMAP);
    private Culling culling = new Culling();
    private Normalization normalization = new Normalization();
    private RasterizationTypeModel rasterProps = new RasterizationTypeModel();

    /**
     * Package-protected constructor to enforce factory usage
     */
    SeismicXsecDisplayParameters() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SeismicXsecDisplayParameters other = (SeismicXsecDisplayParameters) obj;
        if (this.autoGainControl != other.autoGainControl && (this.autoGainControl == null || !this.autoGainControl.equals(other.autoGainControl))) {
            return false;
        }
        if (this.basicProps != other.basicProps && (this.basicProps == null || !this.basicProps.equals(other.basicProps))) {
            return false;
        }
        if (this.colorMapModel != other.colorMapModel && (this.colorMapModel == null || !this.colorMapModel.equals(other.colorMapModel))) {
            return false;
        }
        if (this.culling != other.culling && (this.culling == null || !this.culling.equals(other.culling))) {
            return false;
        }
        if (this.normalization != other.normalization && (this.normalization == null || !this.normalization.equals(other.normalization))) {
            return false;
        }
        if (this.rasterProps != other.rasterProps && (this.rasterProps == null || !this.rasterProps.equals(other.rasterProps))) {
            return false;
        }
        if (this.interpolationSettings != other.interpolationSettings && (this.interpolationSettings == null || !this.interpolationSettings.equals(other.interpolationSettings))) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + (this.autoGainControl != null ? this.autoGainControl.hashCode() : 0);
        hash = 67 * hash + (this.basicProps != null ? this.basicProps.hashCode() : 0);
        hash = 67 * hash + (this.colorMapModel != null ? this.colorMapModel.hashCode() : 0);
        hash = 67 * hash + (this.culling != null ? this.culling.hashCode() : 0);
        hash = 67 * hash + (this.normalization != null ? this.normalization.hashCode() : 0);
        hash = 67 * hash + (this.rasterProps != null ? this.rasterProps.hashCode() : 0);
        return hash;
    }

    public SeismicXsecDisplayParameters(Node node) {
        layerName = ElementAttributeReader.getString(node, "layerName");
        setHidden(ElementAttributeReader.getBoolean(node, "hidden"));
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            String childName = child.getNodeName();
            if (childName.equals("#text")) {
                continue;
            } else if (childName.equals(AutoGainControl.getXmlAlias())) {
                autoGainControl = new AutoGainControl(child);
            } else if (childName.equals(BasicLayerProperties.getXmlAlias())) {
                basicProps = new BasicLayerProperties(child);
            } else if (childName.equals(Culling.getXmlAlias())) {
                culling = new Culling(child);
            } else if (childName.equals(Normalization.getXmlAlias())) {
                normalization = new Normalization(child);
            } else if (childName.equals(RasterizationTypeModel.getXmlAlias())) {
                rasterProps = new RasterizationTypeModel(child);
            } else if (childName.equals(ColorMapModel.getXmlAlias())) {
                colorMapModel = ColorMapModel.restoreState(child);
            } else if (childName.equals(InterpolationSettings.getXmlAlias())) {
                interpolationSettings = new InterpolationSettings(child);
            } else {
                throw new RuntimeException("Unknown child of " + getXmlAlias() + ": " + childName);
            }
        }
    }

    public AutoGainControl getAutoGainControl() {
        return autoGainControl;
    }

    /**
     * Gets the background color from the ColorMap.
     * 
     * @return Color associated with property ColorMap.BACKGROUND
     */
    public Color getBackgroundColor() {
        return colorMapModel.getColorMap().getBackgroundColor();
    }

    /**
     * Gets the ColorMap.
     * 
     * @return the ColorMap associated with this SeismicXsecDisplayParameters.
     */
    public ColorMap getColorMap() {
        return colorMapModel.getColorMap();
    }

    public ColorMapModel getColorMapModel() {
        return colorMapModel;
    }

    /**
     * Gets the Culling parameters.
     * 
     * @return the Culling parameters associated with this set of SeismicXsecDisplayParameters
     */
    public Culling getCulling() {
        return culling;
    }

    /**
     * Gets the normalization properties.
     * 
     * @return the NormalizationProperties associated with this SeismicXsecDisplayParameters.
     */
    public Normalization getNormalizationProperties() {
        return normalization;
    }

    /**
     * PropertyAccessor getter. Accepts any qualifiedField String which begins
     * with a String value of FIELD.
     * 
     * @param qualifiedField String of the format "AutoGainControl.Apply"
     * 
     * @return the String representation of the property mapped to the qualifiedField
     * 
     * @throws com.bhpb.geographics.accessor.PropertyAccessorException if
     * the qualifiedField String is invalid for SeismicXsecDisplayParameters
     */
    public String getProperty(String qualifiedField) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        switch (field) {
            case AutoGainControl:
                return autoGainControl.getProperty(remainder);
            //bug fix: qualified field names omit 'Culling' because it is not found in GUI
            case ClippingFactor:
            case DecimationSpacing:
                return culling.getProperty(field.toString());
            case LayerName:
                return layerName;
            case Normalization:
                return normalization.getProperty(remainder);
            case RasterizingType:
                return rasterProps.getProperty(remainder);
            case Layer:
            case Scale:
                return basicProps.getProperty(remainder);
            case Interpolation:
                return interpolationSettings.getProperty(remainder);
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    /**
     * Gets the RasterizationTypeModel.
     * 
     * @return the RasterizationTypeModel associated with this SeismicXsecDisplayParameters.
     */
    public RasterizationTypeModel getRasterizationTypeModel() {
        return rasterProps;
    }

    public static String getXmlAlias() {
        return "seismicXsecDisplayParameters";
    }

    public String saveState() {
        StringBuffer content = new StringBuffer();

        content.append("<" + getXmlAlias() + " ");
        content.append("layerName=\"" + layerName + "\" ");
        content.append("hidden=\"" + isHidden() + "\" ");
        content.append(">\n");

        content.append(basicProps.saveState());
        content.append(normalization.saveState());
        content.append(autoGainControl.saveState());
        content.append(rasterProps.saveState());
        content.append(culling.saveState());
        content.append(colorMapModel.saveState());
        content.append(interpolationSettings.saveState());

        content.append("</" + getXmlAlias() + ">\n");

        return content.toString();
    }

    /**
     * PropertyAccessor setter.
     * 
     * @param qualifiedField String of the format Normalization.Scale
     * 
     * @param value String representation of a field value, e.g. "1.0" for "Normalization.Scale".
     * 
     * @throws com.bhpb.geographics.accessor.PropertyAccessorException if the String
     * qualifiedField does not map to a valid FIELD, or if value cannot be parsed
     * as a String of the proper type.  There is currently no method for determining
     * the type of a FIELD, and the GUI should be used for examples.
     */
    public void setProperty(String qualifiedField, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        try {
            switch (field) {
                //bug fix: qualifiedFields omit 'Culling' because it is not found in GUI
                case ClippingFactor:
                case DecimationSpacing:
                    culling.setProperty(field.toString(), value);
                    break;
                case AutoGainControl:
                    autoGainControl.setProperty(remainder, value);
                    break;
                case Normalization:
                    normalization.setProperty(remainder, value);
                    break;
                case Layer:
                case Scale:
                    basicProps.setProperty(remainder, value);
                    break;
                case RasterizingType:
                    rasterProps.setProperty(remainder, value);
                    break;
                case LayerName:
                    layerName = value;
                    break;
                case Interpolation:
                    interpolationSettings.setProperty(remainder, value);
                    break;
                default:
                    throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
            }
        } catch (Exception ex) {
            throw new PropertyAccessorException(ex);
        }
    }

    /**
     * Returns a copy of this SeismicXsecDisplayParameters, with all properties except
     * for x scale, y scale and AGC length/flag set to match the corresponding fields of ancestor,
     * if aLayerDisplayParameters is also a SeismicXsecDisplayParameters.  If ancestor
     * is of any other type, then this SeismicXsecDisplayParameters is returned instead*
     *
     * *if the ancestor is not a HorizonXsecLayerDisplayParameters, then the ColorMap _is_ inherited
     *
     * @param ancestor LayerDisplayParameters used for inheritance
     * @throws IllegalArgumentException if ancestor is not an intance of SeismicXsec, ModelXsec, HorizonXsec or MapDisplayParameters
     *
     * @return LayerDisplayParameters inherited from ancestor
     */
    public LayerDisplayParameters inheritFrom(LayerDisplayParameters ancestor) {
        if (ancestor == null) {
            return this;
        } else
        //inherit nothing from Horizon xsec display params
        if (ancestor instanceof HorizonXsecDisplayParameters) {
            return this;
        }
        //inherit only ColorMap from ModelXsec or Map display params
        else if (ancestor instanceof ModelXsecDisplayParameters) {
            ModelXsecDisplayParameters modelXsecDisplayParams = (ModelXsecDisplayParameters) ancestor;
            colorMapModel.setColorMap(modelXsecDisplayParams.getColorMap());
            return this;
        } else if (ancestor instanceof MapDisplayParameters) {
            MapDisplayParameters mapDisplayParams = (MapDisplayParameters) ancestor;
            colorMapModel.setColorMap(mapDisplayParams.getColorAttributes().getColorMap());
            return this;
        } else if (ancestor instanceof SeismicXsecDisplayParameters == false) {
            throw new IllegalArgumentException("Cannot inherit from unknown LayerDisplayParameters subclass: " +
                    ancestor.getClass().getName());
        }

        //and inherit all display params from SeismicXsec ancestor layer
        SeismicXsecDisplayParameters ldParamsCopy =
            SeismicXsecDisplayParametersFactory.createLayerDisplayParameters(this);

        BasicLayerProperties newBLP = ldParamsCopy.getBasicLayerProperties();
        Normalization newNorm = ldParamsCopy.getNormalizationProperties();
        RasterizationTypeModel newRasterType = ldParamsCopy.getRasterizationTypeModel();
        Culling newCulling = ldParamsCopy.getCulling();
        ColorMapModel newColorMapModel = ldParamsCopy.getColorMapModel();

        SeismicXsecDisplayParameters ancestorDisplayParams = (SeismicXsecDisplayParameters) ancestor;
        BasicLayerProperties ancestorBLP = ancestorDisplayParams.getBasicLayerProperties();
        Normalization ancestorNorm = ancestorDisplayParams.getNormalizationProperties();
        RasterizationTypeModel ancestorRasterType = ldParamsCopy.getRasterizationTypeModel();
        Culling ancestorCulling = ancestorDisplayParams.getCulling();
        ColorMapModel ancestorColorMapModel = ancestorDisplayParams.getColorMapModel();

        newBLP.setOpacity(ancestorBLP.getOpacity());

        newNorm.setNormalizationScale(ancestorNorm.getNormalizationScale());
        newNorm.setNormalizationType(ancestorNorm.getNormalizationType());
        newNorm.setNormalizationMinValue(ancestorNorm.getNormalizationMinValue());
        newNorm.setNormalizationMaxValue(ancestorNorm.getNormalizationMaxValue());

        newRasterType.setRasterizeInterpolatedDensity(ancestorRasterType.doRasterizeInterpolatedDensity());
        newRasterType.setRasterizeNegativeFill(ancestorRasterType.doRasterizeNegativeFill());
        newRasterType.setRasterizePositiveFill(ancestorRasterType.doRasterizePositiveFill());
        newRasterType.setRasterizeVariableDensity(ancestorRasterType.doRasterizeVariableDensity());
        newRasterType.setRasterizeWiggleTraces(ancestorRasterType.doRasterizeWiggleTraces());
        
        newCulling.setClippingFactor(ancestorCulling.getClippingFactor());
        newCulling.setDecimationSpacing(ancestorCulling.getDecimationSpacing());

        newColorMapModel.setColorMap(new ColorMap(ancestorColorMapModel.getColorMap()));

        return ldParamsCopy;
    }

    public void setColorMap(ColorMap colorMap) {
        colorMapModel.setColorMap(colorMap);
    }
}