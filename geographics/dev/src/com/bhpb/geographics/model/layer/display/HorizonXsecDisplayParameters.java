/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.display;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geographics.util.XStreamUtil;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.thoughtworks.xstream.XStream;
import java.awt.Color;
import java.io.StringReader;
import org.w3c.dom.Node;

public class HorizonXsecDisplayParameters extends LayerDisplayParameters {
    private static final long serialVersionUID = 3585509881523075250L;

    public enum LINE_STYLE {
      SOLID, EMPTY,
	  DASH, DOT,
	  DASH_DOT, DASH_DOT_DOT
    }

    public enum SYMBOL_STYLE {
        CROSS, CIRCLE, DOT, STAR, PLUS, SQUARE, DIAMOND, TRIANGLE
    }

    public enum FIELD {

        Layer, LineSettings, SymbolSettings, LayerName, DrawLine, ConnectNonContiguousPoints, DrawSymbol, SolidFill,
        LineColor, SymbolColor, SymbolWidth, SymbolHeight, LineWidth, LineStyle,
        SymbolStyle
    }

    public enum COLOR {
        ORANGE,
        PINK,
        RED,
        WHITE,
        YELLOW,
        BLACK,
        BLUE,
        CYAN,
        DARK_GRAY,
        GRAY,
        GREEN,
        LIGHT_GRAY,
        MAGENTA
    }
    
    private static final transient FieldProcessor<FIELD> processor = new FieldProcessor<FIELD>(FIELD.values(), HorizonXsecDisplayParameters.class);
    private Boolean drawLine = Boolean.TRUE;
    private Boolean connectNonContiguousPoints = Boolean.FALSE;
    private Boolean drawSymbol = Boolean.FALSE;
    private Boolean solidFill = Boolean.TRUE;
    private Color lineColor = Color.RED;
    private Color symbolColor = Color.BLACK;
    private Double symbolWidth = 12.0;
    private Double symbolHeight = 12.0;
    private Integer lineWidth = 1;
    private LINE_STYLE lineStyle = LINE_STYLE.SOLID;
    private SYMBOL_STYLE symbolStyle = SYMBOL_STYLE.STAR;

    public static HorizonXsecDisplayParameters restoreState(Node node) {
        XStream xStream = XStreamUtil.getInstance();
        String xml = ElementAttributeReader.nodeToXMLString(node);
        return (HorizonXsecDisplayParameters) xStream.fromXML(new StringReader(xml));
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HorizonXsecDisplayParameters other = (HorizonXsecDisplayParameters) obj;
        if (this.drawLine != other.drawLine && (this.drawLine == null || !this.drawLine.equals(other.drawLine))) {
            return false;
        }
        if (this.connectNonContiguousPoints != other.connectNonContiguousPoints && (this.connectNonContiguousPoints == null || !this.connectNonContiguousPoints.equals(other.connectNonContiguousPoints))) {
            return false;
        }
        if (this.drawSymbol != other.drawSymbol && (this.drawSymbol == null || !this.drawSymbol.equals(other.drawSymbol))) {
            return false;
        }
        if (this.solidFill != other.solidFill && (this.solidFill == null || !this.solidFill.equals(other.solidFill))) {
            return false;
        }
        if (this.lineColor != other.lineColor) {
            return false;
        }
        if (this.symbolColor != other.symbolColor) {
            return false;
        }
        if (this.symbolWidth != other.symbolWidth && (this.symbolWidth == null || !this.symbolWidth.equals(other.symbolWidth))) {
            return false;
        }
        if (this.symbolHeight != other.symbolHeight && (this.symbolHeight == null || !this.symbolHeight.equals(other.symbolHeight))) {
            return false;
        }
        if (this.lineWidth != other.lineWidth && (this.lineWidth == null || !this.lineWidth.equals(other.lineWidth))) {
            return false;
        }
        if (this.lineStyle != other.lineStyle) {
            return false;
        }
        if (this.symbolStyle != other.symbolStyle) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (this.drawLine != null ? this.drawLine.hashCode() : 0);
        hash = 97 * hash + (this.connectNonContiguousPoints != null ? this.connectNonContiguousPoints.hashCode() : 0);
        hash = 97 * hash + (this.drawSymbol != null ? this.drawSymbol.hashCode() : 0);
        hash = 97 * hash + (this.solidFill != null ? this.solidFill.hashCode() : 0);
        hash = 97 * hash + (this.lineColor != null ? this.lineColor.hashCode() : 0);
        hash = 97 * hash + (this.symbolColor != null ? this.symbolColor.hashCode() : 0);
        hash = 97 * hash + (this.symbolWidth != null ? this.symbolWidth.hashCode() : 0);
        hash = 97 * hash + (this.symbolHeight != null ? this.symbolHeight.hashCode() : 0);
        hash = 97 * hash + (this.lineWidth != null ? this.lineWidth.hashCode() : 0);
        hash = 97 * hash + (this.lineStyle != null ? this.lineStyle.hashCode() : 0);
        hash = 97 * hash + (this.symbolStyle != null ? this.symbolStyle.hashCode() : 0);
        return hash;
    }

    public String saveState() {
        StringBuffer content = new StringBuffer();

        XStream xStream = XStreamUtil.getInstance();
        content.append(xStream.toXML(this));
        content.append("\n");

        return content.toString();
    }

    public void setConnectNonContiguousPoints(Boolean connectNonContiguousPoints) {
        this.connectNonContiguousPoints = connectNonContiguousPoints;
    }

    public void setDrawLine(Boolean drawLine) {
        this.drawLine = drawLine;
    }

    public void setDrawSymbol(Boolean drawSymbol) {
        this.drawSymbol = drawSymbol;
    }

    public void setLineColor(Color color) {
        this.lineColor = color;
    }

    public void setLineColor(COLOR color) {
        this.lineColor = getColor(color);
    }

    public void setLineStyle(LINE_STYLE lineStyle) {
        this.lineStyle = lineStyle;
    }

    public void setLineWidth(Integer lineWidth) {
        this.lineWidth = lineWidth;
    }

    public void setSolidFill(Boolean solidFill) {
        this.solidFill = solidFill;
    }

    public void setSymbolColor(Color symbolColor) {
        this.symbolColor = symbolColor;
    }

    public void setSymbolColor(COLOR symbolColor) {
        this.symbolColor = getColor(symbolColor);
    }

    public void setSymbolHeight(Double symbolHeight) {
        this.symbolHeight = symbolHeight;
    }

    public void setSymbolStyle(SYMBOL_STYLE symbolStyle) {
        this.symbolStyle = symbolStyle;
    }

    public void setSymbolWidth(Double symbolWidth) {
        this.symbolWidth = symbolWidth;
    }

    /**
     * Gets the java.awt.Color associated with the GUI COLOR enum.
     * @throws IllegalArgumentException if no java.awt.Color exists
     * with a name exactly matching the COLOR parameter.
     * 
     * @param color one of the COLOR values used by the LineColorButton
     * 
     * @return the associated java.awt.Color
     */
    public static Color getColor(COLOR color) {
        switch (color) {
            case BLACK :
                return Color.BLACK;
            case BLUE :
                 return Color.BLUE;
            case CYAN :
                return Color.CYAN;
            case DARK_GRAY :
                return Color.DARK_GRAY;
            case GRAY :
                return Color.GRAY;
            case GREEN :
                return Color.GREEN;
            case LIGHT_GRAY :
                return Color.LIGHT_GRAY;
            case MAGENTA :
                return Color.MAGENTA;
            case ORANGE :
                return Color.ORANGE;
            case PINK :
                return Color.PINK;
            case RED :
                return Color.RED;
            case WHITE :
                return Color.WHITE;
            case YELLOW :
                return Color.YELLOW;
            default :
                throw new IllegalArgumentException("No java.awt.Color matches COLOR: " + color.toString());
        }
    }

    public Boolean getConnectNonContiguousPoints() {
        return connectNonContiguousPoints;
    }

    public Boolean getDrawLine() {
        return drawLine;
    }

    public Boolean getDrawSymbol() {
        return drawSymbol;
    }

    public Color getLineColor() {
        return lineColor;
    }

    public LINE_STYLE getLineStyle() {
        return lineStyle;
    }

    public Integer getLineWidth() {
        return lineWidth;
    }

    public FieldProcessor<FIELD> getProcessor() {
        return processor;
    }

    public Boolean getSolidFill() {
        return solidFill;
    }

    public Color getSymbolColor() {
        return symbolColor;
    }

    public Double getSymbolHeight() {
        return symbolHeight;
    }

    public SYMBOL_STYLE getSymbolStyle() {
        return symbolStyle;
    }

    public Double getSymbolWidth() {
        return symbolWidth;
    }

    public String getProperty(String qualifiedField) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if (!"".equals(remainder)) {
            throw new PropertyAccessorException("Field: " + field + " is valid was followed by unknown field name: " + remainder);
        }

        switch (field) {
            case DrawLine:
                return FieldProcessor.valueOf(drawLine);
            case ConnectNonContiguousPoints:
                return FieldProcessor.valueOf(connectNonContiguousPoints);
            case DrawSymbol:
                return FieldProcessor.valueOf(drawSymbol);
            case SolidFill:
                return FieldProcessor.valueOf(solidFill);
            case LineColor:
                return lineColor.toString();
            case SymbolColor:
                return symbolColor.toString();
            case SymbolWidth:
                return symbolWidth.toString();
            case SymbolHeight:
                return symbolHeight.toString();
            case LineWidth:
                return lineWidth.toString();
            case LineStyle:
                return lineStyle.toString();
            case SymbolStyle:
                return symbolStyle.toString();
            case LayerName:
                return layerName;
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public static String getXmlAlias() {
        return "horizonXsecDisplayParameters";
    }

    public void setProperty(String qualifiedField, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if (!"".equals(remainder)) {
            throw new PropertyAccessorException("Field: " + field + " is valid was followed by unknown field name: " + remainder);
        }

        switch (field) {
            case DrawLine:
                drawLine = FieldProcessor.booleanValue(value);
                break;
            case ConnectNonContiguousPoints:
                connectNonContiguousPoints = FieldProcessor.booleanValue(value);
                break;
            case DrawSymbol:
                drawSymbol = FieldProcessor.booleanValue(value);
                break;
            case SolidFill:
                solidFill = FieldProcessor.booleanValue(value);
                break;
            case LineColor:
                lineColor = getColor(COLOR.valueOf(value.toUpperCase()));
                break;
            case SymbolColor:
                symbolColor = getColor(COLOR.valueOf(value.toUpperCase()));
                break;
            case SymbolWidth:
                symbolWidth = Double.valueOf(value);
                break;
            case SymbolHeight:
                symbolHeight = Double.valueOf(value);
                break;
            case LineWidth:
                lineWidth = Integer.valueOf(value);
                break;
            case LineStyle:
                lineStyle = LINE_STYLE.valueOf(value);
                break;
            case SymbolStyle:
                symbolStyle = SYMBOL_STYLE.valueOf(value);
                break;
            case LayerName:
                layerName = value;
                break;
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public Color getBackgroundColor() {
        return ColorMap.white_no_alpha;
    }

    /**
     * If ancestor contains a HorizonXsecDisplayParameters, then it is used
     * to construct a new LayerDisplayParameters with inherited display properties.
     *
     * Otherwise, a refrence to this HorizonXsecDisplayParameters is returned.
     *
     * @param ancestor LayerDisplayParameters from which to inherit display properties
     *
     * @return LayerDisplayParameters which may have inherited HorizonXsecDisplayParameters
     */
    public LayerDisplayParameters inheritFrom(LayerDisplayParameters ancestor) {
        if (ancestor instanceof HorizonXsecDisplayParameters) {
            //and inherit all display params from SeismicXsec ancestor layer
            HorizonXsecDisplayParameters ldParamsCopy =
                HorizonXsecDisplayParametersFactory.createLayerDisplayParameters(this);
            HorizonXsecDisplayParameters ancestorDisplayParams = (HorizonXsecDisplayParameters) ancestor;
            
            ldParamsCopy.connectNonContiguousPoints = ancestorDisplayParams.connectNonContiguousPoints;
            ldParamsCopy.drawLine = ancestorDisplayParams.drawLine;
            ldParamsCopy.drawSymbol = ancestorDisplayParams.drawSymbol;
            ldParamsCopy.lineColor = ancestorDisplayParams.lineColor;
            ldParamsCopy.lineStyle = ancestorDisplayParams.lineStyle;
            ldParamsCopy.lineWidth = ancestorDisplayParams.lineWidth;
            ldParamsCopy.solidFill = ancestorDisplayParams.solidFill;
            ldParamsCopy.symbolColor = ancestorDisplayParams.symbolColor;
            ldParamsCopy.symbolHeight = ancestorDisplayParams.symbolHeight;
            ldParamsCopy.symbolWidth = ancestorDisplayParams.symbolWidth;
            ldParamsCopy.symbolStyle = ancestorDisplayParams.symbolStyle;
            
            return ldParamsCopy;
        }
        return this;
    }

    /**
     * @throws UnsupportedOperationException HorizonXsecDisplayParameters do not support configurable ColorMaps
     *
     * @param colorMap ColorMap requested to be set
     */
    public void setColorMap(ColorMap colorMap) {
        throw new UnsupportedOperationException("Setting colorMap is not supported by HorizonXsecDisplayParamters.");
    }
}