/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.shared;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geoio.util.ElementAttributeReader;
import java.io.Serializable;
import java.util.logging.Logger;
import org.w3c.dom.Node;

public class Normalization implements Serializable, PropertyAccessor {
    private static final long serialVersionUID = -3698298271113238985L;

    private static transient final Logger logger =
            Logger.getLogger(Normalization.class.toString());
    private static transient final FieldProcessor<FIELD> processor = 
            new FieldProcessor<FIELD>(FIELD.values(), Normalization.class);

    public enum NORMALIZATION_TYPE { MAXIMUM, TRACE_MAXIMUM, AVERAGE,
        TRACE_AVERAGE, RMS, TRACE_RMS, LIMITS }

    public enum FIELD { Type, Min_Value, Max_Value, Scale}
    
    private Double maxValue;
    private Double minValue;
    private Double scale;
    private NORMALIZATION_TYPE normalizationType;

    public Normalization() {
    }

    public Normalization(Node node) {
        try {
            setProperty(FIELD.Scale.toString(), ElementAttributeReader.getString(node, FIELD.Scale.toString()));
            setProperty(FIELD.Type.toString(), ElementAttributeReader.getString(node, FIELD.Type.toString()));
            setProperty(FIELD.Min_Value.toString(), ElementAttributeReader.getString(node, FIELD.Min_Value.toString()));
            setProperty(FIELD.Max_Value.toString(), ElementAttributeReader.getString(node, FIELD.Max_Value.toString()));
        } catch (PropertyAccessorException pae) {
            String errMsg = pae.getMessage();
            throw new RuntimeException(errMsg);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Normalization other = (Normalization) obj;
        if (this.maxValue != other.maxValue && (this.maxValue == null || !this.maxValue.equals(other.maxValue))) {
            return false;
        }
        if (this.minValue != other.minValue && (this.minValue == null || !this.minValue.equals(other.minValue))) {
            return false;
        }
        if (this.scale != other.scale && (this.scale == null || !this.scale.equals(other.scale))) {
            return false;
        }
        if (this.normalizationType != other.normalizationType) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + (this.maxValue != null ? this.maxValue.hashCode() : 0);
        hash = 71 * hash + (this.minValue != null ? this.minValue.hashCode() : 0);
        hash = 71 * hash + (this.scale != null ? this.scale.hashCode() : 0);
        hash = 71 * hash + (this.normalizationType != null ? this.normalizationType.hashCode() : 0);
        return hash;
    }

    
    public Double getNormalizationMinValue() {
        return minValue;
    }

    public Double getNormalizationMaxValue() {
        return maxValue;
    }

    public Double getNormalizationScale() {
        return scale;
    }

    public NORMALIZATION_TYPE getNormalizationType() {
        return normalizationType;
    }

    public String getProperty(String qualifiedField) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        switch (field) {
            case Min_Value:
                return minValue.toString();
            case Max_Value:
                return maxValue.toString();
            case Scale:
                return scale.toString();
            case Type:
                return normalizationType.toString();
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public static String getXmlAlias() {
        return "normalization";
    }

    /**
     * Serializes this Normalization object as XML, converting each FIELD to an XML attribute.
     * 
     * @return the Normalization state serialized as XML.
     */
    public String saveState() {
        StringBuffer sbuf = new StringBuffer();

        sbuf.append("<" + getXmlAlias() + " ");
        try {
            //append the FIELDs as XML attributes
            sbuf.append(FIELD.Scale + "=\"" + getProperty(FIELD.Scale.toString()) + "\" ");
            sbuf.append(FIELD.Type + "=\"" + getProperty(FIELD.Type.toString()) + "\" ");
            sbuf.append(FIELD.Min_Value + "=\"" + getProperty(FIELD.Min_Value.toString()) + "\" ");
            sbuf.append(FIELD.Max_Value + "=\"" + getProperty(FIELD.Max_Value.toString()) + "\" ");
        } catch (PropertyAccessorException pae) {
            logger.warning("Unexpected PropertyAccesorException while saving state: " + pae.getMessage());
            throw new RuntimeException(pae);
        }

        sbuf.append(" />\n");

        return sbuf.toString();
    }

    public void setNormalizationMinValue(double normalizationMinValue) {
        this.minValue = normalizationMinValue;
    }

    public void setNormalizationMaxValue(double normalizationMaxValue) {
        this.maxValue = normalizationMaxValue;
    }

    public void setNormalizationScale(double normalizationScale) {
        this.scale = normalizationScale;
    }

    public void setNormalizationType(NORMALIZATION_TYPE normalizationType) {
        this.normalizationType = normalizationType;
    }

    public void setProperty(String qualifiedField, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        switch (field) {
            case Min_Value:
                minValue = Double.valueOf(value);
                break;
            case Max_Value:
                maxValue = Double.valueOf(value);
                break;
            case Scale:
                scale = Double.valueOf(value);
                break;
            case Type:
                normalizationType = NORMALIZATION_TYPE.valueOf(value);
                break;
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }
}