/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.shared;

import com.bhpb.geographics.util.ColorMap;
import com.bhpb.qiworkbench.compAPI.XmlUtils;
import java.io.Serializable;
import java.util.logging.Logger;
import org.w3c.dom.Node;

/**
 * Wrapper class which mitigates the need to modify a set of DisplayParameters'
 * ColorMap directly.
 * 
 * It simply allows getting an setting a ColorMap.
 * 
 */
public class ColorMapModel implements Serializable {
    private static final transient Logger logger =
            Logger.getLogger(ColorMapModel.class.toString());
    private static final long serialVersionUID = 4995058216606498160L;

    private ColorMap colorMap;
    
    
    public ColorMapModel(ColorMap colorMap) {
        this.colorMap = colorMap;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ColorMapModel other = (ColorMapModel) obj;
        if (this.colorMap != other.colorMap && (this.colorMap == null || !this.colorMap.equals(other.colorMap))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + (this.colorMap != null ? this.colorMap.hashCode() : 0);
        return hash;
    }
    
    public ColorMap getColorMap() {
        return colorMap;
    }

    public static String getXmlAlias() {
        return "colorMapModel";
    }
    
    public static ColorMapModel restoreState(Node node) {
        Node cMapNode = null;
        
        cMapNode = XmlUtils.getChild(node, ColorMap.getXmlAlias());
        
        if (cMapNode == null) {
            String errMsg = "Unable to restore ColorMapModel - no child node named " + ColorMap.getXmlAlias() + " was found";
            logger.warning(errMsg);
            throw new RuntimeException(errMsg);
        }
        
        ColorMap cMap = ColorMap.restoreState(cMapNode);
        return new ColorMapModel(cMap);
    }

        
    public String saveState() {
        StringBuffer sbuf = new StringBuffer();
        
        sbuf.append("<" + getXmlAlias() + ">\n");
        sbuf.append(colorMap.genState());
        sbuf.append("\n");
        sbuf.append("</" + getXmlAlias() +">\n");

        return sbuf.toString();
    }


    public void setColorMap(ColorMap colorMap) {
        this.colorMap = colorMap;
    }
}