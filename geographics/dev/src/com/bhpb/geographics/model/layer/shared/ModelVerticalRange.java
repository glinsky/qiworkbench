/*
###########################################################################
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.shared;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geoio.util.ElementAttributeReader;
import java.io.Serializable;
import java.util.logging.Logger;
import org.w3c.dom.Node;

public class ModelVerticalRange implements Serializable, PropertyAccessor {
    enum FIELD {

        StructuralInterpolation, StartTime, EndTime
    };
    
    private final static transient Logger logger =
            Logger.getLogger(ModelVerticalRange.class.toString());
    
    private final static transient FieldProcessor<FIELD> processor = new FieldProcessor<FIELD>(FIELD.values(), ModelVerticalRange.class);
    private static final long serialVersionUID = -4744847443530283403L;

    
    private Boolean structuralInterpolationEnabled = Boolean.TRUE;
    private Double startTime = 0.0;
    private Double endTime = 0.0;
    
    public ModelVerticalRange() {
        
    }
    
    public ModelVerticalRange(Node node) {
            try {
            setProperty(FIELD.StartTime.toString(), ElementAttributeReader.getString(node, FIELD.StartTime.toString()));
            setProperty(FIELD.EndTime.toString(), ElementAttributeReader.getString(node, FIELD.EndTime.toString()));
            setProperty(FIELD.StructuralInterpolation.toString(), ElementAttributeReader.getString(node, FIELD.StructuralInterpolation.toString()));
        } catch (PropertyAccessorException pae) {
            String errMsg = pae.getMessage();
            throw new RuntimeException(errMsg);
        }    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ModelVerticalRange other = (ModelVerticalRange) obj;
        if (this.structuralInterpolationEnabled != other.structuralInterpolationEnabled && (this.structuralInterpolationEnabled == null || !this.structuralInterpolationEnabled.equals(other.structuralInterpolationEnabled))) {
            return false;
        }
        if (this.startTime != other.startTime && (this.startTime == null || !this.startTime.equals(other.startTime))) {
            return false;
        }
        if (this.endTime != other.endTime && (this.endTime == null || !this.endTime.equals(other.endTime))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.structuralInterpolationEnabled != null ? this.structuralInterpolationEnabled.hashCode() : 0);
        hash = 97 * hash + (this.startTime != null ? this.startTime.hashCode() : 0);
        hash = 97 * hash + (this.endTime != null ? this.endTime.hashCode() : 0);
        return hash;
    }
    
    public Boolean isStructuralInterpolationEnabled() {
        return structuralInterpolationEnabled;
    }

    public Double getEndTime() {
        return endTime;
    }

    public String getProperty(String qualifiedField) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        switch (field) {
            case StructuralInterpolation:
                return FieldProcessor.valueOf(structuralInterpolationEnabled);
            case StartTime:
                return startTime.toString();
            case EndTime:
                return endTime.toString();
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);

        }
    }

    public Double getStartTime() {
        return startTime;
    }
    
    public String getXmlAlias() {
        return "modelVerticalRange";
    }
    
    /**
     * Serializes this ModelVerticalRange object as XML, converting each FIELD to an XML attribute.
     *
     * @return the Culling state serialized as XML.
     */
    public String saveState() {
        StringBuffer sbuf = new StringBuffer();

        sbuf.append("<" + getXmlAlias() + " ");
        try {
            //append the FIELDs as XML attributes
            sbuf.append(FIELD.StructuralInterpolation + "=\""+ getProperty(FIELD.StructuralInterpolation.toString()) +"\" ");
            sbuf.append(FIELD.StartTime + "=\""+ getProperty(FIELD.StartTime.toString()) +"\" ");
            sbuf.append(FIELD.EndTime + "=\""+ getProperty(FIELD.EndTime.toString()) +"\" ");
        } catch (PropertyAccessorException pae) {
            logger.warning("Unexpected PropertyAccesorException while saving state: " + pae.getMessage());
            throw new RuntimeException(pae);
        }
        
        sbuf.append(" />\n");

        return sbuf.toString();
    }

    public void setEndTime(double endTime) {
        this.endTime = endTime;
    }

    public void setStartTime(double startTime) {
        this.startTime = startTime;
    }

    public void setProperty(String qualifiedField, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if (!"".equals(remainder)) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field  contains additional sub-fields");
        }

        switch (field) {
            case StructuralInterpolation:
                structuralInterpolationEnabled = FieldProcessor.booleanValue(value);
                break;
            case StartTime:
                startTime = Double.valueOf(value);
                break;
            case EndTime:
                endTime = Double.valueOf(value);
                break;
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public void setStructuralInterpolationEnabled(boolean enabled) {
        structuralInterpolationEnabled = enabled;
    }
}