/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.shared;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.bhpb.qiworkbench.compAPI.XmlUtils;
import java.io.Serializable;
import java.util.logging.Logger;
import org.w3c.dom.Node;

public class ColorAttributes implements Serializable, PropertyAccessor {

    private static final transient FieldProcessor<FIELD> processor = new FieldProcessor<FIELD>(FIELD.values(), ColorAttributes.class);
    private static final transient Logger logger =
            Logger.getLogger(ColorAttributes.class.toString());
    private static final long serialVersionUID = 9124108532099151061L;
    
    
    private ColorMapModel colorMapModel = new ColorMapModel(ColorMap.DEFAULT_COLORMAP);
    private Double min = 0.0;
    private Double max = 0.0;

    public ColorAttributes() {
    }

    public ColorAttributes(Node node) {
        try {
            setProperty(FIELD.Min.toString(), ElementAttributeReader.getString(node, FIELD.Min.toString()));
            setProperty(FIELD.Max.toString(), ElementAttributeReader.getString(node, FIELD.Max.toString()));
        } catch (PropertyAccessorException pae) {
            String errMsg = pae.getMessage();
            throw new RuntimeException(errMsg);
        }
        Node colorMapModelNode = XmlUtils.getChild(node, ColorMapModel.getXmlAlias());
        colorMapModel = ColorMapModel.restoreState(colorMapModelNode);
    }

    public enum FIELD {Min, Max }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ColorAttributes other = (ColorAttributes) obj;
        if (this.colorMapModel != other.colorMapModel && (this.colorMapModel == null || !this.colorMapModel.equals(other.colorMapModel))) {
            return false;
        }
        if (this.min != other.min && (this.min == null || !this.min.equals(other.min))) {
            return false;
        }
        if (this.max != other.max && (this.max == null || !this.max.equals(other.max))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (this.colorMapModel != null ? this.colorMapModel.hashCode() : 0);
        hash = 31 * hash + (this.min != null ? this.min.hashCode() : 0);
        hash = 31 * hash + (this.max != null ? this.max.hashCode() : 0);
        return hash;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }

    public Double getMin() {
        return min;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    public ColorMap getColorMap() {
        return colorMapModel.getColorMap();
    }

    public ColorMapModel getColorMapModel() {
        return colorMapModel;
    }

    public String getProperty(String qualifiedFieldName) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedFieldName);
        String remainder = processor.getFieldRemainder(qualifiedFieldName);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        switch (field) {
            case Min:
                return min.toString();
            case Max:
                return max.toString();
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public static String getXmlAlias() {
        return "colorAttributes";
    }

    /**
     * Serializes this BasicLayerProperties object as XML, converting each FIELD to an XML attribute.
     * 
     * @return the BasicLayerProperties state serialized as XML.
     */
    public String saveState() {
        StringBuffer sbuf = new StringBuffer();

        sbuf.append("<" + getXmlAlias() + " ");
        try {
            //append the FIELDs as XML attributes
            sbuf.append(FIELD.Min + "=\"" + getProperty(FIELD.Min.toString()) + "\" ");
            sbuf.append(FIELD.Max + "=\"" + getProperty(FIELD.Max.toString()) + "\" ");
        } catch (PropertyAccessorException pae) {
            logger.warning("Unexpected PropertyAccesorException while saving state: " + pae.getMessage());
            throw new RuntimeException(pae);
        }
        sbuf.append(">");
        sbuf.append(colorMapModel.saveState());
        sbuf.append("</" + getXmlAlias() + ">");

        return sbuf.toString();
    }

    public void setColorMap(ColorMap colorMap) {
        colorMapModel.setColorMap(colorMap);
    }

    public void setProperty(String qualifiedFieldname, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedFieldname);
        String remainder = processor.getFieldRemainder(qualifiedFieldname);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        try {
            switch (field) {
                case Min:
                    min = Double.valueOf(value);
                    break;
                case Max:
                    max = Double.valueOf(value);
                    break;
                default:
                    throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
            }
        } catch (Exception ex) {
            throw new PropertyAccessorException(ex);
        }
    }
}