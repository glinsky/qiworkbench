/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.sync;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geographics.model.SimpleInheritor;
import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParameters;
import com.bhpb.geographics.util.XStreamUtil;
import com.thoughtworks.xstream.XStream;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public abstract class LayerSyncProperties implements PropertyAccessor,
    SimpleInheritor<LayerSyncProperties>{

    public enum SYNC_FLAG {
        BROADCAST, VISIBILITY, OPACITY, NORM_SCALE, INTERPOLATION,
        PLOT_TYPE, COLORMAP, POLARITY, TIME_DEPTH_RANGE, NORM_TYPE, AGC, WIGGLE_DECIMATION,
        COLOR_INTERP, EP_AXIS_DIRECTION, CDP_AXIS_DIRECTION, TRANSPOSE, EVENT_ATTRIBUTE,
        ARB_TRAVERSE    // arb trav is a layer sync flag for purposes of brodcast/receive
                        // but is neither a mutable property, visible property, gettable/settable field
    }

    public enum FIELD {
        visibility, opacity, normalizationScale, interpolation,
        plotType, colorMap, polarity, timeDepthRange, normalizationType, autoGainControl,
        wiggleDecimation, colorInterpolation, broadcast
    }
    private transient FieldProcessor<FIELD> processor = new FieldProcessor<FIELD>(FIELD.values(), SeismicXsecDisplayParameters.class);
    private Map<SYNC_FLAG, Boolean> enabledMap = new EnumMap<SYNC_FLAG, Boolean>(SYNC_FLAG.class);
    private transient List<SYNC_FLAG> allProperties = new ArrayList<SYNC_FLAG>();
    private transient Map<SYNC_FLAG, Boolean> mutableMap = new EnumMap<SYNC_FLAG, Boolean>(SYNC_FLAG.class);
    private transient Map<SYNC_FLAG, Boolean> visibleMap = new EnumMap<SYNC_FLAG, Boolean>(SYNC_FLAG.class);

    /**
     * Constructs a LayerSyncProperties using the specified enabled, mutable
     * and visible properties.  The broadcast flag will always be set visible, mutable and
     * enabled by this constructor.
     * 
     * @param allProperties
     * @param enabledProperties
     * @param mutableProperties
     * @param visibleProperties
     */
    protected LayerSyncProperties(
            List<SYNC_FLAG> allProperties,
            List<SYNC_FLAG> enabledProperties,
            List<SYNC_FLAG> mutableProperties,
            List<SYNC_FLAG> visibleProperties) {
        this.allProperties = allProperties;

        for (SYNC_FLAG syncFlag : SYNC_FLAG.values()) {
            enabledMap.put(syncFlag, Boolean.FALSE);
            mutableMap.put(syncFlag, Boolean.FALSE);
            visibleMap.put(syncFlag, Boolean.FALSE);
        }

        for (SYNC_FLAG syncFlag : enabledProperties) {
            enabledMap.put(syncFlag, Boolean.TRUE);
        }

        for (SYNC_FLAG syncFlag : mutableProperties) {
            mutableMap.put(syncFlag, Boolean.TRUE);
        }

        for (SYNC_FLAG syncFlag : visibleProperties) {
            visibleMap.put(syncFlag, Boolean.TRUE);
        }

        //all layers start with broadcast enabled
        mutableMap.put(SYNC_FLAG.BROADCAST, Boolean.TRUE);
        visibleMap.put(SYNC_FLAG.BROADCAST, Boolean.TRUE);
        enabledMap.put(SYNC_FLAG.BROADCAST, Boolean.TRUE);
    }

    /**
     * Constructs a copy of the specified LayerSyncProperties.
     *
     * @param that LayerSyncPropertise to be copied.
     */
    protected LayerSyncProperties(LayerSyncProperties that) {
        this.allProperties = new ArrayList<SYNC_FLAG>(that.allProperties);
        this.enabledMap = new EnumMap<SYNC_FLAG, Boolean>(that.enabledMap);
        this.mutableMap = new EnumMap<SYNC_FLAG, Boolean>(that.mutableMap);
        this.visibleMap = new EnumMap<SYNC_FLAG, Boolean>(that.visibleMap);
    }

    public List<SYNC_FLAG> getProperties() {
        return allProperties;
    }

    public String getProperty(String qualifiedField) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        switch (field) {
            case broadcast:
                return enabledMap.get(SYNC_FLAG.BROADCAST).toString();
            case visibility:
                return enabledMap.get(SYNC_FLAG.VISIBILITY).toString();
            case opacity:
                return enabledMap.get(SYNC_FLAG.OPACITY).toString();
            case normalizationScale:
                return enabledMap.get(SYNC_FLAG.NORM_SCALE).toString();
            case interpolation:
                return enabledMap.get(SYNC_FLAG.INTERPOLATION).toString();
            case plotType:
                return enabledMap.get(SYNC_FLAG.PLOT_TYPE).toString();
            case colorMap:
                return enabledMap.get(SYNC_FLAG.COLORMAP).toString();
            case polarity:
                return enabledMap.get(SYNC_FLAG.POLARITY).toString();
            case timeDepthRange:
                return enabledMap.get(SYNC_FLAG.TIME_DEPTH_RANGE).toString();
            case normalizationType:
                return enabledMap.get(SYNC_FLAG.NORM_TYPE).toString();
            case autoGainControl:
                return enabledMap.get(SYNC_FLAG.AGC).toString();
            case wiggleDecimation:
                return enabledMap.get(SYNC_FLAG.WIGGLE_DECIMATION).toString();
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public static String getXmlAlias() {
        return "layerSyncProperties";
    }

    public boolean isEnabled(SYNC_FLAG flag) {
        return enabledMap.get(flag);
    }

    public boolean isVisible(SYNC_FLAG flag) {
        return visibleMap.get(flag);
    }

    public boolean isMutable(SYNC_FLAG flag) {
        return mutableMap.get(flag);
    }

    public String saveState() {
        StringBuffer content = new StringBuffer();

        XStream xStream = XStreamUtil.getInstance();
        content.append(xStream.toXML(this));
        content.append("\n");

        return content.toString();
    }

    public void setEnabled(SYNC_FLAG flag, boolean value) {
        if (mutableMap.get(flag)) {
            enabledMap.put(flag, value);
        } else {
            throw new UnsupportedOperationException("Flag is not mutable: " + flag);
        }
    }

    public void setProperty(String qualifiedField, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        if (value == null || !(value.toUpperCase().equals("FALSE") || value.toUpperCase().equals("TRUE"))) {
            throw new PropertyAccessorException("Illegal value: '" + value);
        }

        switch (field) {
            case broadcast:
                setEnabled(SYNC_FLAG.BROADCAST, Boolean.valueOf(value));
                break;
            case visibility:
                setEnabled(SYNC_FLAG.VISIBILITY, Boolean.valueOf(value));
                break;
            case opacity:
                setEnabled(SYNC_FLAG.OPACITY, Boolean.valueOf(value));
                break;
            case normalizationScale:
                setEnabled(SYNC_FLAG.NORM_SCALE, Boolean.valueOf(value));
                break;
            case interpolation:
                setEnabled(SYNC_FLAG.INTERPOLATION, Boolean.valueOf(value));
                break;
            case plotType:
                setEnabled(SYNC_FLAG.PLOT_TYPE, Boolean.valueOf(value));
                break;
            case colorMap:
                setEnabled(SYNC_FLAG.COLORMAP, Boolean.valueOf(value));
                break;
            case polarity:
                setEnabled(SYNC_FLAG.POLARITY, Boolean.valueOf(value));
                break;
            case timeDepthRange:
                setEnabled(SYNC_FLAG.TIME_DEPTH_RANGE, Boolean.valueOf(value));
                break;
            case normalizationType:
                setEnabled(SYNC_FLAG.NORM_TYPE, Boolean.valueOf(value));
                break;
            case autoGainControl:
                setEnabled(SYNC_FLAG.AGC, Boolean.valueOf(value));
                break;
            case wiggleDecimation:
                setEnabled(SYNC_FLAG.WIGGLE_DECIMATION, Boolean.valueOf(value));
                break;
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    /**
     * Sets the list of all available properties.  Used by subclass deserialization code.
     * 
     * @param allProperties 
     */
    protected void setAllProperties(List<SYNC_FLAG> allProperties) {
        this.allProperties = allProperties;
    }

    /**
     * Within these LayerSyncProperties' mutable SYNC_FLAG map, sets the value
     * of every SYNC_FLAG in the List mutableProperties to TRUE, and sets the value
     * of all other SYNC_FLAG values to FALSE.  Used by subclass deserialization code.
     * 
     * @param mutableProperties the List of SYNC_FLAG values which will be made mutable
     */
    protected void setMutableProperties(List<SYNC_FLAG> mutableProperties) {
        if (mutableMap == null) {
            this.mutableMap = new EnumMap<SYNC_FLAG, Boolean>(SYNC_FLAG.class);
        }
        for (SYNC_FLAG flag : SYNC_FLAG.values()) {
            this.mutableMap.put(flag, mutableProperties.contains(flag));
        }
    }

    /**
     * Within these LayerSyncProperties' visible SYNC_FLAG map, sets the value
     * of every SYNC_FLAG in the List visibleProperties to TRUE, and sets the value
     * of all other SYNC_FLAG values to FALSE.  Used by subclass deserialization code.
     * 
     * @param visibleProperties the List of SYNC_FLAG values which will be made visible
     */
    protected void setVisibleProperties(List<SYNC_FLAG> visibleProperties) {
        if (visibleMap == null) {
            this.visibleMap = new EnumMap<SYNC_FLAG, Boolean>(SYNC_FLAG.class);
        }
        for (SYNC_FLAG flag : SYNC_FLAG.values()) {
            this.visibleMap.put(flag, visibleProperties.contains(flag));
        }
    }

    static public String toString(SYNC_FLAG flag) {
        switch (flag) {
            case BROADCAST:
                return "Broadcast";
            case VISIBILITY:
                return "Visibility";
            case OPACITY:
                return "Opacity";
            case NORM_SCALE:
                return "Normalization Scale";
            case INTERPOLATION:
                return "Interpolation";
            case PLOT_TYPE:
                return "Plot Type";
            case COLORMAP:
                return "Color Map";
            case POLARITY:
                return "Polarity";
            case TIME_DEPTH_RANGE:
                return "Time/Depth Range";
            case NORM_TYPE:
                return "Normalization Type";
            case AGC:
                return "AGC";
            case WIGGLE_DECIMATION:
                return "Wiggle Decimation";
            case COLOR_INTERP:
                return "Color Interpolation";
            case EVENT_ATTRIBUTE:
                return "Event Attribute";
            case EP_AXIS_DIRECTION:
                return "ep Axis Direction";
            case CDP_AXIS_DIRECTION:
                return "cdp Axis Direction";
            case TRANSPOSE:
                return "transpose";
            default:
                return flag.toString();
        }
    }

    static public SYNC_FLAG valueOf(String string) {
        for (SYNC_FLAG flag : SYNC_FLAG.values()) {
            if (toString(flag).equals(string)) {
                return flag;
            }
        }
        throw new IllegalArgumentException("Not valid String value of any SYNC_FLAG: " + string);
    }
}