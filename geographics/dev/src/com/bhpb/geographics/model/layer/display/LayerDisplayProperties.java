/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.display;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.layer.sync.HorizonXsecSyncProperties;
import com.bhpb.geographics.model.layer.sync.MapSyncProperties;
import com.bhpb.geographics.model.layer.sync.ModelXsecSyncProperties;
import com.bhpb.geographics.model.layer.sync.SeismicXsecSyncProperties;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoFileType;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;

public class LayerDisplayProperties implements PropertyAccessor {    
    enum FIELD { LayerDisplayParameters, LocalSubsetProperties, LayerSyncProperties};
    
    /**
     * Null Object pattern: allows use of LayerDisplayProperties editor in situations
     * where a valid set of LayerDisplayProperties cannot be constructed, principally,
     * on the initial LayerProperties GUI prior to loading the GeoIO DataObjects.
     * All its getters return null, setters throw UnsupportedOperationException and
     * relevant GUI panels are disabled.
     */
    public static final LayerDisplayProperties NULL
            = new LayerDisplayProperties(null,null,null);

    public static final String NULL_TOOLTIP = "Editing of layer display properties is disabled until layer is loaded";

    private LocalSubsetProperties subsetProps;
    private LayerDisplayParameters displayParams;
    private LayerSyncProperties syncProps;
    private FieldProcessor<FIELD> processor = new FieldProcessor<FIELD>(FIELD.values(),LayerDisplayProperties.class);
    
    public LayerDisplayProperties(LocalSubsetProperties subsetProps, 
            LayerDisplayParameters displayParams, LayerSyncProperties syncProps) {
        this.subsetProps = subsetProps;
        this.displayParams = displayParams;
        this.syncProps = syncProps;
    }

    public static LayerDisplayProperties createDefaultLayerProperties(DatasetProperties dsProps) {
        return createDefaultLayerProperties(dsProps, null);
    }

    /**
     * Creates the default LayerProperties for the given DatasetProperties, inheriting
     * certain propertis from the active Layer when appropriate.  If activeLayer is null, then
     * all LayerDisplayProperties returned will be at default values regardless of the currently
     * active Layer.
     *
     * @param dsProps
     * @param activeLayer
     *
     * @return default LayerDisplayProperties
     */
    public static LayerDisplayProperties createDefaultLayerProperties(DatasetProperties dsProps,
            Layer activeLayer) {
        GeoDataOrder order = dsProps.getMetadata().getGeoDataOrder();
        GeoFileType type = dsProps.getMetadata().getGeoFileType();

        LayerDisplayParameters activeLayerDisplayParams = null;
        LayerSyncProperties activeLayerSyncProps = null;
        
        if (activeLayer != null) {
            activeLayerDisplayParams = activeLayer.getDisplayParameters();
            activeLayerSyncProps = activeLayer.getSyncProperties();
        }

        return new LayerDisplayProperties(
                LocalSubsetPropertiesFactory.createLocalSubsetProperties(dsProps),
                LayerDisplayParametersFactory.createLayerDisplayParameters(dsProps).inheritFrom(activeLayerDisplayParams),
                getLayerSyncProperties(dsProps, order, type).inheritFrom(activeLayerSyncProps));
        
    }

    private static LayerSyncProperties getLayerSyncProperties(DatasetProperties dsProps, GeoDataOrder order, GeoFileType type) {
        switch (order) {
            case CROSS_SECTION_ORDER:
                if (dsProps.isSeismic()) {
                    return new SeismicXsecSyncProperties();
                } else if (dsProps.isModel()) {
                    return new ModelXsecSyncProperties();
                } else if (dsProps.isHorizon()) {
                    return new HorizonXsecSyncProperties();
                } else {
                    throw new UnsupportedOperationException("Cannot create (Xsec) LayerDisplayParameters for GeoFileType: " + type + " because isModel(), isSeismic() and isHorizon() are all false.");
                }
            case MAP_VIEW_ORDER:
                if (dsProps.isSeismic() || dsProps.isModel() || dsProps.isHorizon()) {
                    return new MapSyncProperties();
                } else {
                    throw new UnsupportedOperationException("Cannot create (Map) LayerDisplayParameters for GeoFileType: " + type + " because isModel(), isSeismic() and isHorizon() are all false.");
                }
            default:
                throw new UnsupportedOperationException("Cannot create LayerDisplayParameters for GeoDataOrder: " + order);
        }
    }
    public LocalSubsetProperties getLocalSubsetProperties() {
        return subsetProps;
    }
    
    public LayerDisplayParameters getLayerDisplayParameters() {
        return displayParams;
    }
    
    public LayerSyncProperties getLayerSyncProperties() {
        return syncProps;
    }
    
    public String getProperty(String qualifiedField) throws PropertyAccessorException {
        if (this == NULL) {
            return null;
        }
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);
        
        if ("".equals(remainder)) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field namedoes not contain any additional sub-fields");
        }
        
        switch (field) {
            case LayerDisplayParameters :
                return displayParams.getProperty(remainder);
            case LayerSyncProperties :
                return syncProps.getProperty(remainder);
            case LocalSubsetProperties : 
                return subsetProps.getProperty(remainder);
            default :
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }
    
    public void setProperty(String qualifiedField, String value) throws PropertyAccessorException {
        if (this == NULL) {
            throw new UnsupportedOperationException("NULL LayerDisplayProperties");
        }
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);
        
        if ("".equals(remainder)) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field namedoes not contain any additional sub-fields");
        }
        
        switch (field) {
            case LayerDisplayParameters :
                displayParams.setProperty(remainder, value);
                break;
            case LayerSyncProperties :
                syncProps.setProperty(remainder, value);
                break;
            case LocalSubsetProperties : 
                subsetProps.setProperty(remainder, value);
                break;
            default :
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }
}