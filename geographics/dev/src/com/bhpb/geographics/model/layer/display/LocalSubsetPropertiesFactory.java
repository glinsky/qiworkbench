/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */

package com.bhpb.geographics.model.layer.display;

import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;

public class LocalSubsetPropertiesFactory {
    private static final String pkField = "TRACE_ID";
    private static final int pkStepValue=1;
    private static final boolean applyGaps=true;
    private static final int gapSize=5;
    
    private static final String skValue="NONE";
    private static final double skStartValue = Double.NEGATIVE_INFINITY;
    private static final double skEndValue = Double.POSITIVE_INFINITY;
    private static final double skStepValue = Double.NaN;

    public static LocalSubsetProperties createLocalSubsetProperties(LocalSubsetProperties that) {
        return new LocalSubsetProperties(that);
    }

    /**
     * Create an instance of LocalSubsetProperties with values based on the
     * GeoFileDataSummary.
     * 
     * @param dsProps DatasetProperties from which startValue, EndValue, etc. can be derived
     */
    public static LocalSubsetProperties createLocalSubsetProperties(DatasetProperties dsProps) {
        GeoFileDataSummary summary = dsProps.getSummary();

        LocalSubsetProperties lsProps = new LocalSubsetProperties();
        int pkEndValue = summary.getNumberOfTraces();
        int pkStartValue = Math.min(1, pkEndValue);
        lsProps.setPrimaryKey(pkField, pkStartValue, pkEndValue, pkStepValue, applyGaps, gapSize);
        lsProps.setSecondaryKey(skValue, skStartValue, skEndValue, skStepValue);

        lsProps.setStartTime(dsProps.getVerticalCoordMin());
        lsProps.setEndTime(dsProps.getVerticalCoordMax());

        return lsProps;
    }
    
    /**
     * Create an instance of LocalSubsetProperties with values suitable for use
     * in mock screens or unit tests.
     * <p/>
     * This method is deprecated - it should be replaced by one of the following:
     * 
     * <ol>
     * <li>Another constructor which has parameters for all fields</li>
     * <li>pass a mock GeoFileDataSummary to createLocalSubsetProperties</li>
     * <li>Extract an interface from LocalSubsetProperties and create actual and mock implementations</li>
     * </ol>
     * 
     * @deprecated 
     * @param nTraces the number of traces to use for the primary key EndValue
     * 
     * @return LocalSubsetProperties with all fields other than pkEndValue set to default values
     */
    public static LocalSubsetProperties createLocalSubsetPropertiesTestData(int nTraces) {
        LocalSubsetProperties lsProps = new LocalSubsetProperties();
        int pkEndValue = nTraces;
        int pkStartValue = Math.min(1, pkEndValue);
        lsProps.setPrimaryKey(pkField, pkStartValue, pkEndValue, pkStepValue, applyGaps, gapSize);
        lsProps.setSecondaryKey(skValue, skStartValue, skEndValue, skStepValue);
        return lsProps;
    }
}