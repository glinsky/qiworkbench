/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.display;

import com.bhpb.geographics.model.layer.shared.BasicLayerProperties;
import com.bhpb.geographics.model.layer.shared.ModelVerticalRange;
import com.bhpb.geographics.model.layer.shared.Normalization;
import com.bhpb.geographics.model.layer.shared.Normalization.NORMALIZATION_TYPE;
import com.bhpb.geographics.model.layer.shared.RasterizationTypeModel;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;

/**
 * Constructs a set of SeismicXsecDisplayParameters.  This class will serve
 * as a template for creation of Factory methods for the additional 5 types of DisplayParameters:
 * SeismicMap, ModelXsec, ModelMap, HorizonXsec & HorizonMap.
 * 
 */
public class ModelXsecDisplayParametersFactory {

    private static final String polarity = "Normal";
    private static final double horizontalScale = 1.0;
    private static final double verticalScale = 1.0;
    private static final double opacity = 1.0;
    private static final double normalizationScale = 1.0;
    private static final NORMALIZATION_TYPE normalizationType = NORMALIZATION_TYPE.LIMITS;
    private static final double normalizationMinValue = Double.NaN;
    private static final double normalizationMaxValue = Double.NaN;
    private static final int clippingFactor = 4;
    private static final int decimationSpacing = 5;

    public static ModelXsecDisplayParameters createLayerDisplayParameters(DatasetProperties dsProperties) {
        return createLayerDisplayParameters(dsProperties, new ColorMap(ColorMap.generateDefaultColorMap()));
    }

    public static ModelXsecDisplayParameters createLayerDisplayParameters(DatasetProperties dsProperties, ColorMap colorMap) {
        ModelXsecDisplayParameters ldParams = new ModelXsecDisplayParameters();

        Normalization normProps = ldParams.getNormalizationProperties();

        ldParams.setLayerName(dsProperties.getMetadata().getGeoFileName());
        ldParams.getBasicLayerProperties().setPolarity(polarity);
        ldParams.getBasicLayerProperties().setHorizontalScale(horizontalScale);
        ldParams.getBasicLayerProperties().setVerticalScale(verticalScale);
        ldParams.getBasicLayerProperties().setOpacity(opacity);

        normProps.setNormalizationScale(normalizationScale);
        normProps.setNormalizationType(normalizationType);

        ModelVerticalRange vertRange = ldParams.getModelVerticalRange();
        vertRange.setStartTime(dsProperties.getSummary().getSampleStart());
        vertRange.setEndTime(dsProperties.getSummary().getSampleRate());

        setRasterizationDefaults(dsProperties.getMetadata(), ldParams);

        ldParams.getCulling().setClippingFactor(clippingFactor);
        ldParams.getCulling().setDecimationSpacing(decimationSpacing);
        ldParams.setColorMap(colorMap);

        GeoFileDataSummary summary = dsProperties.getSummary();
        if (summary != null) {
            normProps.setNormalizationMinValue(summary.getMinAmplitude());
            normProps.setNormalizationMaxValue(summary.getMaxAmplitude());
        } else {
            normProps.setNormalizationMinValue(normalizationMinValue);
            normProps.setNormalizationMaxValue(normalizationMaxValue);
        }

        return ldParams;
    }

    public static ModelXsecDisplayParameters createLayerDisplayParameters(ModelXsecDisplayParameters aMXsecDisplayParameters) {
        ModelXsecDisplayParameters ldParams = new ModelXsecDisplayParameters();

        ldParams.setLayerName(aMXsecDisplayParameters.getLayerName());

        BasicLayerProperties aBasicLayerProperties = aMXsecDisplayParameters.getBasicLayerProperties();
        ldParams.getBasicLayerProperties().setPolarity(aBasicLayerProperties.getPolarity());
        ldParams.getBasicLayerProperties().setHorizontalScale(aBasicLayerProperties.getHorizontalScale());
        ldParams.getBasicLayerProperties().setVerticalScale(aBasicLayerProperties.getVerticalScale());
        ldParams.getBasicLayerProperties().setOpacity(aBasicLayerProperties.getOpacity());

        Normalization normProps = ldParams.getNormalizationProperties();
        Normalization aNormProps = aMXsecDisplayParameters.getNormalizationProperties();
        normProps.setNormalizationScale(aNormProps.getNormalizationScale());
        normProps.setNormalizationType(aNormProps.getNormalizationType());
        normProps.setNormalizationMinValue(aNormProps.getNormalizationMinValue());
        normProps.setNormalizationMaxValue(aNormProps.getNormalizationMaxValue());

        RasterizationTypeModel rasterization = ldParams.getRasterizationTypeModel();
        RasterizationTypeModel aRasterization = aMXsecDisplayParameters.getRasterizationTypeModel();

        rasterization.setRasterizeInterpolatedDensity(aRasterization.doRasterizeInterpolatedDensity());
        rasterization.setRasterizeNegativeFill(aRasterization.doRasterizeNegativeFill());
        rasterization.setRasterizePositiveFill(aRasterization.doRasterizePositiveFill());
        rasterization.setRasterizeVariableDensity(aRasterization.doRasterizeVariableDensity());
        rasterization.setRasterizeWiggleTraces(aRasterization.doRasterizeWiggleTraces());

        ldParams.getCulling().setClippingFactor(aMXsecDisplayParameters.getCulling().getClippingFactor());
        ldParams.getCulling().setDecimationSpacing(aMXsecDisplayParameters.getCulling().getDecimationSpacing());
        ldParams.getColorMapModel().setColorMap(aMXsecDisplayParameters.getColorMapModel().getColorMap());

        ldParams.getModelVerticalRange().setStartTime(aMXsecDisplayParameters.getModelVerticalRange().getStartTime());
        ldParams.getModelVerticalRange().setEndTime(aMXsecDisplayParameters.getModelVerticalRange().getEndTime());
        ldParams.getModelVerticalRange().setStructuralInterpolationEnabled(aMXsecDisplayParameters.getModelVerticalRange().isStructuralInterpolationEnabled());

        ldParams.getInterpolationSettings().setType(aMXsecDisplayParameters.getInterpolationSettings().getType());

        return ldParams;
    }

    private static void setRasterizationDefaults(GeoFileMetadata metadata, ModelXsecDisplayParameters ldParams) {
        if (metadata.isModelFile()) {
            ldParams.getRasterizationTypeModel().setRasterizeWiggleTraces(false);
            ldParams.getRasterizationTypeModel().setRasterizePositiveFill(false);
            ldParams.getRasterizationTypeModel().setRasterizeNegativeFill(false);
            ldParams.getRasterizationTypeModel().setRasterizeVariableDensity(true);
            ldParams.getRasterizationTypeModel().setRasterizeInterpolatedDensity(false);
        } else {
            ldParams.getRasterizationTypeModel().setRasterizeWiggleTraces(true);
            ldParams.getRasterizationTypeModel().setRasterizePositiveFill(true);
            ldParams.getRasterizationTypeModel().setRasterizeNegativeFill(false);
            ldParams.getRasterizationTypeModel().setRasterizeVariableDensity(false);
            ldParams.getRasterizationTypeModel().setRasterizeInterpolatedDensity(false);
        }
    }
}