/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.shared;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geoio.util.ElementAttributeReader;
import java.io.Serializable;
import java.util.logging.Logger;
import org.w3c.dom.Node;

public class Culling implements Serializable, PropertyAccessor {

    private static transient final Logger logger =
            Logger.getLogger(Culling.class.toString());
    private static transient final FieldProcessor<FIELD> processor = new FieldProcessor<FIELD>(FIELD.values(), Culling.class);
    private static final long serialVersionUID = -2719314434179989828L;

    public enum FIELD {

        ClippingFactor, DecimationSpacing
    }
    private Integer clippingFactor = 1;
    private Integer decimationSpacing = 5;

    public Culling() {
    }

    public Culling(Node node) {
        try {
            setProperty(FIELD.ClippingFactor.toString(), ElementAttributeReader.getString(node, FIELD.ClippingFactor.toString()));
            setProperty(FIELD.DecimationSpacing.toString(), ElementAttributeReader.getString(node, FIELD.DecimationSpacing.toString()));
        } catch (PropertyAccessorException pae) {
            String errMsg = pae.getMessage();
            throw new RuntimeException(errMsg);
        }
    }

    public Integer getClippingFactor() {
        return clippingFactor;
    }

    public Integer getDecimationSpacing() {
        return decimationSpacing;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Culling other = (Culling) obj;
        if (this.clippingFactor != other.clippingFactor && (this.clippingFactor == null || !this.clippingFactor.equals(other.clippingFactor))) {
            return false;
        }
        if (this.decimationSpacing != other.decimationSpacing && (this.decimationSpacing == null || !this.decimationSpacing.equals(other.decimationSpacing))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + (this.clippingFactor != null ? this.clippingFactor.hashCode() : 0);
        hash = 19 * hash + (this.decimationSpacing != null ? this.decimationSpacing.hashCode() : 0);
        return hash;
    }

    public String getProperty(String qualifiedField) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        switch (field) {
            case ClippingFactor:
                return clippingFactor.toString();
            case DecimationSpacing:
                return decimationSpacing.toString();
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public static String getXmlAlias() {
        return "culling";
    }

    /**
     * Serializes this Culling object as XML, converting each FIELD to an XML attribute.
     *
     * @return the Culling state serialized as XML.
     */
    public String saveState() {
        StringBuffer sbuf = new StringBuffer();

        sbuf.append("<" + getXmlAlias() + " ");
        try {
            //append the FIELDs as XML attributes
            sbuf.append(FIELD.ClippingFactor + "=\"" + getProperty(FIELD.ClippingFactor.toString()) + "\" ");
            sbuf.append(FIELD.DecimationSpacing + "=\"" + getProperty(FIELD.DecimationSpacing.toString()) + "\" ");
        } catch (PropertyAccessorException pae) {
            logger.warning("Unexpected PropertyAccesorException while saving state: " + pae.getMessage());
            throw new RuntimeException(pae);
        }

        sbuf.append(" />\n");

        return sbuf.toString();
    }

    public void setClippingFactor(int value) {
        clippingFactor = value;
    }

    public void setDecimationSpacing(int value) {
        decimationSpacing = value;
    }

    public void setProperty(String qualifiedField, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        switch (field) {
            case ClippingFactor:
                clippingFactor = Integer.valueOf(value);
                break;
            case DecimationSpacing:
                decimationSpacing = Integer.valueOf(value);
                break;
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }
}