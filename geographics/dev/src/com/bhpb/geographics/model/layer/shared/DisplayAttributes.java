/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.shared;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geoio.util.ElementAttributeReader;
import java.io.Serializable;
import java.util.logging.Logger;
import org.w3c.dom.Node;

public class DisplayAttributes implements Serializable, PropertyAccessor {

    public enum FIELD {

        WILDCARD, epReversed, cdpReversed, Transpose
    }

    private static final transient Logger logger =
            Logger.getLogger(DisplayAttributes.class.toString());
    private static final transient FieldProcessor<FIELD> processor = new FieldProcessor<FIELD>(FIELD.values(), DisplayAttributes.class);
    private static final long serialVersionUID = 9056853143336109920L;
    private Boolean epReversed = Boolean.FALSE;
    private Boolean cdpReversed = Boolean.FALSE;
    private Boolean transpose = Boolean.FALSE;

    public DisplayAttributes() {

    }
    
    public DisplayAttributes(Node node) {
        try {
            setProperty(FIELD.epReversed.toString(), ElementAttributeReader.getString(node, FIELD.epReversed.toString()));
            setProperty(FIELD.cdpReversed.toString(), ElementAttributeReader.getString(node, FIELD.cdpReversed.toString()));
            setProperty(FIELD.Transpose.toString(), ElementAttributeReader.getString(node, FIELD.Transpose.toString()));
        } catch (PropertyAccessorException pae) {
            String errMsg = pae.getMessage();
            throw new RuntimeException(errMsg);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DisplayAttributes other = (DisplayAttributes) obj;
        if (this.epReversed != other.epReversed && (this.epReversed == null || !this.epReversed.equals(other.epReversed))) {
            return false;
        }
        if (this.cdpReversed != other.cdpReversed && (this.cdpReversed == null || !this.cdpReversed.equals(other.cdpReversed))) {
            return false;
        }
        if (this.transpose != other.transpose && (this.transpose == null || !this.transpose.equals(other.transpose))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + (this.epReversed != null ? this.epReversed.hashCode() : 0);
        hash = 37 * hash + (this.cdpReversed != null ? this.cdpReversed.hashCode() : 0);
        hash = 37 * hash + (this.transpose != null ? this.transpose.hashCode() : 0);
        return hash;
    }

    public Boolean getCdpReversed() {
        return cdpReversed;
    }

    public Boolean getEpReversed() {
        return epReversed;
    }

    public String getProperty(String qualifiedFieldName) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedFieldName);
        String remainder = processor.getFieldRemainder(qualifiedFieldName);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        switch (field) {
            case epReversed:
                return FieldProcessor.valueOf(epReversed);
            case cdpReversed:
                return FieldProcessor.valueOf(cdpReversed);
            case Transpose:
                return FieldProcessor.valueOf(transpose);
            case WILDCARD:
                throw new PropertyAccessorException("Error: getProperty should not be invoked with the WILDCARD String.");
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public static String getXmlAlias() {
        return "displayAttributes";
    }

    public Boolean isTransposed() {
        return transpose;
    }

    public void setCdpReversed(Boolean cdpReversed) {
        this.cdpReversed = cdpReversed;
    }

    public void setEpReversed(Boolean epReversed) {
        this.epReversed = epReversed;
    }

    public void setTransposed(Boolean transpose) {
        this.transpose = transpose;
    }

    /**
     * Serializes this BasicLayerProperties object as XML, converting each FIELD to an XML attribute.
     * 
     * @return the BasicLayerProperties state serialized as XML.
     */
    public String saveState() {
        StringBuffer sbuf = new StringBuffer();

        sbuf.append("<" + getXmlAlias() + " ");
        try {
            //append the FIELDs as XML attributes
            sbuf.append(FIELD.epReversed + "=\"" + getProperty(FIELD.epReversed.toString()) + "\" ");
            sbuf.append(FIELD.cdpReversed + "=\"" + getProperty(FIELD.cdpReversed.toString()) + "\" ");
            sbuf.append(FIELD.Transpose + "=\"" + getProperty(FIELD.Transpose.toString()) + "\" ");
        } catch (PropertyAccessorException pae) {
            logger.warning("Unexpected PropertyAccesorException while saving state: " + pae.getMessage());
            throw new RuntimeException(pae);
        }

        sbuf.append(" />\n");

        return sbuf.toString();
    }

    public void setProperty(String qualifiedFieldname, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedFieldname);
        String remainder = processor.getFieldRemainder(qualifiedFieldname);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        try {
            switch (field) {
                case epReversed:
                    epReversed = FieldProcessor.booleanValue(value);
                    break;
                case cdpReversed:
                    cdpReversed = FieldProcessor.booleanValue(value);
                    break;
                case Transpose:
                    transpose = FieldProcessor.booleanValue(value);
                    break;
                case WILDCARD:
                    Boolean parsedValue = FieldProcessor.booleanValue(value);
                    epReversed = parsedValue;
                    cdpReversed = parsedValue;
                    transpose = parsedValue;
                    break;
                default:
                    throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
            }
        } catch (Exception ex) {
            throw new PropertyAccessorException(ex);
        }
    }
}