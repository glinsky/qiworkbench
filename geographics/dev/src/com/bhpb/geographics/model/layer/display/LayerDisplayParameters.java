/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.display;

import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.model.SimpleInheritor;
import com.bhpb.geographics.model.layer.shared.BasicLayerProperties;
import com.bhpb.geographics.model.layer.shared.InterpolationSettings;
import com.bhpb.geographics.model.layer.shared.InterpolationSettings.TYPE;
import com.bhpb.geographics.ui.ValidatingPanel;
import com.bhpb.geographics.util.ColorMap;
import java.awt.Color;
import java.io.Serializable;

/**
 * Base class for different kinds of LayerDisplayParameters, such as
 * SesimicXsecDisplayParameters, ModelXsecDisplayParameters, HorizonXsecDisplayParameters
 * as MapDisplayParameters.
 * 
 * It posseses only two essential fields common to all Layers: fileName and backgroundColor.
 * 
 * Callers who wish to set LayerDisplayParameters fields should either cast it based
 * on the known Layer type and orientation or else use the PropertyAccessor interface.
 * 
 */
public abstract class LayerDisplayParameters implements Serializable, PropertyAccessor,
    SimpleInheritor<LayerDisplayParameters> {

    protected BasicLayerProperties basicProps = new BasicLayerProperties();
    protected InterpolationSettings interpolationSettings = new InterpolationSettings(TYPE.STEP);
    protected String layerName = "UNDEFINED FILE NAME";
    
    private boolean hidden = false;

    /**
     * Gets the basic layer properties, including the layer name and scale.
     *
     * @return the BasicLayerProperties associated with this set of SeismicXsecDisplayParameters
     */
    public BasicLayerProperties getBasicLayerProperties() {
        return basicProps;
    }

    public InterpolationSettings getInterpolationSettings() {
        return interpolationSettings;
    }

    /**
     * This is referred to in the LayerDisplayParams GUI as 'layer name'
     * but is actually the file name, without the layer type or ID.
     * 
     * @return the base file name of the Layer associated with this LayerDisplayParameters
     */
    public String getLayerName() {
        return layerName;
    }

    public void setLayerName(String layerName) {
        this.layerName = layerName;
    }

    public abstract String saveState();
    
    public void setLayerName(String layerName, ValidatingPanel panel) {
        String errMsg;
        
        if (layerName == null) {
            errMsg = "layer name may not be null";
        } else if (layerName.trim().equals("")) {
            errMsg = "layer name may not consist entirely of whitespace";
        } else {
            setLayerName(layerName);
            return;
        }
        
        panel.showValidationFailure("'" + layerName + "' is not valid because " + errMsg);
    }
    
    public abstract Color getBackgroundColor();

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public abstract void setColorMap(ColorMap colorMap);
}