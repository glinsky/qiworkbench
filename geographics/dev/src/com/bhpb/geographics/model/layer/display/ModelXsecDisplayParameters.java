/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.display;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geographics.model.layer.shared.BasicLayerProperties;
import com.bhpb.geographics.model.layer.shared.ColorMapModel;
import com.bhpb.geographics.model.layer.shared.Culling;
import com.bhpb.geographics.model.layer.shared.InterpolationSettings;
import com.bhpb.geographics.model.layer.shared.ModelVerticalRange;
import com.bhpb.geographics.model.layer.shared.Normalization;
import com.bhpb.geographics.model.layer.shared.RasterizationTypeModel;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geoio.util.ElementAttributeReader;
import java.awt.Color;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ModelXsecDisplayParameters extends LayerDisplayParameters {

    private static transient final FieldProcessor<FIELD> processor = new FieldProcessor<FIELD>(FIELD.values(), ModelXsecDisplayParameters.class);
    private static final long serialVersionUID = 1397375045208518658L;

    public enum FIELD {

        ModelVerticalRange, RasterizingType, Culling, Normalization,
        LayerName, Layer, Scale
    }
    private ColorMapModel colorMapModel = new ColorMapModel(ColorMap.DEFAULT_COLORMAP);
    private Culling culling = new Culling();
    private ModelVerticalRange modelVerticalRange = new ModelVerticalRange();
    private Normalization normalization = new Normalization();
    private RasterizationTypeModel rasterProps = new RasterizationTypeModel();

    /**
     * Package-protected constructor to enforce factory usage
     */
    ModelXsecDisplayParameters() {
    }

    public ModelXsecDisplayParameters(Node node) {
        layerName = ElementAttributeReader.getString(node, "layerName");
        setHidden(ElementAttributeReader.getBoolean(node, "hidden"));

        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            String childName = child.getNodeName();
            if (childName.equals("#text")) {
                continue;
            } else if (childName.equals(modelVerticalRange.getXmlAlias())) {
                modelVerticalRange = new ModelVerticalRange(child);
            } else if (childName.equals(BasicLayerProperties.getXmlAlias())) {
                basicProps = new BasicLayerProperties(child);
            } else if (childName.equals(Culling.getXmlAlias())) {
                culling = new Culling(child);
            } else if (childName.equals(Normalization.getXmlAlias())) {
                normalization = new Normalization(child);
            } else if (childName.equals(RasterizationTypeModel.getXmlAlias())) {
                rasterProps = new RasterizationTypeModel(child);
            } else if (childName.equals(ColorMapModel.getXmlAlias())) {
                colorMapModel = ColorMapModel.restoreState(child);
            } else if (childName.equals(InterpolationSettings.getXmlAlias())) {
                interpolationSettings = new InterpolationSettings(child);
            } else {
                throw new RuntimeException("Unknown child of " + getXmlAlias() + ": " + childName);
            }
        }
    }

    public ModelVerticalRange getModelVerticalRange() {
        return modelVerticalRange;
    }

    public Color getBackgroundColor() {
        return colorMapModel.getColorMap().getBackgroundColor();
    }

    public ColorMapModel getColorMapModel() {
        return colorMapModel;
    }

    public Culling getCulling() {
        return culling;
    }

    public Normalization getNormalizationProperties() {
        return normalization;
    }

    public String getProperty(String qualifiedField) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        switch (field) {
            case ModelVerticalRange:
                return modelVerticalRange.getProperty(remainder);
            case Culling:
                return culling.getProperty(remainder);
            case LayerName:
                return layerName;
            case Normalization:
                return normalization.getProperty(remainder);
            case Layer:
            case Scale:
                return basicProps.getProperty(remainder);
            case RasterizingType:
                return rasterProps.getProperty(remainder);
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public RasterizationTypeModel getRasterizationTypeModel() {
        return rasterProps;
    }

    public ColorMap getColorMap() {
        return colorMapModel.getColorMap();
    }

    public static String getXmlAlias() {
        return "modelXsecDisplayParameters";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ModelXsecDisplayParameters other = (ModelXsecDisplayParameters) obj;
        if (this.basicProps != other.basicProps && (this.basicProps == null || !this.basicProps.equals(other.basicProps))) {
            return false;
        }
        if (this.colorMapModel != other.colorMapModel && (this.colorMapModel == null || !this.colorMapModel.equals(other.colorMapModel))) {
            return false;
        }
        if (this.culling != other.culling && (this.culling == null || !this.culling.equals(other.culling))) {
            return false;
        }
        if (this.modelVerticalRange != other.modelVerticalRange && (this.modelVerticalRange == null || !this.modelVerticalRange.equals(other.modelVerticalRange))) {
            return false;
        }
        if (this.normalization != other.normalization && (this.normalization == null || !this.normalization.equals(other.normalization))) {
            return false;
        }
        if (this.rasterProps != other.rasterProps && (this.rasterProps == null || !this.rasterProps.equals(other.rasterProps))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + (this.basicProps != null ? this.basicProps.hashCode() : 0);
        hash = 83 * hash + (this.colorMapModel != null ? this.colorMapModel.hashCode() : 0);
        hash = 83 * hash + (this.culling != null ? this.culling.hashCode() : 0);
        hash = 83 * hash + (this.modelVerticalRange != null ? this.modelVerticalRange.hashCode() : 0);
        hash = 83 * hash + (this.normalization != null ? this.normalization.hashCode() : 0);
        hash = 83 * hash + (this.rasterProps != null ? this.rasterProps.hashCode() : 0);
        return hash;
    }

    public String saveState() {
        StringBuffer content = new StringBuffer();

        content.append("<" + getXmlAlias() + " ");
        content.append("layerName=\"" + layerName + "\" ");
        content.append("hidden=\"" + isHidden() + "\" ");
        content.append(">\n");

        content.append(basicProps.saveState());
        content.append(normalization.saveState());
        content.append(modelVerticalRange.saveState());
        content.append(rasterProps.saveState());
        content.append(culling.saveState());
        content.append(colorMapModel.saveState());

        content.append("</" + getXmlAlias() + ">\n");

        return content.toString();
    }

    public void setProperty(String qualifiedField, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        try {
            switch (field) {
                case Culling:
                    culling.setProperty(remainder, value);
                    break;
                case ModelVerticalRange:
                    modelVerticalRange.setProperty(remainder, value);
                    break;
                case Normalization:
                    normalization.setProperty(remainder, value);
                    break;
                case Layer:
                case Scale:
                    basicProps.setProperty(remainder, value);
                    break;
                case LayerName:
                    layerName = value;
                    break;
                case RasterizingType:
                    rasterProps.setProperty(remainder, value);
                    break;
                default:
                    throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
            }
        } catch (Exception ex) {
            throw new PropertyAccessorException(ex);
        }
    }

    public void setColorMap(ColorMap colorMap) {
        colorMapModel.setColorMap(colorMap);
    }

    /**
     * Returns a copy of this ModelXsecDisplayParameters, with all properties except
     * for name set to match the corresponding fields of ancestor,
     * if aLayerDisplayParameters is also a ModelXsecDisplayParameters.  If ancestor
     * is of any other type, then this ModelXsecDisplayParameters is returned instead*
     *
     * *if the ancestor is not a HorizonXsecLayerDisplayParameters, then the ColorMap _is_ inherited
     *
     * @param ancestor LayerDisplayParameters used for inheritance
     * @throws IllegalArgumentException if ancestor is not an intance of SeismicXsec, ModelXsec, HorizonXsec or MapDisplayParameters
     *
     * @return LayerDisplayParameters inherited from ancestor
     */
    public LayerDisplayParameters inheritFrom(LayerDisplayParameters ancestor) {
        if (ancestor == null) {
            return this;
        } //inherit nothing from Horizon xsec display params
        else if (ancestor instanceof HorizonXsecDisplayParameters) {
            return this;
        } //inherit only ColorMap from SeismicXsec or Map display params
        else if (ancestor instanceof SeismicXsecDisplayParameters) {
            SeismicXsecDisplayParameters seismicXsecDisplayParams = (SeismicXsecDisplayParameters) ancestor;
            colorMapModel.setColorMap(seismicXsecDisplayParams.getColorMap());
            return this;
        } else if (ancestor instanceof MapDisplayParameters) {
            MapDisplayParameters mapDisplayParams = (MapDisplayParameters) ancestor;
            colorMapModel.setColorMap(mapDisplayParams.getColorAttributes().getColorMap());
            return this;
        } else if (ancestor instanceof ModelXsecDisplayParameters) {
            ModelXsecDisplayParameters ldParamsCopy =
                    ModelXsecDisplayParametersFactory.createLayerDisplayParameters(this);
            ModelXsecDisplayParameters modelXsecDisplayParams = (ModelXsecDisplayParameters) ancestor;

            Normalization normProps = ldParamsCopy.getNormalizationProperties();
            Normalization aNormProps = modelXsecDisplayParams.getNormalizationProperties();
            normProps.setNormalizationScale(aNormProps.getNormalizationScale());
            normProps.setNormalizationType(aNormProps.getNormalizationType());
            normProps.setNormalizationMinValue(aNormProps.getNormalizationMinValue());
            normProps.setNormalizationMaxValue(aNormProps.getNormalizationMaxValue());

            RasterizationTypeModel rasterization = ldParamsCopy.getRasterizationTypeModel();
            RasterizationTypeModel aRasterization = modelXsecDisplayParams.getRasterizationTypeModel();

            rasterization.setRasterizeInterpolatedDensity(aRasterization.doRasterizeInterpolatedDensity());
            rasterization.setRasterizeNegativeFill(aRasterization.doRasterizeNegativeFill());
            rasterization.setRasterizePositiveFill(aRasterization.doRasterizePositiveFill());
            rasterization.setRasterizeVariableDensity(aRasterization.doRasterizeVariableDensity());
            rasterization.setRasterizeWiggleTraces(aRasterization.doRasterizeWiggleTraces());

            ldParamsCopy.getCulling().setClippingFactor(modelXsecDisplayParams.getCulling().getClippingFactor());
            ldParamsCopy.getCulling().setDecimationSpacing(modelXsecDisplayParams.getCulling().getDecimationSpacing());
            ldParamsCopy.getColorMapModel().setColorMap(modelXsecDisplayParams.getColorMapModel().getColorMap());

            ldParamsCopy.getModelVerticalRange().setStartTime(modelXsecDisplayParams.getModelVerticalRange().getStartTime());
            ldParamsCopy.getModelVerticalRange().setEndTime(modelXsecDisplayParams.getModelVerticalRange().getEndTime());
            ldParamsCopy.getModelVerticalRange().setStructuralInterpolationEnabled(modelXsecDisplayParams.getModelVerticalRange().isStructuralInterpolationEnabled());

            ldParamsCopy.getInterpolationSettings().setType(modelXsecDisplayParams.getInterpolationSettings().getType());

            return ldParamsCopy;
        } else {
            throw new IllegalArgumentException("Cannot inherit from unknown LayerDisplayParameters subclass: " +
                    ancestor.getClass().getName());
        }
    }
}