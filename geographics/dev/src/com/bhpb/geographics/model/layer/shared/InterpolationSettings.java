/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.shared;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geoio.util.ElementAttributeReader;
import java.util.logging.Logger;
import org.w3c.dom.Node;

public class InterpolationSettings implements PropertyAccessor {

    public enum TYPE {

        LINEAR, QUADRATIC , STEP
    }

    public enum FIELD {

        Type
    }

    private static transient final FieldProcessor<FIELD> processor = new FieldProcessor<FIELD>(FIELD.values(), InterpolationSettings.class);
    private static final Logger logger =
            Logger.getLogger(InterpolationSettings.class.getName());

    private TYPE type;
    
    public InterpolationSettings(TYPE initialType) {
        this.type = initialType;
    }

    public InterpolationSettings(Node node) {
        try {
            setProperty(FIELD.Type.toString(), ElementAttributeReader.getString(node, FIELD.Type.toString()));
        } catch (PropertyAccessorException pae) {
            String errMsg = pae.getMessage();
            throw new RuntimeException(errMsg);
        }
    }

    public String getProperty(String qualifiedField) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        switch (field) {
            case Type:
                return type.toString();
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public TYPE getType() {
        return type;
    }

    public static String getXmlAlias() {
        return "interpolationSettings";
    }

    /**
     * Serializes this InterpolationSettings object as XML, converting each FIELD to an XML attribute.
     *
     * @return the BasicLayerProperties state serialized as XML.
     */
    public String saveState() {
        StringBuffer sbuf = new StringBuffer();

        sbuf.append("<" + getXmlAlias() + " ");
        try {
            //append the FIELDs as XML attributes
            sbuf.append(FIELD.Type + "=\"" + getProperty(FIELD.Type.toString()) + "\" ");
        } catch (PropertyAccessorException pae) {
            logger.warning("Unexpected PropertyAccesorException while saving state: " + pae.getMessage());
            throw new RuntimeException(pae);
        }

        sbuf.append(" />\n");

        return sbuf.toString();
    }

    public void setType(TYPE newType) {
        this.type = newType;
    }

    public void setProperty(String qualifiedField, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        try {
            switch (field) {
                case Type:
                    type = TYPE.valueOf(value);
                    break;
                default:
                    throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
            }
        } catch (Exception ex) {
            throw new PropertyAccessorException(ex);
        }
    }
}