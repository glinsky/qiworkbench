/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.layer.display;

import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoFileType;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;

public class LayerDisplayParametersFactory {

    public static LayerDisplayParameters createLayerDisplayParameters(DatasetProperties dsProps) {
        GeoDataOrder order = dsProps.getMetadata().getGeoDataOrder();
        GeoFileType type = dsProps.getMetadata().getGeoFileType();

        switch (order) {
            case CROSS_SECTION_ORDER:
                if (dsProps.isSeismic()) {
                    return SeismicXsecDisplayParametersFactory.createLayerDisplayParameters(dsProps);
                } else if (dsProps.isModel()) {
                    return ModelXsecDisplayParametersFactory.createLayerDisplayParameters(dsProps);
                } else if (dsProps.isHorizon()) {
                    return HorizonXsecDisplayParametersFactory.createLayerDisplayParameters(dsProps);
                } else {
                    throw new UnsupportedOperationException("Cannot create (Xsec) LayerDisplayParameters for GeoFileType: " + type + " because isModel(), isSeismic() and isHorizon() are all false.");
                }
            case MAP_VIEW_ORDER:
                if (dsProps.isSeismic() || dsProps.isModel() || dsProps.isHorizon()) {
                    return MapDisplayParametersFactory.createLayerDisplayParameters(dsProps);
                } else {
                    throw new UnsupportedOperationException("Cannot create (Map) LayerDisplayParameters for GeoFileType: " + type + " because isModel(), isSeismic() and isHorizon() are all false.");
                }
            default:
                throw new UnsupportedOperationException("Cannot create LayerDisplayParameters for GeoDataOrder: " + order);
        }
    }
}
