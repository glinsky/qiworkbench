/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import java.util.logging.Logger;

/**
 * Refactoring in progress - TraceAnnotationMap is deprecated and will be replaced
 * by methods of Annotation which use DataObject[] and DatasetProperties to
 * get HeaderKey and HeaderEntry information using GeoIO.
 *
 * @author folsw9
 */
public class Annotation {
    public enum AXIS { X_AXIS, Y_AXIS };

    private static final Logger logger =
            Logger.getLogger(Annotation.class.getName());

    public static final int DEFAULT_MAJOR_TIC_X_AXIS = 60;
    public static final int DEFAULT_MAJOR_TIC_Y_AXIS_XSEC = 50;
    public static final int DEFAULT_MAJOR_TIC_Y_AXIS_MODEL = 60;
    public static final int DEFAULT_MINOR_TIC = 10;

    private AnnotationTickGenerator tickGenerator;
    private AXIS orientation;
    private DataObject[] dataObjects;
    //private DatasetProperties dsProperties;
    private Layer annotatedLayer;
    private double scale = 1.0;
    private int majorTic;
    private int max;
    private int min;
    private int minorTic = DEFAULT_MINOR_TIC;
    private int tickOffset = 0;
    private String caption;
    private TraceAnnotationMap traceAnnotationMap;

    public Annotation(AXIS axis,
            int min,
            int max,
            String caption,
            int tickOffset,
            DataObject[] dataObjects,
            Layer annotatedLayer,
            double scale) {
        orientation = axis;

        this.min = min;
        this.max = max;
        this.caption = caption;
        this.tickOffset = tickOffset;
        
        this.dataObjects = new DataObject[dataObjects.length];

        System.arraycopy(dataObjects,0,this.dataObjects,0,dataObjects.length);
        
        //this.dsProperties = dsProperties;
        this.annotatedLayer = annotatedLayer;
        this.scale = scale;

        tickGenerator = new DefaultAnnotationTickGenerator();
        
        switch (axis) {
            case X_AXIS:
                majorTic = DEFAULT_MAJOR_TIC_X_AXIS;
                break;
            case Y_AXIS:
                if (annotatedLayer.isModelLayer()) {
                    majorTic = Annotation.DEFAULT_MAJOR_TIC_Y_AXIS_MODEL;
                } else {
                    majorTic = Annotation.DEFAULT_MAJOR_TIC_Y_AXIS_XSEC;
                }
                break;
            default:
                throw new UnsupportedOperationException("Unknown axis: " + axis);
        }
    }

    /**
     * @deprecated Use the new constructor which takes DataObjcet[] and DatasetProperties
     * instead of the deprecated TraceAnnotationMap.
     * 
     * @param axis
     * @param min
     * @param max
     * @param caption
     * @param tickOffset
     * @param traceAnnotationMap
     * @param scale
     */
    public Annotation(AXIS axis,
            int min,
            int max,
            String caption,
            int tickOffset,
            TraceAnnotationMap traceAnnotationMap,
            double scale,
            Layer annotatedLayer) {
        orientation = axis;

        this.min = min;
        this.max = max;
        this.caption = caption;
        this.tickOffset = tickOffset;
        this.traceAnnotationMap = traceAnnotationMap;
        this.scale = scale;

        //this is not necessarily correct default but it would be pointless to 
        //add the parameter needed to correctly set it to a method which is deprecated anyway
        majorTic = DEFAULT_MAJOR_TIC_X_AXIS;

        tickGenerator = new DefaultAnnotationTickGenerator();
    }

    public AXIS getAxis() {
        return orientation;
    }

    public String getCaption() {
        return caption;
    }

    private int getNumDataObjects() {
        if (dataObjects == null) {
            return 0;
        } else {
            return dataObjects.length;
        }
    }

    public int getMinorTic() {
        return minorTic;
    }

    public int getMajorTic() {
        return majorTic;
    }

    public double getScale() {
        return scale;
    }

    public int getTickOffset() {
        return tickOffset;
    }

    public String[] getTickValues(double currentTickScale) {
        if (annotatedLayer != null) {
            return getTickValues(caption, 0, currentTickScale);
        } else {
            logger.warning("Unable to get tick values due to null annotatedLayer");
            return new String[0];
        }
    }

    public int getMax() {
        return max;
    }

    public int getMin() {
        return min;
    }

    /**
     * Get the values of the specified header key every stepSize number of pixels
     * starting at base, until the end of the set of mapped traces.
     * 
     * @param headerKey the name of the header key for which values will be returned
     * @param base the index of the first trace for which to look up the header key value
     * @param currentTickScale actual number of pixels between adjacent ticks
     * based on the axis, configured majorTic parameter and zoom level
     *
     * @return int[] containing the header key values at the indcated traces
     */
    public String[] getTickValues(String headerKey,
            double base,
            double currentTickScale) {

        if (scale < 0.0) {
            throw new IllegalArgumentException("Unable to get annotation tick values - scale may not be < 0.0");
        }

        int nTicks;

        String[] tickCaptionArray;

        if (isVerticalRangeKeyName(headerKey)) {
            //vertical range divided by units per tick plus one for the first tick at min
            nTicks = tickGenerator.getNumTicks(AXIS.Y_AXIS, annotatedLayer, currentTickScale, dataObjects, base);

            tickCaptionArray = tickGenerator.getCaptions(AXIS.Y_AXIS, annotatedLayer, currentTickScale, nTicks,
                    dataObjects, base, headerKey);
        } else {
            nTicks = tickGenerator.getNumTicks(AXIS.X_AXIS, annotatedLayer, currentTickScale, dataObjects, base);

            tickCaptionArray = tickGenerator.getCaptions(AXIS.X_AXIS, annotatedLayer, currentTickScale, nTicks,
                    dataObjects, base, headerKey);
        }

        return tickCaptionArray;
    }

    /**
     * Gets the next integer multiple of the largest power of 10 which is less than or equal to
     * the requested tick scale (# of values between adjacent ticks).  For example, getAdjustedTickIncr(50.0) would return 50.0,
     * getAdjustedTickIncr(51.0) would return (60.0) and getadjusetdTickIncr(101.0) would return (200.0).
     * 
     * Uses the configured majorTic increment and the scale calculated from the
     * current Window properties and zoom level.
     * 
     * @return smallest integer multiple of largest power of 10, <= minTickIncr * scale
     */
    public double getValuesPerTick() {
        switch (orientation) {
            case X_AXIS :
                return majorTic / scale;
            case Y_AXIS :
                return getAdjustedTickScale(majorTic, scale);
            default :
                throw new IllegalArgumentException("Unknown axis: " + orientation);
        }
    }

    /**
     * Gets the next integer multiple of the largest power of 10 which is less than or equal to
     * the requested tick scale (# of values between adjacent ticks).  For example, getAdjustedTickIncr(50.0) would return 50.0,
     * getAdjustedTickIncr(51.0) would return (60.0) and getadjusetdTickIncr(101.0) would return (200.0).
     * 
     * @param minTickIncr the minimum number of pixels between adjacent tickmarks
     * @param the current scale of pixels to tickmarks
     * 
     * @return smallest integer multiple of largest power of 10, <= minTickIncr * scale
     */
    private double getAdjustedTickScale(double minTickIncr, double scale) {
        double minTickScale = minTickIncr/scale;

        //largest integer log base 10 <= minTickScale
        double lowerExpOfTen = Math.floor(Math.log10(minTickScale));
        //largest power of 10 <= minTickScale
        double lowerPowOfTen = Math.pow(10,lowerExpOfTen);
        //smallest integer multiple of the largest power of 10 that is <= minTickScale
        double nextMultipleOfPowOfTen = Math.ceil(minTickScale/lowerPowOfTen);

        //return smallest integer multiple of largest power of 10, <= minTickIncr * scale
        return nextMultipleOfPowOfTen * lowerPowOfTen;
    }

    /**
     * @param name the name of a header key e.g. "ep", "cdp", "offset", "tracl"
     * @return true if and only if the name matches the vertical range key name,
     * which is defined to be the last name in the header key name to range map.
     */
    private boolean isVerticalRangeKeyName(String name) {
        DatasetProperties dsProperties = annotatedLayer.getDatasetProperties();
        String[] headerKeyNames = dsProperties.getMetadata().getKeys().toArray(new String[0]);
        int numHeaderKeys = headerKeyNames.length;
        return headerKeyNames[numHeaderKeys - 1].equals(name);
    }
    
    public void setScale(double scale) {
        this.scale = scale;
    }
}