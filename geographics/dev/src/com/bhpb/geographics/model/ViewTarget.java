/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import com.bhpb.geographics.model.layer.shared.BasicLayerProperties;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geographics.model.window.WindowScaleProperties;
import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.datasystems.UnlimitedKeyRange;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import java.awt.geom.Point2D;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * A point which may be targetted by the cursor within a GeoPlot.  It represents
 * a point within the coordinate system used for communication among with Layers,
 * for example, with the TargetInfoPanel or to broadcast to other ViewerWindows.
 *
 * @author woody
 */
public class ViewTarget {
    private static final transient Logger logger =
            Logger.getLogger(ViewTarget.class.getName());
    private static final transient DecimalFormat valueFormat = new DecimalFormat("#.###");
    private static final transient DecimalFormat verticalFormat = new DecimalFormat("#,###,###");
    /** This ViewTarget indicates that the current target is
     *  invalid or that no target has yet been chosen.
     */
    public static final ViewTarget NO_TARGET =
            new ViewTarget(0.0, new HashMap<String, Number>(), 0.0, "", 0,
            GeoDataOrder.UNKNOWN_ORDER, null, 0);

    private Double traceOffset;
    private Double value;
    private Double vertical;
    private GeoDataOrder dataOrder;
    private Integer nearestTraceIndex;
    private Map<String, Number> headerKeyMap;
    private String verticalKeyName;

    /**
     * If layer is null, a ViewTarget is created which specifies the beginning of
     * the target trace at the indicated vertical value.  Otherwise, an offset ranging from
     * 0.0 to 1.0 inclusive within the trace is calculated, making the ViewTarget specify an
     * arbitrary point within the target trace.
     *
     * @param vertical
     * @param headerKeyMap
     * @param value
     * @param verticalKeyName
     * @param nearestTraceIndex
     * @param dataOrder
     * @param layer
     * @param x
     * @param y
     */
    private ViewTarget(double vertical, Map<String, Number> headerKeyMap,
            double value, String verticalKeyName, int nearestTraceIndex,
            GeoDataOrder dataOrder, Layer layer, int x) {
        this.vertical = vertical;
        this.headerKeyMap = headerKeyMap;
        this.value = value;
        this.verticalKeyName = verticalKeyName;
        this.nearestTraceIndex = nearestTraceIndex;
        this.dataOrder = dataOrder;

        if (layer != null) {
        Point2D nearestTracePoint = getPoint2D(layer,0);
        WindowProperties winProps = layer.getWindowModel().getWindowProperties();
        WindowScaleProperties scaleProps = winProps.getScaleProperties();

        BasicLayerProperties basicLayerProps = layer.getDisplayParameters().getBasicLayerProperties();

        double xScale = scaleProps.getHorizontalPixelIncr() * basicLayerProps.getHorizontalScale();
            this.traceOffset = (x - nearestTracePoint.getX()) / (xScale);
            if (traceOffset < 0 || traceOffset > 1) {
                throw new IllegalArgumentException("Invalid nearestTraceIndex for x: " + x + "; traceOffset out of range [0..1]");
            }
        } else {
            this.traceOffset = 0.0;
        }
    }

    /**
     * Constructor used by the createTargetProperties() method; it takes an explicit traceOffset.
     *
     * @param vertical
     * @param headerKeyMap
     * @param value
     * @param verticalKeyName
     * @param nearestTraceIndex
     * @param dataOrder
     * @param traceOffset
     */
    private ViewTarget(double vertical, Map<String, Number> headerKeyMap,
            double value, String verticalKeyName, int nearestTraceIndex,
            GeoDataOrder dataOrder, double traceOffset) {
        if (traceOffset < 0 || traceOffset > 1) {
            throw new IllegalArgumentException("TraceOffset must be within range [0..1]");
        }
        this.vertical = vertical;
        this.headerKeyMap = headerKeyMap;
        this.value = value;
        this.verticalKeyName = verticalKeyName;
        this.nearestTraceIndex = nearestTraceIndex;
        this.dataOrder = dataOrder;
        this.traceOffset = traceOffset;
    }

    /**
     * Creates ViewTarget for coordinates x,y using the windowProperties currently
     * associated with the indicated Layer.
     * @param layer
     * 
     * @param x horizontal coordinate in pixels
     * @param y vertical coordinate in pixels
     *
     * @return ViewTarget representing the point at coordinates x,y of the specified Layer
     */
    public static ViewTarget createTargetProps(Layer layer, int x, int y) {
        return createTargetProps(layer.getWindowModel().getWindowProperties(), layer, x, y);
    }

    /**
     * Return the properties associated with the element drawn at the
     * specified pixel coordinates.  Returns null if the mouse pointer is outside
     * of the trace boundaries or if the layer is not a seismic layer.
     *
     * @param x int x-coordinate of the target pixel.
     * @param y int y-coordinate of the target pixel.
     *
     * @return ViewTargetProperties representing the attributes of the element
     * under the mouse pointer at coordinates (x,y).
     */
    public static ViewTarget createTargetProps(WindowProperties winProps, Layer layer, int x, int y) {
        GeoDataOrder geoDataOrder = layer.getDatasetProperties().getMetadata().getGeoDataOrder();
        
        WindowScaleProperties scaleProps = winProps.getScaleProperties();

        BasicLayerProperties basicLayerProps = layer.getDisplayParameters().getBasicLayerProperties();

        double xScale = scaleProps.getHorizontalPixelIncr() * basicLayerProps.getHorizontalScale();
        double yScale = scaleProps.getVerticalPixelIncr() * basicLayerProps.getVerticalScale();

        switch (geoDataOrder) {
            case CROSS_SECTION_ORDER :
                return getXsecTargetProps(layer, x, y, xScale, yScale);
            case MAP_VIEW_ORDER :
                return getMapTargetProps(layer, x, y, xScale, yScale);
            default :
                logger.warning("Unable to get ViewTarget for unknown GeoDataOrder: " + geoDataOrder);
                return NO_TARGET;
        }
    }

    private static ViewTarget getXsecTargetProps(Layer layer, int x, int y, double xScale, double yScale) {
        DataObject[] traces = layer.getDataObjects();
        double minTimeOrDepth = layer.getVerticalMin();        
        double maxTimeOrDepth = layer.getVerticalMax();
        double sampleRate = layer.getDatasetProperties().getSummary().getSampleRate();

        TraceAnnotationMap traceAnnotationMap = layer.getTraceAnnotationMap();

        if (x < 0 || y < 0) {
            return NO_TARGET;
        } else if (x >= traces.length * xScale) {
            return NO_TARGET;
        } else if (y >= (maxTimeOrDepth - minTimeOrDepth) * yScale) {
            return NO_TARGET;
        } else {
            double nearestSampleTimeOrDepthOffset = y / yScale;
            int prevSampleIndex = (int) Math.floor(nearestSampleTimeOrDepthOffset / sampleRate);
            int nearestTraceIndex = (int) Math.floor(x / xScale);

            double nearestSampleTimeOrDepth = minTimeOrDepth + nearestSampleTimeOrDepthOffset;
            if (nearestTraceIndex < 0 || nearestTraceIndex > traces.length - 1) {
                return NO_TARGET;
            } else if (nearestSampleTimeOrDepthOffset < 0 || nearestSampleTimeOrDepthOffset > (maxTimeOrDepth-minTimeOrDepth)) {
                return NO_TARGET;
            } else {
                Map<String, Number> hkMap = traceAnnotationMap.getHeaderKeyMap(nearestTraceIndex);

                double value;

                if (layer.isHorizonLayer()) {
                    //HorizonProperties hProps = (HorizonProperties) layer.getDatasetProperties();
                    //value = traces[nearestTraceIndex].getFloatVector()[hProps.getSelectedHorizonIndex(hProps.getSelectedHorizon())];
                    value = traces[nearestTraceIndex].getFloatVector()[0];
                } else {
                    value = traces[nearestTraceIndex].getFloatVector()[prevSampleIndex];
                }

                return new ViewTarget(nearestSampleTimeOrDepth, 
                        hkMap,
                        value,
                        traceAnnotationMap.getVerticalKeyName(),
                        nearestTraceIndex,
                        GeoDataOrder.CROSS_SECTION_ORDER,
                        layer,
                        x);
            }
        }
    }

    private static int getKeyIncr(AbstractKeyRange vKeyRange) {
        if (vKeyRange.isUnlimited()) {
            return (int) Math.round(((UnlimitedKeyRange) vKeyRange).getFullKeyRange().getIncr());
        } else if (vKeyRange.isDiscrete()) {
            return (int) Math.round(((DiscreteKeyRange) vKeyRange).getIncr());
        } else {
            throw new IllegalArgumentException("Vertical Key Range must be unlimited or discrete");
        }
    }

    private static ViewTarget getMapTargetProps(Layer layer, int x, int y,
            double xScale, double yScale) {
        double minTimeOrDepth = layer.getDatasetProperties().getVerticalCoordMin();
        double maxTimeOrDepth = layer.getDatasetProperties().getVerticalCoordMax();
        TraceAnnotationMap traceAnnotationMap = layer.getTraceAnnotationMap();

        DatasetProperties dsProperties = layer.getDatasetProperties();
        ArrayList<String> keyNames = dsProperties.getMetadata().getKeys();
        String vKeyName = keyNames.get(keyNames.size() - 1);
        AbstractKeyRange vKeyRange = dsProperties.getKeyChosenRange(vKeyName);
        int vKeyIncr = getKeyIncr(vKeyRange);

        DataObject[] traces = layer.getDataObjects();

        if (x < 0 || y < 0) {
            return NO_TARGET;
        } else if (x >= traces.length * xScale) {
            return NO_TARGET;
        } else if (y >= (maxTimeOrDepth - minTimeOrDepth) * yScale) {
            return NO_TARGET;
        } else {
            double nearestCrossLineIndex = Math.floor(y / (yScale * vKeyIncr));

            int nearestTraceIndex = (int) Math.floor(x / xScale);

            if (nearestTraceIndex < 0 || nearestTraceIndex > traces.length - 1) {
                return NO_TARGET;
            } else if (nearestCrossLineIndex < 0 || nearestCrossLineIndex > (maxTimeOrDepth - minTimeOrDepth) / vKeyIncr) {
                return NO_TARGET;
            } else {
                //bugfix add the sample Start to the crossline index for map targets
                double sampleStart = layer.getDatasetProperties().getSummary().getSampleStart();
                Map<String, Number> hkMap = traceAnnotationMap.getHeaderKeyMap(nearestTraceIndex);
                return new ViewTarget(sampleStart + vKeyIncr * nearestCrossLineIndex, hkMap, traces[nearestTraceIndex].getFloatVector()[(int) nearestCrossLineIndex],
                        traceAnnotationMap.getVerticalKeyName(), nearestTraceIndex,
                        layer.getDatasetProperties().getMetadata().getGeoDataOrder(),
                        layer, x);
            }
        }
    }

    private static boolean sourceHeaderContainsTargetVerticalKey(ViewTarget src, String targetVkeyName) {
        for (String hKeyName : src.getHeaderKeyNames()) {
            if (hKeyName.equals(targetVkeyName)) {
                return true;
            }
        }

        return false;
    }

    private GeoDataOrder getGeoDataOrder() {
        return dataOrder;
    }

    public static ViewTarget copyTargetProperties(ViewTarget srcProps, Layer destLayer) {
        double vertical = srcProps.getVertical();
        String verticalKeyName;
        TraceAnnotationMap destTraceAnnotMap = destLayer.getTraceAnnotationMap();

        //case 1: either source or target is in UNKNOWN order, no target copy is possible
        GeoDataOrder dataOrder = destLayer.getDatasetProperties().getMetadata().getGeoDataOrder();
        if (srcProps.getGeoDataOrder() == GeoDataOrder.UNKNOWN_ORDER ||
                dataOrder == GeoDataOrder.UNKNOWN_ORDER) {
            return NO_TARGET;
        }
        //case 2: MAP -> XSEC target copy is not permitted because vertical coord cannot be determined
        if (srcProps.getGeoDataOrder() == GeoDataOrder.MAP_VIEW_ORDER &&
                dataOrder == GeoDataOrder.CROSS_SECTION_ORDER) {
            return NO_TARGET;
        }

        //case 3: MAP->MAP or XSEC->XSEC
        verticalKeyName = destTraceAnnotMap.getVerticalKeyName();
        if (dataOrder == srcProps.getGeoDataOrder()) { // XSEC->XSEC or MAP->MAP
            if (!srcProps.getVerticalKeyName().equals(destTraceAnnotMap.getVerticalKeyName())) {
                return NO_TARGET; //geoDataOrders are the same but the vertical keys are different, cannot copy target
            }
        } else { //GeoDataOrders are different and neither are UNKNOWN, therefore this is XSEC -> MAP.
            if (!sourceHeaderContainsTargetVerticalKey(srcProps, verticalKeyName)) {
                return NO_TARGET; //ViewTarget originates from xsec layer with incompatible headers for this map's vertical key
            }
        }

        //axes are compatible but source event vertical coordinate is out of range for target layer
        vertical = srcProps.getVertical();

        AbstractKeyRange destVertKeyRange = destLayer.getDatasetProperties().getKeyChosenRange(verticalKeyName);

        if (destVertKeyRange instanceof DiscreteKeyRange) {
            DiscreteKeyRange asDiscrete = (DiscreteKeyRange) destVertKeyRange;
            if (asDiscrete.getMin() > vertical || asDiscrete.getMax() < vertical) {
                return NO_TARGET;
            }
        } else if (destVertKeyRange instanceof UnlimitedKeyRange) {
            DiscreteKeyRange fullKeyRange = ((UnlimitedKeyRange) destVertKeyRange).getFullKeyRange();
            if (fullKeyRange.getMin() > vertical || fullKeyRange.getMax() < vertical) {
                return NO_TARGET;
            }
        } else {
            return NO_TARGET; //destination vertical key range should always be discrete or unlimited
        }



        if (destTraceAnnotMap.isCompatible(srcProps.getHeaderKeyMap(), dataOrder)) {
            GeoFileDataSummary summary = destLayer.getDatasetProperties().getSummary();
            double sampleStart = summary.getSampleStart();
            double sampleRate = summary.getSampleRate();
            int prevSampleIndex = (int) ((vertical - sampleStart) / sampleRate);

            int nearestTraceIndex = destTraceAnnotMap.calculateTraceIndex(srcProps.getHeaderKeyMap());
            Map<String, Number> headerKeyMap = destTraceAnnotMap.getHeaderKeyMap(nearestTraceIndex);

            double value = destLayer.getDataObjects()[nearestTraceIndex].getFloatVector()[prevSampleIndex];
            return new ViewTarget(vertical, headerKeyMap, value, verticalKeyName, nearestTraceIndex, dataOrder, srcProps.getTraceOffset());
        } else {
            return NO_TARGET;
        }
    }

    private Map<String, Number> getHeaderKeyMap() {
        return new HashMap(headerKeyMap);
    }

    public String[] getHeaderKeyNames() {
        return (String[]) headerKeyMap.keySet().toArray(new String[0]);
    }

    public Number getHeaderKeyValue(String hkName) {
        return headerKeyMap.get(hkName);
    }

    public String getHorizontalString() {
        StringBuilder sb = new StringBuilder();
        for (String hkName : getHeaderKeyNames()) {
            String hkValue = headerKeyMap.get(hkName).toString();
            if (hkName.equals(getVerticalKeyName())) {
                continue;
            }
            sb.append(hkName);
            sb.append(": ");
            sb.append(hkValue);
            sb.append(" ");
        }
        sb.append("value: ");

        if (Double.isNaN(value)) {
            sb.append("NaN");
        } else {
            sb.append(valueFormat.format(value));
        }

        return sb.toString();
    }

    public int getNearestTraceIndex() {
        return nearestTraceIndex;
    }

    public Point2D getPoint2D(Layer layer) {
        return getPoint2D(layer,traceOffset);
    }
    /**
     * Calculates the view-space Point2D equivalent to this ViewTargetProperties using the
     * scale of the specified Layer and its WindowProperties.
     *
     * @param layer
     *
     * @return
     */
    private Point2D getPoint2D(Layer layer, double traceOffset) {
        WindowProperties winProps = layer.getWindowModel().getWindowProperties();
        WindowScaleProperties scaleProps = winProps.getScaleProperties();

        BasicLayerProperties basicLayerProps = layer.getDisplayParameters().getBasicLayerProperties();

        double xScale = scaleProps.getHorizontalPixelIncr() * basicLayerProps.getHorizontalScale();
        double yScale = scaleProps.getVerticalPixelIncr() * basicLayerProps.getVerticalScale();

        //the x coordinate of the point is located in the center of the trace
        double xCoord = (nearestTraceIndex + traceOffset) * xScale;
        
        DatasetProperties dsProperties = layer.getDatasetProperties();
        ArrayList<String> keyNames = dsProperties.getMetadata().getKeys();
        String vKeyName = keyNames.get(keyNames.size() - 1);
        AbstractKeyRange vKeyRange = dsProperties.getKeyChosenRange(vKeyName);
        float vKeyIncr;
        if (vKeyRange.isUnlimited()) {
            vKeyIncr = Math.round(((UnlimitedKeyRange) vKeyRange).getFullKeyRange().getIncr());
        } else if (vKeyRange.isDiscrete()) {
            vKeyIncr = Math.round(((DiscreteKeyRange) vKeyRange).getIncr());
        } else {
            throw new IllegalArgumentException("Vertical Key Range must be unlimited or discrete");
        }

        double yCoord;
        //likewise, the y coordinate is centered on the sample _if map_
        //assuming a 'sample rate' of 1 trace
        double sampleStart = layer.getVerticalMin();
        switch (layer.getOrientation()) {
            case MAP_VIEW_ORDER:
                yCoord = ((vertical - sampleStart) + 0.5 * vKeyIncr) * yScale;
                break;
            case CROSS_SECTION_ORDER:
                //if xsec, the vertical coordinate can be any value in the sample range
                yCoord = (vertical - sampleStart) * yScale;
                break;
            default:
                throw new RuntimeException("Unknown layer orientation: " + layer.getOrientation());
        }

        return new Point2D.Double(xCoord, yCoord);
    }

    /**
     * Gets offset, a factor ranging from 0.0 to 1.0, which indicates the exact horizontal position
     * of the target point within the trace.
     * 
     * @return traceOffset
     */
    public double getTraceOffset() {
        return traceOffset;
    }

    public double getVertical() {
        return vertical;
    }

    public String getVerticalKeyName() {
        return verticalKeyName;
    }

    /**
     * Gets the String description of the vertical target coordinate.  If the vertical
     * key name is 'tracl', that is, this ViewTarget is within a Cross-Section Window,
     * then the vertical keyname is omitted, otherwise, it is prepended to the vetical
     * coordinate, which is shown in the format #,###.
     *
     * @return String description of the ViewTarget's vertical coordinate
     */
    public String getVerticalString() {
        String captionPrefix;
        if ("tracl".equals(verticalKeyName)) {
            captionPrefix = "";
        } else {
            captionPrefix = verticalKeyName + ": ";
        }
        return captionPrefix + verticalFormat.format(vertical);
    }

    public static String getXmlAlias() {
        return "viewTarget";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ViewTarget other = (ViewTarget) obj;
        if (this.traceOffset != other.traceOffset && (this.traceOffset == null || !this.traceOffset.equals(other.traceOffset))) {
            return false;
        }
        if (this.value != other.value && (this.value == null || !this.value.equals(other.value))) {
            return false;
        }
        if (this.vertical != other.vertical && (this.vertical == null || !this.vertical.equals(other.vertical))) {
            return false;
        }
        if (this.dataOrder != other.dataOrder) {
            return false;
        }
        if (this.nearestTraceIndex != other.nearestTraceIndex && (this.nearestTraceIndex == null || !this.nearestTraceIndex.equals(other.nearestTraceIndex))) {
            return false;
        }
        if (this.headerKeyMap != other.headerKeyMap && (this.headerKeyMap == null || !this.headerKeyMap.equals(other.headerKeyMap))) {
            return false;
        }
        if ((this.verticalKeyName == null) ? (other.verticalKeyName != null) : !this.verticalKeyName.equals(other.verticalKeyName)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (this.traceOffset != null ? this.traceOffset.hashCode() : 0);
        hash = 59 * hash + (this.value != null ? this.value.hashCode() : 0);
        hash = 59 * hash + (this.vertical != null ? this.vertical.hashCode() : 0);
        hash = 59 * hash + (this.dataOrder != null ? this.dataOrder.hashCode() : 0);
        hash = 59 * hash + (this.nearestTraceIndex != null ? this.nearestTraceIndex.hashCode() : 0);
        hash = 59 * hash + (this.headerKeyMap != null ? this.headerKeyMap.hashCode() : 0);
        hash = 59 * hash + (this.verticalKeyName != null ? this.verticalKeyName.hashCode() : 0);
        return hash;
    }

}