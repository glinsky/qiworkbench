/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.model;

import com.bhpb.geographics.util.XStreamUtil;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.thoughtworks.xstream.XStream;
import java.awt.Color;
import java.io.StringReader;
import org.w3c.dom.Node;

public class CursorSettings implements GeoGraphicsPropertyGroup {
    public enum CURSOR_STYLE { CROSSHAIR, LG_CROSS, SM_CROSS, NO_CURSOR}
    
    private boolean showPointer=true;
    private Color color = Color.RED;
    private CURSOR_STYLE cursorStyle = CURSOR_STYLE.CROSSHAIR;

    public CursorSettings(){}

    public CursorSettings(CursorSettings aCursorSettings) {
        this.showPointer = aCursorSettings.showPointer;
        this.color = aCursorSettings.color;
        this.cursorStyle = aCursorSettings.cursorStyle;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public CURSOR_STYLE getCursorStyle() {
        return cursorStyle;
    }
    
    public static String getXmlAlias() {
        return "cursorSettings";
    }

    /**
     * Deserializes a PickingSettings.
     * 
     * @param node w3c.dom.node created from the XML String serialization of PickingSettings
     * 
     * @return the deserialized PickingSettings object
     */
    public static CursorSettings restoreState(Node node) {
        XStream xStream = XStreamUtil.getInstance();
        String xml = ElementAttributeReader.nodeToXMLString(node);

        CursorSettings cursorSettings = (CursorSettings) xStream.fromXML(new StringReader(xml));

        return cursorSettings;
    }
    
    /**
     * Serializes the state of this <code>PickingSettings</code> as a {@link String}
     * representation of an XML element.
     */
    public String saveState() {
        XStream xStream = XStreamUtil.getInstance();
        return xStream.toXML(this);
    }
    
    public void setCursorStyle(CURSOR_STYLE cursorStyle) {
        this.cursorStyle = cursorStyle;
    }

    public boolean isShowPointer() {
        return showPointer;
    }

    public void setShowPointer(boolean showPointer) {
        this.showPointer = showPointer;
    }
    
}