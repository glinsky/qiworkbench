/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import com.bhpb.geographics.ui.layer.dataset.DatasetPropertiesPanel;
import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.ArbTravKeyRange;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoFileType;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.datasystems.UnlimitedKeyRange;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import java.util.ArrayList;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;

/**
 * This class is the controller for the SeismicDatasetPropertisePanel view.
 * It is also an adapter, via the getValueAt and setValueAt methods,
 * to allow the SeismicDatasetProperties class to serve as
 * the model.
 */
public class DatasetPropertiesTableModel extends AbstractTableModel {

    static final long serialVersionUID = -6011716587572401079L;
    protected GeoFileMetadata metadata = null;
    protected ArrayList<String> headerKeys;
    protected ArrayList<AbstractKeyRange> chosenRanges;
    private static final String KEY_HEADER = "Key";
    private static final String FULL_RANGE_HEADER = "FullRange";
    private static final String CHOSEN_RANGE_HEADER = "ChosenRange";
    private static final String ORDER_HEADER = "Order";
    private static final String INCREMENT_HEADER = "Increment";
    private static final String OFFSET_HEADER = "Offset";
    private static final String SYNCHRONIZE_HEADER = "Synchronize";
    private static final String INTERPOLATION_HEADER = "Interpolation";
    private static final String BINSIZE_HEADER = "Binsize";
    /**
     * Headers may be reordered without impacting the model functionality.
     */
    private static final String[] TABLE_COLUMN_HEADERS = {KEY_HEADER, FULL_RANGE_HEADER, CHOSEN_RANGE_HEADER,
        ORDER_HEADER, INCREMENT_HEADER, OFFSET_HEADER, SYNCHRONIZE_HEADER, INTERPOLATION_HEADER, BINSIZE_HEADER
    };
    /**
     * If TABLE_COLUMN_HEADERS are reordered, these flags must also be reorderd.
     * If any value are changes from true to false, edit functionality will be disabled for the column.
     * If any value is changed from false to true, setValueAt must be updated to provide
     * setter functionality.  Otherwise, the user will be permitted to edit the cell but
     * the new value will not persist.
     */
    private final boolean[] COLUMN_IS_EDITABLE = {false, false, true,
        false, false, false, true, true, false
    };
    private static final Logger logger = Logger.getLogger(DatasetPropertiesTableModel.class.getName());
    private GeoDataOrder datasetOrder = GeoDataOrder.UNKNOWN_ORDER;
    private GeoFileType datasetType = GeoFileType.UNKNOWN_GEOFILE;
    private boolean syncVertRange = false;
    private boolean useDeadTraces = false;
    private String fileName = null;
    private DatasetProperties datasetProperties;
    private DatasetPropertiesPanel view;

    public DatasetPropertiesTableModel(DatasetProperties modelProperties,
            DatasetPropertiesPanel view) {
        this.datasetProperties = modelProperties;
        this.view = view;
        metadata = datasetProperties.getMetadata();

        fileName = metadata.getGeoFileName();
        datasetType = metadata.getGeoFileType();
        datasetOrder = metadata.getGeoDataOrder();

        //Note; The last key is the key for the full vertical range
        headerKeys = metadata.getKeys();
        chosenRanges = new ArrayList<AbstractKeyRange>();
        for (String headerKey : headerKeys) {
            if (datasetProperties.isArbTrav(headerKey)) {
                chosenRanges.add(datasetProperties.getKeyChosenArbTrav(headerKey));
            } else {
                chosenRanges.add(datasetProperties.getKeyChosenRange(headerKey));
            }
        }
    }

    public String getFileName() {
        return fileName;
    }

    public DiscreteKeyRange getKeyRange(String key) {
        return metadata.getKeyRange(key);
    }

    public GeoFileType getType() {
        return datasetType;
    }

    public GeoDataOrder getOrder() {
        return datasetOrder;
    }

    public ArrayList<String> getHeaderKeys() {
        return headerKeys;
    }

    public void setUseDeadTraces(boolean useDeadTraces) {
        this.useDeadTraces = useDeadTraces;
    }

    public boolean doUseDeadTraces() {
        return useDeadTraces;
    }

    public boolean doSynchronizeVerticalRange() {
        return syncVertRange;
    }

    public void setSynchronizeVerticalRange(boolean sync) {
        syncVertRange = sync;
    }

    /**
     * Get the number of rows in the Table view of this DatasetProperties.
     *
     * Table view rows correspond to the header keys but for the final key, which
     * is the VerticalRange.  If there is only one header key, it is the VerticalRange
     * and there are no rows in the Table, and if there are no header keys, the Table
     * will also contain no rows.
     */
    public int getRowCount() {
        if (headerKeys.size() == 0) {
            return 0;
        } else {
            return headerKeys.size() - 1;
        }
    }

    public int getColumnCount() {
        return TABLE_COLUMN_HEADERS.length;
    }

    /**
     * Gets the value of the cell at row, column.  Returns a non-null Object
     * unless the one o the following conditions is true, in which case
     * an ArrayIndexOutOfBoundsException is thrown:
     * <ul>
     * <li>row is less than 0 or greater than the number of header keys minus 1 (the VerticalRange)</li>
     * <li>column is less than 0 or greater than or equal to 9, as each header key has exactly 9 viewable properties</li>
     * </ul>
     * 
     */
    public Object getValueAt(int row, int column) {
        if ((row < 0) || (row >= headerKeys.size() - 1)) {
            throw new ArrayIndexOutOfBoundsException("This DatasetProperties has no value in a row < 0 or >= " + (headerKeys.size() - 1));
        }

        Object cellValue = null;

        String keyName = headerKeys.get(row);
        DiscreteKeyRange keyFullRange = metadata.getKeyRange(headerKeys.get(row));
        AbstractKeyRange keyChosenRange = chosenRanges.get(row);

        String columnHeader = TABLE_COLUMN_HEADERS[column];

        if (KEY_HEADER.equals(columnHeader)) {
            cellValue = headerKeys.get(row);
        } else if (FULL_RANGE_HEADER.equals(columnHeader)) {
            cellValue = KeyRangeSerializer.getKeyRangeAsLiteralString(keyFullRange);
        } else if (CHOSEN_RANGE_HEADER.equals(columnHeader)) {
            cellValue = chosenRanges.get(row);
        } else if (ORDER_HEADER.equals(columnHeader)) {
            cellValue = Integer.valueOf(datasetProperties.getKeyOrder(keyName));
        } else if (INCREMENT_HEADER.equals(columnHeader)) {
            cellValue = "?";
            if (keyChosenRange instanceof DiscreteKeyRange) {
                cellValue = BigDecimal.valueOf(((DiscreteKeyRange) keyChosenRange).getIncr()).intValue();
            } else if (keyChosenRange instanceof UnlimitedKeyRange) {
                cellValue = BigDecimal.valueOf(keyFullRange.getIncr()).intValue();
            } else if (keyChosenRange instanceof ArbTravKeyRange) {
                cellValue = "n/a";
            }
        } else if (OFFSET_HEADER.equals(columnHeader)) {
            cellValue = Integer.valueOf(datasetProperties.getKeyOffset(keyName));
        } else if (SYNCHRONIZE_HEADER.equals(columnHeader)) {
            cellValue = datasetProperties.getKeySync(keyName);
        } else if (INTERPOLATION_HEADER.equals(columnHeader)) {
            cellValue = Boolean.valueOf(datasetProperties.getKeyNearest(keyName));
        } else if (BINSIZE_HEADER.equals(columnHeader)) {
            cellValue = datasetProperties.getKeyBinsize(keyName);
        } else {
            logger.warning("Unable to get the cell value at row, column : " + row + ", " + column + " because it does not correspond to a known table SeismicPropertiesTableModel column");
        }

        if (cellValue == null) {
            throw new NullPointerException("DatasetProperties returned a null value for table row " + row + " and column " + column);
        }

        return cellValue;
    }

    @Override
    public String getColumnName(int col) {
        return TABLE_COLUMN_HEADERS[col];
    }

    /**
     * This method must be overridden or else booleans and AbstractKeyRanges
     * would be rendered using the DefaultTableCellRender, which uses toString().
     * The former should be rendered as JCheckBoxes and the latter as specially
     * formatted Strings.
     * 
     * @param c the index of the column for which the data Class will be returned
     * @return the class to be displayed in column #C, used to determine which CellRenderer to use for the column
     */
    @Override
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    public GeoFileMetadata getMetadata() {
        return metadata;
    }

    /**
     * True if and only if the cell's column is editable.  For a given column,
     * all cells are either editable or not editable regardless of row.
     *
     * @param rowIndex row on which the cell is located (0-based)
     * @param columnIndex column on which the cell is located (0-based)
     *
     * @return true if and only if the column is editable
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        //tracl row of transpose model is not editable
        if (getMetadata().isModelFile() && getMetadata().getGeoDataOrder() == GeoDataOrder.MAP_VIEW_ORDER && rowIndex == 0) {
            return false;
        } else {
            return COLUMN_IS_EDITABLE[columnIndex];
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        String columnHeader = TABLE_COLUMN_HEADERS[columnIndex];
        String key = headerKeys.get(rowIndex);

        if (CHOSEN_RANGE_HEADER.equals(columnHeader)) {
            if (aValue instanceof KeyRangeParseResult == false) {
                logger.warning("Cannot set value in chosen range column, it is not a KeyRangeParseResult");
                return;
            }
            KeyRangeParseResult result = (KeyRangeParseResult) aValue;

            String errorMessage = result.getErrorMessage();
            if ("".equals(errorMessage)) {
                AbstractKeyRange keyRange = result.getKeyRange();
                //only the chosenRange local to DatasetPropertieTableModel is set here
                //the ranges are all applied to DatasetPropertiesTableModel as a group
                //using the new applyChosenRanges method when 'ok' is clicked, allowing
                //for atomic validation by the datasetProperties
                if (keyRange instanceof DiscreteKeyRange || keyRange instanceof UnlimitedKeyRange) {
                    chosenRanges.set(rowIndex, keyRange);
                } else if (keyRange instanceof ArbTravKeyRange) {
                    chosenRanges.set(rowIndex, keyRange);
                } else {
                    view.showValidationFailure("Unable to set keyRange: unknown type " + keyRange.getClass().getName());
                }
            } else {
                view.showValidationFailure(errorMessage);
            }
        } else if (INTERPOLATION_HEADER.equals(columnHeader)) {
            boolean useInterpolation = Boolean.parseBoolean(aValue.toString());
            datasetProperties.setKeyNearest(key, useInterpolation);
        } else if (SYNCHRONIZE_HEADER.equals(columnHeader)) {
            boolean useSync = Boolean.parseBoolean(aValue.toString());
            datasetProperties.setKeySync(key, useSync);
        } else {
            logger.warning("Cannot set SeismicPropertiesTableModel(" + rowIndex + ", " + columnIndex + " to " + aValue + " because the cell is not editable");
        }
    }

    public boolean applyChosenRanges() {
        Map<String, AbstractKeyRange> keyChosenRangeMap = new HashMap<String, AbstractKeyRange>();
        //Simple bug fix - the vertical key range is either not configurable (model/horizon xsec)
        //or handled separately by the GUI (seismic xsec or any map dataset).  No need to apply it here.
        int numConfigurableKeysInTable = headerKeys.size() - 1;

        for (int i = 0; i < numConfigurableKeysInTable; i++) {
            keyChosenRangeMap.put(headerKeys.get(i), chosenRanges.get(i));
        }

        try {
            datasetProperties.setKeyChosenRanges(keyChosenRangeMap);
        } catch (IllegalArgumentException iae) {
            logger.warning("Validation failed: dsPropertie.setKeyChosenRanges returned: " + iae.getMessage());
            return false;
        }
        return true;
    }
}