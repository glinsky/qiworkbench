/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.window;

import com.bhpb.geographics.util.XStreamUtil;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.thoughtworks.xstream.XStream;
import java.io.StringReader;
import org.w3c.dom.Node;

public class MapAnnotationProperties extends WindowAnnotationProperties {
    private double hMajorStep = 100.0;
    private double hMinorStep = 10.0;
    private double vMajorStep = 1.0;
    private double vMinorStep = 0.5;
    private LABEL_LOCATION hAxisLocation = LABEL_LOCATION.TOP;
    private LABEL_LOCATION vAxisLocation = LABEL_LOCATION.LEFT;

    public MapAnnotationProperties(MapAnnotationProperties aMapAnnotationProperties) {
        super(aMapAnnotationProperties);

        this.hMajorStep = aMapAnnotationProperties.hMajorStep;
        this.hMinorStep = aMapAnnotationProperties.hMinorStep;
        this.vMajorStep = aMapAnnotationProperties.vMajorStep;
        this.vMinorStep = aMapAnnotationProperties.vMinorStep;
        this.hAxisLocation = aMapAnnotationProperties.hAxisLocation;
        this.vAxisLocation = aMapAnnotationProperties.vAxisLocation;
    }

    /**
     * Constructs a set of AnnotationProperties with default values:
     * no annotated axis chosen, blank labels.
     */
    public MapAnnotationProperties(/*WindowModel windowModel*/) {
        super();
        //super(windowModel.getLayers());
    }
    
    public double getHmajorStep() {
        return hMajorStep;
    }
    
    public double getHminorStep() {
        return hMinorStep;
    }
    
    public double getVmajorStep() {
        return vMajorStep;
    }
    
    public double getVminorStep() {
        return vMinorStep;
    }
    
    public LABEL_LOCATION getHaxisLocation() {
        return hAxisLocation;
    }
    
    public LABEL_LOCATION getVaxisLocation() {
        return vAxisLocation;
    }
    
    public static String getXmlAlias() {
        return "mapAnnotationProperties";
    }
        
    public static MapAnnotationProperties restoreState(Node node) {
        XStream xStream = XStreamUtil.getInstance();
        String xml = ElementAttributeReader.nodeToXMLString(node);

        return (MapAnnotationProperties) xStream.fromXML(new StringReader(xml));
    }
    
    public String saveState() {
        StringBuffer content = new StringBuffer();

        XStream xStream = XStreamUtil.getInstance();
        content.append(xStream.toXML(this));
        content.append("\n");

        return content.toString();
    }
        
    public void setHmajorStep(double hMajorStep) {
        this.hMajorStep = hMajorStep;
    }
    
    public void setHminorStep(double hMinorStep) {
        this.hMinorStep = hMinorStep;
    }
    
    public void setVmajorStep(double vMajorStep) {
        this.vMajorStep = vMajorStep;
    }
    
    public void setVminorStep(double vMinorStep) {
        this.vMinorStep = vMinorStep;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final MapAnnotationProperties other = (MapAnnotationProperties) obj;
        if (this.hMajorStep != other.hMajorStep) {
            return false;
        }
        if (this.hMinorStep != other.hMinorStep) {
            return false;
        }
        if (this.vMajorStep != other.vMajorStep) {
            return false;
        }
        if (this.vMinorStep != other.vMinorStep) {
            return false;
        }
        if (this.hAxisLocation != other.hAxisLocation) {
            return false;
        }
        if (this.vAxisLocation != other.vAxisLocation) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.hMajorStep) ^ (Double.doubleToLongBits(this.hMajorStep) >>> 32));
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.hMinorStep) ^ (Double.doubleToLongBits(this.hMinorStep) >>> 32));
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.vMajorStep) ^ (Double.doubleToLongBits(this.vMajorStep) >>> 32));
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.vMinorStep) ^ (Double.doubleToLongBits(this.vMinorStep) >>> 32));
        hash = 83 * hash + (this.hAxisLocation != null ? this.hAxisLocation.hashCode() : 0);
        hash = 83 * hash + (this.vAxisLocation != null ? this.vAxisLocation.hashCode() : 0);
        return hash;
    }
}