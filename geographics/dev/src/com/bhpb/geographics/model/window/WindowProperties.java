/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.window;

import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.util.ElementAttributeReader;
import java.util.ArrayList;
import java.util.logging.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * The visual properties of a ViewerWindow, including annotations, labels,
 * vertical and horizontal scale and synchronization of scrolling, zooming and
 * other types of events.
 * 
 */
public class WindowProperties implements PropertyAccessor {

    private WindowAnnotationProperties annotationProperties;
    private Logger logger = Logger.getLogger(WindowProperties.class.toString());
    private GeoDataOrder orientation;
    private WindowModel windowModel;
    private WindowScaleProperties windowScaleProperties;
    private WindowSyncGroup windowSyncGroup;
    private WindowSyncProperties syncProperties = new WindowSyncProperties();

    /**
     * Constructs a set of WindowProperties including Annotation, Scale
     * and Synchronization Properties.
     */
    public WindowProperties(GeoDataOrder orientation) {
        this.orientation = orientation;

        switch (orientation) {
            case CROSS_SECTION_ORDER:
                annotationProperties = new CrossSectionAnnotationProperties();
                break;
            case MAP_VIEW_ORDER:
                annotationProperties = new MapAnnotationProperties();
                break;
            default:
                throw new IllegalArgumentException("Cannot create AbstractAnnotationProperties subclass for unknown layer orienation: " + orientation);
        }

        windowScaleProperties = new WindowScaleProperties(orientation);
    }

    public WindowProperties(Node node) {
        orientation = GeoDataOrder.valueOf(ElementAttributeReader.getString(node, "orientation"));
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeName().equals(CrossSectionAnnotationProperties.getXmlAlias())) {
                annotationProperties = new CrossSectionAnnotationProperties(child);
            } else if (child.getNodeName().equals(MapAnnotationProperties.getXmlAlias())) {
                annotationProperties = MapAnnotationProperties.restoreState(child);
            } else if (child.getNodeName().equals(WindowScaleProperties.getXmlAlias())) {
                windowScaleProperties = WindowScaleProperties.restoreState(child);
            } else if (child.getNodeName().equals(WindowSyncProperties.getXmlAlias())) {
                syncProperties = WindowSyncProperties.restoreState(child);
            } else if ("#text".equals(child.getNodeName())) {
                logger.fine("Skipping #text node - this may be whitespace for formatting.");
            } else if ("#comment".equals(child.getNodeName())) {
                logger.info("Found comment within WindowProperties element: " + child.getTextContent());
            } else {
                String errMsg = "Unrecognized child node of " + node.getNodeName() + ": " + child.getNodeName();
                logger.warning(errMsg);
                throw new RuntimeException(errMsg);
            }
        }
    }

    /**
     * Copy constructor used to protect GeoPanel, GeoPlot and Layer state from
     * direct modification by outside sources; e.g. the ControlSessionManager.
     * For efficiency, deep copy will only be implemented for fields which
     * need such protection.  Currently, the only such field is windowScaleProperties.
     * 
     * @param that
     */
    public WindowProperties(WindowProperties that) {
       this.annotationProperties = that.annotationProperties;
       this.orientation = that.orientation;
       this.syncProperties = that.syncProperties;
       this.windowScaleProperties = new WindowScaleProperties(that.windowScaleProperties);
       this.windowSyncGroup = that.windowSyncGroup;
    }

    /**
     * Serializes the state of this <code>WindowProperties</code> as a {@link String}
     * representation of an XML element.
     */
    public String saveState() throws Exception {
        String QUOTE_AND_SPACE = "\" ";
        StringBuffer content = new StringBuffer();
        content.append("<" + getXmlAlias() + " ");
        String currentOp = "saving orientation";
        try {
            content.append("orientation=\"" + orientation.toString() + QUOTE_AND_SPACE + ">\n");

            currentOp = "saving AnnotationProperties";
            content.append(annotationProperties.saveState());

            currentOp = "saving WindowScaleProperties";
            content.append(windowScaleProperties.saveState());

            currentOp = "saving WindowSyncProperties";
            content.append(syncProperties.saveState());
        } catch (Exception ex) {
            logger.warning("While " + currentOp + ", caught: " + ex.getMessage());
            throw ex;
        }
        content.append("</" + getXmlAlias() + ">\n");

        return content.toString();
    }

    public void setAnnotationProperties(WindowAnnotationProperties annotationProperties) {
        this.annotationProperties = annotationProperties;
    }

    /**
     * For xsec layer, the default horizontal annotation is the first key which
     * varies over the dataset.
     * For example, if the full key range of 'ep' is 3015-3016, then entering * or 3015-3016
     * would result in 'ep' becoming the default horizontal annotation key, but entering
     * '3015' or '3015-3015' would result in 'cdp' becoming the annotated key.
     *
     * If all header key ranges have explicit single-value chosen ranges, the distinction becomes
     * meaningless as there is only one annotation tickmark/caption pair.
     *
     * If either header key range is an ArbitraryTraverseKeyRange, then both must be, and the first
     * will be selected as the horizontal annotation.
     *
     * This is a slight bug-fix to the bhp and INT viewer functionality,
     * which can be made to display a single annotation value by entering a range in the form
     * 'VAL-VAL' or [Val1-VAl2] where the first value Val1 Incr is outside of the full key range
     * and therefore VAl2 is irrelevant.
     *
     * @param dsProps
     *
     * @return name of the first key which varies over the dataset
     */
    public static String getNonStaticKey(DatasetProperties dsProps) {
        ArrayList<String> keys = dsProps.getMetadata().getKeys();
        String annotatedKeyName = null;
        String firstKeyName = keys.get(0);
        if (dsProps.isArbTrav(firstKeyName)) {
            annotatedKeyName = keys.get(0);
        } else {
            for (String potentialAnnotatedKey : keys) {
                AbstractKeyRange keyRange = dsProps.getKeyChosenRange(potentialAnnotatedKey);
                DiscreteKeyRange fullRange = keyRange.getFullKeyRange();

                //unlimited KeyRange can be annoated if corresponding full keyrange max > min
                if (keyRange.isUnlimited() && fullRange.getMax() >= (fullRange.getMin() + fullRange.getIncr())) {
                    annotatedKeyName = potentialAnnotatedKey;
                    break;
                }

                //it should not be possible to create a ListKeyRange with 1 value
                if (keyRange.isList()) {
                    annotatedKeyName = potentialAnnotatedKey;
                    break;
                }

                if (keyRange.isDiscrete()) {
                    DiscreteKeyRange asDiscrete = (DiscreteKeyRange) keyRange;
                    if (asDiscrete.getMax() >= (asDiscrete.getMin() + asDiscrete.getIncr())) {
                        annotatedKeyName = potentialAnnotatedKey;
                        break;
                    }
                }
            }
            //if no key with a varying range was found, the first header key will be annotated
            if (annotatedKeyName == null) {
                annotatedKeyName = firstKeyName;
            }
        }
        return annotatedKeyName;
    }

    public void setScaleProperties(WindowScaleProperties scaleProperties) {
        this.windowScaleProperties = new WindowScaleProperties(scaleProperties);
    }

    public void setSyncProperties(WindowSyncProperties syncProperties) {
        this.syncProperties = syncProperties;
    }

    public void setWindowModel(WindowModel windowModel) {
        this.windowModel = windowModel;
        this.annotationProperties.setWindowModel(windowModel);
    }

    public WindowModel getWindowModel() {
        return this.windowModel;
    }

    /**
     * Get the properties related to vertical and horizontal annotations and labels.
     * 
     * @return AnnotationProperties the annotation properties of this Window to
     * which this set of WindowProperties belongs.
     */
    public WindowAnnotationProperties getAnnotationProperties() {
        return annotationProperties;
    }

    public String getProperty(String field) {
        throw new UnsupportedOperationException();
    }

    /**
     * Get the properties related to vertical and horizontal scale.
     * 
     * @return ScaleProperties the scale properties of this Window to
     * which this set of WindowProperties belongs.
     */
    public WindowScaleProperties getScaleProperties() {
        return windowScaleProperties;
    }

    /**
     * Get the properties related to synchronization of various types of events.
     * 
     * @return SyncProperties the synchronization properties of this Window to
     * which this set of WindowProperties belongs.
     */
    public WindowSyncProperties getSyncProperties() {
        return syncProperties;
    }

    public WindowSyncGroup getWindowSyncGroup() {
        return windowSyncGroup;
    }

    public static String getXmlAlias() {
        return "windowProperties";
    }

    public void setProperty(String field, String value) {
        throw new UnsupportedOperationException();
    }
}