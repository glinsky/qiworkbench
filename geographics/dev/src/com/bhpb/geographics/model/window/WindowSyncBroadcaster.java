/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.model.window;

import com.bhpb.geographics.model.ViewTarget;

/**
 * Interface for classes which participate in WindowSyncGroups.
 *
 * Allows geoGraphics classes such as GeoPlot to initiate event broadcasting without
 * needing an explicit reference to the enclosing ViewerWindow implementation, which is the actual sender
 * of the WindowSyncEvent.
 *
 * @author folsw9
 */
public interface WindowSyncBroadcaster {
    public void broadcastCursorPos(ViewTarget viewTargetProps);
    public void broadcastScale(WindowScaleProperties windowScaleProps);

    /**
     * Returns true if broadcast is enabled for the associated ViewerWindow and
     * the ViewerWindow is currently selected.
     * @param viewTargetProps
     */
    public void broadcastScrollPos(ViewTarget viewTargetProps);
    public boolean canBroadcast();
    
    /**
     * Get the broadcaster's ID.  Must be unique among all the broadcasters and listeners
     * of a given group.  Two participants with the same ID would not be able to send
     * and receive events with one another.
     * @return window ID String
     */
    public String getWinID();
}
