/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.window;

import com.bhpb.geographics.model.Layer;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import javax.swing.AbstractListModel;

/**
 * The core model class which has a 1:1 relationship with the ViewerWindow
 * and 1:* with Layer.
 *
 * @author folsw9
 */
public class WindowModel extends AbstractListModel {

    public enum LAYER_TARGET {

        LIST_TOP, LIST_BOTTOM
    }
    private static final Logger logger =
            Logger.getLogger(WindowModel.class.getName());
    private static AtomicInteger GUID = new AtomicInteger(1);
    private final String winID;
    /** the selected layer with the highest Z order */
    private LayerLoader layerLoader = null;
    private Layer activeLayer = null;
    /** the complete list of Layers associated with the WindowModel, including hidden layers */
    private List<Layer> layers = new ArrayList<Layer>();
    /** the currently selected set of layers in decreasing Z order */
    private List<Layer> selectedLayers = new ArrayList<Layer>();
    private WindowProperties winProps;

    /**
     * Constructs a WindowModel of the specified orientation, named using the given
     * prefix, plus the String " Window # ", plus a globally-unique Integer ID.
     *
     * @param prefix Window ID prefix e.g. 'Window #'
     * @param orientation GeoDataOrder.MAP_VIEW_ORDER or CROSS_SECTION_ORDER
     */
    public WindowModel(String prefix, GeoDataOrder orientation) {
        winID = prefix + " Window #" + Integer.valueOf(GUID.getAndAdd(1)).toString();
        this.winProps = new WindowProperties(orientation);
        winProps.setWindowModel(this);
    }

    /**
     * Adds the requested layer, inserting it into the specified position in
     * the WindowModel's list.  See {List.add(index, Object)}.  If this is the first
     * layer added, it is also set as the annotated layer, used to configure the
     * initial window scale, and is selected.
     *
     * @throws IllegalArgumentException if the WindowModel already contains at least
     * one layer with a different GeoDataOrder than the layer to be added or if
     * the WindowModel already contains the layer
     * 
     * @param index position at which to add the Layer
     * @param layer Layer to be added
     */
    public void add(int index, Layer layer) {
        if (layers.size() > 0) {
            GeoDataOrder targetOrientation = layers.get(0).getOrientation();
            GeoDataOrder srcOrientation = layer.getOrientation();

            if (srcOrientation != targetOrientation) {
                throw new IllegalArgumentException("Cannot add layer " + layer.getLayerName()
                        + " because layer is " + srcOrientation + " but window is " + targetOrientation + ".");
            }
        }

        if (layers.contains(layer)) {
            throw new IllegalArgumentException("Cannot add layer "+ layer.getLayerName()
                    + " because it is already contained in this WindowModel's list.");
        }
        
        layers.add(index, layer);

        // If the layer is already set as annotated due to deserialization in progress
        // or is the first layer to be added, make it annotated and update the
        // Window scale.
        if (winProps.getAnnotationProperties().getAnnotatedLayerName().equals(layer.getLayerName())
            ||getSize() == 1) {

            selectedLayers.add(layer);
            winProps.getAnnotationProperties().setAnnotatedLayerName(layer.getLayerName());
            //Bug fix for QIVIEWER-78:
            //Re-instantiation of WindowScaleProperties is not correct as this will lose information
            //contained in the deserialized RasterizerContext, if any.
            //winProps.setScaleProperties(new WindowScaleProperties(layer.getDatasetProperties()));
        }
        
        fireIntervalAdded(this, index, index);
    }

    /**
     * This method should be called after a change to any layer property which may
     * cause a GUI update, for example changing a Layer's 'hidden' field.
     * @param layer
     */
    public void fireLayerChanged(Layer layer) {
        int index = layers.indexOf(layer);
        if (index == -1) {
            return;
        }

        fireContentsChanged(this, index, index);
    }

    /**
     * Gets the Layer with the specified zOrder.  Note Layers are displayed in the associated
     * LayerListPanel in decreasing Z order, starting with getSize()-1 and ending with 0 if no layers
     * have been removed from the WindowModel.
     * 
     * @param zOrder integer ranging from 0 to getSize() -
     *
     * @return the Layer associated with the requested zOrder
     */
    public Layer get(int zOrder) {
        return layers.get(zOrder);
    }

    /**
     * Gets the current active Layer, that is, the Layer in the List of selected Layers
     * {see getSelectedLayers} with the highest Z order.  The activeLayer is reset every time
     * setSelectedLayers() is invoked.
     *
     * @return the active Layer or null if there are no selected Layers
     */
    public Layer getActiveLayer() {
        if (activeLayer != null) {
            return activeLayer;
        }

        if (selectedLayers.size() == 0) {
            return null;
        } else {
            int activeLayerZ = -1;

            Layer nextLayer = null;
            Iterator<Layer> iter = selectedLayers.iterator();
            while (iter.hasNext()) {
                nextLayer = iter.next();
                int nextLayerZ = getZorder(nextLayer);
                if (activeLayer == null || activeLayerZ < nextLayerZ) {
                    activeLayer = nextLayer;
                    activeLayerZ = nextLayerZ;
                }
            }
            return activeLayer;
        }
    }

    /**
     * Gets the annotated Layer.
     * @return the annotated Layer or null if the annotated layer name is
     * WindowAnnotationProperties.NO ANNOTATION
     */
    public Layer getAnnotatedLayer() {
        String annotatedLayerName = winProps.getAnnotationProperties().getAnnotatedLayerName();
        for (Layer layer : layers) {
            if (layer.getLayerName().equals(annotatedLayerName)) {
                return layer;
            }
        }
        return null;
    }

    public Object getElementAt(int index) {
        return layers.get(index);
    }

    public Layer getLayer(int zOrder) {
        if (zOrder < 0 || zOrder >= layers.size()) {
            return null;
        }
        return layers.get(layers.size() - zOrder - 1);
    }

    public Layer getLayer(String layerName) {
        if (layerName == null) {
            return null;
        }
        for (Layer layer : layers) {
            if (layerName.endsWith(layer.getLayerName())) {
                return layer;
            }
        }
        return null;
    }

    /*
     * This method returns an IMMUTABLE copy of this WindowModel's layer list.
     * It cannot be used to add layers to or remove layers
     * from the WindowModel.
     */
    public List<Layer> getLayers() {
        return Collections.unmodifiableList(layers);
    }

    /**
     * Gets an IMMUTABLE copy of this WindowModel's selected layer list.
     * It cannot be used to set or change the selected layers.
     *
     * @return unmodifiable List of Layers
     */
    public List<Layer> getSelectedLayers() {
        return Collections.unmodifiableList(selectedLayers);
    }

    public int getSize() {
        return layers.size();
    }

    public String getWindowID() {
        return winID;
    }

    public WindowProperties getWindowProperties() {
        return winProps;
    }

    /**
     * Returns the 'z' order of the indicated layer, where the last layer in the
     * list has a z order of 0 and the first layer has a z order of getSize() -1.
     *
     * If the layer does not exist in this WindowModel, then a value of -1 is returned.
     *
     * @param layer
     *
     * @return zOrder of the requested Layer or -1
     */
    public int getZorder(Layer layer) {
        if (layers.contains(layer)) {
            return layers.size() - layers.indexOf(layer) - 1;
        } else {
            return -1;
        }
    }

    public boolean isSelected(Layer layer) {
        return selectedLayers.contains(layer);
    }

    public Iterator<Layer> iterator() {
        return layers.iterator();
    }

    /**
     * Begins layer loading process and immediately returns.
     * 
     * @param pathName
     * @param index
     */
    public void loadLayer(final String pathName, final int index) {
        if (layerLoader == null) {
            return;
        }
        new Thread(new Runnable() {
            public void run() {
                layerLoader.loadLayer(pathName, index);
            }
        }).start();
    }

    public void setLayerLoader(LayerLoader layerLoader) {
        this.layerLoader = layerLoader;
    }

    public void setSelectedLayers(List<Layer> newSelectedLayers) {
        selectedLayers = new ArrayList(newSelectedLayers);
        activeLayer = null;
        fireContentsChanged(logger, 0, getSize() - 1);
    }

    public void setWindowProperties(WindowProperties winProps) {
        this.winProps = winProps;
        winProps.setWindowModel(this);
    }

    public void moveToIndex(Layer layer, int targetIndex) {
        int sourceIndex = layers.indexOf(layer);
        if (targetIndex == sourceIndex) {
            logger.warning("DnD targetIndex == sourceIndex, no action");
            return;
        }
        layers.add(targetIndex, layer);
        if (sourceIndex < targetIndex) {
            layers.remove(sourceIndex);
        } else {
            layers.remove(sourceIndex + 1);
        }
    }

    public void moveLayer(Layer layer, int positionChange) {
        if (positionChange == 0) {
            return; // move of 0 positions requested, do nothing

        }

        int index = layers.indexOf(layer);
        if (index == -1) {
            throw new IllegalArgumentException("Cannot move layer because it is not in this LayerListPanel's LayerList's ListModel.");
        }
        int finalPos = index + positionChange;
        if (finalPos < 0) {
            throw new IllegalArgumentException("Cannot move layer at position " + index + " by " + positionChange + " because the resulting position is < 0.");
        } else if (finalPos >= layers.size()) {
            throw new IllegalArgumentException("Cannot move layer at position " + index + " by " + positionChange + " because the resulting position is > size of ListModel.");
        }

        if (finalPos + 1 == layers.size()) {
            layers.add(layer);
        } else {
            layers.add(finalPos + 1, layer);
        }
        layers.remove(index);

        fireContentsChanged(this, 0, layers.size() - 1);
    }

    public void moveLayer(Layer layer, LAYER_TARGET target) {
        moveLayers(target, new int[]{layers.indexOf(layer)});
    }

    public void moveLayers(LAYER_TARGET target, int[] layerIndices) {
        int targetIndex;
        if (target == LAYER_TARGET.LIST_TOP) {
            targetIndex = 0;
        } else if (target == LAYER_TARGET.LIST_BOTTOM) {
            targetIndex = layers.size();
        } else {
            logger.warning("Unknown MoveLayerAction target: " + target);
            return;
        }
        //add the selectd indices in reverse order because they are successively inserted at the same location
        int nElementsAdded = 0;
        for (int indexIndex = layerIndices.length - 1; indexIndex >= 0; indexIndex--) {
            int srcIndex = layerIndices[indexIndex];
            //if elements are being inserted at the top, increment the src index by one for each new element added
            if (target == LAYER_TARGET.LIST_TOP) {
                srcIndex += nElementsAdded;
            }
            layers.add(targetIndex, layers.get(srcIndex));
            nElementsAdded++;
        }
        for (int indexIndex = 0; indexIndex < layerIndices.length; indexIndex++) {
            //every time a source element is removed, the index every remaining source element is reduced by one
            int offset = 0;
            //if the layers were copied to the top of the list, the indices of the original layers
            //to remove have increased by the total number of layers copied
            if (target == LAYER_TARGET.LIST_TOP) {
                offset = nElementsAdded;
            }
            layers.remove(offset + layerIndices[indexIndex] - indexIndex);
        }
    }

    /**
     * If this WindowModel contains the specified layer, it is removed from the WindowModel
     * and fireChangeEvent is invoked.
     * @param layer
     */
    public void remove(Layer layer) {
        int index = layers.indexOf(layer);
        if (index == -1) {
            return;
        }

        layers.remove(layer);
        selectedLayers.remove(layer);

        fireIntervalRemoved(this, index, index);
    }

    public double getVerticalOffset(Layer layer) {
        if (!layers.contains(layer)) {
            throw new IllegalArgumentException("Specified layer "
                    + layer.getLayerName() + " is not contained by WindowModel "
                    + getWindowID());
        }

        double minVertCoord = layer.getVerticalMin();
        double verticalOffset = 0.0;
        Iterator<Layer> iter = layers.iterator();
        while (iter.hasNext()) {
            Layer nextLayer = iter.next();
            verticalOffset = Math.max(verticalOffset, minVertCoord - nextLayer.getVerticalMin());
        }
        return verticalOffset;
    }
}