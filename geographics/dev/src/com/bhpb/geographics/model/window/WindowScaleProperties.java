/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.window;

import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.rasterizer.RasterizerContext;
import com.bhpb.geographics.util.ScaleUtil;
import com.bhpb.geographics.util.XStreamUtil;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.thoughtworks.xstream.XStream;
import java.awt.Rectangle;
import java.io.StringReader;
import java.util.logging.Logger;
import org.w3c.dom.Node;

public class WindowScaleProperties implements WindowPropertyGroup {

    public enum UNITS {

        TRACES_PER_CM, CMS_PER_UNIT
    };
    private static final Logger logger =
            Logger.getLogger(WindowScaleProperties.class.getName());
    //This parameter is used to limit the maximum amount of zoom-In (traces/cm)
    //or zoom-out (cms/unit) to limit the impact of double-precision errors, 
    //and so that the zoom scale can
    //be distinguished in the GUI, which permits only 3 decimal places
    private static final double MIN_USERDEF_SCALE = 0.001;
    private static final double MIN_PREZOOM_SCALE = MIN_USERDEF_SCALE * 2;
    
    private boolean settingAspectRatioLocked = false;
    private boolean aspectRatioLocked = false;
    private double horizontalScale;
    private double verticalScale;
    private RasterizerContext rasterizerContext = null;
    private UNITS horizontalScaleUnits = UNITS.TRACES_PER_CM;
    private UNITS verticalScaleUnits;

    //not static or final so that windows can be scaled for non-standard (!= 98 dpi) displays
    private transient ScaleUtil scaleUtil = new ScaleUtil();
    
    public WindowScaleProperties(WindowScaleProperties aWindowScaleProperties) {
        this.settingAspectRatioLocked = aWindowScaleProperties.settingAspectRatioLocked;
        this.aspectRatioLocked = aWindowScaleProperties.aspectRatioLocked;
        this.horizontalScale = aWindowScaleProperties.horizontalScale;
        this.verticalScale = aWindowScaleProperties.verticalScale;
        this.horizontalScaleUnits = aWindowScaleProperties.horizontalScaleUnits;
        this.verticalScaleUnits = aWindowScaleProperties.verticalScaleUnits;
        if (aWindowScaleProperties.rasterizerContext != null) {
            this.rasterizerContext = new RasterizerContext(aWindowScaleProperties.rasterizerContext);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WindowScaleProperties other = (WindowScaleProperties) obj;
        if (this.settingAspectRatioLocked != other.settingAspectRatioLocked) {
            return false;
        }
        if (this.aspectRatioLocked != other.aspectRatioLocked) {
            return false;
        }
        if (this.horizontalScale != other.horizontalScale) {
            return false;
        }
        if (this.verticalScale != other.verticalScale) {
            return false;
        }
        if (this.horizontalScaleUnits != other.horizontalScaleUnits) {
            return false;
        }
        if (this.verticalScaleUnits != other.verticalScaleUnits) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.settingAspectRatioLocked ? 1 : 0);
        hash = 37 * hash + (this.aspectRatioLocked ? 1 : 0);
        hash = 37 * hash + (int) (Double.doubleToLongBits(this.horizontalScale) ^ (Double.doubleToLongBits(this.horizontalScale) >>> 32));
        hash = 37 * hash + (int) (Double.doubleToLongBits(this.verticalScale) ^ (Double.doubleToLongBits(this.verticalScale) >>> 32));
        hash = 37 * hash + (this.horizontalScaleUnits != null ? this.horizontalScaleUnits.hashCode() : 0);
        hash = 37 * hash + (this.verticalScaleUnits != null ? this.verticalScaleUnits.hashCode() : 0);
        return hash;
    }

    /**
     * Package-protected method for testing on non-98-dpi display e.g. vncserver
     */
    WindowScaleProperties(DatasetProperties dsProps, ScaleUtil scaleUtil) {
        this.scaleUtil = scaleUtil;
        setScaleDefaults(dsProps);
    }

    /**
     * Constructs a ScaleProperties with appropriate vertical scale default value
     * depending on whether the dsProps parameter is a Model, Seismic or Horizon DatasetProperties
     * and whether the GeoDataOrder is CROSS_SECTION_ORDER or MAP_VIEW_ORDER.
     * 
     * @param dsProps the DatasetProperties; used to determine whether to use the
     * model or seismic data vertical scale default.
     */
    public WindowScaleProperties(DatasetProperties dsProps) {
        setScaleDefaults(dsProps);
    }

    /**
     * Constructs a ScaleProperties with the appropriate scale defaults for seismic
     * datasets.  The vertical units are determined based on the specified GeoDataOrder.
     * 
     * @param order
     */
    public WindowScaleProperties(GeoDataOrder order) {
        switch (order) {
            case CROSS_SECTION_ORDER:
                verticalScaleUnits = UNITS.CMS_PER_UNIT;
                horizontalScale = scaleUtil.getModelOrSeismicTraceScaleDefault();
                verticalScale = scaleUtil.getSeismicUnitScaleDefault();
                break;
            case MAP_VIEW_ORDER:
                verticalScaleUnits = UNITS.TRACES_PER_CM;
                horizontalScale = scaleUtil.getModelOrSeismicTraceScaleDefault();
                verticalScale = scaleUtil.getModelOrSeismicTraceScaleDefault();
                break;
        }
    }

    private void setScaleDefaults(DatasetProperties dsProps) {
        GeoDataOrder order = dsProps.getMetadata().getGeoDataOrder();

        switch (order) {
            case CROSS_SECTION_ORDER:
                verticalScaleUnits = UNITS.CMS_PER_UNIT;
                if (dsProps.isModel()) {
                    horizontalScale = scaleUtil.getModelOrSeismicTraceScaleDefault();
                    verticalScale = scaleUtil.getModelUnitScaleDefault();
                } else if (dsProps.isHorizon()) {
                    horizontalScale = scaleUtil.getMapTraceScaleDefault();
                    verticalScale = scaleUtil.getHorizonUnitScaleDefault();
                } else if (dsProps.isSeismic()) {
                    horizontalScale = scaleUtil.getModelOrSeismicTraceScaleDefault();
                    verticalScale = scaleUtil.getSeismicUnitScaleDefault();
                }
                break;
            case MAP_VIEW_ORDER:
                verticalScaleUnits = UNITS.TRACES_PER_CM;
                if (dsProps.isModel()) {
                    horizontalScale = scaleUtil.getMapTraceScaleDefault();
                    verticalScale = scaleUtil.getMapTraceScaleDefault();
                } else if (dsProps.isHorizon()) {
                    horizontalScale = scaleUtil.getMapTraceScaleDefault();
                    verticalScale = scaleUtil.getMapTraceScaleDefault();
                } else if (dsProps.isSeismic()) {
                    horizontalScale = scaleUtil.getModelOrSeismicTraceScaleDefault();
                    verticalScale = scaleUtil.getModelOrSeismicTraceScaleDefault();
                }
                break;
            default:
                throw new IllegalArgumentException("Cannot create ScaleProperties for unknown GeoDataOrder: " + order);
        }
    }

    private void setScaleUtil(ScaleUtil scaleUtil) {
        this.scaleUtil = scaleUtil;
    }

    /**
     * Returns true if and only is locking the aspect ratio is enabled.
     * 
     * @return boolean false by default
     */
    public boolean isSettingAspectRatioLockEnabled() {
        return settingAspectRatioLocked;
    }

    /**
     * Returns true if and only if the aspect ratio is locked.
     * 
     * @return boolean false by default
     */
    public boolean isAspectRatioLocked() {
        return aspectRatioLocked;
    }

    /**
     * Gets the horizontal scale coefficent.  If an annotated Layer has been set,
     * then default or input scale coefficient may have been multiplied by
     * a scaling factor as the result of zooming operations.
     * 
     * @return the horizontal scale: 19.291 traces/cm by default.
     */
    public double getHorizontalScale() {
        UNITS units = getHorizontalScaleUnits();

        double scaleX;
        if (rasterizerContext == null) {
            scaleX = 1.0;
        } else {
            scaleX = rasterizerContext.getScaleX();
        }

        switch (units) {
            case CMS_PER_UNIT:
                //return scaleUtil.getPixelsPerUnit(getHorizontalScale()/);
                throw new UnsupportedOperationException("Cannot get horizontal pixel incr for CMS_PER_UNIT: not valid horizontal units.");
            case TRACES_PER_CM:
                return horizontalScale / scaleX;
            default:
                throw new UnsupportedOperationException("Cannot get horizontal pixel scale for unknown units: " + units);
        }
    }

    /**
     * Gets the units of the horizontal scale.
     * 
     * @return the horizontal scale unit: TRACES_PER_CM by default
     */
    public UNITS getHorizontalScaleUnits() {
        return horizontalScaleUnits;
    }

    public double getHorizontalPixelIncr() {
        UNITS units = getHorizontalScaleUnits();

        switch (units) {
            case CMS_PER_UNIT:
                //return scaleUtil.getPixelsPerUnit(getHorizontalScale()/);
                throw new UnsupportedOperationException("Cannot get horizontal pixel incr for CMS_PER_UNIT: not valid horizontal units.");
            case TRACES_PER_CM:
                return scaleUtil.getPixelsPerTrace(getHorizontalScale());
            default:
                throw new UnsupportedOperationException("Cannot get horizontal pixel incr for unknown units: " + units);
        }
    }

    public double getPixelsPerUnit() {
        return scaleUtil.getPixelsPerUnit(getVerticalScale());
    }

    public double getPixelsPerTrace() {
        return scaleUtil.getPixelsPerTrace(getHorizontalScale());
    }

    /**
     * Gets the vertical scale coefficient.  If the annotated Layer has been set,
     * then the default or input vertical scale may have been multiplied by
     * a scaling factor as the result of zooming operations.
     * 
     * @return 0.00216cm/unit by default.
     */
    public double getVerticalScale() {
        UNITS units = getVerticalScaleUnits();

        double scaleY;
        if (rasterizerContext == null) {
            scaleY = 1.0;
        } else {
            scaleY = rasterizerContext.getScaleY();
        }

        switch (units) {
            case CMS_PER_UNIT:
                return verticalScale * scaleY;
            case TRACES_PER_CM:
                return verticalScale / scaleY;
            default:
                throw new UnsupportedOperationException("Cannot get vertical scale for unknown units: " + units);
        }
    }

    /**
     * Gets the double-precision number of pixels allocated to a single vertical
     * data unit, whether this is a trace (map view) or sample unit (cross-section)
     * 
     * @return pixels corresponding to one vertical unit according to the verticalscale
     * and display DPI
     */
    public double getVerticalPixelIncr() {
        UNITS units = getVerticalScaleUnits();

        switch (units) {
            case CMS_PER_UNIT:
                return scaleUtil.getPixelsPerUnit(getVerticalScale());
            case TRACES_PER_CM:
                return scaleUtil.getPixelsPerTrace(getVerticalScale());
            default:
                throw new UnsupportedOperationException("Cannot get horizontal pixel incr for nknown units: " + units);
        }
    }

    /**
     * Gets the units of the vertical scale.
     * 
     * @return CMS_PER_UNIT by default.
     */
    public UNITS getVerticalScaleUnits() {
        return verticalScaleUnits;
    }

    public static String getXmlAlias() {
        return "windowScaleProperties";
    }

    public static WindowScaleProperties restoreState(Node node) {
        XStream xStream = XStreamUtil.getInstance();
        String xml = ElementAttributeReader.nodeToXMLString(node);
        WindowScaleProperties wsProps = (WindowScaleProperties) xStream.fromXML(new StringReader(xml));
        wsProps.setScaleUtil(new ScaleUtil());
        return wsProps;
    }

    public String saveState() {
        StringBuffer content = new StringBuffer();

        XStream xStream = XStreamUtil.getInstance();
        content.append(xStream.toXML(this));
        content.append("\n");

        return content.toString();
    }

    public void setAnnotatedLayer(Layer newAnnotatedLayer) {
        if (rasterizerContext != null) {
            rasterizerContext.setReferenceLayer(newAnnotatedLayer);
        }
    }

    /**
     * Sets whether the aspect ratio of the ViewerWindow with which this
     * ScaleProperties is associated is locked.
     * 
     * @param locked boolean
     */
    public void setAspectRatioLocked(boolean locked) {
        this.aspectRatioLocked = locked;
    }

    /**
     * Sets whether locking/unlocking of the ViewerWindow aspect ratio
     * is enabled.
     * 
     * @param locked boolean
     */
    public void setAspectRatioLockingEnabled(boolean locked) {
        this.settingAspectRatioLocked = locked;
    }

    /**
     * Sets the horizontal scale coefficient.
     * @throws IllegalArgumentException if horizontalScale <= 0.0
     * 
     * @param horizontalScale double > 0.0
     */
    public void setHorizontalScale(double horizontalScale) {
        if (horizontalScale <= 0.0) {
            throw new IllegalArgumentException("Horizontal scale must be > 0.0");
        }
        UNITS units = getHorizontalScaleUnits();

        if (verticalScale < MIN_USERDEF_SCALE) {
            logger.info("Unable to set scale to: " + verticalScale + "; minimum allowed scale is: " + MIN_USERDEF_SCALE);
            return;
        }

        switch (units) {
            case CMS_PER_UNIT:
                throw new UnsupportedOperationException("Cannot set horizontal scale for CMS_PER_UNIT: not valid horizontal units.");
            case TRACES_PER_CM:
                if (rasterizerContext != null) {
                    rasterizerContext.setScaleX(1.0);
                }
                this.horizontalScale = horizontalScale;
                break;
            default:
                throw new UnsupportedOperationException("Cannot set horizontal scale for unknown units: " + units);
        }
    }

    public void setHorizontalPixelIncr(double newHorizontalPixelIncr) {
        setHorizontalScale(horizontalScale * getHorizontalPixelIncr() / newHorizontalPixelIncr);
    }

    public void setVerticalPixelIncr(double newVerticalPixelIncr) {
        switch (verticalScaleUnits) {
            case CMS_PER_UNIT:
                setVerticalScale(verticalScale * newVerticalPixelIncr / getVerticalPixelIncr());
                return;
            case TRACES_PER_CM:
                setVerticalScale(verticalScale * getVerticalPixelIncr() / newVerticalPixelIncr);
                return;
            default:
                throw new UnsupportedOperationException("Cannot set vertical pixel incr: unknown units: " + verticalScaleUnits);
        }
    }

    /**
     * Sets the vertical scale coefficient.
     * 
     * @throws IllegalArgumentException if verticalScale <= 0.0
     * 
     * @param verticalScale the vertical scale, must be greater than 0.0
     */
    public void setVerticalScale(double verticalScale) {
        if (verticalScale <= 0.0) {
            throw new IllegalArgumentException("Vertical scale must be > 0.0");
        }
        UNITS units = getVerticalScaleUnits();

        if (verticalScale < MIN_USERDEF_SCALE) {
            logger.info("Unable to set scale to: " + verticalScale + "; minimum allowed scale is: " + MIN_USERDEF_SCALE);
            return;
        }
        
        switch (units) {
            case CMS_PER_UNIT:
                if (rasterizerContext != null) {
                    rasterizerContext.setScaleY(1.0);
                }
                this.verticalScale = verticalScale;
                break;
            case TRACES_PER_CM:
                if (rasterizerContext != null) {
                    rasterizerContext.setScaleY(1.0);
                }
                this.verticalScale = verticalScale;
                break;
            default:
                throw new UnsupportedOperationException("Cannot set vertical scale incr for unknown units: " + units);
        }
    }

    /**
     * Resets the rasterizerContext's zoom levels and updates the initial width and height
     * of the to match the requested annotatedLayer at the current scale using 1:1 zoom.
     * If the annotatedLayer is null, the initial height and width are unchanged, but the zoom levels
     * are reset.
     *
     * @param referenceLayer the layer which will be used as the reference for zoomAll operations
     */
    public void updateReferenceLayer(Layer referenceLayer) {
        rasterizerContext = new RasterizerContext(referenceLayer,
                referenceLayer.getWidth() * getHorizontalPixelIncr(), referenceLayer.getHeight() * getVerticalPixelIncr());
    }

    public void zoomAll(int width, int height) {
        if (rasterizerContext == null || rasterizerContext.getReferenceLayer() == null) {
            logger.warning("Unable to zoomAll.  RasterizerContext is not yet valid or no annotated Layer exists");
        } else {
            rasterizerContext.resetZoomLevels(width, height);
            setScaleDefaults(rasterizerContext.getReferenceLayer().getDatasetProperties());
            rasterizerContext.zoomAll(aspectRatioLocked, getHorizontalPixelIncr(), getVerticalPixelIncr());
        }
    }

    public void zoomIn() {
        if (rasterizerContext == null) {
            logger.warning("Unable to zoomIn.  RasterizerContext is not yet valid.");
        } else {
            double hScale = getHorizontalScale();
            if (getHorizontalScaleUnits() == UNITS.TRACES_PER_CM && hScale < MIN_PREZOOM_SCALE) {
                logger.info("Unable to zoomIn: horizontal scale is already at minimum: " + hScale + " Traces/CM");
            } else {
                rasterizerContext.zoomIn();
            }
        }
    }

    public void zoomOut() {
        if (rasterizerContext == null) {
            logger.warning("Unable to zoomOut.  RasterizerContext is not yet valid.");
        } else {
            double vScale = getVerticalScale();
             if (getVerticalScaleUnits() == UNITS.CMS_PER_UNIT && vScale < MIN_PREZOOM_SCALE) {
                logger.info("Unable to zoomIn: vertical scale is already at minimum: " + vScale + " CM/Unit");
            } else {
                rasterizerContext.zoomOut();
            }
        }
    }

    public void zoomRect(Rectangle selectedRect) {
        if (rasterizerContext == null) {
            logger.warning("Unable to zoomAll.  RasterizerContext is not yet valid.");
        } else {
            rasterizerContext.zoomRect(selectedRect, aspectRatioLocked);
        }
    }

    public void setVisibleRectangle(Rectangle visRect, Layer referenceLayer) {
        if (visRect.width == 0) {
            logger.warning("Unable to set WindowScaleProperties' visible rectangle: width may not be 0.");
            return;
        }

        if (visRect.height == 0) {
            logger.warning("Unable to set WindowScaleProperties' visible rectangle: height may not be 0.");
            return;
        }

        if (rasterizerContext == null) {
            rasterizerContext = new RasterizerContext(
                    referenceLayer,
                    visRect.width,
                    visRect.height);
            rasterizerContext.setOffsetX(visRect.x);
            rasterizerContext.setOffsetY(visRect.y);
        } else {
            rasterizerContext.setOffsetX(visRect.x);
            rasterizerContext.setOffsetY(visRect.y);
            rasterizerContext.setSize(visRect.getSize());
        }
    }
}