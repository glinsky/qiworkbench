/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.window;

public class AnnotationProperties {
    public enum LABEL_FLAG { ALL, LE_LIMIT, GE_LIMIT }

    private boolean showLines = false;
    private double angle = 0;               //default angle 0 degrees
    private double labelLimit = 0.0;
    private double minLabelSpace = 1.56;    //default spacing approx. 50 pixels
    private LABEL_FLAG labelFlag = LABEL_FLAG.ALL;
    private String keyName;
    private String layerName;

    public AnnotationProperties(String layerName, String keyName) {
        this.keyName = keyName;
        this.layerName = layerName;
    }

    public AnnotationProperties(AnnotationProperties that) {
        this.showLines = that.showLines;
        this.angle = that.angle;
        this.labelLimit = that.labelLimit;
        this.minLabelSpace = that.minLabelSpace;
        this.labelFlag = that.labelFlag;
        this.keyName = that.keyName;
        this.layerName = that.layerName;
    }

    public double getAngle() {
        return angle;
    }

    public String getKeyName() {
        return keyName;
    }

    public String getLayerName() {
        return layerName;
    }
    
    public void setAngle(double angle) {
        this.angle = angle;
    }

    public LABEL_FLAG getLabelFlag() {
        return labelFlag;
    }

    public void setLabelFlag(LABEL_FLAG labelFlag) {
        this.labelFlag = labelFlag;
    }

    public double getLabelLimit() {
        return labelLimit;
    }

    public void setLabelLimit(double labelLimit) {
        this.labelLimit = labelLimit;
    }

    public double getMinLabelSpace() {
        return minLabelSpace;
    }

    public void setMinLabelSpace(double minLabelSpace) {
        this.minLabelSpace = minLabelSpace;
    }

    public boolean isShowLines() {
        return showLines;
    }

    public void setShowLines(boolean showLines) {
        this.showLines = showLines;
    }

}