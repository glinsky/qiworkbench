/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.window;

import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.util.XStreamUtil;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.bhpb.qiworkbench.compAPI.XmlUtils;
import com.thoughtworks.xstream.XStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;
import org.w3c.dom.Node;

/**
 * The visual properties of a ViewerWindow related to vertical and horizontal
 * annotations, labels and axis ticks.
 * 
 * @author Woody folsom
 * @version 1.0
 */
public class CrossSectionAnnotationProperties extends WindowAnnotationProperties {

    private static final Logger logger =
            Logger.getLogger(CrossSectionAnnotationProperties.class.getName());

    public enum STEP_TYPE {

        NONE, AUTOMATIC, USER_DEFINED
    }

    //leave package protected for unit tests
    static final double MAJOR_STEP_DEFAULT = 1.0;
    static final double MINOR_STEP_DEFAULT = 0.5;
    private static final String MAJOR_STEP_ATTR_NAME = "majorStep";
    private static final String MINOR_STEP_ATTR_NAME = "minorStep";
    private static final String STEP_TYPE_ATTR_NAME = "stepType";
    private static final String VERT_ANNOT_ATTR_NAME = "verticalAnnotationLocation";
    private static final String SYNC_KEY_NAME = "synchronizationKey";
    private boolean annotationEnabled = false;
    private double majorStep = MAJOR_STEP_DEFAULT;
    private double minorStep = MINOR_STEP_DEFAULT;
    private List<String> availableKeys = new ArrayList<String>();
    private List<String> selectedKeys = new ArrayList<String>();
    private Map<String, Map<String, AnnotationProperties>> annotationPropertyMap = new HashMap<String, Map<String, AnnotationProperties>>();
    private Map<String, List<String>> selectedKeyMap = new HashMap<String, List<String>>();
    private LABEL_LOCATION horizontalAnnotationLocation = LABEL_LOCATION.TOP;
    private LABEL_LOCATION verticalAnnotationLocation = LABEL_LOCATION.LEFT;
    private STEP_TYPE stepType = STEP_TYPE.AUTOMATIC;
    private String synchronizationKey = "";

    public CrossSectionAnnotationProperties(CrossSectionAnnotationProperties aCrossSectionAnnotationProperties) {
        super(aCrossSectionAnnotationProperties);

        this.annotationEnabled = aCrossSectionAnnotationProperties.annotationEnabled;
        this.majorStep = aCrossSectionAnnotationProperties.majorStep;
        this.minorStep = aCrossSectionAnnotationProperties.minorStep;

        this.availableKeys = new ArrayList<String>(aCrossSectionAnnotationProperties.availableKeys);
        this.selectedKeys = new ArrayList<String>(aCrossSectionAnnotationProperties.selectedKeys);

        this.selectedKeyMap = deepCopyMap(aCrossSectionAnnotationProperties.selectedKeyMap);

        this.horizontalAnnotationLocation = aCrossSectionAnnotationProperties.horizontalAnnotationLocation;
        this.verticalAnnotationLocation = aCrossSectionAnnotationProperties.verticalAnnotationLocation;
        this.stepType = aCrossSectionAnnotationProperties.stepType;
        this.synchronizationKey = aCrossSectionAnnotationProperties.synchronizationKey;
        this.annotationPropertyMap = deepCopyMapMap(aCrossSectionAnnotationProperties.annotationPropertyMap);
    }

    private Map<String, Map<String, AnnotationProperties>> deepCopyMapMap(Map<String, Map<String, AnnotationProperties>> aMap) {
        Map<String, Map<String, AnnotationProperties>> newMapMap = new HashMap<String, Map<String, AnnotationProperties>>();
        for (Entry<String, Map<String, AnnotationProperties>> aMapMapEntry : aMap.entrySet()) {
            Map<String, AnnotationProperties> newMap = new HashMap<String, AnnotationProperties>();
            for (Entry<String, AnnotationProperties> aMapEntry : aMapMapEntry.getValue().entrySet()) {
                newMap.put(aMapEntry.getKey(), new AnnotationProperties(aMapEntry.getValue()));
            }
            newMapMap.put(aMapMapEntry.getKey(), newMap);
        }
        return newMapMap;
    }

    private Map<String, List<String>> deepCopyMap(Map<String, List<String>> aMap) {
        Map<String, List<String>> newMap = new HashMap<String, List<String>>();
        for (Entry<String, List<String>> aMapEntry : aMap.entrySet()) {
            newMap.put(aMapEntry.getKey(), aMapEntry.getValue());
        }
        return newMap;
    }

    public CrossSectionAnnotationProperties(Node node) {
        super(node);
        XStream xStream = XStreamUtil.getInstance();
        
        verticalAnnotationLocation = LABEL_LOCATION.valueOf(ElementAttributeReader.getString(node, VERT_ANNOT_ATTR_NAME));
        stepType = STEP_TYPE.valueOf(ElementAttributeReader.getString(node, STEP_TYPE_ATTR_NAME));
        majorStep = Double.valueOf(ElementAttributeReader.getString(node, MAJOR_STEP_ATTR_NAME));
        minorStep = Double.valueOf(ElementAttributeReader.getString(node, MINOR_STEP_ATTR_NAME));

        //this node must contain exactly 2 children which deserializes to form a list
        //First, get the availableKeys
        Node child = ElementAttributeReader.getChildNode(node, "availableKeys");
        if (child == null) {
            throw new IllegalArgumentException("Node CrossSectionAnnotationProperties must contain a child node named 'availableKeys'");
        }
        Object deserializedGrandChild = xStream.fromXML(ElementAttributeReader.nodeToXMLString(XmlUtils.getNonTextChild(child)));

        if (deserializedGrandChild instanceof List) {
            availableKeys = (List) deserializedGrandChild;
        } else {
            throw new IllegalArgumentException("Child of child node availableKeys did not deserialize to a List, but : " + deserializedGrandChild.getClass().getName());
        }

        //now get the selectedKeys
        child = ElementAttributeReader.getChildNode(node, "selectedKeys");
        if (child == null) {
            throw new IllegalArgumentException("Node CrossSectionAnnotationProperties must contain a child node named 'selectedKeys'");
        }
        deserializedGrandChild = xStream.fromXML(ElementAttributeReader.nodeToXMLString(XmlUtils.getNonTextChild(child)));

        if (deserializedGrandChild instanceof List) {
            selectedKeys = (List) deserializedGrandChild;
        } else {
            throw new IllegalArgumentException("Child of child node selectdKeys did not deserialize to a List, but : " + deserializedGrandChild.getClass().getName());
        }

        //bugfix: if there are no selected keys, there can be no synchronized key
        if (selectedKeys.size() > 0) {
            String syncKey = ElementAttributeReader.getString(node, SYNC_KEY_NAME);
            if (syncKey != null) {
                setSynchronizationKey(syncKey);
            } else {
                if (selectedKeys.size() > 0) {
                    setSynchronizationKey(selectedKeys.get(0));
                }
            }
        }

        String annotatedLayerName = getAnnotatedLayerName();
        if (!WindowAnnotationProperties.NO_ANNOTATION.equals(annotatedLayerName)) {
            child = ElementAttributeReader.getChildNode(node, "annotationProperties");
            Map<String, AnnotationProperties> restoredAnnotatedLayerMap = (Map<String, AnnotationProperties>) xStream.fromXML(ElementAttributeReader.nodeToXMLString(XmlUtils.getNonTextChild(child)));
            annotationPropertyMap = new HashMap<String, Map<String, AnnotationProperties>>();
            annotationPropertyMap.put(annotatedLayerName, restoredAnnotatedLayerMap);
        }
    }

    /**
     * Constructs a set of AnnotationProperties with default values:
     * no annotated axis chosen, blank labels.
     */
    public CrossSectionAnnotationProperties() {
        super();
    }

    /**
     * Invokes super.addLayer() and also adds the new layer's keys to the map
     * of available keys by layer name.
     *
     * Bug fix: as each new layer is added, its first non-static header key (min != max)
     * is made the selected horizontal annotation.
     * 
     * @param layer new Layer to be added to the main AbstractAnnotationProperties panel
     */
    @Override
    protected void addLayer(Layer layer) {
        //add to availablekeymap first because the superclass's behavior will cause
        //this class to update its available key list
        String layerName = layer.getLayerName();
        List<String> newLayerAvailableKeys = getBinaryEntries(layer);
        
        ///
        Map<String, List<String>> availableKeyMap = getAvailableKeyMap(getWindowModel().getLayers());
        availableKeyMap.put(layerName, newLayerAvailableKeys);
        ///

        //for xsec Windows, each layer should automatically have the first non-static header key selected as horiz. annotation
        String firstNonStaticKeyName = WindowProperties.getNonStaticKey(layer.getDatasetProperties());
        newLayerAvailableKeys.remove(firstNonStaticKeyName);
        List<String> newLayerSelectedKeys = new ArrayList<String>();
        newLayerSelectedKeys.add(firstNonStaticKeyName);
        selectedKeyMap.put(layerName, newLayerSelectedKeys);

        super.addLayer(layer);

        if (super.getAnnotatedLayerName().equals(layerName)) {
            selectedKeys = newLayerSelectedKeys;
            availableKeys = newLayerAvailableKeys;
            if (selectedKeys.size() > 0) {
                synchronizationKey = selectedKeys.get(0);
                //bugfix: create a new set of default AnnotationProperties for the synchronizationKey when a new layer is added
                Map<String, AnnotationProperties> newMap = new HashMap<String, AnnotationProperties>();
                newMap.put(synchronizationKey, new AnnotationProperties(layerName, synchronizationKey));
                annotationPropertyMap.put(layerName, newMap);
            }
        }
    }

    public void deselectSelectedKeys() {
        //build array of selectedKeyNames to avoid ConcurrentModificationException
        String[] selectedKeyNames = selectedKeys.toArray(new String[0]);
        for (int i = 0; i < selectedKeyNames.length; i++) {
            setKeyAvailable(selectedKeyNames[i]);
        }
    }

    private void clearSelectedKeyList() {
        selectedKeys.clear();
    }

    /**
     * Get the location of the horizontal annotation.
     * 
     * @return LOCATION.NONE or LOCATION.TOP
     */
    public LABEL_LOCATION getHorizontalAnnotationLocation() {
        return horizontalAnnotationLocation;
    }

    private Map<String, List<String>> getAvailableKeyMap(List<Layer> layerList) {
        Map<String, List<String>> newMap = new HashMap<String, List<String>>();

        //add empty list of keys when no layer is selected
        newMap.put(WindowAnnotationProperties.NO_ANNOTATION, new ArrayList<String>());

        for (Layer layer : layerList) {
            newMap.put(layer.getLayerName(), getBinaryEntries(layer));
        }

        return newMap;
    }

    private ArrayList<String> getBinaryEntries(Layer layer) {
        DataObject[] dataObjects = layer.getDataObjects();
        if (dataObjects.length == 0) {
            return new ArrayList<String>();
        } else if (dataObjects[0].hasTraceHeader()) {
            return dataObjects[0].getTraceHeader().getTraceHeaderFieldNames();
        } else {
            return new ArrayList<String>();
        }
    }

    /**
     * Get the gets available for annotation.  Unlike list shown in the 
     * bhpViewer, which shows a large list of plausible key names, 
     * this list only shows keys which are valid for at least one of
     * the layers of the ViewerWindow to which this AnnotationProperties belongs.
     * 
     * @return String[] of available key names.
     */
    public String[] getAvailableKeys() {
        if (availableKeys == null) {
            addLayer(getWindowModel().getLayer(getAnnotatedLayerName()));
        }
        return availableKeys.toArray(new String[0]);
    }

    /**
     * Gets the annotation axis major step size (used if stepSize is set to USER_DEFINED).
     * 
     * @return double major step size (default is 1.0)
     */
    public double getMajorStep() {
        return majorStep;
    }

    /**
     * Gets the annotation axis minor step size (used if stepSize is set to USER_DEFINED).
     * 
     * @return double minor step size (default is 0.5)
     */
    public double getMinorStep() {
        return minorStep;
    }

    /**
     * Gets the keys selected for horizontal annotation (top to bottom).
     * 
     * @return String[] of selected key names.
     */
    public String[] getSelectedKeys() {
        if (selectedKeys == null) {
            addLayer(getWindowModel().getLayer(getAnnotatedLayerName()));
        }
        return selectedKeys.toArray(new String[0]);
    }

    /**
     * Gets the step size.  If USER_DEFINED, the majorStep and minorStep values
     * are used. 
     * 
     * @return AUTOMATIC or USER_DEFINED
     */
    public STEP_TYPE getStepType() {
        return stepType;
    }

    /**
     * Gets the key on which this window is synchronized or returns null if no keys
     * are selected for annotation.  Always returns a non-null key name if at least
     * one header key is selected for annotation.
     * 
     * @return String name of synchronized key.
     */
    public String getSynchronizationKey() {
        return synchronizationKey;
    }

    /**
     * Gets the location of the vertical annotation.
     * 
     * @return LOCATION.NONE, LOCATION.LEFT or LOCATION.RIGHT
     */
    public LABEL_LOCATION getVerticalAnnotationLocation() {
        return verticalAnnotationLocation;
    }

    public static String getXmlAlias() {
        return "crossSectionAnnotationProperties";
    }

    /**
     * Returns true if and only if annotations are enabled.
     * 
     * @return boolean true by default.
     */
    public boolean isAnnotationEnabled() {
        return annotationEnabled;
    }

    /**
     * Set the name of the layer which is used for annotations.
     * 
     * @param annotatedLayerName the name of the Layer chosen for annotation.
     */
    @Override
    public boolean setAnnotatedLayerName(String annotatedLayerName) {
        //bugfix: first, save selected key list
        selectedKeyMap.put(super.getAnnotatedLayerName(), new ArrayList<String>(selectedKeys));

        boolean result = super.setAnnotatedLayerName(annotatedLayerName);

        if (result == false) {
            return false;
        }

        Map<String, List<String>> availableKeyMap = getAvailableKeyMap(getWindowModel().getLayers());
        List<String> keyList = availableKeyMap.get(annotatedLayerName);
        setAvailableKeys(new ArrayList<String>(keyList));
        clearSelectedKeyList();

        selectedKeys = selectedKeyMap.get(annotatedLayerName);
        if (selectedKeys == null) {
            addLayer(getWindowModel().getLayer(getAnnotatedLayerName()));
        }
        return true;
    }

    /**
     * Gets the AnnotationProperties associated with a given layer and key name.
     * Will create a nwe AnnotationProperties if one does not already exists in the
     * Layer Name to (KeyName to AnnotationProperties) nested Map for this layer-
     * and KeyName.
     * 
     * @param layerName
     * @param keyName
     * 
     * @return AnnotationProperties for the given layer and key name
     */
    public AnnotationProperties getAnnotationProperties(String layerName, String keyName) {
        Map<String, AnnotationProperties> annoPropMap = annotationPropertyMap.get(layerName);
        if (annoPropMap == null) {
            annoPropMap = new HashMap<String, AnnotationProperties>();
            annotationPropertyMap.put(layerName, annoPropMap);
        }
        AnnotationProperties annoProps = annoPropMap.get(keyName);
        if (annoProps == null) {
            annoProps = new AnnotationProperties(layerName, keyName);
            annoPropMap.put(keyName, annoProps);
        }

        return annoProps;
    }

    public void setAnnotationProperties(String layerName, String keyName, AnnotationProperties annoProps) {
        Map<String, AnnotationProperties> annoPropMap = annotationPropertyMap.get(layerName);
        if (annoPropMap == null) {
            annoPropMap = new HashMap<String, AnnotationProperties>();
            annotationPropertyMap.put(layerName, annoPropMap);
        }
        annoPropMap.put(keyName, annoProps);
    }

    /**
     * Serializes the state of this <code>WindowProperties</code> as a {@link String}
     * representation of an XML element.
     */
    public String saveState() throws Exception {
        StringBuilder content = new StringBuilder();
        XStream xStream = XStreamUtil.getInstance();

        content.append("<" + getXmlAlias() + " ");
        content.append(super.getXMLAttributes());

        String QUOTE_AND_SPACE = "\" ";

        content.append(VERT_ANNOT_ATTR_NAME + "=\"");
        content.append(verticalAnnotationLocation.toString());
        content.append(QUOTE_AND_SPACE);

        content.append(STEP_TYPE_ATTR_NAME + "=\"");
        content.append(stepType.toString());
        content.append(QUOTE_AND_SPACE);

        content.append(MAJOR_STEP_ATTR_NAME + "=\"");
        content.append(Double.valueOf(majorStep).toString());
        content.append(QUOTE_AND_SPACE);

        content.append(MINOR_STEP_ATTR_NAME + "=\"");
        content.append(Double.valueOf(minorStep).toString());
        content.append(QUOTE_AND_SPACE);

        //optional attribute - if not present, the first selected Key, if any, will
        //be made the synchronizationKey
        if (synchronizationKey != null) {
            content.append(SYNC_KEY_NAME + "=\"");
            content.append(synchronizationKey);
            content.append(QUOTE_AND_SPACE);
        }

        content.append(">\n");
        content.append(super.getXMLElements());

        content.append("<availableKeys>\n");
        try {
            content.append(xStream.toXML(availableKeys));
        } catch (Exception ex) {
            logger.warning("While saving availableKeys, caught: " + ex.getMessage());
            throw ex;
        }
        content.append("\n");
        content.append("</availableKeys>\n");

        content.append("<selectedKeys>\n");
        try {
            content.append(xStream.toXML(selectedKeys));
        } catch (Exception ex) {
            logger.warning("While saving selectedKeys, caught: " + ex.getMessage());
            throw ex;
        }
        content.append("\n");
        content.append("</selectedKeys>\n");

        String annotatedLayerName = getAnnotatedLayerName();

        //if there is an annotated layer, save the annotation properties
        if (!WindowAnnotationProperties.NO_ANNOTATION.equals(annotatedLayerName)) {
            content.append("<annotationProperties>");
            content.append(xStream.toXML(annotationPropertyMap.get(getAnnotatedLayerName())));
            content.append("</annotationProperties>");
        }

        content.append("</" + getXmlAlias() + ">\n");

        return content.toString();
    }

    private void setAvailableKeys(List<String> keys) {
        this.availableKeys = keys;
    }

    /**
     * Sets the horizontal annotation location to LOCATION.NONE or LOCATION_TOP.
     * 
     * @throws IllegalArgumentException if location value is not one of NONE or TOP.
     * 
     * @param location of horizontal location
     */
    public void setHorizontalLocation(LABEL_LOCATION location) {
        if (location == LABEL_LOCATION.NONE || location == LABEL_LOCATION.TOP) {
            this.horizontalAnnotationLocation = location;
        } else {
            throw new IllegalArgumentException("Horizontal annotation location may only be set to NONE or LEFT.");
        }
    }

    /**
     * Removes the key from the list of selected keys and adds it to the list of
     * keys available for annotation.
     * 
     * @throws IllegalArgumentException if keyName is not in the list of selected keys
     * 
     * @param keyName String name of an available key
     */
    public void setKeyAvailable(String keyName) {
        if (availableKeys.contains(keyName)) {
            return; // do nothing, key is already selected

        }
        if (!selectedKeys.contains(keyName)) {
            throw new IllegalArgumentException("Cannot de-select key: " + keyName + ", because it is not in the list of selected keys.");
        }
        int selectedKeyIndex = selectedKeys.indexOf(keyName);
        selectedKeys.remove(keyName);
        availableKeys.add(keyName);

        //if the key which was deselected was the synchronized key, set
        //a different synchronized key.  If the last selected key was deselected,
        //the synchronization key becomes null.
        if (keyName.equals(synchronizationKey)) {
            if (selectedKeys.size() > 0) {
                setSynchronizationKey(selectedKeys.get(selectedKeyIndex));
            } else {
                synchronizationKey = null;
            }
        }
    }

    /**
     * Removes the key from the list of available keys and adds it to the list of
     * keys selected for annotation.  If the key is already selected, no action is taken.
     * 
     * @throws IllegalArgumentException if keyName is not in the list of available keys
     * 
     * @param keyName String name of an available key
     */
    public void setKeySelected(String keyName) {
        if (selectedKeys.contains(keyName)) {
            return; // do nothing, key is already selected
        }
        if (!availableKeys.contains(keyName)) {
            throw new IllegalArgumentException("Cannot select key: " + keyName + ", because it is not in the list of available keys.");
        }
        availableKeys.remove(keyName);
        selectedKeys.add(keyName);

        if (selectedKeys.size() == 1) {
            setSynchronizationKey(keyName);
        }
    }

    /**
     * Moves the key from the list of selected keys to one step higher than its current location in the list.
     * If the key is already on the top of the list, no action is taken.
     * 
     * @param keyName String name of a selected key
     */
    public void moveSelectedKeyUp(String keyName) {
        if (!selectedKeys.contains(keyName)) {
            return; // do nothing, key is not in the list
        }

        int idx = selectedKeys.indexOf(keyName);
        if (idx == 0) //already on the top
        {
            return;
        }
        String up = selectedKeys.get(idx - 1);
        selectedKeys.set(idx - 1, keyName);
        selectedKeys.set(idx, up);

    }

    /**
     * Moves the key from the list of selected keys to one step lower than its current location in the list.
     * If the key is already on the bottom of the list, no action is taken.
     * 
     * @param keyName String name of a selected key
     */
    public void moveSelectedKeyDown(String keyName) {
        if (!selectedKeys.contains(keyName)) {
            return; // do nothing, key is not in the list
        }
        int idx = selectedKeys.indexOf(keyName);
        if (idx == selectedKeys.size() - 1) {
            return; //already on the bottom
        }
        String next = selectedKeys.get(idx + 1);
        selectedKeys.set(idx + 1, keyName);
        selectedKeys.set(idx, next);
    }

    /**
     * Sets the major step Size
     * @param stepSize double major tick increment, used if stepSize is USER_DEFINED.
     */
    public void setMajorStep(double stepSize) {
        this.majorStep = stepSize;
    }

    /**
     * Sets the minor step Size
     * @param stepSize double major tick increment, used if stepSize is USER_DEFINED.
     */
    public void setMinorStep(double stepSize) {
        this.minorStep = stepSize;
    }

    /**
     * Sets the step type.
     * 
     * @param type STEP_TYPE: AUTOMATIC (default) or USER_DEFINED
     */
    public void setStepType(STEP_TYPE type) {
        stepType = type;
    }

    /**
     * Sets the location of the vertical axis annotation.
     * 
     * @throws IllegalArgumentException if location is not one of: NONE or LEFT.
     * 
     * @param location LOCATION.NONE or LOCATION.LEFT
     */
    public void setVerticalAnnotationLocation(LABEL_LOCATION location) {
        if (location == LABEL_LOCATION.NONE || location == LABEL_LOCATION.LEFT) {
            this.verticalAnnotationLocation = location;
        } else {
            throw new IllegalArgumentException("Vertical annotation location may only be set to NONE or LEFT.");
        }
    }

    /**
     * Set the key on which this window is synchronized.
     * 
     * @param syncKey the name of the synchronized key.
     */
    public void setSynchronizationKey(String syncKey) {
        if (selectedKeys.contains(syncKey)) {
            synchronizationKey = syncKey;
        } else {
            throw new IllegalArgumentException("Key '" + syncKey + "' is not contained in the selectedKeys List");
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CrossSectionAnnotationProperties other = (CrossSectionAnnotationProperties) obj;
        if (this.annotationEnabled != other.annotationEnabled) {
            return false;
        }
        if (this.majorStep != other.majorStep) {
            return false;
        }
        if (this.minorStep != other.minorStep) {
            return false;
        }
        if (this.availableKeys != other.availableKeys && (this.availableKeys == null || !this.availableKeys.equals(other.availableKeys))) {
            return false;
        }
        if (this.selectedKeys != other.selectedKeys && (this.selectedKeys == null || !this.selectedKeys.equals(other.selectedKeys))) {
            return false;
        }
        if (this.annotationPropertyMap != other.annotationPropertyMap && (this.annotationPropertyMap == null || !this.annotationPropertyMap.equals(other.annotationPropertyMap))) {
            return false;
        }
        if (this.selectedKeyMap != other.selectedKeyMap && (this.selectedKeyMap == null || !this.selectedKeyMap.equals(other.selectedKeyMap))) {
            return false;
        }
        if (this.horizontalAnnotationLocation != other.horizontalAnnotationLocation) {
            return false;
        }
        if (this.verticalAnnotationLocation != other.verticalAnnotationLocation) {
            return false;
        }
        if (this.stepType != other.stepType) {
            return false;
        }
        if ((this.synchronizationKey == null) ? (other.synchronizationKey != null) : !this.synchronizationKey.equals(other.synchronizationKey)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.annotationEnabled ? 1 : 0);
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.majorStep) ^ (Double.doubleToLongBits(this.majorStep) >>> 32));
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.minorStep) ^ (Double.doubleToLongBits(this.minorStep) >>> 32));
        hash = 17 * hash + (this.availableKeys != null ? this.availableKeys.hashCode() : 0);
        hash = 17 * hash + (this.selectedKeys != null ? this.selectedKeys.hashCode() : 0);
        hash = 17 * hash + (this.annotationPropertyMap != null ? this.annotationPropertyMap.hashCode() : 0);
        hash = 17 * hash + (this.selectedKeyMap != null ? this.selectedKeyMap.hashCode() : 0);
        hash = 17 * hash + (this.horizontalAnnotationLocation != null ? this.horizontalAnnotationLocation.hashCode() : 0);
        hash = 17 * hash + (this.verticalAnnotationLocation != null ? this.verticalAnnotationLocation.hashCode() : 0);
        hash = 17 * hash + (this.stepType != null ? this.stepType.hashCode() : 0);
        hash = 17 * hash + (this.synchronizationKey != null ? this.synchronizationKey.hashCode() : 0);
        return hash;
    }
}