/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.window;

import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.util.XStreamUtil;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.bhpb.qiworkbench.compAPI.XmlUtils;
import com.thoughtworks.xstream.XStream;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.w3c.dom.Node;

/**
 * Abstract base class for Cross-section and Map Annotation Properties.
 */
public abstract class WindowAnnotationProperties implements WindowPropertyGroup {

    /**
     * Valid locations of various Annotation-related GUI Components.
     */
    public enum LABEL_LOCATION {

        NONE, TOP, BOTTOM, LEFT, RIGHT
    };
    public static final String NO_ANNOTATION = "No Annotation";
    private static final Logger logger =
            Logger.getLogger(WindowAnnotationProperties.class.toString());
    private LABEL_LOCATION colorBarLocation = LABEL_LOCATION.NONE;
    private LABEL_LOCATION titleLocation = LABEL_LOCATION.NONE;
    private Map<LABEL_LOCATION, String> labels = new EnumMap<LABEL_LOCATION, String>(LABEL_LOCATION.class);
    private String annotatedLayerName = NO_ANNOTATION;
    private String title = "";
    
    private transient WindowModel windowModel = null;

    public WindowAnnotationProperties(WindowAnnotationProperties aWindowAnnotationProperties) {
        this.colorBarLocation = aWindowAnnotationProperties.colorBarLocation;
        this.titleLocation = aWindowAnnotationProperties.titleLocation;
        this.labels = new EnumMap<LABEL_LOCATION, String>(aWindowAnnotationProperties.labels);
        //this.layerNames = new ArrayList<String>(aWindowAnnotationProperties.layerNames);
        this.annotatedLayerName = aWindowAnnotationProperties.annotatedLayerName;
        this.title = aWindowAnnotationProperties.title;
        //do not deep copy the window properties, a copy of the windowannotationproperties still references the same window
        this.windowModel = aWindowAnnotationProperties.windowModel;
    }

    public WindowAnnotationProperties(Node node) {
        title = ElementAttributeReader.getString(node, "title");
        titleLocation = LABEL_LOCATION.valueOf(ElementAttributeReader.getString(node, "titleLoc"));

        String cbLocString = ElementAttributeReader.getString(node, "colorBarLoc");

        //temporary check to allow loading of development-version state files, which lacked this attribute
        if (!"".equals(cbLocString)) {
            colorBarLocation = LABEL_LOCATION.valueOf(cbLocString);
        }

        Node child = ElementAttributeReader.getChildNode(node, "labels");
        if (child == null) {
            throw new RuntimeException("Node WindowAnnotationProperties must contain a child named 'labels'.");
        }

        XStream xStream = XStreamUtil.getInstance();

        Object firstChildObject = xStream.fromXML(ElementAttributeReader.nodeToXMLString(XmlUtils.getNonTextChild(child)));
        if (firstChildObject instanceof Map == false) {
            throw new IllegalArgumentException("First child of child node 'labels' must be a Map");
        }
        labels = (Map) firstChildObject;

        Node annotatedLayerNameNode = ElementAttributeReader.getChildNode(node, "annotatedLayer");
        if (annotatedLayerNameNode != null) {
            //This must be set prior to deserializing the available layer name or else
            //the behavior for switching from 'No annotated layer' would cause invalid state.
            //The deserialized selected key(s) would be reset to the empty list
            //when the first layer name is added, and the viewerwindow would not initialize with a single
            //seismic layer but no horizontal annotations.
            //Note that the bug still exists that selecting a different annotated layer does not initially
            //select any keys, but should select the first header key.
            annotatedLayerName = annotatedLayerNameNode.getTextContent();
        } else {
            throw new IllegalArgumentException("Unable to deserialize annotated layer name from child 'annotatedLayer' of node: " + node.getNodeName());
        }

        child = ElementAttributeReader.getChildNode(node, "layerNames");
        if (child == null) {
            throw new RuntimeException("Node WindowAnnotationProperties must contain a child named 'layerNames'.");
        }
        Object secondChildObject = xStream.fromXML(ElementAttributeReader.nodeToXMLString(XmlUtils.getNonTextChild(child)));
        if (secondChildObject instanceof List == false) {
            throw new IllegalArgumentException("First child of child node 'layerNames' must be a List");
        }

        //throw new UnsupportedOperationException("Not yet re-implemented for WindowModel update.");
        //layerNames = (List) secondChildObject;
        logger.info("List of layer names is not used by WindowAnnotationProperties deserialization as it is redundant with the actual layer set.");
    }

    public WindowAnnotationProperties() {
        //layerNames.add(NO_ANNOTATION);
        initializeBlankLabels();
    }

    /**
     * Update the ViewerWindow AnnotationProperties for a new layer.  The new layer's name
     * is added to the list of layers that may be selected for annotation.
     *
     * For xsec layer, the default horizontal annotation is the first key with which
     * varies over the dataset.
     * For example, if the full key range of 'ep' is 3015-3016, then entering * or 3015-3016
     * would result in 'ep' becoming the default horizontal annotation key, but entering
     * '3015' or '3015-3015' would result in 'cdp' becoming the annotated key.
     *
     * If all header key ranges have explicit single-value chosen ranges, the distinction becomes
     * meaningless as there is only one annotation tickmark/caption pair.
     *
     * If either header key range is an ArbitraryTraverseKeyRange, then both must be, and the first
     * will be selected as the horizontal annotation.
     *
     * This is a slight bug-fix to the bhp and INT viewer functionality,
     * which can be made to display a single annotation value by entering a range in the form
     * 'VAL-VAL' or [Val1-VAl2] where the first value Val1 Incr is outside of the full key range
     * and therefore VAl2 is irrelevant.
     * 
     * Subclasses which allow setting of annotations based on layer properties such
     * as key names or ranges should override and call this method.
     * 
     * @param layer new Layer to be added to the main AbstractAnnotationProperties panel
     */
    protected void addLayer(Layer layer) {
        if (NO_ANNOTATION.equals(annotatedLayerName)) {
            setAnnotatedLayerName(layer.getLayerName());
        }
    }

    /**
     * Determines whether the given Layer has a name which exactly matches a name
     * already in the list of layer names available for annotation.
     * 
     * @param layer Layer which may already be in the list of Layers that can be annotated
     * 
     * @return true if and only if the name of Layer is already in this WindowAnnotationProperties' list of Layer names
     */
    /**
     * Gets the name of the layer which is used for annotations.
     * 
     * @return String the name of the Layer chosen for annotation.
     */
    public String getAnnotatedLayerName() {
        return annotatedLayerName;
    }

    /**
     * Get the location of the ColorBar graphic.
     * 
     * @return LOCATION.NONE, LOCATION.LEFT or LOCATION.RIGHT
     */
    public LABEL_LOCATION getColorBarLocation() {
        return colorBarLocation;
    }

    /**
     * Get the label associated with the border at the given location.
     * 
     * @throws IllegalArgumentException if location is not TOP, BOTTOM, LEFT or RIGHT
     * @param location LOCATION.TOP, LOCATION.BOTTOM, LOCATION.LEFT or LOCATION.RIGHT
     * 
     * @return String of label text at given location (not null)
     */
    public String getLabel(LABEL_LOCATION location) {
        if (location == LABEL_LOCATION.TOP || location == LABEL_LOCATION.BOTTOM ||
                location == LABEL_LOCATION.LEFT || location == LABEL_LOCATION.RIGHT) {
            return labels.get(location);
        } else {
            throw new IllegalArgumentException("Location must be TOP, LEFT, RIGHT or BOTTOM.");
        }
    }

    public List<String> getLayerNames() {

        List<String> newLayerNames = new ArrayList<String>();
        newLayerNames.add(NO_ANNOTATION);
        if (getWindowModel() == null) {
            logger.info("Null windowModel");
        }
        for (Layer layer : getWindowModel().getLayers()) {
            newLayerNames.add(layer.getLayerName());
        }
        return newLayerNames;
    }

    /**
     * Gets the title of this set of annotations.
     * 
     * @return String annotation title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets the location of the Annotation title.
     * 
     * @return LOCATION.NONE or LOCATION.TOP
     */
    public LABEL_LOCATION getTitleLocation() {
        return titleLocation;
    }

    /**
     * Gets the XML attribute String representation of this base class's
     * fields for use by subclass saveState() implementation.
     * 
     * @return String representation of title, labels, etc.
     */
    protected String getXMLAttributes() {
        StringBuffer sbuf = new StringBuffer();

        sbuf.append("title=\"" + title + "\" ");
        sbuf.append("titleLoc=\"" + titleLocation.toString() + "\" ");
        sbuf.append("colorBarLoc=\"" + colorBarLocation.toString() + "\" ");

        return sbuf.toString();
    }

    /**
     * Gets the fields of this base class which do not have a convenient representation
     * as a signle attribute String such as the map of label strings to locations
     * and list of available layers.
     * 
     * @return String representations of various WindowAnnotationProperties fields
     * as newline-delimited XML elements.
     */
    protected String getXMLElements() {
        StringBuffer sbuf = new StringBuffer();

        XStream xStream = XStreamUtil.getInstance();

        sbuf.append("<labels>\n");

        String currentOp = "saving label EnumMap";

        try {
            sbuf.append(xStream.toXML((EnumMap<LABEL_LOCATION, String>) labels));
            sbuf.append("\n");
            sbuf.append("</labels>\n");

            sbuf.append("<annotatedLayer>");
            sbuf.append(annotatedLayerName);
            sbuf.append("</annotatedLayer>");

            sbuf.append("<layerNames>\n");

            currentOp = "saving layer names";
            sbuf.append(xStream.toXML(getLayerNames()));
            sbuf.append("\n");
            sbuf.append("</layerNames>\n");
        } catch (Exception ex) {
            logger.warning("While " + currentOp + ", caught: " + ex.getMessage());
        }
        return sbuf.toString();
    }

    private void initializeBlankLabels() {
        labels.put(LABEL_LOCATION.LEFT, "");
        labels.put(LABEL_LOCATION.RIGHT, "");
        labels.put(LABEL_LOCATION.TOP, "");
        labels.put(LABEL_LOCATION.BOTTOM, "");
    }

    public abstract String saveState() throws Exception;

    /**
     * Set the name of the layer which is used for annotations.
     * 
     * @param annotatedLayerName the name of the Layer chosen for annotation
     */
    public boolean setAnnotatedLayerName(String annotatedLayerName) {
        if (annotatedLayerName == null) {
            return false;
        }

        if (getLayerNames().indexOf(annotatedLayerName) == -1) {
            logger.warning("Unable to change annotated layer name because: " + annotatedLayerName + " is not in the list.");
            return false;
        } else {
            this.annotatedLayerName = annotatedLayerName;
            return true;
        }
    }

    /**
     * Sets the location of the graphical colorbar component.
     * 
     * @param location LOCATION.NONE, LOCATION.LEFT or LOCATION.RIGHT
     */
    public void setColorBarLocation(LABEL_LOCATION location) {
        this.colorBarLocation = location;
    }

    /**
     * Sets the String value of the label associated with the given LOCATION.
     * 
     * @param location LOCATION of the label which will be set.
     * @param labelText String value of label text
     */
    public void setLabel(LABEL_LOCATION location, String labelText) {
        labels.put(location, labelText);
    }

    /**
     * Set the title of this set of annotations.
     * 
     * @param title the annotation title.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Set the location of the Annotation title.
     * 
     * @param loc the location of the annotation title: LOCATION.NONE or LOCATION.TOP
     */
    public void setTitleLocation(LABEL_LOCATION loc) {
        if (loc == LABEL_LOCATION.NONE || loc == LABEL_LOCATION.TOP) {
            titleLocation = loc;
        } else {
            throw new IllegalArgumentException("Title location may only be set to NONE or TOP.");
        }
    }

    public void setWindowModel(WindowModel windowModel) {
        this.windowModel = windowModel;
    }

    protected WindowModel getWindowModel() {
        return windowModel;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WindowAnnotationProperties other = (WindowAnnotationProperties) obj;
        if (this.colorBarLocation != other.colorBarLocation) {
            return false;
        }
        if (this.titleLocation != other.titleLocation) {
            return false;
        }
        if (this.labels != other.labels && (this.labels == null || !this.labels.equals(other.labels))) {
            return false;
        }
        if ((this.annotatedLayerName == null) ? (other.annotatedLayerName != null) : !this.annotatedLayerName.equals(other.annotatedLayerName)) {
            return false;
        }
        if ((this.title == null) ? (other.title != null) : !this.title.equals(other.title)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (this.colorBarLocation != null ? this.colorBarLocation.hashCode() : 0);
        hash = 83 * hash + (this.titleLocation != null ? this.titleLocation.hashCode() : 0);
        hash = 83 * hash + (this.labels != null ? this.labels.hashCode() : 0);
        hash = 83 * hash + (this.annotatedLayerName != null ? this.annotatedLayerName.hashCode() : 0);
        hash = 83 * hash + (this.title != null ? this.title.hashCode() : 0);
        return hash;
    }
}