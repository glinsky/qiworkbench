/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.window;

import com.bhpb.geographics.accessor.FieldProcessor;
import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geographics.util.XStreamUtil;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.thoughtworks.xstream.XStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.w3c.dom.Node;

/**
 * WindowSyncProperties is the model for window-level settings such as listening to the
 * horizontal scale, the vertical scale, scrolling and the broadcast of these properties.
 * 
 * @author folsw9
 */
public class WindowSyncProperties implements WindowPropertyGroup, PropertyAccessor {

    public enum SYNC_FLAG {

        BROADCAST, H_SCALE_LISTENING, V_SCALE_LISTENING,
        CURSOR_POS_LISTENING, SCROLL_POS_LISTENING, H_SCROLL_POS_SYNC, V_SCROLL_POS_SYNC
    }
    private Map<SYNC_FLAG, Boolean> flagMap = new HashMap<SYNC_FLAG, Boolean>();

    //Easiest way to customize the PropertyAccessor behavior of this class is to
    //edit the names of these FIELD enum values to exactly match the qualified
    //field names that are passed to getProperty() and setProperty()
    public enum FIELD {

        BROADCAST, H_SCALE_LISTENING, V_SCALE_LISTENING,
        CURSOR_POS_LISTENING, SCROLL_POS_LISTENING, H_SCROLL_POS_SYNC, V_SCROLL_POS_SYNC
    }
    
    private transient FieldProcessor<FIELD> processor = new FieldProcessor<FIELD>(FIELD.values(), WindowSyncProperties.class);

    /**
     * Constructs a WindowSyncProperties, setting all SYNC_FLAGs are set to TRUE.
     */
    public WindowSyncProperties() {
        for (SYNC_FLAG syncFlag : SYNC_FLAG.values()) {
            flagMap.put(syncFlag, Boolean.TRUE);
        }
    }

    public boolean isEnabled(SYNC_FLAG flag) {
        return flagMap.get(flag);
    }

    public void setEnabled(SYNC_FLAG flag, boolean value) {
        flagMap.put(flag, value);
    }

    public String getProperty(String qualifiedField) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        switch (field) {
            case BROADCAST:
                //Processor.valueOf takes Boolean.toString() values and converts them to 'on' or 'off'
                return FieldProcessor.valueOf(flagMap.get(SYNC_FLAG.BROADCAST));
            /////////////////////////////////////////////////////////////
            //Continue implementation of additional field setters here...
            /////////////////////////////////////////////////////////////
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }

    public static String getXmlAlias() {
        return "windowSyncProperties";
    }
    
    public static WindowSyncProperties restoreState(Node node) {
        XStream xStream = XStreamUtil.getInstance();
        String xml = ElementAttributeReader.nodeToXMLString(node);
        return (WindowSyncProperties) xStream.fromXML(new StringReader(xml));
    }

    public String saveState() {
        StringBuffer content = new StringBuffer();

        XStream xStream = XStreamUtil.getInstance();
        content.append(xStream.toXML(this));
        content.append("\n");

        return content.toString();
    }
    
    public Set<SYNC_FLAG> getSyncFlagSet() {
        return flagMap.keySet();
    }

    public void setProperty(String qualifiedField, String value) throws PropertyAccessorException {
        FIELD field = processor.getFirstMatchingField(qualifiedField);
        String remainder = processor.getFieldRemainder(qualifiedField);

        if ("".equals(remainder) == false) {
            throw new PropertyAccessorException("Field: " + field + " is valid but qualified field name contains additional unknown field: " + remainder);
        }

        if (value == null ||
                !(value.equals(FieldProcessor.BOOLEAN_FALSE_STRING) || value.equals(FieldProcessor.BOOLEAN_TRUE_STRING))) {
            throw new PropertyAccessorException("Illegal value: '" + value);
        }

        switch (field) {
            case BROADCAST:
                setEnabled(SYNC_FLAG.BROADCAST, FieldProcessor.booleanValue(value));
                break;
            /////////////////////////////////////////////////////////////
            //Continue implementation of additional field setters here...
            /////////////////////////////////////////////////////////////
            default:
                throw new PropertyAccessorException("Error: field was validated but does not match a known field: " + field);
        }
    }
}