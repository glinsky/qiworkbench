/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import com.bhpb.geoio.datasystems.DataObject;

public class AnnotationFactory {

    private static final int min = 0;
    private static final int tickOffset = 0;

    public static Annotation createXaxisAnnotation(String axisName,
            int pixelWidth,
            DataObject[] dataObjects,
            Layer annotatedLayer,
            double scale) {
        return new Annotation(Annotation.AXIS.X_AXIS, 
                min,
                pixelWidth,
                axisName, 
                tickOffset,
                dataObjects,
                annotatedLayer,
                scale);
    }

    public static Annotation createYaxisAnnotation(String axisName,
            int pixelHeight,
            double scale,
            DataObject[] dataObjects,
            Layer annotatedLayer) {
        return new Annotation(
                Annotation.AXIS.Y_AXIS,
                min,
                pixelHeight,
                axisName,
                tickOffset,
                dataObjects,
                annotatedLayer,
                scale);
    }
}