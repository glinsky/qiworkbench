/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.ArbTravKeyRange;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.datasystems.UnlimitedKeyRange;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

/**
 * KeyRangeSerializer - handles conversions between String and KeyRange.
 * can transform KeyRange to a String Literal in the form min - max [incr],
 * and to the special String "*" indicating equivalence with the full range.
 *
 * Can also parse a String and generate a result indicating KeyRange validity, parser
 * errors, compatibility with full range and other conditions.
 *
 * getKeyRangeAsString works like toStringLiteral, butmay also yield the 
 * String "*" if KeyRange is equal to the header key's full range
 * 
 * parseRange(String chosenRangeString, KeyRange fullKeyRange) produces a result which includes a deserialized KeyRange,
 * or else indicates one of the following:
 *
 * 1) parserErrors - String contains unknown (non-integer) characters or extra delimiters: '-' , '[', ']'
 * 2) intConversionError: min, max or incr is a sequence of digits but cannot be converted to an Integer (out of range)
 * 3) compatibilityError: String represents a valid KeyRange which includes one or more traces which
 *    do not fall within the fullKeyRange (min too great, max too small, min > max or incr not a multiple of fullKeyRange incr
 *    Also, min must be equal to fullKeyRange's min plus an integer multiple of fullKeyRan
 */
public class KeyRangeSerializer {
    private static final Logger logger = Logger.getLogger(KeyRangeSerializer.class.getName());
    private static final KeyRangeTokenizer keyRangeTokenizer = new KeyRangeTokenizer();

    /**
     * A valid range consists of the either a single "*" character or the following
     * sequence of patterns separated
     * by whitespace: integer, "-", integer, "[", integer, "]".
     *
     * One or both ends of the chosen range string may be padded with whitespace.
     *
     * All 'integer' substrings are composed of consecutive digits - no delimiters
     * such as "," or "." are allowed.
     *
     * @param chosenRange String to be matched with the pattern described
     * @return true if the string matches the pattern for a valid KeyRange
     */
    public static boolean isValidFormat(String chosenRange) {
        boolean formatIsValid;

        if (chosenRange == null) {
            formatIsValid = false;
        } else if ("".equals(chosenRange)) {
            formatIsValid = false;
        } else {
            String trimmedChosenRange = chosenRange.trim();
            if ("*".equals(trimmedChosenRange)) {
                formatIsValid = true;
            } else {
                formatIsValid = matchesKeyRangePattern(trimmedChosenRange);
            }
        }

        return formatIsValid;
    }

    private static boolean matchesKeyRangePattern(String chosenRange) {
        KeyRangeToken[] tokens = KeyRangeSerializer.keyRangeTokenizer.getTokens(chosenRange);
        String errorMsgs = KeyRangeTokenizer.getParserErrors(tokens);
        KeyRangeToken[] floatTokens = KeyRangeSerializer.keyRangeTokenizer.getTokenSubset(tokens, KeyRangeToken.TYPE.FLOAT);
        return ("".equals(errorMsgs) && floatTokens.length == 3);
    }

    public static boolean isCompatibleWithFullRange(AbstractKeyRange chosenRange) {
        if (chosenRange instanceof DiscreteKeyRange) {
            return isCompatibleWithFullRange((DiscreteKeyRange) chosenRange);
        } else if (chosenRange instanceof ArbTravKeyRange) {
            return isCompatibleWithFullRange((ArbTravKeyRange) chosenRange);
        } else {
            throw new UnsupportedOperationException("Cannot evaluate compatibility of unknown range type: " + chosenRange.getClass().getName());
        }
    }

    /**
     * Returns true for all ArbTravKeyRanges as the bhpread command seems
     * to accept any valid Arbitrary Traverse notation key ranges.
     * 
     * @param chosenRange chosen KeyRange
     * 
     * @return true if and only if the chosen KeyRange is compatible with
     * the full KeyRange associated with this KeyRangeSerializer.
     */
    public static boolean isCompatibleWithFullRange(ArbTravKeyRange chosenRange) {
        return true;
    }

    /**
     * Test the given KeyRange for compatibility with the full header KeyRange
     * associatd with this KeyRangeSerializer.
     * To be compatible,
     * <ul>
     *   <li>The chosen min is >= the full range min</li>
     *   <li>The chosen max is <= the full range max</li>
     *   <li>The chosen increment is a positive integer multiple of the full range increment</li>
     * </ul>
     * 
     * @param chosenRange chosen KeyRange
     * @return true if and only if the chosen KeyRange is compatible with
     * the full KeyRange associated with this KeyRangeSerializer.
     */
    public static boolean isCompatibleWithFullRange(DiscreteKeyRange chosenRange) {
        boolean compatible = true;

        DiscreteKeyRange fullRange = chosenRange.getFullKeyRange();
        if (fullRange == null) {
            return true; // no ref to full range, perform no bounds checking, only syntax

        }

        if (chosenRange.getMin() < fullRange.getMin()) {
            compatible = false;
        } else if (chosenRange.getMax() > fullRange.getMax()) {
            compatible = false;
        } else if (chosenRange.getIncr() < fullRange.getIncr()) {
            compatible = false;
        } else if (chosenRange.getIncr() % fullRange.getIncr() != 0) {
            compatible = false;
        }

        return compatible;
    }

    private static boolean hasSyntaxErrors(KeyRangeToken[] tokens) {
        KeyRangeToken[] printableTokens = getPrintableTokens(tokens);

        //valid key ranges have 1, 3 or 6 printable tokens
        if (printableTokens.length != 1 && printableTokens.length != 3 && printableTokens.length != 6) {
            return true;
        }

        if (printableTokens.length == 1 && printableTokens[0].getType() == KeyRangeToken.TYPE.WILDCARD) {
            return false; // a single printable token is valid if and only if it is a Wildcard '*'

        }

        switch (printableTokens.length) {
            case 6: // 'min', '-', 'max', '[', 'incr', ']'

                if (printableTokens[3].getType() != KeyRangeToken.TYPE.LEFTBRACKET) {
                    return true;
                }
                if (printableTokens[4].getType() != KeyRangeToken.TYPE.FLOAT) {
                    return true;
                }
                if (printableTokens[5].getType() != KeyRangeToken.TYPE.RIGHTBRACKET) {
                    return true;
                } // fall through and check the first 3 characters

            case 3: // 'min' '-' 'max'

                if (printableTokens[1].getType() != KeyRangeToken.TYPE.DASH) {
                    return true;
                }
                if (printableTokens[2].getType() != KeyRangeToken.TYPE.FLOAT) {
                    return true;
                }
            // fall through and check the first character
            case 1:
                if (printableTokens[0].getType() != KeyRangeToken.TYPE.FLOAT) {
                    return true;
                }
                return false;
            default:
                return true;
        }
    }

    private static KeyRangeToken[] getPrintableTokens(KeyRangeToken[] tokens) {
        List<KeyRangeToken> printableTokens = new LinkedList<KeyRangeToken>();
        for (int i = 0; i < tokens.length; i++) {
            KeyRangeToken token = tokens[i];
            if (token.getType() != KeyRangeToken.TYPE.DELIMITER && token.getType() != KeyRangeToken.TYPE.WHITESPACE) {
                printableTokens.add(token);
            }

        }


        return printableTokens.toArray(
                new KeyRangeToken[0]);
    }

    private static String getPrintableTokensAsString(
            KeyRangeToken[] tokens) {
        StringBuffer sb = new StringBuffer("");
        KeyRangeToken[] printableTokens = getPrintableTokens(tokens);
        for (int i = 0; i < printableTokens.length; i++) {
            if (i == printableTokens.length - 1) {
                sb.append("and ");
            }

            KeyRangeToken token = printableTokens[i];
            sb.append(token.getType().toString());
            if (i < printableTokens.length - 2) {
                sb.append(", ");
            }

        }

        return sb.toString();
    }

    /**
     * Parses the chosenRangeString as one of 3 types of AbstractKeyRange:
     * DiscreteKeyRange, UnlimitedKeyRange or ArbTraverse.  Criteria:
     * 
     * <ol>
     * <li>Unlimited: chosenRangeString equals "*"</li>
     * <li>ArbTraverse: if chosenRangeString contains '^'</li>
     * <li>Discrete: if neither of the above conditions are true, chosenRanegString is validated as DiscreteKeyRange</li>
     * </ol>
     * 
     * @param chosenRangeString one of '*', value, min-max, min-max[incr] or p1^p2^p3...
     * @param fullKeyRange the full keyrange associated with this header key, used to validate chosen DiscreteKeyRange 
     * 
     * @return the KeyRangeParseResult, containing either an AbstractKeyRange subclass or else error messages
     * depending on the validity of the chosenRangeString for the specified fullKeyRange
     */
    public static KeyRangeParseResult parseRange(String chosenRangeString, DiscreteKeyRange fullKeyRange) {

        String trimmedChosenRange = chosenRangeString.trim();

        if (trimmedChosenRange.equals("*")) {
            return new KeyRangeParseResult(new UnlimitedKeyRange(fullKeyRange));
        } else if (trimmedChosenRange.contains("^")) {
            return parseArbTrav(trimmedChosenRange, fullKeyRange);
        } else {
            KeyRangeParseResult parseResult = parseDiscreteRange(trimmedChosenRange, fullKeyRange);

            //if ParseResult has errors, do not attempt to convert Discrete to UnlimitedKeyRange
            //but return immediately
            if (parseResult.hasErrors()) {
                return parseResult;
            }

            DiscreteKeyRange discreteKeyRange = (DiscreteKeyRange) parseResult.getKeyRange();

            //if the parsed discrete keyRange is equivalent to the fullKeyRange,
            //return an UnlimitedKeyRange in the KeyRangeParseResult
            if (discreteKeyRange.equals(fullKeyRange)) {
                return new KeyRangeParseResult(new UnlimitedKeyRange(fullKeyRange));
            //otherwise, return the parsed DiscreteKeyRange
            } else {
                return parseResult;
            }

        }
    }

    public static KeyRangeParseResult parseArbTrav(String chosenRangeString, DiscreteKeyRange fullKeyRange) {
        String parserErrors = "";
        String intConversionErrors = "";
        String compatibilityErrors = "";
        String syntaxErrors = "";

        //replace this with ArbTrav constructor call
        if (!chosenRangeString.contains("^")) {
            parserErrors += "Not an arbitrary traverse, chosen range lacks '^' delimiter.";
        }

        String[] intStrings = chosenRangeString.split("[.^]");
        List<Integer> arbTravInts = new ArrayList<Integer>();
        
        StringBuilder sbuilder = new StringBuilder(intConversionErrors);
        
        for (String intString : intStrings) {
            try {
                arbTravInts.add(Integer.valueOf(intString));
            } catch (NumberFormatException nfe) {
                String errMsg = "Invalid integer: " + intString;
                if (sbuilder.length() != 0) {
                    sbuilder.append(", ");
                }
                sbuilder.append(errMsg);
            }
        }
        
        intConversionErrors = sbuilder.toString();

        if (parserErrors.length() > 0 || intConversionErrors.length() > 0 || compatibilityErrors.length() > 0 || syntaxErrors.length() > 0) {
            return new KeyRangeParseResult(parserErrors, compatibilityErrors, intConversionErrors, syntaxErrors);
        } else {
            int[] arbTravIntArray = new int[arbTravInts.size()];
            for (int i = 0; i < arbTravIntArray.length; i++) {
                arbTravIntArray[i] = arbTravInts.get(i);
            }
            return new KeyRangeParseResult(new ArbTravKeyRange(arbTravIntArray, fullKeyRange));
        }
    }

    /**
     * Accepts a String containing exactly one, two or three Integer values and associated delimiters.
     * If these Integer values are valid and compatible with the full range, a KeyRangeParseResult
     * containing a deserialized KeyRange object is returned.  Otherwise, the KeyRangeParseResult
     * wraps Strings describing all error conditions.
     *
     * @param chosenRangeString specifies a chosen range in one of the following formats:
     *  Integer
     *  Integer - Integer
     *  Integer - Integer [Integer]
     * @param fullKeyRange KeyRange specifying the full range allowed for a header key
     * @return KeyRangeParseResult wrapping a KeyRange and describing any error conditions
     * 
     */
    public static KeyRangeParseResult parseDiscreteRange(
            String chosenRangeString, DiscreteKeyRange fullKeyRange) {
        KeyRangeTokenizer tokenizer = new KeyRangeTokenizer();

        KeyRangeToken[] tokens = tokenizer.getTokens(chosenRangeString);
        KeyRangeToken[] floatTokens = tokenizer.getTokenSubset(tokens, KeyRangeToken.TYPE.FLOAT);

        String parserErrors = KeyRangeTokenizer.getParserErrors(tokens);
        String intConversionErrors = "";
        String compatibilityErrors = "";
        String syntaxErrors = "";
        if (hasSyntaxErrors(tokens)) {
            syntaxErrors = "Valid range syntax is: 'min - max' or 'min - max [incr]',\n" +
                    " but the following tokens were read: \n " +
                    getPrintableTokensAsString(tokens);
        }

        DiscreteKeyRange chosenKeyRange = null;
        double min = 0;
        double max = 0;
        double incr = 0;

        if (floatTokens.length == 0) {
            if (chosenRangeString != null && "*".compareTo(chosenRangeString) == 0) {
                min = fullKeyRange.getMin();
                max = fullKeyRange.getMax();
                incr = fullKeyRange.getIncr();
            } else {
                parserErrors += "\nIf the chosen range contains no Integers, it must be '*', but was: " + chosenRangeString;
            }

        } else if (floatTokens.length == 1) { // case 1 : 1 integer - parse as both min and max, get incr from fullrange

            try {
                min = Double.parseDouble(floatTokens[0].getToken());
                max = min;
                incr = fullKeyRange.getIncr();
            } catch (NumberFormatException nfe) {
                intConversionErrors += "\nCannot convert minimum, maximum from: " + (floatTokens[0].getToken() + " because it is not a valid Integer.");
                logger.warning("NumberFormatException while converting min or max from String: " + nfe.getMessage());
            }

        } else if (floatTokens.length == 2) {
            try {
                min = Double.parseDouble(floatTokens[0].getToken());
                max = Double.parseDouble(floatTokens[1].getToken());
                incr = fullKeyRange.getIncr();
            } catch (NumberFormatException nfe) {
                intConversionErrors += "\nCannot convert minimum or maximum from: " + floatTokens[0].getToken() + " or " + floatTokens[1].getToken() + " because one of these is not a valid Integer.";
                logger.warning("NumberFormatException while converting min or max from String: " + nfe.getMessage());
            }

        } else { // case 2: require 3 integers and parse as min, max and incr

            if (floatTokens.length != 3) {
                intConversionErrors += "\nChosenRange must equal '*' or contain exactly 1, 2 or 3 integer values, but " + floatTokens.length + " were parsed from: " + chosenRangeString;
            }

            if (floatTokens.length > 0) {
                try {
                    min = Double.parseDouble(floatTokens[0].getToken());
                } catch (NumberFormatException nfe) {
                    intConversionErrors += "\nCannot convert minimum integer: " + (floatTokens[0].getToken() + " because it is not a valid Integer.");
                    logger.warning("NumberFormatException while converting min from String: " + nfe.getMessage());
                }

            }

            if (floatTokens.length > 1) {
                try {
                    max = Double.parseDouble(floatTokens[1].getToken());
                } catch (NumberFormatException nfe) {
                    intConversionErrors += "\nCannot convert maximum integer: " + (floatTokens[1].getToken() + " because it is not a valid Integer.");
                    logger.warning("NumberFormatException while converting max from String: " + nfe.getMessage());
                }

            }

            if (floatTokens.length > 2) {
                try {
                    incr = Double.parseDouble(floatTokens[2].getToken());
                } catch (NumberFormatException nfe) {
                    intConversionErrors += "Cannot convert increment integer: " + (floatTokens[2].getToken() + " because it is not a valid Integer.");
                    logger.warning("NumberFormatException while converting incr from String: " + nfe.getMessage());
                }

            }
        }
        
        if (min < fullKeyRange.getMin()) {
            compatibilityErrors += "Cannot set min value to: " + min + " because the full key range min is: " + fullKeyRange.getMin();
        }

        if (max > fullKeyRange.getMax()) {
            compatibilityErrors += "Cannot set max value to: " + max + " because the full key range max is: " + fullKeyRange.getMax();
        }

        if (Math.abs(incr) < fullKeyRange.getIncr()) {
            if (!"".equals(compatibilityErrors)) {
                compatibilityErrors += "\n";
            }

            compatibilityErrors += "Cannot set incr value to: " + incr + " because its absolute value is less than the full key range incr: " + fullKeyRange.getIncr();
        } else if (incr % fullKeyRange.getIncr() != 0) {
            if (!"".endsWith(compatibilityErrors)) {
                compatibilityErrors += "\n";
            }

            compatibilityErrors += "Cannot set incr value to: " + incr + " because it is not a multiple of the full key range incr: " + fullKeyRange.getIncr();
        }

        //bugfix: max may be < min if incr is negative
        //if min == max, incr is irrelevant and may be any value
        if ((min != max) && ((max-min) * incr < 0.0)) {
            compatibilityErrors += "Max must be greater than min if incr is positive, or less than min if incr is negative.";
        }

        if ("".equals(intConversionErrors)) {
            chosenKeyRange = new DiscreteKeyRange(min, max, incr, fullKeyRange.getKeyType(), fullKeyRange);
        } else {
            logger.warning("Cannot set keyRange to " + chosenRangeString + " because of integer conversion errors: " + intConversionErrors);
        }

        if (parserErrors.length() > 0 || intConversionErrors.length() > 0 || compatibilityErrors.length() > 0 || syntaxErrors.length() > 0) {
            return new KeyRangeParseResult(parserErrors, compatibilityErrors, intConversionErrors, syntaxErrors);
        } else {
            return new KeyRangeParseResult(chosenKeyRange);
        }

    }

    /**
     * Generate the String reprsentation of the chosen Range.
     * This returns chosenRange.toScriptString() with the exception
     * of an ArbTrav, which uses a different representation to distinguish
     * an ArbTrav typed into the cell from a comma-separated ListRange.
     *
     * @param chosenRange KeyRange used to generate the String
     * @return String representing the chosenRange in literal or wildcard form
     */
    public static String getKeyRangeAsString(
            AbstractKeyRange chosenRange) {

        String keyRangeString;

        if (chosenRange instanceof UnlimitedKeyRange) {
            keyRangeString = "*";
        } /*else if (chosenRange instanceof DiscreteKeyRange) {
            keyRangeString = KeyRangeSerializer.getKeyRangeAsLiteralString((DiscreteKeyRange) chosenRange);
        } */else if (chosenRange instanceof ArbTravKeyRange) {
            keyRangeString = KeyRangeSerializer.getKeyRangeAsLiteralString((ArbTravKeyRange) chosenRange);
        } else {
            keyRangeString = chosenRange.toScriptString();
        }

        if (isCompatibleWithFullRange(chosenRange)) {
            return keyRangeString;
        } else {
            String fullRangeStr = "UNKNOWN FULL RANGE";
            DiscreteKeyRange fullKeyRange = chosenRange.getFullKeyRange();
            if (fullKeyRange != null) {
                fullRangeStr = KeyRangeSerializer.getKeyRangeAsLiteralString(fullKeyRange);
            }
            throw new IllegalArgumentException("Chosen key range " +
                    keyRangeString + " is not valid for full range " +
                    fullRangeStr);
        }
    }

    /**
     * Generate the String representation of the KeyRange in the format min - max [incr].
     * This methods exists to provide a more human readable or different format
     * that that required by the bhpsu script.
     *
     * If the DiscreteKeyRange's min and max fields are equal, the String consists
     * of a single integer value, without a dash or increment value.
     * 
     * @param keyRange KeyRange which will be parsed to generate the String
     * @return String representation of the KeyRange
     * 
     */
    public static String getKeyRangeAsLiteralString(
            DiscreteKeyRange keyRange) {
        if (keyRange.getMin() == keyRange.getMax()) {
            return BigDecimal.valueOf(keyRange.getMin()).toString();
        } else {
            return BigDecimal.valueOf(keyRange.getMin()).intValue() + " - " + BigDecimal.valueOf(keyRange.getMax()).intValue() + " [" + BigDecimal.valueOf(keyRange.getIncr()).intValue() + "] ";
        }
    }

    public static String getKeyRangeAsLiteralString(ArbTravKeyRange keyRange) {
        int[] arbTravs = keyRange.getArbTravs();

        if (arbTravs.length == 0) {
            return "";
        }

        StringBuilder sbuilder = new StringBuilder();
        sbuilder.append(Integer.toString(arbTravs[0]));

        for (int i = 1; i < arbTravs.length; i++) {
            sbuilder.append("^");
            sbuilder.append(Integer.toString(arbTravs[i]));
        }

        return sbuilder.toString();
    }
}