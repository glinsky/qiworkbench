/*
###########################################################################
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import com.bhpb.geographics.controller.layer.LayerSyncGroup;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParametersFactory;
import com.bhpb.geographics.model.layer.display.LayerDisplayParameters;
import com.bhpb.geographics.model.layer.display.LocalSubsetProperties;
import com.bhpb.geographics.model.layer.display.LocalSubsetPropertiesFactory;
import com.bhpb.geographics.model.layer.display.MapDisplayParameters;
import com.bhpb.geographics.model.layer.display.MapDisplayParametersFactory;
import com.bhpb.geographics.model.layer.sync.HorizonXsecSyncProperties;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties;
import com.bhpb.geographics.model.layer.sync.MapSyncProperties;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geographics.util.HorizonPicker;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.datasystems.BhpSuHorizonDataObject;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import com.bhpb.geoio.util.GeoIOAdapter;
import java.util.logging.Level;
import javax.swing.ImageIcon;

public class HorizonLayer extends Layer {
    private static final String HORIZON_LAYER_ICON = "/icons/EventLayer.gif";

    private float[] mutableHorizon = null;
    
    private transient HorizonHighlightSettings horizonHighlightSettings;
    private transient HorizonPicker picker;
    private transient PickingSettings pickingSettings;

    public HorizonLayer(DatasetProperties dsProperties,
            LayerDisplayParameters layerDisplayParams,
            LocalSubsetProperties subsetProps,
            LayerSyncProperties syncProps,
            BhpSuHorizonDataObject[] horizons,
            GeoIOAdapter ioAdapter,
            String layerID,
            String layerName,
            PickingSettings pickingSettings,
            HorizonHighlightSettings horizonHighlightSettings,
            boolean allowArbTrav,
            ArbitraryTraverse arbTrav,
            LayerSyncGroup layerSyncGroup,
            WindowModel windowModel) {
        super(dsProperties, layerDisplayParams, subsetProps, syncProps, ioAdapter, "H", horizons,
                layerID, layerName, allowArbTrav, arbTrav, layerSyncGroup, windowModel);

        //override the default layername, for horizon layers, it is based on the selected horizon, not dataset name
        super.setLayerName(((HorizonProperties) dsProperties).getSelectedHorizon());

        this.pickingSettings = pickingSettings;
        this.horizonHighlightSettings = horizonHighlightSettings;

        if (horizons == null) {
            throw new IllegalArgumentException("Cannot create HorizonLayer with null horizons array.");
        }

        initMutableHorizon(horizons, (HorizonProperties) dsProperties);

        picker = new HorizonPicker(this);
    }

    public HorizonLayer(DatasetProperties dsProperties,
            LayerDisplayParameters layerDisplayParams,
            LocalSubsetProperties subsetProps,
            LayerSyncProperties syncProps,
            BhpSuHorizonDataObject[] horizons,
            GeoIOAdapter ioAdapter,
            PickingSettings pickingSettings,
            HorizonHighlightSettings horizonHighlightSettings,
            boolean hasArbTrav,
            LayerSyncGroup layerSyncGroup,
            WindowModel windowModel) {
        super(dsProperties, layerDisplayParams, subsetProps, syncProps, ioAdapter, "H", horizons,
                hasArbTrav, layerSyncGroup, windowModel);

        //override the default layername, for horizon layers, it is based on the selected horizon, not dataset name
        super.setLayerName(((HorizonProperties) dsProperties).getSelectedHorizon());

        this.pickingSettings = pickingSettings;
        this.horizonHighlightSettings = horizonHighlightSettings;

        if (horizons == null) {
            throw new IllegalArgumentException("Cannot create HorizonLayer with null horizons array.");
        }

        initMutableHorizon(horizons, (HorizonProperties) dsProperties);

        picker = new HorizonPicker(this);
    }

    public static HorizonLayer createHorizonLayer(HorizonLayer aHorizonLayer, WindowModel aWindowModel) {
        DatasetProperties dsProps = new HorizonProperties((HorizonProperties)aHorizonLayer.getDatasetProperties());
        LayerDisplayParameters layerDisplayParams;
        LayerSyncProperties layerSyncProps;

        GeoDataOrder orientation = aHorizonLayer.getDatasetProperties().getMetadata().getGeoDataOrder();

        switch (orientation) {
            case CROSS_SECTION_ORDER :
                layerDisplayParams = HorizonXsecDisplayParametersFactory.createLayerDisplayParameters(
                        (HorizonXsecDisplayParameters)aHorizonLayer.getDisplayParameters());
                layerSyncProps = new HorizonXsecSyncProperties((HorizonXsecSyncProperties)aHorizonLayer.getSyncProperties());
                break;
            case MAP_VIEW_ORDER :
                layerDisplayParams = MapDisplayParametersFactory.createLayerDisplayParameters(
                        (MapDisplayParameters)aHorizonLayer.getDisplayParameters());
                layerSyncProps = new MapSyncProperties((MapSyncProperties)aHorizonLayer.getSyncProperties());
                break;
            default :
                throw new IllegalArgumentException("Unable to create new HorizonLayer with unrecognized orientation: " + orientation);
        }

        LocalSubsetProperties localSubsetProps = LocalSubsetPropertiesFactory.createLocalSubsetProperties(aHorizonLayer.getLocalSubsetProperties());

        HorizonLayer hLayer = new HorizonLayer(
                dsProps,
                layerDisplayParams,
                localSubsetProps,
                layerSyncProps,
                aHorizonLayer.getHorizons(),
                aHorizonLayer.ioAdapter,
                aHorizonLayer.getPickingSettings(),
                aHorizonLayer.getHorizonHighlightSettings(),
                aHorizonLayer.arbTravEnabled,
                aHorizonLayer.layerSyncGroup,
                aWindowModel
            );

        if (orientation == GeoDataOrder.CROSS_SECTION_ORDER) {
            hLayer.setMutableHorizon(aHorizonLayer.getMutableHorizon());
        }

        return hLayer;
    }
    /**
     * @deprecated BhpSuHorizonDataObject class should be replaced everywhere by ordinary DataObject or ITrace
     * @param horizons
     * @param hProps
     */
    private void initMutableHorizon(BhpSuHorizonDataObject[] horizons,
            HorizonProperties hProps) {
        if (horizons.length > 0 && horizons[0] != null) {

            SeismicFileMetadata metadata = (SeismicFileMetadata) hProps.getMetadata();
            String selectedHorizon = hProps.getSelectedHorizon();

            GeoDataOrder dataOrder = metadata.getGeoDataOrder();

            int horizonIndex = getHorizonIndex(metadata, selectedHorizon);

            if (dataOrder == GeoDataOrder.CROSS_SECTION_ORDER) {
                mutableHorizon = new float[horizons.length];
                for (int traceIndex = 0; traceIndex < horizons.length; traceIndex++) {
                    mutableHorizon[traceIndex] = horizons[traceIndex].getFloatVector()[horizonIndex];
                }
            }
        }
    }

    /**
     * @deprecated BhpSuHorizonDataObject class should be replaced everywhere by ordinary DataObject or ITrace
     * @param horizons
     * @param hProps
     */
    private void initMutableHorizon(DataObject[] horizons,
            HorizonProperties hProps) {
        if (horizons.length > 0 && horizons[0] != null) {

            SeismicFileMetadata metadata = (SeismicFileMetadata) hProps.getMetadata();
            String selectedHorizon = hProps.getSelectedHorizon();

            GeoDataOrder dataOrder = metadata.getGeoDataOrder();

            int horizonIndex = getHorizonIndex(metadata, selectedHorizon);

            if (dataOrder == GeoDataOrder.CROSS_SECTION_ORDER) {
                mutableHorizon = new float[horizons.length];
                for (int traceIndex = 0; traceIndex < horizons.length; traceIndex++) {
                    mutableHorizon[traceIndex] = horizons[traceIndex].getFloatVector()[horizonIndex];
                }
            }
        }
    }

    /** This functionality is disabled pending the ability to read multiple horizons as a single BHPSU dataset.
     * Since only 1 horizon may currently be associated with a single HorizonDataset, this method will
     * always return 0 (the first of 1 horizon indices).
     */
    private int getHorizonIndex(SeismicFileMetadata metadata, String selectedHorizon) {
        return 0;
    }

    public void setHorizonHighlightSettings(HorizonHighlightSettings horizonHighlightSettings) {
        this.horizonHighlightSettings = new HorizonHighlightSettings(horizonHighlightSettings);
    }

    public void setMutableHorizon(float[] inputArray) {
        if (mutableHorizon == null) {
            throw new UnsupportedOperationException("Unable to set mutable horizon because this field is currently null");
        } else if (mutableHorizon.length != inputArray.length) {
            throw new IllegalArgumentException("Unable to set mutable horizon because the array length does not match the current mutableHorizon array length");
        }

        System.arraycopy(inputArray, 0, mutableHorizon, 0, inputArray.length);
    }

    public double getAmplitudeMax() {
        switch (getOrientation()) {
            case MAP_VIEW_ORDER:
                return ((MapDisplayParameters) getDisplayParameters()).getColorAttributes().getMax();
            case CROSS_SECTION_ORDER:
            default:
                return getDatasetProperties().getSummary().getMaxAmplitude();
        }
    }

    public double getAmplitudeMin() {
        switch (getOrientation()) {
            case MAP_VIEW_ORDER:
                return ((MapDisplayParameters) getDisplayParameters()).getColorAttributes().getMin();
            case CROSS_SECTION_ORDER:
            default:
                return getDatasetProperties().getSummary().getMinAmplitude();
        }
    }

    @Override
    public double getHeight() {
        if (getDatasetProperties().getMetadata().getGeoDataOrder() == GeoDataOrder.MAP_VIEW_ORDER) {
            return super.getHeight();
        } else {
            GeoFileDataSummary summary = getDatasetProperties().getSummary();
            return getVerticalMax() - getVerticalMin();
        }
    }

    @Override
    public double getVerticalMin() {
        //Horizon cross-sections are rasterized and annotated starting at 0,
        //if the minAmplitude is positive
        GeoFileMetadata metadata = dsProperties.getMetadata();
        if (metadata.getGeoDataOrder() == GeoDataOrder.MAP_VIEW_ORDER) {
            return super.getVerticalMin();
        } else {
            return Math.min(0,dsProperties.getSummary().getMinAmplitude());
        }
    }

    @Override
    public double getVerticalMax() {
        //Horizon cross-sections are rasterized and annotated starting at 0,
        //if the minAmplitude is positive
        GeoFileMetadata metadata = dsProperties.getMetadata();
        if (metadata.getGeoDataOrder() == GeoDataOrder.MAP_VIEW_ORDER) {
            return super.getVerticalMax();
        } else {
            return dsProperties.getSummary().getMaxAmplitude();
        }
    }

    public HorizonHighlightSettings getHorizonHighlightSettings() {
        return this.horizonHighlightSettings;
    }

    public ImageIcon getImageIcon() {
        return new ImageIcon(getClass().getResource(HORIZON_LAYER_ICON));
    }

    public static String getXmlAlias() {
        return "horizonLayer";
    }

    @Override
    public void handleMouseDrag(ViewTarget targetProps) {
        if (getDatasetProperties().getMetadata().getGeoDataOrder() != GeoDataOrder.CROSS_SECTION_ORDER) {
            logger.warning("Horizon Layer is not cross-section order, mouseDrag will not be handled.");
        } else {
            logger.info("HorizonLayer: " + getLayerName() + " handling mouseDrag at: " +
                    targetProps.getHorizontalString() + ", " + targetProps.getVerticalString());
            mutableHorizon[targetProps.getNearestTraceIndex()] = (float) targetProps.getVertical();
        }
    }

    public void pick(ViewTarget targetProps, DataObject[] dataObjects, GeoFileDataSummary summary) {
        if (logger.isLoggable(Level.FINE)) {
            logger.fine("Picking point: " + targetProps.getHorizontalString() + ", t/d:" + targetProps.getVertical() + " (trace #" + targetProps.getNearestTraceIndex() + ")");
        }

        picker.pick(targetProps.getNearestTraceIndex(),
                (float) targetProps.getVertical(),
                pickingSettings.getSnappingMode(),
                pickingSettings.getSnapLimit(),
                dataObjects,
                summary);

        broadcastChangeEvent();
    }

    /**
     * Gets this Layer's array of DataObjects as an array of BhpSuHorizonDataObjects.
     * 
     * @return the BhpSuHorizonDataObjects associated with this HorizonLayer.
     */
    public BhpSuHorizonDataObject[] getHorizons() {
        DataObject[] doArray = super.getDataObjects();
        BhpSuHorizonDataObject[] castArray = new BhpSuHorizonDataObject[doArray.length];
        for (int i = 0; i < castArray.length; i++) {
            castArray[i] = (BhpSuHorizonDataObject) doArray[i];
        }
        return castArray;
    }

    /**
     * Gets the array of floating-point values corresponding to the BhpSuHorizonDataObject
     * read from geoIO, plus any subsequent changes from picking operations.
     *
     * This is a COPY and will not modify the internal state of the HorizonLayer for the purposes
     * of rasterization or writing the horizon to disk.
     * 
     * @return array of floats
     */
    public float[] getMutableHorizon() {
        if (mutableHorizon == null) {
            return null;
        }
        //bug fix for exposure of internal state
        float[] outputArray = new float[mutableHorizon.length];
        System.arraycopy(mutableHorizon, 0, outputArray, 0, mutableHorizon.length);
        return outputArray;
    }

    public PickingSettings getPickingSettings() {
        return pickingSettings;
    }
    /**
     * Invokes setDatasetProperties(newDatasetProperties,false).
     */
    @Override
    public boolean setDatasetProperties(DatasetProperties newDatasetProperties) {
        if (super.setDatasetProperties(newDatasetProperties, false, false)) {
            initMutableHorizon(super.dataObjects, (HorizonProperties) newDatasetProperties);
            logger.info("Redrawing horizon layer #" + super.getLayerID() + " due to DatasetProperties change or forceReload request.");
            super.geoPlot.redraw(this, true, true);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Invokes setDatasetProperties(newDatasetProperties,false).
     */
    @Override
    public boolean setDatasetProperties(DatasetProperties newDatasetProperties, boolean forceReload, boolean redraw) {
        //Do not reorder these terms and also change the boolean operator from | to ||, or else
        //a forceReload parameter with the value of true would short-circuit the call to super.setDatasetProperties.
        if (super.setDatasetProperties(newDatasetProperties, forceReload, false) | forceReload) {
            initMutableHorizon(super.dataObjects, (HorizonProperties) newDatasetProperties);
            if (redraw) {
                logger.info("Redrawing horizon layer #" + super.getLayerID() + " due to DatasetProperties change or forceReload request.");
                super.geoPlot.redraw(this, true, true);
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void reloadData() {
        String status = ioAdapter.readSummary((HorizonProperties) dsProperties);
        if (!"".equals(status)) {
            throw new RuntimeException("ioAdapter.readSummary() returned '" + status + "' for layer " + super.getLayerID());
        }

        logger.info("Old dataObject count: " + dataObjects.length);
        BhpSuHorizonDataObject[] horizonDOs = ioAdapter.getHorizons((HorizonProperties) dsProperties);
        super.dataObjects = new DataObject[horizonDOs.length];

        for (int i = 0; i < horizonDOs.length; i++) {
            super.dataObjects[i] = (BhpSuHorizonDataObject) horizonDOs[i];
        }

        logger.info("DataObject count after reload: " + horizonDOs.length);
        traceAnnotationMap = new TraceAnnotationMap(dsProperties, horizonDOs);

        numFrames = calcNumFrames();
    }
    
    public ColorMap getColorMap() {
        GeoDataOrder dataOrder = getOrientation();
        switch (dataOrder) {
            case CROSS_SECTION_ORDER :
                throw new UnsupportedOperationException("Cross-section order horizon layer does not support a configurable ColorMap.");
            case MAP_VIEW_ORDER :
                return ((MapDisplayParameters)getDisplayParameters()).getColorAttributes().getColorMap();
            default :
                throw new UnsupportedOperationException("Cannot get ColorMap for SeismicLayer with unknown orientation: " + dataOrder);
        }
    }
}