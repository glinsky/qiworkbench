/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.model;

import com.bhpb.geographics.util.XStreamUtil;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.thoughtworks.xstream.XStream;
import java.io.StringReader;
import org.w3c.dom.Node;

public class MoviePreferenceSettings {
    public enum CAPTURE_OPTION { ENTIRE_DESKTOP, SELECTED_FRAME}
    public enum SAVE_OPTION { SAVE_IN_MEMORY, SAVE_AS_JPEG}
    
    private CAPTURE_OPTION captureOption = CAPTURE_OPTION.ENTIRE_DESKTOP;
    private SAVE_OPTION saveOption = SAVE_OPTION.SAVE_IN_MEMORY;
    private String templateFileName = "";
    private int frameInterval = 500;

    public SAVE_OPTION getSaveOption() {
        return saveOption;
    }

    public void setSaveOption(SAVE_OPTION option) {
        this.saveOption = option;
    }

    public CAPTURE_OPTION getCaptureOption() {
        return captureOption;
    }

    public int getFrameInterval(){
        return frameInterval;
    }
    
    public void setFrameInterval(int interval){
        frameInterval = interval;
    }
    
    public void setCaptureOption(CAPTURE_OPTION option) {
        this.captureOption = option;
    }
    
    public static String getXmlAlias() {
        return "moviePreferenceSettings";
    }

    /**
     * Deserializes a MovieOptionSettings.
     * 
     * @param node w3c.dom.node created from the XML String serialization of PickingSettings
     * 
     * @return the deserialized PickingSettings object
     */
    public static MoviePreferenceSettings restoreState(Node node) {
        XStream xStream = XStreamUtil.getInstance();
        String xml = ElementAttributeReader.nodeToXMLString(node);

        MoviePreferenceSettings settings = (MoviePreferenceSettings) xStream.fromXML(new StringReader(xml));

        return settings;
    }
    
    /**
     * Serializes the state of this <code>PickingSettings</code> as a {@link String}
     * representation of an XML element.
     */
    public String saveState() {
        XStream xStream = XStreamUtil.getInstance();
        return xStream.toXML(this);
    }
    
    public String getTemplateFileName() {
        return templateFileName;
    }

    public void setTemplateFileName(String template) {
        this.templateFileName = template;
    }
    
}