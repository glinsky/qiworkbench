/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.datasystems.UnlimitedKeyRange;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.qiworkbench.api.ITraceHeader;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Given a set of KeyRanges and a number of traces which map to the set,
 * this class calculates the Integer annotation associated with a requested
 * key for a given trace number.
 *
 * Includes convenience methods for returning array of annotations, annotations
 * for scaled views, trace or depth indices for requested annotation base and offset.
 * 
 * This class was originally used for horizontal annotations prior to implementation
 * of ITraceHeader in the qiWb, but is now specifically used to translate trace/sample indices to
 * nearest header key/time/depth values for the purpose of cursor tracking.
 * 
 * While the original purpose of this class has, annotation, been supplanted by trace header entries,
 * TraceAnnotationMap is retained for use by the target info panel since only
 * header key values are shown and horizon xsec layers, if annotated, would lack meaningful trace header keys
 * as present in seismic and model xsecl ayers (although horizon xsec layers are not often annotated, but can be).
 */
public class TraceAnnotationMap {

    DatasetProperties dsProperties;
    DataObject[] dataObjects;
    GeoDataOrder dataOrder;
    Integer numHeaderKeys;
    Integer numMappedTraces;
    Map<String, DiscreteKeyRange> headerKeyRangeMap;
    Map<String, Integer> distinctHeaderKeyValues;
    //primary data structure - a map of header key->value maps by trace index
    //Map<Integer, Map<String, Integer>> traceAnnotationMap;
    String[] headerKeyNames;

    /**
     * Create a map correlating trace indices to descriptive trace annotations
     * for the given dataset.
     * 
     * @param dsProps DatasetProperties containing all header key information
     * necessary to construct the TraceAnnotationMap.
     */
    public TraceAnnotationMap(DatasetProperties dsProps, DataObject[] dataObjects) {
        this.dsProperties = dsProps;

        headerKeyRangeMap = new HashMap<String, DiscreteKeyRange>();

        GeoFileMetadata metadata = dsProps.getMetadata();
        headerKeyNames = metadata.getKeys().toArray(new String[0]);
        dataOrder = metadata.getGeoDataOrder();
        numHeaderKeys = headerKeyNames.length;

        for (String hkName : headerKeyNames) {
            AbstractKeyRange asAbstract = dsProps.getKeyChosenRange(hkName);
            if (asAbstract instanceof DiscreteKeyRange) {
                headerKeyRangeMap.put(hkName, (DiscreteKeyRange) asAbstract);
            } else if (asAbstract instanceof UnlimitedKeyRange) {
                DiscreteKeyRange fullKeyRange = asAbstract.getFullKeyRange();
                DiscreteKeyRange asDiscrete = new DiscreteKeyRange(
                        fullKeyRange.getMin(),
                        fullKeyRange.getMax(),
                        fullKeyRange.getIncr(),
                        fullKeyRange.getKeyType(),
                        fullKeyRange);
                headerKeyRangeMap.put(hkName, asDiscrete);
            } else {
                throw new UnsupportedOperationException(
                        "Only discrete and unlimited chosen key ranges are currently supported by TraceAnnotationMap");
            }
        }

        distinctHeaderKeyValues = calculateDistinctHeaderKeyValues(headerKeyRangeMap);
        numMappedTraces = calculateNumMappedTraces(distinctHeaderKeyValues);

        this.dataObjects = dataObjects;
    //traceAnnotationMap = new HashMap<Integer, Map<String, Integer>>();
    }

    /**
     * Locates the first trace in this TraceAnnotatioMap for which the specified headerKeyValueMap'
     * is a subset of that trace's header key map.  This may only be done if
     * isCompatible() returns true for the given Map.  Otherwise, an Exception will be thrown.
     *
     * @throws IllegalArgumentException if the specified Map does not match the
     * headerKeyValueMap for any trace within this TraceAnnotationMap, or if
     * the specified Map is null or empty.
     *
     * @param headerKeyValueMap a Map of header key names to values which will be matched
     * to a trace contained in this TraceAnnotationMap
     *
     * @return the index of the trace associated with the given header key values if such
     * a trace exists in the dataset
     */
    public Integer calculateTraceIndex(Map<String, Number> headerKeyValueMap) {
        //naive method: perform binary search until the header key values match

        int minIndex = 0;
        //int maxIndex = traceAnnotationMap.size() -1;
        int maxIndex = dataObjects.length - 1;

        int index;
        do {
            index = (maxIndex + minIndex) / 2;
            int order = compareTo(index, headerKeyValueMap);
            if (order == 0) {
                return index;
            } else if (order < 0) {
                maxIndex = index - 1;
            } else if (order > 0) {
                minIndex = index + 1;
            }
        } while (maxIndex >= minIndex);

        throw new IllegalArgumentException("No match found for " + toString(headerKeyValueMap) + " in this TraceAnnotationMap");
    }

    private String toString (Map<String,Number> headerKeyValueMap) {
        StringBuffer sbuf = new StringBuffer();
        for (Entry<String,Number> entry: headerKeyValueMap.entrySet()) {
            sbuf.append(entry.getKey().toString() + ": " + entry.getValue().toString() + " ");
        }
        return sbuf.toString();
    }

    /**
     * Compares the header key values of the specified Map to those of the trace
     * at the given index, using the odrering of header key names in this TraceAnnotationMap.
     * 
     * Only header keys which are in both this TraceAnnotationMap and the parameter headerKeyValueMap
     * are compared.  More than one index may return true for a given headerKeyValueMap.  The isCompatible()
     * method must also be used to determine a meaningful match.
     * 
     * Compare the specified headerKeyValueMap to that of the trace at the given index.
     * For all keys in the headerKeyValueMap, starting with the first, if the value of the
     * Map is less, a negative integer is returned.  If the value is greater, a positive integer is returned.
     * If the values are an exact match, the next header key value is compared.  If all header
     * keys are an exact match, 0 is returned.
     * 
     * @param index
     * @param headerKeyValueMap
     * @return
     */
    private int compareTo(int index, Map<String, Number> headerKeyValueMap) {
        Set<String> mapNameSet = headerKeyValueMap.keySet();
        for (int i = 0; i < headerKeyNames.length; i++) {
            String hkName = headerKeyNames[i];

            //no not compare vs. this TraceAnnotationMap's vertical key name as it
            //represents a trace ID within a dataset and is not useful for synchronization purposes
            if (dataOrder == GeoDataOrder.MAP_VIEW_ORDER && hkName.equals(headerKeyNames[0])) {
                continue; // do not attempt to sync on tracl for map layers; this key
                //is not part of the 2-d coodinate system.
            }
            if (mapNameSet.contains(hkName) && !hkName.equals(getVerticalKeyName())) {
                Number mapValue = headerKeyValueMap.get(hkName);
                Number indexedValue = ((ITraceHeader) dataObjects[index]).getTraceHeaderFieldValue(hkName);

                Double mapValueAsDouble = new Double(mapValue.longValue());
                Double indexedValueAsDouble = new Double(indexedValue.longValue());
                
                int result = mapValueAsDouble.compareTo(indexedValueAsDouble);
                if (result != 0) {
                    return result;
                }
            }
        }
        return 0;
    }


    public Map<String, Number> getHeaderKeyMap(int traceIndex) {
        Map<String, Number> hkValMap = new HashMap<String, Number>();

        for (String hkName : headerKeyNames) {
            hkValMap.put(hkName, ((ITraceHeader)dataObjects[traceIndex]).getTraceHeaderFieldValue(hkName));
        }

        return hkValMap;
    }

    /**
     * Returns true if and only if, for the specified headerKeyValueMap:
     * <ul>
     * <li>every key matches a key of this TraceAnnotationMap</li>
     * <li>every value is within the DiscreteKeyRange to which this TraceAnnotationMap maps the key name<li>
     * </ul>
     *
     * @param headerKeyValueMap
     *
     * @return true if at least one trace in this TraceAnnotationMap's dataset contains
     * the exact values specified in the headerKeyValueMap
     */
    public boolean isCompatible(Map<String, Number> headerKeyValueMap,
            GeoDataOrder dataOrder) {
        Set<String> keyNames = headerKeyValueMap.keySet();
        //Every destination target header key must be a member of the source target's
        //header key set.  Also, its value of each viewtarget header key must be within
        //the chosen ranges of the target layer.  Otherwise, a unique target point could
        //not be determined.  Ther are 2 exceptions to this rule - see below.
        for (String keyName : this.headerKeyRangeMap.keySet()) {
            //Exception #1:The first header key value (tracl) for map layers has a special meaning
            //and is not part of the cursor's coordinate system.
            if (dataOrder == GeoDataOrder.MAP_VIEW_ORDER && keyName.equals(headerKeyNames[0])) {
                continue;
            }

            //Exception #2:The last header key value (tracl) for cross-section layers has a special meaning
            //and is not part of the cursor's coordinate system.
            if (dataOrder == GeoDataOrder.CROSS_SECTION_ORDER && keyName.equals(getVerticalKeyName())) {
                continue;
            }

            if (!keyNames.contains(keyName)) {
                return false;
            }
            DiscreteKeyRange keyRange = this.headerKeyRangeMap.get(keyName);
            Number value = headerKeyValueMap.get(keyName);

            Double valueAsDouble = new Double(value.doubleValue());

            if (valueAsDouble.compareTo(keyRange.getMin()) < 0.0) {
                return false;
            }

            if (valueAsDouble.compareTo(keyRange.getMax()) > 0.0) {
                return false;
            }

            // if the value is not an exact match for a a trace given this key's
            // minimum value and increment, then the headerKeyValueMap is not compatible
            if ((valueAsDouble.intValue() - keyRange.getMin()) % (int) keyRange.getIncr() != 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param name the name of a header key e.g. "ep", "cdp", "offset", "tracl"
     * @return true if and only if the name matches the vertical range key name,
     * which is defined to be the last name in the header key name to range map.
     */
    private boolean isVerticalRangeKeyName(String name) {
        return headerKeyNames[numHeaderKeys.intValue() - 1].equals(name);
    }

    /**
     * Create a map of distinct header key values for all header keys including the vertical range.
     */
    private Map<String, Integer> calculateDistinctHeaderKeyValues(Map<String, DiscreteKeyRange> headerKeyRangeMap) {
        Map<String, Integer> nHeaderKeyValues = new HashMap<String, Integer>(numHeaderKeys.intValue());

        for (int hkNameIndex = 0; hkNameIndex < headerKeyNames.length /*- 1*/; hkNameIndex++) {
            String hkName = headerKeyNames[hkNameIndex];
            DiscreteKeyRange keyRange = headerKeyRangeMap.get(hkName);
            int nDistinctValues = (int) ((keyRange.getMax() - keyRange.getMin()) / keyRange.getIncr()) + 1; // max - min is guaranteed to be divisible by incr

            nHeaderKeyValues.put(hkName, nDistinctValues);
        }

        return nHeaderKeyValues;
    }

    /**
     * Calculate the total number of mapped traces.  Used for bounds checking by  {@link getAnnotation}.
     * The number of distinct values for the vertical range is not used in this calculation, but this header
     * key is allowed in the map so that the same map may be used as a parameter for the method which calculates
     * the annotation for the vertical axis.
     *
     * @param distinctHeaderKeyValues Map of header key names, including vertical range, onto the number of distinct values.
     *
     */
    private Integer calculateNumMappedTraces(Map<String, Integer> distinctHeaderKeyValues) {
        Integer nMappedTraces = Integer.valueOf(1);

        for (Entry<String, Integer> hkEntry : distinctHeaderKeyValues.entrySet()) {
            if (!isVerticalRangeKeyName(hkEntry.getKey())) {
                nMappedTraces = nMappedTraces.intValue() * hkEntry.getValue().intValue();
            }
        }

        return nMappedTraces;
    }

    /**
     * Get the number of header keys including the vertical range.
     */
    public int getNumHeaderKeys() {
        return numHeaderKeys.intValue();
    }

    /**
     * Get the number of traces mapped to the set of header key names and values in the current chosen ranges.
     */
    public int getNumMappedTraces() {
        return numMappedTraces.intValue();
    }

    /**
     * Gets the name of the vertical header key.
     * 
     * @return the vertical header key name or the empty String if this TraceAnnotationMap
     * does not contain at least one header key.
     */
    public String getVerticalKeyName() {
        int nHeaderKeys = headerKeyNames.length;

        if (nHeaderKeys == 0) {
            return "";
        }

        return headerKeyNames[nHeaderKeys - 1];
    }

    /**
     * Return the number of units in a single dataObject, that is, the number
     * of distinct values allowed for the vertical range key multiplid by
     * the sample size.
     */
    public double getNumUnits() {
        return dsProperties.getVerticalCoordMax() - dsProperties.getVerticalCoordMin();
    }
}