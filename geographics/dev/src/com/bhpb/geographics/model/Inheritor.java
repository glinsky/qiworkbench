/*
###########################################################################
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

/**
 * Interface for classes whose instances may inherit properties from any instance
 * of an arbitrary ancestor class.
 *
 * @author folsw9
 * 
 * @param <HEIR>
 * @param <ANCESTOR>
 */
public interface Inheritor<HEIR, ANCESTOR> {
    /**
     * Constructs and returns a new HEIR using logic and properties determined
     * by the type and fields of the ANCESTOR.  If the implementor cannot inherit
     * from ancestor due to type or logic constraints, or if ancestor is null,
     * then a reference to the inheritor itself
     * or a copy of the inheritor may be returned.
     *
     * @param ancestor an Object which serves as a template which is
     * combined with this Inheritor's fields to produce an HEIR
     *
     * @return this Inheritor's HEIR as derived from the specified ANCESTOR and
     * the Inheritor itself
     */
    public HEIR inheritFrom(ANCESTOR ancestor);
}
