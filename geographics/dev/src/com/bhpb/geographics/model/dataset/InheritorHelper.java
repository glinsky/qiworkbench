/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model.dataset;

import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.ArbTravKeyRange;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.datasystems.ListKeyRange;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import java.util.ArrayList;
import java.util.logging.Logger;

public class InheritorHelper {
    private static final Logger logger =
            Logger.getLogger(InheritorHelper.class.getName());

    private GeoFileMetadata metadata;

    public InheritorHelper(GeoFileMetadata metadata) {
        this.metadata = metadata;
    }

    private boolean isCompatible(String keyName, ArbTravKeyRange keyrange) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private boolean isCompatible(String keyName, ListKeyRange keyrange) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private boolean isCompatible(String keyName, DiscreteKeyRange keyRange) {
        if (!metadata.getKeys().contains(keyName)) {
            return false;
        }

        DiscreteKeyRange fullKeyRange = metadata.getKeyRange(keyName);
        if (outOfRange(fullKeyRange, keyRange)) {
                return false;
        }

        if (keyRange.getIncr() < fullKeyRange.getIncr()) {
            return false;
        }

        //is chosen incr a multiple of full keyrange incr?
        if (!isMultiple(keyRange.getIncr(), fullKeyRange.getIncr())) {
            return false;
        }
        return true;
    }

    private boolean outOfRange(DiscreteKeyRange fullKeyRange, DiscreteKeyRange chosenKeyRange) {
        return (   outOfRange(fullKeyRange, chosenKeyRange.getMin())
                || outOfRange(fullKeyRange, chosenKeyRange.getMax())
               );
    }

    private boolean outOfRange(DiscreteKeyRange keyRange, double value) {
        if (value < keyRange.getMin()) {
            return true;
        }

        if (value > keyRange.getMax()) {
            return true;
        }

        if (!isMultiple(value - keyRange.getMin(), keyRange.getIncr())) {
            return true;
        }

        return false;
    }

    boolean isMultiple(double dividend, double divisor) {
        return Math.IEEEremainder(dividend, divisor) <= Double.MIN_VALUE;
    }

    public void inheritKeyChosenRanges(DatasetProperties ancestorProperties,
            DatasetProperties heirProperties) {
        if (ancestorProperties == null) {
            return;
        }

        GeoFileMetadata thisMeta = heirProperties.getMetadata();
        GeoFileMetadata ancestorMeta = ancestorProperties.getMetadata();

        //data order must be == for inheritance to occur
        if (thisMeta.getGeoDataOrder() != ancestorMeta.getGeoDataOrder()) {
            return;
        }

        logger.info("Commencing inheritance for " + thisMeta.getGeoDataOrder() + " layer.");
        ArrayList<String> ancestorKeys = ancestorMeta.getKeys();
        ArrayList<String> theseKeys = thisMeta.getKeys();

        for (String ancestorKey : ancestorKeys) {
            AbstractKeyRange ancestorKeyRange;
            if (ancestorProperties.isArbTrav(ancestorKey)) {
                ancestorKeyRange = ancestorProperties.getKeyChosenArbTrav(ancestorKey);
            } else {
                ancestorKeyRange = ancestorProperties.getKeyChosenRange(ancestorKey);
            }
            if (!theseKeys.contains(ancestorKey)) {
                continue;
            }
            DiscreteKeyRange fullKeyRange = thisMeta.getKeyRange(ancestorKey);

            if (ancestorKeyRange.isArbTrav() && isCompatible(ancestorKey, (ArbTravKeyRange)ancestorKeyRange)) {
                int[] arbTravs = ((ArbTravKeyRange) ancestorKeyRange).getArbTravs();
                heirProperties.setKeyChosenArbTrav(ancestorKey, new ArbTravKeyRange(arbTravs, fullKeyRange));
            } else if (ancestorKeyRange.isDiscrete()) {
                DiscreteKeyRange asDiscrete = (DiscreteKeyRange) ancestorKeyRange;
                heirProperties.setKeyChosenRange(ancestorKey,
                        new DiscreteKeyRange(asDiscrete.getMin(),
                        asDiscrete.getMax(),
                        asDiscrete.getIncr(),
                        asDiscrete.getKeyType(),
                        fullKeyRange));
            } else if (ancestorKeyRange.isList()) {
                int[] listVals = ((ListKeyRange) ancestorKeyRange).getListVals();
                heirProperties.setKeyChosenRange(ancestorKey, new ListKeyRange(listVals, fullKeyRange));
            }
        }
    }
}