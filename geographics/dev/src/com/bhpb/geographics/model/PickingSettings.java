/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import com.bhpb.geographics.util.XStreamUtil;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.thoughtworks.xstream.XStream;
import java.io.StringReader;
import org.w3c.dom.Node;

/**
 * Simple model class for Picking-related settings.
 */
public class PickingSettings {
    /* Ways in which picking can be invoked */

    public enum PICKING_MODE {

        MOUSE_DRAG, MOUSE_CLICK
    };
    /* different snapping modes: none (draw), nearest min, max and zero */

    public enum SNAPPING_MODE {

        NO_SNAP, SNAP_NEAREST_MAX, SNAP_NEAREST_MIN, SNAP_NEAREST_ZERO
    };
    /* default limit for snapping is 20 pixels from mouse coordinates */
    private static final Integer DEFAULT_SNAP_LIMIT = 20;
    private Integer snapLimit = DEFAULT_SNAP_LIMIT;
    private PICKING_MODE pickingMode = PICKING_MODE.MOUSE_DRAG;
    private SNAPPING_MODE snappingMode = SNAPPING_MODE.NO_SNAP;
    
    public PICKING_MODE getPickingMode() {
        return pickingMode;
    }

    public int getSnapLimit() {
        return snapLimit;
    }
    
    public SNAPPING_MODE getSnappingMode() {
        return snappingMode;
    }
    
    public static String getXmlAlias() {
        return "pickingSettings";
    }
    
    /**
     * Deserializes a PickingSettings.
     * 
     * @param node w3c.dom.node created from the XML String serialization of PickingSettings
     * 
     * @return the deserialized PickingSettings object
     */
    public static PickingSettings restoreState(Node node) {
        XStream xStream = XStreamUtil.getInstance();
        String xml = ElementAttributeReader.nodeToXMLString(node);

        PickingSettings pickingSettings = (PickingSettings) xStream.fromXML(new StringReader(xml));

        return pickingSettings;
    }
    
    /**
     * Serializes the state of this <code>PickingSettings</code> as a {@link String}
     * representation of an XML element.
     */
    public String saveState() {
        XStream xStream = XStreamUtil.getInstance();
        return xStream.toXML(this);
    }

    public void setPickingMode(PICKING_MODE pickingMode) {
        this.pickingMode = pickingMode;
    }
        
    public void setSnapLimit(int snapLimit) {
        this.snapLimit = snapLimit;
    }

    public void setSnappingMode(SNAPPING_MODE snappingMode) {
        this.snappingMode = snappingMode;
    }
}