/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import com.bhpb.geoio.datasystems.AbstractKeyRange;
import java.util.HashSet;
import java.util.Set;

public class KeyRangeParseResult {

    String parserErrors = "";
    String syntaxErrors = "";
    String compatibilityErrors = "";
    String intParseErrors = "";
    AbstractKeyRange keyRange;

    public enum ErrorType {

        NONE, PARSER, INT_CONVERSION, COMPATIBILITY, SYNTAX
    };
    //Yes, I realize this can be done with bitwise or and static integers.
    //But doing it this way decouples it from old or broken code that might use
    //the wrong integer values.
    private Set<ErrorType> errorFlags = new HashSet<ErrorType>();

    public String getErrorMessage() {
        StringBuilder sb = new StringBuilder(parserErrors);
        if ("".equals(compatibilityErrors) == false) {
            if ("".equals(sb.toString()) == false) {
                sb.append("\n");
            }
            sb.append(compatibilityErrors);
        }
        if ("".equals(intParseErrors) == false) {
            if ("".equals(sb.toString()) == false) {
                sb.append("\n");
            }
            sb.append(intParseErrors);
        }
        if ("".equals(syntaxErrors) == false) {
            if ("".equals(sb.toString()) == false) {
                sb.append("\n");
            }
            sb.append(syntaxErrors);
        }

        return sb.toString();
    }

    /**
     * Returns true if this KeyRangeParseResult contains exactly the ErrorTypes
     * given in the paramater list.
     * Note that hasErrors(ErrorType.NONE) is equivalent to !hasErrors() (no arg).
     */
    public boolean hasErrors(ErrorType... errors) {
        boolean hasErrors = true;

        for (ErrorType error : errors) {
            hasErrors &= errorFlags.contains(error);
        }

        return hasErrors;
    }

    KeyRangeParseResult(AbstractKeyRange keyRange) {
        this.keyRange = keyRange;
        errorFlags.add(ErrorType.NONE);
    }

    KeyRangeParseResult(String parserErrors, String compatibilityErrors, String intParseErrors,
            String syntaxErrors) {
        this.parserErrors = parserErrors;
        this.compatibilityErrors = compatibilityErrors;
        this.intParseErrors = intParseErrors;
        this.syntaxErrors = syntaxErrors;

        if (parserErrors == null) {
            throw new IllegalArgumentException("parserErrors string cannot be null");
        }

        if (compatibilityErrors == null) {
            throw new IllegalArgumentException("compatibilityErrors string cannot be null");
        }

        if (intParseErrors == null) {
            throw new IllegalArgumentException("intParseErrors string cannot be null");
        }
        if (syntaxErrors == null) {
            throw new IllegalArgumentException("syntaxErrors string cannot be null");
        }

        if ("".compareTo(parserErrors) != 0) {
            errorFlags.add(ErrorType.PARSER);
        }

        if ("".compareTo(compatibilityErrors) != 0) {
            errorFlags.add(ErrorType.COMPATIBILITY);
        }

        if ("".compareTo(intParseErrors) != 0) {
            errorFlags.add(ErrorType.INT_CONVERSION);
        }

        if ("".compareTo(syntaxErrors) != 0) {
            errorFlags.add(ErrorType.SYNTAX);
        }
        if (errorFlags.size() == 0) {
            throw new IllegalArgumentException("Cannot create KeyRangeParseResult " +
                    "using this constructor if parseErrors, compatibilityErrors and intParseErrors strings are all empty.");
        }
    }

    public String getParserErrors() {
        return parserErrors;
    }

    public String getCompatibilityErrors() {
        return compatibilityErrors;
    }

    public String getIntParseErrors() {
        return intParseErrors;
    }

    public String getSyntaxErrors() {
        return syntaxErrors;
    }

    /**
     * Returns true if the set of errorFlags contains at least one value and it is not ErrorType.NONE.
     * Note that all constructors create a valid set of ErrorTypes containing either ErrorType.NONE, or
     * else one or more other ErrorTypes.
     */
    public boolean hasErrors() {
        return (errorFlags.size() > 0 && !(errorFlags.contains(ErrorType.NONE)));
    }

    public AbstractKeyRange getKeyRange() {
        return keyRange;
    }
}