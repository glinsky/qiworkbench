/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import com.bhpb.geographics.model.Annotation.AXIS;
import com.bhpb.geographics.util.DecimalFormatter;
import com.bhpb.geoio.datasystems.DataObject;
import java.util.logging.Logger;

/**
 * This class was initially used to generate vertical annotation tick values
 * for all layer types except for horizon cross sections.  Following changes to GeoIO
 * which resulted in multiple HorizonTraces being returned from a single READ_GEOFILE_CMD,
 * and additional changes to DatasetProperties to return min/max amplitude as vertical coordinates
 * for horizon xsec datasets, this class is now capable of generating annotation ticks
 * for all layer types.  Consequently, the previous alternate AnnotationTickGenerator
 * implementation, HorizonXsecAnnotationTickGenerator, has been removed and this class
 * should be used in its place.
 * 
 * @author folsw9
 */
public class DefaultAnnotationTickGenerator implements AnnotationTickGenerator {
    private static final Logger logger =
            Logger.getLogger(DefaultAnnotationTickGenerator.class.getName());

    private static final DecimalFormatter formatter = DecimalFormatter.getInstance(10);

    public int getNumTicks(AXIS axis, Layer annotatedLayer,
            double tickScale, DataObject[] dataObjects, double base) {
        switch (axis) {
            case Y_AXIS:
                //for non-horizon-xsec layers, use the dsProperties min/max t/d
                double minValue = annotatedLayer.getVerticalMin();
                double maxValue = annotatedLayer.getVerticalMax();

                return (int) ((maxValue - minValue) / tickScale) + 1;
            case X_AXIS:
                return (int) ((dataObjects.length - base) / tickScale) + 1;
            default:
                throw new IllegalArgumentException("Unknown axis: " + axis);
        }
    }

    private String[] getPlaceHolderCaptions(int nTicks) {
        String[] placeHolderCaptions = new String[nTicks];
        for (int i = 0; i < placeHolderCaptions.length; i++) {
            placeHolderCaptions[i] = "?";
        }
        return placeHolderCaptions;
    }

    public String[] getCaptions(AXIS axis,
            Layer annotatedLayer,
            double tickScale,
            int nTicks,
            DataObject[] dataObjects,
            double base,
            String headerKey) {

        String[] tickCaptions = getPlaceHolderCaptions(nTicks);
        int tickIndex = 0;

        switch (axis) {
            case Y_AXIS:
                double minValue = annotatedLayer.getVerticalMin();
                double maxValue = annotatedLayer.getVerticalMax();

                for (double currentDepth = minValue; currentDepth <= maxValue /* && tickIndex < tickCaptions.length */; currentDepth += tickScale) {
                    tickCaptions[tickIndex] = formatter.getFormattedString(currentDepth, tickScale);
                    tickIndex++;
                }
                break;
            case X_AXIS:
                for (double traceIndex = base; (int) Math.round(traceIndex) < dataObjects.length; traceIndex += tickScale) {
                    int roundedIndex = (int) Math.round(traceIndex);
                    DataObject dataObject = dataObjects[roundedIndex];
                    if (dataObject.hasTraceHeader()) {
                        String keyValueString = dataObjects[roundedIndex].getTraceHeader().getTraceHeaderFieldValue(headerKey).toString();
                        String formattedKeyValueString;
                        try {
                            Double keyValueDouble = Double.valueOf(keyValueString);
                            // for horiz. key tick captions, show up to 2 decimal places but omit trailing zeroes
                            formattedKeyValueString = formatter.getFormattedString(keyValueDouble, 2, false);
                        } catch (Exception ex) {
                            logger.fine("While converting header key '" + headerKey + "' value String '" +
                                    keyValueString + "' to Double, caught: " + ex.getMessage() + "; using unformatted String.");
                            formattedKeyValueString = keyValueString;
                        }
                        
                        tickCaptions[tickIndex] = formattedKeyValueString;
                    } else {
                        tickCaptions[tickIndex] = "?";
                    }
                    tickIndex++;
                }

                break;
            default:
                throw new IllegalArgumentException("Unknown axis: " + axis);
        }
        return tickCaptions;
    }
}