/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;
import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.Comparator;

/**
 * Comparator which sorts objects which extend Point2D in order of their X coordinates.
 * Points which have the same X coordinate are equivalent under this Comparator
 * regardless of Y coordinate.
 */
class Point2Dcomparator implements Comparator<Point2D>, Serializable {
	public int compare(Point2D pointA, Point2D pointB) {
		double a = pointA.getX();
		double b = pointB.getX();
		if (a < b)
			return -1;
		else if (a > b)
			return 1;
		else
			return 0;
	}
}
