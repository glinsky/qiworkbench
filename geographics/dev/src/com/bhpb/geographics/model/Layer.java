/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geographics.model.layer.display.LocalSubsetProperties;
import com.bhpb.geographics.controller.EditorResultProcessor;
import com.bhpb.geographics.controller.LayerProperties;
import com.bhpb.geographics.controller.layer.LayerSyncBroadcaster;
import com.bhpb.geographics.controller.layer.LayerSyncGroup;
import com.bhpb.geographics.controller.layer.LayerSyncListener;
import com.bhpb.geographics.model.layer.display.LayerDisplayParameters;
import com.bhpb.geographics.model.layer.shared.BasicLayerProperties;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties;
import com.bhpb.geographics.model.window.CrossSectionAnnotationProperties;
import com.bhpb.geographics.model.window.MapAnnotationProperties;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.model.window.WindowPropertyGroup;
import com.bhpb.geographics.model.window.WindowScaleProperties;
import com.bhpb.geographics.model.window.WindowSyncProperties;
import com.bhpb.geographics.rasterizer.LayeredRenderableImageProducer;
import com.bhpb.geographics.ui.AbstractGeoPlot;
import com.bhpb.geographics.ui.event.ArbTravEvent;
import com.bhpb.geographics.ui.layerlist.LayerListPanel;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.ArbTravKeyRange;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.SeismicProperties;
import com.bhpb.geoio.util.GeoIOAdapter;
import java.awt.Dimension;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public abstract class Layer implements Serializable,
        EditorResultProcessor<GeoGraphicsPropertyGroup>,
        LayerSyncBroadcaster, LayerSyncListener {

    protected static transient final Logger logger =
            Logger.getLogger(Layer.class.toString());
    /** 
     * Special layer id used for methods which take a layerID to indicate
     *  that the parameter designates not-a-layer, deselecting of any layers, etc.
     */
    public static final String NaL = "Not a Layer";
    public static final int DEFAULT_WIGGLE_DECIMATION = 10;
    protected AbstractGeoPlot geoPlot;
    protected boolean arbTravEnabled = false;
    protected DataObject[] dataObjects;
    protected DatasetProperties dsProperties;
    protected GeoIOAdapter ioAdapter;
    protected int numFrames;
    protected TraceAnnotationMap traceAnnotationMap;
    protected final Collection<ChangeListener> listeners = new ArrayList<ChangeListener>();
    private static AtomicInteger GUID = new AtomicInteger(1);
    private ArbitraryTraverse arbTrav;
    private volatile boolean loadingData = false;
    private int currentFrame = 0;
    private LayerDisplayParameters layerDisplayParams;
    private LayerListPanel listPanel;
    protected LayerSyncGroup layerSyncGroup;
    private LayerSyncProperties syncProps;
    private LayeredRenderableImageProducer rasterizer;
    private LocalSubsetProperties subsetProps;
    private GeoDataOrder orientation;
    private String layerID = Integer.valueOf(GUID.getAndAdd(1)).toString();
    private String prefix = "NULL PREFIX";
    private String layerName = "NULL PREFIX #? : NULL FILENAME";
    private transient WindowModel windowModel;

    public Layer(DatasetProperties dsProperties,
            LayerDisplayParameters layerDisplayParams,
            LocalSubsetProperties subsetProps,
            LayerSyncProperties syncProps,
            GeoIOAdapter ioAdapter,
            String prefix,
            DataObject[] dataObjects,
            String layerID,
            String layerName,
            boolean allowArbTrav,
            ArbitraryTraverse arbTrav,
            LayerSyncGroup layerSyncGroup,
            WindowModel windowModel) {
        validateParams(dsProperties, dataObjects, layerDisplayParams);

        this.dsProperties = dsProperties;
        this.layerDisplayParams = layerDisplayParams;
        this.subsetProps = subsetProps;
        this.syncProps = syncProps;
        this.ioAdapter = ioAdapter;
        this.prefix = prefix;
        this.layerName = layerName;
        this.layerSyncGroup = layerSyncGroup;
        this.windowModel = windowModel;
        //Make an array copy to avoid mutable internal state
        this.dataObjects = new DataObject[dataObjects.length];
        System.arraycopy(dataObjects, 0, this.dataObjects, 0, dataObjects.length);

        this.layerID = layerID;
        Integer layerIDval;
        try {
            layerIDval = Integer.valueOf(layerID);
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException("Unable to instantiate layer: " + layerID + " is not a valid Integer");
        }
        if (layerIDval < GUID.get()) {
            logger.info("Setting layerID as requested: " + layerID + " may not be unique.");
        } else {
            //increment GUID 
            GUID = new AtomicInteger(layerIDval + 1);
        }

        traceAnnotationMap = new TraceAnnotationMap(dsProperties, dataObjects);
        numFrames = calcNumFrames();

        orientation = dsProperties.getMetadata().getGeoDataOrder();

        initArbTrav(allowArbTrav, arbTrav);
    }

    public Layer(DatasetProperties dsProperties,
            LayerDisplayParameters layerDisplayParams,
            LocalSubsetProperties subsetProps,
            LayerSyncProperties syncProps,
            GeoIOAdapter ioAdapter,
            String prefix,
            DataObject[] dataObjects,
            boolean allowArbTrav,
            LayerSyncGroup layerSyncGroup,
            WindowModel windowModel) {
        this.dsProperties = dsProperties;
        this.layerDisplayParams = layerDisplayParams;
        this.subsetProps = subsetProps;
        this.syncProps = syncProps;
        this.ioAdapter = ioAdapter;
        this.prefix = prefix;
        this.layerSyncGroup = layerSyncGroup;
        this.windowModel = windowModel;

        //Make an array copy to avoid mutable internal state
        this.dataObjects = new DataObject[dataObjects.length];
        System.arraycopy(dataObjects, 0, this.dataObjects, 0, dataObjects.length);

        setLayerName(layerDisplayParams.getLayerName());
        traceAnnotationMap = new TraceAnnotationMap(dsProperties, dataObjects);
        numFrames = calcNumFrames();

        orientation = dsProperties.getMetadata().getGeoDataOrder();

        initArbTrav(allowArbTrav, null);
    }

    public void broadcastArbTrav() {
        ArbTravEvent arbTravEvent = new ArbTravEvent(
                this,
                getArbitraryTraverse());
        layerSyncGroup.broadcast(arbTravEvent);
    }

    public void handleArbitraryTraverse(ArbTravEvent ate) {
        ArbitraryTraverse evtArbTrav = ate.getArbTrav();

        Map<String, ArbTravKeyRange> headerKeyMap = evtArbTrav.getHeaderKeyMap();


        if (!this.isSeismicLayer()) {
            return; // only seismic layers can handle arbtrav events
        }

        if (dsProperties.getMetadata().getGeoDataOrder() != GeoDataOrder.CROSS_SECTION_ORDER) {
            return; // only xsec layers can handle arbtrav events
        }

        if (dsProperties.getMetadata().getNumKeys() != 3) {
            return; // only cube seismic xsec datasets can handle arbtrav events
        }

        List<String> keys = dsProperties.getMetadata().getKeys();

        //finally, the source header keys must correspond to the horizontal header keys
        //of this seismic xsec layer
        if (!headerKeyMap.containsKey(keys.get(0))) {
            return;
        }
        if (!headerKeyMap.containsKey(keys.get(1))) {
            return;
        }

        DatasetProperties dsPropsCopy = new SeismicProperties((SeismicProperties) dsProperties);

        dsPropsCopy.setKeyChosenRanges(headerKeyMap);
        setDatasetProperties(dsPropsCopy);
        geoPlot.redraw(this, true, true);
    }

    /**
     * Initializes the Layer's ArbitraryTraverse.  If allowArbTrav is false,
     * the ArbitraryTraverse is null.  If allowArbTrav is true and arbTrav is non-null
     * then the layer's ArbitraryTraverse is set to arbTrav, and the arbTrav.layer field
     * is set to this Layer.
     * If arbTrav is null,
     * then a new ArbitraryTraverse is constructed for the layer.
     *
     * @param allowArbTrav true if the Layer is a Map which should allow Arbitrary Traverse
     *
     * @param arbTrav non-null if a preset ArbitraryTravers is used to initialize the Layer,
     * for example, from XML deserialization
     */
    private void initArbTrav(boolean allowArbTrav, ArbitraryTraverse arbTrav) {
        arbTravEnabled = allowArbTrav;
        if (allowArbTrav) {
            if (arbTrav == null) {
                this.arbTrav = new ArbitraryTraverse(this);
            } else {
                this.arbTrav = arbTrav;
                arbTrav.setLayer(this);
            }
        } else {
            arbTrav = null;
        }
    }

    public DataObject[] getDataObjects() {
        DataObject[] wtCopy = new DataObject[dataObjects.length];
        System.arraycopy(dataObjects, 0, wtCopy, 0, dataObjects.length);
        return wtCopy;
    }

    public GeoIOAdapter getIOadapter() {
        return ioAdapter;
    }

    public void addChangeListener(ChangeListener listener) {
        listeners.add(listener);
    }

    protected void broadcastChangeEvent() {
        ChangeEvent e = new ChangeEvent(this);
        for (ChangeListener listener : listeners) {
            listener.stateChanged(e);
        }
    }

    private String buildLayerName(String baseLayerName) {
        return prefix + " " + layerID + " : " + baseLayerName;
    }

    public void decrementFrame() {
        logger.info("Invoking stub decrementFrame...");
    }

    /**
     * Gets the max amplitude for the purposes of color interpolation.  If the layer
     * is a map, the configured colormap maximum is returned.  Otherwise, the dataset's
     * maximum amplitude is returned since cross-sections do not support a configurable
     * colormap maximum.
     * 
     * @return double maximum amplitude
     */
    public abstract double getAmplitudeMax();

    /**
     * Gets the min amplitude for the purposes of color interpolation.  If the layer
     * is a map, the configured colormap minimum is returned.  Otherwise, the dataset's
     * minimum amplitude is returned since cross-sections do not support a configurable
     * colormap minimum.
     * 
     * @return double minimum amplitude
     */
    public abstract double getAmplitudeMin();

    public ArbitraryTraverse getArbitraryTraverse() {
        return arbTrav;
    }

    public int getCurrentFrame() {
        return currentFrame;
    }

    public DatasetProperties getDatasetProperties() {
        return dsProperties;
    }

    public String getLayerName() {
        return this.layerName;
    }

    /**
     * Get all the properties of the layer, wrapped in a LayerProperties object.
     * Until the the LayerProperties is changed to Transaction<LayerProperties>,
     * the type of LayerProperties returned is 'accepted', but this is
     * meaningless in this context.
     * 
     * @return LayerProperties associated with this Layer.
     */
    public LayerProperties getLayerProperties() {
        LayerDisplayProperties ldProperties =
                new LayerDisplayProperties(subsetProps, layerDisplayParams, syncProps);
        return LayerProperties.createUserAcceptedResult(dsProperties, ldProperties);
    }

    public int getNumFrames() {
        return numFrames;
    }

    public GeoDataOrder getOrientation() {
        return orientation;
    }

    public void handleMouseDrag(ViewTarget targetProps) {
        logger.warning("Method should only be invoked on Layer subclasses.  Possible incorrect actionMode for the active Layer type.");
    }

    public boolean hasArbitraryTraverse() {
        if (arbTravEnabled) {
            return arbTrav.isDefined();
        } else {
            return false;
        }
    }

    public boolean isArbitraryTraverseEnabled() {
        return arbTravEnabled;
    }

    /**
     * Sets the layer name to the format "LAYERID : GEOFILENAME".
     * If the specified geoFileName is null, the layer name is
     * "LAYERID : NULL GEOFILE NAME";
     * 
     * Access is protected because some subclasses such as HorizonLayer do not use
     * the base GeoFileName as the layer Name.
     * 
     * @param baseLayerName Name of the data file as read from DatasetProperties.getMetadata()
     */
    protected void setLayerName(String baseLayerName) {
        if (baseLayerName == null) {
            baseLayerName = "NULL LAYER NAME";
        }

        layerName = buildLayerName(baseLayerName);
        this.layerDisplayParams.setLayerName(baseLayerName);
    }

    /**
     * Invokes setDatasetProperties(newDatasetProperties,false).  
     */
    public boolean setDatasetProperties(DatasetProperties newDatasetProperties) {
        return setDatasetProperties(newDatasetProperties, false, true);
    }

    /**
     * Updates this Layer's DatasetProperties.
     *
     * Update the layer's dataset properties, possibly causing the layer to reload
     * its data.  Because this setter is a method of the base class Layer,
     * it must check to ensure that dsProperties is an instance of the correct
     * subclass of DatasetProperties.
     *
     * If forceReload is true, the associated Summary and DataObjects will always be reloaded,
     * even if dsProperties is equal to the Layer's current DatasetProperties or even a reference
     * to the same Object.
     *
     * If this.dsProperties and dsProperties are not both instances of
     * SeismicProperties, HorizonProperties or ModelProperties, an
     * IllegalArgumentException will be thrown.
     *
     * If forceReload is set to true, the layer will also be re-rasterized for immediate effect.
     *
     * @param newDatasetProperties new DatasetProperties for this Layer.
     * @param forceReload if true, the assoc. summary and dataObjects will always be reloaded. 
     * @param redraw
     *
     * @return true if the new properties differ from the current properties, or false otherwise
     */
    public synchronized boolean setDatasetProperties(DatasetProperties newDatasetProperties, boolean forceReload, boolean redraw) {

        if (!this.dsProperties.getClass().equals(dsProperties.getClass())) {
            throw new IllegalArgumentException("Cannot set Layer.dsPropertise to instance of: " + dsProperties.getClass().getSimpleName() + " because this layer is currently assigned dsProperties that are " + this.dsProperties.getClass().getSimpleName());
        }

        loadingData = true;

        DatasetProperties oldDatasetProperties = dsProperties;
        dsProperties = newDatasetProperties;

        //Requirements change - do not check for equality, but optionally reload summary and dataObject[] on any call to this method
        //the qiViewer ControlSessionManager gets a new reference to the Layer's DatasetProperties between setting
        //new chosen range(s) and invoking this method, and thus depends on access to the Layer/LayerProperties' internal state.
        //The recommended usage is to create a copy of the DatasetProperties, set fields as desired, then invoke this method with
        //the copy as a parameter, as in the qiViewer LayerPropertiesEditor controller classes.

        boolean propertyChangeDetected = !dsProperties.equals(oldDatasetProperties);
        if (forceReload || propertyChangeDetected) {
            reloadData();
        }

        loadingData = false;

        if (redraw && (forceReload || propertyChangeDetected)) {
            logger.info("Redrawing layer #" + layerID + " due to DatasetProperties change or forceReload request.");
            geoPlot.redraw(this, true, true);
        }

        return propertyChangeDetected;
    }

    protected void reloadData() {
        String status = ioAdapter.readSummary(dsProperties);
        if (!"".equals(status)) {
            throw new RuntimeException("ioAdapter.readSummary() returned '" + status + "' for layer " + layerID);
        }

        logger.info("Old dataObject count: " + dataObjects.length);
        dataObjects = ioAdapter.getTraces(dsProperties);
        logger.info("DataObject count after reload: " + dataObjects.length);
        traceAnnotationMap = new TraceAnnotationMap(dsProperties, dataObjects);

        numFrames = calcNumFrames();
    }

    /**
     * Update the full set of layer properties and redraws the associated GeoPlot.
     * This will also cause a new dataset
     * to be read based on the revised datasetProperties. if any.
     * 
     * @param layerProps
     */
    public void setLayerProperties(LayerProperties layerProps) {
        setDatasetProperties(layerProps.getDatasetProperties());

        LayerDisplayProperties ldProps = layerProps.getLayerDisplayProperties();
        subsetProps = ldProps.getLocalSubsetProperties();
        layerDisplayParams = ldProps.getLayerDisplayParameters();
        syncProps = ldProps.getLayerSyncProperties();

        geoPlot.redraw(this, true, true);
        if (listPanel != null) {
            listPanel.repaint();
        }
    }

    /**
     * Calculates the number of Frames for increment/decrement based on the current dsProps.
     *
     * @throws IllegalArgumentException if any of the chosen key ranges are associated with null
     * full key ranges.
     *
     * @return the number of frames availabe for layer increment/decrement
     */
    protected int calcNumFrames() {
        int newNumFrames = 0;
        for (String keyName : dsProperties.getMetadata().getKeys()) {
            if (!dsProperties.isArbTrav(keyName)) {
                AbstractKeyRange keyRange = dsProperties.getKeyChosenRange(keyName);
                //only discreteKeyRanges may be incremented and then only if synchronize is checked
                if (dsProperties.getKeySync(keyName) && keyRange instanceof DiscreteKeyRange) {
                    DiscreteKeyRange fullRange = keyRange.getFullKeyRange();
                    if (fullRange == null) {
                        throw new IllegalArgumentException("DatasetProperties contains a chosenKeyRange with a null fullKeyRange for key: " + keyName);
                    }
                    int nSteps = ((int) (fullRange.getMax() - fullRange.getMin())) / ((int) (fullRange.getIncr()));
                    newNumFrames = Math.max(numFrames, nSteps);
                } else {
                    continue; //only DiscreteKeyRange can be incremented
                }
            }
        }
        return newNumFrames;
    }

    public LayerDisplayParameters getDisplayParameters() {
        return layerDisplayParams;
    }

    public LayerDisplayProperties getDisplayProperties() {
        return new LayerDisplayProperties(subsetProps, layerDisplayParams, syncProps);
    }

    public LocalSubsetProperties getLocalSubsetProperties() {
        return subsetProps;
    }

    /*
    public LayerPanel getLayerPanel() {
    return layerPanel;
    }
     */
    public Dimension getSize() {
        WindowScaleProperties scaleProps = windowModel.getWindowProperties().getScaleProperties();

        double width = scaleProps.getHorizontalPixelIncr() * getWidth();
        double height = scaleProps.getVerticalPixelIncr() * getHeight();

        BasicLayerProperties basicProps = layerDisplayParams.getBasicLayerProperties();
        return new Dimension((int) (width * basicProps.getHorizontalScale()),
                (int) (height * basicProps.getVerticalScale()));
    }

    public LayerSyncProperties getSyncProperties() {
        return syncProps;
    }

    public TraceAnnotationMap getTraceAnnotationMap() {
        return traceAnnotationMap;
    }

    public WindowModel getWindowModel() {
        return windowModel;
    }

    public boolean isActive() {
        return this == windowModel.getActiveLayer();
    }

    /**
     * Returns true if and only if this Layer's name matches the annotated layer name
     * of the associated window's WindowProperties' annotation Properties.
     * @return boolean
     */
    public boolean isAnnotated() {
        //if this is null, no layer is annotated yet
        return this == windowModel.getAnnotatedLayer();
    }

    public int getZorder() {
        return windowModel.getZorder(this);
    }

    public synchronized void shiftFrame(int increment) {
        loadingData = true;
        if (currentFrame < numFrames && dsProperties instanceof SeismicProperties) {
            currentFrame++;
            SeismicProperties newSeismicProps = new SeismicProperties((SeismicProperties) dsProperties);
            for (String keyName : dsProperties.getMetadata().getKeys()) {
                if (!dsProperties.isArbTrav(keyName)) {
                    AbstractKeyRange keyRange = dsProperties.getKeyChosenRange(keyName);
                    //only discreteKeyRanges may be incremented and then only if synchronize is checked
                    if (dsProperties.getKeySync(keyName) && keyRange instanceof DiscreteKeyRange) {
                        DiscreteKeyRange asDiscrete = (DiscreteKeyRange) keyRange;
                        double frameShift = increment * asDiscrete.getIncr();
                        if (asDiscrete.getMin() != asDiscrete.getMax()) {
                            logger.warning("Cannot increment - chosen range is not a single value");
                        } else if (asDiscrete.getMin() + frameShift > dsProperties.getMetadata().getKeyRange(keyName).getMax()) {
                            logger.warning("Cannot increment - current chosen min/max + positive incr is > metadata key max");
                        } else if (asDiscrete.getMin() + frameShift < dsProperties.getMetadata().getKeyRange(keyName).getMin()) {
                            logger.warning("Cannot decrement - current chosen min/max + negative incr is < metadata key min");
                        } else {
                            double newMinMax = asDiscrete.getMin() + frameShift;
                            newSeismicProps.setKeyChosenRange(keyName, new DiscreteKeyRange(newMinMax,
                                    newMinMax, asDiscrete.getIncr(), asDiscrete.getKeyType(), asDiscrete.getFullKeyRange()));
                        }
                    } else {
                        continue; //only DiscreteKeyRange can be incremented
                    }
                }
            }
            setDatasetProperties(newSeismicProps);
        }
        loadingData = false;
        geoPlot.redraw(this, true, false);
    }

    public boolean isLoadingData() {
        return loadingData;
    }

    public boolean isHidden() {
        return layerDisplayParams.isHidden();
    }

    public boolean isHorizonLayer() {
        return this instanceof HorizonLayer;
    }

    public boolean isModelLayer() {
        return this instanceof ModelLayer;
    }

    public boolean isSeismicLayer() {
        return this instanceof SeismicLayer;
    }

    public boolean isSelected() {
        return windowModel.isSelected(this);
    }

    public boolean isValidTimeOrDepth(double timeOrDepth) {
        double minTime = dsProperties.getVerticalCoordMin();
        double maxTime = dsProperties.getVerticalCoordMax();

        return (minTime <= timeOrDepth && maxTime >= timeOrDepth);
    }

    /**
     * Gets the full width of the dataset.
     *
     * @return dataset width measured in traces
     */
    public double getWidth() {
        GeoFileDataSummary summary = getDatasetProperties().getSummary();
        return summary.getNumberOfTraces();
    }

    /**
     * Gets the full height of the dataset
     *
     * @return dataset height measured in time/depth units (xsec) or traces (map)
     */
    public double getHeight() {
        return getVerticalMax() - getVerticalMin();
    }

    public double getVerticalMin() {
        return dsProperties.getVerticalCoordMin();
    }

    public double getVerticalMax() {
        return dsProperties.getVerticalCoordMax();
    }

    public String getLayerID() {
        return layerID;
    }

    public LayeredRenderableImageProducer getRasterizer() {
        return rasterizer;
    }

    public void setCurrentFrame(int frameIndex) {
        if (frameIndex >= 0 && frameIndex < numFrames) {
            currentFrame = frameIndex;
        }
    }

    public void setGeoPlot(AbstractGeoPlot geoPlot) {
        this.geoPlot = geoPlot;
    }

    public void setLayerListPanel(LayerListPanel listPanel) {
        this.listPanel = listPanel;
    }

    public void setLocalSubsetProperties(LocalSubsetProperties subsetProps) {
        this.subsetProps = subsetProps;
    }

    public void setDisplayParameters(LayerDisplayParameters layerDisplayParams) {
        this.layerDisplayParams = layerDisplayParams;
    }

    public void setLayerSyncProperties(LayerSyncProperties syncProps) {
        this.syncProps = syncProps;
    }

    /**
     * Sets the layer's hidden field as requested and invokes fireLayerChanged()
     * on the associated WindowModel.
     * 
     * @param hidden
     */
    public void setHidden(boolean hidden) {
        layerDisplayParams.setHidden(hidden);
        windowModel.fireLayerChanged(this);
    }

    protected void setRasterizer(LayeredRenderableImageProducer rasterizer) {
        this.rasterizer = rasterizer;
    }

    /**
    @deprecated
     */
    public void setZorder(int zOrder) {
        logger.warning("setZorder is deprecated and does nothing");
    }

    /**
     * Saves the state of this Layer as an unformatted XML String.
     * 
     * @return String representation of this Layer's state
     */
    public String saveState() throws Exception {
        String QUOTE_AND_SPACE = "\" ";
        StringBuffer content = new StringBuffer();
        String xmlAlias;
        if (this instanceof SeismicLayer) {
            xmlAlias = SeismicLayer.getXmlAlias();
        } else if (this instanceof ModelLayer) {
            xmlAlias = ModelLayer.getXmlAlias();
        } else if (this instanceof HorizonLayer) {
            xmlAlias = HorizonLayer.getXmlAlias();
        } else {
            xmlAlias = getClass().getName();
        }

        content.append("<" + xmlAlias + " ");

        String currentOp = "saving layer field";
        try {
            content.append("layerID=\"" + getLayerID() + QUOTE_AND_SPACE);
            content.append("layerName=\"" + getLayerName() + QUOTE_AND_SPACE);
            content.append("zOrder=\"" + windowModel.getZorder(this) + QUOTE_AND_SPACE);
            content.append(">");

            currentOp = "saving DatasetProperties";
            content.append(dsProperties.saveState());

            currentOp = "saving LayerDisplayParameters";
            content.append(layerDisplayParams.saveState());

            currentOp = "saving LocalSubsetProperties";
            content.append(subsetProps.saveState());

            currentOp = "saving LayerSyncProperties";
            content.append(syncProps.saveState());
        } catch (Exception ex) {
            logger.warning("While " + currentOp + ", caught: " + ex.getMessage());
            throw ex;
        }

        if (arbTravEnabled) {
            try {
                content.append(arbTrav.saveState());
            } catch (Exception ex) {
                logger.warning("While saving arbTrav state, caught: " + ex.getMessage());
                throw ex;
            }
        }

        content.append("</" + xmlAlias + ">");

        return content.toString();
    }

    public abstract ImageIcon getImageIcon();

    public void processEditorResult(GeoGraphicsPropertyGroup properties) {
        if (properties instanceof WindowPropertyGroup) {
            process((WindowPropertyGroup) properties);
        } else if (properties instanceof LayerProperties) {
            process((LayerProperties) properties);
        } else {
            logger.info("Unrecognized type of GeoGraphicsPropertyGroup: " + properties.getClass().getName() + ", no action taken");
        }
    }

    /**
     * Updates the WindowProperties of this ViewerWindow if obj is one of the 
     * following types: ScaleProperties, AnnotationProperties, SyncProperties.
     * 
     * If the object is not one of these types, an IllegalArgumentException
     * is thrown.
     */
    private void process(WindowPropertyGroup properties) {
        if (properties instanceof WindowScaleProperties) {
            process((WindowScaleProperties) properties);
        } else if (properties instanceof CrossSectionAnnotationProperties) {
            process((CrossSectionAnnotationProperties) properties);
        } else if (properties instanceof MapAnnotationProperties) {
            process((MapAnnotationProperties) properties);
        } else if (properties instanceof WindowSyncProperties) {
            process((WindowSyncProperties) properties);
        } else {
            throw new IllegalArgumentException("Cannot proces object of type: " + properties.getClass().getName());
        }
    }

    private void process(WindowScaleProperties properties) {
        logger.info("Processing WindowScaleProperties... no action taken");
    }

    private void process(CrossSectionAnnotationProperties properties) {
        logger.info("Processing CrossSectionAnnotationProperties... no action taken");
    }

    private void process(MapAnnotationProperties properties) {
        logger.info("Processing MapAnnotationProperties... no action taken");
    }

    private void process(WindowSyncProperties properties) {
        logger.info("Processing WindowSyncProperties... no action taken");
    }

    private void process(LayerProperties result) {
        if (result.isCancelled()) {
            logger.fine("Layer property changes, if any, ignored - user cancelled.");
        } else if (result.isErrorCondition()) {
            logger.warning("Unable to process layer property change(s): " + result.getErrorMsg());
        } else {
            setLayerProperties(result);
        }
    }

    private void validateParams(DatasetProperties dsProperties,
            DataObject[] wiggles,
            LayerDisplayParameters layerDisplayParams) {
        if (dsProperties == null) {
            throw new IllegalArgumentException("Parameter: datasetProperties was null in Layer constructor");
        }
        if (dsProperties.getSummary() == null) {
            throw new IllegalArgumentException("DatasetProperties' summary was null in Layer constructor");
        }
        if (wiggles == null) {
            throw new IllegalArgumentException("DataObject[] was null in Layer constructor");
        }

        if (wiggles.length == 0) {
            throw new IllegalArgumentException("DataObject[] length was 0 in Layer constructor");
        }

        if (layerDisplayParams == null) {
            throw new IllegalArgumentException("Parameter: LayerDisplayParams was null in Layer constructor");
        }
    }

    /**
     * Method to be overriden by layers such as model xsec for which the GeoIO sampleRate does
     * not contain a meaningful value.
     */
    public double getSampleRate() {
        return dsProperties.getSummary().getSampleRate();
    }

    /**
     * Gets the y axis coordinate, in pixels, corresponding to the requested vertical time,
     * depth or trace coordinate
     * value, using the current active layer's vertical minimum and scale,
     * and the vertical scale of the WindowProperties associated with this LayerPanel.
     *
     * @param verticalDataCoord vertical coordinate for any layer
     *
     * @return vertical pixel coordinate of requested time/depth/trace coordinate
     */
    public int getVerticalPixelCoord(double verticalDataCoord) {
        double timeDelta = verticalDataCoord - getVerticalMin();
        double nSamples = timeDelta / getSampleRate();

        return (int) Math.round(nSamples * windowModel.getWindowProperties().getScaleProperties().getVerticalPixelIncr() *
                layerDisplayParams.getBasicLayerProperties().getVerticalScale());
    }

    public double getVerticalOffset() {
        return windowModel.getVerticalOffset(this);
    }

    public float[] getFloatVector(int traceIndex) {
        float[] floatVector = dataObjects[traceIndex].getFloatVector();
        String polarity = getLayerProperties().getLayerDisplayParameters().getBasicLayerProperties().getPolarity();
        if (BasicLayerProperties.REVERSED_POLARITY.equals(polarity)) {
            for (int i = 0; i < floatVector.length; i++) {
                floatVector[i] = floatVector[i] * -1.0f;
            }
        }
        return floatVector;
    }

    /**
     * Gets the ColorMap associated with this Layer.  If the layer's orientation is unknown or it
     * is a cross-section horizon, then an UnsupportedOperationException is thrown because
     * the Layer does not support a configurable ColorMap.
     *
     * @throws UnsupportedOperationException if one of the following conditions is true
     * <ul>
     *   <li>getOrientation() != GeoDataOrder.CROSS_SECTION_ORDER && getOrientation != GeoDataOrder.MAP_VIEW_ORDER</li>
     *   <li>isHorizonLayer() == true && getOrientation() == GeoDataOrder.CROSS_SECTION_ORDER
     * </ul>
     *
     * @return ColorMap associated with this Layer if it supports a user-configurable ColorMap
     */
    public abstract ColorMap getColorMap();
}