/*
###########################################################################
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import com.bhpb.geographics.controller.layer.LayerSyncGroup;
import com.bhpb.geographics.model.layer.display.LayerDisplayParameters;
import com.bhpb.geographics.model.layer.display.LocalSubsetProperties;
import com.bhpb.geographics.model.layer.display.LocalSubsetPropertiesFactory;
import com.bhpb.geographics.model.layer.display.MapDisplayParameters;
import com.bhpb.geographics.model.layer.display.MapDisplayParametersFactory;
import com.bhpb.geographics.model.layer.display.ModelXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.ModelXsecDisplayParametersFactory;
import com.bhpb.geographics.model.layer.shared.BasicLayerProperties;
import com.bhpb.geographics.model.layer.shared.ModelVerticalRange;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties;
import com.bhpb.geographics.model.layer.sync.MapSyncProperties;
import com.bhpb.geographics.model.layer.sync.ModelXsecSyncProperties;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.ModelProperties;
import com.bhpb.geoio.util.GeoIOAdapter;
import java.util.List;
import javax.swing.ImageIcon;

/**
 * ModelLayer - contains all data and context information necessary to generate an
 * image of a Seismic Layer.  This should possibly be merged into the LayerPanel
 * class.
 * 
 * SeismicLayer is final because otherwise calling 
 * getClass().getResource() could be unsafe.
 * 
 * @author Woody Folsom
 */
final public class ModelLayer extends Layer {

    private static final long serialVersionUID = 2292195460950584325L;
    private static final String MODEL_LAYER_ICON = "/icons/ModelLayer.gif";

    public ModelLayer(DatasetProperties dsProperties,
            LayerDisplayParameters layerDisplayParams,
            LocalSubsetProperties subsetProps,
            LayerSyncProperties syncProps,
            DataObject[] dataObjects,
            GeoIOAdapter ioAdapter,
            String layerID,
            String layerName,
            boolean allowArbTrav,
            ArbitraryTraverse arbTrav,
            LayerSyncGroup layerSyncGroup,
            WindowModel windowModel) { //minimum number of pixels between rendered traces

        super(dsProperties, layerDisplayParams, subsetProps, syncProps,
                ioAdapter, "M", dataObjects,
                layerID, layerName, allowArbTrav, arbTrav, layerSyncGroup,
                windowModel);
    }

    public ModelLayer(DatasetProperties dsProperties,
            LayerDisplayParameters layerDisplayParams,
            LocalSubsetProperties subsetProps,
            LayerSyncProperties syncProps,
            DataObject[] dataObjects,
            GeoIOAdapter ioAdapter,
            boolean allowArbTrav,
            LayerSyncGroup layerSyncGroup,
            WindowModel windowModel) { //minimum number of pixels between rendered traces

        super(dsProperties, layerDisplayParams, subsetProps, syncProps,
                ioAdapter, "M", dataObjects, allowArbTrav, layerSyncGroup,
                windowModel);
    }

    public static ModelLayer createModelLayer(ModelLayer aModelLayer, WindowModel aWindowModel) {
        DatasetProperties dsProps = new ModelProperties((ModelProperties) aModelLayer.getDatasetProperties());
        LayerDisplayParameters layerDisplayParams;
        LayerSyncProperties layerSyncProps;

        GeoDataOrder orientation = aModelLayer.getDatasetProperties().getMetadata().getGeoDataOrder();

        switch (orientation) {
            case CROSS_SECTION_ORDER:
                layerDisplayParams = ModelXsecDisplayParametersFactory.createLayerDisplayParameters(
                        (ModelXsecDisplayParameters) aModelLayer.getDisplayParameters());
                layerSyncProps = new ModelXsecSyncProperties((ModelXsecSyncProperties) aModelLayer.getSyncProperties());
                break;
            case MAP_VIEW_ORDER:
                layerDisplayParams = MapDisplayParametersFactory.createLayerDisplayParameters(
                        (MapDisplayParameters) aModelLayer.getDisplayParameters());
                layerSyncProps = new MapSyncProperties((MapSyncProperties) aModelLayer.getSyncProperties());
                break;
            default:
                throw new IllegalArgumentException("Unable to create new SeismicLayer with unrecognized orientation: " + orientation);
        }

        LocalSubsetProperties localSubsetProps = LocalSubsetPropertiesFactory.createLocalSubsetProperties(aModelLayer.getLocalSubsetProperties());

        return new ModelLayer(
                dsProps,
                layerDisplayParams,
                localSubsetProps,
                layerSyncProps,
                aModelLayer.getDataObjects(),
                aModelLayer.ioAdapter,
                aModelLayer.arbTravEnabled,
                aModelLayer.layerSyncGroup,
                aWindowModel);
    }

    public double getVerticalStepSize() {
        List<String> keys = getDatasetProperties().getMetadata().getKeys();
        int nKeys = keys.size();
        return getDatasetProperties().getKeyIncrement(keys.get(nKeys - 1));
    }

    public static String getXmlAlias() {
        return "modelLayer";
    }

    public double getAmplitudeMax() {
        switch (getOrientation()) {
            case MAP_VIEW_ORDER:
                return ((MapDisplayParameters) getDisplayParameters()).getColorAttributes().getMax();
            case CROSS_SECTION_ORDER:
            default:
                return getDatasetProperties().getSummary().getMaxAmplitude();
        }
    }

    public double getAmplitudeMin() {
        switch (getOrientation()) {
            case MAP_VIEW_ORDER:
                return ((MapDisplayParameters) getDisplayParameters()).getColorAttributes().getMin();
            case CROSS_SECTION_ORDER:
            default:
                return getDatasetProperties().getSummary().getMinAmplitude();
        }
    }

    public ImageIcon getImageIcon() {
        return new ImageIcon(getClass().getResource(MODEL_LAYER_ICON));
    }

    @Override
    public double getVerticalMin() {
        GeoFileMetadata metadata = dsProperties.getMetadata();
        if (metadata.getGeoDataOrder() == GeoDataOrder.MAP_VIEW_ORDER) {
            return super.getVerticalMin();
        } else {
            ModelVerticalRange modelVertRange = ((ModelXsecDisplayParameters) getLayerProperties().getLayerDisplayParameters()).getModelVerticalRange();
            return modelVertRange.getStartTime();
        }
    }

    @Override
    public double getVerticalMax() {
        GeoFileMetadata metadata = dsProperties.getMetadata();
        if (metadata.getGeoDataOrder() == GeoDataOrder.MAP_VIEW_ORDER) {
            return super.getVerticalMax();
        } else {
            ModelVerticalRange modelVertRange = ((ModelXsecDisplayParameters) getLayerProperties().getLayerDisplayParameters()).getModelVerticalRange();
            return modelVertRange.getEndTime();
        }
    }

    @Override
    public double getSampleRate() {
        if (dsProperties.getMetadata().getGeoDataOrder() == GeoDataOrder.MAP_VIEW_ORDER) {
            return super.getSampleRate();
        } else {
            return 1.0;
        }
    }

    /**
     * If this ModelLayer is in Cross-section order, then the getFloatVector method
     * is overriden due to the structure of this data format, which includes both time
     * and sample information in the traces' float vectors.  Otherwise, Layer.getFloatVector()
     * is invoked.
     *
     * @param traceIndex
     *
     * @return array of sample values for the specified trace index
     */
    @Override
    public float[] getFloatVector(int traceIndex) {
        if (getOrientation() == GeoDataOrder.CROSS_SECTION_ORDER) {
            float[] floatVector = dataObjects[traceIndex].getFloatVector();
            String polarity = getLayerProperties().getLayerDisplayParameters().getBasicLayerProperties().getPolarity();
            if (BasicLayerProperties.REVERSED_POLARITY.equals(polarity)) {
                //only reverse the sign of the samples, not the vertical coordinates
                int nlayers = (floatVector.length - 1) / 2;
                for (int i = 0; i < nlayers; i++) {
                    floatVector[i] = floatVector[i] * -1.0f;
                }
            }
            return floatVector;
        } else {
            return super.getFloatVector(traceIndex);
        }
    }

    public ColorMap getColorMap() {
        GeoDataOrder dataOrder = getOrientation();
        switch (dataOrder) {
            case CROSS_SECTION_ORDER :
                return ((ModelXsecDisplayParameters)getDisplayParameters()).getColorMap();
            case MAP_VIEW_ORDER :
                return ((MapDisplayParameters)getDisplayParameters()).getColorAttributes().getColorMap();
            default :
                throw new UnsupportedOperationException("Cannot get ColorMap for SeismicLayer with unknown orientation: " + dataOrder);
        }
    }
}