/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#f
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.model;

import com.bhpb.geographics.controller.layer.LayerSyncGroup;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.LayerDisplayParameters;
import com.bhpb.geographics.model.layer.display.LocalSubsetProperties;
import com.bhpb.geographics.model.layer.display.MapDisplayParameters;
import com.bhpb.geographics.model.layer.display.ModelXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParameters;
import com.bhpb.geographics.model.layer.sync.HorizonXsecSyncProperties;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties;
import com.bhpb.geographics.model.layer.sync.MapSyncProperties;
import com.bhpb.geographics.model.layer.sync.ModelXsecSyncProperties;
import com.bhpb.geographics.model.layer.sync.SeismicXsecSyncProperties;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.ArbTravKeyRange;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.datasystems.BhpSuHorizonDataObject;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.datasystems.UnlimitedKeyRange;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.metadata.ModelFileMetadata;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import com.bhpb.geoio.filesystems.properties.ModelProperties;
import com.bhpb.geoio.filesystems.properties.SeismicProperties;
import com.bhpb.geoio.util.GeoIOAdapter;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.compAPI.XmlUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Deserializes a Layer from a w3c.dom.Node.
 * 
 */
public class LayerFactory {

    private static final Logger logger =
            Logger.getLogger(LayerFactory.class.toString());
    private final GeoIOAdapter geoIOadapter;

    /**
     * Constructs a LayerFactory which uses the delagated IMessagingManager
     * to send and receive geoIO commands necessary to reconstruct DatasetProperties
     * and DataObjects from serialized parameters.
     * 
     * @param msgMgr
     */
    public LayerFactory(IMessagingManager msgMgr) {
        geoIOadapter = new GeoIOAdapter(msgMgr);
    }

    /**
     * Gets the child of the node with name matching childName.
     * 
     * @param node Node whose children will be searched.
     * @param childName Exact String name of child node to locate
     * 
     * @return The node with Name matching childName, or null if no such child is located
     */
    private Node getMatchingChild(Node node, String childName) {
        NodeList childList = node.getChildNodes();

        for (int i = 0; i < childList.getLength(); i++) {
            Node child = childList.item(i);
            if (child.getNodeName().equals(childName)) {
                return child;
            }
        }

        return null;
    }

    /**
     * Returns the values of selectedProperties/propertyName as a List<String>
     * 
     * @param dsPropsNode the parent node of selectedProperties
     * 
     * @return arrayList of selected property names, of length 1 for qiViewer
     */
    private List<String> getSelectedProperties(Node dsPropsNode) {
        List<String> selectedProps = new ArrayList<String>();
        Node selectedPropertiesNode = XmlUtils.getChild(dsPropsNode, "selectedProperties");
        NodeList propertyNameNodeCandidates = selectedPropertiesNode.getChildNodes();

        for (int i = 0; i < propertyNameNodeCandidates.getLength(); i++) {
            Node propertyNameNodeCandidate = propertyNameNodeCandidates.item(i);
            if (propertyNameNodeCandidate.getNodeName().equals("propertyName")) {
                selectedProps.add(propertyNameNodeCandidate.getTextContent());
            }
        }

        return selectedProps;
    }

    /**
     * Reads and sets the GeoFileSummary of the given DatasetProperties, using
     * GeoIO.
     * 
     * @param dsProperties DatasetProperties associated with the GeoFileSummary
     */
    private void readSummary(DatasetProperties dsProperties) {
        String summaryStatus = geoIOadapter.readSummary(dsProperties);
        if (summaryStatus.equals("") == false) {
            String errMsg = "Unable to read or set dsProperties summary due to: " + summaryStatus;
            logger.warning(errMsg);
            throw new RuntimeException(errMsg);
        }
    }

    /**
     *
     * @param node w3c.dom.Node associated with the LayerDisplayParameters
     *
     * @return deserialized ArbTrav or null if no such optional element exists
     */
    private ArbitraryTraverse restoreArbTrav(Node parentNode) {
       String childName = ArbitraryTraverse.getXmlAlias();

       Node arbTravNode = XmlUtils.getChild(parentNode, childName);

       if (arbTravNode != null) {
            ArbitraryTraverse arbTrav = ArbitraryTraverse.restoreState(arbTravNode);
            return arbTrav;
       } else {
            return null;
       }
    }

    /**
     * Creates a DatasetProperties from the given Node, searching for the specified
     * DatasetProperties class name and GeoFileMetadata classname.  If no children
     * of layerNode can be found corresponding to serialized DatasetProperties and
     * GeoFileMetadata of the appropriate type, a warning is logged and a RuntimeException
     * is thrown.
     * 
     * @param layerNode Node corresponding to a serialized Layer
     * @param dsPropsClassName name of Node corresponding to a serialized DatasetProperties subclass
     * @param metadataClassName name of Node corresponding to a serialized GeoFileMetadata subclass
     * 
     * @return deserialized DatasetProperties
     */
    private DatasetProperties restoreDatasetProperties(Node layerNode, String dsPropsClassName, String metadataClassName) {
        String childName = dsPropsClassName;
        Node dsPropsNode = getMatchingChild(layerNode, childName);

        if (dsPropsNode == null) {
            throw new IllegalArgumentException("Cannot restore datasetProperties from node: " + layerNode.getNodeName() + " no child with found with name matching: " + childName);
        }

        GeoFileMetadata metadata = restoreMetadata(dsPropsNode, metadataClassName);
        DatasetProperties dsProps;

        if (dsPropsClassName.equals(HorizonProperties.class.getName())) {
            dsProps = new HorizonProperties((SeismicFileMetadata) metadata);
            String selectedHorizon = ElementAttributeReader.getString(dsPropsNode, "selectedHorizon");
            ((HorizonProperties)dsProps).setSelectedHorizon(selectedHorizon);
        } else if (dsPropsClassName.equals(ModelProperties.class.getName())) {
            dsProps = new ModelProperties((ModelFileMetadata) metadata);
            ((ModelProperties) dsProps).setSelectedProperties(getSelectedProperties(dsPropsNode));
        } else if (dsPropsClassName.equals(SeismicProperties.class.getName())) {
            dsProps = new SeismicProperties((SeismicFileMetadata) metadata);
        } else {
            throw new IllegalArgumentException("Cannot restore datasetProperties: unknown dsPropsClassName: " + dsPropsClassName);
        }

        Map<String, AbstractKeyRange> chosenKeyRanges = new HashMap<String, AbstractKeyRange>();
        //now iterate through all the child nodes, search for keys and setting chosen ranges
        NodeList dsPropsChildren = dsPropsNode.getChildNodes();
        for (int i = 0; i < dsPropsChildren.getLength(); i++) {
            Node potentialKeyNode = dsPropsChildren.item(i);
            if (potentialKeyNode.getNodeName().equals("key")) {
                String keyName = ElementAttributeReader.getString(potentialKeyNode, "name");
                Node keyRangeNode = XmlUtils.getNonTextChild(potentialKeyNode);
                //if (keyRangeNode == null) {
                //    String errMsg = "Node <key> must have exactly non-text child, named 'keyRange'. XML is not valid.";
                //    logger.warning(errMsg);
                //    throw new RuntimeException(errMsg);
                //}
                if (keyRangeNode.getNodeName().equals("UnlimitedKeyRange")) {
                    UnlimitedKeyRange unlimitedKeyRange = new UnlimitedKeyRange((DiscreteKeyRange)metadata.getKeyRange(keyName));
                    chosenKeyRanges.put(keyName, unlimitedKeyRange);
                } else if (keyRangeNode.getNodeName().equals("DiscreteKeyRange")) {
                    DiscreteKeyRange discreteKeyRange = new DiscreteKeyRange(keyRangeNode,(DiscreteKeyRange)metadata.getKeyRange(keyName));
                    chosenKeyRanges.put(keyName, discreteKeyRange);
                } else if (keyRangeNode.getNodeName().equals("ArbTravKeyRange")) {
                    ArbTravKeyRange arbTravKeyRange = new ArbTravKeyRange(keyRangeNode,(DiscreteKeyRange)metadata.getKeyRange(keyName));
                    chosenKeyRanges.put(keyName, arbTravKeyRange);
                } else {
                    String errMsg = "Node <key> must have exactly non-text child, named 'keyRange'. XML is not valid.";
                    logger.warning(errMsg);
                    throw new RuntimeException(errMsg);
                }
            }
        }
        dsProps.setKeyChosenRanges(chosenKeyRanges);
        return dsProps;
    }

    /**
     * Deserialize a HorizonLayer.
     * 
     * @param node w3c.dom.Node containing HorizonLayer state
     * 
     * @return HorizonLayer instance
     */
    private HorizonLayer restoreHorizonLayer(Node node,
            String layerID, String layerName, PickingSettings pickingSettings,
            HorizonHighlightSettings horizonHighlightSettings,
            LayerSyncGroup layerSyncGroup, WindowModel windowModel) {
        //first, restore the dsProperties including header key chosen ranges
        DatasetProperties dsProperties = restoreDatasetProperties(node, HorizonProperties.class.getName(),
                SeismicFileMetadata.class.getName());
        //now, read the summary and save it in the dsProperties for geoIO to use
        readSummary(dsProperties);
        logger.fine("Summary data set for dataSetProperties.");
        //finally, read the data objects
        BhpSuHorizonDataObject[] dataObjects = geoIOadapter.getHorizons((HorizonProperties) dsProperties);
        logger.info(dataObjects.length + " DataObjects read.");
        //now, deserialize the layer properties
        LayerDisplayParameters layerDisplayParams = restoreLayerDisplayParameters(node);
        LocalSubsetProperties subsetProps = restoreLocalSubsetProperties(node);
        LayerSyncProperties syncProps = restoreLayerSyncProperties(node);

        logger.warning("zOrder may not be restored properly");
        int zOrder = restoreZorder(node);

        GeoDataOrder dataOrder = dsProperties.getMetadata().getGeoDataOrder();
        boolean allowArbTrav = dataOrder == GeoDataOrder.MAP_VIEW_ORDER;

        ArbitraryTraverse arbTrav = restoreArbTrav(node);

        HorizonLayer hLayer = new HorizonLayer(dsProperties,
                layerDisplayParams,
                subsetProps,
                syncProps,
                dataObjects,
                geoIOadapter,
                layerID,
                layerName,
                pickingSettings,
                horizonHighlightSettings,
                allowArbTrav,
                arbTrav,
                layerSyncGroup,
                windowModel);

        return hLayer;
    }

    /**
     * Deserialize a Layer from the given Node, instantiated it with
     * the specified WindowProperties.  If any portion of the Layer cannot
     * be deserialized, a warning is logged and a RuntimeException is thrown.
     * 
     * @param node w3c.dom.Node corresponding to a serialized Layer
     * @param winProps WindowProperties associated with the Layer
     * 
     * @return deserialized Layer subclass
     */
    public Layer restoreLayer(Node node, WindowProperties winProps, PickingSettings pickingSettings,
            HorizonHighlightSettings horizonHighlightSettings, LayerSyncGroup layerSyncGroup,
            WindowModel windowModel) {
        String nodeName = node.getNodeName();
        
        String layerID = ElementAttributeReader.getString(node, "layerID");
        String layerName = ElementAttributeReader.getString(node, "layerName");
        
        if (HorizonLayer.getXmlAlias().equals(nodeName)) {
            return restoreHorizonLayer(node, layerID, layerName, pickingSettings,
                    horizonHighlightSettings, layerSyncGroup, windowModel);
        } else if (ModelLayer.getXmlAlias().equals(nodeName)) {
            return restoreModelLayer(node, layerID, layerName, layerSyncGroup, windowModel);
        } else if (SeismicLayer.getXmlAlias().equals(nodeName)) {
            return restoreSeismicLayer(node, layerID, layerName, layerSyncGroup, windowModel);
        } else {
            String errMsg = "Unrecognized child node: " + nodeName;
            logger.info(errMsg);
            throw new UnsupportedOperationException("Unable to restore layer: " + errMsg);
        }
    }

    /**
     * Deserializes LayerDisplayParameters from the specified Node.
     * 
     * @param node w3c.dom.Node associated with the LayerDisplayParameters
     * 
     * @return deserialized LayerDisplayParameters subclass
     */
    private LayerDisplayParameters restoreLayerDisplayParameters(Node node) {
        LayerDisplayParameters layerDisplayParameters = null;
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            String childName = child.getNodeName();
            if (childName.equals("#text")) {
                continue;
            } else if (childName.equals(SeismicXsecDisplayParameters.getXmlAlias())) {
                layerDisplayParameters = new SeismicXsecDisplayParameters(child);
                break;
            } else if (childName.equals(ModelXsecDisplayParameters.getXmlAlias())) {
                layerDisplayParameters = new ModelXsecDisplayParameters(child);
                break;
            } else if (childName.equals(HorizonXsecDisplayParameters.getXmlAlias())) {
                layerDisplayParameters = HorizonXsecDisplayParameters.restoreState(child);
                break;
            } else if (childName.equals(MapDisplayParameters.getXmlAlias())) {
                layerDisplayParameters = new MapDisplayParameters(child);
                break;
            }
        }
        if (layerDisplayParameters == null) {
            String errMsg = "No node corresponding to a deserializable LayerDisplayParameters class was found as a child of: " + node.getNodeName();
            logger.warning(errMsg);
            throw new RuntimeException(errMsg);
        }
        return layerDisplayParameters;
    }

    /**
     * Deserializes LayerSyncProperties from the specified Node.
     * 
     * @param node w3c.dom.Node associated with the LayerDisplayParameters
     * 
     * @return deserialized LayerSyncProperties subclass
     */
    private LayerSyncProperties restoreLayerSyncProperties(Node node) {
        LayerSyncProperties layerSyncProperties = null;
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            String childName = child.getNodeName();
            if (childName.equals("#text")) {
                continue;
            } else if (childName.equals(SeismicXsecSyncProperties.getXmlAlias())) {
                layerSyncProperties = SeismicXsecSyncProperties.restoreState(child);
                break;
            } else if (childName.equals(ModelXsecSyncProperties.getXmlAlias())) {
                layerSyncProperties = ModelXsecSyncProperties.restoreState(child);
                break;
            } else if (childName.equals(HorizonXsecSyncProperties.getXmlAlias())) {
                layerSyncProperties = HorizonXsecSyncProperties.restoreState(child);
                break;
            } else if (childName.equals(MapSyncProperties.getXmlAlias())) {
                layerSyncProperties = MapSyncProperties.restoreState(child);
                break;
            }
        }
        if (layerSyncProperties == null) {
            String errMsg = "No node corresponding to a deserializable LayerSyncProperties class was found as a child of: " + node.getNodeName();
            logger.warning(errMsg);
            throw new RuntimeException(errMsg);
        }
        return layerSyncProperties;
    }

    /**
     * LocalSubsetProperties LayerSyncProperties from the specified Node.
     * 
     * @param node w3c.dom.Node associated with the LayerDisplayParameters
     * 
     * @return deserialized LocalSubsetProperties subclass
     */
    private LocalSubsetProperties restoreLocalSubsetProperties(Node node) {
        LocalSubsetProperties localSubsetProperties = null;
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            String childName = child.getNodeName();
            if (childName.equals("#text")) {
                continue;
            } else if (childName.equals(LocalSubsetProperties.getXmlAlias())) {
                localSubsetProperties = LocalSubsetProperties.restoreState(child);
                break;
            }
        }
        if (localSubsetProperties == null) {
            String errMsg = "No node corresponding to a deserializable LocalSubsetProperties class was found as a child of: " + node.getNodeName();
            logger.warning(errMsg);
            throw new RuntimeException(errMsg);
        }
        return localSubsetProperties;
    }

    /**
     * Deserialize a ModelLayer.
     * 
     * @param node w3c.dom.Node containing ModelLayer state
     * 
     * @return ModelLayer instance
     */
    private ModelLayer restoreModelLayer(Node node,
            String layerID, String layerName, LayerSyncGroup layerSyncGroup,
            WindowModel windowModel) {
        //first, restore the dsProperties including header key chosen ranges
        DatasetProperties dsProperties = restoreDatasetProperties(node, ModelProperties.class.getName(),
                ModelFileMetadata.class.getName());
        //now, read the summary and save it in the dsProperties for geoIO to use
        readSummary(dsProperties);
        logger.fine("Summary data set for dataSetProperties.");
        //finally, read the data objects
        DataObject[] dataObjects = geoIOadapter.getTraces(dsProperties);
        logger.info(dataObjects.length + " DataObjects read.");
        //now, deserialize the layer properties
        LayerDisplayParameters layerDisplayParams = restoreLayerDisplayParameters(node);
        LocalSubsetProperties subsetProps = restoreLocalSubsetProperties(node);
        LayerSyncProperties syncProps = restoreLayerSyncProperties(node);

        logger.warning("zOrder may not be restored properly");
        int zOrder = restoreZorder(node);

        GeoDataOrder dataOrder = dsProperties.getMetadata().getGeoDataOrder();
        boolean allowArbTrav = dataOrder == GeoDataOrder.MAP_VIEW_ORDER;

        ArbitraryTraverse arbTrav = restoreArbTrav(node);

        ModelLayer mLayer = new ModelLayer(dsProperties,
                layerDisplayParams,
                subsetProps,
                syncProps,
                dataObjects,
                geoIOadapter,
                layerID,
                layerName,
                allowArbTrav,
                arbTrav,
                layerSyncGroup,
                windowModel);

        return mLayer;
    }

    /**
     * Deserialize a GeoFileMetadata of the requsted type.  Throws RuntimeException
     * if no node corresponding to a serialized instance of the indicated type
     * can be found as a child of parentNode
     * 
     * @param parentNode w3c.dom.Node containing GeoFileMetadata state
     * 
     * @return GeoFileMetadata instance
     */
    private GeoFileMetadata restoreMetadata(Node parentNode, String metadataClassName) {
        String childName = metadataClassName;
        Node metadataNode = getMatchingChild(parentNode, childName);

        if (metadataNode == null) {
            throw new IllegalArgumentException("Cannot restore metadata from node: " + parentNode.getNodeName() + " no child with found with name matching: " + childName);
        }

        String geoFileName = ElementAttributeReader.getString(metadataNode, "geoFilePath");
        logger.info("geoFileName: " + geoFileName);
        return geoIOadapter.readMetadata(geoFileName);
    }

    /**
     * Deserializes a SeismicLayer from org.w3c.dom.Node, using geoIO to read the
     * associated Summary and DataObjects.  The LayerProperties are deserialized from
     * children of the Node and the specified WindowProperties are also used
     * to initialize the SeismicLayer.
     *
     * Adapted from com.bhpb.qiviewer.adapter.geoio.GeoIODirector
     *
     * @param node
     * @param layerID
     * @param layerName
     * @param layerSyncGroup
     * @param windowModel
     *
     * @return SeismicLayer created from the specified Node and WindowProperties
     */
    public SeismicLayer restoreSeismicLayer(Node node,
            String layerID, String layerName, LayerSyncGroup layerSyncGroup,
            WindowModel windowModel) {
        //first, restore the dsProperties including header key chosen ranges
        DatasetProperties dsProperties = restoreDatasetProperties(node, SeismicProperties.class.getName(),
                SeismicFileMetadata.class.getName());
        //now, read the summary and save it in the dsProperties for geoIO to use
        readSummary(dsProperties);
        logger.fine("Summary data set for dataSetProperties.");
        //finally, read the data objects
        DataObject[] dataObjects = geoIOadapter.getTraces(dsProperties);
        logger.info(dataObjects.length + " DataObjects read.");
        //now, deserialize the layer properties
        LayerDisplayParameters layerDisplayParams = restoreLayerDisplayParameters(node);
        LocalSubsetProperties subsetProps = restoreLocalSubsetProperties(node);
        LayerSyncProperties syncProps = restoreLayerSyncProperties(node);

        logger.warning("zOrder may not be restored properly");
        int zOrder = restoreZorder(node);

        ArbitraryTraverse arbTrav = restoreArbTrav(node);

        GeoDataOrder dataOrder = dsProperties.getMetadata().getGeoDataOrder();
        boolean allowArbTrav = dataOrder == GeoDataOrder.MAP_VIEW_ORDER;

        SeismicLayer sLayer = new SeismicLayer(dsProperties,
                layerDisplayParams,
                subsetProps,
                syncProps,
                dataObjects,
                geoIOadapter,
                layerID,
                layerName,
                allowArbTrav,
                arbTrav,
                layerSyncGroup,
                windowModel);

        return sLayer;
    }

    /**
     * Deserializes the zOrder of a Layer node.
     * 
     * @param node Node corresponding to a serialized Layer
     * 
     * @return the value of the zOrder attribute as an integer
     */
    private int restoreZorder(Node node) {
        return ElementAttributeReader.getInt(node, "zOrder");
    }
}