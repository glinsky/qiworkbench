/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.model;

import com.bhpb.geographics.util.XStreamUtil;
import com.bhpb.geoio.datasystems.ArbTravKeyRange;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.thoughtworks.xstream.XStream;
import java.awt.geom.Point2D;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.w3c.dom.Node;

/**
 * Represents a piecewise linear path selected on a Map-order Layer
 * @author folsw9
 */
public class ArbitraryTraverse {
    private transient Layer layer;
    private List<ViewTarget> targetPoints = new ArrayList<ViewTarget>();
    private String horizontalKeyName;
    private String verticalKeyName;

    /**
     * Constructs an ArbitraryTraverse.
     *
     * @throws IllegalArgumentException if the layer is not MAP order
     * or does not have 3 or more header keys (tracl, cdp, ep...).
     * May only be associated with a layer
     * of MAP orientation and only accepts ViewTargets with exactly 1 horizontal
     * and 1 vertical header key coordinate.
     *
     * @param layer
     */
    public ArbitraryTraverse(Layer layer) {
        if (layer.getOrientation() != GeoDataOrder.MAP_VIEW_ORDER) {
            throw new IllegalArgumentException("ArbitraryTraverse may only be created for a layer with ORIENTATION.MAP");
        }

        this.layer = layer;
        
        DatasetProperties dsProps = layer.getDatasetProperties();
        GeoFileMetadata metadata = dsProps.getMetadata();

        ArrayList<String> headerKeys = metadata.getKeys();

        if (headerKeys.size() < 3) {
            throw new IllegalArgumentException("Arbitrary traverse cannot be created for a Layer with <3 header keys.");
        }
        horizontalKeyName = headerKeys.get(1);
        verticalKeyName = headerKeys.get(2);
    }

    public void addPoint(ViewTarget targetPoint) {
        targetPoints.add(targetPoint);
    }

    public void clearPoints() {
        targetPoints.clear();
    }
    
    public String getHorizontalKeyName() {
        return horizontalKeyName;
    }

    public Map<String, ArbTravKeyRange> getHeaderKeyMap() {
        Map<String, ArbTravKeyRange> headerKeyMap = new HashMap<String, ArbTravKeyRange>();

        int nPoints = targetPoints.size();

        int[] hKeyPoints = new int[nPoints];
        int[] vKeyPoints = new int[nPoints];

        for (int i = 0; i < nPoints; i++) {
            hKeyPoints[i] = targetPoints.get(i).getHeaderKeyValue(horizontalKeyName).intValue();
            vKeyPoints[i] = (int) targetPoints.get(i).getVertical();
        }
        GeoFileMetadata metadata = layer.getDatasetProperties().getMetadata();

        ArbTravKeyRange hKeyRange = new ArbTravKeyRange(hKeyPoints, metadata.getKeyRange(horizontalKeyName));
        ArbTravKeyRange vKeyRange = new ArbTravKeyRange(vKeyPoints, metadata.getKeyRange(verticalKeyName));

        headerKeyMap.put(horizontalKeyName, hKeyRange);
        headerKeyMap.put(verticalKeyName, vKeyRange);

        return headerKeyMap;
    }

    public List<Point2D> getPoints() {
        List<Point2D> pointList = new ArrayList<Point2D>();
            for (ViewTarget targetPoint : targetPoints) {
                pointList.add(targetPoint.getPoint2D(layer));
            }
        return pointList;
    }

    public String getVerticalKeyName() {
        return verticalKeyName;
    }

    public static String getXmlAlias() {
        return "arbTrav";
    }

    public boolean isDefined() {
        return targetPoints.size() != 0;
    }

        public static ArbitraryTraverse restoreState(Node node) {
        XStream xStream = XStreamUtil.getInstance();
        String xml = ElementAttributeReader.nodeToXMLString(node);

        ArbitraryTraverse arbTrav = (ArbitraryTraverse) xStream.fromXML(new StringReader(xml));

        return arbTrav;
    }

    public String saveState() {
        StringBuffer content = new StringBuffer();

        XStream xStream = XStreamUtil.getInstance();
        content.append(xStream.toXML(this));
        content.append("\n");

        return content.toString();
    }

    protected void setLayer(Layer layer) {
        this.layer = layer;
    }
}