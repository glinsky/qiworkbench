/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geographics.util;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JColorChooser;

import com.bhpb.qiworkbench.compAPI.BooleanSettings;

public class ColorSwatch extends JButton {
    private static final Logger logger =
            Logger.getLogger(ColorSwatch.class.toString());
    private Color color;
    private final String toolTip;
    
    private BooleanSettings isLoweredBevelBorder = new BooleanSettings();
    
    public ColorSwatch(Color color, ActionListener actionListener) {
        this.color = color;
        toolTip = null;
        addActionListener(actionListener);
        isLoweredBevelBorder.setValue(false);
        setColor(color);
    }
    
    public ColorSwatch(Color color, String toolTip, int colorIndex) {
        if (color == null) {
            throw new IllegalArgumentException("Color must be non-null");
        }
        if (toolTip == null || toolTip.length() == 0) {
            throw new IllegalArgumentException("Tooltip next must be non-null and of length > 0");    
        }
        
        this.color = color;
        this.toolTip = toolTip;
        setToolTipText(toolTip);
        setColor(color);
        MouseListener[] mouseListeners = getMouseListeners();
        for(MouseListener ml : mouseListeners)
        	removeMouseListener(ml);

        addActionListener(new ColorSwatchActionListener(this));
        
        
        setText(Integer.valueOf(colorIndex).toString());
        isLoweredBevelBorder.setValue(false);
    }
    
    public Color getColor() {
        return color;
    }
    
    public String getToolTip() {
        return toolTip;
    }
    
    public boolean isLoweredBevelBorder(){
    	return isLoweredBevelBorder.getValue();
    }

    public void setLoweredBevelBorder(BooleanSettings b){
    	isLoweredBevelBorder = b;
    }
    
    public void setColor(Color color) {
        this.color = color;
        setBackground(color);
    }


    private static class ColorSwatchActionListener implements ActionListener {
        ColorSwatch colorSwatch;
        public ColorSwatchActionListener(ColorSwatch colorSwatch) {
            this.colorSwatch = colorSwatch;
        }
        public void actionPerformed(ActionEvent ae) {
            Color chosenColor = JColorChooser.showDialog(colorSwatch, colorSwatch.getToolTip(), colorSwatch.getColor());
            if (chosenColor != null) {
                colorSwatch.setColor(chosenColor);
            } else {
                logger.info("Color picking canceled.");
            }
        }
    }
}