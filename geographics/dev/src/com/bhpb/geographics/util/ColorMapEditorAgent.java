/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import com.bhpb.geographics.io.IODirector;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.workbench.QiComponentBase;
import java.awt.Component;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * QiComponent which allows ColorMapEditor to send and receive asynchronous 
 * file-related messages which require a QiComponent recipient.
 */
public class ColorMapEditorAgent extends QiComponentBase implements IqiWorkbenchComponent, Runnable {

    private static final int MINUTE_WAIT_TIME = 60000;
    private static final Logger logger =
            Logger.getLogger(ColorMapEditorAgent.class.toString());

    public enum FILECHOOSER_ACTION {

        OPEN_COLORMAP, SAVE_COLORMAP
    }
    private boolean stopThread = false;
    private ColorMapEditorMsgProcessor messageProcessor;
    private Component editor;
    private IMessagingManager messagingMgr;
    private IODirector ioDirector;
    private QiProjectDescriptor qiProjDesc = new QiProjectDescriptor();
    private String myCID = "";
    private String chosenFileName = null;

    public ColorMapEditorAgent(ColorMapEditor editor) {
        ioDirector = new IODirector(this);
        this.editor = editor;
    }

    public void init() {
        logger.fine("Entering ViewerAgent.init...");
        logger.fine("Setting SYNC_LOCK");
        QIWConstants.SYNC_LOCK.lock();
        messagingMgr = new MessagingManager();
        myCID = Thread.currentThread().getName();
        logger.fine("Thread name: " + myCID);
        logger.fine("Registering Component");
        messagingMgr.registerComponent(QIWConstants.COLORMAP_EDITOR_AGENT, QIWConstants.COLORMAP_EDITOR_NAME, myCID);
        logger.fine("Clearing SYNC_LOCK");
        QIWConstants.SYNC_LOCK.unlock();
        logger.fine("Exiting ViewerAgent.init.");

        super.setInitSuccessful(true);
        super.setInitFinished(true);
    }

    public void run() {
        init();
        messageProcessor = new ColorMapEditorMsgProcessor(this);
        
        // process any messages received from other components
        while (stopThread == false) {
            String myPid = QiProjectDescUtils.getPid(qiProjDesc);
            if (!"".equals(myPid)) {
                String requestID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_ASSOC_PROJMGR_CMD, QIWConstants.STRING_TYPE, myPid);
                logger.info("ProjMgrDesc is null, requesting associated PROJMGR for this component with Pid: " + myPid);
                while (messagingMgr.isMsgQueueEmpty()) {
                    logger.info("Msg queue is empty, waiting 1 second for response to GET_ASSOC_PROJMGR_CMD (requet ID: " + requestID + ")");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ie) {
                        logger.info("Caught: " + ie.getMessage() + " while waiting for response to GET_ASSOC_PROJMGR_CMD in ViewerAgent.run()");
                    }
                }
            }
            
            if (Thread.interrupted() == true) {
                stopThread = true;
            } else {
                //if a data message with skip flag set to true, it will not be processed here
                //instead it will be claimed by calling getMatchingResponseWait
                IQiWorkbenchMsg msg = messagingMgr.peekNextMsg();
                if (msg != null && !(msg.getMsgKind().equals(QIWConstants.DATA_MSG) && msg.skip())) {
                    msg = messagingMgr.getNextMsgWait();
                    messageProcessor.processMsg(msg);
                } else {
                    try {
                        Thread.sleep(250);
                    } catch (InterruptedException ie) {
                        logger.warning("Interruptepd while sleeping 250ms after null/skippable msg: " + ie.getMessage());
                    }
                }
            }
        }

    }

    public void invokeFileChooser(FILECHOOSER_ACTION action) {
        ioDirector.callFileChooser(editor,
                "/",
                action,
                ".cmp",
                action.toString(),
                action.toString());
    }

    /**
     * Gets the {@link IComponentDescriptor} of this <code>ViewerAgent</code>.
     */
    public IComponentDescriptor getComponentDescriptor() {
        return messagingMgr.getMyComponentDesc();
    }

    /**
     * Gets the name of the file most recently chosen using the filechooser service.
     * If an error occured, the user cancelled the FileChooser or no response was 
     * received within the allotted time, the empty String is returned.
     * 
     * @return The name of the file selected from the FileChooser service Dialog
     */
    public synchronized String getFileChooserResult() {
        long startTime = System.currentTimeMillis();
        while (!isFileChooserResultAvailable() && System.currentTimeMillis() - startTime < MINUTE_WAIT_TIME) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ie) {
                logger.warning(ie.getMessage());
            }
        }
        if (isFileChooserResultAvailable()) {
            return getChosenFile();
        } else {
            logger.warning("No file was chosen after " + MINUTE_WAIT_TIME / 1000 + " seconds, returning empty String.");
            return "";
        }
    }

    public String getFileAsAscii(String fileName) {
        String msgID = "";
        ArrayList<String> params = new ArrayList<String>();
        params.add(messagingMgr.getLocationPref());
        params.add(fileName);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.FILE_READ_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);

        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, MINUTE_WAIT_TIME);
        if (response == null) {
            logger.info("Response to FILE_READ_CMD was null ");
            return "";
        } else if (response.isAbnormalStatus()) {
            logger.info("Response to FILE_READ_CMD was abnormal: " + response.getContent().toString());
            return "";
        }
        ArrayList<String> xml = (ArrayList<String>) MsgUtils.getMsgContent(response);

        StringBuffer xmlContent = new StringBuffer();
        for (int i = 0; i < xml.size(); i++) {
            xmlContent.append(xml.get(i));
            xmlContent.append(System.getProperty("line.separator"));
        }

        return xmlContent.toString();
    }

    public IMessagingManager getMessagingManager() {
        return messagingMgr;
    }

    /**
     * Returns true if and only if chosenFileName is non-null
     * @return
     */
    private boolean isFileChooserResultAvailable() {
        return chosenFileName != null;
    }

    /**
     * Gets the chosen file name and rests the field to null.
     * Synchronized so that it will return the 
     * @return
     */
    private synchronized String getChosenFile() {
        String fileChosenCopy = chosenFileName;
        chosenFileName = null;
        return fileChosenCopy;
    }

    public void setComponent(Component editor) {
        this.editor = editor;
    }

    void setChosenFileName(String chosenFileName) {
        this.chosenFileName = chosenFileName;
    }

    void stopThread() {
        logger.info("Stopping ColorMapEditorAgent thread...");
        stopThread = true;
    }
}