/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.util;

import com.bhpb.geographics.model.ArbitraryTraverse;
import com.bhpb.geographics.model.CursorSettings;
import com.bhpb.geographics.model.layer.shared.AutoGainControl;
import com.bhpb.geographics.model.layer.shared.ColorAttributes;
import com.bhpb.geographics.model.layer.shared.ColorMapModel;
import com.bhpb.geographics.model.layer.shared.Culling;
import com.bhpb.geographics.model.layer.shared.DisplayAttributes;
import com.bhpb.geographics.model.HorizonHighlightSettings;
import com.bhpb.geographics.model.HorizonLayer;
import com.bhpb.geographics.model.ModelLayer;
import com.bhpb.geographics.model.layer.shared.Normalization;
import com.bhpb.geographics.model.PickingSettings;
import com.bhpb.geographics.model.layer.shared.RasterizationTypeModel;
import com.bhpb.geographics.model.SeismicLayer;
import com.bhpb.geographics.model.ViewTarget;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters.LINE_STYLE;
import com.bhpb.geographics.model.layer.display.LocalSubsetProperties;
import com.bhpb.geographics.model.layer.display.ModelXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParameters;
import com.bhpb.geographics.model.layer.shared.InterpolationSettings;
import com.bhpb.geographics.model.layer.sync.HorizonXsecSyncProperties;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties;
import com.bhpb.geographics.model.layer.sync.MapSyncProperties;
import com.bhpb.geographics.model.layer.sync.ModelXsecSyncProperties;
import com.bhpb.geographics.model.layer.sync.SeismicXsecSyncProperties;
import com.bhpb.geographics.model.window.MapAnnotationProperties;
import com.bhpb.geographics.model.window.WindowAnnotationProperties;
import com.bhpb.geographics.model.window.WindowScaleProperties;
import com.bhpb.geographics.model.window.WindowSyncProperties;
import com.bhpb.geographics.rasterizer.RasterizerContext;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import com.thoughtworks.xstream.security.AnyTypePermission;       // JE FIELD                             

public class XStreamUtil {
    /**
     * Convenience method to create a new XStream parser using a new instance
     * of DomDriver, taking the classLoader from this XStreamUtil class and 
     * setting short aliases for all serialized GeoGraphicsLib classes.
     * 
     * @return new instance of XStream
     */
    public static XStream getInstance() {
        XStream xStream = new XStream(new DomDriver());

  // JE FIELD added remove all xstream security
        xStream.addPermission(AnyTypePermission.ANY);
        
        xStream.setClassLoader(XStreamUtil.class.getClassLoader());
        
        //window-level aliases
        xStream.alias("windowAnnotationProperties", WindowAnnotationProperties.class);
        xStream.alias(WindowSyncProperties.getXmlAlias(), WindowSyncProperties.class);
        xStream.alias(WindowScaleProperties.getXmlAlias(), WindowScaleProperties.class);
        xStream.alias("windowSyncFlag", WindowSyncProperties.SYNC_FLAG.class);
        xStream.alias(MapAnnotationProperties.getXmlAlias(), MapAnnotationProperties.class);
        
        //layer-level aliases
        xStream.alias(SeismicLayer.getXmlAlias(), SeismicLayer.class);
        xStream.alias(ModelLayer.getXmlAlias(), ModelLayer.class);
        xStream.alias(HorizonLayer.getXmlAlias(), HorizonLayer.class);
        
        xStream.alias(LocalSubsetProperties.getXmlAlias(), LocalSubsetProperties.class);
        xStream.alias("labelLocation", WindowAnnotationProperties.LABEL_LOCATION.class);
        xStream.alias(LayerSyncProperties.getXmlAlias(), LayerSyncProperties.class);
        xStream.alias("layerSyncFlag", LayerSyncProperties.SYNC_FLAG.class);
        xStream.alias(SeismicXsecSyncProperties.getXmlAlias(), SeismicXsecSyncProperties.class);
        xStream.alias(ModelXsecSyncProperties.getXmlAlias(), ModelXsecSyncProperties.class);
        xStream.alias(HorizonXsecSyncProperties.getXmlAlias(), HorizonXsecSyncProperties.class);
        xStream.alias(MapSyncProperties.getXmlAlias(), MapSyncProperties.class);
        
        //display property aliases
        xStream.alias(SeismicXsecDisplayParameters.getXmlAlias(), SeismicXsecDisplayParameters.class);
        xStream.alias(ModelXsecDisplayParameters.getXmlAlias(), ModelXsecDisplayParameters.class);
        xStream.alias(HorizonXsecDisplayParameters.getXmlAlias(), HorizonXsecDisplayParameters.class);
        
        //display parameters composite classes
        xStream.alias("normalization", Normalization.class);
        xStream.alias("autoGainControl", AutoGainControl.class);
        xStream.alias("rasterization", RasterizationTypeModel.class);
        xStream.alias("culling", Culling.class);
        xStream.alias("colorMapModel", ColorMapModel.class);
        xStream.alias(DisplayAttributes.getXmlAlias(), DisplayAttributes.class);
        xStream.alias(ColorAttributes.getXmlAlias(), ColorAttributes.class);
        xStream.alias(InterpolationSettings.getXmlAlias(), InterpolationSettings.class);

        xStream.alias(PickingSettings.getXmlAlias(), PickingSettings.class);
        xStream.alias("pickingMode", PickingSettings.PICKING_MODE.class);
        xStream.alias("snappingMode", PickingSettings.SNAPPING_MODE.class);
        
        xStream.alias(CursorSettings.getXmlAlias(), CursorSettings.class);
        xStream.alias("cursorStyle", CursorSettings.CURSOR_STYLE.class);
        
        xStream.alias(HorizonHighlightSettings.getXmlAlias(), HorizonHighlightSettings.class);
        xStream.alias("lineStyle", LINE_STYLE.class);

        xStream.alias(RasterizerContext.getXmlAlias(), RasterizerContext.class);
        xStream.alias(ArbitraryTraverse.getXmlAlias(), ArbitraryTraverse.class);
        xStream.alias(ViewTarget.getXmlAlias(), ViewTarget.class);

        return xStream;
    }
}
