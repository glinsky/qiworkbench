/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

/**
 * Super naive implementation of linear interpolation.
 *
 * @param values original values (not modified)
 * @param interpolatedValues the output array consisting of original and interpolated values - it is (subSamples + 1) times as long as original array
 * @param subSamples number of points to interpolate between original values
 *
 */
public class LinearInterpolator extends AbstractInterpolator implements Interpolator {

    public float[] interpolate(float[] values, double sampleRate) {
        if (sampleRate < 1.0) {
            throw new IllegalArgumentException("SampleRate must be >= 1.0 for interpolation");
        }
        float[] interpolatedValues = new float[(int) Math.round((values.length - 1) * (sampleRate + 1) + 1)];

        interpolatedValues[0] = values[0];
        int lastSampleIndex = 0;
        for (int i = 1; i < values.length; i++) {
            int nextSampleIndex = (int) Math.round(i * (sampleRate + 1));
            if (nextSampleIndex >= interpolatedValues.length) {
                break;
            }
            interpolatedValues[nextSampleIndex] = values[i];
            if (lastSampleIndex < nextSampleIndex - 1) {
                int nInterpolatedSegments = nextSampleIndex - lastSampleIndex;
                double interpolatedSegmentLength = 1.0 / nInterpolatedSegments;
                double slope = interpolatedValues[nextSampleIndex] - interpolatedValues[lastSampleIndex];
                for (int j = 1; j < nInterpolatedSegments; j++) {
                    interpolatedValues[lastSampleIndex + j] = (float) (interpolatedValues[lastSampleIndex] + j * interpolatedSegmentLength * slope);
                }
            }
            lastSampleIndex = nextSampleIndex;
        }
        interpolatedValues[interpolatedValues.length - 1] = values[values.length - 1];

        return interpolatedValues;
    }
}
