/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

public class StepInterpolator extends AbstractInterpolator implements Interpolator {
    /**
     * Step interpolation - values are extended over the requesetd number of subSamples
     * between any initial value and the next.
     *
     * @param values original values (not modified)
     * @param sampleRate number of points to interpolate between original values
     *
     * @return interpolatedValues the output array consisting of original and interpolated values - it is (subSamples + 1) times as long as original array
     *
     */
    public float[] interpolate(float[] values, double sampleRate) {
        if (sampleRate < 1.0) {
            throw new IllegalArgumentException("SampleRate must be >= 1.0 for interpolation");
        }
        float[] interpolatedValues = new float[(int) Math.round((values.length - 1) * (sampleRate + 1) + 1)];

        interpolatedValues[0] = values[0];
        int lastIndex = 0;
        for (int i = 1; i < values.length; i++) {
            int index = (int)Math.round(i * (sampleRate + 1));
            if (index >= interpolatedValues.length) {
                break;
            }
            interpolatedValues[index] = values[i];
            if (lastIndex < index - 1) {
                for (int j = 1; lastIndex + j < index; j++) {
                    interpolatedValues[lastIndex + j] = interpolatedValues[lastIndex];
                }
            }
            lastIndex = index;
        }
        interpolatedValues[interpolatedValues.length - 1] = values[values.length - 1];

        return interpolatedValues;
    }
}
