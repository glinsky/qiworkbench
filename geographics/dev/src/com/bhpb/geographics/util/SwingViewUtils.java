/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.ListModel;

/**
 * Collection of util methods for setting Swing component values, including safe
 * defaults in the case of null or invalid values.
 */
public class SwingViewUtils {

    Logger logger;

    /**
     * Construct a SwingViewUtils using the specified Logger
     * 
     * @param logger java.util.logging.Logger, cannot be null
     */
    public SwingViewUtils(Logger logger) {
        if (logger == null) {
            throw new IllegalArgumentException("null logger");
        }
        this.logger = logger;
    }

    /**
     * Removes all data from the ComboBox's ListModel, then adds the data
     * passed in to this method.
     * 
     * @param comboBox
     * @param data
     */
    public void setComboBoxData(JComboBox comboBox, Object[] data) {
        ListModel listModel = comboBox.getModel();
        if (listModel instanceof DefaultComboBoxModel == false) {
            throw new IllegalArgumentException("Unable to set the data of the JComboBox because its model was not a DefaultComboBoxModel");
        }
        DefaultComboBoxModel model = (DefaultComboBoxModel) listModel;
        model.removeAllElements();
        for (Object datum : data) {
            model.addElement(datum);
        }
    }

    /**
     * Safely sets the value of the JComboBox as requested.  If the specified value if null
     * or does not equal a value contained in the ListModel, a Logger warning is generated.
     * Otherwise, the ListModel's selected item is changed.
     * 
     * @throws IllegalArgumentException if the comboBox's model is not an instance of DefaultListModel
     * 
     * @param comboBox the JComboBox which will be updated with a new selected value
     * @param value the String value which will be selected, if it exists in the model of comboBox
     * @param listName the name of the list shown in the comboBox, used for diagnostic logging
     */
    public void setComboBoxValue(JComboBox comboBox, String value, String listName) {
        ListModel listModel = comboBox.getModel();
        if (listModel instanceof DefaultComboBoxModel == false) {
            throw new IllegalArgumentException("Unable to set the selected value of the JComboBox because its model was not a DefaultComboBoxModel");
        }
        DefaultComboBoxModel model = (DefaultComboBoxModel) comboBox.getModel();
        int index = model.getIndexOf(value);
        listName = listName == null ? "UNKNOWN LIST NAME" : listName;
        if (value == null) {
            logger.warning("Unable to set the selected item of " + listName + " to null.");
        } else if (index == -1) {
            logger.warning("Unable to set the selected item of " + listName + " to " + value + " because it is not in the list of valid values.");
        } else {
            comboBox.setSelectedIndex(index);
        }
    }

    /**
     * Sets the JTextField's text.  If the requested value is null,
     * the JTextField's text becomes the empty String.
     * 
     * @param textField a JTextField
     * @param value any String value
     */
    public void setText(JTextField textField, String value) {
        value = value == null ? "" : value;
        textField.setText(value);
    }
}