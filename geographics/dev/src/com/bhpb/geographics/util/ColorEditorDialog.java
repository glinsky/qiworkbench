/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JPanel;

/**
 * Allows the user to select from a predefined list of 13 colors, or any color
 * available to the standard Java JColorChooser.  This class is designed to be
 * functionality equivalent to the Horizon line/symbol color editor in the
 * bhpViewer component.
 * 
 * This dialog is not modal.
 */
public class ColorEditorDialog extends JDialog {

    public enum SELECTION {
        NONE, OK, CANCEL
    };
    
    private static final int PARENT_FRAME_OFFSET = 50;
    private static final Logger logger = Logger.getLogger(ColorEditorDialog.class.toString());
    //default size per old viewers
    private static final Dimension DEFAULT_EDITOR_SIZE = new Dimension(120,160);
    

    private ActionListener colorSelectionListener = new ColorSelectionListener();
    private Color selectedColor;
    private List<Color> defaultColors = new ArrayList<Color>();
    private SELECTION selection = SELECTION.NONE;

    public ColorEditorDialog(int x, int y, Color initialColor) {
        super.setLayout(new BorderLayout());
        selectedColor = initialColor;

        initDefaultColors();
        add(createColorSwatchPanel(), BorderLayout.CENTER);

        JButton moreColorsButton = new JButton("More Colors...");
        moreColorsButton.addActionListener(new MoreColorsActionListener());
        add(moreColorsButton, BorderLayout.SOUTH);

        setMinimumSize(DEFAULT_EDITOR_SIZE);
        setPreferredSize(DEFAULT_EDITOR_SIZE);
        setTitle("Color Dialog");

        super.pack();
        super.setLocation(x + PARENT_FRAME_OFFSET, y + PARENT_FRAME_OFFSET);
    }

    private JPanel createColorSwatchPanel() {
        JPanel csPanel = new JPanel();
        csPanel.setLayout(new GridLayout(4, 4));

        int index = 1;
        for (Color color : defaultColors) {
            csPanel.add(new ColorSwatch(color, colorSelectionListener));
            index++;
        }

        return csPanel;
    }

    private void initDefaultColors() {
        defaultColors.add(Color.ORANGE);
        defaultColors.add(Color.PINK);
        defaultColors.add(Color.RED);
        defaultColors.add(Color.WHITE);

        defaultColors.add(Color.YELLOW);
        defaultColors.add(Color.BLACK);
        defaultColors.add(Color.BLUE);
        defaultColors.add(Color.CYAN);

        defaultColors.add(Color.DARK_GRAY);
        defaultColors.add(Color.GRAY);
        defaultColors.add(Color.GREEN);
        defaultColors.add(Color.LIGHT_GRAY);

        defaultColors.add(Color.MAGENTA);
    }

    private class ColorSelectionListener implements ActionListener {

        public void actionPerformed(ActionEvent ae) {
            selectedColor = ((ColorSwatch) ae.getSource()).getColor();
            selection = SELECTION.OK;
            dispose();
        }
    }

    private class CancelActionListener implements ActionListener {

        public void actionPerformed(ActionEvent ae) {
            selection = SELECTION.CANCEL;
            dispose();
        }
    }

    private class MoreColorsActionListener implements ActionListener {

        public void actionPerformed(ActionEvent ae) {
            selectedColor = JColorChooser.showDialog(null, "More Colors", selectedColor);
            selection = SELECTION.OK;
            dispose();
        }
    }

    public SELECTION getSelection() {
        return selection;
    }

    public Color getColor() {
        return selectedColor;
    }

    public Color showDialog() {
        setVisible(true);
        while (selection == SELECTION.NONE) {
            try {
                Thread.sleep(500L);
            } catch (InterruptedException ie) {
                logger.warning(ie.getMessage());
            }
        }
        return selectedColor;
    }
}