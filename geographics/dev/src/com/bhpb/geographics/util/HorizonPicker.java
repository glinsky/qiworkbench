/*
###########################################################################
# This program module Copyright (C) 2009 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import com.bhpb.geographics.model.HorizonLayer;
import com.bhpb.geographics.model.PickingSettings.SNAPPING_MODE;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;

public class HorizonPicker {

    private HorizonLayer horizonLayer;
    private Integer previousTargetTraceIndex = null;

    public HorizonPicker(HorizonLayer horizonLayer) {
        this.horizonLayer = horizonLayer;
    }

    public void pick(Integer targetTraceIndex, float targetValue, SNAPPING_MODE snappingMode,
            int snapLimit, DataObject[] dataObjects, GeoFileDataSummary summary) {

        float[] mutableHorizon = horizonLayer.getMutableHorizon();
        mutableHorizon[targetTraceIndex] = targetValue;

        if (previousTargetTraceIndex != null) {
            snapRange(mutableHorizon, targetTraceIndex, snappingMode, snapLimit, dataObjects, summary);
        }

        previousTargetTraceIndex = targetTraceIndex;
        horizonLayer.setMutableHorizon(mutableHorizon);
    }

    private void snapRange(float[] mutableHorizon, int targetTraceIndex, SNAPPING_MODE snappingMode, int snapLimit,
            DataObject[] dataObjects, GeoFileDataSummary summary) {
        switch (snappingMode) {
            case NO_SNAP:
                doLinearInterpolation(mutableHorizon, targetTraceIndex);
                break;
            case SNAP_NEAREST_MAX:
                doLinearInterpolation(mutableHorizon, targetTraceIndex);
                snapNearest(mutableHorizon, targetTraceIndex, snappingMode, snapLimit, dataObjects, summary);
                break;
            case SNAP_NEAREST_MIN:
                doLinearInterpolation(mutableHorizon, targetTraceIndex);
                snapNearest(mutableHorizon, targetTraceIndex, snappingMode, snapLimit, dataObjects, summary);
                break;
            case SNAP_NEAREST_ZERO:
                doLinearInterpolation(mutableHorizon, targetTraceIndex);
                snapNearest(mutableHorizon, targetTraceIndex, snappingMode, snapLimit, dataObjects, summary);
                break;
            default:
                throw new UnsupportedOperationException("Unknown snapping mode: " + snappingMode);
        }
    }

    private void doLinearInterpolation(float[] mutableHorizon, int targetTraceIndex) {
        int indexSpan = targetTraceIndex - previousTargetTraceIndex;
        double slope = (mutableHorizon[targetTraceIndex] - mutableHorizon[previousTargetTraceIndex]) / (indexSpan);
        for (int i = previousTargetTraceIndex; i < targetTraceIndex; i++) {
            float deltaValue = (float) slope * (i - previousTargetTraceIndex);
            mutableHorizon[i] = mutableHorizon[previousTargetTraceIndex] + deltaValue;
        }
    }

    /**
     * Gets the nearest sample index (0-based) to the specified vertical
     * value and returns the nearest integer.  This depends on the GeoFileDataSummary
     * sampleRate of the file which generated the DataObjects being picked.
     * 
     * @param vertical the floating point vertical coordinate (time or depth)
     * 
     * @return the nearest sample index of the associated DataObject array
     */
    private int getNearestSampleIndex(float vertical, GeoFileDataSummary summary) {
        double nSamples = vertical / summary.getSampleRate();
        return (int) Math.round(nSamples);
    }

    //returns true if the value of the indicated trace at the indicated vector index
    //is 0.0, optionally less than EPSILON in absolute value or is one of the nearest
    //indices to a point where a zero-crossing can be inferred from a change in value sign.
    private boolean isZeroPoint(float[] values, int candidateIndex) {
        if (values[candidateIndex] == 0.0) {
            return true;
        }

        if (values.length == 1) {
            return false; //values array only has 1 value and it is not 0.0
        }

        float prevValue;
        float candidateValue = values[candidateIndex];
        float nextValue;

        if (candidateIndex == 0) {
            prevValue = values[0];
            nextValue = values[1];
        } else if (candidateIndex == values.length - 1) {
            prevValue = values[candidateIndex - 1];
            nextValue = values[candidateIndex];
        } else {
            prevValue = values[candidateIndex - 1];
            nextValue = values[candidateIndex + 1];
        }

        //if the candidate value is different in sign from the previous or next value
        //and is closer to the 0-point than the previous or next value, then return true
        //otherwise, return false - one of the neighbors is a 0-point

        if ((prevValue < 0 != candidateValue < 0) && Math.abs(prevValue) > Math.abs(candidateValue)) {
            return true;
        }

        if ((nextValue < 0 != candidateValue < 0) && Math.abs(nextValue) > Math.abs(candidateValue)) {
            return true;
        }

        //float array has at least 2 values and the candidate value is not 0.0,
        //nor is it smaller in absolute value than a neighbor with a different sign.
        //it is not a 0-point
        return false;
    }

    private boolean snapMatch(
            int candidateIndex,
            int minIndex,
            int maxIndex,
            SNAPPING_MODE snappingMode,
            float[] values) {
        switch (snappingMode) {
            case NO_SNAP:
                throw new UnsupportedOperationException("snapMatch() method is meaningless for mode NO_SNAP");
            case SNAP_NEAREST_MAX:
                return isMaximum(candidateIndex, minIndex, maxIndex, values);
            case SNAP_NEAREST_MIN:
                return isMinimum(candidateIndex, minIndex, maxIndex, values);
            case SNAP_NEAREST_ZERO:
                return isZeroPoint(values, candidateIndex);
            default:
                throw new UnsupportedOperationException("Unable to snap using unknown mode: " + snappingMode);
        }
    }

    private boolean isMaximum(int index, int minIndex, int maxIndex, float[] values) {
        float indexValue = values[index];
        for (int neighborIndex = index + 1; neighborIndex <= maxIndex; neighborIndex++) {
            float neighborValue = values[neighborIndex];
            if (neighborValue < indexValue) {
                break; // nearest non-equal neighor within range is less, index may be local max
            } else if (neighborValue > indexValue) {
                return false; //nearest non-equal neibhor is greater, value is not a local max
            } //else continue looking
        }
        for (int neighborIndex = index - 1; neighborIndex >= minIndex; neighborIndex--) {
            float neighborValue = values[neighborIndex];
            if (neighborValue < indexValue) { //nearest non-equal neighbor is greater, index is local max
                return true;
            } else if (neighborValue > indexValue) {
                break; //nearest non-equal neighbor with smaller index is greater, index is not local max
            } //else continue looking
        }
        return false; //all adjacent value within range are == value at index; there is no local min, max or inflection point
    }

    private boolean isMinimum(int index, int minIndex, int maxIndex, float[] values) {
        float indexValue = values[index];
        for (int neighborIndex = index + 1; neighborIndex <= maxIndex; neighborIndex++) {
            float neighborValue = values[neighborIndex];
            if (neighborValue > indexValue) {
                break; // nearest non-equal neighor within range is greater, index may be local minimum
            } else if (neighborValue < indexValue) {
                return false; //nearest non-equal neibhor is less, value is not a local minimum
            } //else continue looking
        }
        for (int neighborIndex = index - 1; neighborIndex >= minIndex; neighborIndex--) {
            float neighborValue = values[neighborIndex];
            if (neighborValue > indexValue) { //nearest non-equal neighbor is greater, index is local min
                return true;
            } else if (neighborValue < indexValue) {
                break; //nearest non-equal neighbor with smaller index is less, index is not local min
            } //else continue looking
        }
        return false; //all adjacent value within range are == value at index; there is no local min, max or inflection point
    }

    private void snapNearest(float[] mutableHorizon, int targetTrace, SNAPPING_MODE snappingMode, int snapLimit,
            DataObject[] dataObjects, GeoFileDataSummary summary) {
        for (int i = previousTargetTraceIndex; i < targetTrace; i++) {
            float[] dataObjectVector = dataObjects[i].getFloatVector();
            int targetIndex = getNearestSampleIndex(mutableHorizon[i], summary);

            //max # of sample indices to snap is snapLimit/sampleRate
            int sampleLimit = (int) Math.round(((double) snapLimit) / ((double) summary.getSampleRate()));

            int minSnapIndex = Math.max(0, targetIndex - sampleLimit);
            int maxSnapIndex = Math.min(dataObjectVector.length - 1, targetIndex + sampleLimit);

            int snapIndex = targetIndex;
            int bestDistanceFromTarget = Integer.MAX_VALUE;

            for (int snapCandidateIndex = minSnapIndex; snapCandidateIndex <= maxSnapIndex; snapCandidateIndex++) {
                if (snapMatch(snapCandidateIndex, minSnapIndex, maxSnapIndex, snappingMode, dataObjectVector)) {
                    int distanceFromTarget = Math.abs(snapCandidateIndex - targetIndex);
                    if (bestDistanceFromTarget <= distanceFromTarget) {
                        break; //match distance from target is increasing, closest match has already been found
                    } else { // update the snapIndex if this is the first match,
                             // or it is strictly closer to the target index than the previous match
                        snapIndex = snapCandidateIndex;
                        bestDistanceFromTarget = distanceFromTarget;
                    }
                }
            }

            mutableHorizon[i] = (float) (snapIndex * summary.getSampleRate());
        }
    }
}