/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import com.bhpb.geographics.model.window.WindowScaleProperties.UNITS;
import java.awt.Toolkit;

/**
 * Contains static method usd to scale GeoPlot rendering
 * according to the physical display resolution reported by Toolkit.
 */
public class ScaleUtil {
    private static double CM_PER_INCH = 2.54;
    private static double IN_PER_CM = 0.3937007874016;
    private double pixelsPerInch;
    private double inchesPerPixel;

    /**
     * Constructs a ScaleUtil using the default AWT Toolkit's screen resolution.
     */
    public ScaleUtil() {
        this(Toolkit.getDefaultToolkit().getScreenResolution());
    }

    /**
     * Constructrs a ScaleUtil using the given screen resolution.
     * 
     * @param pixelsPerInch screen resolution in pixels per inch
     */
    public ScaleUtil(double pixelsPerInch) {
        this.pixelsPerInch = pixelsPerInch;
        inchesPerPixel = 1.0 / pixelsPerInch;
    }

    /**
     * Gets the scale such that the pixel : trace scale
     * is exactly 2:1 within the precision of Java double.
     * 
     * @return the default trace/cm scale for 2:1 pixel:trace ratio at 98dpi
     */
    public double getModelOrSeismicTraceScaleDefault() {
        return pixelsPerInch / (CM_PER_INCH * 2);
    }

    /**
     * Gets the scale such that the pixel : trace scale
     * is exactly 4:1 within the precision of Java double.
     * 
     * @return the default trace/cm scale for 4:1 pixel:trace ratio at 98dpi
     */
    public double getMapTraceScaleDefault() {
        return pixelsPerInch / (CM_PER_INCH * 4);
    }

    /**
     * Gets the scale such that the pixel : unit scale
     * is exactly 1:8 within the precision of Java double.
     * 
     * @return the default cm/unit scale for 1:8 pixel:unit ratio at 98dpi
     */
    public double getHorizonUnitScaleDefault() {
        return getSeismicUnitScaleDefault();
    }

    /**
     * Gets the scale such that the pixel : unit scale
     * is exactly 1:1 within the precision of Java double.
     * 
     * @return the default cm/unit scale for 1:1 pixel:unit ratio at 98dpi
     */
    public double getModelUnitScaleDefault() {
        return CM_PER_INCH / pixelsPerInch;
    }

    /**
     * Gets the scale such that the pixel : unit scale
     * is exactly 1:8 within the precision of Java double.
     * 
     * @return the default cm/unit scale for 1:8 pixel:unit ratio at 98dpi
     */
    public double getSeismicUnitScaleDefault() {
        return CM_PER_INCH / (pixelsPerInch * 8);
    }

    double getPixelsPerInch() {
        return pixelsPerInch;
    }

    public double getPixelsPerUnit(double scale) {
        return calcNumPixels(scale, UNITS.CMS_PER_UNIT);
    }

    public double getPixelsPerTrace(double scale) {
        return calcNumPixels(scale, UNITS.TRACES_PER_CM);
    }

    //Package-protected for unit testing.
    double calcNumPixels(double scale, UNITS units) {
        switch (units) {
            case TRACES_PER_CM:
                /* 1 / (traces / cm * cm / in * in / pix) = pix/trace */
                return 1.0 / (scale * CM_PER_INCH * inchesPerPixel);
            case CMS_PER_UNIT:
                /* cms/unit * inches /cm * pixels/in = pix/unit */
                return scale * IN_PER_CM * pixelsPerInch;
            default:
                throw new IllegalArgumentException("Unknown units: " + units);
        }
    }

    public static String toString(UNITS units) {
        switch (units) {
            case CMS_PER_UNIT:
                return "CMs/Unit";
            case TRACES_PER_CM:
                return "Traces/CM";
            default:
                return units.toString();
        }
    }
}