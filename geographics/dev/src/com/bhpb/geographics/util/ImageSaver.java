/*
###########################################################################
# This program module Copyright (C) 2009 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class ImageSaver {

    private static final Logger logger =
            Logger.getLogger(ImageSaver.class.getName());

    public static void saveImage(String fileName, String imageFormatName, BufferedImage bufferedImage) {
        String errMsg = null;
        FileOutputStream outputFile = null;
        try {
            outputFile = new FileOutputStream(new File(fileName + "." + imageFormatName));

            List<String> writerFormatNames = Arrays.asList(ImageIO.getWriterFormatNames());

            if (!writerFormatNames.contains(imageFormatName)) {
                logger.warning("Image format name not available to ImageIO.write: " + imageFormatName);
                return;
            }

            ImageIO.write(bufferedImage, imageFormatName, outputFile);
        } catch (FileNotFoundException fnfe) {
            errMsg = fnfe.getMessage();
        } catch (IOException ioe) {
            errMsg = ioe.getMessage();
        } finally {
            if (outputFile != null) {
                try {
                    outputFile.close();
                } catch (IOException ioe) {
                    logger.warning("Caught while attempting to close FileOutputStream: " + ioe.getMessage());
                }
            }
        }

        if (errMsg != null) {
            logger.warning("Unable to save JPEG to file: " + fileName + " due to: " + errMsg);
        }
    }
}