/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * A ColorMap is a set of property names, each of which is assocated with one or more java.awt.Colors.
 * Many properties such as Foreground or Highlighting are associated with a single Color.  The 'Color Ramp'
 * property, however, is usually associated with an array of Colors and is used for density rendering in the qiViewer.
 *
 * This class also contains utility methods used to serialize the ColorMap in text
 * or XML format, and also methods to interpolate Colors between normalized data values.
 *
 * @author folsw9
 */
public class ColorMap implements Serializable {

    private static final Logger logger = Logger.getLogger(ColorMap.class.toString());
    public static final ColorMap DEFAULT_COLORMAP = new ColorMap(ColorMap.generateDefaultColorMap());
    public static final Color black_full_alpha = new Color(0, 0, 0, 255);
    public static final Color red_full_alpha = new Color(255, 0, 0, 255);
    public static final Color white_no_alpha = new Color(255, 255, 255, 0);
    public static final String FOREGROUND = "Foreground";
    public static final String BACKGROUND = "Background";
    public static final String POSFILL = "Positive MF";
    public static final String NEGFILL = "Negative MF";
    public static final String HIGHLIGHT = "Highlighting";
    public static final String RAMP = "Color Ramp";
    private static final String[] ORDERED_KEYS = {RAMP, POSFILL, NEGFILL, FOREGROUND, BACKGROUND, HIGHLIGHT};
    private Map<String, List<Color>> propertyColorListMap;

    /**
     * Create a ColorMap using the qiViewer default values for the standard propeties:
     * <p>
     * 
     * All are full Alpha (255). 
     * <ul>
     *   <li>POSFILL Color.BLACK</li>
     *   <li>NEGFILL: Color.BLACK</li>
     *   <li>FOREGROUND: Color.BLACK</li>
     *   <li>BACKGROUND: Color.WHITE</li>
     *   <li>HIGHLIGHT: Color.RED</li>
     * </ul>
     */
    public ColorMap() {
        this(generateDefaultColorMap());
    }

    /**
     * Initialize this ColorMap from the legacy format (bhpViewer application/component)
     * flat file.
     *
     * @param file File containing the ColorMap text.
     *
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     */
    public ColorMap(File file) throws FileNotFoundException, IOException {
        propertyColorListMap = new HashMap<String, List<Color>>();
        loadFromFile(file);
    }

    /**
     * Initialize this ColorMap from the legacy (bhpViewer application/component)
     * text format.
     *
     * @param string
     * @throws java.io.IOException
     */
    public ColorMap(String string) throws IOException {
        propertyColorListMap = new HashMap<String, List<Color>>();
        loadFromString(string);
    }

    /**
     * Initialize this ColorMap from a Map of Strings to Lists of java.awt.Color objects.
     * This method is usued to create ColorMaps such as the default ColorMap programatically.
     *
     * @param colorMap
     */
    public ColorMap(Map<String, List<Color>> colorMap) {
        if (colorMap == null) {
            throw new IllegalArgumentException("Map of property names to color lists must be non-null");
        }
        this.propertyColorListMap = colorMap;
    }

    /**
     * Instatiate this ColorMap as a copy of the specified ColorMap.
     *
     * @param cMap the ColorMap to be copied (must be non-null)
     */
    public ColorMap(ColorMap cMap) {
        if (cMap == null) {
            throw new IllegalArgumentException("Map of property names to color lists must be non-null");
        }
        this.propertyColorListMap = new HashMap<String, List<Color>>();
        Set set = cMap.propertyColorListMap.keySet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            List<Color> colors = cMap.propertyColorListMap.get(key);
            List<Color> newColors = new ArrayList<Color>();
            for (Color c : colors) {
                newColors.add(new Color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()));
            }
            propertyColorListMap.put(key, newColors);
        }
    }

    /**
     * Returns true if this ColorMap contains a property which is an exact String match
     * for the propertyName parameter.
     *
     * @param propertyName String an arbitrary propertyName
     *
     * @return true if this ColorMap contains the specified property name
     */
    public boolean containsProperty(String propertyName) {
        return propertyColorListMap.containsKey(propertyName);
    }

    /**
     * Given given normalized start and end values associated with start and end coordinates,
     * and the number of colors in a color map, uses linear interpolation to calculate
     * the starting coordinates of any colors between those associated with the start and end values.
     * If the start and end values are the same, then two Point2Ds are returned.  The Point2D at index 0
     * corresponds to (startCoord, colormap index of startVal) and (endCoord, undefined y value).
     *
     * Otherwise, for every Point2D 'i' in the returned array, the x coordinate is the starting coordinate of
     * the line segment associated with the color given by the value of the y coordinate.  The final Point2D
     * of the array has an undefined y value.
     *
     * @param startCoord the (horizontal) coordinate at which to start interpolation
     * @param endCoord the (horizontal) coordinate at which to end interpolation
     * @param startVal the value associated with start coordinate, must be >= -1.0 and <= 1.0
     * @param endVal the value associated with end coordinate, must be >= -1.0 and <= 1.0
     *
     * @return array of Point2Ds containing the starting point (x) and color index (y)
     * of every color transition in the ColorRamp spectrum from startVal to endVal,
     * terminated by the ending point of the final color in the specified range.
     */
    public Point2D[] doLinearInterp(double startCoord, double endCoord, double startVal,
            double endVal) {
        if (endCoord < startCoord) {
            throw new IllegalArgumentException("endCoord < startCoord");
        }

        List<Point2D> interpolatedPoints = new ArrayList<Point2D>();

        int firstColorIndex = getColorIndex(ColorMap.RAMP, startVal);

        interpolatedPoints.add(new Point2D.Double(startCoord, firstColorIndex));

        //calculate intermediate coordinates and colors
        double[] colorBounds = getSpectrumBounds(ColorMap.RAMP);
        int nColors = colorBounds.length - 1;

        if (startVal < endVal) {
            for (int nextColorIndex = firstColorIndex + 1; nextColorIndex < nColors; nextColorIndex++) {
                double colorBound = colorBounds[nextColorIndex];
                if (colorBound >= endVal) {
                    break;
                }
                double colorStartCoord = (colorBound - startVal) * (endCoord - startCoord) / (endVal - startVal) + startCoord;
                interpolatedPoints.add(new Point2D.Double(colorStartCoord,
                        nextColorIndex));
            }
        } else if (endVal < startVal) {
            for (int nextColorIndex = firstColorIndex - 1; nextColorIndex >= 0; nextColorIndex--) {
                double colorBound = colorBounds[nextColorIndex + 1];
                if (colorBound <= endVal) {
                    break;
                }
                double colorStartCoord = (colorBound - startVal) * (endCoord - startCoord) / (endVal - startVal) + startCoord;
                interpolatedPoints.add(new Point2D.Double(colorStartCoord,
                        nextColorIndex));
            }
        } //if startVal == endVal, there is only 1 line segment, which uses startColor

        //last coordinate is simply a line segment endpoint and has no color value
        interpolatedPoints.add(new Point2D.Double(endCoord, 0));

        return interpolatedPoints.toArray(new Point2D[0]);
    }

    /**
     * Returns true if all of the following conditions are true
     * <ol>
     *   <li>other is an instance of ColorMap</li>
     *   <li>other has the same number of properties as this ColorMap</li>
     *   <li>every property of other is also a property of this ColorMap</li>
     *   <li>every property shared by this ColorMap and the other ColorMap
     *       is associated with an identical array of Colors</li>
     * </ol>
     * @param other
     *
     * @return true if this ColorMap is equivalent to the other ColorMap
     */
    @Override
    public boolean equals(Object other) {
        if (other instanceof ColorMap) {

            ColorMap otherMap = (ColorMap) other;

            if (otherMap.getNumProperties() != propertyColorListMap.keySet().size()) {
                logger.fine("Colormaps have different numbers of property names.");
                return false;
            } else {
                for (String propertyName : otherMap.getProperties()) {
                    if (!propertyColorListMap.keySet().contains(propertyName)) {
                        logger.fine("colorMap keyset does not contain: " + propertyName);
                        return false;
                    } else {
                        Color[] myColors = otherMap.getColors(propertyName);
                        List<Color> colors = propertyColorListMap.get(propertyName);

                        if (myColors.length != colors.size()) {
                            logger.fine("Color lists for " + propertyName + " have different sizes.");
                            return false;
                        } else {
                            for (int i = 0; i < myColors.length; i++) {
                                Color myCurrentColor = myColors[i];
                                Color currentColor = colors.get(i);
                                if (myCurrentColor.getRed() != currentColor.getRed() || myCurrentColor.getGreen() != currentColor.getGreen() || myCurrentColor.getBlue() != currentColor.getBlue() || myCurrentColor.getAlpha() != currentColor.getAlpha()) {
                                    logger.fine("Color lists for " + propertyName + " have different values at index " + i);
                                    logger.fine("myColor vs. argColor:");
                                    logger.fine("Red: " + myCurrentColor.getRed() + " vs. " + currentColor.getRed());
                                    logger.fine("Green: " + myCurrentColor.getGreen() + " vs. " + currentColor.getGreen());
                                    logger.fine("Blue: " + myCurrentColor.getBlue() + " vs. " + currentColor.getBlue());
                                    logger.fine("Alpha: " + myCurrentColor.getAlpha() + " vs. " + currentColor.getAlpha());
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            return true;
        } else {
            logger.info("Other object is not a ColorMap, objects cannot be equal.");
            return false;
        }
    }

    /**
     * Gets the String representation of this ColorMap in XML format.
     *
     * @return String
     */
    public String genState() {
        StringBuffer contents = new StringBuffer();
        contents.append("<" + getXmlAlias() + ">\n");
        Set keys = propertyColorListMap.keySet();
        Iterator it = keys.iterator();
        while (it.hasNext()) {
            Object key = it.next();
            List<Color> colors = propertyColorListMap.get(key);
            if (colors != null && colors.size() > 0) {
                contents.append("<entry name=\"" + key + "\">\n");
                for (Color color : colors) {
                    contents.append("<color red=\"" + color.getRed() + "\" green=\"" + color.getGreen() + "\" blue=\"" + color.getBlue() + "\" alpha=\"" + color.getAlpha() + "\" />\n");
                }
                contents.append("</entry>\n");
            } else {
                throw new RuntimeException("Colormap must have at least one color for each property: " + key);
            }
        }
        contents.append("</" + getXmlAlias() + ">");
        return contents.toString();
    }

    /**
     * Creates a map of Strings to Lists of Color that defines the default
     * GeoRasterizer color scheme.  Foreground is black, background is white
     * and highlight is red.
     */
    public static Map<String, List<Color>> generateDefaultColorMap() {
        //Color ramp from full blue to 240,240,255 (neearly white)
        //then 255,240,240 (nearly white) to full red.
        final Color[] blue_red_ramp_32 = {
            new Color(0, 0, 255, 255), new Color(16, 16, 255, 255),
            new Color(32, 32, 255, 255), new Color(48, 48, 255, 255),
            new Color(64, 64, 255, 255), new Color(80, 80, 255, 255),
            new Color(96, 96, 255, 255), new Color(112, 112, 255, 255),
            new Color(128, 128, 255, 255), new Color(144, 144, 255, 255),
            new Color(160, 160, 255, 255), new Color(176, 176, 255, 255),
            new Color(192, 192, 255, 255), new Color(208, 208, 255, 255),
            new Color(224, 224, 255, 255), new Color(240, 240, 255, 255),
            new Color(255, 240, 240, 255), new Color(255, 224, 224, 255),
            new Color(255, 208, 208, 255), new Color(255, 192, 192, 255),
            new Color(255, 176, 176, 255), new Color(255, 160, 160, 255),
            new Color(255, 144, 144, 255), new Color(255, 128, 128, 255),
            new Color(255, 112, 112, 255), new Color(255, 96, 96, 255),
            new Color(255, 80, 80, 255), new Color(255, 64, 64, 255),
            new Color(255, 48, 48, 255), new Color(255, 32, 32, 255),
            new Color(255, 16, 16, 255), new Color(255, 0, 0, 255)
        };

        Map<String, List<Color>> colorMap = new HashMap<String, List<Color>>();
        colorMap.put("Positive MF", Arrays.asList(black_full_alpha));
        colorMap.put("Negative MF", Arrays.asList(black_full_alpha));
        colorMap.put("Foreground", Arrays.asList(black_full_alpha));
        colorMap.put("Background", Arrays.asList(white_no_alpha));
        colorMap.put("Highlighting", Arrays.asList(red_full_alpha));
        colorMap.put("Color Ramp", Arrays.asList(blue_red_ramp_32));
        return colorMap;
    }

    /**
     * Convenience method to get the first element of the List of Colors associated
     * with property ColorMap.BACKGROUND.
     *
     * @return Color
     */
    public Color getBackgroundColor() {
        if (!containsProperty(ColorMap.BACKGROUND)) {
            throw new IllegalArgumentException("ColorMap does not contain a color for property: " + ColorMap.BACKGROUND);
        }
        return getColors(ColorMap.BACKGROUND)[0];
    }

    /**
     * Map the indices of Colors asociated with the specified propertyName onto
     * the floating point range[-1.0,1.0].  For normalizedSamples between -1.0 and
     * 1.0 inclusive, the nearest color value is returned.  For floating point values
     * outside of this range, the first or last Color in the map is returned.
     *
     * If Math.abs(colorMapMax - colorMapMin) is less than 0.00001, then all
     * samples will be normalized to 0.0.
     *
     * @param sample The sample value for which the Color is to be
     * determined, which will be normalized to fall within the range [-1.0, 1.0]
     *
     * @return The color associated with the normalizedSample, rounded up if
     * noramlizedSample falls exactly between two indices, e.g. 0.0 with a 2-color map.
     *
     * @throws IllegalArgumentException if propertyName is not contained in this ColorMap
     */
    public Color getColor(String propertyName, double sample, double colorMapMin, double colorMapMax) {
        if (!containsProperty(propertyName)) {
            throw new IllegalArgumentException("ColorMap does not contain a map for the property name: " + propertyName);
        } else {
            double normalizedSample;

            double normRange = (colorMapMax - colorMapMin) / 2.0;
            double normMidpoint = colorMapMin + normRange;

            if (Math.abs(normRange) < 0.00001) {
                normalizedSample = 0.0f;
            } else if (sample <= normMidpoint - normRange) {
                normalizedSample = -1.0f;
            } else if (sample >= normMidpoint + normRange) {
                normalizedSample = 1.0f;
            } else {
                normalizedSample = (sample - normMidpoint) / normRange;
            }

            return getColor(propertyName, normalizedSample);
        }
    }

    /**
     * Map the indices of Colors asociated with the specified propertyName onto
     * the floating point range[-1.0,1.0].  For normalizedSamples between -1.0 and
     * 1.0 inclusive, the nearest color value is returned.  For floating point values
     * outside of this range, the first or last Color in the map is returned.
     *
     * @param propertyName name of the property associated with the color map to use
     *
     * @param normalizedSample The sample value for which the Color is to be
     * determined, which has been normalized to fall within the range [-1.0, 1.0]
     *
     * @return The color associated with the normalizedSample, rounded up if
     * noramlizedSample falls exactly between two indices, e.g. 0.0 with a 2-color map.
     *
     * @throws IllegalArgumentException if propertyName is not contained in this ColorMap
     */
    public Color getColor(String propertyName, double normalizedSample) {
        Color[] map = getColors(propertyName);

        return map[getColorIndex(propertyName, normalizedSample)];
    }

    /**
     * For the given property name, return the index of the color which is associated
     * with the range containing the specified normalized sample value.
     *
     * @param propertyName
     * @param normalizedSample
     *
     * @return index of the color associated with the normalizedSample
     */
    public int getColorIndex(String propertyName, double normalizedSample) {
        Color[] map = getColors(propertyName);
        int nColors = map.length;

        int index = (int) (nColors * (normalizedSample + 1.0) / 2.0);

        if (index < 0) {
            return 0;
        } else if (index >= nColors) {
            return nColors - 1;
        } else {
            return index;
        }
    }

    /**
     *
     * @param propertyName
     *
     * @return the array of Colors associated with the given property name
     */
    public Color[] getColors(String propertyName) {
        return propertyColorListMap.get(propertyName).toArray(new Color[0]);
    }

    /**
     * Convenience method to get the first element of the List of Colors associated
     * with property ColorMap.FOREGROUND.
     *
     * @return Color
     */
    public Color getForegroundColor() {
        if (!containsProperty(ColorMap.FOREGROUND)) {
            throw new IllegalArgumentException("ColorMap does not contain a color for property: " + ColorMap.FOREGROUND);
        }
        return getColors(ColorMap.FOREGROUND)[0]; // foreground may contin multiple colors but should not and only the first will be used

    }

    /**
     * Convenience method to get the first element of the List of Colors associated
     * with property ColorMap.HIGHLIGHT.
     *
     * @return Color
     */
    public Color getHighlightColor() {
        if (!containsProperty(ColorMap.HIGHLIGHT)) {
            throw new IllegalArgumentException("ColorMap does not contain a color for property: " + ColorMap.HIGHLIGHT);
        }
        return getColors(ColorMap.HIGHLIGHT)[0]; // highlight may contin multiple colors but should not and only the first will be used

    }

    /**
     * Convenience method to get the first element of the List of Colors associated
     * with property ColorMap.NEGFILL.
     *
     * @return Color
     */
    public Color getNegFillColor() {
        if (!containsProperty(ColorMap.NEGFILL)) {
            logger.info("ColorMap does not contain a color for property: " + ColorMap.NEGFILL + ", color for property: " + ColorMap.FOREGROUND + " will be used.");
            return getForegroundColor();
        } else {
            return getColors(ColorMap.NEGFILL)[0];
        }
    }

    /**
     * Gets the number of properties defined in this ColorMap.
     *
     * @return int
     */
    public int getNumProperties() {
        return propertyColorListMap.size();
    }

    /**
     * Convenience method to get the first element of the List of Colors associated
     * with property ColorMap.POSFILL.
     *
     * @return Color
     */
    public Color getPosFillColor() {
        if (!containsProperty(ColorMap.POSFILL)) {
            logger.info("ColorMap does not contain a color for property: " + ColorMap.POSFILL + ", color for property: " + ColorMap.FOREGROUND + " will be used.");
            return getForegroundColor();
        } else {
            return getColors(ColorMap.POSFILL)[0];
        }
    }

    /**
     * Gets an array containing the names of properties defined in this ColorMap.
     *
     * @return array of property Strings in arbitrary order
     */
    public String[] getProperties() {
        return propertyColorListMap.keySet().toArray(new String[0]);
    }

    /**
     * Gets the length of the normalized data range from -1.0 to 1.0 inclusive which is mapped
     * to a single Color of the spectrum associated with the given propertyName.
     *
     * @param propertyName String any property Name defined in this ColorMap
     *
     * @throws IllegalArgumentException if the specified propertyName is null or is not defined in this
     * ColorMap
     *
     * @return double 2.0 divided by the number of Colors in the associated List of Colors
     */
    public double getRangePerColor(String propertyName) {
        if (propertyName == null) {
            throw new IllegalArgumentException("Cannot get range per color for null propertyName");
        }

        if (!containsProperty(propertyName)) {
            throw new IllegalArgumentException("ColorMap does not contain a property named " + propertyName);
        }
        int nColorsInMap = getColors(propertyName).length;
        return 2.0 / nColorsInMap;
    }

    /**
     * Gets the points along the normalized data range from -1.0 to 1.0 inclusive
     * which coincide with the boundaries between adjacent colors of the List of Colors
     * associated with the given property name.  The array will have 1 more element
     * than the number of Colors in the spectrum associated with the property name.
     *
     * @param propertyName String propertyName
     * @throws IllegalArgumentException if propertyName is null or not defined in this ColorMap
     *
     * @return array of Color boundaries from -1.0 to 1.0
     */
    public double[] getSpectrumBounds(String propertyName) {
        if (propertyName == null) {
            throw new IllegalArgumentException("Cannot get spectrum bounds for null propertyName");
        }

        if (!containsProperty(propertyName)) {
            throw new IllegalArgumentException("ColorMap does not contain a property named " + propertyName);
        }

        Color[] colorRamp = getColors(propertyName);
        double[] colorBounds = new double[colorRamp.length + 1];
        double rangePerColor = getRangePerColor(propertyName);

        for (int i = 0; i < colorBounds.length; i++) {
            colorBounds[i] = i * rangePerColor - 1.0;
        }

        return colorBounds;
    }

    /**
     * Get the concise alias for the ColorMap class.
     * 
     * @return String "Colormap"
     */
    public static String getXmlAlias() {
        return "Colormap";
    }

    @Override
    public int hashCode() {
        int hashcode = 0;
        for (Entry<String, List<Color>> entry : propertyColorListMap.entrySet()) {
            String propName = entry.getKey();
            hashcode += propName.hashCode();
            List<Color> cList = entry.getValue();
            hashcode += cList.hashCode();
        }
        return hashcode;
    }

    private void loadFromFile(File file) throws FileNotFoundException, IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        loadFromReader(reader);
    }

    private void loadFromString(String string) throws IOException {
        BufferedReader reader = new BufferedReader(new StringReader(string));
        loadFromReader(reader);
    }

    /**
     * Populates this ColorMap with data read from the input BufferedReader.
     * Closes the bufferedReader when finished or when an error occurs.
     *
     * @param reader
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     */
    private void loadFromReader(BufferedReader reader) throws IOException {
        String line = null;
        int nColorsToBeRead = 0;
        String currentPropertyName = null;
        List<Color> currentColorList = null;
        line = reader.readLine();
        try {
            while (line != null) {
                if (line == null) {
                    break;
                }
                if (nColorsToBeRead > 0) {
                    String workingLine = line;

                    int index = workingLine.indexOf(' ');
                    if (index == -1) {
                        throw new RuntimeException("Each colormap file line specifying a color must contain exactly 4 integers, but read: " + line);
                    }

                    String subString = workingLine.substring(0, index);
                    try {
                        int redValue = Integer.valueOf(subString.trim()).intValue();

                        workingLine = workingLine.substring(index + 1);
                        index = workingLine.indexOf(' ');
                        if (index == -1) {
                            throw new RuntimeException("Each colormap file line specifying a color must contain exactly 4 integers, but read: " + line);
                        }
                        subString = workingLine.substring(0, index);
                        int greenValue = Integer.valueOf(subString.trim()).intValue();

                        workingLine = workingLine.substring(index + 1);
                        index = workingLine.indexOf(' ');
                        if (index == -1) {
                            throw new RuntimeException("Each colormap file line specifying a color must contain exactly 4 integers, but read: " + line);
                        }
                        subString = workingLine.substring(0, index);
                        int blueValue = Integer.valueOf(subString.trim()).intValue();

                        workingLine = workingLine.substring(index + 1);
                        int alphaValue = Integer.valueOf(workingLine.trim()).intValue();

                        currentColorList.add(new Color(redValue, greenValue, blueValue, alphaValue));
                    //logger.info("Read color:  " + redValue + " " + greenValue + " " + blueValue + " " + alphaValue + ".  " + nColorsToBeRead + " colors remain for property: " + currentPropertyName);

                    } catch (NumberFormatException nfe) {
                        throw new RuntimeException("Unable to parse the current line, a colormap file line specifying a color value must have exactly 4 space-delimited 8-bit values, failed on line: " + line + " while parsing the substring '" + subString + "'.", nfe);
                    }
                    nColorsToBeRead--;
                    if (nColorsToBeRead == 0) {
                        logger.info("0 colors remain to be read, putting '" + currentPropertyName + "' into colorMap with " + currentColorList.size() + " colors.");
                        propertyColorListMap.put(currentPropertyName, currentColorList);
                    }
                } else {
                    String[] splitLine = line.split("=");
                    if (splitLine.length != 2) {
                        throw new RuntimeException("Invalid file - expected 'propertyname = numColors' but read: " + line);
                    } else {
                        currentPropertyName = splitLine[0].trim();
                        currentColorList = new ArrayList<Color>();
                        try {
                            nColorsToBeRead = Integer.valueOf(splitLine[1].trim()).intValue();
                        } catch (NumberFormatException nfe) {
                            throw new RuntimeException("Each colormap file line specifying a property must be of the format 'propertyname = numColors', failed on line: " + line +
                                    " because the number of colors '" + splitLine[1] + "' could not be parsed as an Integer.", nfe);
                        }
                    }
                }
                line = reader.readLine();
            }
        } finally {
            reader.close();
        }
    }

    /**
     * Returns a ColorMap initialized from the w3c.dom.Node named "ColorMap"
     * which has no attributes and contains an arbitrary number of elements of the format:
     * <entry name="Foreground">
     *   <color alpha="255" blue="0" green="0" red="0" />
     * </entry>
     *
     * Each entry element contains an arbitrary nonzero number of color elements.
     *
     * @param node
     *
     * @return ColorMap deserialized from the Node
     */
    public static ColorMap restoreState(Node node) {
        if (!node.getNodeName().equals("Colormap")) {
            return null;
        }
        Map<String, List<Color>> colorMap = new HashMap<String, List<Color>>();
        NodeList children = node.getChildNodes();
        for (int i = 0; children != null && i < children.getLength(); i++) {

            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals("entry")) {
                    Element el = (Element) child;
                    String name = el.getAttribute("name");
                    if (name != null && name.length() > 0) {
                        NodeList grandChildren = child.getChildNodes();
                        List<Color> list = new ArrayList<Color>();
                        for (int j = 0; grandChildren != null && j < grandChildren.getLength(); j++) {
                            Node grandChild = grandChildren.item(j);
                            if (grandChild.getNodeType() == Node.ELEMENT_NODE) {
                                if (grandChild.getNodeName().equals("color")) {
                                    Element elem = (Element) grandChild;
                                    String att = elem.getAttribute("red");
                                    int red = Integer.valueOf(att).intValue();
                                    att = elem.getAttribute("blue");
                                    int blue = Integer.valueOf(att).intValue();
                                    att = elem.getAttribute("green");
                                    int green = Integer.valueOf(att).intValue();
                                    att = elem.getAttribute("alpha");
                                    int alpha = Integer.valueOf(att).intValue();
                                    Color c = new Color(red, green, blue, alpha);
                                    list.add(c);
                                }
                            }
                        }
                        colorMap.put(name, list);

                    }
                }
            }
        }
        if (colorMap != null) {
            return new ColorMap(colorMap);
        }
        return null;
    }

    /**
     * Writes this ColorMap to a file in the legacy text format, used to export
     * user-modified ColorMaps to the bhpViewer applications.
     *
     * @param file
     *
     * @throws java.io.IOException
     */
    public void saveToFile(File file) throws IOException {
        FileWriter writer = new FileWriter(file);
        try {
            Set<String> colorMapKeys = propertyColorListMap.keySet();
            List<String> processedKeys = new ArrayList<String>();
            //Write the known property names first in a specific order
            for (String propertyName : ORDERED_KEYS) {
                if (!colorMapKeys.contains(propertyName)) {
                    continue;
                }
                List<Color> colors = propertyColorListMap.get(propertyName);
                writeToFile(writer, colors, propertyName);
                processedKeys.add(propertyName);
            }
            //Then, write any unique user-defined properties in arbitrary order
            for (String propertyName : colorMapKeys) {
                if (processedKeys.contains(propertyName)) {
                    continue;
                }
                List<Color> colors = propertyColorListMap.get(propertyName);
                writeToFile(writer, colors, propertyName);
            }
        } finally {
            writer.close();
        }
    }

    /**
     * 
     * @param sbuf
     */
    private void saveToStringBuffer(StringBuffer sbuf) {
        Set<String> colorMapKeys = propertyColorListMap.keySet();
        List<String> processedKeys = new ArrayList<String>();
        //Write the known property names first in a specific order
        for (String propertyName : ORDERED_KEYS) {
            if (!colorMapKeys.contains(propertyName)) {
                continue;
            }
            List<Color> colors = propertyColorListMap.get(propertyName);
            writeToStringBuffer(sbuf, colors, propertyName);
            processedKeys.add(propertyName);
        }
        //Then, write any unique user-defined properties in arbitrary order
        for (String propertyName : colorMapKeys) {
            if (processedKeys.contains(propertyName)) {
                continue;
            }
            List<Color> colors = propertyColorListMap.get(propertyName);
            writeToStringBuffer(sbuf, colors, propertyName);
        }
    }

    /**
     * Set the color map associated with propertyName.
     * 
     * @throws IllegalArgumentException if propertyName or colors are null,
     * or if colors is an empty List.
     * 
     * @param propertyName
     * @param colors
     */
    public void setColors(String propertyName, List<Color> colors) {
        if (propertyName == null) {
            throw new IllegalArgumentException("Cannot setColors for null propertyName");
        }
        if (colors == null) {
            throw new IllegalArgumentException("Cannot setColors using null color List");
        }
        if (colors.size() == 0) {
            throw new IllegalArgumentException("Cannot setColors using an empty color list");
        }
        propertyColorListMap.put(propertyName, colors);
    }

    private void writeToFile(FileWriter writer, List<Color> colors, String propertyName) throws IOException {
        final String LINE_SEPARATOR = System.getProperty("line.separator");

        if (colors == null) {
            writer.write(propertyName + " = 0" + LINE_SEPARATOR);
        } else {
            writer.write(propertyName + " = " + colors.size() + LINE_SEPARATOR);
            for (Color color : colors) {
                StringBuilder sb = new StringBuilder(Integer.valueOf(color.getRed()).toString());
                sb.append(" ");
                sb.append(Integer.valueOf(color.getGreen()).toString());
                sb.append(" ");
                sb.append(Integer.valueOf(color.getBlue()).toString());
                sb.append(" ");
                sb.append(Integer.valueOf(color.getAlpha()).toString());
                sb.append(LINE_SEPARATOR);
                writer.write(sb.toString());
            }
        }
    }

    private void writeToStringBuffer(StringBuffer sbuf, List<Color> colors, String propertyName) {
        final String LINE_SEPARATOR = System.getProperty("line.separator");

        if (colors == null) {
            sbuf.append(propertyName + " = 0" + LINE_SEPARATOR);
        } else {
            sbuf.append(propertyName + " = " + colors.size() + LINE_SEPARATOR);
            for (Color color : colors) {
                StringBuilder sb = new StringBuilder(Integer.valueOf(color.getRed()).toString());
                sb.append(" ");
                sb.append(Integer.valueOf(color.getGreen()).toString());
                sb.append(" ");
                sb.append(Integer.valueOf(color.getBlue()).toString());
                sb.append(" ");
                sb.append(Integer.valueOf(color.getAlpha()).toString());
                sb.append(LINE_SEPARATOR);
                sbuf.append(sb.toString());
            }
        }
    }
}