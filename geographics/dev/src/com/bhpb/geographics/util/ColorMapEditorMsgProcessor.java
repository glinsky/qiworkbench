/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import com.bhpb.geographics.util.ColorMapEditorAgent.FILECHOOSER_ACTION;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

public class ColorMapEditorMsgProcessor {
    private Logger logger = Logger.getLogger(ColorMapEditorMsgProcessor.class.toString());
    private ColorMapEditorAgent agent;
    
    public ColorMapEditorMsgProcessor(ColorMapEditorAgent agent) {
        this.agent = agent;
    }
    
    /**
     * Process the given QiWorkbenchMsg, which may be either a request or a response.
     * 
     * @param msg The QiWorkbenchMsg to be processed.
     */
    public void processMsg(IQiWorkbenchMsg msg) {
        logger.finest("ViewerAgent::processMsg: msg=" + msg.toString());
        IMessagingManager messagingMgr = agent.getMessagingManager();
        try {
            // Check if a response. If so, process and consume response
            if (messagingMgr.isResponseMsg(msg)) {
                messagingMgr.checkForMatchingRequest(msg);
                // check if from the message dispatcher
                if (MsgUtils.getMsgCommand(msg).equals(QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD)) {
                        processFileChooserMsg(msg);
                }
            }
        //This catch block explicitly handles RuntimeExceptions in order to return
        //an abnormal response in the event of catastrophic component failure to handle message.
        } catch (RuntimeException ex) {
            String errorMessage = "Caught exception in ColorMapEditorMsgProcessor.ProcessMsg() : " + ex.getMessage();
            logger.warning(errorMessage);
        }
    }
    
    private void processFileChooserMsg(IQiWorkbenchMsg msg) {
        ArrayList list = (ArrayList) MsgUtils.getMsgContent(msg);
       
        if (((Integer) list.get(0)).intValue() == JFileChooser.APPROVE_OPTION) {
            String filePath = (String) list.get(1);
            String action = (String) list.get(3);

            if (action.equals(FILECHOOSER_ACTION.OPEN_COLORMAP.toString())) {
                agent.setChosenFileName(filePath);
            } else {
                logger.warning("Saving colormaps is not yet supported");
            }
        } else {
            logger.info("FileChooser response received but selection was cancelled.");
        }
    }
}