/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;

import com.bhpb.geographics.util.ColorMapEditorAgent.FILECHOOSER_ACTION;

/**
 * Allows the user to edit one colormap.  A colormap is defined as a set of 
 * properties and associated colors.  Each property may be associated with one or
 * multiple colors, but the type of association may not be changed from within
 * the editor.  Also, in this version, the user may only change the color values,
 * not add or subtract individual color values from the color map for a property.
 * In addition to these limitations, the user may not add or subtract properties from
 * a color map using this editor.  In other words, this class is designed to be
 * functionality equivalent to the color map editor of the INT viewer.
 * 
 * This dialog is not modal.
 */
public class ColorMapEditor extends JDialog {

    public enum SELECTION {

        NONE, OK, APPLY, CANCEL
    };
    
    public enum INTERPOLATION {

        NONE, LINEAR
    };

    public static final String NO_INTERPOLATION = "No Interpolation";
    public static final String LINEAR_INTERPOLATION = "Linear Interpolation";
    static final int PARENT_FRAME_OFFSET = 50;
    private static final Logger logger = Logger.getLogger(ColorMapEditor.class.toString());
    private ColorMap propertyColorMap;
    private ColorMapEditorAgent agent;
    private List<ColorPanel> editorPanels = new ArrayList<ColorPanel>();
    private SELECTION selection = SELECTION.NONE;
    private INTERPOLATION interpolation = INTERPOLATION.NONE;
    private JTabbedPane tabbedPane;
    public ColorMapEditor(int x, int y, ColorMap colorMap) {
        super.setLayout(new BorderLayout());
        super.setJMenuBar(createMenuBar());

        setColorMap(colorMap);

        super.pack();
        super.setLocation(x + PARENT_FRAME_OFFSET, y + PARENT_FRAME_OFFSET);

        agent = new ColorMapEditorAgent(this);
        new Thread(agent).start();
    }

    /**
     * If the selection is 'APPLY', reset it to 'NONE'.
     * Otherwise, an UnsupportedOperationException is thrown.
     */
    public void resetSelection() {
        if (selection != SELECTION.APPLY) {
            throw new UnsupportedOperationException("Error - resetSelection can only be called if the selection value is " + SELECTION.APPLY);
        } else {
            selection = SELECTION.NONE;
        }
    }

    private void setColorMap(ColorMap propertyColorMap) {
        logger.info("propertyColorMap has " + propertyColorMap.getNumProperties() + " entries.");

        if (getContentPane().getComponentCount() != 0) {
            getContentPane().removeAll();
        }

        this.propertyColorMap = propertyColorMap;
        getContentPane().add(createContentPanel());

        if (isVisible()) {
            validate();
        }
    }

    private JMenuBar createMenuBar() {
        JMenuBar menubar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");

        JMenuItem loadItem = new JMenuItem("Load...");
        loadItem.addActionListener(new LoadActionListener());
        JMenuItem saveItem = new JMenuItem("Save...");
        saveItem.addActionListener(new SaveActionListener());

        fileMenu.add(loadItem);
        fileMenu.add(saveItem);

        menubar.add(fileMenu);

        return menubar;
    }

    private class LoadActionListener implements ActionListener {

        public void actionPerformed(ActionEvent ae) {
            new Thread(new Runnable() {

                //start this action handler on a separate thread because it will need to allow
                //a dialog to pop up, hence it should not block the event dispatch Thread
                public void run() {
                    agent.invokeFileChooser(FILECHOOSER_ACTION.OPEN_COLORMAP);
                    String fileName = agent.getFileChooserResult();
                    try {
                        ColorMap newColorMap = new ColorMap(agent.getFileAsAscii(fileName));
                        setColorMap(newColorMap);
                    } catch (Exception ex) {
                        logger.warning("An error occurred while attemptingto choose, read or parse the colormap file: " + ex.getMessage());
                    }
                }
            }).start();
        }
    }

    private class SaveActionListener implements ActionListener {

        public void actionPerformed(ActionEvent ae) {
            JFileChooser fc = new JFileChooser();
            fc.showDialog(null, "Select ColorMap File");
            File selectedFile = fc.getSelectedFile();
            if (selectedFile == null) {
                logger.info("Selected file is null");
            } else {
                try {
                    getColorMap().saveToFile(selectedFile);
                } catch (FileNotFoundException fnfe) {
                    JOptionPane.showMessageDialog(null, fnfe.getMessage());
                } catch (IOException ioe) {
                    JOptionPane.showMessageDialog(null, ioe.getMessage());
                }
            }
        }
    }

    public ColorMap getColorMap() {
        return propertyColorMap;
    }

    private JPanel createContentPanel() {
        JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());
        contentPanel.add(createColorSwatchPanel(), BorderLayout.CENTER);
        contentPanel.add(createOkCancelPanel(), BorderLayout.SOUTH);
        return contentPanel;
    }

    private JPanel createColorSwatchPanel() {
        JPanel csPanel = new JPanel();
        csPanel.setLayout(new BorderLayout());
        JPanel colorTabbedPanel = new JPanel();
        colorTabbedPanel.setLayout(new BorderLayout());
        tabbedPane = createColorTabbedPane();
        colorTabbedPanel.add(tabbedPane,BorderLayout.CENTER);
        colorTabbedPanel.add(createShowColorIndexPanel(),BorderLayout.SOUTH);
        csPanel.add(colorTabbedPanel);
        csPanel.add(createInterpolateOptionPanel(), BorderLayout.SOUTH);
        return csPanel;
    }

    private JTabbedPane createColorTabbedPane() {
        JTabbedPane newTabbedPane = new JTabbedPane();

        if (propertyColorMap == null) {
            throw new IllegalArgumentException("Property color map cannot be null");
        } else if (propertyColorMap.getNumProperties() == 0) {
            throw new IllegalArgumentException("Property color map must have at least one key");
        } else {
            for (String key : propertyColorMap.getProperties()) {
                Color[] values = propertyColorMap.getColors(key);
                //logger.info("Creating colorEditorPanel for: " + key);
                ColorPanel editorPanel = new ColorPanel(key, Arrays.asList(values),interpolation);
                editorPanels.add(editorPanel);
                newTabbedPane.addTab(key, editorPanel);
            }
        }

        return newTabbedPane;
    }

    private JPanel createShowColorIndexPanel() {
        JPanel showColorIndexpPanel = new JPanel();
        showColorIndexpPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        JCheckBox showColorIndexCheckBox = new JCheckBox("Show Color Index");
        showColorIndexCheckBox.setSelected(true);
        showColorIndexCheckBox.addActionListener(new ShowColorIndexActionListener());
        showColorIndexpPanel.add(showColorIndexCheckBox);

        return showColorIndexpPanel;
    }

    
    private JPanel createInterpolateOptionPanel() {
        JPanel interpPanel = new JPanel();
        interpPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        JRadioButton noInterpBtn = new JRadioButton(NO_INTERPOLATION);
        noInterpBtn.addActionListener(new InterpolationActionListener());
        noInterpBtn.setActionCommand(NO_INTERPOLATION);
        JRadioButton linearInterpBtn = new JRadioButton(LINEAR_INTERPOLATION);
        linearInterpBtn.setActionCommand(LINEAR_INTERPOLATION);
        linearInterpBtn.addActionListener(new InterpolationActionListener());
        ButtonGroup btnGrp = new ButtonGroup();
        btnGrp.add(noInterpBtn);
        btnGrp.add(linearInterpBtn);
        
        if(interpolation == INTERPOLATION.NONE)
        	noInterpBtn.setSelected(true);
        else if(interpolation == INTERPOLATION.LINEAR)
        	linearInterpBtn.setSelected(true);
        
        JPanel interpPanel2 = new JPanel();
        interpPanel2.setLayout(new GridLayout(2, 1));
        interpPanel2.add(noInterpBtn);
        interpPanel2.add(linearInterpBtn);

        interpPanel.add(interpPanel2);

        return interpPanel;
    }

    private JPanel createOkCancelPanel() {
        JPanel okCancelPanel = new JPanel();
        okCancelPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));

        JButton okBtn = new JButton("Ok");
        okBtn.addActionListener(new OkActionListener());

        JButton applyBtn = new JButton("Apply");
        applyBtn.addActionListener(new ApplyActionListener());

        JButton cancelBtn = new JButton("Cancel");
        cancelBtn.addActionListener(new CancelActionListener());

        okCancelPanel.add(okBtn);
        okCancelPanel.add(applyBtn);
        okCancelPanel.add(cancelBtn);

        return okCancelPanel;
    }

    private class InterpolationActionListener implements ActionListener {
    	public void actionPerformed(ActionEvent e){
    		JRadioButton button = (JRadioButton)e.getSource();
   			for(int i = 0; i < tabbedPane.getTabCount(); i++){
   				ColorPanel cp = (ColorPanel)tabbedPane.getComponentAt(i);
   				if(button.getActionCommand().equals(LINEAR_INTERPOLATION)){
   					if(button.isSelected())
   						cp.setInterpolationMode(INTERPOLATION.LINEAR);
    				else
    					cp.setInterpolationMode(INTERPOLATION.NONE);
   				}else if(button.getActionCommand().equals(NO_INTERPOLATION)){
   					if(button.isSelected())
   						cp.setInterpolationMode(INTERPOLATION.NONE);
    				else
    					cp.setInterpolationMode(INTERPOLATION.LINEAR);
    			}
   			}
    	}
    }
    
    private class ShowColorIndexActionListener implements ActionListener {
        public void actionPerformed(ActionEvent ae) {
        	if(ae.getSource() instanceof JCheckBox){
        		JCheckBox checkBox = (JCheckBox)ae.getSource();
        		ColorPanel colorPanel = (ColorPanel)tabbedPane.getSelectedComponent();
        		if(!checkBox.isSelected()){
        	   		List<ColorSwatch> colorSwatches = colorPanel.getColorSwatches();
        	   		for(ColorSwatch cs : colorSwatches){
        	   			Dimension dim = cs.getPreferredSize();
        	   			cs.setText("");
        	   			cs.setPreferredSize(dim);
        	   		}
        		}else{
        	   		List<ColorSwatch> colorSwatches = colorPanel.getColorSwatches();
        	   		for(int i = 0; i < colorSwatches.size(); i++){
        	   			colorSwatches.get(i).setText(String.valueOf(i));
        	   		}
        		}
        	}
        }
    }
    
    private class OkActionListener implements ActionListener {

        public void actionPerformed(ActionEvent ae) {
            updateColorMap();
            selection = SELECTION.OK;
            dispose();
            agent.stopThread();
        }
    }

    private class ApplyActionListener implements ActionListener {

        public void actionPerformed(ActionEvent ae) {
            updateColorMap();
            selection = SELECTION.APPLY;
        }
    }

    private class CancelActionListener implements ActionListener {

        public void actionPerformed(ActionEvent ae) {
            selection = SELECTION.CANCEL;
            dispose();
            agent.stopThread();
        }
    }

    private void updateColorMap() {
        for (ColorPanel cPanel : editorPanels) {
            propertyColorMap.setColors(cPanel.getPropertyName(), cPanel.getColors());
        }
    }

    public SELECTION getSelection() {
        return selection;
    }
}