/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

public class QuadraticInterpolator extends AbstractInterpolator implements Interpolator {

    /**
     * Super naive implementation quadratic interpolation based on explanation
     * of Lagrangian interpolation at
     * http://numericalmethods.eng.usf.edu/ebooks/largrange_05inp_ebook.htm#Quadr
     * 
     * @param values original values (not modified)
     * @param sampleRate number of points to interpolate between original values
     *
     * @return interpolatedValues the output array consisting of original and interpolated values - it is (subSamples + 1) times as long as original array
     *
     */
    public float[] interpolate(float[] values, double sampleRate) {
        //kludge to use quadratic interpolation which requires the same integral number of interpolated points between all samples
        //the results are then subsampled to produce results of the requested size
        if (sampleRate < 1.0) {
            throw new IllegalArgumentException("SampleRate must be >= 1.0 for interpolation");
        }
        float[] interpolatedValues = new float[(int) Math.round((values.length - 1) * (sampleRate + 1) + 1)];

        int integralSampleRate = (int) Math.ceil(sampleRate);
        //System.out.println("sampleRate: " + sampleRate + ", integralSampleRate: " + integralSampleRate);
        float[] integralSampleRateInterpolatedValues = new float[(integralSampleRate + 1) * (values.length - 1) + 1];

        //seed the actual values
        for (int valIndex = 0; valIndex < values.length; valIndex++) {
            integralSampleRateInterpolatedValues[valIndex * (integralSampleRate+1)] = values[valIndex];
        }

        //now interpolate the intermediate values
        for (int index = 1; index < integralSampleRateInterpolatedValues.length - 1; index++) {

            //is it a known value? continue
            if (index % (integralSampleRate + 1) == 0) {
                continue;
            }
            int nearMiddleIndex = getNearMiddleIndex(index, integralSampleRate, interpolatedValues.length);

            //System.out.println("nmi: " + nearMiddleIndex);
            float[] coeffs = new float[3];
            for (int i = 0; i < 3; i++) {
                coeffs[i] = getCoeff(nearMiddleIndex, i, integralSampleRate, interpolatedValues, index);
            }

            //System.out.println("Coeffs: " + coeffs[0] + " " + coeffs[1] + " " + coeffs[2]);
            integralSampleRateInterpolatedValues[index] =
                    coeffs[0] * integralSampleRateInterpolatedValues[nearMiddleIndex - integralSampleRate - 1] + coeffs[1] * integralSampleRateInterpolatedValues[nearMiddleIndex] + coeffs[2] * integralSampleRateInterpolatedValues[nearMiddleIndex + integralSampleRate + 1];
        }

        double subSampleRate = sampleRate / ((double) integralSampleRate);
        float[] subSampledInterpolatedValues;
        if (Math.abs(subSampleRate - 1.0) < 0.00001) {
            subSampledInterpolatedValues = integralSampleRateInterpolatedValues;
        } else {
            subSampledInterpolatedValues = subSample(integralSampleRateInterpolatedValues, subSampleRate);
        }
        
        System.arraycopy(subSampledInterpolatedValues, 0, interpolatedValues, 0, Math.min(interpolatedValues.length, subSampledInterpolatedValues.length));
        return interpolatedValues;
    }

    private int getNearMiddleIndex(int index, int subSamples, int length) {
        int mod = index % (subSamples + 1);

        if (mod == 0) {
            //the interpolation index may not be a multiple of subSamples+1, 
            //in other words, an original value
            throw new UnsupportedOperationException("illegal interpolation index: " + index);
        }

        float fraction = (mod) / (subSamples + 1);
        int nmi;
        if (fraction >= 0.5) {
            nmi = index + (subSamples + 1 - mod);
        } else {
            nmi = index - mod;
        }
        if (nmi == 0) { // middle index cannot be first
            return subSamples + 1;
        } else if (nmi == length) {
            return length - subSamples - 1; // or last index
        } else {
            return nmi;
        }
    }

    private float getCoeff(int middleIndex, int coeff, int subSamples, float[] interpValues, int index) {
        float term = 1.0f;
        int[] indices = {middleIndex - subSamples - 1, middleIndex, middleIndex + subSamples + 1};
        for (int i = 0; i < 3; i++) {
            if (i == coeff) {
                continue;
            }
            // (t - tj) / (ti - tj) i != j
            //if (index == indices[i]) {
            //    System.out.println("index == indices[i] = " + index);
            //}
            term *= ((float) (index - indices[i])) / ((float) (indices[coeff] - indices[i]));
        }
        return term;
    }
}