/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.border.Border;

import com.bhpb.geographics.util.ColorMapEditor.INTERPOLATION;
import com.bhpb.qiworkbench.compAPI.BooleanSettings;

public class ColorPanel extends JPanel {

    private INTERPOLATION interpolation = INTERPOLATION.NONE;
    private static final transient Logger logger =
            Logger.getLogger(ColorPanel.class.toString());
    private static final int maxColumns = 7; //up to 7 columns of color swatches just like the INT viewer editor
    private static Border defaultButtonBorder = new JButton().getBorder(); //a default JButton border
    private static Border loweredBevelBorder = javax.swing.BorderFactory.createLoweredBevelBorder();
    private List<ColorSwatch> colorSwatches = new ArrayList<ColorSwatch>();
    private final String propertyName;
    
    public ColorPanel(String propertyName, List<Color> colors, INTERPOLATION interpolation) {
        super();

        setName(propertyName);

        this.propertyName = propertyName;
        this.interpolation = interpolation;
        setLayout(new FlowLayout());

        if (colors == null) {
            throw new IllegalArgumentException("Unable to create ColorPanel with null list of colors.");
        } else if (colors.size() == 0) {
            logger.info("No color defined for property: " + propertyName);
        } else {
            int nColors = colors.size();
            int nColumns = Math.min(maxColumns, nColors);
            int nRows = nColors / 7;
            if (nColors % 7 != 0) {
                nRows++;
            }
            JPanel colorGrid = createColorGridPanel(nColumns, nRows, colors,interpolation);
            add(colorGrid);
        }
    }

    private static String generateToolTip(String propertyName, int colorIndex, Color color){
    	if(colorIndex < 0 || color == null)
    		return null;
    	StringBuffer buff = new StringBuffer();
    	buff.append(propertyName + " Color");
    	buff.append(" #" + Integer.valueOf(colorIndex).toString());
    	String colorToolTip = " (" + color.getRed() + "," + color.getGreen() + "," + color.getBlue() + ")";
    	buff.append(colorToolTip);
    	return buff.toString();
    }
    
    public JPanel createColorGridPanel(int nColumns, int nRows, List<Color> colors, INTERPOLATION interpolation) {
        JPanel colorGridPanel = new JPanel();
        colorGridPanel.setLayout(new GridLayout(nRows, nColumns));
        for (int colorIndex = 0; colorIndex < colors.size(); colorIndex++) {
            Color color = colors.get(colorIndex);
            String toolTip = propertyName + " Color";
            if (colors.size() > 1) {
                toolTip = toolTip + " #" + Integer.valueOf(colorIndex).toString();
            }
            ColorSwatch colorSwatch = new ColorSwatch(color, toolTip, colorIndex);
            colorSwatch.setToolTipText(generateToolTip(propertyName, colorIndex, color));
            colorSwatch.addMouseListener(new ColorSwatchMouseAdapter(colorSwatch,this));
            colorSwatches.add(colorSwatch);
            colorGridPanel.add(colorSwatch);
        }

        return colorGridPanel;
    }

    public void setInterpolationMode(INTERPOLATION interp){
    	interpolation = interp;
    }
    
    public String getPropertyName() {
        return propertyName;
    }

    public List<ColorSwatch> getColorSwatches() {
        return colorSwatches;
    }

    public List<Color> getColors() {
        List<Color> colors = new ArrayList<Color>(colorSwatches.size());
        for (ColorSwatch swatch : colorSwatches) {
            colors.add(swatch.getColor());
        }
        return colors;
    }
    
    private class ColorSwatchMouseAdapter extends MouseAdapter{
    	private ColorSwatch colorSwatch; 
    	private ColorPanel colorPanel; 
    	private Timer delayTimer;
    	private int Delay = 300;
    	private boolean singleClick = true;
    	public ColorSwatchMouseAdapter(ColorSwatch colorSwatch,ColorPanel colorPanel){
    		this.colorSwatch = colorSwatch;
    		this.colorPanel = colorPanel;
    		ActionListener al = new ActionListener(){
    			public void actionPerformed(ActionEvent evt) {
    				if(singleClick){
    					executeSingleClickEvent();
    				}
    			}
    		};
    		delayTimer = new Timer(Delay, al);
    		delayTimer.setRepeats(false);
    	}
    	
    	private void executeSingleClickEvent(){
	    	if(colorPanel.interpolation == INTERPOLATION.NONE)
				colorSwatch.setBorder(defaultButtonBorder);
			else if(colorPanel.interpolation == INTERPOLATION.LINEAR){
				if(colorSwatch.isLoweredBevelBorder()){
					colorSwatch.setBorder(defaultButtonBorder);
					colorSwatch.setLoweredBevelBorder(new BooleanSettings(false));
				}else{
					colorSwatch.setBorder(loweredBevelBorder);
					colorSwatch.setLoweredBevelBorder(new BooleanSettings(true));
				}
				linearInterpolateColors();
			}
    	}
    	private void linearInterpolateColors(){
    		List<ColorSwatch> list = colorPanel.getColorSwatches();
			int start = 0;
			int end = 0;
			for(int i = 0; i < list.size(); i++){
				if(list.get(i).isLoweredBevelBorder()){
					start = i;
					break;
				}
			}
			
			for(int i = list.size()-1; i >= 0; i--){
				if(list.get(i).isLoweredBevelBorder()){
					end = i;
					break;
				}
			}
			if(start == end)
				return;
			else{
				int startRed = list.get(start).getColor().getRed();
				int endRed = list.get(end).getColor().getRed();
				int inc = (endRed-startRed) / (end-start);
				int [] redArray = formLinearColorArray(end-start-1,inc,startRed);
				
				int startGreen = list.get(start).getColor().getGreen();
				int endGreen = list.get(end).getColor().getGreen();
				inc = (endGreen-startGreen) / (end-start);
				int [] greenArray = formLinearColorArray(end-start-1,inc,startGreen);
				
				int startBlue = list.get(start).getColor().getBlue();
				int endBlue = list.get(end).getColor().getBlue();
				inc = (endBlue-startBlue) / (end-start);
				int [] blueArray = formLinearColorArray(end-start-1,inc,startBlue);
				
				int startAlpha = list.get(start).getColor().getAlpha();
				int endAlpha = list.get(end).getColor().getAlpha();
				inc = (endAlpha-startAlpha) / (end-start);
				int [] alphaArray = formLinearColorArray(end-start-1,inc,startAlpha);
				
				for(int i = 0; i < end-start-1; i++){
					Color color = new Color(redArray[i],greenArray[i],blueArray[i],alphaArray[i]);
					list.get(start+1+i).setColor(color);
					String toolTip = generateToolTip(propertyName, start+1+i, color);
					list.get(start+1+i).setToolTipText(toolTip);
				}
			}
    	}
    	
    	private int[] formLinearColorArray(int arrayLength, int inc, int startColor){
    		int [] colorArr = new int[arrayLength];
    		if(inc != 0){
				for(int i = 0; i < arrayLength; i++){
					colorArr[i] = startColor + inc*(i+1);
				}
			}else{
				for(int i = 0; i < arrayLength; i++){
					colorArr[i] = startColor;
				}
			}
    		return colorArr;
    	}
    	
 
    	public void mousePressed(MouseEvent e){
    		if(SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 1){
				singleClick = true;
				delayTimer.start();
			}else if(SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2){
				singleClick = false;
				Color chosenColor = JColorChooser.showDialog(colorSwatch, colorSwatch.getToolTip(), colorSwatch.getColor());
	            if (chosenColor != null) {
	                colorSwatch.setColor(chosenColor);
	                if(colorSwatch.isLoweredBevelBorder()){
	                	colorSwatch.setBorder(loweredBevelBorder);
	                	linearInterpolateColors();
	                }
	                String toolTip = colorSwatch.getToolTip();
	                String colorToolTip = " (" + chosenColor.getRed() + "," + chosenColor.getGreen() + "," + chosenColor.getBlue() + ")";
	                colorSwatch.setToolTipText(toolTip + colorToolTip);
	            } else {
	                logger.info("Color picking canceled.");
	            }
			}
		}
    }
}