/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.util;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * DecimalFormatter which can return a variable-precision
 * String representation of a double-precision number according to the following rule.
 * 
 * N represents the distance between 'tics' on the Annotation axis.
 * D represents the number of decimal places in the formatted String
 * M is the maximum number of decimal places which can be returned by this AnnotationNumberFormatter
 * 
 * D is the maximum integer between 0 and M inclusive such as N >= 10^(-D)
 * 
 * AnnotationFormatter can determine the number of decimal places for a given scale,
 * and can format a double-precision number with given number of decimal places.
 * 
 * @author folsw9
 */
public class DecimalFormatter {
    private static final Map<Integer, DecimalFormatter> instanceMap = new HashMap<Integer, DecimalFormatter>();
    
    private final int maxPrecision;
    private DecimalFormat[] omitTrailingZeroes;
    private DecimalFormat[] showTrailingZeroes;
    private String[] formatStringsWithoutZeroes;
    private String[] formatStringsWithZeroes;
    
    static public DecimalFormatter getInstance(int maxPrecision) {
        DecimalFormatter instance = instanceMap.get(maxPrecision);
        
        if (instance != null) {
            return instance;
        }
        
        instance = new DecimalFormatter(maxPrecision);
        instanceMap.put(maxPrecision, instance);
        return instance;
    }
    /**
     * Constructs an DecimalFormatter with the given maximum precison.
     * 
     * @param maxPrecision the maximum number of decimal places to display, must be between 0 and 10 inclusive
     */
     private DecimalFormatter(int maxPrecision) {
        if (maxPrecision < 0 || maxPrecision > 10) {
            throw new IllegalArgumentException("Max precision must be >= 0 and <= 10.");
        }
        this.maxPrecision = maxPrecision;

        formatStringsWithoutZeroes = buildFormatStrings(maxPrecision, "#########0", "#");
        formatStringsWithZeroes = buildFormatStrings(maxPrecision, "#########0", "0");

        omitTrailingZeroes = buildDecimalFormats(formatStringsWithoutZeroes);
        showTrailingZeroes = buildDecimalFormats(formatStringsWithZeroes);
    }

    private DecimalFormat[] buildDecimalFormats(String[] formatStringArray) {
        DecimalFormat[] decimalFormats = new DecimalFormat[formatStringArray.length];

        for (int i = 0; i < formatStringArray.length; i++) {
            decimalFormats[i] = new DecimalFormat(formatStringArray[i]);
        }

        return decimalFormats;
    }

    private String[] buildFormatStrings(int maxPrecision, String baseFormatString, String maskSuffix) {
        String[] formatStrings = new String[maxPrecision + 1];
        formatStrings[0] = baseFormatString;
        for (int i = 1; i <= maxPrecision; i++) {
            StringBuffer sbuf = new StringBuffer(formatStrings[i - 1]);
            if (i == 1) {
                sbuf.append("." + maskSuffix);
            } else {
                sbuf.append(maskSuffix);
            }
            formatStrings[i] = sbuf.toString();
        }

        return formatStrings;
    }

    /**
     * Return the greatest number N such that [0 <= N <= maxPrecision]
     * and scale >= 10^(-N).  If no such N exists, return maxPrecision.
     * 
     * @param scale the Annotation scale (distance between tics)
     * 
     * @return number of decimal places applicable to the scale
     */
    int getNumDecimalPlaces(double scale) {
        //negative scale is invalid
        if (scale < 0) {
            throw new IllegalArgumentException("Scale must be a positive number");
        //scale greater than or equal to 1 does not require decimal places
        } else if (scale >= 1.0) {
            return 0; //0 decimal places are needed if the scale is >= 1
        } else {
            // find the smallest N such that 10^-N is <= scale, or maxPrecision, whichever is smaller
            return (int) Math.min(maxPrecision, Math.abs(Math.floor(Math.log10(scale))));
        }
    }

    /**
     * Gets the formatted, rounded String representation of the given number
     * with the requested number of decimal places.
     * 
     * @param number the double-precision number to format
     * @param precision the maximum number of decimal places
     * 
     * @return String representation of number, with 0-10 decimal places
     */
    public String getFormattedString(double number, int precision) {
        return getFormattedString(number, precision, false);
    }

    /**
     * Gets the formatted, rounded String representation of the given number
     * with the requested number of decimal places.
     * 
     * @param number the double-precision number to format
     * @param precision the maximum number of decimal places
     * 
     * @return String representation of number, with 0-10 decimal places
     */
    public String getFormattedString(double number, int precision, boolean doShowTrailingZeroes) {
        if (doShowTrailingZeroes) {
            return showTrailingZeroes[precision].format(number);
        } else {
            return omitTrailingZeroes[precision].format(number);
        }
    }

    /**
     * Gets the formatted, rounded String representation of the given number
     * with the requested number of decimal places.
     * 
     * @param number the double-precision number to format
     * @param doShowTrailingZeroes if true, show insignificant zeroes, otherwise, omit them
     * 
     * @return String representation of number, with 0-10 decimal places
     */
    public String getFormattedString(double number, boolean doShowTrailingZeroes) {
        if (doShowTrailingZeroes) {
            return showTrailingZeroes[maxPrecision].format(number);
        } else {
            return omitTrailingZeroes[maxPrecision].format(number);
        }
    }

    /**
     * Gets the formatted, rounded String representation of the given number
     * at the indicated scale.
     * 
     * @param number any double-precision floating-point number
     * 
     * @param scale any positive, nonzero double-precision floating-point scale
     * 
     * @throws IllegalArgumentException if scale < 0.0
     * 
     * @return the String representation of number formatted for the given annotation scale
     */
    public String getFormattedString(double number, double scale) {
        if (scale <= 0.0) {
            throw new IllegalArgumentException("scale must be >= 0.0");
        }
        return getFormattedString(number, getNumDecimalPlaces(scale));
    }
}
