/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.layer.display.MapDisplayParameters;
import com.bhpb.geographics.model.layer.display.ModelXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParameters;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.logging.Logger;
import javax.swing.JComponent;

public class ColorBar extends JComponent {
    public enum ORIENTATION { HORIZONTAL, VERTICAL }

    private static final int INSET = 15;
    private static final Logger logger =
            Logger.getLogger(ColorBar.class.getName());
    private final ORIENTATION orientation;
    private final WindowModel windowModel;

    public ColorBar(ORIENTATION orientation, WindowModel windowModel) {
        this.orientation = orientation;
        this.windowModel = windowModel;

        switch(orientation) {
            case HORIZONTAL :
                setMinimumSize(new Dimension(INSET * 2, INSET));
                break;
            case VERTICAL :
                setMinimumSize(new Dimension(INSET, INSET*2));
                break;
            default :
                throw new IllegalArgumentException(orientation.toString());
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        Rectangle bounds = getBounds();

        Layer annotatedLayer = windowModel.getAnnotatedLayer();
        ColorMap colorMap;

        GeoDataOrder layerOrder = annotatedLayer.getOrientation();
        if (layerOrder == GeoDataOrder.MAP_VIEW_ORDER) {
            colorMap = ((MapDisplayParameters)annotatedLayer.getDisplayParameters()).getColorAttributes().getColorMap();
        } else if (layerOrder == GeoDataOrder.CROSS_SECTION_ORDER) {
            if (annotatedLayer.isSeismicLayer()) {
                    colorMap = ((SeismicXsecDisplayParameters)annotatedLayer.getDisplayParameters()).getColorMap();
            } else if (annotatedLayer.isModelLayer()) {
                    colorMap = ((ModelXsecDisplayParameters)annotatedLayer.getDisplayParameters()).getColorMap();
            } else {
                logger.warning("Unable to paint ColorBar for non-seismic, non-model cross-section (no ColorMap).");
                return;
            }
        } else {
            logger.warning("Unable to paint ColorBar for Layer in unknown orientation: " + layerOrder);
            return;
        }

        Color[] spectrum = colorMap.getColors(ColorMap.RAMP);

        double barLength;
        double swatchLength;

        switch(orientation) {
            case HORIZONTAL :
                barLength = bounds.width - 2 * INSET;
                swatchLength = barLength / spectrum.length;
                for (int i = 0; i < spectrum.length; i++) {
                    g.setColor(spectrum[i]);
                    //rectangle is 1 pixel longer than swatchlength due to truncation required by fillRect int params
                    g.fillRect(INSET + (int)(i * swatchLength), 0, (int)(swatchLength + 1), bounds.height);
                }
                g.setColor(Color.BLACK);
                g.drawRect(INSET, 0, (int)barLength, bounds.height);
                break;
            case VERTICAL :
                barLength = bounds.height - 2 * INSET;
                swatchLength = barLength / spectrum.length;
                for (int i = 0; i < spectrum.length; i++) {
                    g.setColor(spectrum[i]);
                    //rectangle is 1 pixel taller than swatchlength due to truncation required by fillRect int params
                    g.fillRect(0, INSET + (int)(i * swatchLength), bounds.width, (int)(swatchLength + 1));
                }
                g.setColor(Color.BLACK);
                g.drawRect(0, INSET, bounds.width, (int)barLength);
                break;
            default :
                throw new IllegalArgumentException(orientation.toString());
        }
    }
}
