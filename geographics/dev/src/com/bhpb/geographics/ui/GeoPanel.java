/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import com.bhpb.geographics.model.CursorSettings;
import com.bhpb.geographics.model.HorizonHighlightSettings;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.ViewTarget;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geographics.ui.AbstractGeoPlot.ACTION_MODE;
import com.bhpb.geographics.ui.GeoPanelBuilder.DECORATION_LOC;
import com.bhpb.geographics.ui.GeoPanelBuilder.DECORATION_TYPE;
import com.bhpb.geographics.ui.event.GeoPlotEventListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class GeoPanel extends JPanel {

    private static final long serialVersionUID = 2245250263105990577L;
    private static final Logger logger =
            Logger.getLogger(GeoPanel.class.toString());
    private AnnotationPane annotationPane;
    private CursorSettings cursorSettings;
    private AbstractGeoPlot geoPlot;
    private TargetInfoPanel targetInfoPanel;
    private WindowProperties winProps;
    private WindowModel windowModel;

    GeoPanel(int panelWidth, int panelHeight,
            int imageWidth, int imageHeight, Layer layer,
            WindowProperties winProps,
            CursorSettings cursorSettings,
            WindowModel windowModel) {
        super(new BorderLayout());
        this.winProps = winProps;
        this.cursorSettings = cursorSettings;
        this.windowModel = windowModel;

        Color bgColor = Color.WHITE;
        super.setBackground(bgColor);

        super.setPreferredSize(
                new Dimension(panelWidth,
                panelHeight));

        super.setMaximumSize(
                new Dimension(panelWidth,
                panelHeight));
    }

    public void addGeoPlotEventListener(GeoPlotEventListener listener) {
        geoPlot.addGeoPlotEventListener(listener);
    }

    public boolean isRendering() {
        return geoPlot.isRendering();
    }

    public void redraw(boolean doImmediately, boolean updatePlotSize) {
        annotationPane.repaintAnnotationPanels();
        geoPlot.redraw(doImmediately, updatePlotSize);
    }

    public void clearAnnotationPanels() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                annotationPane.clearHeaderPanels();
                //reset the geoPlot's size to its current visible size
                //since it will be empty
                //geoPlot.setSize(geoPlot.getVisibleRect().getSize());
                Dimension blankPlotSize = geoPlot.getViewportBounds().getSize();
                geoPlot.setPreferredSize(blankPlotSize);
                geoPlot.setMaximumSize(blankPlotSize);
                targetInfoPanel.setTargetInfo(ViewTarget.NO_TARGET);
                geoPlot.revalidate();
                revalidate();
            }
        });
    }

    public void updateAnnotationPanels() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                Layer annotatedLayer = windowModel.getAnnotatedLayer();
                JComponent columnHeader;
                JComponent rowHeader;
                if (annotatedLayer == null) {
                    columnHeader = null;
                    rowHeader = null;
                } else {
                    GeoPanelBuilder builder = GeoPanelBuilder.getInstance(annotatedLayer);

                    JComponent cornerPanel = builder.getDecoration(DECORATION_TYPE.CORNER_CAP, annotatedLayer, winProps, geoPlot);
                    columnHeader = builder.getDecoration(DECORATION_TYPE.H_ANNOT, annotatedLayer, winProps, geoPlot);
                    rowHeader = builder.getDecoration(DECORATION_TYPE.V_ANNOT, annotatedLayer, winProps, geoPlot);

                    //Now translate the configurable corner_cap location into a scrollpane location enum
                    DECORATION_LOC location = builder.getLocation(DECORATION_TYPE.CORNER_CAP);
                    //and set the annotation's corner to the new cornerpanel (keys, # of keys or order of keys may have changed)
                    annotationPane.setCorner(builder.asScrollPaneCorner(location), cornerPanel);
                }

                annotationPane.setColumnHeaderView(columnHeader);
                annotationPane.setRowHeaderView(rowHeader);

                targetInfoPanel.setTargetInfo(ViewTarget.NO_TARGET);
                geoPlot.revalidate();
            }
        });
    }

    public void setShutdown(boolean value) {
        geoPlot.setShutdown(value);
    }

    void setTargetInfoPanel(TargetInfoPanel targetInfoPanel) {
        this.targetInfoPanel = targetInfoPanel;
    }

    public ACTION_MODE getActionMode() {
        return geoPlot.getActionMode();
    }

    public ViewTarget getCursorTarget() {
        return geoPlot.getCursorTarget();
    }

    public int getHorizontalScrollPos() {
        return annotationPane.getHorizontalScrollBar().getValue();
    }

    public int getVerticalScrollPos() {
        return annotationPane.getVerticalScrollBar().getValue();
    }

    /**
     * Invokes {@link AbstractGeoPlot#scrollToOrigin(ViewTarget)}.
     *
     * @param originTarget requested upper-left visible ViewTarget
     */
    public void scrollToOrigin(ViewTarget originTarget) {
        geoPlot.scrollToOrigin(originTarget);
    }

    public TargetInfoPanel getTargetInfoPanel() {
        return targetInfoPanel;
    }

    public void setActionMode(ACTION_MODE actionMode) {
        geoPlot.setActionMode(actionMode);
    }

    public void setAnnotationPane(AnnotationPane annotationPane) {
        this.annotationPane = annotationPane;
        add(annotationPane, BorderLayout.CENTER);
        geoPlot.setScrollPane(annotationPane);
        geoPlot.updateAnnotationSizes(getPreferredSize());
    }

    public void setCursorTarget(ViewTarget target) {
        geoPlot.setCursorTarget(target);
    }

    void setGeoPlot(AbstractGeoPlot geoPlot) {
        this.geoPlot = geoPlot;
    }

    public void zoomIn() {
        geoPlot.zoomIn();
        annotationPane.repaintAnnotationPanels();
        revalidate();
    }

    public void zoomOut() {
        geoPlot.zoomOut();
        annotationPane.repaintAnnotationPanels();
        revalidate();
    }

    public void zoomRubberBand() {
        geoPlot.zoomRubberband();
        annotationPane.repaintAnnotationPanels();
        revalidate();
    }

    public void zoomAll() {
        geoPlot.zoomAll();
        annotationPane.repaintAnnotationPanels();
        revalidate();
    }

    public void updateWindowProperties(WindowProperties newWindowProperties) {
        winProps = newWindowProperties;
        geoPlot.updateWindowProperties(newWindowProperties);
    }

    public void updateHorizonHighlightSettings(HorizonHighlightSettings horizonHighlightSettings) {
        geoPlot.updateHorizonHighlightSettings(horizonHighlightSettings);
    }

    public void updateCursorSettings(CursorSettings cursorSettings) {
        this.cursorSettings = cursorSettings;
        geoPlot.updateCursorSettings(cursorSettings);
    }

    AbstractGeoPlot getGeoPlot() {
        return geoPlot;
    }

    public List<GeoPlotEventListener> getGeoPlotEventListeners() {
        return geoPlot.getGeoPlotEventListeners();
    }
    /**
     * Uses the JPanel implementation to reposition the JScrollPane associated
     * with this ViewerWindow so that the requested depth is visible.  If possible given
     * the scale and dimensions of the GeoPlot, this point will be positioned at the
     * topmost row of pixels, but this is not guaranteed.
     *
     * @param dataCoord time or depth value to be made visible
     */
    public void scrollToVisiblePoint(double dataCoord) throws IllegalArgumentException {
        Layer annotatedLayer = windowModel.getAnnotatedLayer();
        if (annotatedLayer == null) {
            logger.info("Unable to scroll to: " + dataCoord + "; GeoPlot has no annotated layer.");
        } else if (!annotatedLayer.isValidTimeOrDepth(dataCoord)) {
            logger.info("Unable to scroll to: " + dataCoord + "; Out of range for annotated layer: " + annotatedLayer.getLayerName());
        } else {
            geoPlot.scrollToVisiblePoint(dataCoord);
        }
    }

    public void scrollToVisiblePoint(int x, int y) {
        geoPlot.scrollToVisiblePoint(x, y);
    }
}