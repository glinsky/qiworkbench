/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

/**
 * MouseMotionListener implementation which invokes GeoPanel.getTargetProps()
 * and then GeoPanel.setTargetProps().
 */
public class CaptionListener implements MouseMotionListener {
    
    private GeoPanel geoPanel;
    
    /**
     * 
     * Constructs a CaptionListener associted with the specified GeoPanel.
     *
     * @param geoPanel The associated GeoPanel.
     */
    public CaptionListener(GeoPanel geoPanel) {
        this.geoPanel = geoPanel;
    }
    
    /**
     * Invoked when a mouse button is pressed on a component and then dragged. 
     * MOUSE_DRAGGED events will continue to be delivered to the component where 
     * the drag originated until the mouse button is released (regardless of whether 
     * the mouse position is within the bounds of the component).
     * <p />
     * Due to platform-dependent Drag&Drop implementations, MOUSE_DRAGGED events 
     * may not be delivered during a native Drag&Drop operation. 
     * 
     * @param me
     */
    public void mouseDragged(MouseEvent me) {
        processMouseMovement(me.getX(), me.getY());
    }

    /**
     * Invoked when the mouse cursor has been moved onto a component but no buttons have been pushed.
     * 
     * @param me
     */
    public void mouseMoved(MouseEvent me) {
        processMouseMovement(me.getX(), me.getY());
    }

    private void processMouseMovement(int x, int y) {    
        //Just ask for the current target; the logic which calculates the current target
        //based on annotataed layer and pointer coordinates is encapsulated within GeoPanel
        //ViewTargetProperties targetProps = geoPanel.getTargetProps(geoPanel.getAnnotatedLayer(),x,y);
        geoPanel.getTargetInfoPanel().setTargetInfo(geoPanel.getCursorTarget());
    }
}