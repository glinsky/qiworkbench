/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import com.bhpb.geographics.model.CursorSettings;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.ViewTarget;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.ui.AbstractGeoPlot.ACTION_MODE;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * Paints cursors in various styles according to CursorSettings.  Also provides methods
 * for changing the mouse cursor of the associated GeoPanel.
 * 
 */
public class CursorPainter {

    private enum CURSOR_TYPE {

        NORMAL, WAIT
    }

    private AbstractGeoPlot geoPlot;
    private CursorSettings cursorSettings;
    private ViewTarget cursorTarget = ViewTarget.NO_TARGET;
    private WindowModel windowModel;

    public CursorPainter(CursorSettings cursorSettings, AbstractGeoPlot geoPlot,
            WindowModel windowModel) {
        this.cursorSettings = cursorSettings;
        this.geoPlot = geoPlot;
        this.windowModel = windowModel;
    }

    /**
     * Woody Folsom :
     * Technique for setting invisible cursor is from :
     *
     * http://sdnshare.sun.com/view.jsp?id=1101
     */
    private Cursor createInvisibleCursor() {
        return geoPlot.getToolkit().createCustomCursor(
                new BufferedImage(3, 3, BufferedImage.TYPE_INT_ARGB),
                new Point(0, 0),
                "null");
    }

    /**
     * Gets the current cursor target
     *
     * @return this CursorPainter's current CursorTarget
     */
    public ViewTarget getCursorTarget() {
        return cursorTarget;
    }

    /**
     * Paints the cursor according to the current cursor settings, visible rectangle
     * and ongoing action, if any.
     *
     * @param g Graphics2D context into which cursor will be painted
     * @param annotatedLayer current annotated layer, needed to get the Point2D
     * for the current CursorTarget
     * @param actionMode if ZOOM_REGION (and isZooming parameter is true), a transparent red rectangle depicting the zoom region
     * will be shown, otherwise, only the cursor is drawn
     * @param isZooming if false, only the cursor will be drawn regardless of the actionMode parameter
     * @param zoomStart Point2D at which ongoing zoom operation, if any, began
     * @param visRect if the cursor is CROSSHAIR, it will be rendered to extend to the edges of this Rectangle
     */
    public void paintCursor(Graphics2D g, Layer annotatedLayer, ACTION_MODE actionMode,
            boolean isZooming, Point2D zoomStart, Rectangle visRect) {
        switch (cursorSettings.getCursorStyle()) {
            case NO_CURSOR:
                return; //do not paint the Cursor, it has been disabled
            default:
            //do nothing yet, continue painting the cursor
        }

        Color col = cursorSettings.getColor();

        if (cursorTarget != ViewTarget.NO_TARGET && annotatedLayer != null) {
            Point2D cursorPos = cursorTarget.getPoint2D(annotatedLayer);

            if (actionMode == ACTION_MODE.ZOOM_REGION && isZooming) { // draw selection rectangle
                int x = (int) zoomStart.getX();
                int y = (int) zoomStart.getY();
                int width = (int) (cursorPos.getX() - zoomStart.getX());
                int height = (int) (cursorPos.getY() - zoomStart.getY());

                //same as cursor color but 50% alpha
                g.setColor(new Color(col.getRed(), col.getGreen(), col.getBlue(), 128));
                g.fillRect(x, y, width, height);

                g.setStroke(new BasicStroke(3));
                g.setColor(Color.CYAN);
                g.drawRect(x, y, width, height);
            } else { //draw cusor
                int minx, maxx, miny, maxy;
                Stroke cursorStroke;

                int cursorX = (int) cursorPos.getX();
                int cursorY = (int) cursorPos.getY();

                switch (cursorSettings.getCursorStyle()) {
                    case CROSSHAIR:
                        minx = (int) visRect.getMinX();
                        maxx = (int) visRect.getMaxX();
                        miny = (int) visRect.getMinY();
                        maxy = (int) visRect.getMaxY();
                        cursorStroke = new BasicStroke(1);

                        break;
                    case LG_CROSS:
                        minx = cursorX - 10;
                        maxx = cursorX + 10;
                        miny = cursorY - 10;
                        maxy = cursorY + 10;
                        cursorStroke = new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER);

                        break;
                    case SM_CROSS:
                        minx = cursorX - 5;
                        maxx = cursorX + 5;
                        miny = cursorY - 5;
                        maxy = cursorY + 5;
                        cursorStroke = new BasicStroke(1);

                        break;
                    case NO_CURSOR:
                    default:
                        //cursor style is NO_CURSOR or unknown, do nothing
                        return;
                }

                g.setColor(col);
                g.setStroke(cursorStroke);
                g.drawLine(minx, cursorY, maxx, cursorY);
                g.drawLine(cursorX, miny, cursorX, maxy);
            }
        }
    }

    /**
     * Updates the internal state of this CursorPainter to use the indicated cursorSettings.
     *
     * @param cursorSettings new CursorSettings to be used by this CursorPainter
     */
    public void setCursorSettings(CursorSettings cursorSettings) {
        this.cursorSettings = cursorSettings;
    }

    /**
     * The ViewTarget at which the cursor will be painted is updated and the GeoPlot is redrawn.
     * If the mouseIsDragging parameter is 'true' and the actionMode is ZOOM_REGION, the active layer's
     * handleMouseDrag() method is invoked and the active layer is also redrawn.  If the mouseIsDragging parameter
     * is true but there is no active (selected) layer, this method has no effect beyond showing a warning MessageBox
     * prompting the user to select a layer prior to dragging.
     *
     * @param target ViewTarget specifying the cursor point relative to the annotated layer
     * @param mouseIsDragging may modify the cursor appearance depending on the actionMode and active layer
     * @param actionMode may modify the cursor appearance depending on the active layer and value of mouseIsDragging
     */
    public void setCursorTarget(ViewTarget target, boolean mouseIsDragging, ACTION_MODE actionMode) {
        ViewTarget oldCursorTarget = cursorTarget;
        cursorTarget = target;

        if (mouseIsDragging) {
            Layer currentActiveLayer = windowModel.getActiveLayer();

            if (currentActiveLayer == null) {
                //Do not update the cursor target if a messagebox is shown -
                //this prevents noticeable unexpected cursor 'jump'when the user closes the MessageBox
                cursorTarget = oldCursorTarget;
                JOptionPane.showMessageDialog(geoPlot, "Please select a layer prior to attempting a mouse-drag operation.",
                        "No Active Layer", JOptionPane.WARNING_MESSAGE);
                return;
            } else {

                if (actionMode != ACTION_MODE.ZOOM_REGION && currentActiveLayer.isHorizonLayer() && currentActiveLayer.getDatasetProperties().getMetadata().getGeoDataOrder() == GeoDataOrder.CROSS_SECTION_ORDER) {
                    currentActiveLayer.handleMouseDrag(cursorTarget);
                    geoPlot.redraw(currentActiveLayer, true, false);
                }
            }
        }

        //repaint() simply forces the cursor itself to be redrawn if no dragging is happening
        SwingUtilities.invokeLater(
                new Runnable() {

                    public void run() {
                        geoPlot.repaint();
                    }
                });
    }

    /**
     * Sets whether the mouse cursor (pointer) is enabled for this plot.  To
     * disable or change the _tracking_ cursor, invoke the appropriate
     * CursorSetings method.
     *
     * @param enabled show the mouse cursor in this GeoPlot
     */
    public void setMouseCursorEnabled(boolean enabled) {
        if (enabled) {
            geoPlot.setCursor(Cursor.getDefaultCursor());
        } else {
            geoPlot.setCursor(createInvisibleCursor());
        }
    }

    /**
     * Sets the mouse pointer to the OS-dependent DefaultCursor.
     */
    public void setNormalCursor() {
        if (SwingUtilities.isEventDispatchThread()) {
            setMouseCursorEnabled(cursorSettings.isShowPointer());
        } else {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    setMouseCursorEnabled(cursorSettings.isShowPointer());
                }
            });
        }
    }

    /**
     * Sets the mouse pointer to the OS-dependent Cursor.WAIT_CURSOR.
     */
    public void setWaitCursor() {
        if (SwingUtilities.isEventDispatchThread()) {
            geoPlot.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        } else {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    geoPlot.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                }
            });
        }
    }
}