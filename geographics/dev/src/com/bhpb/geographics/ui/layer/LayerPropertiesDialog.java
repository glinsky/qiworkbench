/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui.layer;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.controller.AbstractEditorPanelController.SELECTION;
import com.bhpb.geographics.controller.DatasetPropertiesController;
import com.bhpb.geographics.controller.LayerDisplayParametersController;
import com.bhpb.geographics.controller.LayerProperties;
import com.bhpb.geographics.controller.layer.subset.LocalSubsetPropertiesController;
import com.bhpb.geographics.controller.layer.sync.SyncPropertiesController;
import com.bhpb.geographics.model.layer.display.LayerDisplayParameters;
import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geographics.model.layer.display.LocalSubsetProperties;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

public class LayerPropertiesDialog extends EditorDialog {

    private static final int PARENT_FRAME_OFFSET = 50;
    private DatasetProperties dsPropsModel;
    private LayerDisplayProperties ldProps;
    private LayerDisplayParametersController displayParamsController;

    public LayerPropertiesDialog(
            JFrame frame,
            DatasetPropertiesController dsPropsController,
            JTabbedPane tabbedPane,
            DatasetProperties dsPropsModel) {
        super(frame, tabbedPane, dsPropsController);
        this.dsPropsModel = dsPropsModel;
        this.ldProps = LayerDisplayProperties.NULL;
        this.setName("layerPropertiesDialog");
        setLocation(frame.getX() + PARENT_FRAME_OFFSET, frame.getY() + PARENT_FRAME_OFFSET);
    }

    public LayerPropertiesDialog(
            JFrame frame,
            DatasetPropertiesController dsPropsController,
            LocalSubsetPropertiesController subsetPropsController,
            LayerDisplayParametersController displayParamsController,
            SyncPropertiesController syncController,
            JTabbedPane tabbedPane,
            DatasetProperties dsPropsModel,
            LayerDisplayProperties ldProps) {
        super(frame, tabbedPane, dsPropsController,
                subsetPropsController, displayParamsController, syncController);
        this.dsPropsModel = dsPropsModel;
        this.ldProps = ldProps;
        this.setName("layerPropertiesDialog");
        setLocation(frame.getX() + PARENT_FRAME_OFFSET, frame.getY() + PARENT_FRAME_OFFSET);

        this.displayParamsController = displayParamsController;
    }

    public DatasetProperties getDatasetProperties() {
        return dsPropsModel;
    }

    public LocalSubsetProperties getSubsetProps() {
        return ldProps.getLocalSubsetProperties();
    }

    public LayerDisplayParameters displayParams() {
        return ldProps.getLayerDisplayParameters();
    }

    public LayerSyncProperties syncProps() {
        return ldProps.getLayerSyncProperties();
    }

    public LayerProperties getTransactionResult() {

        List<SELECTION> selections = new ArrayList<SELECTION>();

        for (AbstractEditorPanelController controller : controllers) {
            controller.waitForSelection();
            selections.add(controller.getSelection());
            //reset the controllers afte getting the current selections in the case of APPLY
            controller.setSelection(SELECTION.NONE);
        }

        if (selections.size() == 0) {
            throw new RuntimeException("Cannot getTransactionResult : no controllers in Collection.");
        } else {
            Iterator<SELECTION> iter = selections.iterator();
            SELECTION selection = iter.next();
            while (iter.hasNext()) {
                if (selection != iter.next()) {
                    throw new RuntimeException("Cannot complete FileChooserDirector.getTransactionResult: controller selections are in conflicting states");
                }
            }

            switch (selection) {
                case OK:
                    //If LayerDisplayProperties are NULL, then they are immutable and only the dataset properties have been edited
                    if (ldProps.equals(LayerDisplayProperties.NULL)) {
                        return LayerProperties.createUserAcceptedResult(dsPropsModel, ldProps);
                    } else {
                        //otherwise, return the results containing the modified LayerDisplayParameters model
                        return LayerProperties.createUserAcceptedResult(
                                dsPropsModel,
                                new LayerDisplayProperties(
                                ldProps.getLocalSubsetProperties(),
                                (LayerDisplayParameters) displayParamsController.getModel(),
                                ldProps.getLayerSyncProperties()));
                    }
                case APPLY:
                    //If LayerDisplayProperties are NULL, then they are immutable and only the dataset properties have been edited
                    if (ldProps.equals(LayerDisplayProperties.NULL)) {
                        return LayerProperties.createUserAppliedResult(dsPropsModel, ldProps);
                    } else {
                        //otherwise, return the results containing the modified LayerDisplayParameters model
                        return LayerProperties.createUserAppliedResult(
                                dsPropsModel,
                                new LayerDisplayProperties(
                                ldProps.getLocalSubsetProperties(),
                                (LayerDisplayParameters) displayParamsController.getModel(),
                                ldProps.getLayerSyncProperties()));
                    }
                case CANCEL:
                    return LayerProperties.createUserCancelledResult();
                default:
                    return LayerProperties.createErrorResult("No user selection was available after waitForSelection.  The controller was interrupted while waiting or an error occurred.");
            }
        }
    }
}