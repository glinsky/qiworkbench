/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.ui.layer;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.controller.ApplyActionListener;
import com.bhpb.geographics.controller.CancelActionListener;
import com.bhpb.geographics.controller.EditorDialogListener;
import com.bhpb.geographics.controller.OkActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * Transactional Editor Dialog featuring OK and CANCEL buttons.  Accepts content
 * in the form of a JComponent and an arbitrary number of TransactionalEditor
 * controllers.  The Ok and CancelActionListeners are used to broadcast the user's
 * selection to the controllers associatd with this content.
 * 
 * This may cause one or more of the TransactionalEditors to execute a rollback
 * operation when the user clicks Ok or Cancel.
 */

public class EditorDialog extends JDialog{
    protected final List<AbstractEditorPanelController> controllers = new ArrayList<AbstractEditorPanelController>();
    
    private JButton applyBtn;

    public EditorDialog(JFrame frame, JComponent content, AbstractEditorPanelController ... editors) {
        super(frame);
        
        for (AbstractEditorPanelController editor : editors) {
            controllers.add(editor);
        }
        
        JPanel contentPanel = new JPanel();
        contentPanel.setName("contentPanel");
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
        
        contentPanel.add(content);
        contentPanel.add(Box.createVerticalGlue());
        contentPanel.add(createButtonPanel());
        
        super.add(contentPanel);

        addWindowListener(new EditorDialogListener(controllers));
        startPanelWorkers(editors);

        super.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    
    private JPanel createButtonPanel() {
        JPanel btnPanel = new JPanel();
        btnPanel.setName("btnPanel");
        btnPanel.setLayout(new BoxLayout(btnPanel, BoxLayout.X_AXIS));
        btnPanel.add(Box.createHorizontalGlue());
        
        JButton okBtn = new JButton("Ok");
        okBtn.addActionListener(new OkActionListener(controllers, this));
        okBtn.setName("Ok");
        btnPanel.add(okBtn);
        
        applyBtn = new JButton("Apply");
        applyBtn.addActionListener(new ApplyActionListener(controllers, this));
        applyBtn.setName("Apply");
        btnPanel.add(applyBtn);
        
        JButton cancelBtn = new JButton("Cancel");
        cancelBtn.addActionListener(new CancelActionListener(controllers, this));
        cancelBtn.setName("Cancel");
        btnPanel.add(cancelBtn);
        
        JButton helpBtn = new JButton("Help");
        helpBtn.setName("Help");
        btnPanel.add(helpBtn);
        
        btnPanel.add(Box.createHorizontalGlue());
        return btnPanel;
    }
    
    /**
     * Starts a worker thread for each controller.  Some may update once and terminate,
     * while others may continue to drive animation or other functionality.
     * 
     * @param editors
     */
    private void startPanelWorkers(AbstractEditorPanelController ... controllers) {
        for (AbstractEditorPanelController controller : controllers) {
            new Thread(controller).start();
        }
    }

    public void setApplyEnabled(final boolean enabled) {
        if (SwingUtilities.isEventDispatchThread()) {
            applyBtn.setEnabled(enabled);
        } else {
            SwingUtilities.invokeLater(new Runnable(){
                public void run() {
                    applyBtn.setEnabled(enabled);
                }
            });
        }
    }
}