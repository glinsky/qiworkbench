/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui.layer.display;

import com.bhpb.geographics.ui.layer.shared.AutoGainControlPanel;
import com.bhpb.geographics.ui.layer.shared.CullingPanel;
import com.bhpb.geographics.ui.layer.shared.InterpolationSettingsPanel;
import com.bhpb.geographics.ui.layer.shared.LayerInfoPanel;
import com.bhpb.geographics.ui.layer.shared.NormalizationPanel;
import com.bhpb.geographics.ui.layer.shared.RasterizingTypePanel;
import com.bhpb.geographics.ui.layer.shared.XsecColorMapPanel;
import java.awt.GridLayout;
import javax.swing.JPanel;

public class SeismicXsecDisplayParametersPanel extends JPanel {
    private static final long serialVersionUID = 9189362340620127L;
    
    private AutoGainControlPanel agcPanel = new AutoGainControlPanel();
    private CullingPanel cullingPanel = new CullingPanel();
    private LayerInfoPanel layerInfoPanel = new LayerInfoPanel();
    private NormalizationPanel normalizationPanel = new NormalizationPanel();
    private RasterizingTypePanel rasterizingTypePanel = new RasterizingTypePanel();
    private XsecColorMapPanel xSecColorMapPanel = new XsecColorMapPanel();
    private InterpolationSettingsPanel interpolationSettingsPanel = new InterpolationSettingsPanel();
    
    SeismicXsecDisplayParametersPanel() {
        setLayout(new GridLayout(1, 3));

        JPanel firstColumn = LayerDisplayParametersPanelFactory.createVerticalBox(
                layerInfoPanel);
        JPanel secondColumn = LayerDisplayParametersPanelFactory.createVerticalBox(
                normalizationPanel,
                interpolationSettingsPanel,
                agcPanel);

        JPanel thirdColumn = LayerDisplayParametersPanelFactory.createVerticalBox(
                rasterizingTypePanel,
                cullingPanel,
                xSecColorMapPanel);

        add(firstColumn);
        add(secondColumn);
        add(thirdColumn);
    }

    public AutoGainControlPanel getAutoGainControlPanel() {
        return agcPanel;
    }

    public CullingPanel getCullingPanel() {
        return cullingPanel;
    }
    
    public LayerInfoPanel getLayerInfoPanel() {
        return layerInfoPanel;
    }

    public NormalizationPanel getNormalizationPanel() {
        return normalizationPanel;
    }
    
    public RasterizingTypePanel getRasterizingTypePanel() {
        return rasterizingTypePanel;
    }
    
    public XsecColorMapPanel getXsecColorMapPanel() {
        return xSecColorMapPanel;
    }

    public InterpolationSettingsPanel getInterpolationSettingsPanel() {
        return interpolationSettingsPanel;
    }
}
