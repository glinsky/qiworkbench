/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui.layer.sync;

import com.bhpb.geographics.model.layer.sync.LayerSyncProperties;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties.SYNC_FLAG;
import java.awt.GridLayout;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class LayerSyncPropertiesPanel extends JPanel {
    private static final long serialVersionUID = -8813044971325913352L;
    private static final String LISTEN_TO_CAPTION = "Listen to ";
    
    private ChangeListener checkBoxListener = new CheckBoxListener();
    private JCheckBox broadcastCheckBox;
    private JPanel broadcastPanel;
    private JPanel checkBoxPanel;
    private LayerSyncProperties syncProps;
    
    LayerSyncPropertiesPanel(int nRows, int nCols) {
        initComponents(nRows, nCols);
    }
    
    /**
     * Lays out the LayerSyncPropertiesPanel, which consists primary of two JPanels
     * in a vertical BoxLayout: an upper JPanel containing a checkbox labeled 'Broadcast'
     * and a lower JPanel containing a grid (generally two columns) of checkboxes displaying
     * property names as governed by this LayerSyncPropertiesPanel's SyncPropertie.
     */
    private void initComponents(int nRows, int nCols) {
        this.setName("LayerSyncPropertiesPanel");
        
        broadcastPanel = new JPanel();
        broadcastPanel.setLayout(new BoxLayout(broadcastPanel,BoxLayout.Y_AXIS));
        broadcastPanel.add(Box.createVerticalStrut(20));
        
        broadcastCheckBox = new JCheckBox(LayerSyncProperties.toString(SYNC_FLAG.BROADCAST));
        broadcastPanel.add(broadcastCheckBox);
        
        broadcastPanel.add(Box.createVerticalStrut(20));
        
        checkBoxPanel = new JPanel();
        checkBoxPanel.setName("CheckBoxPanel");
        checkBoxPanel.setLayout(new GridLayout(nRows,nCols));
        
        JPanel centralPanel = new JPanel();
        centralPanel.setName("CentralPanel");
        centralPanel.setLayout(new BoxLayout(centralPanel, BoxLayout.Y_AXIS));
        centralPanel.add(Box.createVerticalGlue());
        centralPanel.add(broadcastPanel);
        centralPanel.add(Box.createVerticalGlue());
        centralPanel.add(checkBoxPanel);
        centralPanel.add(Box.createVerticalGlue());
        
        setLayout(new BoxLayout(this,BoxLayout.X_AXIS));
        add(Box.createHorizontalGlue());
        add(centralPanel);
        add(Box.createHorizontalGlue());
    }
    
    /**
     * Adds an nRowsxnCols grid of checkboxes labeled in the order of SyncProperties.getProperties().
     */
    private void decorateCheckBoxPanel(LayerSyncProperties syncProps) {
        if (syncProps.isEnabled(SYNC_FLAG.BROADCAST)) {
            broadcastCheckBox.setSelected(true);
        }
        
        for (SYNC_FLAG flag : syncProps.getProperties()) {
            if (!syncProps.isVisible(flag)) {
                // property is accessible in the model but not visible in the view, use blank label
                checkBoxPanel.add(new JLabel());
            } else {
                JCheckBox flagCheckBox = new JCheckBox(LISTEN_TO_CAPTION + LayerSyncProperties.toString(flag));
                flagCheckBox.setName(flag.toString());
            
                if (!syncProps.isMutable(flag)) {
                    flagCheckBox.setEnabled(false);
                }
                if (syncProps.isEnabled(flag)) {
                    flagCheckBox.setSelected(true);
                }
                flagCheckBox.addChangeListener(checkBoxListener);
                checkBoxPanel.add(flagCheckBox);
            }
        }
    }
    
    private class CheckBoxListener implements ChangeListener {
        public void stateChanged(ChangeEvent ce) {
            JCheckBox checkBox = (JCheckBox) ce.getSource();
            SYNC_FLAG flag = LayerSyncProperties.valueOf(checkBox.getText().substring(LISTEN_TO_CAPTION.length()));
            syncProps.setEnabled(flag, checkBox.isSelected());
        }
    }
    
    public void setModel(LayerSyncProperties syncProps) {
        this.syncProps = syncProps;
        decorateCheckBoxPanel(syncProps);
    }
}