/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.ui.layer.display;

import com.bhpb.geographics.ui.layer.shared.ColorAttributesPanel;
import com.bhpb.geographics.ui.layer.shared.DisplayAttributesPanel;
import com.bhpb.geographics.ui.layer.shared.LayerInfoPanel;
import java.awt.GridLayout;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;

public class MapDisplayParametersPanel extends JPanel {
    private static final long serialVersionUID = -1209482187367679586L;
    
    private ColorAttributesPanel colorAttributesPanel = new ColorAttributesPanel();
    private DisplayAttributesPanel displayAttributesPanel = new DisplayAttributesPanel();
    private LayerInfoPanel layerInfoPanel = new LayerInfoPanel();

    public MapDisplayParametersPanel() {
        setLayout(new GridLayout(1, 2));

        JPanel firstColumn = LayerDisplayParametersPanelFactory.createVerticalBox(
                layerInfoPanel);

        JPanel secondColumn = LayerDisplayParametersPanelFactory.createVerticalBox(
                leftJustifyInPanel(displayAttributesPanel),
                leftJustifyInPanel(colorAttributesPanel));

        add(firstColumn);
        add(secondColumn);
    }
    
    public ColorAttributesPanel getColorAttributesPanel() {
        return colorAttributesPanel;
    }
    
    public DisplayAttributesPanel getDisplayAttributesPanel() {
        return displayAttributesPanel;
    }
    
    public LayerInfoPanel getLayerInfoPanel() {
        return layerInfoPanel;
    }
    
    private JPanel leftJustifyInPanel(JComponent component) {
        JPanel wrapperPanel = new JPanel();
        wrapperPanel.setLayout(new BoxLayout(wrapperPanel,BoxLayout.X_AXIS));
        wrapperPanel.add(component);
        wrapperPanel.add(Box.createHorizontalGlue());
        return wrapperPanel;
    }
}
