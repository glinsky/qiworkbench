/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui.layer.shared;

import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters.LINE_STYLE;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

public class LineStyleComboBox extends JComboBox {
    private static final Logger logger =
            Logger.getLogger(LineStyleComboBox.class.getName());
    private static final Dimension DEFAULT_COMBOBOX_SIZE =
            new Dimension(90,20);
    private static final LINE_STYLE[] LINE_STYLES = LINE_STYLE.values();
    
    /**
     * Constructor.
     */
    public LineStyleComboBox() {

        super(LINE_STYLES);
        setToolTipText("Choose Line Style");

        super.setRenderer(new LineStyleRenderer());
        setRenderer(renderer);
        
        setPreferredSize(DEFAULT_COMBOBOX_SIZE);
        setMaximumSize(DEFAULT_COMBOBOX_SIZE);
        setMinimumSize(DEFAULT_COMBOBOX_SIZE);
    }

    /**
     * Sets the new line style for this chooser.
     */
    public void setLineStyle(LINE_STYLE lineStyle) {
        setSelectedItem(lineStyle);
    }

    /**
     * Get the current selected line style .
     */
    public int getLineStyle() {
        return ((Integer) getSelectedItem()).intValue();
    }

    static class LineStyleRenderer extends JPanel implements ListCellRenderer {

        private LINE_STYLE style;

        /**
         * Constructor.
         *
         */
        public LineStyleRenderer() {
            setLayout(null);
            setPreferredSize(DEFAULT_COMBOBOX_SIZE);
            setMaximumSize(DEFAULT_COMBOBOX_SIZE);
            setMinimumSize(DEFAULT_COMBOBOX_SIZE);
        }

        /**
         * interface ListCellRenderer.
         */
        public Component getListCellRendererComponent(JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {
            if (isSelected) {
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
            } else {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }

            style = (LINE_STYLE) value;
            logger.info("ListCellRendererComponent for line style: " + value.toString());
            return this;
        }

        //overwrite the paintComponent method
        @Override
        protected void paintComponent(Graphics g) {
            Dimension d = getSize();

            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);

            g2.setColor(getBackground());
            g2.fillRect(0, 0, d.width, d.height);

            g2.setColor(Color.black);


            float dash[] = null;
            switch (style) {
                case SOLID:
                    g2.setStroke(new BasicStroke(3, 1, 1, 1));
                    g2.drawLine(5, d.height / 2, d.width - 10, d.height / 2);
                    break;
                case EMPTY:
                    g2.setStroke(new BasicStroke(0.3f));
                    g2.drawRect(5, d.height / 2 - 2, d.width - 10, 2);
                    break;
                case DOT:
                    dash = new float[]{1, 5};
                    g2.setStroke(new BasicStroke(3, 1, 1, 1, dash, 0));
                    g2.drawLine(5, d.height / 2, d.width - 10, d.height / 2);
                    break;
                case DASH:
                    dash = new float[]{5, 5};
                    g2.setStroke(new BasicStroke(3, 1, 1, 1, dash, 0));
                    g2.drawLine(5, d.height / 2, d.width - 10, d.height / 2);
                    break;
                case DASH_DOT:
                    dash = new float[]{5, 5, 1, 5};
                    g2.setStroke(new BasicStroke(3, 1, 1, 1, dash, 0));
                    g2.drawLine(5, d.height / 2, d.width - 10, d.height / 2);
                    break;
                case DASH_DOT_DOT:
                    dash = new float[]{5, 5, 1, 5, 1, 5};
                    g2.setStroke(new BasicStroke(3, 1, 1, 1, dash, 0));
                    g2.drawLine(5, d.height / 2, d.width - 10, d.height / 2);
                    break;
            }
        }
    }
}