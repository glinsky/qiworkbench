/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui.layer.shared;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

public class LineWidthComboBox extends JComboBox {
    private static final Dimension DEFAULT_COMBOBOX_SIZE = 
            new Dimension(80,20);
    
    private static final Integer[] LINE_SIZES = {
        1,
        2, 3, 4, 5,
        6, 7, 8, 9,
        10, 11, 12
    };

    /**
     *  Constructor.
     */
    public LineWidthComboBox() {
        super(LINE_SIZES);
        setToolTipText("Choose Line Width");

        super.setRenderer(new LineWidthRenderer());
        
        setMinimumSize(DEFAULT_COMBOBOX_SIZE);
        setMaximumSize(DEFAULT_COMBOBOX_SIZE);
        setPreferredSize(DEFAULT_COMBOBOX_SIZE);
    }

    /**
     * Set line width.
     */
    public void setLineWidth(int lineWidth) {
        if (lineWidth > 0 && lineWidth <= 12) {
            setSelectedItem(Integer.valueOf(lineWidth));
        }
    }

    /**
     * Get selected line width.
     */
    public int getLineWidth() {
        return ((Integer) getSelectedItem()).intValue();
    }

    static class LineWidthRenderer extends JPanel implements ListCellRenderer {

        private Integer value;
        private Font font = new Font("Arial", Font.PLAIN, 10);

        /**
         * Constructor.
         */
        public LineWidthRenderer() {
            setLayout(null);
            setPreferredSize(DEFAULT_COMBOBOX_SIZE);
            setMaximumSize(DEFAULT_COMBOBOX_SIZE);
            setMinimumSize(DEFAULT_COMBOBOX_SIZE);
        }

        /**
         * Interface ListCellRenderer
         */
        public Component getListCellRendererComponent(JList list,
                Object value,
                int index,
                boolean isSelected,
                boolean cellHasFocus) {

            if (isSelected || cellHasFocus) {
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
            } else {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }
            this.value = (Integer) value;

            return this;
        }

        /**
         * Overrids the paintComponent method of JPanel.
         */
        @Override
        protected void paintComponent(Graphics g) {

            Dimension d = getSize();

            Graphics2D g2d = (Graphics2D) g;
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            g2d.setColor(getBackground());
            g2d.fillRect(0, 0, d.width, d.height);

            g2d.setColor(Color.black);
            g2d.setFont(font);
            g2d.drawString(value.toString(), 5, d.height / 2 + 2);
            g2d.setStroke(new BasicStroke(value.intValue(), BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
            g2d.drawLine(d.width / 2 - 10, d.height / 2, d.width - 10, d.height / 2);

        }
    }
}