/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui.layer;

import com.bhpb.geographics.ui.layer.dataset.SeismicDatasetPropertiesPanel;
import com.bhpb.geographics.ui.layer.dataset.ModelDatasetPropertiesPanel;
import com.bhpb.geographics.ui.layer.dataset.HorizonDatasetPropertiesPanel;
import com.bhpb.geographics.controller.DatasetPropertiesController;
import com.bhpb.geographics.controller.HorizonDatasetPropertiesController;
import com.bhpb.geographics.controller.LayerDisplayParametersController;
import com.bhpb.geographics.controller.SeismicDatasetPropertiesController;
import com.bhpb.geographics.controller.ModelDatasetPropertiesController;
import com.bhpb.geographics.controller.layer.HorizonXsecDisplayParametersController;
import com.bhpb.geographics.controller.layer.MapDisplayParametersController;
import com.bhpb.geographics.controller.layer.ModelXsecDisplayParametersController;
import com.bhpb.geographics.controller.layer.SeismicXsecDisplayParametersController;
import com.bhpb.geographics.controller.layer.subset.LocalSubsetPropertiesController;
import com.bhpb.geographics.controller.layer.sync.SyncPropertiesController;
import com.bhpb.geographics.model.HorizonLayer;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geographics.model.layer.display.LocalSubsetProperties;
import com.bhpb.geographics.model.ModelLayer;
import com.bhpb.geographics.model.SeismicLayer;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParametersFactory;
import com.bhpb.geographics.model.layer.display.LayerDisplayParameters;
import com.bhpb.geographics.model.layer.display.MapDisplayParameters;
import com.bhpb.geographics.model.layer.display.MapDisplayParametersFactory;
import com.bhpb.geographics.model.layer.display.ModelXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParameters;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties;
import com.bhpb.geographics.ui.layer.display.LayerDisplayParametersPanelFactory;
import com.bhpb.geographics.ui.layer.display.MapDisplayParametersPanel;
import com.bhpb.geographics.ui.layer.display.ModelXsecDisplayParametersPanel;
import com.bhpb.geographics.ui.layer.display.SeismicXsecDisplayParametersPanel;
import com.bhpb.geographics.ui.layer.display.HorizonXsecDisplayParametersPanel;
import com.bhpb.geographics.ui.layer.subset.LocalSubsetPropertiesPanel;
import com.bhpb.geographics.ui.layer.sync.LayerSyncPropertiesPanel;
import com.bhpb.geographics.ui.layer.sync.LayerSyncPropertiesPanelFactory;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import com.bhpb.geoio.filesystems.properties.ModelProperties;
import com.bhpb.geoio.filesystems.properties.SeismicProperties;
import com.bhpb.geoio.util.GeoIOAdapter;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class PropertiesEditorFactory extends JDialog {

    /**
     * Create a TransactionalEditorDialog in which the contentPanel is a JTabbedPane
     * displaying the 4 views and which uses the 1 controllers to handle the user
     * selections (Ok, Apply, Cancel) for the DatasetProperties tab.  The other 4 tables are disabled
     * when this constructor is used.
     */
    private static LayerPropertiesDialog createLayerPropertiesDialog(
            JFrame frame,
            DatasetProperties datasetProperties,
            JPanel dsPropsView,
            DatasetPropertiesController dsPropsController) {

        JTabbedPane tabbedPane = getTabbedPane(dsPropsView);

        LayerPropertiesDialog layerPropsDlg = getLayerPropertiesDialog(frame,
                datasetProperties,
                dsPropsController,
                tabbedPane);

        layerPropsDlg.setTitle("Open Dataset in New X-Section Window");
        
        return layerPropsDlg;
    }

    /**
     * Create a TransactionalEditorDialog in which the contentPanel is a JTabbedPane
     * displaying the 4 views and which uses the 4 controllers to handle the user
     * selections ("Ok" or "Cancel").
     */
    private static LayerPropertiesDialog createLayerPropertiesDialog(
            JFrame frame,
            DatasetProperties datasetProperties,
            LayerDisplayProperties ldProps,
            JPanel dsPropsView,
            LocalSubsetPropertiesPanel subsetPropsView,
            JPanel displayParamsView,
            LayerSyncPropertiesPanel syncPropsView,
            DatasetPropertiesController dsPropsController,
            LocalSubsetPropertiesController subsetPropsController,
            LayerDisplayParametersController displayParamsController,
            SyncPropertiesController syncController) {

        JTabbedPane tabbedPane = getTabbedPane(
                dsPropsView,
                subsetPropsView,
                displayParamsView,
                syncPropsView);

        LayerPropertiesDialog layerPropsDlg = getLayerPropertiesDialog(frame,
                datasetProperties,
                ldProps,
                dsPropsController,
                subsetPropsController,
                displayParamsController,
                syncController,
                tabbedPane);

        layerPropsDlg.setTitle("Edit Layer Properties");
        
        return layerPropsDlg;
    }

    private static JTabbedPane getTabbedPane(
            JPanel dsPropsView,
            LocalSubsetPropertiesPanel subsetPropsView,
            JPanel displayParamsView,
            LayerSyncPropertiesPanel syncPropsView) {
        JTabbedPane tabbedPane = new JTabbedPane();

        //now instead of adding the view directly to the datasetPropertiesDlg, it will be contained within a JTabbedPane
        tabbedPane.add("Dataset", dsPropsView);

        JComponent subsetPropsViewComponent;
        JComponent displayParamsViewComponent;
        JComponent syncPropsViewComponent;

        subsetPropsViewComponent = subsetPropsView;
        displayParamsViewComponent = displayParamsView;
        syncPropsViewComponent = syncPropsView;

        tabbedPane.add("Local Subset", subsetPropsViewComponent);
        tabbedPane.add("Display Parameters", displayParamsViewComponent);
        tabbedPane.add("Synchronization", syncPropsViewComponent);

        return tabbedPane;
    }

    private static JTabbedPane getTabbedPane(
            JPanel dsPropsView) {
        JTabbedPane tabbedPane = new JTabbedPane();

        //now instead of adding the view directly to the datasetPropertiesDlg, it will be contained within a JTabbedPane
        tabbedPane.add("Dataset", dsPropsView);

        JComponent subsetPropsViewComponent = new JPanel();
        JComponent displayParamsViewComponent = new JPanel();
        JComponent syncPropsViewComponent = new JPanel();

        tabbedPane.add("Local Subset", subsetPropsViewComponent);
        tabbedPane.add("Display Parameters", displayParamsViewComponent);
        tabbedPane.add("Synchronization", syncPropsViewComponent);

        tabbedPane.setEnabledAt(1, false);
        tabbedPane.setEnabledAt(2, false);
        tabbedPane.setEnabledAt(3, false);

        return tabbedPane;
    }

    private static LayerPropertiesDialog getLayerPropertiesDialog(JFrame frame,
            DatasetProperties datasetProperties,
            DatasetPropertiesController dsPropsController,
            JTabbedPane tabbedPane) {

        return new LayerPropertiesDialog(frame, dsPropsController,
                tabbedPane,
                datasetProperties);
    }

    private static LayerPropertiesDialog getLayerPropertiesDialog(JFrame frame,
            DatasetProperties datasetProperties,
            LayerDisplayProperties ldProps,
            DatasetPropertiesController dsPropsController,
            LocalSubsetPropertiesController subsetPropsController,
            LayerDisplayParametersController displayParamsController,
            SyncPropertiesController syncController,
            JTabbedPane tabbedPane) {

        return new LayerPropertiesDialog(frame, dsPropsController,
                subsetPropsController, displayParamsController, syncController, tabbedPane,
                datasetProperties, ldProps);
    }

    public static LayerPropertiesDialog createLayerPropertiesDialog(JFrame frame, Layer layer) {
        if (layer instanceof SeismicLayer) {
            return createSeismicPropertiesDialog(
                    frame,
                    new SeismicProperties((SeismicProperties) layer.getDatasetProperties()),
                    new LayerDisplayProperties(
                    layer.getLocalSubsetProperties(),
                    layer.getDisplayParameters(),
                    layer.getSyncProperties()));
        } else if (layer instanceof ModelLayer) {
            return createModelPropertiesDialog(
                    frame,
                    new ModelProperties((ModelProperties) layer.getDatasetProperties()),
                    new LayerDisplayProperties(
                    layer.getLocalSubsetProperties(),
                    layer.getDisplayParameters(),
                    layer.getSyncProperties()));
        } else if (layer instanceof HorizonLayer) {
            return createHorizonPropertiesDialog(
                    frame,
                    new HorizonProperties((HorizonProperties) layer.getDatasetProperties()),
                    new LayerDisplayProperties(
                    layer.getLocalSubsetProperties(),
                    layer.getDisplayParameters(),
                    layer.getSyncProperties()),
                    layer.getIOadapter(),
                    ((HorizonLayer) layer).getMutableHorizon());
        } else {
            throw new IllegalArgumentException("Layer is not an instanceof Seismic, Model or Horizon layer.");
        }
    }

    public static LayerPropertiesDialog createSeismicPropertiesDialog(JFrame frame, SeismicProperties datasetProperties,
            LayerDisplayProperties layerProps) {
        LayerPropertiesDialog layerPropertiesDlg;
        SeismicDatasetPropertiesPanel dsPropsView = new SeismicDatasetPropertiesPanel();
        SeismicDatasetPropertiesController dsPropsController = new SeismicDatasetPropertiesController(datasetProperties, dsPropsView);

        if (layerProps != LayerDisplayProperties.NULL) {

            LocalSubsetProperties subsetProps = layerProps.getLocalSubsetProperties();
            LayerDisplayParameters displayParams = layerProps.getLayerDisplayParameters();
            LayerSyncProperties syncProps = layerProps.getLayerSyncProperties();

            LocalSubsetPropertiesPanel subsetPropsView = new LocalSubsetPropertiesPanel();
            JPanel displayParamsView;

            GeoDataOrder order = datasetProperties.getMetadata().getGeoDataOrder();
            switch (order) {
                case CROSS_SECTION_ORDER:
                    displayParamsView = LayerDisplayParametersPanelFactory.createSeismicXsecPanel();
                    break;
                case MAP_VIEW_ORDER:
                    displayParamsView = LayerDisplayParametersPanelFactory.createSeismicMapPanel();
                    break;
                default:
                    throw new IllegalArgumentException("Unable to create displayParamsView for unknown GeoDataOrder: " + order);
            }

            //LayerSyncPropertiesPanel syncPropsView = new LayerSyncPropertiesPanel();
            LayerSyncPropertiesPanel syncPropsView;
            switch (order) {
                case CROSS_SECTION_ORDER:
                    syncPropsView = LayerSyncPropertiesPanelFactory.createSeismicXsecSyncPanel();
                    break;
                case MAP_VIEW_ORDER:
                    syncPropsView = LayerSyncPropertiesPanelFactory.createMapSyncPanel();
                    break;
                default:
                    throw new IllegalArgumentException("Unable to create displayParamsView for unknown GeoDataOrder: " + order);
            }

            LocalSubsetPropertiesController subsetPropsController = new LocalSubsetPropertiesController(subsetProps, subsetPropsView);

            LayerDisplayParametersController displayParamsController;
            switch (order) {
                case CROSS_SECTION_ORDER:
                    displayParamsController = new SeismicXsecDisplayParametersController((SeismicXsecDisplayParameters) displayParams,
                            (SeismicXsecDisplayParametersPanel) displayParamsView);
                    break;
                case MAP_VIEW_ORDER:
                    displayParamsController = new MapDisplayParametersController((MapDisplayParameters) displayParams,
                            (MapDisplayParametersPanel) displayParamsView);
                    break;
                default:
                    throw new IllegalArgumentException("Unable to create displayParamsView for unknown GeoDataOrder: " + order);
            }

            SyncPropertiesController syncController = new SyncPropertiesController(syncProps, syncPropsView);

            layerPropertiesDlg = PropertiesEditorFactory.createLayerPropertiesDialog(
                    frame,
                    datasetProperties, layerProps,
                    dsPropsView, subsetPropsView, displayParamsView, syncPropsView,
                    dsPropsController, subsetPropsController, displayParamsController,
                    syncController);

        } else {
            layerPropertiesDlg = PropertiesEditorFactory.createLayerPropertiesDialog(
                    frame,
                    datasetProperties,
                    dsPropsView,
                    dsPropsController);
        }
        return layerPropertiesDlg;
    }

    public static LayerPropertiesDialog createModelPropertiesDialog(JFrame frame, ModelProperties datasetProperties,
            LayerDisplayProperties layerProps) {
        ModelDatasetPropertiesPanel dsPropsView = new ModelDatasetPropertiesPanel();
        LayerPropertiesDialog layerPropertiesDlg;
        ModelDatasetPropertiesController dsPropsController = new ModelDatasetPropertiesController(datasetProperties, dsPropsView);

        if (layerProps != LayerDisplayProperties.NULL) {

            LocalSubsetProperties subsetProps = layerProps.getLocalSubsetProperties();
            LayerDisplayParameters displayParams = layerProps.getLayerDisplayParameters();
            LayerSyncProperties syncProps = layerProps.getLayerSyncProperties();

            LocalSubsetPropertiesPanel subsetPropsView = new LocalSubsetPropertiesPanel();

            //this is invalid but reflects the previous functionality - it should invoke createModelPropertiesDialog()
            JPanel displayParamsView;
            GeoDataOrder order = datasetProperties.getMetadata().getGeoDataOrder();
            switch (order) {
                case CROSS_SECTION_ORDER:
                    displayParamsView = LayerDisplayParametersPanelFactory.createModelXsecPanel();
                    break;
                case MAP_VIEW_ORDER:
                    displayParamsView = LayerDisplayParametersPanelFactory.createModelMapPanel();
                    break;
                default:
                    throw new IllegalArgumentException("Unable to create displayParamsView for unknown GeoDataOrder: " + order);
            }

            //LayerSyncPropertiesPanel syncPropsView = new LayerSyncPropertiesPanel();
            LayerSyncPropertiesPanel syncPropsView;
            switch (order) {
                case CROSS_SECTION_ORDER:
                    syncPropsView = LayerSyncPropertiesPanelFactory.createModelXsecSyncPanel();
                    break;
                case MAP_VIEW_ORDER:
                    syncPropsView = LayerSyncPropertiesPanelFactory.createMapSyncPanel();
                    break;
                default:
                    throw new IllegalArgumentException("Unable to create displayParamsView for unknown GeoDataOrder: " + order);
            }

            LocalSubsetPropertiesController subsetPropsController = new LocalSubsetPropertiesController(subsetProps, subsetPropsView);

            LayerDisplayParametersController displayParamsController;
            switch (order) {
                case CROSS_SECTION_ORDER:
                    displayParamsController = new ModelXsecDisplayParametersController((ModelXsecDisplayParameters) displayParams,
                            (ModelXsecDisplayParametersPanel) displayParamsView);
                    break;
                case MAP_VIEW_ORDER:
                    displayParamsController = new MapDisplayParametersController((MapDisplayParameters) displayParams,
                            (MapDisplayParametersPanel) displayParamsView);
                    break;
                default:
                    throw new IllegalArgumentException("Unable to create displayParamsView for unknown GeoDataOrder: " + order);
            }

            SyncPropertiesController syncController = new SyncPropertiesController(syncProps, syncPropsView);

            layerPropertiesDlg = PropertiesEditorFactory.createLayerPropertiesDialog(
                    frame,
                    datasetProperties, layerProps,
                    dsPropsView, subsetPropsView, displayParamsView, syncPropsView,
                    dsPropsController, subsetPropsController, displayParamsController,
                    syncController);
        } else {
            layerPropertiesDlg = PropertiesEditorFactory.createLayerPropertiesDialog(
                    frame,
                    datasetProperties,
                    dsPropsView,
                    dsPropsController);
        }
        return layerPropertiesDlg;
    }

    public static LayerPropertiesDialog createHorizonPropertiesDialog(JFrame frame, HorizonProperties datasetProperties,
            LayerDisplayProperties layerProps, GeoIOAdapter geoIOadapter, float[] mutableHorizon) {
        LayerPropertiesDialog layerPropertiesDlg;
        HorizonDatasetPropertiesPanel dsPropsView = new HorizonDatasetPropertiesPanel();
        HorizonDatasetPropertiesController dsPropsController =
                new HorizonDatasetPropertiesController(datasetProperties, dsPropsView,
                geoIOadapter, mutableHorizon);

        if (layerProps != LayerDisplayProperties.NULL) {
            LocalSubsetProperties subsetProps = layerProps.getLocalSubsetProperties();
            LayerDisplayParameters displayParams = layerProps.getLayerDisplayParameters();
            LayerSyncProperties syncProps = layerProps.getLayerSyncProperties();

            LocalSubsetPropertiesPanel subsetPropsView = new LocalSubsetPropertiesPanel();
            //this is invalid but reflects the previous functionality - it should invoke createModelPropertiesDialog()

            JPanel displayParamsView;
            GeoDataOrder order = datasetProperties.getMetadata().getGeoDataOrder();
            switch (order) {
                case CROSS_SECTION_ORDER:
                    displayParamsView = LayerDisplayParametersPanelFactory.createHorizonXsecPanel();
                    break;
                case MAP_VIEW_ORDER:
                    displayParamsView = LayerDisplayParametersPanelFactory.createHorizonMapPanel();
                    break;
                default:
                    throw new IllegalArgumentException("Unable to create displayParamsView for unknown GeoDataOrder: " + order);
            }

            //LayerSyncPropertiesPanel syncPropsView = new LayerSyncPropertiesPanel();
            LayerSyncPropertiesPanel syncPropsView;
            switch (order) {
                case CROSS_SECTION_ORDER:
                    syncPropsView = LayerSyncPropertiesPanelFactory.createModelXsecSyncPanel();
                    break;
                case MAP_VIEW_ORDER:
                    syncPropsView = LayerSyncPropertiesPanelFactory.createMapSyncPanel();
                    break;
                default:
                    throw new IllegalArgumentException("Unable to create displayParamsView for unknown GeoDataOrder: " + order);
            }

            LocalSubsetPropertiesController subsetPropsController = new LocalSubsetPropertiesController(subsetProps, subsetPropsView);

            LayerDisplayParametersController displayParamsController;
            switch (order) {
                case CROSS_SECTION_ORDER:
                    displayParamsController = new HorizonXsecDisplayParametersController(
                            HorizonXsecDisplayParametersFactory.createLayerDisplayParameters((HorizonXsecDisplayParameters) displayParams),
                            (HorizonXsecDisplayParametersPanel) displayParamsView);
                    break;
                case MAP_VIEW_ORDER:
                    displayParamsController = new MapDisplayParametersController(
                            MapDisplayParametersFactory.createLayerDisplayParameters((MapDisplayParameters) displayParams),
                            (MapDisplayParametersPanel) displayParamsView);
                    break;
                default:
                    throw new IllegalArgumentException("Unable to create displayParamsView for unknown GeoDataOrder: " + order);
            }

            SyncPropertiesController syncController = new SyncPropertiesController(syncProps, syncPropsView);

            layerPropertiesDlg = PropertiesEditorFactory.createLayerPropertiesDialog(
                    frame, datasetProperties, layerProps,
                    dsPropsView, subsetPropsView, displayParamsView, syncPropsView,
                    dsPropsController, subsetPropsController, displayParamsController,
                    syncController);
        } else {
            layerPropertiesDlg = PropertiesEditorFactory.createLayerPropertiesDialog(
                    frame,
                    datasetProperties,
                    dsPropsView,
                    dsPropsController);
        }
        return layerPropertiesDlg;
    }
}