/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui.layer;

import com.bhpb.geographics.model.window.WindowModel;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragGestureRecognizer;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.swing.JList;
import javax.swing.ListSelectionModel;

public class ListDnD extends JList implements DragSourceListener, DragGestureListener {

    private static final Logger logger =
            Logger.getLogger(ListDnD.class.toString());
    private DragGestureRecognizer dragGestureRecognizer;
    private DragSource dragSource = new DragSource();
    private LayerListTransferHandler transferHandler;
    private Shape dndLine = null;
    private Transferable transferable;

    public ListDnD(WindowModel windowModel) {
        super(windowModel);
        setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        transferHandler = new LayerListTransferHandler(ArrayList.class, windowModel);
        setTransferHandler(transferHandler);
        dragGestureRecognizer = dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_MOVE, this);
        setDragEnabled(true);
    }

    private void setDndLine(Shape dndLine) {
        this.dndLine = dndLine;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (dndLine == null) {
            return;
        }
        g.setColor(Color.BLACK);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setStroke(new BasicStroke(2));
        g2d.draw(dndLine);
    }

    public void dragGestureRecognized(DragGestureEvent dge) {
        logger.info("Drag gesture recognized.");
        transferable = transferHandler.createTransferable(this);
        dragSource.startDrag(dge, DragSource.DefaultMoveDrop, transferable, this);
    }

    public void dragEnter(DragSourceDragEvent dsde) {
        logger.info("Drag Enter");
        updateDnDLine(getMousePosition());
    }

    public void dragExit(DragSourceEvent dse) {
        logger.info("Drag Exit");
        dndLine = null;
        repaint();
    }

    public void dragOver(DragSourceDragEvent dsde) {
        logger.info("Drag Over");
        updateDnDLine(getMousePosition());
    }

    public void dragDropEnd(DragSourceDropEvent dsde) {
        String result;
        if (dsde.getDropSuccess()) {
            result = "success";
        } else {
            result = "fail";
        }
        logger.info("Drag Drop End: " + result);

        dndLine = null;
        repaint();
    }

    public void dropActionChanged(DragSourceDragEvent dsde) {
        logger.info("Drop Action Changed");
    }

    public void updateDnDLine(Point point) {
        int index = locationToIndex(point);
        boolean lineBelow = true;

        Rectangle cellBounds = getCellBounds(index, index);

        if ((point.getY() - cellBounds.getY()) / cellBounds.getHeight() < 0.5) {
            lineBelow = false;
        }

        if (lineBelow) {
            setDndLine(new Line2D.Double(cellBounds.getMinX(), cellBounds.getMaxY(), cellBounds.getMaxX(), cellBounds.getMaxY()));
        } else {
            setDndLine(new Line2D.Double(cellBounds.getMinX(), cellBounds.getMinY(), cellBounds.getMaxX(), cellBounds.getMinY()));
        }

        logger.info("Index: " + index + " lineBelow: " + lineBelow);
        repaint();
    }
}