/*
###########################################################################
# This program module Copyright (C) 2009 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui.layer.dataset;

import com.bhpb.geographics.controller.DatasetPropertiesController;
import com.bhpb.geographics.ui.ValidatingPanel;

public class VerticalRangePanel extends javax.swing.JPanel {

    private transient DatasetPropertiesController controller = null;
    private boolean focusListenersEnabled = true;
    private ValidatingPanel parentView = null;

    /** Creates new form VerticalRangePanel */
    public VerticalRangePanel() {
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        fullVertRangeField = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        vertRangeMinField = new javax.swing.JFormattedTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        vertRangeMaxField = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        vertRangeStepField = new javax.swing.JTextField();
        sync = new javax.swing.JPanel();
        syncVertRange = new javax.swing.JCheckBox();

        jPanel1.setLayout(new java.awt.GridLayout(2, 1));

        jLabel1.setText("Available Range");
        jPanel1.add(jLabel1);

        fullVertRangeField.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.add(fullVertRangeField);

        add(jPanel1);

        jPanel3.setLayout(new java.awt.GridLayout(2, 1));

        jLabel2.setText("Minimum");
        jPanel3.add(jLabel2);

        vertRangeMinField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vertRangeMinFieldsetVerticalRangeMin(evt);
            }
        });
        vertRangeMinField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                vertRangeMinFieldFocusLost(evt);
            }
        });
        jPanel3.add(vertRangeMinField);

        add(jPanel3);

        jPanel4.setLayout(new java.awt.GridLayout(2, 1));

        jLabel3.setText("Maximum");
        jPanel4.add(jLabel3);

        vertRangeMaxField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vertRangeMaxFieldsetVerticalRangeMax(evt);
            }
        });
        vertRangeMaxField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                vertRangeMaxFieldFocusLost(evt);
            }
        });
        jPanel4.add(vertRangeMaxField);

        add(jPanel4);

        jPanel5.setLayout(new java.awt.GridLayout(2, 1));

        jLabel4.setText("Step");
        jPanel5.add(jLabel4);

        vertRangeStepField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vertRangeStepFieldActionPerformed(evt);
            }
        });
        vertRangeStepField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                vertRangeStepFieldFocusLost(evt);
            }
        });
        jPanel5.add(vertRangeStepField);

        add(jPanel5);

        syncVertRange.setText("Synchronize");
        sync.add(syncVertRange);

        add(sync);
    }// </editor-fold>//GEN-END:initComponents

    public void setController(DatasetPropertiesController controller) {
        this.controller = controller;
    }

    public void setParentView(ValidatingPanel parentView) {
        this.parentView = parentView;
    }

    private void vertRangeMinFieldsetVerticalRangeMin(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vertRangeMinFieldsetVerticalRangeMin
        String vertRangeStr = vertRangeMinField.getText();
        controller.setVerticalRangeMin(vertRangeStr);
        setVertRangeMin(controller.getVerticalRangeMin());
}//GEN-LAST:event_vertRangeMinFieldsetVerticalRangeMin

    private void vertRangeMinFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_vertRangeMinFieldFocusLost
        if (focusListenersEnabled) {
            String vertRangeStr = vertRangeMinField.getText();
            controller.setVerticalRangeMin(vertRangeStr);
            setVertRangeMin(controller.getVerticalRangeMin());
        }
}//GEN-LAST:event_vertRangeMinFieldFocusLost

    public void setFocusListenersEnabled(boolean enabled) {
        focusListenersEnabled = enabled;
    }

    private void vertRangeMaxFieldsetVerticalRangeMax(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vertRangeMaxFieldsetVerticalRangeMax
        String vertRangeStr = vertRangeMaxField.getText();
        controller.setVerticalRangeMax(vertRangeStr);
        setVertRangeMax(controller.getVerticalRangeMax());
}//GEN-LAST:event_vertRangeMaxFieldsetVerticalRangeMax

    private void vertRangeMaxFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_vertRangeMaxFieldFocusLost
        if (focusListenersEnabled) {
            String vertRangeStr = vertRangeMaxField.getText();
            controller.setVerticalRangeMax(vertRangeStr);
            setVertRangeMax(controller.getVerticalRangeMax());
        }
}//GEN-LAST:event_vertRangeMaxFieldFocusLost

    private void vertRangeStepFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vertRangeStepFieldActionPerformed
        String vertRangeStr = vertRangeStepField.getText();
        controller.setVerticalRangeIncr(vertRangeStr);
        setVertRangeStep(controller.getVerticalRangeIncr());
    }//GEN-LAST:event_vertRangeStepFieldActionPerformed

    private void vertRangeStepFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_vertRangeStepFieldFocusLost
        if (focusListenersEnabled) {
            String vertRangeStr = vertRangeStepField.getText();
            controller.setVerticalRangeIncr(vertRangeStr);
            setVertRangeStep(controller.getVerticalRangeIncr());
        }
    }//GEN-LAST:event_vertRangeStepFieldFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField fullVertRangeField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel sync;
    private javax.swing.JCheckBox syncVertRange;
    private javax.swing.JTextField vertRangeMaxField;
    private javax.swing.JFormattedTextField vertRangeMinField;
    private javax.swing.JTextField vertRangeStepField;
    // End of variables declaration//GEN-END:variables

    public void setFullVertRange(String range) {
        fullVertRangeField.setText(range);
    }

    public void setVerticalKeyName(String vKeyName) {
        setBorder(
                javax.swing.BorderFactory.createTitledBorder(
                javax.swing.BorderFactory.createLineBorder(
                new java.awt.Color(153, 204, 255)),
                "Vertical Range (" + vKeyName + ")",
                javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                javax.swing.border.TitledBorder.DEFAULT_POSITION,
                new java.awt.Font("Dialog", 1, 11))); // NOI18N
    }

    public void setVertRangeMin(Double value) {
        vertRangeMinField.setText(value.toString());
    }

    public void setVertRangeMax(Double value) {
        vertRangeMaxField.setText(value.toString());
    }

    public void setVertRangeStep(Double value) {
        vertRangeStepField.setText(value.toString());
    }

    public void setVertRangeSync(boolean setting) {
        syncVertRange.setSelected(setting);
    }

    public void setFullKeyRange(String fullKeyRange) {
        fullVertRangeField.setText(fullKeyRange);
    }
}