/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui.layer;

import com.bhpb.geographics.model.HorizonLayer;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.ModelLayer;
import com.bhpb.geographics.model.SeismicLayer;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.qiworkbench.compAPI.DataDataDescriptor;
import com.bhpb.qiworkbench.compAPI.DataDataDescriptorTransferable;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.TransferHandler;

public class LayerListTransferHandler extends TransferHandler {

    private static final Logger logger =
            Logger.getLogger(LayerListTransferHandler.class.toString());
    private DataFlavor prjMgrFlavor;
    private DataFlavor qiViewerFlavor;
    private WindowModel winModel;

    public LayerListTransferHandler(Class clazz, WindowModel winModel) {
        this.winModel = winModel;

        String localType = DataFlavor.javaJVMLocalObjectMimeType + ";class=" + clazz.getName();
        try {
            qiViewerFlavor = new DataFlavor(localType);
        } catch (ClassNotFoundException cnfe) {
            logger.warning("Drag-and-Drop flavor disabled: " + cnfe.getMessage());
        }

        prjMgrFlavor = DataDataDescriptorTransferable.localDataDataDescriptorFlavor;
    }

    @Override
    public boolean canImport(JComponent c, DataFlavor[] flavors) {
        return hasFlavor(flavors, qiViewerFlavor) || hasFlavor(flavors, prjMgrFlavor);
    }

    private boolean hasFlavor(DataFlavor[] flavors, DataFlavor flavor) {
        if (flavor != null) {
            for (int i = 0; i < flavors.length; i++) {
                if (flavors[i].equals(flavor)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean importData(JComponent c, Transferable t) {
        if (!canImport(c, t.getTransferDataFlavors())) {
            return false;
        }
        try {
            JList target = (JList) c;
            int targetIndex = target.getSelectedIndex();
            Point mousePos = target.getMousePosition();

            //insertBelow is now the default to make it invariant with the case targetIndex == -1
            boolean insertBelowTargetCell = true;

            if (targetIndex != -1) {
                Rectangle targetCellBounds = target.getCellBounds(targetIndex, targetIndex);
                
                //if mouse is on or above the target cell's horizontal center line, insert ABOVE the target cell
                if ((mousePos.getY() - targetCellBounds.getY())/(targetCellBounds.getHeight()) < 0.5) {
                    insertBelowTargetCell = false;
                }
            }
            WindowModel targetModel;

            if (target.getModel() instanceof WindowModel) {
                targetModel = (WindowModel) target.getModel();
            } else {
                logger.warning("Cannot import data - target ListModel is not a WindowModel");
                return false;
            }

            //as per bhpViewer, if the drop destination is invalid, treat it as the final list cell
            if (targetIndex == -1) {
                targetIndex = targetModel.getSize() - 1;
            }

            String locString = insertBelowTargetCell ? " below " : " above";
            logger.info("Inserting the dropped List of Layers " + locString + " cell #" + targetIndex);
            
            if (insertBelowTargetCell) {
                targetIndex++;
            }

            if (hasFlavor(t.getTransferDataFlavors(), qiViewerFlavor)) {
                return importLayerArrayList(targetModel, (ArrayList) t.getTransferData(qiViewerFlavor), targetIndex);
            }
            if (hasFlavor(t.getTransferDataFlavors(), prjMgrFlavor)) {
                return importDataDataDescriptor(targetModel, (DataDataDescriptor) t.getTransferData(prjMgrFlavor), targetIndex);
            } else {
                return false;
            }
        } catch (UnsupportedFlavorException ufe) {
            logger.warning("caught UnsupportedFlavorException while importing data for DnD: " + ufe.getMessage());
            return false;
        } catch (IOException ioe) {
            logger.warning("caught IOException while importing data for DnD: " + ioe.getMessage());
            return false;
        }
    }

    private boolean importDataDataDescriptor(WindowModel targetModel, DataDataDescriptor ddd, int targetIndex) {
        try {
            String path = ddd.getPath();
            if (path != null) {
                targetModel.loadLayer(path, targetIndex);
                return true;
            } else {
                logger.info("importDataDataDescriptor failed due to null path");
                return false;
            }
        } catch (Exception e) {
            logger.info("importDataDataDescriptor failed due to: " + e.getMessage());
            return false;
        }
    }

    private boolean importLayerArrayList(WindowModel targetModel, ArrayList draggedList, int targetIndex) {
        int nDraggedElementsInTargetModel = getNumElementsIn(targetModel, draggedList);

        if (nDraggedElementsInTargetModel == draggedList.size()) {
            reOrderModelElements(targetModel, draggedList, targetIndex);
            return true;
        } else if (nDraggedElementsInTargetModel == 0) {
            addFromList(targetModel, draggedList);
            return true;
        } else {
            logger.warning("Unable to drop a list when some elements are in the target listmodel and some are not");
            return false;
        }
    }

    private void addFromList(WindowModel targetModel, ArrayList draggedList) {
        int nLayersAdded = 0;

        for (int i = draggedList.size() - 1; i >= 0; i--) {
            Object obj = draggedList.get(i);
            if (obj instanceof Layer) {
                Layer layer = null;
                if (obj instanceof SeismicLayer) {
                    layer = SeismicLayer.createSeismicLayer((SeismicLayer) obj, targetModel);
                } else if (obj instanceof ModelLayer) {
                    layer = ModelLayer.createModelLayer((ModelLayer) obj, targetModel);
                } else if (obj instanceof HorizonLayer) {
                    layer = HorizonLayer.createHorizonLayer((HorizonLayer) obj, targetModel);
                } else {
                    logger.warning("Cannot import Layer of unrecognized subclass: " + obj.getClass().getName());
                }
                if (layer != null) {
                    try {
                        targetModel.add(0, layer);
                        nLayersAdded++;
                    } catch (IllegalArgumentException iae) {
                        logger.warning(iae.getMessage());
                        JOptionPane.showMessageDialog(null, iae.getMessage());
                    }
                }
            } else {
                logger.warning("Cannot import non-Layer object of type: " + obj.getClass().getName());
            }
        }
    }

    private int getNumElementsIn(WindowModel targetModel, ArrayList draggedList) {
        int nHits = 0;
        for (Object obj : draggedList) {
            if (obj instanceof Layer && targetModel.getZorder((Layer) obj) != -1) {
                nHits++;
            }
        }
        return nHits;
    }

    private void reOrderModelElements(WindowModel targetModel, ArrayList draggedList, int targetIndex) {
        //iterate over the list of objects to insert backward, as each successive
        //insertion will push the previously added objects down the list
        for (int i = draggedList.size() - 1; i >= 0; i--) {
            Object dndObj = draggedList.get(i);
            if (dndObj instanceof Layer) {
                targetModel.moveToIndex((Layer) dndObj, targetIndex);
            }
        }
    }

    @Override
    public Transferable createTransferable(JComponent c) {
        if (c instanceof ListDnD) {
            ListDnD source = (ListDnD) c;
            int[] indices = source.getSelectedIndices();
            Object[] values = source.getSelectedValues();
            if (values == null || values.length == 0) {
                return null;
            } else {
                ArrayList<Layer> layerList = new ArrayList<Layer>(values.length);
                for (int i = 0; i < values.length; i++) {
                    layerList.add((Layer) values[i]);
                }
                return new LayerListTransferable(indices, layerList);
            }
        } else {
            logger.warning("Cannot create Transferable - JComponent is not a ListDnD");
            return null;
        }
    }

    class LayerListTransferable implements Transferable {

        private ArrayList data;

        public LayerListTransferable(int[] indices, ArrayList alist) {
            data = alist;
        }

        public Object getTransferData(DataFlavor flavor)
                throws UnsupportedFlavorException {
            if (!isDataFlavorSupported(flavor)) {
                throw new UnsupportedFlavorException(flavor);
            }
            return data;
        }

        public DataFlavor[] getTransferDataFlavors() {
            return new DataFlavor[]{qiViewerFlavor};
        }

        public boolean isDataFlavorSupported(DataFlavor flavor) {
            return qiViewerFlavor.equals(flavor);
        }
    }
}