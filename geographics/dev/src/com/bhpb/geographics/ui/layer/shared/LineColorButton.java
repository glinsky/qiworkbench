/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui.layer.shared;

import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters.COLOR;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JButton;

public class LineColorButton extends JButton {
    private static final int LINE_OFFSET = 10;
    private static final int LINE_THICKNESS = 5;
    
    private Color lineColor = Color.BLACK;
    
    public void setColor(Color color) {
        lineColor = color;
        repaint();
    }
    
    public void setColor(String colorName) {
        setColor(HorizonXsecDisplayParameters.getColor(COLOR.valueOf(colorName)));
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        int y = super.getHeight() / 2;
        int startX = LINE_OFFSET;
        int endX = super.getWidth() - LINE_OFFSET;
        
        ((Graphics2D)g).setStroke(new BasicStroke(LINE_THICKNESS));
        g.setColor(lineColor);
        g.drawLine(startX, y, endX, y);
    }
    
    public Color getColor() {
        return lineColor;
    }
}