/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;

public class VerticalLabel extends HeaderLabel {
    private int defaultWidth;
    private int defaultHeight;

    /**
     * No-arg constructor for Matisse.
     */
    public VerticalLabel() {
        this("VerticalLabel");
    }

    public VerticalLabel(String text) {
        super(text);
        Dimension preferredSize = super.getPreferredSize();
        defaultWidth = preferredSize.height;
        defaultHeight = preferredSize.width;
        setMinimumSize(new Dimension(defaultWidth, defaultHeight));
        setPreferredSize(new Dimension(defaultWidth, defaultHeight));
    }

    @Override
    public void paint(Graphics g) {
        ((Graphics2D) g).rotate(Math.toRadians(270));
        g.setColor(Color.BLACK);
        FontRenderContext frc = ((Graphics2D) g).getFontRenderContext();
        String caption = super.getText();
        TextLayout cString = new TextLayout(caption, super.getFont(), frc);
        cString.draw(((Graphics2D) g), calcRotatedXoffset(), calcRotatedYoffset());
    }

    private int calcRotatedXoffset() {
        return -1 * ((getSize().height + defaultHeight) / 2);
    }

    private int calcRotatedYoffset() {
        return ((getSize().width + defaultWidth) / 2);
    }
}