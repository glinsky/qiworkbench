/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.util.logging.Logger;
import javax.swing.JComponent;

public class RotatedLabel extends JComponent {

    private static final int DEFAULT_FONT_SIZE = 12;
    private static final Font DEFAULT_FONT = new Font("Times", Font.PLAIN, DEFAULT_FONT_SIZE);
    private static final Logger logger =
            Logger.getLogger(RotatedLabel.class.getName());

    //text angle(radians)
    private double textAngle;
    private double textHeight;
    private double textWidth;
    private String text;

    /**
     * No-arg constructor for Matisse.
     */
    public RotatedLabel() {
        this("RotatedLabel");
    }

    /**
     * Creates a vertical label with the specified text rotation 90 degrees counter-clockwise;
     * @param text
     */
    public RotatedLabel(String text) {
        this(text, 270.0);
    }

    /**
     * Creates a vertical label with the text rotated the specified number of degrees (clockwise).
     *
     * @param text a non-null String of any size
     * @param angle rotation of text angle in degrees (clockwise)
     */
    public RotatedLabel(String text, double angle) {
        setText(text);
        setFont(DEFAULT_FONT);
        setTextAngle(angle);
        setOpaque(true);
    }

    private double calcHeight() {
        AffineTransform af = AffineTransform.getRotateInstance(textAngle);
        FontRenderContext frc = new FontRenderContext(af, false, false);
        return getFont().getStringBounds(text, frc).getHeight();
    }

    private double calcWidth() {
        AffineTransform af = AffineTransform.getRotateInstance(textAngle);
        FontRenderContext frc = new FontRenderContext(af, false, false);
        return getFont().getStringBounds(text, frc).getWidth();
    }

    public double calcRotatedHeight() {
        return textWidth * Math.abs(Math.sin(textAngle)) +
                textHeight * Math.abs(Math.cos(textAngle));
    }

    public double calcRotatedWidth() {
        return textWidth * Math.abs(Math.cos(textAngle)) +
                textHeight * Math.abs(Math.sin(textAngle));
    }

    public String getText() {
        return text;
    }

    public double getTextAngle() {
        return 180 * textAngle / Math.PI;
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        AffineTransform at = g2d.getTransform();

        Rectangle bounds = getBounds();

        g2d.setColor(super.getForeground());


        int halfWidth = (int) Math.round(bounds.width / 2.0);
        int halfHeight = (int) Math.round(bounds.height / 2.0);

        g2d.translate(halfWidth, halfHeight);
        g2d.rotate(textAngle);
        g2d.translate(-textWidth/2.0, textHeight/2.0);
        g2d.drawString(text, 0, 0);

        //restore the original transformation matrix
        g2d.setTransform(at);
    }

    @Override
    public void setFont(Font newFont) {
        super.setFont(newFont);
        textWidth = calcWidth();
        textHeight = calcHeight();
    }

    public void setTextAngle(double textAngle) {
        this.textAngle = (Math.PI * textAngle) / 180.0;
        textWidth = calcWidth();
        textHeight = calcHeight();

        Dimension newSize = new Dimension((int)Math.ceil(calcRotatedWidth()), (int)Math.ceil(calcRotatedHeight()));
        //setMinimumSize(newSize);
        setPreferredSize(newSize);
        logger.info("w: " + newSize.width + " h: " + newSize.height);
    }

    public void setText(String text) {
        this.text = text;
    }
}