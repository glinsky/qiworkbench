/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import com.bhpb.geographics.model.ViewTarget;
import com.bhpb.geographics.model.window.WindowSyncBroadcaster;
import com.bhpb.geographics.ui.AbstractGeoPlot.ACTION_MODE;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.util.logging.Logger;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * GeoPanelMouseAdapter handles mouse events related to clicking, dragging, mouse entry &
 * mouse exit for GeoPlot.  It must explicitly implement MouseMotionListener for J2SE 1.5
 * compatibility.
 */
public class GeoPanelMouseAdapter extends MouseAdapter implements MouseMotionListener {

    private static final Logger logger =
            Logger.getLogger(GeoPanelMouseAdapter.class.toString());
    private boolean processMouseDrag = true;
    private AbstractGeoPlot geoPlot;
    private JPopupMenu popup = new JPopupMenu();
    private Point zoomStart;
    private WindowSyncBroadcaster windowBroadcaster;

    public GeoPanelMouseAdapter(AbstractGeoPlot geoPanel, WindowSyncBroadcaster broadcaster) {
        this.geoPlot = geoPanel;
        this.windowBroadcaster = broadcaster;

        JMenuItem menuItem = new JMenuItem("Broadcast traverse");
        menuItem.addActionListener(new BroadcastTraverseActionListener(geoPlot));
        popup.add(menuItem);

        menuItem = new JMenuItem("Delete traverse");
        menuItem.addActionListener(new DeleteTraverseActionListener());
        popup.add(menuItem);

        menuItem = new JMenuItem("Broadcast point");
        menuItem.addActionListener(new BroadcastPointActionListener());
        popup.add(menuItem);
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        switch (me.getButton()) {
            case 1:
                handleLeftClick(me);
                break;
            case 3:
                handleRightClick(me);
                break;
            default:
                logger.info("No action associated with mouse button #" + me.getButton());
        }
    }

    private void handleRightClick(MouseEvent me) {
        popup.show(me.getComponent(), me.getX(), me.getY());
    }

    private void handleLeftClick(MouseEvent me) {
        int clickCount = me.getClickCount();
        switch (clickCount) {
            case 1:
                //do nothing unless arbitrary traverse drawing is in progress
                //Note that picking occurs as in bhpViewer, when button is pressed, not clicked.
                if (geoPlot.isArbitraryTraverseEnabled()) {
                    if (geoPlot.getActionMode() == ACTION_MODE.DRAW_ARB_TRAV) {
                        geoPlot.addToArbitraryTraverse(me.getPoint());
                    }
                }
                break;
            case 2:
                if (geoPlot.isArbitraryTraverseEnabled()) {
                    if (geoPlot.getActionMode() == ACTION_MODE.DRAW_ARB_TRAV) {
                        logger.info("Double click ignoreod - drawing arbitrary traverse is already in progress.");
                    } else {
                        geoPlot.startArbitraryTraverse(me.getPoint());
                    }
                }
                break;
            default:
                logger.info("No action associated with " + clickCount + "-click event.");
        }
    }

    @Override
    /**
     * (Re) enables mouse dragging.  Determines whether or not the end point of
     * a rubberband zoom should result in a valid zoom action or be ignored and the
     * ongoing action cancelled.
     */
    public void mouseEntered(MouseEvent me) {
        processMouseDrag = true;
        geoPlot.setMouseInsideGeoPlot(true);
    }

    @Override
    /**
     * Cancels any ongoing rectangle zoom action and sets the ViewTarget associated
     * with the GeoPlot to (@link ViewTarget.NO_TARGET}.
     */
    public void mouseExited(MouseEvent me) {
        processMouseDrag = false;
        if (geoPlot.getActionMode() == ACTION_MODE.ZOOM_REGION) {
            geoPlot.cancelZoom();
        }
        geoPlot.setCursorTarget(ViewTarget.NO_TARGET);
        geoPlot.setMouseInsideGeoPlot(false);
    }

    /**
     * As per bhpViewer, pressing the mouse initiates a rectangle zoom action
     * on non-horizon-xsec layers, and a picking action on horizon-xsec layers.
     * One of these two types of layers should always be active (selected).
     *
     * @param me MouseEvent containing the point at which the mouse button was pressed
     */
    @Override
    public void mousePressed(MouseEvent me) {
        Point point = me.getPoint();
        if (geoPlot.isZoomEnabled()) {
            zoomStart = point;
            geoPlot.setZoomStart(zoomStart);
        } else if (geoPlot.isPickingEnabled() /*&& geoPlot.getActionMode() == ACTION_MODE.PICK*/) {
            geoPlot.pick(point);
        } else {
            logger.warning("Mouse press not processed beacuse neither picking nor zooming is enabled: " + toString(me));
        }
    }

    @Override
    /**
     * Initiates a rectangle zoom and repaint of the associated GeoPlot if
     * a dragging action started in the GeoPlot is occurring.  Otherwise, a message
     * is logged to the console.
     */
    public void mouseReleased(MouseEvent me) {
        //This differentiates click from drag and release
        if (geoPlot.getActionMode() == ACTION_MODE.ZOOM_REGION && me.getX() != zoomStart.getX() && me.getY() != zoomStart.getY()) {
            geoPlot.setZoomEnd(me.getPoint());
            geoPlot.repaint();
            geoPlot.zoomRubberband();
        } else {
            geoPlot.cancelZoom();
        }
    }

    private String toString(MouseEvent me) {
        Point point = me.getPoint();
        return ("x: " + point.getX() + "y: " + point.getY());
    }

    //@Override
    /**
     * Override annotation is deliberately omitted as MouseAdapter only implements MouseListener,
     * not MouseMotionListener, in Java 1.5.  This annotation would be permitted in J2SE 1.6.
     *
     * @param me MouseEvent
     */
    public void mouseMoved(MouseEvent me) {
        Point2D point = me.getPoint();
        geoPlot.setCursorPos(point, false);
        windowBroadcaster.broadcastCursorPos(geoPlot.getCursorTarget());
    }

    //@Override
    /**
     * Override annotation is deliberately omitted as MouseAdapter only implements MouseListener,
     * not MouseMotionListener, in Java 1.5.  This annotation would be permitted in J2SE 1.6.
     *
     * @param me MouseEvent
     *
     */
    public void mouseDragged(MouseEvent me) {
        //ignore additional mouse drags if the mouse has left the plot and not re-entered it
        if (!processMouseDrag) {
            return;
        }
        Point2D point = me.getPoint();
        geoPlot.setCursorPos(point, true);
        windowBroadcaster.broadcastCursorPos(geoPlot.getCursorTarget());
    }
}
