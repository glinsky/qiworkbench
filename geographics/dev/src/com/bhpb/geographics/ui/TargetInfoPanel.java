/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import com.bhpb.geographics.model.ViewTarget;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class TargetInfoPanel extends JPanel {
    private static final String NO_TARGET = "No target";
    private JTextField vTextField = new JTextField(8);
    private JTextField hTextField = new JTextField();
    
    public TargetInfoPanel() {
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

        add(new JLabel("Vertical: "));
        vTextField.setEditable(false);
        vTextField.setMaximumSize(new Dimension(88,40));
        vTextField.setBackground(Color.WHITE);
        
        add(vTextField);

        add(new JLabel("Horizontal: "));
        hTextField.setEditable(false);
        hTextField.setBackground(Color.WHITE);
        add(hTextField);

        setTargetInfo(ViewTarget.NO_TARGET);
    }
    
    public void setTargetInfo(ViewTarget targetProps) {
        if (targetProps == ViewTarget.NO_TARGET) {
            vTextField.setText(NO_TARGET);
            hTextField.setText(NO_TARGET);
        } else {
            vTextField.setText(targetProps.getVerticalString());
            hTextField.setText(targetProps.getHorizontalString());
        }

    }
}