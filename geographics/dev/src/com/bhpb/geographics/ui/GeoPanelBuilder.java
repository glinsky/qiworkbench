/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import com.bhpb.geographics.RenderableImageLayer;
import com.bhpb.geographics.listener.ScrollPaneAdjustmentListener;
import com.bhpb.geographics.model.CursorSettings;
import com.bhpb.geographics.model.HorizonLayer;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.ModelLayer;
import com.bhpb.geographics.model.SeismicLayer;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geographics.model.window.WindowSyncBroadcaster;
import com.bhpb.geographics.rasterizer.LayeredRenderableImageProducer;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseListener;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

/**
 * Builder class which uses abstract methods to create the
 * <ul>
 *  <li>GeoPlot</li>
 *  <li>Horizontal Annotation Panel</li>
 *  <li>Vertical Annotation Panel</li>
 *  <li>Optional corner caption panel(s)</li>
 *  <li>Renderable Image Layer (consumer)</li>
 *  <li>Renderable Image Producer (rasterizer)</li>
 * </ul>
 * 
 * The builder methods are used by this GeoPanelBuilder to create the central
 * GeoPlot and decorate the enclosing scrollpane.
 * 
 * @author folsw9
 */
public abstract class GeoPanelBuilder {

    protected enum DECORATION_LOC {

        LEFT, RIGHT, TOP, BOTTOM, UL, UR, LL, LR
    };

    protected enum DECORATION_TYPE {

        V_ANNOT, H_ANNOT, CORNER_CAP
    };
    private static final Logger logger = Logger.getLogger(GeoPanelBuilder.class.toString());

    public String asScrollPaneCorner(DECORATION_LOC location) {
        switch (location) {
            case UL:
                return JScrollPane.UPPER_LEFT_CORNER;
            case UR:
                return JScrollPane.UPPER_RIGHT_CORNER;
            case LL:
                return JScrollPane.LOWER_LEFT_CORNER;
            case LR:
                return JScrollPane.LOWER_RIGHT_CORNER;
            default:
                throw new IllegalArgumentException("Location is not a corner: " + location);
        }
    }

    /**
     * Builds a GeoPanel with the specified initial Image and Panel dimensions.
     * The appearance of the rendered image is governed by the data and properties
     * contained in the Layer and WindowProperties.
     * 
     * @param panelWidth initial GeoPanel width
     * @param panelHeight initial GeoPanel height
     * @param imageWidth initial preferred width of the GeoPlot, determines scrollbar position if any
     * @param imageHeight initial preferred height of the GeoPlot, determines scrollbar position if any
     * @param layer initial layer to be displayed - empty GeoPanel lacking a Layer cannot be instantiated
     * @param winProps window properties associated with any Layer displayed in the new GeoPanel
     * 
     * @return GeoPanel of the requested width and height, with scrollbars if necessary,
     * configured to rasterize the specified layer using its properties and the specified WindowProperties.
     */
    public GeoPanel buildGeoPanel(int panelWidth, int panelHeight,
            int imageWidth, int imageHeight, Layer layer,
            WindowProperties winProps, 
            CursorSettings cursorSettings,
            WindowSyncBroadcaster winSyncBroadcaster,
            WindowModel winModel) {

        GeoPanel geoPanel = new GeoPanel(panelWidth, panelHeight,
                imageWidth, imageHeight, layer,
                winProps, cursorSettings, winModel);

        AbstractGeoPlot geoPlot = createGeoPlot(
                imageWidth, 
                imageHeight, 
                layer, 
                winProps, 
                cursorSettings,
                winSyncBroadcaster,
                winModel);

        MouseListener[] defaultListeners = geoPlot.getMouseListeners();
        for (MouseListener ml : defaultListeners) {
            logger.info("Removing mouse listener: " + ml.getClass().toString());
            geoPlot.removeMouseListener(ml);
        }

        geoPlot.setPreferredSize(new Dimension(imageWidth, imageHeight));
        geoPlot.setBackground(geoPanel.getBackground());

        GeoPanelMouseAdapter mouseAdapter = new GeoPanelMouseAdapter(geoPlot, winSyncBroadcaster);
        geoPlot.addMouseListener(mouseAdapter);
        geoPlot.addMouseMotionListener(mouseAdapter);

        geoPlot.addMouseMotionListener(new CaptionListener(geoPanel));
        
        geoPanel.setGeoPlot(geoPlot);

        AnnotationPane annotationPane = createScrollPane(geoPlot, layer, winProps);
        geoPanel.setAnnotationPane(annotationPane);

        TargetInfoPanel targetInfoPanel = new TargetInfoPanel();
        geoPanel.setTargetInfoPanel(targetInfoPanel);
        geoPanel.add(targetInfoPanel, BorderLayout.SOUTH);

        return geoPanel;
    }

    /**
     * NOTE: This method will likely become abstract or call new abstract methods to determine
     * behavior as GeoPanels are decorated differently depending on which of the 6
     * Layer types is used to initialize the GeoPanel.  Most likely it will invoke an abstract method
     * getBorderDecoration(int) where int is one of the BorderLayout positions.
     * 
     * Decorates the JScrollPane with captions, annotations or other Components
     * as determined by the implementation of the following abstract methods in
     * the concrete GeoPanelBuilder subclass:
     * <ul>
     *   <li>createCaptionBorderPanel</li>
     *   <li>createHorizontalAnnotationPanel</li>
     *   <li>createUndecoratedBorderPanel</li>
     *   <li>createVerticalAnnotationPanel</li>
     * </ul>
     * 
     * @param layer the Layer used to determine the initial annotations and captiosn
     * @param scrollPane the JScrollPane to be decorated
     * @param winProps the WindowProperties used to determine the names, orientations and other Annotation properties
     */
    private void createAnnotationPanels(Layer layer, AnnotationPane scrollPane, WindowProperties winProps, AbstractGeoPlot geoPlot) {
        scrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER, createUndecoratedCorner());
        scrollPane.setCorner(JScrollPane.UPPER_RIGHT_CORNER, createUndecoratedCorner());
        scrollPane.setCorner(JScrollPane.LOWER_RIGHT_CORNER, createUndecoratedCorner());
        scrollPane.setCorner(JScrollPane.LOWER_LEFT_CORNER, createUndecoratedCorner());

        if (hasDecoration(DECORATION_TYPE.CORNER_CAP)) {
            DECORATION_LOC location = getLocation(DECORATION_TYPE.CORNER_CAP);
            JComponent decoration = getDecoration(DECORATION_TYPE.CORNER_CAP, layer, winProps, geoPlot);
            scrollPane.setCorner(asScrollPaneCorner(location), decoration);
        }

        if (hasDecoration(DECORATION_TYPE.H_ANNOT)) {
            try {
                JComponent annotationPanel = getDecoration(DECORATION_TYPE.H_ANNOT, layer, winProps, geoPlot);
                scrollPane.setColumnHeaderView(annotationPanel);
            } catch (Exception ex) {
                logger.warning("Unable to create annotationPanel due to: " + ex.getMessage());
            }
        }

        if (hasDecoration(DECORATION_TYPE.V_ANNOT)) {
            try {
                JComponent annotationPanel = getDecoration(DECORATION_TYPE.V_ANNOT, layer, winProps, geoPlot);
                scrollPane.setRowHeaderView(annotationPanel);
            } catch (Exception ex) {
                logger.warning("Unable to create annotationPanel due to: " + ex.getMessage());
            }
        }
    }

    /**
     * Creaets the central GeoPlot using the concrete subclass implementations of
     * the createRenderableImageLayer and createRenderableImageProducer methods.
     *
     * @param imageWidth the initial image width
     * @param imageHeight the initial image height
     * @param layer the first layer to display in the GeoPlot
     * @param winProps the windowProperties used by the rasterizer
     *
     * @return a new GeoPlot customized for the type and orientation of the Layer
     */
    private AbstractGeoPlot createGeoPlot(
            int imageWidth, 
            int imageHeight, 
            Layer layer, 
            WindowProperties winProps,
            CursorSettings cursorSettings,
            WindowSyncBroadcaster winSyncBroadcaster,
            WindowModel winModel) {
        AbstractGeoPlot geoPlot = new NonBlockingGeoPlot(true,
                imageWidth,
                imageHeight,
                winProps, cursorSettings,
                winSyncBroadcaster,
                winModel);
        return geoPlot;
    }

    /**
     * Creates the RenderableImageLayer which receives rasterized images from the RenderableImageProducer
     * 
     * @param layer the Layer to be rasterized
     * 
     * @return a new RenderableImageLayer
     */
    public abstract RenderableImageLayer createRenderableImageLayer(Layer layer);

    /**
     * Creates the RenderableImageProducer which sends rasterized images to the RenderableImageLayer.
     * 
     * @param layer the Layer to be rasterized
     * @param img the RenderableImageLayer which consumed rasterized images
     * @param geoPlot the GeoPlot which contains the visible rectangle geometry
     *
     * @return a new LayeredRenderableImageProducer(rasterizer)
     */
    public abstract LayeredRenderableImageProducer createRenderableImageProducer(Layer layer,
            RenderableImageLayer img, AbstractGeoPlot geoPlot);

    /**
     * Encloses the GeoPlot with a JScrollPane, decorating the vertical and horizontal axes with
     * captions and/or annotations or other Components by invoking createAnnotationPanels().
     * 
     * @param geoPlot will be wrapped in the new JScrollPane
     * @param layer contains properties and data necessary to initialize the AnnotationPanels
     * @param winProps WindowPropeties also govern the configuration of the AnnotationPanels
     * 
     * @return JScrollPane wrapping the specified GeoPlot
     */
    AnnotationPane createScrollPane(AbstractGeoPlot geoPlot, Layer layer, WindowProperties winProps) {
        AnnotationPane scrollPane = new AnnotationPane(geoPlot);
        //geoPlot.setScrollPane(scrollPane);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        ScrollPaneAdjustmentListener listener = new ScrollPaneAdjustmentListener(geoPlot);
        scrollPane.getHorizontalScrollBar().addAdjustmentListener(listener);
        scrollPane.getVerticalScrollBar().addAdjustmentListener(listener);

        createAnnotationPanels(layer, scrollPane, winProps, geoPlot);

        return scrollPane;
    }

    /**
     * Gets the JComponent (JLabel) which will be used to fill any undecorated border regions
     * of the GeoPanel.
     *
     * @return an empty JComponent with Color.WHITE background
     */
    protected JComponent createUndecoratedCorner() {
        JComponent cornerComp = new JPanel();
        cornerComp.setPreferredSize(new Dimension(30, 30));
        cornerComp.setMinimumSize(new Dimension(30, 30));
        cornerComp.setBackground(Color.WHITE);
        return cornerComp;
    }

    public static GeoPanelBuilder getInstance(Layer layer) {
        GeoDataOrder order = layer.getDatasetProperties().getMetadata().getGeoDataOrder();
        if (layer instanceof SeismicLayer) {
            switch (order) {
                case CROSS_SECTION_ORDER:
                    return new SeismicXsecGeoPanelBuilder();
                case MAP_VIEW_ORDER:
                    return new SeismicMapGeoPanelBuilder();
                default:
                    throw new UnsupportedOperationException("Cannot create GeoPanel for unknown layer order: " + order);
            }

        } else if (layer instanceof ModelLayer) {
            switch (order) {
                case CROSS_SECTION_ORDER:
                    return new ModelXsecGeoPanelBuilder();
                case MAP_VIEW_ORDER:
                    return new ModelMapGeoPanelBuilder();
                default:
                    throw new UnsupportedOperationException("Cannot create GeoPanel for unknown layer order: " + order);
            }
        } else if (layer instanceof HorizonLayer) {
            switch (order) {
                case CROSS_SECTION_ORDER:
                    return new HorizonXsecGeoPanelBuilder();
                case MAP_VIEW_ORDER:
                    return new HorizonMapGeoPanelBuilder();
                default:
                    throw new UnsupportedOperationException("Cannot create GeoPanel for unknown layer order: " + order);
            }
        } else {
            throw new UnsupportedOperationException("Unable to create GeoPanel for layer of type: " + layer.getClass().getName());
        }
    }

    protected abstract boolean hasDecoration(DECORATION_TYPE decorType);

    protected abstract JComponent getDecoration(DECORATION_TYPE decorType, Layer layer, WindowProperties winProps, AbstractGeoPlot geoPlot);

    protected abstract DECORATION_LOC getLocation(DECORATION_TYPE decorType);

    /**
     * Checks whether the given Layer is of the correct type for the concrete
     * GeoPanelBuilder subclass.
     * 
     * @param layer The Layer for which the GeoPanel will be initialized.
     * 
     * @return true if and only if the Layer type and GeoDataOrder are correct
     * for the GeoPanelBuilder.
     */
    protected abstract boolean isValid(Layer layer);
}