/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import com.bhpb.geographics.LayeredImageConsumer;
import com.bhpb.geographics.RenderableImageLayer;
import com.bhpb.geographics.model.Annotation;
import com.bhpb.geographics.model.ArbitraryTraverse;
import com.bhpb.geographics.model.CursorSettings;
import com.bhpb.geographics.model.HorizonHighlightSettings;
import com.bhpb.geographics.model.HorizonLayer;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.PickingSettings.SNAPPING_MODE;
import com.bhpb.geographics.model.ViewTarget;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geographics.model.window.WindowSyncBroadcaster;
import com.bhpb.geographics.rasterizer.LayeredRenderableImageProducer;
import com.bhpb.geographics.ui.event.GeoPlotEvent;
import com.bhpb.geographics.ui.event.GeoPlotEventListener;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.Point2D;
import java.awt.image.ColorModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

/**
 * Abstract subclass of JPanel which renders the ordered images of a set of Layers
 * according to the associated LayeredRenderableImageProducers, the
 * CursorSettings, HorizonHighlightSettings and WindowProperties.
 *
 * The paintComponent(Graphics) method
 * should be overriden by subclasses as it is in the NonBlockingGeoPlot implementations.
 */
public abstract class AbstractGeoPlot extends JPanel implements LayeredImageConsumer {

    private static final long serialVersionUID = -3236786102452391738L;
    private static final transient String CANNOT_PICK_MSG = "Picking and snapping are disabled when no active layer is selected.";
    private static final transient String CANNOT_SNAP_MSG = "For snapping, the annotation layer must be a seismic cross-section.";

    //at any given time, the GeoPlot is performing exactly one action:
    public static enum ACTION_MODE {

        ZOOM_REGION, DRAW_ARB_TRAV, PICK, NONE
    }

    private enum ZOOM_TYPE {

        ZOOM_IN, ZOOM_OUT, ZOOM_ALL, ZOOM_RUBBERBAND
    }
    protected ACTION_MODE actionMode = ACTION_MODE.NONE;
    protected boolean mouseInsideGeoPlot = false;
    protected boolean shutdown = false;
    protected boolean redraw = true;
    protected boolean newCompositeImageCreated = false;
    protected CursorPainter cursorPainter;
    protected int compositeOriginX;
    protected int compositeOriginY;
    protected int compositeImageHeight = 0;
    protected int compositeImageWidth = 0;
    protected Layer lastAnnotatedLayer = null;
    protected List<GeoPlotEventListener> geoPlotEventListeners =
            new ArrayList<GeoPlotEventListener>();
    protected transient Map<Integer, SparseBufferedImage> backBuffers =
            new HashMap<Integer, SparseBufferedImage>();
    protected transient Map<Integer, SparseBufferedImage> bufferedImages =
            new HashMap<Integer, SparseBufferedImage>();
    protected volatile Map<LayeredRenderableImageProducer, Thread> currentRasterizer =
            new HashMap<LayeredRenderableImageProducer, Thread>();
    protected volatile Map<LayeredRenderableImageProducer, Thread> nextRasterizer =
            new HashMap<LayeredRenderableImageProducer, Thread>();
    protected Map<LayeredRenderableImageProducer, Layer> producerLayerMap =
            new HashMap<LayeredRenderableImageProducer, Layer>();
    protected Map<Layer, LayeredRenderableImageProducer> layerProducerMap =
            new HashMap<Layer, LayeredRenderableImageProducer>();
    protected Map<Annotation.AXIS, List<AnnotationLabel>> axisAnnotationLabelMap =
            new HashMap<Annotation.AXIS, List<AnnotationLabel>>();
    protected Point2D zoomStart;
    protected Point2D zoomEnd;
    protected Rectangle lastVisibleRect = new Rectangle(0, 0, 0, 0);
    protected Set<LayeredRenderableImageProducer> producers =
            new HashSet<LayeredRenderableImageProducer>();
    protected SparseBufferedImage compositeImage; // holds the blended image prior to drawing the cursor
    protected WindowModel windowModel;
    protected WindowProperties winProps;
    protected WindowSyncBroadcaster windowSyncBroadcaster;
    private JScrollPane scrollPane;
    private ViewTarget lastOriginTarget = ViewTarget.NO_TARGET;

    /**
     * Constructor for geoPlots which use a configurable verticale step size: CrossSectionImage
     * @param isDoubleBuffered
     * @param width
     * @param height
     * @param ldParams
     */
    AbstractGeoPlot(boolean isDoubleBuffered, int width, int height,
            WindowProperties winProps, CursorSettings cursorSettings,
            WindowSyncBroadcaster windowSyncBroadcaster, WindowModel windowModel) {
        super(isDoubleBuffered);

        this.windowSyncBroadcaster = windowSyncBroadcaster;
        this.windowModel = windowModel;

        cursorPainter = new CursorPainter(cursorSettings, this, windowModel);

        changeWindowProperties(winProps);

        addComponentListener(new ComponentAdapter() {

            @Override
            public void componentResized(ComponentEvent ce) {
                broadcastEvent(GeoPlotEvent.GEOPLOT_RESIZED);
            }

            @Override
            public void componentShown(ComponentEvent ce) {
                broadcastEvent(GeoPlotEvent.GEOPLOT_SHOWN);
            }
        });

        //Initialize the annotation label maps with empty lists so that calls to updateSize() prior to the creation of the
        //annotation scrollpane will not cause a NPE.
        axisAnnotationLabelMap.put(Annotation.AXIS.X_AXIS, new ArrayList<AnnotationLabel>(0));
        axisAnnotationLabelMap.put(Annotation.AXIS.Y_AXIS, new ArrayList<AnnotationLabel>(0));

        initRasterizers();
    }

    public void addGeoPlotEventListener(GeoPlotEventListener listener) {
        geoPlotEventListeners.add(listener);
    }

    /**
     * Adds a point to the active Layer's arbitrary traverse.  Has no effect is there is currently
     * no active Layer.
     *
     * @param arbTravStart Point2D to add to the arbitrary traverse (in view coordinates)
     */
    synchronized void addToArbitraryTraverse(Point2D arbTravStart) {
        Layer activeLayer = windowModel.getActiveLayer();
        if (activeLayer == null) {
            return;
        }
        activeLayer.getArbitraryTraverse().addPoint(
                ViewTarget.createTargetProps(activeLayer, (int) arbTravStart.getX(), (int) arbTravStart.getY()));
    }

    /**
     * Where W is the width of the visible Rectangle and H is the height,
     * adjusts the visible Rectangle such that minX = minX - w,
     * maxX = maxX + w, minY = minY - h, maxY = maxY + h.
     *
     * Essentially the new min x,y is one full viewport width up and left of the
     * actual visible rectangle and the new max x,y is one full viewport down and right,
     * limited by the preferredsize of the entire plot.
     *
     * The new rectangle will be clipped at the bounds of the visible Rectangle.
     *
     * @param visRect
     * @return
     */
    
    private Rectangle adjustVisibleRect(Rectangle visRect, Dimension plotSize) {
        double width = visRect.getWidth();
        double height = visRect.getHeight();

        double adjustedMinX = Math.max(0, visRect.getMinX() - width);
        double adjustedMaxX = Math.min(plotSize.width, adjustedMinX + 3 * width);
        double adjustedMinY = Math.max(0, visRect.getMinY() - height);
        double adjustedMaxY = Math.min(plotSize.height, adjustedMinY + 3 * height);

        return new Rectangle(
                (int) adjustedMinX,
                (int) adjustedMinY,
                (int) (adjustedMaxX - adjustedMinX),
                (int) (adjustedMaxY - adjustedMinY));
    }

    protected void broadcastEvent(GeoPlotEvent event) {
        for (GeoPlotEventListener listener : geoPlotEventListeners) {
            listener.handleEvent(event);
        }
    }

    /**
     * Updates the rasterizer contexts for all Layers to the requested size.
     * This should be invoked when the associated JScrollPane receives a resize event.
     *
     * @param size the revised visible rectangle size associated with RasterizerContexts of
     * this GeoPlot
     */
    public void broadcastResize(Dimension size) {
        broadcastEvent(GeoPlotEvent.GEOPLOT_RESIZED);
    }

    /**
     * If broadcast is enabled and any layer is currently selected for annotation,
     * the ViewTarget coresponding to the upper-left corner of the current visbile region
     * is broadcast.  Otherwise, this method returns with no action.
     */
    public void broadcastScroll() {
        if (!windowSyncBroadcaster.canBroadcast()) {
            return;
        }
        Layer currentAnnotatedLayer = windowModel.getAnnotatedLayer();

        if (currentAnnotatedLayer == null) {
            return;
        }

        Rectangle currentVisRect = getVisibleRect();
        ViewTarget currentOriginTarget = ViewTarget.createTargetProps(
                currentAnnotatedLayer,
                currentVisRect.x,
                currentVisRect.y);

        //Bugfix for spurious sync events caused by redundant or automatic scroll events.
        //One example of this was broadcast to 0,0 when a window was initially displayed.
        //If this is not the first attempt to broadcast (which occurs when the scrollbars are initially positioned)
        //and the originTarget has change, broadcast, otherwise, simply update the lastOriginTarget.
        if (lastOriginTarget != ViewTarget.NO_TARGET && !currentOriginTarget.equals(lastOriginTarget)) {
            windowSyncBroadcaster.broadcastScrollPos(currentOriginTarget);
        }

        lastOriginTarget = currentOriginTarget;
    }

    /**
     * Broadcasts the activeLayer's arbitrary traverse using the active Layer's
     * sync group.  Has no effect if there is currently no active Layer.
     */
    public void broadcastTraverse() {
        Layer activeLayer = windowModel.getActiveLayer();

        if (activeLayer == null) {
            return;
        }
        activeLayer.broadcastArbTrav();
    }

    /**
     * Cancels the zoom action currently in progress.  If the current ACTION_MODE
     * is ZOOM_REGION, it is changed to NONE, otherwise, this method has no effect.
     */
    synchronized void cancelZoom() {
        //do not invoke synchronized getter/setter in case of deadlock
        if (actionMode == ACTION_MODE.ZOOM_REGION) {
            zoomStart = null;
            actionMode = ACTION_MODE.NONE;
        }
    }

    /**
     * Updates the cursor settings used by this GeoPlot, redrawing immediately.
     * @param cursorSettings
     */
    public void changeCursorSettings(CursorSettings cursorSettings) {
        cursorPainter.setCursorSettings(cursorSettings);
        repaint();
    }

    /**
     * Updates the window properties shared by all layers of this GeoPlot,
     * redrawing if necessary.
     * @param winProps
     */
    public void changeWindowProperties(WindowProperties winProps) {
        this.winProps = new WindowProperties(winProps);
        if (isValid()) {
            redraw(true, true);
        }
    }

    protected void clearPlotBackground(Graphics g, Color bgColor, int width, int height) {
        g.setColor(bgColor);
        g.fillRect(0, 0, width, height);
    }

    public ACTION_MODE getActionMode() {
        return actionMode;
    }

    public ViewTarget getCursorTarget() {
        return cursorPainter.getCursorTarget();
    }

    private GeoPanelBuilder getGeoPanelBuilder(Layer layer) {
        GeoDataOrder orientation = layer.getOrientation();

        switch (orientation) {
            case CROSS_SECTION_ORDER:
                if (layer.isSeismicLayer()) {
                    return new SeismicXsecGeoPanelBuilder();
                } else if (layer.isModelLayer()) {
                    return new ModelXsecGeoPanelBuilder();
                } else if (layer.isHorizonLayer()) {
                    return new HorizonXsecGeoPanelBuilder();
                } else {
                    throw new IllegalArgumentException("Unknown layer type: " + layer.getLayerID());
                }
            case MAP_VIEW_ORDER:
                if (layer.isSeismicLayer()) {
                    return new SeismicMapGeoPanelBuilder();
                } else if (layer.isModelLayer()) {
                    return new ModelMapGeoPanelBuilder();
                } else if (layer.isHorizonLayer()) {
                    return new HorizonMapGeoPanelBuilder();
                } else {
                    throw new IllegalArgumentException("Unknown layer type: " + layer.getLayerID());
                }
            default:
                throw new IllegalArgumentException("Unknown orientation: " + orientation);
        }
    }

    public List<GeoPlotEventListener> getGeoPlotEventListeners() {
        return geoPlotEventListeners;
    }
    
    /**
     * Returns the Rectangle showing the AbstractGeoPlot's boundaries within the
     * enclosing JScrollPane.  If this AbstractGeoPlot has not yet been added
     * to a JScrollPane, then the Rectangle (0,0,0,0) is returned.
     * 
     * @return Rectangle bounding this AbstractGeoPlot component
     */
    public Rectangle getViewportBounds() {
        if (scrollPane == null) {
            return new Rectangle(0, 0, 0, 0);
        } else {
            return scrollPane.getViewport().getViewRect();
        }
    }

    public String getWindowID() {
        return windowSyncBroadcaster.getWinID();
    }

    public void initRasterizers() {

        Iterator<Layer> iter = windowModel.iterator();

        //clear all maps and sets: producer->layer, layer->producer, producers, current & next rasterizers
        producerLayerMap = new HashMap<LayeredRenderableImageProducer, Layer>();
        layerProducerMap = new HashMap<Layer, LayeredRenderableImageProducer>();
        producers = new HashSet<LayeredRenderableImageProducer>();
        currentRasterizer.clear();
        nextRasterizer.clear();

        while (iter.hasNext()) {
            Layer layer = iter.next();
            RenderableImageLayer img = getGeoPanelBuilder(layer).createRenderableImageLayer(layer);
            LayeredRenderableImageProducer producer = getGeoPanelBuilder(layer).createRenderableImageProducer(layer, img, this);
            img.setRasterizer(producer);

            producer.addConsumer(this);
            producers.add(producer);
            producerLayerMap.put(producer, layer);
            layerProducerMap.put(layer, producer);
            nextRasterizer.put(producer, new Thread(producer));
            layer.setGeoPlot(this);
        }

        updateSize();
    }

    private void interruptCurrentRasterizer(Thread currentRasterizerThread) {
        if (currentRasterizerThread != null && currentRasterizerThread.isAlive()) {
            currentRasterizerThread.interrupt();
        }
    }

    public synchronized boolean isArbitraryTraverseEnabled() {
        Layer activeLayer = windowModel.getActiveLayer();

        if (activeLayer != null) {
            return activeLayer.isArbitraryTraverseEnabled();
        } else {
            return false; //arbitrary traverse can only occur if a layer is selected
        }
    }

    /**
     * If picking is currently happening, this method returns true.  If no action is currently
     * ongoing and the active layer is a non-null horizon xsec, then this method returns true.
     * Otherwise, it returns false, for example if the active layer is not a horizon xsec or the current
     * action is DRAW_ARB_TRAV (in which case the test for active layer being horizon xsec should also fail).
     *
     * @return true if picking is enabled
     */
    public synchronized boolean isPickingEnabled() {
        Layer activeLayer = windowModel.getActiveLayer();

        if (actionMode == ACTION_MODE.PICK) {
            return true;
        } else if (actionMode == ACTION_MODE.NONE && activeLayer != null) {
            return activeLayer.isHorizonLayer() &&
                    activeLayer.getDatasetProperties().getMetadata().getGeoDataOrder() == GeoDataOrder.CROSS_SECTION_ORDER;
        } else {
            return false;
        }
    }

    /**
     * Returns true if any layer associated with this GeoPlot has a living current rasterizer
     * or non-null 'next' rasterizer.
     *
     * @return true if rendering is ongoing or has been requested
     */
    public boolean isRendering() {
        if (mapContainsLivingThread(currentRasterizer)) {
            return true;
        }
        if (mapContainsNonNullThread(nextRasterizer)) {
            return true;
        }
        return false;
    }

    /**
     * Returns true if and only if the current ACTION_MODE is ZOOM.
     *
     * @return true if zooming is enabled for the active layer
     */
    boolean isZoomEnabled() {
        Layer activeLayer = windowModel.getActiveLayer();

        //if zooming is already happening, it is enabled
        if (actionMode == ACTION_MODE.ZOOM_REGION) {
            return true;
        //if arb traverse drawing or picking is happening, zoom is disabled
        } else if (actionMode == ACTION_MODE.DRAW_ARB_TRAV || actionMode == ACTION_MODE.PICK) {
            return false;
        } else if (actionMode == ACTION_MODE.NONE && activeLayer != null) {
            //if the no action is ongoing, picking is the default for horizon xsec layers
            //and zooming is not
            if (activeLayer.isHorizonLayer() &&
                    activeLayer.getDatasetProperties().getMetadata().getGeoDataOrder() == GeoDataOrder.CROSS_SECTION_ORDER) {
                return false;
            //for all other layer types, zooming is the default behavior
            } else {
                return true;
            }
        //if there is no active layer, zooming is disabled
        } else {
            return false;
        }
    }

    boolean isZooming() {
        return zoomStart != null;
    }

    protected boolean mapContainsLivingThread(Map<LayeredRenderableImageProducer, Thread> map) {
        for (Entry<LayeredRenderableImageProducer, Thread> rasterizerEntry : map.entrySet()) {
            Thread rasterizer = rasterizerEntry.getValue();
            if (rasterizer != null && rasterizer.isAlive()) {
                return true;
            }
        }
        return false;
    }

    protected boolean mapContainsNonNullThread(Map<LayeredRenderableImageProducer, Thread> map) {
        for (Entry<LayeredRenderableImageProducer, Thread> rasterizerEntry : map.entrySet()) {
            Thread rasterizer = rasterizerEntry.getValue();
            if (rasterizer != null) {
                return true;
            }
        }
        return false;
    }

    protected void paintArbTrav(Graphics2D g, ArbitraryTraverse arbTrav,
            Color squareFillColor, Color squareBorderColor, Color lineColor) {
        double arbTravPointWidth = 5.0;

        Point2D prevPoint = null;
        Color prevColor = g.getColor();

        for (Point2D nextPoint : arbTrav.getPoints()) {
            if (prevPoint != null) {
                g.setColor(lineColor);
                g.drawLine(
                        (int) Math.round(prevPoint.getX()),
                        (int) Math.round(prevPoint.getY()),
                        (int) Math.round(nextPoint.getX()),
                        (int) Math.round(nextPoint.getY()));
            }
            g.setColor(squareFillColor);
            g.fillRect((int) (nextPoint.getX() - arbTravPointWidth / 2.0),
                    (int) (nextPoint.getY() - arbTravPointWidth / 2.0),
                    (int) (arbTravPointWidth),
                    (int) (arbTravPointWidth));
            g.setColor(squareBorderColor);
            g.drawRect((int) (nextPoint.getX() - arbTravPointWidth / 2.0),
                    (int) (nextPoint.getY() - arbTravPointWidth / 2.0),
                    (int) (arbTravPointWidth),
                    (int) (arbTravPointWidth));
            prevPoint = nextPoint;
        }

        g.setColor(prevColor);
    }

    /**
     * If picking is enabled for the active layer, the requested point is picked
     * otherwise, no action occurs.
     */
    public void pick(Point point) {
        if (isPickingEnabled()) { //picking enabled is only true if the active layer is a horizon xsec and the action mode is picking

            HorizonLayer currentActiveLayer = (HorizonLayer) windowModel.getActiveLayer();
            Layer annotatedLayer = windowModel.getAnnotatedLayer();
            if (currentActiveLayer == null) {
                new Thread(new Runnable() {

                    public void run() { // this error message should never happen if isPickingEnabled() == true
                        JOptionPane.showMessageDialog(null, CANNOT_PICK_MSG);
                    }
                }).start();
                return;
            } else if ( // snapping is only allowed if the annotated layer is a seismic xsec
                    currentActiveLayer.getPickingSettings().getSnappingMode() != SNAPPING_MODE.NO_SNAP &&
                    !(annotatedLayer.isSeismicLayer() && annotatedLayer.getOrientation() == GeoDataOrder.CROSS_SECTION_ORDER)) {
                new Thread(new Runnable() {

                    public void run() {
                        JOptionPane.showMessageDialog(null, CANNOT_SNAP_MSG);
                    }
                }).start();
                return;
            } else {
                currentActiveLayer.pick(
                        ViewTarget.createTargetProps(annotatedLayer, (int) point.getX(), (int) point.getY()),
                        annotatedLayer.getDataObjects(),
                        annotatedLayer.getDatasetProperties().getSummary());
            }
            redraw(currentActiveLayer, true, false);
        }
    }

    /**
     * Create a Thread to run the RenderableImageProducer after the current producer,
     * if any, finishes.  If doImmediately is true, then the current producer is interrupted.
     *
     * Redraws all layers.
     *
     * @param doImmediately interrupt the rasterization currently in progress
     */
    public synchronized void redraw(boolean doImmediately, boolean updatePlotSize) {

        if (updatePlotSize) {
            updateSize();
        }

        Rectangle viewPortBounds = adjustVisibleRect(getViewportBounds(), getPreferredSize());
        //uncomment this line to enable rendering of one extra viewport area above, below,
        //left, right and diagonally from the actual GeoPlot bounds.
        //as this is done for every redraw(), it is quite time-consuming.
        //Rectangle viewPortBounds = getViewportBounds();

        for (LayeredRenderableImageProducer producer : producers) {
            //reversed order of interrupt... nextRasterizer.put to eliminate possibility
            //of interrupting newly created rasterizer before it is finished
            if (doImmediately) {
                interruptCurrentRasterizer(currentRasterizer.get(producer));
            }

            //producer.setVisibleRectangle(adjustedVisRect);
            producer.setVisibleRectangle(viewPortBounds);
            // do not rasterize hidden layers, put null rasterizer to overwrite any
            // initialized but not yet started rasterizer created prior to layer being hiddden
            if (producerLayerMap.get(producer).isHidden()) {
                nextRasterizer.put(producer, null);
            } else {
                nextRasterizer.put(producer, new Thread(producer));
            }
        }
        broadcastEvent(GeoPlotEvent.RENDERING_STARTED);
        cursorPainter.setWaitCursor();
        //If there are currently no producers, force an immediate repaint
        //so that the previous image will be cleared.
        if (producers.size() == 0) {
            redraw = true;
            repaint();
        }

        notify();
    }

    /**
     * Create a Thread to run the RenderableImageProducer after the current producer,
     * if any, finishes.  If doImmediately is true, then the current producer is interrupted.
     *
     * Redraws specified layer only.
     *
     * @param layer the Layer to redraw
     * @param doImmediately interrupt the rasterization currently in progress
     * @param updatePlotSize if true, the GeoPlot's size will be changed to match
     * that of the current annotated layer
     */
    public void redraw(Layer layer, boolean doImmediately, boolean updatePlotSize) {
        Layer currentAnnotatedLayer = windowModel.getAnnotatedLayer();
        winProps.getScaleProperties().setVisibleRectangle(getVisibleRect(),
                currentAnnotatedLayer);

        if (updatePlotSize) {
            updateSize();
        }
        LayeredRenderableImageProducer producer = layerProducerMap.get(layer);
        //reversed order of interrupt... nextRasterizer.put to eliminate possibility
        //of interrupting newly created rasterizer before it is finished
        if (doImmediately) {
            interruptCurrentRasterizer(currentRasterizer.get(producer));
        }
        producer.setVisibleRectangle(getVisibleRect());
        nextRasterizer.put(producer, new Thread(producer));
        broadcastEvent(GeoPlotEvent.RENDERING_STARTED);
        cursorPainter.setWaitCursor();

        //if layer is hidden, no image will be generated, therefore repaint will only
        //occur if explicitly invoked
        if (layer.isHidden()) {
            redraw = true;
            repaint();
        }
    }

    public void removeEventListener(GeoPlotEventListener listener) {
        geoPlotEventListeners.remove(listener);
    }

    protected void resetPlot() {
        compositeOriginX = 0;
        compositeOriginY = 0;
        compositeImage = null;
        Rectangle visRect = getViewportBounds();
        clearPlotBackground(getGraphics(), Color.WHITE, visRect.width, visRect.height);
    }

    /**
     * Attempts to scroll this AbstractGeoPlot so that the ViewTarget.getPoint() relative
     * to the currently annotated layer is the upper-leftmost pixel of this AbstractGeoPlot's
     * bounds.
     *
     * Depending on the dataset, scale and AbstractGeoPlot size, this may also result in a
     * visible but not upper-left ViewTarget.
     *
     * If the ViewTarget is not valid for the current annotated layer, then this
     * method has no effect.
     *
     * @param originTarget if possible, the point to position at the upper-left corner
     * of this AbstractGeoPlot
     */
    public void scrollToOrigin(ViewTarget originTarget) {
        scrollToOrigin(originTarget, getViewportBounds(), windowModel.getAnnotatedLayer());
    }

    protected void scrollToOrigin(ViewTarget originTarget, Rectangle currentVisRect, Layer currentAnnotatedLayer) {
        Point2D newViewPoint = originTarget.getPoint2D(windowModel.getAnnotatedLayer());
        //slight bug fix: position scrollbar to view the left edge, not the center, of the target trace
        final Rectangle newVisRect = new Rectangle(
                (int) newViewPoint.getX(),
                (int) newViewPoint.getY(),
                currentVisRect.width,
                currentVisRect.height);

        if (SwingUtilities.isEventDispatchThread()) {
            scrollRectToVisible(newVisRect);
        } else {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    scrollRectToVisible(newVisRect);
                }
            });
        }
    }

    public void scrollToVisiblePoint(double dataCoord) {
        Rectangle boundsRect = getViewportBounds();

        int yCoord = windowModel.getAnnotatedLayer().getVerticalPixelCoord(dataCoord);

        scrollRectToVisible(new Rectangle((int) boundsRect.getX(), yCoord,
                (int) boundsRect.getWidth(), (int) boundsRect.getHeight()));
    }

    public void scrollToVisiblePoint(int x, int y) {
        scrollRectToVisible(new Rectangle(x, y,
                1,1));
    }

    public synchronized void setActionMode(ACTION_MODE actionMode) {
        this.actionMode = actionMode;
    }

    public void setAnnotationLabels(Annotation.AXIS axis, List<AnnotationLabel> labels) {
        axisAnnotationLabelMap.put(axis, labels);
    }

    public void setColorModel(ColorModel mode, int zOrderl) {
        throw new UnsupportedOperationException("Not implemented");
    }

    /**
     * Sets the cursor position.  If the mouse is being dragged, then the current
     * action is set to ACTION.ZOOM_REGION.  This prevents a button press in the absence
     * of mouse movement from being handled as a zoom, and allows a click to initiate
     * other actions such as drawing arbitrary traverse points.
     *
     * @param cursorPos
     * @param mouseIsDragging
     */
    synchronized void setCursorPos(Point2D cursorPos, boolean mouseIsDragging) {
        ViewTarget target;

        Layer annotatedLayer = windowModel.getAnnotatedLayer();

        if (annotatedLayer == null) {
            target = ViewTarget.NO_TARGET;
        } else {
            target = ViewTarget.createTargetProps(
                    annotatedLayer,
                    (int) cursorPos.getX(),
                    (int) cursorPos.getY());
        }

        cursorPainter.setCursorTarget(target, mouseIsDragging, actionMode);
    }

    void setCursorTarget(ViewTarget target) {
        cursorPainter.setCursorTarget(target, false, actionMode);
    }

    public void setMouseInsideGeoPlot(boolean mouseInsideGeoPlot) {
        this.mouseInsideGeoPlot = mouseInsideGeoPlot;
    }

    public void setScrollPane(JScrollPane scrollPane) {
        this.scrollPane = scrollPane;
    }

    public void setShutdown(boolean value) {
        shutdown = value;
    }

    public void setSparseImage(SparseBufferedImage image, int zOrder) {
        backBuffers.put(zOrder, image);
        bufferedImages.put(zOrder, backBuffers.get(zOrder));
        redraw = true;
        repaint();
    }

    /**
     * Now thread-safe - rapid clicking should not result in a corrupt zoom() operation.
     */
    synchronized void setZoomEnd(Point2D zoomEnd) {
        if (getActionMode() != ACTION_MODE.ZOOM_REGION) {
            return;
        }
        if (zoomStart == null) {
            return;
        }
        this.zoomEnd = zoomEnd;
    }

    /**
     * Now thread-safe - rapid clicking should not result in a corrupt zoom() operation.
     */
    synchronized void setZoomStart(Point2D zoomStart) {
        this.zoomStart = zoomStart;
        actionMode = ACTION_MODE.ZOOM_REGION;
    }

    /**
     * If there is currently no active Layer or the action mode is other than NONE
     * or DRAW_ARB_TRAV, then this method does nothing.
     *
     * Otherwise, the current action mode is set to DRAW_ARB_TRAV
     * and the current arbitrary traverse is cleared.
     *
     * The current arbitrary traverse is then set to begin at the requested Point2D.
     *
     * @param arbTravStart view-space Point2D at which to start a new arbitrary traverse
     */
    synchronized void startArbitraryTraverse(Point2D arbTravStart) {
        Layer activeLayer = windowModel.getActiveLayer();

        if (activeLayer == null ||
                (actionMode != ACTION_MODE.NONE && actionMode != ACTION_MODE.DRAW_ARB_TRAV)) {
            return; // either there is no active layer or another type of action is currently in progress
        }

        actionMode = ACTION_MODE.DRAW_ARB_TRAV;

        //do not invoke synchronized getter in case of deadlock
        ArbitraryTraverse arbTrav = activeLayer.getArbitraryTraverse();

        arbTrav.clearPoints();
        arbTrav.addPoint(
                ViewTarget.createTargetProps(activeLayer, (int) arbTravStart.getX(), (int) arbTravStart.getY()));
    }

    public void updateAnnotationSizes(Dimension plotSize) {
        List<AnnotationLabel> annotationLabels = axisAnnotationLabelMap.get(Annotation.AXIS.X_AXIS);

        if (annotationLabels != null) {
            Iterator<AnnotationLabel> hAnnotIter = annotationLabels.iterator();
            while (hAnnotIter.hasNext()) {
                hAnnotIter.next().updateSize(plotSize);
            }
        }

        annotationLabels = axisAnnotationLabelMap.get(Annotation.AXIS.Y_AXIS);
        if (annotationLabels != null) {
            Iterator<AnnotationLabel> vAnnotIter = annotationLabels.iterator();
            while (vAnnotIter.hasNext()) {
                vAnnotIter.next().updateSize(plotSize);
            }
        }
    }

    private void updateSize() {
        int compositeWidth = 0;
        int compositeHeight = 0;

        Iterator<Layer> iter = windowModel.iterator();

        while (iter.hasNext()) {
            Layer layer = iter.next();
            if (layer.isHidden()) {
                continue;
            }
            Dimension layerSize = layer.getSize();

            compositeWidth = Math.max(compositeWidth, layerSize.width);
            compositeHeight = Math.max(compositeHeight, layerSize.height);
        }

        if (compositeWidth == 0 && compositeHeight == 0) {
            Dimension currentPreferredSize = getPreferredSize();
            compositeWidth = currentPreferredSize.width;
            compositeHeight = currentPreferredSize.height;
        }

        Dimension newScrollPaneSize = new Dimension(compositeWidth, compositeHeight);
        super.setPreferredSize(newScrollPaneSize);
        super.setMaximumSize(newScrollPaneSize);

        updateAnnotationSizes(newScrollPaneSize);

        revalidate();
    }

    /**
     * Update the window properties, resetting the scroll position as necessary
     * if window scale properties have been changed.
     * 
     * @param newWindowProperties
     */
    public void updateWindowProperties(WindowProperties newWindowProperties) {
        updateWindowProperties(newWindowProperties, true);
    }

    /**
     * Update the window propeties.
     *
     * @param newWindowProperties
     * @param adjustScroll if true, the scrollbar will be positioned so that the
     * origin's ViewTarget is equivalent to that prior to the udpate
     */
    private void updateWindowProperties(WindowProperties newWindowProperties, boolean adjustScroll) {
        ViewTarget preUpdateOrigin = ViewTarget.NO_TARGET;
        Layer currentAnnotatedLayer = windowModel.getAnnotatedLayer();

        if (adjustScroll) {
            Rectangle viewPortBounds = getViewportBounds();
            preUpdateOrigin =
                    ViewTarget.createTargetProps(winProps, currentAnnotatedLayer, viewPortBounds.x, viewPortBounds.y);
        }

        winProps = new WindowProperties(newWindowProperties);
        winProps.getScaleProperties().setVisibleRectangle(getViewportBounds(),
                currentAnnotatedLayer);

        if (currentAnnotatedLayer != null) {
            currentAnnotatedLayer.getWindowModel().getWindowProperties().setScaleProperties(winProps.getScaleProperties());
        }

        redraw(true, true);

        if (adjustScroll) {
            scrollToOrigin(preUpdateOrigin);
        }
    }

    public void updateHorizonHighlightSettings(HorizonHighlightSettings horizonHighlightSettings) {
        Iterator<Layer> iter = windowModel.iterator();

        while (iter.hasNext()) {
            Layer layer = iter.next();
            if (layer.isHorizonLayer()) {
                ((HorizonLayer) layer).setHorizonHighlightSettings(horizonHighlightSettings);
                redraw(layer, true, false);
            }
        }
    }

    public void updateCursorSettings(CursorSettings cursorSettings) {
        cursorPainter.setCursorSettings(cursorSettings);
        repaint();
    }

    /**
     * It does not change the current actionMode.
     * Performs zoom in, zoom out, zoom all or rectangle zooming based on the current annotated layer.
     *
     * @param zoomType ZOOM_TYPE
     */
    private synchronized void zoom(ZOOM_TYPE zoomType) {
        Layer currentAnnotatedLayer = windowModel.getAnnotatedLayer();

        if (currentAnnotatedLayer == null) {
            return;
        } else if (currentAnnotatedLayer != lastAnnotatedLayer) {
            winProps.getScaleProperties().updateReferenceLayer(currentAnnotatedLayer);
            lastAnnotatedLayer = currentAnnotatedLayer;
        }

        Rectangle currentVisRect = getViewportBounds();

        //bugfix for zooming when window is offscreen and has NEVER been visible (display results script)
        if (currentVisRect.height == 0 && currentVisRect.width == 0) {
            winProps.getScaleProperties().setVisibleRectangle(getViewportBounds(),
                    currentAnnotatedLayer);
        }

        ViewTarget preZoomOrigin = ViewTarget.createTargetProps(
                currentAnnotatedLayer,
                currentVisRect.x,
                currentVisRect.y);
        ViewTarget postZoomOrigin = ViewTarget.NO_TARGET;

        switch (zoomType) {
            case ZOOM_IN:
                //zoomIn does not change the upper-left target (model space that is, view space coords are 2x)
                postZoomOrigin = preZoomOrigin;
                winProps.getScaleProperties().zoomIn();
                break;
            case ZOOM_OUT:
                //zoomOut does not change the upper-left target (model space that is, view space coords are 2x)
                postZoomOrigin = preZoomOrigin;
                winProps.getScaleProperties().zoomOut();
                break;
            case ZOOM_ALL:
                //zoomAll changes the post-zoom origin to 0,0 (view space)
                postZoomOrigin = ViewTarget.createTargetProps(currentAnnotatedLayer,
                        0,
                        0);
                winProps.getScaleProperties().zoomAll(currentVisRect.width, currentVisRect.height);
                break;
            case ZOOM_RUBBERBAND:
                double upperLeftX = Math.min(zoomStart.getX(), zoomEnd.getX());
                double upperLeftY = Math.min(zoomStart.getY(), zoomEnd.getY());
                double lowerRightX = Math.max(zoomStart.getX(), zoomEnd.getX());
                double lowerRightY = Math.max(zoomStart.getY(), zoomEnd.getY());

                double width = lowerRightX - upperLeftX;
                double height = lowerRightY - upperLeftY;

                Rectangle newVisRect = new Rectangle((int) upperLeftX, (int) upperLeftY, (int) width, (int) height);

                //zoomRubberband _does_ change the view origin (model coords)
                postZoomOrigin = ViewTarget.createTargetProps(
                        currentAnnotatedLayer,
                        (int) Math.round(upperLeftX),
                        (int) Math.round(upperLeftY));

                winProps.getScaleProperties().zoomRect(newVisRect);

                zoomStart = null;
                zoomEnd = null;
        }

        //do not adjust scroll position after updating window properties;
        //the various zoom functions have specific scrolling behavior already
        updateWindowProperties(winProps, false);

        scrollToOrigin(postZoomOrigin, currentVisRect, currentAnnotatedLayer);

        if (windowSyncBroadcaster.canBroadcast()) {
            windowSyncBroadcaster.broadcastScale(winProps.getScaleProperties());
            windowSyncBroadcaster.broadcastScrollPos(postZoomOrigin);
        }
    }

    public void zoomAll() {
        zoom(ZOOM_TYPE.ZOOM_ALL);
    }

    public void zoomIn() {
        zoom(ZOOM_TYPE.ZOOM_IN);
    }

    public void zoomOut() {
        zoom(ZOOM_TYPE.ZOOM_OUT);
    }

    public void zoomRubberband() {
        zoom(ZOOM_TYPE.ZOOM_RUBBERBAND);
    }
}