/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.ui;

import com.bhpb.geographics.model.CursorSettings;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geographics.model.window.WindowSyncBroadcaster;

/**
 * Encapsulates the creation of one of 6 different Builders, returning the
 * appropriate Composite GeoPanel for the specific Layer type and GeoDataOrder.
 * 
 * This allows the design logic for each kind of GeoPanel to be separated from the
 * relatively simply structure of the GeoPanel itself.
 * 
 * @author folsw9
 */
public class GeoPanelFactory {
    public GeoPanel createGeoPanel(int panelWidth, int panelHeight,
            int imageWidth, 
            int imageHeight, 
            Layer layer,
            WindowProperties winProps, 
            CursorSettings cursorSettings,
            WindowSyncBroadcaster winSyncBroadcaster,
            WindowModel winModel) {
        
        GeoPanelBuilder geoPanelBuilder = GeoPanelBuilder.getInstance(layer);

        return geoPanelBuilder.buildGeoPanel(
                panelWidth,
                panelHeight,
                imageWidth,
                imageHeight,
                layer,
                winProps,
                cursorSettings,
                winSyncBroadcaster,
                winModel);
    }
}