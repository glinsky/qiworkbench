/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import com.bhpb.geographics.model.Annotation;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geographics.rasterizer.AnnotationRasterizer;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.logging.Logger;
import javax.swing.JPanel;

/**
 * AnnotationLabel - allows rendering of only the visible portion of an Annotation
 * Image, using an AnnotationRenderer.
 * 
 */
public class AnnotationLabel extends JPanel {

    private static final long serialVersionUID = -5801534441319200461L;
    private static final transient Logger logger =
            Logger.getLogger(AnnotationLabel.class.toString());
    private AnnotationRasterizer rasterizer;
    private Dimension prevSize = null;
    private WindowProperties winProps;

    public AnnotationLabel(AnnotationRasterizer rasterizer, WindowProperties winProps, AbstractGeoPlot geoPlot) {
        this.rasterizer = rasterizer;
        this.winProps = winProps;
        updateSize(geoPlot.getPreferredSize());
    }

    /**
     * Updates this AnnotationLabel's preferredSize based on the GeoPlot size.
     *
     * @param plotSize the size of the GeoPlot component associated with this
     * AnnotationLabel
     */
    public void updateSize(Dimension plotSize) {
        Dimension annotationSize;
        Annotation annotation = rasterizer.getAnnotation();
        Annotation.AXIS axis = annotation.getAxis();

        if (axis == Annotation.AXIS.X_AXIS) {
            int height = rasterizer.getSize();
            annotationSize = new Dimension(plotSize.width, height);
        } else if (axis == Annotation.AXIS.Y_AXIS) {
            int width = rasterizer.getSize();
            annotationSize = new Dimension(width, plotSize.height);
        } else {
            logger.warning("Unknown axis: " + axis);
            return;
        }

        if (prevSize == null || prevSize.width != annotationSize.width || prevSize.height != annotationSize.height) {
            setPreferredSize(annotationSize);
            setMinimumSize(annotationSize);
            setMaximumSize(annotationSize);
            prevSize = annotationSize;
            if (isValid()) {
                revalidate();
            }
        }
    }

    @Override
    public void paint(Graphics g) {
        try {
            Rectangle annotationVisRect = getVisibleRect();
            if (annotationVisRect.width == 0 || annotationVisRect.height == 0) {
                return; // do nothing because there is no area to paint
            }

            if (!(g instanceof Graphics2D)) {
                logger.info("Graphics context not instanceof Graphics2D.");
                return;
            }
            Graphics2D g2d = (Graphics2D) g;
            rasterizer.rasterize(winProps, annotationVisRect, g2d);
        } catch (Exception ex) {
            logger.warning("Aborting annotationLabel.paint() - while rasterizing, caught: " + ex.getMessage());
        }
    }
}