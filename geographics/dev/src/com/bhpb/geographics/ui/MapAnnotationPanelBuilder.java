/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import com.bhpb.geographics.model.Annotation;
import com.bhpb.geographics.model.AnnotationFactory;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.window.WindowAnnotationProperties.LABEL_LOCATION;
import com.bhpb.geographics.model.window.MapAnnotationProperties;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geographics.rasterizer.AnnotationRasterizer;
import com.bhpb.geographics.rasterizer.RasterizerFactory;
import com.bhpb.geographics.ui.HeaderPanel.LAYOUT;
import com.bhpb.geographics.util.ScaleUtil;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * New implementation of AnnotationPanel.  Supports mutable WindowAnnotationProperties,
 * title String, label Strings on top, bottom, left, right.
 *
 * Unlike the original AnnotationPanel, it does NOT use the TraceAnnotationMap,
 * but relies on the new geoIO binary header entry functionality to get header
 * key and entry values for traces at annotation coordinates.
 *
 * @author folsw9
 */
public class MapAnnotationPanelBuilder extends JPanel {

    private static final Logger logger =
            Logger.getLogger(MapAnnotationPanelBuilder.class.toString());
    private static final int X_AXIS_ANNOTATION_HEIGHT = 20;
    private static final int Y_AXIS_ANNOTATION_WIDTH = 30;
    private static final ScaleUtil scaleUtil = new ScaleUtil();

    /**
     * Constructs an AnnotationPanel for the specified axis, using the
     * given DatasetProperties and WindowProperties.
     * @param axis the vertical or horizontal axis: Axis.Y_AXIS or Axis.X_AXIS
     * @param layer the Layer to be annotated, used to determine the initial HeaderPanel dimensions
     * @param winProps the WindowProperties associated with the annotated layer,
     * used for determining the names and order of annotations, as well as the
     * spacing of major and minor tick marks
     * 
     */
    public static HeaderPanel createMapAnnotationPanel(Annotation.AXIS axis, Layer layer, WindowProperties winProps, AbstractGeoPlot geoPlot) {
        int annotationSize;
        switch (axis) {
            case X_AXIS :
                annotationSize = X_AXIS_ANNOTATION_HEIGHT;
                break;
            case Y_AXIS :
                annotationSize = Y_AXIS_ANNOTATION_WIDTH;
                break;
            default :
                throw new RuntimeException("Cannot create annotation panel for unknown axis: " + axis);
        }
        int nAnnotations = 1;
        Annotation[] annotations = initAnnotations(layer, axis, annotationSize, winProps);
        AnnotationRasterizer[] annotationRasterizers = initRasterizers(
                layer,
                axis,
                annotations,
                nAnnotations,
                annotationSize);
        MapAnnotationProperties annotationProps = (MapAnnotationProperties) winProps.getAnnotationProperties();

        List<JComponent> headerCaptions = new ArrayList<JComponent>();
        List<AnnotationLabel> headerAnnotations = new ArrayList<AnnotationLabel>();

        HeaderPanel headerPanel;

        switch (axis) {
            case X_AXIS:
                //insert the GeoPanel title if the location is TOP
                if (annotationProps.getTitleLocation() == LABEL_LOCATION.TOP) {
                    headerCaptions.add(new HeaderLabel(annotationProps.getTitle()));
                }

                //insert the top label if it is set
                if ("".equals(annotationProps.getLabel(LABEL_LOCATION.TOP)) == false) {
                    headerCaptions.add(new HeaderLabel(annotationProps.getLabel(LABEL_LOCATION.TOP)));
                }

                for (int annIndex = 0; annIndex < nAnnotations; annIndex++) {
                    //maps have only 1 vertical and 1 horizontal annotation
                    //Image image = annotationRasterizers[annIndex].rasterize();
                    AnnotationLabel imageLabel = new AnnotationLabel(annotationRasterizers[annIndex], winProps, geoPlot);
                    imageLabel.setBackground(Color.WHITE);
                    headerAnnotations.add(imageLabel);
                }
                geoPlot.setAnnotationLabels(axis, headerAnnotations);
                headerPanel = new HeaderPanel(LAYOUT.VERTICAL, headerCaptions.size() + headerAnnotations.size(), Color.WHITE);

                break;
            case Y_AXIS:
                //insert the left label if it is set
                if ("".equals(annotationProps.getLabel(LABEL_LOCATION.LEFT)) == false) {
                    headerCaptions.add(new VerticalLabel(annotationProps.getLabel(LABEL_LOCATION.LEFT)));
                }

                for (int annIndex = 0; annIndex < nAnnotations; annIndex++) {
                    //maps have only 1 vertical and 1 horizontal annotation
                    //Image image = annotationRasterizers[annIndex].rasterize();
                    AnnotationLabel imageLabel = new AnnotationLabel(annotationRasterizers[annIndex], winProps, geoPlot);
                    imageLabel.setBackground(Color.WHITE);
                    headerAnnotations.add(imageLabel);
                }
                geoPlot.setAnnotationLabels(axis, headerAnnotations);
                headerPanel = new HeaderPanel(LAYOUT.HORIZONTAL, headerCaptions.size() + headerAnnotations.size(), Color.WHITE);

                break;
            default:
                throw new IllegalArgumentException("Unknown annotation axis: " + axis);
        }

        for (JComponent headerComp : headerCaptions) {
            headerPanel.add(headerComp, HeaderPanel.POSITION.CENTER);
        }
        for (JComponent headerComp : headerAnnotations) {
            headerPanel.add(headerComp, HeaderPanel.POSITION.LEADING);
        }

        return headerPanel;
    }

    private static Annotation[] initAnnotations(Layer layer, Annotation.AXIS axis, 
            int annotationSize, WindowProperties winProps) {
        Annotation[] annotations = new Annotation[1];
        ArrayList<String> headerKeys = layer.getDatasetProperties().getMetadata().getKeys();
        switch (axis) {
            case X_AXIS:
                    annotations[0] =
                            AnnotationFactory.createXaxisAnnotation(
                            //skip tracl
                            headerKeys.get(1),
                            annotationSize,
                            layer.getDataObjects(),
                            layer,
                            scaleUtil.getPixelsPerTrace(winProps.getScaleProperties().getHorizontalScale()));
                break;
            case Y_AXIS:
                    annotations[0] =
                            AnnotationFactory.createYaxisAnnotation(
                            //vertical annotation key is the last one
                            headerKeys.get(headerKeys.size()-1),
                            annotationSize,
                            scaleUtil.getPixelsPerTrace(winProps.getScaleProperties().getVerticalScale()),
                            layer.getDataObjects(),
                            layer);
                break;
            default:
                throw new UnsupportedOperationException("Cannot init rasterizer for unknown axis: " + axis);
        }
        return annotations;
    }

    /** 
     * Recreate the image annotations at the requested scale factor of the original size
     * This re-rasterizes the annotations without changing the caption font,
     * resulting in ticks at the same pixel coordinates as at scaleFactor 1.0,
     * but showing a larger or smaller number of ticks, on a wider or narrower image,
     * depending on whether scaleFactor is greater than or equal to one.
     * 
     */
    private static AnnotationRasterizer[] initRasterizers(Layer layer, Annotation.AXIS axis,
            Annotation[] annotations, int nAnnotations, int annotationSize) {
        AnnotationRasterizer[] annotationRasterizers;
        switch (axis) {
            case X_AXIS:
                logger.fine("Creating rasterizers for " + nAnnotations + " x-axis annotations");
                annotationRasterizers = new AnnotationRasterizer[nAnnotations];
                for (int annIndex = 0; annIndex < nAnnotations; annIndex++) {
                    annotationRasterizers[annIndex] =
                            RasterizerFactory.createAnnotationRasterizer(
                            annotations[annIndex],
                            annotationSize,
                            layer);
                }
                break;
            case Y_AXIS:
                logger.fine("Creating rasterizers for " + nAnnotations + " y-axis annotations");
                annotationRasterizers = new AnnotationRasterizer[nAnnotations];
                for (int annIndex = 0; annIndex < nAnnotations; annIndex++) {
                    annotationRasterizers[annIndex] =
                            RasterizerFactory.createAnnotationRasterizer(
                            annotations[annIndex],
                            annotationSize,
                            layer);
                }
                break;
            default:
                throw new UnsupportedOperationException("Cannot init rasterizer for unknown axis: " + axis);
        }
        return annotationRasterizers;
    }
}