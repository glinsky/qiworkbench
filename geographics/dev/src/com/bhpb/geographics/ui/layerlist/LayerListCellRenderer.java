/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui.layerlist;

import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.LayerDisplayParameters;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * <code>LayerListCellRenderer</code> paints the name and status of a Layer in a
 * JList.  Implementation borrowed from ListCellRenderer interface JavaDoc.
 *
 */
public class LayerListCellRenderer extends JLabel implements ListCellRenderer {

    /** The gray background of a hidden layer */
    private static final Color LIGHT_GRAY = new Color(195, 195, 195);
    /** The dark gray background of a selected hidden layer */
    private static final Color DARK_GRAY = new Color (160, 160, 160);
    /** The light blue background of a selected layer */
    private static final Color SELECTED_LAYER_BG = new Color(184, 207, 229);
    /** The default font size is 12 point */
    private static final int DEFAULT_FONT_SIZE = 12;
    /** The annotated layer font is 12 point Dialog Bold */
    private static final Font ANNOTATED_LAYER_FONT = new Font("Dialog", Font.BOLD, DEFAULT_FONT_SIZE);
    /** The selected layer font is 12 point Dialog Italic */
    private static final Font SELECTED_LAYER_FONT = new Font("Dialog", Font.ITALIC, DEFAULT_FONT_SIZE);
    /** The non-selected layer font is 12 point Dialog Plain */
    private static final Font NON_SELECTED_LAYER_FONT = new Font("Dialog", Font.PLAIN, DEFAULT_FONT_SIZE);

    /**
     * Creates a new instance of LayerListCellRenderer
     * @param listDnd JList to be rendered by this LayerListCellRenderer
     */
    public LayerListCellRenderer(JList listDnd) {
        setOpaque(true);
    }

    /**
     * Gets the Component (JLabel) which display the icon and layer name
     * for the cell containing the indicated Object.
     * 
     * @param list The JList to which the rendered Component belongs
     * @param value The Object being rendered
     * @param index The index of the Object beign rendered
     * @param isSelected true if and only if the rendered Object is selected
     * @param cellHasFocus true if and only if the rendered Cell currently has focus
     * 
     * @return Component displaying the Object at the given index, with italic font and 
     * shaded background if selected, or normal font and no background if not
     */
    public Component getListCellRendererComponent(JList list,
            Object value,
            int index,
            boolean isSelected,
            boolean cellHasFocus) {

        if (value instanceof Layer) {
            Layer asLayer = (Layer) value;
            setText(getLayerCellText(asLayer));
            setIcon(((Layer) value).getImageIcon());
                setBackground(isSelected, list.getBackground(), asLayer.isHidden());
                setFont(asLayer, isSelected);
            setForeground(getForeground(asLayer.getLayerProperties().getLayerDisplayParameters()));
            setToolTipText(((Layer)value).getLayerName());
        } else {
            setText(value.toString());
        }
        return this;
    }

    private Color getForeground(LayerDisplayParameters ldParams) {
        if (ldParams instanceof HorizonXsecDisplayParameters) {
            return ((HorizonXsecDisplayParameters) ldParams).getLineColor();
        } else {
            return Color.BLACK;
        }
    }

    private String getLayerCellText(Layer layer){
        return layer.getLayerName();
    }
    /**
     * Sets the JComponent foreground and backrounding basd on whether the layer is
     * hidden or visible and selected or not selected.
     * 
     * The color scheme is:
     * <ul>
     * <li>light gray on dark gray backgound - if the layer is seleceted and hidden</li>
     * <li>black on light blue background - if the layer is selected but not hidden</li>
     * <li>dark gray on light gray background - if the layer is not selected but is hidden</li>
     * <li>black on default background - if the layer is neither selected nor hidden</li>
     * </ul>
     * 
     * @param selected true if the layer is currently selected
     * @param defaultBackground the JList's default background color
     * @param hidden true if the layer is currently hidden
     */
    private void setBackground(boolean selected, Color defaultBackground, boolean hidden) {
        if (selected && hidden) {
            super.setBackground(DARK_GRAY);
            super.setForeground(LIGHT_GRAY);
        } else if (selected && !hidden) {
            super.setBackground(SELECTED_LAYER_BG);
            super.setForeground(Color.BLACK);
        } else if (!selected && hidden) {
            super.setBackground(LIGHT_GRAY);
            super.setForeground(DARK_GRAY);
        } else { //!selected && !hidden
            super.setBackground(defaultBackground);
            super.setForeground(Color.BLACK);
        }
    }
    
    private void setFont(Layer layer, boolean selected) {
        if (layer.isAnnotated()) {
            super.setFont(ANNOTATED_LAYER_FONT);
        } else if (selected) {
            super.setFont(SELECTED_LAYER_FONT);
        } else {
            super.setFont(NON_SELECTED_LAYER_FONT);
        }
    }
}