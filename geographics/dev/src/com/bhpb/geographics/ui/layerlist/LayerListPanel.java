/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui.layerlist;

import com.bhpb.geographics.LayerListMouseAdapter;
import com.bhpb.geographics.controller.LayerPropsEditorWorker;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.model.window.WindowModel.LAYER_TARGET;
import com.bhpb.geographics.ui.LayerPanel;
import com.bhpb.geographics.ui.layer.ListDnD;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * <code>LayerListView</code> displays the names of the layers in a Collection.
 * The user selects a Layer from this view to display in the asscociated
 * ViewerWindow's LayerPanel.
 *
 */
public class LayerListPanel extends JPanel {
    private static final Logger logger =
            Logger.getLogger(LayerListPanel.class.toString());
    
    private LayerPanel layerPanel;
    private LayerPropsEditorWorker propsEditorWorker = null;
    private ListDnD layerList = null; //JList subclass with drag-and-drop
    private WindowModel windowModel;

    private transient Thread propsEditorThread = null;

    /**
     * Constructs a LayerListPanel.
     * 
     * @param windowModel
     * @param layerPanel
     */
    public LayerListPanel(final WindowModel windowModel, LayerPanel layerPanel) {
        super();
        super.setName("layerListView");
        super.setLayout(new BorderLayout());

        this.windowModel = windowModel;
        this.layerPanel = layerPanel;

        layerList = new ListDnD(windowModel);
        layerList.setName("layerList");
        layerList.setCellRenderer(new LayerListCellRenderer(layerList));

        layerList.setBackground(Color.WHITE);
        layerList.setBorder(BorderFactory.createEtchedBorder());
        layerList.addMouseListener(new LayerListMouseAdapter(layerList, this));

        add(layerList, BorderLayout.CENTER);
        setBorder(BorderFactory.createTitledBorder("Layers"));

        if (windowModel.getSize() > 0) {
            select(0);
        }

        ListSelectionListener selectionListener = new ListSelectionListener() {

            public void valueChanged(ListSelectionEvent lse) {
                List<Layer> layers = new ArrayList<Layer>();

                ListDnD listDnD = (ListDnD) lse.getSource();
                
                for (int index = listDnD.getMinSelectionIndex(); index <= listDnD.getMaxSelectionIndex(); index++) {
                    //do not assume that the LSE indices are valid - check them
                    if (index < 0 || index >= layerList.getModel().getSize()) {
                        logger.warning("Unable to process index of ListSelectionEvent: invalid index: " + index);
                        continue;
                    }
                    Object elem = layerList.getModel().getElementAt(index);
                    if (elem instanceof Layer) {
                        layers.add((Layer) elem);
                    } else {
                        logger.warning("Cannot add: " + elem + " to list of selected layers, it is not a 'Layer'.");
                    }
                }
                windowModel.setSelectedLayers(layers);
            }
        };
        addSelectionListener(selectionListener);
    }

    /**
     * Selects a single layer by 0-based index.  If layerIndex is < 0 or >=
     * the number of layers in the LayerListPanel, then this method has no effect
     * beyond logging a warning.
     *
     * @param layerIndex 0-based index of the layer to select
     */
    public void select(int layerIndex) {
        if (layerIndex < 0 || layerIndex >= windowModel.getSize()) {
            logger.warning("Cannot select layer index " + layerIndex + ", select() has no effect");
            return;
        }
        layerList.setSelectedIndex(layerIndex);
    }

    public void show(Layer layer) {
        layer.setHidden(false);
        layerPanel.redraw(true, false);
    }

    /**
     * Adds a {@link ListSelectionListener} to this <code>LayerListView</code>.
     *
     * @param listener ListSelectionListener to register.  Multiple listeners may
     * be registered for the same LayerListView.
     */
    public void addSelectionListener(ListSelectionListener listener) {
        layerList.addListSelectionListener(listener);
    }

    public void hide(Layer layer) {
        layer.setHidden(true);
        layerPanel.redraw(true, false);
    }

    public void showPropertiesDialog(Layer layer) {
        if (propsEditorThread == null || propsEditorThread.isAlive() == false) {
            propsEditorWorker = new LayerPropsEditorWorker(
                    (JFrame) SwingUtilities.getRoot(this),
                    layer);
            propsEditorThread = new Thread(propsEditorWorker);
            propsEditorThread.start();
        } else {
            propsEditorWorker.bringDialogToFront();
        }
    }

    public void moveSelectedLayers(LAYER_TARGET target) {
        windowModel.moveLayers(target, layerList.getSelectedIndices());
    }
    
    
    public void remove(Layer layer) {
        windowModel.remove(layer);
    }
}