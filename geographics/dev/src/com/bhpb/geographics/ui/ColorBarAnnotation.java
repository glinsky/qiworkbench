/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.window.WindowAnnotationProperties.LABEL_LOCATION;
import com.bhpb.geographics.model.window.WindowModel;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.util.logging.Logger;
import javax.swing.JComponent;

public class ColorBarAnnotation extends JComponent {

    private LABEL_LOCATION location;
    private static final Logger logger =
            Logger.getLogger(ColorBarAnnotation.class.getName());
    private static final int DEFAULT_FONT_SIZE = 12;
    private static final Font DEFAULT_FONT = new Font("Times", Font.PLAIN, DEFAULT_FONT_SIZE);
    private Rectangle stringBounds;
    private String[] formattedValues = new String[5];
    private WindowModel windowModel;

    public ColorBarAnnotation(LABEL_LOCATION location, WindowModel windowModel) {
        this.location = location;
        this.windowModel = windowModel;
        initValues();
        setFont(DEFAULT_FONT);
        initSize();
    }

    public void initSize() {
        FontRenderContext frc = new FontRenderContext(new AffineTransform(), false, false);
        stringBounds = DEFAULT_FONT.getStringBounds(formattedValues[0], frc).getBounds();

        switch (location) {
            case LEFT:
            case RIGHT:
                setMinimumSize(new Dimension(stringBounds.width + 10, stringBounds.height * 5));
                break;
            case TOP:
            case BOTTOM:
                setMinimumSize(new Dimension(stringBounds.width * 5, stringBounds.height + 10));
                break;
            default:
                logger.warning("Unable to paint ColorBarAnnotation for location: " + location);
                return;
        }
    }

    public void initValues() {
        Layer annotatedLayer = windowModel.getAnnotatedLayer();

        double[] annotationValues = new double[5];
        annotationValues[0] = annotatedLayer.getAmplitudeMin();
        annotationValues[4] = annotatedLayer.getAmplitudeMax();

        if (annotationValues[0] * annotationValues[4] < 0) {
            annotationValues[2] = 0.0;
        } else {
            annotationValues[2] = (annotationValues[0] + annotationValues[4]) / 2.0;
        }

        annotationValues[1] = (annotationValues[2] + annotationValues[0]) / 2.0;
        annotationValues[3] = (annotationValues[4] + annotationValues[2]) / 2.0;

        for (int i = 0; i < 5; i++) {
            if (Double.isNaN(annotationValues[i])) {
                formattedValues[i] = "NaN";
            } else {
                formattedValues[i] = Double.toString(annotationValues[i]);
            }
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        Rectangle compBounds = getBounds();
        int xOffset;
        int yOffset;
        int xDelta;
        int yDelta;

        switch (location) {
            case BOTTOM:
            case TOP:
                xOffset = /*(stringBounds.width / 2*/ 0;
                yOffset = compBounds.height;
                xDelta = (compBounds.width - stringBounds.width) / 4;
                yDelta = 0;
                break;
                //xOffset = /*stringBounds.width / 2*/ 0;
                //yOffset = stringBounds.height;
                //xDelta = (compBounds.width - stringBounds.width) / 4;
                //yDelta = 0;
                //break;
            case LEFT:
            case RIGHT:
                xOffset = 0;
                yOffset = stringBounds.height;
                xDelta = 0;
                yDelta = (compBounds.height - 2 * stringBounds.height) / 4;
                break;
            default:
                logger.warning("Cannot paint colorBarAnnotation for location: " + location);
                return;
        }

        for (int i = 0; i < 5; i++) {
            g.drawString(formattedValues[i],
                    xOffset + i * xDelta,
                    yOffset + i * yDelta);
        }
    }
}