/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class CaptionBorderPanel extends JPanel {

    /**
     * @deprecated Use the constructor which takes String[] as
     * returned by CrossSectionAnnotationProperties.getSelectedKeys();
     * @param dsProperties
     */
    public CaptionBorderPanel(DatasetProperties dsProperties) {
        int nHeaders = dsProperties.getMetadata().getNumKeys() - 1;

        String[] hAxisTitles = new String[nHeaders];
        for (int i = 0; i < nHeaders; i++) {
            hAxisTitles[i] = dsProperties.getMetadata().getKeys().get(i);
        }

        init(hAxisTitles);
    }

    public CaptionBorderPanel(String[] hAxisTitles) {
        init(hAxisTitles);
    }

    private void init(String[] hAxisTitles) {
        setBackground(Color.WHITE);

        if (hAxisTitles.length > 0) {
            setLayout(new GridLayout(hAxisTitles.length, 0, 0, 0));
            setPreferredSize(new Dimension(100, 20 * hAxisTitles.length));
            setMinimumSize(new Dimension(40, 20 * hAxisTitles.length));
            
            for (String annotationTitle : hAxisTitles) {
                add(new JLabel(annotationTitle));
            }
        }
    }
}