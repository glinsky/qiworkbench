/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.ui.window;

import com.bhpb.geographics.controller.window.AbstractAnnotationPropertiesController;
import com.bhpb.geographics.controller.window.Controllable;
import com.bhpb.geographics.util.SwingViewUtils;
import java.util.logging.Logger;
import javax.swing.JPanel;

/**
 *
 * @author  folsw9
 */
public class AbstractAnnotationPropertiesPanel<C extends AbstractAnnotationPropertiesController> extends javax.swing.JPanel 
    implements Controllable<C>
{
    private static final Logger logger =
            Logger.getLogger(AbstractAnnotationPropertiesPanel.class.toString());
    
    private AbstractAnnotationPropertiesController controller = null;
    //this flag prevents the setting the initial list from invoking the stateChanged behavior,
    //which would reset the selected layer to item #1, "No Annotated Layer"
    //there may be a better way to do this but this is caused by teh Matisse-created
    //statelistener being instantiated in the superclass constructor
    //before the layer name list can be set
    private boolean layerSelectionEnabled = false;
    private MiscAnnotationPropertiesPanel miscAnnotPanel= new MiscAnnotationPropertiesPanel();
    private SwingViewUtils utils = new SwingViewUtils(logger);
    
    /** Creates new form AnnotationPropertiesPanel */
    public AbstractAnnotationPropertiesPanel() {
        initComponents();
        addPanel("Misc.", miscAnnotPanel);
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        stepTypeButtonGroup = new javax.swing.ButtonGroup();
        annotatedLayerPanel = new javax.swing.JPanel();
        annotatedLayerLabel = new javax.swing.JLabel();
        annotatedLayerComboBox = new javax.swing.JComboBox();
        tabPanel = new javax.swing.JPanel();
        tabbedPane = new javax.swing.JTabbedPane();

        annotatedLayerPanel.setLayout(new java.awt.GridLayout(1, 2));

        annotatedLayerLabel.setText("Annotated Layer");
        annotatedLayerPanel.add(annotatedLayerLabel);

        annotatedLayerComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No Annotation" }));
        annotatedLayerComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                annotatedLayerComboBoxItemStateChanged(evt);
            }
        });
        annotatedLayerPanel.add(annotatedLayerComboBox);

        org.jdesktop.layout.GroupLayout tabPanelLayout = new org.jdesktop.layout.GroupLayout(tabPanel);
        tabPanel.setLayout(tabPanelLayout);
        tabPanelLayout.setHorizontalGroup(
            tabPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 632, Short.MAX_VALUE)
            .add(tabPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(tabbedPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 632, Short.MAX_VALUE))
        );
        tabPanelLayout.setVerticalGroup(
            tabPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 441, Short.MAX_VALUE)
            .add(tabPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(org.jdesktop.layout.GroupLayout.TRAILING, tabbedPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 441, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(annotatedLayerPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 632, Short.MAX_VALUE)
            .add(tabPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(annotatedLayerPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(tabPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

private void annotatedLayerComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_annotatedLayerComboBoxItemStateChanged
    if (layerSelectionEnabled) {
        controller.setAnnotatedLayer((String)evt.getItem());
    }
}//GEN-LAST:event_annotatedLayerComboBoxItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox annotatedLayerComboBox;
    private javax.swing.JLabel annotatedLayerLabel;
    private javax.swing.JPanel annotatedLayerPanel;
    private javax.swing.ButtonGroup stepTypeButtonGroup;
    private javax.swing.JPanel tabPanel;
    private javax.swing.JTabbedPane tabbedPane;
    // End of variables declaration//GEN-END:variables

    /**
     * Adds the panel to this AbstractAnnotationPropertiesPanels' tabbedPane,
     * setting the title of the associated tab as requested.
     * 
     * @param title The title which will appear in the JTabbedPane.
     * @param panel The JPanel will be appear when the associated tab is clicked.
     */
    public void addPanel(String title, JPanel panel) {
        int nPanels = tabbedPane.getTabCount();
        tabbedPane.add(panel);
        tabbedPane.setTitleAt(nPanels, title);
    }
    
    /**
     * Gets miscellaneous annotations sub-panel.
     * 
     * @return the MiscPropertiesPanel of this MapAnnotationPropertiesPanel.
     */

    public MiscAnnotationPropertiesPanel getMiscPanel() {
        return miscAnnotPanel;
    }
    
    public void setAnnotatedLayer(String layerName) {
        utils.setComboBoxValue(annotatedLayerComboBox, layerName, "Annotated Layer");
    }
    
    public void setAnnotatedLayerNames(String[] layerNames) {
        utils.setComboBoxData(annotatedLayerComboBox, layerNames);
    }
    
    /**
     * Assigns a controller to this AbstractAnnotationPropertiesPanel and its
     * enclosed MiscAnnotationPropertiesPanel.
     * 
     * @param controller an instance of an AbstractAnnotationPropertiesController
     * concrete subclass.
     */
    public void setController(C controller) {
        this.controller = controller;
        miscAnnotPanel.setController(controller);
    }
    
    public void setLayerSelectionEnabled(boolean enabled) {
        this.layerSelectionEnabled = enabled;
    }
}