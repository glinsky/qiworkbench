/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.geographics.ui.window;

import com.bhpb.geographics.controller.window.Controllable;
import com.bhpb.geographics.controller.window.CrossSectionAnnotationPropertiesController;

public class CrossSectionAnnotationPropertiesPanel extends AbstractAnnotationPropertiesPanel<CrossSectionAnnotationPropertiesController> 
        implements Controllable<CrossSectionAnnotationPropertiesController> {
    private HorizontalAnnotationPropertiesPanel hAnnotPanel = new HorizontalAnnotationPropertiesPanel();
    private VerticalAnnotationPropertiesPanel vAnnotPanel = new VerticalAnnotationPropertiesPanel();
    
    /**
     * Construct an AbstractAnnotationPropertiesPanel with sub-panels customized for CrossSection ViewerWindows.
     */
    public CrossSectionAnnotationPropertiesPanel() {
        super();
        
        super.addPanel("Horizontal", hAnnotPanel);
        super.addPanel("Vertical", vAnnotPanel);
    }
    
    /**
     * Get this CrossSectionAnnotationPropertiesPanel's horizontal annotations sub-panel.
     * 
     * @return the HorizontalAnnotationPropertiesPanel of this CrossSectionAnnotationProeprtiesPanel.
     */
    public HorizontalAnnotationPropertiesPanel getHorizontalAnnotationPropertiesPanel() {
        return hAnnotPanel;
    }
    
    /**
     *  Get this CrossSectionAnnotationPropertiesPanel's vertical annotations sub-panel.
     * 
     * @return the VerticalAnnotationPropertiesPanel of this CrossSectionAnnotationProeprtiesPanel.
     */
    public VerticalAnnotationPropertiesPanel getVerticalAnnotationPropertiesPanel() {
        return vAnnotPanel;
    }
    
    @Override
    public void setController(CrossSectionAnnotationPropertiesController controller) {
        super.setController(controller);
        hAnnotPanel.setController(controller);
        vAnnotPanel.setController(controller);
    }
}