/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui.window;

import com.bhpb.geographics.controller.window.ScalePropertiesController;
import com.bhpb.geographics.ui.ValidatingPanel;
import com.bhpb.geographics.ui.ValidatingPanelAdapter;
import com.bhpb.geographics.util.DecimalFormatter;

public class ScalePropertiesPanel extends javax.swing.JPanel implements ValidatingPanel {

    private boolean focusListenersEnabled = true;
    private DecimalFormatter formatter = DecimalFormatter.getInstance(3);
    private ScalePropertiesController controller;

    /** Creates new form ScalePropertiesPanel */
    public ScalePropertiesPanel() {
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        gridPanel = new javax.swing.JPanel();
        hScaleLabel = new javax.swing.JLabel();
        hScaleTextField = new javax.swing.JTextField();
        vScaleLabel = new javax.swing.JLabel();
        vScaleTextField = new javax.swing.JTextField();
        lockAspectRatioCheckBox = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();

        gridPanel.setPreferredSize(new java.awt.Dimension(372, 69));
        gridPanel.setLayout(new java.awt.GridLayout(3, 2));

        hScaleLabel.setText("Horizontal Scale (Traces/CM): ");
        gridPanel.add(hScaleLabel);

        hScaleTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hScaleTextFieldActionPerformed(evt);
            }
        });
        hScaleTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                hScaleTextFieldFocusLost(evt);
            }
        });
        gridPanel.add(hScaleTextField);

        vScaleLabel.setText("Vertical Scale (CMs/Unit): ");
        gridPanel.add(vScaleLabel);

        vScaleTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vScaleTextFieldActionPerformed(evt);
            }
        });
        vScaleTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                vScaleTextFieldFocusLost(evt);
            }
        });
        gridPanel.add(vScaleTextField);

        lockAspectRatioCheckBox.setText("Lock Aspect Ratio");
        lockAspectRatioCheckBox.setEnabled(false);
        gridPanel.add(lockAspectRatioCheckBox);

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 17, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 20, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 20, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 19, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(18, 18, 18)
                .add(gridPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(gridPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(55, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

private void hScaleTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hScaleTextFieldActionPerformed
    controller.setHscale(hScaleTextField.getText());
    setHscale(controller.getHscale());
}//GEN-LAST:event_hScaleTextFieldActionPerformed

private void hScaleTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_hScaleTextFieldFocusLost
    if (focusListenersEnabled) {
        controller.setHscale(hScaleTextField.getText());
        setHscale(controller.getHscale());
    }
}//GEN-LAST:event_hScaleTextFieldFocusLost

private void vScaleTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_vScaleTextFieldFocusLost
    if (focusListenersEnabled) {
        controller.setVscale(vScaleTextField.getText());
        setVscale(controller.getVscale());
    }
}//GEN-LAST:event_vScaleTextFieldFocusLost

private void vScaleTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vScaleTextFieldActionPerformed
    controller.setVscale(vScaleTextField.getText());
    setVscale(controller.getVscale());
}//GEN-LAST:event_vScaleTextFieldActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel gridPanel;
    private javax.swing.JLabel hScaleLabel;
    private javax.swing.JTextField hScaleTextField;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JCheckBox lockAspectRatioCheckBox;
    private javax.swing.JLabel vScaleLabel;
    private javax.swing.JTextField vScaleTextField;
    // End of variables declaration//GEN-END:variables

    public void setController(ScalePropertiesController controller) {
        this.controller = controller;
    }

    public void setHscale(double hScale) {
        hScaleTextField.setText(formatter.getFormattedString(hScale, 3, true));
    }

    public void setHscaleUnits(String units) {
        hScaleLabel.setText("Horizontal Scale (" + units + ")");
    }

    public void setVscaleUnits(String units) {
        vScaleLabel.setText("Vertical Scale (" + units + ")");
    }

    public void setVscale(double vScale) {
        vScaleTextField.setText(formatter.getFormattedString(vScale, 3, true));
    }

    public void showValidationFailure(String value) {
        focusListenersEnabled = false;
        ValidatingPanelAdapter.showValidationFailure(value, this);
        focusListenersEnabled = true;
    }
}