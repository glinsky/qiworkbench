/*
###########################################################################
# This program module Copyright (C) 2009 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui.window;

import com.bhpb.geographics.controller.window.AnnotationPropertiesController;
import com.bhpb.geographics.model.window.AnnotationProperties.LABEL_FLAG;
import com.bhpb.geographics.ui.ValidatingPanel;
import com.bhpb.geographics.ui.ValidatingPanelAdapter;
import com.bhpb.geographics.util.SwingViewUtils;
import java.text.DecimalFormat;
import java.util.EnumMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

public class AnnotationPropertiesPanel extends javax.swing.JPanel implements ValidatingPanel {

    private static final Logger logger =
            Logger.getLogger(AnnotationPropertiesPanel.class.getName());
    private AnnotationPropertiesController controller;
    private boolean focusListenersEnabled = true;
    private DecimalFormat format = new DecimalFormat("0.00");
    private Map<LABEL_FLAG, String> labelFlagDescriptionMap = new EnumMap<LABEL_FLAG, String>(LABEL_FLAG.class);
    private SwingViewUtils utils = new SwingViewUtils(logger);

    private static final String ALL_STRING = "Show All";
    private static final String GE_LIMIT_STRING = ">= Limit:";
    private static final String LE_LIMIT_STRING = "<= Limit:";
    private static final String[] LABEL_FLAG_STRINGS = { ALL_STRING, GE_LIMIT_STRING, LE_LIMIT_STRING };

    /** Creates new form AnnotationPropertiesPanel */
    public AnnotationPropertiesPanel() {
        initComponents();

        //if the model for the ComboBox is changed in Matisse, these Strings must be changed as well
        labelFlagDescriptionMap.put(LABEL_FLAG.ALL, ALL_STRING);
        labelFlagDescriptionMap.put(LABEL_FLAG.GE_LIMIT, GE_LIMIT_STRING);
        labelFlagDescriptionMap.put(LABEL_FLAG.LE_LIMIT, LE_LIMIT_STRING);

        utils.setComboBoxData(flagComboBox, LABEL_FLAG_STRINGS);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        gridPanel = new javax.swing.JPanel();
        stepLabel = new javax.swing.JLabel();
        stepTextField = new javax.swing.JTextField();
        angleLabel = new javax.swing.JLabel();
        angleTextField = new javax.swing.JTextField();
        minSpaceLabel = new javax.swing.JLabel();
        minSpaceTextField = new javax.swing.JTextField();
        flagLabel = new javax.swing.JLabel();
        flagComboBox = new javax.swing.JComboBox();
        limitLabel = new javax.swing.JLabel();
        limitTextField = new javax.swing.JTextField();
        showLinesCheckBox = new javax.swing.JCheckBox();

        gridPanel.setLayout(new java.awt.GridLayout(5, 2));

        stepLabel.setText("Label Step (in traces)");
        gridPanel.add(stepLabel);

        stepTextField.setText("10.0");
        stepTextField.setEnabled(false);
        gridPanel.add(stepTextField);

        angleLabel.setText("Label Angle");
        gridPanel.add(angleLabel);

        angleTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                angleTextFieldActionPerformed(evt);
            }
        });
        angleTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                angleTextFieldFocusLost(evt);
            }
        });
        gridPanel.add(angleTextField);

        minSpaceLabel.setText("Min Label Space (in CM)");
        gridPanel.add(minSpaceLabel);

        minSpaceTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                minSpaceTextFieldActionPerformed(evt);
            }
        });
        minSpaceTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                minSpaceTextFieldFocusLost(evt);
            }
        });
        gridPanel.add(minSpaceTextField);

        flagLabel.setText("Label Flag");
        gridPanel.add(flagLabel);

        flagComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Show All", ">= Limit:", "<= Limit:" }));
        flagComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                flagComboBoxItemStateChanged(evt);
            }
        });
        gridPanel.add(flagComboBox);

        limitLabel.setText("Label Limit");
        gridPanel.add(limitLabel);

        limitTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                limitTextFieldActionPerformed(evt);
            }
        });
        limitTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                limitTextFieldFocusLost(evt);
            }
        });
        gridPanel.add(limitTextField);

        showLinesCheckBox.setText("Show lines");
        showLinesCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showLinesCheckBoxActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(137, 137, 137)
                .add(gridPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(117, Short.MAX_VALUE))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap(237, Short.MAX_VALUE)
                .add(showLinesCheckBox)
                .add(218, 218, 218))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(gridPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(showLinesCheckBox)
                .addContainerGap(127, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void showLinesCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showLinesCheckBoxActionPerformed
        controller.setShowLines(showLinesCheckBox.isSelected());
    }//GEN-LAST:event_showLinesCheckBoxActionPerformed

    private void flagComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_flagComboBoxItemStateChanged
        Object selectedItem = flagComboBox.getSelectedItem();
        //must check for controller == null because this is invoked when the constructor
        //changes the combobox model
        if (selectedItem == null || !(selectedItem instanceof String) || controller == null) {
            return;
        }
        String selectedString = (String) selectedItem;
        for (Entry<LABEL_FLAG, String> entry : labelFlagDescriptionMap.entrySet()) {
            if (selectedString.equals(entry.getValue())) {
                controller.setLabelFlag(entry.getKey());
                return;
            }
        }
        logger.warning("Unable to set flag for unknown LABEL_FLAG description: " + selectedString);
    }//GEN-LAST:event_flagComboBoxItemStateChanged

    private void angleTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_angleTextFieldActionPerformed
        controller.setAngle(angleTextField.getText());
    }//GEN-LAST:event_angleTextFieldActionPerformed

    private void minSpaceTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_minSpaceTextFieldActionPerformed
        controller.setMinLabelSpace(minSpaceTextField.getText());
    }//GEN-LAST:event_minSpaceTextFieldActionPerformed

    private void limitTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limitTextFieldActionPerformed
        controller.setLabelLimit(limitTextField.getText());
    }//GEN-LAST:event_limitTextFieldActionPerformed

    private void angleTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_angleTextFieldFocusLost
        if (focusListenersEnabled) {
            controller.setAngle(angleTextField.getText());
        }
    }//GEN-LAST:event_angleTextFieldFocusLost

    private void minSpaceTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_minSpaceTextFieldFocusLost
        if (focusListenersEnabled) {
            controller.setMinLabelSpace(minSpaceTextField.getText());
        }
    }//GEN-LAST:event_minSpaceTextFieldFocusLost

    private void limitTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_limitTextFieldFocusLost
        if (focusListenersEnabled) {
            controller.setLabelLimit(limitTextField.getText());
        }
    }//GEN-LAST:event_limitTextFieldFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel angleLabel;
    private javax.swing.JTextField angleTextField;
    private javax.swing.JComboBox flagComboBox;
    private javax.swing.JLabel flagLabel;
    private javax.swing.JPanel gridPanel;
    private javax.swing.JLabel limitLabel;
    private javax.swing.JTextField limitTextField;
    private javax.swing.JLabel minSpaceLabel;
    private javax.swing.JTextField minSpaceTextField;
    private javax.swing.JCheckBox showLinesCheckBox;
    private javax.swing.JLabel stepLabel;
    private javax.swing.JTextField stepTextField;
    // End of variables declaration//GEN-END:variables

    public void setController(AnnotationPropertiesController controller) {
        this.controller = controller;
    }

    public void setAngle(double angle) {
        angleTextField.setText(format.format(angle));
    }

    public void setMinLabelSpace(double minSpace) {
        minSpaceTextField.setText(format.format(minSpace));
    }

    public void setLabelFlag(LABEL_FLAG labelFlag) {
        flagComboBox.setSelectedItem(labelFlagDescriptionMap.get(labelFlag));
    }

    public void setLabelLimit(double limit) {
        limitTextField.setText(format.format(limit));
    }

    public void setShowLines(boolean showLines) {
        showLinesCheckBox.setSelected(showLines);
    }

    public void showValidationFailure(String value) {
        focusListenersEnabled = false;
        ValidatingPanelAdapter.showValidationFailure(value, this);
        focusListenersEnabled = true;
    }
}