/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import com.bhpb.geographics.model.window.WindowAnnotationProperties.LABEL_LOCATION;
import com.bhpb.geographics.model.window.WindowModel;
import java.awt.Color;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.jdesktop.layout.GroupLayout;

public class ColorBarPanel extends JPanel {

    private static final Color BG_COLOR = Color.WHITE;
    private static final String COLORBAR_CAPTION_TEXT = "Color Interpolation";
    private LABEL_LOCATION location;

    public ColorBarPanel(LABEL_LOCATION location, WindowModel windowModel) {
        this.location = location;
        setBackground(BG_COLOR);
        initComponents(windowModel);
    }

    private JComponent createAnnotationSubPanel(WindowModel windowModel) {
        return new ColorBarAnnotation(location, windowModel);
    }

    private JComponent createBarSubPanel(WindowModel windowModel) {
        if (isHorizontalLayout()) {
            return new ColorBar(ColorBar.ORIENTATION.HORIZONTAL, windowModel);
        } else {
            return new ColorBar(ColorBar.ORIENTATION.VERTICAL, windowModel);
        }
    }

    private JPanel createCaptionSubPanel(String captionText) {
        JPanel captionSubPanel = new JPanel();

        captionSubPanel.setBackground(BG_COLOR);
        if (isHorizontalLayout()) {
            captionSubPanel.setLayout(new BoxLayout(captionSubPanel, BoxLayout.X_AXIS));
            captionSubPanel.add(Box.createHorizontalGlue());
            captionSubPanel.add(new JLabel(captionText));
            captionSubPanel.add(Box.createHorizontalGlue());
        } else {
            double rotation;
            if (location == LABEL_LOCATION.RIGHT) {
                rotation = 90.0;
            } else {
                rotation = 270.0;
            }
            captionSubPanel.setLayout(new BoxLayout(captionSubPanel, BoxLayout.Y_AXIS));
            captionSubPanel.add(Box.createVerticalGlue());
            captionSubPanel.add(new RotatedLabel(captionText, rotation));
            captionSubPanel.add(Box.createVerticalGlue());
        }
        return captionSubPanel;
    }

    private void initComponents(WindowModel windowModel) {
        if (location == LABEL_LOCATION.NONE) {
            throw new IllegalArgumentException("Unable to instantiate ColorBarPanel with location: " + LABEL_LOCATION.NONE);
        }

        //setLayout(new BorderLayout());
        JComponent cP = createCaptionSubPanel(COLORBAR_CAPTION_TEXT);
        JComponent aP = createAnnotationSubPanel(windowModel);
        JComponent bP = createBarSubPanel(windowModel);

        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);

        /*
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        */
        switch (location) {
            case LEFT :
                layout.setHorizontalGroup(
                        layout.createSequentialGroup()
                        .add(cP)
                        .add(aP)
                        .add(bP)
                        );
                layout.setVerticalGroup(
                        layout.createParallelGroup()
                        .add(cP)
                        .add(aP)
                        .add(bP)
                        );
                break;
            case TOP :
                layout.setHorizontalGroup(
                        layout.createParallelGroup()
                        .add(cP)
                        .add(aP)
                        .add(bP)
                        );
                layout.setVerticalGroup(
                        layout.createSequentialGroup()
                        .add(cP)
                        .add(aP)
                        .add(bP)
                        );
                break;
            case RIGHT :
                layout.setHorizontalGroup(
                        layout.createSequentialGroup()
                        .add(bP)
                        .add(aP)
                        .add(cP)
                        );
                layout.setVerticalGroup(
                        layout.createParallelGroup()
                        .add(bP)
                        .add(aP)
                        .add(cP)
                        );
                break;
            case BOTTOM :
                layout.setHorizontalGroup(
                        layout.createParallelGroup()
                        .add(bP)
                        .add(aP)
                        .add(cP)
                        );
                layout.setVerticalGroup(
                        layout.createSequentialGroup()
                        .add(bP)
                        .add(aP)
                        .add(cP)
                        );
                break;
            default :
                throw new IllegalArgumentException(location.toString());
        }

        //layout.linkSize(new JComponent[] {cP,bP},GroupLayout.HORIZONTAL);
        //layout.linkSize(new JComponent[] {cP,bP},GroupLayout.VERTICAL);
    }

    private boolean isHorizontalLayout() {
        switch (location) {
            case LEFT:
            case RIGHT:
                return false;
            case BOTTOM:
            case NONE:
            case TOP:
            default:
                return true;
        }
    }
}