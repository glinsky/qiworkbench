/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import com.bhpb.geographics.model.CursorSettings;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geographics.model.window.WindowSyncBroadcaster;
import com.bhpb.geographics.rasterizer.LayeredRenderableImageProducer;
import com.bhpb.geographics.ui.event.GeoPlotEvent;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.logging.Logger;

/**
 * Subclass of AbstractGeoPlot which polls a Collection of SparseBufferedImages every
 * 200ms and combines the Images according to the zOrders of the associated Layers when
 * new Images are available.  This class's paintComponent(Graphics) method does not initiate rasterization,
 * allowing scrolling and resizing to function at a rate indepenent of long rasterization times.
 *
 */
public class NonBlockingGeoPlot extends AbstractGeoPlot {

    private static final transient Logger logger =
            Logger.getLogger(NonBlockingGeoPlot.class.toString());
    protected Thread imgProducerMgrThread;

    /**
     * Constructor for geoPlots which use a configurable verticale step size: CrossSectionImage
     * @param isDoubleBuffered
     * @param width
     * @param height
     * @param ldParams
     */
    NonBlockingGeoPlot(boolean isDoubleBuffered, int width, int height,
            WindowProperties winProps, CursorSettings cursorSettings,
            WindowSyncBroadcaster windowSyncBroadcaster, WindowModel windowModel) {
        super(isDoubleBuffered, width, height, winProps, cursorSettings, windowSyncBroadcaster, windowModel);

        imgProducerMgrThread = new Thread(new ImageProducerManager(this), "ImageProducerManager-winID-" + windowSyncBroadcaster.getWinID());
        imgProducerMgrThread.start();
    }

    @Override
    public void paintComponent(final Graphics g) {
        super.paintComponent(g);

        //Always start rerasterization if the visible rectangle has changed
        //then if the current composite image does not fill the visible rectangle,
        //return and wait for rasterization completion to cause redraw
        //No need to actually redraw the old composite image as the visible portion, if any,
        //is still shown.
        Rectangle newVisRect = getVisibleRect();

        if (lastVisibleRect.x != newVisRect.x ||
                lastVisibleRect.y != newVisRect.y ||
                lastVisibleRect.width != newVisRect.width ||
                lastVisibleRect.height != newVisRect.height) {

            new Thread(new Runnable() {
                //Redraw due to new visible rectangle does not interrupt rasterization in progress.
                //It may or may not be more efficient to do so.

                public void run() {
                    redraw(false, false);
                }
            }).start();

            lastVisibleRect = newVisRect;
        }

        if (g instanceof Graphics2D) {
            Graphics2D g2d = (Graphics2D) g;
            if (redraw) {
                compositeOriginX = 0;
                compositeOriginY = 0;
                compositeImageWidth = 0;
                compositeImageHeight = 0;

                boolean completedImageFound = false;
                Iterator<Layer> iter = windowModel.iterator();
                boolean allLayersHidden = true;
                while (iter.hasNext()) {
                    Layer layer = iter.next();
                    if (!layer.isHidden()) {
                        allLayersHidden = false;
                        SparseBufferedImage img = (SparseBufferedImage) bufferedImages.get(layer.getZorder());
                        if (img == null) {
                            continue;
                        }
                        //the composite image's origin and dimensions will be exactly sufficient to enclose
                        //the origin and dimensions of all layers
                        compositeOriginX = Math.max(compositeOriginX, img.getOffsetX());
                        compositeOriginY = Math.max(compositeOriginY, img.getOffsetY());
                        compositeImageWidth = Math.max(compositeImageWidth, img.getWidth() - compositeOriginX + img.getOffsetX());
                        compositeImageHeight = Math.max(compositeImageHeight, img.getHeight() - compositeOriginY + img.getOffsetY());
                        completedImageFound = true;
                    }
                }

                //If no completed image is found, the first rasterization has not yet completed,
                if (!completedImageFound) {
                    //If all layers have been hidden or removed, reset the plot image to the background color,
                    //otherwise, simply return with no effect.
                    if (allLayersHidden || windowModel.getSize() == 0) {
                        resetPlot();
                        redraw = false;
                    }
                    return;
                }
                compositeImage = new SparseBufferedImage(compositeImageWidth, compositeImageHeight, BufferedImage.TYPE_INT_ARGB, compositeOriginX, compositeOriginY);
                Graphics2D compositeContext = (Graphics2D) compositeImage.getGraphics();
                for (int zOrder = 0; zOrder < windowModel.getSize(); zOrder++) {
                    Layer layer = windowModel.getLayer(zOrder);
                    if (layer == null) {
                        logger.info("windowModel.getSize() == " + windowModel.getSize() + " but getLayer(" + zOrder + ") returned null.");
                        continue;
                    }
                    if (layer.isHidden()) {
                        logger.info("Skipping rasterization of hidden layer: " + layer.getLayerName());
                    } else {
                        SparseBufferedImage bufferedImage = bufferedImages.get(layer.getZorder());

                        if (bufferedImage != null) {
                            int imgOriginX = ((SparseBufferedImage) bufferedImage).getOffsetX();
                            int imgOriginY = ((SparseBufferedImage) bufferedImage).getOffsetY();

                            float opacity = (float) layer.getDisplayParameters().getBasicLayerProperties().getOpacity();
                            AlphaComposite alphaComposite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, opacity);
                            compositeContext.setComposite(alphaComposite);

                            compositeContext.drawImage(bufferedImage,
                                    imgOriginX - compositeOriginX, imgOriginY - compositeOriginY, null);
                        }
                    }
                }
                newCompositeImageCreated = true;
                redraw = false;
            }
            if (compositeOriginX != Integer.MAX_VALUE && compositeOriginY != Integer.MAX_VALUE && compositeImage != null) {
                g2d.drawImage(compositeImage, compositeOriginX, compositeOriginY, null);

                if (newCompositeImageCreated) {
                    broadcastEvent(GeoPlotEvent.RENDERING_COMPLETE);
                    newCompositeImageCreated = false;
                }

                //bugfix: after drawing, set the cursor as appropriate based on current rendering status
                //bugfix #2: only change the cursor (either way) if this plot's ViewerWindow is selected
                if (mouseInsideGeoPlot) {
                    if (isRendering()) {
                        cursorPainter.setWaitCursor();
                    } else {
                        cursorPainter.setNormalCursor();
                    }
                }

                //check for activeLayer == null; there may be no active Layer
                Layer activeLayer = windowModel.getActiveLayer();
                if (activeLayer != null && activeLayer.hasArbitraryTraverse()) {
                    paintArbTrav(g2d, activeLayer.getArbitraryTraverse(),
                            Color.RED,
                            Color.BLACK,
                            Color.RED);
                }

                cursorPainter.paintCursor(g2d,
                        windowModel.getAnnotatedLayer(),
                        getActionMode(),
                        isZooming(),
                        zoomStart,
                        getViewportBounds());
            }
        } else {
            throw new UnsupportedOperationException("Cannot paint Component - Graphics context is not Graphics2D");
        }
    }

    /**
     * Polls currentRasterizer and nextRasterizer every 200ms.  If the former
     * is null or dead and the latter is non-null, currentRasterizer is set to
     * nextRasterizer and started on another Thread.  This allows the latest redraw()
     * request to be processed as soon as the current rasterization is completed.
     */
    private class ImageProducerManager implements Runnable {

        final private AbstractGeoPlot geoPlot;

        public ImageProducerManager(AbstractGeoPlot geoPlot) {
            this.geoPlot = geoPlot;
        }

        public void run() {
            try {
                while (!shutdown) {
                    synchronized (geoPlot) {
                        //Synchronized and waiting on producers lock
                        //so that this manager is awakened when a new producer is
                        //added or a redraw is requested, otherwise it waits for 1/5 second at a time
                        for (LayeredRenderableImageProducer producer : producers) {
                            Thread currentRasterizerThread = currentRasterizer.get(producer);
                            if (currentRasterizerThread != null && currentRasterizerThread.isAlive()) {
                                //do nothing, producer's currentRasterizer is working
                            } else {
                                Thread nextRasterizerThread = nextRasterizer.get(producer);
                                if (nextRasterizerThread != null) {
                                    //the 'next' rasterizer Thread is now the 'current' Thread
                                    //so the 'next' rasterizer Thread ref should be set to null
                                    //logger.fine("Found non-null nextRasterizer for layer: " + producerLayerMap.get(producer).getLayerName());
                                    nextRasterizer.put(producer, null);
                                    currentRasterizer.put(producer, nextRasterizerThread);
                                    nextRasterizerThread.start();
                                }
                            }
                        }
                        try {
                            //yield causes java to overutilize cpu power
                            geoPlot.wait(200);
                        } catch (InterruptedException ie) {
                            logger.warning("ImageProducerManager INTERRUPTED: " + ie.getMessage());
                        }
                    }
                }
            } catch (Exception ex) {
                logger.warning("ImageProducerManager SHUTDOWN due to Exception: " + ex.getMessage());
            }
        }
    }
}