/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

/**
 * JPanel specialized for GridLayout, intended for use as JScrollPane column header
 * (LAYOUT.VERTICAL) or row header (LAYOUT.HORIZONTAL) component.
 *  
 * Added components may be individually
 * positioned to LEADING (left/bottom), CENTER or TRAILING (right/top).
 *
 */
public class HeaderPanel extends JPanel {
    public enum LAYOUT { VERTICAL, HORIZONTAL};
    public enum POSITION {LEADING, CENTER, TRAILING}
    
    private final LAYOUT layout;
    private List<AnnotationLabel> annotationLabels = new ArrayList<AnnotationLabel>();
    /**
     * Creates a HeaderPanel with single row or column in the requested orientation,
     * which columns or rows for the requested number of child Components.
     * 
     * @param layout if VERTICAL, the GridLayout has a single column,
     *  if HORIZONTAL, a single row
     * @param nChildren if layout is VERTICAL, the number of rows, if HORIZONTAL,
     *  columns
     */
    public HeaderPanel(LAYOUT layout, int nChildren, Color bgColor) {
        this.layout = layout;
        setLayout(getHeaderPanelLayout(layout, nChildren));
        setBackground(bgColor);
    }
    
    private GridLayout getHeaderPanelLayout(LAYOUT layout, int nChildren) {
        switch(layout) {
            case HORIZONTAL :
                return new GridLayout (1, nChildren, 0, 0);
            case VERTICAL :
                return new GridLayout (nChildren, 1, 0, 0);
            default:
                throw new UnsupportedOperationException("Unknown layout: " + layout);
        }
    }
    
    private BoxLayout getWrapperPanelLayout(JPanel wrapperPanel, LAYOUT layout) {
        switch(layout) {
            case HORIZONTAL :
                return new BoxLayout(wrapperPanel, BoxLayout.Y_AXIS);
            case VERTICAL :
                return new BoxLayout(wrapperPanel, BoxLayout.X_AXIS);
            default:
                throw new UnsupportedOperationException("Unknown layout: " + layout);
        }
    }
    
    private Component getWrapperPanelGlue(LAYOUT layout) {
         switch(layout) {
            case HORIZONTAL :
                return Box.createVerticalGlue();
            case VERTICAL :
                return Box.createHorizontalGlue();
            default:
                throw new UnsupportedOperationException("Unknown layout: " + layout);
        }
    }
    
    /**
     * 
     * @param c
     */
    public Component add(Component c, POSITION pos) {
        if (c instanceof AnnotationLabel) {
            annotationLabels.add((AnnotationLabel)c);
        }
        
        JPanel wrapperPanel = new JPanel();
        wrapperPanel.setBackground(getBackground());
        switch (pos) {
            case CENTER:
                wrapperPanel.setLayout(getWrapperPanelLayout(wrapperPanel, this.layout));
                wrapperPanel.add(getWrapperPanelGlue(this.layout));
                wrapperPanel.add(c);
                wrapperPanel.add(getWrapperPanelGlue(this.layout));
                return super.add(wrapperPanel);
            case LEADING:
                wrapperPanel.setLayout(getWrapperPanelLayout(wrapperPanel, this.layout));
                wrapperPanel.add(c);
                wrapperPanel.add(getWrapperPanelGlue(this.layout));
                return super.add(wrapperPanel);
            case TRAILING:
                wrapperPanel.setLayout(getWrapperPanelLayout(wrapperPanel, this.layout));
                wrapperPanel.add(getWrapperPanelGlue(this.layout));
                wrapperPanel.add(c);
                return super.add(wrapperPanel);
            default :
                throw new UnsupportedOperationException("Unknown position: " + pos);
        }
    }
    
    public void repaintAnnotationLabels() {
        for (AnnotationLabel annotationLabel : annotationLabels) {
            annotationLabel.repaint();
        }
    }
}