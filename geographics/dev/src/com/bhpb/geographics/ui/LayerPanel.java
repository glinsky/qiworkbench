/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import com.bhpb.geographics.model.CursorSettings;
import com.bhpb.geographics.model.HorizonHighlightSettings;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.ViewTarget;
import com.bhpb.geographics.model.window.WindowAnnotationProperties;
import com.bhpb.geographics.model.window.WindowAnnotationProperties.LABEL_LOCATION;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geographics.model.window.WindowScaleProperties;
import com.bhpb.geographics.model.window.WindowSyncBroadcaster;
import com.bhpb.geographics.ui.AbstractGeoPlot.ACTION_MODE;
import com.bhpb.geographics.ui.event.GeoPlotEventListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * LayerPanel contains a list of layers which may be combined into a composite
 * image of z-orderd, optionally transparant, cross section or map images.  LayerPanel
 * contains only a single Component, a GeoPanel, and should be merged with that class.
 */
public class LayerPanel extends JPanel implements ListDataListener {

    private static final long serialVersionUID = 166059365061915629L;
    public static final Logger logger = Logger.getLogger(LayerPanel.class.toString());
    private ColorBarPanel colorBarPanel = null;
    private CursorSettings cursorSettings;
    private GeoPanel geoPanel;
    private GeoPanelPlaceHolder placeHolder;
    private WindowModel windowModel;
    private WindowProperties winProps;
    private WindowSyncBroadcaster winSyncBroadcaster;

    /**
     * Creates a LayerPanel populated with a List of Layers.
     * 
     * @param layers List of layers available to be displayed.
     * @param winProps the window properties are shared by all layers associated
     * with this LayerPanel
     */
    public LayerPanel(List<Layer> layers,
            WindowProperties winProps,
            CursorSettings cursorSettings,
            WindowSyncBroadcaster winSyncBroadcaster,
            WindowModel windowModel) {
        super();
        this.winProps = winProps;
        this.cursorSettings = cursorSettings;
        this.winSyncBroadcaster = winSyncBroadcaster;
        this.windowModel = windowModel;

        setBackground(Color.WHITE);
        setLayout(new BorderLayout(0, 0));

        if (layers.size() > 0) {
            initGeoPanel();
            updateColorBar();
        } else {
            initPlaceHolder();
        }

        windowModel.addListDataListener(this);
    }

    public void addGeoPlotEventListener(GeoPlotEventListener listener) {
        if (geoPanel != null) {
            geoPanel.addGeoPlotEventListener(listener);
        } else if (placeHolder != null) {
            placeHolder.addGeoPlotEventListener(listener);
        } else {
            logger.warning("Unable to add GeoPlotEventListener: both geoPanel and PlaceHolder are null.");
        }
    }

    public ACTION_MODE getActionMode() {
        return geoPanel.getActionMode();
    }

    public boolean isRendering() {
        return geoPanel.isRendering();
    }

    /**
     * Redraw the composite image displayed in this LayerPanel's GeoPanel
     * 
     * @param doImmediately if true, any rasterization curently in progress is interrupted
     */
    public void redraw(boolean doImmediately, boolean updatePlotSize) {
        geoPanel.redraw(doImmediately, updatePlotSize);
    }

    /**
     * Uses the JPanel implementation to reposition the JScrollPane associated
     * with this ViewerWindow so that the requested depth is visible.  If possible given
     * the scale and dimensions of the GeoPlot, this point will be positioned at the
     * topmost row of pixels, but this is not guaranteed.
     * 
     * @param timeOrDepth time or depth value to be made visible
     */
    public void scrollToVisiblePoint(double timeOrDepth) throws IllegalArgumentException {
        geoPanel.scrollToVisiblePoint(timeOrDepth);
    }

    /**
     * Uses the JPanel implementation to reposition the JScrollPane associated
     * with this ViewerWindow so that the requested depth is visible.  If possible given
     * the scale and dimensions of the GeoPlot, this point will be positioned at the
     * topmost row of pixels, but this is not guaranteed.  If the WindowModel contains no layer
     * (and a GeoPanelPlaceHolder is being displayed) then scrolling is disabled.
     *
     * @param x horizontal pixel to position as near the left side of the GeoPlot as possible
     * @param y vertical pixel to position as near the top of the GeoPlot as possible
     */
    public void scrollPlotToVisiblePoint(final int x, final int y) throws IllegalArgumentException {
        //disable scrolling if geoPanel is empty
        if (geoPanel == null) {
            return;
        }

        logger.info("Processing request scrollToVisibleRect(" + x + "," + y + ")");
        if (x < 0 || y < 0) {
            logger.info("Unable to scroll to requested coordinates - x and y must be >= 0.");
        }

        final Dimension plotSize = geoPanel.getPreferredSize();

        if (x > plotSize.width || y > plotSize.height) {
            logger.info("Unable to scroll to requested coordinates x and y must be within plot dimensions " +
                    plotSize.width + " by " + plotSize.height);
        }

        logger.info("Scrolling to Rectangle(" +
                x + "," +
                y + "," +
                (int) plotSize.getWidth() + "," +
                (int) plotSize.getHeight() + ")");

        if (SwingUtilities.isEventDispatchThread()) {
            geoPanel.getGeoPlot().scrollRectToVisible(new Rectangle(x, y,
                    (int) plotSize.getWidth(), (int) plotSize.getHeight()));
        } else {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    geoPanel.getGeoPlot().scrollRectToVisible(new Rectangle(x, y,
                            (int) plotSize.getWidth(), (int) plotSize.getHeight()));
                }
            });
        }
    }

    /**
     * If this LayerPanel's WindowModel contains no layers, then this method returns
     * with no effect.  Otherwise, see {@link GeoPanel#scrollToOrigin(ViewTarget)}.
     * @param originTarget
     */
    public void scrollToOrigin(ViewTarget originTarget) {
        if (geoPanel != null) {
            geoPanel.scrollToOrigin(originTarget);
        }
    }

    public void setCursorTarget(ViewTarget target) {
        geoPanel.setCursorTarget(target);
        geoPanel.getTargetInfoPanel().setTargetInfo(target);
    }

    /**
     * Invokes JPanel.setPreferredSize, then redraws the LayerPanel's Image
     * if necessary
     */
    @Override
    public void setPreferredSize(Dimension preferredSize) {
        super.setPreferredSize(preferredSize);
        logger.info("LayerPanel.setPreferredSize invoked - resetting active layer to 0");
    }

    public void setActionMode(ACTION_MODE actionMode) {
        geoPanel.setActionMode(actionMode);
    }

    private void updateColorBar() {
        if (colorBarPanel != null) {
            remove(colorBarPanel);
            colorBarPanel = null;
        }

        WindowAnnotationProperties annoProps = winProps.getAnnotationProperties();
        LABEL_LOCATION colorbarLoc = annoProps.getColorBarLocation();

        //Create and place a new ColorBarPanel unless the location is NONE or no annotated layer is set
        if (!(colorbarLoc == LABEL_LOCATION.NONE || annoProps.getAnnotatedLayerName().equals(Layer.NaL))) {

            colorBarPanel = new ColorBarPanel(colorbarLoc, windowModel);

            switch (colorbarLoc) {
                case LEFT:
                    add(colorBarPanel, BorderLayout.WEST);
                    break;
                case RIGHT:
                    add(colorBarPanel, BorderLayout.EAST);
                    break;
                case TOP:
                    add(colorBarPanel, BorderLayout.NORTH);
                    break;
                case BOTTOM:
                    add(colorBarPanel, BorderLayout.SOUTH);
                    break;
                case NONE:
                default:
                    throw new IllegalArgumentException("Unable to initialize ColorBar with LOCATION: " + colorbarLoc);
            }
        }

        //revalidate so that the old ColorBarPanel (if any) is removed and the new one (if any) is positioned
        revalidate();
    }

    private void initGeoPanel() {
        Layer activeLayer = windowModel.getActiveLayer();
        if (activeLayer == null) {
            logger.warning("Unable to initGeoPanel(), no layer is active");
            initPlaceHolder();
            return;
        }
        if (placeHolder != null) {
            remove(placeHolder);
            placeHolder = null;
        }
        GeoPanelBuilder geoPanelBuilder = GeoPanelBuilder.getInstance(activeLayer);

        Dimension initialLayerSize = activeLayer.getSize();
        int roundedWidth = (int) Math.round(initialLayerSize.getWidth());
        int roundedHeight = (int) Math.round(initialLayerSize.getHeight());

        geoPanel = geoPanelBuilder.buildGeoPanel(
                roundedWidth - 20,
                roundedHeight - 20,
                roundedWidth,
                roundedHeight,
                activeLayer,
                winProps,
                cursorSettings,
                winSyncBroadcaster,
                windowModel);
        add(geoPanel, BorderLayout.CENTER);

        if (placeHolder != null) {
            for (GeoPlotEventListener listener : placeHolder.getGeoPlotEventListeners()) {
                geoPanel.addGeoPlotEventListener(listener);
            }
        }

        validate();
    }

    private void initPlaceHolder() {
        logger.info("All layers removed from WindowModel, redrawing geoPlot");
        if (geoPanel != null) {
            remove(geoPanel);
        }
        placeHolder = new GeoPanelPlaceHolder();
        add(placeHolder, BorderLayout.CENTER);

        if (geoPanel != null) {
            for (GeoPlotEventListener listener : geoPanel.getGeoPlotEventListeners()) {
                placeHolder.addGeoPlotEventListener(listener);
            }
        }

        geoPanel = null;
        validate();
    }

    /**
     * Shuts down any rasterization currently in progress for the Layers associated
     * with this LayerPanel.
     * 
     * @param value If true, begin shutting down the rasterizers immediately.
     */
    public void setShutdown(boolean value) {
        geoPanel.setShutdown(value);
    }

    public void contentsChanged(ListDataEvent evt) {
        logger.info("WindowModel contents changed, redrawing geoPlot");

        //This occurs when a new layer is added after all layers have been deleted
        if (geoPanel == null) {
            initGeoPanel();
        }

        geoPanel.getGeoPlot().initRasterizers();
        //redraw(true, true);
        List<Layer> layers = windowModel.getLayers();

        int evtStart = evt.getIndex0();
        int evtEnd = evt.getIndex1();

        if (evtEnd < evtStart) {
            int temp = evtStart;
            evtStart = evtEnd;
            evtEnd = temp;
        }
        //for (int i = 0; i < layers.size(); i++) {
        //if (i >= evtStart && i <= evtEnd) {
        for (int i = evtStart; i <= evtEnd; i++) {
            Layer layer = layers.get(i);
            //if (layer.getOrientation() == GeoDataOrder.CROSS_SECTION_ORDER && layer.isHorizonLayer()) {
            geoPanel.getGeoPlot().redraw(layer, true, true);
        //}
        }
    //}
    }

    public void intervalRemoved(ListDataEvent evt) {
        if (windowModel.getSize() == 0) {
            initPlaceHolder();
        } else {
            logger.info("Some layer(s) removed from WindowModel, redrawing geoPlot");
            geoPanel.getGeoPlot().initRasterizers();
            redraw(true, true);
        }
    }

    public void intervalAdded(ListDataEvent evt) {
        //This is required by deserialization, which creates the LayerPanel prior to reading any layers,
        //and therefore cannot finish intializing the geoPanel until a layer has been added to the WindowModel;
        if (windowModel.getSize() == 1) {
            logger.info("First layer(s) added to WindowModel, redrawing geoPlot");
            initGeoPanel();
            updateColorBar();
        } else {
            logger.info("Additional layer(s) added to WindowModel, redrawing geoPlot");
            geoPanel.getGeoPlot().initRasterizers();
            redraw(true, true);
        }
    }

    /**
     * Recreates the vertical and horizontal annotations as specified by the
     * current WindowAnnotationProperties of the currently annotated layer, if any.
     */
    public void updateAnnotations() {
        geoPanel.updateAnnotationPanels();
        //colorbar is a format of annotation, update it as well
        updateColorBar();
    }

    /**
     * Zooms in the associated GeoPanel by a factor of 2.
     */
    public void zoomIn() {
        geoPanel.zoomIn();
    }

    /**
     * Zooms out the associated GeoPanel by a factor of 2.
     */
    public void zoomOut() {
        geoPanel.zoomOut();
    }

    /**
     * Sets the zoom level of the associated GeoPlot so that the composite image
     * exactly fills the GeoPlot.  The Image may not completely fill the GeoPlot
     * depending on the width, height and data if the ViewerWindow's aspect ratio
     * is locked.
     */
    public void zoomAll() {
        geoPanel.zoomAll();
    }

    public void updateHorizonHighlightSettings(HorizonHighlightSettings horizonHighlightSettings) {
        geoPanel.updateHorizonHighlightSettings(horizonHighlightSettings);
    }

    public void updateCursorSettings(CursorSettings cursorSettings) {
        geoPanel.updateCursorSettings(cursorSettings);
    }

    public GeoPanel getGeoPanel() {
        return geoPanel;
    }

    public int getHorizontalScrollPos() {
        return geoPanel.getHorizontalScrollPos();
    }

    public int getVerticalScrollPos() {
        return geoPanel.getVerticalScrollPos();
    }

    public WindowProperties getWindowProperties() {
        return winProps;
    }

    public void updateWindowAnnotationProperties(WindowAnnotationProperties winAnnoProps) {
        winProps.setAnnotationProperties(winAnnoProps);
        geoPanel.updateWindowProperties(winProps);
    }

    public void updateWindowScaleProperties(WindowScaleProperties winScaleProps) {
        winProps.setScaleProperties(winScaleProps);
        geoPanel.updateWindowProperties(winProps);
    }
}