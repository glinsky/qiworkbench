/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import com.bhpb.geographics.ui.event.GeoPlotEventListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * PlaceHolder for GeoPanel which is simply a label reading "Empty GeoPlot"
 * and a List of GeoPlotEventListeners which may be retrieved for use if
 * the placeholder instance is replaced with a GeoPanel.
 *
 * @author folsw9
 */
public class GeoPanelPlaceHolder extends JLabel {
    private List<GeoPlotEventListener> geoPlotEventListeners =
            new ArrayList<GeoPlotEventListener>();

    public GeoPanelPlaceHolder() {
        super("Empty Plot", SwingConstants.CENTER);
    }

    public void addGeoPlotEventListener(GeoPlotEventListener listener) {
        geoPlotEventListeners.add(listener);
    }

    public List<GeoPlotEventListener> getGeoPlotEventListeners() {
        return geoPlotEventListeners;
    }
}