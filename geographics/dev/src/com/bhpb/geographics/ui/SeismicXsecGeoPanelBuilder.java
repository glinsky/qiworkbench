/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.ui;

import com.bhpb.geographics.GeoImage;
import com.bhpb.geographics.RenderableImageLayer;
import com.bhpb.geographics.model.Annotation;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.SeismicLayer;
import com.bhpb.geographics.model.window.CrossSectionAnnotationProperties;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geographics.rasterizer.LayeredRenderableImageProducer;
import com.bhpb.geographics.rasterizer.RasterizerFactory;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import javax.swing.JComponent;

public class SeismicXsecGeoPanelBuilder extends GeoPanelBuilder {

    @Override
    public RenderableImageLayer createRenderableImageLayer(Layer layer) {
        return new GeoImage((SeismicLayer) layer);
    }

    @Override
    public LayeredRenderableImageProducer createRenderableImageProducer(Layer layer,
            RenderableImageLayer img, AbstractGeoPlot geoPlot) {
        return (LayeredRenderableImageProducer) RasterizerFactory.createSeismicXsecRasterizer((SeismicLayer)layer,
                img,
                geoPlot.getViewportBounds());
    }

    @Override
    protected JComponent getDecoration(DECORATION_TYPE decorType, Layer layer, WindowProperties winProps, AbstractGeoPlot geoPlot) {
        switch (decorType) {
            case CORNER_CAP:
                //return new CaptionBorderPanel(layer.getDatasetProperties());
                String[] selectedKeys = ((CrossSectionAnnotationProperties)winProps.getAnnotationProperties()).getSelectedKeys();
                return new CaptionBorderPanel(selectedKeys);
            case H_ANNOT:
                return CrossSectionAnnotationPanelBuilder.createXsecAnnotationPanel(Annotation.AXIS.X_AXIS, layer, winProps, geoPlot);
            case V_ANNOT:
                return CrossSectionAnnotationPanelBuilder.createXsecAnnotationPanel(Annotation.AXIS.Y_AXIS, layer, winProps, geoPlot);
            default:
                return null;
        }
    }

    @Override
    protected DECORATION_LOC getLocation(DECORATION_TYPE decorType) {
        switch (decorType) {
            case CORNER_CAP :
                return DECORATION_LOC.UL;
            case H_ANNOT :
                return DECORATION_LOC.LEFT;
            case V_ANNOT :
                return DECORATION_LOC.RIGHT;
            default :
                return null;
        }
    }
    
    @Override
    protected boolean hasDecoration(DECORATION_TYPE decorType) {
            switch (decorType) {
            case CORNER_CAP :
                return true;
            case H_ANNOT :
                return true;
            case V_ANNOT :
                return true;
            default :
                return false;
        }
    }
    
    @Override
    protected boolean isValid(Layer layer) {
        return ((layer instanceof SeismicLayer) &&
                (layer.getDatasetProperties().getMetadata().getGeoDataOrder() == GeoDataOrder.CROSS_SECTION_ORDER));
    }
}