/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.rasterizer;

import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParameters;
import com.bhpb.geographics.model.layer.shared.InterpolationSettings;
import com.bhpb.geographics.util.ColorMap;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * AbstractTraceRasterizer subclass which renders Seismic cross-section data in
 * any of three major rendering modes:
 * <ul>
 *   <li>Wiggle Trace, with optional positive and/or negative fill</li>
 *   <li>variable density trace with uniform color along the horizontal axis<li>
 *   <li>density trace with linearly interpolated color along the horizontal axis between adjacent rasterized traces<li>
 * </ul>
 *
 * Note that the wiggle trace mode may be combined with either density mode for a given
 * cross-section layer, but the density rendering modes are mutually exclusive.
 *
 * @author folsw9
 */
public class SeismicXsecTraceRasterizer extends AbstractTraceRasterizer {

    private static void drawInterpolatedColorLine(Graphics2D g, Color[] colorRamp, Point2D[] interpolatedPoints, int y1, int y2) {
        for (int segmentIndex = 0; segmentIndex < interpolatedPoints.length - 1; segmentIndex++) {
            g.setColor(colorRamp[(int) interpolatedPoints[segmentIndex].getY()]);

            int x1 = (int) Math.round(interpolatedPoints[segmentIndex].getX());
            int x2 = (int) Math.round(interpolatedPoints[segmentIndex + 1].getX());

            for (int y = y1; y <= y2; y++) {
                g.drawLine(x1, y, x2, y);
            }
        }
    }

    private static void drawInterpolatedDensityTrace(float xBase,
            Graphics2D g,
            List<Point2D> prevTrace,
            List<Point2D> densityTrace,
            List<Point2D> nextTrace,
            double traceSeparation,
            ColorMap colorMap) {

        double halfTraceWidth = traceSeparation / 2.0;
        Color[] colorRamp = colorMap.getColors(ColorMap.RAMP);

        for (int index = 0; index < densityTrace.size(); index++) {

            double prevSample = prevTrace.get(index).getX();
            double sample = densityTrace.get(index).getX();
            double nextSample = nextTrace.get(index).getX();

            // linearly interpolate the sample value at the left and right edges of this trace
            prevSample = prevSample + (sample - prevSample) / 2.0;
            nextSample = sample + (nextSample - sample) / 2.0;

            if (isNull((float) sample)) {
                continue;
            } else {
                float start;
                if (index == 0) {
                    start = 0f;
                } else {
                    start = (float) (densityTrace.get(index - 1).getY());
                }

                float end = (float) (densityTrace.get(index).getY());

                float minx = (float) (xBase - halfTraceWidth);
                float maxx = (float) (xBase + halfTraceWidth);

                int y1 = Math.round(start);
                int y2 = Math.round(end);

                //interpolate and render first half of trace
                Point2D[] interpolatedPoints = colorMap.doLinearInterp(minx, xBase, prevSample, sample);
                drawInterpolatedColorLine(g, colorRamp, interpolatedPoints, y1, y2);

                //interpolate and render second half of trace
                interpolatedPoints = colorMap.doLinearInterp(xBase, maxx, sample, nextSample);
                drawInterpolatedColorLine(g, colorRamp, interpolatedPoints, y1, y2);
            }
        }
    }

    static void drawVariableDensityTrace(float xBase,
            Graphics2D g,
            List<Point2D> densityTrace,
            double traceSeparation,
            ColorMap colorMap) {

        for (int index = 0; index < densityTrace.size(); index++) {

            float normalizedSample = (float) densityTrace.get(index).getX();

            float start;
            if (index == 0) {
                start = 0f;
            } else {
                start = (float) (densityTrace.get(index - 1).getY());
            }

            float end = (float) (densityTrace.get(index).getY());

            float minx = (float) (xBase - traceSeparation / 2.0);
            float maxx = (float) (xBase + traceSeparation / 2.0);

            if (isNull(normalizedSample)) {
                continue;
            } else {
                GeneralPath path = new GeneralPath();
                path.moveTo(minx, start);

                path.lineTo(maxx, start);
                path.lineTo(maxx, end);
                path.lineTo(minx, end);
                path.lineTo(minx, start);

                g.setColor(colorMap.getColor(ColorMap.RAMP, normalizedSample));
                g.fill(path);
            }
        }
    }

    private static void drawWiggleTrace(Graphics2D g, Color fgColor, GeneralPath wigglePath) {
        g.setColor(fgColor);
        g.draw(wigglePath);
    }

    public static List<Point2D> getDensityTrace(float[] values,
            float yOffset) {

        //Cannot create arrays of generics in JDK 1.5
        List<Point2D> wigglePath = new ArrayList<Point2D>();

        for (int i = 0; i < values.length; i++) {
            //float normalizedSample = normalizeByLimits(values[i], minAmplitude, maxAmplitude);
            float ycoord = (float) (yOffset + i);
            wigglePath.add(
                    new Point2D.Float(
                    values[i],
                    ycoord));
        }

        return wigglePath;
    }

    /**
     * Given the wigglePath of actual data points, create a GeneralPath
     * which would be correctly filled, with a line along the vertical axis,
     * if the first and last points were connected.  This would not be the case
     * for the wigglePath unless the first and last points both had an amplitude
     * of 0.0.
     */
    private static GeneralPath getFillablePath(GeneralPath wigglePath, float firstYCoord, float lastYCoord) {

        GeneralPath fillablePath = new GeneralPath();
        fillablePath.moveTo(0.0f, lastYCoord);
        fillablePath.lineTo(0.0f, firstYCoord);
        fillablePath.append(wigglePath, true);
        fillablePath.lineTo(0.0f, lastYCoord);

        return fillablePath;
    }

    /**
     *
     * @param values
     * @param xBase
     * @param pixelsPerSample
     * @param traceSeparation
     * @param interpType
     * @param numInterpolatedSamples
     * @param interpolatedValues
     * @param yOffset
     *
     * @return
     */
    private static GeneralPath getWigglePath(float[] values,
            float xBase,
            double traceSeparation,
            float yOffset, //offset of the first sample's y coordinate in pixels (not traces or time/depth units)
            InterpolationSettings.TYPE interpType
            ) {

        GeneralPath wigglePath = new GeneralPath();

        float xCoord;
        if (AbstractTraceRasterizer.isNull(values[0])) {
            //change this from xBase to O.0f for a clear indication of null regions while debugging
            //or else refactor the rasterizer to handle and fill discontinuous regions
            xCoord = xBase;
        } else {
            xCoord = (float) (xBase + values[0] * traceSeparation);
        }

        wigglePath.moveTo(
                xCoord,
                yOffset);

        float prevXcoord = xCoord;
        for (int i = 1;
                i < values.length;
                i++) {
            if (AbstractTraceRasterizer.isNull(values[i])) {
                xCoord = 0.0f;
            } else {
                xCoord = (float) (xBase + values[i] * traceSeparation);
            }

            float yCoord = (float) (yOffset + i);

            //If interpolation mode is STEP and the previous interpolated,
            //subsampled or actual amplitude is different, draw a vertical line to
            //this data point's yCoordinate from the previous data point first.
            if (interpType == InterpolationSettings.TYPE.STEP) {
                if (values[i-1] != xCoord) {
                    wigglePath.lineTo(prevXcoord, yCoord);
                }
            }
            wigglePath.lineTo(xCoord, yCoord);
            prevXcoord = xCoord;
        }

        return wigglePath;
    }

    private static void rasterizeShapesToImage(
            boolean fillNeg,
            Area negativeArea,
            Color negColor,
            boolean fillPos,
            Area positiveArea,
            Color posColor,
            Color fgColor,
            GeneralPath wigglePath,
            Graphics2D g) {

        //first, fill the negative region if requested
        if (fillNeg) {
            g.setColor(negColor);
            if (negativeArea != null) {
                g.fill(negativeArea);
            }
        }

        //next, fill the positive region
        if (fillPos) {
            g.setColor(posColor);
            if (positiveArea != null) {
                g.fill(positiveArea);
            }
        }

        if (wigglePath != null) {
            drawWiggleTrace(g, fgColor, wigglePath);
        }
    }

    /**
     * Rasterizes the density trace at the given xBase coordinate,
     * using linear interpolation to select colors between adjacent data points of
     * the densityTrace and the previous and next density traces.
     *
     * @param xBase float the x coordinate of the trace
     * @param pixelsPerTrace double scale of pixels per trace
     * @param ldParams SeismicXsecLayerDisplayParameters
     * @param visRect Rectangle the visible rectangle
     * @param prevDensityTrace the previous non-decimated trace, used for interpolation
     * @param densityTrace the list of normalized, clipped values of the trace being rasterized
     * @param nextDensityTrace the next non-decimated trace, used for interpolation
     * @param g
     */
    public static void rasterizeInterpolatedDensity(
            float xBase,
            double pixelsPerTrace,
            SeismicXsecDisplayParameters ldParams,
            Rectangle visRect,
            List<Point2D> prevDensityTrace,
            List<Point2D> densityTrace,
            List<Point2D> nextDensityTrace,
            Graphics2D g) {

        drawInterpolatedDensityTrace(xBase,
                g,
                prevDensityTrace,
                densityTrace,
                nextDensityTrace,
                pixelsPerTrace,
                ldParams.getColorMap());
    }

    public static void rasterizeVariableDensity(float[] values,
            float xBase,
            double pixelsPerTrace,
            SeismicXsecDisplayParameters ldParams,
            Rectangle visRect,
            Graphics2D g) {

        List<Point2D> densityTrace = getDensityTrace(values,
                visRect.y);

        drawVariableDensityTrace(xBase,
                g,
                densityTrace,
                pixelsPerTrace,
                ldParams.getColorMap());
    }

    /**
     * Rasterizes the given array of floating-point values onto the tileSet.
     *
     * @param values
     * @param xBase
     * @param pixelsPerTrace
     * @param pixelsPerSample
     * @param traceSeparation
     * @param ldParams
     * @param visRect
     * @param normScale
     * @param interpType
     * @param g
     * @param sampleStart y coordinate at which the first sample in the value array starts.
     * Will usually be on or before the visible rectangle, but may occur below the top
     * of the visible rectangle if the viewport extends above the dataset's start time.
     */
    static void rasterizeWiggle(float[] values,
            float xBase,
            double pixelsPerTrace,
            double pixelsPerSample,
            double traceSeparation,
            SeismicXsecDisplayParameters ldParams,
            Rectangle visRect,
            double normScale,
            InterpolationSettings.TYPE interpType,
            Graphics2D g,
            double yCoordOffset) {

        Area fillableArea = null;
        Area negativeArea = null;
        Area positiveArea = null;

        boolean fillPos = ldParams.getRasterizationTypeModel().doRasterizePositiveFill();
        boolean fillNeg = ldParams.getRasterizationTypeModel().doRasterizeNegativeFill();
        Color posColor = ldParams.getColorMap().getPosFillColor();
        Color negColor = ldParams.getColorMap().getNegFillColor();
        Color fgColor = ldParams.getColorMap().getForegroundColor();


        GeneralPath wigglePath = getWigglePath(
                values,
                xBase,
                traceSeparation,
                (float) (yCoordOffset),
                interpType);

        GeneralPath fillablePath;

        if (fillNeg || fillPos) {
            fillablePath = getFillablePath(wigglePath, (float) visRect.getMinY(), (float) visRect.getMaxY());
            fillableArea = new Area(fillablePath);
        }

        //by design, positive and negative fill stop at normalized amplitude 1.0f
        //bhpViewer also does this, although the unfilled wiggle may extend to an arbitrary
        //distance from the trace axis
        double maxAbs = getMaxAbs(values);

        //if the entire trace has 0 amplitude, disable positive/negative fill for this trace
        if (maxAbs == 0.0) {
            fillNeg = false;
            fillPos = false;
        }

        if (fillNeg) {
            negativeArea = getNegativeMask(xBase, visRect.y, (float) (traceSeparation * maxAbs), (float) (values.length /* * pixelsPerSample */));
            //negativeArea.exclusiveOr(fillableArea);
            negativeArea.subtract(fillableArea);
        }

        if (fillPos) {
            positiveArea = getPositiveMask(xBase, visRect.y, (float) (traceSeparation * maxAbs), (float) (values.length /* * pixelsPerSample */));
            positiveArea.intersect(fillableArea);
        }

        rasterizeShapesToImage(
                fillNeg,
                negativeArea,
                negColor,
                fillPos,
                positiveArea,
                posColor,
                fgColor,
                wigglePath,
                g);
    }
}