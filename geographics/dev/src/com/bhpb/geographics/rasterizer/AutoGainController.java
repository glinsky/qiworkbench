/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.rasterizer;

import com.bhpb.geographics.model.layer.shared.AutoGainControl;
import com.bhpb.geographics.model.layer.shared.AutoGainControl.UNITS;

/**
 * AutoGainController - performs trace value scaling based on normalization scale
 * and the average of trace values read from a configurable window around the scaled
 * trace value.  The paramters used by the AutoGainController are located in the AutoGainControl class.
 *
 * The AGC window length may be specified in time or sample units.  The scale factor
 * is not configurable independent of normalization scale.  Based on bhpViewer AutoGainController,
 * which was based on sugain.c.
 *
 * @author folsw9
 */
public class AutoGainController {

    private static final double ZERO = 1e-8;

    /**
     * Performs AutoGainControl of trace values.  If agc is disabled or the specified AGC
     * window length is 0, an identical copy of the trace value array is returned.
     *
     * @return a copy of the trace value array with samples between sampleStart
     * and sampleEnd multiplied by the AGC factor.
     */
    public static float[] doAGC(float[] trace, int sampleStart, int sampleEnd, AutoGainControl agc,
            double normalizationScale, double sampleRate) {
        final UNITS units = agc.getUnits();

        int windowLength;
        switch (units) {
            case Sample:
                windowLength = agc.getWindowLength();
                break;
            case Time:
                windowLength = (int) Math.ceil(agc.getWindowLength() / sampleRate);
                break;
            default:
                throw new IllegalArgumentException("Cannot do AGC due to unknown units: " + units);
        }

        float[] traceCopy = new float[trace.length];
        System.arraycopy(trace, 0, traceCopy, 0, trace.length);

        if (!agc.isEnabled() || windowLength <= 1) {
            return traceCopy;
        }

        int windowStart = -windowLength / 2;
        int windowEnd = windowLength / 2;
        int sampleCount = 0;
        int sampleIndex;
        double sampleTotal = 0;

        //initialize sampleTotal, sampleCount with half of windowLength containing
        //traceCopy[] values with indices >= 0
        for (sampleIndex = windowStart; sampleIndex < windowEnd; sampleIndex++) {
            if (sampleIndex >= 0 && sampleIndex < traceCopy.length) {
                float sampleValue = trace[sampleIndex];
                if (!AbstractTraceRasterizer.isNull(sampleValue)) {
                    sampleTotal += Math.abs(sampleValue);
                    sampleCount++;
                }
            } else {
                continue;
            }
        }

        //iterate over all trace samples, updating sampleTotal and sampleCount
        //depending on whether windowStart, windowEnd are within the range 0..traceCopy.length
        for (sampleIndex = 0; sampleIndex < trace.length; sampleIndex++) {
            float sampleValue = trace[sampleIndex];
            if (!AbstractTraceRasterizer.isNull(sampleValue)) {
                if (sampleTotal > ZERO) {
                    double multiplier = (normalizationScale * sampleCount) / sampleTotal;
                    traceCopy[sampleIndex] = (float) (sampleValue * multiplier);
                } else if (sampleTotal < 0.0) {
                    throw new IllegalStateException("SampleTotal < 0.0");
                } else {
                    traceCopy[sampleIndex] = 0.0f;
                }
            }

            if (windowStart >= 0) {
                sampleValue = trace[windowStart];
                if (!AbstractTraceRasterizer.isNull(sampleValue)) {
                    sampleCount--;
                    sampleTotal -= Math.abs(sampleValue);
                }
            }

            if (windowEnd < trace.length) {
                sampleValue = trace[windowEnd];
                if (!AbstractTraceRasterizer.isNull(sampleValue)) {
                    sampleCount++;
                    sampleTotal += Math.abs(sampleValue);
                }
            }

            windowStart++;
            windowEnd++;
        }

        //return a copy of the original trace array with each value scaled by
        //normalization scale and divided by the average value within the AGC window
        return traceCopy;
    }
}