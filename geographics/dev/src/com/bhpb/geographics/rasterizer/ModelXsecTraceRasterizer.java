/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.rasterizer;

import com.bhpb.geographics.model.layer.display.ModelXsecDisplayParameters;
import com.bhpb.geographics.util.ColorMap;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * ModelXsecTraceRasterizer draws cross-section model 'trace' data, of which each trace's
 * floating-point data array contains N property values and N+1 time values.  The final value
 * of each data array is the time at which the last property terminates.  Any or all of the
 * properties may contain one of the special values indicating NULL data in the BHP-SU format (-999.0 or
 * -999.25f).
 *
 * @author folsw9
 */
public class ModelXsecTraceRasterizer extends AbstractTraceRasterizer {

    private static void drawVariableDensityTrace(float xBase,
            Graphics2D g,
            List<Point2D> densityTrace,
            float prevXbase,
            List<Point2D> prevDensityTrace,
            ColorMap colorMap,
            double pixelsPerUnit) {
        for (int index = 0; index < densityTrace.size() / 2; index++) {

            float normalizedSample = (float) densityTrace.get(index * 2).getX();
            float prevNormalizedSample = (float) prevDensityTrace.get(index * 2).getX();

            float start = (float) (densityTrace.get(index * 2).getY() * pixelsPerUnit);
            float end = (float) (densityTrace.get(index * 2 + 1).getY() * pixelsPerUnit);

            float prevStart = (float) (prevDensityTrace.get(index * 2).getY() * pixelsPerUnit);
            float prevEnd = (float) (prevDensityTrace.get(index * 2 + 1).getY() * pixelsPerUnit);

            if (isNull(normalizedSample)) {
                continue;
            } else {
                GeneralPath path = new GeneralPath();
                path.moveTo(prevXbase, prevStart);
                path.lineTo(xBase, start);
                path.lineTo(xBase, end);
                path.lineTo(prevXbase, prevEnd);
                if (isNull(prevNormalizedSample)) {
                    // triangle, layer is null in previous trace
                    //do Nothing, shape is already closed
                } else {
                    // rectangle, layer is non-null in both traces
                    path.lineTo(prevXbase, prevStart);
                }
                g.setColor(colorMap.getColor(ColorMap.RAMP, normalizedSample));
                g.fill(path);
            }
        }
    }

    /**
     * Enables wiggle-trace rendering mode for model xsec data.  Enabled for parity with
     * bhpViewer plots, although this functionality may not be intended for model layers.
     *
     * @param g
     * @param fgColor
     * @param wigglePath
     */
    private static void drawWiggleTrace(Graphics2D g, Color fgColor, GeneralPath wigglePath) {
        g.setColor(fgColor);
        g.draw(wigglePath);
    }

    private static List<Point2D> getDensityTrace(float[] values,
            double sampleStart) {
        int nlayers = (values.length - 1) / 2;

        List<Point2D> wigglePath = new ArrayList<Point2D>();

        for (int i = 0; i < nlayers; i++) {
            wigglePath.add(
                    new Point2D.Float(
                    values[i],
                    (float) (values[i + nlayers] - sampleStart)));
            wigglePath.add(
                    new Point2D.Float(
                    values[i],
                    (float) (values[i + nlayers + 1] - sampleStart)));
        }

        return wigglePath;
    }

    /**
     * @param values
     * @param xBase
     * @param sectionSize
     * @param pixelsPerSample
     * @param scaleY
     * @return
     */
    private static GeneralPath getWigglePath(float[] values,
            float xBase,
            double sampleStart) {
        int nlayers = (values.length - 1) / 2;

        GeneralPath wigglePath = new GeneralPath();

        for (int i = 0; i < nlayers; i++) {
            if (!isNull(values[i])) {
                wigglePath.moveTo(xBase,
                        (int) (values[i + nlayers] - sampleStart));
                wigglePath.lineTo(xBase + values[i],
                        (int) (values[i + nlayers] - sampleStart));
                wigglePath.lineTo(xBase + values[i],
                        (int) (values[i + nlayers + 1] - sampleStart));
                //if the next value
                wigglePath.lineTo(xBase,
                        (int) (values[i + nlayers + 1] - sampleStart));
            }
        }

        return wigglePath;
    }

    public static List<Point2D> rasterizeDensity(float[] values,
            float xBase,
            float[] prevValues,
            float prevXbase,
            double pixelsPerTrace,
            double pixelsPerUnit,
            ModelXsecDisplayParameters ldParams,
            List<Point2D> prevDensityTrace,
            Rectangle visRect,
            double sampleStart,
            Graphics2D g) {
        List<Point2D> densityTrace = getDensityTrace(values,
                /*(float) pixelsPerUnit,*/
                sampleStart);

        if (prevDensityTrace == null || !ldParams.getModelVerticalRange().isStructuralInterpolationEnabled()) {
            prevDensityTrace = densityTrace;
        }
        drawVariableDensityTrace(xBase,
                g,
                densityTrace,
                prevXbase,
                prevDensityTrace,
                ldParams.getColorMap(),
                pixelsPerUnit);

        return densityTrace;
    }

    /**
     * Enables wiggle-trace rendering mode for model xsec data.  Enabled for parity with
     * bhpViewer plots, although this functionality may not be intended for model layers.
     *
     * @param values array of floating-point data values
     * @param xBase x coordinate at which to render the model trace
     * @param pixelsPerTrace horizontal scale in pixels per trace
     * @param pixelsPerUnit vertical scale in pixels per unit (not sample)
     * @param ldParams ModelXsecLayerDisplayParameters
     * @param visRect visible Rectangle
     * @param g Graphics2D context
     * @param sampleStart configurable sampleStart necessary to place Model xsec layers relative to seismic layers and one another
     */
    public static void rasterizeWiggle(float[] values,
            float xBase,
            double pixelsPerTrace,
            double pixelsPerUnit,
            ModelXsecDisplayParameters ldParams,
            Rectangle visRect,
            Graphics2D g,
            double sampleStart) {

        GeneralPath wigglePath = getWigglePath(values,
                xBase,
                sampleStart);

        Color fgColor = ldParams.getColorMap().getForegroundColor();

        drawWiggleTrace(g, fgColor, wigglePath);
    }
}