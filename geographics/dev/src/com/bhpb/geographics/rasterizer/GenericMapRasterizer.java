/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.rasterizer;

import com.bhpb.geographics.RenderableImageLayer;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.layer.display.MapDisplayParameters;
import com.bhpb.geographics.model.layer.shared.BasicLayerProperties;
import com.bhpb.geographics.model.layer.shared.ColorAttributes;
import com.bhpb.geographics.model.window.WindowScaleProperties;
import com.bhpb.geographics.ui.SparseBufferedImage;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Rasterizer for seismic, horizon or model maps.  This is possible because the format
 * off map-order data is the same regardless of the GeoFileType of the underlying dataset.
 *
 * @author folsw9
 */
public class GenericMapRasterizer extends AbstractGeoRasterizer {

    private static Logger logger = Logger.getLogger(GenericMapRasterizer.class.toString());
    private Layer layer;

    /**
     * Constructs a generic map rasterizer.
     *
     * @param layer
     * @param rdblImage
     * @param visRect
     */
    public GenericMapRasterizer(
            Layer layer,
            RenderableImageLayer rdblImage,
            Rectangle visRect) {
        super(rdblImage, visRect);
        this.layer = layer;
    }

    private synchronized SparseBufferedImage getNewImage(
            Rectangle visRect,
            double pixelsPerTraceH,
            double pixelsPerTraceV,
            MapDisplayParameters displayParams) {

        Thread currentThread = Thread.currentThread();

        SparseBufferedImage newImage = createSparseBufferedImage(visRect);
        Graphics2D g = (Graphics2D) newImage.getGraphics();
        g.translate(-visRect.x, -visRect.y);

        rasterizeDensity(visRect, pixelsPerTraceH, pixelsPerTraceV, g, displayParams);

        if (currentThread.isInterrupted()) {
            logRasterizationAbortedMessage(logger);
            return SparseBufferedImage.NULL;
        } else {
            return newImage;
        }
    }

    /**
     * Rasterizer the DataObjects into a BufferedImage
     * 
     * @param visRect the current visible rectangel of the GeoPlot for which
     * a SparseBufferedImage will be created.
     * 
     * @return SparseBufferedImage containing the complete seismic plot, or a partial
     * image if rasterization was interrupted.
     */
    synchronized public SparseBufferedImage rasterize(Rectangle visRect) {
        if (visRect.getWidth() <= 0 || visRect.getHeight() <= 0) {
            logger.info("visRect has width, height: " + visRect.getWidth() + ", " + visRect.getHeight() + ", skipping rasterization");
            return SparseBufferedImage.NULL;
        }

        WindowScaleProperties scaleProps = layer.getWindowModel().getWindowProperties().getScaleProperties();

        MapDisplayParameters layerDisplayParams = (MapDisplayParameters) layer.getDisplayParameters();
        BasicLayerProperties basicLayerProps = layerDisplayParams.getBasicLayerProperties();

        double pixelsPerTrace = scaleProps.getHorizontalPixelIncr() * basicLayerProps.getHorizontalScale();

        double vKeyIncr = getVerticalKeyIncr(layer.getDatasetProperties());
        /* px/sample = px/units * units/sample */
        double pixelsPerSample = scaleProps.getVerticalPixelIncr() * vKeyIncr * basicLayerProps.getVerticalScale();

        if (!Thread.currentThread().isInterrupted()) {
            return getNewImage(visRect,
                    pixelsPerTrace,
                    pixelsPerSample,
                    layerDisplayParams);
        } else {
            logRasterizationAbortedMessage(logger);
            return SparseBufferedImage.NULL;
        }
    }

    private void rasterizeDensity(Rectangle visRect,
            double pixelsPerTraceH,
            double pixelsPerTraceV,
            Graphics2D g,
            MapDisplayParameters displayParams) {
        float lastRenderedTraceCoord = 0.0f;

        int firstTraceToRender = getFirstVisibleTraceIndex(visRect, pixelsPerTraceH);
        float[][] values = getValues(layer.getDataObjects());

        int lastTraceToRender = getLastVisibleTraceIndex(visRect,
                pixelsPerTraceH, values.length);

        DatasetProperties dsProperties = layer.getDatasetProperties();
        ArrayList<String> keyNames = dsProperties.getMetadata().getKeys();
        String vKeyName = keyNames.get(keyNames.size() - 1);
        AbstractKeyRange vKeyRange = dsProperties.getKeyChosenRange(vKeyName);
        
        ColorAttributes colorAttribs = displayParams.getColorAttributes();
        ColorMap colorMap = colorAttribs.getColorMap();
        double colorMapMin = colorAttribs.getMin();
        double colorMapMax = colorAttribs.getMax();

        for (int wIndex = firstTraceToRender; wIndex <=
                lastTraceToRender; wIndex++) {
            if (Thread.currentThread().isInterrupted()) {
                break;
            }

            //by default, render a trace at every other pixel starting at #1, not 0
            float xPixel = (float) (pixelsPerTraceH * (wIndex + 1));

            GenericMapTraceRasterizer.rasterizeDensity(values[wIndex],
                    xPixel,
                    lastRenderedTraceCoord,
                    pixelsPerTraceV,
                    colorMap,
                    colorMapMin,
                    colorMapMax,
                    g);
            lastRenderedTraceCoord = xPixel;
        }
    }
}