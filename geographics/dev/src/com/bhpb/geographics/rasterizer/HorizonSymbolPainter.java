/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.rasterizer;

import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters.SYMBOL_STYLE;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.util.logging.Logger;

/**
 * HorizonSymbolPainter draws different styles of symbols which indicate horizon data points.
 * Valid styles are defined by the com.bhpb.geographics.model.display.HorizonXsecDisplayParameters.SYMBOL_STYLE
 * enumeration.
 */
public class HorizonSymbolPainter {

    private static final Logger logger =
            Logger.getLogger(HorizonSymbolPainter.class.getName());

    /**
     * Paints a horizon data point symbol at the specified coordinates of the given
     * Graphics2D context.  The symbol style, size and color are defined by the
     * displayParams, fillSymbol flag and specified SYMBOL_STYLE.
     *
     * @param g Graphics2D
     * @param symbolStyle HorizonXsecDisplayParameters.SYMBOL_STYLE
     * @param displayParams HorizonXsecDisplayParameters
     * @param minX float x coordinate
     * @param minY float y coordinate
     * @param width float symbol width in pixels
     * @param height symbol height in pixels
     * @param fillSymbol if true, the interior of the symbol is filled using displayParams.getSymbolColor()
     */
    public static void paintSymbol(Graphics2D g,
            SYMBOL_STYLE symbolStyle,
            HorizonXsecDisplayParameters displayParams, float minX, float minY,
            float width, float height,
            boolean fillSymbol) {
        Shape horizonSymbol;

        float midX = (minX + width / 2.0f);
        float maxX = minX + width;
        float midY = (minY + height / 2.0f);
        float maxY = minY + height;

        switch (symbolStyle) {
            case CIRCLE:
                horizonSymbol = new Ellipse2D.Double(minX, minY, width, height);
                break;
            case CROSS:
                horizonSymbol = new GeneralPath();

                ((GeneralPath) horizonSymbol).moveTo(minX, minY);
                ((GeneralPath) horizonSymbol).lineTo(maxX, maxY);

                ((GeneralPath) horizonSymbol).moveTo(minX, maxY);
                ((GeneralPath) horizonSymbol).lineTo(maxX, minY);
                break;
            case DIAMOND:
                horizonSymbol = new GeneralPath();

                ((GeneralPath) horizonSymbol).moveTo(midX, minY);
                ((GeneralPath) horizonSymbol).lineTo(maxX, midY);
                ((GeneralPath) horizonSymbol).lineTo(midX, maxY);
                ((GeneralPath) horizonSymbol).lineTo(minX, midY);
                ((GeneralPath) horizonSymbol).lineTo(midX, minY);

                break;
            case DOT:
                horizonSymbol = new GeneralPath();

                ((GeneralPath) horizonSymbol).moveTo(midX, midY);
                ((GeneralPath) horizonSymbol).lineTo(midX + 1, midY);
                break;
            case PLUS:
                horizonSymbol = new GeneralPath();

                ((GeneralPath) horizonSymbol).moveTo(midX, minY);
                ((GeneralPath) horizonSymbol).lineTo(midX, maxY);

                ((GeneralPath) horizonSymbol).moveTo(minX, midY);
                ((GeneralPath) horizonSymbol).lineTo(maxX, midY);
                break;
            case SQUARE:
                horizonSymbol = new Rectangle2D.Double(minX, minY, width, height);
                break;
            case STAR: //star = cross + plus
                horizonSymbol = new GeneralPath();

                //draw cross
                ((GeneralPath) horizonSymbol).moveTo(minX, minY);
                ((GeneralPath) horizonSymbol).lineTo(maxX, maxY);

                ((GeneralPath) horizonSymbol).moveTo(minX, maxY);
                ((GeneralPath) horizonSymbol).lineTo(maxX, minY);

                //then draw plus
                ((GeneralPath) horizonSymbol).moveTo(midX, minY);
                ((GeneralPath) horizonSymbol).lineTo(midX, maxY);

                ((GeneralPath) horizonSymbol).moveTo(minX, midY);
                ((GeneralPath) horizonSymbol).lineTo(maxX, midY);
                break;
            case TRIANGLE:
                horizonSymbol = new GeneralPath();

                //draw cross
                ((GeneralPath) horizonSymbol).moveTo(midX, minY);
                ((GeneralPath) horizonSymbol).lineTo(maxX, maxY);
                ((GeneralPath) horizonSymbol).lineTo(minX, maxY);
                ((GeneralPath) horizonSymbol).lineTo(midX, minY);

                break;
            default:
                logger.warning("Unknown symbol style: " + symbolStyle);
                return;
        }

        //optionally fill symbols which may be filled.
        if (fillSymbol) {
            g.fill(horizonSymbol);
        } else {
            g.draw(horizonSymbol);
        }
    }
}