/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.rasterizer;

import com.bhpb.geographics.model.Annotation;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.layer.shared.BasicLayerProperties;
import com.bhpb.geographics.model.window.AnnotationProperties;
import com.bhpb.geographics.model.window.CrossSectionAnnotationProperties;
import com.bhpb.geographics.model.window.CrossSectionAnnotationProperties.STEP_TYPE;
import com.bhpb.geographics.model.window.WindowAnnotationProperties;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geographics.model.window.WindowScaleProperties;
import com.bhpb.geographics.util.DecimalFormatter;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.util.logging.Logger;

/**
 * AnnotationRasterizer creates an Image of a vertical or horizontal axis line,
 * with captioned numerical tick marks, from an Annotation.
 * 
 */
public class AnnotationRasterizer {

    private static final Logger logger =
            Logger.getLogger(AnnotationRasterizer.class.getName());
    private Annotation annotation;
    private static final DecimalFormatter formatter = DecimalFormatter.getInstance(10);
    // the height of horiz. annotation component or width of a vertical one
    private int annotationSize;
    private Layer annotatedLayer;

    /**
     * Constructs an annotation rasterizer which can be used to create an Image
     * of the specified annotation with the requestd height and width.
     *
     * @param annotation the annotation model
     * @param annotationSize height for horizontal annotation or width for vertical
     * @param annotatedLayer the layer being annotated
     */
    public AnnotationRasterizer(Annotation annotation, int annotationSize, Layer annotatedLayer) {
        this.annotation = annotation;
        this.annotationSize = annotationSize;
        this.annotatedLayer = annotatedLayer;
    }

    private void drawAxis(Graphics2D g,
            Rectangle visRect) {
        g.setStroke(new BasicStroke(1));

        int height = visRect.height;
        int width = visRect.width;

        if (annotation.getAxis() == Annotation.AXIS.X_AXIS) {
            g.drawLine(visRect.x, height - 1,
                    visRect.x + width, height - 1);
        } else if (annotation.getAxis() == Annotation.AXIS.Y_AXIS) {
            g.drawLine(width - 1, visRect.y,
                    width - 1, visRect.y + height);
        } else {
            throw new UnsupportedOperationException("Cannot render axis in " + annotation.getAxis() + " orientation, only X_AXIS and Y_AXIS annotations may be rasterized at this time.");
        }
    }

    private void drawHorizontalCaption(float coordinate, DataObject trace, Graphics2D g,
            Font tickFont, float height) {

        String caption = getFormattedTickValue(trace);

        double theta = getCaptionAngle();

        AffineTransform af = AffineTransform.getTranslateInstance(/*point.getX()*/+coordinate, /*point.getY()*/ +height / 2.0f);

        af.rotate(theta);

        AffineTransform af2 = g.getTransform();
        g.transform(af);
        g.setFont(tickFont);
        g.drawString(caption, 0, 0);
        g.setTransform(af2);
    }

    private void drawTick(float coordinate, Graphics2D g,
            int height, int width) {
        g.setStroke(new BasicStroke(1));

        if (annotation.getAxis() == Annotation.AXIS.X_AXIS) {
            g.drawLine((int) coordinate, height / 2, (int) coordinate, height - 1);
        } else if (annotation.getAxis() == Annotation.AXIS.Y_AXIS) {
            g.drawLine(width / 2, (int) coordinate, width - 1, (int) coordinate);
        } else {
            throw new UnsupportedOperationException("Cannot render axis in " + annotation.getAxis() + " orientation, only X_AXIS and Y_AXIS annotations may be rasterized at this time.");
        }
    }

    private void drawVerticalCaption(float annotationCoord, double plotCoord, Graphics2D g,
            Font tickFont) {

        String caption;
        TextLayout cString;

        caption = getFormattedPlotCoord(plotCoord);

        AffineTransform af = new AffineTransform();
        FontRenderContext frc = new FontRenderContext(af, false, false);

        cString = new TextLayout(caption, tickFont, frc);
        cString.draw(g, (float) 0, annotationCoord);
    }

    /**
     * Gets the next integer multiple of the largest power of 10 which is less than or equal to
     * the requested tick scale (# of values between adjacent ticks).  For example, getAdjustedTickIncr(50.0) would return 50.0,
     * getAdjustedTickIncr(51.0) would return (60.0) and getadjusetdTickIncr(101.0) would return (200.0).
     *
     * @param tickCoord the minimum number of pixels between adjacent tickmarks
     *
     * @return smallest integer multiple of largest power of 10, >= tickCoord
     */
    private double getAdjustedTickCoord(double tickCoord) {
        //largest integer log base 10 <= minTickScale
        double lowerExpOfTen = Math.floor(Math.log10(tickCoord));
        //largest power of 10 <= minTickScale
        double lowerPowOfTen = Math.pow(10, lowerExpOfTen);
        //smallest integer multiple of the largest power of 10 that is <= minTickScale
        double nextMultipleOfPowOfTen = Math.ceil(tickCoord / lowerPowOfTen);

        //return smallest integer multiple of largest power of 10, <= minTickIncr * scale
        return nextMultipleOfPowOfTen * lowerPowOfTen;
    }

    /**
     * Gets the Annotation which is rendered by this AnnotationRasterizer.
     * 
     * @return Annotation
     */
    public Annotation getAnnotation() {
        return annotation;
    }

    private double getCaptionAngle() {
        WindowAnnotationProperties winAnnoProps = annotatedLayer.getWindowModel().getWindowProperties().getAnnotationProperties();

        //only cross-section layers support configurable annotation caption angle
        if (annotatedLayer.getOrientation() == GeoDataOrder.CROSS_SECTION_ORDER) {
            CrossSectionAnnotationProperties csAnnoProps = (CrossSectionAnnotationProperties) winAnnoProps;
            AnnotationProperties annoProps = csAnnoProps.getAnnotationProperties(annotatedLayer.getLayerName(), annotation.getCaption());
            double captionAngle = annoProps.getAngle();
            //positive angle rotates text counter-clockwise
            return -1.0 * captionAngle * Math.PI / 180.0;
        } else {
            return 0.0;
        }

    }

    private String getFormattedPlotCoord(double vCoord) {
        try {
            return formatter.getFormattedString(vCoord, 2, false);
        } catch (Exception ex) {
            logger.fine("While formatteing vertical coordinate " + vCoord + ", caught: " + ex.getMessage());
            return "?";
        }
    }

    private String getFormattedTickValue(DataObject trace) {
        if (trace.hasTraceHeader()) {
            String headerKey = annotation.getCaption();
            String keyValueString = trace.getTraceHeader().getTraceHeaderFieldValue(headerKey).toString();
            String formattedKeyValueString;
            try {
                Double keyValueDouble = Double.valueOf(keyValueString);
                // for horiz. key tick captions, show up to 2 decimal places but omit trailing zeroes
                formattedKeyValueString = formatter.getFormattedString(keyValueDouble, 2, false);
            } catch (Exception ex) {
                logger.fine("While converting header key '" + headerKey + "' value String '" +
                        keyValueString + "' to Double, caught: " + ex.getMessage() + "; using unformatted String.");
                formattedKeyValueString = keyValueString;
            }

            return formattedKeyValueString;
        } else {
            return "?";
        }
    }

    /**
     * Returns a single (height for horizontal annotations, width for vertical)
     * which should be sufficient to show the full caption, so long as the caption
     * fits between adjacent tick marks at the default tick configuration.  Used to
     * resize the AnnotationLabel when the text is rotated.
     *
     * Uses a simple heuristic of size = Math.min(3.0, 1 + tan(-angle))
     * 
     * @return rasterized image height (horizontal annotation) or width (vertical annotation)
     */
    public int getSize() {
        double theta = Math.abs(getCaptionAngle());
        double tan = Math.tan(theta);
        return (int) Math.ceil(annotationSize * Math.min(3.0, 1 + tan));
    }

    private void rasterizeHorizontalAnnotation(WindowScaleProperties scaleProps,
            BasicLayerProperties basicLayerProps,
            Rectangle visRect, Graphics2D g2d) {

        g2d.setClip(visRect);
        g2d.setColor(Color.WHITE);
        g2d.fill(visRect);

        double horizPixelIncr = scaleProps.getHorizontalPixelIncr() * basicLayerProps.getHorizontalScale();
        annotation.setScale(horizPixelIncr);

        int width = visRect.width;
        int height = visRect.height;
        double minimumXcoord = visRect.getMinX();
        double maximumXcoord = visRect.getMaxX();

        int FONT_SIZE = 10;
        Font tickFont = new Font("Times", Font.PLAIN, FONT_SIZE);
        
        g2d.setColor(Color.BLACK);

        DataObject[] traces = annotatedLayer.getDataObjects();

        boolean prevTickExists = false;
        double prevXcoord = Double.NaN;

        double minTickSpacing = Annotation.DEFAULT_MAJOR_TIC_X_AXIS;

        for (int traceIndex = 0; traceIndex < traces.length; traceIndex++) {
            double xCoord = (traceIndex + 0.5) * horizPixelIncr;

            if (!prevTickExists || xCoord - prevXcoord >= minTickSpacing) {
                if (xCoord >= minimumXcoord - minTickSpacing) {
                    if (xCoord <= maximumXcoord) {
                        drawTick((float) xCoord, g2d,
                                (int) height, (int) width);
                        drawHorizontalCaption((float) xCoord, traces[traceIndex], g2d, tickFont, height);
                    } else {
                        break;
                    }
                }
                prevTickExists |= true;
                prevXcoord = xCoord;
            }
        }

        drawAxis(g2d, visRect);
    }

    private void rasterizeVerticalAnnotation(WindowScaleProperties scaleProps, BasicLayerProperties basicLayerProps,
            Rectangle visRect, Graphics2D g2d) {
        double vertPixelIncr = scaleProps.getVerticalPixelIncr() * basicLayerProps.getVerticalScale();
        int width = visRect.width;
        int height = visRect.height;
        annotation.setScale(vertPixelIncr);
        int FONT_SIZE = 10;
        Font tickFont = new Font("Times", Font.PLAIN, FONT_SIZE);

        double minTickSpacing;
        WindowAnnotationProperties annoProps = annotatedLayer.getWindowModel().getWindowProperties().getAnnotationProperties();
        if (annoProps instanceof CrossSectionAnnotationProperties) {
            CrossSectionAnnotationProperties csAnnoProps = (CrossSectionAnnotationProperties) annoProps;
            if (csAnnoProps.getStepType() == STEP_TYPE.USER_DEFINED) {
                minTickSpacing = csAnnoProps.getMajorStep();
            } else if (annotatedLayer.isModelLayer()) {
                minTickSpacing = Annotation.DEFAULT_MAJOR_TIC_Y_AXIS_MODEL;
            } else {
                minTickSpacing = Annotation.DEFAULT_MAJOR_TIC_Y_AXIS_XSEC;
            }
        } else {
            minTickSpacing = Annotation.DEFAULT_MAJOR_TIC_X_AXIS;
        }

        g2d.setColor(Color.WHITE);
        g2d.fillRect(visRect.x, (int) (visRect.y - minTickSpacing), width, (int) (height + 2 * minTickSpacing));
        g2d.setColor(Color.BLACK);

        GeoFileMetadata metadata = annotatedLayer.getDatasetProperties().getMetadata();

        //layers now hide the implementation which determines whether
        //the dsProps or display params, or some other class (local subset?), holds the vertical range min.
        double minVertCoord = annotatedLayer.getVerticalMin();

        double initialTickCoord;
        if ((annotatedLayer.isHorizonLayer() || annotatedLayer.isModelLayer()) && metadata.getGeoDataOrder() == GeoDataOrder.CROSS_SECTION_ORDER) {
            initialTickCoord = getAdjustedTickCoord(Annotation.DEFAULT_MAJOR_TIC_Y_AXIS_MODEL / vertPixelIncr);
        } else {
            initialTickCoord = getAdjustedTickCoord(Annotation.DEFAULT_MAJOR_TIC_Y_AXIS_XSEC / vertPixelIncr);
        }

        minTickSpacing = initialTickCoord * vertPixelIncr;

        //reset the initial tick coord to the smallest value >= minVertCoord in case of nonzero start time
        initialTickCoord = Math.ceil(minVertCoord / initialTickCoord) * initialTickCoord;

        double initialYcoord = (initialTickCoord - minVertCoord) * vertPixelIncr;

        //Calculate tick positions from the first tick until 1 full tick space past the height of the visible rectangle
        for (double yCoord = initialYcoord;
                yCoord <= initialYcoord + visRect.getMaxY() + minTickSpacing;
                yCoord += minTickSpacing) {
            //but only actually draw the tick if it is within 1 tick space of the visbile rectangle
            if (yCoord >= visRect.getMinY() - minTickSpacing) {
                drawTick((float) yCoord, g2d,
                        (int) height, (int) width);
                drawVerticalCaption((float) yCoord, minVertCoord + (yCoord / vertPixelIncr), g2d, tickFont);
            } else {
                continue;
            }
        }
        drawAxis(g2d, visRect);
    }

    /**
     * Creates an Image based on this AnnotationRasterizer's Annotation, height, width
     * and tickMark fields.
     *
     * @param winProps WindowProperties containing the WindowScaleProperties used to determine location, spacing and captions of Annotation tick marks
     * @param visRect the visible Rectangle which determines the boundaries of the rasterized Annotation
     * @param g2d Graphics2D context used for rendering
     */
    public void rasterize(WindowProperties winProps, Rectangle visRect, Graphics2D g2d) {
        WindowScaleProperties scaleProps = winProps.getScaleProperties();
        BasicLayerProperties basicLayerProps = annotatedLayer.getLayerProperties().getLayerDisplayParameters().getBasicLayerProperties();

        Annotation.AXIS annotationAxis = annotation.getAxis();

        if (annotationAxis == Annotation.AXIS.X_AXIS) {
            g2d.setColor(Color.BLACK);
            rasterizeHorizontalAnnotation(scaleProps, basicLayerProps, visRect, g2d);
        } else if (annotationAxis == Annotation.AXIS.Y_AXIS) {
            g2d.setColor(Color.BLACK);
            rasterizeVerticalAnnotation(scaleProps, basicLayerProps, visRect, g2d);
        } else {
            throw new UnsupportedOperationException("Cannot render axis in " + annotationAxis + " orientation, only X_AXIS and Y_AXIS annotations may be rasterized at this time.");
        }
    }
}