/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.rasterizer;

import com.bhpb.geographics.RenderableImageLayer;
import com.bhpb.geographics.model.HorizonHighlightSettings;
import com.bhpb.geographics.model.HorizonLayer;
import com.bhpb.geographics.model.ViewTarget;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters.SYMBOL_STYLE;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geographics.ui.AbstractGeoPlot;
import com.bhpb.geographics.ui.SparseBufferedImage;
import com.bhpb.geographics.util.ColorMap;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.util.logging.Logger;

/**
 * HorizonXsecRasterizer renders a horizon cross-section line as determined by the visible rectangle,
 * data values, layer display parameters, window scale and whether the associated HorizonLayer is selected,
 * which enables the highlighted apearance.
 */
public class HorizonXsecRasterizer extends AbstractGeoRasterizer {

    private static final Logger logger = Logger.getLogger(HorizonXsecRasterizer.class.toString());
    private HorizonLayer layer;

    /**
     * Constructs a HorizonXsecRasterizer.
     * 
     * @param horizons BhpSuhorizonDataObjects to be rendered by this rasterizer
     * @param layerDisplayParams LayerDisplayParameters containing settings for line thickness, shapes etc.
     * @param rdblImage RenderableImageLayer used by Components to invoke this rasterizer and get SparseBufferedImages.
     * @param summary GeoFileDataSummary containing paremeters which influence rasterization such as RMS
     * @param winProps window properties which influence rasterization, such as scale
     * @param layer the associated HorizonLayer
     */
    HorizonXsecRasterizer(RenderableImageLayer rdblImage,
            HorizonLayer layer,
            AbstractGeoPlot geoPlot) {
        super(rdblImage, geoPlot.getVisibleRect());

        this.layer = layer;
    }

    /**
     * Rasterize the BhpSuHorizonDataObjects into a {@link SparseBufferedImage}.
     * 
     * @return BufferedImage containing the complete horizon plot, or a partial
     * image if rasterization was interrupted.
     */
    synchronized public SparseBufferedImage rasterize(
            Rectangle visRect) {

        if (visRect.getWidth() <= 0 || visRect.getHeight() <= 0) {
            logger.info("visRect has width, height: " + visRect.getWidth() + ", " + visRect.getHeight() + ", skipping rasterization");
            return SparseBufferedImage.NULL;
        }

        WindowProperties winProps = layer.getWindowModel().getWindowProperties();
        double pixelsPerTrace = winProps.getScaleProperties().getPixelsPerTrace();
        double pixelsPerSample = winProps.getScaleProperties().getPixelsPerUnit();

        HorizonHighlightSettings highlightSettings = layer.getHorizonHighlightSettings();

        int viewWidth = (int) Math.min(4096.0, visRect.getWidth());
        int viewHeight = (int) Math.min(2048.0, visRect.getHeight());

        SparseBufferedImage img = new SparseBufferedImage(
                viewWidth,
                viewHeight,
                BufferedImage.TYPE_INT_ARGB,
                visRect.x,
                visRect.y);

        rasterizeXsec(layer.getMutableHorizon(), img, (HorizonXsecDisplayParameters) layer.getDisplayParameters(),
                pixelsPerTrace, pixelsPerSample, visRect.x, visRect.y,
                highlightSettings);

        return img;
    }

    private void rasterizeXsec(float[] mutableHorizon, BufferedImage img,
            HorizonXsecDisplayParameters ldParams, double pixelsPerTrace,
            double pixelsPerUnit, int xOffset, int yOffset,
            HorizonHighlightSettings highlightSettings) {
        Graphics2D g = (Graphics2D) img.getGraphics();
        g.setColor(ColorMap.white_no_alpha);

        g.fill(new Rectangle(xOffset, yOffset, img.getWidth(), img.getHeight()));

        GeneralPath horizonPath = new GeneralPath();
        boolean prevPointNull = true;

        ViewTarget annotationOrigin = ViewTarget.createTargetProps(layer.getWindowModel().getAnnotatedLayer(), 0, 0);
        double rasterOffsetY = annotationOrigin.getVertical();

        for (int i = 0; i < mutableHorizon.length; i++) {
            if (AbstractTraceRasterizer.isNull(mutableHorizon[i])) {
                prevPointNull = true;
                continue;
            }
            float xCoord = (float) ((i - 0.5) * pixelsPerTrace);
            float yCoord = (float) ((mutableHorizon[i] - rasterOffsetY) * pixelsPerUnit);
            if (prevPointNull) {
                horizonPath.moveTo(xCoord, yCoord);
                prevPointNull = false;
            } else {
                horizonPath.lineTo(xCoord, yCoord);
            }
        }

        g.translate(-xOffset, -yOffset);

        if (layer.isActive()) {
            Color highlight = highlightSettings.getLineColor();
            int alpha = highlightSettings.getColorOpacityAsByte();
            highlight = new Color(highlight.getRed(), highlight.getGreen(), highlight.getBlue(), alpha);
            g.setColor(highlight);
            g.setStroke(new BasicStroke(highlightSettings.getLineWidth()));

            g.draw(horizonPath);
        }

        g.setColor(ldParams.getLineColor());
        float[] dash;

        float lineWidth = ldParams.getLineWidth();
        switch (ldParams.getLineStyle()) {
            case SOLID:
                g.setStroke(new BasicStroke(lineWidth, 1, 1, 1));
                break;
            case EMPTY: // per bhpviewer, draw nothing
                return;
            case DOT:
                dash = new float[]{1, 5};
                g.setStroke(new BasicStroke(lineWidth, 1, 1, 1, dash, 0));
                break;
            case DASH:
                dash = new float[]{5, 5};
                g.setStroke(new BasicStroke(lineWidth, 1, 1, 1, dash, 0));
                break;
            case DASH_DOT:
                dash = new float[]{5, 5, 1, 5};
                g.setStroke(new BasicStroke(lineWidth, 1, 1, 1, dash, 0));
                break;
            case DASH_DOT_DOT:
                dash = new float[]{5, 5, 1, 5, 1, 5};
                g.setStroke(new BasicStroke(lineWidth, 1, 1, 1, dash, 0));
                break;
            default:
                logger.warning("Unknown LineStyle: " + ldParams.getLineStyle());
                return;
        }

        g.draw(horizonPath);

        //finally, if the displayParams are configured to draw horizon symbols, draw them
        if (!(ldParams.getDrawSymbol())) {
            return;
        }

        g.setColor(ldParams.getSymbolColor());

        double symbolWidth = ldParams.getSymbolWidth();
        double symbolHeight = ldParams.getSymbolHeight();

        SYMBOL_STYLE symbolStyle = ldParams.getSymbolStyle();

        boolean fillSymbol =
                ldParams.getSolidFill() &&
                (symbolStyle == SYMBOL_STYLE.CIRCLE ||
                symbolStyle == SYMBOL_STYLE.SQUARE ||
                symbolStyle == SYMBOL_STYLE.DIAMOND ||
                symbolStyle == SYMBOL_STYLE.TRIANGLE);

        for (int i = 0; i < mutableHorizon.length; i++) {
            if (AbstractTraceRasterizer.isNull(mutableHorizon[i])) {
                continue;
            }
            double xCoord = (i - 0.5) * pixelsPerTrace;
            double yCoord = (mutableHorizon[i] - rasterOffsetY) * pixelsPerUnit;

            HorizonSymbolPainter.paintSymbol(
                    g,
                    symbolStyle,
                    ldParams,
                    (float) (xCoord - symbolWidth / 2),
                    (float) (yCoord - symbolHeight / 2),
                    (float) symbolWidth,
                    (float) symbolHeight,
                    fillSymbol);
        }
    }
}