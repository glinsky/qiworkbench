/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.rasterizer;

import com.bhpb.geographics.RenderableImageLayer;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.window.WindowScaleProperties;
import com.bhpb.geographics.ui.SparseBufferedImage;
import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.datasystems.UnlimitedKeyRange;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.logging.Logger;

public abstract class AbstractGeoRasterizer extends LayeredRenderableImageProducer {

    /** set by the subclass when a relevant change should cause the tileSet to become stale */
    private boolean datasetPropertiesChanged = false;
    private boolean displayParametersChanged = false;
    
    protected AbstractGeoRasterizer(RenderableImageLayer image, Rectangle visRect) {
        super(image, visRect);
    }

    /**
     * Creates a SparseBufferedImage of type BufferedImage.TYPE_INT_ARGB with
     * width, height, x and y offsets matching the visible Rectangle.
     *
     * @param visRect Rectangle defining the x offset, y offset, width and height
     * of the requested SparseBufferedImage
     *
     * @return new SparseBufferedImage with the requested dimensions
     */
    protected static SparseBufferedImage createSparseBufferedImage(Rectangle visRect) {
        final int type = BufferedImage.TYPE_INT_ARGB;
        return new SparseBufferedImage(visRect.width, visRect.height, type, visRect.x, visRect.y);
    }

    protected int getFirstVisibleTraceIndex(Rectangle visRect, double pixelsPerTrace) {
        return (int) (visRect.getMinX() / pixelsPerTrace);
    }

    /**
     * Get the index of the wiggle which will be visible on the last vertical line
     * of the rightmost visible tile, if such a wiggle exists (the final tile
     * may not be completely filled).
     * 
     * @param visRect the ViewPort of the GeoPlot being rasterized.
     * @return the index of the last visible within within visRect
     */
    protected int getLastVisibleTraceIndex(Rectangle visRect, double pixelsPerTrace, int nTraces) {
        int calculatedLastIndex = (int) (visRect.getMaxX() / pixelsPerTrace);
        return Math.min(nTraces - 1, calculatedLastIndex);
    }

    /**
     * Returns the y coordinate (time/depth) associated with the sample which occurs
     * at or across the lower boundary of the visibe rectangle, or the last actual sample,
     * whichever is less.  Note that depending
     * on the dataset and visible rectangle geometry this may be at the lower boundary,
     * within the visible rectangle, or above the rectangle entirely.
     *
     * The getVisibleTraceSection() method must be used in conjunction with this
     * method for clipping and filling.
     *
     * @param dsProps DatasetProperties
     * @param visRect WindowScaleProperties
     * 
     * @return maximum visible y coordinate
     */
    protected static double getMaxVisibleYcoord(DatasetProperties dsProps, WindowScaleProperties winScaleProps,
            Rectangle visRect, Layer layer) {
        double vertPixelIncr = winScaleProps.getVerticalPixelIncr();
        return Math.min(
                layer.getVerticalMax(),
                Math.ceil(visRect.getMaxY() / vertPixelIncr) + layer.getVerticalMin());
    }

    /**
     * Returns the y coordinate (time/depth) associated with the sample which occurs
     * at or across the upper boundary of the visibe rectangle.  Note that depending
     * on the layer's start time and visible rectangle geometry this may not correspond to an actual sample;
     * getVisibleTraceSection must be used in conjunction with this method.
     *
     * @param dsProps DatasetProperties
     * @param visRect WindowScaleProperties
     *
     * @return minimum visible y coordinate
     */
    protected static double getMinVisibleYcoord(DatasetProperties dsProps, WindowScaleProperties winScaleProps, Rectangle visRect,
            Layer layer) {
        double vertPixelIncr = winScaleProps.getVerticalPixelIncr();
        return Math.floor(visRect.getMinY() / vertPixelIncr);
    }

    /**
     * Calculates the next greater integer after value which
     * is a multiple of the divisor parameter.  If value is itself a multiple of
     * divisor, then it is returned.
     */
    protected int getNextMultiple(int value, int divisor) {
        int remainder = value % divisor;

        if (remainder == 0) {
            return value;
        } else {
            return value + divisor - remainder;
        }

    }

    protected float[][] getValues(DataObject[] traces) {
        float[][] output = new float[traces.length][];
        for (int i = 0; i < traces.length; i++) {
            output[i] = traces[i].getFloatVector();
        }
        return output;
    }

    /**
     * Gets the increment for the dataset's vertical key.
     * 
     * @param dsProperties the DatasetProperties
     * 
     * @return selected vertical key increment or the full key range increment
     * if the vertical key is unlimited
     */
    public static double getVerticalKeyIncr(DatasetProperties dsProperties) {
        ArrayList<String> keyNames = dsProperties.getMetadata().getKeys();
        String vKeyName = keyNames.get(keyNames.size() - 1);
        AbstractKeyRange vKeyRange = dsProperties.getKeyChosenRange(vKeyName);
        double vKeyIncr;
        if (vKeyRange.isUnlimited()) {
            vKeyIncr = Math.round(((UnlimitedKeyRange) vKeyRange).getFullKeyRange().getIncr());
        } else if (vKeyRange.isDiscrete()) {
            vKeyIncr = Math.round(((DiscreteKeyRange) vKeyRange).getIncr());
        } else {
            throw new IllegalArgumentException("Vertical Key Range must be unlimited or discrete");
        }
        return vKeyIncr;
    }

    /**
     * Note that this method does not indicate the vertical coordinate associated with
     * the first sample within the visible rectangle; getMinVisibleYcoord() must be used in
     * conjunction with this method.
     *
     * @param floatVector array of sample data
     * @param dsProps DatasetProperties
     * @param visRect Rectangle
     * @param winScaleProps WindowScaleProperties
     * @param layer Layer
     *
     * @return visible range of samples from floatVector
     */
    protected static float[] getVisibleTraceSection(float[] floatVector, DatasetProperties dsProps, Rectangle visRect,
            WindowScaleProperties winScaleProps, Layer layer) {
        if (!visibleDataExists(dsProps, winScaleProps, visRect, layer)) {
            return new float[0];
        } else {
            double vKeyIncr = getVerticalKeyIncr(dsProps);
            double firstIndexOffset, lastIndexOffset;

            if (layer.getVerticalMin() >= 0.0) {
                firstIndexOffset = -layer.getVerticalMin();
                lastIndexOffset = -layer.getVerticalMin();
            } else {
                firstIndexOffset = 0.0;
                lastIndexOffset = -layer.getVerticalMin();
            }

            int firstIndex = (int) Math.floor((getMinVisibleYcoord(dsProps, winScaleProps, visRect, layer) + firstIndexOffset) / (vKeyIncr));
            int lastIndex = (int) Math.ceil((getMaxVisibleYcoord(dsProps, winScaleProps, visRect, layer) + lastIndexOffset) / (vKeyIncr));

            if (firstIndex >= floatVector.length || lastIndex < 0) {
                return new float[0];
            }

            int firstSrcIndex = Math.max(0, firstIndex);
            int visibleDataCount = Math.min(lastIndex + 1, floatVector.length) - firstSrcIndex;

            float[] clippedVector = new float[visibleDataCount];

            System.arraycopy(floatVector, firstSrcIndex, clippedVector, 0, visibleDataCount);

            return clippedVector;
        }
    }

    /**
     * Gets the time value corresponding to the start of the first visible sample
     * given the visisble rectangle geometry, window scale, layer scale, sample start
     * and sample rate.  If the dataset starts below or ends above the visible rectangle,
     * then the sample start time is returned.
     *
     * @param visRect
     * @param winScaleProps
     * @param layer
     *
     * @return start time of the first visible sample
     */
    protected static double getVisibleTraceStartTime(Rectangle visRect, WindowScaleProperties winScaleProps,
            Layer layer) {
        DatasetProperties dsProps = layer.getDatasetProperties();
        double verticalCoordMin = dsProps.getVerticalCoordMin();

        if (!visibleDataExists(dsProps, winScaleProps, visRect, layer)) {
            return verticalCoordMin;
        } else {
            double vKeyIncr = getVerticalKeyIncr(dsProps);
            int firstIndex = (int) Math.max(
                    0,
                    Math.floor((getMinVisibleYcoord(dsProps, winScaleProps, visRect, layer) - verticalCoordMin) / vKeyIncr));
            return layer.getVerticalMin() + firstIndex * layer.getSampleRate();
        }
    }

    protected boolean isDatasetPropertiesChanged() {
        return datasetPropertiesChanged;
    }

    protected boolean isDisplayParametersChanged() {
        return displayParametersChanged;
    }

    protected void setDatasetPropertiesChanged(boolean datasetPropertiesChanged) {
        this.datasetPropertiesChanged = datasetPropertiesChanged;
    }

    protected void setDisplayParametersChanged(boolean displayParametersChanged) {
        this.displayParametersChanged = displayParametersChanged;
    }

    /**
     * Determines whether the vertical geometry o the visible rectangle and the min, max vertical coordinates
     * according to the DatasetProperties would result in visible data.
     *
     * @param dsProps
     * @param winScaleProps
     * @param visRect
     * @param layer
     *
     * @return true if traces from the requested layer are visible within visRect
     */
    protected static boolean visibleDataExists(DatasetProperties dsProps, WindowScaleProperties winScaleProps, Rectangle visRect,
            Layer layer) {
        double datasetMinY = layer.getVerticalMin();
        double datasetMaxY = layer.getVerticalMax();

        double visRectMinY = getMinVisibleYcoord(dsProps, winScaleProps, visRect, layer);
        double visRectMaxY = getMaxVisibleYcoord(dsProps, winScaleProps, visRect, layer);

        if (visRectMinY > datasetMaxY || visRectMaxY < datasetMinY) {
            return false;
        } else {
            return true;
        }
    }

    protected void logRasterizationAbortedMessage(Logger logger) {
        logger.info("Thread interrupted, aborting rasterization.");
    }
}