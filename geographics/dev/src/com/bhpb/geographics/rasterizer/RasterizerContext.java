/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.rasterizer;

import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.util.XStreamUtil;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.thoughtworks.xstream.XStream;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Node;

/**
 * Context used to translate graphical elements among VIEW, MODEL and eventually,
 * with Well Log layers, WORLD coordinates.
 * 
 * Note that the actual extent of the visible rectangle is unknown by the RasterizerContext2.
 * This is why the zoomAll() method requires a viewPort.
 * However, this allows the same context to synchronize multiple views.
 * 
 * <ul>
 *   <li>VIEW: specifies location in pixels on screen - (0,0) is the upper left corner of the visible rectangle</li>
 *   <li>MODEL: specifies location within dataset - (0,0) is the beginning of the first sample of the first trace</li>
 *   <li>WORLD: specifies location in real world coordinates - requirements to be gathered from Mike Glinsky as of 9/6/2008</li>
 * </ul>
 */
public class RasterizerContext {

    private static final Logger logger =
            Logger.getLogger(RasterizerContext.class.toString());
    private AffineTransform at = new AffineTransform();
    private double viewHeight;
    private double viewOffsetX = 0;
    private double viewOffsetY = 0;
    private double viewScaleX = 1.0;
    private double viewScaleY = 1.0;
    private double viewWidth;
    private transient Layer referenceLayer;

    // the origin of the visible rectangle in MODEL coordinates
    private Point viewOrigin = new Point(0, 0);

    /**
     * Constructs a RasterizerContext.
     *
     * @param referenceLayer
     * @param viewWidth
     * @param viewHeight
     */
    public RasterizerContext(Layer referenceLayer,
            double viewWidth, double viewHeight) {
        this.referenceLayer = referenceLayer;
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;
    }

    public RasterizerContext(RasterizerContext that) {
        this.at = new AffineTransform(that.at);
        this.viewHeight = that.viewHeight;
        this.viewOffsetX = that.viewOffsetX;
        this.viewOffsetY = that.viewOffsetY;
        this.viewScaleX = that.viewScaleX;
        this.viewScaleY = that.viewScaleY;
        this.viewWidth = that.viewWidth;
        this.referenceLayer = that.referenceLayer;
        this.viewOrigin = that.viewOrigin;
    }

    /**
     * Gets the AffineTransform used by this RasterizerContext for translation and scaling.
     *
     * @return AffineTransform
     */
    public AffineTransform getAffineTransform() {
        return at;
    }

    /**
     * Gets the rectangle whose width and height are the view-space dimensions
     * of the entire intial rectangle at the current zoom level.
     *
     * @return entire current view rectangle (not the visible portion only)
     */
    public synchronized Rectangle getCurrentRect() {
        double scaleX = getScaleX();
        double scaleY = getScaleY();
        double currentRectX = viewOrigin.getX() * scaleX;
        double currentRectY = viewOrigin.getY() * scaleY;

        double currentRectWidth = referenceLayer.getWidth() * scaleX;
        double currentRectHeight = referenceLayer.getHeight() * scaleY;

        return new Rectangle((int) Math.round(currentRectX),
                (int) Math.round(currentRectY),
                (int) Math.round(currentRectWidth),
                (int) Math.round(currentRectHeight));
    }

    /**
     * Returns the VIEW coordinate Point in MODEL (visible area) coordinates.
     *
     * @param viewPoint Point in view (trace/sample) coordinates to be translated
     * into (zoomed/scrolled) MODEL coordinates.
     *
     * @return this RasterizerContext2's inverse AffineTransform of viewPoint
     */
    public Point getModelCoordinate(Point viewPoint) {
        Point transformedPoint = new Point();
        try {
            at.inverseTransform(viewPoint, transformedPoint);
        } catch (NoninvertibleTransformException nite) {
            logger.warning(nite.getMessage());
        }
        return transformedPoint;
    }

    public Layer getReferenceLayer() {
        return referenceLayer;
    }

    /**
     * Gets the X scale coefficent, including both zooming in/out and rectangle zooming.
     *
     * @return the X axis scale coefficent, which is always > 0
     */
    public synchronized double getScaleX() {
        return viewScaleX;
    }

    /**
     * Gets the Y scale coefficient, including both zooming in/out and rectangle zooming.
     *
     * @return the Y axis scale coefficent, which is always > 0
     */
    public synchronized double getScaleY() {
        return viewScaleY;
    }

    /**
     * Returns the MODEL coordinate Point in VIEW (visible area) coordinates.
     *
     * @param modelPoint Point in model (trace/sample) coordinates to be translated
     * into (zoomed/scrolled) view coordinates.
     *
     * @return this RasterizerContext2's AffineTransform of modelPoint
     */
    public Point getViewCoordinate(Point modelPoint) {
        Point transformedPoint = new Point();

        at.transform(modelPoint, transformedPoint);

        return transformedPoint;
    }

    /**
     * Gets the current visible Rectangle in VIEW coordinates.  Used by the rasterizer to determine
     * which traces and samples to render;
     *
     * @return the current visible Rectangle
     */
    public synchronized Rectangle getVisibleRectVC() {
        return new Rectangle((int) Math.round(-1 * viewOffsetX), (int) Math.round(-1 * viewOffsetY),
                (int) Math.round(viewWidth), (int) Math.round(viewHeight));
    }

    public static String getXmlAlias() {
        return "rasterizerContext";
    }

    /**
     * Sets this x scale, y scale and zoomlevel of this rasterizer context to that of
     * aRasterizerContext.
     *
     * @param that RasterizerContext used for the source of x scale, y scale and zoom level
     */
    public void inheritFrom(RasterizerContext that) {
        this.viewScaleX = that.viewScaleX;
        this.viewScaleY = that.viewScaleY;
        updateAffineTransform();
    }

    public static RasterizerContext restoreState(Node node) {
        XStream xStream = XStreamUtil.getInstance();
        String xml = ElementAttributeReader.nodeToXMLString(node);

        RasterizerContext rasterizerContext = (RasterizerContext) xStream.fromXML(new StringReader(xml));

        return rasterizerContext;
    }

    /**
     * Sets the x scale, y scale and zoomlevel of this RasterizerContext
     * to 1.0, 1.0 and 0 respectively.
     */
    public void resetZoomLevels(int newWidth, int newHeight) {
        this.viewWidth = newWidth;
        this.viewHeight = newHeight;
        this.viewScaleX = 1.0;
        this.viewScaleY = 1.0;
    }

    public String saveState() {
        StringBuffer content = new StringBuffer();

        XStream xStream = XStreamUtil.getInstance();
        content.append(xStream.toXML(this));
        content.append("\n");

        return content.toString();
    }

    public void setScaleY(double newYscale) {
        this.viewScaleY = newYscale;
    }

    public void setScaleX(double newXscale) {
        this.viewScaleX = newXscale;
    }

    public void setOffsetX(int offset) {
        viewOffsetX = -1 * offset;
        updateAffineTransform();
    }

    public void setOffsetY(int offset) {
        viewOffsetY = -1 * offset;
        updateAffineTransform();
    }

    public synchronized void setSize(Dimension dim) {
        this.viewWidth = dim.getWidth();
        this.viewHeight = dim.getHeight();
    }

    public void sync(RasterizerContext srcRC) {
        synchronized (srcRC) {
            viewScaleX = srcRC.viewScaleX;
            viewScaleY = srcRC.viewScaleY;
            viewOffsetX = srcRC.viewOffsetX;
            viewOffsetY = srcRC.viewOffsetY;
            viewWidth = srcRC.viewWidth;
            viewHeight = srcRC.viewHeight;
        }

        updateAffineTransform();
    }

    /**
     * Returns the string representation of the current visible rectange in world coordinates,
     * in the format origin.X, origin.Y, height, width
     *
     * @return the String representation of the current visible rectangle in MODEL coordinates.
     */
    @Override
    public String toString() {
        return toString(getCurrentRect());
    }

    private String toString(AffineTransform at) {
        return "Scale: " + at.getScaleX() + " " + at.getScaleY() + " " + "Translation: " + at.getTranslateX() + " " + at.getTranslateY();
    }

    private String toString(Rectangle rect) {
        StringBuilder sb = new StringBuilder(Double.toString(rect.getX()));
        sb.append(", ");
        sb.append(rect.getY());
        sb.append(", ");
        sb.append(rect.getWidth());
        sb.append(", ");
        sb.append(rect.getHeight());
        return sb.toString();
    }

    public void translate(int xIncr, int yIncr) {
        viewOffsetX -= xIncr;
        viewOffsetY -= yIncr;
        updateAffineTransform();
    }

    private void updateAffineTransform() {
        at = new AffineTransform();
        at.setToScale(getScaleX(), getScaleY());
        AffineTransform at2 = new AffineTransform();
        at2.setToTranslation(viewOffsetX, viewOffsetY);
        at.concatenate(at2);
        if (logger.isLoggable(Level.FINE)) {
            logger.fine("Updated AffineTransform: " + toString(at));
        }
    }

    /**
     * Resets the Origin to 0,0 in MODEL coordinates and sets the x, y
     * zoom so that the entire intialRect, specified in MODEL coordinates,
     * is within the current visible Rectangle
     * 
     * @param aspectRatioLocked if true, then either the new X or Y scale will be
     * reduced if necessary to preserve the current X : Y scale ratio while also
     * fulfilling the other functionality of zoomAll
     */
    public synchronized void zoomAll(boolean aspectRatioLocked, double hPixelIncr, double vPixelIncr) {
        viewScaleX = this.viewWidth / (referenceLayer.getWidth() * hPixelIncr);
        viewScaleY = this.viewHeight / (referenceLayer.getHeight() * vPixelIncr);
        viewOffsetX = 0;
        viewOffsetY = 0;

        updateAffineTransform();
    }

    /**
     * Multiplies the X and Y scale by 2.0, up to a maximum of 10 times zoomed in.
     */
    public synchronized void zoomIn() {
        viewScaleX *= 2;
        viewScaleY *= 2;
        viewOffsetX *= 2;
        viewOffsetY *= 2;

        updateAffineTransform();
    }

    /**
     * Divides the X and Y scale by 2.0, up to a maximum to 10 times zoomed out.
     */
    public synchronized void zoomOut() {
        viewScaleX /= 2;
        viewScaleY /= 2;
        viewOffsetX /= 2;
        viewOffsetY /= 2;

        updateAffineTransform();
    }

    /**
     * Sets the RasterizerContext2 transformation so that the given Rectangle,
     * specified in VIEW coordinates
     */
    public synchronized void zoomRect(Rectangle zoomRect, boolean aspectRatioLocked) {
        Point newOriginMC = getModelCoordinate(zoomRect.getLocation());

        //NOTE: the width, height of the view itself are unchanged when a rectangular zoom region is selected
        viewScaleX *= viewWidth / (zoomRect.getWidth());
        viewScaleY *= viewHeight / (zoomRect.getHeight());

        viewOffsetX = -1.0 * newOriginMC.getX() * viewScaleX;
        viewOffsetY = -1.0 * newOriginMC.getY() * viewScaleY;

        updateAffineTransform();
    }

    /**
     * Resets the RasterizerContext2 transformation so that the origin is at 0,0
     * (both MODEL and VIEW coordinates) and the scale is 1x the default X and Y scale.
     */
    public synchronized void zoomReset() {
        logger.info("Resetting zoom level to 1.0x & view origin to (0,0)");
        viewScaleX = 1.0;
        viewScaleY = 1.0;
        updateAffineTransform();
    }

    public void setReferenceLayer(Layer newReferencLayer) {
        this.referenceLayer = newReferencLayer;
    }
}