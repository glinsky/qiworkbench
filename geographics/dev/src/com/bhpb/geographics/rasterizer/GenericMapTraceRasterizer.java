/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.rasterizer;

import com.bhpb.geographics.util.ColorMap;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Rasterizer for BHP-SU map (transpose) datasets: Seismic, Horizon and Model.
 */
public class GenericMapTraceRasterizer extends AbstractTraceRasterizer {

    private static void drawDensityTrace(float xBase,
            Graphics2D g,
            List<Point2D> densityTrace,
            float prevXbase,
            ColorMap colorMap) {

        for (int index = 0; index < densityTrace.size(); index++) {

            float normalizedSample = (float) densityTrace.get(index).getX();

            float start;
            if (index == 0) {
                start = 0.0f;
            } else {
                start = (float) densityTrace.get(index - 1).getY();
            }
            float end = (float) densityTrace.get(index).getY();

            if (isNull(normalizedSample)) {
                continue;
            } else {
                GeneralPath path = new GeneralPath();
                path.moveTo(prevXbase, start);
                path.lineTo(xBase, start);
                path.lineTo(xBase, end);
                path.lineTo(prevXbase, end);
                path.lineTo(prevXbase, start);

                g.setColor(colorMap.getColor(ColorMap.RAMP, normalizedSample));
                g.fill(path);
            }
        }
    }

    private static List<Point2D> getDensityTrace(float[] values,
            float pixelsPerSample,
            double colorMapMin,
            double colorMapMax) {

        List<Point2D> wigglePath = new ArrayList<Point2D>();

            for (int i = 0; i < values.length; i++) {
                float normalizedSample = normalizeByLimits(values[i],
                        colorMapMin,
                        colorMapMax);
                wigglePath.add(
                        new Point2D.Float(
                        normalizedSample,
                        (int) (i * pixelsPerSample)));
            }

        return wigglePath;
    }

    /**
     * Rasterizes the array of floating point values as a vertical series 
     * of rectangles colored according to the associated ColorMap and values
     * normalized by the datset limits.
     *
     * @param values float[] of normalized values
     * @param xBase x coordinate of trace
     * @param prevXbase x coordinate of previous visible trace
     * @param pixelsPerTraceV vertical pixels per trace scale
     * @param colorMapMin configurable value for colormap normalization minimum
     * @param colorMapMax configurable value for colormap normalization maximum
     * @param g Graphics2D context
     */
    public static void rasterizeDensity(float[] values,
            float xBase,
            float prevXbase,
            double pixelsPerTraceV,
            ColorMap colorMap,
            double colorMapMin,
            double colorMapMax,
            Graphics2D g) {

        List<Point2D> densityTrace = getDensityTrace(values,
                (float) pixelsPerTraceV,
                colorMapMin,
                colorMapMax);

        drawDensityTrace(xBase,
                g,
                densityTrace,
                prevXbase,
                colorMap);
    }
}