/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.rasterizer;

import com.bhpb.geographics.model.layer.shared.Normalization;
import com.bhpb.geographics.model.layer.shared.Normalization.NORMALIZATION_TYPE;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoFileType;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.util.logging.Logger;

public abstract class AbstractTraceRasterizer {
    private static final Logger logger =
            Logger.getLogger(AbstractTraceRasterizer.class.getName());
    
    public static float getMaxAbs(float[] values) {
        float maxAbs = Math.abs(values[0]);
        for (int i = 1; i < values.length; i++) {
            float nextAbs = Math.abs(values[i]);
            if (nextAbs > maxAbs) {
                maxAbs = nextAbs;
            }
        }
        return maxAbs;
    }

    public static Area getNegativeMask(float xBase, float yBase, float width, float height) {
        return new Area(new Rectangle2D.Float(xBase - width, yBase, width, height));
    }

    public static Area getPositiveMask(float xBase, float yBase, float width, float height) {
        return new Area(new Rectangle2D.Float(xBase, yBase, width, height));
    }

    /**
     * Returns true if the floating point value exactly matches one of the two
     * BHP-SU 'null' values: -999.0f or -999.25f or if Float.isNaN(value) is true;
     * 
     * @param value a float
     * 
     * @return true if and only if value is equal to -999.0f or -999.25f
     */
    public static boolean isNull(float value) {
        return value == -999.0f || value == -999.25f || Float.isNaN(value);
    }

    /**
     * Normalize the array of floats based on the settings of normalization and
     * the properties of summary.
     * 
     * @param input array of floating-points values to normalize
     * @param normalization Normalization properties including type and possible min and max
     * @param summary GeoFileSummary containing statistics such as RMS
     * 
     * @return a new float[] containing normalized values or a copy of the original input
     * array if the normalization type was not RMS
     */
    static float[] normalize(float[] input, Normalization normalization,
            GeoFileDataSummary summary, GeoFileType fileType) {
        if (input == null || input.length == 0) {
            throw new IllegalArgumentException("float[] input must be non-null and have non-zero length");
        }

        float[] output = new float[input.length];

        NORMALIZATION_TYPE normType = normalization.getNormalizationType();
        double normScale = normalization.getNormalizationScale();

        switch (normType) {
            case RMS:
                double rms = summary.getRMS();

                for (int j = 0; j < output.length; j++) {
                    output[j] = (float) (input[j] * normScale / rms);
                }
                break;
            case LIMITS:
                double min = normalization.getNormalizationMinValue();
                double max = normalization.getNormalizationMaxValue();

                int nValuesToNormalize;

                if (fileType == GeoFileType.MODEL_GEOFILE) {
                    nValuesToNormalize = (int)(input.length - 1) / 2;
                } else {
                    nValuesToNormalize = output.length;
                }
                
                for (int j = 0; j < nValuesToNormalize; j++) {
                    output[j] = (float)(normalizeByLimits(input[j], min, max) * normScale);
                }
                if (nValuesToNormalize != input.length) {
                    System.arraycopy(input, nValuesToNormalize, output, nValuesToNormalize, input.length - nValuesToNormalize);
                }
                break;
            default:
                logger.warning("Unsupported normalization mode: " + normType + ", rasterizing original values.");
                System.arraycopy(input, 0, output, 0, input.length);
        }

        return output;
    }

    /**
     * 
     * Normalize the value to fall within the range [-1.0, 1.0], using
     * the minAmplitude and maxAmplitude values from the GeoFileSummary.
     * 
     * If value is one of -999.0f, -999.25f, it is returned unchanged.
     * 
     * Buf fix: positive and negative values are no longer normalized independently;
     * a value exactly at the midpoint between min and maxAmplitude will normalize
     * to 0.0f.
     * 
     * @param value the float to be normalized
     * @param minAmplitude the value which will equal -1.0f after normalization
     * @param maxAmplitude the value which will equal 1.0f after normalization
     */
    protected static float normalizeByLimits(float value, double minAmplitude, double maxAmplitude) {
        if (isNull(value)) {
            return value;
        }
        if (value < minAmplitude) {
            return -1.0f;
        }
        if (value > maxAmplitude) {
            return 1.0f;
        }
        return (float) (2 * (value - minAmplitude) / (maxAmplitude - minAmplitude) - 1.0);
    }
}