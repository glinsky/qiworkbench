/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.rasterizer;

import com.bhpb.geographics.RenderableImageLayer;
import com.bhpb.geographics.model.ModelLayer;
import com.bhpb.geographics.model.layer.display.ModelXsecDisplayParameters;
import com.bhpb.geographics.model.layer.shared.BasicLayerProperties;
import com.bhpb.geographics.model.layer.shared.Normalization;
import com.bhpb.geographics.model.window.WindowScaleProperties;
import com.bhpb.geographics.ui.SparseBufferedImage;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoFileType;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.List;
import java.util.logging.Logger;

public class ModelXsecRasterizer extends AbstractGeoRasterizer {

    private static Logger logger = Logger.getLogger(ModelXsecRasterizer.class.toString());
    private ModelLayer layer;
    private volatile Thread activeRenderingThread = null;

    /**
     * Constructs a ModelXsecRasterizer.
     *
     * @param rdblImage
     * @param visRect
     * @param layer
     */
    public ModelXsecRasterizer(RenderableImageLayer rdblImage,
            Rectangle visRect,
            ModelLayer layer) {
        super(rdblImage, visRect);
        this.layer = layer;
    }

    private synchronized SparseBufferedImage getNewImage(
            Rectangle visRect,
            ModelXsecDisplayParameters ldParams,
            double pixelsPerTrace,
            double pixelsPerUnit,
            double sampleStart) {
        SparseBufferedImage newImage = createSparseBufferedImage(visRect);
        Graphics2D g = (Graphics2D) newImage.getGraphics();
        g.translate(-visRect.x, -visRect.y);

        //since this is a model xsec, the height is (misnamed) summary.getSampleRate() - summary.getSampleStart,
        //and is NOT related to the number of samples per trace
        if (ldParams.getRasterizationTypeModel().doRasterizeVariableDensity()) {
            rasterizeDensity(visRect, ldParams, pixelsPerTrace, pixelsPerUnit, g, sampleStart);
        }
        if (ldParams.getRasterizationTypeModel().doRasterizeWiggleTraces()) {
            rasterizeWiggles(visRect, ldParams, pixelsPerTrace, pixelsPerUnit, g, sampleStart);
        }

        return newImage;
    }

    /**
     * Rasterize the Model DataObjects into a BufferedImage.
     * 
     * @param visRect the current visible rectangel of the GeoPlot for which
     * 
     * @return BufferedImage containing the complete Model plot, or a partial
     * image if rasterization was interrupted.
     */
    synchronized public SparseBufferedImage rasterize(
            Rectangle visRect) {
        Thread currentThread = Thread.currentThread();
        activeRenderingThread = currentThread;

        if (visRect.getWidth() <= 0 || visRect.getHeight() <= 0) {
            logger.info("visRect has width, height: " + visRect.getWidth() + ", " + visRect.getHeight() + ", skipping rasterization");
            return SparseBufferedImage.NULL;
        }

        final SparseBufferedImage modelImage;

        WindowScaleProperties winScaleProps = layer.getWindowModel().getWindowProperties().getScaleProperties();
        ModelXsecDisplayParameters layerDisplayParams = (ModelXsecDisplayParameters) layer.getDisplayParameters();
        BasicLayerProperties basicLayerProps = layerDisplayParams.getBasicLayerProperties();

        double pixelsPerTrace = winScaleProps.getHorizontalPixelIncr() * basicLayerProps.getHorizontalScale();

        //this should always be 1.0 for model xsec but leaving it here will allow promoting rasterizer() to parent class
        double vKeyIncr = getVerticalKeyIncr(layer.getDatasetProperties());
        /* px/sample = px/units * units/sample */
        //because unitspersample is always 1.0 (meaningless) for xsec models,ayerDisplayParams
        //this variable should actually be called pixelsPerUnit as that is how ModelXsecRasterizer uses it
        double pixelsPerSample = winScaleProps.getVerticalPixelIncr() * vKeyIncr * basicLayerProps.getVerticalScale();

        //Bug fix: use model vertical range properties, no the dataset's defaults;
        //Should also use ModelVerticalRange to calculate the full layer height.
        //double sampleStart = dsProperties.getVerticalCoordMin();
        double sampleStart = layerDisplayParams.getModelVerticalRange().getStartTime();

        modelImage = getNewImage(visRect, (ModelXsecDisplayParameters) layerDisplayParams, pixelsPerTrace, pixelsPerSample,
                sampleStart);

        return modelImage;
    }

    private void rasterizeDensity(Rectangle visRect,
            ModelXsecDisplayParameters ldParams,
            double pixelsPerTrace,
            double pixelsPerUnit,
            Graphics2D g,
            double sampleStart) {
        float lastRenderedTraceCoord = 0.0f;

        int firstTraceToRender = getFirstVisibleTraceIndex(visRect, pixelsPerTrace);
        Thread currentThread = Thread.currentThread();
        List<Point2D> prevDensityTrace = null;
        double EPSILON = 0.1;

        float[] lastRenderedTrace = null;
        Normalization normalization = ldParams.getNormalizationProperties();

        GeoFileDataSummary summary = layer.getDatasetProperties().getSummary();
        for (int wIndex = firstTraceToRender; wIndex <=
                getLastVisibleTraceIndex(visRect, pixelsPerTrace, summary.getNumberOfTraces()); wIndex++) {
            if (activeRenderingThread != currentThread) {
                break;
            }
            //by default, render a trace at every other pixel starting at #1, not 0
            float xPixel = (float) Math.round((wIndex + 1) * pixelsPerTrace - 1);
            //if the rounded xPixel is < 1/10 pixel from the previous rendered coordinate
            if (Math.abs(xPixel - lastRenderedTraceCoord) < EPSILON) {
                continue;
            }
            //The previous density trace is used for structural interpolation during
            //rendering, and the new density trace is returned after rendering for use
            //while rendering the next trace, and so on...
            float[] currentTrace = layer.getFloatVector(wIndex);

            currentTrace = ModelXsecTraceRasterizer.normalize(currentTrace, normalization, summary, GeoFileType.MODEL_GEOFILE);

            prevDensityTrace = ModelXsecTraceRasterizer.rasterizeDensity(
                    currentTrace,
                    xPixel,
                    lastRenderedTrace,
                    lastRenderedTraceCoord,
                    pixelsPerTrace,
                    pixelsPerUnit,
                    ldParams,
                    prevDensityTrace,
                    visRect,
                    sampleStart,
                    g);
            lastRenderedTraceCoord = xPixel;
            lastRenderedTrace = currentTrace;
        }
    }

    /**
     * Although it is not the default and is not commonly used, the design inherited from the 
     * bhpViewer allows the ModelXsec layer to be rendered using wiggle traces, with or
     * without positive/negative fill, as well as using the default, VARIABLE_DENSITY.
     * 
     * @param visRect
     * @param ldParams
     * @param scaleX
     * @param scaleY
     */
    private void rasterizeWiggles(Rectangle visRect,
            ModelXsecDisplayParameters ldParams,
            double pixelsPerTrace,
            double pixelsPerUnit,
            Graphics2D g,
            double sampleStart) {
        float lastRenderedTraceCoord = Float.NaN; // this will be ignored the first time by

        int firstTraceToRender = getFirstVisibleTraceIndex(visRect, pixelsPerTrace);
        Thread currentThread = Thread.currentThread();
        GeoFileDataSummary summary = layer.getDatasetProperties().getSummary();
        double wiggleDecimation = ldParams.getCulling().getDecimationSpacing();

        for (int wIndex = firstTraceToRender; wIndex <=
                getLastVisibleTraceIndex(visRect, pixelsPerTrace, summary.getNumberOfTraces()); wIndex++) {
            if (activeRenderingThread != currentThread) {
                break;
            }

            //by default, render a trace at every other pixel starting at #1, not 0
            float xPixel = (float) ((wIndex + 1) * pixelsPerTrace - 1);
            //and only if the distance from the center of the first pixel
            //to this one is divisible by the wiggle decimation value
            //otherwise, skip it

            if ((wIndex == firstTraceToRender) || (xPixel - lastRenderedTraceCoord > wiggleDecimation)) {
                //model xsec does not honor wiggledecimation ? it simply renders 1 trace per vertical pixel
                //if ((wIndex == firstTraceToRender) || (xPixel > lastRenderedTraceCoord)) {
                lastRenderedTraceCoord = xPixel;
                ModelXsecTraceRasterizer.rasterizeWiggle(
                        layer.getFloatVector(wIndex),
                        xPixel,
                        pixelsPerTrace,
                        pixelsPerUnit,
                        ldParams,
                        visRect,
                        g,
                        sampleStart);
            }

        }
    }
}