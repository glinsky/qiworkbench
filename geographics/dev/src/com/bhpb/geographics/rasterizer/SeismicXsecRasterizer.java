/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.rasterizer;

import com.bhpb.geographics.RenderableImageLayer;
import com.bhpb.geographics.model.SeismicLayer;
import com.bhpb.geographics.model.layer.shared.Normalization;
import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParameters;
import com.bhpb.geographics.model.layer.shared.AutoGainControl;
import com.bhpb.geographics.model.layer.shared.BasicLayerProperties;
import com.bhpb.geographics.model.layer.shared.InterpolationSettings;
import com.bhpb.geographics.model.window.WindowScaleProperties;
import com.bhpb.geographics.ui.SparseBufferedImage;
import com.bhpb.geographics.util.Interpolator;
import com.bhpb.geographics.util.LinearInterpolator;
import com.bhpb.geographics.util.QuadraticInterpolator;
import com.bhpb.geographics.util.StepInterpolator;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoFileType;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class SeismicXsecRasterizer extends AbstractGeoRasterizer {

    private static Logger logger = Logger.getLogger(SeismicXsecRasterizer.class.toString());
    private SeismicLayer layer;
    
    public SeismicXsecRasterizer(SeismicLayer layer,
            RenderableImageLayer rdblImage,
            Rectangle visRect) {
        super(rdblImage, visRect);

        this.layer = layer;
    }

    /**
     * Rasterizer the Seismic DataObjects into a BufferedImage
     * 
     * @param visRect the current visible rectangel of the GeoPlot for which
     *
     * @return SparseBufferedImage containing the complete seismic plot, or a partial
     * image if rasterization was interrupted.
     */
    synchronized public SparseBufferedImage rasterize(Rectangle visRect) {
        if (visRect.getWidth() <= 0 || visRect.getHeight() <= 0) {
            logger.info("visRect has width, height: " + visRect.getWidth() + ", " + visRect.getHeight() + ", skipping rasterization");
            return SparseBufferedImage.NULL;
        }

        WindowScaleProperties winScaleProps = layer.getWindowModel().getWindowProperties().getScaleProperties();
        SeismicXsecDisplayParameters layerDisplayParams = (SeismicXsecDisplayParameters)layer.getDisplayParameters();
        BasicLayerProperties basicLayerProps = layerDisplayParams.getBasicLayerProperties();

        double pixelsPerTrace = winScaleProps.getHorizontalPixelIncr() * basicLayerProps.getHorizontalScale();

        DatasetProperties dsProperties = layer.getDatasetProperties();
        double vKeyIncr = getVerticalKeyIncr(dsProperties);
        /* px/sample = px/units * units/sample */
        double pixelsPerSample = winScaleProps.getVerticalPixelIncr() * vKeyIncr * basicLayerProps.getVerticalScale();

        if (!Thread.currentThread().isInterrupted()) {
            return getNewImage(visRect, layerDisplayParams, pixelsPerTrace, pixelsPerSample,
                    layerDisplayParams.getNormalizationProperties(), layer.getDatasetProperties(),
                    layerDisplayParams.getAutoGainControl());
        } else {
            logRasterizationAbortedMessage(logger);
            return SparseBufferedImage.NULL;
        }
    }

    /** Rasterizes into any uncached tiles and sets the cache flags for any new tiles.
     * Returns the relevant SparseBufferedImage for the specified visible rectangle.
     *
     * @param visRect
     * @param ldParams
     * @param pixelsPerTrace
     * @param pixelsPerSample
     * @param normalization
     * @param summary
     * @param traces
     * @return
     */
    private synchronized SparseBufferedImage getNewImage(
            Rectangle visRect,
            SeismicXsecDisplayParameters ldParams,
            double pixelsPerTrace,
            double pixelsPerSample,
            Normalization normalization,
            DatasetProperties dsProps,
            AutoGainControl agc) {
        Thread currentThread = Thread.currentThread();

        SparseBufferedImage newImage = createSparseBufferedImage(visRect);

        Graphics2D g = (Graphics2D) newImage.getGraphics();
        g.translate(-visRect.x, -visRect.y);

        if (ldParams.getRasterizationTypeModel().doRasterizeVariableDensity()) {
            if (!currentThread.isInterrupted()) {
                rasterizeTraces(visRect, ldParams, pixelsPerTrace, pixelsPerSample, normalization,
                        dsProps, RASTER_TYPE.VARIABLE_DENSITY, g, agc);
            } else {
                logRasterizationAbortedMessage(logger);
                return SparseBufferedImage.NULL;
            }
        }

        if (ldParams.getRasterizationTypeModel().doRasterizeInterpolatedDensity()) {
            if (!currentThread.isInterrupted()) {
                rasterizeTraces(visRect, ldParams, pixelsPerTrace, pixelsPerSample, normalization,
                        dsProps, RASTER_TYPE.INTERPOLATED_DENSITY, g, agc);
            } else {
                logRasterizationAbortedMessage(logger);
                return SparseBufferedImage.NULL;
            }
        }

        if (ldParams.getRasterizationTypeModel().doRasterizeWiggleTraces()) {
            if (!currentThread.isInterrupted()) {
                rasterizeTraces(visRect, ldParams, pixelsPerTrace, pixelsPerSample, normalization,
                        dsProps, RASTER_TYPE.WIGGLE_TRACE, g, agc);
            } else {
                logRasterizationAbortedMessage(logger);
                return SparseBufferedImage.NULL;
            }
        }

        return newImage;
    }

    private void rasterizeTraces(Rectangle visRect,
            SeismicXsecDisplayParameters ldParams,
            double pixelsPerTrace,
            double pixelsPerSample,
            Normalization normalization,
            DatasetProperties dsProps,
            RASTER_TYPE traceType,
            Graphics2D g,
            AutoGainControl agc) {
        double wiggleDecimation = ldParams.getCulling().getDecimationSpacing();
        double traceSeparation = getVisibleWiggleSeparation(pixelsPerTrace, wiggleDecimation);

        GeoFileDataSummary summary = dsProps.getSummary();

        int firstTraceToRender = getFirstVisibleTraceIndex(visRect, pixelsPerTrace);
        int lastTraceToRender = getLastVisibleTraceIndex(visRect, pixelsPerTrace, layer.getDatasetProperties().getSummary().getNumberOfTraces());

        double normScale = ldParams.getNormalizationProperties().getNormalizationScale();

        List<Integer> tracesToRasterize = new ArrayList<Integer>();

        float lastRenderedTraceCoord = Float.NaN; // this will be ignored the first time by
        for (int wIndex = firstTraceToRender; wIndex <= lastTraceToRender; wIndex++) {
            float xPixel = (float) (pixelsPerTrace * (wIndex + 0.5));

            if ((wIndex == firstTraceToRender) || (xPixel - lastRenderedTraceCoord > wiggleDecimation)) {
                tracesToRasterize.add(wIndex);
                lastRenderedTraceCoord = xPixel;
            }
        }

        float[] normalizedInterpolatedValues = null;
        float[] nextNormalizedInterpolatedValues = null;

        //iterate over the visible, non-decimated traces and rasterize them,
        //retaining references to the previous and next traces as necessary for
        //density rendering modes, while avoiding unnecessary recalculation of
        //previously normalized values

        //sampleRate <1.0 = subsampling, 1.0 = no interpolation, >1.0 = interpolated data points
        Interpolator interpolator;
        InterpolationSettings.TYPE interpType = ldParams.getInterpolationSettings().getType();

        switch (interpType) {
            case LINEAR:
                interpolator = new LinearInterpolator();
                break;
            case QUADRATIC:
                interpolator = new QuadraticInterpolator();
                break;
            case STEP:
                interpolator = new StepInterpolator();
                break;
            default:
                throw new IllegalArgumentException("Unknown interpolation type requested: " + interpType);
        }

        List<Point2D> prevDensityTrace = null;
        List<Point2D> densityTrace = null;
        List<Point2D> nextDensityTrace = null;

        WindowScaleProperties winScaleProps = layer.getWindowModel().getWindowProperties().getScaleProperties();

        //y coordinate at which the first sample of this layer starts
        double layerYoffset = layer.getVerticalOffset() * winScaleProps.getVerticalPixelIncr();
        //y coordinate at which the first _visible_ sample of this layer starts
        //double visRectYoffset = getMinVisibleYcoord(dsProps, winScaleProps, visRect, layer);
        double vertPixelIncr = winScaleProps.getVerticalPixelIncr();
        double sampleRate = layer.getSampleRate();
        int visRectYoffset = (int)Math.round(sampleRate * vertPixelIncr * Math.floor(visRect.getMinY()/(vertPixelIncr *sampleRate)));

        for (int wIndex = 0; wIndex < tracesToRasterize.size(); wIndex++) {
            if (Thread.currentThread().isInterrupted()) {
                break;
            }

            int traceIndex = tracesToRasterize.get(wIndex);

            int nextIndex;

            if (wIndex < tracesToRasterize.size() - 1) {
                nextIndex = tracesToRasterize.get(wIndex + 1);
            } else {
                nextIndex = wIndex;
            }

            float xPixel = (float) (pixelsPerTrace * (traceIndex + 0.5));

            //only trace and nextTrace are ever created; prevTrace is always
            //from the previous iteration and is initially set to current trace
            //prevNormalizedValues array is instantiated within the loop
            //because it is never reused, although normalizedValues and nextNormalizedValues may be
            float[] trace, nextTrace;

            //general case: this is not the first trace, copy the trace which
            //was normalized during the previous iteration becomes 'prevNormalizedValues'
            //and the trace which was 'next' during the previous iteration becomes
            //'normalizedValues'

            if (wIndex != 0) {
                normalizedInterpolatedValues = nextNormalizedInterpolatedValues;

                nextTrace = layer.getFloatVector(nextIndex);
                nextNormalizedInterpolatedValues = SeismicXsecTraceRasterizer.normalize(nextTrace,
                        normalization, summary, GeoFileType.SEISMIC_GEOFILE);

                if (agc.doApplyAutoGainControl()) {
                    nextNormalizedInterpolatedValues = AutoGainController.doAGC(
                            nextNormalizedInterpolatedValues, 0,
                            nextNormalizedInterpolatedValues.length - 1, agc,
                            normScale, layer.getSampleRate());
                }

                nextNormalizedInterpolatedValues = getVisibleTraceSection(nextNormalizedInterpolatedValues, 
                        dsProps, visRect, winScaleProps, layer);

                if (pixelsPerSample > 1.0) {
                    nextNormalizedInterpolatedValues = interpolator.interpolate(nextNormalizedInterpolatedValues, pixelsPerSample);
                } else if (pixelsPerSample < 1.0) {
                    nextNormalizedInterpolatedValues = interpolator.subSample(nextNormalizedInterpolatedValues, pixelsPerSample);
                } //else do nothing if number of pixels per sample is == 1.0

                prevDensityTrace = densityTrace;
                densityTrace = nextDensityTrace;

                if (traceType.equals(RASTER_TYPE.INTERPOLATED_DENSITY) || traceType.equals(RASTER_TYPE.VARIABLE_DENSITY)) {
                    nextDensityTrace =
                            SeismicXsecTraceRasterizer.getDensityTrace(nextNormalizedInterpolatedValues,
                            visRect.y);
                }
            } else {
                //initial case: normalize the first trace to be rasterized
                //normalize it and assign it to prevNormalizedValues and normalizedValues
                trace = layer.getFloatVector(traceIndex);
                
                normalizedInterpolatedValues = SeismicXsecTraceRasterizer.normalize(trace,
                        normalization, summary, GeoFileType.SEISMIC_GEOFILE);
                if (agc.doApplyAutoGainControl()) {
                    normalizedInterpolatedValues = AutoGainController.doAGC(
                            normalizedInterpolatedValues, 0,
                            normalizedInterpolatedValues.length - 1, agc, normScale, layer.getSampleRate());
                }
                normalizedInterpolatedValues = getVisibleTraceSection(normalizedInterpolatedValues, dsProps, visRect, winScaleProps,
                        layer);

                nextTrace = layer.getFloatVector(nextIndex);
                
                nextNormalizedInterpolatedValues = SeismicXsecTraceRasterizer.normalize(nextTrace,
                        normalization, summary, GeoFileType.SEISMIC_GEOFILE);

                if (agc.doApplyAutoGainControl()) {
                    nextNormalizedInterpolatedValues = AutoGainController.doAGC(
                            nextNormalizedInterpolatedValues, 0,
                            nextNormalizedInterpolatedValues.length - 1, agc, normScale, layer.getSampleRate());
                }

                nextNormalizedInterpolatedValues = getVisibleTraceSection(nextNormalizedInterpolatedValues, dsProps, visRect, 
                        winScaleProps, layer);

                if (pixelsPerSample > 1.0) {
                    normalizedInterpolatedValues = interpolator.interpolate(normalizedInterpolatedValues, pixelsPerSample);
                    nextNormalizedInterpolatedValues = interpolator.interpolate(nextNormalizedInterpolatedValues, pixelsPerSample);
                } else if (pixelsPerSample < 1.0) {
                    normalizedInterpolatedValues = interpolator.subSample(normalizedInterpolatedValues, pixelsPerSample);
                    nextNormalizedInterpolatedValues = interpolator.subSample(nextNormalizedInterpolatedValues, pixelsPerSample);
                }

                if (traceType.equals(RASTER_TYPE.INTERPOLATED_DENSITY) || traceType.equals(RASTER_TYPE.VARIABLE_DENSITY)) {

                    densityTrace =
                            SeismicXsecTraceRasterizer.getDensityTrace(normalizedInterpolatedValues,
                            visRect.y);
                    prevDensityTrace = densityTrace;

                    nextDensityTrace =
                            SeismicXsecTraceRasterizer.getDensityTrace(nextNormalizedInterpolatedValues,
                            visRect.y);
                }
            }

            switch (traceType) {
                case WIGGLE_TRACE:
                case POS_FILL:
                case NEG_FILL:
                    SeismicXsecTraceRasterizer.rasterizeWiggle(normalizedInterpolatedValues,
                            xPixel,
                            Math.max(1.0f, pixelsPerTrace),
                            pixelsPerSample,
                            traceSeparation,
                            ldParams,
                            visRect,
                            normScale,
                            interpType,
                            g,
                            //this is currently necessary for seismic layers which do have a negative startTime
                            Math.max(layerYoffset,visRectYoffset));
                    break;
                case VARIABLE_DENSITY:
                    SeismicXsecTraceRasterizer.rasterizeVariableDensity(normalizedInterpolatedValues,
                            xPixel,
                            traceSeparation,
                            ldParams,
                            visRect,
                            g);
                    break;
                case INTERPOLATED_DENSITY:
                    SeismicXsecTraceRasterizer.rasterizeInterpolatedDensity(
                            xPixel,
                            traceSeparation,
                            ldParams,
                            visRect,
                            prevDensityTrace,
                            densityTrace,
                            nextDensityTrace,
                            g);
                    break;
                default:
                    throw new UnsupportedOperationException("Unknown traceType: " + traceType);
            }
        }
    }

    static double getVisibleWiggleSeparation(double pixelsPerTrace,
            double wiggleDecimation) {
        //if scaled PPT < wiggleDecimation, return the distance between two visible wiggles
        if (pixelsPerTrace < wiggleDecimation) {
            //return Math.ceil(wiggleDecimation / pixelsPerTrace) * pixelsPerTrace;
            return Math.ceil(wiggleDecimation / pixelsPerTrace) * pixelsPerTrace;
        //otherwise, every trace should be visible, simply return the scaled pixelsPerTrace
        } else {
            return pixelsPerTrace;
        }
    }
}