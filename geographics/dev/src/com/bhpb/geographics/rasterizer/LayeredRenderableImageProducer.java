/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.rasterizer;

import com.bhpb.geographics.LayeredImageConsumer;
import com.bhpb.geographics.LayeredImageProducer;
import com.bhpb.geographics.RenderableImageLayer;
import com.bhpb.geographics.ui.SparseBufferedImage;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class LayeredRenderableImageProducer implements LayeredImageProducer, Runnable {

    public enum RASTER_TYPE {

        WIGGLE_TRACE, POS_FILL, NEG_FILL, VARIABLE_DENSITY, INTERPOLATED_DENSITY
    };
    private final ArrayList consumers = new ArrayList();
    private Rectangle visRect;
    private RenderableImageLayer image;

    public LayeredRenderableImageProducer(RenderableImageLayer image, Rectangle visRect) {
        this.image = image;
        this.visRect = visRect;
    }

    public void addConsumer(LayeredImageConsumer consumer) {
        synchronized (consumers) {
            if (!consumers.contains(consumer)) {
                consumers.add(consumer);
            }
        }
    }

    public RenderableImageLayer getImage() {
        return image;
    }

    public boolean isConsumer(LayeredImageConsumer consumer) {
        synchronized (consumers) {
            return consumers.contains(consumer);
        }
    }

    public void removeConsumer(LayeredImageConsumer consumer) {
        synchronized (consumers) {
            consumers.remove(consumer);
        }
    }

    /**
     * In a Thread-safe manner, invokes the createDefaultRendering method of
     * this LayeredRenderableImageProducer's LayeredRenderableImage, sending 
     * the resulting SparseBufferedImage to each LayeredImageConsumer in
     * this producer's list of consumers.
     */
    public void run() {
        // This isn't ideal but it avoids fail-fast problems.
        // Alternatively, we could clone 'consumers' here or use an iterator.
        synchronized (consumers) {
            SparseBufferedImage newImage;
            newImage = image.createDefaultRendering(visRect);

            //if the rasterizer returned SparseBufferedImage.NULL because the thread was
            //interrupted, do not send an image to the consumer.  This can occur by design
            //if a GUI action requests a new Image before the ongoing rasterization
            //has completed.
            if (SparseBufferedImage.NULL == newImage) {
                return;
            }

            Iterator it = consumers.iterator();
            while (it.hasNext()) {
                LayeredImageConsumer target = (LayeredImageConsumer) it.next();
                target.setSparseImage(newImage, image.getZorder());
            }
            return;

        }
    }

    public void setVisibleRectangle(Rectangle visRect) {
        this.visRect = visRect;
    }
}