/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.colorbar;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.bhpb.geographics.util.ColorMap;

public class ColorbarUtil {

    private static final Logger logger = Logger.getLogger(ColorbarUtil.class.toString());
    private static final String COLORBAR_DIR_NAME = "colorbars/";
    private static final String COLORBAR_EXTENSION = ".cmp";

    public static List<String> getListOfavailableColorbarNames() {
        String jarFileName = ColorbarUtil.class.getProtectionDomain().getCodeSource().getLocation().toString();
        System.out.println("jarFileName=" + jarFileName);
        int index = jarFileName.indexOf("file:");
        if (index != -1) {
            jarFileName = jarFileName.substring("file:".length());
        }
        ZipFile zip = null;
        List<String> list = new ArrayList<String>();
        try {
            System.out.println("jarFileName=" + jarFileName);
            zip = new ZipFile(jarFileName);
            Enumeration<ZipEntry> entries = (Enumeration<ZipEntry>) zip.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) entries.nextElement();
                String line = entry.getName();
                if (line.length() > 0 && line.contains(COLORBAR_DIR_NAME) && line.endsWith(COLORBAR_EXTENSION)) {
                    index = line.indexOf(COLORBAR_DIR_NAME);
                    String name = line.substring(index + COLORBAR_DIR_NAME.length(), line.indexOf(COLORBAR_EXTENSION));
                    if (!list.contains(name)) {
                        list.add(name);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (zip != null) {
                    zip.close();
                }
            } catch (IOException ioe) {
                logger.warning("IOException occurred while closing ZipFile for " + jarFileName + ": " + ioe.getMessage());
            }
        }
        return list;
    }

    /*
     * Generate the ColorMap object by a given colorbar file base name (omitting .cmp).
     *
     * @param name  colorbar file base name
     * @throws IOException if an IO error occurs while reading the colormap resource file
     *
     * @return   a ColorMap object representing the colorbar file.
     */
    public static ColorMap getColorMapByResourceName(String name) throws IOException {
        String resource = COLORBAR_DIR_NAME + name + COLORBAR_EXTENSION;
        //load the colorbar file
        ClassLoader classLoader = ColorbarUtil.class.getClassLoader();
        InputStream inStream = classLoader.getResourceAsStream(resource);
        if (inStream == null) {
            URL[] urls = ((URLClassLoader) classLoader).getURLs();
            logger.finest("Failed to create an InputStream for " + resource + " at any of the following URLs:");
            for (URL url : urls) {
                logger.finest(url.toString());
            }
            throw new IllegalArgumentException("Cannot load specified resource, it does not exist: " + resource);
        }
        InputStreamReader reader = new InputStreamReader(inStream);

        String currentPropertyName = null;
        BufferedReader br = null;
        List<Color> currentColorList = null;
        int nColorsToBeRead = 0;
        Map<String, List<Color>> colorMap = new HashMap<String, List<Color>>();
        try {
            //reading colorbar file line by line
            String line = null;
            br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                if (nColorsToBeRead > 0) {
                    String workingLine = line;

                    String[] rgb = workingLine.split(" +");
                    if (rgb.length != 4) {
                        throw new RuntimeException("Each colormap file line specifying a color must contain exactly 4 integers, but read: " + line);
                    }

                    try {
                        int redValue = Integer.valueOf(rgb[0]).intValue();
                        int greenValue = Integer.valueOf(rgb[1]).intValue();
                        int blueValue = Integer.valueOf(rgb[2]).intValue();
                        int alphaValue = Integer.valueOf(rgb[3]).intValue();

                        currentColorList.add(new Color(redValue, greenValue, blueValue, alphaValue));
                    } catch (NumberFormatException nfe) {
                        throw new RuntimeException("Unable to parse the current line, a colormap file line specifying a color value must have exactly 4 space-delimited 8-bit values, failed on line: " + line + " while parsing the substring.", nfe);
                    }
                    nColorsToBeRead--;
                    if (nColorsToBeRead == 0) {
                        colorMap.put(currentPropertyName, currentColorList);
                    }
                } else {
                    String[] splitLine = line.split("=");
                    if (splitLine.length != 2) {
                        throw new RuntimeException("Invalid file - expected 'propertyname = numColors' but read: " + line);
                    } else {
                        currentPropertyName = splitLine[0].trim();
                        currentColorList = new ArrayList<Color>();
                        try {
                            nColorsToBeRead = Integer.valueOf(splitLine[1].trim()).intValue();
                        } catch (NumberFormatException nfe) {
                            throw new RuntimeException("Each colormap file line specifying a property must be of the format 'propertyname = numColors', failed on line: " + line +
                                    " because the number of colors '" + splitLine[1] + "' could not be parsed as an Integer.", nfe);
                        }
                    }
                }
            }
            return new ColorMap(colorMap);
        } finally {
            try {
                if (inStream != null) {
                    inStream.close();
                }
                if (reader != null) {
                    reader.close();
                }
                if (br != null) {
                    br.close();
                }
            } catch (IOException ioe) {
                logger.warning("IOException occurred while closing InputStream for " + resource + ": " + ioe.getMessage());
            }
        }
    }
}
