/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics.accessor;

import java.util.ArrayList;
import java.util.List;

public class FieldProcessor<FIELD_ENUM extends Enum> {

    public static final String DELIMITER_REGEX = "[.]";
    public static final String DELIMITER_STRING = ".";
    private List<FIELD_ENUM> fields;
    private String className;
    public static final String BOOLEAN_TRUE_STRING = "on";
    public static final String BOOLEAN_FALSE_STRING = "off";
    private static final String WILDCARD_STRING = "*";
    private static final String WILDCARD_IDENTIFIER = "WILDCARD";

    public FieldProcessor(FIELD_ENUM[] fields, Class clazz) {
        this.fields = new ArrayList<FIELD_ENUM>();
        for (FIELD_ENUM field : fields) {
            this.fields.add(field);
        }
        this.className = clazz.toString();
    }

    static public Boolean booleanValue(String str) {
        if (BOOLEAN_TRUE_STRING.equals(str)) {
            return Boolean.TRUE;
        } else if (BOOLEAN_FALSE_STRING.equals(str)) {
            return Boolean.FALSE;
        } else {
            throw new IllegalArgumentException("Illegal Boolean string '" + str + "' : valid values are: " + BOOLEAN_TRUE_STRING + " and " + BOOLEAN_FALSE_STRING);
        }
    }

    /**
     * Returns the first unqualified field name contained in the parameter.
     * If the parameter String is not qualified, the entire String is returned.
     * 
     * @throws com.bhpb.geographics.accessor.PropertyAccessorException if qualifiedField
     * does not match the String of some E.
     * 
     * @param qualifiedField the String value for which an associated E will be returned
     * 
     * @return the FIELD_ENUM which has a toString() value which is an exact match
     * with qualifiedField
     * 
     */
    public FIELD_ENUM getFirstMatchingField(String qualifiedField)
            throws PropertyAccessorException {

        String firstField = qualifiedField.split(DELIMITER_REGEX)[0];

        //Special case - '*' is not a valid java identifier, but stands for the
        //FIELD value WILDCARD
        if (WILDCARD_STRING.equals(firstField)) {
            firstField = WILDCARD_IDENTIFIER;
        }

        for (FIELD_ENUM validField : fields) {
            if (firstField.equals(validField.toString())) {
                return validField;
            }
        }
        throw new PropertyAccessorException("Invalid field for " + className + ": " + firstField);
    }

    /**
     * Returns a string consisting of any and all characters in the qualified field
     * after the first occurance of the delimiter.  If the field is not qualified,
     * the empty string is returned
     * 
     * @param qualifiedField
     *
     * @return the portion of the String following the delimiter, if any, otherwise,
     * the empty string
     */
    public String getFieldRemainder(String qualifiedField) {
        int index = qualifiedField.indexOf(DELIMITER_STRING);
        if (index == -1) {
            return "";
        }
        return qualifiedField.substring(index + 1);
    }

    /**
     * Returns the String representation of a Boolean as expected by the ControlSessionManager.
     * 
     * @param bool a Boolean
     * @return "on" if bool is Boolean.TRUE, otherwise "off"
     */
    static public String valueOf(Boolean bool) {
        return bool ? BOOLEAN_TRUE_STRING : BOOLEAN_FALSE_STRING;
    }
}