/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geographics;

import com.bhpb.geographics.ui.layerlist.LayerListPanel;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.window.WindowModel.LAYER_TARGET;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Logger;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.ListModel;

public class LayerListMouseAdapter extends MouseAdapter {

    private static final Logger logger =
            Logger.getLogger(LayerListMouseAdapter.class.toString());

    private JList jlist;
    private LayerListPanel layerList;
    private JPopupMenu popup;
    private Layer selectedLayer;

    public LayerListMouseAdapter(JList jlist, LayerListPanel layerList) {
        this.jlist = jlist;
        this.layerList = layerList;

        popup = new JPopupMenu();

        JMenuItem menuItem = new JMenuItem("Property ...");
        menuItem.addActionListener(new PropertyChangeActionListener());
        popup.add(menuItem);

        menuItem = new JMenuItem("Move Layers To bottom");
        menuItem.addActionListener(new MoveLayersActionListener(LAYER_TARGET.LIST_BOTTOM));
        popup.add(menuItem);

        menuItem = new JMenuItem("Move Layers To Top");
        menuItem.addActionListener(new MoveLayersActionListener(LAYER_TARGET.LIST_TOP));
        popup.add(menuItem);

        menuItem = new JMenuItem("Hide Layers");
        menuItem.addActionListener(new HideLayerActionListener());
        popup.add(menuItem);

        menuItem = new JMenuItem("Show Layers");
        menuItem.addActionListener(new ShowLayerActionListener());
        popup.add(menuItem);

        menuItem = new JMenuItem("Delete Layers");
        menuItem.addActionListener(new DeleteLayerActionListener());
        popup.add(menuItem);

    }

    private class DeleteLayerActionListener implements ActionListener {

        public void actionPerformed(ActionEvent ae) {
            if (selectedLayer != null) {
                layerList.remove(selectedLayer);
            } else {
                logger.warning("Unable to show properties dialog - selected layer is null.");
            }
        }
    }

    private class HideLayerActionListener implements ActionListener {

        public void actionPerformed(ActionEvent ae) {
            if (selectedLayer != null) {
                layerList.hide(selectedLayer);
            } else {
                logger.warning("Unable to hide layer - selected layer is null.");
            }
        }
    }

    private class MoveLayersActionListener implements ActionListener {

        LAYER_TARGET target;

        public MoveLayersActionListener(LAYER_TARGET target) {
            this.target = target;
        }

        public void actionPerformed(ActionEvent ae) {
            layerList.moveSelectedLayers(target);
        }
    }

    private class PropertyChangeActionListener implements ActionListener {

        public void actionPerformed(ActionEvent ae) {
            if (selectedLayer != null) {
                layerList.showPropertiesDialog(selectedLayer);
            } else {
                logger.warning("Unable to show properties dialog - selected layer is null.");
            }
        }
    }

    private class ShowLayerActionListener implements ActionListener {

        public void actionPerformed(ActionEvent ae) {
            if (selectedLayer != null) {
                layerList.show(selectedLayer);
            } else {
                logger.warning("Unable to show layer - selected layer is null.");
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int index = jlist.locationToIndex(e.getPoint());
        ListModel dlm = jlist.getModel();
        Object item = dlm.getElementAt(index);
        jlist.ensureIndexIsVisible(index); // scroll to index?

        if (item == null) {
            logger.warning("Unable to process mouse click at position " + e.getX() + ", " + e.getY() + " because no item exists at that location.");
            return;
        }

        if (item instanceof Layer) {
            selectedLayer = (Layer) item;
        } else {
            logger.warning("Unable to display properties for layer at index #" + index + " because it is not a Layer.");
            return;
        }

        if (e.getButton() == 3) { // if right click, show popup menu

            popup.show(e.getComponent(), e.getX(), e.getY());
        } else // if double click, show properties dialog 
        if (e.getButton() == 1 && e.getClickCount() == 2) {
            layerList.showPropertiesDialog(selectedLayer);
        }
    }
}