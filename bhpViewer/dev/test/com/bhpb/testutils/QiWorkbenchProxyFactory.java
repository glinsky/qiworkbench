/*
 * QiWorkbenchProxyFactory.java
 *
 * Created on November 13, 2007, 3:24 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpb.testutils;

import com.bhpb.qiworkbench.IqiMessageHandler;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author folsw9
 */
public class QiWorkbenchProxyFactory {    
    private Map proxyActionMap = new HashMap();
    
    /** Creates a new instance of QiWorkbenchProxyFactory */
    public QiWorkbenchProxyFactory() {
    }
    
    public Object createProxy(IqiMessageHandler realComponent) {
        return QiComponentProxy.proxyFor(realComponent);
    }
    
    public void addDelay(Object proxy, int delayTime, String methodName, Class... parameterTypes) 
        throws NoSuchMethodException
    {
        validateProxyObject(proxy);
        
        QiComponentProxy qiComponentProxy = (QiComponentProxy)Proxy.getInvocationHandler(proxy);
        qiComponentProxy.addProxyAction(new ProxyDelay(delayTime), methodName, parameterTypes);
    }
    
    public void removeProxyActions(Object proxy, String methodName, Class... parameterTypes) 
        throws NoSuchMethodException
    {
        validateProxyObject(proxy);
        
        QiComponentProxy qiComponentProxy = (QiComponentProxy)Proxy.getInvocationHandler(proxy);
        qiComponentProxy.removeProxyActions(methodName, parameterTypes);
    }
    
    protected abstract class ProxyAction {};
    
    protected class ProxyDelay extends ProxyAction {
        private final int delayTime;
        public int getDelayTime() { return delayTime; }
        private ProxyDelay(int delayTime) {
            this.delayTime = delayTime;
        }
    }
    
    protected class ProxyReturnOverride extends ProxyAction {
        private final Object returnValue;
        private boolean invocationPerformed;
        public boolean isInvocationPerformed() { return invocationPerformed; }
        public Object getReturnValue() { return returnValue; }
        private ProxyReturnOverride(Object returnValue, boolean invocationPerformed) {
            this.returnValue = returnValue;
            this.invocationPerformed = invocationPerformed;
        }
    }
    
    private void validateProxyObject(Object proxy) {
        if (Proxy.isProxyClass(proxy.getClass()) == false)
            throw new IllegalArgumentException("addDelay parameter 'proxy' must be a Proxy class");
        Object invocationHandler = Proxy.getInvocationHandler(proxy);
        if ((invocationHandler instanceof QiComponentProxy) == false)
            throw new IllegalArgumentException("addDelay parameter 'proxy' is a Proxy class but not of type QiComponentProxy, it is: " + proxy.getClass().getName());
    }
}