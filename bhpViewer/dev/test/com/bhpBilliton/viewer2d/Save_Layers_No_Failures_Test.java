package com.bhpBilliton.viewer2d;

import com.bhpb.qiworkbench.IqiMessageHandler;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
import com.bhpb.testutils.MockQiComponent;
import com.bhpb.testutils.QiWorkbenchProxyFactory;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 *
 * @author folsw9
 */
public class Save_Layers_No_Failures_Test extends TestCase {
    QiWorkbenchProxyFactory factory;
    MockQiComponent mock1;
    IqiMessageHandler comp1;
    
    private static final Logger logger = Logger.getLogger(Save_Layers_No_Failures_Test.class.toString());
    
    public void setUp() throws NoSuchMethodException, InterruptedException {
        factory = new QiWorkbenchProxyFactory();
        assertNotNull(factory);
        
        setUpMessageDispatcher();
        mock1 = setUpMockQiComponent();
        
        comp1 = setUpProxy(mock1);
    }
    
    public Save_Layers_No_Failures_Test() {
    }
    
    private void setUpMessageDispatcher() {
        System.setProperty("deployServerURL","http://localhost:8080");   
        System.setProperty("install","false");
        
        MessageDispatcher.mainNoGUI();
        
        MessageDispatcher dispatcher  = MessageDispatcher.getInstance();
        assertTrue(dispatcher.isInitSuccessful());
    }
    
    private MockQiComponent setUpMockQiComponent()  {
        MockQiComponent mock = new MockQiComponent();
        assertNotNull(mock);
        mock.isInitFinished(); // test mock functionality
        
        return mock;
    }
    
    private IqiMessageHandler setUpProxy(IqiMessageHandler component) throws NoSuchMethodException {
        Object proxy = factory.createProxy(component);
        assertNotNull(proxy);
        
        IqiMessageHandler proxyComponent = (IqiMessageHandler) proxy;
        proxyComponent.isInitFinished(); // test proxy invocation of mock functionality
        
        return (IqiMessageHandler) proxyComponent;
    }
    
    public void testMessageDispatcher_getResponseWait_Timeout_And_Response() throws InterruptedException, NoSuchMethodException {
        mock1.startComponent("MOCK1");
        
        Thread.sleep(2000);

        logger.info("Mock1.getCID() = " + comp1.getCID());
        
        IComponentDescriptor cd1 = MessageDispatcher.getInstance().getComponentDesc(comp1.getCID());
        
        assertNotNull("md.getComponentDesc() returned null comp descriptor for CID: " +comp1.getCID(), cd1);
            
        //String fastMsgID = mock1.getMessagingMgr().sendRequest("CMD", 
        //         "SEND_TEST_RESPONSE", cd2, QIWConstants.STRING_TYPE, "Test Content", true);
        //QiWorkbenchMsg fastResponse = mock1.getMessagingMgr().getMatchingResponseWait(fastMsgID, 5000);
        //assertTrue("Response from fast test msg '" + fastMsgID + "' should not have been null after 5 seconds", fastResponse != null);
                
        assertTrue("Message queue should be empty before slow synchronous message test, but it was not",mock1.getMessagingMgr().isMsgQueueEmpty());
        //String slowMsgID = mock1.getMessagingMgr().sendRequest("CMD", 
        //         "SEND_TEST_RESPONSE", cd2, QIWConstants.STRING_TYPE, "Test Content", true);
       
        //QiWorkbenchMsg slowResponse = mock1.getMessagingMgr().getMatchingResponseWait(slowMsgID, 5000);
        //assertTrue("Response from slow test msg " + slowMsgID + " should have been null after 5 seconds due to 6 second proxy delay, but was non-null.", slowResponse == null);
        
        Thread.sleep(3000);
        
        assertTrue("Message queue should be empty if the late response was discarded, but it was not",mock1.getMessagingMgr().isMsgQueueEmpty());      
    }
}