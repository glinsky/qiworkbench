/*
 * SeismicDataSet.java
 *
 * Created on December 13, 2007, 1:33 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpBilliton.viewer2d;

import com.bhpBilliton.viewer2d.ui.DataSetHeaderKeyInfo;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author folsw9
 */
public class SeismicDataSet {
    private Map<String, DataSetHeaderKeyInfo> headerInfo = new HashMap<String, DataSetHeaderKeyInfo>();
    private boolean isXsec = false;
    
    /** Creates a new instance of SeismicDataSet */
    public SeismicDataSet() {
    }
    
    /**
     * @return the number of header info keys - can be used to determine whether valid headers were parsed.
     */
    public int getHeaderLength() {
        return headerInfo.keySet().size();
    }
    
    /**
     * @return true if the seismic data set is an X-section, otherwise false (Map).
     */
    public boolean isXsec() {
        return isXsec;
    }
    
    public void setXsec(boolean isXsec) {
        this.isXsec = isXsec;
    }
    
    public void setHeaderInfo(Map<String, DataSetHeaderKeyInfo> headerInfo) {
        this.headerInfo = headerInfo;
    }
}