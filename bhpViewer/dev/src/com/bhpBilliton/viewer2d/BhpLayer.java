/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.*;
import java.util.*;
import org.w3c.dom.*;

import com.bhpBilliton.viewer2d.data.*;
import com.bhpBilliton.viewer2d.ui.BhpBusyDialog;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.gw2d.model.CommonDataModel;
import com.gwsys.gw2d.model.CommonShapeLayer;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;
import com.gwsys.gw2d.view.SimpleViewContainer;
import com.gwsys.seismic.indexing.SegyHeaderFormat;
import com.gwsys.seismic.core.DataChooser;
import com.gwsys.seismic.core.SegyTraceImage;
import com.gwsys.seismic.core.TraceNormalizer;
import com.gwsys.seismic.core.SeismicWorkflow;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.util.SeismicColorMap;
import com.gwsys.seismic.util.SeismicColorEvent;
import com.gwsys.seismic.util.SeismicColorListener;

/**
 * Title: BHP Viewer <br>
 * <br>
 * Description: This abstract class provides default implementation and common
 * fields for different kinds of data display layer. In this program, data is
 * displayed in layer. Currently, four different types of layer are provided for
 * different data type and display type. <br>
 * When some attribute of a layer is changed, an
 * <code>{@link BhpLayerEvent}</code> is generated and broadcasted if the
 * layer is not set to mute. All the other layers get the event. A layer decides
 * if it needs to change itself accrodingly based on its value of the
 * synchronized flag. However, this kind of passive change won't generate new
 * BhpLayerEvent. <br>
 * When a new layer needs to be created and inserted, it is a two steps process.
 * Firstly, a new layer is created. When creating BhpSegyReader, a new loading
 * thread is started to generate and data and read it. If the data is generated
 * successfully, a layer will be actually added to the view. <br>
 * <br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public abstract class BhpLayer extends SimpleViewContainer
    implements SeismicColorListener {


  private final int _idNumber;

  protected String _name;

  protected String _dataName;

  protected String _pathlist;

  protected String _dataSource;

  protected boolean _enableTalk;

  protected int _synFlag;

  protected int _transparent;

  protected boolean _reversedPolarity;

  protected boolean _reversedSampleOrder;

  protected BhpSeismicTableModel _parameter;

  //LM: added to handle layer visiblility after loading
  // THIS IS A HACK
  protected boolean loadedVisibility = true;

  protected boolean hasLoaded = false;

  protected int _dataType;

  protected boolean _isReady;

  protected BhpDataReader _reader;

  protected SeismicWorkflow _pipeline;

  protected SegyTraceImage _image; // for seismic and model layer

  protected BhpEventShape _shape; // for event layer

  protected String _propertyName;

  protected boolean _propertySync;

  protected double _hscaleFactor;

  protected double _vscaleFactor;

  protected Bound2D _modelBox;

  protected Bound2D _deviceBox;

  protected CommonShapeLayer _shapeListLayer;

  protected CommonDataModel _containerModel;

  protected Transform2D _transformation;

  protected BhpViewerBase _viewer;

  protected BhpWindow _window;

  protected BhpLayer _sampleLayer;

  protected Node _pipelineNode;

  public BhpViewerBase getViewer() {
      return _viewer;
  }

  // this constructor will try to initiate the layer with the currently
  // selected layer
  /**
   * Constructs a new layer with a reference layer. <br>
   * This construtor is usually used when a new layer is created by the users
   * with GUI control.
   *
   * @param viewer
   *            the parent viewer of the new layer.
   * @param win
   *            the parent window of the new layer.
   * @param id
   *            the identification number of the new layer.
   * @param source
   *            the name of the data source.
   * @param dname
   *            name of the selected dataset.
   * @param name
   *            name of the new layer.
   * @param pathlist
   *            path of the selected dataset. <br>
   *            For bhpio data, it is the pathlist. For OpenSpirit data, it is
   *            the session name.
   * @param parameter
   *            a table model stores the user data selection. In the case of
   *            bhpio, it can be used to construct the query string used to
   *            run bhpio.
   * @param property
   *            the name of the selected property. <br>
   *            In the case of seismic data, it is null. In the case of model
   *            or event data, it is the selected property name or event name.
   * @param propertySync
   *            the boolean flag for property synchronization.
   * @param ref
   *            the reference layer. <br>
   *            The new layer will inherit from this reference layer things
   *            like scales, pipeline settings, display options, and etc.
   * @param type
   *            data type. <br>
   *            It can be BHP_DATA_TYPE_SEISMIC, BHP_DATA_TYPE_MODEL,
   *            BHP_DATA_TYPE_EVENT, and BHP_DATA_TYPE_UNKNOWN.
   */
  public BhpLayer(BhpViewerBase viewer, BhpWindow win, int id, String source,
                  String dname, String name, String pathlist,
                  BhpSeismicTableModel parameter, String property,
                  boolean propertySync, BhpLayer ref, int type) {
    super();
    //this.setCacheMode(ModelViewContainer.CG_VIEW_SIZE_CACHE);
    _viewer = viewer;
    _window = win;
    _idNumber = id;
    //_dataSourceType = sourceType;
    _dataSource = source;
    _dataName = dname;
    _parameter = parameter;
    if (_dataSource.equals("TemporaryEventDataSource")
        && name.equals("UnnamedEvent")) {
      name = "UnnamedEvent" + id;
    }
    this.setLayerName(name);
    this.setPropertyName(property);
    _pathlist = pathlist;
    _propertySync = propertySync;
    _pipelineNode = null;
    _dataType = type;

    _hscaleFactor = 1.0;
    _vscaleFactor = 1.0;

    _sampleLayer = ref;
    if (_sampleLayer == null) {
      _enableTalk = true;
      _synFlag = 0;
      _transparent = 255;
      _reversedPolarity = false;
      _reversedSampleOrder = false;
    } else {
      _enableTalk = _sampleLayer.getEnableTalk();
      _synFlag = _sampleLayer.getSynFlag();
      _transparent = _sampleLayer.getTransparent();
      _reversedPolarity = _sampleLayer.getReversedPolarity();
      _reversedSampleOrder = _sampleLayer.getReversedSampleOrder();
    }
    initialize();
  }

  private void initialize(){
    _isReady = false;
    setNotificationFlag(true);
    _pipeline = null;
  }

  // this constructor will try to initiate the layer will the parameters
  // passed in
  /**
   * Consturcts a new layer with specified attributes. <br>
   * This construtor is usually used when a new layer is created parsing saved
   * XML configuration file.
   *
   * @param viewer
   *            the parent viewer of the new layer.
   * @param win
   *            the parent window of the new layer.
   * @param id
   *            the identification number of the new layer.
   * @param source
   *            the name of the data source.
   * @param dname
   *            name of the selected dataset.
   * @param name
   *            name of the new layer.
   * @param pathlist
   *            path of the selected dataset. <br>
   *            For bhpio data, it is the pathlist. For OpenSpirit data, it is
   *            the session name.
   * @param parameter
   *            a table model stores the user data selection. In the case of
   *            bhpio, it can be used to construct the query string used to
   *            run bhpio.
   * @param property
   *            the name of the selected property. <br>
   *            In the case of seismic data, it is null. In the case of model
   *            or event data, it is the selected property name or event name.
   * @param talk
   *            a boolean indicates if the new layer should broadcase its
   *            change event.
   * @param synFlag
   *            the synchronization flag of the new layer.
   * @param trans
   *            transparency(opacity) of the new layer.
   * @param propertySync
   *            a boolean indicates if the new layer will be synchronized for
   *            property(event) name.
   * @param pnode
   *            the pipeline node in the DOM tree. <br>
   *            It is used to initialize the pipeline of the new layer.
   * @param type
   *            data type. <br>
   *            It can be BHP_DATA_TYPE_SEISMIC, BHP_DATA_TYPE_MODEL,
   *            BHP_DATA_TYPE_EVENT, and BHP_DATA_TYPE_UNKNOWN.
   * @param rp
   *            a boolean for polarity.
   * @param ro
   *            a boolean for sample order, reversed or not.
   */
  public BhpLayer(BhpViewerBase viewer, BhpWindow win, int id, String source,
                  String dname, String name, String pathlist,
                  BhpSeismicTableModel parameter, String property, boolean talk,
                  int synFlag, int trans, boolean propertySyn, Node pnode, int type,
                  boolean rp, boolean ro) {
    super();
    _viewer = viewer;
    _window = win;
    _idNumber = id;
    _dataSource = source;
    _dataName = dname;
    _parameter = parameter;
    this.setLayerName(name);
    this.setPropertyName(property);
    _pathlist = pathlist;
    _propertySync = propertySyn;
    _pipelineNode = pnode;
    _dataType = type;

    _enableTalk = talk;
    _synFlag = synFlag; // no synchronization by default is 0
    _transparent = trans; // default 255
    _reversedPolarity = rp;
    _reversedSampleOrder = ro;

    initialize();
  }

  /**
   * Scale the layer, the anchor point is the up-left corner.
   *
   * @param x
   *            horizontal scale factor.
   * @param y
   *            vertical scale factor.
   * @param xp
   *            horizontal view position.
   * @param yp
   *            vertical view position.
   */
  public void scaleToWithLayer(double x, double y, double xp, double yp) {

    this.updateLayer(false);

    if (getModel() != null && getModel().getBoundingBox() != null) {
      setLimits((int) Math.abs(getModel().getBoundingBox().width * _transformation.getScaleX()),
          (int) Math.abs(getModel().getBoundingBox().height* _transformation.getScaleY()));
    }

    int vx = (int) (xp * x);
    int vy = (int) (yp * y);
    if (vx + getSize().width > this.getActualSize().width) {
      vx = this.getActualSize().width - getSize().width;
    }
    if (vy + getSize().height > this.getActualSize().height) {
      vy = this.getActualSize().height - getSize().height;
    }

    if (vx < 0) vx = 0;
    if (vy < 0) vy = 0;
    if (this.getActualSize().width <= getSize().width) {
      vx = 0;
    }
    if (this.getActualSize().height <= getSize().height) {
      vy = 0;
    }
    setViewPosition( new Point(vx, vy) );
    setDirtyMark(true);
  }

  /**
   * Handle the color map change evnet. <br>
   * If the layer is not muted, a <code>{@link BhpLayerEvent}</code> with
   * flag COLORBAR_FLAG will be generated annd broadcasted.
   */
  public void colorMapUpdated(SeismicColorEvent event) {
    this.repaint();
    if (this.getEnableTalk()) {
      SeismicColorMap cmap = (SeismicColorMap) event.getSource();
      BhpLayerEvent e = new BhpLayerEvent(BhpViewer.COLORBAR_FLAG, this,
          cmap.clone());
      _viewer.broadcastBhpLayerEvent(e);
    }
  }

  /**
   * Gets the identification number of the layer.
   */
  public final int getIdNumber() {
    return _idNumber;
  }

  /**
   * Gets the name of the underlying dataset.
   */
  public final String getDataName() {
    return _dataName;
  }

  /**
   * Gets the name of the layer.
   */
  public final String getLayerName() {
    return _name;
  }

  /**
   * Gets the path of the selected dataset. <br>
   * For bhpio data, it is the pathlist. While for OpenSpirit data, it is the
   * session name.
   */
  public final String getPathlist() {
    return _pathlist;
  }

  /**
   * Gets the data source.
   */
  public final String getDataSource() {
    return _dataSource;
  }

  /**
   * Finds out if the layer is muted.
   */
  public final boolean getEnableTalk() {
    return _enableTalk;
  }

  /**
   * Gets the synchronization flag of the layer.
   */
  public final int getSynFlag() {
    return _synFlag;
  }

  /**
   * Gets the transparency setting of the layer.
   */
  public final int getTransparent() {
    return _transparent;
  }

  /**
   * Finds out the polarity setting of the layer.
   */
  public final boolean getReversedPolarity() {
    return _reversedPolarity;
  }

  /**
   * Finds out the sample order setting of the layer.
   */
  public final boolean getReversedSampleOrder() {
    return _reversedSampleOrder;
  }

  /**
   * Finds out if the layer is ready to be added to the window.
   */
  public final boolean getIsReady() {
    return _isReady;
  }

  /**
   * Retrieves the table model of the layer.
   */
  public BhpSeismicTableModel getParameter() {
    return _parameter;
  }

  /**
   * Gets the data type of the layer.
   */
  public int getDataType() {
    return _dataType;
  }

  /**
   * Gets the name of the selected property(event).
   */
  public String getPropertyName() {
    return _propertyName;
  }

  /**
   * Finds out if the property(event) is set to synchronize.
   */
  public boolean getPropertySync() {
    return _propertySync;
  }

  /**
   * Retrieves the container model of the layer.
   */
  public CommonDataModel getContainerModel() {
    return _containerModel;
  }

  /**
   * Retrieves the shape list layer where shapes are stored.
   */
  public CommonShapeLayer getShapeListLayer() {
    return _shapeListLayer;
  }

  /**
   * Sets the name of the selected dataset.
   */
  public void setDataName(String n) {
    _dataName = n;
  }

  /**
   * Sets the layer name.
   */
  public void setLayerName(String name) {
    _name = name;               // LM: Moved from setPropertyName to fix Issue 91 (not required for fix)
  }

  /**
   * Sets the path of the selected dataset.
   */
  public void setPathlist(String p) {
    _pathlist = p;
  }

  public void setEnableTalk(boolean b) {
    _enableTalk = b;
  }

  public void setIsReady(boolean b) {
    _isReady = b;
  }

  /**
   * Sets the synchronization flag of the layer.
   */
  public void setSynFlag(int f) {
    _synFlag = f;
  }

  /**
   * Sets the name of the selected property(event).
   */
  public void setPropertyName(String p) {
    _propertyName = p;
  }

  public void setPropertySync(boolean s) {
    _propertySync = s;
  }

  public void setReversedPolarity(boolean r) {
    _reversedPolarity = r;
    _reader.setReversed(_reversedPolarity);
  }

  public void setReversedSampleOrder(boolean r) {
    _reversedSampleOrder = r;
    _reader.setReversedDirection(_reversedSampleOrder);
  }

  /**
   * Gets the parameter string used when query the data for the layer. <br>
   * The returned string can be used as the parameter of some other process.
   * For example, bhpControl.SelectedScriptAction uses the string as the
   * parameter of some external shell program. <br>
   */
  public String getParameterString() {
    return _reader.getParameterString();
  }

  // This method will be called when a BhpLayerEvent of type TIMERANGE_FLAG is
  // handled
  /**
   * Sets the vertical range of the layer.
   */
  public abstract void setTimeRange(double start, double end);

  /**
   * Gets the vertical start of the layer.
   */
  public abstract double getTimeRangeStart();

  /**
   * Gets the vertical end of the layer.
   */
  public abstract double getTimeRangeEnd();

  /*
   * Get user's selection for handling missing data
  */
  public abstract int getMissingDataSelection();

  /**
   * Update the layer when only display option of the layer is changed.
   *
   * @param redraw
   *            if a redraw is requested.
   */
  public abstract void updateLayer(boolean redraw);

  /**
   * Set up the layer or update the lay when the underlying data of the layer
   * is changed.
   */
  public abstract void setupSeismicDisplay();

  /**
   * Update the layer when both display option and data processing may have
   * been changed.
   */
  public abstract void updateSeismicDisplay();

  /**
   * Processes the <code>{@link BhpLayerEvent}</code>.
   */
  public abstract boolean receiveBhpLayerEvent(BhpLayerEvent e);

  /**
   * Overloads method updateLayer(true).
   */
  public void updateLayer() {
    updateLayer(true);
  }

  /**
   * Converts the layer object into an XML string. <br>
   * This is used when save the working session into an XML file. Please refer
   * to BhpViewer.dtd for the detail of the format.
   *
   * @return an XML string for the object.
   */
  public String toXMLString() {
    StringBuffer content = new StringBuffer();
    content.append("            " + "<BhpLayer ");
    content.append("idNumber=\"" + _idNumber + "\" ");
    content.append("dsType=\"" + _dataSource + "\" ");
    content.append("name=\"" + getLayerName() + "\" ");
    content.append("dataName=\"" + _dataName + "\" ");
    content.append("pathlist=\"" + _pathlist + "\" ");
    content.append("enableTalk=\"" + _enableTalk + "\" ");
    content.append("synFlag=\"" + _synFlag + "\" ");
    content.append("transparent=\"" + _transparent + "\" ");
    content.append("property=\"" + getPropertyName() + "\" ");
    content.append("propertySync=\"" + _propertySync + "\" ");
    content.append("scaleFactorH=\"" + _hscaleFactor + "\" ");
    content.append("scaleFactorV=\"" + _vscaleFactor + "\" ");
    content.append("reversedPolarity=\"" + _reversedPolarity + "\" ");
    
    //LM: added layer visibility - it wasn't in here before
    content.append("visible=\"" + Boolean.toString(isBhpLayerVisible())
                   + "\" ");
    content.append("reversedSampleOrder=\"" + _reversedSampleOrder
                   + "\">\n");

    content.append(_parameter.toXMLString(_dataSource));
    content.append(BhpViewerHelper.toXMLString(_viewer, _pipeline));

    content.append("            " + "</BhpLayer>\n");
    return content.toString();
  }


  /**
   * Sets up the data source of the layer. <br>
   * This method is usually called when the underlying data of the layer is
   * changed and needs to be regenerated.
   */
  public void generateLayer() {
    this.cleanupLayer();
    if (_dataSource.equals("OpenSpirit")) {
      this._parameter.setDataSource(null);
    }
    // make sure if ^ value is used, they are matching
    _parameter.matchArbitraryTraverse();
    setupSeismicReader();
  }

  /**
   * Cleans up the layer before it is deleted or redefined. This method
   * Deletes the temporary file for the data. <br>
   * When bhpio is used to retrieve data, a temporary data file is written.
   * This method tries to clean up by deleting the file.
   */
  public void cleanupLayer() {
    if (_reader != null) {
      _reader.cleanupDataReader();
    }
  }

  /**
   * Stops the thread that loads the data. <br>
   * This method is only useful when a separated thread is used to load the
   * data.
   */
  public void stopLoading() {
    try {
      _reader.stopLoading();
    } catch (Exception ex) {
        ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                            "BhpLayer.stopLoading failed");
    }
  }

  /**
   * Retreive the BhpViewerBase which contains this BhpLayer.
   * @return BhpViewerBase containing this <code>BhpLayer</code>
   */
  public BhpViewerBase getBhpViewerBase(){
      return _viewer;
  }

  /**
   * This method is called when data loading fails for the layer. <br>
   * When this happens, the layer will be removed from the display and a
   * message will be shown.
   */
  public void loadingLayerError() {
    cleanupLayer();
    _viewer.changeLoadingLayerNumber(BhpBusyDialog.RMV_LAYER_ERROR);
    _viewer.loadingLayerError(_window, this);
  }

  /**
   * Handles the OutOfMemory error. <br>
   * This program tries to recover from it by closing some windows.
   */
  public void processingOFM() {
    _viewer.processingOFM(_window);
  }

  /**
   * Retrieves the pipline used to process the data.
   */
  public SeismicWorkflow getPipeline() {
    return _pipeline;
  }

  /**
   * Retrieves the horizontal scale factor of the layer.
   */
  public double getHorizontalScaleFactor() {
    return _hscaleFactor;
  }

  /**
   * Retrieves the vertical scale factor of the layer.
   */
  public double getVerticalScaleFactor() {
    return _vscaleFactor;
  }

  /**
   * Sets the horizontal scale factor of the layer.
   */
  public void setHorizontalScaleFactor(double scale) {
    _hscaleFactor = scale;
  }

  /**
   * Sets the vertical scale factor of the layer.
   */
  public void setVerticalScaleFactor(double scale) {
    _vscaleFactor = scale;
  }

  /**
   * Gets the horizontal scale of the layer. <br>
   * The scale is calculated with the scale stored in and the scale factor of
   * this layer.
   */
  public double getLayerHorizontalScale() {
    // modelUnitPerTrace
    double re = _window.getBhpPlot().getHorizontalScale() * _hscaleFactor;
    return re;
  }

  /**
   * Gets the vertical scale of the layer. <br>
   * The scale is calculated with the scale stored in
   * <code>{@link AbstractPlot}</code> and the scale factor of this layer.
   */
  public double getLayerVerticalScale() {
    // modelUnitPerSample
    double re = _window.getBhpPlot().getVerticalScale() * _vscaleFactor;
    return re;
  }

  /**
   * Sets the visibility of the layer, applied immediately after loading
   *
   * @param v
   *            Layer is visible
   */
  public void setLoadedVisiblity(boolean v) {
    loadedVisibility = v;
  }

  public boolean getLoadedVisibility() {
    return loadedVisibility;
  }

  public boolean getHasLoaded() {
    return hasLoaded;
  }

  public void setHasLoaded(boolean v) {
    hasLoaded = v;
  }

  /**
   * Sets the visibility of the layer.
   */
  public void setBhpLayerVisible(boolean v) {
    _shapeListLayer.setVisible(v);
  }

  /**
   * Finds out the visibility of the layer.
   */
  public boolean isBhpLayerVisible() {
    return _shapeListLayer.isVisible();
  }

  /**
   * Changes the data ensemble. <br>
   * The current implementation finds out the secondary key selection setting
   * of cgDataSelector and increases/decreases it once by its step value.
   *
   * @param direction
   *            direction>0 increase by the step value; direction <0 decrease
   *            by the step value.
   * @return a boolean indicates if the data change secceeds.
   */
  public boolean changeEnsemble(int direction) {
    if (_pipeline == null) {
      return false;
    }
    DataChooser selector = _pipeline.getDataLoader().getDataSelector();
    int key = selector.getSecondaryKey();
    double step = selector.getSecondaryKeyStep();
    double start = selector.getSecondaryKeyStart();
    double end = selector.getSecondaryKeyEnd();

    if (key == DataChooser.NO_SECONDARY_KEY) {
      return false;
    }
    if (direction > 0) { // increase
      selector.setSecondaryKeyStart(start + step);
      selector.setSecondaryKeyEnd(end + step);
      _pipeline.invalidate(selector);
      return true;
    }
    if (direction < 0) {
      selector.setSecondaryKeyStart(start - step);
      selector.setSecondaryKeyEnd(end - step);
      _pipeline.invalidate(selector);
      return true;
    }
    return false;
  }

  /**
   * Sets the transparency of the layer. <br>
   * This method changes all the colors in the colormap of the rasterizer.
   *
   * @param ntrans
   *            transparent value where 0=transparent and 255=opaque.
   */
  public void setTransparent(int ntrans) {
    _transparent = ntrans;
    if (_pipeline == null) {
      return;
    }
    SeismicColorMap cmap = _pipeline.getTraceRasterizer().getColorMap();
    cmap.setNotify(false);
    int cmapSize = cmap.getSize();
    int trans;
    int bgIndex = new Byte(cmap.getBackgroundIndex()).intValue();
    
    Color oc, nc;
    for (int i = 0; i < cmapSize; i++) {
      oc = cmap.getColor(i);
      if (i == bgIndex) {
        trans = 0;
      } else {
        trans = _transparent;
      }
      nc = new Color(oc.getRed(), oc.getGreen(), oc.getBlue(), trans);
      cmap.setColor(i, nc, false);
    }
    
    cmap.setNotify(true);
  }

  /**
   * Transforms a vertical model space coordinate to virtual vertical value.
   * <br>
   * This method is used for mouse click data synchronization.
   *
   * @param modelV
   *            vertical model sapce coordinate.
   * @return vertical axis reading.
   */
  public double findVerticalReadingFromModel(double modelV) {
    double[] vsettings = _parameter.getVerticalSettings();
    double verticalValue = vsettings[0] + modelV * vsettings[2];
    if (verticalValue > vsettings[1]) {
      verticalValue = vsettings[1];
    }
    return verticalValue;
  }

  /**
   * Finds out the read data value at the specific point. <br>
   * This default implementation simply returns 0. Concrete class extends
   * BhpLayer should have meaningful implemenation.
   *
   * @param traceid
   *            trace number, the horizontal position.
   * @param vreading
   *            vertical axis reading
   * @return data value of the given trace at the specific vertical position.
   */
  public double findSpecificDataValue(int traceid, double vreading) {
    return 0;
  }

  /**
   * Transforms a virtual vertical value to model space coordinate. <br>
   * This method is used for mouse click data synchronization.
   *
   * @param readingV
   *            vertical axis reading
   * @return model space coordinate
   */
  public int findModelFromVerticalReading(double readingV) {
    double[] vsettings = _parameter.getVerticalSettings();
    int result = (int) ((readingV - vsettings[0]) / vsettings[2]);
    return result;
  }

  /**
   * Finds the trace with specific header field value. <br>
   * If multiple traces have the specific value, the trace id of the first
   * occurance is returned. If no suce trace is found, -999 is returned. This
   * method is used to mouse click data synchronization.
   *
   * @param fname
   *            header field name.
   * @param fvalue
   *            header field value.
   * @return trace id
   */
  public int findTraceIdFromField(String fname, double fvalue) {
    int tid = _parameter.findTraceIdFromField(fname, fvalue);
    if (tid == -998) {
      return -999; // out of range. Won't even try.
    }

    SegyHeaderFormat hformat = (SegyHeaderFormat) (_reader.getDataFormat()
        .getTraceHeaderFormats().firstElement());
    Integer fid = BhpSegyFormat.getFieldForName(fname, hformat);
    if (fid == null) {
      return tid;
    }

    boolean match = false;
    if (tid != -999) {
      TraceHeader tmeta = _pipeline.getDataLoader().getDataSelector().
      getTraceHeader(tid);
      if (tmeta != null) {
        if (fvalue == Double.parseDouble(tmeta.getDataValues().get(fid)
            .toString())) {
          match = true;
        }
      }
    }
    if (match) {
      return tid;
    }

    int totTrace = (int) (_pipeline.getVirtualModelLimits().getMaxX());
    int middle = tid;
    if (middle <= 0) {
      middle = totTrace / 2;
    }
    int start = 0;
    int end = totTrace - 1;
    TraceHeader mmeta = _pipeline.getDataLoader().getDataSelector().
    getTraceHeader(middle);
    TraceHeader smeta = _pipeline.getDataLoader().getDataSelector().
    getTraceHeader(start);
    TraceHeader emeta = _pipeline.getDataLoader().getDataSelector().
    getTraceHeader(end);
    double mvalue, svalue, evalue;
    while (!match && end > start) {
      if (mmeta != null && smeta != null && emeta != null) {
        mvalue = Double.parseDouble(mmeta.getDataValues().get(fid)
                                    .toString());
        svalue = Double.parseDouble(smeta.getDataValues().get(fid)
                                    .toString());
        evalue = Double.parseDouble(emeta.getDataValues().get(fid)
                                    .toString());
        if (fvalue == mvalue || fvalue == svalue || fvalue == evalue) {
          if (fvalue == mvalue) {
            tid = middle;
          } else if (fvalue == svalue) {
            tid = start;
          } else {
            tid = end;
          }
          match = true;
          break;
          } else if (Math.min(svalue, mvalue) < fvalue
                     && Math.max(svalue, mvalue) > fvalue) {
            //                    start = start;
            end = middle;
            middle = (end + start) / 2;
            if (middle == end) {
              break;
            }
            emeta = mmeta;
            mmeta = _pipeline.getDataLoader().getDataSelector().
            getTraceHeader(middle);
            } else if (Math.min(evalue, mvalue) < fvalue
                       && Math.max(evalue, mvalue) > fvalue) {
              start = middle;
              //                    end = end;
              middle = (end + start) / 2;
              if (middle == start) {
                break;
              }
              smeta = mmeta;
              mmeta = _pipeline.getDataLoader().getDataSelector().
                getTraceHeader(middle);
            } else {
              break;
            }
      } else {
        break;
      }
    }
    return tid;
  }

  /**
   * This method calculates the device position according to the trace id and
   * the virtual vertical value.
   *
   * @param tid
   *            trace id.
   * @param vvalue
   *            virtual vertical reading.
   * @return a Point in the device space.
   */
  public Point reverseSelectByPoint(int tid, double vvalue) {
    int resultx = 0;
    int resulty = 0;
    //FIXME: IS THIS INTENTIONAL? tid getting reassigned
    DataChooser dataSelector = _pipeline.getDataLoader().getDataSelector();
    if (tid < dataSelector.getVirtualModelLimits().getMinX()) {
      tid = (int) (dataSelector.getVirtualModelLimits().getMinX());
    } else if (tid > dataSelector.getVirtualModelLimits().getMaxX()) {
      tid = (int) (dataSelector.getVirtualModelLimits().getMaxX());
    }
    // FIXME : this is an ugly hack. but it works.
    double interx = 0;
    double intery = 0;
    if (tid == 0) {
      interx = 0;
    } else {
      interx = (0.5 + tid) * getLayerHorizontalScale();
    }
    if (vvalue == 0) {
      intery = 0;
    } else {
      intery = (0.5 + vvalue) * getLayerVerticalScale();
    }

    Bound2D rect = _transformation.transform(new Bound2D(0, 0, interx,
        intery));
    resultx = (int) (rect.getMaxX());
    resulty = (int) (rect.getMaxY());

    return new Point(resultx, resulty);
  }

  /**
   * This method calculates the virtual position according to the device
   * position.
   *
   * @return a Point where x is the virtual trace id, and y is the virtual
   *         vertical position
   */
  // method to calculate the model position according to device position
  // (including the view position)
  public Point selectByPoint(Point deviceP) {
    int virtTraceId = -999;
    int virtualY = 0;
    if (_pipeline != null && _reader != null) {
      double scalex = this.getLayerHorizontalScale();
      double scaley = this.getLayerVerticalScale();
      
      Bound2D box = _transformation.inverseTransform(new Bound2D(0, 0,
          deviceP.x, deviceP.y));
      int finalx = (int) (Math.floor(box.getMaxX() / scalex));
      int finaly = (int) (Math.floor(box.getMaxY() / scaley));
      virtualY = finaly;
      
      DataChooser dataSelector = _pipeline.getDataLoader().getDataSelector();
      if (finalx >= dataSelector.getVirtualModelLimits().getMaxX()) {
        virtTraceId = (int) (dataSelector.getVirtualModelLimits()
                             .getMaxX()) - 1;
      } else {
        virtTraceId = finalx;
      }
    }
    Point result = new Point(virtTraceId, virtualY);
    return result;
  }

  /**
   * Sets up the data reader of the layer.
   */
  public void setupSeismicReader() {
    _viewer.changeLoadingLayerNumber(BhpBusyDialog.ADD_LAYER);
    _reader = BhpDataAdapterFactory.createBhpDataReader(this, _viewer.getPropertyManager());

    if (_reader != null && !_reader.isThreaded()) {
      if (_pipeline == null) {
        this.setupSeismicDisplay();
      } else {
        this.updateSeismicDisplay();
      }
    }
  }

  protected void updateColorMap(SeismicColorMap source) {
    SeismicColorMap cmap = _pipeline.getTraceRasterizer().getColorMap();
    cmap.setNotify(false);
    int size = source.getSize();
    int bgIndex = new Byte(source.getBackgroundIndex()).intValue();
    int trans;
    Color sc, nc;
    size = Math.min(size, cmap.getSize());
    cmap.setSize(size);
    for (int i = 0; i < size; i++) {
      sc = source.getColor(i);
      if (i == bgIndex) {
        trans = 0;
      } else {
        trans = _transparent;
      }
      nc = new Color(sc.getRed(), sc.getGreen(), sc.getBlue(), trans);
      cmap.setColor(i, nc, false);
    }
    cmap.setNotify(true);
  }

  private void updateDataSelector(DataChooser src) {
    DataChooser des = _pipeline.getDataLoader().getDataSelector();
    des.setPrimaryKey(src.getPrimaryKey());
    des.setPrimaryKeyEnd(src.getPrimaryKeyEnd());
    des.setPrimaryKeyStart(src.getPrimaryKeyStart());
    des.setPrimaryKeyStep(src.getPrimaryKeyStep());
    des.setSecondaryKey(src.getSecondaryKey());
    des.setSecondaryKeyEnd(src.getSecondaryKeyEnd());
    des.setSecondaryKeyStart(src.getSecondaryKeyStart());
    des.setSecondaryKeyStep(src.getSecondaryKeyStep());
    des.applyGaps(src.isGapApplied());
    des.setGapInTraces(src.getGapInTraces());
  }

  /**
   * This method is called by BhpLayer.receiveBhpLayerEvent to provide
   * functionality that is common to its subclasses.
   */
  protected int handleBhpLayerEvent(BhpLayerEvent e) {
    BhpLayer layer = e.getSource();
    int flag = e.getTypeFlag();
    int sflag = this.getSynFlag();
    int mflag = 0;

    if (((flag & BhpViewer.TIMERANGE_FLAG) != 0) && ((sflag & BhpViewer.TIMERANGE_FLAG) != 0)) {
      this.setTimeRange(layer.getTimeRangeStart(), layer
                        .getTimeRangeEnd());
      mflag = mflag | BhpViewer.TIMERANGE_FLAG;
    }

    // AGC_FLAG is only for seismic layer
    // WIGGLEDECIMATION_FLAG is now for seismic layer only
    if (((flag & BhpViewer.NORMALIZE_FLAG) != 0) && ((sflag & BhpViewer.NORMALIZE_FLAG) != 0)
        && (_pipeline != null)  &&
        (_pipeline.getInterpretation().getTraceNormalization() != null)) {
      TraceNormalizer traceNormalization =
          layer.getPipeline().getInterpretation().getTraceNormalization();
      int nNormMode = traceNormalization.getNormalizationMode();
      _pipeline.getInterpretation().getTraceNormalization().setNormalizationMode(nNormMode);
      if (nNormMode == TraceNormalizer.AMPLITUDE_LIMITS) {
        _pipeline.getInterpretation().getTraceNormalization()
            .setNormalizationLimits(traceNormalization.getNormLimitMax(),
                    traceNormalization.getNormLimitMin());
      }
      _pipeline.invalidate(_pipeline.getInterpretation().getTraceNormalization());
      mflag = mflag | BhpViewer.NORMALIZE_FLAG;
    }
    if (((flag & BhpViewer.SELECTOR_FLAG) != 0) && ((sflag & BhpViewer.SELECTOR_FLAG) != 0)
        && (_pipeline != null) && (_pipeline.getDataLoader().getDataSelector() != null)) {
      this.updateDataSelector(layer.getPipeline().getDataLoader().getDataSelector());
      _pipeline.invalidate(_pipeline.getDataLoader().getDataSelector());
      mflag = mflag | BhpViewer.SELECTOR_FLAG;
    }
    if (((flag & BhpViewer.DISPLAY_FLAG) != 0) && ((sflag & BhpViewer.DISPLAY_FLAG) != 0)
        && (_pipeline != null)
        && (_pipeline.getTraceRasterizer() != null)) {
      //this.setDisplayType(layer.getDisplayType());
      int nDisplayType = layer.getPipeline().getTraceRasterizer()
                       .getPlotType();
      _pipeline.getTraceRasterizer().setPlotType(nDisplayType);
      mflag = mflag | BhpViewer.DISPLAY_FLAG;
    }
    if (((flag & BhpViewer.SCALEAMP_FLAG) != 0) && ((sflag & BhpViewer.SCALEAMP_FLAG) != 0)
        && (_pipeline != null)
        && (_pipeline.getInterpretation().getTraceNormalization() != null)) {
      TraceNormalizer traceNormalization =
          layer.getPipeline().getInterpretation().getTraceNormalization();
      float nAmpScale = traceNormalization.getScale();
      _pipeline.getInterpretation().getTraceNormalization().setScale(nAmpScale);
      _pipeline.invalidate(_pipeline.getInterpretation().getTraceNormalization());
      mflag = mflag | BhpViewer.SCALEAMP_FLAG;
    }
    if (((flag & BhpViewer.VISIBLE_FLAG) != 0) && ((sflag & BhpViewer.VISIBLE_FLAG) != 0)) {
      this.setBhpLayerVisible(layer.isBhpLayerVisible());
      mflag = mflag | BhpViewer.VISIBLE_FLAG;
    }
    if (((flag & BhpViewer.TRANSPARENCY_FLAG) != 0)
        && ((sflag & BhpViewer.TRANSPARENCY_FLAG) != 0)) {
      this.setTransparent(layer.getTransparent());
      mflag = mflag | BhpViewer.TRANSPARENCY_FLAG;
    }
    if (((flag & BhpViewer.COLORBAR_FLAG) != 0) && ((sflag & BhpViewer.COLORBAR_FLAG) != 0)
        && (_pipeline != null)
        && (_pipeline.getTraceRasterizer() != null)) {
      SeismicColorMap sourceCmp = (SeismicColorMap) e.getParameter();
      updateColorMap(sourceCmp);
      mflag = mflag | BhpViewer.COLORBAR_FLAG;
    }
    if (((flag & BhpViewer.COLORINTER_FLAG) != 0) && ((sflag & BhpViewer.COLORINTER_FLAG) != 0)
        && (_pipeline != null)
        && (_pipeline.getTraceRasterizer() != null)) {
      if (_pipeline.getTraceRasterizer() instanceof AbstractBhpColorInterpolatedRasterizer) {
        AbstractBhpColorInterpolatedRasterizer rast = (AbstractBhpColorInterpolatedRasterizer) (_pipeline
            .getTraceRasterizer());
        AbstractBhpColorInterpolatedRasterizer rastSrc = (AbstractBhpColorInterpolatedRasterizer) (layer
            .getPipeline().getTraceRasterizer());
        rast.setColorMapStartValue(rastSrc.getColorMapStartValue());
        rast.setColorMapEndValue(rastSrc.getColorMapEndValue());
        mflag = mflag | BhpViewer.COLORINTER_FLAG;
      }
    }
    if (((flag & BhpViewer.NAME_FLAG) != 0) && ((sflag & BhpViewer.NAME_FLAG) != 0)) {
      this.setDataName(layer.getDataName());
      mflag = mflag | BhpViewer.NAME_FLAG;
    }
    if (((flag & BhpViewer.POLARITY_FLAG) != 0) && ((sflag & BhpViewer.POLARITY_FLAG) != 0)) {
      this.setReversedPolarity(layer.getReversedPolarity());
      _pipeline.invalidate(_pipeline.getDataLoader().getDataReader());
      mflag = mflag | BhpViewer.POLARITY_FLAG;
    }
    // need code to handle cursor, polarity
    if ((flag & BhpViewer.DATAINCREMENT_FLAG) != 0) {
      Integer dir = (Integer) e.getParameter();
      boolean parameterChanged = _parameter.stepChange(dir.intValue());
      if (parameterChanged) {
        mflag = mflag | BhpViewer.DATAINCREMENT_FLAG;
      }
    }
    if ((flag & BhpViewer.DATASELECTION_FLAG) != 0) {
      if (e.getParameter() != null) { // mouse click synchronization

        Hashtable parameter = (Hashtable) (e.getParameter());
        boolean change = _parameter.updateWithHashtable(parameter);
        if (change) {
          mflag = mflag | BhpViewer.DATASELECTION_FLAG;
        }
      } else {
        //BhpSeismicLayer tsLayer = (BhpSeismicLayer) layer;
        boolean change = _parameter.updateWithAnother(layer.getParameter());
        if (change) {
          mflag = mflag | BhpViewer.DATASELECTION_FLAG;
        }
      }
    }
    return mflag;
  }

  public void notifyLayerListener(){
    _viewer.broadcastViewerEvent(
        new BhpLayerEvent(BhpViewer.EVENTVALUE_FLAG, this));
  }

  public double getDeviceHeight(){
      return _deviceBox.getHeight();
  }

  public double getDeviceWidth(){
      return _deviceBox.getWidth();
  }

  public BhpWindow getBhpWindow(){
      return _window;
  }
}
