/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.data;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  The abstract data source for a
 *               <code>{@link BhpLayer}</code>. <br><br>
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public interface GeneralDataSource {
    public static final String DATA_SOURCE_BHPSU = "BHP-SU";
    public static final String DATA_SOURCE_OSP = "OpenSpirit";
    public static final String DATA_SOURCE_ISEGY = "Indexed-Segy";
    public static final String DATA_SOURCE_XMLHRZ = "XML-Horizon";
    public static final String DATA_SOURCE_SEGY = "Segy";
    public static final String DATA_SOURCE_LAS = "WellLog";
    /**
     * Gets the name of this data source implementation.
     */
    public String getDataSourceName();
    /**
     * Gets the name of the data set.
     */
    public String getDataName();
    /**
     * Gets the path of the data set. <br>
     * For different data source, this path may mean different things.
     */
    public String getDataPath();
    /**
     * Gets a brief description of the data set, it can be anything.
     */
    public String getDataAtt();
    /**
     * Gets the type of the data set. <br>
     * The values BHP_DATA_TYPE_XXXX are defined in
     * <code>{@link BhpLayer}</code>.
     * @return the type of the data.
     */
    public int getDataType();
    /**
     * Gets the order of the data set. <br>
     * The values BHP_DATA_ORDER_XXXX are defined in
     * <code>{@link BhpLayer}</code>.
     * @return the order of the data.
     */
    public int getDataOrder();
    /**
     * Gets the available properties/events of the data.
     * @return property/event names separated by ",".
     */
    public String getProperties();
    /**
     * Gets the name of the headers. <br>
     * These headers are usually the keys that used to index the data.
     */
    public String[] getHeaders();
    /**
     * Gets the setting of the specific header.
     * @return a string representing the setting of the header.
     *         The format of the string is: start-end[step].
     */
    public String getHeaderSetting(String header);
    /**
     * Creates a <code>{@link BhpDataReader}</code> for this data source.
     * @param layer the <code>{@link BhpLayer}</code> that this reader will
     *        be associated with.
     * @param pmanager the property manager where program settings can be found.
     */
    public BhpDataReader createBhpDataReader(com.bhpBilliton.viewer2d.BhpLayer layer,
                                             com.bhpBilliton.viewer2d.BhpPropertyManager pmanager);
}
