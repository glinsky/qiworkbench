/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.data;

import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpPropertyManager;
import com.bhpBilliton.viewer2d.BhpSeismicTableModel;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.dataAdapter.bhpsu.BhpSegyReader;
import com.bhpBilliton.viewer2d.ui.OpenDataPanel;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import javax.swing.JOptionPane;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  Factory to create different implementations of
 *               <code>{@link BhpDataReader}</code>. Currently,
 *               <code>{@link BhpSegyReader}</code> and
 *               OpenSpirit reader are supported.
 *               While OpenSpirit reader includes
 *               <code>{@link OspHorizonReader}</code>,
 *               <code>{@link OspVolumeReader}</code>,
 *               <code>{@link OspRemoteHorizonReader}</code>, and
 *               <code>{@link OspRemoteVolumeReader}</code>. <br><br>
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpDataAdapterFactory {
    /**
     * Creates a new instance. <br>
     * The constructor is made private, since this class is not
     * supposed to be instantiated.
     */
    private BhpDataAdapterFactory() {
    }

    public static GeneralDataChooser createDataChooser(BhpViewerBase viewer,
                                     OpenDataPanel opanel, String dataSourceName) {
        String chooserClass = viewer.getPropertyManager().getProperty(dataSourceName);
        if (chooserClass!=null && chooserClass.length()!=0) {
            return findDataChooser(viewer, opanel, chooserClass);
        }
        return null;
    }

    /**
     * Creates a <code>{@link BhpDataReader}</code>.
     * Different reader will be created according to the
     * data source type of <code>{@link BhpLayer}</code>.
     * @param layer a <code>{@link BhpLayer}</code>
     * where the created reader will be used.
     * @param pmanager a <code>{@link BhpPropertyManager}</code>
     * used to retrieve environment settings.
     * @return the newly created reader
     */
    public static BhpDataReader createBhpDataReader(BhpLayer layer, BhpPropertyManager pmanager) {
        BhpSeismicTableModel tm = layer.getParameter();
        if (tm.getDataSource() != null) {
            return tm.getDataSource().createBhpDataReader(layer, pmanager);
        }
        // we are openning a xml configuration session file
        String dataSourceName = layer.getDataSource();
        String cname = pmanager.getProperty(dataSourceName);
        
        if ("".equals(cname)) {
            JOptionPane.showMessageDialog(null,
                    "Unable to load " + dataSourceName + " file(s) because this format has been disabled.\nConvert the files using the qiDataConverter and recreate the workbench session\nor contact technical support for assistance.",
                    "Error loading " + dataSourceName + " file",
                    JOptionPane.WARNING_MESSAGE);
        } else {
            String mname = "createDataSource";
            GeneralDataSource ds = findDataSource(pmanager.getViewer(), cname, mname, layer.getDataName(), layer.getPathlist(), layer.getDataType());
            if (ds != null) {
                tm.setDataSource(ds);
                return ds.createBhpDataReader(layer, pmanager);
            }
        }
        
        return null;
    }

    private static GeneralDataChooser findDataChooser(BhpViewerBase viewer,
                                                    OpenDataPanel opanel, String name) {
        try {
            Class theClass = Class.forName(name);
            if (theClass != null) {
                java.lang.reflect.Constructor[] theConstructors = theClass.getConstructors();
                Object[] parameters = {viewer, opanel};
                GeneralDataChooser theChooser = (GeneralDataChooser) (theConstructors[0].newInstance(parameters));
                return theChooser;
            }
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                        "BhpDataAdapterFactory.findDataChooser failed to construct Class" + name);
            return null;
        }
        return null;
    }

    private static GeneralDataSource findDataSource(BhpViewerBase viewer, String cname, String mname,
                                                    String data, String path, int type) {
        try {
            Class theClass = Class.forName(cname);
            if (theClass != null) {
                Integer dataType = new Integer(type);
                Class[] parameterTypes = {viewer.getClass(), data.getClass(), path.getClass(), dataType.getClass()};
                java.lang.reflect.Method theMethod = theClass.getMethod(mname, parameterTypes);
                Object[] parameters = {viewer, data, path, dataType};
                GeneralDataSource reValue = (GeneralDataSource)(theMethod.invoke(null, parameters));
                return reValue;
            }
        } catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                        "BhpDataAdapterFactory.findDataSource failed because " + ex.getMessage());
            return null;
        }
        return null;
    }
}
