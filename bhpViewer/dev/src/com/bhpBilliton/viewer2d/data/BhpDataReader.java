/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.data;

import com.gwsys.seismic.reader.SeismicReader;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  The abstract reader. Concrete data adapter implementation
 *               should have its own reader inherit from this class,
 *               interacting with the actual data source and extracting
 *               real data. Please refer to the JSeismic documentation
 *               for the detail of cgSeismicReader. <br><br>
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public abstract class BhpDataReader extends SeismicReader {
    protected boolean _reversed;
    protected boolean _reversedDirection;   // flip the axis, fill the data backwards

    /***
     * Sets the flag indicating if the reader is set to reverse the sample value.
     * @param r true if the reader reverse the sample value,
     *         multiple the original value by -1.
     *         False if the original value is used directly.
     */
    public void setReversed(boolean r) {
        _reversed = r;
    }

    /**
     * Sets the flag indicating if the reader is set to reverse the direction.
     * @param r true if the reader fills the sample data array backwards,
     *         where the last sample becomes the first one.
     *         False if the reader fills the sample data array normally.
     */
    public void setReversedDirection(boolean r) {
        _reversedDirection = r;
    }

    /**
     * Finds out if the reader is running another thread.
     * @ return true if the reader uses another thread; false otherwise.
     */
    public abstract boolean isThreaded();
    /**
     * Stops the data loading. <br>
     * This method only makes sense if the reader is threaded.
     * However, it can also be used to do the clean up job if necessary.
     * By default, it does nothing.
     */
    public void stopLoading() {}
    /**
     * Cleans up. <br>
     * This method is called when the reader is about to be
     * destroied, or redefined. All the resources held should
     * be freed. If any temporary file is generated, it should
     * be deleted.
     * Its default implementation does nothing.
     */
    public void cleanupDataReader() {
    }
    /**
     * Gets the parameter string used when query the data for the reader. <br>
     * The returned string can be used as the parameter of some other
     * process. For example, bhpControl.SelectedScriptAction uses the
     * string as the parameter of some external shell program. <br>
     * Its default implementation returns an empty string.
     */
    public String getParameterString() {
        return "";
    }
    public double getDefinedNullValue() {
        return -999.25;
    }
}
