/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.text.NumberFormat;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This panel is the status bar showing the mouse
 *               information in the <code>{@link BhpWindow}</code>.
 *               To be more specific, it displays the reading of
 *               the vertical and horizontal annotations. <br><br>
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpWindowStatusBar extends JPanel implements BhpMouseTrackListener {
    private JTextField _vlabel;
    private JTextField _hlabel;
    private boolean _showVerticalName;
    private boolean _enableStatusbar;

    /**
     * Constructs the status bar and sets up its GUI.
     */
    public BhpWindowStatusBar(boolean showVerticalName) {
        super();
        _showVerticalName = showVerticalName;
        setupGUI();
    }

    /**
     * This method is called for mouse tracking to update this status bar.
     * @param vname the field name of the vertical annotation.
     * @param parameter annotation readings, the first element is the
     *        vertical annotation, the rest are the horizontal annotations.
     */
    public void handleMouseTracking(String vname, Hashtable parameter) {

        if (parameter == null || parameter.size() == 0) {
        _vlabel.setText("       ");
            _hlabel.setText("       ");
        return;
    }

        NumberFormat nformat = NumberFormat.getInstance();
        nformat.setMaximumFractionDigits(3);
        String vvalue = "";
        String valuevalue = null;
        StringBuffer hbuffer = new StringBuffer();
        Enumeration keyList = parameter.keys();
        Object keyObj = null;
        while(keyList.hasMoreElements()) {
            keyObj = keyList.nextElement();
            if (vname.equals(keyObj.toString()))
                vvalue = parameter.get(keyObj).toString();
            else if (keyObj.toString().equals("value"))
                valuevalue = parameter.get(keyObj).toString();
            else
                hbuffer.append(" " + keyObj.toString() + ":" + parameter.get(keyObj).toString());
        }
        if (valuevalue != null) {
            try {
                double doublevalue = Double.parseDouble(valuevalue);
                hbuffer.append(" " + "value:" + nformat.format(doublevalue));
            } catch (Exception ex) {
                hbuffer.append(" " + "value:" + valuevalue);
            }
        }

        String vvalueString = nformat.format(Double.parseDouble(vvalue));
        if (_showVerticalName)
            vvalueString = (vname + " : " + vvalueString);
        _vlabel.setText(vvalueString);
        _hlabel.setText(hbuffer.toString());

    }

    private void setupGUI() {
        JLabel vl = new JLabel("  Vertical: ");
        JLabel hl = new JLabel("  Horizontal: ");
        _vlabel = new JTextField("      ");
        _hlabel = new JTextField("      ");
        _vlabel.setSize(new Dimension(100, 25));
        _vlabel.setPreferredSize(new Dimension(100, 25));
        _vlabel.setMinimumSize(new Dimension(100, 25));
        _vlabel.setMaximumSize(new Dimension(100, 25));
        _hlabel.setMinimumSize(new Dimension(100, 25));

        GridBagLayout grid = new GridBagLayout();
        this.setLayout(grid);

        GridBagConstraints cons1 = new GridBagConstraints();
        cons1.gridx = 0;
        cons1.gridy = 0;
        cons1.gridheight = 1;
        cons1.gridwidth = 1;
        cons1.weightx = 0;
        cons1.weighty = 0;
        grid.setConstraints(vl, cons1);
        this.add(vl);
        GridBagConstraints cons2 = new GridBagConstraints();
        cons2.gridx = GridBagConstraints.RELATIVE;
        cons2.gridy = 0;
        cons2.gridheight = 1;
        cons2.gridwidth = 1;
        cons2.weightx = 0;
        cons2.weighty = 0;
        grid.setConstraints(_vlabel, cons2);
        this.add(_vlabel);
        GridBagConstraints cons3 = new GridBagConstraints();
        cons3.gridx = GridBagConstraints.RELATIVE;
        cons3.gridy = 0;
        cons3.gridheight = 1;
        cons3.gridwidth = 1;
        cons3.weightx = 0;
        cons3.weighty = 0;
        grid.setConstraints(hl, cons3);
        this.add(hl);
        GridBagConstraints cons4 = new GridBagConstraints();
        cons4.gridx = 3;
        cons4.gridy = 0;
        cons4.gridheight = 1;
        cons4.gridwidth = GridBagConstraints.REMAINDER;
        cons4.weightx = 10;
        cons4.weighty = 0;
        cons4.fill = GridBagConstraints.BOTH;
        grid.setConstraints(_hlabel, cons4);
        this.add(_hlabel);
    }

}
