/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.actions;

import java.awt.event.*;
import javax.swing.*;

import com.bhpBilliton.viewer2d.*;
import com.bhpBilliton.viewer2d.ui.OpenDataDialog;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This action will pop up a dialog with OpenDataPanel.
 *               A user can select the data and specify query parameters for
 *               bhpio. A new layer will be generated for the selected data,
 *               and the layer will be inserted into the selected window.
 *               <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class NewLayerAction extends AbstractAction {
    private String _dataSourceName;
    private BhpViewerBase _viewer;
    private OpenDataDialog _dialog;
    private BhpLayer _layer;

    public NewLayerAction(BhpViewerBase viewer, String dsname) {
        super(dsname + " ...", "Add a new layer of " + dsname + " data to the active window");
        _viewer = viewer;
        _dataSourceName = dsname;
    }

    public void actionPerformed(ActionEvent e) {
        BhpWindow win = (BhpWindow) (_viewer.getDesktop().getSelectedFrame());
        _layer = null;
        if (win != null) {
            _layer = win.getSelectedLayer();
        }
        else{
          JOptionPane.showMessageDialog(null,"Select Seismic Layer First!");
          return;
        }
        boolean isMap = false;
        if (win.getBhpPlot() instanceof BhpPlotMV) {
            isMap = true;
        }
        _dialog = new OpenDataDialog(_viewer, _layer, isMap);
        _dialog.pack();
        _dialog.setSize(700, 400);
        _dialog.getOpenPanel().showChooser(_dataSourceName);
    }
}
