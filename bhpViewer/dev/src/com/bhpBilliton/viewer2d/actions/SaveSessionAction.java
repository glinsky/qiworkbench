/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

import com.bhpBilliton.viewer2d.Bhp2DviewerAgent;
import com.bhpBilliton.viewer2d.Bhp2DviewerConstants;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.util.FileChooserDialog;
import com.bhpBilliton.viewer2d.util.GenericFileFilter;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This action will save the current working session in an XML
 *               configuration file. This file can be loaded with
 *               OpenSessionAction to restore the working session.
 *               <br><br>
 *
 * Copyright:    Copyright (c) 2001-2006 <br>
 * @author Synthia Kong
 * @author Gil Hansen
 * @version 1.1
 */

public class SaveSessionAction extends AbstractAction {
    private BhpViewerBase _viewer;

    public SaveSessionAction(BhpViewerBase viewer) {
        super("Save Session ...", "Save current work session to a file");
        _viewer = viewer;
    }

    public void actionPerformed(ActionEvent e) {
        //send a message to the component and request for state information returned as xml string
        IMessagingManager messaginMgr = ((Bhp2DviewerAgent)_viewer.getAgent()).getMessagingManager();
        // get state manager who will handle the state serialization
        IComponentDescriptor stMgrDesc = messaginMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        String msgID = messaginMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_CMD,stMgrDesc,
                                             QIWConstants.STRING_TYPE,messaginMgr.getMyComponentDesc());
        //use new saving mechanism
        if(true)
            return;

      //Get relative path of savesets
      QiProjectDescriptor projDesc = _viewer.getAgent().getQiProjectDescriptor();
      String savesetsRelPath = QiProjectDescUtils.getSavesetsReloc(projDesc);

      FileChooserDialog fchooser = new FileChooserDialog(_viewer,"Save Session",_viewer.getAgent().getMessagingManager());
      fchooser.addChoosableFileFilter(getSessionFileFilter());
      fchooser.setPathFieldText(fchooser.getPath() + "/" + savesetsRelPath);
      int reFchooser = fchooser.showSaveDialog();
      if(reFchooser == JFileChooser.APPROVE_OPTION) {
        String fileName = fchooser.getPath() + "/" + fchooser.getName();
        if(!fileName.endsWith(".cfg"))
          fileName += ".cfg";
        saveSessionFile(fileName);
      }
      else
        JOptionPane.showMessageDialog(_viewer,"Save Session Cancelled");
    }

    /**
     * @return
     */
    private FileFilter getSessionFileFilter() {
        String[] cfgExtensions = new String[]{".cfg", ".CFG"};
        FileFilter cfgFilter = (FileFilter) new GenericFileFilter(cfgExtensions, "Configuration File (*.cfg)");
        return cfgFilter;
    }

    private void saveSessionFile(String fileName) {
      String msgID = "";
      StringBuffer content = new StringBuffer();
      ArrayList<String> params = new ArrayList<String>();
      params.add(_viewer.getAgent().getMessagingManager().getLocationPref());
      params.add(fileName);
      content.append("<?xml version=\"1.0\" ?>\n");
      content.append(_viewer.toXMLString());
      params.add(content.toString());
      msgID = _viewer.getAgent().getMessagingManager().sendRequest(QIWConstants.CMD_MSG,QIWConstants.FILE_WRITE_CMD,QIWConstants.ARRAYLIST_TYPE,params);
      IQiWorkbenchMsg response = _viewer.getAgent().getMessagingManager().getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
      if(MsgUtils.isResponseAbnormal(response))
        JOptionPane.showMessageDialog(_viewer,"Error writing " + fileName);
    }
}