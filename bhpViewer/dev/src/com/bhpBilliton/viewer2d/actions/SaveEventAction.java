/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.actions;

import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

import com.bhpBilliton.viewer2d.*;
import com.bhpBilliton.viewer2d.ui.BhpEventSavingDialog;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This action will save the event data of the currently selected
 *               layer of the selected frame back to disk with bhpwrite.
 *               If there is no layer selected or the selected layer is not
 *               an instance of BhpEventLayer, this action will return immediately.
 *               <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class SaveEventAction extends AbstractAction {
    private BhpViewerBase _viewer;
    BhpEventSavingDialog _dialog = null;

    public SaveEventAction(BhpViewerBase viewer) {
        super("Save Horizon ...", "Save the horizon of all the currently selected event layers");
        _viewer = viewer;
    }

    public void actionPerformed(ActionEvent e) {
        BhpWindow win = (BhpWindow) (_viewer.getDesktop().getSelectedFrame());
        BhpLayer[] layer = win.getSelectedLayers();
        if (layer == null || layer.length == 0) {
            JOptionPane.showMessageDialog(_viewer,"Please select a horizon layer first.");
            return;
        }

        if (layer[0].getDataSource().equals(
                com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_BHPSU)) {
            for (int i=0; i<layer.length; i++) {
                if ((layer[i] instanceof BhpEventLayer) &&
                    (layer[i].getDataSource().equals(com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_BHPSU))) {
                    String fileName = layer[i].getPathlist();
                    String layerName = layer[i].getLayerName();
                    String msg = "Saving BHP-SU horizon "+layerName+" to file " + fileName;

                    //Note: Cannot write to status bar until action handler finishes.
//                    System.out.println(msg);
//                    _viewer.getStatusBar().showMessage(BhpViewer.INFO_MESSAGE, msg);

                    ((BhpEventLayer)layer[i]).saveEvent();
                }
            }
            _viewer.getStatusBar().showMessage(BhpViewer.INFO_MESSAGE, "BHP-SU horizon data saved.");
        }
        else {
            // save xmlhorizon
            if (_dialog == null) _dialog = new BhpEventSavingDialog(_viewer);
            _dialog.initWithBhpWindow(win);
            _dialog.setVisible(true);
        }
    }
}
