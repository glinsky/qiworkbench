/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.actions;

import javax.swing.*;

import java.awt.event.*;
import javax.help.CSH;

import com.bhpBilliton.viewer2d.util.IconResource;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is the base class for all the actions in the package.
 *               It provides methods that are common for all the actions.
 *               This abstrace class makes actions easy to be added to either
 *               a menu bar or a tool bar. Its subclasses need only
 *               to implement the method actionPerformed to carry on the actual
 *               task of the action. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public abstract class AbstractAction implements ActionListener {
    private final String _menuText;
    private final String _tooltip;
	private final String iconName;

    public AbstractAction(String menu, String tooltip) {
        this(menu,tooltip,"");
    }

    public AbstractAction(String menu, String tooltip, String iconName) {
        this._menuText = menu;
        this._tooltip = tooltip;
        this.iconName = iconName;
    }

    public abstract void actionPerformed(ActionEvent e);

    public final String getTooltip() { return _tooltip; }

    public final String getMenuText() { return _menuText; }

    public final String getIconName() { return iconName; }

    public JMenuItem addMenuItem(JMenu menu, ImageIcon icon) {
        JMenuItem mi = createMenuItem(icon);
        menu.add(mi);
        return mi;
    }

    public JMenuItem addMenuItem(JPopupMenu menu, ImageIcon icon) {
        JMenuItem mi = createMenuItem(icon);
        menu.add(mi);
        return mi;
    }

    public JCheckBoxMenuItem addCheckBoxMenuItem(JMenu menu, ImageIcon icon, boolean state) {
        JCheckBoxMenuItem mi = createCheckBoxMenuItem(icon, state);
        menu.add(mi);
        return mi;
    }

    public JCheckBoxMenuItem addCheckBoxMenuItem(JPopupMenu menu, ImageIcon icon, boolean state) {
        JCheckBoxMenuItem mi = createCheckBoxMenuItem(icon, state);
        menu.add(mi);
        return mi;
    }

    public JButton addToToolBar(JToolBar toolbar, ImageIcon icon) {
    	JButton btn=getButton(icon);
        toolbar.add(btn);
        return btn;
    }

    /**
	 * @param icon
	 * @return
	 */
	public JButton getButton(ImageIcon icon) {
		JButton btn = new JButton();
    	if (icon != null) {
	        btn.setIcon(icon);
    	}
        btn.setToolTipText(getTooltip());
        btn.addActionListener(this);
        CSH.setHelpIDString(btn,_menuText);
		return btn;
	}

	public JButton getButton() {
		JButton btn;
		if (iconName.length()>0) {
			btn = getButton(IconResource.getInstance().getImageIcon(iconName));
		} else {
			btn = getButton(null);
		}
		return (btn);
	}

	private JMenuItem createMenuItem(ImageIcon icon) {
        JMenuItem mi = new JMenuItem(getMenuText());
        if (icon != null) {
            mi.setIcon(icon);
        }
        mi.setToolTipText(getTooltip());
        mi.addActionListener(this);
        return mi;
    }

	public JMenuItem getMenuItem (ImageIcon icon) {
		return createMenuItem(icon);
	}
	public JMenuItem getMenuItem () {
		return createMenuItem(null);
	}
	public JCheckBoxMenuItem getCheckBoxMenuItem () {
		return createCheckBoxMenuItem(null, true);
	}
	public JCheckBoxMenuItem getCheckBoxMenuItem (boolean state) {
		return createCheckBoxMenuItem(null, state);
	}
    private JCheckBoxMenuItem createCheckBoxMenuItem(ImageIcon icon, boolean state) {
        JCheckBoxMenuItem mi = new JCheckBoxMenuItem(getMenuText(), state);
        if (icon != null) {
            mi.setIcon(icon);
        }
        mi.setToolTipText(getTooltip());
        mi.addActionListener(this);
        return mi;
    }

}
