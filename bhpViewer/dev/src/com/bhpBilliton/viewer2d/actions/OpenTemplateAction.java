/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.actions;

//import java.awt.*;
import java.awt.Container;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import javax.xml.parsers.*;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import com.bhpBilliton.viewer2d.Bhp2DviewerConstants;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.BhpViewerHelper;
import com.bhpBilliton.viewer2d.dataAdapter.bhpsu.BhpIODataSource;
import com.bhpBilliton.viewer2d.dataAdapter.bhpsu.BhpIODataSummary;
import com.bhpBilliton.viewer2d.ui.DataFileSubstitutionDialog;
import com.bhpBilliton.viewer2d.ui.DataLayerHandle;
import com.bhpBilliton.viewer2d.ui.DataLayerSubstitutionHandle;
import com.bhpBilliton.viewer2d.ui.util.ElementAttributeReader;
import com.bhpBilliton.viewer2d.ui.util.FileTools;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpBilliton.viewer2d.util.GenericFileFilter;
import com.bhpBilliton.viewer2d.util.FileChooserDialog;

import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This action enables the user to select a saved configuration
 *               file and open it as a 'template'.
 *               <br><br>
 *
 * Copyright:    Copyright (c) 2001-2006 <br>
 * @author Synthia Kong
 * @author Gil Hansen
 * @version 1.1
 */

// Removed applet references
// TODO Implement Local/Remote file services

public class OpenTemplateAction extends AbstractAction {
    private BhpViewerBase _viewer;
    private String currentDirectory;
    private SaveSessionAction saverAction;
    private FileChooserDialog remoteFileChooser;
    private static String bhpFileConfigPathProperty;

    public OpenTemplateAction(BhpViewerBase viewer, SaveSessionAction saver) {
        super("Open Session as Template ...", "Open a saved session file as a template");
        _viewer = viewer;
        saverAction = saver;
        currentDirectory=null;
    }

    public void actionPerformed(ActionEvent e) {
        if(cancelOpenSessionByUser()) return;
        String fileName=null;
        setCurrentDirectory();

        InputStream xmlStream=null;

        if (_viewer.getAgent().getMessagingManager().getLocationPref().equals(QIWConstants.REMOTE_SERVICE_PREF)) {
            /*ArrayList list = new ArrayList();
            //1st element the parent GUI object
            list.add(_viewer);
            //2nd element is the dialog title
            list.add("Open Session File as Template");
            //3rd element is the current directory to start with
            list.add(dir);
            //4th element is the file filter
            list.add(getSessionFileFilter());
            //5th element is the navigation flag
            list.add(true);
            //6th element is the producer thread
            list.add(_viewer.getAgent());
            //7th element is the type of file chooser either Open or Save
            list.add("Open");
            //8th element is the original qiworkbench message command that request for this service
            list.add("");
            _viewer.getAgent().doFileChooser(list);
            */
            if (remoteFileChooser == null) {
                remoteFileChooser = new FileChooserDialog(_viewer, "Open Session File as Template", _viewer.getAgent().getMessagingManager());
                remoteFileChooser.addChoosableFileFilter(getSessionFileFilter());
            }
            int reFchooser = remoteFileChooser.showOpenDialog();
            if (reFchooser == JFileChooser.APPROVE_OPTION) {
                fileName = getRemoteFileName(remoteFileChooser);
                currentDirectory = FileTools.getPathFromFilePath(fileName);
                xmlStream=openRemote(fileName);
            } else {
//TODO: show message to user indicating they were not allowed to read file
                System.out.println("not approved " + reFchooser);
            }
        } else {
         /* qiwb deprecates
            JFileChooser fchooser = getLocalConfigFileChooser();
            int reFchooser = fchooser.showOpenDialog(_viewer);
            if (reFchooser == JFileChooser.APPROVE_OPTION) {
                fileName = fchooser.getSelectedFile().getAbsolutePath();
                currentDirectory = fchooser.getCurrentDirectory().getAbsolutePath();
         */

            FileChooserDialog fchooser = new FileChooserDialog(_viewer, "Open Session File as Template", _viewer.getAgent().getMessagingManager());
            fchooser.addChoosableFileFilter(getSessionFileFilter());
            int reFchooser = fchooser.showOpenDialog();
            if (reFchooser == JFileChooser.APPROVE_OPTION) {
                fileName = fchooser.getSelectedFileAbsolutePathname();
                currentDirectory = fchooser.getSelectedDirAbsolutePathname();
                File file = new File(fileName);
                try {
                    xmlStream=openLocal(file);
                }
                catch (Exception ex) {
                    ErrorDialog.showErrorDialog(_viewer, QIWConstants.ERROR_DIALOG,
                                                Thread.currentThread().getStackTrace(),
                                                ("IO error loading session file"),
                                                new String[] {"Read permission not set",
                                                              "File is empty"},
                                                new String[] {"Fix permission",
                                                       "If file already OK, contact workbench support"});
                }
            }
        }
        if(xmlStream != null)
            loadTemplate(fileName, xmlStream);
    }

    /**
     * The whole session-as-template shamozzle including:
     *  parse xml, find seismic elements, prompt for replacements, replace, attempt load!
     * @param fileName The name of the file that was processed. Only used to label the document tree.
     * @param xmlStream An imput stream containing XML data.
     */
    private void loadTemplate(String fileName, InputStream xmlStream) {
        Document sessionDoc = null;
        DataFileSubstitutionDialog dfsDialog;
        try {
            sessionDoc = parseSessionXMLStream(xmlStream);
            sessionDoc.getDocumentElement().setAttribute("title", fileName);
            ArrayList dataHandles = (ArrayList) extractFileNamesFromXML(sessionDoc);
            dfsDialog = new DataFileSubstitutionDialog(_viewer);
            dfsDialog.setCurrentPath(currentDirectory);
            dfsDialog.setBase(_viewer);
            dfsDialog.addHandle(dataHandles);
            boolean keepGoing = true;
            while (keepGoing) {
                if (dfsDialog.showDialog() == DataFileSubstitutionDialog.APPROVE_OPTION) {
                    Map subs = dfsDialog.getFileSubstitutionMap();
                    Map badSubs = getInvalidSubstitutions (subs);
                    if (badSubs.isEmpty()) { // no problems here
                        keepGoing = false;
                        performDataSubstitutions (sessionDoc, subs);
                        adjustDataRanges(sessionDoc);
                        _viewer.initWithXML( sessionDoc.getDocumentElement());
                    } else {
                        // update warning states
                        for (Iterator i = subs.keySet().iterator();i.hasNext();) {
                            String subID = (String) i.next();

                            dfsDialog.setWarningState(subID,badSubs.containsKey(subID));
                        }
                        showWarningDialog(dfsDialog.getView(),badSubs);
                    }
                } else {
                    keepGoing = false;
                }
            }
        }
        catch (IOException ex) {
            ErrorDialog.showErrorDialog(_viewer, QIWConstants.ERROR_DIALOG,
                                        Thread.currentThread().getStackTrace(),
                                        ("IO error loading session file"),
                                        new String[] {"Read permission not set",
                                                      "File is empty"},
                                        new String[] {"Fix permission",
                                                      "If file already OK, contact workbench support"});
        } catch (ParserConfigurationException ex) {
            // TODO Auto-generated catch block
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "ParserConfigurationException");
        } catch (SAXException ex) {
            // TODO Auto-generated catch block
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "SAXException");
        }
    }

    private void showWarningDialog(Container frame, Map badSubs) {
        StringBuffer warningMessage = new StringBuffer();

        for (Iterator i = badSubs.keySet().iterator();i.hasNext();) {
            String id = (String) i.next();
            List missingKeys = (List) badSubs.get(id);
            for (Iterator j = missingKeys.iterator(); j.hasNext();) {
                String[] output = ((String) j.next()).split("\\$",2);
                warningMessage.append(output[1] + " does not contain the key '"+ output[0]+"'\n");
            }
        }

        JOptionPane.showMessageDialog(frame, warningMessage.toString());
    }

    private Map getInvalidSubstitutions(Map substitutions) {
        Map badSubs = new HashMap();
        for (Iterator i = substitutions.keySet().iterator();i.hasNext();) {
            DataLayerSubstitutionHandle substitution = (DataLayerSubstitutionHandle) substitutions.get(i.next());
            List missingKeys = substitution.getMissingKeys();
            if (! missingKeys.isEmpty()) {
                badSubs.put (substitution.getIdentifier(),missingKeys);
            }
        }
        return badSubs;
    }

    private void adjustDataRanges (Document xmlTree) {
        Collection nodes = getNodesByName(xmlTree, "Parameter");

        for (Iterator i = nodes.iterator();i.hasNext();) {
            ElementAttributeReader emt = new ElementAttributeReader((Element) i.next());

            ElementAttributeReader layerNode = new ElementAttributeReader(emt.getParentNode());

            String dataName = layerNode.getString("dataName");
            String pathList = layerNode.getString("pathlist");
            String name = layerNode.getString("name");
            String type = layerNode.getString("dsType");

            // get new summary
            BhpIODataSummary summary = new BhpIODataSummary (_viewer.getPropertyManager(), dataName, pathList,_viewer);
            BhpIODataSource summaryData = summary.getDataSource();

            String parameterString = emt.getString("content");

            // adjust old settings
            String[] headers = summaryData.getHeaders();
            for (int j = 0;j < headers.length;j++) {    // each header in the new dataset
                String header = headers[j].trim();
                int headerPos = parameterString.indexOf(header);
                if ( headerPos >=0 ) {                  // header exists in parameters, replace it
                    String left = parameterString.substring(0,headerPos + header.length() + 1);
                    String right = parameterString.substring(headerPos + header.length() + 1);
                    right = right.substring(right.indexOf('|'));
                    parameterString = left + summaryData.getHeaderSetting(headers[j]) + right;
                }
            }

            // store new settings in xml tree
            emt.setAttribute("content",parameterString);
        }
    }

    private void performDataSubstitutions (Document xmlTree, Map substitutions) {
        Collection nodes = getNodesByName(xmlTree,"BhpLayer");

        for (Iterator i = nodes.iterator();i.hasNext();) {
            ElementAttributeReader emt = new ElementAttributeReader((Element) i.next());

            DataLayerHandle dlh = new DataLayerHandle(emt.getString("name"),emt.getString("dataName"),emt.getString("pathlist"),_viewer);
            String id = dlh.getIdentifier();
            if (substitutions.containsKey(id)) {
                // this layer needs a substitution...
                DataLayerSubstitutionHandle substitution = (DataLayerSubstitutionHandle) substitutions.get(id);

                emt.setAttribute("name",substitution.getNewName());
                emt.setAttribute("dataName",substitution.getNewName());
                emt.setAttribute("pathlist",substitution.getNewPathlist());
                if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
                    System.out.println ("Substitution: " + dlh.getFull() + "     New: " + substitution.getNewHandle().getFull());
            }
        }

    }
    private Collection getNodesByName (Document tree, String name) {
        ArrayList dataHandles = new ArrayList();

        NodeList nnm = tree.getElementsByTagName(name);
        for (int i=0;i<nnm.getLength();i++) {
            dataHandles.add( nnm.item(i));
        }
        return (dataHandles);
    }

    private String getRemoteFileName(FileChooserDialog rfc) {
        String pathString = rfc.getPath();
        String name = rfc.getName();
        String absoluteName = pathString + "/" + name;
        if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("(" + name + ") OpenSessionTemplate " + absoluteName);
        return absoluteName;
    }


  /* deprecated
    private JFileChooser getLocalConfigFileChooser() {
        JFileChooser fchooser = new JFileChooser(new File(currentDirectory));
        fchooser.addChoosableFileFilter(getSessionFileFilter());
        fchooser.setDialogTitle("Open Session File as Template");
        fchooser.setMultiSelectionEnabled(false);
        return fchooser;
    }
  */

    private void setCurrentDirectory() {
        setBhpFileConfigPathProperty();
        if (currentDirectory == null || currentDirectory.length() == 0) {
            if (bhpFileConfigPathProperty!=null) {
                currentDirectory = bhpFileConfigPathProperty;
            } else {
                currentDirectory = ".";
            }
        }

    }

    /**
     *
     */
    private void setBhpFileConfigPathProperty() {
        if (bhpFileConfigPathProperty == null && _viewer!=null) {
            bhpFileConfigPathProperty = _viewer.getPropertyManager().getProperty("bhpFilecfgPath");
            if (bhpFileConfigPathProperty.length()==0) {
                bhpFileConfigPathProperty = new File (".").getAbsolutePath();
            }
        }
    }


    private boolean cancelOpenSessionByUser() {
        boolean cancelAction = false;
        if (_viewer != null && _viewer.getDesktop().getAllFrames().length != 0) {
            // something is open now.
            int choice = JOptionPane.showConfirmDialog(_viewer, "Save the current session?", "Save", JOptionPane.YES_NO_CANCEL_OPTION);
            if (choice == JOptionPane.CANCEL_OPTION) {
                cancelAction = true;
            } else if (choice == JOptionPane.YES_OPTION) {
                if (saverAction != null) {
                    saverAction.actionPerformed(null);
                }
            }
        }

        return cancelAction;
    }

    private GenericFileFilter getSessionFileFilter() {
        String[] cfgExtensions = new String[]{"cfg"};
        GenericFileFilter cfgFilter = new GenericFileFilter(cfgExtensions, "Session File (*.cfg)");
        return cfgFilter;
    }

    //TODO: use workbench IO service
    private InputStream openRemote(String fname) {
        InputStream xmlStream = null;
        try {
            String urlString = _viewer.getPropertyManager().getProperty("servletReadFilecfgURL");
            URL url = new URL(urlString + "?FileName=" + URLEncoder.encode(fname, "iso-8859-1"));
            URLConnection conn = url.openConnection();

            if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
                System.out.println("OpenSessionTemplateAction contentLength " + conn.getContentLength() + " : " + conn.getInputStream().available());

            String connXML = connectionXMLtoString(conn);
            xmlStream = new ByteArrayInputStream(connXML.getBytes());
        }
        catch (Exception e) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "OpenSessionTemplateAction.openRemote Exception");
        }
        return (xmlStream);
    }

    /**
     * @param xmlStream
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    private Document parseSessionXMLStream(InputStream xmlStream) throws ParserConfigurationException, SAXException, IOException {
        Element root = BhpViewerHelper.getXMLTree(xmlStream);
        Document doc = root.getOwnerDocument();
        return (doc);
    }

    private Collection extractFileNamesFromXML (Document tree) {
        ArrayList dataHandles = new ArrayList();

        //slurp those nodes
        NodeList temp = tree.getElementsByTagName("BhpLayer");
        for (int i=0;i<temp.getLength();i++) {
            ElementAttributeReader emt = new ElementAttributeReader(temp.item(i));
            DataLayerHandle dlh = new DataLayerHandle(emt.getString("name"),emt.getString("dataName"),emt.getString("pathlist"),_viewer);
            dataHandles.add(dlh);
        }

        //write back to mum
        return dataHandles;
    }

    private String connectionXMLtoString(URLConnection conn) throws IOException {
        StringBuffer xmlContent = new StringBuffer();
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = reader.readLine();

        while(line != null) {
            xmlContent.append(line);
            line = reader.readLine();
        }
        return (xmlContent.toString());
    }

    //TODO: use workbench IO service
    private InputStream openLocal(File file) throws IOException {
        InputStream xmlStream = new BufferedInputStream(new FileInputStream(file));
        return xmlStream;
    }

}
