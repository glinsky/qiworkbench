/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.actions;

import java.awt.event.*;
import javax.swing.*;

import com.bhpBilliton.viewer2d.BhpViewer;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This action cascades all the internal frames.
 *               <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 * @version 2.0 Use Interface
 */

public class WindowCascadeAction extends AbstractAction {
    private static final int FRAME_DISTANCE_H = 40;
    private static final int FRAME_DISTANCE_V = 40;

    private BhpViewer _viewer;

    public WindowCascadeAction(BhpViewer viewer) {
        super("Cascade All", "Cascade all the windows");
        _viewer = viewer;
    }

    public void actionPerformed(ActionEvent e) {
        JDesktopPane desktop = _viewer.getDesktop();
        JInternalFrame[] frames = desktop.getAllFrames();
        int x = 0;
        int y = 0;
        int width = desktop.getWidth() / 2;
        int height = desktop.getHeight() / 2;

        for (int i=0; i<frames.length; i++) {
            if (!(frames[i].isIcon())) {
                try {
                    frames[i].setMaximum(false);
                    frames[i].reshape(x, y, width, height);
                    x = x + FRAME_DISTANCE_H;
                    y = y + FRAME_DISTANCE_V;
                    if (x+width > desktop.getWidth()) x = 0;
                    if (y+height > desktop.getHeight()) y = 0;
                }
                catch (Exception ex) {
                    String msg = "WindowCascadeAction exception : " + ex;
                    _viewer.getStatusBar().showMessage(BhpViewer.ERROR_MESSAGE, msg);

                }
            }
        }
    }
}
