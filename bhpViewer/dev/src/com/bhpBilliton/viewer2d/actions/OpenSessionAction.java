/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.actions;

import java.awt.event.ActionEvent;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import org.w3c.dom.Element;

import com.bhpBilliton.viewer2d.Bhp2DviewerConstants;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.BhpViewerHelper;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.util.GenericFileFilter;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This action enables the user to select a saved configuration
 *               file and restore the working session. All the currently opened
 *               BhpWindow will be closed. New windows will be created according
 *               to the configuration file. <br><br>
 *
 * Copyright:    Copyright (c) 2001-2006 <br>
 * @author Synthia Kong
 * @author Gil Hansen
 * @version 1.1
 */


public class OpenSessionAction extends AbstractAction {
    private BhpViewerBase _viewer;
    private SaveSessionAction saverAction;

    public OpenSessionAction(BhpViewerBase viewer, SaveSessionAction saver) {
        //super("Open Old Session ...", "Open a saved session file");
    	super("Pre-qiWorkbench Format", "Open a pre-qiWorkbench saved session file");
        _viewer = viewer;
        saverAction = saver;
    }

    public void actionPerformed(ActionEvent e) {
        if(cancelOpenSessionByUser()) return;

          /* qiwb deprecates
            JFileChooser fchooser = getLocalConfigFileChooser();
            int reFchooser = fchooser.showOpenDialog(_viewer);
            if (reFchooser == JFileChooser.APPROVE_OPTION) {
                String fileName = fchooser.getSelectedFile().getAbsolutePath();
                currentDirectory = fchooser.getCurrentDirectory().getAbsolutePath();
          */
        ArrayList list = new ArrayList();
        //1st element the parent GUI object
        list.add(_viewer);
        //2nd element is the dialog title
        list.add("Open Configuration File");
        // 3rd element is a list that contains current directory to start with
          //and a flag (yes or no) indicating if a already remembered directory
          //should be used instead
        ArrayList lst = new ArrayList();
        lst.add(_viewer.getAgent().getMessagingManager().getProject());
        lst.add("yes");
        list.add(lst);
        //4th element is the file filter
        list.add(getSessionFileFilter());
        //5th element is the navigation flag
        list.add(true);
        //6th element is the producer component descriptor
        list.add(_viewer.getAgent().getMessagingManager().getMyComponentDesc());
        //7th element is the type of file chooser either Open or Save
        list.add(QIWConstants.FILE_CHOOSER_TYPE_OPEN);
        //8th element is the message command
        list.add(QIWConstants.OPEN_SESSION_ACTION_BY_2D_VIEWER);
        //9th element is the default pretext for the file name if user fails to pretext it. If no pretext is required then use empty String
        String userName = System.getProperty("user.name");
        list.add(userName);
        //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
        list.add("cfg");
        //11th element is the target tomcat url where the file chooser chooses
        list.add(_viewer.getAgent().getMessagingManager().getTomcatURL());
        try{
        _viewer.getAgent().doFileChooser(list);
        }catch(Exception ex){
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "Cannot start file chooser service");
        }

        /*
        FileChooserDialog fchooser = new FileChooserDialog(_viewer, "Open Configuration File", _viewer.getMsgMgr());
        fchooser.setLocation(_viewer.getMyLocation());
        fchooser.addChoosableFileFilter(getSessionFileFilter());
        fchooser.setPathFieldText(fchooser.getPath() + "/int_savesets");
        int reFchooser = fchooser.showOpenDialog();
        */
        //int reFchooser = ((Integer)lst.get(0)).intValue();
        //String selectedPath = (String)lst.get(1);
        //if (reFchooser == JFileChooser.APPROVE_OPTION)
        //    openSessionFile(selectedPath);
    }

    /**
     * @return
     */
/* Deprecated
    private String getRemoteFileName(FileChooserDialog rfc) {
        String pathString = rfc.getPath();
        String name = rfc.getName();
        String absoluteName = pathString + "/" + name;
        System.out.println("(" + name + ")OpenSession " + absoluteName);
        return absoluteName;
    }
*/
/* Deprecated since using FileChooserDialog
    private JFileChooser getLocalConfigFileChooser() {
      JFileChooser fchooser = null;
        if (currentDirectory!=null)
          fchooser = new JFileChooser(new File(currentDirectory));
        else{
          fchooser = new JFileChooser(com.gwsys.viewer3d.util.ViewerUtils.getDataDirectory());
        }
        fchooser.addChoosableFileFilter((FileFilter) getSessionFileFilter());
        fchooser.setDialogTitle("Open Configuration File");
        fchooser.setMultiSelectionEnabled(false);
        return fchooser;
    }
*/

/* Deprecated.
    private void setCurrentDirectory() {
        setBhpFileConfigPathProperty();
        if (currentDirectory == null || currentDirectory.length() == 0) {
            currentDirectory = bhpFileConfigPathProperty;
        }
    }
*/

/* Deprecated
    private void setBhpFileConfigPathProperty() {
        if (bhpFileConfigPathProperty == null) {
            bhpFileConfigPathProperty = _viewer.getPropertyManager().getProperty("bhpFilecfgPath");
            if (bhpFileConfigPathProperty.length()==0) {
                bhpFileConfigPathProperty = new File (".").getAbsolutePath();
            }
        }
    }
*/

    /**
     * @return
     */
    private boolean cancelOpenSessionByUser() {
        boolean cancelAction = false;
        if (_viewer.getDesktop().getAllFrames().length != 0) {
            // something is open now.
            int choice = JOptionPane.showConfirmDialog(_viewer, "Save the current session?", "Save", JOptionPane.YES_NO_CANCEL_OPTION);
            if (choice == JOptionPane.CANCEL_OPTION) {
                cancelAction = true;
            } else if (choice == JOptionPane.YES_OPTION) {
                if (saverAction != null) {
                    saverAction.actionPerformed(null);
                }
            }
            cancelAction = false;
        }
        return cancelAction;
    }

    /**
     * @return
     */
    private FileFilter getSessionFileFilter() {
        String[] cfgExtensions = new String[]{".cfg", ".CFG"};
        FileFilter cfgFilter = (FileFilter) new GenericFileFilter(cfgExtensions, "Configuration File (*.cfg)");
        return cfgFilter;
    }

    /*
     * openSessionFile replaces openRemote and openLocal
     * @param fileName is /path/file from chooser
    */
    private void openSessionFile(String fileName) {
      String msgID = "";
      ArrayList<String> params = new ArrayList<String>();
      params.add(_viewer.getAgent().getMessagingManager().getLocationPref());
      params.add(fileName);
      msgID = _viewer.getAgent().getMessagingManager().sendRequest(QIWConstants.CMD_MSG,
                                                                   QIWConstants.FILE_READ_CMD,
                                                                   QIWConstants.ARRAYLIST_TYPE,params);
      IQiWorkbenchMsg response = _viewer.getAgent().getMessagingManager().getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
      if (MsgUtils.isResponseAbnormal(response)) {
          ErrorDialog.showErrorDialog(_viewer, QIWConstants.ERROR_DIALOG, Thread.currentThread().getStackTrace(),
                                      ("IO error loading session file"),
                                      new String[] {"Read permission not set","File is empty"},
                                      new String[] {"Fix permission",
                                                    "If file already OK, contact workbench support"});
      }
      ArrayList<String> xml = (ArrayList<String>)MsgUtils.getMsgContent(response);
      if(Bhp2DviewerConstants.DEBUG_PRINT > 0) {
          for(int i=0; i<xml.size(); i++)
              System.out.println(xml.get(i));
      }
      StringBuffer xmlContent = new StringBuffer();
      for(int i=0; i<xml.size(); i++)
        xmlContent.append(xml.get(i));

      InputStream ins = new ByteArrayInputStream(xmlContent.toString().getBytes());
      try {
        Element root = BhpViewerHelper.getXMLTree(ins);
        root.setAttribute("title",fileName);
        _viewer.initWithXML(root);
      }
      catch(Exception e) {
          ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                              "Caught exception in OpenSessionAction");
      }
    }


/* Deprecated
    private void openRemote(String fname) {
        try {
            String urlString = _viewer.getPropertyManager().getProperty("servletReadFilecfgURL");
            URL url = new URL(urlString + "?FileName=" + URLEncoder.encode(fname, "iso-8859-1"));
            URLConnection conn = (URLConnection) url.openConnection();

            System.out.println("OpenSessionAction contentLength " + conn.getContentLength() + " : " + conn.getInputStream().available());

            String connXML = connectionXMLtoString(conn);
            InputStream xmlStream = new ByteArrayInputStream(connXML.getBytes());

            loadSessionFromXMLStream(fname.substring(fname.lastIndexOf("/")+1), xmlStream);
        }
        catch (Exception e) {
            System.out.println("OpenSessionAction.openRemote Exception " + e.toString());
            e.printStackTrace();
        }
    }
*/

    /**
     * @param fname
     * @param xmlStream
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
/* Deprecated
    private void loadSessionFromXMLStream(String titleText, InputStream xmlStream) throws ParserConfigurationException, SAXException, IOException {
        Element root = BhpViewerHelper.getXMLTree(xmlStream);

        root.setAttribute("title", titleText);
        _viewer.initWithXML(root);
    }
*/

/* Deprecated
    private String connectionXMLtoString(URLConnection conn) throws IOException {
        StringBuffer xmlContent = new StringBuffer();
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = reader.readLine();

        while(line != null) {
            xmlContent.append(line);
            line = reader.readLine();
        }
        reader.close();
        return (xmlContent.toString());
    }
*/

/* Deprecated
    private void openLocal(File file) throws ParserConfigurationException, SAXException, IOException {
        InputStream xmlInStream = new BufferedInputStream(new FileInputStream(file));
        loadSessionFromXMLStream(file.getName(),xmlInStream);
        xmlInStream.close();
    }
*/
}
