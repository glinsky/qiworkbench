/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.actions;

import java.awt.event.*;
import javax.swing.JOptionPane;

import com.bhpBilliton.viewer2d.*;
import com.bhpBilliton.viewer2d.util.ErrorDialog;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This action changes the data and captures the image
 *               automatically. <br>
 *               It will increase the chosen range of the synchronized
 *               headers once by its increment value automatically for
 *               several times until either the data boundary is met or
 *               the maximum step numbers set in
 *               <code>{@link BhpMovieManager}</code> is reached.
 *               Every time, after the image is updated, the image will
 *               be captured for movie making. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class StepUpAutoAction extends AbstractAction {
    private BhpViewerBase _viewer;
    private boolean _continue;

    public StepUpAutoAction(BhpViewerBase viewer) {
        super("Capture Movie", "Forward increment of the data automatically and capture image");
        _viewer = viewer;
        _continue = true;
    }

    public synchronized void loadingLayerNumberChanged(int number) {
        if (number==0) _continue = true;
        else _continue = false;
    }

    public void actionPerformed(ActionEvent e) {
        BhpWindow win = (BhpWindow) (_viewer.getDesktop().getSelectedFrame());
        if (win == null) {
            JOptionPane.showMessageDialog(_viewer,"Please select a layer first.");
            return;
        }
        BhpLayer layer = win.getFeaturedLayer();
        if (layer == null) {
            JOptionPane.showMessageDialog(_viewer,"Please select a layer first.");
            return;
        }

        Thread thread = new Thread(new StepUpAuto());
        thread.start();
    }

    private class StepUpAuto implements Runnable {
        public void run() {
            _continue = true;
            BhpWindow win = (BhpWindow) (_viewer.getDesktop().getSelectedFrame());
            //boolean oldMouseTracking = win.isMouseTracking();
            //if (oldMouseTracking) win.setMouseTracking(false);
            win.getBhpPlot().getMouseCenter().setMouseEventHandlingEnable(false);
            BhpLayer layer = win.getSelectedLayer();
            int maxStep = _viewer.getMovieManager().getMaxStep();
            // loop one more time for image capturing
            for (int i=0; i<maxStep+1; i++) {
                while(!_continue) {
                    try {
                        Thread.sleep(500);
                    }
                    catch (Exception ex) {
                        ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                            "StepUpAutoAction.actionPerformed exception");
                        break;
                    }
                }
                // capture the image
                _viewer.getMovieManager().createSnapshot();
                if (i == maxStep) break;
                // _continue true now, ready to go
                boolean changeHappened = layer.getParameter().stepChange(1);
                if (changeHappened) {
                    layer.generateLayer();
                    int eventFlag = BhpViewer.DATAINCREMENT_FLAG;
                    BhpLayerEvent event = new BhpLayerEvent(eventFlag, layer, new Integer(1));
                    if (layer.getEnableTalk()) _viewer.broadcastBhpLayerEvent(event);
                }
                else {
                    break;
                }
            }
            //if (oldMouseTracking) win.setMouseTracking(oldMouseTracking);
            win.getBhpPlot().getMouseCenter().setMouseEventHandlingEnable(true);
        }
    }
}
