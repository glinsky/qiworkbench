/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.actions;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.bhpBilliton.viewer2d.*;
import com.bhpBilliton.viewer2d.ui.OpenDataPanel;
import com.bhpBilliton.viewer2d.ui.OpenEventPanel;
import com.bhpBilliton.viewer2d.ui.OpenGeneralPanel;
import com.bhpBilliton.viewer2d.ui.OpenModelPanel;
import com.bhpBilliton.viewer2d.ui.OpenSeismicPanel;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This action will pop up a dialog with OpenDataPanel.
 *               A user can select the data and specify query parameters for
 *               bhpio. A new layer will be generated for the selected data,
 *               and the layer will be inserted into a new created BhpWindow
 *               of type map viewer.
 *               <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class OpenDataMapAction extends AbstractAction {
    private String _dataSourceName;
    private BhpViewerBase _viewer;
    private OpenDataDialog _dialog;
    private BhpLayer _layer;

    public OpenDataMapAction(BhpViewerBase viewer, String dsname) {
        super(dsname+" ...", "Open a " + dsname + " dataset in a new map Window");
        _viewer = viewer;
        _dataSourceName = dsname;
    }

    public void actionPerformed(ActionEvent e) {
        BhpWindow win = (BhpWindow) (_viewer.getDesktop().getSelectedFrame());
        _layer = null;
       	if (win!=null && win.getWindowType()==BhpViewer.WINDOW_TYPE_MAPVIEWER) {

            // only inherit from the same type
            _layer = win.getSelectedLayer();
        }
        _dialog = new OpenDataDialog(_viewer, _layer);
        _dialog.pack();
        _dialog.setSize(700, 400);
        _dialog.getOpenPanel().showChooser(_dataSourceName);
        //_dialog.setVisible(true);
    }

    private class OpenDataDialog extends JDialog {
        private OpenDataPanel cpanel;

        OpenDataDialog(BhpViewerBase v, BhpLayer l) {
          setTitle("Open Dataset in New Map Viewer");
          setModal(true);
          setLocation(_viewer.getMyLocation());
          //super(v, "Open Dataset in New Map Viewer", true);
          setupGUI(v, l);
        }

        public OpenDataPanel getOpenPanel() {
            return cpanel;
        }

        public boolean acceptTableChange() {
            return cpanel.acceptTableChange();
        }

        // change Frame to Container
        //private void setupGUI(Frame parent, BhpLayer layer) {
        private void setupGUI(Container parent, BhpLayer layer) {
            cpanel = new OpenDataPanel(parent, layer, true);
            JPanel tpanel = new JPanel(new FlowLayout());
            JButton ob = new JButton("Ok");
            JButton cb = new JButton("Cancel");
            ob.addActionListener(new OkButtonListener());
            cb.addActionListener(new CancelButtonListener());
            tpanel.add(ob);
            tpanel.add(cb);

            this.getContentPane().add(tpanel, BorderLayout.SOUTH);
            this.getContentPane().add(cpanel, BorderLayout.CENTER);
        }
    }

    private class OkButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            boolean valid = _dialog.acceptTableChange();
            if (valid == false) return;

            OpenDataPanel opanel = _dialog.getOpenPanel();
            OpenGeneralPanel ogpanel = opanel.getDataPanel();
            if (!opanel.isValidMapSetting()) {
                String msg = "Invalid map setting, first key must be single value.";
                _viewer.getStatusBar().showMessage(BhpViewer.ERROR_MESSAGE, msg);
                return;
            }
            BhpSeismicTableModel parameter = ogpanel.getTableModelForNewLayer();
            boolean continueLoad = parameter.checkMaxTraceNumber(_viewer);
            if (!continueLoad) return;

            _dialog.setVisible(false);

            BhpWindow winSample = (BhpWindow) (_viewer.getDesktop().getSelectedFrame());
            BhpWindow win ;
           	if (winSample == null || winSample.getWindowType() != BhpViewer.WINDOW_TYPE_MAPVIEWER) {
				win = new BhpWindow(_dialog.getOpenPanel().getDatasetName(),
						BhpViewer.WINDOW_TYPE_MAPVIEWER, AbstractPlot.DEFAULT_MAP_SCALE,
						AbstractPlot.DEFAULT_MAP_SCALE, _viewer.getShowPointer(),_viewer);//LTL
			} else {
				AbstractPlot plotSample = winSample.getBhpPlot();
				double hs = plotSample.getHorizontalScale();
				double vs = plotSample.getVerticalScale();
				win = new BhpWindow(_dialog.getOpenPanel().getDatasetName(),
							BhpViewer.WINDOW_TYPE_MAPVIEWER, hs, vs, _viewer.getShowPointer(),_viewer);//LTL


				AbstractPlot plotNew = win.getBhpPlot();
				plotNew.setVMajorStep(plotSample.getVMajorStep());
				plotNew.setVMinorStep(plotSample.getVMinorStep());
				plotNew.setHMajorStep(plotSample.getHMajorStep());
				plotNew.setHMinorStep(plotSample.getHMinorStep());
				plotNew.setLockAspectRatio(plotSample.getLockAspectRatio());
			}
            _viewer.getDesktop().add(win, JDesktopPane.DRAG_LAYER);
            win.pack();
            win.setSize(600, 560);
            win.setVisible(true);
            _viewer.repaint();

            win.setMouseTracking(true);

            int dataType = BhpViewer.BHP_DATA_TYPE_UNKNOWN;
            String propertyString = null;
            boolean propertySync = false;
            boolean layerSync = false;

            if (ogpanel instanceof OpenSeismicPanel) {
                dataType = BhpViewer.BHP_DATA_TYPE_SEISMIC;
            }
            else if (ogpanel instanceof OpenModelPanel) {
                dataType = BhpViewer.BHP_DATA_TYPE_MODEL;
                layerSync = ((OpenModelPanel) ogpanel).getLayerSyncValue();
                propertySync = ((OpenModelPanel) ogpanel).getPropertySyncValue();
                String pstring = ((OpenModelPanel) ogpanel).getSelectedPropertyName();
                String lstring = ((OpenModelPanel) ogpanel).getSelectedLayerName();
                if (pstring != null && lstring != null)
                    propertyString = pstring + ":" + lstring;
            }
            else if (ogpanel instanceof OpenEventPanel) {
                dataType = BhpViewer.BHP_DATA_TYPE_EVENT;
                propertySync = ((OpenEventPanel) ogpanel).getPropertySyncValue();
                propertyString = ((OpenEventPanel) ogpanel).getSelectedEventName();
            }

            win.createBhpMapLayer(_viewer, opanel.getDataSource(),
                                  opanel.getDatasetName(), opanel.getPathlist(),
                                  propertyString, propertySync, layerSync,
                                  parameter, _layer, dataType);

            _dialog.dispose();
        }
    }

    private class CancelButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            _dialog.setVisible(false);
            _dialog.dispose();
        }
    }
}
