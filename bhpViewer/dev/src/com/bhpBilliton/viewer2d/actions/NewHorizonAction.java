/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.actions;

import java.awt.event.*;

import javax.swing.JOptionPane;

import com.bhpBilliton.viewer2d.*;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.ui.CreateHorizonsDialog;
import com.bhpBilliton.viewer2d.util.ErrorDialog;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This action will create a new BhpEventLayer.
 *               Initially, all the data of the event is null.
 *               The user can use the picking tool to define the event.
 *               The resulting event can be saved back with "Save Event" option.
 *               <br><br>
 *
 * @author Synthia Kong
 * @author Gil Hansen
 * @version 2.0
 */

public class NewHorizonAction extends AbstractAction {
    private BhpViewerBase _viewer;
    private BhpLayer _layer;

    public NewHorizonAction(BhpViewerBase viewer) {
//        super("Create New Horizon", "Add a new event layer to the active window");
        super("Create New Horizon(s)", "Make a BHP-SU events dataset");
        _viewer = viewer;
    }

/* DEPRECATED: no longer create a horizon in a XSection window.
    public void actionPerformed(ActionEvent e) {
        BhpWindow win = (BhpWindow) (_viewer.getDesktop().getSelectedFrame());
        _layer = null;
        if (win == null) {
            JOptionPane.showMessageDialog(_viewer,"No destination window to create new horizon.");
            return;
        }
        if (win.getWindowType() != BhpViewer.WINDOW_TYPE_XSECTION) {
            JOptionPane.showMessageDialog(_viewer,"Incorrect window type to create new horizon.");
            return;
        }

        _layer = win.getFeaturedLayer();
        if (_layer==null || !(_layer instanceof BhpSeismicLayer)){
            JOptionPane.showMessageDialog(_viewer,"Please select a seismic layer in the window first.");
            return;
        }
        TemporaryEventDataSource eventSource = new TemporaryEventDataSource(_layer);
        BhpSeismicTableModel parameter = new BhpSeismicTableModel(_viewer, false);

        parameter.setProperties("UnnamedEvent");
        parameter.setDataAttributeString("Unnamed new HORIZON Data");
        parameter.setDataSource(eventSource);

        win.createXVLayer(_viewer, "TemporaryEventDataSource",
                "UnnamedData", "UnknownPathlist",
                "UnnamedEvent", false,
                parameter, _layer, BhpViewer.BHP_DATA_TYPE_EVENT,
                null);
    }
*/

    /**
     * Invoke a dialog to create a BHP-SU event dataset with one or more NULL events.
     * The NULL events may be added to an existing BHP-SU event dataset
     */
    public void actionPerformed(ActionEvent e) {
        CreateHorizonsDialog chDialog = new CreateHorizonsDialog(this._viewer.getAgent(), this._viewer, true);
        javax.swing.JDialog.setDefaultLookAndFeelDecorated(true);
        chDialog.setLocation(200, 100);
        chDialog.setSize(650, 620);
        chDialog.setDefaultCloseOperation(javax.swing.JDialog.DISPOSE_ON_CLOSE);
        chDialog.setVisible(true);
    }
}
