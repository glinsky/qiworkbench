/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.actions;

import com.bhpBilliton.viewer2d.AbstractPlot;
import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpSeismicTableModel;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.BhpWindow;
import com.bhpBilliton.viewer2d.ui.OpenDataPanel;
import com.bhpBilliton.viewer2d.ui.OpenDataDialog2;
import com.bhpBilliton.viewer2d.ui.OpenEventPanel;
import com.bhpBilliton.viewer2d.ui.OpenGeneralPanel;
import com.bhpBilliton.viewer2d.ui.OpenModelPanel;
import com.bhpBilliton.viewer2d.ui.OpenSeismicPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This action will pop up a dialog with OpenDataPanel.
 *               A user can select the data and specify query parameters for
 *               bhpio. A new layer will be generated for the selected data,
 *               and the layer will be inserted into a new created BhpWindow
 *               of type cross section viewer.
 *               <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class OpenDataAction extends AbstractAction {
    private String _dataSourceName;
    private BhpViewerBase _viewer;
    private BhpLayer _layer;
    private OpenDataDialog2 _dialog;

    public OpenDataAction(BhpViewerBase viewer, String dsname) {
        super(dsname+" ...", "Open a " + dsname + " dataset in a new X-section Window");
        _viewer = viewer;
        _dataSourceName = dsname;
    }

    public void actionPerformed(ActionEvent e) {
        showDialog();
    }

    public void showDialog() {
      BhpWindow win = (BhpWindow)(_viewer.getDesktop().getSelectedFrame());
      _layer = null;
   	  if (win!=null && win.getWindowType()==BhpViewer.WINDOW_TYPE_XSECTION) {
          // only inherit from the same type
          _layer = win.getSelectedLayer();
      }
      _dialog = new OpenDataDialog2(_viewer, _layer, new OkButtonListener(), new CancelButtonListener());
      _dialog.pack();
      _dialog.setSize(700, 400);
      _dialog.getOpenPanel().showChooser(_dataSourceName);
    }
    
    private class OkButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            boolean valid = _dialog.acceptTableChange();
            if (valid == false) return;

            OpenDataPanel opanel = _dialog.getOpenPanel();
            OpenGeneralPanel ogpanel = opanel.getDataPanel();
            BhpSeismicTableModel parameter = ogpanel.getTableModelForNewLayer();
            boolean continueLoad = true;
            if (!(ogpanel instanceof OpenEventPanel))
                continueLoad = parameter.checkMaxTraceNumber(_viewer);
            if (!continueLoad) return;

            _dialog.setVisible(false);

            BhpWindow winSample = (BhpWindow) (_viewer.getDesktop().getSelectedFrame());
            double hs = AbstractPlot.DEFAULT_XSEC_HSCALE;
            double vs = AbstractPlot.DEFAULT_XSEC_VSCALE;
            if (ogpanel instanceof OpenSeismicPanel) {
                vs = 0.5;
                double[] vsettings = parameter.getVerticalSettings();
                if (vsettings != null) {
                    double vstep = 1;
                    if (vsettings[2] > vstep) vstep = vsettings[2];
                    vs = vs / vstep;
                }
            }
            if (winSample!=null && winSample.getWindowType()==BhpViewer.WINDOW_TYPE_XSECTION) {
                hs = winSample.getBhpPlot().getHorizontalScale();
                vs = winSample.getBhpPlot().getVerticalScale();
            }
            BhpWindow win = new BhpWindow(_dialog.getOpenPanel().getDatasetName(),
            		BhpViewer.WINDOW_TYPE_XSECTION, hs, vs, _viewer.getShowPointer(),_viewer);//LTL
            _viewer.getDesktop().add(win, JDesktopPane.DRAG_LAYER);
            win.pack();
            win.setSize(600, 560);
            win.setVisible(true);
            _viewer.repaint();

            win.setMouseTracking(true);

            String propertyName = "";
            boolean propertySync = false;
            String[] selectedEvents = null;
            int dataType = BhpViewer.BHP_DATA_TYPE_SEISMIC;
            if (ogpanel instanceof OpenModelPanel) {
            	dataType = BhpViewer.BHP_DATA_TYPE_MODEL;
                propertySync = ((OpenModelPanel)ogpanel).getPropertySyncValue();
                propertyName = ((OpenModelPanel)ogpanel).getSelectedPropertyName();
            }
            else if (ogpanel instanceof OpenEventPanel) {
            	dataType = BhpViewer.BHP_DATA_TYPE_EVENT;
                propertySync = ((OpenEventPanel)ogpanel).getPropertySyncValue();
                selectedEvents = ((OpenEventPanel)ogpanel).getSelectedEventNames();
                propertyName = ((OpenEventPanel)ogpanel).getSelectedEventName();
            }
            if (dataType == BhpViewer.BHP_DATA_TYPE_EVENT && selectedEvents != null) {
                if (!(opanel.getDataSource().equals(com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_BHPSU))) {
                    // non bhpsu data, cannot open event layer as the first in the window
                    JOptionPane.showMessageDialog(_viewer,
                                         "Please open seismic data and select the seismic layer first.");
                    _dialog.dispose();
                    return;
                }
                for (int i=0; i<selectedEvents.length; i++) {
                    BhpSeismicTableModel theParameter = parameter;
                    if (i != 0)
                        theParameter = (BhpSeismicTableModel) parameter.clone();
                    win.createXVLayer(_viewer, opanel.getDataSource(),
                            opanel.getDatasetName(), opanel.getPathlist(),
                            selectedEvents[i], propertySync,
                            theParameter, _layer, dataType,
                            ((OpenEventPanel)ogpanel).getSampleEventShape());
                }
            }
            else {
                win.createXVLayer(_viewer, opanel.getDataSource(),
                    opanel.getDatasetName(), opanel.getPathlist(),
                    propertyName, propertySync, parameter,
                    _layer, dataType, null);
            }
            _dialog.dispose();
        }
    }

    private class CancelButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            _dialog.setVisible(false);
            _dialog.dispose();
        }
    }
}