/*
 bhpViewer - a 2D seismic viewer
 This program module Copyright (C) Interactive Network Technologies 2006

 The following is important information about the license which accompanies this copy of
 the bhpViewer in either source code or executable versions (hereinafter the "Software").
 The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
 is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
 http://www.gnu.org/licenses/gpl.txt.
 This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with this program;
 if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 For a license to use the Software under conditions other than in a way compliant with the
 GPL or the additional restrictions set forth in this license agreement or to purchase
 support for the Software, please contact: Interactive Network Technologies, Inc.,
 2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

 All licensees should note the following:

 *        In order to compile and/or modify the source code versions of the Software,
 a user may require one or more of INT's proprietary toolkits or libraries.
 Although all modifications or derivative works based on the source code are governed by the
 GPL, such toolkits are proprietary to INT, and a library license is required to make use of
 such toolkits when making modifications or derivatives of the Software.
 More information about obtaining such a license can be obtained by contacting sales@int.com.

 *        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
 serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
 submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
 This will allow the Custodian to consider integrating such revised versions into the
 version of the Software it distributes. Doing so will foster future innovation
 and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
 the Custodian is under no obligation to include modifications or revisions in future
 distributions.

 This program module may have been modified by BHP Billiton Petroleum,
 G&W Systems Consulting Corp or other third parties, and such portions are licensed
 under the GPL.
 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
 visit http://qiworkbench.org to learn more.
 */

package com.bhpBilliton.viewer2d.actions;

import java.awt.Color;
import java.awt.FlowLayout;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Title: BHP Viewer <br>
 * <br>
 * Description: This action will pop up a dialog showing the license.txt file
 * 
 * 
 * @author Mario Mena (G&W Systems)
 * 
 */
public class ViewLicenseAction extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	/**
	 * Constructor.
	 *
	 */
	public ViewLicenseAction() {
		setBackground(Color.white);
		JTextArea license = new JTextArea(20, 60);
		license.setLayout(new FlowLayout(FlowLayout.CENTER));
		license.setBorder(BorderFactory.createTitledBorder(" License "));
		license.setLineWrap(true);
		license.setWrapStyleWord(true);
		license.setEditable(false);
		readFileIntoTextArea(license);
		license.setCaretPosition(0);
		JScrollPane sp = new JScrollPane(license);
		add(sp);
	}

	private void readFileIntoTextArea(JTextArea license) {
		BufferedReader br = null;
		String str;
		try {
			InputStream io = this.getClass().getResourceAsStream("license.txt");
			br = new BufferedReader(new InputStreamReader(io)); 
			while ((str = br.readLine()) != null)
				license.append(str + "\n");
		} catch (FileNotFoundException fnfe) {
			license.append(fnfe.getMessage());
		} catch (IOException ioe) {
			license.append(ioe.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (Exception ignored) {
					license.append(ignored.getMessage());
				}
			}
		}
	}
}
