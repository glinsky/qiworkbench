/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.DefaultDesktopManager;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bhpBilliton.viewer2d.actions.AnnotationAction;
import com.bhpBilliton.viewer2d.actions.ContextSensitiveHelpAction;
import com.bhpBilliton.viewer2d.actions.DeleteLayerAction;
import com.bhpBilliton.viewer2d.actions.EnsembleUpAction;
import com.bhpBilliton.viewer2d.actions.EventGraphAction;
import com.bhpBilliton.viewer2d.actions.HideLayerAction;
import com.bhpBilliton.viewer2d.actions.LayerPropertyAction;
import com.bhpBilliton.viewer2d.actions.MovebackLayerAction;
import com.bhpBilliton.viewer2d.actions.MoveupLayerAction;
import com.bhpBilliton.viewer2d.actions.MovieSettingAction;
import com.bhpBilliton.viewer2d.actions.NewHorizonAction;
import com.bhpBilliton.viewer2d.actions.NewLayerAction;
import com.bhpBilliton.viewer2d.actions.OpenDataAction;
import com.bhpBilliton.viewer2d.actions.OpenDataMapAction;
import com.bhpBilliton.viewer2d.actions.OpenSessionAction;
import com.bhpBilliton.viewer2d.actions.PickingSettingAction;
import com.bhpBilliton.viewer2d.actions.PlotSettingAction;
import com.bhpBilliton.viewer2d.actions.PlotSynchronizationAction;
import com.bhpBilliton.viewer2d.actions.PreferenceAction;
import com.bhpBilliton.viewer2d.actions.PreferenceHLAction;
import com.bhpBilliton.viewer2d.actions.SaveEventAction;
import com.bhpBilliton.viewer2d.actions.SaveSessionAction;
import com.bhpBilliton.viewer2d.actions.SegyIndexingWizardAction;
import com.bhpBilliton.viewer2d.actions.ShowLayerAction;
import com.bhpBilliton.viewer2d.actions.SnapshotAction;
import com.bhpBilliton.viewer2d.actions.StatusBarAction;
import com.bhpBilliton.viewer2d.actions.StepDownAction;
import com.bhpBilliton.viewer2d.actions.StepUpAction;
import com.bhpBilliton.viewer2d.actions.StepUpAutoAction;
import com.bhpBilliton.viewer2d.actions.StopLoadAction;
import com.bhpBilliton.viewer2d.actions.ToolBarAction;
import com.bhpBilliton.viewer2d.actions.ViewAllAction;
import com.bhpBilliton.viewer2d.actions.WindowCascadeAction;
import com.bhpBilliton.viewer2d.actions.WindowTileAction;
import com.bhpBilliton.viewer2d.actions.ZoomInAction;
import com.bhpBilliton.viewer2d.actions.ZoomOutAction;
import com.bhpBilliton.viewer2d.actions.ZoomResetAction;
import com.bhpBilliton.viewer2d.actions.ZoomRubberAction;
import com.bhpBilliton.viewer2d.ui.BhpBusyDialog;
import com.bhpBilliton.viewer2d.ui.util.ElementAttributeReader;
import com.bhpBilliton.viewer2d.ui.util.FileTools;
import com.bhpBilliton.viewer2d.ui.util.TempFileManager;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpBilliton.viewer2d.util.IconResource;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ComponentStateUtils;
import com.bhpb.qiworkbench.compAPI.ComponentUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.compAPI.XmlUtils;

import com.gwsys.gw2d.model.Attribute2D;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.seismic.util.SeismicColorMap;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;

/**
 * BHP 2D Viewer GUI. This is the basic entry point of the program. This
 * frame may contain multiple <code>{@link BhpWindow}</code>, subclasses of
 * JInternalFrame.
 * <p>
 * The GUI is a separate component so it has its own message queue. If it
 * shares its parent message queue, the parent will get responses from its
 * requests. It is not necessary to start the GUI as a thread because by
 * definition a Swing GUI is a separate thread.
 *
 * @author Synthia Kong
 * @author Bob Miller
 * @author Gil Hansen
 * @version 1.2
 */

public class BhpViewerBase extends JInternalFrame implements BhpViewer {
    private static Logger logger = Logger.getLogger(BhpViewerBase.class.getName());

    /** CID for component instance. Generated before the thread is started and carried as the thread's name. */
    /** CID of parent, i.e., plugin
     * @deprecated Never used.
     */
    private String viewerCID = "";
    /** CID of the Workbench Manager
     * @deprecated Never used.
     */
    private String wbmCID = "";

    /** parent object, i.e., plugin */
    private Bhp2DviewerAgent agent = null;

    // messaging manager for GUI component

    /** Component descriptor of the Workbench Manager */
    private ComponentDescriptor wbmDesc = null;
    private DTListener dtListener;
    private DropTarget dropTarget;
    
    public static final int acceptableActions = DnDConstants.ACTION_COPY;
    private static String project = "";
    private static String fileSystem = "";

    public static String getProject() {
      return project;
    }

    protected void renameViewer(String name) {
        QiProjectDescriptor qpDesc = agent.getQiProjectDescriptor();
        project = QiProjectDescUtils.getQiProjectName(qpDesc);
        resetTitle(project);
    }

    /**
     * Initialize the viewer's GUI component:
     * <ul>
     * <li>Create its messaging manager</li>
     * <li>Create its CID</li>
     * <li>Register with the Message Dispatcher</li>
     * </ul>
     */
    public void init() {
        //this.dtListener = new DTListener(this);
        //this.dropTarget = new DropTarget(this, this.acceptableActions, this.dtListener, true);
    }

  private JMenuBar _menubar;

  private JToolBar _toolbar;

  private JDesktopPane _desktop;

  private StatusBar _statusbar;

  private JMenu newXSectionMenu;

  private JMenu newMapMenu;

  private JMenu _newLayerMenu;

  private JCheckBoxMenuItem _statusbarMI;

  private JCheckBoxMenuItem _toolbarMI;

  private JCheckBoxMenuItem _horizonGraphMI;

  private JMenuItem _saveEventMenu;

  private BhpPropertyManager _propertyManager;

  private BhpPickingManager _pickingManager;

  private BhpMovieManager _movieManager;

  private BhpBusyDialog _busyDialog;

  private RenderingAttribute _highlightAttribute;

  private RenderingAttribute _cursorAttribute;

  private double _cursorWidth;

  private double _cursorHeight;

  private boolean _showPointer;

  private StepUpAutoAction _stepAutoAction = null;

  public static HelpBroker broker = null;

  public static HelpSet helpSet = null;

  private static int lastLayerID=0;

  private List _listeners;
  private Properties props;

    /**
     * Constructor: create the 2D viewer's GUI.
     *
     * @param wbmCID CID of the Workbench Manager
     * @param agent Bhp2DviewerAgent instance
     */
    public BhpViewerBase(String wbmCID, Bhp2DviewerAgent agent) {
        super("bhp2Dviewer");
        FileTools.setFilesep(agent.getMessagingManager().getServerOSFileSeparator());
        // Make self a component: get own messaging mgr, create own CID, register self
        init();
        this.agent = agent;

        buildGUI();
    }

    /**
     * Constructor: create the 2D viewer's GUI.
     *
     * @param agent of the plugin adaptor
     */
    public BhpViewerBase(Bhp2DviewerAgent agent) {
        super("bhp2Dviewer");
        FileTools.setFilesep(agent.getMessagingManager().getServerOSFileSeparator());

        // Make self a component: get own messaging mgr, create own CID, register self
        init();
        this.agent = agent;
        buildGUI();
    }

    public Bhp2DviewerAgent getAgent() {
        return agent;
    }
    public Bhp2DviewerAgent getViewerAgent() {
        return agent;
    }

  /**
   * Constructs a new instance of the bhpViewer qiComponent. <br>
   * This method also initiates the <code>{@link BhpPropertyManager}</code>,
   * initiates the Java Help hooks, and sets up the GUI.
   *
   */
  private void buildGUI() {
    if (Bhp2DviewerConstants.DEBUG_PRINT > 0)
          System.out.println ("bhpViewer version: " + VersionInfo.versionString + " ["
                              + VersionInfo.dateString + "]");

    QiProjectDescriptor desc = agent.getQiProjectDescriptor();
    project = QiProjectDescUtils.getQiProjectName(desc);

    fileSystem = QiProjectDescUtils.getQiSpace(desc) + QiProjectDescUtils.getQiProjectReloc(desc);
    _listeners = new ArrayList();

    // Set JInternalFrame attributes
    this.setResizable(true);
    this.setIconifiable(true);
    this.setClosable(false);
    this.setMaximizable(true);

    setDefaultHighlight();
    setDefaultCursor();

    _pickingManager = new BhpPickingManager(this);
    _movieManager = new BhpMovieManager(this);
    // Removed all applet references
    // TODO setup JInternalFrame constants, including closable(false)
    _propertyManager = new BhpPropertyManager(this);
    // for JInternalFrame, set closable false
    //this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    // hard-code bhpiobinpath for now
    // set datapath and session path to project directory
    // TODO get system properties at statup
    _propertyManager.setProperty("bhpioBinPath","/hou/apps/gttapps/bhp_su/beta/bin/Linux");
    _propertyManager.setProperty("bhpDataPath",fileSystem + "/" + project);
    _propertyManager.setProperty("bhpFilecfgPath",fileSystem + "/" + project);

    System.setProperty("sun.awt.exception.handler", TheHandler.class.getName());
    attachHelp();
    applyLookAndFeel();
    if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
        System.out.println("Creating Menubar");
    buildMenubarToolbar();
    if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
        System.out.println("Creating Desktop");
    buildDesktop();
    if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
        System.out.println("Creating Statusbar");
    buildStatusbar();

    if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
        System.out.println("Configuring Layout");
    this.setJMenuBar(_menubar);
    this.getContentPane().add(_desktop, BorderLayout.CENTER);
    this.getContentPane().add(_toolbar, BorderLayout.NORTH);
    this.getContentPane().add((JPanel)_statusbar, BorderLayout.SOUTH);

    this.pack();
    this.setSize(800,500);
    resetTitle(project);
  }

  public static class TheHandler {
    public TheHandler() {
    }

    public void handle(Throwable throwable) {
      if (throwable instanceof OutOfMemoryError) {
        logger.severe("System Error: Out of Memory\n"+Thread.currentThread().getStackTrace());
        JOptionPane.showMessageDialog(null, "Out Of Memory: ", "AWT Error", JOptionPane.ERROR_MESSAGE);

        System.exit(1);
      } else {
        String excep = throwable.getClass().getName();
        if (excep.equals("java.lang.ArrayIndexOutOfBoundsException")) {
            //Ignore exception that occurs when closing down a bhpViewer
            //and its internal windows.
            logger.info("AWT Error: "+throwable.getMessage()+"\n"+Thread.currentThread().getStackTrace());
        } else {
            JOptionPane.showMessageDialog(null, throwable.getMessage() + " in " + throwable.getClass().getName(), "System Error", JOptionPane.ERROR_MESSAGE);
            logger.severe("AWT Error: "+throwable.getMessage()+"\n"+Thread.currentThread().getStackTrace());
        }
      }
    }
  }


  private void setDefaultCursor() {
    _cursorAttribute = new RenderingAttribute();
    _cursorAttribute.setLineColor(Color.red);
    _cursorAttribute.setLineWidth(1.0f);
    _cursorAttribute.setLineStyle(Attribute2D.LINE_STYLE_SOLID);
    _cursorWidth = 10000.0;
    _cursorHeight = 10000.0;
    _showPointer = true;
  }

  /**
   *
   */
  private void setDefaultHighlight() {
    _highlightAttribute = new RenderingAttribute();
    _highlightAttribute.setLineStyle(Attribute2D.LINE_STYLE_SOLID);
    _highlightAttribute.setLineWidth(6.0f);
    _highlightAttribute.setLineColor(new Color(255, 255, 0, 100));
  }

  /**
   *
   */
  private void attachHelp() {
      if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
          System.out.println("Connecting Help System");
    try {
      ClassLoader cl = BhpViewerBase.class.getClassLoader();
      URL helpSetURL = HelpSet.findHelpSet(cl, "help/INTHelp.hs");
      helpSet = new HelpSet(cl, helpSetURL);
      broker = helpSet.createHelpBroker();
    } catch (Exception e) {
        ErrorDialog.showInternalErrorDialog(this, Thread.currentThread().getStackTrace(),
                                            "Couldn't load help set: ");
    }
  }

  /**
   *
   */
  private void applyLookAndFeel() {
      if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
          System.out.println("Configuring lookAndFeel");
  }

  /**
   * Initiates the viewer with an XML node. <br>
   * This is used when a saved XML configuration file is opened to restore a
   * working session. Before creating new <code>{@link BhpWindow}</code>,
   * all the old ones will be closed and removed from the desktop.
   */
  public void initWithXML(Node node) {
    if (!node.getNodeName().equals("BhpViewer")) {
        ErrorDialog.showInternalErrorDialog(this, Thread.currentThread().getStackTrace(),
                                            "BhpViewer initWithXML error : Expected BhpViewer, found " +
                                                node.getNodeName());
      return;
    }

    // clear the current internal windows first
    closeAllInternalFrames();
    _desktop.removeAll();

    ElementAttributeReader emt = new ElementAttributeReader(node);
    this.setTitle(emt.getString("title"));
    int width = emt.getInt("width");
    int height = emt.getInt("height");
    int hlLineStyle = emt.getInt("highlightLS");
    float hlLineWidth = emt.getFloat("highlightLW");
    Color hlLineColor = emt.getColor("highlightLC");
    int csLineStyle = emt.getInt("cursorLS");
    Color csLineColor = emt.getColor("cursorLC");
    float csLineWidth = emt.getFloat("cursorLW");
    double csWidth = emt.getDouble("cursorW");
    double csHeight = emt.getDouble("cursorH");

    try {
      this.setSize(width, height);
      this.validate();
    } catch (Exception ex) {
        ErrorDialog.showInternalErrorDialog(this, Thread.currentThread().getStackTrace(),
                                            "BhpViewer initWithXML Exception ");
    }

    try {
      _highlightAttribute.setLineStyle(hlLineStyle);
      _highlightAttribute.setLineWidth(hlLineWidth);
      _highlightAttribute.setLineColor(hlLineColor);
      _cursorAttribute.setLineStyle(csLineStyle);
      _cursorAttribute.setLineWidth(csLineWidth);
      _cursorAttribute.setLineColor(csLineColor);

      setCursorSize(csWidth, csHeight);
    } catch (Exception ex) {
        ErrorDialog.showInternalErrorDialog(this, Thread.currentThread().getStackTrace(),
                                            "BhpViewer initWithXML Exception ");
    }

    NodeList children = node.getChildNodes();
    String nodeName;
    Node child;
    for (int i = 0; i < children.getLength(); i++) {
      child = children.item(i);
      if (child.getNodeType() == Node.ELEMENT_NODE) {
        nodeName = child.getNodeName();
        emt.setNode(child);
        if (nodeName.equals("MenuState")) {
          setToolbarVisibility(emt.getBoolean("ShowToolbar",true),emt.getBoolean("ShowStatusbar",true));
        } else if (nodeName.equals("BhpWindow")) {
          int wtype = BhpViewer.WINDOW_TYPE_XSECTION;
          double ploths = AbstractPlot.DEFAULT_XSEC_HSCALE;
          double plotvs = AbstractPlot.DEFAULT_XSEC_VSCALE;
          wtype = emt.numberAttribute("windowType",wtype);
          plotvs = emt.numberAttribute("plotVScale",plotvs);
          ploths = emt.numberAttribute("plotHScale",ploths);
          BhpWindow win = new BhpWindow(emt.getString("title"), wtype, ploths, plotvs, _showPointer,this);
          _desktop.add(win, javax.swing.JDesktopPane.DRAG_LAYER);
          win.pack();
          try {
            int x = emt.getInt("positionX");
            int y = emt.getInt("positionY");
            int w = emt.getInt("width");
            int h = emt.getInt("height");
            win.reshape(x, y, w, h);
          } catch (Exception ex) {
            win.setSize(600, 400);
          }
          win.setVisible(true);
          repaint();
          win.initWithXML(child);
        }
      }
    }
  }

  /**
   * Initiates the viewer with an XML node. <br>
   * This is used when a saved XML configuration file is opened to restore a
   * working session. Before creating new <code>{@link BhpWindow}</code>,
   * all the old ones will be closed and removed from the desktop.
   *
   * NOTE: The project descriptor is restored separately (by the bhpViewer agent)
   * before restoring the rest of the state, because the GUI builder needs it.
   */
  public void restoreState(Node node) {
    if (!node.getNodeName().equals(this.getClass().getName())) {
        ErrorDialog.showInternalErrorDialog(this, Thread.currentThread().getStackTrace(), "restoreState error: State not bhpViewer's but of " + node.getNodeName());
        return;
    }

    // clear the current internal windows first
    closeAllInternalFrames();
    _desktop.removeAll();

    ElementAttributeReader emt = new ElementAttributeReader(node);
    int xx = emt.getInt("x");
    int yy = emt.getInt("y");
    int width = emt.getInt("width");
    int height = emt.getInt("height");
    int hlLineStyle = emt.getInt("highlightLS");
    float hlLineWidth = emt.getFloat("highlightLW");
    Color hlLineColor = emt.getColor("highlightLC");
    int csLineStyle = emt.getInt("cursorLS");
    Color csLineColor = emt.getColor("cursorLC");
    float csLineWidth = emt.getFloat("cursorLW");
    double csWidth = emt.getDouble("cursorW");
    double csHeight = emt.getDouble("cursorH");

    try {
      this.setLocation(xx, yy);
      this.setSize(width, height);
      this.validate();
    } catch (Exception ex) {
        ErrorDialog.showInternalErrorDialog(this, Thread.currentThread().getStackTrace(),
                                            "bhpViewer initWithXML Exception");
    }

    try {
      _highlightAttribute.setLineStyle(hlLineStyle);
      _highlightAttribute.setLineWidth(hlLineWidth);
      _highlightAttribute.setLineColor(hlLineColor);
      _cursorAttribute.setLineStyle(csLineStyle);
      _cursorAttribute.setLineWidth(csLineWidth);
      _cursorAttribute.setLineColor(csLineColor);

      setCursorSize(csWidth, csHeight);
    } catch (Exception ex) {
        ErrorDialog.showInternalErrorDialog(this, Thread.currentThread().getStackTrace(),
                                            "BhpViewer initWithXML Exception");
    }

    NodeList children = node.getChildNodes();
    String nodeName;
    Node child;
    for (int i = 0; i < children.getLength(); i++) {
      child = children.item(i);
      if (child.getNodeType() == Node.ELEMENT_NODE) {
        nodeName = child.getNodeName();
        emt.setNode(child);
        if (nodeName.equals("MenuState")) {
          setToolbarVisibility(emt.getBoolean("ShowToolbar",true),emt.getBoolean("ShowStatusbar",true));
        } else if (nodeName.equals("BhpWindow")) {
          int wtype = BhpViewer.WINDOW_TYPE_XSECTION;
          double ploths = AbstractPlot.DEFAULT_XSEC_HSCALE;
          double plotvs = AbstractPlot.DEFAULT_XSEC_VSCALE;
          wtype = emt.numberAttribute("windowType",wtype);
          plotvs = emt.numberAttribute("plotVScale",plotvs);
          ploths = emt.numberAttribute("plotHScale",ploths);
          BhpWindow win = new BhpWindow(emt.getString("title"), wtype, ploths, plotvs, _showPointer,this);
          _desktop.add(win, JLayeredPane.DRAG_LAYER);
          //win.initWithXML(child);
          //win.pack();
          try {
            int x = emt.getInt("positionX");
            int y = emt.getInt("positionY");
            int w = emt.getInt("width");
            int h = emt.getInt("height");
            win.reshape(x, y, w, h);
          } catch (Exception ex) {
            win.setSize(600, 400);
          }
          win.setVisible(true);
          // add BhpViewerBase to Open Session
          //win.initWithXML(child);
          win.restoreState(child);
        }
      }
    }
  }

  private void setToolbarVisibility(boolean toolBarVisible, boolean statusBarVisible) {
    _toolbarMI.setState(toolBarVisible);
    _statusbarMI.setState(statusBarVisible);
    updateToolbar();
  }

  /**
   * Converts the viewer into an XML string. <br>
   * This is used when saving the working session into an XML file. Please refer
   * to BhpViewer.dtd for the detail of the format. This method in turn asks
   * all <code>{@link BhpWindow}</code> to streamlize themselves.
   *
   * @return an XML string for the object.
   */
  public String toXMLString() {
    StringBuffer content = new StringBuffer();
    content.append("<BhpViewer ");
    content.append("title=\"" + this.getTitle() + "\" ");
    content.append("highlightLS=\"" + _highlightAttribute.getLineStyle() + "\" ");
    content.append("highlightLW=\"" + _highlightAttribute.getLineWidth() + "\" ");
    content.append("highlightLC=\"" + BhpViewerHelper.toStringColor(_highlightAttribute.getLineColor()) + "\" ");
    content.append("cursorLS=\"" + _cursorAttribute.getLineStyle() + "\" ");
    content.append("cursorLW=\"" + _cursorAttribute.getLineWidth() + "\" ");
    content.append("cursorLC=\"" + BhpViewerHelper.toStringColor(_cursorAttribute.getLineColor()) + "\" ");
    content.append("cursorW=\"" + _cursorWidth + "\" ");
    content.append("cursorH=\"" + _cursorHeight + "\" ");
    content.append("version=\"" + VersionInfo.versionString + "\" ");
    content.append("width=\"" + this.getWidth() + "\" ");
    content.append("height=\"" + this.getHeight() + "\">\n");

    content.append("    <MenuState ");
    content.append("showToolbar=\"" + _toolbarMI.getState() + "\" ");
    content.append("showStatusbar=\"" + _statusbarMI.getState() + "\"/>\n");

    //save the project descriptor
    String pdXml = XmlUtils.objectToXml(agent.getQiProjectDescriptor());
    content.append(pdXml);

    JInternalFrame[] frames = _desktop.getAllFrames();
    BhpWindow w;
    for (int i = 0; i < frames.length; i++) {
      w = (BhpWindow) (frames[i]);
      content.append(w.toXMLString());
    }
    content.append("</BhpViewer>\n");
    return content.toString();
  }

  /**
   * Converts the viewer into an XML string. <br>
   * This is used when save the working session into an XML file. Please refer
   * to BhpViewer.dtd for the detail of the format. This method in turn asks
   * all <code>{@link BhpWindow}</code> to streamlize themselves.
   *
   * @return an XML string for the object.
   */
  public String saveState() {
    StringBuffer content = new StringBuffer();
    content.append("<" + this.getClass().getName() + " ");
    content.append("title=\"" + this.getTitle() + "\" ");
    content.append("highlightLS=\"" + _highlightAttribute.getLineStyle() + "\" ");
    content.append("highlightLW=\"" + _highlightAttribute.getLineWidth() + "\" ");
    content.append("highlightLC=\"" + BhpViewerHelper.toStringColor(_highlightAttribute.getLineColor()) + "\" ");
    content.append("cursorLS=\"" + _cursorAttribute.getLineStyle() + "\" ");
    content.append("cursorLW=\"" + _cursorAttribute.getLineWidth() + "\" ");
    content.append("cursorLC=\"" + BhpViewerHelper.toStringColor(_cursorAttribute.getLineColor()) + "\" ");
    content.append("cursorW=\"" + _cursorWidth + "\" ");
    content.append("cursorH=\"" + _cursorHeight + "\" ");
    content.append("version=\"" + VersionInfo.versionString + "\" ");
    content.append("x=\"" + this.getMyLocation().x + "\" ");
    content.append("y=\"" + this.getMyLocation().y + "\" ");
    content.append("width=\"" + this.getWidth() + "\" ");
    content.append("height=\"" + this.getHeight() + "\">\n");

    content.append("    <MenuState ");
    content.append("showToolbar=\"" + _toolbarMI.getState() + "\" ");
    content.append("showStatusbar=\"" + _statusbarMI.getState() + "\"/>\n");

    //save the project descriptor
    String pdXml = XmlUtils.objectToXml(agent.getQiProjectDescriptor());
    content.append(pdXml);

    JInternalFrame[] frames = _desktop.getAllFrames();
    BhpWindow w;
    for (int i = 0; i < frames.length; i++) {
      w = (BhpWindow)(frames[i]);
      content.append(w.toXMLString());
    }
    content.append("</" + this.getClass().getName() + ">\n");
    return content.toString();
  }

  /**
   * Broadcasts a <code>{@link BhpLayerEvent}</code>.<br>
   * This method will inform all the <code>{@link BhpWindow}</code> about
   * the event.
   *
   * @param e the event that will be broadcasted.
   */
  public void broadcastBhpLayerEvent(BhpLayerEvent e) {
    JInternalFrame[] frms = _desktop.getAllFrames();
    BhpWindow win;

    for (int i = 0; i < frms.length; i++) {
      win = (BhpWindow) frms[i];
      win.receiveBhpLayerEvent(e);
    }
  }

  /**
   * Stops the process of loading layers. <br>
   * This method will ask all the <code>{@link BhpWindow}</code> to stop
   * loading layers. As a result, the loading thread will be stopped, and
   * unfinished layers will not be displayed.
   */
  public void stopLoadingLayer() {
    JInternalFrame[] frames = _desktop.getAllFrames();
    BhpWindow win;
    for (int i = 0; i < frames.length; i++) {
      win = (BhpWindow) frames[i];
      win.stopLoadingLayer();
    }
    changeLoadingLayerNumber(BhpBusyDialog.CLEAR_LAYER);
  }

  /**
   * Changes the number of the layers that is been loading. When layer loading
   * status changed, the <code>{@link BhpBusyDialog}</code> needs to update
   * its information properly.
   *
   * @param l
   *            an integer indicates the event. It can be: <br>
   *            BhpBusyDialog.ADD_LAYER indicates one new loading process;
   *            <br>
   *            BhpBusyDialog.RMV_LAYER indicates one loading process finishs
   *            successfully; <br>
   *            BhpBusyDialog.RMV_LAYER_ERROR indicates one loading process
   *            fails; <br>
   *            BhpBusyDialog.CLEAR_LAYER clean up everything; <br>
   */
  public void changeLoadingLayerNumber(int l) {
    if(_busyDialog == null)
      _busyDialog = new BhpBusyDialog(this);
    _busyDialog.changeLoadingLayerNumber(l);
    _stepAutoAction.loadingLayerNumberChanged(_busyDialog.getLoadingLayerNumber());
  }

  public BhpBusyDialog getBusyDialog() {
    return _busyDialog;
  }

  /**
   * Adds the layer to the given window. <br>
   *
   * @param win
   *            the window for the new layer.
   * @param layer
   *            the layer that needs to be added.
   */
  public void addBhpLayer(BhpWindow win, BhpLayer layer) {
    win.addBhpLayer(layer);
    if (layer.getPipeline() != null && layer.getPipeline().getTraceRasterizer() != null) {
      SeismicColorMap cmap = layer.getPipeline().getTraceRasterizer().getColorMap();
      cmap.addColorMapListener(layer);
    }
    if (!layer.getHasLoaded()) {
      layer.setHasLoaded(true);
      layer.setBhpLayerVisible(layer.getLoadedVisibility());
    }
  }

  /**
   * Informs that loading a specific layer is not successful.
   *
   * @param win
   *            the window that the layer belongs to.
   * @param layer
   *            the layer that fails loading.
   */
  public void loadingLayerError(BhpWindow win, BhpLayer layer) {
    win.loadingLayerError(layer);
  }

  /**
   * Handles the OutOfMemory error. <br>
   * When it happens, the program tries to recover by closing the specific
   * window, or the currently selected window. Also, a dialog will be popped
   * to show an error message.
   *
   * @param win
   *            the window where the error originates.
   */
  public void processingOFM(BhpWindow win) {
    if (win != null) {
      win.dispose();
    } else {
      _desktop.getSelectedFrame().dispose();
    }
    ErrorDialog.showInternalErrorDialog(this, Thread.currentThread().getStackTrace(),
                                        "OutOfMemory");
  }

  /**
   * Retrieves the desk top pane of the frame.
   */
  public JDesktopPane getDesktop() {
    return _desktop;
  }

  /**
   * Retrieves the status bar the frame.
   */
  public StatusBar getStatusBar() {
    return _statusbar;
  }

  /**
   * Retrieves the picking manager of the viewer.
   */
  public BhpPickingManager getPickingManager() {
    return _pickingManager;
  }

  public int getNextLayerID() {
    JInternalFrame[] windows = getDesktop().getAllFrames();

    int nextID =0;
    for (int i=0;i<windows.length;i++) {
      int id =  ((BhpWindow)windows[i]).getNextLayerID();
      if (id > nextID) {
        nextID = id;
      }
    }

    nextID = getConfirmedUnique (nextID);

    return nextID;
  }

  private static synchronized int getConfirmedUnique (int nextID) {
    if (lastLayerID >= nextID) { // this number has been used already
      lastLayerID++;            // this one is unique
    } else {
      lastLayerID = nextID; // number is fine, update our tracking value
    }
    return lastLayerID;
  }

  /**
   * Retrieves the movie manager of the viewer.
   */
  public BhpMovieManager getMovieManager() {
    return _movieManager;
  }

  /**
   * Retrieves the property manager of the viewer.
   */
  public BhpPropertyManager getPropertyManager() {
    return _propertyManager;
  }

  public RenderingAttribute getHighlightAttribute() {
    return _highlightAttribute;
  }

  public RenderingAttribute getCursorAttribute() {
    return _cursorAttribute;
  }

  public double getCursorHeight() {
    return _cursorHeight;
  }

  public double getCursorWidth() {
    return _cursorWidth;
  }

  public boolean getShowPointer() {
    return _showPointer;
  }

  public void setShowPointer(boolean s) {
    _showPointer = s;
  }

  public void setCursorSize(double w, double h) {
    _cursorWidth = w;
    _cursorHeight = h;
  }

  /**
   * Updates the visibility of the tool bar. <br>
   * User can trun on and off the tool bar from menu.
   */
  public void updateToolbar() {
    if (_toolbarMI.isSelected()) {
      _toolbar.setVisible(true);
    } else {
      _toolbar.setVisible(false);
    }
  }

  /**
   * Updates the visibility of the status bar. <br>
   * User can trun on and off the status bar from menu.
   */
  public void updateStatusbar() {
    if (_statusbarMI.isSelected()) {
      _statusbar.setVisible(true);
    } else {
      _statusbar.setVisible(false);
    }
  }

  public void setSaveHorizonenabled(boolean s) {
    _saveEventMenu.setEnabled(s);
  }

  void updateNewLayerDataMenu() {
    if (_newLayerMenu == null) {
      return;
    }
    boolean enable = true;
    BhpWindow win = (BhpWindow) this.getDesktop().getSelectedFrame();
    if (win != null) {
      if (win.getWindowType() == BhpViewer.WINDOW_TYPE_MAPVIEWER) {
        enable = false;
      }
    }
    JMenuItem mi = null;
    String dataname = null;
    for (int i = 0; i < _newLayerMenu.getItemCount(); i++) {
      mi = _newLayerMenu.getItem(i);
      dataname = mi.getText().split(" ")[0].trim();
      String mapString = getPropertyManager().getProperty(dataname + "MapEnabled");
      if (mapString != null && mapString.equals("true")) {
        mi.setEnabled(true);
      } else {
        mi.setEnabled(enable);
      }
    }
  }

  protected void buildNewLayerDataMenu(JMenu newLayerMenu) {
    String dsProperty = getPropertyManager().getProperty("dataAdapter");
    String[] datanames = dsProperty.split(",");
    String adapterFlag;
    for (int i=0; i<datanames.length; i++) {
      String adapterName = datanames[i];
      adapterFlag = getPropertyManager().getProperty(adapterName + "Flag");
      if (isAdapterEnabled(adapterFlag, adapterName)) {
        newLayerMenu.add( new NewLayerAction(this, adapterName).getMenuItem());
      }
    }
  }

  protected void buildXSectionDataMenu(JMenu xsWindowMenu) {
    String dsProperty = getPropertyManager().getProperty("dataAdapter");
    String[] datanames = dsProperty.split(",");
    String adapterFlag;
    for (int i=0; i<datanames.length; i++) {
      String adapterName = datanames[i];
      adapterFlag = getPropertyManager().getProperty(adapterName + "Flag");
      if (isAdapterEnabled(adapterFlag, adapterName)) {
        // this block is redundant, the adapter is disabled in the dataAdapter.xml file - WHF 1/31/2008
        /*if (adapterName.equals("XML-Horizon")) {
          continue;
        }*/
        OpenDataAction openDataAction = new OpenDataAction(this, adapterName);
        JMenuItem menuItem = openDataAction.getMenuItem();
        xsWindowMenu.add (menuItem);
      }
    }
  }

  /**
   * @param adapterFlag
   * @param adapterName
   * @return
   */
  private boolean isAdapterEnabled(String adapterFlag, String adapterName) {
    boolean rval = false;

    if (adapterName != null && adapterName.length() >0) {
      if (adapterFlag != null && adapterFlag.length()>0) {
        if (adapterFlag.equalsIgnoreCase(adapterName)) {
          rval = true;
        } else if (adapterFlag.equalsIgnoreCase("yes")) {
          rval = true;
        } else if (!adapterFlag.equalsIgnoreCase("false") && !adapterFlag.equalsIgnoreCase("no")) {
          rval = true;
        }
      }
    }

    return rval;
  }

  protected void buildMapDataMenu(JMenu mapWindowMenu) {
    String dsProperty = getPropertyManager().getProperty("dataAdapter");
    String[] datanames = dsProperty.split(",");
    String adapterFlag;
    for (int i=0; i<datanames.length; i++) {
      String adapterName = datanames[i];
      adapterFlag = getPropertyManager().getProperty(adapterName + "Flag");
      if (isAdapterEnabled(adapterFlag, adapterName)) {
        String mapString = getPropertyManager().getProperty(adapterName+"MapEnabled");
        if (mapString!=null && mapString.equals("true")) {

          mapWindowMenu.add (new OpenDataMapAction(this, adapterName).getMenuItem());
        }
      }
    }
  }

  private void buildMenubarToolbar() {
    _menubar = getMainMenu();
    _toolbar = getToolBar();
  }

  private JToolBar getToolBar() {
    JToolBar toolbar = new JToolBar();
    toolbar.setBorder(new javax.swing.border.EtchedBorder());
    try {
      toolbar.add (new ViewAllAction(this,  IconResource.VIEWALL_ICON).getButton());
      toolbar.add (new ZoomRubberAction(this,   IconResource.ZOOMRUBBER_ICON).getButton());
      toolbar.add (new ZoomInAction(this,       IconResource.ZOOMIN_ICON).getButton());
      toolbar.add (new ZoomOutAction(this,  IconResource.ZOOMOUT_ICON).getButton());
      toolbar.addSeparator();
      toolbar.add (new StepUpAction(this,       IconResource.STEPPLUS_ICON).getButton());
      toolbar.add (new StepDownAction(this, IconResource.STEPMINUS_ICON).getButton());
      toolbar.addSeparator();
      toolbar.add (new ShowLayerAction(this,    IconResource.SHOWLAYER_ICON).getButton());
      toolbar.add (new HideLayerAction(this,    IconResource.HIDELAYER_ICON).getButton());
      toolbar.addSeparator();
      toolbar.add (new PickingSettingAction(this,IconResource.PICKMODEPBP_ICON).getButton());
      toolbar.add (new SnapshotAction(this, IconResource.SNAPSHOT_ICON).getButton());
      toolbar.addSeparator();
      toolbar.add (new ContextSensitiveHelpAction(this,IconResource.CSH_ICON).getButton());

    } catch (Exception ex) {
        ErrorDialog.showInternalErrorDialog(this, Thread.currentThread().getStackTrace(),
                                            "BhpViewer.buildMenubarToolbar Exception: Failed to load icons.");
    }
    return toolbar;
  }

  private JMenuBar getMainMenu() {
    JMenuBar mb = new JMenuBar();
    mb.add(getFileMenu());
    mb.add(getWindowMenu());
    mb.add(getLayerMenu());
    mb.add(getToolsMenu());
    mb.add(getPrefsMenu());
    mb.add(getHelpMenu());
    return mb;
  }

  private JMenu getHelpMenu() {
    JMenu helpMenu = new JMenu("Help");

    //helpMenu.add (new HelpAboutAction(this).getMenuItem());
    JMenuItem aboutHelp = new JMenuItem("About ...");
    final Component comp = this;
    aboutHelp.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
            if(props == null){
                String compName = agent.getMessagingManager().getRegisteredComponentDisplayNameByDescriptor(agent.getComponentDescriptor());
                props = ComponentUtils.getComponentVersionProperies(compName);
            }
            new HelpAboutDialog(comp,props);
        }
    });
    helpMenu.add(aboutHelp);
    javax.swing.AbstractAction ojhAct = new javax.swing.AbstractAction("Open Help ...") {
      public void actionPerformed(ActionEvent ae) {
      showHelp();
      }
    };
    helpMenu.add(ojhAct);
    return (helpMenu);
  }

  private JMenu getPrefsMenu() {
    JMenu prefsMenu = new JMenu("Preferences");

    _toolbarMI = new ToolBarAction(this).getCheckBoxMenuItem();
    prefsMenu.add (_toolbarMI);
    _statusbarMI = new StatusBarAction(this).getCheckBoxMenuItem();
    prefsMenu.add (_statusbarMI);
    prefsMenu.addSeparator();
    prefsMenu.add (new PickingSettingAction(this,"").getMenuItem());
    prefsMenu.add (new PreferenceAction(this).getMenuItem());
    prefsMenu.add (new PreferenceHLAction(this).getMenuItem());
    return prefsMenu;
  }

  private JMenu getToolsMenu() {
    JMenu toolsMenu = new JMenu("Tools");

    //Tools
    _stepAutoAction = new StepUpAutoAction(this);

    toolsMenu.add(new MovieSettingAction(this, _stepAutoAction).getMenuItem());
    toolsMenu.add(new SnapshotAction(this,"").getMenuItem());
    toolsMenu.addSeparator();
    toolsMenu.add(new SegyIndexingWizardAction(this).getMenuItem());
    return toolsMenu;
  }

  private JMenu getLayerMenu() {
    JMenu layerMenu = new JMenu("Layer");

    _newLayerMenu = new JMenu("Add Layer");
    _newLayerMenu.addMenuListener(new MenuListener() {
      public void menuCanceled(MenuEvent e) {
      }

      public void menuDeselected(MenuEvent e) {
      }

      public void menuSelected(MenuEvent e) {
        updateNewLayerDataMenu();
      }
    });
    buildNewLayerDataMenu(_newLayerMenu);
    layerMenu.add(new LayerPropertyAction(this).getMenuItem());
    layerMenu.add(_newLayerMenu);
    layerMenu.add(new NewHorizonAction(this).getMenuItem());
    _saveEventMenu = new SaveEventAction(this).getMenuItem();
    _saveEventMenu.setEnabled(false);
    layerMenu.add(_saveEventMenu);
    layerMenu.addSeparator();
    layerMenu.add(new StepUpAction(this,"").getMenuItem());
    layerMenu.add(new StepDownAction(this,"").getMenuItem());
    layerMenu.add(new EnsembleUpAction(this).getMenuItem());
    layerMenu.addSeparator();
    layerMenu.add(new HideLayerAction(this,"").getMenuItem());
    layerMenu.add(new ShowLayerAction(this, "").getMenuItem());
    layerMenu.add(new DeleteLayerAction(this).getMenuItem());
    layerMenu.add(new MoveupLayerAction(this).getMenuItem());
    layerMenu.add(new MovebackLayerAction(this).getMenuItem());
    return (layerMenu);
  }

  private JMenu getWindowMenu() {
    JMenu windowMenu = new JMenu("Window");
    windowMenu.add(new PlotSettingAction(this).getMenuItem());
    windowMenu.add(new AnnotationAction(this).getMenuItem());
    windowMenu.add(new PlotSynchronizationAction(this).getMenuItem());
    _horizonGraphMI = new EventGraphAction(this).getCheckBoxMenuItem(false);
    windowMenu.add(_horizonGraphMI);
    windowMenu.addSeparator();
    windowMenu.add(new ZoomInAction(this,"").getMenuItem());
    windowMenu.add(new ZoomOutAction(this,"").getMenuItem());
    windowMenu.add(new ZoomResetAction(this).getMenuItem());
    windowMenu.add(new ZoomRubberAction(this,"").getMenuItem());
    windowMenu.add(new ViewAllAction(this,"").getMenuItem());
    windowMenu.addSeparator();
    windowMenu.add(new WindowCascadeAction(this).getMenuItem());
    windowMenu.add(new WindowTileAction(this).getMenuItem());
    return windowMenu;
  }

  private JMenu getFileMenu() {
    JMenu fileMenu = new JMenu("File");
    newXSectionMenu = new JMenu("New X-Section Window");
    newMapMenu = new JMenu("New Map Window");

    buildXSectionDataMenu(newXSectionMenu);
    buildMapDataMenu(newMapMenu);
    fileMenu.add(newXSectionMenu);
    if (newMapMenu.getMenuComponentCount() != 0) {
      fileMenu.add(newMapMenu);
    }

    fileMenu.add(new StopLoadAction(this).getMenuItem());

    fileMenu.addSeparator();
    SaveSessionAction ssAct = new SaveSessionAction(this);

    JMenu importMenu = new JMenu("Import ...");
    JMenuItem qiWorkbenchFormatMenuItem = new JMenuItem("qiWorkbench Format");
    qiWorkbenchFormatMenuItem.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            importMenuItemActionPerformed(evt);
        }
    });
    importMenu.add(qiWorkbenchFormatMenuItem);

    importMenu.add(new OpenSessionAction(this, ssAct).getMenuItem());
    fileMenu.add(importMenu);

    JMenuItem exportMenuItem = new JMenuItem("Export ...");
    exportMenuItem.setMnemonic('E');
    exportMenuItem.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            exportMenuItemActionPerformed(evt);
        }
    });
    fileMenu.add(exportMenuItem);

    fileMenu.addSeparator();

    JMenuItem quitMenuItem = new JMenuItem("Quit");
    quitMenuItem.setMnemonic('Q');
    quitMenuItem.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            quitMenuItemActionPerformed(evt);
        }
    });
    fileMenu.add(quitMenuItem);

    JMenuItem saveMenuItem = new JMenuItem();
    saveMenuItem.setMnemonic('S');
    saveMenuItem.setText("Save");
    saveMenuItem.setToolTipText("Select to Save state");
    saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            saveMenuItemActionPerformed(evt);
        }
    });

    fileMenu.add(saveMenuItem);

    JMenuItem saveAsMenuItem = new JMenuItem();
    saveAsMenuItem.setMnemonic('A');
    saveAsMenuItem.setText("Save As");
    saveAsMenuItem.setToolTipText("Select to Save state as a clone");
    saveAsMenuItem.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            saveAsMenuItemActionPerformed(evt);
        }
    });

    fileMenu.add(saveAsMenuItem);
    
    
    JMenuItem saveQuitMenuItem = new JMenuItem("Save, Quit");
    saveQuitMenuItem.setMnemonic('a');
    saveQuitMenuItem.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            saveQuitMenuItemActionPerformed(evt);
        }
    });
    fileMenu.add(saveQuitMenuItem);

    return fileMenu;
  }

  private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuItemActionPerformed
      agent.saveState();
  }//GEN-LAST:event_saveMenuItemActionPerformed

  private void saveAsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {                                               
      agent.saveStateAsClone();
  }                                                
  
  private void saveQuitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveQuitMenuItemActionPerformed
      agent.saveStateThenQuit();
  }//GEN-LAST:event_saveQuitMenuItemActionPerformed
  private void buildDesktop() {
    _desktop = new JDesktopPane();
    _desktop.setDesktopManager(new MyDesktopManager());
  }

  private void buildStatusbar() {
    _statusbar = new BhpStatusBar();
  }

  void adjustSeletionSensetiveMenuState() {
    BhpWindow win = (BhpWindow) _desktop.getSelectedFrame();
    if (win.getWindowType() == BhpViewer.WINDOW_TYPE_MAPVIEWER) {
      _horizonGraphMI.setSelected(false);
      _horizonGraphMI.setEnabled(false);
    } else {
      _horizonGraphMI.setEnabled(true);
      _horizonGraphMI.setSelected(win.isShowEventGraphPlot());
    }
  }

  /**
   * Closes all the internal frames. This is called at exit.
   */
  public void closeAllInternalFrames() {
    if (_desktop == null) {
      return;
    }
    JInternalFrame[] frms = _desktop.getAllFrames();
    for (int i = 0; i < frms.length; i++) {
      try {
        frms[i].setClosed(true);
        frms[i].dispose();
      } catch (Exception ex) {
          ErrorDialog.showInternalErrorDialog(this, Thread.currentThread().getStackTrace(),
                                              "BhpViewer failed closing a window");
      }
    }

    //clean up old files
    TempFileManager.getInstance().cleanTempFiles();
    lastLayerID = 0;
  }

  void showHelp() {
    try {
      broker.setDisplayed(true);
    } catch (javax.help.UnsupportedOperationException uoe) {
        ErrorDialog.showInternalErrorDialog(this, Thread.currentThread().getStackTrace(),
                                            "Viewer cannot open help");
    }
  }

  private class MyWindowListener extends WindowAdapter {
    public void windowClosing(WindowEvent e) {
      closeAllInternalFrames();
    }
  }

  private class MyDesktopManager extends DefaultDesktopManager {
    public void activateFrame(JInternalFrame f) {
      super.activateFrame(f);
      adjustSeletionSensetiveMenuState();
    }
  }

  public void addBhpViewerListener(BhpViewerListener l) {
    if (l==null || _listeners.contains(l)) {
      return;
    }
    _listeners.add(l);
  }

  public void removeBhpViewerListener(BhpViewerListener l) {
    if (l==null || !_listeners.contains(l)) {
      return;
    }
    _listeners.remove(l);
  }


  /**
   * Broadcasts a <code>{@link BhpLayerEvent}</code>. <br>
   * This method will inform all the <code>{@link BhpWindow}</code>
   * about the event.
   * @param e the event that will be broadcasted.
   */
  public void broadcastViewerEvent(BhpLayerEvent e) {
    if (_listeners==null || _listeners.size()==0) {
      return;
    }
    for (int i=0; i<_listeners.size(); i++) {
      ((BhpViewerListener)_listeners.get(i)).viewerChanged(e);
    }
  }

  /*
   * getMyLocation returns BhpViewerBase screen location to caller so it can set it's location relative to BhpViewerBase
   * @return Point  x,y of upper left corner
   */
    public Point getMyLocation() {
      return getLocation();
    }

  // Close the 2D viewer within the qiWorkbench
  public void closeViewerGUI() {
    closeAllInternalFrames();
    setVisible(false);
    dispose();
  }

    /**
     * Reset the GUI's title
     *
     * @param name Name of the associated project.
     */
    public void resetTitle(String projName) {
        String compName = "";
        IComponentDescriptor agentDesc = agent.getComponentDescriptor();
        String pdn = CompDescUtils.getDescPreferredDisplayName(agentDesc);
        String screenName = CompDescUtils.getDescDisplayName(agentDesc);
        if (pdn.trim().length() > 0)
            compName = pdn.trim();
        else
            compName = screenName;

        //get the type of qiComponent
        int idx = screenName.indexOf("#");
        if (idx != -1) screenName = screenName.substring(0, idx);
        this.setTitle(screenName+": " + compName + "  Project: " + projName);
    }

    private void importMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        //Use a file chooser to select the state file
        QiProjectDescriptor projDesc = agent.getQiProjectDescriptor();
        ComponentStateUtils.callFileChooser(this, QiProjectDescUtils.getXmlPath(projDesc), QIWConstants.IMPORT_COMP_STATE, agent.getComponentDescriptor(), agent.getMessagingManager());

        //NOTE: agent will invoke a file chooser service. When it processes the GET_RESULT_FROM_FILE_CHOOSER_CMD,
        //     it will read the state of the PM in XML and restore it using its importState() method.
    }

    private void exportMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        //Use a file chooser to specify the path of the file where the qiComponent's state will be saved.
        QiProjectDescriptor projDesc = agent.getQiProjectDescriptor();
        ComponentStateUtils.callFileChooser(this, QiProjectDescUtils.getXmlPath(projDesc), QIWConstants.EXPORT_COMP_STATE, agent.getComponentDescriptor(), agent.getMessagingManager());

        //NOTE:  agent will invoke a file chooser service. When it processes the GET_RESULT_FROM_FILE_CHOOSER_CMD,
        //     it will capture the state of the PM in XML by invoking its genState() method. Then it will
        //     invoke an IO service to write the state to a .xml file.
    }

    private void quitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.deactivateSelf();
    }
}