/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.indexing.SegyHeaderFormat;
import com.gwsys.seismic.core.SeismicWorkflow;
import com.gwsys.seismic.io.TraceHeader;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class handles the mouse events for a
 *               <code>{@link AbstractPlot}</code>. <br>
 *               Since mouse event needs to be handled differently according
 *               to the type of the current active layer, this class
 *               implements ListSelectionListener and listens to
 *               <code>{@link BhpLayerList}</code>. <br>
 *               When mouse is double clicked, a trace will be selected from
 *               the active layer and a new
 *               <code>{@link BhpLayerEvent}</code> of type DATASELECTION_FLAG
 *               will be generated and broadcasted. <br>
 *               When rubber band zooming is activated, the mouse event will
 *               be forwarded to <code>{@link RubberView}</code>. <br>
 *               When the active layer is a
 *               <code>{@link BhpEventLayer}</code>, all the mouse events
 *               will be forwarded to
 *               <code>{@link BhpSelectionController}</code>. User can edit the
 *               event with mouse.<br>
 *               When the active layer is a
 *               <code>{@link BhpMapLayer}</code>, all the mouse events
 *               will be forwarded to <code>{@link BhpMapMouseHandler}</code>.
 *               User can create, edit, and delete polylines.
 *               This class is also responsible for mouse tracking, to track
 *               the mouse move events. <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpMouseCenter implements MouseListener, MouseMotionListener,
                                        ListSelectionListener {
    private AbstractPlot _plot;
    private BhpLayer _selectedLayer;
    private BhpSelectionController _selector;
    private BhpMapMouseHandler _mapHandler;
    private boolean _zoomRubber;
    private boolean _inZooming;

    private BhpMouseTrackListener _mtListener;

    private boolean _enableEventHandling = true;

    private BhpViewerBase bhpViewerBase = null;

    /**
     * Creates a new instance.
     * @param plot the associate plot which is the source of the events.
     */
    public BhpMouseCenter(AbstractPlot plot) {
        _plot = plot;
        _selectedLayer = null;
        _selector = null;
        _mapHandler = null;
        _zoomRubber = false;
        _inZooming = false;
        _mtListener = null;
        // get BhpViewerbase for later use

    }

    public void setMouseEventHandlingEnable(boolean b) {
        _enableEventHandling = b;
    }

    public void updateEventHighlight() {
        if (_selector == null) return;
        _selector.refreshControlLayer();
    }

    /**
     * Sets the mouse track listener. <br>
     * The listener will be notified for the mouse move event.
     */
    public void setMouseTrackListener(BhpMouseTrackListener l) {
        _mtListener = l;
    }

    /**
     * Sets to start rubber band zooming. <br>
     * When this method is called, rubber band zooming
     * is enabled, and the mouse events will be forwarded
     * to the <code>{@link RubberView}</code>.
     * After one rubber band zooming is completed,
     * the flag will be set back. The method needs
     * to be called again for another rubber band zooming.
     */
    public void setZoomRubberFlag() {
        _zoomRubber = true;
        _inZooming = false;
    }

    /**
     * Notify the mouse track listener. <br>
     * There are two cases that the method will be called: <br>
     * First, when handling a mouse move event. In this case,
     * the method is called in a active mode, with false as
     * the third parameter, and a new BhpLayerEvent with flag
     * CURSOR_FLAG will be created and broadcasted.
     * The method is also called when the plot synchronizing with other
     * plots for the mouse position. In this case, it is in passive mode,
     * with true as the third parameter.
     * @param virtTraceId the horizonal position of the point.
     * @param vritualY the vertical position of the point.
     * @param passive a boolean flag indicates the mode of this notification.
     */
    public void notifyMouseTracking(int virtTraceId, int virtualY, boolean passive) {
        BhpLayer theLayer = _plot.getBhpLayer(_plot.getAxisAssociateLayer());
        String vname = "";
        Hashtable selectParameter = null;
        if (theLayer != null && virtTraceId >= 0) {
            BhpSeismicTableModel layerParameter = theLayer.getParameter();
            vname = layerParameter.getVerticalName();
            try {
                selectParameter = _plot.buildSelectedAnnotationParameter(virtTraceId, virtualY);
            } catch (Exception ex) {
                ErrorDialog.showInternalErrorDialog(bhpViewerBase, Thread.currentThread().getStackTrace(),
                                                    "BhpMouseCenter.notifyMouseTracking exception :" +
                                                    ex + "    virtTraceId=" + virtTraceId +
                                                    " virtualY=" + virtualY);
                return;
            }
            if (_mtListener != null) {
                _mtListener.handleMouseTracking(vname, selectParameter);
            }
            if (!passive && _plot.getEnablePlotTalk()) {
                BhpLayerEvent event = new BhpLayerEvent(BhpViewer.CURSOR_FLAG, theLayer, selectParameter);

                bhpViewerBase = event.getSource().getBhpViewerBase();
                bhpViewerBase.broadcastBhpLayerEvent(event);
            }
        }

    }

    /**
     * Method of interface ListSelectionListener. <br>
     * This method is called when a selection is made in <code>{@link BhpLayerList}</code>. According to the type of layer that
     * is selected, it will create appropriate object to handle the mouse events.
     */
    public void valueChanged(ListSelectionEvent e) {
        if (_selector != null) {

            _selector.removeManipulationObjects();
            if (_selectedLayer instanceof BhpEventLayer) {
                ((BhpEventLayer)_selectedLayer).removeEventLayerListener(_selector);
            }
            _selector = null;
        }
        if (_mapHandler != null) {
            _mapHandler.cleanupMapMouseHandler();
            _mapHandler = null;
        }


        if (e.getSource() instanceof BhpLayerList) { // it gets to be
            BhpLayerList blist = (BhpLayerList) e.getSource();
            int layerid = blist.getSelectedLayerId();
            _selectedLayer = _plot.getBhpLayer(layerid);
            RubberView rbview = _plot.getMainView().getRubberView();
            if (_selectedLayer!=null){
                bhpViewerBase = _selectedLayer.getBhpViewerBase();
            }
            if(bhpViewerBase != null)
                bhpViewerBase.setSaveHorizonenabled(false);
            if ((_selectedLayer instanceof BhpEventLayer)) {
        bhpViewerBase.setSaveHorizonenabled(true);
        _selector = new BhpSelectionController(_selectedLayer
                .getContainerModel(), (BhpEventLayer) _selectedLayer,
                _selectedLayer.getShapeListLayer(),
                bhpViewerBase.getHighlightAttribute(),
                bhpViewerBase.getPickingManager(), _plot);
        ((BhpEventLayer) _selectedLayer).addEventLayerListener(_selector);
        } else if (_selectedLayer instanceof BhpMapLayer) {
        _mapHandler = new BhpMapMouseHandler((BhpMapLayer)_selectedLayer, rbview);
        }
        }
    }

    /**
     * Actions for the mouse entering events. <br>
     * No action is implemented now.
     */
    public void mouseEntered(MouseEvent me) {
    }
    /**
     * Actions for the mouse exiting events. <br>
     * No action is implemented now.
     */
    public void mouseExited (MouseEvent me) {
        if (!_enableEventHandling) return;
        RubberView rbview = _plot.getMainView().getRubberView();
        if (rbview != null) rbview.hideSymbol();
        if ((_plot.getSynFlag() & BhpViewer.CURSOR_FLAG) != 0)
            notifyMouseTracking(-1, -1, false);
    }

    /**
     * Actions for the mouse moving events. <br>
     * Mouse moving events are meaningful in two scenarios:
     * when mouse tracking is enabled, and
     * when a map layer is selected.
     */
    public void mouseMoved(MouseEvent me) {
        if (!_enableEventHandling) return;
        if (_mapHandler != null) {
            _mapHandler.processMouseEvent(me);
        }

        RubberView rbview = _plot.getMainView().getRubberView();
        if (rbview == null) return;

        Point pos = new Point(me.getX(), me.getY());
        rbview.setSymbolAnchor(pos.x, pos.y);
        if ((_plot.getSynFlag() & BhpViewer.CURSOR_FLAG) == 0) return;

        BhpLayer theLayer = _plot.getBhpLayer(_plot.getAxisAssociateLayer());
        int virtTraceId = -999;
        int virtualY = 0;
        if (theLayer != null) {
            Point selectP = theLayer.selectByPoint(pos);
            if (selectP != null) {
                virtTraceId = selectP.x;
                virtualY = selectP.y;
            }
        }

        notifyMouseTracking(virtTraceId, virtualY, false);
    }

    /**
     * Actions for the mouse clicking events. <br>
     * Only right mouse double(multiple) clicking evnets are
     * processed here for data synchronization. To be more specific,
     * selection is made based on the mouse position, in
     * terms of trace id and vertical reading. Then a
     * <code>{@link BhpLayerEvent}</code> is created with
     * flag BhpLayer.DATASELECTION_FLAG and broadcasted.
     * However, this mouse click data synchronization is
     * for seismic layer, model layer, and map layer, not
     * for event layer.
     */
    public void mouseClicked(MouseEvent me) {
        if (!_enableEventHandling) return;
        /* must have double left click to process per M Glinsky 07/11/09*/
        if(me.getClickCount() < 2) {
            return;
        }
        // map layer is handled by _mapHandler
        if (_mapHandler != null) {
            // this is now also double-left-click per M Glinsky
            // see Jira Issue BHPVIEWER-4 for more info
            //if (SwingUtilities.isRightMouseButton(me))
            _mapHandler.processMouseDoubleClick(me);
            return;
        }
        // event layer does not respond
        if (_selector != null) return;
        if (SwingUtilities.isLeftMouseButton(me) == false) return;


        // select on the selected layer only, model or seismic. Always pick a trace
        int virtTraceId = -999;
        int virtualY = 0;
        if (_selectedLayer != null) {
            Point viewPos = _selectedLayer.getViewPosition();
            Point mousePos = me.getPoint();
            //Adding the viewPos.x and .y to the mouse position caused the wrong virtTraceId to be calculated when
            //the view was scrolled or zoomed.  Specifically, if the viewport's origin was at (X,Y), then the virtualTraceId calculated
            //would be that of the trace at horizontal position (X + mousePos.x) if the layer were shown in its entirety without scrolling, 
            //zooming, or changing the horizontal scale with 'zoom all'.
            //This was most obvious in that the synchronized window(s) would show the wrong trace.
            //Removed by Woody Folsom 07/11/13.
            Point selectedP = _selectedLayer.selectByPoint(new Point(mousePos.x/*+viewPos.x*/, mousePos.y/*+viewPos.y*/));
            if (selectedP != null) {
                virtTraceId = selectedP.x;
                virtualY = selectedP.y;
            }
        }
        if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("BhpMouseCenter select trace " + virtTraceId);
        if (virtTraceId == -999) {
            JOptionPane.showMessageDialog(null,"No trace is selected");
            return;
        }

        SeismicWorkflow pipeline = _selectedLayer.getPipeline();
        BhpSeismicTableModel layerParameter = _selectedLayer.getParameter();
        boolean isSeismicData = false;
        if (_selectedLayer.getDataType() == BhpViewer.BHP_DATA_TYPE_SEISMIC)
            isSeismicData = true;

        Hashtable selectParameter = new Hashtable();
        // eventually data selector will translate virtual trace id to real trace id
        TraceHeader tmeta = pipeline.getDataLoader().getDataSelector().
        getTraceHeader(virtTraceId);
        Hashtable headerValues = tmeta.getDataValues();
        Vector formats = pipeline.getDataLoader().getDataReader().getDataFormat().getTraceHeaderFormats();
        SegyHeaderFormat headerFormat = (SegyHeaderFormat) (formats.firstElement());
        Integer headerId;
        String headerName;
        Object headerValue;
        for (int j=0; j<layerParameter.getRowCount(); j++) {
            headerName = ((String)(layerParameter.getValueAt(j, 0))).trim();
            if (headerName.equals("tracl") && !isSeismicData)  continue;
            headerId = BhpSegyFormat.getFieldForName(headerName, headerFormat);
            headerValue = null;
            if (headerId!=null && headerValues.containsKey(headerId)) {
                headerValue = headerValues.get(headerId);
            }
            if (headerName.equals(layerParameter.getVerticalName())) {
                double verticalValue = _selectedLayer.findVerticalReadingFromModel(virtualY);
                headerValue = new Double(verticalValue);
            }
            if (headerValue != null) {
                selectParameter.put(headerName, headerValue);
            }
            else {
                ErrorDialog.showInternalErrorDialog(bhpViewerBase, Thread.currentThread().getStackTrace(),
                                               "BhpMouseCenter.mouseClicked error: Cannot find key headerName");
            }
        }

        BhpLayerEvent event = new BhpLayerEvent(
                            BhpViewer.DATASELECTION_FLAG, _selectedLayer, selectParameter);
        bhpViewerBase = event.getSource().getBhpViewerBase();
        bhpViewerBase.broadcastBhpLayerEvent(event);
    }

    /**
     * Actions for the mouse pressing events. <br>
     * When rubber band zooming flag is on, the event is
     * forwarded to the <code>{@link RubberView}</code>.
     * When a event layer is selected, the event is
     * forwarded to the <code>{@link BhpSelectionController}</code>.
     * When a map layer is selected, the event is
     * forwarded to the <code>{@link BhpMapMouseHandler}</code>.
     */
    public void mousePressed(MouseEvent me) {
        if (!_enableEventHandling) return;
        if (_zoomRubber) {
            if (_plot.getMainView().getRubberView() != null) {
                (_plot.getMainView().getRubberView()).mousePressed(me);
                _inZooming = true;
                _zoomRubber = false;
                return;
            }
        }

        if (_selector != null || _mapHandler != null) {

            if (_selector != null) _selector.processMouseEvent(me);
            else if (_mapHandler != null) _mapHandler.processMouseEvent(me);
            return;
        }
    }

    /**
     * Actions for the mouse releasing events. <br>
     * When rubber band zooming flag is on, the event is
     * forwarded to the <code>{@link RubberView}</code>.
     * When a event layer is selected, the event is
     * forwarded to the <code>{@link BhpSelectionController}</code>.
     * When a map layer is selected, the event is
     * forwarded to the <code>{@link BhpMapMouseHandler}</code>.
     */
    public void mouseReleased(MouseEvent me) {
        if (!_enableEventHandling) return;
        if (_inZooming) {
            if (_plot.getMainView().getRubberView() != null) {
                (_plot.getMainView().getRubberView()).mouseReleased(me);
                _inZooming = false;
                return;
            }
        }

        if (_selector != null) {
            _selector.processMouseEvent(me);
            _selectedLayer.notifyLayerListener();
            return;
        }
        if (_mapHandler != null) {
            _mapHandler.processMouseEvent(me);
            return;
        }
    }

    /**
     * Actions for the mouse dragging events. <br>
     * When rubber band zooming flag is on, the event is
     * forwarded to the <code>{@link RubberView}</code>.
     * When a event layer is selected, the event is
     * forwarded to the <code>{@link BhpSelectionController}</code>.
     * When a map layer is selected, the event is
     * forwarded to the <code>{@link BhpMapMouseHandler}</code>.
     */
    public void mouseDragged(MouseEvent me) {
        if (!_enableEventHandling) return;
        if (_inZooming) {
            if (_plot.getMainView().getRubberView() != null) {
                (_plot.getMainView().getRubberView()).mouseDragged(me);
                return;
            }
        }

        if (_selector != null) {
            _selector.processMouseEvent(me);
            return;
        }
        if (_mapHandler != null) {
            _mapHandler.processMouseEvent(me);
            return;
        }
    }

}
