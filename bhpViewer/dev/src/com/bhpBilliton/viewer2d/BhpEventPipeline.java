/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;
import com.gwsys.seismic.core.DataChooser;
import com.gwsys.seismic.core.DataLoader;
import com.gwsys.seismic.core.Interpretation;
import com.gwsys.seismic.core.ProcessingList;
import com.gwsys.seismic.core.SeismicLoader;
import com.gwsys.seismic.core.TraceInterpolator;
import com.gwsys.seismic.core.TraceNormalizer;
import com.gwsys.seismic.core.TraceProcessor;
import com.gwsys.seismic.core.TraceRasterizer;
import com.gwsys.seismic.core.AbstractWorkflow;
import com.gwsys.seismic.core.AutoGainController;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.reader.SeismicReader;
import com.gwsys.seismic.util.NumberRange;
import com.gwsys.seismic.io.TraceData;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is the special pipeline for event data processing.
 *               It has only a data selector,
 *               <code>{@link BhpEventDataSelector}</code>,
 *               following the reader, an instance of
 *               <code>{@link BhpDataReader}</code> implements
 *               <code>{@link BhpEventReaderInterface}</code>.
 *               Since this pipeline is simpler than standard
 *               pipeline, many methods declared in SeismicWorkflow
 *               interface is not supported.
 * @author Synthia Kong
 * @version 1.0
 *
 * This design needs to do refactoring.
 */

public class BhpEventPipeline extends AbstractWorkflow {
    private String _name;

    private DataLoader loader;
    private Interpretation interp = new ProcessingList();

    private BhpViewerBase viewer;

    /**
     * Constructs an event pipeline.
     */
    public BhpEventPipeline(BhpViewerBase viewer, SeismicReader reader) {
        this.viewer = viewer;
        _name = "Attributes";

        loader = new SeismicLoader();
        reader.setWorkflow(this);
        reader.setParentProcessor(null);

        loader.setDataReader(reader);
        BhpEventDataSelector dataSelector = new BhpEventDataSelector(viewer);
        dataSelector.setWorkflow(this);
        dataSelector.setParentProcessor(reader);

        loader.setDataViewport(dataSelector);
    }


    /**
     * Retrieves the name of the pipeline.
     */
    public String getName() { return _name; }
    /**
     * Sets the name of the pipeline.
     */
    public void setName(String name) { _name = name; }

    /**
     * This method is requested by interface cgPipeline of JSeismic.
     * It is not supported here.
     */
    public void setReverseOutput(boolean r) {
        throw new UnsupportedOperationException(
                "This is not supported for event;");
    }


    /**
     * Sets the data selector of the pipeline.
     */
    public void setDataSelector(DataChooser selector) {
        if (selector == null) return;
        if (!(selector instanceof BhpEventDataSelector)) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "BhpEventPipeline expects a BhpEventDataSelector, not " +
                                                    selector.getClass().getName());
            return;
        }
        selector.setWorkflow(this);
        DataChooser _dataSelector = loader.getDataSelector();
        selector.setParentProcessor(_dataSelector.getParentProcessor());

        _dataSelector.setWorkflow(null);
        _dataSelector.setParentProcessor(null);
        loader.setDataViewport(selector);
    }

    /**
     * Gets the trace meta data for a specific trace.
     */
    public TraceHeader getTraceHeader(int virtualTraceId) {
        return loader.getDataSelector().getTraceHeader(virtualTraceId);
    }
    public Bound2D getRealModelLimits() {
        return loader.getDataSelector().getRealModelLimits();
    }
    public Bound2D getVirtualModelLimits() {
        return loader.getDataSelector().getVirtualModelLimits();
    }

    public void invalidate(TraceProcessor process) {

    }
    public float[] getTraceSamples(int traceId) {
        TraceData data = new TraceData();
        loader.getDataReader().process((traceId-1), null, data);
        return (data.getSamples());
    }

    /**
     * Gets the event data values.
     */
    public float[] getEvent(String ename, BhpSeismicLayer seismic) {
        // implementation assumes that the pipeline is
        // BhpEventSegyReader->BhpEventDataSelector
        if (!(loader.getDataSelector() instanceof BhpEventDataSelector)) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "BhpEventPipeline.getEvent : null or invalid data selector");
            return null;
        }
        return ((BhpEventDataSelector)loader.getDataSelector()).getEvent(ename, seismic);
    }

    public void setTraceNormalization(TraceNormalizer parm1) {
        System.out.println("BhpEventPipeline does not have a normalization process.");
    }
    public void setTraceAGC(AutoGainController parm1) {
        System.out.println("BhpEventPipeline does not have a AGC process.");
    }
    public void setTraceInterpolator(TraceInterpolator parm1) {
        System.out.println("BhpEventPipeline does not have a interpolation process.");
    }

    public TraceRasterizer getTraceRasterizer() {
        //System.out.println("BhpEventPipeline does not have a rasterizer.");
        return null;
    }
    public void setTraceRasterizer(TraceRasterizer parm1) {
        System.out.println("BhpEventPipeline does not have a rasterizer.");
    }
    public void setInterpretation(Interpretation parm1) {
        throw new UnsupportedOperationException(
                    "This method is not supported in BhpEventPipeline");
    }
    public Interpretation getInterpretation() {
        return interp;
    }
    public void draw(Transform2D parm1) {
        System.out.println("BhpEventPipeline does not support draw.");
    }
    public float getTraceSampleByTime(int parm1, double parm2) {
        System.out.println("BhpEventPipeline does not support getTraceSampleByTime.");
        return 0;
    }
    public void setBoundingBox(Bound2D parm1, int parm2) {
        System.out.println("BhpEventPipeline does not support setBoundingBox.");
    }
    public void highlightTrace(int parm1, boolean parm2) {
        System.out.println("BhpEventPipeline does not support trace highlight.");
    }
    public boolean isTraceHighlighted(int parm1) {
        System.out.println("BhpEventPipeline does not support trace highlight.");
        return false;
    }
    public boolean isAnyTraceHighlighted() {
        System.out.println("BhpEventPipeline does not support trace highlight.");
        return false;
    }
    public void removeAllHighlighting() {
        System.out.println("BhpEventPipeline does not support trace highlight.");
    }

    public DataLoader getDataLoader() {
        return loader;
    }

    public void setDataLoader(DataLoader loader) {
        this.loader = loader;
    }


    public boolean process(int traceIdx, NumberRange sampleRange, TraceData traceData) {
        throw new UnsupportedOperationException("BhpEventPipeline");
    }


    public int getDirection() {
        throw new UnsupportedOperationException("BhpEventPipeline");
    }
}
