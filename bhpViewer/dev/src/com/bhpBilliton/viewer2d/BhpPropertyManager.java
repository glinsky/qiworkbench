/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.io.*;
import java.util.*;

import org.w3c.dom.*;

import com.bhpBilliton.viewer2d.ui.util.ElementAttributeReader;
import com.bhpBilliton.viewer2d.ui.util.FileTools;
import com.bhpBilliton.viewer2d.util.ErrorDialog;

import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Title: BHP Viewer <br>
 * <br>
 * Description: This class manages program properties and provides a general way
 * to set and get their values for both application and applet. <br>
 *
 * Currently, these properties are used:
 * setvletReadDataURL : URL for the servlet that extracts data with bhpio <br>
 * servletReadFilecfgURL : URL for the servlet that reads an XML configuration
 * file <br>
 * servletSaveFilecfgURL : URL for the servlet that saves the current working
 * session into an XML file <br>
 * setvletGeneralURL : URL of the general servlet <br>
 * appletInitFilecfg : the name (with path) of the XML configuration file which
 * will be opened when starts the applet <br>
 * bhpioBinPath : the path for the bhpio <br>
 * bhpDataPath : the path for the data, directory where *.dat file will be
 * found. <br>
 * bhpScriptPath : the path for the executable scripts <br>
 * bhpFilecfgPath : the directory where the XML configuration files can be
 * found. <br>
 * temporaryDirectory : the directory where the temporary file of bhpread output
 * will be put. <br>
 * dataAdapter : the available data adapters. This is a comma separated string
 * for available data adapters. Currently, these data adapters are implemented:
 * bhpsu, OpenSpirit. <br>
 * The first eight properties are for applet only. <br>
 * servletSummaryDataURL and servletReadDataURL are actually for bhpsu data
 * adapter only. They should be in sub-package bhpsuServlet. However, that will
 * need server configuration change. To avoid that, it is kept here. <br>
 * For every data adapter, it has its own property where its name is the string
 * as it is show in property dataAdapter, and its value is the corresponding
 * chooser class for that data adapter. It may also have additional properties
 * as defined in DataAdapterConfig.xml. There is also a property named xxxxFlag
 * where xxxx is the name of the data adapter. If its value equals to the name
 * of the data adapter, the data adapter is enabled at runtime. Otherwise, it is
 * treated as disabled. By default, bhpsu data adapter is enabled, and all the
 * other data adapters are disabled. <br>
 * <br>
 * Copyright: Copyright (c) 2001-2006 <br>
 * <br>
 *
 * @author Synthia Kong
 * @authod Gil Hansen
 * @version 1.1
 */

public class BhpPropertyManager {
  // Property names
  public static final String CFG_LOOKANDFEEL = "lookAndFeel";
  public static final String CFG_UNITS = "unitOption";
  public static final String CFG_UNITS_ALT = "units";
  public static final String CFG_MAXTRACENUM = "bhpMaxTraceNum";
  public static final String CFG_MAXTRACENUM_ALT = "maxTraces";
  public static final String CFG_DATAADAPTER = "dataAdapter";
  public static final String PATH_TEMP = "temporaryDirectory";
  public static final String PATH_TEMP_ALT = "tempPath";
  public static final String PATH_SESSIONS = "bhpFilecfgPath";
  public static final String PATH_SESSIONS_ALT = "sessionPath";
  public static final String PATH_SCRIPTS = "bhpScriptPath";
  public static final String PATH_SCRIPTS_ALT = "scriptPath";
  public static final String PATH_DATA = "bhpDataPath";
  public static final String PATH_DATA_ALT = "dataPath";
  public static final String PATH_BHPIO_BIN = "bhpioBinPath";
  public static final String PATH_BHPIO_BIN_ALT = "bhpioPath";

  public static final String PATH_SYSTEMP = "java.io.tmpdir";
  public static final String PATH_SYSUSERHOME = "user.home";
  public static final String PATH_SYSUSERDIR = "user.dir";

  public static final String DEFAULT_CFG_LOOKANDFEEL = "Standard";
  public static final String DEFAULT_CFG_UNITS = "metric";
  public static final String DEFAULT_PATH_BHPIO_BIN = "/usr/local/BHP_SU_V6.0/dev/bin/Linux/";
  public static final int DEFAULT_MAX_TRACE_NUM = 5000;

  public static  String DEFAULT_MAX_TRACE_NUM_STRING;
  public static  String DEFAULT_PATH_TEMP;
  public static  String DEFAULT_PATH_SESSIONS;
  public static  String DEFAULT_PATH_SCRIPTS;
  public static  String DEFAULT_PATH_DATA;
  private BhpViewerBase viewerBase;

  static void localDefaults() {
    DEFAULT_MAX_TRACE_NUM_STRING = Integer.toString(DEFAULT_MAX_TRACE_NUM);
    DEFAULT_PATH_TEMP = System.getProperty(PATH_SYSTEMP);
    DEFAULT_PATH_SESSIONS = System.getProperty(PATH_SYSUSERHOME);
    DEFAULT_PATH_SCRIPTS = System.getProperty(PATH_SYSUSERHOME);
    DEFAULT_PATH_DATA = System.getProperty(PATH_SYSUSERHOME);
  }

  static void remoteDefaults () {
    DEFAULT_MAX_TRACE_NUM_STRING = Integer.toString(DEFAULT_MAX_TRACE_NUM);
    DEFAULT_PATH_TEMP = ".";
    DEFAULT_PATH_SESSIONS = ".";
    DEFAULT_PATH_SCRIPTS = ".";
    DEFAULT_PATH_DATA = ".";
  }
  /**
   * Property names for those specific to remote access only.
   */
  public static final String CFG_SERVLETURL = "servletGeneralURL";
  public static final String CFG_APPLETINITFILE = "appletInitFilecfg";

/*
  public static final String[] SERVLET_PROPERTY_NAMES = {
    "servletReadDirectoryURL", "servletExecuteScriptURL",
    "servletSummaryDataURL", "servletReadDataURL",
    "servletReadFilecfgURL", "servletSaveFilecfgURL" };
*/

  private static final String[] DEFAULT_SERVLET_NAMES = {
    "servlet/readDirectory", "servlet/executeScript",
    "servlet/summaryData", "servlet/getData", "servlet/openConfig",
    "servlet/saveConfig" };

  //Default remote config parameters
  public static final String REMOTE_PATH_BHPIO_BIN = "/hou/apps/gttapps/bhp_su/beta/bin/Linux";
  public static final String REMOTE_PATH_DATA = "/home/ahglim/disk152/deepshelf/int_datasets";
  public static final String REMOTE_PATH_SCRIPTS = "/hou/apps/SU/INT/su_scripts";
  public static final String REMOTE_PATH_SESSIONS = "/home/ahglim/disk152/deepshelf/int_savesets";
  public static final String REMOTE_PATH_TEMP = "/tmp";
  public static final String REMOTE_CFG_APPLETINITFILE = "/home/ahglim/disk152/deepshelf/int_savesets/anthony_L4193_1a.cfg";
//  public static final String SERVLET_READ_DIR_URL = "http://houu120.pethou.bhp.com.au:8080/bhpviewer/servlet/readDirectory";
//  public static final String SERVLET_EXE_SCRIPT_URL = "http://houu120.pethou.bhp.com.au:8080/bhpviewer/servlet/executeScript";
//  public static final String SERVLET_SUM_DATA_URL = "http://houu120.pethou.bhp.com.au:8080/bhpviewer/servlet/summaryData";
  public static final String SERVLET_READ_DATA_URL = "http://houu120.pethou.bhp.com.au:8080/bhpviewer/servlet/getData";
  public static final String SERVLET_READ_CFG_URL = "http://houu120.pethou.bhp.com.au:8080/bhpviewer/servlet/openConfig";
  public static final String SERVLET_SAVE_CFG_URL = "http://houu120.pethou.bhp.com.au:8080/bhpviewer/servlet/saveConfig";

  private Hashtable propertyMap;

  /**
   * Consturcts a new property manager and initiate it. <br>
   * In the case of application, the property values are retrieved with
   * System.getProperty(). <br>
   */
  public BhpPropertyManager(BhpViewerBase viewer) {
      viewerBase = viewer;
    propertyMap = new Hashtable();
    if (isServerRemote()) {
      initAsRemote();
    } else {
      initAsLocal();
    }
  }

  public BhpViewerBase getViewer() {
      return viewerBase;
  }

  private void initAsRemote() {
    remoteDefaults();
    this.setProperty(PATH_BHPIO_BIN, REMOTE_PATH_BHPIO_BIN);
    this.setProperty(PATH_DATA, REMOTE_PATH_DATA);
    this.setProperty(PATH_SCRIPTS, REMOTE_PATH_SCRIPTS);
    this.setProperty(PATH_SESSIONS, REMOTE_PATH_SESSIONS);
    this.setProperty(PATH_TEMP, REMOTE_PATH_TEMP);
//NOTE: CFG_DATAADAPTER only used by Applet, but not in any HTML webpage
//    this.setProperty(CFG_DATAADAPTER, _japplet.getParameter(CFG_DATAADAPTER));

// meaningless, for reset a few lines below!
//    this.setProperty(CFG_MAXTRACENUM, _japplet.getParameter(CFG_MAXTRACENUM));

// meaningless, for reset a few lines below!
//    this.setProperty(CFG_UNITS, _japplet.getParameter(CFG_UNITS));

//NOTE: CFG_LOOKANDFEEL not in any HTML webpage
//    this.setProperty(CFG_LOOKANDFEEL, _japplet.getParameter(CFG_LOOKANDFEEL));

    setPropertyFromSystem(CFG_MAXTRACENUM, DEFAULT_MAX_TRACE_NUM_STRING);
    setPropertyFromSystem(CFG_UNITS, DEFAULT_CFG_UNITS);

    this.setProperty(CFG_APPLETINITFILE, REMOTE_CFG_APPLETINITFILE);

//NOTE: CFG_SERVLETURL not in any HTML webpage. However, meaningless
//      since set by getServletURL() below.
//    this.setProperty(CFG_SERVLETURL, _japplet.getParameter(CFG_SERVLETURL));

    String value = null;
    String servletServer = getServletURL();
  /*
    for (int k = 0; k < SERVLET_PROPERTY_NAMES.length; k++) {
      value = _japplet.getParameter(SERVLET_PROPERTY_NAMES[k]);
      if (value == null && servletServer != null) {
        value = servletServer + DEFAULT_SERVLET_NAMES[k];
      }
      this.setProperty(SERVLET_PROPERTY_NAMES[k], value);
    }
  */
//    this.setProperty("servletReadDirectoryURL", SERVLET_READ_DIR_URL);
//    this.setProperty("servletExecuteScriptURL", SERVLET_EXE_SCRIPT_URL);
//    this.setProperty("servletSummaryDataURL", SERVLET_SUM_DATA_URL);
    this.setProperty("servletReadDataURL", SERVLET_READ_DATA_URL);
    this.setProperty("servletReadFilecfgURL", SERVLET_READ_CFG_URL);
    this.setProperty("servletSaveFilecfgURL", SERVLET_SAVE_CFG_URL);
    if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
        System.out.println("Configuring Data Adapters");
    setupDataAdapters();

    // global file access methods
//      FIXME: FRAGILE! THIS IS GOING TO BREAK IN THE REFACTORED VERSION UNLESS IT IS FIXED FIRST
    FileTools.setRemoteFiles(true);
    FileTools.setURL(servletServer);

    if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
        System.out.println("Testing OSP Connection");
    // if OpenSpirit (ospFlag)
    String ospFlag = this.getProperty(CFG_DATAADAPTER);
    if ((ospFlag != null) && (ospFlag.indexOf("OpenSpirit") > -1)) {
      initiateOpenSpiritSessionController(servletServer);
    }
  }

//The servlet URL is the Tomcat URL + /servlet. But we are not going
//to use the viewer's servlets to do remote IO, but instead the workbench's
//remote seismic IO services. Until then, this functionality will not work.
//Therefore, for now, we use the Tomcat URL for the workbench which is
//not the code base to access the viewer's servlets.
  private String getServletURL() {
    String servletServer = this.getProperty(CFG_SERVLETURL);
    if (servletServer != null && servletServer.length() > 0) {
      if (!servletServer.endsWith("/")) {
        servletServer = servletServer + "/";
        this.setProperty(CFG_SERVLETURL,servletServer);
      }
    } else {
      servletServer = setServletURLfromCodeBase();
    }
    return servletServer;
  }

  private String setServletURLfromCodeBase() {
    String servletServer;
    // if it is not specified, guess it from the code base
//    servletServer = _japplet.getCodeBase().toString();
    servletServer = viewerBase.getAgent().getMessagingManager().getTomcatURL();
    if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
        System.out.println("Guessing servletServer from CodeBase. [" + servletServer + "]");
    if (servletServer != null) {
      // code base is ..../lib/
      if (servletServer.endsWith("/lib") || servletServer.endsWith("/lib/")) {
        servletServer = servletServer.substring(0, servletServer.lastIndexOf("lib"));
      }
      this.setProperty(CFG_SERVLETURL, servletServer);
    }
    if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
        System.out.println("Using name: " + servletServer);
    return servletServer;
  }

  private void initAsLocal() {
    localDefaults();
    setPropertyFromSystem(PATH_BHPIO_BIN, DEFAULT_PATH_BHPIO_BIN);
    setPropertyFromSystem(PATH_BHPIO_BIN_ALT, getProperty(PATH_BHPIO_BIN));

    setPropertyFromSystem(PATH_DATA, DEFAULT_PATH_DATA);
    setPropertyFromSystem(PATH_DATA_ALT, getProperty(PATH_DATA));

    setPropertyFromSystem(PATH_SCRIPTS, DEFAULT_PATH_SCRIPTS);
    setPropertyFromSystem(PATH_SCRIPTS_ALT, getProperty(PATH_SCRIPTS));

    setPropertyFromSystem(PATH_SESSIONS, DEFAULT_PATH_SESSIONS);
    setPropertyFromSystem(PATH_SESSIONS_ALT, getProperty(PATH_SESSIONS));

    setPropertyFromSystem(PATH_TEMP, DEFAULT_PATH_TEMP);
    setPropertyFromSystem(PATH_TEMP_ALT, getProperty(PATH_TEMP));

    setPropertyFromSystem(CFG_MAXTRACENUM, DEFAULT_MAX_TRACE_NUM_STRING);
    setPropertyFromSystem(CFG_MAXTRACENUM_ALT, getProperty(CFG_MAXTRACENUM));

    setPropertyFromSystem(CFG_UNITS, DEFAULT_CFG_UNITS);
    setPropertyFromSystem(CFG_UNITS_ALT, getProperty(CFG_UNITS));

    setPropertyFromSystem(CFG_LOOKANDFEEL, DEFAULT_CFG_LOOKANDFEEL);

    setupDataAdapters();

  }

  private void setPropertyFromSystem (String propertyName, String defaultValue) {
    String systemProperty = getSystemProperty(propertyName, defaultValue);
    setProperty(propertyName,systemProperty);
    if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
        System.out.println(propertyName + " = " + systemProperty);
  }

  private String getSystemProperty (String propertyName, String defaultValue) {
    String rval;
    try {
      rval = System.getProperty(propertyName);
      if (rval == null || rval.length() == 0) {
//              System.out.println("Could not find property: " + propertyName);
        rval = defaultValue;
      }
    }
    catch (Exception e) {
      rval = defaultValue;
    }
    return rval;
  }

  /**
   * Sets the value of the given property.
   *
   * @param key
   *            the name of the property.
   * @param value
   *            the value of the property.
   */
  public void setProperty(String key, String value) {
    String lValue = value;
    if (lValue == null) {
      lValue = "";
    }
    propertyMap.put(key, lValue);
  }

  /**
   * Gets the value of a specific property.
   *
   * @param key
   *            the name of the property.
   * @return the value of the requested property.
   */
  public String getProperty(String key) {
    Object property = propertyMap.get(key);
    String rval = "";
    if (property!=null) {
      rval = property.toString();
    }
    return rval;
  }

  /**
   * Determine if Tomcat server is running local or remote
   *
   * @return True, if Tomcat running remotely; otherwise, false.
   */
  public boolean isServerRemote() {
    return viewerBase.getAgent().getMessagingManager().getLocationPref().equals(QIWConstants.REMOTE_SERVICE_PREF);
  }

  private void setupDataAdapters() {
    // this file specifies the available data adapters.
    InputStream ins = this.getClass().getResourceAsStream("DataAdapterConfig.xml");

    StringBuffer adapterNames = new StringBuffer();
    try {
      Element root = BhpViewerHelper.getXMLTree(ins);

      if (!root.getNodeName().equals("DataAdapterConfig")) {
          ErrorDialog.showInternalErrorDialog(viewerBase, Thread.currentThread().getStackTrace(),
                                              "Incorrect data adapter configuration.");
        return;
      }

      NodeList children = root.getElementsByTagName("DataAdapter");
      ElementAttributeReader emt;
      for (int i = 0; i < children.getLength(); i++) {
        emt = new ElementAttributeReader(children.item(i));
        if (emt.getBoolean("enableFlag")) {
          addDataAdapter(emt);    ////////////////////////////
          adapterNames.append(emt.getString("adapterName") + ",");
        }
      }
    } catch (Exception ex) {
        ErrorDialog.showInternalErrorDialog(viewerBase, Thread.currentThread().getStackTrace(),
                                            "Failed to read file DataAdapterConfig.xml.");
    }
    String returnString = "";
    if (adapterNames.length() == 0) {
      // no data adapter is found from the configuration file
        ErrorDialog.showInternalErrorDialog(viewerBase, Thread.currentThread().getStackTrace(),
                                            "No proper data adapter is found.");
    } else {
      // Remove the trailing comma
      returnString = adapterNames.substring(0,adapterNames.length()-1);
    }
    this.setProperty(CFG_DATAADAPTER, returnString);
  }

  private void addDataAdapter(Element adapter) {
    ElementAttributeReader emt = new ElementAttributeReader(adapter);

    String adapterName = emt.getString("adapterName");
    String chooser;
    String flagValue;
    String flagName = adapterName + "Flag";
    String flagNameNew = "enable_" + adapterName;

    if (adapterName.equals("OpenSpirit"))
      flagName = "ospFlag";

    if (isServerRemote()) {
      chooser = emt.getString("remoteChooser");
      flagValue = System.getProperty(flagName);//_japplet.getParameter(flagName);
    } else {
      chooser = emt.getString("localChooser");
      flagValue = System.getProperty(flagName);
    }

    if (flagValue==null || flagValue.length()==0) {
      if (isServerRemote()) {
        flagValue = System.getProperty(flagName);//_japplet.getParameter(flagNameNew);
      } else {
        flagValue = System.getProperty(flagNameNew);
      }
    }

    // Set default to Index-Segy, Horizon, Log-Las, BHP-SU
    if (flagValue == null) {
      if (adapterName.equals(com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_BHPSU)) {
        flagValue = com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_BHPSU;
      }
      else if (adapterName.equals(com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_ISEGY)){
        flagValue = com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_ISEGY;
      }else if (adapterName.equals(com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_XMLHRZ)) {
        flagValue = com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_XMLHRZ;
      }else if (adapterName.equals(com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_LAS)) {
        flagValue = com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_LAS;
      }else if (adapterName.equals(com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_BHPSU)) {
            flagValue = com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_BHPSU;
      }else if (adapterName.equals(com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_SEGY)) {
            flagValue = com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_SEGY;
      }
      else {
        flagValue = "";
      }
    }

    boolean mapEnabled = emt.getBoolean("mapEnabled");
    this.setProperty(adapterName + "MapEnabled", Boolean.toString(mapEnabled));
    this.setProperty(adapterName, chooser);
    this.setProperty(flagName, flagValue);

    NodeList children = emt.getElementsByTagName("AdditionalProperty");
    for (int i = 0; i < children.getLength(); i++) {
      emt.setNode(children.item(i));
      this.setProperty(emt.getString("propertyName"), emt.getString("propertyValue"));
    }
  }

  private void initiateOpenSpiritSessionController(String url) {
    try {

      Class theClass = Class.forName("external.ospDataAdapter.OspRemoteSessionController");
      if (theClass != null) {
        Class[] parameterTypes = { url.getClass() };
        java.lang.reflect.Method theMethod = theClass.getMethod("setUrlString", parameterTypes);
        Object[] parameters = { url };
        theMethod.invoke(null, parameters);
      }
    } catch (Exception ex) {
        ErrorDialog.showInternalErrorDialog(viewerBase, Thread.currentThread().getStackTrace(),
                                            "No OSP");
    }
  }
}
