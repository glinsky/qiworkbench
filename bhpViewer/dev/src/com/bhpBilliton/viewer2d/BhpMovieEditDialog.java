/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.ui.BhpMovieDialog;
import com.bhpBilliton.viewer2d.util.ErrorDialog;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This dialog is for editing the captured
 *               frames. <br><br>
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpMovieEditDialog extends JDialog {
    private BhpMovieDialog _parentDialog;
    private BhpMovieManager _mmanager;

    private JList _list;
    private DefaultListModel _model;

    private BhpViewerBase viewer;

    /**
     * Constructs a new dialog and sets up the GUI.
     */
    public BhpMovieEditDialog(BhpViewerBase viewer, BhpMovieDialog parent, BhpMovieManager mmanager) {
        super(parent, "Movie Edit", true);
        this.viewer = viewer;
        _parentDialog = parent;
        _mmanager = mmanager;
        this.getContentPane().add(buildOptionContentPanel(), BorderLayout.CENTER);
        this.getContentPane().add(buildOptionControlPanel(), BorderLayout.SOUTH);
        this.setSize(200, 400);
    }

    /**
     * Gets related information of movie settings
     * and fill the fields of GUI. <br>
     */
    public void initEditDialog() {
        _model.clear();
        int imageNumber = _mmanager.getImageNumber();
        for (int i=0; i<imageNumber; i++)
            _model.addElement("Frame " + i);
    }

    private void applyEditDialog() {
        int[] order = new int[_model.size()];
        try {
            for (int i=0; i<order.length; i++) {
                order[i] = Integer.parseInt(_model.get(i).toString().substring(6));
            }
            _mmanager.updateImageList(order);
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "BhpMovieEditDialog.applyEditDialot Exception:");
        }
    }

    private void deleteFrames() {
        int oldSelectIndex = _list.getSelectedIndex();
        Object[] selected = _list.getSelectedValues();
        for (int i=0; i<selected.length; i++)
            _model.removeElement(selected[i]);

        _list.clearSelection();
        if (_model.size() > 0) {
            if (_model.size() > oldSelectIndex)
                _list.setSelectedIndex(oldSelectIndex);
            else _list.setSelectedIndex(0);
        }
    }

    private void moveFrames(int direction) {
        if (direction>0 && _list.getMinSelectionIndex()==0) return;
        if (direction<0 && _list.getMaxSelectionIndex()==_model.getSize()-1) return;

        int insertPosition = 0;
        if (direction > 0) insertPosition = _list.getMinSelectionIndex() - 1;
        if (direction < 0) insertPosition = _list.getMinSelectionIndex() + 1;
        if (insertPosition < 0) insertPosition = 0;
        if (insertPosition > _model.getSize()) insertPosition = _model.getSize();

        Object[] selected = _list.getSelectedValues();
        for (int i=0; i<selected.length; i++)
            _model.removeElement(selected[i]);
        for (int i=0; i<selected.length; i++) {
            _model.add(insertPosition+i, selected[i]);
        }
        _list.addSelectionInterval(insertPosition, insertPosition+selected.length-1);
    }

    private void listSelectionChanged() {
        if (_list.getSelectedValue() == null) {
            _parentDialog.setImageFrame(-1);
            return;
        }
        String selected = _list.getSelectedValue().toString();
        if (selected != null && selected.length() > 0) {
            selected = selected.trim();
            try {
                int selectedIndex = Integer.parseInt(selected.substring(6));
                _parentDialog.setImageFrame(selectedIndex);
            }
            catch (Exception ex) {
                ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                        "BhpMovieEditDialog.listSelectedChanged Exception");
            }
        }
    }

    private JPanel buildOptionContentPanel() {
        _model = new DefaultListModel();
        _list = new JList(_model);
        _list.addListSelectionListener(new MovieListListener());
        JPanel listPanel = new JPanel(new BorderLayout());
        listPanel.add(new JScrollPane(_list));

        JButton deleteButton = new JButton("Delete");
        deleteButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                deleteFrames();
            }
        });
        JButton upButton = new JButton("Move Up");
        upButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                moveFrames(1);
            }
        });
        JButton downButton = new JButton("Move Down");
        downButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                moveFrames(-1);
            }
        });
        Box box1 = new Box(BoxLayout.Y_AXIS);
        box1.add(Box.createVerticalGlue());
        box1.add(deleteButton);
        box1.add(upButton);
        box1.add(downButton);

        Box box = new Box(BoxLayout.X_AXIS);
        box.add(Box.createHorizontalStrut(20));
        box.add(listPanel);
        box.add(box1);
        box.add(Box.createHorizontalGlue());

        JPanel panel = new JPanel(new BorderLayout(20, 20));
        panel.setBorder(new EtchedBorder());
        panel.add(box);

        return panel;
    }

    private JPanel buildOptionControlPanel() {
        JButton okb = new JButton("Ok");
        okb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                applyEditDialog();
                setVisible(false);
            }
        });
        JButton clb = new JButton("Cancel");
        clb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });
        JPanel panel = new JPanel(new FlowLayout());
        panel.add(okb);
        panel.add(clb);
        return panel;
    }

    private class MovieListListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent e) {
            listSelectionChanged();
        }
    }
}
