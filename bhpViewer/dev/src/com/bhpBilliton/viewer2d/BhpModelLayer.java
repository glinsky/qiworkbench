/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d;


import java.awt.Dimension;

import org.w3c.dom.Node;

import com.bhpBilliton.viewer2d.data.BhpModelReaderInterface;
import com.bhpBilliton.viewer2d.ui.BhpBusyDialog;
import com.bhpBilliton.viewer2d.util.ErrorDialog;

import com.gwsys.seismic.core.DataChooser;
import com.gwsys.seismic.core.DefaultRasterizer;
import com.gwsys.seismic.core.SegyTraceImage;
import com.gwsys.seismic.core.TraceNormalizer;
import com.gwsys.seismic.core.TraceRasterizer;
import com.gwsys.seismic.core.SeismicWorkflow;
import com.gwsys.seismic.core.DefaultWorkflow;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.SeismicColorMap;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.gw2d.model.CommonShapeLayer;
import com.gwsys.gw2d.model.CommonDataModel;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class is used to display the model data.
 *               Model data has the same format as the seismic data.
 *               However, for each trace, only the first half is the attribute
 *               value, and the second half is the depth value.
 *               The model data can be displayed as normal seismic data.
 *               It also has a structural interpolation flag. <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 * @version 2.0 Add refLayer variable to ref this model to SeismicLayer
 */

public class BhpModelLayer extends BhpLayer {
    private BhpLayer refLayer;

    private static final long serialVersionUID = -2222429477065893274L;
    private BhpModelTraceInterpolator _interpolator;
    private BhpModelTraceRasterizer _rasterizer;

    private BhpViewerBase viewer;

    /**
     * Creates a model layer with a reference layer. <br>
     * This construtor is usually used when a new layer is created
     * by the users with GUI control.
     * @param viewer the parent viewer of the new layer.
     * @param win the parent window of the new layer.
     * @param id the identification number of the new layer.
     * @param dstype the name of the data source.
     * @param name name of the selected dataset.
     *        This is also the default name of the layer.
     * @param pathlist path of the selected dataset. <br>
     *        For bhpio data, it is the pathlist. For OpenSpirit data,
     *        it is the session name.
     * @param property the name of the selected property. <br>
     * @param propertySync the boolean flag for property synchronization.
     * @param parameter a table model stores the user data selection.
     *        In the case of bhpio, it can be used to construct the
     *        query string used to run bhpio.
     * @param ref the reference layer. <br>
     *        The new layer will inherit from this reference layer things
     *        like scales, pipeline settings, display options, and etc.
     */
    public BhpModelLayer(BhpViewerBase viewer, BhpWindow win,
                         int id, String dstype, String name, String pathlist,
                         String property, boolean propertySync,
                         BhpSeismicTableModel parameter, BhpLayer ref) {
        super(viewer, win, id, dstype, name, name, pathlist,
              parameter, property, propertySync, ref, BhpViewer.BHP_DATA_TYPE_MODEL);
        this.viewer = viewer;
    }

    /**
     * Creates a new model layer with specified attributes. <br>
     * This construtor is usually used when a new layer is created
     * parsing saved XML configuration file.
     * @param viewer the parent viewer of the new layer.
     * @param win the parent window of the new layer.
     * @param id the identification number of the new layer.
     * @param dstype the name of the data source.
     * @param dname name of the selected dataset.
     * @param name name of the new layer.
     * @param pathlist path of the selected dataset. <br>
     *        For bhpio data, it is the pathlist. For OpenSpirit data,
     *        it is the session name.
     * @param property the name of the selected property. <br>
     * @param parameter a table model stores the user data selection.
     *        In the case of bhpio, it can be used to construct the
     *        query string used to run bhpio.
     * @param mupt horizontal scale with unit of model-unit-per-trace.
     * @param mups vertical scale with unit model-unit-per-sample.
     * @param talk a boolean indicates if the new layer should
     *        broadcase its change event.
     * @param syn the synchronization flag of the new layer.
     * @param trans transparency(opacity) of the new layer.
     * @param propertySync a boolean indicates if the new layer will be
     *        synchronized for property name.
     * @param pnode the pipeline node in the DOM tree. <br>
     *        It is used to initialize the pipeline of the new layer.
     * @param rp a boolean for polarity.
     */
    public BhpModelLayer(BhpViewerBase viewer, BhpWindow win, int id, String dstype,
                         String dname, String name, String pathlist,
                         String property, BhpSeismicTableModel parameter,
                         double mupt, double mups, boolean talk, int syn,
                         int trans, boolean propertySyn, Node pipelineNode, boolean rp) {
        super(viewer, win, id, dstype, dname, name, pathlist,
              parameter, property, talk, syn, trans, propertySyn,
              pipelineNode, BhpViewer.BHP_DATA_TYPE_MODEL, rp, false);
        _hscaleFactor = mupt;
        _vscaleFactor = mups;
        this.viewer = viewer;
    }

    /**
     * Converts the model layer object into an XML string. <br>
     * This is used when save the working session into an XML file.
     * Please refer to BhpViewer.dtd for the detail of the format.
     * @return an XML string for the object.
     */
    public String toXMLString() {
        StringBuffer content = new StringBuffer();
        content.append("        " + "<BhpModelLayer>");
        content.append(super.toXMLString());
        content.append("        " + "</BhpModelLayer>\n");
        return content.toString();
    }

    /**
     * Transforms a vertical model space coordinate to virtual vertical value. <br>
     * This method is used for mouse click data synchronization.
     * It overrides the implementation in <code>{@link BhpLayer}</code>.
     * @param modelV vertical model sapce coordinate.
     * @return vertical axis reading.
     */
    public double findVerticalReadingFromModel(double modelV) {
        float startDepth = _rasterizer.getStartDepth();
        // allow 4 fraction digit at most
        double verticalValue = ((int)((startDepth + modelV)*10000.0)) / 10000.0;
        return verticalValue;
    }

    /**
     * Transforms a virtual vertical value to model space coordinate. <br>
     * This method is used for mouse click data synchronization.
     * It overrides the implementation in <code>{@link BhpLayer}</code>.
     * @param readingV vertical axis reading
     * @return model space coordinate
     */
    public int findModelFromVerticalReading(double readingV) {
        float startDepth = _rasterizer.getStartDepth();
        int result = (int) (readingV - startDepth);
        return result;
    }

    /**
     * Finds out the read data value at the specific point. <br>
     * This default implementation simply returns 0.
     * Concrete class extends BhpLayer should have meaningful implemenation.
     * @param traceid trace number, the horizontal position.
     * @param vreading vertical axis reading
     * @return data value of the given trace at the specific vertical position.
     */
    public double findSpecificDataValue(int traceid, double vreading) {
        double re = 0;
        if (_reader != null) {
            TraceData traceData = new TraceData();
            boolean presult = _reader.process(traceid, null, traceData);
            if (presult) {
                float[] dataArray = traceData.getSamples();
                float[] depthArray = ((BhpModelReaderInterface)_reader).getDepthArray(traceid);
                if (depthArray != null && depthArray.length>0) {
                    if (vreading >= depthArray[0] &&
                        vreading <=depthArray[depthArray.length-1]) {
                        int index = 0;
                        for (int i=1; i<depthArray.length; i++) {
                            if (vreading <= depthArray[i]){
                                index = i-1;
                                break;
                            }
                        }
                        //System.out.println("BhpModelLayer.findSpecificDataValue : " + vreading + "->" + index);
                        re = dataArray[index];
                    }
                }
            }
        }
        return re;
    }

    public void updateLayer(boolean redraw) {
        if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("BhpModelLayer.updateLayer=" + _image);

        double modelUnitPerTrace = getLayerHorizontalScale();
        double modelUnitPerSample = getLayerVerticalScale();

        double modelWidth = _reader.getMetaData().getNumberOfTraces() * modelUnitPerTrace;

        BhpModelTraceRasterizer rasterizer = (BhpModelTraceRasterizer) _pipeline.getTraceRasterizer();
        double minDepth = rasterizer.getStartDepth() * modelUnitPerSample;
        double modelHeight = (rasterizer.getEndDepth() -
                              rasterizer.getStartDepth()) *
                              modelUnitPerSample;
        boolean oldNotify = _image.isNotificationEnabled();
        _image.setNotification(false);
        _image.setScale(modelUnitPerTrace, modelUnitPerSample);
        if (refLayer != null)
            _image.setLocation(0, minDepth);
        _image.setNotification(oldNotify);

        _modelBox = new Bound2D(0, minDepth, modelWidth, minDepth+modelHeight);
        _containerModel.setBoundingBox(_modelBox);
        _shapeListLayer.setBoundingBox(_modelBox);
        if (redraw) {
            AbstractPlot ppp = _window.getBhpPlot();
            if (ppp.getAxisAssociateLayer() == this.getIdNumber()) {
                ppp.updateVAxes();
                ppp.updateHAxes();
                ppp.updateMiscAnnotation();
            }
        }
    }

    public void updateSeismicDisplay() {
        if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("BhpModelLayer.updateSeismicDisplay=" + _pipeline);
        if (!(_reader instanceof BhpModelReaderInterface)) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "BhpModelLayer.updateSeismicDisplay incorrect reader");
            return;
        }

        BhpModelTraceRasterizer rasterizer = (BhpModelTraceRasterizer) _pipeline.getTraceRasterizer();
        float oldStartDepth = rasterizer.getStartDepth();
        float oldEndDepth = rasterizer.getEndDepth();
        DataChooser oldSelector = _pipeline.getDataLoader().getDataSelector();
        BhpModelDataSelector newSelector = new BhpModelDataSelector();
        _pipeline.getDataLoader().setDataReader(_reader);
        _pipeline.setDataSelector(newSelector);

        _interpolator.setBhpModelReader((BhpModelReaderInterface)_reader);
        _rasterizer.setBhpModelReader((BhpModelReaderInterface)_reader);
        newSelector.setStartDepth(oldStartDepth);
        newSelector.setEndDepth(oldEndDepth);
        _interpolator.setStartDepth(oldStartDepth);
        _interpolator.setEndDepth(oldEndDepth);
        _rasterizer.setStartDepth(oldStartDepth);
        _rasterizer.setEndDepth(oldEndDepth);

        if (oldSelector.getPrimaryKey() != DataChooser.TRACE_ID_AS_KEY) {
            newSelector.setPrimaryKey(oldSelector.getPrimaryKey());
            newSelector.setPrimaryKeyEnd(oldSelector.getPrimaryKeyEnd());
            newSelector.setPrimaryKeyStart(oldSelector.getPrimaryKeyStart());
            newSelector.setPrimaryKeyStep(oldSelector.getPrimaryKeyStep());
        }
        else {
            // for TRACE_ID_AS_KEY, let's inherit start and step value
            newSelector.setPrimaryKey(oldSelector.getPrimaryKey());
            newSelector.setPrimaryKeyStart(oldSelector.getPrimaryKeyStart());
            newSelector.setPrimaryKeyStep(oldSelector.getPrimaryKeyStep());
        }
        if (oldSelector.getSecondaryKey() != DataChooser.NO_SECONDARY_KEY) {
            newSelector.setSecondaryKey(oldSelector.getSecondaryKey());
            newSelector.setSecondaryKeyEnd(oldSelector.getSecondaryKeyEnd());
            newSelector.setSecondaryKeyStart(oldSelector.getSecondaryKeyStart());
            newSelector.setSecondaryKeyStep(oldSelector.getSecondaryKeyStep());
        }

        _pipeline.invalidate(_reader);

        //double minDepth = ((BhpModelSegyReader)_reader).getMinDepth();
        double minDepth = 0;//rasterizer.getStartDepth();
        _shapeListLayer.removeShape(_image);
        double modelUnitPerTrace = getLayerHorizontalScale();
        double modelUnitPerSample = getLayerVerticalScale();
        _image = new SegyTraceImage(0, minDepth, modelUnitPerTrace,
                                            modelUnitPerSample, _pipeline);
        RenderingAttribute att = new RenderingAttribute(_containerModel);
        _image.setAttribute(att);
        _shapeListLayer.addShape(_image);

        double modelWidth = _reader.getMetaData().getNumberOfTraces() * modelUnitPerTrace;
        //double modelHeigth = _reader.getMetaData().getSamplesPerTrace() * _modelUnitPerSample;
        double modelHeigth = (rasterizer.getEndDepth() -
                              rasterizer.getStartDepth()) *
                              modelUnitPerSample;
        //_modelBox = new Bound2D(0, 0, modelWidth, modelHeigth);
        _modelBox = new Bound2D(0, minDepth, modelWidth, minDepth+modelHeigth);
        _containerModel.setBoundingBox(_modelBox);
        _shapeListLayer.setBoundingBox(_modelBox);
        //_shapeListLayer.invalidateLayer();

        Dimension wsize = _window.getSize();
        _window.setSize((int) (wsize.getWidth()-1), (int) (wsize.getHeight()-1));
        AbstractPlot ppp = _window.getBhpPlot();
        if (ppp.getAxisAssociateLayer() == this.getIdNumber()) {
            //ppp.setAxisAssociateLayer(-1, true);
            ppp.updateHAxes();
            ppp.updateVAxes();
            ppp.updateMiscAnnotation();
        }
        this.setTransformationWithoutRepaint(this.getTransformation());
        _window.setSize((int) wsize.getWidth(), (int) wsize.getHeight());
        _viewer.changeLoadingLayerNumber(BhpBusyDialog.RMV_LAYER);
    }

    public double getTimeRangeStart() {
        return _rasterizer.getStartDepth();
    }

    public double getTimeRangeEnd() {
        return _rasterizer.getEndDepth();
    }

    public void setTimeRange(double start, double end) {
        if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("BhpModelLayer.setTimeRange=" + start);
        BhpModelTraceRasterizer raster = (BhpModelTraceRasterizer)getPipeline().getTraceRasterizer();
        BhpModelDataSelector selector = (BhpModelDataSelector)
        getPipeline().getDataLoader().getDataSelector();
        BhpModelTraceInterpolator interpolator = (BhpModelTraceInterpolator)
        getPipeline().getInterpretation().getTraceInterpolator();
        float fs = (float) start;
        float fe = (float) end;
        raster.setStartDepth(fs);
        selector.setStartDepth(fs);
        interpolator.setStartDepth(fs);
        raster.setEndDepth(fe);
        selector.setEndDepth(fe);
        interpolator.setEndDepth(fe);
        _pipeline.invalidate(_pipeline.getInterpretation().getTraceInterpolator());
    }

    public boolean receiveBhpLayerEvent(BhpLayerEvent e) {
        BhpLayer layer = e.getSource();
        int flag = e.getTypeFlag();

        if (layer.getIdNumber() == this.getIdNumber()) {
            if ((flag & BhpViewer.COLORBAR_FLAG) != 0) { // use my transparent value
                updateColorMap((SeismicColorMap) e.getParameter());
            }

            return false;
        }

        int mflag = super.handleBhpLayerEvent(e);

        if (((flag & BhpViewer.MODEL_FLAG)!=0) && (this.getPropertySync())) {
            String properties = this.getParameter().getProperties();
            String np = layer.getPropertyName();
            if (layer instanceof BhpMapLayer) {
                np = np.substring(0, np.lastIndexOf(":"));
            }
            if (properties!=null && properties.length()>0 &&
                properties.indexOf(np)!=-1) {
                this.setPropertyName(np);
                mflag = mflag | BhpViewer.MODEL_FLAG;
            }
        }

        if (((mflag & BhpViewer.DATAINCREMENT_FLAG)!=0) ||
            ((mflag & BhpViewer.DATASELECTION_FLAG)!=0) ||
            ((mflag & BhpViewer.MODEL_FLAG)!=0) ||
            ((mflag & BhpViewer.NAME_FLAG)!=0)) {
            this.generateLayer();
        }
        else if (mflag != 0) {
            this.updateLayer();
        }
        return false;
    }

    public void setupSeismicDisplay() {
        if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("BhpModelLayer.setupSeismicDisplay=" + _pipeline);
        if (!(_reader instanceof BhpModelReaderInterface)) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "BhpModelLayer.setupSeismicDisplay incorrect reader");
            return;
        }
        _pipeline = new DefaultWorkflow(_reader);
        _interpolator =
            new BhpStructuralInterpolator((BhpModelReaderInterface)_reader);
        _rasterizer =
            new BhpStructuralRasterizer((BhpModelReaderInterface)_reader,
                    (BhpStructuralInterpolator)_interpolator);

        BhpModelTraceNormalization normalizer = new BhpModelTraceNormalization();

        _pipeline.setDataSelector(new BhpModelDataSelector());
        _pipeline.setTraceNormalization(normalizer);
        _pipeline.setTraceInterpolator(_interpolator);

        _pipeline.setTraceRasterizer(_rasterizer);

        _pipeline.getTraceRasterizer().setPlotType(DefaultRasterizer.DENSITY_TRACE);
        normalizer.setNormalizationMode(TraceNormalizer.AMPLITUDE_LIMITS);
        normalizer.setNormalizationLimits(_reader.getMetaData().getMinimumAmplitude(),
                                          _reader.getMetaData().getMaximumAmplitude());

        if (_pipelineNode != null) {
            BhpViewerHelper.initWithXML(viewer, _pipelineNode, _pipeline);
            _pipelineNode = null;
        }
        else {
            //BhpLayer sampleLayer = _window.getSelectedLayer();
            if (_sampleLayer != null) {
                boolean sampleLayerIsModel = false;
                if (_sampleLayer instanceof BhpModelLayer) sampleLayerIsModel = true;
                initPipelineWithAnother(_sampleLayer.getPipeline(), sampleLayerIsModel);
            }
        }

        double modelUnitPerTrace = getLayerHorizontalScale();
        double modelUnitPerSample = getLayerVerticalScale();
        double modelWidth = _reader.getMetaData().getNumberOfTraces() * modelUnitPerTrace;

        BhpModelTraceRasterizer rasterizer = (BhpModelTraceRasterizer) _pipeline.getTraceRasterizer();
        double modelHeigth = (rasterizer.getEndDepth() -
                              rasterizer.getStartDepth()) *
                              modelUnitPerSample;

        double minDepth = 0;
        _modelBox = new Bound2D(0, minDepth, modelWidth, minDepth+modelHeigth);
        _deviceBox = new Bound2D(0, 0, modelWidth, modelHeigth);
        _transformation = new Transform2D(_modelBox, _deviceBox, false, false);

        _shapeListLayer = new CommonShapeLayer();
        _containerModel = new CommonDataModel();
        _containerModel.addLayer(_shapeListLayer);
        _containerModel.setBoundingBox(_modelBox);
        _shapeListLayer.setBoundingBox(_modelBox);

        //fix bug : match load model on the seismic layer
        double oldImageLocation =  minDepth;
        if (_window.getSelectedLayer() instanceof BhpSeismicLayer){
            refLayer = _window.getSelectedLayer();
            oldImageLocation = rasterizer.getStartDepth()*modelUnitPerSample;

        }
        _image = new SegyTraceImage(0, oldImageLocation,
                modelUnitPerTrace, modelUnitPerSample, _pipeline);
        RenderingAttribute att = new RenderingAttribute(_containerModel);
        _image.setAttribute(att);

        _shapeListLayer.addShape(_image);
        this.setModel(_containerModel);
        this.setTransformation(_transformation);

        _viewer.addBhpLayer(_window, this);
        _viewer.changeLoadingLayerNumber(BhpBusyDialog.RMV_LAYER);
    }

    private void initPipelineWithAnother(SeismicWorkflow plR, boolean isModel) {
        BhpModelDataSelector sltr = (BhpModelDataSelector)
        _pipeline.getDataLoader().getDataSelector();
        sltr.applyGaps(plR.getDataLoader().getDataSelector().isGapApplied());
        sltr.setGapInTraces(plR.getDataLoader().getDataSelector().getGapInTraces());
        //sltr.setStartSampleValue(plR.getDataSelector().getStartSampleValue());
        //sltr.setEndSampleValue(plR.getDataSelector().getEndSampleValue());

        if (plR.getInterpretation().getTraceNormalization() != null) {
            TraceNormalizer norm =
                (_pipeline.getInterpretation().getTraceNormalization());
            TraceNormalizer normR =
                (plR.getInterpretation().getTraceNormalization());
            norm.setNormalizationMode(normR.getNormalizationMode());
            norm.setScale(normR.getScale());
            norm.setNormalizationLimits(normR.getNormLimitMin(), normR.getNormLimitMax());
        }

        if (plR.getInterpretation().getTraceInterpolator() != null) {
            BhpModelTraceInterpolator inter = (BhpModelTraceInterpolator)
            _pipeline.getInterpretation().getTraceInterpolator();

            //inter.setInterpolationType(plR.getTraceInterpolator().getInterpolationType());
            inter.setResamplingFactor(plR.getInterpretation().getTraceInterpolator().getResamplingFactor());
        }

        // model layer has no AGC

        if (plR.getTraceRasterizer() != null) {
            BhpModelTraceRasterizer rast = (BhpModelTraceRasterizer)_pipeline.getTraceRasterizer();
            TraceRasterizer rastR = plR.getTraceRasterizer();
            rast.setClippingValue(rastR.getClippingValue());
            if (isModel) rast.setPlotType(rastR.getPlotType());
            BhpViewerHelper.initColormapWithAnother(rast.getColorMap(), plR.getTraceRasterizer().getColorMap());
        }

        if(isModel) {
            BhpModelTraceInterpolator inter = (BhpModelTraceInterpolator)
            _pipeline.getInterpretation().getTraceInterpolator();
            BhpModelTraceRasterizer rast = (BhpModelTraceRasterizer)_pipeline.getTraceRasterizer();
            BhpModelTraceRasterizer mrastR = (BhpModelTraceRasterizer) (plR.getTraceRasterizer());
            rast.setStructuralInterpFlag(mrastR.getStructuralInterpFlag());
            float sdepth = mrastR.getStartDepth();
            float edepth = mrastR.getEndDepth();
            rast.setStartDepth(sdepth);
            sltr.setStartDepth(sdepth);
            inter.setStartDepth(sdepth);
            rast.setEndDepth(edepth);
            sltr.setEndDepth(edepth);
            inter.setEndDepth(edepth);
        }
    }

    // stub
    public int getMissingDataSelection() {
      return 0;
    }
}
