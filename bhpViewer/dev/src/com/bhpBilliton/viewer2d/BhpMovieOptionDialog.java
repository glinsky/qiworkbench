/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
//import javax.swing.event.*;
import javax.swing.border.*;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This dialog is for changing the movie
 *               settings of the viewer. <br><br>
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpMovieOptionDialog extends JDialog {
    private BhpMovieManager _mmanager;

    private JTextField _autoFileField = null;

    private JRadioButton _memoryButton = null;
    private JRadioButton _fileButton = null;

    private JRadioButton _allFrameButton = null;
    private JRadioButton _oneFrameButton = null;

    private JSlider _speedSlider = null;

    /**
     * Constructs a new dialog and sets up the GUI.
     */
    public BhpMovieOptionDialog(Frame parent, BhpMovieManager mmanager) {
        super(parent, "Movie Options", true);
        _mmanager = mmanager;
        this.getContentPane().add(buildOptionContentPanel(), BorderLayout.CENTER);
        this.getContentPane().add(buildOptionControlPanel(), BorderLayout.SOUTH);
        this.setSize(400, 500);
    }

    /**
     * Gets related information of movie settings
     * and fill the fields of GUI. <br>
     */
    public void initOptionSettings() {
        _allFrameButton.setSelected(_mmanager.isAllFrames());
        _autoFileField.setText(_mmanager.getMediaFileName());
        _memoryButton.setSelected(_mmanager.isSaveInMemory());
        _speedSlider.setValue(_mmanager.getFrameSpeed());
    }

    private void applyOptionSettings() {
        _mmanager.setAllFrames(_allFrameButton.isSelected());
        _mmanager.setSaveInMemory(_memoryButton.isSelected());
        if (!_memoryButton.isSelected())
            _mmanager.setMediaFileName(_autoFileField.getText());
        _mmanager.setFrameSpeed(_speedSlider.getValue());
    }

    private JPanel buildOptionContentPanel() {
        _allFrameButton = new JRadioButton("Capture image of the entire desktop", true);
        _oneFrameButton = new JRadioButton("Capture image of the selected frame", false);
        ButtonGroup frameGroup = new ButtonGroup();
        frameGroup.add(_allFrameButton);
        frameGroup.add(_oneFrameButton);
        JPanel framePanel = new JPanel(new GridLayout(2, 1));
        framePanel.setBorder(new TitledBorder("Capture"));
        framePanel.add(_allFrameButton);
        framePanel.add(_oneFrameButton);

        _memoryButton = new JRadioButton("Save captured images in memory", true);
        _fileButton = new JRadioButton("Save captured images as JPEG files", false);
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(_memoryButton);
        buttonGroup.add(_fileButton);
        _autoFileField = new JTextField("");
        JPanel autoPanel = new JPanel(new GridLayout(4, 1));
        autoPanel.setBorder(new TitledBorder("Save"));
        autoPanel.add(_memoryButton);
        autoPanel.add(_fileButton);
        autoPanel.add(new JLabel("File name template (absolute path):"));
        autoPanel.add(_autoFileField);

        _speedSlider = new JSlider(JSlider.HORIZONTAL, 100, 1000, 500);
        _speedSlider.setPaintLabels(true);
        _speedSlider.setPaintTicks(true);
        _speedSlider.setMajorTickSpacing(150);
        JPanel playPanel = new JPanel(new GridLayout(2, 1));
        playPanel.setBorder(new TitledBorder("Play"));
        playPanel.add(new JLabel("Interval between frames (in ms):"));
        playPanel.add(_speedSlider);

        Box box1 = new Box(BoxLayout.Y_AXIS);
        box1.add(Box.createVerticalStrut(20));
        box1.add(framePanel);
        box1.add(Box.createVerticalStrut(20));
        box1.add(autoPanel);
        box1.add(Box.createVerticalStrut(20));
        box1.add(playPanel);
        box1.add(Box.createVerticalGlue());

        Box box = new Box(BoxLayout.X_AXIS);
        box.add(Box.createHorizontalStrut(20));
        box.add(box1);
        box.add(Box.createHorizontalGlue());

        JPanel panel = new JPanel(new BorderLayout(20, 20));
        panel.setBorder(new EtchedBorder());
        panel.add(box);

        return panel;
    }

    private JPanel buildOptionControlPanel() {
        JButton okb = new JButton("Ok");
        okb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                applyOptionSettings();
                setVisible(false);
            }
        });
        JButton clb = new JButton("Cancel");
        clb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });
        JPanel panel = new JPanel(new FlowLayout());
        panel.add(okb);
        panel.add(clb);
        return panel;
    }
}
