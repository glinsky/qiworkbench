/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.util.*;

import com.gwsys.seismic.indexing.SegyBinaryEntry;
import com.gwsys.seismic.indexing.HeaderFieldEntry;
import com.gwsys.seismic.indexing.SegyHeaderFormat;
import com.gwsys.seismic.reader.SeismicFormat;
import com.gwsys.seismic.indexing.SeismicConstants;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  The special segy format is defined according to segy.h of SU.
 *               It is used by <code>{@link BhpSegyReader}</code>. <br>
 *
 * SegyBinaryEntry is used to describe the header.
 * Each of this field is constructed with 6 parameters. <br>
 * offset - the byte offset from the beginning of the header. <br>
 * type - the data type for this field. <br>
 * identifier - the constant used to identify this field. <br>
 * name - the name of this field. <br>
 * keyType - the key type of this field.
 *           Possible values are notKey, anyKey, pKey, and sKey <br>
 * isAnnStepTraceRelative - true if step is trace relative and
 *                          false if step is value relative <br>
 * Please refer to JSeismic documentation for more detail.
 * <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpSegyFormat implements SeismicFormat {
    private Vector _lineHeaderFormats = new Vector();
    private Vector _traceHeaderFormats = new Vector();

    public BhpSegyFormat() {
        // the header has three parts
        SegyHeaderFormat ebcdicHeaderFormat = new SegyHeaderFormat(SegyHeaderFormat.EBCDIC_HEADER, 3200);
        SegyHeaderFormat binaryHeaderFormat = new SegyHeaderFormat(SegyHeaderFormat.BINARY_HEADER, 400);
        SegyHeaderFormat traceHeaderFormat  = new SegyHeaderFormat(SegyHeaderFormat.BINARY_HEADER, 240);

        int idCount = 600;
        // indicates an int data format (32-bit signed integer)
        int intType = SeismicConstants.DATA_FORMAT_INT;
        // indicates an unsigned long data format (32-bit unsigned integer)
        int uintType = SeismicConstants.DATA_FORMAT_UNSIGNEDINT;
        // indicates a short data format (16-bit signed integer)
        int shortType = SeismicConstants.DATA_FORMAT_SHORT;
        // indicates an unsigned short data format (16-bit unsigned integer)
        int ushortType = SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT;
        // indicates a float data format (32-bit IEEE floating point)
        int floatType = SeismicConstants.DATA_FORMAT_FLOAT;
        // Indicates that this header field is a data header field or a trace header
        // field that should not be used for trace selection or annotation
        int notKey = HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED;
        // Indicates that this trace header field can be used to annotate or select
        // both the primary and the secondary key field
        int anyKey = HeaderFieldEntry.FIELD_TYPE_ANY;
        // Indicates that this trace header field can only be used to
        // select or annotate as a primary key field
        // int pKey = HeaderFieldEntry.FIELD_TYPE_PRIMARY;
        // Indicates that this trace header field can only be used to
        // select or annotate as a secondary key field
        // int sKey = HeaderFieldEntry.FIELD_TYPE_SECONDARY;

        binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            4, uintType, SeismicFormat.HEADER_LINE_NUMBER,
                            "Line Number", notKey, false));
        binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            12, ushortType, SeismicFormat.HEADER_TRACES_PER_ENSEMBLE,
                            "Trace per Ensembel", notKey, false));
        binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            16, ushortType, SeismicFormat.HEADER_SAMPLE_INTERVAL,
                            "Sample Interval", notKey, false));
        binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            20, ushortType, SeismicFormat.HEADER_SAMPLES_PER_TRACE,
                            "Samples per Trace", notKey, false));
        binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            24, ushortType, SeismicFormat.HEADER_DATA_FORMAT_CODE,
                            "Data Format", notKey, false));
        binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            26, ushortType, SeismicFormat.HEADER_TRACES_PER_CDP_ENSEMBLE,
                            "Trace per CDP", notKey, false));
        binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            54, ushortType, SeismicFormat.HEADER_MEASUREMENT_SYSTEM,
                            "Measurement System", notKey, false));
        binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            300, ushortType, SeismicFormat.HEADER_SEGY_REVISION_NUMBER,
                            "Segy Revision Number", notKey, false));
        binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            302, ushortType, SeismicFormat.HEADER_FIXED_LENGTH_FLAG,
                            "Fixed Length Flag", notKey, false));
        binaryHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            304, ushortType, SeismicFormat.HEADER_MAXIMUM_SAMPLES_PER_TRACE,
                            "Max Samples per Trace", notKey, false));


        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            0, intType, SeismicFormat.HEADER_TRACE_SEQUENCE_NUMBER,
                            "tracl", anyKey, true));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            4, intType, idCount++,
                            "tracr", anyKey, true));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            8, intType, SeismicFormat.HEADER_FIELD_RECORD,
                            "fldr", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            12, intType, SeismicFormat.HEADER_FIELD_TRACE,
                            "tracf", anyKey, true));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            16, intType, idCount++,
                            "ep", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            20, intType, SeismicFormat.HEADER_CDP_NUMBER,
                            "cdp", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            24, intType, SeismicFormat.HEADER_CDP_TRACE,
                            "cdpt", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            28, shortType, SeismicFormat.HEADER_TRACE_ID,
                            "trid", anyKey, false));

        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            30, shortType, idCount++, "nvs", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            32, shortType, idCount++, "nhs", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            34, shortType, idCount++, "duse", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            36, intType, idCount++, "offset", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            40, intType, idCount++, "gelev", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            44, intType, idCount++, "selev", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            48, intType, idCount++, "sdepth", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            52, intType, idCount++, "gdel", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            56, intType, idCount++, "sdel", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            60, intType, idCount++, "swdep", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            64, intType, idCount++, "gwdep", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            68, shortType, idCount++, "scalel", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            70, shortType, idCount++, "scalco", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            72, intType, idCount++, "sx", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            76, intType, idCount++, "sy", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            80, intType, idCount++, "gx", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            84, intType, idCount++, "gy", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            88, shortType, idCount++, "counit", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            90, shortType, idCount++, "wevel", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            92, shortType, idCount++, "swevel", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            94, shortType, idCount++, "sut", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            96, shortType, idCount++, "gut", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            98, shortType, idCount++, "sstat", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            100, shortType, idCount++, "gstat", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            102, shortType, idCount++, "tstat", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            104, shortType, idCount++, "laga", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            106, shortType, idCount++, "lagb", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            108, shortType, idCount++, "delrt", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            110, shortType, idCount++, "muts", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            112, shortType, idCount++, "mute", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            114, ushortType, idCount++, "ns", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            116, ushortType, idCount++, "dt", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            118, shortType, idCount++, "gain", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            120, shortType, idCount++, "igc", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            122, shortType, idCount++, "igi", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            124, shortType, idCount++, "corr", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            126, shortType, idCount++, "sfs", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            128, shortType, idCount++, "sfe", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            130, shortType, idCount++, "slen", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            132, shortType, idCount++, "stype", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            134, shortType, idCount++, "stas", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            136, shortType, idCount++, "stae", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            138, shortType, idCount++, "tatyp", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            140, shortType, idCount++, "afilf", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            142, shortType, idCount++, "afils", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            144, shortType, idCount++, "nofilf", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            146, shortType, idCount++, "nofils", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            148, shortType, idCount++, "lcf", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            150, shortType, idCount++, "hcf", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            152, shortType, idCount++, "lcs", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            154, shortType, idCount++, "hcs", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            156, shortType, idCount++, "year", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            158, shortType, idCount++, "day", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            160, shortType, idCount++, "hour", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            162, shortType, idCount++, "minute", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            164, shortType, idCount++, "sec", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            166, shortType, idCount++, "timbas", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            168, shortType, idCount++, "trwf", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            170, shortType, idCount++, "grnors", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            172, shortType, idCount++, "grnofr", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            174, shortType, idCount++, "grnlof", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            176, shortType, idCount++, "gaps", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            178, shortType, idCount++, "otrav", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            180, floatType, idCount++, "d1", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            184, floatType, idCount++, "f1", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            188, floatType, idCount++, "d2", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            192, floatType, idCount++, "f2", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            196, floatType, idCount++, "ungpow", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            200, floatType, idCount++, "unscale", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            204, intType, idCount++, "ntr", anyKey, false));
        traceHeaderFormat.addFieldDescription(new SegyBinaryEntry(
                            208, shortType, idCount++, "mark", anyKey, false));

        _lineHeaderFormats.addElement(ebcdicHeaderFormat);
        _lineHeaderFormats.addElement(binaryHeaderFormat);
        _traceHeaderFormats.addElement(traceHeaderFormat);
    }
    public Vector getDataHeaderFormats() {
        return _lineHeaderFormats;
    }
    public int getDataHeadersSize() {
        return _lineHeaderFormats.size();
    }
    public Vector getTraceHeaderFormats() {
        return _traceHeaderFormats;
    }
    public int getTraceHeadersSize() {
        return _traceHeaderFormats.size();
    }

    /**
     * Static method to find out the identifier of the named header.
     * @param name name of the requested header.
     * @param format the format of the header.
     * @return an Integer for the header identifier. If the named header
     *         is not found in the format, null will be returned.
     */
    public static Integer getFieldForName(String name, SegyHeaderFormat format) {
        Vector fields = format.getHeaderFields();
        HeaderFieldEntry headerField;
        for (int i=0; i<fields.size(); i++) {
            headerField = (HeaderFieldEntry) (fields.elementAt(i));
            if (headerField.getName().equals(name))
                {
            	return new Integer(headerField.getFieldId());
                }
        }
        return null;
    }
}
