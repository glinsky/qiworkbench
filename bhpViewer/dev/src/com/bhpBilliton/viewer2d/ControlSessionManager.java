/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d;

import java.util.ArrayList;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
//import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.ControlConstants;
import java.util.logging.Logger;

/**
 * Manage a control session, i.e., when another qiComponent is specifying
 * the actions to be taken instead of the user.
 */
public class ControlSessionManager {
    private static final Logger logger = Logger.getLogger(ControlSessionManager.class.toString());
    /** Viewer Agent's messaging manager. */
    MessagingManager messagingMgr;
    // GUI component
    private BhpViewerBase viewer;
    /** Component descriptor of the bhpViewer controlling */
    ComponentDescriptor bhpViewerDesc;

    /**
     * Constructor. Initialize the finite state machine.
     * @param messagingMgr Viewer Agent's messaging manager.
     * @param viewer bnpViewer GUI instance.
     */
    public ControlSessionManager(BhpViewerBase viewer, MessagingManager messagingMgr) {
        this.messagingMgr = messagingMgr;
        this.viewer = viewer;
    }

    /**
     * Process the CONTROL_COMPONENT request and return the response. The request
     * may contain one or more (action, params) pairs. When there are multiple
     * actions in a request, none of the intermediate actions return a response.
     * Only the last action returns a response; normal if no error while performing
     * any of the actions; otherwise, an abormal response containing the error(s).
     * @param req The request message.
     */
    public void processReq(IQiWorkbenchMsg req) {
        logger.info("bhpViewer::processReq: request="+req);
        ArrayList<String> nameValuePairs = new ArrayList<String>();

        ArrayList actions = (ArrayList)req.getContent();

        boolean multiactions = actions.size()>2 ? true : false;
        for (int k=0; k<actions.size(); k += 2) {
            String action = (String)actions.get(k);
            ArrayList actionParams = (ArrayList)actions.get(k+1);
            nameValuePairs = parseNameValuePairs(actionParams);

            //Note: Cannot occur in a multiaction command.
            if (action.indexOf(ControlConstants.CREATE_WINDOW_ACTION) != -1) {
                String winID = "", type = "", format="";
                //get the action parameters
                for (int j=0; j<nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j+1);
                    if (name.equals("winID")) winID = value;
                    else if (name.equals("type")) type = value;
                    else if (name.equals("format")) format = value;
                }

                //return the ID of the opened XSection window
                messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, genWindowID());
                return;
            } else

            //Note: Cannot occur in a multiaction command.
            if (action.indexOf(ControlConstants.LOAD_DATA_ACTION) != -1) {
                String winID = "", format = "", file="";
                //get the action parameters
                for (int j=0; j<nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j+1);
                    if (name.equals("winID")) winID = value;
                    else if (name.equals("format")) format = value;
                    else if (name.equals("file")) file = value;
                }

                //return the ID of the Open dialog for the input cube
                messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, genWindowID());
                return;
            } else

            //Note: Cannot occur in a multiaction command.
            if (action.indexOf(ControlConstants.GET_PROP_ACTION) != -1) {
                String winID = "", field = "";
                //get the action parameters
                for (int j=0; j<nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j+1);
                    if (name.equals("winID")) winID = value;
                    else if (name.equals("field")) field = value;
                }

                //execute action
                String fieldValue = "3587 -3425 [2]";

                //return the value of the specified field
                messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, fieldValue);
                return;
            } else

            if (action.indexOf(ControlConstants.SET_PROP_ACTION) != -1) {
                String winID = "", field = "", value="";
                //get the action parameters
                for (int j=0; j<nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String nvalue = nameValuePairs.get(j+1);
                    if (name.equals("winID")) winID = nvalue;
                    else if (name.equals("field")) field = nvalue;
                    else if (name.equals("value")) value = nvalue;
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
            } else

            if (action.indexOf(ControlConstants.SELECT_PROP_ACTION) != -1) {
                String winID = "", field = "", value="";
                //get the action parameters
                for (int j=0; j<nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String nvalue = nameValuePairs.get(j+1);
                    if (name.equals("winID")) winID = nvalue;
                    else if (name.equals("field")) field = nvalue;
                    else if (name.equals("value")) value = nvalue;
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
            } else

            if (action.indexOf(ControlConstants.OK_ACTION) != -1) {
                String winID = "";
                //get the action parameters
                for (int j=0; j<nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j+1);
                    if (name.equals("winID")) winID = value;
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
            } else

            if (action.indexOf(ControlConstants.RESIZE_WINDOW_ACTION) != -1) {
                String winID = "", width = "", height = "";
                //get the action parameters
                for (int j=0; j<nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j+1);
                    if (name.equals("winID")) winID = value;
                    else if (name.equals("width")) width = value;
                    else if (name.equals("height")) height = value;
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
            } else

            if (action.indexOf(ControlConstants.VIEW_ALL_ACTION) != -1) {
                String winID = "";
                //get the action parameters
                for (int j=0; j<nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j+1);
                    if (name.equals("winID")) winID = value;
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
            } else

            //Note: Cannot occur in a multiaction command.
            if (action.indexOf(ControlConstants.OPEN_DIALOG_ACTION) != -1) {
                //layer is an optional parameter. It refers to a layer within
                //the specified window.
                String winID = "", type = "", layer = "";
                //get the action parameters
                for (int j=0; j<nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j+1);
                    if (name.equals("winID")) winID = value;
                    else if (name.equals("type")) type = value;
                    else if (name.equals("layer")) layer = value;
                }

                //return the ID of the Layer Properties dialog for the input cube layer in the XSection window
                messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, genWindowID());
                return;
            } else

            if (action.indexOf(ControlConstants.ADD_LAYER_ACTION) != -1) {
                String winID = "", format = "", file = "";
                //get the action parameters
                for (int j=0; j<nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j+1);
                    if (name.equals("winID")) winID = value;
                    else if (name.equals("format")) format = value;
                    else if (name.equals("file")) file = value;
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
            } else

            if (action.indexOf(ControlConstants.POSITION_WINDOW_ACTION) != -1) {
                String winID = "", x = "", y = "";
                //get the action parameters
                for (int j=0; j<nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j+1);
                    if (name.equals("winID")) winID = value;
                    else if (name.equals("x")) x = value;
                    else if (name.equals("y")) y = value;
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
            }
        }
        //The last action in a multiaction request sends back the response
        if (multiactions) {
            //TODO: Handle any action error(s)
            messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
        }
    }

    static long lastControlSessionID = 0;
    /**
     * Generate a unique control session ID (sessionID). The sessionID is the current time
     * and date. However, if the new sessionID is less than or equal to the last one,
     * the last one is incremented by 1 to make it unique.
     *
     * @return Unique control session ID
     */
    static public String genControlSessionID() {
        long sessionID = System.currentTimeMillis();
        if (sessionID <= lastControlSessionID) sessionID = ++lastControlSessionID;
        lastControlSessionID = sessionID;
        return Long.toString(sessionID);
    }

    static long lastWinID = 0;
    /**
     * Generate a unique (GUI) window ID (winID). The winID is the current time
     * and date. However, if the new winID is less than or equal to the last one,
     * the last one is incremented by 1 to make it unique.
     *
     * @return Unique window ID
     */
    static public String genWindowID() {
        long winID = System.currentTimeMillis();
        if (winID <= lastWinID) winID = ++lastWinID;
        lastWinID = winID;
        return Long.toString(winID);
    }

    /**
     * Parse the (name, value) pairs into a linear list.
     * @param actionParams List of name=value action parameters
     */
    private ArrayList<String> parseNameValuePairs(ArrayList<String> actionParams) {
        ArrayList<String> nameValuePairs = new ArrayList<String>();
        for (int i=0; i<actionParams.size(); i++ ) {
            String nameValue = actionParams.get(i);
            int idx = nameValue.indexOf("=");
            nameValuePairs.add(nameValue.substring(0, idx));
            nameValuePairs.add(nameValue.substring(idx+1));
        }
        return nameValuePairs;
    }
}
