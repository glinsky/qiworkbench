/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.util.*;
import java.awt.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.TitledBorder;

import org.w3c.dom.*;


import com.bhpBilliton.viewer2d.data.GeneralDataSource;
import com.bhpBilliton.viewer2d.dataAdapter.DataSourceFactory;
// QIW Disable references to log data
// import com.bhpBilliton.viewer2d.dataAdapter.las.*;
import com.bhpBilliton.viewer2d.ui.util.ElementAttributeReader;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.gw2d.view.*;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  Internal frame for data display.
 *               It has split pane displaying a
 *               <code>{@link BhpLayerList}</code> on the left
 *               and layers on the right in a
 *               <code>{@link AbstractPlot}</code>.
 *               A user can select on the list, the first selected layer
 *               is the active layer. Most actions, such as zooming, mouse
 *               event will be targeted at this active layer. <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpWindow extends JInternalFrame {
  //private static int _layerCounter = 0;

  private int _windowType;

  private JPanel _listPanel;

  private JPanel _mainPanel;

  private JSplitPane _splitPane;

  private BhpWindowStatusBar _statusbar;

  private AbstractPlot _plot;

  private BhpEventGraphPlot _eventGraphPlot;

  private BoxContainer _plotContainer;

  private BhpLayerList _list;

  private boolean _mouseTracking;

  private boolean _showEventGraphPlot;

  private Vector _loadingLayers;

  private boolean _oldFormatXML;

  private BhpViewerBase _viewer;

  public BhpViewerBase getViewer() {
      return _viewer;
  }

  /**
   * Constructs a new instance and sets up the GUI..
   * @param name name of the window.
   * @param type type of the window, it is either WINDOW_TYPE_XSECTION
   *        or WINDOW_TYPE_MAPVIEWER.
   * @param hs horizontal scale of the plot
   * @param vs vertical scale of the plot
   */
  public BhpWindow(String name, int type, double hs, double vs, boolean showPointer, BhpViewerBase viewer) {
    super(name, true, true, true, true);
    _mouseTracking = false;
    _showEventGraphPlot = false;
    _windowType = type;
    _oldFormatXML = false;
    // save BhpViewerBase
    _viewer = viewer;
    if (_windowType == BhpViewer.WINDOW_TYPE_MAPVIEWER) {
      this.setTitle("Map Window");
    } else {
      this.setTitle("XSection Window");
    }
    _loadingLayers = new Vector();
    setupMainPanel(hs, vs, showPointer);
    setupListPanel();
    setupTopLevelGUI();

    _list.addListSelectionListener(_plot.getMouseCenter());
    this.addInternalFrameListener(new MyInternalFrameAdapter());
  }

  /**
   * Initiates the viewer with an XML node. <br>
   * This is used when a saved XML configuration file is opened to
   * restore a working session.
   */
  public void initWithXML(Node node) {
    ElementAttributeReader emt = new ElementAttributeReader(node);

    int axisAssociateLayer = 0;
    int layerCounter = 0;
    int viewPositionX = -999;
    int viewPositionY = -999;
    int plotSynFlag = 0;
    int dividerLocation = -999;     //FIXME globalise default values
    axisAssociateLayer = emt.numberAttribute("axisAssociate", axisAssociateLayer);
    layerCounter = emt.numberAttribute("layerCounter", layerCounter);
    viewPositionX = emt.numberAttribute("viewPositionX", viewPositionX);
    viewPositionY = emt.numberAttribute("viewPositionY", viewPositionY);
    plotSynFlag = emt.numberAttribute("plotSynFlag", plotSynFlag);
    dividerLocation = emt.numberAttribute("dividerLocation", dividerLocation);

    boolean mouseTracking = emt.getBoolean("mouseTracking");
    boolean plotTalkEnabled = emt.getBoolean("enablePlotTalk");
    boolean eventGraphPlotVisible = emt.getBoolean("showHorizonGraph");
    boolean lockAspectRatio = emt.getBoolean("plotLockRatio");
    boolean syncHScroll = emt.getBoolean("syncHScroll");
    boolean syncVScroll = emt.getBoolean("syncVScroll");

    _oldFormatXML = true;
    if (dividerLocation != -999) {
      _plot.setSynFlag(plotSynFlag);
      _oldFormatXML = false;
    } else {
      dividerLocation = 50;
    }

    _plot.setEnablePlotTalk(plotTalkEnabled);
    _plot.setLockAspectRatio(lockAspectRatio);
    _plot.setSynScrollHorizontal(syncHScroll);
    _plot.setSynScrollVertical(syncVScroll);
    setShowEventGraphPlot(eventGraphPlotVisible);
    setMouseTracking(mouseTracking);

    //removing unsafe assignment to static var from instance member - Woody Folsom 1/9/2008
    //_layerCounter = layerCounter;

    _plot.setAxisAssociateLayer(axisAssociateLayer, false);
    _list.setAxisAssociateLayer(axisAssociateLayer);

    if (dividerLocation > 0) {
      _splitPane.setDividerLocation(dividerLocation);
    }

    NodeList children = node.getChildNodes();

    int nl = BhpViewerHelper.getBhpLayerCount(children);
    if (viewPositionX != -999 && viewPositionY != -999 && nl > 0) {
      _plot.setXmlRelatedInfo(nl, viewPositionX, viewPositionY);
    }

    //Element echild;
    for (int i = 0; i < children.getLength(); i++) {
      Node child = children.item(i);
      if (child.getNodeType() == Node.ELEMENT_NODE) {
        initLayerFromNode(child);
      }
    }
  }

  public BhpViewerBase getBhpViewerBase(){
      return _viewer;
  }
  /**
   * Initiates the viewer with an XML node. <br>
   * This is used when a saved XML configuration file is opened to
   * restore a working session.
   */
  public void restoreState(Node node) {
    ElementAttributeReader emt = new ElementAttributeReader(node);

    int axisAssociateLayer = 0;
    int layerCounter = 0;
    int viewPositionX = -999;
    int viewPositionY = -999;
    int plotSynFlag = 0;
    int dividerLocation = -999;     //FIXME globalise default values
    axisAssociateLayer = emt.numberAttribute("axisAssociate", axisAssociateLayer);
    layerCounter = emt.numberAttribute("layerCounter", layerCounter);
    viewPositionX = emt.numberAttribute("viewPositionX", viewPositionX);
    viewPositionY = emt.numberAttribute("viewPositionY", viewPositionY);
    plotSynFlag = emt.numberAttribute("plotSynFlag", plotSynFlag);
    dividerLocation = emt.numberAttribute("dividerLocation", dividerLocation);

    boolean mouseTracking = emt.getBoolean("mouseTracking");
    boolean plotTalkEnabled = emt.getBoolean("enablePlotTalk");
    boolean eventGraphPlotVisible = emt.getBoolean("showHorizonGraph");
    boolean lockAspectRatio = emt.getBoolean("plotLockRatio");
    boolean syncHScroll = emt.getBoolean("syncHScroll");
    boolean syncVScroll = emt.getBoolean("syncVScroll");

    _oldFormatXML = true;
    if (dividerLocation != -999) {
      _plot.setSynFlag(plotSynFlag);
      _oldFormatXML = false;
    } else {
      dividerLocation = 50;
    }

    _plot.setEnablePlotTalk(plotTalkEnabled);
    _plot.setLockAspectRatio(lockAspectRatio);
    _plot.setSynScrollHorizontal(syncHScroll);
    _plot.setSynScrollVertical(syncVScroll);
    setShowEventGraphPlot(eventGraphPlotVisible);
    setMouseTracking(mouseTracking);

    //removing unsafe assignment to static var from instance member - Woody Folsom 1/9/2008
    //_layerCounter = layerCounter;

    _plot.setAxisAssociateLayer(axisAssociateLayer, false);
    _list.setAxisAssociateLayer(axisAssociateLayer);

    if (dividerLocation > 0) {
      _splitPane.setDividerLocation(dividerLocation);
    }

    NodeList children = node.getChildNodes();

    int nl = BhpViewerHelper.getBhpLayerCount(children);
    if (viewPositionX != -999 && viewPositionY != -999 && nl > 0) {
      _plot.setXmlRelatedInfo(nl, viewPositionX, viewPositionY);
    }

    //Element echild;
    for (int i = 0; i < children.getLength(); i++) {
      Node child = children.item(i);
      if (child.getNodeType() == Node.ELEMENT_NODE) {
        initLayerFromNode(child);
      }
    }
  }

  private void initLayerFromNode(Node child) {
    String nodeName = child.getNodeName();
    if (nodeName.equals("Annotation")) {
      BhpViewerHelper.initAnnotationWithXML(_viewer, child, _plot);
    } else if (nodeName.equals("HorizonGraph")) {
      initHorizonGraphPlotWithXML(child);
    } else {
      makeBhpLayerWithXML(child);
    }
  }

  /**
   * Converts the window into an XML string. <br>
   * This is used when save the working session into an XML file.
   * Please refer to BhpViewer.dtd for the detail of the format.
   * This method in turn asks all <code>{@link BhpWindow}</code>
   * to streamlize themselves.
   * @return an XML string for the object.
   */
  public String toXMLString() {
    StringBuffer content = new StringBuffer();
    content.append("    " + "<BhpWindow ");
    content.append("title"+"=\"" + this.getTitle() + "\" ");
    content.append("width"+"=\"" + this.getWidth() + "\" ");
    content.append("height"+"=\"" + this.getHeight() + "\" ");
    content.append("positionX"+"=\"" + this.getLocation().x + "\" ");
    content.append("positionY"+"=\"" + this.getLocation().y + "\" ");
    content.append("layerCounter"+"=\"" + _plot.getMainView().getNumberOfBhpLayers() + "\" ");
    content.append("axisAssociate"+"=\"" + _plot.getAxisAssociateLayer() + "\" ");
    content.append("numberOfLayers"+"=\"" + _plot.getMainView().getNumberOfBhpLayers() + "\" ");
    content.append("viewPositionX"+"=\"" + _plot.getViewPosition().x + "\" ");
    content.append("viewPositionY"+"=\"" + _plot.getViewPosition().y + "\" ");
    content.append("dividerLocation"+"=\"" + _splitPane.getDividerLocation() + "\" ");
    content.append("showHorizonGraph"+"=\"" + this._showEventGraphPlot + "\" ");
    content.append("mouseTracking"+"=\"" + this._mouseTracking + "\" ");
    content.append("enablePlotTalk"+"=\"" + _plot.getEnablePlotTalk() + "\" ");
    content.append("plotSynFlag"+"=\"" + _plot.getSynFlag() + "\" ");
    content.append("plotLockRatio"+"=\"" + _plot.getLockAspectRatio() + "\" ");
    content.append("plotHScale"+"=\"" + _plot.getHorizontalScale() + "\" ");
    content.append("plotVScale"+"=\"" + _plot.getVerticalScale() + "\" ");
    content.append("syncHScroll"+"=\"" + _plot.getSynScrollHorizontal() + "\" ");
    content.append("syncVScroll"+"=\"" + _plot.getSynScrollVertical() + "\" ");
    content.append("windowType"+"=\"" + _windowType + "\">\n");

    content.append(_plot.toXMLString());

    if (_eventGraphPlot != null && _showEventGraphPlot) {
      content.append(_eventGraphPlot.toXMLString());
    }

    content.append("    " + "</BhpWindow>\n");
    return content.toString();
  }

  public boolean isShowEventGraphPlot() {
    return _showEventGraphPlot;
  }

  public void setShowEventGraphPlot(boolean b) {
    if (_showEventGraphPlot == b) {
      return;
    }
    _showEventGraphPlot = b;
    if (_showEventGraphPlot) {
      _plotContainer.add(_eventGraphPlot, 0);
    } else {
      _plotContainer.remove(_eventGraphPlot);
    }
    this.validate();
  }

  /**
   * Finds out if mouse tracking is enabled. <br>
   * If enabled, a cursor is displayed and the annotation
   * reading of the cursor position will be displayed in
   * <code>{@link BhpWindowStatusBar}</code>.
   */
  public boolean isMouseTracking() {
    return _mouseTracking;
  }

  /**
   * Sets the status of mouse tracking. <br>
   * If enabled, a cursor is displayed and the annotation
   * reading of the cursor position will be displayed in
   * <code>{@link BhpWindowStatusBar}</code>. <br>
   * This method should be called after calling BhpPlot.setSynFlag,
   * since it may overwrite the settings made by this method.
   * @param v true to enable mouse tracking, false to disable it.
   */
  public void setMouseTracking(boolean v) {
    if (_mouseTracking == v) {
      return;
    }
    _mouseTracking = v;
    if (_mouseTracking) {
      _mainPanel.add(_statusbar, BorderLayout.SOUTH);
    } else {
      _mainPanel.remove(_statusbar);
    }
    _mainPanel.validate();
    if (_mouseTracking) {
      _plot.setSynFlag(_plot.getSynFlag() | BhpViewer.CURSOR_FLAG);
    } else {
      _plot.setSynFlag(_plot.getSynFlag() & (~(BhpViewer.CURSOR_FLAG)));
    }
  }

  /**
   * Gets the type of the window.
   *
   * @return the type of the window, it is either WINDOW_TYPE_XSECTION or
   *         WINDOW_TYPE_MAPVIEWER.
   */
  public int getWindowType() {
    return _windowType;
  }

  /**
   * Gets the <code>{@link AbstractPlot}</code>
   * that is shown in the right part of the window.
   */
  public AbstractPlot getBhpPlot() {
    return _plot;
  }

  /**
   * Gets the <code>{@link BhpEventGraphPlot}</code>
   * that is shown in the right part of the window at the top.
   */
  public BhpEventGraphPlot getBhpEventGraphPlot() {
    return _eventGraphPlot;
  }

  /**
   * Gets the plot container that contains <code>{@link AbstractPlot}</code>
   * and the possible <code>{@link BhpEventGraphPlot}</code>.
   */
  public BoxContainer getPlotContainer() {
    return _plotContainer;
  }

  /**
   * Gets the <code>{@link BhpLayerList}</code>
   * that is shown in the left part of the window.
   */
  public BhpLayerList getBhpLayerList() {
    return _list;
  }

  /**
   * Handles a <code>{@link BhpLayerEvent}</code>. <br>
   * This method informs both <code>{@link AbstractPlot}</code>
   * and <code>{@link BhpLayerList}</code> about the event.
   * @param e the current event.
   */
  public boolean receiveBhpLayerEvent(BhpLayerEvent e) {
    // we might need to do something on the list for some event
    boolean updated=_plot.receiveBhpLayerEvent(e);
    _list.receiveBhpLayerEvent(e);
    return updated;
  }

  /**
   * Sets the layer that based on which the annotation will be drawn.
   * This method informs both <code>{@link AbstractPlot}</code>
   * and <code>{@link BhpLayerList}</code> about it.
   * @param id the identification number of the layer.
   */
  public void setAxisAssociateLayer(int id) {
    // need to do something to the list as well
    //if (id < 0) _statusbar.setEnableStatusbar(false);
    //else _statusbar.setEnableStatusbar(true);
    _plot.setAxisAssociateLayer(id, false);
    _list.setAxisAssociateLayer(id);
    //_plot.invalidate();
    _plot.validate();
  }

  /**
   * Deletes the layers.
   * This deletes the layers from both <code>{@link AbstractPlot}</code>
   * and <code>{@link BhpLayerList}</code>.
   * @param layer the layers that will be deleted.
   */
  public void deleteBhpLayer(BhpLayer[] layer) {
    _plot.removeBhpLayer(layer);
    _list.removeBhpLayer(layer);
    if (_eventGraphPlot != null) {
      _eventGraphPlot.removeBhpLayer(layer);
    }
    for (int i = 0; i < layer.length; i++) {
      layer[i].cleanupLayer();
      layer[i] = null;
    }
  }

  /**
   * Creates a new layer for cross-section display. <br>
   * This method creates the layer and makes call to set up the reader
   * for the layer. However, the layer is not inserted until later
   * when the reader has enough information.
   * @param viewer the viewer that this is in.
   * @param dstype the name of the data source.
   * @param dataset name of the selected data set.
   * @param pathlist path of the selected data set.
   * @param property the name of the selected property. <br>
   *        In the case of seismic data, it is null.
   *        In the case of model or event data, it is the selected
   *        property name or event name.
   * @param propertySync the boolean flag for property synchronization.
   * @param parameter a table model stores the user data selection.
   *        In the case of bhpio, it can be used to construct the
   *        query string used to run bhpio.
   * @param ref the reference layer. <br>
   *        The new layer will inherit from this reference layer things
   *        like scales, pipeline settings, display options, and etc.
   * @param dataType data type, using constants
   *        defined in both <code>{@link BhpLayer}</code>.<br>
   * @param refShape the reference event shape. <br>
   *        It is only useful for <code>{@link BhpLayer}</code>.
   *        In all the other cases, it is null and will be ignored.
   *        The new event shape will inherit its graphic settings
   *        from the reference shape.
   */
  public void createXVLayer(BhpViewerBase viewer, String dstype, String dataset, String pathlist, String property,
                            boolean propertySync, BhpSeismicTableModel parameter, BhpLayer ref, int dataType, BhpEventShape refShape) {
    if (!checkWindowType(BhpViewer.WINDOW_TYPE_XSECTION)) {
        ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                            "BhpWindow.createXVLayer failed : non-matching window type.");
      return;
    }

    int id = viewer.getNextLayerID();
    BhpLayer layer = null;
    if (dataType == BhpViewer.BHP_DATA_TYPE_SEISMIC) {
      layer = new BhpSeismicLayer(viewer, this, id, dstype, dataset, pathlist, parameter, ref);
    } else if (dataType == BhpViewer.BHP_DATA_TYPE_MODEL) {
      layer = new BhpModelLayer(viewer, this, id, dstype, dataset, pathlist, property, propertySync, parameter, ref);
    } else if (dataType == BhpViewer.BHP_DATA_TYPE_EVENT) {
      layer = new BhpEventLayer(viewer, this, id, dstype, dataset, pathlist, property, propertySync, parameter, ref, refShape);
    }

    if (layer != null) {
      _loadingLayers.add(layer);
      layer.setupSeismicReader();
    } else {
        ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                            "BhpWindow.createXVLayer failed : null layer.");
    }
  }

  /**
   * Creates a new layer for map display. <br>
   * This method creates the layer and makes call to set up the reader
   * for the layer. However, the layer is not inserted until later
   * when the reader has enough information.
   * @param viewer the viewer that this is in.
   * @param dstype the name of the data source.
   * @param dataset name of the selected data set.
   * @param pathlist path of the selected data set.
   * @param property the name of the selected property. <br>
   *        In the case of seismic data, it is null.
   *        In the case of model or event data, it is the selected
   *        property name or event name.
   * @param propertySync the boolean flag for property synchronization.
   * @param layerSync the boolean flag for model layer number synchronization.
   * @param parameter a table model stores the user data selection.
   *        In the case of bhpio, it can be used to construct the
   *        query string used to run bhpio.
   * @param ref the reference layer. <br>
   *        The new layer will inherit from this reference layer things
   *        like scales, pipeline settings, display options, and etc.
   */
  public BhpMapLayer createBhpMapLayer(BhpViewerBase viewer, String dstype, String dataset, String pathlist, String property,
                                       boolean propertySync, boolean layerSync, BhpSeismicTableModel parameter, BhpLayer ref, int type) {
    if (!checkWindowType(BhpViewer.WINDOW_TYPE_MAPVIEWER)) {
      return null;
    }
    int id = viewer.getNextLayerID();
    String lname = dataset;
    if (type == BhpViewer.BHP_DATA_TYPE_EVENT || type == BhpViewer.BHP_DATA_TYPE_MODEL) {
      lname = property;
    }
    BhpMapLayer layer = new BhpMapLayer(viewer, this, id, dstype, dataset, lname, pathlist, property, propertySync, layerSync,
                                        parameter, ref, type);

    _loadingLayers.add(layer);
    layer.setupSeismicReader();

    return layer;
  }

  /**
   * Informs that loading a specific layer is not successful. <br>
   * Failed layer will not be removed.
   * @param layer the layer that fails loading.
   */
  public void loadingLayerError(BhpLayer layer) {
    _loadingLayers.remove(layer);
  }

  /**
   * Adds a layer to both <code>{@link AbstractPlot}</code>
   * and <code>{@link BhpLayerList}</code>. <br>
   * A vector is used to control the adding order, to
   * make sure the layers are added in acoordance with
   * the order that they are created. This adding
   * order affects the displaying order as which is
   * on top, and which is at the botoom.
   * This method is called when the specific layer is
   * ready to be added. But whether the layer will be
   * added right away is also affected the the state
   * of the other layers in the vector.
   * @param layer that layer that is ready to be added.
   */
  public void addBhpLayer(BhpLayer layer) {
      if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
          System.out.println("BhpWindow.addBhpLayer " + layer.getIdNumber() + " vector of " +
                                 _loadingLayers.size());
    layer.setIsReady(true);
    int windowTypeNeeded = BhpViewer.WINDOW_TYPE_MAPVIEWER;
    boolean adjustVerticalScale = false;
    if (layer instanceof BhpSeismicLayer ||
        layer instanceof BhpModelLayer ||
        layer instanceof BhpEventLayer) {
        // QIW Disable references to log data
        /*layer instanceof BhpEventLayer ||
        layer instanceof LogTrackLayer) {*/
      windowTypeNeeded = BhpViewer.WINDOW_TYPE_XSECTION;
    }
    if (!checkWindowType(windowTypeNeeded)) {
      _loadingLayers.remove(layer);
      return;
    }

    if (_loadingLayers.size() == 0) {
      // empty vector, not a threaded layer, add directly
      _plot.addBhpLayer(layer);
      _list.addBhpLayer(layer);
      if (_eventGraphPlot != null) {
        _eventGraphPlot.addBhpLayer(layer);
      }
    }

    if ((layer.getIdNumber() == _plot.getAxisAssociateLayer()) && (layer instanceof BhpSeismicLayer)) {
      adjustVerticalScale = _oldFormatXML;
    }

    boolean allReady = true;
    for (int i = 0; i < _loadingLayers.size(); i++) {
      BhpLayer bhpLayer = ((BhpLayer) (_loadingLayers.get(i)));
      if (!bhpLayer.getIsReady()) {
        allReady = false;
      } else if (adjustVerticalScale) {
        bhpLayer.updateLayer();
      }
    }
    if (allReady) {
      BhpLayer tmpLayer = null;
      for (int j = 0; j < _loadingLayers.size(); j++) {
        tmpLayer = (BhpLayer) (_loadingLayers.get(j));
        _plot.addBhpLayer(tmpLayer);
        _list.addBhpLayer(tmpLayer);
        if (_eventGraphPlot != null) {
          _eventGraphPlot.addBhpLayer(tmpLayer);
        }
      }
      _loadingLayers.removeAllElements();
    }
  }

  /**
   * Stops the process of all loading layers. <br>
   */
  public void stopLoadingLayer() {
    BhpLayer theLayer;
    if (!_loadingLayers.isEmpty()) {
      for (int i = 0; i < _loadingLayers.size(); i++) {
        theLayer = (BhpLayer) (_loadingLayers.elementAt(i));
        theLayer.stopLoading();
      }
      _loadingLayers.removeAllElements();
    }
    ListModel lmodel = _list.getModel();
    for (int i = 0; i < lmodel.getSize(); i++) {
      theLayer = _plot.getBhpLayer(_list.getLayerIdAt(i));
      theLayer.stopLoading();
    }
  }

  /**
   * Retrieves a laye of the window. <br>
   * This layer is one of the following: <br>
   * 2. It is currently selected according to
   * the selection made in <code>{@link BhpLayerList}</code>. <br>
   * 1. It is the layer that annotation is associated with. <br>
   * 3. The first component in <code>{@link BhpLayerList}</code>. <br>
   */
  public BhpLayer getFeaturedLayer() {
    int id = _plot.getAxisAssociateLayer();//_list.getSelectedLayerId();
    if (id != -1) {
      BhpLayer tmpLayer = _plot.getBhpLayer(id);
      if (tmpLayer instanceof BhpEventLayer) {
        if (!tmpLayer.getDataSource().equals(GeneralDataSource.DATA_SOURCE_BHPSU)) {
          // dependent event layer, apply this to the underlying seismic layer
          id = ((BhpEventLayer) tmpLayer).getRefSeismicLayerId();
        }
      }
    }
    if (id == -1) {
      id = _list.getSelectedLayerId();//_plot.getAxisAssociateLayer();
    }
    if (id == -1) {
      id = _list.getLayerIdAt(0);
    }

    if (id == -1) {
      return null;
    }
    return _plot.getBhpLayer(id);
  }

  /**
   * Retrieves the laye that is currently selected according to
   * the selection made in <code>{@link BhpLayerList}</code>. <br>
   * This is the active layer that most actions are targeted to.
   * If multiple layers are selected, only the first one is returned.
   */
  public BhpLayer getSelectedLayer() {
    int id = _list.getSelectedLayerId();
    BhpLayer layer = _plot.getBhpLayer(id);
    return layer;
  }

  /**
   * Retrieves the layes that are currently selected according to
   * the selection made in <code>{@link BhpLayerList}</code>. <br>
   * All the selected layers will be returned. Some of the actions,
   * such as delete layer, change layer visibility are targeted to
   * multiple layers.
   */
  public BhpLayer[] getSelectedLayers() {
    int ids[] = _list.getSelectedLayerIds();
    BhpLayer[] layers = new BhpLayer[ids.length];
    for (int i = 0; i < ids.length; i++) {
      layers[i] = (_plot.getBhpLayer(ids[i]));
    }
    return layers;
  }

  private void initHorizonGraphPlotWithXML(Node node) {
    if (_eventGraphPlot == null) {
      return;
    }
    ElementAttributeReader emt = new ElementAttributeReader(node);

    double minValue = 0;
    double maxValue = 0;
    double majorStep = 0;
    double minorStep = 0;

    _eventGraphPlot.setShowGrid(emt.getBoolean("showGrid"));
    minValue = emt.numberAttribute("minValue", minValue);
    maxValue = emt.numberAttribute("maxValue", maxValue);
    majorStep = emt.numberAttribute("majorStep", majorStep);
    minorStep = emt.numberAttribute("minorStep", minorStep);
    try {
      _eventGraphPlot.setMinValue(minValue);
    } catch (Exception ex) {
        ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                         "initHorizonGraphPlotWithXML Exception:Failed to set the minValue " +
                                             minValue);
    }
    try {
      _eventGraphPlot.setMaxValue(maxValue);
    } catch (Exception ex) {
        ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                            "initHorizonGraphPlotWithXML Exception:Failed to set the maxValue " +
                                                maxValue);
    }
    try {
      _eventGraphPlot.setMajorStep(majorStep);
    } catch (Exception ex) {
        ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                    "initHorizonGraphPlotWithXML Exception:Failed to set the majorStep " + majorStep);
   }
    try {
      _eventGraphPlot.setMinorStep(minorStep);
    } catch (Exception ex) {
        ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                    "initHorizonGraphPlotWithXML Exception:Failed to set the minorStep " + minorStep);
    }

    _eventGraphPlot.updateTransformation();
  }

  private void makeBhpLayerWithXML(Node node) {
    String dstype = com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_BHPSU;
    String nodeName = node.getNodeName();
    if (!BhpViewerHelper.isDataLayer(nodeName) ) {
        ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                            "BhpWindow.makeBhpLayerWithXML error : not a valid node name " +
                                                nodeName);
      return;
    }

    Node tmpNode;
    NodeList nodeList = node.getChildNodes();
    ElementAttributeReader emt= null;
    Element polylineElement = null;
    Element eventShapeElement = null;
    for (int i = 0; i < nodeList.getLength(); i++) {
      tmpNode = nodeList.item(i);
      if (tmpNode.getNodeType() == Node.ELEMENT_NODE) {
        String tmpNodeName = tmpNode.getNodeName();
        if (tmpNodeName.equals("BhpLayer")) {
          emt = new ElementAttributeReader(tmpNode);
          //break;
        } else if (tmpNodeName.equals("BhpMapPolylines")) {
          polylineElement = (Element) tmpNode;
        } else if (tmpNodeName.equals("BhpEventShapeSetting")) {
          eventShapeElement = (Element) tmpNode;
        }
      }
    }
    if (emt == null) {
        ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                            "BhpWindow.makeBhpLayerWithXML error : Cannot find node BhpLayer");
      return;
    }

    Node parameterNode = null;
    Node pipelineNode = null;
    nodeList = emt.getChildNodes();
    for (int i = 0; i < nodeList.getLength(); i++) {
      tmpNode = nodeList.item(i);
      String tmpNodeName = tmpNode.getNodeName();
      if (tmpNodeName.equals("Parameter")) {
        parameterNode = tmpNode;
      } else if (tmpNodeName.equals("Pipeline")) {
        pipelineNode = tmpNode;
      }
    }

      int id = emt.getInt("idNumber");
      String name = emt.getString("name");
      String dataName = emt.getString("dataName");
      String pathlist = emt.getString("pathlist");
      String dstypeString = emt.getString("dsType");
      dstype = BhpViewerHelper.readDSType(dstypeString);
      boolean enableTalk = emt.getBoolean("enableTalk");
      int synFlag = emt.getInt("synFlag");
      int transparent = emt.getInt("transparent");
      String property = emt.getString("property");
      boolean propertySyn = emt.getBoolean("propertySync");
      boolean reversedPolarity = emt.getBoolean("reversedPolarity");
      boolean visible = emt.getBoolean("visible",true);
      boolean reversedSampleOrder = emt.getBoolean("reversedSampleOrder");

      // handle the scale
      boolean oldFormat;
      double mupt = emt.getDouble("scaleFactorH");
      double mups = emt.getDouble("scaleFactorV");
      if (mupt>0 && mups>0) {
        oldFormat = false;
      } else {
        mupt =1;
        mups =1;
        oldFormat = true;
      }
      if (id == _plot.getAxisAssociateLayer() && oldFormat) {
        _plot._modelUnitPerTrace = emt.getDouble("modelUnitPerTrace");
        _plot._modelUnitPerSample = emt.getDouble("modelUnitPerSample");
        // apply its synchronization flag on the plot
        applyPSFlag(synFlag);
      }

      emt.setNode(node);
      BhpLayer layer=null;
      if (nodeName.equals("BhpSeismicLayer")) {
        BhpSeismicTableModel parameter = new BhpSeismicTableModel(_viewer, true);
        parameter.initWithXML(parameterNode, dstype);
        layer = new BhpSeismicLayer(_viewer, this, id, dstype, dataName, name, pathlist, parameter, mupt,
                                    mups, enableTalk, synFlag, transparent, pipelineNode, reversedPolarity, oldFormat);
      } else if (nodeName.equals("BhpModelLayer")) {
        BhpSeismicTableModel parameter = new BhpSeismicTableModel(_viewer, false);
        parameter.initWithXML(parameterNode, dstype);
        layer = new BhpModelLayer(_viewer, this, id, dstype, dataName, name, pathlist, property, parameter,
                                  mupt, mups, enableTalk, synFlag, transparent, propertySyn, pipelineNode, reversedPolarity);
      } else if (nodeName.equals("BhpEventLayer")) {
        BhpSeismicTableModel parameter = new BhpSeismicTableModel(_viewer, false);
        parameter.initWithXML(parameterNode, dstype);
        int refSeismicLayerId = -1;
        refSeismicLayerId = emt.numberAttribute("refSeismicLayerId", refSeismicLayerId);
        int eventLS = emt.getInt("eventLineStyle");
        float eventLW = emt.getFloat("eventLineWidth");
        Color eventLC = emt.getColor("eventLineColor");
        layer = new BhpEventLayer(_viewer, this, id, dstype, dataName, name, pathlist, property, parameter,
                                  mupt, mups, enableTalk, synFlag, transparent, propertySyn, refSeismicLayerId, eventLS, eventLW, eventLC,
                                  pipelineNode, reversedPolarity, eventShapeElement);
      } else if (nodeName.equals("BhpMapLayer")) {
        int mapDataType = emt.getInt("dataType");
        boolean isSeismic = false;
        if (mapDataType == BhpViewer.BHP_DATA_TYPE_SEISMIC) {
          isSeismic = true;
        }
        BhpSeismicTableModel parameter = new BhpSeismicTableModel(_viewer, isSeismic);
        parameter.initWithXML(parameterNode, dstype);
        int mapSynFlag = emt.getInt("mapSynFlag");
        boolean modelLayerSync = emt.getBoolean("modelLayerSync");
        boolean lockAspectRatio = emt.getBoolean("lockAspectRatio");
        boolean transposeImage = emt.getBoolean("transposeImage");
        boolean reverseh = emt.getBoolean("reversedHDirection");
        if (lockAspectRatio && id == _plot.getAxisAssociateLayer()) {
          _plot.setLockAspectRatio(lockAspectRatio);
        }
        layer = new BhpMapLayer(_viewer, this, id, dstype, dataName, name, pathlist, property, parameter, mupt,
                                mups, enableTalk, synFlag, transparent, propertySyn, modelLayerSync, pipelineNode, mapDataType, mapSynFlag,
                                lockAspectRatio, reversedPolarity, reversedSampleOrder, polylineElement, transposeImage, reverseh);
      // QIW Disable references to log data
      }
      if (layer != null) {
        layer.setLoadedVisiblity(visible);
        _loadingLayers.add(layer);
        layer.setupSeismicReader();
      }
  }

  /**
   * @param synFlag
   */
  private void applyPSFlag(int synFlag) {
    int psflag = 0;
    if ((synFlag & BhpViewer.CURSOR_FLAG) != 0) {
      psflag = psflag | BhpViewer.CURSOR_FLAG;
    }
    if ((synFlag & BhpViewer.WINDOW_VIEWPOS_FLAG) != 0) {
      psflag = psflag | BhpViewer.WINDOW_VIEWPOS_FLAG;
    }
    if ((synFlag & BhpViewer.SCALEH_FLAG) != 0) {
      psflag = psflag | BhpViewer.SCALEH_FLAG;
    }
    if ((synFlag & BhpViewer.SCALEV_FLAG) != 0) {
      psflag = psflag | BhpViewer.SCALEV_FLAG;
    }
    _plot.setSynFlag(psflag);
  }



  public int getNextLayerID() {
    int id = _plot.getNextUniqueLayerID();
    //removing this unsafe assignment to static var from instance method
    //it is never read anyway - Woody Folsom 1/9/2008
    //_layerCounter=id;
    return id;
  }

  private void setupMainPanel(double hs, double vs, boolean showPointer) {
    _mainPanel = new JPanel(new BorderLayout());
    _plotContainer = new BoxContainer(BoxLayout.Y_AXIS);
    if (_windowType == BhpViewer.WINDOW_TYPE_XSECTION) {
      _plot = new BhpPlotXV(hs, vs);
      _eventGraphPlot = new BhpEventGraphPlot(_plot);
      _plot.addChangeListener(_eventGraphPlot);
      _plot.getMainView().addViewEventListener(_eventGraphPlot);
      if (_showEventGraphPlot) {
        _plotContainer.add(_eventGraphPlot);
      }
      _plotContainer.add(_plot);
    } else {
      _plot = new BhpPlotMV(hs, vs);
      _plotContainer.add(_plot);
    }

    _plot.getMainView().setShowPointer(showPointer);
    _mainPanel.add(_plotContainer, BorderLayout.CENTER);

    boolean showVerticalName = true;
    if (_windowType == BhpViewer.WINDOW_TYPE_XSECTION) {
      showVerticalName = false;
    }
    _statusbar = new BhpWindowStatusBar(showVerticalName);

    _plot.getMainView().addRubberView(new RubberView(this));

    _plot.getMouseCenter().setMouseTrackListener(_statusbar);
  }

  private void setupListPanel() {
    _listPanel = new JPanel(new BorderLayout());
    _listPanel.setBorder(new TitledBorder("Layers"));
    _list = new BhpLayerList(this);
    BhpLayerListMouseListener listListener = new BhpLayerListMouseListener(this);
    _list.addMouseListener(listListener);
    JScrollPane scrollp = new JScrollPane(_list);
    _listPanel.add(scrollp, BorderLayout.CENTER);
  }

  private void setupTopLevelGUI() {
    _splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, _listPanel, _mainPanel);
    _splitPane.setDividerLocation(50);
    _splitPane.setOneTouchExpandable(true);
    this.getContentPane().add(_splitPane);
  }

  private boolean checkWindowType(int windowTypeNeeded) {
    if (_windowType == windowTypeNeeded) {
      return true;
    }
    ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                "BhpWindow.checkWindowType: unmatched window type, need " + windowTypeNeeded);
    return false;
  }

  private void windowClosing() {
    Object[] views = _plot.getMainView().getAllBhpLayers();
    int layerId=-1;
    for (int i = 0; i < views.length; i++) {
      if (views[i] != null && (views[i] instanceof BhpLayer)) {
        layerId = ((BhpLayer) views[i]).getIdNumber();
        ((BhpLayer) views[i]).cleanupLayer();
      }
    }
    if (layerId>0){
      _viewer.broadcastViewerEvent(new BhpLayerEvent(BhpViewer.DISPLAY_FLAG,null,new Integer(layerId)));
    }
  }

  public void createLayerFromDragDrop(Hashtable table) {
    if (layerExists(table.get("layerId"))) return;
    // check layer type
    String layerType = table.get("layerType").toString();
    if (layerType.equals("XML-Horizon")){
      // Change method for getting BhpViewerBase
      //BhpViewerBase viewer = (BhpViewerBase) (SwingUtilities.windowForComponent(this));
      BhpViewerBase viewer = getSelectedLayer()._viewer;
      try {
        // create data source
        int dataType = BhpViewer.BHP_DATA_TYPE_EVENT;
        String dataName = table.get("dataName").toString();
        String dataPath = table.get("dataPath").toString();
        boolean propertySync = false;
        boolean isSeismic = false;

        //get seismic layer
        BhpLayer seismicLayer = getFeaturedLayer();
        String dsType = GeneralDataSource.DATA_SOURCE_XMLHRZ;//table.get("dsType").toString();

        //BhpSeismicTableModel seismicModel = seismicLayer.getParameter();
        // need seismic layer as parent
        if (seismicLayer == null || !(seismicLayer instanceof BhpSeismicLayer)) {
            JOptionPane.showMessageDialog(_viewer,"need a seismic layer as parent");
          return;
        }
        GeneralDataSource dataSource = DataSourceFactory.createDataSource(_viewer, dataPath);
        //get property of dragged horizon
        Object event = table.get("sharedObject");
        String horizon = dataSource.getProperties();
        //if (event!=null) horizon = ((Horizon)event).getName();
        BhpSeismicTableModel horizonModel = new BhpSeismicTableModel(_viewer, false);
        horizonModel.setDataSource(dataSource);
        horizonModel.addRowData("tracl", "1-1[1]");

        String[] names = dataSource.getHeaders();
        BhpSeismicTableModel seismicModel=seismicLayer.getParameter();
        for (int i=0; i<names.length; i++) {
          horizonModel.addRowData(names[i], dataSource.getHeaderSetting(names[i]));
        }
        horizonModel.setProperties(horizon);
        horizonModel.setDataAttributeString(dataSource.getDataAtt());
        for (int i=0; i<horizonModel.getRowCount(); i++){
          for (int j=0; j<seismicModel.getRowCount(); j++){
            if ((horizonModel.getValueAt(i,0).toString().equalsIgnoreCase(
                seismicModel.getValueAt(j,0).toString()))){
              Object chosen=seismicModel.getValueAt(j,2);
              if (chosen.toString().compareTo("*")==0)
                chosen=seismicModel.getValueAt(j,1);
              horizonModel.setValueAt(chosen,i,2);
            }
          }
        }
        //cretae event layer
        createXVLayer(viewer, dsType, dataName, dataPath, horizon, propertySync,
                      horizonModel, seismicLayer, dataType, null);
      }
      catch (Exception ex) {
          ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                              "Failed to create new layer from drag/drop event");
      }
    }
  }

  private boolean layerExists(Object idobj) {
    if (idobj == null) return false;
    try {
      int layerid = Integer.parseInt(idobj.toString());
      BhpLayer layer = _plot.getBhpLayer(layerid);
      if (layer != null) return true;
    }
    catch (Exception ex) {
      return false;
    }
    return false;
  }

  private class MyInternalFrameAdapter extends InternalFrameAdapter {
    public void internalFrameClosing(InternalFrameEvent e) {
      windowClosing();
    }
  }

  /**
   * Because of bug in JScrollPane, we force the JScrollPane to update ScrollBar.
   * May 18, 2006
   */
  public void updatePlot(){
    Dimension size = this.getSize();
    this.setSize(size.width+1, size.height);
    this.setSize(size);
  }
}
