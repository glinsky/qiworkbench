/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import com.gwsys.gw2d.model.*;
import com.gwsys.gw2d.shape.*;
import com.gwsys.gw2d.view.*;
import com.gwsys.gw2d.util.*;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This view is placed on top of all the other views.
 *               If the rubber band zooming has been activated, this
 *               class responses to the mouse event, draws the rubber
 *               band box and asks the active layer to zoom.
 *               This layer is also responsible for mouse tracking.<br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class RubberView extends SimpleViewContainer {

	/**
	 *
	 */
	private static final long serialVersionUID = 3257850978341763381L;

	//private boolean _visibleSymbol = false;
	private BhpWindow _bhpWindow = null;

	private boolean _flag = false;

	private SymbolShape _symbol;

	private RectangleShape _rband;

	// Initializes the rubber band x-coordinate.
	private int _xrub = -1;

	// Initializes the rubber band y-coordinate.
	private int _yrub = -1;

	// Initializes the x-coordinate of the mouse cursor.
	private int _xcur = -1;

	// Initializes the y-coordinate of the mouse cursor.
	private int _ycur = -1;

	// Initializes the mouse tracking flag to false
	//private boolean _mousetracked = false;

	/**
	 * Creates the view that is on top of the stacked view.
	 * @param w the window that this layer belongs to.
	 */
	public RubberView(BhpWindow w) {

		_bhpWindow = w;

		CommonDataModel model = new CommonDataModel();
		CommonShapeLayer layer = new CommonShapeLayer();

		setModel(model);
		setTransformation(new Transform2D());

		RenderingAttribute attr = new RenderingAttribute(model);
		attr.setLineColor(Color.cyan);
		attr.setFillPaint(new Color(255, 0, 0, 100));
		attr.setLineWidth(3.0f);
		attr.setLineStyle(RenderingAttribute.LINE_STYLE_SOLID);
		attr.setFillStyle(RenderingAttribute.FILL_STYLE_SOLID);

		_rband = new RectangleShape();
		_rband.setVisible(false);
		_rband.setAttribute(attr);

		_symbol = new SymbolShape(0, 0, 10.0, 10.0,
				LocationBasedShape.LOCATION_CENTER, new PlusSymbolPainter(),
				true, true);
		_symbol.setVisible(false);
		_symbol.setAttribute(null);
		//_visibleSymbol = false;

		layer.addShape(_symbol);
		layer.addShape(_rband);
		model.addLayer(layer);
	}

	/**
	 * Sets the rubber band zooming flag to true. <br>
	 * The flag will be set to false when one rubber band
	 * zooming finishes.
	 * This method also set the cursor to the hand cursor.
	 */
	public void setFlag() {
		_flag = true;
		_bhpWindow.getContentPane().setCursor(
				Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	}

	public void updateSymbolSize(double w, double h) {
		if (_bhpWindow.isShowEventGraphPlot())
			_bhpWindow.getBhpEventGraphPlot().updateEventGraphSymbolSize(w, h);
		_symbol.setSize(w, h);
	}

	/**
	 * For temporarily hiding the cursor.
	 * The method is called when the mouse exit the window.
	 */
	public void hideSymbol() {
		if (_bhpWindow.isShowEventGraphPlot())
			_bhpWindow.getBhpEventGraphPlot().hideEventGraphSymbol();

		_symbol.setVisible(false);
		repaint();
	}

	/**
	 * Sets the anchor position of the mouse tracking symbol.
	 * @param x the horizontal position.
	 * @param y the vertical position.
	 */
	public void setSymbolAnchor(int x, int y) {
		
		if (_bhpWindow.isShowEventGraphPlot())
			_bhpWindow.getBhpEventGraphPlot().setEventGraphSymbolAnchor(x, y);
		//get viewer from bhpwindow
		if (_symbol.getAttribute() == null) {
			BhpViewerBase viewer = _bhpWindow.getBhpViewerBase();
            _symbol.setAttribute(viewer.getCursorAttribute());
            _symbol.setSize(viewer.getCursorWidth(),viewer.getCursorHeight());
		}
		_symbol.setVisible(true);		
		_symbol.setLocation(x, y);		
		repaint();
	}

	/**
	 * Gets the horizontal position of the mouse tracking symbol.
	 */
	public int getSymbolAnchorX() {
		return (int) _symbol.getLocationX();
	}

	/**
	 * Gets the vertical position of the mouse tracking symbol.
	 */
	public int getSymbolAnchorY() {
		return (int) _symbol.getLocationY();
	}

	// Tracks the mouse position and starts drawing the rectangle (rubberband).
	/**
	 * Method to handle the mouse pressing event. <br>
	 * Left mouse pressing signifies the start
	 * of rubber band zooming. A rectangle (rubberband)
	 * will be drawn for the zooming area, its top-left
	 * corner is defined by the mouse postion of this
	 * mouse pressing event.
	 */
	public void mousePressed(MouseEvent me) {

		if (!SwingUtilities.isLeftMouseButton(me))
			return;
		
		_xrub = me.getX();
		_yrub = me.getY();
		_xcur = _xrub + 1;
		_ycur = _yrub + 1;

		Bound2D box = new Bound2D(_xrub, _yrub, _xcur, _ycur);
		box = getTransformation().inverseTransform(box);

		_rband.setRectangle(box);
		_rband.setVisible(true);
	}

	// Draws the rectangle (rubberband).
	/**
	 * Method to handle the mouse dragging event. <br>
	 * Left mouse dragging signifies the continuing
	 * of rubber band zooming. A rectangle (rubberband)
	 * will be drawn for the zooming area, its changing bottom-right
	 * corner is defined by the mouse postion of this
	 * mouse dragging event.
	 */
	public void mouseDragged(MouseEvent me) {

		if (!SwingUtilities.isLeftMouseButton(me))
			return;
		
		repaint(_xrub, _yrub, _xcur + 1, _ycur + 1);
		
		_xcur = me.getX();
		_ycur = me.getY();

		Bound2D box = new Bound2D(_xrub, _yrub, _xcur, _ycur);
		box = getTransformation().inverseTransform(box);
		_rband.setRectangle(box);
	}

	// Removes the rubberband (rectangle) from view.
	/**
	 * Method to handle the mouse releasing event. <br>
	 * Left mouse releasing signifies the ending
	 * of rubber band zooming. The final rectangle
	 * of the zooming area is defined by the previous
	 * position of the mouse pressing event and the
	 * position of this mouse releasing event.
	 * This method calculate scale factor based on
	 * the zooming rectangle and the current view size.
	 * Then, the active layer is asked to scale and a
	 * <code>{@link BhpLayerEvent}</code> with flag
	 * (SCALEH_FLAG|SCALEV_FLAG) will be created and
	 * broadcasted.
	 */
	public void mouseReleased(MouseEvent me) {

		if (!SwingUtilities.isLeftMouseButton(me))
			return;
		
		_rband.setVisible(false);

		repaint(_xrub, _yrub, _xcur + 1, _ycur + 1);
		
		if (_flag == false)
			return;
		// Scale stacked view
		_flag = false; // reset the flag
		_bhpWindow.getContentPane().setCursor(
				Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		BhpLayer layer = _bhpWindow.getFeaturedLayer();
		if (layer == null)
			return;

		Dimension vsize = _bhpWindow.getBhpPlot().getViewport().getExtentSize();
		Bound2D r = _rband.getRectangle();
		double xFactor = vsize.width / r.width;
		double yFactor = vsize.height / r.height;

		//******See getViewPosition()***************************
		_bhpWindow.getBhpPlot().scaleToWithLayer(layer, xFactor, yFactor, _xrub, _yrub);
		Point newPosition = _bhpWindow.getBhpPlot().getViewPosition();
		if (_bhpWindow.getBhpPlot().getEnablePlotTalk()) {
			int eventFlag = BhpViewer.SCALEH_FLAG | BhpViewer.SCALEV_FLAG;
            		BhpLayerEvent event = new BhpLayerEvent(eventFlag, layer);
            		_bhpWindow.getBhpViewerBase().broadcastBhpLayerEvent(event);
		}
		_bhpWindow.getBhpPlot().setViewPosition(newPosition.x, newPosition.y);
		//note this position is changed in someplace, not sure why
		_bhpWindow.getBhpPlot().broadcastWindowPosition();
	}
	
}
