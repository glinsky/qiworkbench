/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;


import java.util.Hashtable;
import java.util.Vector;

import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpBilliton.viewer2d.data.BhpEventReaderInterface;

import com.gwsys.seismic.core.TraceKeyRangeChooser;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.indexing.SegyHeaderFormat;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is the customized data selector component
 *               used in BhpEventPipeline. Method getEvent(String name)
 *               is used to get the event data. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpEventDataSelector extends TraceKeyRangeChooser {
    double _startSampleValue;
    double _endSampleValue;
    boolean _startSampleChanged;
    boolean _endSampleChanged;

    private BhpViewerBase viewer;

    /**
     * Constructs a BhpEventDataSelector.
     */
    public BhpEventDataSelector(BhpViewerBase viewer) {
        super();
        this.viewer = viewer;
        _startSampleValue = Double.NaN;
        _endSampleValue = Double.NaN;
        _startSampleChanged = false;
        _endSampleChanged = false;
    }

    /**
     * Sets the start sample value.
     */
    public void setStartSampleValue(double s) {
        _startSampleValue = s;
        _startSampleChanged = true;
    }

    /**
     * Sets the end sample value.
     */
    public void setEndSampleValue(double e) {
        _endSampleValue = e;
        _endSampleChanged = true;
    }

    /**
     * Finds out if start sample value has been set
     * programmatically calling setStartSampleValue.
     */
    public boolean getStartSampleChanged() { return _startSampleChanged; }
    /**
     * Finds out if the end smaple value has been set
     * programmatically calling setEndSampleValue.
     */
    public boolean getEndSampleChanged()   { return _endSampleChanged;   }

    /**
     * Retrieves the start sample value.
     * Its default value is the minimum depth
     * value of the selected dataset.
     */
    public double getStartSampleValue() {
        if (Double.isNaN(_startSampleValue)) {
            Object parent = this.getParentProcessor();
            if (isValidBhpEventReaderParent(parent)) {
                _startSampleValue = ((BhpEventReaderInterface)parent).getMinDepth();
            }
        }
        return _startSampleValue;
    }

    /**
     * Retrieves the end sample value.
     * Its default value is the maximum depth
     * value of the selected dataset.
     */
    public double getEndSampleValue() {
        if (Double.isNaN(_endSampleValue)) {
            Object parent = this.getParentProcessor();
            if (isValidBhpEventReaderParent(parent)) {
                _endSampleValue = ((BhpEventReaderInterface)parent).getMaxDepth();
            }
        }
        return _endSampleValue;
    }

    /**
     * Gets the data array of a specific event.
     * This method has not implemented the primary and secondary
     * data selection of standard cgSeismicDataSelector.
     * It always returns the full event data array.
     */
    public float[] getEvent(String ename, BhpSeismicLayer seismic) {
        Object parent = this.getParentProcessor();
        if (!isValidBhpEventReaderParent(parent)) {
            return null;
        }

        float nullValue = (float) ((com.bhpBilliton.viewer2d.data.BhpDataReader) parent).getDefinedNullValue();
        int eventValueLength = (int) this.getVirtualModelLimits().width;
        if (seismic == null) {
            // temporary implementation for testing, need to add the real selection
            // return ((BhpEventReaderInterface)parent).getEvent(ename);
            float[] eventValues = new float[eventValueLength];
            float[] readerValues = ((BhpEventReaderInterface) parent).getEvent(ename);
            int realId = -1;
            for (int i = 0; i < eventValueLength; i++) {
                realId = this.virtualToReal(i);
                if (realId < 0 || readerValues==null || realId >= readerValues.length) {
                    eventValues[i] = nullValue;
                } else {
                    eventValues[i] = readerValues[realId];
                }
            }
            return eventValues;
        }

        eventValueLength = (int) seismic.getPipeline().getDataLoader().getDataSelector().getVirtualModelLimits().width;
        float[] eventValues = new float[eventValueLength];
        if (parent instanceof TemporaryEventDataReader) {
            int validSize = ((TemporaryEventDataReader) parent).getValidEventSize();
            if (validSize == 0) {
                for (int i = 0; i < eventValueLength; i++) {
                    eventValues[i] = Float.NaN;
                }
                return eventValues;
            } else if (validSize < eventValueLength) {
                if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
                    System.out.println("Temporarydata " + validSize);
                return getEventFromTemporaryDataReader(seismic, ename, (TemporaryEventDataReader) parent);
            }
        }
        //int numTracesV =(int) (seismic.getPipeline().getDataSelector().getVirtualModelLimits().getWidth());
        //float[] values = new float[numTracesV];
        Vector fields = new Vector();
        BhpSeismicTableModel parameter = seismic.getParameter();
        for (int i=0; i<parameter.getRowCount()-1; i++) {
            Object paramValue = parameter.getValueAt(i, 0);
            fields.add(paramValue.toString());
        }
        Integer[] fieldsId = new Integer[fields.size()];
        Vector formats = seismic.getPipeline().getDataLoader().getDataReader().getDataFormat().getTraceHeaderFormats();
        SegyHeaderFormat headerFormat = (SegyHeaderFormat) (formats.firstElement());
        for (int j=0; j<fields.size(); j++) {
            fieldsId[j] = BhpSegyFormat.getFieldForName(((String)(fields.get(j))), headerFormat);
        }
        Hashtable param = new Hashtable(fields.size());
        TraceHeader seismicMeta = null;
        Hashtable headerValues = null;
        String hname;
        double[] vsettings = parameter.getVerticalSettings();
        for (int j=0; j<eventValueLength; j++) {
            try {
                //realId = this.virtualToReal(j);
                seismicMeta = seismic.getPipeline().getDataLoader().getDataSelector().getTraceHeader(j);
                if (seismicMeta == null) {
                    eventValues[j] = nullValue;
                }
                else {
                    headerValues = seismicMeta.getDataValues();
                    for (int k=0; k<fieldsId.length; k++) {
                        if (fieldsId[k] != null) {
                            hname = (String) (fields.get(k));
                            Object headerValue = headerValues.get(fieldsId[k]);
                            param.put(hname, headerValue);
                        }
                    }
                    eventValues[j] = (((BhpEventReaderInterface)parent).
                            getEventValueAt(ename, param)) - (float)vsettings[0];
                }
            }
            catch (Exception ex) {
                ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                    "Cannot get event data");
                eventValues[j] = (float)((com.bhpBilliton.viewer2d.data.BhpDataReader)parent).getDefinedNullValue();
                break;
            }
        }
        return eventValues;
    }

    /**
     * @param parent
     * @return
     */
    private boolean isValidBhpEventReaderParent(Object parent) {
        if (parent == null || !(parent instanceof BhpEventReaderInterface)) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "BhpEventDataSelector.getEvent : null or invalid reader");
            return false;
        } else {
            return true;
        }
    }

    private float[] getEventFromTemporaryDataReader(BhpSeismicLayer seismic,
            String ename, TemporaryEventDataReader eventReader) {
        int eventValueLength = (int) seismic.getPipeline().getDataLoader().getDataSelector().
                           getVirtualModelLimits().width;
        int validEventSize = eventReader.getValidEventSize();
        float[] eventValues = new float[eventValueLength];
        Vector fields = new Vector();
        for (int i=0; i<seismic.getParameter().getRowCount()-1; i++) {
            fields.add(seismic.getParameter().getValueAt(i, 0).toString());
        }
        Hashtable param = new Hashtable(fields.size());
        Vector formats = seismic.getPipeline().getDataLoader().getDataReader().getDataFormat().getTraceHeaderFormats();
        SegyHeaderFormat headerFormat = (SegyHeaderFormat) (formats.firstElement());
        TraceHeader seismicMeta = null;
        Hashtable headerValues = null;
        String hname;
        double[] vsettings = seismic.getParameter().getVerticalSettings();
        float nullValue = (float)(eventReader.getDefinedNullValue());
        int validFoundNumber = 0;
        for (int j=0; j<eventValueLength; j++) {
            if (validFoundNumber >= validEventSize) {
                eventValues[j] = nullValue;
                continue;
            }
            try {
                //realId = this.virtualToReal(j);
                seismicMeta = seismic.getPipeline().getDataLoader().getDataSelector().getTraceHeader(j);
                if (seismicMeta == null) {
                    eventValues[j] = nullValue;
                }
                else {
                    headerValues = seismicMeta.getDataValues();
                    for (int k=0; k<fields.size(); k++) {
                        hname = (String) (fields.get(k));
                        Integer hId = BhpSegyFormat.getFieldForName(hname, headerFormat);
                        if (hId != null) {
                            Object headerValue = headerValues.get(hId);
                            param.put(hname, headerValue);
                        }
                    }
                    eventValues[j] = (eventReader.
                            getEventValueAt(ename, param)) - (float)vsettings[0];
                    if (!BhpEventShape.isNullValue(eventValues[j], nullValue))
                        {
                        validFoundNumber++;
                        }
                }
            }
            catch (Exception ex) {
                ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                    "BhpEventDataSelector.getEvent excpetion");
                eventValues[j] = (float)(eventReader.getDefinedNullValue());
                break;
            }
        }
        return eventValues;
    }

}
