/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.Dimension;

import com.bhpBilliton.viewer2d.data.BhpModelReaderInterface;
import com.gwsys.seismic.core.RenderingParameters;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is the rasterizer that really draws the model display image.
 *               It can draw only the model display as structural interpolated.
 *               With Structual interpolated
 *               density, model layers are clearly displayed.
 *               This rasterizer uses the depth array of the corresponding
 *               trace to determine the value at certain vertical position
 *               in a trace. <br><br>
 * @version 1.0
 */

public class BhpStructuralRasterizer extends BhpModelTraceRasterizer {

    private BhpStructuralInterpolator _interpolator;

    private boolean _structuralInterpFlag;

    /**
     * Constructs a new instance.
     * @param reader the model reader of the pipeline.
     */
    public BhpStructuralRasterizer(BhpModelReaderInterface reader,
            BhpStructuralInterpolator interp) {
        super(reader);
        _structuralInterpFlag = true;
        _interpolator = interp;

        _interpolator.setStructuralInterpFlag(getStructuralInterpFlag());
    }

    /**
     * Gets the flag of structural interpolation.
     */
    public boolean getStructuralInterpFlag() {
        return _structuralInterpFlag;
    }

    /**
     * Sets the flag of structural interpolation.
     */
    public void setStructuralInterpFlag(boolean f) {
        _structuralInterpFlag = f;
        _interpolator.setStructuralInterpFlag(f);
    }

    /**
     * Rasterizes a seismic trace, no highlight. <br>
     * Please refer to JSeismic documentation for more detail.
     * @param prevTrace the trace previous to this trace, used for interpolation.
     * @param thisTrace the trace that is being rasterized.
     * @param sampleRange the sample range thiat is being rasterized.
     * @param lastTraceLoc the horizontal location (in pixels) of the previous trace.
     * @param thisTraceLoc the horizontal location (in pixels) of this trace.
     */
    public void rasterizeTrace(TraceData prevTrace, TraceData thisTrace,
            NumberRange sampleRange, int lastTraceLoc, int thisTraceLoc) {
        rasterizeTrace(prevTrace, thisTrace, sampleRange, lastTraceLoc,
                thisTraceLoc, false);
    }

    /**
     * Rasterizes a seismic trace. <br>
     * Please refer to JSeismic documentation for more detail.
     * @param prevTrace the trace previous to this trace, used for interpolation.
     * @param thisTrace the trace that is being rasterized.
     * @param sampleRange the sample range thiat is being rasterized.
     * @param lastTraceLoc the horizontal location (in pixels) of the previous trace.
     * @param thisTraceLoc the horizontal location (in pixels) of this trace.
     * @param isHighlighted flag indicates if the trace should be highlighted.
     */
    public void rasterizeTrace(TraceData prevTrace, TraceData thisTrace,
            NumberRange sampleRange, int lastTraceLoc, int thisTraceLoc,
            boolean isHighlighted) {

        if (!getStructuralInterpFlag()) {
            super.rasterizeTrace(prevTrace, thisTrace, sampleRange,
                    lastTraceLoc, thisTraceLoc, isHighlighted);
            return;
        }

        if (_colorMap == null)
            return;
        _numColors = _colorMap.getDensityColorSize();
        _backgroundPixel = _colorMap.getBackgroundIndex();

        if ((lastTraceLoc == Integer.MIN_VALUE)
                || (lastTraceLoc == Integer.MAX_VALUE)) {
            lastTraceLoc = thisTraceLoc - (int) _deltaPixels;
        }

        int i, j, k;
        double theColorMapStartValue = this.getColorMapStartValue();
        double theColorMapEndValue = this.getColorMapEndValue();
        int deltaPixels = thisTraceLoc - lastTraceLoc;
        int thisTraceId = ((Integer) thisTrace.getUniqueKey()).intValue();
        float[] thisData = thisTrace.getSamples();
        float[] thisDepth = _reader.getDepthArray(thisTraceId);
        int lastDensityLoc = lastTraceLoc;
        int thisDensityLoc = thisTraceLoc;
        int startOffset = 0;
        if (lastDensityLoc < 0) {
            startOffset = -lastDensityLoc;
            lastDensityLoc = 0;
        }
        if (thisDensityLoc > _imageWidth)
            thisDensityLoc = _imageWidth;
        int traceWidth = thisDensityLoc - lastDensityLoc;

        float[] prevData;
        float[] prevDepth;

        if (prevTrace == null) {
            prevData = thisData;
            prevDepth = thisDepth;
        } else {
            prevData = prevTrace.getSamples();
            Integer prevId = (Integer) prevTrace.getUniqueKey();
            prevDepth = _reader.getDepthArray(prevId.intValue());
        }
        double[] varA = new double[deltaPixels];
        double[] varB = new double[deltaPixels];

        double varC = _numColors
                / (theColorMapEndValue - theColorMapStartValue);
        double b;
        for (i = 0; i < deltaPixels; i++) {
            if (_interpolatedDensityPlot && deltaPixels > 2) {
                // if traces are too close to each other, variable density is enough
                // the algorithm here may cause divided by 0
                b = ((double) (i + startOffset)) / (deltaPixels - 1);
                varA[i] = (1. - b);
                varB[i] = b;
            } else { // _densityPlot
                if (i < deltaPixels / 2)
                    b = 0;
                else
                    b = 1;
                varA[i] = 1 - b;
                varB[i] = b;
            }
        }
        int[] prevTurnIndexes = findTurnIndexes(prevDepth);
        int[] thisTurnIndexes = findTurnIndexes(thisDepth);
        int turnIndexesLength = Math.min(prevTurnIndexes.length,
                thisTurnIndexes.length);
        int heightStart = sampleRange.getMin().intValue();
        int heightEnd = _imageHeight + heightStart;
        // Structural drawing block -->
        {
            double prevSampleValue, thisSampleValue;
            byte pixel = 0;
            int sampleValue = 0;
            if (_interpolatedDensityPlot || _densityPlot)
                for (j = lastDensityLoc; j < thisDensityLoc; j++) {
                    k = j - lastTraceLoc;
                    double c = (double) k / (thisTraceLoc - lastTraceLoc);
                    int bitmapLineIndex = 0;
                    int layerIndex = 0;
                    pixel = -1;
                    boolean recalc = true;
                    for (i = heightStart; i < heightEnd; i++, bitmapLineIndex += _scanLineSize) {
                        if (_imageData[bitmapLineIndex + j] != _backgroundPixel)
                            continue;

                        while (layerIndex < turnIndexesLength
                                && (c
                                        * (thisTurnIndexes[layerIndex] - prevTurnIndexes[layerIndex]) + prevTurnIndexes[layerIndex]) <= i) {
                            layerIndex++;
                            recalc = true;
                        }
                        if (recalc)
                            if (layerIndex < 1
                                    || layerIndex >= turnIndexesLength)
                                pixel = _backgroundPixel;
                            else {
                                if (((prevData[layerIndex - 1] < BhpModelTraceNormalization.NULL_LIMIT) && (prevData[layerIndex - 1] > BhpModelTraceNormalization.NULL_LIMIT2))
                                        || ((thisData[layerIndex - 1] < BhpModelTraceNormalization.NULL_LIMIT) && (thisData[layerIndex - 1] > BhpModelTraceNormalization.NULL_LIMIT2)))
                                    continue;

                                prevSampleValue = (prevData[layerIndex - 1] - theColorMapStartValue)
                                        * varC;
                                thisSampleValue = (thisData[layerIndex - 1] - theColorMapStartValue)
                                        * varC;
                                sampleValue = (int) (varA[k] * prevSampleValue + varB[k]
                                        * thisSampleValue);
                                if (_reverseFill)
                                    sampleValue = _numColors - sampleValue - 1;
                                if (sampleValue < 0)
                                    sampleValue = 0;
                                else if (sampleValue >= _numColors)
                                    sampleValue = _numColors - 1;
                                pixel = (byte) sampleValue;
                                recalc = false;
                            }
                        _imageData[bitmapLineIndex + j] = pixel;
                    }
                }
        }

        // Wiggle & P/N Fills drawing block -->
        //  if (_positiveColorFillPlot || _negativeColorFillPlot
        // || _positiveFillPlot || _negativeFillPlot || _wigglePlot)
        {
            int clipLeft = thisTraceLoc
                    - (int) (getClippingValue() * _deltaPixels + 0.5);
            int clipRight = thisTraceLoc
                    + (int) (getClippingValue() * _deltaPixels + 0.5);
            if (clipLeft < 0)
                clipLeft = 0;
            if (clipRight > _imageWidth)
                clipRight = _imageWidth;
            int wiggleLeft = 0;
            int wiggleRight = 0;
            int fillLeft, fillRight;
            int fillColor = -1;
            int bitmapLineIndex = 0;
            double scaler = _deltaPixels;
            int nextValue;

            int layerIndex = 0;
            boolean recalc = true;
            while (layerIndex < turnIndexesLength
                    && thisTurnIndexes[layerIndex] < heightStart) {
                layerIndex++;
            }
            recalc = true;
            if (layerIndex >= turnIndexesLength)
                return;

            nextValue = Integer.MIN_VALUE;
            if (layerIndex >= 1)
                nextValue = mathFloor(thisData[layerIndex - 1] * scaler);
            for (i = heightStart; i < heightEnd; i++, bitmapLineIndex += _scanLineSize) {
                while (layerIndex < turnIndexesLength
                        && thisTurnIndexes[layerIndex] <= i) {
                    layerIndex++;
                    recalc = true;
                }

                if (layerIndex < 1 || layerIndex >= turnIndexesLength)
                    continue;

                int value = mathFloor(thisData[layerIndex - 1] * scaler);
                int thisValue = (nextValue == Integer.MIN_VALUE) ? value
                        : nextValue;
                nextValue = value;

                if (_wigglePlot) {
                    byte wPixel = (isHighlighted) ? _highlightPixel
                            : _foregroundPixel;
                    if (thisValue < nextValue) {
                        for (j = thisTraceLoc + thisValue; j <= thisTraceLoc
                                + nextValue; j++)
                            if (j >= clipLeft && j < clipRight)
                                _imageData[bitmapLineIndex + j] = wPixel;
                    } else {
                        for (j = thisTraceLoc + thisValue; j >= thisTraceLoc
                                + nextValue; j--)
                            if (j >= clipLeft && j < clipRight)
                                _imageData[bitmapLineIndex + j] = wPixel;
                    }

                }

                if (thisValue < nextValue) {
                    wiggleLeft = thisTraceLoc + thisValue;
                    wiggleRight = thisTraceLoc + nextValue;
                } else {
                    wiggleLeft = thisTraceLoc + nextValue;
                    wiggleRight = thisTraceLoc + thisValue;
                }
                if (wiggleLeft < clipLeft) {
                    wiggleLeft = clipLeft;
                    if (wiggleRight < clipLeft)
                        wiggleRight = clipLeft - 1;
                }
                if (wiggleRight >= clipRight) {
                    wiggleRight = clipRight - 1;
                    if (wiggleLeft >= clipRight)
                        wiggleLeft = clipRight;
                }

                if (_positiveColorFillPlot || _negativeColorFillPlot) {
                    fillColor = mathFloor((thisData[layerIndex - 1] - theColorMapStartValue)
                            * varC);
                    if (_reverseFill)
                        fillColor = _numColors - fillColor - 1;
                    if (fillColor < 0)
                        fillColor = 0;
                    else if (fillColor >= _numColors)
                        fillColor = _numColors - 1;
                }

                boolean plotType1 = _negativeColorFillPlot || _negativeFillPlot;
                boolean plotType2 = _positiveColorFillPlot || _positiveFillPlot;
                byte pixelType1 = _negativeFillPixel;
                byte pixelType2 = _positiveFillPixel;

                if (_reverseFill) {
                    plotType1 = _positiveColorFillPlot || _positiveFillPlot;
                    plotType2 = _negativeColorFillPlot || _negativeFillPlot;
                    pixelType1 = _positiveFillPixel;
                    pixelType2 = _negativeFillPixel;
                }

                if (_positiveColorFillPlot || _negativeColorFillPlot)
                    pixelType1 = pixelType2 = (byte) fillColor;

                if (thisValue < 0 && plotType1) {
                    if (_wigglePlot)
                        fillLeft = wiggleRight + 1;
                    else
                        fillLeft = thisTraceLoc + thisValue;
                    fillRight = thisTraceLoc;
                    if (fillLeft < clipLeft)
                        fillLeft = clipLeft;
                    if (fillRight >= clipRight)
                        fillRight = clipRight - 1;
                    for (j = fillLeft; j <= fillRight; j++)
                        _imageData[bitmapLineIndex + j] = pixelType1;
                } else if (thisValue > 0 && plotType2) {
                    fillLeft = thisTraceLoc;
                    if (_wigglePlot)
                        fillRight = wiggleLeft - 1;
                    else
                        fillRight = thisTraceLoc + thisValue;
                    if (fillLeft < clipLeft)
                        fillLeft = clipLeft;
                    if (fillRight >= clipRight)
                        fillRight = clipRight - 1;
                    for (j = fillLeft; j <= fillRight; j++) {
                        if (_imageData[bitmapLineIndex + j] == _backgroundPixel)
                            _imageData[bitmapLineIndex + j] = pixelType2;
                    }
                }
            }
        }
        return;
    }

    private int[] findTurnIndexes(float[] depth) {
        int[] re = new int[depth.length];
        float depthMin = _reader.getMinDepth();
        depthMin = Math.min(depthMin, getStartDepth());
        for (int i = 0; i < depth.length; i++) {
            re[i] = (int) Math.floor((depth[i] - depthMin)
                    * _interpolator.getResamplingFactor());
        }
        return re;
    }

    private int mathFloor(double amp) {
        //FIXME: check this with a profiler... is it worth the custom stupidness?

        if (amp > 0)
            return (int) (amp + 0.5);
        else if (amp < 0)
            return (int) (amp - 0.5);
        else
            return (int) amp;
    }

    public void rasterizeTrace(RenderingParameters rasterParameters) {
        rasterizeTrace(rasterParameters.getPreviousTrace(),
                rasterParameters.getCurrentTrace(),
                rasterParameters.getSampleRange(),
                rasterParameters.getPreviousTraceLoc(),
                rasterParameters.getCurrentTraceLoc(), false);
    }

}
