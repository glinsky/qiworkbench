/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import com.gwsys.seismic.core.*;
import com.gwsys.seismic.io.SeismicMetaData;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.util.NumberRange;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is the customized trace normalization process
 *               for model data. It recognizes null value and pass
 *               them to the next component without change.
 *               Normal trace normalization process's computation will
 *               cause the null values lost. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpModelTraceNormalization extends TraceNormalizer {
    public static final int NULL_LIMIT = -998;
    public static final int NULL_LIMIT2 = -1000;
    private float _shift = 0;
    private float _multiplier = 1;

    private float _min;
    private float _max;

    /**
     * Constructs a new instance.
     */
    public BhpModelTraceNormalization() {
        super();
    }

    /**
     * Gets the minimum value used for normalization.
     */
    public float getRangeMin() { return _min; }
    /**
     * Gets the maximum value used for normalization.
     */
    public float getRangeMax() { return _max; }

    /**
     * Normalizes the specified trace.
     * @param traceId the trace ID of the trace that will be normalized.
     * @param sampleRange the range of required samples from this process.
     * @param traceDataOut the destination trace data for the trace process.
     * @return a flag indicates if the process is successful.
     */
    public boolean process(int traceId, NumberRange sampleRange, TraceData traceDataOut) {
        if(!(getParentProcessor().process(traceId, sampleRange, getTraceData())))
            return false;
        int i, j;

        float[] outputData = null;
        int inLength = getTraceData().getNumAppliedSamples();
        float[] inputData = getTraceData().getSamples();
        int start = 0;
        int end = inLength;
//        if (sampleRange != null) {
//            start = sampleRange.getMin();
//            end = sampleRange.getMax() + 1;
//        }
        if ((end - start) > traceDataOut.getNumAppliedSamples()) {
            outputData = new float[inLength];
            traceDataOut.setSamples(outputData);
            traceDataOut.setNumAppliedSamples(inLength);
        }
        else outputData = traceDataOut.getSamples();


        calculateNormalizationFactorsInternal(traceId);

        for (i=start, j=0; i<end; i++, j++) {
            if ((inputData[i] < NULL_LIMIT) && (inputData[i] > NULL_LIMIT2)) {
                outputData[j] = inputData[i];
            }
            else outputData[j] = (inputData[i] + _shift) * _multiplier;
        }

        traceDataOut.setUniqueKey(getTraceData().getUniqueKey());
        return true;
    }

    private void calculateNormalizationFactorsInternal(int traceId) {
        double min = 0, max = 0;
        SeismicWorkflow pipeline = this.getWorkflow();
        NumberRange range = new NumberRange();
        TraceHeader metaData;
        SeismicMetaData smetaData = pipeline.getDataLoader().getDataReader().getMetaData();
        int mode = this.getNormalizationMode();

        switch(mode) {
            case AMPLITUDE_MAXIMUM:
                min = smetaData.getMinimumAmplitude();
                max = smetaData.getMaximumAmplitude();
                if (min < 0 && max > 0) {
					double max_amp = Math.max(Math.abs(min), Math.abs(max));
                    range.setRange(-max_amp, max_amp);
                }
                else range.setRange(min, max);
                break;
            case TRACE_MAXIMUM:
                metaData = pipeline.getDataLoader().getDataSelector().getTraceHeader(traceId);
                if (!metaData.isStatisticsCollected())
                    calculateTraceStatistics(metaData);
                min = metaData.getMinimumAmplitude();
                max = metaData.getMaximumAmplitude();
                if (min < 0 && max > 0) {
					double max_amp = Math.max(Math.abs(min), Math.abs(max));
                    range.setRange(-max_amp, max_amp);
                }
                else range.setRange(min, max);
                break;
            case AMPLITUDE_AVERAGE:
                min = smetaData.getMinimumAmplitude();
                max = smetaData.getMaximumAmplitude();
                if (min < 0 && max > 0) {
                    float average = (float) (smetaData.getAverage());
                    range.setRange(-average, average);
                }
                else range.setRange(min, max);
                break;
            case TRACE_AVERAGE:
                metaData = pipeline.getDataLoader().getDataSelector().getTraceHeader(traceId);
                if (!metaData.isStatisticsCollected())
                    calculateTraceStatistics(metaData);
                min = metaData.getMinimumAmplitude();
                max = metaData.getMaximumAmplitude();
                if (min < 0 && max > 0) {
                    float average = (float) (metaData.getAverage());
                    range.setRange(-average, average);
                }
                else range.setRange(min, max);
                break;
            case AMPLITUDE_RMS:
                min = smetaData.getMinimumAmplitude();
                max = smetaData.getMaximumAmplitude();
                if (min < 0 && max > 0) {
                    float rms = (float) (smetaData.getRMS());
                    range.setRange(-rms, rms);
                }
                else range.setRange(min, max);
                break;
            case TRACE_RMS:
                metaData = pipeline.getDataLoader().getDataSelector().getTraceHeader(traceId);
                if (!metaData.isStatisticsCollected())
                    calculateTraceStatistics(metaData);
                min = metaData.getMinimumAmplitude();
                max = metaData.getMaximumAmplitude();
                if (min < 0 && max > 0) {
                    float rms = (float) (metaData.getRMS());
                    range.setRange(-rms, rms);
                }
                else range.setRange(min, max);
                break;
            case AMPLITUDE_LIMITS:
                min = getNormLimitMin();
                max = getNormLimitMax();
                range.setRange(min, max);
                break;
        }
		if (min < 0 && max > 0) {
			_shift = 0;
		} else {
			_shift = (range.getMin().floatValue() + range.getMax().floatValue()) / (-2.0f);
		}
		if (range.getMax() != range.getMin()) {
			_multiplier = (2 * getScale()) /
			(range.getMax().floatValue() - range.getMin().floatValue());
		} else {
			_multiplier = 0;
		}
		_min = range.getMin().floatValue();
		_max = range.getMax().floatValue();
	}

    private void calculateTraceStatistics(TraceHeader metaData) {
        float min = Float.MAX_VALUE;
        float max = Float.MIN_VALUE;
        double sumx = 0;
        double sumx2 = 0;
        int count = getTraceData().getNumAppliedSamples();
        float[] samples = getTraceData().getSamples();
        for (int i=0; i<count; i++) {
            if (samples[i] < min) min = samples[i];
            if (samples[i] > max) max = samples[i];
            if (samples[i] < 0) sumx -= samples[i];
            else                sumx += samples[i];
            sumx2 += samples[i] * samples[i];
        }
        if (count == 0)
            metaData.setTraceStatistics(min, max, 0, 0);
        else
            metaData.setTraceStatistics(min, max, sumx/count, Math.sqrt(sumx2/count));
    }
}
