/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.Dimension;

import com.gwsys.seismic.core.RenderingParameters;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;
import com.gwsys.seismic.util.SeismicColorMap;
import com.gwsys.seismic.util.SeismicColorEvent;
import com.gwsys.seismic.util.SeismicColorListener;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is the class that draws BhpMapGenericSeismicImage.
 *               Please refer to the documentation of TraceRasterizer in
 *               JSeismic for more detail.
 *               <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpMapRasterizer extends AbstractBhpColorInterpolatedRasterizer {
    private boolean _interpFlag;

    private byte[] _imageData = null;
    private int _imageWidth;
    private int _imageHeight;
    private int _scanLineSize;

    private double _deltaPixels;
    private float _clippingValue;
    private byte _foregroundPixel;
    private byte _backgroundPixel;
    private byte _highlightPixel;

    private int _numColors;
    private SeismicColorMap _colorMap = null;

    private SeismicColorListener _cml = new SeismicColorListener() {
        public void colorMapUpdated(SeismicColorEvent e) {
            updateColorMap();
        }
    };

    /**
     * Constructs a new instance.
     */
    public BhpMapRasterizer() {
        super();
        _interpFlag = false;
        setColorMap(new SeismicColorMap());
        setClippingValue(4);
    }

    /**
     * Gets the flag of color interpolation. <br>
     * Currently, there is no implementation of
     * color-interpolated rasterizing.
     */
    public boolean getInterpFlag() { return _interpFlag; }
    /**
     * Sets the flag of color interpolation.
     */
    public void setInterpFlag(boolean f) { _interpFlag = f; }

    /**
     * Retrieves the clipping value that determines to
     * which extent traces are displayed. <br>
     * Please refer to JSeismic documentation for more detail.
     */
    public float getClippingValue() {
        return _clippingValue;
    }

    /**
     * Retrieves the color map currently used by the rasterizer. <br>
     * Please refer to JSeismic documentation for more detail.
     */
    public SeismicColorMap getColorMap() {
        return _colorMap;
    }

    /**
     * Sets the clipping value that determines to
     * which extent traces are displayed. <br>
     * Please refer to JSeismic documentation for more detail.
     */
    public void setClippingValue(float cv) {
        _clippingValue = cv;
    }

    /**
     * Sets the color map used by the rasterizer. <br>
     * Please refer to JSeismic documentation for more detail.
     */
    public void setColorMap(SeismicColorMap colorMap) {
        if (_colorMap != null) _colorMap.removeColorMapListener(_cml);
        _colorMap = colorMap;
        if (_colorMap != null) {
            updateColorMap();
            _colorMap.addColorMapListener(_cml);
        }
    }

    /**
     * Sets the spacing between traces in screen coordinates(pixels). <br>
     * Please refer to JSeismic documentation for more detail.
     */
    public void setTraceSpace(double d) {
        _deltaPixels = d;
    }

    /**
     * Defines the destination image data array for rasterizer. <br>
     * Please refer to JSeismic documentation for more detail.
     * @param imageData the destination image data array.
     * @param w the width, in bytes, of the data array.
     * @param h the height, in pixels, of the drawable area.
     */
    public void setImageData(byte[] imageData, int w, int h) {
        _imageData = imageData;
        _imageWidth = w;
        _imageHeight = h;
        _scanLineSize = ((_imageWidth - 1) / 4 + 1) * 4;
    }

    /**
     * Rasterizes a seismic trace, no highlight. <br>
     * Please refer to JSeismic documentation for more detail.
     * @param prevTrace the trace previous to this trace, used for interpolation.
     * @param thisTrace the trace that is being rasterized.
     * @param sampleRange the sample range thiat is being rasterized.
     * @param lastTraceLoc the horizontal location (in pixels) of the previous trace.
     * @param thisTraceLoc the horizontal location (in pixels) of this trace.
     */
    public void rasterizeTrace(TraceData prevTrace, TraceData thisTrace,
                                NumberRange sampleRange,
                                int lastTraceLoc, int thisTraceLoc) {
        rasterizeTrace(prevTrace, thisTrace, sampleRange,
                        lastTraceLoc, thisTraceLoc, false);
    }

    /**
     * Rasterizes a seismic trace. <br>
     * Please refer to JSeismic documentation for more detail.
     * @param prevTrace the trace previous to this trace, used for interpolation.
     * @param thisTrace the trace that is being rasterized.
     * @param sampleRange the sample range thiat is being rasterized.
     * @param lastTraceLoc the horizontal location (in pixels) of the previous trace.
     * @param thisTraceLoc the horizontal location (in pixels) of this trace.
     * @param isHighlighted flag indicates if the trace should be highlighted.
     */
    public void rasterizeTrace(TraceData prevTrace, TraceData thisTrace,
                                NumberRange sampleRange, int lastTraceLoc,
                                int thisTraceLoc, boolean isHighlighted) {

        int i, j, k;
        boolean plotType1, plotType2;
        byte pixelType1, pixelType2;

        if (_colorMap == null) return;
        if ((lastTraceLoc==Integer.MIN_VALUE) || (lastTraceLoc==Integer.MAX_VALUE)) {
            lastTraceLoc = thisTraceLoc - (int)_deltaPixels;
        }

        int deltaPixels = thisTraceLoc - lastTraceLoc;
        float[] thisData = thisTrace.getSamples();

        if (_interpFlag) {
            rasterizeTraceInterpolated(prevTrace, thisTrace,
                                        sampleRange, lastTraceLoc,
                                        thisTraceLoc, isHighlighted);
            return;
        }

        int lastDensityLoc = lastTraceLoc + (int)(0.5*_deltaPixels);
        int thisDensityLoc = thisTraceLoc + (int)(0.5*_deltaPixels);
        if (lastDensityLoc < 0) lastDensityLoc = 0;
        if (thisDensityLoc > _imageWidth) thisDensityLoc = _imageWidth;
        byte pixel = 0;
        int sampleColorValue = 0;
        double sampleValue = 0.0;
        int bitmapLineIndex = 0;
        double varC = (getColorMapEndValue() - getColorMapStartValue()) / _numColors;
        int imageStartV = 0;
        int imageEndV = imageStartV + _imageHeight;

        double theColorMapStartValue = this.getColorMapStartValue();
        for (i=imageStartV; i<imageEndV; i++) {
            //sampleValue = thisData[(int)dataIndex];
            sampleValue = thisData[i];
            if (isNullValue(sampleValue)) {
                pixel = this.getColorMap().getBackgroundIndex();
            }
            else {
                sampleColorValue = (int)((sampleValue - theColorMapStartValue) / varC);
                if (sampleColorValue < 0)           sampleColorValue = 0;
                if (sampleColorValue >= _numColors) sampleColorValue = _numColors -1;
                pixel = (byte)sampleColorValue;
            }
            //System.out.println("MapRasterizer (" + i + "," + lastDensityLoc + ") : " + sampleColorValue + " data[" + dataIndex + "]=" + sampleValue);
            for (j=lastDensityLoc; j<thisDensityLoc; j++) {
                //if (_imageData[bitmapLineIndex+j] == _backgroundPixel)
                _imageData[bitmapLineIndex+j] = pixel;
            }
            bitmapLineIndex += _scanLineSize;
            //dataIndex += dataIncrement;
        }
    }

    private boolean isNullValue(double value) {
        if (value == -999.25) return true;
        return false;
    }

    protected void updateColorMap() {
        _numColors = _colorMap.getDensityColorSize();
        _foregroundPixel = _colorMap.getForegroundIndex();
        _backgroundPixel = _colorMap.getBackgroundIndex();
        _highlightPixel = _colorMap.getHighlightIndex();
    }

    private void rasterizeTraceInterpolated(
                                TraceData prevTrace, TraceData thisTrace,
                                NumberRange sampleRange, int lastTraceLoc,
                                int thisTraceLoc, boolean isHighlighted) {
/*
        int i, j, k;
        int deltaPixels = thisTraceLoc - lastTraceLoc;
        int thisTraceId = ((Integer) thisTrace.getKey()).intValue();
        float[] thisData = thisTrace.getSamples();
        //for (int kkkk=0; kkkk<thisData.length; kkkk++)
        //    System.out.println("RR " + thisTraceId + " : " + kkkk+ " " + thisData[kkkk]);
        float[] thisDepth = _reader.getDepthArray(thisTraceId);
        int lastDensityLoc = lastTraceLoc;
        int thisDensityLoc = thisTraceLoc;
        int startOffset = 0;
        if (lastDensityLoc < 0) {
            startOffset = -lastDensityLoc;
            lastDensityLoc = 0;
        }
        if (thisDensityLoc > _imageWidth) thisDensityLoc = _imageWidth;
        int traceWidth = thisDensityLoc - lastDensityLoc;

        float[] prevData;
        float[] prevDepth;
        int prevTraceNumberOfSamples;
        if (prevTrace == null) {
            prevData = thisData;
            prevDepth = thisDepth;
            prevTraceNumberOfSamples = thisTrace.getNumberOfSamples();
        }
        else {
            prevData = prevTrace.getSamples();
            Integer prevId = (Integer)prevTrace.getKey();
            prevDepth = _reader.getDepthArray(prevId.intValue());
            prevTraceNumberOfSamples = prevTrace.getNumberOfSamples();
        }
        double[] varA = new double[deltaPixels];
        double[] varB = new double[deltaPixels];
        //double   varC = _numColors / 2.;
        double varC = _numColors / (_colorMapEndValue - _colorMapStartValue);
        double   b;
        for (i=0; i<deltaPixels; i++) {
            if (_interpolatedDensityPlot) {
                b = ((double)(i + startOffset)) / (deltaPixels - 1);
                varA[i] = (1. - b);
                varB[i] = b;
            }
            else {  // _densityPlot
                if (i < deltaPixels/2) b = 0;
                else b = 1;
                varA[i] = 1 - b;
                varB[i] = b;
            }
        }
        //System.out.println(thisTraceId + " " + traceWidth + " RRRRRRRRRRRRRRR " + (thisDensityLoc-lastDensityLoc) + " : " + sampleRange.getMin() + "," + sampleRange.getMax());
        //int[] prevTurnIndexes = findTurnIndexes(prevData);
        //int[] thisTurnIndexes = findTurnIndexes(thisData);
        int[] prevTurnIndexes = findTurnIndexes(prevDepth, prevTraceNumberOfSamples);
        int[] thisTurnIndexes = findTurnIndexes(thisDepth, thisTrace.getNumberOfSamples());
        //for (int kkkk=0; kkkk<thisTurnIndexes.length; kkkk++)
        //    System.out.println("Trace " + thisTraceId + " : " + kkkk+ " " + thisTurnIndexes[kkkk]);
        double prevSampleValue, thisSampleValue;
        byte pixel = 0;
        int sampleValue = 0;
        int bitmapLineIndex = 0;
        //double[] tmpSampleValueArray = new double[2];
        //int[] tmpSampleIndexArray = new int[3]; // the last integer for layer
        int[] lastLineLayerIndexes = new int[Math.abs(thisDensityLoc - lastDensityLoc)];
        int layerIndex = -1;
        int prevDataIndex = 0;
        int thisDataIndex = 0;
        int realTraceWidth = traceWidth + startOffset;
        int heightStart = sampleRange.getMin();
        int heightEnd = _imageHeight + heightStart;
        for (i = heightStart; i < heightEnd; i++) {
            if (((prevData[i] < BhpModelTraceNormalization.NULL_LIMIT) &&
                 (prevData[i] > BhpModelTraceNormalization.NULL_LIMIT2)) ||
                ((thisData[i] < BhpModelTraceNormalization.NULL_LIMIT) &&
                 (thisData[i] > BhpModelTraceNormalization.NULL_LIMIT2))) {
                bitmapLineIndex += _scanLineSize;
                continue;
            }

            for (j=lastDensityLoc, k=0; j<thisDensityLoc; j++, k++) {
                layerIndex = findLayerIndex(j-lastDensityLoc+startOffset, i,
                                realTraceWidth, prevTurnIndexes, thisTurnIndexes,
                                lastLineLayerIndexes[k]);
                lastLineLayerIndexes[k] = layerIndex;

                if (layerIndex < 1)  continue; // not a normal layer, no draw

                prevDataIndex = Math.max(0, prevTurnIndexes[layerIndex] - 1);
                thisDataIndex = Math.max(0, thisTurnIndexes[layerIndex] - 1);
                prevSampleValue = (prevData[prevDataIndex] - _colorMapStartValue) * varC;
                thisSampleValue = (thisData[thisDataIndex] - _colorMapStartValue) * varC;
                sampleValue = (int)(varA[k] * prevSampleValue + varB[k] * thisSampleValue);
                if (_reverseFill) sampleValue = _numColors - sampleValue - 1;
                if (sampleValue < 0) sampleValue = 0;
                else if (sampleValue >= _numColors) sampleValue = _numColors-1;
                pixel = (byte)sampleValue;
                if (_imageData[bitmapLineIndex+j] == _backgroundPixel) {
                    _imageData[bitmapLineIndex+j] = pixel;
                    //if (tmpSampleIndexArray[2] < 1)    // not in a normal layer
                    //    _imageData[bitmapLineIndex+j] = _backgroundPixel;
                }
            }
            bitmapLineIndex += _scanLineSize;
        }
*/
    }

    /**
     * Sets the plot type. Not supported by this class.
     */
    public void setPlotType(int type) {}
    /**
     * Gets the plot type. Not supported by this class.
     */
    public int getPlotType() { return 0; }
    /**
     * Sets the flag of reverse fill. Not supported by this class.
     */
    public void setReverseFill(boolean r) {}
    /**
     * Gets the flag of reverse fill. Not supported by this class.
     */
    public boolean isReverseFill() { return false; }

    public void rasterizeTrace( RenderingParameters rasterParameters ){
        rasterizeTrace(rasterParameters.getPreviousTrace(),
                rasterParameters.getCurrentTrace(),
                rasterParameters.getSampleRange(),
                rasterParameters.getPreviousTraceLoc(),
                rasterParameters.getCurrentTraceLoc(), false);
    }

   /**
    * Retrives the size of raster.
    */
   public Dimension getRasterSize(){
           return new Dimension(_imageWidth, _imageHeight);
   }

}
