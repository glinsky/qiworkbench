/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


/*############################################################################
# Copyright  2005 BHP Billiton Petroleum, G&W Systems and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the GPL included with the
# distribution.
#
############################################################################*/
package com.bhpBilliton.viewer2d;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.util.Hashtable;
import java.util.Vector;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.bhpBilliton.viewer2d.data.BhpEventReaderInterface;
import com.bhpBilliton.viewer2d.data.GeneralDataSource;
import com.bhpBilliton.viewer2d.ui.BhpBusyDialog;
import com.bhpBilliton.viewer2d.ui.util.ElementAttributeReader;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.gw2d.model.Attribute2D;
import com.gwsys.gw2d.model.CommonDataModel;
import com.gwsys.gw2d.model.CommonShapeLayer;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;
import com.gwsys.seismic.core.DataChooser;
import com.gwsys.seismic.core.SeismicWorkflow;
import com.gwsys.seismic.indexing.SegyHeaderFormat;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.util.SeismicColorMap;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class is used to display event/horizon data.
 *               The data for an event takes the form of one sample traces.
 *               An event will be displayed as a polyline, or polylines, if
 *               there are null values(holes) in the data.
 *               There is also an option to draw symbols for the event.
 *               User can pick the event and edit it with mouse.
 *               After editing, the new data can be saved back.
 *               A special pipeline
 *               <code>{@link BhpEventPipeline}</code> is created for event
 *               data processing.  <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpEventLayer extends BhpLayer implements BhpSeismicLayerListener {
    private RenderingAttribute _initAtt;
    private Element _shapeSetting;
    //private BhpEventLayerListener _listener;
    private Vector _listeners;
    private BhpEventShape _refShape;

    private int _refSeismicLayerId;
    private int _layerSnapMode = BhpPickingManager.SNAP_NO;
    private int _layerSnapLimit = BhpPickingManager.SNAP_LIMIT_DEFAULT;
    //LM: track whether this layer has been changed since the last save
    private boolean changedSinceLastSave;

    private BhpViewerBase viewer;
    public BhpViewerBase getViewer() {
        return viewer;
    }

    /**
     * Creates a model layer with a reference layer. <br>
     * This construtor is usually used when a new layer is created
     * by the users with GUI control.
     * @param viewer the parent viewer of the new layer.
     * @param win the parent window of the new layer.
     * @param id the identification number of the new layer.
     * @param dstype the name of the data source.
     * @param name name of the selected dataset.
     *        This is also the default name of the layer.
     * @param pathlist path of the selected dataset. <br>
     *        For bhpio data, it is the pathlist. For OpenSpirit data,
     *        it is the session name.
     * @param property the name of the selected event. <br>
     * @param propertySync the boolean flag for property synchronization.
     * @param parameter a table model stores the user data selection.
     *        In the case of bhpio, it can be used to construct the
     *        query string used to run bhpio.
     * @param ref the reference layer. <br>
     *        The new layer will inherit from this reference layer things
     *        like scales, pipeline settings, display options, and etc.
     * @param refShape the reference shape. <br>
     *        The new event shape will inherit its graphic settings
     *        from the reference shape.
     */
    public BhpEventLayer(BhpViewerBase viewer, BhpWindow win,
                         int id, String dstype, String name, String pathlist,
                         String property, boolean propertySync,
                         BhpSeismicTableModel parameter,
                         BhpLayer ref, BhpEventShape refShape) {
        super(viewer, win, id, dstype, name, property, pathlist,
              parameter, property, propertySync, ref, BhpViewer.BHP_DATA_TYPE_EVENT);
        this.viewer = viewer;
        //super.setCacheMode(com.interactive.jcarnac2d.view.cgPlotView.CG_VIEW_SIZE_CACHE);
        _initAtt = null;
        _listeners = new Vector();
        _shapeSetting = null;
        _refShape = refShape;

        _layerSnapMode = BhpPickingManager.SNAP_NO;
        _refSeismicLayerId = -1;
        if (!parameter.getDataSource().getDataSourceName().equals(GeneralDataSource.DATA_SOURCE_BHPSU)) {
            // bhpsu data has independent event layer
            // dependent event layer won't talk.
            _enableTalk = false;
            if ((ref != null) && (ref instanceof BhpSeismicLayer)) {
                _refSeismicLayerId = ref.getIdNumber();
            }
        }

    }

    /**
     * Creates a new model layer with specified attributes. <br>
     * This construtor is usually used when a new layer is created
     * parsing saved XML configuration file.
     * @param viewer the parent viewer of the new layer.
     * @param win the parent window of the new layer.
     * @param id the identification number of the new layer.
     * @param dstype the name of the data source.
     * @param dname name of the selected dataset.
     * @param name name of the new layer.
     * @param pathlist path of the selected dataset. <br>
     *        For bhpio data, it is the pathlist. For OpenSpirit data,
     *        it is the session name.
     * @param property the name of the selected event. <br>
     * @param parameter a table model stores the user data selection.
     *        In the case of bhpio, it can be used to construct the
     *        query string used to run bhpio.
     * @param mupt horizontal scale with unit of model-unit-per-trace.
     * @param mups vertical scale with unit model-unit-per-sample.
     * @param talk a boolean indicates if the new layer should
     *        broadcase its change event.
     * @param syn the synchronization flag of the new layer.
     * @param trans transparency(opacity) of the new layer.
     * @param propertySync a boolean indicates if the new layer will be
     *        synchronized for event name.
     * @param ls event line style.
     * @param lw event line width.
     * @param lc event line color.
     * @param pnode the pipeline node in the DOM tree. <br>
     *        It is used to initialize the pipeline of the new layer.
     * @param rp a boolean for polarity.
     * @param shapeSetting the event shape graphic setting element in the DOM tree.
     */
    public BhpEventLayer(BhpViewerBase viewer, BhpWindow win, int id, String dstype,
                         String dname, String name, String pathlist,
                         String property, BhpSeismicTableModel parameter,
                         double mupt, double mups, boolean talk, int syn,
                         int trans, boolean propertySyn, int refSeismicLayerId,
                         int ls, float lw,
                         Color lc, Node pipelineNode, boolean rp, Element shapeSetting) {
        super(viewer, win, id, dstype, dname, name, pathlist,
              parameter, property, talk, syn, trans, propertySyn,
              pipelineNode, BhpViewer.BHP_DATA_TYPE_EVENT, rp, false);
        this.viewer = viewer;

        _hscaleFactor = mupt;
        _vscaleFactor = mups;
        _initAtt = new RenderingAttribute();
        _initAtt.setLineStyle(ls);
        _initAtt.setLineWidth(lw);
        _initAtt.setLineColor(lc);
        _listeners = new Vector();
        _shapeSetting = shapeSetting;
        _refSeismicLayerId = refSeismicLayerId;
        // independent event layer won't talk.
        _enableTalk = false;

    }

    /**
     * Converts the model layer object into an XML string. <br>
     * This is used when save the working session into an XML file.
     * Please refer to BhpViewer.dtd for the detail of the format.
     * @return an XML string for the object.
     */
    public String toXMLString() {
        if (_dataSource.equals("TemporaryEventDataSource")) return "";
        StringBuffer content = new StringBuffer();
        content.append("        " + "<BhpEventLayer ");
        content.append("refSeismicLayerId=\"" + _refSeismicLayerId + "\" ");
        content.append("eventLineStyle=\"" + _shape.getAttribute().getLineStyle() + "\" ");
        content.append("eventLineWidth=\"" + _shape.getAttribute().getLineWidth() + "\" ");
        content.append("eventLineColor=\"" + BhpViewerHelper.toStringColor(
                                             _shape.getAttribute().getLineColor()) + "\">\n");
        content.append(super.toXMLString());

        content.append("            " + "<BhpEventShapeSetting ");
        content.append("eventDrawLine=\"" + _shape.isDrawLine() + "\" ");
        content.append("eventDrawSymbol=\"" + _shape.isDrawSymbol() + "\" ");
        content.append("eventDrawAllLine=\"" + _shape.isDrawAllLine() + "\" ");
        content.append("eventSymbolStyle=\"" + _shape.getSymbolStyle() + "\" ");
        content.append("eventSymbolWidth=\"" + _shape.getSymbolWidth() + "\" ");
        content.append("eventSymbolHeight=\"" + _shape.getSymbolHeight() + "\" ");
        content.append("eventSymbolLineStyle=\"" + _shape.getSymbolAttribute().getLineStyle() + "\" ");
        content.append("eventSymbolFillStyle=\"" + _shape.getSymbolAttribute().getFillStyle() + "\" ");
        content.append("eventSymbolColor=\"" + BhpViewerHelper.toStringColor(
                                                _shape.getSymbolAttribute().getLineColor()) + "\">\n");
        content.append("            " + "</BhpEventShapeSetting>\n");

        content.append("        " + "</BhpEventLayer>\n");
        return content.toString();
    }

    public int getLayerSnapMode() { return _layerSnapMode; }
    public void setLayerSnapMode(int s) { _layerSnapMode = s; }
    
    public int getLayerSnapLimit() { return _layerSnapLimit; }
    public void setLayerSnapLimit(int layerSnapLimit) { _layerSnapLimit = layerSnapLimit; }

    public int getRefSeismicLayerId () { return _refSeismicLayerId; }
    public BhpSeismicLayer getRefSeismicLayer() {
        BhpSeismicLayer re = null;
        BhpLayer generalRef = _window.getBhpPlot().getBhpLayer(_refSeismicLayerId);
        if ((generalRef != null) && (generalRef instanceof BhpSeismicLayer)) {
            re = (BhpSeismicLayer) generalRef;
        }
        return re;
    }

    /**
     * Sets the visibility of this event layer. <br>
     * This method overrides the implementation in
     * <code>{@link BhpLayer}</code>
     * to inform <code>{@link BhpEventLayerListener}</code>
     * about the chagne.
     */
    public void setBhpLayerVisible(boolean v) {
        _shapeListLayer.setVisible(v);
        notifyEventLayerListeners();
    }

    /**
     * Transforms a vertical model space coordinate to virtual vertical value. <br>
     * This method is used for mouse click data synchronization.
     * It overrides the implementation in <code>{@link BhpLayer}</code>.
     * @param modelV vertical model sapce coordinate.
     * @return vertical axis reading.
     */
    public double findVerticalReadingFromModel(double modelV) {
        return modelV;
    }

    /**
     * Transforms a virtual vertical value to model space coordinate. <br>
     * This method is used for mouse click data synchronization.
     * It overrides the implementation in <code>{@link BhpLayer}</code>.
     * @param readingV vertical axis reading
     * @return model space coordinate
     */
    public int findModelFromVerticalReading(double readingV) {
        return (int) readingV;
    }

    public void changeTemporaryDataSource(String fname) {
        if (_dataSource.equals("TemporaryEventDataSource")) {
            _dataSource = GeneralDataSource.DATA_SOURCE_XMLHRZ;
            setDataName(fname);
            setPathlist(fname);
            ((TemporaryEventDataSource)(_parameter.getDataSource())).
                    setFileName(fname);
        }
    }

    /**
     * Saves the event back to its original data source. <br>
     * Only classes implementing
     * <code>{@link BhpEventReaderInterface}</code>
     * support event saving.
     */
    public void saveEvent() {
        if ((_pipeline==null) || !(_pipeline instanceof BhpEventPipeline)) {
            return;
        }
        BhpSeismicLayer seismicRef = getRefSeismicLayer();
        // for bhpsu data, it is always null
        try {
            if (seismicRef != null) {
                ((BhpEventReaderInterface)_reader).saveEvent();
            }
            else if (_shape != null) {
                ((BhpEventReaderInterface)_reader).saveEvent(getPropertyName(), _shape.getValues());
                //((BhpEventDataSelector)(_pipeline.getDataSelector())).saveEvent(getPropertyName(), _shape.getValues(), null);
            }
            else {
                ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                    "BhpEventLayer.saveEvent failed.");
            }
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "Exception when saving horizon layer " + getIdNumber() +
                                                    ": " + ex.toString());
        }
    }

    public void updateMemoryEventValueAt(int index, double value) {
        BhpSeismicLayer seismic = this.getRefSeismicLayer();
        if (seismic == null) {
            return;
        }

        Vector fields = new Vector();
        for (int i=0; i<seismic.getParameter().getRowCount()-1; i++) {
            fields.add(seismic.getParameter().getValueAt(i, 0).toString());
        }
        Hashtable param = new Hashtable(fields.size());
        Vector formats = seismic.getPipeline().getDataLoader().getDataReader().getDataFormat().getTraceHeaderFormats();
        SegyHeaderFormat headerFormat = (SegyHeaderFormat) (formats.firstElement());
        TraceHeader seismicMeta = null;
        Hashtable headerValues = null;
        String hname;
        try {
            seismicMeta = seismic.getPipeline().getDataLoader().
            getDataSelector().getTraceHeader(index);
            if (seismicMeta == null) return;
            headerValues = seismicMeta.getDataValues();
            for (int k=0; k<fields.size(); k++) {
                hname = (String) (fields.get(k));
                Integer hId = BhpSegyFormat.getFieldForName(hname, headerFormat);
                if (hId != null) {
                    Object headerValue = headerValues.get(hId);
                    param.put(hname, headerValue);
                }
            }
            ((BhpEventReaderInterface)_reader).putEventValueAt(
                    getPropertyName(), value, param);
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "BhpEventLayer.updateMemoryEventValueAt exception:" + ex.getMessage());
        }
    }

    /**
     * Retrieves the event data values.
     */
    public double[] getEventValues() {
        if (_shape != null) return _shape.getValues();
        return null;
    }

    public void setEventValueAt(int index, double value, boolean reset) {
        BhpSeismicLayer seismic = getRefSeismicLayer();
        double valueReading = value;
        if (seismic != null)
            valueReading = value + seismic.getParameter().getVerticalSettings()[0];
        updateMemoryEventValueAt(index, valueReading);
        _shape.setValueAt(index, value, true);
        _window.getBhpEventGraphPlot().eventValueChanged(
                index, valueReading, reset, getIdNumber());
        //_eventGraphPlot.eventValueChanged(thisIndex, tmpValue, _view.getIdNumber());
        _viewer.broadcastBhpLayerEvent(new BhpLayerEvent(BhpViewer.EVENTVALUE_FLAG, this));
    }
    /**
     * Sets the event data values.
     * This method is only used by BhpEventDataTable,
     * applying the values user typed into the table.
     */
    public void setEventValues(double[] values) {
        if (_shape != null) _shape.setValues(values);

        BhpSeismicLayer seismic = getRefSeismicLayer();
        double verticalReadingStart = 0;
        if (seismic != null)
            verticalReadingStart = seismic.getParameter().getVerticalSettings()[0];
        for (int i=0; i<values.length; i++) {
            updateMemoryEventValueAt(i, values[i]+verticalReadingStart);
        }
        notifyEventLayerListeners();
        _viewer.broadcastBhpLayerEvent(new BhpLayerEvent(BhpViewer.EVENTVALUE_FLAG, this));
    }

    /**
     * Sets the listener of the layer. <br>
     * The listener will be informed when the event display
     * is changed.
     */
    public void addEventLayerListener(BhpEventLayerListener l) {
        if (!_listeners.contains(l)) {
            _listeners.add(l);
        }
    }

    public void removeEventLayerListener(BhpEventLayerListener l) {
        if (_listeners.contains(l)) {
            _listeners.remove(l);
        }
    }

    /**
     * This method calculates the device position according
     * to the trace id and the virtual vertical value.
     * @param tid trace id.
     * @param vvalue virtual vertical reading.
     * @return a Point in the device space.
     */
    public Point reverseSelectByPoint(int tid, double vvalue) {
        int resultx = 0;
        int resulty = 0;

        DataChooser dataSelector = _pipeline.getDataLoader().getDataSelector();
        if (tid == -999) {
            tid = 0;
        }
        else if (tid > dataSelector.getVirtualModelLimits().getMaxX()) {
            tid = (int) (dataSelector.getVirtualModelLimits().getMaxX());
        }

        double interx = tid + 0.5;
        double intery = vvalue + 0.5;
        java.awt.geom.Point2D resultPos = _transformation.transform(
                new java.awt.geom.Point2D.Double(interx, intery),
                new java.awt.geom.Point2D.Double(0, 0));
        resultx = (int) resultPos.getX();
        resulty = (int) resultPos.getY();
        return new Point(resultx, resulty);
    }

    /**
     * This method calculates the virtual position according to
     * the device position.
     * @return a Point where x is the virtual trace id,
     * and y is the virtual vertical position
     */
    public Point selectByPoint(Point deviceP) {
        int virtTraceId = -999;
        int virtualY = 0;
        if (_pipeline != null) {
            int finalx = 0;
            int finaly = 0;
            try {
                java.awt.geom.Point2D finalPos = _transformation.inverseTransform(
                                new Point(deviceP.x, deviceP.y),
                                new java.awt.geom.Point2D.Double(0, 0));
                finalx = (int) Math.floor(finalPos.getX());
                finaly = (int) Math.floor(finalPos.getY());
            }
            catch (Exception ex) {
                ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                    "BhpEventLayer.selectByPoint exception : " + ex.getMessage());
            }
/*
            Bound2D box = _transformation.inverseTransform(
                    new Bound2D(0, 0, deviceP.x+position.x, deviceP.y+position.y));
            int finalx = (int) (Math.floor(box.getRight()));
            int finaly = (int) (Math.floor(box.getTop()));
*/
            virtualY = finaly;
            DataChooser dataSelector = _pipeline.getDataLoader().getDataSelector();
            if (finalx >= dataSelector.getVirtualModelLimits().getMaxX()) {
                virtTraceId = (int)(dataSelector.getVirtualModelLimits().getMaxX()) - 1;
            }
            else {
                virtTraceId = finalx;
            }
        }
        Point result = new Point(virtTraceId, virtualY);
        return result;
    }

    public void updateLayer(boolean redraw) {
        double modelUnitPerTrace = getLayerHorizontalScale();
        double modelUnitPerSample = getLayerVerticalScale();
        boolean oldNotify = _shape.isNotificationEnabled();
        _shape.setNotification(false);
        _shape.setScale(modelUnitPerTrace, modelUnitPerSample);
        _shape.setNotification(oldNotify);
        int numberOfTraces = _reader.getMetaData().getNumberOfTraces();
        BhpLayer refLayer = getRefSeismicLayer();
        if (refLayer != null) {
            numberOfTraces = (int) refLayer.getPipeline().getDataLoader().
                             getDataSelector().getVirtualModelLimits().width;
        }

        DataChooser theSelector = _pipeline.getDataLoader().getDataSelector();
        if (refLayer != null) {
               theSelector = refLayer.getPipeline().getDataLoader().getDataSelector();
        }
        double minDepth = theSelector.getStartSampleValue();
        double maxDepth = theSelector.getEndSampleValue();
        double modelWidth = numberOfTraces * modelUnitPerTrace;
        double modelHeight = Math.abs(maxDepth-minDepth) * modelUnitPerSample;
        //double modelHeight = Math.abs(theReader.getMaxDepth()-theReader.getMinDepth()) * _modelUnitPerSample;

        //_modelBox = new Bound2D(0, minDepth, modelWidth, minDepth+modelHeight);
        _modelBox = new Bound2D(0, minDepth, numberOfTraces, maxDepth);
        _deviceBox = new Bound2D(0, 0, modelWidth, modelHeight);
        this.setLimits((int)modelWidth, (int)modelHeight);
        _containerModel.setBoundingBox(_modelBox);
        _shapeListLayer.setBoundingBox(_modelBox);
        _transformation = new Transform2D(_modelBox, _deviceBox, false, false);
        if (redraw) {
            this.setTransformation(_transformation);

            AbstractPlot ppp = _window.getBhpPlot();
            if (ppp.getAxisAssociateLayer() == this.getIdNumber()) {
                ppp.updateHAxes();
                ppp.updateVAxes();
                ppp.updateMiscAnnotation();
            }
        }
        else {  // transformation changed here
            this.setTransformationWithoutRepaint(_transformation);
        }
/*
        _image.setScale(_modelUnitPerTrace, _modelUnitPerSample);
        double minDepth = 0;
        double modelWidth = _reader.getMetaData().getNumberOfTraces() * _modelUnitPerTrace;
        BhpModelTraceRasterizer rasterizer = (BhpModelTraceRasterizer) _pipeline.getTraceRasterizer();
        double modelHeigth = (rasterizer.getEndDepth() -
                              rasterizer.getStartDepth()) *
                              _modelUnitPerSample;

        _modelBox = new Bound2D(0, minDepth, modelWidth, minDepth+modelHeigth);
*/

        notifyEventLayerListeners();
    }

    public void updateSeismicDisplay() {
        updateSeismicDisplay(true);
    }

    private void updateSeismicDisplay(boolean loadingInvolved) {

        BhpEventShape oldShape = _shape;
        BhpEventDataSelector selector = (BhpEventDataSelector) (
                _pipeline.getDataLoader().getDataSelector());
        double oldStartDepth = selector.getStartSampleValue();
        double oldEndDepth = selector.getEndSampleValue();
        _pipeline.getDataLoader().setDataReader(_reader);
        BhpEventDataSelector newSelector = new BhpEventDataSelector(viewer);
        _pipeline.setDataSelector(newSelector);
        if (selector.getStartSampleChanged())
            newSelector.setStartSampleValue(oldStartDepth);
        if (selector.getEndSampleChanged())
            newSelector.setEndSampleValue(oldEndDepth);

        BhpLayer generalRef = _window.getBhpPlot().getBhpLayer(_refSeismicLayerId);
        BhpSeismicLayer seismicRef = null;
        if ((generalRef != null) && (generalRef instanceof BhpSeismicLayer)) {
            seismicRef = (BhpSeismicLayer) generalRef;
            seismicRef.addSeismicLayerListener(this);
        }
        if (seismicRef != null) {
            newSelector.setStartSampleValue(
                    seismicRef.getPipeline().getDataLoader().getDataSelector().getStartSampleValue());
            newSelector.setEndSampleValue(
                    seismicRef.getPipeline().getDataLoader().getDataSelector().getEndSampleValue());
        }
        _pipeline.invalidate(_reader);

        int numberOfTraces = _reader.getMetaData().getNumberOfTraces();
        double modelUnitPerTrace = getLayerHorizontalScale();
        double modelUnitPerSample = getLayerVerticalScale();
        double modelWidth = numberOfTraces * modelUnitPerTrace;
        BhpEventReaderInterface theReader = (BhpEventReaderInterface) _reader;
        double minDepth = newSelector.getStartSampleValue();
        double maxDepth = newSelector.getEndSampleValue();
        double modelHeight = Math.abs(maxDepth-minDepth) * modelUnitPerSample;
        _modelBox = new Bound2D(0, minDepth, numberOfTraces, maxDepth);
        _deviceBox = new Bound2D(0, 0, modelWidth, modelHeight);
        _transformation = new Transform2D(_modelBox, _deviceBox, false, false);

        _shapeListLayer.removeShape(_shape);
        _containerModel.setBoundingBox(_modelBox);
        _shapeListLayer.setBoundingBox(_modelBox);

        //float[] yvalues = ((BhpEventPipeline)_pipeline).getEvent(_propertyName);
        float[] yvalues =
                ((BhpEventPipeline)_pipeline).getEvent(_propertyName,
                (BhpSeismicLayer)(_window.getBhpPlot().getBhpLayer(_refSeismicLayerId)));
        float xstart = 0;
        float xstep = 1;
        _shape = new BhpEventShape(yvalues, xstart, xstep,
                        theReader.getMinDepth(), theReader.getMaxDepth(),
                        modelUnitPerTrace, modelUnitPerSample,
                        _reader.getDefinedNullValue());

        //_shape.setAttribute(oldAttribute);
        if (theReader.getEventAttribute(_propertyName) != null) {
            _shape.setAttribute(theReader.getEventAttribute(_propertyName));
        }
        _shape.setEventAttribute(oldShape);

        _shapeListLayer.removeShape(oldShape);
        oldShape = null;

        _shapeListLayer.addShape(_shape);
        this.setTransformation(_transformation);

        AbstractPlot ppp = _window.getBhpPlot();
        if (ppp.getAxisAssociateLayer() == this.getIdNumber()) {
            ppp.updateHAxes();
            ppp.updateVAxes();
            ppp.updateMiscAnnotation();
        }
        this.setTransformationWithoutRepaint(this.getTransformation());
        Dimension wsize = _window.getSize();
//        _window.setSize((int) (wsize.getWidth()-1), (int) (wsize.getHeight()-1));
        _window.setSize((int) wsize.getWidth(), (int) wsize.getHeight());

        if (loadingInvolved)
            _viewer.changeLoadingLayerNumber(BhpBusyDialog.RMV_LAYER);

        notifyEventLayerListeners();
    }

    public double getTimeRangeStart() {
        return _pipeline.getDataLoader().getDataSelector().getStartSampleValue();
    }

    public double getTimeRangeEnd() {
        return _pipeline.getDataLoader().getDataSelector().getEndSampleValue();
    }

    public void setTimeRange(double start, double end) {
        _pipeline.getDataLoader().getDataSelector().setStartSampleValue(start);
        _pipeline.getDataLoader().getDataSelector().setEndSampleValue(end);
        _pipeline.invalidate(_pipeline.getDataLoader().getDataSelector());
    }

    public void seismicLayerChanged(BhpLayer source, int flag) {
        //System.out.println("Seismic changed " + source.getIdNumber());
        // there is a problem when using this flag.
        // but, update display all the time make it very slow for zooming.
        // Wait to find out what exactly is the problem.
        if (flag == BhpSeismicLayer.SEISMIC_IMAGE_UPDATED)
            this.updateLayer();
        else
            this.updateSeismicDisplay(false);
    }

    public boolean receiveBhpLayerEvent(BhpLayerEvent e) {
        BhpLayer layer = e.getSource();
        int flag = e.getTypeFlag();

        if (layer.getIdNumber() == this.getIdNumber()) {
            if ((flag & BhpViewer.COLORBAR_FLAG) != 0) { // use my transparent value
                updateColorMap((SeismicColorMap) e.getParameter());
            }
            return false;
        }

        int mflag = super.handleBhpLayerEvent(e);

        if (((flag & BhpViewer.EVENTNAME_FLAG)!=0) && (this.getPropertySync())) {
            String properties = this.getParameter().getProperties();
            String np = layer.getPropertyName();
            if (properties!=null && properties.length()>0 &&
                properties.indexOf(np)!=-1) {
                this.setPropertyName(np);
                mflag = mflag | BhpViewer.EVENTNAME_FLAG;
            }
        }
        if (((flag & BhpViewer.EVENTATT_FLAG)!=0) && ((this.getSynFlag() & BhpViewer.EVENTATT_FLAG)!=0)) {
            BhpEventLayer elayer = (BhpEventLayer) layer;
            //_shape.setAttribute(elayer._shape.getAttribute());
            _shape.setLineAttribute(elayer._shape.getAttribute());
            _shape.setDrawLine(elayer._shape.isDrawLine());
            _shape.setDrawSymbol(elayer._shape.isDrawSymbol());
            _shape.setDrawAllLine(elayer._shape.isDrawAllLine());
            _shape.setSymbolStyle(elayer._shape.getSymbolStyle());
            _shape.setSymbolSize(elayer._shape.getSymbolWidth(),
                                 elayer._shape.getSymbolHeight());
            _shape.setSymbolAttribute(elayer._shape.getSymbolAttribute().getLineStyle(),
                                      elayer._shape.getSymbolAttribute().getFillStyle(),
                                      elayer._shape.getSymbolAttribute().getLineColor());
            mflag = mflag | BhpViewer.EVENTATT_FLAG;
        }

        if (((mflag & BhpViewer.DATAINCREMENT_FLAG)!=0) ||
            ((mflag & BhpViewer.DATASELECTION_FLAG)!=0) ||
            ((mflag & BhpViewer.EVENTNAME_FLAG)!=0) ||
            ((mflag & BhpViewer.NAME_FLAG)!=0)) {
            this.generateLayer();
        }
        else if (mflag != 0) {
            this.updateLayer();
        }
        return false;
    }

    public void setupSeismicDisplay() {
        _pipeline = new BhpEventPipeline(this.viewer, _reader);

        if (_pipelineNode != null) {
            BhpViewerHelper.initWithXML(viewer, _pipelineNode, _pipeline);
            _pipelineNode = null;
        }
        else {
            if (_sampleLayer != null)
                initPipelineWithAnother(_sampleLayer.getPipeline());
        }

        BhpLayer refLayer = getRefSeismicLayer();
        int numberOfTraces = _reader.getMetaData().getNumberOfTraces();
        if (refLayer != null) {
             numberOfTraces = (int) refLayer.getPipeline().getDataLoader().
                              getDataSelector().getVirtualModelLimits().width;
        }
        else if (_sampleLayer != null) {
            numberOfTraces = (int) _sampleLayer.getPipeline().getDataLoader().
                             getDataSelector().getVirtualModelLimits().width;
        }
        double modelUnitPerTrace = getLayerHorizontalScale();
        double modelUnitPerSample = getLayerVerticalScale();
        double modelWidth = numberOfTraces * modelUnitPerTrace;
        BhpEventReaderInterface theReader = (BhpEventReaderInterface) _reader;
        DataChooser theSelector = _pipeline.getDataLoader().getDataSelector();
        if (refLayer != null) {
               theSelector = refLayer.getPipeline().getDataLoader().getDataSelector();
        }

        double minDepth = theSelector.getStartSampleValue();
        double maxDepth = theSelector.getEndSampleValue();

        double modelHeight = Math.abs(maxDepth-minDepth) * modelUnitPerSample;
        _modelBox = new Bound2D(0, minDepth, numberOfTraces, maxDepth);

        _deviceBox = new Bound2D(0, 0, modelWidth, modelHeight);

        _transformation = new Transform2D(_modelBox, _deviceBox, false, false);

        _shapeListLayer = new CommonShapeLayer();
        _containerModel = new CommonDataModel();
        _containerModel.addLayer(_shapeListLayer);
        _containerModel.setBoundingBox(_modelBox);
        _shapeListLayer.setBoundingBox(_modelBox);

        BhpSeismicLayer seismicRef = null;
        if ((refLayer != null) && (refLayer instanceof BhpSeismicLayer)) {
            seismicRef = (BhpSeismicLayer) refLayer;
            seismicRef.addSeismicLayerListener(this);
        }

        // we need to get the value from the pipeline
        if (_dataSource.equals("TemporaryEventDataSource")) {
            // first display of a new horizon, all values are null anyway.
            seismicRef = null;
        }
        float[] yvalues = ((BhpEventPipeline)_pipeline).getEvent(_propertyName, seismicRef);
        if (yvalues == null) {
            _viewer.changeLoadingLayerNumber(BhpBusyDialog.RMV_LAYER_ERROR);
            return;
        }
        float xstart = 0;
        float xstep = 1;
        _shape = new BhpEventShape(yvalues, xstart, xstep,
                        theReader.getMinDepth(), theReader.getMaxDepth(),
                        modelUnitPerTrace, modelUnitPerSample,
                        _reader.getDefinedNullValue());

        Attribute2D att = theReader.getEventAttribute(_propertyName);
        if (att == null) {
            att = new RenderingAttribute(_containerModel);
            att.setLineStyle(Attribute2D.LINE_STYLE_SOLID);
            att.setLineColor(Color.red);
            att.setFillStyle(Attribute2D.FILL_STYLE_EMPTY);
        }
        if (_initAtt != null) {
            att.setLineStyle(_initAtt.getLineStyle());
            att.setLineWidth(_initAtt.getLineWidth());
            att.setLineColor(_initAtt.getLineColor());
            _initAtt = null;
        }
        _shape.setAttribute(att);

        if (_shapeSetting != null) {
            initShapeSettingWithXML(_shapeSetting);
            _shapeSetting = null;
        }
        else if (_refShape != null || _sampleLayer != null) {
            if (_refShape == null) {
                if (_sampleLayer instanceof BhpEventLayer)
                    _refShape = ((BhpEventLayer)_sampleLayer).getEventShape();
            }
            if (_refShape != null) _shape.setEventAttribute(_refShape);
        }

        _shapeListLayer.addShape(_shape);
        this.setModel(_containerModel);
        this.setTransformation(_transformation);

        _viewer.addBhpLayer(_window, this);
        _viewer.changeLoadingLayerNumber(BhpBusyDialog.RMV_LAYER);
    }

    private void initPipelineWithAnother(SeismicWorkflow plR) {

        DataChooser sltr = _pipeline.getDataLoader().getDataSelector();
        DataChooser splr = plR.getDataLoader().getDataSelector();
        sltr.applyGaps(splr.isGapApplied());
        sltr.setGapInTraces(splr.getGapInTraces());

        sltr.setStartSampleValue(splr.getStartSampleValue());
        sltr.setEndSampleValue(splr.getEndSampleValue());
        BhpLayer generalRef = _window.getBhpPlot().getBhpLayer(_refSeismicLayerId);
        if (generalRef != null && (generalRef instanceof BhpSeismicLayer)) {
            sltr.setStartSampleValue(
                    generalRef.getPipeline().getDataLoader().getDataSelector().getStartSampleValue());
            sltr.setEndSampleValue(
                    generalRef.getPipeline().getDataLoader().getDataSelector().getEndSampleValue());
        }
    }

    private void initShapeSettingWithXML(Element ss) {
        ElementAttributeReader ear = new ElementAttributeReader(ss);
        boolean drawAllLine = ear.getBoolean("eventDrawAllLine",true);
        boolean drawSymbol = ear.getBoolean("eventDrawSymbol",true);
        boolean drawLine = ear.getBoolean("eventDrawLine",true);

        int symbolStyle = ear.getInt("eventSymbolStyle");
        float symbolW = ear.getFloat("eventSymbolWidth");
        float symbolH = ear.getFloat("eventSymbolHeight");
        int symbolLS = ear.getInt("eventSymbolLineStyle");
        int symbolFS = ear.getInt("eventSymbolFillStyle");
        Color symbolColor = ear.getColor("eventSymbolColor");

        _shape.setDrawLine(drawLine);
        _shape.setDrawSymbol(drawSymbol);
        _shape.setDrawAllLine(drawAllLine);

        _shape.setSymbolSize(symbolW, symbolH);
        _shape.setSymbolStyle(symbolStyle);
        _shape.setSymbolAttribute(symbolLS, symbolFS, symbolColor);
    }

    private void notifyEventLayerListeners() {
        if (_listeners.size() > 0) {
            for (int i=0; i<_listeners.size(); i++)
                ((BhpEventLayerListener)_listeners.get(i)).eventLayerChanged(this);
        }
    }

    public BhpEventShape getEventShape(){
        return _shape;
    }

    @Override
    public int getMissingDataSelection() {
        return 0;
    }

}
