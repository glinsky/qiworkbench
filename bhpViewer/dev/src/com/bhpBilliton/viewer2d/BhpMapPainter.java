/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;

import com.gwsys.gw2d.model.ScenePainter;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;
import com.gwsys.seismic.core.SegyTraceImage;
import com.gwsys.seismic.core.SeismicWorkflow;
import com.gwsys.seismic.core.TraceImage;
import com.gwsys.seismic.core.TraceImageCreator;
import com.gwsys.seismic.indexing.SeismicConstants;
import com.gwsys.seismic.reader.SeismicReader;
import com.gwsys.seismic.util.SeismicColorMap;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  Special version of SegyTraceImage.
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpMapPainter extends SegyTraceImage {
	public static final double ROUNDING_ERROR = 0.00001;
    private boolean _transpose;
    private double[] _flatF = {0, 1, 1, 0};

    /**
     * Constructs a new instance and set the transpose to false by default. <br>
     * Please refer to JSeismic documentation for more detail.
     */
    public BhpMapPainter(double x, double y,
                         double modelUnitPerTrace, double modelUnitPerSample,
                         SeismicWorkflow pipeline) {
        super(x, y, modelUnitPerTrace, modelUnitPerSample, pipeline);
        _transpose = false;
    }

    /**
     * Finds out if the image is set to transpose.
     */
    public boolean getTranspose() { return _transpose; }
    /**
     * Sets the transpose flag.
     */
    public void setTranspose(boolean s) {
        _transpose = s;
    }

    public void updateMapImageSize() {
        if (_transpose)
            setSize(calculateImageHeight(), calculateImageWidth());
    }

    /**
     * Renders the image.
     * This method overrides the implementation of its
     * super class to take care of transposed image.
     * Please refer to JSeismic documentation for more detail.
     */
    public void render (ScenePainter shr, Bound2D bbox) {

        if (_transpose) {
            renderTranspose(shr, bbox);
            return;
        }
        super.render(shr, bbox);
        //Transform2D trans=new Transform2D();

        //shr.drawImage(0,0, trans, createDataImage());
    }

    private void renderTranspose(ScenePainter shr, Bound2D bbox) {

        if (isVisible() && getAttribute() != null) {
            if (getPipeline() == null) return;
            if (shr.getClipping() == null) return;

            setSize(calculateImageHeight(), calculateImageWidth());
            Transform2D shrT = shr.getTransformation();
            Bound2D clipBox = new Bound2D(shr.getClipping());
            Bound2D modelBox = shrT.inverseTransform(clipBox);

            Bound2D vl = getPipeline().getVirtualModelLimits();
            Bound2D sl = getBoundingBox(shrT);
            Transform2D trf = new Transform2D(_flatF);

            if (super.getDirection() == TraceImage.DRAW_TRACE_RIGHT_TO_LEFT) {
                // need inverse as well
                trf.scale(-1, 1);
                trf.translate(-vl.width, 0);
            }

            Transform2D trs = new Transform2D(
                                        new Bound2D(0, 0, vl.height, vl.width),
                                        sl, false, false);

            if (modelBox.intersects(sl.x, sl.y, sl.width, sl.height)) {
                modelBox = modelBox.intersection(sl);
                clipBox = shrT.transform(modelBox);
            }
            else return;

            Bound2D modelBoxSF = trs.inverseTransform(modelBox);
            Bound2D modelBoxS  = trf.inverseTransform(modelBoxSF);

            int width = (int) (Math.ceil(clipBox.getHeight()));
            int height = (int) (Math.ceil(clipBox.getWidth()));
            Bound2D bmpBox = new Bound2D(0, 0, width, height);
            SeismicColorMap colorMap = getPipeline().getTraceRasterizer().getColorMap();
            this.setImageCreator(new TraceImageCreator(width, height, colorMap));
            Transform2D localT = new Transform2D(modelBoxS, bmpBox, false, false);

            Double fl = new Double(localT.getScaleY()*
                        getPipeline().getDataLoader().getDataReader().getMetaData().getSampleRate());
            float intFactor = fl.floatValue();
            float oldFactor = getPipeline().getInterpretation().getTraceInterpolator().getResamplingFactor();
            if(Math.abs(oldFactor-intFactor) > ROUNDING_ERROR) {
                getPipeline().getInterpretation().getTraceInterpolator().setResamplingFactor(intFactor );
                getPipeline().invalidate( getPipeline().getInterpretation().getTraceInterpolator() );
            }
            renderToBitmap(modelBoxS, localT);

            Transform2D fittr = new Transform2D(_flatF);
            if (super.getDirection() == TraceImage.DRAW_TRACE_RIGHT_TO_LEFT) {
                // need inverse as well
                fittr.scale(-1, 1);
                fittr.translate(-clipBox.height, 0);
                fittr.translate(-clipBox.y, clipBox.x);
            }
            else fittr.translate(clipBox.y, clipBox.x);
            Graphics2D graphics = shr.getGraphics();
            AffineTransform originalTrans = (AffineTransform) graphics.getTransform().clone();
            graphics.transform(fittr);
            graphics.drawImage(this.getImageCreator().getBufferedImage(), new Transform2D(), null);
            graphics.setTransform(originalTrans);
        }
    }

    private double calculateImageWidth() {
        if (getPipeline() != null) {
            return getPipeline().getVirtualModelLimits().width * getModelUnitsPerTrace();
        }
        return 0;
    }

    private double calculateImageHeight() {
        if (getPipeline() != null) {
            SeismicReader sreader=getPipeline().getDataLoader().getDataReader();
            double sampleRate = sreader.getMetaData().getSampleRate();
            if (sampleRate == 0) sampleRate = 1.0;
            Bound2D vl = getPipeline().getVirtualModelLimits();
            double heightValue = vl.height * getModelUnitsPerSample();
            //System.out.println("MapImage height " + heightValue + " : " + heightValue/sampleRate);
            heightValue = heightValue / sampleRate;
            return heightValue;
        }
        return 0;
    }
}
