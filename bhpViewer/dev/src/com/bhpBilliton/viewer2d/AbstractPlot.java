/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d;

import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.AdjustmentEvent;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.SwingUtilities;

import com.bhpBilliton.viewer2d.util.ErrorDialog;

import com.gwsys.gw2d.gui.ColorGradientModel;
import com.gwsys.gw2d.gui.ColorPanel;
import com.gwsys.gw2d.gui.OrientedLabel;
import com.gwsys.gw2d.model.CommonDataModel;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.gw2d.ruler.RulerView;
import com.gwsys.gw2d.util.Transform2D;
import com.gwsys.gw2d.view.ScrollableView;
import com.gwsys.gw2d.view.BoxContainer;
import com.gwsys.seismic.core.TraceNormalizer;
import com.gwsys.seismic.core.TraceRasterizer;
import com.gwsys.seismic.core.SeismicWorkflow;
import com.gwsys.seismic.util.NumberRange;
import com.gwsys.seismic.util.PlotConstants;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This abstract class is the parent of BhpPlotXV and BhpPlotMV.
 *               In the center of this plot is an
 *               <code>{@link OrderedStackPlotView}</code>.
 *
 * @author Synthia Kong
 * @version 1.0
 */

public abstract class AbstractPlot extends ScrollableView {
    public static final int DEFAULT_MAP_SCALE = 4;
    public static final int DEFAULT_XSEC_HSCALE = 2;
    public static final int DEFAULT_XSEC_VSCALE = 1;
    private boolean eventReceiver = false;
    protected CommonDataModel _model;

    protected OrderedStackPlotView _view;

    protected BhpMouseCenter _mCenter;

    protected int _axisAssociateId;

    protected BoxContainer _annL; //annotation on left
    protected BoxContainer _annR;
    protected BoxContainer _annT; //annotation on top
    protected BoxContainer _annTL;//label on corner

    /**
     * Field name on which the plot is synchronized horizontally.
     */
    protected String _hAxesSyncName;
    /**
     * Major step value of the vertical axis.
     */
    protected double _vMajorStep;
    /**
     * Minor step value of the vertical axis.
     */
    protected double _vMinorStep;
    /**
     * Major step value of the horizontal axis.
     */
    protected double _hMajorStep;
    /**
     * Minor step value of the horizontal axis.
     */
    protected double _hMinorStep;
    /**
     * Location of the horizontal axis.
     */
    protected int _hAxesLoc;
    /**
     * Location of the vertical axis.
     */
    protected int _vAxesLoc;
    /**
     * Location of the title.
     */
    protected int _titleLoc, textHeight = 20;
    /**
     * Location of the color bar.
     */
    private int _colorbarLoc = PlotConstants.NONE;

    /**
     * The vertical axis transformation.
     */
    protected Transform2D _vaxisTrans;
    /**
     * The horizontal axis transformation.
     */
    protected Transform2D _haxisTrans;

    /**
     * The title string.
     */
    protected String _title;
    /**
     * The left label.
     */
    protected String _labelL;
    /**
     * The right label.
     */
    protected String _labelR;
    /**
     * The top label.
     */
    protected String _labelT;
    /**
     * The bottom label.
     */
    protected String _labelB;

    private OrientedLabel _plotTitle;
    /**
     * Graphic attribute used to draw the labels.
     */
    protected RenderingAttribute _attribute = new RenderingAttribute();

    private int _xmlNumberOfLayers = -1;
    private int _xmlViewPositionX = -1;
    private int _xmlViewPositionY = -1;

    private int _hsValue = -1;
    private int _vsValue = -1;

    private int _synFlag;
    private boolean _enablePlotTalk;
    private boolean _lockAspectRatio;
    private boolean _synScrollHorizontal;
    private boolean _synScrollVertical;

    private double _defaultModelUnitPerTrace;
    private double _defaultModelUnitPerSample;
    protected double _modelUnitPerTrace;
    protected double _modelUnitPerSample;
    private ColorPanel colorPanel;
    private BhpViewerBase bhpViewerBase = null;
    public BhpViewerBase getViewer() {
        return this.getBhpLayer(_axisAssociateId).getBhpViewerBase();
    }

    /**
     * Constructs a new instance and sets up its GUI.
     * @param hs horizontal scale of the plot
     * @param vs vertical scale of the plot
     */
    public AbstractPlot(double hs, double vs) {
        super();
        setupMainView();
        setupAnnotation();

        _enablePlotTalk = true;
        _synFlag = BhpViewer.SCALEH_FLAG | BhpViewer.SCALEV_FLAG |
                   BhpViewer.CURSOR_FLAG | BhpViewer.WINDOW_VIEWPOS_FLAG;
        _synScrollHorizontal = true;
        _synScrollVertical = true;
        _lockAspectRatio = false;
        _defaultModelUnitPerTrace = hs;
        _defaultModelUnitPerSample = vs;
        _modelUnitPerTrace = hs;
        _modelUnitPerSample = vs;
        _xmlNumberOfLayers = -1;
        _hAxesLoc = PlotConstants.TOP;
        _vAxesLoc = PlotConstants.LEFT;


    }

    /**
     * Converts the plot object into an XML string. <br>
     * This is used when save the working session into an XML file.
     * Please refer to BhpViewer.dtd for the detail of the format.
     * @return an XML string for the object.
     */
    public String toXMLString() {
        StringBuffer content = new StringBuffer();
        if (_axisAssociateId != -1) {
            content.append(BhpViewerHelper.toXMLStringAnnotation(this));
        }
        BhpLayer[] allLayers = _view.getAllBhpLayers();
        for (int i=0; i<allLayers.length; i++) {
            content.append(allLayers[i].toXMLString());
        }

        return content.toString();
    }

    public boolean getEnablePlotTalk() { return _enablePlotTalk; }
    public void setEnablePlotTalk(boolean t) { _enablePlotTalk = t; }

    public boolean getSynScrollHorizontal() { return _synScrollHorizontal; }
    public void setSynScrollHorizontal(boolean s) { _synScrollHorizontal = s; }
    public boolean getSynScrollVertical() { return _synScrollVertical; }
    public void setSynScrollVertical(boolean s) { _synScrollVertical = s; }

    public boolean getSyncHScale () {return (_synFlag & BhpViewer.SCALEH_FLAG) != 0;}
    public boolean getSyncVScale () {return (_synFlag & BhpViewer.SCALEV_FLAG) != 0;}

    /**
     * Gets the synchronization flag of the plot. <br>
     * Plot level synchronization includes synchronization for:<br>
     * horizontal scale, vertical scale, cursor position, and window position.
     */
    public int getSynFlag() { return _synFlag; }

    /**
     * Sets the synchronization flag of the plot.
     */
    public void setSynFlag(int f) {
        // if set to syn window position, by default, both directions are syned.
        if (((_synFlag & BhpViewer.WINDOW_VIEWPOS_FLAG) == 0) &&
            ((f & BhpViewer.WINDOW_VIEWPOS_FLAG) != 0)) {
            _synScrollHorizontal = true;
            _synScrollVertical = true;
        }
        _synFlag = f;
    }

    /**
     * Finds out if the map plot is set to lock the aspect ratio.
     */
    public boolean getLockAspectRatio()       { return _lockAspectRatio; }
    /**
     * Sets the flag of lock aspect ratio.
     */
    public void setLockAspectRatio(boolean b) { _lockAspectRatio = b; }

    /**
     * Gets the default horizontal scale.
     */
    public final double getDefaultHorizontalScale() { return _defaultModelUnitPerTrace; }
    /**
     * Gets the default vertical scale.
     */
    public final double getDefaultVerticalScale()   { return _defaultModelUnitPerSample; }
    /**
     * Retrieves the horizontal scale of the layer.
     */
    public double getHorizontalScale()        { return _modelUnitPerTrace; }
    /**
     * Retrieves the vertical scale of the layer.
     */
    public double getVerticalScale()          { return _modelUnitPerSample; }

    /**
     * Gets the location of the horizontal axis.
     */
    public int getHAxesLoc()  { return _hAxesLoc; }
    /**
     * Gets the location of the vertical axis.
     */
    public int getVAxesLoc()  { return _vAxesLoc; }
    /**
     * Gets the location of the title.
     */
    public int getTitleLoc()      { return _titleLoc; }
    /**
     * Gets the location of the color bar.
     */
    public int getColorbarLoc()   { return _colorbarLoc; }
    /**
     * Gets the major step value of the horizontal axis.
     */
    public double getHMajorStep()  { return _hMajorStep; }
    /**
     * Gets the minor step value of the horizontal axis.
     */
    public double getHMinorStep()  { return _hMinorStep; }
    /**
     * Gets the major step value of the vertical axis.
     */
    public double getVMajorStep()  { return _vMajorStep; }
    /**
     * Gets the minor step value of the vertical axis.
     */
    public double getVMinorStep()  { return _vMinorStep; }
    /**
     * Gets the title string.
     */
    public String getTitle()  { return _title; }
    /**
     * Gets the left label string.
     */
    public String getLabelL() { return _labelL; }
    /**
     * Gets the right label string.
     */
    public String getLabelR() { return _labelR; }
    /**
     * Gets the top label string.
     */
    public String getLabelT() { return _labelT; }
    /**
     * Gets the bottom label string.
     */
    public String getLabelB() { return _labelB; }
    /**
     * Gets the field name on which the plot is synchronized horizontally.
     */
    public String getHAxesSyncName() { return _hAxesSyncName; }

    /**
     * Sets the location of the horizontal axis.
     */
    public void setHAxesLoc(int l) { _hAxesLoc = l; }
    /**
     * Sets the location of the vertical axis.
     */
    public void setVAxesLoc(int l) { _vAxesLoc = l; }
    /**
     * Sets the location of the title.
     */
    public void setTitleLoc(int l)      { _titleLoc = l; }
    /**
     * Sets the location of the color bar.
     */
    public void setColorbarLoc(int l)   { _colorbarLoc = l; }
    /**
     * Sets the major step value of the vertical axis.
     */
    public void setVMajorStep(double f) { _vMajorStep = f; }
    /**
     * Sets the minor step value of the vertical axis.
     */
    public void setVMinorStep(double f) { _vMinorStep = f; }
    /**
     * Sets the major step value of the horizontal axis.
     */
    public void setHMajorStep(double f) { _hMajorStep = f; }
    /**
     * Sets the minor step value of the horizontal axis.
     */
    public void setHMinorStep(double f) { _hMinorStep = f; }
    /**
     * Sets the string of the title.
     */
    public void setTitle(String s)  {
        if (s==null || s.equals("null")) _title = "";
        else _title = s;
        if (_plotTitle != null) _plotTitle.setText(_title);
    }
    /**
     * Sets the string of the left label.
     */
    public void setLabelL(String s) {
        if (s==null || s.equals("null")) _labelL = "";
        else _labelL = s;
    }
    /**
     * Sets the string of the right label.
     */
    public void setLabelR(String s) {
        if (s==null || s.equals("null")) _labelR = "";
        else _labelR = s;
    }
    /**
     * Sets the string of the top label.
     */
    public void setLabelT(String s) {
        if (s==null || s.equals("null")) _labelT = "";
        else _labelT = s;
    }
    /**
     * Sets the string of the bottom label.
     */
    public void setLabelB(String s) {
        if (s==null || s.equals("null")) _labelB = "";
        else _labelB = s;
    }
    /**
     * Sets the field name on which the plot is synchronized horizontally.
     */
    public void setHAxesSyncName(String s) { _hAxesSyncName = s; }

    /**
     * Scales the plot along with the given layer.
     * @param layer the layer that will be scaled.
     * @param x horizontal scale factor.
     * @param y vertical scale factor.
     */
    public void scaleToWithLayer(BhpLayer layer, double x, double y) {
        //Point oldvp = layer.getViewPosition();
        Point oldvp = this.getViewport().getViewPosition();
        scaleToWithLayer(layer, x, y, oldvp.getX(), oldvp.getY());
    }

    /**
     * Scales the plot along with the given layer. <br>
     * The original view position will be kept and the scroll bar
     * will be updated. If the annotation is associated with the
     * given layer, the annotation will be updated too.
     * @param layer the layer that will be scaled.
     * @param x horizontal scale factor.
     * @param y vertical scale factor.
     * @param xp horizontal view position in device space.
     * @param yp vertical view position in device space.
     */
    public void scaleToWithLayer(BhpLayer layer, double x, double y, double xp, double yp) {
        if (_lockAspectRatio) {
            x = Math.min(x, y);
            y = x;
        }
        if (x==1.0 && y==1.0) return;
        _modelUnitPerTrace = getHorizontalScale() * x;
        _modelUnitPerSample = getVerticalScale() * y;
        if (_view != null) {
            _view.scaleToWithLayer(layer, x, y, xp, yp);
        } else return;

        //update annotation
        _annT.scaleTo(xp, yp, x, y);
        _annL.scaleTo(xp, yp, x, y);
        _annR.scaleTo(xp, yp, x, y);
        //updateHAxes();
        //updateVAxes();
        updateMiscAnnotation();
        //Bound2D newLoc = new Bound2D(xp, yp, xp, yp);
        //layer.getTransformation().transform(newLoc);
        //this.notifyChangeListeners();
        //eventReceiver = true; // do not adjust scroll bar again
        this.revalidate();
        this.setViewPosition((int)(xp*x), (int)(yp*y));
        if (Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("New Scroll Bar Y Position = " + (int)(yp*y));
        _hsValue = (int)(xp*x);
        _vsValue = (int)(yp*y);
        notifyChangeListeners();
        layer._window.updatePlot(); //force to update scroll bar
    }

    /**
     * This method is called when scroll bar moves. <br>
     * It overrides the implementation in its super class
     * for view position synchronization. A
     * <code>{@link BhpLayerEvent}</code> will be built
     * and broadcasted. The event has the flag
     * <code>{@link BhpLayer}</code>.WINDOW_VIEWPOS_FLAG,
     * and the annotation readings is put into the event
     * parameter.
     */
    public void adjustmentValueChanged(AdjustmentEvent e) {
        JScrollBar sb = (JScrollBar) e.getSource();

        if (eventReceiver){
           eventReceiver = false;
           return;
        }

        boolean isVertical = true;
        if (sb.getOrientation() == Adjustable.HORIZONTAL) {
            if (e.getValue() == _hsValue) return;
            _hsValue = e.getValue();
            isVertical = false;
        }

        if (sb.getOrientation() == Adjustable.VERTICAL) {
            if (e.getValue() == _vsValue) return;
            _vsValue = e.getValue();
            isVertical = true;
        }

        //repaint title and labels
        _annL.repaint();
        _annT.repaint();

        BhpLayer axisLayer = this.getBhpLayer(_axisAssociateId);
        if (axisLayer == null) {
            return;
        }
        else if ((_synFlag & BhpViewer.WINDOW_VIEWPOS_FLAG) == 0) {
            return;
        }

        if (axisLayer instanceof BhpMapLayer) {
            if (((BhpMapLayer)axisLayer).isTransposeImage())
                isVertical = !isVertical;
        }

        // only do selection and broadcasting if it is set to synchronize

        //super.adjustmentValueChanged(e);

        //Point oldPosition = axisLayer.getViewPosition();

        _view.setViewPosition(_hsValue, _vsValue);
        Point newPosition = getViewPosition();

        if (!_enablePlotTalk) return;
        int virtTraceId = -999;
        int virtualY = 0;
        Point selectP = axisLayer.selectByPoint(new Point(newPosition.x, newPosition.y));
        //System.out.println("\n    selectP " + selectP.toString());
        if (selectP != null) {
            virtTraceId = selectP.x;
            virtualY = selectP.y;
        }
        if (virtTraceId == -999) return;

        Hashtable selectParameter = this.buildSelectedAnnotationParameter(virtTraceId, virtualY);
        String vname = axisLayer.getParameter().getVerticalName();
        if (isVertical) {
            Object vnameValue = selectParameter.get(vname);
            Object valueValue = selectParameter.get("value");
        if (valueValue==null||vnameValue==null)
                return;
            selectParameter = new Hashtable(2);
            selectParameter.put(vname, vnameValue);
            selectParameter.put("value", valueValue);
        }
        else {
            selectParameter.remove(vname);
        }
        BhpLayerEvent event = new BhpLayerEvent(BhpViewer.WINDOW_VIEWPOS_FLAG,
                axisLayer, selectParameter);

        bhpViewerBase = axisLayer.getBhpViewerBase();
        bhpViewerBase.broadcastBhpLayerEvent(event);

    }

    public void broadcastWindowPosition() {
        if (!_enablePlotTalk) return;
        BhpLayer axisLayer = this.getBhpLayer(_axisAssociateId);
        Point newPosition = getViewPosition();//axisLayer.getViewPosition();
        int virtTraceId = -999;
        int virtualY = 0;
        Point selectP = axisLayer.selectByPoint(new Point(newPosition.x, newPosition.y));
        if (selectP != null) {
            virtTraceId = selectP.x;
            virtualY = selectP.y;
        }
        if (virtTraceId == -999) return;

        Hashtable selectParameter = this.buildSelectedAnnotationParameter(virtTraceId, virtualY);

        BhpLayerEvent event = new BhpLayerEvent(BhpViewer.WINDOW_VIEWPOS_FLAG,
                                                axisLayer, selectParameter);
        bhpViewerBase = axisLayer.getBhpViewerBase();
        bhpViewerBase.broadcastBhpLayerEvent(event);
        /*
       ((BhpViewer)SwingUtilities.windowForComponent(this)).
                                         broadcastBhpLayerEvent(event);
                                         */
    }

    /**
     * This method is called when a broadcasted BhpLayerEvent is received. <br>
     * This method itself handles window-level events, events with flag
     * CURSOR_FLAG, and WINDOW_VIEWPOS_FLAG. Other layer-level events
     * are forwarded to individual <code>{@link BhpLayer}</code>.
     * At the end, if the axis associate layer has some significant change,
     * the annotation will be updated.
     * @param e the broadcasted event.
     */
    public boolean receiveBhpLayerEvent(BhpLayerEvent e) {

        if ((e.getTypeFlag() & BhpViewer.CURSOR_FLAG) != 0) {
            // this event is for plot, not for layer, don't broadcast it to layers
            // also, this event won't be combined with other evnet,
            // so we can return directly after handling it
            if (_axisAssociateId != -1) {
                BhpLayer axisLayer = this.getBhpLayer(_axisAssociateId);
                if (axisLayer != null &&
                    e.getSource().getIdNumber() != axisLayer.getIdNumber() &&
                    ((this.getSynFlag() & BhpViewer.CURSOR_FLAG) != 0)) {
                    setSyncCursorPosition((Hashtable) e.getParameter());
                }
            }
            return false;
        }
        if ((e.getTypeFlag() & BhpViewer.WINDOW_VIEWPOS_FLAG) != 0) {
            if (_axisAssociateId != -1) {
                BhpLayer axisLayer = this.getBhpLayer(_axisAssociateId);
                int layerId = e.getSource().getIdNumber();
                if (axisLayer != null &&
                    layerId != axisLayer.getIdNumber() &&
                    ((this.getSynFlag() & BhpViewer.WINDOW_VIEWPOS_FLAG) != 0)) {
                   eventReceiver = true;
                   setSyncWindowPosition((Hashtable) e.getParameter());
                   //update the persisted internal position of the scroll bar
                   _hsValue = getHorizontalScrollBar().getValue();
                   _vsValue = getVerticalScrollBar().getValue();
                }
            }
            return false;
        }
        if (((e.getTypeFlag() & BhpViewer.SCALEH_FLAG)!=0) ||
            ((e.getTypeFlag() & BhpViewer.SCALEV_FLAG)!=0)) {
            // originated from this very window. ignore.
            if (getBhpLayer(e.getSource().getIdNumber()) != null)
              return false;
            double xFactor = 1.0;
            double yFactor = 1.0;
            // get gui for error dialog
            BhpLayer axisLayer = this.getBhpLayer(_axisAssociateId);
            bhpViewerBase = axisLayer.getBhpViewerBase();
            BhpLayer sourceLayer = e.getSource();
            BhpLayer desLayer = getBhpLayer(getAxisAssociateLayer());
            if (desLayer == null) {
                try {
                    desLayer = _view.getAllBhpLayers()[0];
                }
                catch (Exception innerEx) {
                    ErrorDialog.showInternalErrorDialog(bhpViewerBase, Thread.currentThread().getStackTrace(),
                                                        "Cannot get layer");
                    //innerEx.printStackTrace();
                }
            }
            if (desLayer == null) {
                ErrorDialog.showInternalErrorDialog(bhpViewerBase, Thread.currentThread().getStackTrace(),
                                             "BhpPlot.receiveBhpLayerEvent scale change error: no layer found");
                return false;
            }

            double newxs = sourceLayer._window.getBhpPlot().getHorizontalScale();
            double newys = sourceLayer._window.getBhpPlot().getVerticalScale();
            int flag = e.getTypeFlag();
            int sflag = this._synFlag;
            if (((flag & BhpViewer.SCALEH_FLAG)!=0) && ((sflag & BhpViewer.SCALEH_FLAG)!=0))
                xFactor = newxs / getHorizontalScale();
            if (((flag & BhpViewer.SCALEV_FLAG)!=0) && ((sflag & BhpViewer.SCALEV_FLAG)!=0)) {
                if (sourceLayer._window.getWindowType() == desLayer._window.getWindowType())
                    yFactor = newys / getVerticalScale();
            }

            this.scaleToWithLayer(desLayer, xFactor, yFactor);
        }

        BhpLayer layer;
        BhpLayer sourceLayer = e.getSource();
        boolean isEventOrigin = false, layerUpdated=false;
        BhpLayer[] allLayers = _view.getAllBhpLayers();
        int size = allLayers.length;
        for (int i=0; i<size; i++) {    // first layer is RubberView
            layer = allLayers[i];
            if (sourceLayer!=null&&sourceLayer.getIdNumber()==layer.getIdNumber())
              isEventOrigin = true;
            else isEventOrigin = false;
            if (layer.receiveBhpLayerEvent(e)) layerUpdated=true;
        }
        // update axis transformation
        if (_axisAssociateId != -1) {
            boolean scaleChange = false;
            BhpLayer axisLayer = this.getBhpLayer(_axisAssociateId);
            if (axisLayer != null && (sourceLayer==null||
                                      sourceLayer.getIdNumber() != axisLayer.getIdNumber())) {
                if (((e.getTypeFlag() & BhpViewer.SCALEH_FLAG) != 0) &&
                    ((axisLayer.getSynFlag() & BhpViewer.SCALEH_FLAG) != 0))
                    scaleChange = true;
                else if (((e.getTypeFlag() & BhpViewer.SCALEV_FLAG) != 0) &&
                    ((axisLayer.getSynFlag() & BhpViewer.SCALEV_FLAG) != 0))
                    scaleChange = true;

                if (scaleChange) {
                    updateHAxes();
                    updateVAxes();
                    updateMiscAnnotation();
                }
            }
        }
        if (isEventOrigin == false) // the other plots needs to be updated
            _view.setTransformationWithoutPropogation(_view.getTransformation());
        //it won't update otherwise
        return layerUpdated;
    }

    /**
     * Sets the XML related information about the plot. <br>
     * This information is the result of paring XML file,
     * however, they can only take affect when all the
     * layers are ready and inserted.
     * @param nl total number of <code>{@link BhpLayer}</code> in the plot.
     * @param vx horizontal view position.
     * @param vy vertical view position.
     */
    public void setXmlRelatedInfo(int nl, int vx, int vy) {
        _xmlNumberOfLayers = nl;
        _xmlViewPositionX = vx;
        _xmlViewPositionY = vy;
    }

    /**
     * Sets the flag to enable rubber band zooming. <br>
     * Sets the flag enables one rubber band zooming only.
     */
    public void setRubberViewFlag() {
        getMainView().getRubberView().setFlag();
        getMouseCenter().setZoomRubberFlag();
    }

    public void updateCursorSetting(double w, double h, boolean showPointer) {
        getMainView().getRubberView().updateSymbolSize(w, h);
        getMainView().setShowPointer(showPointer);
        boolean showCursor = false;
        if (!showCursor) {
            getMainView().setCursor(Cursor.getDefaultCursor());
        }
    }

    /**
     * Removes layers from the plot. <br>
     * If the axis associate layer is removed, no annotation will be drawn.
     * @param layer layers that need to be removed.
     */
    public void removeBhpLayer(BhpLayer[] layer) {
        for (int i=0; i<layer.length; i++) {
            if (_axisAssociateId == layer[i].getIdNumber())
                setAxisAssociateLayer(-1, true);
            _view.removeBhpLayer(layer[i]);
        }

        _view.setTransformationWithoutPropogation(_view.getTransformation());
        this.validate();
    }

    /**
     * Adds a layer to the plot and adjust annotation accordingly.
     * @param layer the layer needs to be added.
     */
    public void addBhpLayer(BhpLayer layer) {
        _view.addBhpLayer(layer);
        this.getMainView().setTransformationWithoutPropogation(this.getMainView().getTransformation());
        if (_axisAssociateId> 0 && _axisAssociateId == layer.getIdNumber()) {
            updateAnnotation();
        }
        if (_view.getNumberOfBhpLayers() == _xmlNumberOfLayers) {
            _xmlNumberOfLayers = -1;
            this.setViewPosition(_xmlViewPositionX, _xmlViewPositionY);
            _view.setViewPosition(_xmlViewPositionX, _xmlViewPositionY);
        }


        if (_view.getNumberOfBhpLayers() > 0 && _axisAssociateId < 0) {
            this.setAxisAssociateLayer(layer.getIdNumber(), true);
        }
        this.invalidate();
    }

    /**
     * Retrieves the layer with given layer identification number. <br>
     * @param id the identification number of the requested layer.
     * @return the layer with the given identification number.
     *         If no such layer can be found, null will be returned.
     */
    public BhpLayer getBhpLayer(int id) {
        BhpLayer[] allLayers = _view.getAllBhpLayers();
        for (int i=0; i<allLayers.length; i++) {
            if (allLayers[i]!=null && allLayers[i].getIdNumber()==id)
                return allLayers[i];
        }
        return null;
    }
  
   /**
     * Retrieves the axis-associated layer
     * @return the axis-associatd BhpLayer.
     *         If no such layer can be found, null will be returned.
     */
    public BhpLayer getBhpLayer() {
        return getBhpLayer(_axisAssociateId);
    }
    
    public int getNextUniqueLayerID() {
        int layerID=0;
        int maxExistingID=0;

        BhpLayer[] allLayers = _view.getAllBhpLayers();
        for (int i=0;i<allLayers.length;i++) {
            layerID = allLayers[i].getIdNumber();
            if (layerID > maxExistingID) {
                maxExistingID = layerID;
            }
        }
        layerID=maxExistingID+1;

        return layerID;
    }

    /**
     * Retrieves the mouse handler of the plot.
     */
    public BhpMouseCenter getMouseCenter() { return _mCenter; }
    /**
     * Retrieves the <code>{@link OrderedStackPlotView}</code> of the plot.
     */
    public OrderedStackPlotView getMainView() { return _view; }
    /**
     * Gets the identification number of the axis associate layer.
     */
    public int getAxisAssociateLayer() { return _axisAssociateId; }

    /**
     * Updates the title, color bar, and labels.
     */
    public void updateMiscAnnotation() {
        clearAnnotation(_annL, null, false);
        clearAnnotation(_annR, null, false);
        clearAnnotation(_annT, _annTL, false);

        updateLabels();
        updateTitleAndColorBar();

    Dimension viewSize=_view.getActualSize();
        _annT.setSize(viewSize.width, _annT.getSize().height);
        _annL.setSize(_annL.getSize().width, viewSize.height);
        _annR.setSize(_annR.getSize().width, viewSize.height);
        _annTL.setSize(viewSize.width, _annT.getSize().height);
        this.revalidate();
    }

    /**
     * Updates the annotation fo the plot. <br>
     * Implementation of the method is reponsible for
     * updating the vertical axis, horizontal axis, title,
     * color bar, and labels.
     */
    public abstract void updateAnnotation();
    /**
     * Sets the layer that based on which the annotation will be drawn.
     * @param id the identification number of the layer.
     * @param doUpdate a boolean flag indicates if the display shouble be
     *        updated right away. This can be used to avoid multiple repaint.
     */
    public abstract void setAxisAssociateLayer(int id, boolean doUpdate);
    /**
     * Updates the horizontal axis.
     */
    public abstract void updateHAxes();
    /**
     * Updates the vertical axis.
     */
    public abstract void updateVAxes();
    /**
     * Retrieves the annotation readings for the given position. <br>
     * The implementation of the method should find out the reading
     * for all the current shown axes and put them into the hashtable.
     * @param virtTraceId the horizontal position, the trace ID of the interested trace.
     * @param virtualY the vertical position, in model space.
     * @return the annotation readings.
     *         The fields name and value pairs are put in a Hashtable.
     */
    public abstract Hashtable buildSelectedAnnotationParameter(int virtTraceId, int virtualY);

    /**
     * Updates the left, right, top, and bottom labels.
     */
    protected void updateLabels() {
        BhpLayer layer = this.getBhpLayer(_axisAssociateId);
        if (layer == null) return;
        if (_labelL != null && _labelL.length() > 0) {
            OrientedLabel label = new OrientedLabel(_labelL,
                    OrientedLabel.ALIGNMENT_VERTICAL_CENTER);
            label.setBackground(Color.white);
            label.setLabelAttribute(_attribute);
            label.setPreferredSize(new Dimension(_labelL.length()*10, textHeight));
            _annL.add(label, 0);
        }
        if (_labelR != null && _labelR.length() > 0) {
            OrientedLabel label = new OrientedLabel(_labelR,
                    OrientedLabel.ALIGNMENT_VERTICAL_CENTER);
            label.setBackground(Color.white);
            label.setLabelAttribute(_attribute);
            _annR.add(label, _annR.getComponentCount());
        }
        if (_labelT != null && _labelT.length() > 0) {
            OrientedLabel label = new OrientedLabel(_labelT);
            label.setBackground(Color.white);
            label.setLabelAttribute(_attribute);
            _annT.add(label, 0);
            OrientedLabel dummyLabel = new OrientedLabel(" ");
            dummyLabel.setPreferredSize(label.getPreferredSize());
            _annTL.add(dummyLabel, 0);
        }

    }

    /**
     * Updates the title and the color bar.
     */
    protected void updateTitleAndColorBar() {
        BhpLayer layer = this.getBhpLayer(_axisAssociateId);
        if (layer == null) return;
        SeismicWorkflow pipeline = layer.getPipeline();
        if (_titleLoc != PlotConstants.NONE) {
            _plotTitle = new OrientedLabel(_title);
            _plotTitle.setBackground(Color.white);
            _plotTitle.setLabelAttribute(_attribute);
            _plotTitle.setPreferredSize(new Dimension(_title.length()*10, textHeight));
            _annT.add(_plotTitle, 0);
        }
      //remove existing one
      Container topPanel = getParent().getParent();
      if (colorPanel!=null)
        topPanel.remove(colorPanel);
        if (_colorbarLoc != PlotConstants.NONE && pipeline.getTraceRasterizer() != null) {
            ColorGradientModel cbm=new ColorGradientModel(
                    pipeline.getTraceRasterizer().getColorMap().getColorRamp(),
               pipeline.getDataLoader().getDataReader().getMetaData().getMinimumAmplitude(),
               pipeline.getDataLoader().getDataReader().getMetaData().getMaximumAmplitude());

            String colorbarName = "Color Interpolation";
            TraceRasterizer rast = pipeline.getTraceRasterizer();
            if (rast instanceof AbstractBhpColorInterpolatedRasterizer) {
                AbstractBhpColorInterpolatedRasterizer colorRasterizer = (AbstractBhpColorInterpolatedRasterizer) rast;
                double minValue = pipeline.getDataLoader().getDataReader().getMetaData().getMinimumAmplitude();
                double maxValue = pipeline.getDataLoader().getDataReader().getMetaData().getMaximumAmplitude();
                if (rast instanceof BhpModelTraceRasterizer) {
                    TraceNormalizer mnorm = pipeline.getInterpretation().getTraceNormalization();
                    NumberRange range = mnorm.getDataRangeFor(new NumberRange (-1,1));
                    minValue = range.getMin().floatValue();
                    maxValue = range.getMax().floatValue();
                    colorbarName = layer.getPropertyName();
                }
                double colorStart = colorRasterizer.getTranslatedColorMapStartValue(minValue, maxValue);
                double colorEnd = colorRasterizer.getTranslatedColorMapEndValue(minValue, maxValue);
                cbm.setRanges(colorStart, colorEnd);

            }
            else {
                TraceNormalizer norm = pipeline.getInterpretation().getTraceNormalization();
                NumberRange range = norm.getDataRangeFor(new NumberRange(-1, 1));
                cbm.setRanges(range.getMin().floatValue(), range.getMax().floatValue());
            }
            //support in left and right position
            colorPanel=new ColorPanel(cbm, colorbarName);
            switch (_colorbarLoc) {

            case PlotConstants.LEFT:
        case PlotConstants.TOP:
                topPanel.add(colorPanel, BorderLayout.WEST);
                break;
            case PlotConstants.RIGHT:
        case PlotConstants.BOTTOM:
                topPanel.add(colorPanel, BorderLayout.EAST);
                break;
            default:
                break;
            }

        }
      if (topPanel instanceof JComponent)
            ((JComponent)topPanel).revalidate();
    }
    /**
     * Updates the color array.
     */
    public void updateColorMap(Color[] colors){
        if (colorPanel!=null){
            colorPanel.getColorPanelModel().setColorArray(colors);
            colorPanel.repaint();
        }
    }
    /**
     * Removes all axes and lables.
     */
    protected void cleanAxesAndAnnotations() {
        clearAnnotation(_annT, _annTL, true);
        clearAnnotation(_annL, null, true);
        clearAnnotation(_annR, null, true);
        clearAnnotation(_annT, _annTL, false);
        clearAnnotation(_annL, null, false);
        clearAnnotation(_annR, null, false);
    }

    protected void clearAnnotation(BoxContainer container, boolean isAxis) {
        clearAnnotation(container, null, isAxis);
    }

    /**
     * Clears object of the given container.
     * @param container the container that need to be cleaned.
     * @param isAxis if true, all cgAxisView will be removed.
     *        If false, other view will be removed.
     */
    protected void clearAnnotation(BoxContainer container,
                                   BoxContainer cornerContainer,
                                   boolean isAxis) {

        ArrayList v = new ArrayList();
        ArrayList cornerV = new ArrayList();
        JComponent plot;
        for (int i=0; i<container.getComponentCount(); i++) {
            plot = (JComponent) container.getComponent(i);
            if ((plot instanceof RulerView) == isAxis) {
                v.add(plot);
                if (cornerContainer != null &&
                    i < cornerContainer.getComponentCount()) {
                    cornerV.add(cornerContainer.getComponent(i));
                }
            }
        }
        for (int i=0; i<v.size(); i++)
            container.remove((Component) v.get(i));
        for (int i=0; i<cornerV.size(); i++)
            cornerContainer.remove((Component)cornerV.get(i));
        v.clear();
        cornerV.clear();
    }


    private void setSyncCursorPosition(Hashtable parameter) {
        BhpLayer axisLayer = this.getBhpLayer(_axisAssociateId);
        if (axisLayer == null) return;

        if (parameter == null) {
            getMainView().getRubberView().hideSymbol();
            _mCenter.notifyMouseTracking(-1, -1, true);
            return;
        }

        String vname = axisLayer.getParameter().getVerticalName();
        Object vvalue = null;
        if (parameter.containsKey(vname)) vvalue = parameter.get(vname);

        Object hvalue = null;
        if (parameter.containsKey(_hAxesSyncName)) hvalue = parameter.get(_hAxesSyncName);

        if (vvalue != null || hvalue != null) {
            int virtualY = 0;
            int traceId = -999;
            if (vvalue != null) {
                double vv = Double.parseDouble(vvalue.toString());
                virtualY = axisLayer.findModelFromVerticalReading(vv);
            }
            if (hvalue != null) {
                double hv = Double.parseDouble(hvalue.toString());
                traceId = axisLayer.findTraceIdFromField(_hAxesSyncName, hv);
            }
            if (traceId == -999) {
                // match not found, hide symbol
                getMainView().getRubberView().hideSymbol();
                _mCenter.notifyMouseTracking(-1, -1, true);
                return;
            }
            Point newMousePos = axisLayer.reverseSelectByPoint(traceId, virtualY);
            int newMouseX = getMainView().getRubberView().getSymbolAnchorX();
            int newMouseY = getMainView().getRubberView().getSymbolAnchorY();
            Point originalSelectedPoint = axisLayer.selectByPoint(new Point(newMouseX, newMouseY));
            int finalVirtTraceId = originalSelectedPoint.x;
            int finalVirtualY = originalSelectedPoint.y;
            if (hvalue != null) {
                newMouseX = newMousePos.x;
                int totTraceNumber = axisLayer.getPipeline().getDataLoader().getDataReader().getMetaData().getNumberOfTraces();
                if (traceId != -999 && traceId < totTraceNumber) finalVirtTraceId = traceId;
            }
            if (vvalue != null) {
                newMouseY = newMousePos.y;
                finalVirtualY = virtualY;
            }
            getMainView().getRubberView().setSymbolAnchor(newMouseX, newMouseY);
            _mCenter.notifyMouseTracking(finalVirtTraceId, finalVirtualY, true);
        }
    }

    private void setSyncWindowPosition(Hashtable parameter) {
        BhpLayer axisLayer = this.getBhpLayer(_axisAssociateId);
        if (axisLayer == null) return;

        String vname = axisLayer.getParameter().getVerticalName();
        Object vvalue = null;
        if (parameter.containsKey(vname)) vvalue = parameter.get(vname);

        Object hvalue = null;
        if (parameter.containsKey(_hAxesSyncName)) hvalue = parameter.get(_hAxesSyncName);

        if (vvalue != null || hvalue != null) {
            double virtualY = 0;
            int traceId = -999;
            if (vvalue != null) {
                double vv = Double.parseDouble(vvalue.toString());
                virtualY = axisLayer.findModelFromVerticalReading(vv);
                //System.out.println("    Ver : " + vname + " = " + vvalue.toString() + "==>" + virtualY);
            }
            if (hvalue != null) {
                double hv = Double.parseDouble(hvalue.toString());
                traceId = axisLayer.findTraceIdFromField(_hAxesSyncName, hv);
                //System.out.println("    Hor : " + _hAxesSyncName + " = " + hvalue.toString() + "==>" + traceId);
            }
            Point newWindowPos = axisLayer.reverseSelectByPoint(traceId, virtualY);
            //System.out.println("    WPos " + newWindowPos.toString());
            int newWindowX = this.getViewPosition().x;
            int newWindowY = this.getViewPosition().y;
            if (hvalue != null && _synScrollHorizontal) {
                int viewSize = (int) this.getViewport().getViewSize().getWidth();
                //int viewSize = this.getLimits().width;
                int visibleSize = this.getViewport().getVisibleRect().width; //this.getSize().width;
                if (visibleSize > viewSize)
                    newWindowX = 0;
                else if ((viewSize-newWindowPos.x) < visibleSize)
                    newWindowX = viewSize - visibleSize;
                else
                    newWindowX = newWindowPos.x;

            }
            if (vvalue != null && _synScrollVertical) {
                int viewSize = (int) this.getViewport().getViewSize().getHeight();
                int visibleSize = getViewport().getVisibleRect().height; //this.getSize().height;
                if (visibleSize > viewSize)
                    newWindowY = 0;
                else if ((viewSize-newWindowPos.y) < visibleSize)
                    newWindowY = viewSize - visibleSize;
                else
                    newWindowY = newWindowPos.y;
            }
            //System.out.println("New Position=" + newWindowPos);
            this.setViewPosition(newWindowX, newWindowY);
        }
    }

    private void setupMainView() {
        _model = new CommonDataModel();
        _view = new OrderedStackPlotView();
        _mCenter = new BhpMouseCenter(this);
        _view.addMouseListener(_mCenter);
        _view.addMouseMotionListener(_mCenter);

        this.setView(_view);
    }

    private void setupAnnotation() {
        // by defule, no axis
        _axisAssociateId = -1;

        _annL = new BoxContainer(BoxLayout.X_AXIS);
        _annR = new BoxContainer(BoxLayout.X_AXIS);
        _annT = new BoxContainer(BoxLayout.Y_AXIS);

        this.setHorizontalAxis(_annT);
        this.setVerticalAxis(_annL);

        _annTL = new BoxContainer(BoxLayout.Y_AXIS);

        this.setCorner(ScrollableView.UPPER_LEFT_CORNER, _annTL);
        _vMajorStep = 1.0;
        _vMinorStep = 0.5;
        _hMajorStep = 100;
        _hMinorStep = 10;

    }

    public BhpLayer getFirstLayer(){
       BhpLayer[] allLayers = _view.getAllBhpLayers();
       if (allLayers.length>0)
           return allLayers[0];
       else
           return null;
    }
}
