package com.bhpBilliton.viewer2d;

import com.bhpBilliton.viewer2d.data.GeneralDataSource;
import com.bhpBilliton.viewer2d.dataAdapter.bhpsu.BhpIODataSummary;
import com.bhpBilliton.viewer2d.ui.LayerPropertyDialog;
import com.bhpBilliton.viewer2d.ui.OpenDataPanel;
import com.bhpBilliton.viewer2d.ui.OpenEventPanel;
import com.bhpBilliton.viewer2d.ui.OpenGeneralPanel;
import com.bhpBilliton.viewer2d.ui.OpenModelPanel;
import com.bhpBilliton.viewer2d.ui.OpenSeismicPanel;
import com.bhpb.qiworkbench.compAPI.DataDataDescriptor;
import com.bhpb.qiworkbench.compAPI.DataDataDescriptorTransferable;
import java.awt.Container;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.File;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 * <code>DTListener</code> extends the DropTargetListener for use by BhpViewer.
 * It accepts Drag operations of DataDataDescriptor flavor and invokes Model or Seismic
 * data loading if Drop operation is onto a BhpViewer XSec or Map window.
 *
 * Based on the qiProjectManager's DTListener by Marcus Vaal.
 * @author folsw9
 */
class DTListener implements DropTargetListener {
    private AbstractPlot _plot;

    public static final Logger logger = Logger.getLogger(DTListener.class.toString());
    
    public DTListener(AbstractPlot plot) {
        _plot = plot;
    }
    
    /**
     * Checks if the DataFlavor is supposed when being dragged
     * @param evt DropTargetDragEvent
     * @return
     */
    private boolean isDragFlavorSupported(DropTargetDragEvent evt) {
        boolean ok=false;
        DataFlavor chosen = null;
        if (evt.isDataFlavorSupported(DataDataDescriptorTransferable.dataDataDescriptorFlavor)) {
            chosen = DataDataDescriptorTransferable.dataDataDescriptorFlavor;
            ok=true;
        } else if (evt.isDataFlavorSupported(DataDataDescriptorTransferable.localDataDataDescriptorFlavor)) {
            chosen = DataDataDescriptorTransferable.localDataDataDescriptorFlavor;
            ok=true;
        } else if (evt.isDataFlavorSupported(new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType + "; class=DataDataDescriptor", "Local DataDataDescriptor"))) {
            chosen = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType + "; class=DataDataDescriptor", "Local DataDataDescriptor");
            ok=true;
        } else if (evt.isDataFlavorSupported(new DataFlavor(DataDataDescriptor.class, "Non local DataDataDescriptor"))) {
            chosen = new DataFlavor(DataDataDescriptor.class, "Non local DataDataDescriptor");
            ok=true;
        }
        
        Object data=null;
        try {
            data = evt.getTransferable().getTransferData(chosen);
            if (data == null) {
                throw new NullPointerException();
            }
        } catch ( Throwable thrown ) {
            System.err.println( "Couldn't get transfer data: " + thrown.getMessage());
            thrown.printStackTrace();
            return false;
        }
        if(data instanceof DataDataDescriptor) {
            if(!(new File(((DataDataDescriptor)data).getPath())).exists()) {
                ok = false;
            }
        }
        
        return ok;
    }
    
    /**
     * Returns your chosen Drop DataFlavor
     * @param evt DropTargetDropEvent
     * @return Returns the Drop DataFlavor
     */
    private DataFlavor chooseDropFlavor(DropTargetDropEvent evt) {
        if (evt.isLocalTransfer() == true &&
                evt.isDataFlavorSupported(DataDataDescriptorTransferable.localDataDataDescriptorFlavor)) {
            return DataDataDescriptorTransferable.localDataDataDescriptorFlavor;
        }
        DataFlavor chosen = null;
        if (evt.isDataFlavorSupported(DataDataDescriptorTransferable.dataDataDescriptorFlavor)) {
            chosen = DataDataDescriptorTransferable.dataDataDescriptorFlavor;
        } else if (evt.isDataFlavorSupported(DataDataDescriptorTransferable.localDataDataDescriptorFlavor)) {
            chosen = DataDataDescriptorTransferable.localDataDataDescriptorFlavor;
        } else if (evt.isDataFlavorSupported(new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType + "; class=DataDataDescriptor", "Local DataDataDescriptor"))) {
            chosen = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType + "; class=DataDataDescriptor", "Local DataDataDescriptor");
        } else if (evt.isDataFlavorSupported(new DataFlavor(DataDataDescriptor.class, "Non local DataDataDescriptor"))) {
            chosen = new DataFlavor(DataDataDescriptor.class, "Non local DataDataDescriptor");
        }
        return chosen;
    }
    
    /**
     * Checks if drag is ok
     * @param evt DropTargetDragEvent
     * @return Returns true if drag DataFlavor is supported, evt is a drop action and an exceptable action, false otherwise
     */
    private boolean isDragOk(DropTargetDragEvent evt) {
        if(isDragFlavorSupported(evt) == false) {
            return false;
        }
        
        int da = evt.getDropAction();
        if ((da & BhpViewerBase.acceptableActions) == 0) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Invoked to signify a Drag and Drop item is dragged into the DesktopPane
     */
    public void dragEnter(DropTargetDragEvent evt) {
        if(isDragOk(evt) == false) {
            evt.rejectDrag();
            return;
        }
        evt.acceptDrag(evt.getDropAction());
    }
    
    /**
     * Invoked to signify a Drag and Drop item is dragged over the DesktopPane
     */
    public void dragOver(DropTargetDragEvent evt) {
        if(isDragOk(evt) == false) {
            evt.rejectDrag();
            return;
        }
        evt.acceptDrag(evt.getDropAction());
    }
    
    /**
     * Called if the user has modified the current drop gesture
     */
    public void dropActionChanged(DropTargetDragEvent evt) {
        if(isDragOk(evt) == false) {
            evt.rejectDrag();
            return;
        }
        evt.acceptDrag(evt.getDropAction());
    }
    
    /**
     * Invoked to signify a Drag and Drop item is dragged out of the DesktopPane
     */
    public void dragExit(DropTargetEvent e) {
    }
    
    /**
     * Invoked to signify a Drag and Drop item is dropped into the DesktopPane
     */
    public void drop(DropTargetDropEvent evt) {
        String errorMsg = "";
        
        //Getting of layer, viewer and window deferred until Drop because the
        //Viewer was not yet populated when the DTListener was instantiated, and
        //also the active layer may have changed.
        final BhpLayer layer = _plot.getBhpLayer();
        if (layer == null) {
            errorMsg = "Cannot perform Drop: target plot contains null BhpLayer";
            logger.warning(errorMsg);
            JOptionPane.showMessageDialog(null, errorMsg);
            return;
        }
        
        final BhpViewerBase viewer = layer.getBhpViewerBase();
        if (viewer == null) {
            errorMsg = "Cannot perform Drop: target layer associated with null BhpViewerBase";
            logger.warning(errorMsg);
            JOptionPane.showMessageDialog(null, errorMsg);
        }
        
        final BhpWindow win = layer.getBhpWindow();
        if (win == null) {
            errorMsg = "Cannot perform Drop: target BhpViewerBase associated with null BhpWindow";
            logger.warning(errorMsg);
            JOptionPane.showMessageDialog(null, errorMsg);
        }
        
        DataFlavor chosen = chooseDropFlavor(evt);
        if (chosen == null) {
            logger.finest("No flavor match found" );
            evt.rejectDrop();
            return;
        }
        
        int da = evt.getDropAction();
        int sa = evt.getSourceActions();
        
        if ((sa & BhpViewerBase.acceptableActions) == 0) {
            logger.finest("No action match found");
            evt.rejectDrop();
            return;
        }
        
        Object data=null;
        try {
            evt.acceptDrop(BhpViewerBase.acceptableActions);
            
            data = evt.getTransferable().getTransferData(chosen);
            if (data == null) {
                throw new NullPointerException();
            }
        } catch ( Throwable thrown ) {
            logger.finest("Couldn't get transfer data: " + thrown.getMessage());
            logger.finest(thrown.toString());
            evt.dropComplete(false);
            return;
        }
        
        if (data instanceof DataDataDescriptor) {
            logger.info("Drop complete at: " + evt.getLocation());
            String unparsedNodeName = ((DataDataDescriptor)data).getName();
            final String nodeName = unparsedNodeName.substring(0,unparsedNodeName.indexOf(".dat"));
            final String nodePath = ((DataDataDescriptor)data).getPath();
            
            boolean isMapViewer = false;
            
            if (_plot != null) {
                if (_plot instanceof BhpPlotXV) {
                    logger.info("File dropped into XSec bhpViewer window: " + nodePath);
                } else if (_plot instanceof BhpPlotMV) {
                    logger.info("File dropped into Map bhpViewer window: " + nodePath);
                    isMapViewer = true;
                } else {
                    logger.warning("File dropped into unknown type of bhpViewer window: " +_plot.getClass().getName());
                }
            } else{
                JOptionPane.showMessageDialog(null,"Select Seismic Layer First!");
                return;
            }
            
            OpenDataPanel oPanel;
            Container parentContainer = layer.getParent();
            
            while (parentContainer.getParent() != null)
                parentContainer = parentContainer.getParent();
            
            final BhpIODataSummary summary;
            final OpenGeneralPanel _dataPanel;
            final JDialog dialog;
            
            logger.info("Attempting to add XSec/Map layer...");
            
            oPanel = new OpenDataPanel(viewer, layer, false);
            
            summary =
                    new BhpIODataSummary(viewer.getPropertyManager(), nodeName,
                    nodePath,
                    nodePath.substring(0,nodePath.lastIndexOf("/")),
                    "", viewer);
            
            GeneralDataSource dataSource = summary.getDataSource();
            oPanel.describeData(dataSource, true);
            
            int dataType = dataSource.getDataType();
            int dataOrder = dataSource.getDataOrder();
            
            //TODO make dataType an enum, removing the need
            //to use an extra variable for 'unknown' and simplifying error reporting.
            
            boolean unknownDataType = false;
            
            if (dataType == BhpViewer.BHP_DATA_TYPE_SEISMIC) {
                _dataPanel = new OpenSeismicPanel(viewer, layer);
            } else if (dataType == BhpViewer.BHP_DATA_TYPE_MODEL) {
                _dataPanel = new OpenModelPanel(viewer, layer, dataOrder);
            } else if (dataType == BhpViewer.BHP_DATA_TYPE_EVENT) {
                _dataPanel = new OpenEventPanel(viewer, layer, dataOrder);
            } else {
                _dataPanel = null;
                unknownDataType=true;
                errorMsg = "Cannot add file of unknown type: " + dataType + " to bhpViewer XSec/Map window.";
            }            
            
            if ((!isMapViewer && dataOrder == BhpViewer.BHP_DATA_ORDER_CROSSSECTION) ||
                    (isMapViewer && dataOrder == BhpViewer.BHP_DATA_ORDER_MAPVIEW)) {
                // data order matches the viewer type
                
                if (_dataPanel != null) {
                    _dataPanel.describeData(summary.getDataSource());
                    
                    dialog = new LayerPropertyDialog(_dataPanel, win, viewer,
                            layer, dataType, isMapViewer, dataSource);
                    
                    dialog.setVisible(true);
                    
                    evt.dropComplete(true);
                    
                    logger.info("Done adding XSec/Map layer.");
                }
            } else {
                String dataOrderString = "UNKNOWN";
                
                if (dataOrder == BhpViewer.BHP_DATA_ORDER_CROSSSECTION)
                    dataOrderString = "CROSSSECTION";
                else if (dataOrder == BhpViewer.BHP_DATA_ORDER_MAPVIEW)
                    dataOrderString = "MAPVIEW";
                
                String mapViewerString = "";
                if (!isMapViewer)
                    mapViewerString = "not ";
                
                errorMsg = "Cannot add layer: data order is : " + dataOrderString + " but target window is: " + mapViewerString + "a map viewer.";
            }
            
            if ("".equals(errorMsg) == false) {
                logger.warning(errorMsg);
                
                evt.dropComplete(false);
                
                JOptionPane.showMessageDialog(null, errorMsg);
            }
        }
    }
}