/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.util.Vector;

import com.gwsys.gw2d.ruler.DataRange;
import com.gwsys.gw2d.ruler.TickDefinition;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.seismic.core.DataChooser;
import com.gwsys.seismic.core.SeismicWorkflow;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.indexing.HeaderFieldEntry;
import com.gwsys.seismic.indexing.SeismicConstants;
import com.gwsys.seismic.reader.SeismicReader;
/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class generates tick for trace axis of
 *               <code>{@link BhpPlotXV}</code>. <br>
 *               If the field of the axis is trace related, a user can define
 *               the step of the axis. Otherwise, a user can define a gap
 *               between two ticks next to each other. Define a reasonable
 *               gap can avoid label overlapping. <br>
 *
 * @author Synthia Kong
 * @version 1.0
 * Fix bug in setVisibleRange() minV/scale
 */

public class BhpTraceTickGenerator implements TickDefinition {
    private static final double _DEFAULT_LABEL_SIZE = 100;
    private static final int _SINGLECHARSPACE = 8;     // space for a single character in device space
    private double _gap;            // gap between labels in device space

    private SeismicWorkflow _pipeline=null;
    private BhpLayer _layer = null;
    private int        _keyField; // key field
    private double     _step;     // step
    private double     _value;    // current value
    private boolean    _isAnnStepTraceRelative;

    private double _modelStart;  // The beginning of the model interval covered by axis
    private double _modelStop;   // The end of the model interval covered by axis
    private double _modelStep;   // The step (in model space)
    private double _modelOrigin; // The origin (in model space)

    private int    _lastIdx;     // The index of the last tick in the whole range of values
    private int    _firstIdx;    // The index of the first tick in the whole range of values
    private int     _minIdx;
    private int     _maxIdx;
    private double  _min;
    private double  _max;

    private boolean _firstTick=true;
    private int     _virtualTraceId;
    private int     _nextTraceId;

    private Bound2D _virtualModelLimits;

    /**
     * Creates a new tick generator for given trace header field
     * in given pipeline and with given step value. <br>
     * @param layer the BhpLayer to be used for data retrieving.
     * @param keyField field for which ticks and labels will be generated.
     * @param step desired step value.
     */
    public BhpTraceTickGenerator(BhpLayer layer, int keyField, double step) {
        this(0, layer, keyField, step, 0);
    }

    /**
     * Creates a new tick generator for given trace header field
     * in given pipeline and with given step value. <br>
     * @param modelOrigin the origin in model space for initial tick in the sequence.
     * @param layer the BhpLayer to be used for data retrieving.
     * @param keyField field for which ticks and labels will be generated.
     * @param step desired step value.
     * @param gap desired gap between ticks. <br>
     *        If this value is 0, a gap value will be calculated to avoid overlapping.
     */
    public BhpTraceTickGenerator(double modelOrigin, BhpLayer layer, int keyField, double step, double gap) {
        _layer = layer;
        _pipeline     = _layer.getPipeline();
        _keyField     = keyField;
        _step         = step;
        _modelOrigin  = modelOrigin;
        _gap = gap;

        HeaderFieldEntry fldDesc;
        _isAnnStepTraceRelative = true;
        int keyFieldType = SeismicConstants.DATA_FORMAT_INT;
        if(_keyField != DataChooser.TRACE_ID_AS_KEY) {
            Vector flds = _pipeline.getDataLoader().getDataReader().getKeys();
            for(int i=0; i < flds.size(); i++) {
                fldDesc = (HeaderFieldEntry) flds.get(i);
                if(fldDesc.getFieldId() == _keyField) {
                    keyFieldType = fldDesc.getValueType();
                    _isAnnStepTraceRelative = fldDesc.getFieldState();
                    break;
                }
            }
        }

		SeismicReader sreader=_pipeline.getDataLoader().getDataReader();

        int tnumber = sreader.getMetaData().getNumberOfTraces();

        if (_gap == 0&&tnumber > 0) {
            // calculate a smart gap number
                double svalue = sreader.getTraceMetaData(0).getFieldValue(_keyField).doubleValue();
                double evalue = sreader.getTraceMetaData(tnumber-1).getFieldValue(_keyField).doubleValue();
                double mvalue = sreader.getTraceMetaData((int)((tnumber-1)/2)).getFieldValue(_keyField).doubleValue();
                double maxValue = Math.max(svalue, Math.max(evalue, mvalue));
                int numberOfDigit = 1;
                if (maxValue >0)
                    numberOfDigit = (int)(Math.log(Math.abs(maxValue))/Math.log(10)) + 1;
                int extraDigit = (int)(numberOfDigit / 3) + 0;      // for ,
                if (keyFieldType == SeismicConstants.DATA_FORMAT_FLOAT ||
                    keyFieldType == SeismicConstants.DATA_FORMAT_IBM_FLOAT ||
                    keyFieldType == SeismicConstants.DATA_FORMAT_DOUBLE)
                    extraDigit = extraDigit + 3;
                _gap = (numberOfDigit + extraDigit) * _SINGLECHARSPACE + 20;

        }

    }

    /**
     * Gets the gap value.
     */
    public double getGap() { return _gap; }
    /**
     * Sets the gap value.
     */
    public void setGap(double g) { _gap = g; }

    /**
     * Resets the tick generator.
     * @param modelStart the start point of model space covered by axis.
     * @param modelStop the end point of model space covered by the axis.
     * @param min the start point of tick generation (in model space)
     * @param max the end point of tick generation (in model space)
     * @param scale the scale from model to device space along the axis' baseline.
     */
    public void reset(double modelStart, double modelStop, double min, double max, double scale) {
        _modelStart = modelStart;
        _modelStop  = modelStop;
        _min=min;
        _max=max;
        if(scale < 0) scale = -scale;
        if(scale == 0) scale = 1;

        _max = Math.ceil( _max ) + _DEFAULT_LABEL_SIZE / scale;
        if(_max > _modelStop) _max = _modelStop;
        _min = Math.floor( _min ) - _DEFAULT_LABEL_SIZE / scale;
        if(_min < _modelStart) _min = _modelStart;

        _firstIdx = (int)Math.ceil((_modelStart - _modelOrigin) / _modelStep);
        _lastIdx  = (int)Math.floor((_modelStop - _modelOrigin) / _modelStep);
        _minIdx   = (int)Math.floor((_min - _modelOrigin) / _modelStep);
        _maxIdx   = (int)Math.ceil((_max - _modelOrigin) / _modelStep);
        if(_minIdx < _firstIdx) _minIdx = _firstIdx;
        if(_maxIdx > _lastIdx)  _maxIdx = _lastIdx;

        _firstTick=true;
        _virtualModelLimits = new Bound2D(_pipeline.getVirtualModelLimits());
        _virtualTraceId = 0;//(_min > _modelStart)? (int)_min : (int)_modelStart;
        if(_isAnnStepTraceRelative) {
            _step = Math.round(_step);
            if(_step == 0) _step = 1;
        }
        else {
            _step = Math.ceil(_gap / scale);
        }
        _modelStep = _step;

        double endTrace = Math.round(_modelStop);
        _value = -Double.MAX_VALUE;     // Get first value

        // if there are gaps in selected traces
        if(_pipeline.getDataLoader().getDataSelector().isGapApplied()) {
            DataChooser ds = _pipeline.getDataLoader().getDataSelector();

            if(_keyField == DataChooser.TRACE_ID_AS_KEY) {
                if(_virtualTraceId < endTrace) {
                    while(((_virtualTraceId - ds.getNumberOfGapTraces(_virtualTraceId)) % _step != 0 ||
                            ds.virtualToReal(_virtualTraceId) == -1) &&
                            _virtualTraceId < endTrace) {
                        _virtualTraceId++;
                    }
                }
                if(_virtualTraceId < endTrace) {
                    _value = ds.virtualToReal(_virtualTraceId); // + 1;
                    double[] idSettings = _layer.getParameter().
                        getKeySettingsOfName("TraceNumber");
                    _value = (int) (idSettings[0] + _value * idSettings[2]);
                }
            }
            // else if(_isAnnStepTraceRelative) {
            else {
                while(((_virtualTraceId - ds.getNumberOfGapTraces(_virtualTraceId)) % _step != 0 ||
                            ds.virtualToReal(_virtualTraceId) == -1) &&
                            _virtualTraceId < endTrace) {
                    _virtualTraceId++;
                }
                if(_virtualTraceId < endTrace) {
                    TraceHeader metadata = _pipeline.getDataLoader().getDataSelector().
		            getTraceHeader(_virtualTraceId);
                    if(metadata != null)
                        _value = metadata.getFieldValue(_keyField).doubleValue();
                }
            }

        }
        // if without gaps
        else {
            if(_keyField == DataChooser.TRACE_ID_AS_KEY) {
                while(_virtualTraceId % _step != 0 && _virtualTraceId < endTrace)
                    _virtualTraceId++;
                if(_virtualTraceId < endTrace) {
                    _value = _pipeline.getDataLoader().getDataSelector().virtualToReal(_virtualTraceId); // + 1;
                    double[] idSettings = _layer.getParameter().getKeySettingsOfName("TraceNumber");
                    _value = (int) (idSettings[0] + _value * idSettings[2]);
                }
            }
            // else if(_isAnnStepTraceRelative) {
            else {
                while(_virtualTraceId % _step != 0 && _virtualTraceId < endTrace)
                    _virtualTraceId++;
                if(_virtualTraceId < endTrace) {
                    TraceHeader metadata = _pipeline.getDataLoader().getDataSelector().
		            getTraceHeader(_virtualTraceId);
                    if(metadata != null) _value = metadata.getFieldValue(_keyField).doubleValue();
                }
            }
        }
        _nextTraceId=_virtualTraceId;
    }

    /**
     * Finds out if the tick generator can produce more ticks.
     */
    public boolean moreTicks() {

        if(_firstTick != true) {
            // Gets the next virtual tick without changing the data
            _value=generateNextTick(_virtualTraceId);
        }
        
        return hasNextTick(_nextTraceId);
    }

    private boolean hasNextTick(int virtualTraceId) {
        return  (virtualTraceId < _virtualModelLimits.getMaxX() &&
                      virtualTraceId  < (int) _max);
 
    }

    private double generateNextTick(int virtualTraceId) {
        TraceHeader metadata;
        _nextTraceId=virtualTraceId;
        double  value=_value;

        //if(_isAnnStepTraceRelative || _keyField == cgDataSelector.CG_TRACE_ID) {
            double count = 0;
            double endTrace = _max;
            // First determine virtual trace ID taking in account possible presence of gaps
            while((count < _step) && (_nextTraceId  < endTrace)) {
                _nextTraceId++;
                if(_pipeline.getDataLoader().getDataSelector().virtualToReal(_nextTraceId) != -1) count++;
            }
            // Then determine desired value
            if( hasNextTick(_nextTraceId) ) {
                if( _keyField == DataChooser.TRACE_ID_AS_KEY ) {
                    value = _pipeline.getDataLoader().getDataSelector().virtualToReal( _nextTraceId ); // + 1;
                    double[] idSettings = _layer.getParameter().getKeySettingsOfName("TraceNumber");
                    value = (int) (idSettings[0] + value * idSettings[2]);
                }
                else {
                    metadata = _pipeline.getDataLoader().getDataSelector().
		            getTraceHeader( _nextTraceId );
                    if (metadata == null) value = 0.0;
                    else value = metadata.getFieldValue( _keyField ).doubleValue();
                }
            }

        return value;
    }

    /**
     * Steps to the next tick and returns the object associated with tick.
     * @return the object associated with tick
     */
    public Object getTickData() {
            _virtualTraceId=_nextTraceId;
            _firstTick=false;
            return new Double( _value + _modelOrigin);

    }

    /**
     * Returns the position in model space.
     */
    public double getTickPosition() {
        return _virtualTraceId + 0.5+ _modelOrigin;
    }

	public DataRange getModelRange() {
		return new DataRange(0, _pipeline.getDataLoader().getDataReader().getMetaData().getNumberOfTraces());
	}

	public void setModelRange(double start, double stop) {
		_modelStart = start;
        _modelStop  = stop;
	}

	public void setVisibleRange(double minV, double maxV, double scale) {
		if(_keyField == DataChooser.TRACE_ID_AS_KEY) {
			int nt = _pipeline.getDataLoader().getDataReader().getMetaData().getNumberOfTraces();
			reset(0, nt*scale, minV/scale, maxV/scale, scale);
		}else{
			double s=_pipeline.getDataLoader().getDataSelector().getPrimaryKeyStart();
			double e=_pipeline.getDataLoader().getDataSelector().getPrimaryKeyEnd();
			reset(s, e, minV/scale, maxV/scale, scale);
		}
	}

}
