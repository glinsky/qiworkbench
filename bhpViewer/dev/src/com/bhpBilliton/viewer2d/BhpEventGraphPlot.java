/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;


import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;

import com.bhpBilliton.viewer2d.data.BhpEventReaderInterface;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.gw2d.event.ViewChangeEvent;
import com.gwsys.gw2d.event.ViewChangeListener;
import com.gwsys.gw2d.model.CommonDataModel;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.gw2d.model.CommonShapeLayer;
import com.gwsys.gw2d.ruler.AdjustableStepTick;
import com.gwsys.gw2d.ruler.FixedableStepTick;
import com.gwsys.gw2d.ruler.RangeRulerModel;
import com.gwsys.gw2d.ruler.RulerModel;
import com.gwsys.gw2d.ruler.RulerView;
import com.gwsys.gw2d.ruler.TickDefinition;
import com.gwsys.gw2d.shape.GridLine;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;
import com.gwsys.gw2d.view.BoxContainer;
import com.gwsys.gw2d.view.MultipleViewContainer;
import com.gwsys.gw2d.view.ScrollableView;
import com.gwsys.gw2d.view.SimpleViewContainer;
import com.gwsys.seismic.io.SeismicMetaData;
import com.gwsys.seismic.reader.SeismicReader;
import com.gwsys.seismic.util.PlotConstants;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This plot is used to show horizon amplitude graph. <br>
 *               For each horizon layer showing in the main plot,
 *               its amplitude line is drawn here use the same graphic
 *               line attribute used to draw the horizon itself.
 *               <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 * @version 2.0 To match new API
 */

public class BhpEventGraphPlot extends ScrollableView
        implements BhpEventLayerListener, ViewChangeListener {
    private static final long serialVersionUID = -2729346643945364431L;
    private AbstractPlot _mainPlot;
    private SimpleViewContainer _plotView;
    private CommonShapeLayer _shapeLayer;
    private CommonDataModel _model;
    private MultipleViewContainer _view;
    private BhpEventGraphRubberView _rubberView;
    private Transform2D _transformation;
    //private Transform2D _vaxisTrans;
    //private RenderingAttribute _attribute;

    private BoxContainer _annL;

    private int _plotHeight = 100;

    private boolean _showGrid;
    private GridLine _gridShape;
    private CommonShapeLayer _gridLayer;
    private RenderingAttribute _gridAttribute;
    private boolean _verticalAuto;
    private double _minValue;
    private double _maxValue;
    private double _autoMinValue;
    private double _autoMaxValue;
    private double _majorStep;
    private double _minorStep;

    /**
     * Constructs a new instance and sets up its GUI.
     */
    public BhpEventGraphPlot(AbstractPlot p) {
        super();
        _mainPlot = p;
        _transformation = null;
        //_attribute = new RenderingAttribute();
        _showGrid = true;
        _gridShape = null;
        _gridLayer = null;
        _gridAttribute = new RenderingAttribute();
        _gridAttribute.setLineColor(Color.lightGray);
        _verticalAuto = true;
        _minValue = 0;
        _maxValue = 0;
        _autoMinValue = 0;
        _autoMaxValue = 0;
        _majorStep = 0;
        _minorStep = 0;
        setupMainView();
        setupAnnotation();
        //updateAnnotation();
        Dimension size = new Dimension(300, _plotHeight+2);
        this.setPreferredSize(size);
        this.setMinimumSize(size);
        this.setMaximumSize( new Dimension(100000, _plotHeight+2));
        this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        this.setViewportBorder(BorderFactory.createLineBorder(Color.black));
    }

    private void setupMainView() {
        _model = new CommonDataModel();
        _shapeLayer = new CommonShapeLayer();
        _model.addLayer(_shapeLayer);

        _view = new MultipleViewContainer();
        //_view.setCacheMode(cgStackedPlotView.CG_VIEW_SIZE_CACHE);
        _plotView = new SimpleViewContainer(_model);
        _view.add(_plotView);
        _rubberView = new BhpEventGraphRubberView(_mainPlot, this);
        _view.add(_rubberView, 0);
        _view.setPreferredSize(new Dimension(_mainPlot.getSize().width, _plotHeight+2));
        this.getViewport().setView(_view);
        _view.addMouseListener(_rubberView);
        _view.addMouseMotionListener(_rubberView);
    }

    private void setupAnnotation() {
        _annL = new BoxContainer(BoxLayout.X_AXIS);
               setVerticalAxis(_annL);
    }


    /**
     * Converts the plot object into an XML string. <br>
     * This is used when save the working session into an XML file.
     * Please refer to BhpViewer.dtd for the detail of the format.
     * @return an XML string for the object.
     */
    public String toXMLString() {
        StringBuffer content = new StringBuffer();
        content.append("        " + "<HorizonGraph ");
        content.append("showGrid=\"" + this._showGrid + "\" ");
        content.append("verticalAuto=\"" + this._verticalAuto + "\" ");
        content.append("minValue=\"" + this._minValue + "\" ");
        content.append("maxValue=\"" + this._maxValue + "\" ");
        content.append("majorStep=\"" + this._majorStep + "\" ");
        content.append("minorStep=\"" + this._minorStep + "\"/>\n");
        return content.toString();
    }

    /**
     * This method is called when the event layer and its shape is changed. <br>
     * It clears the current selection and re-selects the shape thus
     * selection object will be updated accordingly.
     * @param source the layer that has changed.
     */
    public void eventLayerChanged(BhpLayer source) {
        BhpEventGraphShape shape =
                findEventGraphShapeForId(source.getIdNumber());
        if (shape != null) _shapeLayer.removeShape(shape);
        addEventGraphShapeForLayer(source);
        this.repaint();
    }

    public void viewChanged(ViewChangeEvent e) {
        if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("viewChanged : " + e.toString());
        if (_transformation == null) return;
        int nwidth = _mainPlot.getMainView().getWidth();
        int xpos = _mainPlot.getMainView().getX();
        if (nwidth != _view.getWidth() || xpos != _view.getX()) {
            // annotation changed
            updateVAxes();
            _annL.setPreferredSize(new Dimension(xpos, _plotHeight));
            this.invalidate();
            this.validate();
        }
    }

    /**
     * Method of ChangeListener.
     */
    public void stateChanged(ChangeEvent e) {

        if (_transformation == null) return;
        if (e == null) return;

        if (e.getSource() instanceof BhpPlotXV) {

            if (_transformation != null) {
                double xs = _transformation.getScaleX();
                double newxs = _mainPlot.getHorizontalScale();
                _transformation.scale(newxs/xs, 1.0);
                _view.setTransformation(_transformation);
                if (_gridShape != null) {
                    double xfactor = _transformation.getScaleX();
                    Bound2D obox = _gridShape.getBoundingBox(null);
                    Bound2D nbox = new Bound2D(0, obox.getMinY(),
                            100000/xfactor, obox.getMaxY());
                    _gridShape.setBoundingBox(nbox, true);
                }
            }

            //_view.setViewPosition(_mainPlot.getMainView().getViewPosition().x, 0);
        }
    }

    public boolean getShowGrid() { return _showGrid; }
    public void setShowGrid(boolean b) { _showGrid = b; }
    public boolean getVerticalAuto() { return _verticalAuto; }
    public void setVerticalAuto(boolean b) { _verticalAuto = b; }
    public double getMinValue() { return _minValue; }
    public void setMinValue(double m) { _minValue = m; }
    public double getMaxValue() { return _maxValue; }
    public void setMaxValue(double m) { _maxValue = m; }
    public double getMajorStep() { return _majorStep; }
    public void setMajorStep(double s) { _majorStep = s; }
    public double getMinorStep() { return _minorStep; }
    public void setMinorStep(double s) { _minorStep = s; }

    public void eventValueChanged(int index, double value, boolean reset, int layerid) {
        BhpEventGraphShape shape =
                findEventGraphShapeForId(layerid);
        if (shape != null) {
            BhpLayer refLayer = _mainPlot.getBhpLayer(_mainPlot.getAxisAssociateLayer());
            if (refLayer.getDataType() == BhpViewer.BHP_DATA_TYPE_SEISMIC) {
                if (!(BhpEventShape.isNullValue(value, shape.getNullValue())))
                    value = refLayer.findSpecificDataValue(index, value);
            }
            shape.setValueAt(index, value, reset);
        }
    }

    /**
     * Removes layers from the plot. <br>
     * If the layer is a BhpEventLayer, its corresponding shape will
     * be removed. Otherwise, nothing will happen.
     * @param layer layers that need to be removed.
     */
    public void removeBhpLayer(BhpLayer[] layer) {
        for (int i=0; i<layer.length; i++) {
            if (!(layer[i] instanceof BhpEventLayer)) continue;
            BhpEventGraphShape shape =
                    findEventGraphShapeForId(layer[i].getIdNumber());
            if (shape != null) _shapeLayer.removeShape(shape);
        }
        //_view.setTransformationWithoutPropogation(_view.getTransformation());
        //this.validate();
    }

    /**
     * Adds a layer to the plot. <br>
     * If the layer is a BhpEventLayer, a shape will be added.
     * @param layer the layer needs to be added.
     */
    public void addBhpLayer(BhpLayer layer) {
        if (!(layer instanceof BhpEventLayer)) return;
        BhpEventLayer elayer = (BhpEventLayer) layer;

        try {
            addEventGraphShapeForLayer(elayer);
            elayer.addEventLayerListener(this);

            if (_transformation == null) {
                BhpEventReaderInterface theReader =
                        (BhpEventReaderInterface) elayer.getPipeline().getDataLoader().getDataReader();
                double minValue = theReader.getMinDepth();
                double maxValue = theReader.getMaxDepth();
                int numberOfTraces = elayer.getEventValues().length;
                BhpLayer refLayer = _mainPlot.getBhpLayer(_mainPlot.getAxisAssociateLayer());
                if (refLayer.getDataType() == BhpViewer.BHP_DATA_TYPE_SEISMIC) {
                    SeismicReader refReader = refLayer.getPipeline().getDataLoader().getDataReader();
                    minValue = refReader.getMetaData().getMinimumAmplitude();
                    maxValue = refReader.getMetaData().getMaximumAmplitude();
                }
                // round off the min and max value
                double diffValue = Math.abs(maxValue - minValue);
                if (diffValue != 0) {
                    int sigDigit = (int) (Math.log(diffValue) / Math.log(10.0));
                    double factor = Math.pow(10.0, sigDigit);
                    if (_majorStep == 0) _majorStep = factor;
                    if (_minorStep == 0) _minorStep = factor / 2.0;
                    double maxBase = maxValue / factor;
                    double minBase = minValue / factor;
                    double maxMove = Math.ceil(maxBase) - maxBase;
                    double minMove = Math.floor(minBase) - minBase;
                    if (Math.abs(maxMove) > 0.5) {
                        maxMove = 0.5;
                        if (maxValue < 0) maxMove = -0.5;
                    }
                    else maxMove = 0;
                    if (Math.abs(minMove) > 0.5) {
                        minMove = 0.5;
                        if (minValue < 0) minMove = -0.5;
                    }
                    else minMove = 0;
                    //System.out.println("Before round " + minValue + " - " + maxValue);
                    maxValue = (Math.ceil(maxBase)-maxMove) * factor;
                    minValue = (Math.floor(minBase)-minMove) * factor;
                    //System.out.println("After  round " + minValue + " - " + maxValue);
                }

                _autoMinValue = minValue;
                _autoMaxValue = maxValue;
                _minValue = minValue;
                _maxValue = maxValue;

                updateTransformation();
            }
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(layer.getBhpViewerBase(), Thread.currentThread().getStackTrace(),
                                                "Failed to add layer " + elayer.getIdNumber() +
                                                    " to the horizon graph plot.");
            //((BhpViewerBase)SwingUtilities.windowForComponent(_mainPlot)).
            //        getStatusBar().showMessage(BhpViewer.ERROR_MESSAGE, msg);
        }
    }

    public void hideEventGraphSymbol() {
        _rubberView.hideEventGraphSymbol();
        this.repaint();
    }

    public void updateEventGraphSymbolSize(double w, double h) {
        _rubberView.updateEventGraphSymbolSize(w, h);
    }

    public void setEventGraphSymbolAnchor(int x, int y) {
        _rubberView.setEventGraphSymbolAnchor(x, y);
    }

    public void updateTransformation() {
        if (_verticalAuto)
            calculateTransformation(_autoMinValue, _autoMaxValue);
        else {
            calculateTransformation(_minValue, _maxValue);
        }
    }

    private void calculateTransformation(double minValue, double maxValue) {

        //Transform2D transx = layer.getTransformation();
        int numberOfTraces = 100000;
        BhpLayer refLayer = _mainPlot.getBhpLayer(_mainPlot.getAxisAssociateLayer());
        if (refLayer != null) {
            numberOfTraces = refLayer.getPipeline().
            getDataLoader().getDataReader().getMetaData().
                             getNumberOfTraces();
        }
        Bound2D model = new Bound2D(0, minValue, numberOfTraces, maxValue);
        _model.setBoundingBox(model);
        _shapeLayer.setBoundingBox(model);
        Bound2D device = new Bound2D(0, 0, numberOfTraces, _plotHeight);
        Transform2D transy = new Transform2D(model, device, true, false);
        double sx = _mainPlot.getHorizontalScale();
        double sy = transy.getScaleY();
        double ty = transy.getTranslateY();
        _transformation = new Transform2D(sx, sy, 0, ty);
        //System.out.println("Trans : " + sx + ", " + sy + ", " + ty);
        //System.out.println("Lim   : " + minValue + ", " + maxValue);
        _view.setTransformation(_transformation);

        updateAnnotation();
   }

    /**
     * Updates the annotation fo the plot. <br>
     * Implementation of the method is reponsible for
     * updating the vertical axis, horizontal axis, title,
     * color bar, and labels.
     */
    private void updateAnnotation() {
        updateVAxes();
    }

    /**
     * Clears object of the given container.
     * @param container the container that need to be cleaned.
     * @param isAxis if true, all cgAxisView will be removed.
     *        If false, other view will be removed.
     */
    private void clearAnnotation(BoxContainer container, boolean isAxis) {
        ArrayList v = new ArrayList();
        ScrollableView plot;
        for (int i=0; i<container.getComponentCount(); i++) {
            plot = (ScrollableView) container.getComponent(i);
            if ((plot.getComponent(0) instanceof RulerView) == isAxis)//
                v.add(plot);
        }
        for (int i=0; i<v.size(); i++)
            container.remove((Component) v.get(i));
        v.clear();
    }

    private void updateVAxes() {
        clearAnnotation(_annL, true);

        int vAxesLoc = _mainPlot.getVAxesLoc();
        SeismicMetaData meta = _mainPlot.getFirstLayer().getPipeline().
        getDataLoader().getDataReader().getMetaData();

        if (vAxesLoc != PlotConstants.NONE) {
                //double scaley = 1.0;
                //double transy = 0.0;
                double realStart = meta.getMinimumAmplitude();
                double realEnd = meta.getMaximumAmplitude();
                /* if (_transformation != null) {
                scaley = _transformation.getScaleY();
                transy = _transformation.getTranslateY();

                Bound2D device = new Bound2D(0, 0, 100, _plotHeight);
                Bound2D model = _transformation.inverseTransform(device);
                realStart = model.getMinY();
                realEnd = model.getMaxY();
            }*/
//          _vaxisTrans = new Transform2D(1.0, scaley, 0, transy);
            TickDefinition tg1 = new FixedableStepTick(realStart, realEnd,
                    Math.abs(realStart));
            NumberFormat nfMajor = NumberFormat.getInstance();
            NumberFormat nfMinor = NumberFormat.getInstance();
            if (realStart < 1000) {
                nfMajor.setMinimumFractionDigits(2);
                nfMajor.setMaximumFractionDigits(2);
                nfMinor.setMinimumFractionDigits(2);
                nfMinor.setMaximumFractionDigits(2);
            }
            else {
                nfMajor.setMaximumFractionDigits(0);
                nfMinor.setMaximumFractionDigits(0);
            }
            //tg1.setModelRange(realStart, realEnd);

            if ((vAxesLoc & PlotConstants.LEFT) != 0) {

                RulerModel axisModel=new RangeRulerModel(RangeRulerModel.VERTICAL,tg1);
                RulerView axisL = new RulerView(axisModel);
                //cgAxisView axisL = new cgAxisView(cgAxisShape.WEST, realStart, realEnd, tg1, ar);
                axisL.setBackground(Color.white);
                axisL.setPreferredSize(new Dimension(80, _plotHeight));
                _annL.add(axisL, _annL.getComponentCount());
            }
            else {
                // draw a baseline only left axis as the border
                RulerModel axisModel=new RangeRulerModel(RangeRulerModel.VERTICAL,tg1);
                axisModel.setTickVisible(false);
                RulerView axisL = new RulerView(axisModel);
                axisL.setBackground(Color.white);
                axisL.setPreferredSize(new Dimension(2, _plotHeight));
                _annL.add(axisL, _annL.getComponentCount());
            }
            if (_gridLayer == null) {
                _gridLayer = new CommonShapeLayer();
                _model.insertLayer(_gridLayer, 0); // this put grid behind the curves
            }
            _gridLayer.removeAllShapes();
            if (_showGrid && _transformation != null) {
                //TickGenerator gtg = new NumericAdaptiveTickGenerator(20, 0);
                //cgGridRenderer gr = new cgRegularGridRenderer(_gridAttribute);

                double xfactor = 1;
                if (_transformation!=null)
                    xfactor = _transformation.getScaleX();
                Bound2D gridBbox = new Bound2D(0, realStart, 100000/xfactor, realEnd);
                TickDefinition tickY = new FixedableStepTick(realStart, realEnd,
                        Math.abs(realStart));
                TickDefinition tickX = new AdjustableStepTick(
                        0,100000/xfactor, 5000);
                _gridShape = new GridLine(gridBbox, tickY, tickX);
                _gridShape.setAttribute(_gridAttribute);
                _gridShape.setVisible(true);
                _gridLayer.addShape(_gridShape);
            }
        }
        _annL.validate();
    }

    private void addEventGraphShapeForLayer(BhpLayer elayer) {
        BhpEventPipeline epipeline = (BhpEventPipeline) elayer.getPipeline();
        BhpEventReaderInterface theReader =
                (BhpEventReaderInterface) epipeline.getDataLoader().getDataReader();
        //double modelUnitPerTrace = elayer.getLayerHorizontalScale();
        //double modelUnitPerSample = elayer.getLayerVerticalScale();
        double minValue = theReader.getMinDepth();
        double maxValue = theReader.getMaxDepth();
        BhpEventShape eshape = (BhpEventShape) elayer.getShapeListLayer().getShape(0);
        double[] yvalues = ((BhpEventLayer)elayer).getEventValues();
        BhpLayer refLayer = _mainPlot.getBhpLayer(_mainPlot.getAxisAssociateLayer());
        if (refLayer.getDataType() == BhpViewer.BHP_DATA_TYPE_SEISMIC) {
            SeismicReader refReader = refLayer.getPipeline().getDataLoader().getDataReader();
            minValue = refReader.getMetaData().getMinimumAmplitude();
            maxValue = refReader.getMetaData().getMaximumAmplitude();
            for (int i=0; i<yvalues.length; i++) {
                if (!(BhpEventShape.isNullValue(yvalues[i], eshape.getNullValue())))
                    yvalues[i] = refLayer.findSpecificDataValue(i, yvalues[i]);
            }
        }
        float xstart = 0;
        float xstep = 1;
        BhpEventGraphShape shape = new BhpEventGraphShape(
                yvalues, xstart, xstep, minValue, maxValue,
                elayer.getIdNumber(), eshape.getNullValue());
        shape.setEventAttribute(eshape);
        _shapeLayer.addShape(shape);

    }

    private BhpEventGraphShape findEventGraphShapeForId(int id) {
        Object shape = null;
        BhpEventGraphShape eshape = null;
        Iterator iter = _shapeLayer.getShapes();
        while(iter.hasNext()) {
            shape = iter.next();
            if (shape instanceof BhpEventGraphShape) {
                eshape = (BhpEventGraphShape) shape;
                if (eshape.getShapeId() == id) return eshape;
            }
        }
        return null;
    }
}
