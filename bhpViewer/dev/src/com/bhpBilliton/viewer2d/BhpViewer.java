/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d;

import javax.swing.JDesktopPane;

import org.w3c.dom.Node;

/**
 * Refactor the BhpViewerBase
 * @author
 *
 */
public interface BhpViewer {
	public static final int ERROR_MESSAGE = 101;

	public static final int WARNING_MESSAGE = 102;

	public static final int INFO_MESSAGE = 103;

	//Window Type
	public static final int WINDOW_TYPE_XSECTION = 1;

	public static final int WINDOW_TYPE_MAPVIEWER = 2;

	//Layer Type
	/**
	 * Unknow data.
	 */
	public static final int BHP_DATA_TYPE_UNKNOWN = -1;

	/**
	 * Seismic data.
	 */
	public static final int BHP_DATA_TYPE_SEISMIC = 1;

	/**
	 * Model data.
	 */
	public static final int BHP_DATA_TYPE_MODEL = 2;

	/**
	 * Event data.
	 */
	public static final int BHP_DATA_TYPE_EVENT = 3;

	/**
	 * Log data.
	 */
	public static final int BHP_DATA_TYPE_LOG = 5;

	/**
	 * Normal data stored in cross section order.
	 */
	public static final int BHP_DATA_ORDER_CROSSSECTION = 0;

	/**
	 * Transposed data stored in map view order.
	 */
	public static final int BHP_DATA_ORDER_MAPVIEW = 1;

	/**
	 * Local bhpio data.
	 */
	public static final int BHP_DATA_SOURCE_LOCAL = 1;

	/**
	 * Remote bhpio data access via servlets.
	 */
	public static final int BHP_DATA_SOURCE_REMOTE = 2;

	/**
	 * OpenSpirit data with local OpenSpirit client.
	 */
	public static final int BHP_DATA_SOURCE_OPENSPIRIT = 3;

	/**
	 * OpenSpirit data with remote OpenSpirit client and access via servlets.
	 */
	public static final int BHP_DATA_SOURCE_OPENSPIRIT_REMOTE = 4;

	/**
	 * Flag for cursor setting and cursor synchronizatioin.
	 */
	public static final int CURSOR_FLAG = 0x0000002;

	/**
	 * Flag for color map.
	 */
	public static final int COLORBAR_FLAG = 0x0000004;

	/**
	 * Flag for plot type of a rasterizer.
	 */
	public static final int DISPLAY_FLAG = 0x0000008;

	/**
	 * Flag for layer visibility.
	 */
	public static final int VISIBLE_FLAG = 0x0000010;

	/**
	 * Flag for layer displaying order.
	 */
	public static final int ORDER_FLAG = 0x0000020;

	/**
	 * Flag for layer opacity.
	 */
	public static final int TRANSPARENCY_FLAG = 0x0000040;

	/**
	 * Flag for the scale of trace normalization.
	 */
	public static final int SCALEAMP_FLAG = 0x0000080;

	/**
	 * Flag for horizontal scale.
	 */
	public static final int SCALEH_FLAG = 0x0000100;

	/**
	 * Flag for vertical scale.
	 */
	public static final int SCALEV_FLAG = 0x0000200;

	/**
	 * Flag for polarity.
	 */
	public static final int POLARITY_FLAG = 0x0000400;

	/**
	 * Flag for trace interpolation type.
	 */
	public static final int INTERPOLATION_FLAG = 0x0000800;

	/**
	 * Flag for data selector settings.
	 */
	public static final int SELECTOR_FLAG = 0x0001000;

	/**
	 * Flag for vertical range settings.
	 */
	public static final int TIMERANGE_FLAG = 0x0002000;

	/**
	 * Flag for the type of trace normalization.
	 */
	public static final int NORMALIZE_FLAG = 0x0004000;

	/**
	 * Flag for AGC settings.
	 */
	public static final int AGC_FLAG = 0x0008000;

	/**
	 * Flag for color interpolation.
	 */
	public static final int COLORINTER_FLAG = 0x0010000;

	/**
	 * Flag for event shape graphic attribute.
	 */
	public static final int EVENTATT_FLAG = 0x0020000;

	/**
	 * Flag for event name.
	 */
	public static final int EVENTNAME_FLAG = 0x0040000;

	/**
	 * Flag for mat setting, includes image transpose, lock aspect ratio, axes
	 * directions, and the start and end values of horizontal axis.
	 */
	public static final int MAPSETTING_FLAG = 0x0080000;

	/**
	 * Flag for data name. <br>
	 * This flag usually indicates new data.
	 */
	public static final int NAME_FLAG = 0x0100000;

	/**
	 * Flag for making data increment. Please refer to
	 * <code>{@link StepUpAction}</code> for more detail. <br>
	 * This flag usually indicates new data.
	 */
	public static final int DATAINCREMENT_FLAG = 0x0200000;

	/**
	 * Flag for data selction made in <code>{@link BhpSeismicTableModel}</code>.
	 * <br>
	 * This flag usually indicates new data.
	 */
	public static final int DATASELECTION_FLAG = 0x0400000;

	/**
	 * Flag for attribute name of <code>{@link BhpModelLayer}</code>.<br>
	 * This flag usually indicates new data.
	 */
	public static final int MODEL_FLAG = 0x0800000;

	/**
	 * Flag for window view position synchronization.
	 */
	public static final int WINDOW_VIEWPOS_FLAG = 0x1000000;

	/**
	 * Flag for seismic wiggle decimation
	 */
	public static final int WIGGLEDECIMATION_FLAG = 0x2000000;

	/**
	 * Flag for event data change
	 */
	public static final int EVENTVALUE_FLAG = 0x4000000;

	public void initWithXML(Node node);

	public String toXMLString();

	public JDesktopPane getDesktop();

	public void broadcastBhpLayerEvent(BhpLayerEvent event);

	public StatusBar getStatusBar();
}
