/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;


import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.bhpBilliton.viewer2d.data.BhpDataReader;
import com.bhpBilliton.viewer2d.data.GeneralDataSource;
import com.bhpBilliton.viewer2d.util.ErrorDialog;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class represents temporary event data source.
 *               When a new event layer is created with
 *               "Create New Horizon" menu, an instance of this
 *               data source is created. The user can later save
 *               the event data to disk with appropriate format.<br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class TemporaryEventDataSource implements GeneralDataSource {
    private ArrayList _headersName;
    private ArrayList _headersSetting;
    private ArrayList _eventData;  // list of double[], headerValues+eventValue
    private BhpLayer _refLayer;
    private BhpLayer _eventLayer = null;

    private String _fileName;

    /**
     * Constructs a new instance.
     */
    public TemporaryEventDataSource(BhpLayer layer) {
        _headersName = new ArrayList();
        _headersSetting = new ArrayList();
        _eventData = new ArrayList();
        _fileName = null;
        _refLayer = layer;
        if (_refLayer != null) {
            GeneralDataSource source = _refLayer.getParameter().getDataSource();
            if (source != null) {
                String[] headers = source.getHeaders();
                for (int i=0; i<headers.length-1; i++) {
                    _headersName.add(headers[i].trim());
                    _headersSetting.add(source.getHeaderSetting(headers[i]));
                }
            }
        }
    }

    /**
     * Gets the name of this data source implementation.
     */
    public String getDataSourceName() { return "TemporaryEventDataSource"; }
    /**
     * Gets the name of the data.
     */
    public String getDataName() { return "UnnamedData"; }
    /**
     * Gets the pathlist of the data.
     */
    public String getDataPath() { return "UnknownPath"; }
    /**
     * Gets the data attribute string.
     */
    public String getDataAtt()  { return "Unnamed new HORIZON Data";  }
    /**
     * Gets the type of the data. <br>
     * Please refer to <code>{@link BhpLayer}</code>
     * for data type definitions.
     */
    public int getDataType() { return BhpViewer.BHP_DATA_TYPE_EVENT; }

    /**
     * Retrieves all the available properties/events.
     * @return a string with all properties/events separated by spacce.
     */
    public String getProperties() { return "UnnamedEvent"; }

    /**
     * Gets the name of all headers.
     */
    public String[] getHeaders() {
        Object[] nobj = _headersName.toArray();
        String[] result = new String[nobj.length];
        for (int i=0; i<nobj.length; i++) {
            result[i] = nobj[i].toString();
        }
        return result;
    }

    /**
     * Gets the setting of a specific header.
     * @param name name of the requested header.
     * @return the settings of the header with format: start-end[step].
     */
    public String getHeaderSetting(String name) {
        String tname = null;
        for (int i=0; i<_headersName.size(); i++) {
            tname = _headersName.get(i).toString();
            if (tname.equals(name))
                return _headersSetting.get(i).toString();
        }
        return "";
    }

    /**
     * Creates a <code>{@link BhpDataReader}</code> for this data source.
     * @param layer the <code>{@link BhpLayer}</code> that this reader will
     *        be associated with.
     */
    public BhpDataReader createBhpDataReader(BhpLayer layer, BhpPropertyManager pmanager) {
        _eventLayer = layer;
        return new TemporaryEventDataReader(_refLayer, this);
    }

    /**
     * Gets the order of the data.
     * Please refer to <code>{@link BhpLayer}</code>
     * for data order definitions.
     */
    public int getDataOrder()     { return BhpViewer.BHP_DATA_ORDER_CROSSSECTION;  }

    public void addHeader(String name, String att) {
        _headersName.add(name);
        _headersSetting.add(att);
    }

    /**
     * Updates the value of the named event with specific header values.
     * @param name the name of the requested event.
     * @param value the new event value.
     * @param param a hashtalbe specify the name and value of the headers.
     */
    public void putEventValueAt(String name, double value, Hashtable param) {
        double[] queryValues = checkHeaders(param);
        if (queryValues == null) return;
        double[] dataRecord;
        for (int i=0; i<_eventData.size(); i++) {
            dataRecord = (double[]) (_eventData.get(i));
            int j;
            for (j=0; j<queryValues.length; j++) {
                if (queryValues[j] != dataRecord[j]) break;
            }
            if (j == queryValues.length) {
                // loop to the end, match found;
                if (BhpEventShape.isNullValue(value, Double.NaN)) {
                    _eventData.remove(i);
                    return;
                }
                dataRecord[j] = value;
                return;
            }
        }
        // not an existing record, create new
        if (BhpEventShape.isNullValue(value, Double.NaN)) return;
        double[] newRecord = new double[queryValues.length+1];
        int k;
        for (k=0; k<queryValues.length; k++)
            newRecord[k] = queryValues[k];
        newRecord[k] = value;
        _eventData.add(newRecord);
    }

    /**
     * Retrieves the value of the named event with specific header values.
     * @param name the name of the requested event.
     * @param param a hashtalbe specify the name and value of the headers.
     * @return a float for the specific event point.
     */
    public float getEventValueAt(String name, Hashtable param) {
        float nullValue = Float.NaN;
        if (_eventData == null || _eventData.size() == 0) return nullValue;
        if (param.size() != _headersName.size()) return nullValue;

        double[] queryValues = checkHeaders(param);
        if (queryValues == null) return nullValue;

        double[] dataRecord;
        for (int i=0; i<_eventData.size(); i++) {
            dataRecord = (double[]) (_eventData.get(i));
            int j;
            for (j=0; j<queryValues.length; j++) {
                if (queryValues[j] != dataRecord[j]) break;
            }
            if (j == queryValues.length) {
                // loop to the end, match found;
                return (float)(dataRecord[j]);
            }
        }
        return nullValue;
    }

    //public String getFileName() { return _fileName; }
    public void setFileName(String f) { _fileName = f; }

    public List getEventDataList() { return _eventData; }

    /**
     * Saves the event data in memory back to disk.
     */
/*
    public void saveEvent() throws Exception {
        BhpViewerBase viewer = (BhpViewerBase) (SwingUtilities.windowForComponent(_refLayer));
        GeneralDataChooser chooser = BhpDataAdapterFactory.createDataChooser(
                            viewer, null, GeneralDataSource.DATA_SOURCE_XMLHRZ);
        if (chooser instanceof EventDataSaver) {
            cgAttribute cgAttr = null;
            DataShape shape = _eventLayer.getShapeListLayer().getShape(0);
            if (shape != null) cgAttr = shape.getAttribute();
            _fileName = ((EventDataSaver)chooser).saveEventDataToFile(
                    _headersName, _eventData, cgAttr, _fileName);
        }
    }
*/
/*
    private void saveLocal(File file) throws Exception {
        OutputStream output = new BufferedOutputStream(new FileOutputStream(file));
        PrintWriter writer = new PrintWriter(output);

        writer.println("<?xml version=\"1.0\" ?>");
        writer.println("");

        writer.println("<EVENTS>");

        writer.println("    <KEYS>");
        String name;
        String[] names = new String[_headersName.size()+1];
        for (int i=0; i<_headersName.size(); i++) {
            name = _headersName.get(i).toString();
            names[i] = name;
            writer.println("        <KEY name=\""+name+"\">");
            writer.println("            <FORMAT>FLOAT64</FORMAT>");
            writer.println("        </KEY>");
        }
        names[names.length-1] = "Time";
        writer.println("    </KEYS>");

        writer.println("    <EVENT name=\"NewHorizon\">");
        double[] dataRecord = null;
        StringBuffer line = null;
        for (int j=0; j<_eventData.size(); j++) {
            dataRecord = (double[]) (_eventData.get(j));
            line = new StringBuffer();
            line.append("        <DATA ");
            for (int k=0; k<names.length; k++) {
                line.append(names[k]+"=\""+dataRecord[k]+"\" ");
            }
            line.append("/>");
            writer.println(line.toString());
        }
        writer.println("    </EVENT>");

        writer.println("</EVENTS>");

        writer.close();
    }
*/
    private double[] checkHeaders(Hashtable param) {
        // what's in the param has to be the same of headers
        ArrayList queryValues = new ArrayList();
        for (int i=0; i<_headersName.size(); i++) {
            if (param.containsKey(_headersName.get(i))) {
                queryValues.add(param.get(_headersName.get(i)));
            }
        }
        if (queryValues.size() != param.size()) return null;
        double[] queryArray = new double[queryValues.size()];
        try {
            for (int i=0; i<queryArray.length; i++) {
                queryArray[i] = Double.parseDouble(queryValues.get(i).toString());
            }
        } catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_refLayer.getViewer(), Thread.currentThread().getStackTrace(),
                                                "TemporaryEventDataSource.checkHeaders exception");
            return null;
        }
        return queryArray;
    }
}
