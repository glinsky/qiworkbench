/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.text.NumberFormat;
import java.util.Hashtable;
import java.util.Vector;

import javax.help.CSH;

import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.gw2d.gui.OrientedLabel;
import com.gwsys.gw2d.ruler.AdjustableStepTick;
import com.gwsys.gw2d.ruler.RangeRulerModel;
import com.gwsys.gw2d.ruler.RulerModel;
import com.gwsys.gw2d.ruler.RulerView;
import com.gwsys.gw2d.ruler.TickDefinition;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;
import com.gwsys.gw2d.view.BoxContainer;
import com.gwsys.seismic.core.DataChooser;
import com.gwsys.seismic.core.SeismicWorkflow;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.indexing.SegyHeaderFormat;
import com.gwsys.seismic.util.PlotConstants;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.util.logging.Logger;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is the main plot for the map viewer.
 *               It can display <code>{@link BhpMapLayer}</code> and
 *               annotate them correctly. For annotation, it takes
 *               care of the image transposing. <br>
 *               The annotation of a map display may include a vertical
 *               axis, a horizontal axis, a title, a color bar, and four
 *               labels, one on each side. <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpPlotMV extends AbstractPlot {
    private static final Logger logger = Logger.getLogger(BhpPlotMV.class.toString());
    private static final long serialVersionUID = 3977021738565776947L;

    //This may be redundant with DND code in BhpPlotXV but left in place until
    //determining whether all AbstractPlot subclasses should implement DND.
    private DTListener dtListener;
    private DropTarget dropTarget;
    public static final int acceptableActions = DnDConstants.ACTION_COPY;
    
    /**
     * Creates a new map plot.
     * @param hs horizontal scale of the plot
     * @param vs vertical scale of the plot
     */
    public BhpPlotMV(double hs, double vs) {
        super(hs, vs);
        CSH.setHelpIDString(this,"map_viewer");
        this.dtListener = new DTListener(this);
        this.dropTarget = new DropTarget(this, this.acceptableActions, this.dtListener, true);
    }

    /**
     * Retrieves the annotation readings for the given position.
     * @param virtTraceId the horizontal position, the trace ID of the interested trace.
     * @param virtualY the vertical position, in model space.
     * @return the annotation readings.
     *         The fields name and value pairs are put in a Hashtable.
     */
    public Hashtable buildSelectedAnnotationParameter(int virtTraceId, int virtualY) {
        BhpLayer theLayer = this.getBhpLayer(_axisAssociateId);
        Hashtable selectParameter = new Hashtable();
        SeismicWorkflow pipeline = theLayer.getPipeline();
        TraceHeader tmeta = pipeline.getDataLoader().getDataSelector().
        getTraceHeader(virtTraceId);
        if (tmeta == null) return selectParameter;

        BhpSeismicTableModel layerParameter = theLayer.getParameter();
        String vname = layerParameter.getVerticalName();
        double[] vsettings = layerParameter.getVerticalSettings();
        double verticalValue = vsettings[0] + virtualY * vsettings[2];
        if (verticalValue > vsettings[1]) verticalValue = vsettings[1];
        selectParameter.put(vname, new Double(verticalValue));

        String hname = layerParameter.getMapHorizontalKeyName();
        Hashtable headerValues = tmeta.getDataValues();
        Vector formats = pipeline.getDataLoader().getDataReader().getDataFormat().getTraceHeaderFormats();
        SegyHeaderFormat headerFormat = (SegyHeaderFormat) (formats.firstElement());
        Integer headerId;
        String headerName;
        Object headerValue;
        for (int j=0; j<layerParameter.getRowCount(); j++) {
            headerName = layerParameter.getRowHeaderName(j);
            if (headerName.equals(vname)) continue;
            headerId = BhpSegyFormat.getFieldForName(headerName, headerFormat);
            if (headerId != null) {
                headerValue = headerValues.get(headerId);
                selectParameter.put(headerName, headerValue);
            }
        }
        selectParameter.put("value", ""+theLayer.findSpecificDataValue(virtTraceId, verticalValue));
        return selectParameter;
    }

    /**
     * Updates the annotation of the plot.
     */
    public void updateAnnotation() {
        BhpLayer layer = this.getBhpLayer(_axisAssociateId);
        if (layer == null) {
            //ErrorDialog.showInternalErrorDialog(layer.getViewer(), Thread.currentThread().getStackTrace(),
            //                                    "BhpPlotMV: Cannot get BhpLayer instance");
            logger.warning("Cannot perform BhpPlotMV.updateAnnotation - getBhpLayer(_axisAssociateId) returned null.");
            return;
        }
        SeismicWorkflow pipeline = layer.getPipeline();
        if (pipeline == null) {
            //ErrorDialog.showInternalErrorDialog(layer.getViewer(), Thread.currentThread().getStackTrace(),
            //                                    "BhpPlotMV: Cannot get SeismicWorkflow instance");
            logger.warning("Cannot perform BhpPlotMV.updateAnnotation - layer.getPipeline() returned null SeismicWorkflow instance.");
            return;
        }

        updateMiscAnnotation();
        updateVAxes();
        updateHAxes();

        this.invalidate();
        this.validate();
    }

    /**
     * Sets the layer that based on which the annotation will be drawn.
     * @param id the identification number of the layer.
     * @param doUpdate a boolean flag indicates if the display shouble be
     *        updated right away. This can be used to avoid multiple repaint.
     */
    public void setAxisAssociateLayer(int id, boolean doUpdate) {
        if (id == _axisAssociateId) return;

        if (id < 0)  id = -1;

        if (id < 0) {
            _axisAssociateId = id;
            cleanAxesAndAnnotations();

            return;
        }

        _axisAssociateId = id;

        BhpLayer layer = this.getBhpLayer(id);
        if (layer == null) {
            return;
        }
        SeismicWorkflow pipeline = layer.getPipeline();
        if (pipeline == null) return;

        // title, labels, color bar
        _titleLoc = PlotConstants.TOP;
        //_colorbarLoc = PlotConstants.NONE;
        _title = "";
        NumberFormat titleFormat = NumberFormat.getInstance();
        titleFormat.setMaximumFractionDigits(4);
        if (layer.getDataType() == BhpViewer.BHP_DATA_TYPE_SEISMIC) {
            String value = layer.getParameter().getValueAt(0, 2).toString();
            _title = "Time/Depth: " + titleFormat.format(Double.parseDouble(value));
        }
        else {
            _title = layer.getPropertyName();
        }
        _labelL = layer.getParameter().getVerticalName();
        _labelT = layer.getParameter().getMapHorizontalKeyName();

        // draw annotation
        if (doUpdate) {
            updateHAxes();
            updateVAxes();
            updateMiscAnnotation();
        }

    }

    /**
     * Updates the horizontal axes.
     */
    public void updateHAxes() {
        BhpLayer layer = this.getBhpLayer(_axisAssociateId);
        if (layer == null) return;

        BhpMapLayer bhpMapLayer = (BhpMapLayer) layer;

        boolean imageTransposed = false;
        boolean imageReversed = false;
        if (bhpMapLayer.isTransposeImage()) {
            imageTransposed = true;
        }
        if (bhpMapLayer.isReversedHorizontalDirection()) {
            imageReversed = true;
        }

        clearXAnnotations(imageTransposed);

        if (_hAxesLoc != PlotConstants.NONE) {
            updateAxes(layer, _hAxesLoc, false, imageTransposed, imageReversed, _hMajorStep, _hMinorStep);
        }

        validateXAnnotations(imageTransposed);
    }

    public void updateVAxes() {
        BhpLayer layer = this.getBhpLayer(_axisAssociateId);
        if (layer == null) return;

        BhpMapLayer bhpMapLayer = (BhpMapLayer) layer;

        boolean imageTransposed = false;
        boolean imageReversed = false;
        if (bhpMapLayer.isTransposeImage()) {
            imageTransposed = true;
        }
        if (bhpMapLayer.getReversedSampleOrder()) {
            imageReversed = true;
        }

        clearYAnnotations(imageTransposed);

        if (_vAxesLoc != PlotConstants.NONE) {
            updateAxes(layer, _vAxesLoc, true, imageTransposed, imageReversed, _vMajorStep, _vMinorStep);
        }

        validateYAnnotations(imageTransposed);
    }


    /**
     * @param layer
     * @param displaySettings
     * @param isVertical
     * @param isTransposed
     * @param isReversed
     * @param majorStep
     * @param minorStep
     */
    private void updateAxes(BhpLayer layer, int displaySettings, boolean isVertical,
            boolean isTransposed, boolean isReversed, double majorStep, double minorStep) {
        Bound2D vl = getPlotBound(layer, isVertical, isTransposed);
        double[] settings = getAxisValueSettings(layer, isVertical);

        Transform2D trans1 = layer.getTransformation();
        double scalex = trans1.getScaleX() * layer.getLayerHorizontalScale();
        double scaley = trans1.getScaleY() * layer.getLayerVerticalScale();
        double realStartValue = vl.getMinX();
        if (settings[0] != -1)
            realStartValue = realStartValue + settings[0];

        double sampleRate;  //trace-level sample rate
        if (isVertical)     // trace-level sample rates only apply to the vertical axis
            sampleRate = layer.getPipeline().getDataLoader().getDataReader().getMetaData().getSampleRate();
        else
            sampleRate = 1.0;

        double traceStepSize;   //sample rate from data set (step value)
        if (settings[2] != -1)
            traceStepSize = settings[2];
        else
            traceStepSize = 1.0;

        double sampleRateFactor = traceStepSize / sampleRate;

        double majorStepValue = majorStep * traceStepSize;
        double minorStepValue = minorStep * traceStepSize;

        double translateX = trans1.getTranslateX();
        double translateY = trans1.getTranslateY();
        double tParam[] = getTransformationParams (isTransposed, (scalex), (scaley), translateX, translateY);

        Transform2D axisTrans = new Transform2D(tParam[0], tParam[1], tParam[2], tParam[3]);

        if (isReversed) {
            axisTrans = reverseAxis(axisTrans, vl, isVertical, isTransposed, traceStepSize);
        }

        //todo: the logic is really confused here, do refactoring later
        //*************************************************************
        double axisStart = 0;

        double axisWidth = vl.getWidth() / sampleRate;      // number of values (columns/rows) on axis
        Dimension size = getAxisSize(layer, isVertical, isTransposed);
        Point axisPos = getAxisPosition(layer.getViewPosition().x,layer.getViewPosition().y, isVertical, isTransposed);

        if (displayBefore(displaySettings, isVertical)) {
            int ori = getPosBefore(isVertical, isTransposed);
            double startValue, endValue;
        boolean horizon=!isVertical;
        if (isTransposed)
        horizon = isVertical;
            if (horizon){ //==RulerModel.HORIZONTAL
                if (isReversed){
                    startValue = realStartValue+vl.getMaxX();
                    endValue = realStartValue+vl.getMinX();
                }else{
                    startValue = realStartValue+vl.getMinX();
                    endValue = realStartValue+vl.getMaxX();
                }
            }else{
                if (isReversed){
                    startValue = realStartValue+vl.getMaxY();
                    endValue = realStartValue+vl.getMinY();
                }else{
                    startValue = realStartValue+vl.getMinY();
                    endValue = realStartValue+vl.getMaxY();
                }
            }
        //if the settings are available, we use
        if (settings[0]>0){
        startValue = settings[0];
                endValue = settings[1];
        if (isReversed){
                    startValue = settings[1];
                    endValue = settings[0];
                }
        }
            AdjustableStepTick tg1 = new AdjustableStepTick(startValue,endValue,50);
            attachAxis(getPlotBoxBefore (isVertical, isTransposed),
                    createAxisView(ori, axisWidth, axisStart,size, axisPos, tg1, axisTrans), false);
        }
        if (displayAfter(displaySettings, isVertical)) {
            int ori = getPosAfter(isVertical, isTransposed);
            AdjustableStepTick tg1 = null;
            if (isVertical)
                tg1 = new AdjustableStepTick(realStartValue+vl.getMinX(),realStartValue+vl.getMaxX(),50);
            else
                tg1 = new AdjustableStepTick(realStartValue+vl.getMinY(),realStartValue+vl.getMaxY(),50);
            attachAxis(getPlotBoxAfter (isVertical, isTransposed),
                    createAxisView(ori, axisWidth, axisStart,size, axisPos, tg1, axisTrans), true);
        }
    }

    private boolean displayBefore (int displaySettings, boolean isVertical) {
        if ( (displaySettings & getTestBefore (isVertical)) != 0) {
            return true;
        } else {
            return false;
        }
    }
    private boolean displayAfter (int displaySettings, boolean isVertical) {
        if ( (displaySettings & getTestAfter (isVertical)) != 0) {
            return true;
        } else {
            return false;
        }
    }

    private BoxContainer getPlotBoxAfter(boolean isVertical, boolean isTransposed) {
        boolean rotate = isVertical ^ isTransposed;
        BoxContainer ann;

        if (rotate) {
            ann=_annR;
        } else {
            ann=null;
        }
        return (ann);
    }

    private BoxContainer getPlotBoxBefore(boolean isVertical, boolean isTransposed) {
        BoxContainer ann;

        if ( isVertical ^ isTransposed ) {
            ann=_annL;
        } else {
            ann=_annT;
        }
        return (ann);
    }

    /**
     * @param isVertical
     */
    private int getTestBefore(boolean isVertical) {
        int test;

        if (isVertical) {
            test = PlotConstants.LEFT;
        } else {
            test = PlotConstants.TOP;
        }
        return (test);
    }
    private int getTestAfter(boolean isVertical) {
        int test;

        if (isVertical) {
            test = PlotConstants.RIGHT;
        } else {
            test = PlotConstants.BOTTOM;
        }
        return (test);
    }
    private int getPosBefore(boolean isVertical, boolean isTransposed) {
        boolean rotate = isVertical ^ isTransposed;
        int pos;

        if (rotate) {
            pos = RulerModel.VERTICAL;//WEST
        } else {
            pos = RulerModel.HORIZONTAL;//NORTH;
        }
        return (pos);
    }
    private int getPosAfter(boolean isVertical, boolean isTransposed) {
        boolean rotate = isVertical ^ isTransposed;
        int pos;

        if (rotate) {
            pos = RulerModel.VERTICAL;
        } else {
            pos = RulerModel.HORIZONTAL;
        }
        return (pos);
    }

    /**
     * @param imageTransposed
     * @param viewPos
     */
    private Point getAxisPosition(int X, int Y, boolean isVertical, boolean isTransposed) {
        boolean rotate = isVertical ^ isTransposed;
        Point position=new Point (X, Y);

        if (!rotate) {
            position.y = 0;
        } else {
            position.x = 0;
        }
        return (position);
    }

    /**
     * @param layer
     * @param imageTransposed
     * @param size
     */
    private Dimension getAxisSize(BhpLayer layer, boolean isVertical, boolean isTransposed) {
        boolean rotate = isVertical ^ isTransposed;
        Dimension size = new Dimension (80,40);

        if (!rotate) {
            size.width = (int)layer.getDeviceWidth();
        } else {
            size.height = (int)layer.getDeviceHeight();
        }
        return (size);
    }

    /**
     * @param axisTrans
     * @param vl
     * @param isTransposed
     * @param imageReversed
     */
    private Transform2D reverseAxis(Transform2D axisTrans, Bound2D vl, boolean isVertical, boolean isTransposed, double sampleRate) {
        boolean rotate = isVertical ^ isTransposed;
        double[] matrix = new double[9];
        axisTrans.getMatrix(matrix);
        Transform2D newTrans = new Transform2D(matrix);

        double displacement = -vl.getWidth();

        if (isVertical) {
            displacement /= sampleRate;
        }

        if (!rotate) {
            newTrans.scale(-1, 1);
            newTrans.translate(displacement, 0);
        }
        else {
            newTrans.scale(1, -1);
            newTrans.translate(0,displacement);
        }

        return (newTrans);
    }

    /**
     * @param layer
     * @return
     */
    private double[] getAxisValueSettings(BhpLayer layer, boolean isVertical) {
        double[] settings;
        if (isVertical)
            settings = layer.getParameter().getVerticalSettings();
        else {
            _hAxesSyncName = layer.getParameter().getMapHorizontalKeyName();
            settings = layer.getParameter().getHorizontalMapSettings();
        }
        return settings;
    }

    /**
     * @param isTransposed
     */
    private void clearXAnnotations(boolean isTransposed) {
        if (!isTransposed) {
            clearAnnotationsHorizontal();
        }
        else {
            clearAnnotationsVertical();
        }
    }

    /**
     * @param isTransposed
     */
    private void validateXAnnotations(boolean isTransposed) {
        if (!isTransposed) {
            validateAnnotationsHorizontal();
        }
        else {
            validateAnnotationsVertical();
        }
    }

    private double[] getTransformationParams (boolean isTransposed, double scaleX, double scaleY, double transX, double transY) {
        double[] tp = new double[4];

        tp[0] = scaleX;
        tp[1] = scaleY;
        tp[2] = transX;
        tp[3] = transY;

        if (isTransposed) {
            tp = transpose(tp);
        }

        return tp;
    }

    private double[] transpose(double[] tp) {
        double[] txp=new double[4];

        txp[0] = tp[1];
        txp[1] = tp[0];
        txp[2] = tp[3];
        txp[3] = tp[2];

        return txp;
    }

    /**
     * @param layer
     * @return
     */
    private Bound2D getPlotBound(BhpLayer layer, boolean isVertical, boolean isTransposed) {
        DataChooser pipeline = layer.getPipeline().getDataLoader().getDataSelector();
        Bound2D vl = new Bound2D(pipeline.getVirtualModelLimits());
        vl.x += (int)(pipeline.getPrimaryKeyStart()) -1;
        vl.y += (int)(pipeline.getStartSampleValue());
        if (isTransposed){
            vl = new Bound2D(vl.getMinY(), vl.getMinX(), vl.getMaxY(), vl.getMaxX());
        }
        return vl;
    }

    /**
     * @param isTransposed
     */
    private void validateYAnnotations(boolean isTransposed) {
        if (isTransposed) {
            validateAnnotationsHorizontal();
        }
        else {
            validateAnnotationsVertical();
        }
    }

    /**
     * @param isTransposed
     */
    private void clearYAnnotations(boolean isTransposed) {
        if (isTransposed) {
            clearAnnotationsHorizontal();
        }
        else {
            clearAnnotationsVertical();
        }
    }

    /**
     *
     */
    private void validateAnnotationsVertical() {
        _annL.validate();
        _annR.validate();
    }

    /**
     *
     */
    private void validateAnnotationsHorizontal() {
        _annT.validate();
    }

    /**
     *
     */
    private void clearAnnotationsVertical() {
        clearAnnotation(_annL, true);
        clearAnnotation(_annR, true);
    }

    /**
     *
     */
    private void clearAnnotationsHorizontal() {
        clearAnnotation(_annT, true);
    }

    /**
     * @param ann
     * @param axisL
     */
    private void attachAxis(BoxContainer ann, RulerView axisL, boolean firstInAxis) {
        if (firstInAxis)
            ann.add((axisL), 0);
        else
            ann.add((axisL), ann.getComponentCount());
    }

    /**
     * @param position Orientation of Ruler.
     * @param axisWidth
     * @param size
     * @param viewPos
     * @param tg1
     * @param ar
     * @param transform
     * @return
     */
    private RulerView createAxisView(int position, double axisWidth, double axisStart,
            Dimension size, Point viewPos, TickDefinition tick, Transform2D transform) {
        NumberFormat nfMajor = NumberFormat.getInstance();
        NumberFormat nfMinor = NumberFormat.getInstance();
        //cgAxisRenderer ar = new cgMajorMinorAxisRenderer(10, 15, 5, 5, 12, 3, 0, true, _attribute, _attribute, _attribute, nfMajor, nfMinor);
        RulerModel axisModel=new RangeRulerModel(position, tick);

        RulerView axisL = new RulerView(axisModel);
        axisL.setBackground(Color.white);
        //axisL.setTransformation(transform);
        axisL.setPreferredSize(size);
        //axisL.setViewPosition(viewPos);
        return axisL;
    }

    /**
     * Updates the labels of the plot.
     * If the image is not transposed, the implementation in
     * its super class will be called to update labels.
     * Otherwise, this method handles it.
     */
    protected void updateLabels() {
        BhpLayer layer = this.getBhpLayer(_axisAssociateId);
        if (layer == null) return;
        boolean imageTransposed = ((BhpMapLayer)layer).isTransposeImage();
        if (!imageTransposed) {
            super.updateLabels();
            return;
        }

        if (_labelL!=null && _labelL.length() > 0) {
            OrientedLabel label = new OrientedLabel(_labelL);
            label.setBackground(Color.white);
            label.setLabelAttribute(_attribute);
            _annT.add(label, 0);
        }
        if (_labelT != null && _labelT.length() > 0) {
            OrientedLabel label = new OrientedLabel(_labelT);
            label.setBackground(Color.white);
            label.setLabelAttribute(_attribute);
            label.setPreferredSize(new Dimension(_labelT.length()*10, 15));
            _annL.add(label, 0);
        }
        if (_labelB != null && _labelB.length() > 0) {
            OrientedLabel label = new OrientedLabel(_labelB);
            label.setBackground(Color.white);
            label.setLabelAttribute(_attribute);
            _annR.add(label, _annR.getComponentCount());
        }
    }
}