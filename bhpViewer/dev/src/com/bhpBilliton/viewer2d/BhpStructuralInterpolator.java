/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;


import com.bhpBilliton.viewer2d.data.BhpModelReaderInterface;
import com.gwsys.seismic.core.TraceProcessor;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;

class BhpStructuralInterpolator extends BhpModelTraceInterpolator {
	  private boolean _structuralInterpFlag;
	    /**
	     * Constructs a trace interpolator with interpolation
	     * type set to cgTraceInterpolator.CG_IT_LINEAR.
	     * @param reader the model reader of the pipeline.
	     */
	    public BhpStructuralInterpolator(BhpModelReaderInterface reader) {
	      super(reader);
	      //  super(cgTraceInterpolator.CG_IT_LINEAR);
	    }

	    /**
	     * Method not supported by this class. <br>
	     * Please refer to the JSeismic documentation for more detail.
	     */
	/*    public void setDecimationThreshold(double v) {
	        System.out.println("BhpModelTraceInterpolator does not implement setDecimationThreshold");
	    }
	    */
	    /**
	     * Method not supported by this class. <br>
	     * Please refer to the JSeismic documentation for more detail.
	     */
	    /*
	    public double getDecimationThreshold() {
	        System.out.println("BhpModelTraceInterpolator does not implement getDecimationThreshold");
	        return 0;
	    }*/
	    /**
	     * Interpolates the trace according to its depth value.
	     * @param traceId the trace ID of the trace that will be interpolated.
	     * @param sampleRange the range of required samples from this process.
	     * @param traceDataOut the destination trace data for the trace process.
	     * @return a flag indicates if the process is successful.
	     */
	    public boolean process(int traceId, NumberRange sampleRange, TraceData traceDataOut) {
	      if(!getStructuralInterpFlag())
	      {
	        return super.process(traceId, sampleRange, traceDataOut);
	      }
	      return (((TraceProcessor)getParentProcessor()).process(traceId, null, traceDataOut));
	    }
	    /**
	     * Gets the flag of structural interpolation.
	     */
	    public boolean getStructuralInterpFlag() { return _structuralInterpFlag; }
	    /**
	     * Sets the flag of structural interpolation.
	     */
	    public void setStructuralInterpFlag(boolean f) { _structuralInterpFlag = f; }

	}
