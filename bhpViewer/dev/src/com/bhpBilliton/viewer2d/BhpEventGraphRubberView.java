/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import com.bhpBilliton.viewer2d.ui.BhpEventGraphPlotDialog;
import com.gwsys.gw2d.model.*;
import com.gwsys.gw2d.shape.*;
import com.gwsys.gw2d.view.*;
import com.gwsys.gw2d.util.*;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This view is placed on top of the BhpEventGraphPlot
 *               for mouse tracking.<br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpEventGraphRubberView extends SimpleViewContainer
        implements MouseListener, MouseMotionListener {

    private AbstractPlot _mainPlot;
    private BhpEventGraphPlot _graphPlot;
    private SymbolShape _symbol;

    private BhpEventGraphPlotDialog _dialog;

    /**
     * Creates the view that is on top of the stacked view.
     * @param w the window that this layer belongs to.
     */
    public BhpEventGraphRubberView(AbstractPlot p, BhpEventGraphPlot gp) {
        _mainPlot = p;
        _graphPlot = gp;
        CommonDataModel model = new CommonDataModel();
        CommonShapeLayer layer = new CommonShapeLayer();
        model.addLayer( layer );
        setModel( model );
        setTransformation( new Transform2D() );

        _symbol = new SymbolShape(0, 0, 10.0, 10.0, LocationBasedShape.LOCATION_CENTER,
                               new PlusSymbolPainter(), true, true);
        _symbol.setVisible(false);
        _symbol.setAttribute(null);
        layer.addShape(_symbol);
    }

    public void updateEventGraphSymbolSize(double w, double h) {
        _symbol.setSize(1, h);
    }

    /**
     * For temporarily hiding the cursor.
     * The method is called when the mouse exit the window.
     */
    public void hideEventGraphSymbol() {
        _symbol.setVisible(false);
        //repaint();
    }

    /**
     * Sets the anchor position of the mouse tracking symbol.
     * @param x the horizontal position.
     * @param y the vertical position.
     */
    public void setEventGraphSymbolAnchor(int x, int y) {
        if (_symbol.getAttribute() == null) {
            Window win = SwingUtilities.windowForComponent(_mainPlot);
            RenderingAttribute attr2 = null;
            double cursorW = 0;
            double cursorH = 0;
            BhpViewerBase viewer = _mainPlot.getBhpLayer(_mainPlot.getAxisAssociateLayer())._viewer;
            _symbol.setAttribute(viewer.getCursorAttribute());
            _symbol.setSize(1,viewer.getCursorHeight());
            /*if (win instanceof BhpViewerBase) {
                _symbol.setAttribute(((BhpViewerBase)win).getCursorAttribute());
                _symbol.setSize(1, ((BhpViewerBase)win).getCursorHeight());
            }*/
        }
        _symbol.setVisible(true);
        int oldX = (int) _symbol.getLocationX();
        int oldY = (int) _symbol.getLocationY();
        if (x != oldX) {
            _symbol.setLocation(x, oldY);
            this.repaint();
        }
        //Point p = getViewPosition();
        //int h = (int) _symbol.getHeight()/2 + 2*(int) _symbol.getAttribute().getLineWidth();
        //int w = (int) _symbol.getWidth()/2 + 2*(int) _symbol.getAttribute().getLineWidth();
        //repaint((oldX-p.x)-w, (oldY-p.y)-h, (x-oldX)+2*w, (y-oldY)+2*h);
    }

    /**
     * Method to handle the mouse entering event. No action is taken now.
     */
    public void mouseEntered( MouseEvent me ) {}
    /**
     * Method to handle the mouse exiting event. No action is taken now.
     */
    public void mouseExited( MouseEvent me ) {
        me.setSource(_mainPlot.getMainView());
        _mainPlot.getMouseCenter().mouseExited(me);
    }
    /**
     * Method to handle the mouse clicking event. <br>
     * Double click pops up the dialog for event graph plot settings.
     */
    public void mouseClicked( MouseEvent me ) {
        if (SwingUtilities.isLeftMouseButton(me) && me.getClickCount() >= 2) {
            showEventGraphPlotDialog(me);
        }
    }
    /**
     * Method to handle the mouse moving event. No action is taken now.
     */
    public void mouseMoved( MouseEvent me ) {
        me.setSource(_mainPlot.getMainView());
        me.translatePoint(0, -me.getY());
        _mainPlot.getMouseCenter().mouseMoved(me);
    }

    /**
     * Method to handle the mouse pressing event. <br>
     */
    public void mousePressed( MouseEvent me ) {
    }
    /**
     * Method to handle the mouse dragging event. <br>
     */
    public void mouseDragged( MouseEvent me ) {

    }
    /**
     * Method to handle the mouse releasing event. <br>
     */
    public void mouseReleased( MouseEvent me ) {
    }

    private void showEventGraphPlotDialog(MouseEvent e) {
        if (_dialog == null) {
          //BhpViewerBase v = (BhpViewerBase)(SwingUtilities.windowForComponent(this));
          BhpViewerBase v = _mainPlot.getBhpLayer(_mainPlot.getAxisAssociateLayer())._viewer;
          _dialog = new BhpEventGraphPlotDialog(v);
          _dialog.pack();
          _dialog.setSize(350, 400);
        }

        if (_graphPlot == null) return;
        if (_dialog.isVisible()) _dialog.setVisible(false);
        _dialog.initWithEventGraphPlot(_graphPlot);
        _dialog.setVisible(true);
    }
}
