/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.Dimension;
import javax.swing.JComponent;

import com.gwsys.gw2d.view.MultipleViewContainer;
import com.gwsys.gw2d.view.SimpleViewContainer;
import com.gwsys.gw2d.view.VisuableComponent;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class is a customized MultipleViewContainer.
 *               It provides extra methods for easy plot view order handling.
 *               <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class OrderedStackPlotView extends MultipleViewContainer {
    private boolean _showPointer = true;
    private MultipleViewContainer _innerView; // keep all the BhpLayer
    private MultipleViewContainer _overView;  // keep all the other layers, grid, rubber.
    private RubberView _rubberView;

    /**
     * Constructs a new instance.
     */
    public OrderedStackPlotView() {
        super();
        _innerView = new MultipleViewContainer();
        _innerView.setNotificationFlag(true);
	  _innerView.setOpaque(false);
	  _innerView.enableCache(true);
        _overView = new MultipleViewContainer();
	  _overView.setOpaque(false);
	  setOpaque(true);
	  setBackground(Color.white);
        add(_innerView);
        add(_overView);

    }

    public RubberView getRubberView() { return _rubberView; }

    /**
     * Adds the <code>{@link RubberView}</code> to the
     * <code>{@link OrderedStackPlotView}</code>. <br>
     * @rv the RubberView.
     */
    public void addRubberView(RubberView rv) {
        _rubberView = rv;
        _overView.add(_rubberView, _overView.getNumberOfViews());
    }

    public void addGridView(SimpleViewContainer grid) {
        _overView.add(grid, 0);
    }

    public void setCursor(Cursor cursor) {
        if (cursor.getType() == Cursor.DEFAULT_CURSOR && !_showPointer) {
            BufferedImage bim = new BufferedImage(1, 1, BufferedImage.TYPE_4BYTE_ABGR);
            cursor = getToolkit().createCustomCursor(bim, new Point(0, 0), "HiddenM");
        }
        super.setCursor(cursor);
    }

    public void setShowPointer(boolean show) {
        _showPointer = show;
        if (!_showPointer) {
            setCursor(Cursor.getDefaultCursor());
        }
    }

    // add it to the top, but underneath all non-BhpLayer plot views.
    public void addBhpLayer(BhpLayer pv) {
        _innerView.add(pv, _innerView.getNumberOfViews());

    }

    /**
     * Moves the specific layer to the very bottom.
     * @param pv the layer that needs to be moved.
     */
    public void movePlotViewBack(BhpLayer pv) {
        JComponent bottomcomp = this.getView(0);
        _innerView.moveViewToBottom(pv);
        super.setTransformationWithoutPropogation(super.getTransformation());
    }

    /**
     * Moves the specific layer to the top, right below the
     * <code>{@link RubberView}</code>.
     * @param pv the layer that needs to be moved.
     */
    public void movePlotViewUp(BhpLayer pv) {
/*
        super.moveViewToTop(pv);
        int size = getNumberOfViews();
        if (size >= 3) {
            // grid view probably
            if (!(getView(size-3) instanceof BhpLayer)) {
                super.moveViewToTop(getView(size-3));
            }
        }
        // for rubber view
        super.moveViewToTop(super.getView(super.getNumberOfViews()-2));
*/
        _innerView.moveViewToTop(pv);
        super.setTransformationWithoutPropogation(super.getTransformation());
/*
        // order = order + 1;
        if (_views.contains(pv) == false) {
            System.out.println("OrderedStackPlotView Error : Cannot find the plotview.");
            return;
        }
        int index = _views.indexOf(pv);
        // at the top already, rubberview at the very top
        if (index >= _views.size()-2) return;
        setPlotViewOrder(pv, index+1);
*/
    }

    /**
     * Removes a layer. <br>
     * Current implementation does nothing other than call the
     * corresponding method in its super class.
     * @param pv the layer that will be removed.
     */
    public void removeBhpLayer(BhpLayer pv) {
        _innerView.remove(pv);
    }

    public int getNumberOfBhpLayers() { return _innerView.getNumberOfViews(); }

    public BhpLayer[] getAllBhpLayers() {
        int size = getNumberOfBhpLayers();
        BhpLayer[] result = new BhpLayer[size];
        for (int i=0; i<size; i++) {
            result[i] = (BhpLayer) (_innerView.getView(i));
        }
        return result;
    }

    /**
     * Moves the layer to the specific index position.
     * @param pv the layer that may be moved.
     * @param index the requested index position.
     */
    public void setBhpLayerOrder(BhpLayer pv, int index) {
        if (index < 0)
            _innerView.moveViewToBottom(pv);
        else if (index >= _innerView.getNumberOfViews())
            _innerView.moveViewToTop(pv);
        else {
            _innerView.remove(pv);
            _innerView.add(pv, index);
        }
        super.setTransformationWithoutPropogation(super.getTransformation());
    }


    /**
     * Scale the stacked view. <br>
     * This method scales the sepcific layer first and
     * recalculates the limits of itself.
     * @param layer the layer needs to be scaled.
     * @param x horizontal scale factor.
     * @param y vertical scale factor.
     * @param xp horizontal view position.
     * @param yp vertical view position.
     */
    public void scaleToWithLayer(BhpLayer layer, double x, double y, double xp, double yp) {
        int innerSize = _innerView.getNumberOfViews();
        for( int i=0; i < innerSize; i++ ) {
            if( _innerView.getView(i) instanceof BhpLayer ) {
                ((BhpLayer) _innerView.getView(i)).scaleToWithLayer(x, y, xp, yp);
            }
        }
        // calculate new limits & set view position
		//java.awt.Point vp = layer.getViewPosition();
        //setViewPosition(new Point(vp));

        _limits.setSize( 0, 0 );
        VisuableComponent plot = null;

        for( int i=0; i < innerSize; i++ ) {
            Dimension limit = null;

            if( _innerView.getView(i) instanceof VisuableComponent ) {
                plot = (VisuableComponent) _innerView.getView(i);
                limit = plot.getActualSize();


            }
            else {
                limit = ((JComponent)_innerView.getView(i)).getSize();
            }

            if( limit.width  > _limits.width  )
				_limits.width  = limit.width;
            if( limit.height > _limits.height )
				_limits.height = limit.height;
        }
        //we get new size to notify scrollpane
        this.setPreferredSize(_limits);
	layer._window.updatePlot();  //force JscrollPane to be resized
	_innerView.setDirtyMark(true);
        this.repaint();
    }


	public void setPreferredSize(Dimension size){
		if (_innerView == null ) return; //not ready
		_innerView.setPreferredSize(size);
		_overView.setPreferredSize(size);
		_limits = size;
		this.revalidate();
	}

	/**
	 * Update this actual size of component and revalidate
	 * @param size this actual size
	 */
	public void setActualSize(Dimension size){
		if (size.width != _limits.width || size.height != _limits.height){
			setPreferredSize(size);
		}
	}
	/**
	 * All children have same position
	 * @param x
	 * @param y
	 */
	public void setViewPosition(int x, int y){
		Point p = new Point(x,y);
		_rubberView.setViewPosition(p);
		this.setViewPosition(p);
	}
}
