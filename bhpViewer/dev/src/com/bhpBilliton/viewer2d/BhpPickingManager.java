/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import javax.swing.*;

import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpBilliton.viewer2d.util.IconResource;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class manages picking related settings. <br>
 *               The picking setting is "global", affecting all the
 *               windows of the same viewer. There are two modes,
 *               point-by-point picking defines points of a horizon
 *               for mouse clicking or along the mouse dragging path.
 *               While point-to-point mode also defines points of a horizon
 *               for mouse clicking, it defines points between two mouse
 *               clicking using a linear interpolation. The default mode
 *               is point-by-point. <br>
 *               There are also different snapping mode. When snapping is
 *               used, the user selected horizon value is adjusted according
 *               to the underlying seismic display and the snap mode. The
 *               default snap setting is no snap. <br><br>
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpPickingManager {
    public static final int PICK_POINT_BY_POINT = 1;
    public static final int PICK_POINT_TO_POINT = 2;
    public static final int SNAP_NO = -1;
    public static final int SNAP_MAX = 0;
    public static final int SNAP_MIN = 1;
    public static final int SNAP_ZERO = 2;

    public static final int SNAP_LIMIT_DEFAULT = 20; // default snap limit in ms
    
    private static final String[] PICK_MODE_TEXT = {"point by point", "point to point"};
    private static final String[] SNAP_MODE_TEXT = {"no snap", "snap to the nearest maximum",
                                                    "snap to the nearest minimum",
                                                    "snap to the nearest zero crosss"};
    private static final String PICK_MODE_TOOLTIP = "Click to change the picking mode. Current value is: ";
    private static final String SNAP_MODE_TOOLTIP = "Click to change the snap mode. Current value is: ";
    private static final int PICK_MODE_NUMBER = 2;
    private static final int SNAP_MODE_NUMBER = 4;

    private int _pickMode;
    private int _snapMode;
    private int _snapLimit = SNAP_LIMIT_DEFAULT;
    private boolean _reset;

    private JButton _pickModeGUI;
    private JButton _snapModeGUI;
    private ImageIcon[] _pickModeIcons;
    private ImageIcon[] _snapModeIcons;

    /**
     * Constructs a new instance. <br>
     * The default pick mode is PICK_POINT_BY_POINT,
     * the default snap mode is SNAP_NO.
     */
    public BhpPickingManager(BhpViewerBase viewer) {
        _pickMode = PICK_POINT_BY_POINT;
        _snapMode = SNAP_NO;
        _reset = false;

        _pickModeIcons = new ImageIcon[PICK_MODE_NUMBER];
        _snapModeIcons = new ImageIcon[SNAP_MODE_NUMBER];
        try {
            _pickModeIcons[0] = IconResource.getInstance().getImageIcon(IconResource.PICKMODEPBP_ICON);
            _pickModeIcons[1] = IconResource.getInstance().getImageIcon(IconResource.PICKMODEPTP_ICON);
            _snapModeIcons[0] = IconResource.getInstance().getImageIcon(IconResource.PICKSNAPNO_ICON);
            _snapModeIcons[1] = IconResource.getInstance().getImageIcon(IconResource.PICKSNAPMAX_ICON);
            _snapModeIcons[2] = IconResource.getInstance().getImageIcon(IconResource.PICKSNAPMIN_ICON);
            _snapModeIcons[3] = IconResource.getInstance().getImageIcon(IconResource.PICKSNAPZERO_ICON);
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "BhpPickingManager failed to load icons");
        }
    }

    public boolean checkReset() {
        boolean re = _reset;
        _reset = false;
        return re;
    }

    public int getPickMode() { return _pickMode; }
    public int getSnapMode() { return _snapMode; }
    
    public int getSnapLimit() { return _snapLimit; }
    public void setSnapLimit(int s) { _snapLimit = s; }
    
    public void setPickModeGUI(JButton btn) { _pickModeGUI = btn; }
    public void setSnapModeGUI(JButton btn) { _snapModeGUI = btn; }

    public void changePickMode() {
        int newMode = ((_pickMode-1+1) % PICK_MODE_NUMBER) + 1;
        setPickMode(newMode);
    }

    public void changeSnapMode() {
        int newMode = ((_snapMode+1+1) % SNAP_MODE_NUMBER) - 1;
        setSnapMode(newMode);
    }

    public void setPickMode(int m) {
        _pickMode = m;
        _reset = true;
        if (_pickModeGUI != null) {
            _pickModeGUI.setIcon(_pickModeIcons[_pickMode-1]);
            String tip = PICK_MODE_TOOLTIP + PICK_MODE_TEXT[_pickMode-1];
            _pickModeGUI.setToolTipText(tip);
        }
    }
    public void setSnapMode(int m) {
        _snapMode = m;
        if (_snapModeGUI != null) {
            _snapModeGUI.setIcon(_snapModeIcons[_snapMode+1]);
            String tip = SNAP_MODE_TOOLTIP + SNAP_MODE_TEXT[_snapMode+1];
            _snapModeGUI.setToolTipText(tip);
        }
    }
}
