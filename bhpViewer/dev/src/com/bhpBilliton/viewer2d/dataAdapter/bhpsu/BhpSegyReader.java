/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter.bhpsu;

import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpSegyFormat;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.seismic.reader.SeismicFormat;
import com.gwsys.seismic.indexing.SegyHeaderFormat;
import com.gwsys.seismic.io.SeismicMetaData;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.util.NumberRange;
import java.io.BufferedReader;
import java.io.File;
import java.io.PrintWriter;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  In the case that bhpio data is used,
 *               this class is the ultimate data provider for the pipeline
 *               and display. Different readers are used for different types
 *               of layer. <br>
 *               This reader starts a thread of
 *               <code>{@link BhpSeismicDataLoader}</code>, either
 *               <code>{@link LocalBhpSeismicDataLoader}</code> or
 *               <code>{@link RemoteBhpSeismicDataLoader}</code>,
 *               at the time of construction. The loader thread actually create
 *               a temporary data file, read the data from it and put the data in
 *               <code>{@link BhpSeismicDataCache}</code>.
 *               The reader in turn retrieves the data from the cache.
 *               This reader uses <code>{@link BhpSegyFormat}</code>. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpSegyReader extends com.bhpBilliton.viewer2d.data.BhpDataReader {
    public static final int TRACE_HEADER_SIZE = 240;

    protected SeismicFormat _segyFormat;
    protected Bound2D _modelLimits;
    protected SeismicMetaData _metaData;

    protected BhpSeismicDataCache _cache;
    protected BhpSeismicDataLoader _thread;

    private String _urlParameter;
    private String _queryParameter;
    private boolean _isRemote;

    private BhpLayer layer;

    /**
     * Constructs a new instance.
     * @param layer the layer where the data will be displayed.
     * @param isRemote flag indicates if the data is fetched remotely.
     * @param servletReadURL part 1 of the bhpio query string. <br>
     *        It is either the remote servlet url or
     *        local bhpio command with path.
     * @param binIOPath the query string to run bhpio.
     * @param outputFile name of the temporary data file.
     * @param chunksz reading chunk size in traces.
     *        This controls how many trace the loader will read
     *        at most at one time.
     */
    public BhpSegyReader(BhpLayer layer,
                         boolean isRemote, String servletReadURL, String binIOPath,
                         String outputFile, int chunksz) {
        this.layer = layer;
        _segyFormat = new BhpSegyFormat();
        _cache = new BhpSeismicDataCache(layer);
        _reversed = layer.getReversedPolarity();
        _reversedDirection = layer.getReversedSampleOrder();
        _urlParameter = servletReadURL;
        _queryParameter = binIOPath;

        _isRemote = isRemote;

        if (isRemote) {
            _thread = new RemoteBhpSeismicDataLoader(
                        _cache, servletReadURL, binIOPath, outputFile, chunksz);

        } else {
            _thread = new LocalBhpSeismicDataLoader(_cache, servletReadURL, binIOPath, outputFile, chunksz);
        }
        
        _metaData = null;
        _modelLimits = null;

        _thread.setPriority(Thread.MIN_PRIORITY);
        _thread.start();
    }

    /**
     * Finds out if the reader runs another thread to load the data.
     */
    public boolean isThreaded() { return true; }

    public void stopLoading() {
        BhpSeismicDataLoader thread = this.getLoadingThread();
        if (thread!=null && thread.isAlive()) {
            thread.setMustDie();
        }
    }

    /**
     * Gets the parameter string used when running bhpio
     * querying the data for the reader. <br>
     * The returned string can be used as the parameter of some other
     * process. For example, bhpControl.SelectedScriptAction uses the
     * string as the parameter of some external shell program. <br>
     */
    public String getParameterString() {
        return _queryParameter;
    }

    //public boolean getReversedDirection() { return _reversedDirection; }

    /**
     * Retrieves the thread that is used to load the data.
     */
    public BhpSeismicDataLoader getLoadingThread() {
      return _thread;
    }

    /**
     * Gets the trace meta data of a specific trace.
     */
    public TraceHeader getTraceMetaData(int traceId) {
        if (traceId < 0) return null;
        if (_metaData != null) {
            if (traceId >= _metaData.getNumberOfTraces()) return null;
        }
        BhpTraceObject traceObject = (BhpTraceObject) _cache.get(new Integer(traceId));
        if (traceObject != null) {
            SegyHeaderFormat headerFormat = (SegyHeaderFormat)
                                          (_segyFormat.getTraceHeaderFormats().elementAt(0));
            return traceObject.getTraceMetaData(traceId, headerFormat.getHeaderFields());
        }
        return null;
    }

    /**
     * Performs this trace process, generating a cgTraceData
     * corresponds to the given input trace id.
     * @param traceId the desired trace id to process.
     * @param sampleRange the range of smaples from this process.
     * @param traceDataOut the target trace data for the trace process.
     * @return a boolean flag indicates if the process is successfully completed.
     */
    public boolean process(int traceId, NumberRange sampleRange, TraceData traceDataOut) {
        if (traceId < 0) return false;
        if (_metaData != null) {
            if (traceId >= _metaData.getNumberOfTraces()) return false;
        }
        
        BhpTraceObject traceObject = (BhpTraceObject) _cache.get(new Integer(traceId));
        if (traceObject == null) return false;
        if (traceObject.getTraceData() == null) return false;

        int samplesPerTrace = this.getMetaData().getSamplesPerTrace();
        int numSamples;
        int startSample;
        if (sampleRange == null) {
            numSamples = samplesPerTrace;
            startSample = 0;
        }
        else {
            numSamples = sampleRange.getRange();
            //startSample = sampleRange.getMin();
            startSample = sampleRange.getMin().intValue() -
            (int)(_metaData.getSampleStart()/_metaData.getSampleRate());
        }

        if (_reversedDirection) {
            startSample = samplesPerTrace - startSample - numSamples;
        }

        if (startSample < 0) startSample = 0;
        if (startSample > samplesPerTrace-1) startSample = 0;
        if (startSample+numSamples > samplesPerTrace)
            numSamples = samplesPerTrace - startSample;

        float[] dataArray = new float[numSamples];
        for (int i=0; i<numSamples; i++)
            dataArray[i] = traceObject.getTraceData()[startSample+i];

        if (_reversed) {
            for (int i=0; i<numSamples; i++)
                dataArray[i] = -dataArray[i];
        }

        if (_reversedDirection) {
            float[] tmpDataArray = new float[numSamples];
            for (int i=0; i<numSamples; i++) {
                tmpDataArray[i] = dataArray[numSamples-1-i];
            }
            traceDataOut.setSamples(tmpDataArray);
        }
        else {
            traceDataOut.setSamples(dataArray);
        }

        traceDataOut.setNumAppliedSamples(numSamples);
        traceDataOut.setUniqueKey(new Integer(traceId));

        return true;
    }

    /**
     * Retrieves the data format used by this reader.
     */
    public SeismicFormat getDataFormat() {
        return _segyFormat;
    }

    /**
     * Retrieves the seismic meta data.
     */
    public SeismicMetaData getMetaData() {
        if (_metaData == null) initMetaDataAndModelLimits();
        return _metaData;
    }

    /**
     * Gets the model limtis of the data.
     */
    public Bound2D getModelLimits() {
        if (_modelLimits == null) initMetaDataAndModelLimits();
        return _modelLimits;
    }

    /**
     * Deletes the temporary file generated for data output. <br>
     */
    public void cleanupDataReader() {
        String tmpFileName = _cache.getTempFileName();
        PrintWriter out = null;
        BufferedReader reader = null;
        try {
            File file = new File(tmpFileName);
            if (!file.isDirectory() && file.exists()) file.delete();
        } catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(layer.getViewer(), Thread.currentThread().getStackTrace(), "BhpSegyReader.deleteTempFile Exception");
        }
    }

    protected void initMetaDataAndModelLimits() {
        _metaData = new SeismicMetaData();
        _cache.getMetaInitiated();
        if (_cache.getSampleRate() == 0) _metaData.setSampleRate(1.0);
        else _metaData.setSampleRate(_cache.getSampleRate());
        _metaData.setSampleStart(0.0);
        _metaData.setStartValue(_cache.getSampleStart());
        _metaData.setSampleUnits(_cache.getSampleUnit());

        _metaData.setSamplesPerTrace(_cache.getSamplesPerTrace());
        _metaData.setNumberOfTraces(_cache.getNumberOfTraces());
        _metaData.setDataStatistics(_cache.getMin(), _cache.getMax(),
                                    _cache.getAvg(), _cache.getRms());
        _modelLimits = new Bound2D(0, 0,
                                  _metaData.getNumberOfTraces()-1,
                                  (_metaData.getSamplesPerTrace()-1)*
                                  _metaData.getSampleRate());
    }

}