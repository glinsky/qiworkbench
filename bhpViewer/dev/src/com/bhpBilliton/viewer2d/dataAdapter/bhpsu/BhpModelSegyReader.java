/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter.bhpsu;


import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.data.BhpModelReaderInterface;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.io.SeismicMetaData;
import com.gwsys.seismic.reader.*;
import com.gwsys.gw2d.util.*;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is the reader for the model data.
 *               Since the model data has two parts, the value and the depth,
 *               in each trace, it has to be handled differently.
 *               This reader has a method getDepthArray to return the depth
 *               value. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */
public class BhpModelSegyReader extends BhpSegyReader implements BhpModelReaderInterface {
    private float _minDepth;
    private float _maxDepth;

    private BhpLayer layer;

    /**
     * Constructs a new instance.
     */
    public BhpModelSegyReader(BhpLayer layer, boolean isRemote,
                                String servletReadURL, String binIOPath,
                                String outputFile, int chunksz) {
        super(layer, isRemote, servletReadURL, binIOPath, outputFile, chunksz);
        this.layer = layer;
    }

    /**
     * Gets the minimum depth read.
     */
    public float getMinDepth() { return _minDepth; }
    /**
     * Gets the maximum depth read.
     */
    public float getMaxDepth() { return _maxDepth; }

    /**
     * Retrieves the depth array for a specific trace.
     * @param traceId the trace ID of the trace for which the depth array is requested.
     * @return a float array of depth values of the specific trace.
     */
    public float[] getDepthArray(int traceId) {
        if (traceId < 0) return null;
        if (_metaData != null) {
            if (traceId >= _metaData.getNumberOfTraces()) return null;
        }
        Integer key = new Integer(traceId);
        BhpTraceObject bhpTraceObject = (BhpTraceObject) _cache.get(key);
        if (bhpTraceObject == null || bhpTraceObject.getTraceData() == null) {
            ErrorDialog.showInternalErrorDialog(layer.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpModelSegyReader.getDepthArray Error");
            return null;
        }

        int samplesPerTrace = this.getMetaData().getSamplesPerTrace();
        float[] depthArray = new float[samplesPerTrace+1];
        for (int i=0; i<depthArray.length; i++) {
            depthArray[i] = bhpTraceObject.getTraceData()[samplesPerTrace+i];
        }
        return depthArray;
    }

    protected void initMetaDataAndModelLimits() {
        _metaData = new SeismicMetaData();
        _cache.getMetaInitiated();
        int samplesPerTrace = _cache.getSamplesPerTrace();
        _maxDepth = (float) _cache.getSampleRate();
        _minDepth = (float) _cache.getSampleStart();

        _metaData.setSampleStart(0);
        _metaData.setStartValue(_minDepth);
        _metaData.setSampleUnits(_cache.getSampleUnit());    // 1 for depth
        _metaData.setSamplesPerTrace((samplesPerTrace-1)/2);
        _metaData.setNumberOfTraces(_cache.getNumberOfTraces());
        //_metaData.setSampleRate((_maxDepth-_minDepth)/((samplesPerTrace-1)/2));
        _metaData.setSampleRate(1.0);
        _metaData.setDataStatistics(_cache.getMin(), _cache.getMax(),
                                    _cache.getAvg(), _cache.getRms());

        _modelLimits = new Bound2D(0,
                                  //_minDepth,
                                  0,
                                  _metaData.getNumberOfTraces()-1,
                                  //_minDepth +
                                  _maxDepth - _minDepth);
    }
}
