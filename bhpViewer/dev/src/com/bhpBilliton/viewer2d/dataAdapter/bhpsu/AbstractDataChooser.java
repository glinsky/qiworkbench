/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

 *        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

 *        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
 */
package com.bhpBilliton.viewer2d.dataAdapter.bhpsu;

import javax.swing.JOptionPane;
import java.util.ArrayList;
import java.util.StringTokenizer;

import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.data.GeneralDataChooser;
import com.bhpBilliton.viewer2d.data.GeneralDataSource;
import com.bhpBilliton.viewer2d.ui.OpenDataPanel;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpBilliton.viewer2d.util.IconResource;

import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This dialog is used to choose a dataset for display.
 *               Currently, the dialog can only be popped from the
 *               <code>{@link OpenDataPanel}</code>
 *               with the "Choose Dataset..." button.
 *               <br>
 *               The dialog is similar to the JFileChooser. It displays the
 *               entries in the specified path, BhpDataPath, in a list.
 *               The entries are divided into 3 categories, eligible file,
 *               non-eligible file, and directory. Each category is displayed
 *               with a different icon. For this chooser, eligible file is
 *               file with ".dat" extension. These files are used to specify
 *               the location of bhpio data files. <br>
 *               Users can navigate the file system with the up button,
 *               home button, double click, and ect, the same as in a
 *               JFileChooser. In addition, this dialog has a field showing
 *               BhpIoBinPath, specifying where bhpio executables are located.
 *               Users can change it with the "Edit" button. <br>
 *               When an eligible file is chosen, "bhpio" is ran with
 *               Runtime.getRuntime.exec(String) to summarize
 *               the selected data set. Then a
 *               <code>{@link BhpIODataSource}</code> object is
 *               constructed and initialized with the output of the "bhpio".
 *               OpenDataPanel.describeData(GeneralDataSource) is called
 *               and returns the control to the
 *               <code>{@link OpenDataPanel}</code>.
 *                <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

// Removed applet references
// TODO Delete remote subclass
public abstract class AbstractDataChooser extends JDialog implements GeneralDataChooser {

    protected static final int _ELIGIBLE_FILE = 1;
    protected static final int _GENERAL_FILE = 2;
    protected static final int _DIRECTORY = 3;
    private static final Logger logger =
            Logger.getLogger(AbstractDataChooser.class.toString());
    protected JList _list;
    protected DefaultListModel _listModel;
    protected JTextField _bhpioPathField;
    protected JTextField _bhpDataField;
    protected JButton _upButton;
    protected String _filename;
    protected OpenDataPanel _opanel;
    protected String _homeDirectory;
    private ImageIcon _efileIcon = null;
    private ImageIcon _gfileIcon = null;
    private ImageIcon _dirIcon = null;
    private ImageIcon _homeIcon = null;
    private ImageIcon _upIcon = null;

    // Save BhpViewrbase from constructor
    BhpViewerBase _viewer;

    /**
     * Constructs the chooser.
     * @param parent the Container parent of the dialog.
     * @param p the panel that will be used to display the selected data.
     */
    // Replaced Frame with BhpViewerBase
    public AbstractDataChooser(BhpViewerBase v, OpenDataPanel p) {
        _viewer = v;
        setTitle("Select bhpsu Data");
        setModal(true);
        setLocation(_viewer.getMyLocation());
        _opanel = p;
        _filename = "";
        _homeDirectory = "";
        try {
            _efileIcon = IconResource.getInstance().getImageIcon(IconResource.ELIGIBLEFILE_ICON);
            _gfileIcon = IconResource.getInstance().getImageIcon(IconResource.GENERALFILE_ICON);
            _dirIcon = IconResource.getInstance().getImageIcon(IconResource.DIRECTORY_ICON);
            _upIcon = IconResource.getInstance().getImageIcon(IconResource.UP_ICON);
            _homeIcon = IconResource.getInstance().getImageIcon(IconResource.HOME_ICON);
        } catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                    "AbstractDataChooser.constructor failed to load images");
        }
        String dataPath = setupGUI();
        if (!dataPath.equals("")) {
            //Woody Folsom:
            //Bug fix 9/19/2008 for 2/6/2008 bug
            //The AbstractDataChooserlogic assumes local IO and shows a specious error
            //messageDialog if the client cannot see the datasets directory when using remoteIO
            //because this condition is ignored other than displaying the warning, I have changed it to a log message
            //if this functionality is needed, a new qiWb command must be implemented for the specific purpose
            //of allowing the client to check the existence of a file or directory using remoteIO.
            //
            //JOptionPane.showMessageDialog(_viewer, dataPath + " doesn't exist. Define in associated project manager.", "File Chooser Error", JOptionPane.ERROR_MESSAGE);
            logger.warning(dataPath + " doesn't exist. Define in associated project manager.  If using remoteIO this may be caused by bug QIWB-30.");
        }
        this.setSize(400, 500);
    }

    /**
     * Retrieves the pathlist of the selected data.
     * @return the absolute path of the ".dat" file for the selected dataset.
     */
    public String getPathlist() {
        String path = _bhpDataField.getText();
        if (path.length() != 0 && !path.endsWith("/")) {
            path = path + "/";
        }
        String pathlist = path + getFilename() + ".dat";
        return pathlist;
    }

    /**
     * Retrieves the name of the selected dataset.
     */
    public String getFilename() {
        return _filename;
    }

    public void setFilename(String fileName) {
        _filename = fileName;
    }

    public static GeneralDataSource createDataSource(BhpViewerBase viewer, String fname,
            String pathlist, Integer dtype) {
        BhpIODataSource sc = new BhpIODataSource(viewer, fname, pathlist);
        return sc;
    }

    public void setDataChooserVisible(boolean v) {
        this.setVisible(v);
    }

    /**
     * This method should be implemented by subclasses to
     * update the content of the list according to the current
     * value of BhpDataPath.
     */
    protected abstract void updateList();

    protected void hideDialog() {
        this.setVisible(false);
    }

    protected void setBhpDataFieldText(String text) {
        _bhpDataField.setText(text);
    }

    /**
     * Add entries to the list model.
     * @param content A semicolon separated string of file names..
     *        Each file name is started with a letter, indicating
     *        if it is a directory ('D') or a file ('F'), and followed by
     *        the file's name.
     */
    protected void addListModelElements(String content) {
        StringTokenizer stk = new StringTokenizer(content, ";");
        String token = null;
        while (stk.hasMoreTokens()) {
            token = stk.nextToken().trim();
            if (token.startsWith("D")) {    // a directory

                _listModel.addElement(new ListObject(_DIRECTORY, token.substring(1)));
            } else {
                if (token.endsWith(".dat")) {
                    _listModel.addElement(new ListObject(_ELIGIBLE_FILE, token.substring(1, token.lastIndexOf(".dat"))));
                } else {
                    _listModel.addElement(new ListObject(_GENERAL_FILE, token.substring(1)));
                }
            }
        }
    }

    /**
     * Add entries to the list model.
     * @param dirList List of directories; empty if none.
     * @param fileList List of files; empty if none.
     */
    protected void addListModelElements(ArrayList<String> dirList, ArrayList<String> fileList) {
        for (String s : dirList) {
            _listModel.addElement(new ListObject(_DIRECTORY, s));
        }
        for (String s : fileList) {
            if (s.endsWith(".dat")) {
                _listModel.addElement(new ListObject(_ELIGIBLE_FILE, s.substring(0, s.lastIndexOf(".dat"))));
            } else {
                _listModel.addElement(new ListObject(_GENERAL_FILE, s));
            }
        }
    }

    /**
     * @return Path of dataset directory if it doesn't exist or exists and is not
     * a directory; otherwise, the empty string.empty string.
     */
    private String setupGUI() {
        String datadirExists = "";
        JPanel extraButtonPanel = new JPanel(new FlowLayout());
        _upButton = new JButton(_upIcon);
        _upButton.setPreferredSize(new Dimension(25, 25));
        _upButton.addActionListener(new UpListener());
        _upButton.setEnabled(true);
        JButton hmButton = new JButton(_homeIcon);
        hmButton.setPreferredSize(new Dimension(25, 25));
        hmButton.addActionListener(new HomeListener());
        hmButton.setEnabled(true);
        extraButtonPanel.add(_upButton);
        extraButtonPanel.add(hmButton);
        // Deprecated: functionality removed so inp1 never used. However, _bhpioPathField needs to be set for it it
        // used in openSelectedData().
        JPanel inp1 = new JPanel(new BorderLayout(10, 10));
        inp1.add(new JLabel("  BhpIoBinPath"), BorderLayout.WEST);
        _bhpioPathField = new JTextField("");
        _bhpioPathField.setEditable(false);
        inp1.add(_bhpioPathField, BorderLayout.CENTER);
        JButton changeB = new JButton("Edit...");
        changeB.addActionListener(new ChangeBinPathListener());
        inp1.add(changeB, BorderLayout.EAST);

        JPanel inp2 = new JPanel(new BorderLayout(10, 10));
        inp2.add(new JLabel("  BhpDataPath"), BorderLayout.WEST);
        _bhpDataField = new JTextField("");
        inp2.add(_bhpDataField, BorderLayout.CENTER);
        inp2.add(extraButtonPanel, BorderLayout.EAST);

        Box p1 = new Box(BoxLayout.Y_AXIS);
        p1.add(Box.createVerticalStrut(10));
        p1.add(Box.createVerticalStrut(10));
        p1.add(inp2);
        p1.add(Box.createVerticalStrut(10));
        _bhpDataField.addKeyListener(new MyDataPathKeyListener());

        QiProjectDescriptor projDesc = _viewer.getAgent().getQiProjectDescriptor();
        _homeDirectory = QiProjectDescUtils.getDatasetsPath(projDesc);
        File dataDir = new File(_homeDirectory);
        if (!(dataDir.exists() && dataDir.isDirectory())) {
            datadirExists = _homeDirectory;
        }
        setBhpDataFieldText(_homeDirectory);

        JPanel p2 = new JPanel(new BorderLayout());
        _listModel = new DefaultListModel();
        _list = new JList(_listModel);
        _list.addMouseListener(new MyListListener());
        _list.setCellRenderer(new MyCellRenderer());
        _list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        updateList();
        p2.add(new JScrollPane(_list), BorderLayout.CENTER);

        JPanel p3 = new JPanel(new FlowLayout());
        JButton b1 = new JButton("Ok");
        b1.addActionListener(new OkListener());
        JButton b2 = new JButton("Apply");
        b2.addActionListener(new ApplyListener());
        JButton b3 = new JButton("Cancel");
        b3.addActionListener(new CancelListener());
        p3.add(b1);
        p3.add(b3);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(p1, BorderLayout.NORTH);
        panel.add(p2, BorderLayout.CENTER);

        this.getContentPane().add(panel, BorderLayout.CENTER);
        this.getContentPane().add(p3, BorderLayout.SOUTH);

        return datadirExists;
    }

    private class OkListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            hideDialog();
            openSelectedData();
        }
    }

    private class ApplyListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            openSelectedData();
        }
    }

    private class CancelListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            hideDialog();
        }
    }

    /** @deprecated Removed _bhpioPathField from GUI */
    private class ChangeBinPathListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String msg = "Please specify the new bhpio path";
            String re = JOptionPane.showInputDialog(_list, msg);
            if (re == null) {
                return;
            }
            re = re.trim();
            if (re.length() > 0) {
                _bhpioPathField.setText(re);
            }
        }
    }

    private class UpListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String curDir = _bhpDataField.getText().trim();
            if (curDir != null && curDir.lastIndexOf("/") >= 0) {
                curDir = curDir.substring(0, curDir.lastIndexOf("/"));
            }
            if (curDir.length() == 0) {
                curDir = "/";
            }
            setBhpDataFieldText(curDir);
            updateList();
        }
    }

    private class HomeListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            setBhpDataFieldText(_homeDirectory);
            updateList();
        }
    }

    private class ListObject {

        private int _type;
        private String _name;

        public ListObject(int type, String name) {
            _type = type;
            _name = name;
        }

        public int getType() {
            return _type;
        }
        
        @Override
        public String toString() {
            return _name;
        }

        public ImageIcon getSymbolIcon() {
            if (_type == _ELIGIBLE_FILE) {
                return _efileIcon;
            } else if (_type == _DIRECTORY) {
                return _dirIcon;
            } else {
                return _gfileIcon;
            }
        }
    }

    private class MyCellRenderer extends JLabel implements ListCellRenderer {

        public Component getListCellRendererComponent(JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {
            if (value == null) {
                return this;
            }
            String s = value.toString();
            setOpaque(true);
            setText(s);
            if (value instanceof ListObject) {
                setIcon(((ListObject) value).getSymbolIcon());
            }
            if (isSelected) {
                setBackground(list.getSelectionBackground());
            } else {
                setBackground(list.getBackground());
            }
            return this;
        }
    }

    private class MyListListener extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isRightMouseButton(e) || e.getClickCount() < 2) {
                return;
            }
            JList list = (JList) e.getSource();
            Object selectedObj = list.getSelectedValue();
            if (selectedObj instanceof ListObject) {
                ListObject listObj = (ListObject) selectedObj;
                int type = listObj.getType();
                if (type == _ELIGIBLE_FILE) {
                    hideDialog();
                    openSelectedData();
                } else if (type == _DIRECTORY) {
                    String oldPath = _bhpDataField.getText();
                    if (!oldPath.endsWith("/")) {
                        oldPath = oldPath + "/";
                    }
                    setBhpDataFieldText(oldPath + listObj.toString());
                    updateList();
                } else {
                }
            }
        }
    }

    private class MyDataPathKeyListener extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                updateList();
            }
        }
    }
}