/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter.bhpsu;

import java.io.*;

import java.awt.Container;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.bhpBilliton.viewer2d.*;
import com.bhpBilliton.viewer2d.ui.OpenDataPanel;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpb.qiworkbench.compAPI.QIWConstants;



/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class is used by application to
 *               choose data set. It uses Runtime.exec() to run
 *               bhpio to get the summary information about the
 *               selected data set. This summary information
 *               is used by OpenDataPanel to populate the JTable
 *               it contained. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class LocalSeismicChooser extends AbstractDataChooser {
    private String _filename; //EXISTS JUST FOR CONSTRUCTOR

    private BhpViewerBase _viewer;

    public LocalSeismicChooser(BhpViewerBase parent, OpenDataPanel p) {
      super(parent,p);
      _viewer = parent;
    }

    public void openSelectedData(String filename) {
        _filename = filename;
        openSelectedData();
    }

    /**
     * Open a local dataset.
     */
    public void openSelectedData() {
        String file=null;
        if (_list.getSelectedValue() != null) {
            file = _list.getSelectedValue().toString();
        }
        if (file==null || file.length() == 0) {
            JOptionPane.showMessageDialog(_viewer,"Please select a file");
            return;
        }

        setFilename(file);
        //LTL BhpViewerBase bhpViewerBase = BhpViewerBase.getBhpViewerBase();

        BhpIODataSummary summary = new BhpIODataSummary(_viewer.getPropertyManager(),file, getPathlist(),
                                                        _bhpDataField.getText(),_bhpioPathField.getText(),_viewer);
        /*BhpIODataSummary summary = new BhpIODataSummary (((BhpViewerBase)
                                   (SwingUtilities.windowForComponent(this))).getPropertyManager(),
                                   file, getPathlist(),_bhpDataField.getText(),_bhpioPathField.getText());*/
        _opanel.describeData(summary.getDataSource());
    }


    /**
     * This implementation uses File.listFiles() to read the content of the
     * specified directory. It assemble a string and call
     * super.addListModelElements(String) with it.
     */
    protected void updateList() {
        /*BhpViewerBase theViewer = null;
        try {
            theViewer = (BhpViewerBase) (SwingUtilities.windowForComponent(this));
          theViewer = _parent;
        }
        catch (Exception ex) {
        }*/
        //if (theViewer != null) {
        if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("bhpDataPath" + _bhpDataField.getText());
      //LTL BhpViewerBase bhpViewerBase = BhpViewerBase.getBhpViewerBase();
      _viewer.getPropertyManager().setProperty("bhpDataPath", _bhpDataField.getText());
        //}
        _listModel.removeAllElements();

        String pathString = _bhpDataField.getText();
        File dataDir = new File(pathString);
        String filename;
        if (dataDir.exists() && dataDir.isDirectory()) {
            File[] dataFiles = dataDir.listFiles();
            String[] fileNames = new String[dataFiles.length];
            for (int i=0; i<dataFiles.length; i++) {
                filename = dataFiles[i].getName().trim();
                if (dataFiles[i].isFile()) {
                    //if (filename.endsWith(".dat"))
                    //    fileNames[i] = new String(AbstractDataChooser._ELIGIBLE_FILE + filename.substring(0, filename.lastIndexOf(".dat")) + ";");
                    //else
                        fileNames[i] = "F" + filename + ";";
                }
                else
                    {
                    fileNames[i] = "D" + filename + ";";
                    }
            }
            java.util.Arrays.sort(fileNames);
            StringBuffer content = new StringBuffer();
            for (int j=0; j<fileNames.length; j++)
                {
                content.append(fileNames[j]);
                }
            super.addListModelElements(content.toString());
        }
        else {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "Cannot find bhpDataPath");
        }
    }

}
