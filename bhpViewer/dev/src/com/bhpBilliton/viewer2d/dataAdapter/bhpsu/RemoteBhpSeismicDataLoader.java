/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

 *        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

 *        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
 */

package com.bhpBilliton.viewer2d.dataAdapter.bhpsu;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.bhpBilliton.viewer2d.Bhp2DviewerConstants;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.ui.util.FileTools;
import com.bhpBilliton.viewer2d.ui.util.TempFileManager;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  Read and save the data remotely using remote
 *               seismic IO <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @author Gil Hansen
 * @version 1.1
 */

public class RemoteBhpSeismicDataLoader extends BhpSeismicDataLoader {
    private static final int MAX_WAIT_TIME = 30*Bhp2DviewerConstants.MINUTE_WAIT_TIME;   //1/2 hour
    private static final Logger logger = Logger.getLogger(RemoteBhpSeismicDataLoader.class.getName());

    private byte[] seismicData = null;
    private BhpViewerBase viewerBase = null;
    private IMessagingManager messagingMgr = null;
    private RandomAccessFile _localFile = null;
    //private String _outputFile=null;
    private String _remoteFile = null; // name retained to issue remoteIO read commands

    /**
     * Constructs a new remote loader.
     * @param cache the place where trace data is stored.
     * @param parameter1 the servlet URL.
     * @param parameter2 bhpio query string.
     * @param outputFile name of the temporary file used to store the bhpread output.
     * @param chunkSize the maximum number of traces that will be read at one time.
     */
    public RemoteBhpSeismicDataLoader(BhpSeismicDataCache cache,
            String parameter1, String parameter2,
            String outputFile, int chunkSize) {
        super(cache, parameter1, parameter2, outputFile, chunkSize);
        viewerBase = cache.getBhpViewerBase();
        messagingMgr = viewerBase.getAgent().getMessagingManager();
    }

    /**
     * Informs the thread to stop. <br>
     * This method is called when the action is cancelled.
     */
    public void setMustDie() {
        // stop whatever is going on gracefully
        super.setMustDie();
    }

    /**
     * Saves the data back with servlet. <br>
     * @param name Name of the temporary file.
     * @param values The data that will be saved.
     *        The length of the array should be the same as the
     *        total number of traces.
     * @param index The vertical sample index of the data.
     *        For event data, since there is only one event in
     *        the temporary file, it is always 0.
     */
    public void saveData(String ename, double[] values, int index) throws Exception {
        String msgID;
        ArrayList params;
        String jobID = "";
        //QiWorkbenchMsg response;
        String command;

        int traceSize = BhpSegyReader.TRACE_HEADER_SIZE + 4 * _metaData.getSamplesPerTrace();
        int traceOffset = BhpSegyReader.TRACE_HEADER_SIZE + 4 * index;

        int pathIndex = _parameter2.indexOf("pathlist=");
        String pathlist = _parameter2.substring(pathIndex+9, _parameter2.indexOf(" ", pathIndex)).trim();
        int startIndex = _parameter2.indexOf("filename=");
        String filename = _parameter2.substring(startIndex+9, _parameter2.indexOf(" ", startIndex)).trim();

        //get the horizon data into a form for being written to a tmp file.
        //Note; based on the code in the ServletGetDataFile class.
        byte[] vals = toByteArray(values);

        //First, modify the existing temp file...
        
        params = new ArrayList();
        String locationPref = messagingMgr.getLocationPref();
        params.add(locationPref);
        params.add(_remoteFile);
        params.add(QIWConstants.HORIZON_FORMAT);
        params.add(vals);
        params.add(Integer.valueOf(traceOffset));
        params.add(Integer.valueOf(traceSize));
        
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.BINARY_FILE_WRITE_CMD,
                QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg response = processResponse(msgID);
        if (response.isAbnormalStatus()) {
            logger.info("An abnormal response was received when attempting to update the temp file with new event data: " + response.getContent());
            return;
        }
        
        // Use JobServices to write data
        params = new ArrayList<String>();
        params.add(locationPref);
        params.add("bash");
        params.add("-c");
        //remove dependency on hard-wired bhpPath; path taken from the platform environment
        params.add("source ~/.bashrc; "+ "bhpwritecube" + " < " + _remoteFile + " filename=" + filename + " pathlist=" +
                pathlist + " stdin_endian=1 horizons=" + ename);
        params.add("ture"); //doBlock
        
        if (Bhp2DviewerConstants.DEBUG_PRINT > 0) {
            command = "Executing: ";
            for (int i=1; i<params.size(); i++)
                command += params.get(i);
            logger.info(command);
        }

        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SUBMIT_JOB_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        // wait for the response. Note: There can only be 1 response since there
        // is a separate job service executing the request and is not executing
        // parallel jobs.
        
        response = processResponse(msgID);
        if (response == null) return;

        jobID = (String)MsgUtils.getMsgContent(response);
        if (Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("jobID = " + jobID);

        //get jobs's stderr and stdout = don't need either one
        params.clear();
        params.add(locationPref);
        params.add(jobID);
        //viewerBase.getAgent().setWait(true);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
                QIWConstants.ARRAYLIST_TYPE, params,true);
        response = processResponse(msgID);
        if (response == null) return;

        ArrayList<String> lines = null;
        if (Bhp2DviewerConstants.DEBUG_PRINT > 0) {
            System.out.println("job "+jobID+" standard err:");
            lines = (ArrayList<String>)response.getContent();
            for (int i=0; i < lines.size(); i++)
                System.out.println((String)lines.get(i));
        }

        params.clear();
        params.add(locationPref);
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_JOB_OUTPUT_CMD,
                QIWConstants.ARRAYLIST_TYPE, params,true);
        response = processResponse(msgID);
        if (response == null) return;
        if (Bhp2DviewerConstants.DEBUG_PRINT > 0) {
            System.out.println("job "+jobID+" standard out:");
            lines = (ArrayList<String>)response.getContent();
            for (int i=0; i < lines.size(); i++)
                System.out.println((String)lines.get(i));
        }

        //Wait until the command has finished
        params.clear();
        params.add(locationPref);
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
                QIWConstants.ARRAYLIST_TYPE, params,true);
        response = processResponse(msgID);
        if (response == null) return;

        //release the job
        params.clear();
        params.add(locationPref);
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.RELEASE_JOB_CMD,
                QIWConstants.ARRAYLIST_TYPE,params,true);
        //consume the response
        response = processResponse(msgID);
    }

    private void convertToBytes(double value, byte[] bv) {
        int bits = Float.floatToIntBits((float)value);
        bv[0] = (byte)((bits & 0xff000000) >> 24);
        bv[1] = (byte)((bits & 0xff0000) >> 16);
        bv[2] = (byte)((bits & 0xff00) >> 8);
        bv[3] = (byte)(bits & 0xff);
    }

    /**
     * Closes the file and set the reference to null.  TODO inform the consumer thread that no more data will be forthcoming, in case this producer thread has encountered an error
     */
    protected void finishLoading() {
        if (_localFile != null) {
            try {
                _localFile.close();
                //Create a new File for the purpose of deleting it when the JVM exits,
                //which File allows although RandomAccessFile does not
                File localFile = new File(_outputFile);
                if ((localFile) != null && localFile.exists()) {
                    logger.fine("READ_BHPSU_DATA_CMD readTrace response file exists, requesting deleteOnExit for: " + localFile.getAbsolutePath());
                    localFile.deleteOnExit();
                } else {
                    logger.fine("Cannot invoke deleteOnExit(), file cannot be accessed or is already deleted: " + _outputFile);
                }
                _localFile = null;
                //FIXME: This is deleting files before their time. :(
                //File file = new File(_outputFile);
                //file.delete();
                //WHF 1/18/2008 this legacy code has been retained but seems to do nothing
                TempFileManager.getInstance().addTempFile(_outputFile);
            } catch (Exception ex) {
                ErrorDialog.showInternalErrorDialog(viewerBase, Thread.currentThread().getStackTrace(),
                        "BhpSeismicDataLoader Failed to close _localFile");
            }
        }
    }

    /**
     * Issues two commands to the IOService :
     * <ol>
     *   <li>generate data and summary</li>
     *   <li>retrieve the trace data and cache it in a client side <code>RandomAccessFile</code></li>
     * </ol>
     *
     * The summary is sent back along with the name of the generated temporary file.
     * @retrun true if the action is successful, false otherwise.
     */
    protected boolean getDataAttributes() {
        IQiWorkbenchMsg response = null;

        // form parameters for remote BHP-SU read command
        ArrayList<String> params = new ArrayList<String>();
        // [0] IO preference
        params.add(QIWConstants.REMOTE_PREF);
        // [1] read request
        params.add("readReq=startRead");
        // [2] query string
        params.add("query="+_parameter2);
        // [3] output filename
        params.add("outFilename="+_outputFile);

        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.READ_BHPSU_DATA_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        // wait for the response, which will come.
        response = messagingMgr.getMatchingResponseWait(msgID, MAX_WAIT_TIME);

        boolean attributesReceived = false;

        if (MsgUtils.isResponseAbnormal(response)) {
            logger.finest("BHP-SU read data error:"+(String)MsgUtils.getMsgContent(response));
        } else {
            String tempFileName = (String) response.getContent();
            //TextBuffer txtbuf = new TextBuffer((ByteBuffer)response.getContent());
            BufferedReader reader = null;
            File tempFile = new File(tempFileName);
            try {
                reader = new BufferedReader(new FileReader(tempFile));
                if (reader == null) {
                    logger.warning("Opened null BufferedReader for message response temp file.");
                }


                if (reader != null) {
                    // first line is the remote file name, last line, the useful summary data
                    String line = reader.readLine();
                    //if (line == null || line.length() == 0) {
                    if (line == null) {
                        //TOO: This indicates a server-side bug: response should not be normal if first line is null!
                        logger.finest("RemoteBhpSeismicDataLoader error : first line is null");
                    } else if (line.indexOf("BHPVIEWERERROR") != -1) {
                        if (Bhp2DviewerConstants.DEBUG_PRINT > 0) {
                            logger.finest("RemoteBhpSeismicDataLoader : " + line);
                        }
                        while (line != null) {
                            line = reader.readLine();
                            logger.finest("    " + line);
                        }
                        //TOO: This indicates a server-side bug: response should not be normal if content contains BHPVIEWERERROR!
                    } else {
                        String content = null;
                        while (line != null) {
                            content = line;
                            if (content.startsWith("BHPVIEWERNAME")) {
                                _remoteFile = line.substring(13).trim();
                            }
                            if (Bhp2DviewerConstants.DEBUG_PRINT > 0)
                                logger.finest("RemoteBhpSeismicDataLoader file : " + content);
                            line = reader.readLine();
                        }

                        if (_mustDie) {
                            finishLoading();
                        }

                        // last line is the summary of the data: _min, _max, ...
                        calculateStatistics(content);

                        _outputFile = getAllSeismicData();
                        if (_outputFile != null) {
                            attributesReceived = true;
                            logger.info("Seismic data cached on client side: " + _outputFile);
                        } else {
                            logger.warning("Seismic data could not be cached on the client side.");
                        }
                    }
                }
            } catch (Exception ex) {
                logger.warning("Unable to read message response from temp file: " + tempFileName);
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException ioe) {
                        logger.warning("While closing reader for message response temp file, caught: " + ioe.getMessage());
                    }
                }
                if (tempFile != null) {
                    if (tempFile.exists()) {
                        logger.fine("READ_BHPSU_DATA_CMD startRead response tempFile exists, requesting deleteOnExit for: " + tempFile.getAbsolutePath());
                        tempFile.deleteOnExit();
                    }
                }
            }
        }
        return attributesReceived;
    }

    /**
     * Opens the temporary file and prepares for the reading.
     *
     * @return true if the action is successful, false otherwise.
     */
    protected boolean initiateLoading() {
        try {
            FileTools.finalizeStream(_localFile);

            _localFile = new RandomAccessFile(_outputFile, "r");
            int numberOfTraces = _metaData.getNumberOfTraces();
            if (numberOfTraces<0){
                System.out.println("There are no traces in temp file.");
                return false;
            }
            _loaded = new boolean[numberOfTraces];
            for (int i=0; i<numberOfTraces; i++) {
                _loaded[i] = false;}
            return true;
        } catch (Exception ex) {
            FileTools.finalizeStream(_localFile);
            return false;
        }
    }

    /**
     * Reads the local file and puts the trace data into the
     * <code>{@link BhpSeismicDataCache}</code>.
     * @param start the start position for reading, in trace number.
     * @param num the number of traces to be read.
     * @return true if valid data is put into the cache. False otherwise.
     */
    protected boolean getSeismicData(int start, int num) {
        byte[] data = getSeismicDataStream(start, num);
        return addToCache(start, num, data);
    }

    private byte[] getSeismicDataStream(int start, int num) {
        if (_localFile == null) {
            return null;
        }

        int offset = start * _traceSize;
        byte[] data = new byte[_traceSize*num];
        try {
            _localFile.seek(offset);
            _localFile.readFully(data);
            return data;
        } catch (Exception ex) {
            ErrorDialog.showErrorDialog(viewerBase, QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                    "Error reading data for viewer",
                    new String[] {"Possible permissions problem"},
                    new String[] {"Fix any permissions errors",
                    "If permissions are OK, contact workbench support"});
            return null;
        }
    }

    /**
     * Requests the entire set of trace data from the server stores the
     * name ofthe client-side temp-file in _localFile.
     *
     * Returns the name of the client-side temp file if successful, or
     * null otherwise.
     */
    private String getAllSeismicData() {
        String outputFileName = null;

        int start = 0;
        int num = _metaData.getNumberOfTraces();
        int traceSize = BhpSegyReader.TRACE_HEADER_SIZE + _metaData.getSamplesPerTrace() * 4;
        int offset = traceSize * start;
        int length = traceSize * num;

        IQiWorkbenchMsg response = null;

        // form parameters for remote BHP-SU read command
        ArrayList<String> params = new ArrayList<String>();
        // [0] IO preference
        params.add(QIWConstants.REMOTE_PREF);
        // [1] read request
        params.add("readReq=readTrace");
        // [2] remote file
        params.add("remoteFile="+_remoteFile);
        // [3] offset
        params.add("offset="+offset);
        // [4] start
        params.add("length="+length);

        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.READ_BHPSU_DATA_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);

        // wait for the response, which will come.
        // The time to read the data is indefinite because it depends on the
        // amount of data to be read. Wait up to 1/2 hour. Either the all of
        // the data will have been read by then or an abnormal response is
        // returned because of an IO error.
        response = messagingMgr.getMatchingResponseWait(msgID, MAX_WAIT_TIME);
        String cmdResponseMsg = (String)response.getContent();

        if (MsgUtils.isResponseAbnormal(response)) {
            logger.warning("READ_BHPSU_DATA_CMD failed: " + cmdResponseMsg);
        }

        if (cmdResponseMsg != null) {
            outputFileName = cmdResponseMsg;
        }

        return outputFileName;
    }

    /**
     * Process JobService response, waiting up to 60 seconds for server response
     * @param msgID Message ID of request
     * @return Response if received and matches request; otherwise, null
     */
    private IQiWorkbenchMsg processResponse(String msgID){
        IQiWorkbenchMsg resp;
        
        resp = messagingMgr.getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);

        if (resp == null) {
            logger.severe("SYSTEM ERROR: Timed out waiting for response to save data/ get attributes after 60 seconds.");
            return resp;
        }

        if (!resp.getMsgID().equals(msgID)) {
            logger.severe("Expected response to msg: " + msgID + " but received reponse to: " + resp.getMsgID());
            return null;
        }

        if (MsgUtils.isResponseAbnormal(resp)) {
            logger.severe("Job service error: "+(String)MsgUtils.getMsgContent(resp)+"; statucCode="+MsgUtils.getMsgStatusCode(resp));
            return null;
        }

        return resp;
    }
    
    private byte[] toByteArray(double[] values) {
        byte[] vals = new byte[4*values.length];

        byte[] bvalue = new byte[4];
        
        for (int i=0; i<values.length; i++) {
            convertToBytes(values[i], bvalue);
            int offset = 4*i;
            vals[offset] = bvalue[0];
            vals[offset+1] = bvalue[1];
            vals[offset+2] = bvalue[2];
            vals[offset+3] = bvalue[3];
        }
        
        return vals;
    }
}
