/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter.bhpsu;

import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.data.BhpEventReaderInterface;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.io.SeismicMetaData;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;
import com.gwsys.gw2d.model.Attribute2D;
import com.gwsys.gw2d.util.Bound2D;
import java.util.Hashtable;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is the customized reader for the event data.
 *               Method getEvent(String name) is used to get the whole
 *               event data.
 *               <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpEventSegyReader extends BhpSegyReader implements BhpEventReaderInterface {
    private float _minDepth;
    private float _maxDepth;
    private float[] _eventData;

    private BhpLayer layer;

    /**
     * Constructs a new instance.
     * @param layer the layer where the data will be displayed.
     * @param isRemote flag indicates if the data is fetched remotely.
     * @param par1 part 1 of the bhpio query string. <br>
     *        It is either the remote servlet url or
     *        local bhpio command with path.
     * @param par2 the query string to run bhpio.
     * @param outputFile name of the temporary data file.
     * @param chunksz reading chunk size in traces.
     *        This controls how many trace the loader will read
     *        at most at one time.
     */
    public BhpEventSegyReader(BhpLayer layer, boolean isRemote,
                                String par1, String par2,
                                String outputFile, int chunksz) {
        super(layer, isRemote, par1, par2, outputFile, chunksz);
        _eventData = null;
        this.layer = layer;
    }

    public Attribute2D getEventAttribute(String name) {
        return null;
    }

    /**
     * Gets the lower bound of vertical display limits.
     */
    public float getMinDepth() { return _minDepth; }
    /**
     * Gets the higher bound of vertical display limits.
     */
    public float getMaxDepth() { return _maxDepth; }

    /**
     * Saves the event back with bhpio.
     */
    public void saveEvent(String ename, double[] values) throws Exception {
        _thread.saveData(ename, values, 0);
    }

    /**
     * Process the desired trace.
     * In the case of event data, this method should not be called
     * at all. Method getEvent should be used instead to retrieves
     * the event data.
     */
    public boolean process(int traceId, NumberRange sampleRange, TraceData traceDataOut) {
        ErrorDialog.showInternalErrorDialog(layer.getViewer(), Thread.currentThread().getStackTrace(),
                                            "BhpEventSegyReader.process called in error");
        return false;
    }

    /**
     * Retrieves the event data.
     * @param name event name.
     * @return event data values in a float array.
     */
    public float[] getEvent(String name) {
        // we need add implementation to get a specific event
        // for now, get the first depth instead
        int eventIndex = 0;
        if (_eventData == null) {
            _eventData = new float[this.getMetaData().getNumberOfTraces()];
            int samplesPerTrace = this.getMetaData().getSamplesPerTrace();  // this is 1
            BhpTraceObject bhpTraceObject = null;
            int startPosition = TRACE_HEADER_SIZE;
            for (int j=0; j<_eventData.length; j++) {
                Integer key = new Integer(j);
                bhpTraceObject = (BhpTraceObject) _cache.get(key);
                if (bhpTraceObject == null || bhpTraceObject.getTraceData() == null) {
                    ErrorDialog.showInternalErrorDialog(layer.getViewer(), Thread.currentThread().getStackTrace(),
                                      "BhpEventSegyReader.getEvent Error: Incorrect data for trace" + j);
                   break;
                }
                _eventData[j] = bhpTraceObject.getTraceData()[eventIndex];
            }
        }

        if (_eventData == null) return null;
        float[] eventArray = new float[_eventData.length];
        System.arraycopy(_eventData, 0, eventArray, 0, _eventData.length);
        return eventArray;
    }

    /**
     * Saves the event data in memory back to disk.
     */
    public void saveEvent() throws Exception {
        throw new java.lang.UnsupportedOperationException(
                "Method saveEvent() not yet implemented by BhpEventSegyReader.");
    }

    /**
     * Updates the value of the named event with specific header values.
     * @param name the name of the requested event.
     * @param value the new event value.
     * @param param a hashtalbe specify the name and value of the headers.
     */
    public void putEventValueAt(String name, double value, Hashtable param) {
        throw new java.lang.UnsupportedOperationException("Method putEventValueAt() not yet implemented by BhpEventSegyReader.");
    }

    /**
     * Retrieves the value of the named event with specific header values.
     * @param name the name of the requested event.
     * @param param a hashtalbe specify the name and value of the headers.
     * @return a float for the specific event point.
     */
    public float getEventValueAt(String name, Hashtable param) {
                              ErrorDialog.showInternalErrorDialog(layer.getViewer(), Thread.currentThread().getStackTrace(),
                              "BhpEventSegyReader.getEventValueAt  is not properly implemented");
        return -999.25f;
    }

    protected void initMetaDataAndModelLimits() {
        _metaData = new SeismicMetaData();
        _cache.getMetaInitiated();
        int samplesPerTrace = _cache.getSamplesPerTrace();
        _maxDepth = (float) _cache.getMax();
        _minDepth = (float) _cache.getMin();
        if (_maxDepth == _minDepth) {
            _minDepth = _minDepth - 50;
            _maxDepth = _maxDepth + 50;
        }

        _metaData.setSampleRate(_cache.getSampleRate());
        _metaData.setStartValue(_cache.getSampleStart());
        _metaData.setSampleUnits(_cache.getSampleUnit());
        _metaData.setSamplesPerTrace(_cache.getSamplesPerTrace());
        _metaData.setNumberOfTraces(_cache.getNumberOfTraces());
        _metaData.setDataStatistics(_cache.getMin(), _cache.getMax(),
                                    _cache.getAvg(), _cache.getRms());

        _modelLimits = new Bound2D(0,
                                  //_minDepth,
                                  0,
                                  _metaData.getNumberOfTraces()-1,
                                  //_minDepth +
                                  _maxDepth - _minDepth);
    }
}