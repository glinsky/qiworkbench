/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter.bhpsu;

import javax.swing.SwingUtilities;

import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.io.SeismicMetaData;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class keeps a java.util.Hashtable to store all the traces
 *               that has been read. This hashtable is written by the loading
 *               thread, <code>{@link BhpSeismicDataLoader}</code>,
 *               and is read by <code>{@link BhpSegyReader}</code>.
 *               It also keeps the meta information about the data, such as
 *               the minimum value, the maximum value, the average value and ect.
 *               Please refer to the cgSeismicMetaData documentation in JSeismic
 *               for more detail on the seismic meta data and defined fields.
 *               When the layer is created, it has not been inserted into the
 *               view. When the loading thread has successfully generated
 *               the temporary data file and got the summary of the data, it
 *               will set those meta data into this cache. At that time, the layer
 *               will be inserted into the view with SwingUtilities.invokeLater().
 *               <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpSeismicDataCache {
    private volatile ConcurrentHashMap _cache;
    private Integer _reqKey;
    private String _tempFileName;
    private SeismicMetaData _metaData;

    private boolean _metaInitiated;

    private BhpLayer _layer;

    private boolean _loaderDied = false;
    private static final int SEISMIC_CACHE_WAIT_TIME = 50; // wait 1/20 second
    //private final Object META_INFO_LOCK = new Object();
    
    /**
     * Constructs the data cache.
     * @param layer the layer for which the data will be stored here.
     */
    public BhpSeismicDataCache(BhpLayer layer) {
        _layer = layer;
        _cache = new ConcurrentHashMap();
        _reqKey = null;

        _tempFileName = null;

        _metaData = new SeismicMetaData();
        _metaData.setDataStatistics(-1.0f, 1.0f, 0.0, 0.8);
        _metaData.setNumberOfTraces(0);
        _metaData.setSamplesPerTrace(2001);
        _metaData.setSampleRate(4);
        _metaData.setSampleUnits(0);
        _metaData.setSampleStart(0);
        _metaInitiated = false;

    }

    /**
     * Informs that the data loading thread has been stopped.
     */
    public void loaderBeingStopped() { _loaderDied = true; }

    public BhpViewerBase getBhpViewerBase(){
        return _layer.getBhpViewerBase();
    }
    /**
     * Finds out if the meta data of the selected data set has been initialized.
     * This method causes the calling thread block until the meta data
     * is initialized successfully by the data thread or something abnormal,
     * such as an exception, or a cancellation, happens.
     */
    public boolean getMetaInitiated() {
            while(_metaInitiated == false && !_loaderDied) {
                try {
                    Thread.sleep(SEISMIC_CACHE_WAIT_TIME);
                } catch (InterruptedException ie) {
                    ErrorDialog.showInternalErrorDialog(_layer.getViewer(), Thread.currentThread().getStackTrace(),
                        "BhpSeismicDataCache.getMetaInitiated::InterruptedException: "+ie.getMessage());
                }
            }
        return _metaInitiated;
        
    }

    /**
     * Gets the name of the temporary file that bhpread outputs to.
     */
    public String getTempFileName() { return _tempFileName; }
    /**
     * Gets the minimum amplitude value of the dataset,
     * which is part of the seismic meta data.
     */
    public float getMin()           {
        return (float)_metaData.getMinimumAmplitude(); }
    /**
     * Gets the maximum amplitude value of the dataset,
     * which is part of the seismic meta data.
     */
    public float getMax()           {
        return (float)_metaData.getMaximumAmplitude(); }
    /**
     * Gets the average sample value of the dataset,
     * which is part of the seismic meta data.
     */
    public double getAvg()          { return _metaData.getAverage(); }
    /**
     * Gets the root mean squared sample value of the dataset,
     * which is part of the seismic meta data.
     */
    public double getRms()          { return _metaData.getRMS(); }
    /**
     * Gets the number of samples per trace for the data set,
     * which is part of the seismic meta data.
     */
    public int getSamplesPerTrace() { return _metaData.getSamplesPerTrace(); }
    /**
     * Gets the total number of traces of the dataset,
     * which is part of the seismic meta data.
     */
    public int getNumberOfTraces()  { return _metaData.getNumberOfTraces(); }
    /**
     * Gets the sample rate unit of the dataset,
     * which is part of the seismic meta data.
     */
    public int getSampleUnit()      { return _metaData.getSampleUnits(); }
    /**
     * Gets the sample rate of the dataset,
     * which is part of the seismic meta data.
     */
    public double getSampleRate()   { return _metaData.getSampleRate(); }
    /**
     * Gets the depth/time position of the first sample in the dataset,
     * which is part of the seismic meta data.
     */
    public double getSampleStart()  { return _metaData.getSampleStart(); }

    /**
     * Informs that there is a problem getting the meta data of the data set. <br>
     * This method will in turn inform the layer that its data cannot
     * be successfully loaded. Eventually, such layer won't be inserted
     * and shown.
     */
    public void setMetaInfoError(String tfn) {
        _tempFileName = tfn;;
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                _layer.loadingLayerError();
            }
        });
    }

    /**
     * Handles the out of memory error.
     * When <code>{@link BhpSeismicDataLoader}</code>
     * loads the data, it may run out of memory.
     * In that case, this method will be called by the
     * loading thread so that it can clear the cache to
     * release some memory. It will also inform the
     * layer about the error and eventually the
     * frame where the problem happens will be closed
     * by <code>{@link BhpViewerBase}</code> for clean up.
     */
    public void outOfMemoryError() {
        _cache.clear();
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                _layer.processingOFM();
            }
        });
    }

    /**
     * Sets the meta data of the data set. <br>
     * In this method, after initiates fields of the seismic meta data,
     * the layer is asked to be set up and shown.
     * @param min minimum amplitude value.
     * @param max maximum amplitude value.
     * @param avg average sample value.
     * @param rms root mean squared smaple value.
     * @param spt number of samples per trace.
     * @param tnum total number of traces.
     * @param unit sample unit.
     * @param rate sample rate.
     * @param start sample start value.
     * @param tfn temporary file name.
     */
    public void setMetaInfo(SeismicMetaData meta, String tfn) {
        _tempFileName =  tfn;
        _metaData.setDataStatistics(meta.getMinimumAmplitude(),
                                    meta.getMaximumAmplitude(),
                                    meta.getAverage(), meta.getRMS());
        _metaData.setNumberOfTraces(meta.getNumberOfTraces());
        _metaData.setSamplesPerTrace(meta.getSamplesPerTrace());
        _metaData.setSampleRate(meta.getSampleRate());
        _metaData.setSampleUnits(meta.getSampleUnits());
        _metaData.setSampleStart(meta.getSampleStart());

        _metaInitiated = true;

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                if (_layer.getPipeline() == null)
                    _layer.setupSeismicDisplay();
                else _layer.updateSeismicDisplay();
            }
        });
    }

    /**
     * Puts a trace in the cache.
     *
     * It is not synchronized as it is only called from
     * within the synchronized BhpSeismicDataLoader's addToCache statement
     *
     * @param key the object for the trace ID.
     * @param value the binary data for the trace.
     */
    public Object put(Integer key, Object value) {
            return _cache.put(key, value);
    }

    /**
     * Gets the binary data of a given trace. <br>
     * If the requested trace is a possible one, but has
     * not been loaded yet, it waits for
     * <code>{@link BhpSeismicDataLoader}</code>
     * to load it.
     * @param key the trace id.
     * @return the binary data of the requested trace.
     */
    public Object get(Integer key) {
        if (_cache.containsKey(key)) {
            return _cache.get(key);
        } else {
            if (key.intValue() >= _metaData.getNumberOfTraces() || key.intValue() < 0) {
                _reqKey = null;
                return null;
            }
            synchronized(_cache) {
                while(_cache.containsKey(key) == false && !_loaderDied) {
                    try {
                        _reqKey = key;
                        _cache.notifyAll(); // wake up the writer thread that may be blocked on this get()
                        _cache.wait(SEISMIC_CACHE_WAIT_TIME); //and remove this reader from the pool of blocked threads for 1s
                    } catch (InterruptedException ie) {
                        ErrorDialog.showInternalErrorDialog(_layer.getViewer(), Thread.currentThread().getStackTrace(),
                                "BhpSeismicDataCache.get caught exception " + ie.getMessage());
                    }
                }
                _reqKey = null;
                return _cache.get(key);
            }
        }
    }

    /**
     * Gets the trace id that needs to be loaded now. <br>
     * <code>{@link BhpSeismicDataLoader}</code>
     * uses this method to find out which trace to load.
     */
    public Integer getRequiredKey() {
        return _reqKey;
    }

    public ConcurrentHashMap getCacheForSync() {
        return _cache;
    }
}