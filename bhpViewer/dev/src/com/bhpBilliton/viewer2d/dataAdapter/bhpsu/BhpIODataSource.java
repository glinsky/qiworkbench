/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter.bhpsu;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import com.bhpBilliton.viewer2d.Bhp2DviewerConstants;
import com.bhpBilliton.viewer2d.BhpEventLayer;
import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpModelLayer;
import com.bhpBilliton.viewer2d.BhpPropertyManager;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.data.BhpDataReader;
import com.bhpBilliton.viewer2d.data.GeneralDataSource;
import com.bhpBilliton.viewer2d.util.ErrorDialog;

import com.bhpb.qiworkbench.compAPI.QIWConstants;


/**
 * Title: BHP Viewer <br>
 * <br>
 * Description: This class represents the data source of BhpIO data. <br>
 * <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpIODataSource implements GeneralDataSource {
    private String _dataName;
    private String _pathlist;
    private int _dataType;
    private int _dataOrder;
    private String _dataAtt;
    private String _properties;

    private ArrayList _headersName;
    private ArrayList _headersSetting;

    private BhpViewerBase viewer;

    /**
     * Constructs a new instance.
     *
     * @param viewer Instance of the viewer.
     * @param dname
     *            name of the selected data set.
     * @param dpath
     *            pathlist of the selected data set.
     */
    public BhpIODataSource(BhpViewerBase viewer, String dname, String dpath) {
        this.viewer = viewer;
        _dataName = dname;
        _pathlist = dpath;

        _dataType = BhpViewer.BHP_DATA_TYPE_SEISMIC;
        _dataOrder = BhpViewer.BHP_DATA_ORDER_CROSSSECTION;
        _dataAtt = "";
        _properties = "";
        _headersName = new ArrayList();
        _headersSetting = new ArrayList();
    }

    /**
     * Initiates the data source with the remote stream.
     *
     * @param ins The remote input stream where data information can be found.
     */
    public void initWithStream(InputStream ins) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(ins));
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
                    System.out.println("    " + line);

                if (line.indexOf("data, stored in") != -1 && line.endsWith("order")) {
                    _dataAtt = line.trim();
                    if (_dataAtt.indexOf("SEISMIC") != -1) {
                        _dataType = BhpViewer.BHP_DATA_TYPE_SEISMIC;
                    } else if (_dataAtt.indexOf("HORIZON") != -1) {
                        _dataType = BhpViewer.BHP_DATA_TYPE_EVENT;
                    } else if (_dataAtt.indexOf("PROPERTY") != -1) {
                        _dataType = BhpViewer.BHP_DATA_TYPE_MODEL;
                    } else {
                        _dataType = BhpViewer.BHP_DATA_TYPE_UNKNOWN;
                    }
                    if (_dataAtt.indexOf("MAP-VIEW") != -1) {
                        _dataOrder = BhpViewer.BHP_DATA_ORDER_MAPVIEW;
                    } else {
                        _dataOrder = BhpViewer.BHP_DATA_ORDER_CROSSSECTION;
                    }
                } else if (line.indexOf("Number of Header Keys") != -1) {
                    String numberOfKeysString = line.substring(line.indexOf(":") + 1);
                    numberOfKeysString = numberOfKeysString.trim();
                    int numberOfKeys = Integer.parseInt(numberOfKeysString);
                    for (int i = 0; i < numberOfKeys; i++) {
                        String keyLine = reader.readLine();
                        if (keyLine == null) {
                            break;
                        }
                        String keyName = keyLine.substring(keyLine.indexOf(":") + 1, keyLine.indexOf(","));
                        String keyAtt = getKeyAttributeString(keyLine);
                        this.addHeader(keyName, keyAtt);
                    }
                } else if (line.indexOf("PROPERTIES") != -1 || line.indexOf("HORIZONS") != -1) {
                    _properties = line.substring(line.indexOf(":") + 1).trim();
                    //_properties = _properties.replace(' ', ',');
                }
            } catch (Exception ex) {
                ErrorDialog.showErrorDialog(viewer, QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                            "Error reading data for viewer",
                             new String[] {"Possible permissions problem or corrupted file"},
                             new String[] {"Fix any permissions errors",
                                           "Verify file is Ok",
                                     "If permissions are OK and file is OK, contact workbench support"});
                break;
            }
        }
    }

    /**
     * Initiates the data source with the stream converted to an array list.
     *
     * @param ins
     *            the input stream as an array list where data information can be found. This stream is the result of running bhpio command. Please
     *            refer to bhpio documentation for its content and format.
     */
  // Change data type to ArrayList<String>
  //public void initWithStream(InputStream ins) {
  public void initWithStream(ArrayList<String> ins) {
    String line;
    for(int i=0; i<ins.size(); i++) {
      line = ins.get(i);
      if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
          System.out.println("    " + line);

      if(line.indexOf("data, stored in") != -1 && line.endsWith("order")) {
              _dataAtt = line.trim();
                if(_dataAtt.indexOf("SEISMIC") != -1)
                    _dataType = BhpViewer.BHP_DATA_TYPE_SEISMIC;
        else if (_dataAtt.indexOf("HORIZON") != -1)
                  _dataType = BhpViewer.BHP_DATA_TYPE_EVENT;
        else if (_dataAtt.indexOf("PROPERTY") != -1)
                  _dataType = BhpViewer.BHP_DATA_TYPE_MODEL;
        else
                  _dataType = BhpViewer.BHP_DATA_TYPE_UNKNOWN;
                if(_dataAtt.indexOf("MAP-VIEW") != -1)
                  _dataOrder = BhpViewer.BHP_DATA_ORDER_MAPVIEW;
        else
          _dataOrder = BhpViewer.BHP_DATA_ORDER_CROSSSECTION;
      }
      else if(line.indexOf("Number of Header Keys") != -1) {
              String numberOfKeysString = line.substring(line.indexOf(":") + 1);
                numberOfKeysString = numberOfKeysString.trim();
                int numberOfKeys = Integer.parseInt(numberOfKeysString);
                for(int j=i+1; j<i+1+numberOfKeys; j++) {
                  String keyLine = ins.get(j);
                  String keyName = keyLine.substring(keyLine.indexOf(":") + 1, keyLine.indexOf(","));
                  String keyAtt = getKeyAttributeString(keyLine);
                  this.addHeader(keyName,keyAtt);
              }
          }
      else if(line.indexOf("PROPERTIES") != -1 || line.indexOf("HORIZONS") != -1) {
              _properties = line.substring(line.indexOf(":") + 1).trim();
              //_properties = _properties.replace(' ', ',');
          }
    }
    }

    /**
     * Gets the name of this data source implementation.
     */
    public String getDataSourceName() {
        return GeneralDataSource.DATA_SOURCE_BHPSU;
    }

    /**
     * Gets the name of the data.
     */
    public String getDataName() {
        return _dataName;
    }

    /**
     * Gets the pathlist of the data.
     */
    public String getDataPath() {
        return _pathlist;
    }

    /**
     * Gets the data attribute string.
     */
    public String getDataAtt() {
        return _dataAtt;
    }

    /**
     * Gets the type of the data. <br>
     * Please refer to <code>{@link BhpLayer}</code> for data type definitions.
     */
    public int getDataType() {
        return _dataType;
    }

    /**
     * Retrieves all the available properties/events.
     *
     * @return a string with all properties/events separated by spacce.
     */
    public String getProperties() {
        return _properties;
    }

    /**
     * Gets the name of all headers.
     */
    public String[] getHeaders() {
        Object[] nobj = _headersName.toArray();
        String[] result = new String[nobj.length];
        for (int i = 0; i < nobj.length; i++) {
            result[i] = nobj[i].toString();
        }
        return result;
    }

    /**
     * Gets the setting of a specific header.
     *
     * @param name
     *            name of the requested header.
     * @return the settings of the header with format: start-end[step].
     */
    public String getHeaderSetting(String name) {
        for (int i = 0; i < _headersName.size(); i++) {
            if (name.equalsIgnoreCase((String) _headersName.get(i))) {
                return (String) _headersSetting.get(i);
            }
        }
        return "";
    }

    /**
     * Creates a <code>{@link BhpDataReader}</code> for this data source.
     *
     * @param layer
     *            the <code>{@link BhpLayer}</code> that this reader will be associated with.
     */
    public BhpDataReader createBhpDataReader(BhpLayer layer, BhpPropertyManager pmanager) {
        String servletReadURL = "";;
        int chunkSz = 256; // This is a magic number that results in
                           // better performance during remoteIO than 
                           // the previous value of 20.
        boolean isRemote = pmanager.isServerRemote();
        StringBuffer binIOPath = new StringBuffer();

        binIOPath.append(" bhpread");

        binIOPath.append(" pathlist=");
        binIOPath.append(layer.getPathlist());
        binIOPath.append(" filename=");
        binIOPath.append(layer.getDataName());

        // add missingData selection to bhpread command - missingdata=["ignore","fill"]
        if(layer.getDataType() == BhpViewer.BHP_DATA_TYPE_SEISMIC) {
          binIOPath.append(" missingdata=");
          int md = layer.getMissingDataSelection();
          if(md == 0)
            binIOPath.append("ignore");
          else
            binIOPath.append("fill");
        }

        int dataType = layer.getDataType();
        if (dataType == BhpViewer.BHP_DATA_TYPE_MODEL) {
            binIOPath.append(" properties=");
            binIOPath.append(layer.getPropertyName().trim());
        } else if (dataType == BhpViewer.BHP_DATA_TYPE_EVENT) {
            binIOPath.append(" horizons=");
            binIOPath.append(layer.getPropertyName().trim());
        }

        binIOPath.append(layer.getParameter().getParameterString());

        //String par3 = layer.getDataName() + layer.getIdNumber();
        String tempFilePath = pmanager.getProperty("temporaryDirectory");
        if (!tempFilePath.endsWith("/")) {
            tempFilePath = tempFilePath + "/";
        }

        if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("BhpIODataSource.createBhpDataReader " + binIOPath.toString());

        if (layer instanceof BhpModelLayer) {
            return new BhpModelSegyReader(layer, isRemote,  servletReadURL, binIOPath.toString(), tempFilePath, chunkSz);
        } else if (layer instanceof BhpEventLayer) {
            return new BhpEventSegyReader(layer, isRemote, servletReadURL, binIOPath.toString(), tempFilePath, chunkSz);
        } else {
            return new BhpSegyReader(layer, isRemote, servletReadURL, binIOPath.toString(), tempFilePath, chunkSz);
        }

        //try { _reader = new TestSegyReader("/usr/local/tmp/cdp_gathers0.segy"); }
        //catch (Exception ex) {}
    }

    /**
     * Gets the order of the data. Please refer to <code>{@link BhpLayer}</code> for data order definitions.
     */
    public int getDataOrder() {
        return _dataOrder;
    }

    private void addHeader(String name, String att) {
        _headersName.add(name);
        _headersSetting.add(att);
    }

    private String getKeyAttributeString(String keyLine) {
        StringBuffer keyAtt = new StringBuffer();
        StringTokenizer tokenizer = new StringTokenizer(keyLine);
        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            if (token.indexOf("Name:") != -1) {
                token = tokenizer.nextToken();
                continue;
            } else if (token.indexOf("Min:") != -1) {
                token = tokenizer.nextToken();
                keyAtt.append(token.trim());
            } else if (token.indexOf("Max:") != -1) {
                token = tokenizer.nextToken();
                keyAtt.append("-");
                keyAtt.append(token.trim());
            } else if (token.indexOf("Incr:") != -1) {
                token = tokenizer.nextToken();
                keyAtt.append(" [");
                keyAtt.append(token.trim());
                keyAtt.append("] ");
            } else if (token.indexOf("Type") != -1) {
                token = tokenizer.nextToken();
                keyAtt.append(token.trim());
            } else {
                break;
            }
        }
        return keyAtt.toString().replace(',', ' ');
    }
}
