/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter.bhpsu;

import java.util.*;

import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.gwsys.seismic.indexing.SegyBinaryEntry;
import com.gwsys.seismic.reader.SeismicFormat;
import com.gwsys.seismic.indexing.SeismicConstants;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.util.SeismicByteFactory;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  For internal usage. This trace object is used in
 *               BhpSeismicDataCache to cache the converted seismic
 *               trace data and the original binary trace meta data. <br><br>
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

class BhpTraceObject {
    private float[] _traceData;
    private byte[] _binaryMeta;

    private BhpViewerBase viewer;
    //public cgTraceMetaData _traceMeta;
/*
    public BhpTraceObject(float[] d, cgTraceMetaData m) {
        _traceData = d;
        //_traceMeta = m;
        _traceMeta = null;
    }
*/

    /**
     * Constructs a new instance.
     * @viewer Instance of the viewer.
     * @param d float array of the trace data converted from binary data.
     * @param m byte array of the original trace meta data.
     */
    public BhpTraceObject(BhpViewerBase viewer, float[] d, byte[] m) {
        this.viewer = viewer;
        _traceData = d;
        _binaryMeta = m;
    }

    /**
     * Retrieves the trace data.
     * @return the float array of the trace data.
     */
    public float[] getTraceData() {
        return _traceData;
    }

    /**
     * Retrieves the trace meta data.
     * @param traceId the trace ID of this trace.
     * @param headerFields a vector of header fields in the trace meta data.
     */
    public TraceHeader getTraceMetaData(int traceId, Vector headerFields) {
        if (_binaryMeta == null) return null;
        return parseTraceHeader(traceId, _binaryMeta, headerFields, 0);
    }

    // don't worry about the following part!!!!!!!!!!!!!!!!!!!!!!!
    private TraceHeader parseTraceHeader(int traceId, byte[] header,
                                             Vector headerFields, int offset) {
        TraceHeader traceMetaData = new TraceHeader(traceId,(double)traceId,
                parseStartValue(header, headerFields, offset),
                parseNumberOfSamples(header, headerFields, offset),
                parseTraceStatus(header, headerFields, offset));

        parseHeaderFields(header, headerFields, traceMetaData.getDataValues(), offset);
        return traceMetaData;
    }

    private void parseHeaderFields(byte[] header, Vector headerFields, Hashtable dataValues, int offset) {
        SegyBinaryEntry field;
        int position = 0;
        for (Enumeration e = headerFields.elements(); e.hasMoreElements();) {
            field = (SegyBinaryEntry) e.nextElement();
            position = offset + field.getByteOffset();
            dataValues.put(new Integer(field.getFieldId()),
                    SeismicByteFactory.toNumber(field.getValueType(),header,position));

        }
    }

    private double parseStartValue(byte[] header, Vector headerFields, int offset) {
        Integer startValue = parseFieldInt(SeismicFormat.HEADER_START_TIME,
                                            header, headerFields, offset);
        if (startValue == null) return 0.0;
        else return (startValue.intValue()/1000000.0);
    }

    private int parseNumberOfSamples(byte[] header, Vector headerFields, int offset) {
        Integer numberOfSamples = parseFieldInt(SeismicFormat.HEADER_SAMPLES_IN_TRACE,
                                                header, headerFields, offset);
        if (numberOfSamples == null) return 0;
        return (numberOfSamples.intValue());
    }

    private int parseTraceStatus(byte[] header, Vector headerFields, int offset) {
        return SeismicConstants.NORMAL_TRACE;
    }

    private Integer parseFieldInt(int desiredField, byte[] header, Vector headerFields, int offset) {
        for (Enumeration fieldEnum = headerFields.elements(); fieldEnum.hasMoreElements();) {
            SegyBinaryEntry field = (SegyBinaryEntry) (fieldEnum.nextElement());
            int position = offset + field.getByteOffset();
            if (field.getFieldId() == desiredField) {
                switch(field.getValueType()) {
                    case SeismicConstants.DATA_FORMAT_FLOAT:
                    case SeismicConstants.DATA_FORMAT_IBM_FLOAT:
                    case SeismicConstants.DATA_FORMAT_DOUBLE:
                        ErrorDialog.showErrorDialog(viewer, QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                                    "BhpSegyReader.parseFieldInt Error :Get type " + field.getValueType(),
                                    new String[] {"Possible data corruption"},
                                    new String[] {"Check data",
                                                  "If data is OK, contact workbench support"});
                        return null;
                    default:
                        return new Integer(
                                SeismicByteFactory.toNumber(field.getValueType(),header,position).intValue());
                }
            }
        }
        return null;
    }
}
