/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter.bhpsu;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.bhpBilliton.viewer2d.Bhp2DviewerConstants;
import com.bhpBilliton.viewer2d.BhpPropertyManager;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.ui.util.FileTools;
import com.bhpBilliton.viewer2d.util.ErrorDialog;

import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

public class BhpIODataSummary {
    private String              fileName;
    private BhpIODataSource     dataSource;
    private BhpViewerBase       _viewer;
    private BhpPropertyManager  prop;
    private String dataPath;
    private String binPath;
    private String pathList;

    /** File separator of OS on which bhpio command will execute. */
    private String filesep = "/";

    // setup logging
    private static Logger logger = Logger.getLogger(BhpIODataSummary.class.getName());

    private boolean isLocal = true;

    // standard out from bhpio command
    private ArrayList<String> bhpioStdOut;

    public void setPropertyManager(BhpPropertyManager prop) {
        this.prop = prop;
    }

    public void setPropertyManager(BhpViewerBase vwr) {
        setPropertyManager(vwr.getPropertyManager());
    }

    private void setPaths() {
        binPath = prop.getProperty("bhpioBinPath");
        dataPath = prop.getProperty("bhpDataPath");
    }

    public BhpIODataSummary(BhpPropertyManager window, String fileName, String pathList, BhpViewerBase viewer) {
        setPropertyManager(window);
        setPaths();
        this.fileName = fileName;
        this.pathList = pathList;
        this._viewer = viewer;

        filesep = _viewer.getAgent().getMessagingManager().getServerOSFileSeparator();

        isLocal = _viewer.getAgent().getMessagingManager().getLocationPref().equals(QIWConstants.LOCAL_SERVICE_PREF);
    }

    public BhpIODataSummary(BhpPropertyManager window, String fileName, String pathList, String dataPath, String binPath, BhpViewerBase viewer) {
        this (window,fileName,pathList,viewer);

        setDataPath(dataPath);
        setBinPath(binPath);

        if (binPath.length()>0)
            this.binPath = binPath;
    }

    private void setBinPath(String newPath) {
        //if bin path remote, assumed it is on users PATH (also true if local)
        if (isLocal && !FileTools.isUsefulDirectory(newPath)) {
            return;
        }

        binPath = FileTools.getStandardPath(newPath);
    }

    private void setDataPath(String newPath) {
        //if data path remote, file already verified by remote file chooser
        if (isLocal && !FileTools.isUsefulDirectory(newPath)) {
            return;
        }

        dataPath = FileTools.getStandardPath(newPath);
    }

    public BhpIODataSource getDataSource() {
        bhpioStdOut = new ArrayList<String>(100);
        int status = getSummaryStream();
        if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("getSummaryStream returned " + status);
        if(status != 0)
            ErrorDialog.showErrorDialog(_viewer, QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                                        "Error reading data for viewer",
                                        new String[] {"Possible permissions problem or corrupted file"},
                                        new String[] {"Fix any permissions errors",
                                                      "Verify file is OK",
                                     "If permissions are OK and file is OK, contact workbench support"});
        dataSource = new BhpIODataSource(_viewer, fileName,pathList);
        dataSource.initWithStream(bhpioStdOut);
        return dataSource;
    }

    private int getSummaryStream() {
        String command = buildSummaryCommand();
        if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("BhpIODataSummary command : " + command);

        ArrayList<String> params = new ArrayList<String>();
        String locationPref = _viewer.getAgent().getMessagingManager().getLocationPref();
        params.add(locationPref);
        params.add("bash");
        params.add("-c");
        params.add(command);
        params.add("true"); //doBlock
        
        String msgID = _viewer.getAgent().getMessagingManager().sendRequest(QIWConstants.CMD_MSG,QIWConstants.SUBMIT_JOB_CMD,
                                            QIWConstants.ARRAYLIST_TYPE, params,true);
        
        //wait for the response.
        IQiWorkbenchMsg response = null;
        int ii = 0;
        
        response = _viewer.getAgent().getMessagingManager().getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);

        if (response == null){
            logger.severe("SYSTEM ERROR: Timed out after while waiting for response to BhpIODataSummary command : " + command);
            return -1;
        }

        if (MsgUtils.isResponseAbnormal(response)) {
          //TODO: Warning dialog: notify user could not submit job
          logger.finest("Job error:"+(String)MsgUtils.getMsgContent(response));
          return 1;
        }
        String jobID = (String)MsgUtils.getMsgContent(response);
        if (Bhp2DviewerConstants.DEBUG_PRINT > 0)
            logger.info("jobID = " + jobID);
        //Get the commands standard out and err which has already been captured;
        //process. The job has already terminated.
        //get jobs's standard out
        params.clear();
        params.add(locationPref);
        params.add(jobID);
        //_viewer.getAgent().setWait(true);
        msgID = _viewer.getAgent().getMessagingManager().sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_JOB_OUTPUT_CMD, QIWConstants.ARRAYLIST_TYPE, params,true);

        response = _viewer.getAgent().getMessagingManager().getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
        
        if (response == null){
            logger.severe("SYSTEM ERROR: Timed out waiting for response to BhpIODataSummary command : " + command);
            return -1;
        }
        
        if (MsgUtils.isResponseAbnormal(response)) {
          //TODO: Warning dialog: notify user could not submit job
          logger.finest("Job error:"+(String)MsgUtils.getMsgContent(response));
          return 1;
        }

        if (Bhp2DviewerConstants.DEBUG_PRINT > 0) {
            logger.info("job's standard out:");
            bhpioStdOut = (ArrayList<String>)response.getContent();
            for (int i=0; i < bhpioStdOut.size(); i++) {
                logger.info((String)bhpioStdOut.get(i));
            }
        }
        
        //get jobs's standard err
        params.clear();
        params.add(locationPref);
        params.add(jobID);
        
        msgID = _viewer.getAgent().getMessagingManager().sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_JOB_ERROR_OUTPUT_CMD, QIWConstants.ARRAYLIST_TYPE, params,true);

        response = _viewer.getAgent().getMessagingManager().getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
        
        if (response == null){
            logger.severe("SYSTEM ERROR: Timed out waiting for response to BhpIODataSummary command : " + command);
            return -1;
        }
        
        if (MsgUtils.isResponseAbnormal(response)) {
          //TODO: Warning dialog: notify user could not submit job
          logger.finest("Job error:"+(String)MsgUtils.getMsgContent(response));
          return 1;
        }

        if (Bhp2DviewerConstants.DEBUG_PRINT > 0) {
            logger.info("job's standard err:");
            ArrayList<String> lines = (ArrayList<String>)response.getContent();
            for (int i=0; i < lines.size(); i++)
                logger.info((String)lines.get(i));
        }
        //Wait until the command has finished
        params.clear();
        params.add(locationPref);
        params.add(jobID);
        
        msgID = _viewer.getAgent().getMessagingManager().sendRequest(QIWConstants.CMD_MSG, QIWConstants.WAIT_FOR_JOB_EXIT_CMD, QIWConstants.ARRAYLIST_TYPE, params,true);
        
        response = _viewer.getAgent().getMessagingManager().getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
        
        if (response == null){
            logger.severe("SYSTEM ERROR: Timed out waiting for response to BhpIODataSummary command : " + command);
            return -1;
        }
        
        if (MsgUtils.isResponseAbnormal(response)) {
          //TODO: Warning dialog: notify user could not submit job
          logger.finest("Job error:"+(String)MsgUtils.getMsgContent(response));
          return 1;
        }

        Integer status = (Integer)MsgUtils.getMsgContent(response);
        //No more commands; release the job
        params.clear();
        params.add(locationPref);
        params.add(jobID);
        
        msgID = _viewer.getAgent().getMessagingManager().sendRequest(QIWConstants.CMD_MSG, QIWConstants.RELEASE_JOB_CMD, QIWConstants.ARRAYLIST_TYPE, params,true);
            //consume the response
        response = _viewer.getAgent().getMessagingManager().getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
        
        return status.intValue();
    }

    /**
     * @param theViewer
     * @param file
     * @return
     */
    private String buildSummaryCommand() {
        if (!binPath.equals(prop.getProperty("bhpioBinPath"))) {
            prop.setProperty("bhpioBinPath", binPath);
        }
        if (binPath.length() != 0 && ! binPath.endsWith(filesep)) {
            binPath = binPath + filesep;
        }
        if (pathList.length() == 0 || !(new File(pathList)).exists() ) {
            if (!dataPath.equals(prop.getProperty("bhpDataPath"))) {
                prop.setProperty("bhpDataPath", dataPath);
            }
            if (dataPath.length() != 0 && ! dataPath.endsWith(filesep)) {
                dataPath = dataPath + filesep;
            }

            this.pathList = dataPath + fileName + ".dat";
        }
        //remove the hard-wired bin Path
        //String command = binPath + "bhpio" + " pathlist=" + this.pathList + " filename=" + fileName
        String command = "source ~/.bashrc; "+ "bhpio" + " pathlist=" + this.pathList + " filename=" + fileName
                + " summary=yes";
        return command;
    }
}
