/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter.bhpsu;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.bhpBilliton.viewer2d.Bhp2DviewerConstants;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.ui.util.FileTools;
import com.bhpBilliton.viewer2d.ui.util.TempFileManager;
import com.bhpBilliton.viewer2d.util.ErrorDialog;

import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This loader is used by the application to read and save the
 *               data locally. It uses Runtime.exec() to start bhpio process
 *               to read and save the data. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class LocalBhpSeismicDataLoader extends BhpSeismicDataLoader {
    private RandomAccessFile _localFile;
    //private String _remoteFile;
    IMessagingManager messagingMgr = null;

    // setup logging
    private static Logger logger = Logger.getLogger(LocalBhpSeismicDataLoader.class.getName());
    private BhpViewerBase viewerBase;
    // stderr from bhpread and write commands
    private ArrayList<String> bhpioStderr;

    /**
     * Constructs a new local loader.
     * @param cache the place where trace data is stored.
     * @param parameter1 the bhpio command with path.
     * @param parameter2 bhpio query string.
     * @param outputFile name of the temporary file used to store the bhpread output.
     * @param chunkSize the maximum number of traces that will be read at one time.
     */
    public LocalBhpSeismicDataLoader(BhpSeismicDataCache cache,
                                    String parameter1, String parameter2,
                                    String outputFile, int chunkSize) {
        super(cache, parameter1, parameter2, outputFile, chunkSize);
        _localFile = null;
        viewerBase = cache.getBhpViewerBase();
        messagingMgr = viewerBase.getAgent().getMessagingManager();
    }

    /**
     * Informs the thread to stop. <br>
     * If there is a running process of bhpio, kill it.
     * This method is called when the action is cancelled.
     */
    public void setMustDie() {
        // stop whatever is going on gracefully
        super.setMustDie();
    }

    /**
     * Save the data back. <br>
     * @param values The data that will be saved.
     *        The length of the array should be the same as the
     *        total number of traces.
     * @param index The vertical sample index of the data.
     *        For event data, since there is only one event in
     *        the temporary file, it is always 0.
     */
    public void saveData(String ename, double[] values, int index) throws Exception {
      String msgID;
      ArrayList<String> params;
      String jobID = "";
      //QiWorkbenchMsg response;
      String command;
      ArrayList<String> lines;
      ArrayList<String> stdOut;

      String wfilename = _cache.getTempFileName();
      RandomAccessFile wfile = null;

      wfile = new RandomAccessFile(wfilename, "rw");
      if (wfile == null) {
          throw new Exception("LocalBhpSeismicDataLoader.saveData null file : " + wfilename);
      }
      if (Bhp2DviewerConstants.DEBUG_PRINT > 0)
          System.out.println("LocalBhpSeismicDataLoader.saveData START");

      int headerSize = BhpSegyReader.TRACE_HEADER_SIZE;
      int traceSize = headerSize + _metaData.getSamplesPerTrace() * 4;
      int offset = headerSize + index * 4;
      byte[] bvalue = new byte[4];

      for (int i = 0; i < values.length; i++) {
        wfile.seek(i * traceSize + offset);
        convertToBytes(values[i], bvalue);
        wfile.write(bvalue);
      }

      int binIndex = _parameter2.indexOf("/bhpread");
      String bhpPath = _parameter2.substring(0, binIndex + 1);
      int pathIndex = _parameter2.indexOf("pathlist=");
      String pathlist = _parameter2.substring(pathIndex + 9, _parameter2.indexOf(" ", pathIndex)).trim();
      int startIndex = _parameter2.indexOf("filename=");
      String filename = _parameter2.substring(startIndex + 9, _parameter2.indexOf(" ", startIndex)).trim();

      // Use JobServices to write data
      params = new ArrayList<String>();
      String locationPref = messagingMgr.getLocationPref();
      params.add(locationPref);
      params.add("bash");
      params.add("-c");
      //remove dependency on hard-wired bhpPath; path taken from the platform environment
      //params.add(bhpPath + "bhpwritecube" + " < " + wfilename + " filename=" + filename + " pathlist=" +
      params.add("source ~/.bashrc; "+ "bhpwritecube" + " < " + wfilename + " filename=" + filename + " pathlist=" +
                 pathlist + " stdin_endian=1 horizons=" + ename);
      if (Bhp2DviewerConstants.DEBUG_PRINT > 0) {
          command = "Executing: ";
          for(int i=1; i<params.size(); i++)
              command += params.get(i);
          System.out.println(command);
      }

      ///viewerBase.getAgent().setWait(true);
      msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SUBMIT_JOB_CMD,
                                                           QIWConstants.ARRAYLIST_TYPE,params,true);
      // wait for the response. Note: There can only be 1 response since there
      // is a separate job service executing the request and is not executing
      // parallel jobs.

//    wait for the response.
      IQiWorkbenchMsg response = processResponse(msgID);
      if (response == null) return;

      //response = messagingMgr.getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
      //viewerBase.getAgent().setWait(false);
      jobID = (String)MsgUtils.getMsgContent(response);
      if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
          System.out.println("jobID = " + jobID);
      //get jobs's stderr and stdout = don't need either one
      params.clear();
      params.add(locationPref);
      params.add(jobID);
      //viewerBase.getAgent().setWait(true);
      msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
                                                    QIWConstants.ARRAYLIST_TYPE, params,true);
      response = processResponse(msgID);
      if (response == null) return;
      //response = messagingMgr.getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
      //viewerBase.getAgent().setWait(false);
      //if(!response.getMsgID().equals(msgID))
      //  System.out.println("Expected response to msg " + msgID + " received " + response.getMsgID());
      //if (MsgUtils.isResponseAbnormal(response)) {
        //TODO: Warning dialog: notify user cannot get job's std err
      //  logger.finest("Job error:"+(String)MsgUtils.getMsgContent(response));
      //}
      if(Bhp2DviewerConstants.DEBUG_PRINT > 0) {
          System.out.println("job's standard err:");
          lines = (ArrayList<String>)response.getContent();
          for (int i=0; i < lines.size(); i++)
              System.out.println((String)lines.get(i));
      }
      params.clear();
      params.add(locationPref);
      params.add(jobID);
      msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_JOB_OUTPUT_CMD,
              QIWConstants.ARRAYLIST_TYPE, params,true);

      response = processResponse(msgID);
      if (response == null) return;
      //response = messagingMgr.getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
      //viewerBase.getAgent().setWait(false);
      //if(!response.getMsgID().equals(msgID))
      //  System.out.println("Expected response to msg " + msgID + " received " + response.getMsgID());
      //if (MsgUtils.isResponseAbnormal(response)) {
        //TODO: Warning dialog: notify user cannot get job's std err
      //  logger.finest("Job error:"+(String)MsgUtils.getMsgContent(response));
      //}
      if(Bhp2DviewerConstants.DEBUG_PRINT > 0) {
          System.out.println("job's standard out:");
          lines = (ArrayList<String>)response.getContent();
          for (int i=0; i < lines.size(); i++)
              System.out.println((String)lines.get(i));
      }
      //Wait until the command has finished
      params.clear();
      params.add(locationPref);
      params.add(jobID);
      msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
              QIWConstants.ARRAYLIST_TYPE, params,true);
      response = processResponse(msgID);
      if (response == null) return;
      //response = messagingMgr.getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
      //viewerBase.getAgent().setWait(false);
      //if(!response.getMsgID().equals(msgID))
      //  System.out.println("Expected response to msg " + msgID + " received " + response.getMsgID());
      //if (MsgUtils.isResponseAbnormal(response)) {
        //TODO: Warning dialog: notify user issues with job
      //  logger.finest("Job error:"+(String)MsgUtils.getMsgContent(response));
      //}
      //release the job
      params.clear();
      params.add(locationPref);
      params.add(jobID);
      msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.RELEASE_JOB_CMD,
              QIWConstants.ARRAYLIST_TYPE,params,true);
      //consume the response
      response = processResponse(msgID);
      if (response == null) return;
      //response = messagingMgr.getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
      viewerBase.getAgent().setWait(false);
        /*String[] cmdArray = new String[3];
        cmdArray[0] = "sh";
        cmdArray[1] = "-c";
        cmdArray[2] = bhpPath + "bhpwritecube" + " < " + wfilename + " filename=" + filename + " pathlist=" + pathlist
                + " stdin_endian=1 horizons=" + ename;

        System.out.println(".saveData command : " + cmdArray[0] + cmdArray[1] + cmdArray[2]);
        ExecRunner er = new ExecRunner (cmdArray, ExecRunner.STREAM_STDOUT);
        er.run();
        System.out.println(".saveData EXIT: " + er.getOutput());*/

    }

    /**
     * Closes the file and set the reference to null.  TODO inform the consumer thread that no more data will be forthcoming, in case this producer thread has encountered an error
     */
    protected void finishLoading() {
        if (_localFile != null) {
            try {
                _localFile.close();
                _localFile = null;
                //FIXME: This is deleting files before their time. :(
                //File file = new File(_outputFile);
                //file.delete();
                TempFileManager.getInstance().addTempFile(_outputFile);
            }
            catch (Exception ex) {
                ErrorDialog.showInternalErrorDialog(viewerBase, Thread.currentThread().getStackTrace(),
                                                    "BhpSeismicDataLoader Failed to close _localFile");
            }
        }
    }

    /**
     * Calls bhpread to generate the temporary file and the data summary.
     * @return true if the action is successful, false otherwise.
     */
    protected boolean getDataAttributes() {
      String msgID;
      ArrayList<String> params;
      String jobID;

      String command;
      ArrayList<String> lines;
      ArrayList<String> stdOut;

      if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
          System.out.println("LocalBhpSeismicDataLoader.getDataAttributes START");

      // Use job services instead of Runtime and streams
      String tmpDir = _outputFile.substring(0, _outputFile.lastIndexOf("/"));
      try {
        _outputFile = File.createTempFile("BHP_", ".sutmp", new File(tmpDir)).getAbsolutePath();
      } catch(Exception e) {
          ErrorDialog.showErrorDialog(viewerBase, QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                                      "Error Creating Temp File",
                                      new String[] {"Possible permissions problem",
                                                    "Possible disk full condition"},
                                      new String[] {"Fix any permissions errors",
                                                    "Create more disk space",
                                      "If permissions and space are OK, contact workbench support"});
      }

      // read data to temp file
      params = new ArrayList<String>();
      String locationPref = messagingMgr.getLocationPref();
      params.add(locationPref);
      params.add("bash");
      params.add("-c");
      params.add("source ~/.bashrc; "+ _parameter2 + " > " + _outputFile + " out=" + _outputFile);
System.out.println("_parameter2="+_parameter2);
      if (Bhp2DviewerConstants.DEBUG_PRINT > 0) {
          command = "Executing: ";
          for (int i=1; i<params.size(); i++)
              command += params.get(i);
          System.out.println(command);
      }

      msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SUBMIT_JOB_CMD,
              QIWConstants.ARRAYLIST_TYPE,params,true);
      // wait for the response. Note: There can only be 1 response since there
      // is a separate job service executing the request and is not executing
      // parallel jobs.
      IQiWorkbenchMsg response = processResponse(msgID);
      if (response == null) return false;

      jobID = (String)MsgUtils.getMsgContent(response);
      if (Bhp2DviewerConstants.DEBUG_PRINT > 0)
          System.out.println("jobID = " + jobID);

      params.clear();

      params.add(locationPref);
      params.add(jobID);
      msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
              QIWConstants.ARRAYLIST_TYPE, params,true);
      response = processResponse(msgID);
      if (response == null) return false;

      if (Bhp2DviewerConstants.DEBUG_PRINT > 0) {
          System.out.println("job "+jobID+" standard err:");
          lines = (ArrayList<String>)response.getContent();
          for (int i=0; i < lines.size(); i++)
              System.out.println((String)lines.get(i));
      }

      params.clear();
      params.add(locationPref);
      params.add(jobID);
      msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_JOB_OUTPUT_CMD,
              QIWConstants.ARRAYLIST_TYPE, params,true);
      response = processResponse(msgID);
      if (response == null) return false;

      if (Bhp2DviewerConstants.DEBUG_PRINT > 0) {
          System.out.println("job "+jobID+" standard out:");
          lines = (ArrayList<String>)response.getContent();
          for (int i=0; i < lines.size(); i++)
              System.out.println((String)lines.get(i));
      }

      //Wait until the command has finished
      params.clear();
      params.add(locationPref);
      params.add(jobID);
      msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
              QIWConstants.ARRAYLIST_TYPE, params,true);
      response = processResponse(msgID);
      if (response == null) return false;

      //release the job
      params.clear();
      params.add(locationPref);
      params.add(jobID);
      //viewerBase.getAgent().setWait(true);
      msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.RELEASE_JOB_CMD,
                                                    QIWConstants.ARRAYLIST_TYPE,params,true);
      response = processResponse(msgID);
      if (response == null) return false;

      // get summary
      params = new ArrayList<String>();
      params.add(locationPref);
      params.add("bash");
      params.add("-c");
      params.add("source ~/.bashrc; "+ _parameter2 + " request=summary");
      params.add("true"); // doBlock
      
      if (Bhp2DviewerConstants.DEBUG_PRINT > 0 && logger.isLoggable(Level.INFO)) {
          command = "Executing: ";
          for (int i=1; i<params.size(); i++)
              command += params.get(i);
          logger.info(command);
      }

      msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SUBMIT_JOB_CMD,
              QIWConstants.ARRAYLIST_TYPE,params,true);
      // wait for the response. Note: There can only be 1 response since there
      // is a separate job service executing the request and is not executing
      // parallel jobs.
      response = processResponse(msgID);
      if (response == null) return false;

      jobID = (String)MsgUtils.getMsgContent(response);
      if (Bhp2DviewerConstants.DEBUG_PRINT > 0)
          System.out.println("jobID = " + jobID);

      // stdout - used for stats
      params.clear();
      params.add(locationPref);
      params.add(jobID);
      msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_JOB_OUTPUT_CMD,
              QIWConstants.ARRAYLIST_TYPE, params,true);
      response = processResponse(msgID);
      if (response == null) return false;

      stdOut = (ArrayList<String>)response.getContent();
      if (Bhp2DviewerConstants.DEBUG_PRINT > 0 && logger.isLoggable(Level.INFO)) {
          System.out.println("job "+jobID+" standard out:");
          for (int i=0; i <stdOut.size(); i++)
              logger.info((String)stdOut.get(i));
      }

      // stderr - not used
      params.clear();
      params.add(locationPref);
      params.add(jobID);
      msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
              QIWConstants.ARRAYLIST_TYPE, params,true);
      response = processResponse(msgID);
      if (response == null) return false;

      if (Bhp2DviewerConstants.DEBUG_PRINT > 0) {
          System.out.println("job's standard err:");
          lines = (ArrayList<String>)response.getContent();
          for (int i=0; i < lines.size(); i++)
              System.out.println((String)lines.get(i));
      }

      //Wait until the command has finished
      params.clear();
      params.add(locationPref);
      params.add(jobID);
      msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
              QIWConstants.ARRAYLIST_TYPE, params,true);
      response = processResponse(msgID);
      if (response == null) return false;

      //release the job
      params.clear();
      params.add(locationPref);
      params.add(jobID);
      msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.RELEASE_JOB_CMD,
              QIWConstants.ARRAYLIST_TYPE, params,true);

      //consume the response
      response = processResponse(msgID);

      if (response == null) return false;
System.out.println("stats="+stdOut.get(0));
      // get stats (if any)
      if (stdOut.size() > 0)
          calculateStatistics(stdOut.get(0));

      return true;
    }

    /**
     * Opens the temporary file and prepares for the reading.
     *
     * @return true if the action is successful, false otherwise.
     */
    protected boolean initiateLoading() {
        try {
            FileTools.finalizeStream(_localFile);

            _localFile = new RandomAccessFile(_outputFile, "r");
            int numberOfTraces = _metaData.getNumberOfTraces();
            if (numberOfTraces<0){
                System.out.println("There are no traces in temp file.");
                return false;
            }
            _loaded = new boolean[numberOfTraces];
            for (int i=0; i<numberOfTraces; i++)
                {_loaded[i] = false;}
            return true;
        } catch (Exception ex) {
            //System.out.println("LocalBhpSeismicDataLoader.intiateLoading Exception :"+ex.getMessage());
            FileTools.finalizeStream(_localFile);
            return false;
        }
    }

    /**
     * Reads the local file and puts the trace data into the
     * <code>{@link BhpSeismicDataCache}</code>.
     * @param start the start position for reading, in trace number.
     * @param num the number of traces to be read.
     * @return true if valid data is put into the cache. False otherwise.
     */
    protected boolean getSeismicData(int start, int num) {
        //System.out.println("LocalBhpSeismicDataLoader.getSeismicData : " + start);
        byte[] data = getSeismicDataStream(start, num);
        return addToCache(start, num, data);
    }

    private byte[] getSeismicDataStream(int start, int num) {
        if (_localFile == null) {
            return null;
        }

        int offset = start * _traceSize;
        byte[] data = new byte[_traceSize*num];
        try {
            _localFile.seek(offset);
            _localFile.readFully(data);
            return data;
        }
        catch (Exception ex) {
            ErrorDialog.showErrorDialog(viewerBase, QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                                        "Error reading data for viewer",
                                        new String[] {"Possible permissions problem"},
                                        new String[] {"Fix any permissions errors",
                                                      "If permissions are OK, contact workbench support"});
            return null;
        }
    }

    private void convertToBytes(double value, byte[] bv) {
        int bits = Float.floatToIntBits((float)value);
        bv[0] = (byte)((bits & 0xff000000) >> 24);
        bv[1] = (byte)((bits & 0xff0000) >> 16);
        bv[2] = (byte)((bits & 0xff00) >> 8);
        bv[3] = (byte)(bits & 0xff);
    }

    /**
     * Process JobService response.
     * @param msgID Message ID of request
     * @return Response if received and matches request; otherwise, null
     */
    private IQiWorkbenchMsg processResponse(String msgID){
        IQiWorkbenchMsg resp = null;

        int ii = 0;
        //wait for the response
        while (resp == null) {
            resp = messagingMgr.getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
            ii++;
            if (ii >= 24) break;
        }

        if (resp == null){
            logger.severe("SYSTEM ERROR: Timed out waiting for response to save data/ get attributes");
            return resp;
        }

        if (!resp.getMsgID().equals(msgID)){
            logger.severe("Expected response to msg " + msgID + " received " + resp.getMsgID());
            return null;
        }

        if (MsgUtils.isResponseAbnormal(resp)) {
            logger.severe("Job service error:"+(String)MsgUtils.getMsgContent(resp)+"; statucCode="+MsgUtils.getMsgStatusCode(resp));
            return null;
        }

        return resp;
    }
}
