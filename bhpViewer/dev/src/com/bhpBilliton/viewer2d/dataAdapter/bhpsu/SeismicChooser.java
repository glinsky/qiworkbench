/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter.bhpsu;

import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import com.bhpBilliton.viewer2d.Bhp2DviewerConstants;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.ui.OpenDataPanel;
import com.bhpBilliton.viewer2d.util.ErrorDialog;

import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Choose a local/remote dataset. Executes a bhpio command to get the
 * data summary which is used to populate an OpenDataPanel.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class SeismicChooser extends AbstractDataChooser {
    private static Logger logger = Logger.getLogger(SeismicChooser.class.getName());

    private String _filename; //EXISTS JUST FOR CONSTRUCTOR

    public SeismicChooser(BhpViewerBase parent, OpenDataPanel p) {
      super(parent,p);
      _bhpDataField.setEditable(false);
    }

    public void openSelectedData(String filename) {
        _filename = filename;
        openSelectedData();
    }

    /**
     * Open dataset and get summary information using a bhpio command..
     */
    public void openSelectedData() {

        if (_viewer == null) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "Cannot find BhpViewer instance");
            return;
        }

        String file = _list.getSelectedValue().toString();

        if (file == null || file.length() == 0) {
            JOptionPane.showMessageDialog(_viewer,"Please select a file");
            return;
        }
        setFilename(file);

        BhpIODataSummary summary =
            new BhpIODataSummary(_viewer.getPropertyManager(), file,
                                 getPathlist(),
                                 _bhpDataField.getText(),
                                 _bhpioPathField.getText(),_viewer);

        _opanel.describeData(summary.getDataSource());
    }

    /**
     * Generate a ';' separated list of directories and files in a
     * directory. Preappend "D" to a directory name, "F" to a file name;
     */
    protected void updateList() {
        if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("bhpDataPath" + _bhpDataField.getText());
        BhpViewerBase bhpViewerBase = _viewer;
        if (bhpViewerBase == null) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "Cannot find BhpViewer instance");
            return;
        }

        String pathString = _bhpDataField.getText();
        bhpViewerBase.getPropertyManager().setProperty("bhpDataPath", pathString);
        _listModel.removeAllElements();

        IMessagingManager messagingMgr = _viewer.getAgent().getMessagingManager();

        try {
            boolean isLocal = messagingMgr.getLocationPref().equals(QIWConstants.LOCAL_SERVICE_PREF);
            IQiWorkbenchMsg request = null;
            IQiWorkbenchMsg response = null;

            if (isLocal) {
                ArrayList<String> list = new ArrayList<String>();
                list.add(QIWConstants.LOCAL_SERVICE_PREF);
                list.add(pathString);
                String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_DIR_FILE_LIST_CMD,QIWConstants.ARRAYLIST_TYPE,list,true);
                response = messagingMgr.getMatchingResponseWait(msgId, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
            } else {
                request = new QiWorkbenchMsg(CompDescUtils.getDescCID(messagingMgr.getMyComponentDesc()),
                    CompDescUtils.getDescCID(messagingMgr.getComponentDescFromDisplayName(QIWConstants.SERVLET_DISPATCHER_NAME)),
                    QIWConstants.CMD_MSG,
                    QIWConstants.GET_REMOTE_DIR_FILE_LIST_CMD,
                    MsgUtils.genMsgID(),
                    QIWConstants.STRING_TYPE, pathString, true);

                response = messagingMgr.sendRemoteRequest(request);
            }

            if(response == null){
                ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                        "SeismicChooser timed out waiting for response to GET_REMOTE_DIR_FILE_LIST_CMD");
                logger.severe("SYSTEM ERROR: Timed out waiting for response to GET_REMOTE_DIR_FILE_LIST_CMD ");
                return;
            }
            if (!MsgUtils.isResponseAbnormal(response)) {
                ArrayList<ArrayList> list = (ArrayList<ArrayList>)MsgUtils.getMsgContent(response);
                ArrayList<String> dirList = list.get(0);  //directory list
                ArrayList<String> fileList = list.get(1);  //file list

                super.addListModelElements(dirList, fileList);
            }
        } catch (Exception e) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "SeismicChooser::updateList Exception");
        }
    }
}
