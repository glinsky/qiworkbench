/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter;

import java.awt.Container;
import javax.swing.filechooser.FileFilter;

import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.ui.OpenDataPanel;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpb.qiworkbench.api.IMessagingManager;
import java.util.ArrayList;

import com.bhpb.qiworkbench.client.util.GenericFileFilter;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 *  A local/remote file chooser for Indexed Segy Files
 *
 * @created    July 09, 2003
 * @author Gil Hansen
 * @version 1.1
 */

public class SegyChooser extends SeismicChooser {
  BhpViewerBase viewer;

  /**
   *  Constructs the SegyChooser for the specified OpenDataPanel
   *
   * @param  parent  Container, i.e., the viewer
   * @param  p       OpenDataPanel
   */
  // Changed Frame to Container
  public SegyChooser(Container parent, OpenDataPanel p ) {
    super( parent, p );
    viewer = (BhpViewerBase)parent;
  }

  /**
   *  Sets the dataChooserVisible attribute of the SegyChooser
   *
   * @param  v  The new dataChooserVisible value
   */
  public void setDataChooserVisible( boolean v ) {
      if ( v ) {
          String[] segyExtensions = new String[]{"sgy", "segy"};
          
          FileFilter segyFilter = (FileFilter)new GenericFileFilter( segyExtensions, "Segy (*.sgy)" );
          
          IMessagingManager messagingMgr = viewer.getAgent().getMessagingManager();
          
          //Removed ~50 commented lines except for the following 'TODO', which
          //was apparently never implemented
          //TODO: tell user why file not approved!! Can't simply silently ignore!
          
          ArrayList list = new ArrayList();
          //1st element the parent GUI object
          list.add(viewer);
          //2nd element is the dialog title
          list.add("Select SEG-Y File");
          // 3rd element is a list that contains current directory to start with
          //and a flag (yes or no) indicating if a already remembered directory
          //should be used instead
          ArrayList lst = new ArrayList();
          lst.add(messagingMgr.getProject());
          lst.add("yes");
          list.add(lst);
          //4th element is the file filter
          list.add(segyFilter);
          //5th element is the navigation flag
          list.add(true);
          //6th element is the producer component descriptor
          list.add(messagingMgr.getMyComponentDesc());
          //7th element is the type of file chooser either Open or Save
          list.add(QIWConstants.FILE_CHOOSER_TYPE_OPEN);
          //8th element is the message command
          list.add(QIWConstants.OPEN_SEGY_DATA_BY_2D_VIEWER);
          //9th element is the default prefix for the file name if user fails to prefix it. If no prefix is required then use empty String
          list.add("");
          //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
          list.add("sgy");
          //11th element is the target Tomcat url where the file chooser chooses
          list.add(messagingMgr.getTomcatURL());
          try{
              viewer.getAgent().doFileChooser(list, SegyChooser.class.getName(), this);
          } catch(Exception ex) {
              ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(), "Cannot start file chooser service");
          }
      }
  }
}
