/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.dataAdapter;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.*;

import com.bhpBilliton.viewer2d.*;
import com.bhpBilliton.viewer2d.data.*;

/**
 * <p>Abstract implementation of data source</p>
 * <p>Description: Abstract implementation of data source</p>
 * <p>
 * @author not attributable
 * @author Gil hansen
 * @version 1.1
 */
public abstract class AbstractDataSource implements GeneralDataSource {

    private String _dataName;
    private String _pathlist    = "";
    private int _dataType       = BhpViewer.BHP_DATA_TYPE_SEISMIC;
    private int _dataOrder      = BhpViewer.BHP_DATA_ORDER_CROSSSECTION;
    private String _dataAtt     = "";
    private String _properties  = "";

    private ArrayList _headersName = new ArrayList();
    private ArrayList _headersSetting = new ArrayList();

    /**
     * Creates data source
     * @param dataType data type
     */
    protected AbstractDataSource( int dataType ) {
        _dataType = dataType;
    }

    /**
     * Gets data name
     * @return the data name
     */
    public String getDataName() {
        return _dataName;
    }

    /**
     * Gets data path
     * @return the path
     */
    public String getDataPath() {
        return _pathlist;
    }

    /**
     * Gets attribute
     * @return the attribute
     */
    public String getDataAtt() {
        return _dataAtt;
    }

    /**
     * Type of the data.
     * The possible values are:
     * <table border="1" cellpadding="3" cellspacing="0">
     * <tr>
     * <td>BhpLayer.BHP_DATA_TYPE_SEISMIC</td>
     * </tr>
     * <tr>
     * <td>BhpLayer.BHP_DATA_TYPE_EVENT</td>
     * </tr>
     * </table>
     * @return the data type
     */
    public int getDataType() {
        return _dataType;
    }

    /**
     * Gets the properties
     * @return the properties
     */
    public String getProperties() {
        return _properties;
    }

    /**
     * Initiates the data source with the stream.
     * @param ins the input stream where data information can be found.
     */
    public void initWithStream(InputStream ins) {
    }

    /**
     * Initiates the data source with the ByteBuffer.
     * @param bytebuf The ByteBuffer where data information can be found.
     */
    public void initWithByteBuffer(ByteBuffer bytebuf) {
    }

    /**
     * Gets a list of the headers
     * @return the list
     */
    public String[] getHeaders() {

        Object[] nobj = _headersName.toArray();

        if( nobj == null || nobj.length == 0 ) return null;

        String[] result = new String[ nobj.length ];

        for( int i = 0; i < nobj.length; i++ ) {
            result[i] = nobj[i].toString();
        }

        return result;
    }

    /**
     * Get header setting by name
     * @param name the name of header's item
     * @return the header setting
     */
    public String getHeaderSetting( String name ) {
        String tname = null;
        for( int i = 0; i < _headersName.size(); i++ ) {
            tname = _headersName.get( i ).toString();
            if( tname.equals( name ) )
                return _headersSetting.get( i ).toString();
        }
        return "";
    }

    /**
     * Gets data order
     * The possible values are:
     * <table border="1" cellpadding="3" cellspacing="0">
     * <tr>
     * <td>BhpLayer.BHP_DATA_ORDER_CROSSSECTION</td>
     * </tr>
     * <tr>
     * <td>BhpLayer.BHP_DATA_ORDER_MAPVIEW</td>
     * </tr>
     * </table>
     * @return the data order
     */
    public int getDataOrder() {
        return _dataOrder;
    }

    protected void addHeader( String name, String att ) {
        _headersName.add( name );
        _headersSetting.add( att );
    }

    protected void setDataName( String name ) {
        _dataName = name;
    }

    protected void setProperties( String properties ) {
        _properties = properties;
    }

    protected void setDataPath( String path ) {
        _pathlist = path;
    }

    public abstract BhpDataReader createBhpDataReader( BhpLayer layer,
        BhpPropertyManager pmanager );

}
