package com.bhpBilliton.viewer2d.dataAdapter;

import java.io.*;
import java.util.*;


import com.bhpBilliton.viewer2d.*;
import com.bhpBilliton.viewer2d.data.*;
import com.gwsys.seismic.core.*;
import com.gwsys.seismic.reader.SeismicReader;
import com.gwsys.xmlhorizon.reader.*;

/**
 *  This class writes an array of header and event data information of Horizon
 *  file into a file using XML format. <p>
 *
 *  Helper class to HorizonDataWriter. Only used when for default Horizon saving
 *
 * @author     Ruby Varghese
 * @created    June 16, 2003
 * @version    1.0
 */

public class XmlHorizonWriter
{
    public static void writeHorizons(String fname, List layers)
            throws java.io.IOException {
        writeHorizons(new FileOutputStream(fname), layers);
    }
    public static void writeHorizons(OutputStream out, List layers)
            throws java.io.IOException {
        if (layers==null || layers.size()==0) return;
        BhpEventLayer sampleLayer = (BhpEventLayer) layers.get(0);
        if (sampleLayer == null) return;

        //1)get the header info first
        GeneralDataSource sampleSource = sampleLayer.getParameter().getDataSource();
        HorizonDescription desc = createHorizonDescription(sampleSource.getHeaders());
        if (desc == null) return;
        SimpleHorizonDataSource resultSource = new SimpleHorizonDataSource(desc);
        //if (defineNull) resultSource.setNaNValue(nullValue);
        //2)get the event data
        BhpEventLayer tmpLayer = null;
        GeneralDataSource tmpSource = null;
        SeismicReader tmpReader = null;
        for (int i=0; i<layers.size(); i++) {
            tmpLayer = (BhpEventLayer) layers.get(i);
            tmpSource = tmpLayer.getParameter().getDataSource();
            tmpReader = tmpLayer.getPipeline().getDataLoader().getDataReader();
            Horizon tmpHorizon = null;
            if (tmpReader instanceof EventSegyReader) {
                List tmpHzs = ((EventSegyReader)tmpReader).getSource().getHorizons();
                Iterator it = tmpHzs.iterator();
                while(it.hasNext()) {
                    Horizon h = (Horizon)it.next();
                    if(h.getName().compareToIgnoreCase(tmpLayer.getLayerName()) == 0) {
                        tmpHorizon = h;
                        break;
                    }
                }
            }
            else if (tmpReader instanceof TemporaryEventDataReader) {
                tmpHorizon = new SimpleHorizon(tmpLayer.getLayerName(), desc);
                List eventData = ((TemporaryEventDataSource)tmpSource).getEventDataList();
                double[] dataRecord = null;
                int uniqueId = 0;
                for (int j=0; j<eventData.size(); j++) {
                    //get a eventdata record
                    dataRecord = (double[]) (eventData.get(j));
                    //create a HorizonMetaData for each event data record
                    HorizonMetaData metaData = createHorizonMetaData(uniqueId++, dataRecord);
                    ((SimpleHorizon)tmpHorizon).addMetaData(metaData);
                }
            }
            if (tmpHorizon != null) {
                tmpHorizon.setHorizonParameter(
                    HorizonAttributeBuilder.convertToHorizonParemeters(
                    tmpLayer.getShapeListLayer().getShape(0).getAttribute()));
                resultSource.getHorizons().add(tmpHorizon);
            }
        }
        // 3) use HorizonDataWrter to write this info into a file
        HorizonDataWriter writer = new HorizonDataWriter();
        writer.saveDataSource(out, resultSource);
    }
  //private String fileName;
  private ArrayList headers;
  private ArrayList eventData;

  private boolean defineNull = false;
  private double nullValue = Double.NaN;
  private HorizonParameters horParams;

  /**
   * Constructor for the XmlHorizonWriter for the specified Horizon information
   * 
   * @param headers
   *            Header info of Horizon
   * @param eventData
   *            A List of event Data. Each event data has an array of double values
   * @param params
   *            Description of the Parameter
   */
	public XmlHorizonWriter(HorizonParameters params, ArrayList headers, ArrayList eventData) {
		//this.fileName = fileName;
		this.headers = headers;
		this.eventData = eventData;
		horParams = params;
	}

  public void save(String fileName) throws java.io.IOException
  {
      save(new FileOutputStream(fileName));
  }

  /**
   * Save the Horizon data into a file in XML format.
   * 
   * @throws java.io.IOException
   *             Throws an IOException if there is some problem in writing to the specified fileName
   */
	public void save(OutputStream outStream) throws java.io.IOException {
		HorizonDescription desc = null;
		SimpleHorizon horizon = null;

		//1)save the header info first
		desc = createHorizonDescription((String[]) headers.toArray());
		if (desc != null) {
			horizon = new SimpleHorizon("Horizon", desc);
			horizon.setHorizonParameter(horParams);
		}

		//2)save the event data info next
		double[] dataRecord = null;
		int uniqueId = 0;
		for (int j = 0; j < eventData.size(); j++) {
			//get a eventdata record
			dataRecord = (double[]) (eventData.get(j));
			//create a HorizonMetaData for each event data record
			HorizonMetaData metaData = createHorizonMetaData(uniqueId++, dataRecord);
			horizon.addMetaData(metaData);
		}

		// 3) use HorizonDataWrter to write this info into a file
		if (horizon != null && desc != null) {
			SimpleHorizonDataSource source = new SimpleHorizonDataSource(desc);
			source.getHorizons().add(horizon);
			if (defineNull) {
				source.setNaNValue(nullValue);
			}

			HorizonDataWriter writer = new HorizonDataWriter();
			writer.saveDataSource(outStream, source);
		}
	}

  /**
   * Creates a HorizonDescription for the List of header information specified
   * 
   * @param headerList
   *            ArrayList of header information
   * @return HorizonDescription
   */
	private static HorizonDescription createHorizonDescription(String[] headerList) {
		HorizonDescription desc = new HorizonDescription();
		boolean hasTime = false;

		int id = 0;

		for (int i = 0; i < headerList.length; i++) {
			String name = headerList[i];
			//need to recheck this part. XML parser gives error for "white space"
			String strTmp = "";
			if (name.length() > 0) {
				StringTokenizer stk = new StringTokenizer(name, " ");
				while (stk.hasMoreTokens()) {
					strTmp = strTmp + stk.nextToken().trim();
				}
				if (strTmp.length() == 0) {
					continue;
				}

				desc.addField(new HorizonField(strTmp, DataConstants.CG_DF_DOUBLE, id++));
				if (strTmp.equals("Time"))
					hasTime = true;
//				System.out.println("Field" + id + ": " + name);
			}
		}
		//needed to add this manually as per customer spec.
		if (!hasTime)
			desc.addField(new HorizonField("Time", DataConstants.CG_DF_DOUBLE, id++));

		return desc;
	}

  /**
	 * Create a HorizonMetaData for an event data record.
	 * 
	 * @param uniqueId
	 *            A unique id that describes the eventDataRecord
	 * @param eventDataRecord
	 *            An array of double data values
	 * @return HorizonMetaData
	 */
	private static HorizonMetaData createHorizonMetaData(int uniqueId, double[] eventDataRecord) {

		int countFields = eventDataRecord.length;
		//System.out.println("countFields"+countFields);

		HorizonMetaData meta = new HorizonMetaData(uniqueId, countFields);

		//unique id for each attribute
		int index = 0;

		for (int i = 0; i < countFields; i++) {
			if (index < countFields) {
				meta.putFieldValue(index++, eventDataRecord[i]);
			}
		}
		return meta;
	}

}
