/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter.segy;

import java.io.InputStream;
import java.util.*;

import javax.xml.parsers.*;

import org.w3c.dom.*;

import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.indexing.SegyBinaryEntry;
import com.gwsys.seismic.indexing.HeaderFieldEntry;
import com.gwsys.seismic.indexing.SegyHeaderFormat;
import com.gwsys.seismic.reader.SeismicFormat;
import com.gwsys.seismic.indexing.SeismicConstants;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  The special dynamic segy format is defined according to define by user.
 *
 * SegyBinaryEntry is used to describe the header.
 * Each of this field is constructed with 6 parameters. <br>
 * offset - the byte offset from the beginning of the header. <br>
 * type - the data type for this field. <br>
 * identifier - the constant used to identify this field. <br>
 * name - the name of this field. <br>
 * keyType - the key type of this field.
 *           Possible values are notKey, anyKey, pKey, and sKey <br>
 * isAnnStepTraceRelative - true if step is trace relative and
 *                          false if step is value relative <br>
 * Please refer to JSeismic documentation for more detail.
 * <br><br>
 *   Supported data types:
 *   <table border="1" cellpadding="3" cellspacing="0">
 *    <tr><td>Name</td><td>Equivalent in JSeismic</td></tr>
 *   <tr><td>INT8</td><td>SeismicConstants.DATA_FORMAT_BYTE</td></tr>
 *   <tr><td>INT16</td><td>SeismicConstants.DATA_FORMAT_SHORT</td></tr>
 *   <tr><td>INT32</td><td>SeismicConstants.DATA_FORMAT_INT</td></tr>
 *   <tr><td>FLOAT32</td><td>SeismicConstants.DATA_FORMAT_FLOAT</td></tr>
 *   <tr><td>IBM_FLOAT32</td><td>SeismicConstants.DATA_FORMAT_IBM_FLOAT</td></tr>
 *   <tr><td>FLOAT64</td><td>SeismicConstants.DATA_FORMAT_DOUBLE</td></tr>
 *   <tr><td>UINT8</td><td>SeismicConstants.DATA_FORMAT_UNSIGNEDBYTE</td></tr>
 *   <tr><td>UINT16</td><td>SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT</td></tr>
 *   <tr><td>UINT32</td><td>SeismicConstants.DATA_FORMAT_UNSIGNEDINT</td></tr>
 *  </table>
 * Copyright:    Copyright (c) 2003 <br>
 * Company:      INT <br>
 * @version 1.0
 */

public class DynamicSegyFormat implements SeismicFormat {

    private Vector _lineHeaderFormats = new Vector();
    private Vector _traceHeaderFormats = new Vector();
    private int _idCount = 600;


    /**
     * Creates dynamic segy format
     */
    public DynamicSegyFormat() {

    }

    /**
     * Creates dynamic segy format
     * @param is the input stream
     */
    public DynamicSegyFormat( InputStream is ) {
      load( is );
    }

    private Vector parsePlace( String place )
    {
        if( place.compareToIgnoreCase("line") == 0 )
        return _lineHeaderFormats;

        return _traceHeaderFormats;
    }
    private int parseHeaderFormat(String attrFormat )
    {
      final String DefEBCDIC  = "EBCDIC";
      if( attrFormat != null ) {
        if( attrFormat.compareToIgnoreCase(DefEBCDIC) == 0 )
         return SegyHeaderFormat.EBCDIC_HEADER;

      }
      return SegyHeaderFormat.BINARY_HEADER;
    }

    private SegyHeaderFormat parseHeader( Node header, NamedNodeMap attrs, String place )
    {
       final String DefSizeTag           = "Size"; // size
       final String DefFormatTag         = "Format"; // format
       final String DefFieldTag          = "Field"; // field
       final String DefIncludeFieldsTag  = "Fields"; // fields

        SegyHeaderFormat headerFormat = null;
        Node attrSize =  attrs.getNamedItem( DefSizeTag );
        Node attrFormat =  attrs.getNamedItem( DefFormatTag );
        Node attrIncludeFields =  attrs.getNamedItem( DefIncludeFieldsTag );

        boolean bIncludeDefLineFields =  (attrIncludeFields != null &&
                                          attrIncludeFields.getNodeValue().equalsIgnoreCase("Segy") &&
                                          place != null && place.equalsIgnoreCase("line")  );

        if( attrSize != null && attrSize.getNodeValue()  != null && attrFormat != null ) {

          int size = Integer.parseInt( attrSize.getNodeValue() );
          if(  size >= 0 ) {

            headerFormat = new SegyHeaderFormat( parseHeaderFormat( attrFormat.getNodeValue()  ), size );

            if( bIncludeDefLineFields ) addDefaultLineHeaders( headerFormat );

            NodeList fieldsList = header.getChildNodes();

            if( fieldsList != null && fieldsList.getLength() > 0 ) {

              int nFields = fieldsList.getLength();

              for( int i=0; i < nFields; i++ ) {
                if( fieldsList.item( i ).getNodeName().equalsIgnoreCase( DefFieldTag ) )
                {
                  parseField( fieldsList.item( i ), headerFormat );
                }
              }
            }
          }
        }
        return headerFormat;
    }

    private void parseField( Node field, SegyHeaderFormat format ) {
         // gets attributs
         NamedNodeMap attrs = field.getAttributes();

         String name = "";
         int type   = SeismicConstants.DATA_FORMAT_INT;
         int offset = 0;
         int id     = -1;
         int key = HeaderFieldEntry.FIELD_TYPE_ANY;
         boolean primary = false;

         if( attrs == null || attrs.getLength() == 0 ) return;

         for( int i=0; i < attrs.getLength(); ++i ) {
            Node node = attrs.item( i );

            if( node.getNodeName().compareToIgnoreCase( "Name" ) == 0 ) {
              name = node.getNodeValue().trim();
            }
            if( node.getNodeName().compareToIgnoreCase( "Format" ) == 0 ) {
              type = SegyConstantsParser.format(node.getNodeValue().trim()).intValue();
            }
            if( node.getNodeName().compareToIgnoreCase( "Offset" ) == 0 ) {
              offset = Integer.parseInt( node.getNodeValue().trim() );
            }
            if( node.getNodeName().compareToIgnoreCase( "SegyId" ) == 0 ) {
              Integer val = SegyConstantsParser.getSegyField( node.getNodeValue().trim());
              id = (val != null ) ? val.intValue() : ++_idCount;
            }
            if( node.getNodeName().compareToIgnoreCase( "Key" ) == 0 ) {
              key = parseKeyType( node.getNodeValue().trim() );
            }
            if( node.getNodeName().compareToIgnoreCase( "Primary" ) == 0 ) {
              primary = node.getNodeValue().trim().equalsIgnoreCase("true");
            }
         }
         if( id == -1 ) id = (++_idCount);
         format.addFieldDescription( new SegyBinaryEntry(offset, type, id,
         name, key, primary ));
    }
    private int parseKeyType(String key )
    {
      if (key.equalsIgnoreCase("No")) {
        return HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED;
      }

      return HeaderFieldEntry.FIELD_TYPE_ANY;
    }


    protected void addDefaultLineHeaders( SegyHeaderFormat binaryHeaderFormat  )
    {
        // Declares the binary header field description variable.
        SegyBinaryEntry fieldDesc;

        // ====================================================
        // Fill binary header description
        // ====================================================

        fieldDesc =
          new SegyBinaryEntry( 4, SeismicConstants.DATA_FORMAT_UNSIGNEDINT, SeismicFormat.HEADER_LINE_NUMBER,
                                       "Line Number", HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false );
            binaryHeaderFormat.addFieldDescription( fieldDesc );

            fieldDesc =
          new SegyBinaryEntry( 12, SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT, SeismicFormat.HEADER_TRACES_PER_ENSEMBLE,
                                       "Traces per Ensemble", HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false );
            binaryHeaderFormat.addFieldDescription( fieldDesc );


            fieldDesc =
          new SegyBinaryEntry( 16, SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT, SeismicFormat.HEADER_SAMPLE_INTERVAL,
                                       "Sample Interval", HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false );
            binaryHeaderFormat.addFieldDescription( fieldDesc );

            fieldDesc =
          new SegyBinaryEntry( 20, SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT, SeismicFormat.HEADER_SAMPLES_PER_TRACE,
                                       "Samples per Trace", HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false );
            binaryHeaderFormat.addFieldDescription( fieldDesc );

            fieldDesc =
          new SegyBinaryEntry( 24, SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT, SeismicFormat.HEADER_DATA_FORMAT_CODE,
                                       "Data Format", HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false );
            binaryHeaderFormat.addFieldDescription( fieldDesc );

            fieldDesc =
          new SegyBinaryEntry( 26, SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT, SeismicFormat.HEADER_TRACES_PER_CDP_ENSEMBLE,
                                       "Traces per CDP", HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false );
            binaryHeaderFormat.addFieldDescription( fieldDesc );

            fieldDesc =
          new SegyBinaryEntry( 54, SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT, SeismicFormat.HEADER_MEASUREMENT_SYSTEM,
                                       "Measurement System", HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false );
            binaryHeaderFormat.addFieldDescription( fieldDesc );

            fieldDesc =
          new SegyBinaryEntry( 300, SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT, SeismicFormat.HEADER_SEGY_REVISION_NUMBER,
                                       "Segy Revision Number", HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false );
            binaryHeaderFormat.addFieldDescription( fieldDesc );

            fieldDesc =
          new SegyBinaryEntry( 302, SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT, SeismicFormat.HEADER_FIXED_LENGTH_FLAG,
                                       "Fixed Length Flag", HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false );
            binaryHeaderFormat.addFieldDescription( fieldDesc );

            fieldDesc =
          new SegyBinaryEntry( 304, SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT, SeismicFormat.HEADER_MAXIMUM_SAMPLES_PER_TRACE,
                                       "Max Samples per Trace", HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false );
            binaryHeaderFormat.addFieldDescription( fieldDesc );


    }

    /**
     * Load segy format from stream to be presented as XML.
     * @param is the input stream
     */
    public void load( InputStream is )
    {
        final String DefHeaderTag     = "Header"; // headers
        final String DefPlaceTag     = "Place"; // headers

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document doc = null;

        _lineHeaderFormats = new Vector();
        _traceHeaderFormats = new Vector();

        _idCount  = 600;

        try {

            DocumentBuilder builder = factory.newDocumentBuilder();

            doc = builder.parse( is );

            if( doc != null ) {
              NodeList list = doc.getElementsByTagName( DefHeaderTag );
              int nHeaders = list.getLength();

              for( int i = 0; i < nHeaders; ++i ) {

                  Node header = list.item( i );;
                  // gets attributs
                  NamedNodeMap attrs = header.getAttributes();
                  Node attr = attrs.getNamedItem( DefPlaceTag );

                  if( attr != null )
                  {
                     Vector vPlace = parsePlace( attr.getNodeValue() );
                     SegyHeaderFormat headerFormat = parseHeader( header, attrs, attr.getNodeValue() );

                     if( headerFormat != null ) {
                        vPlace.add( headerFormat );
                     }
                  }
              }
            }
        } catch( Exception e ) {
            e.printStackTrace();
/*
            ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                "Exception in DynamicSegyFormat");
*/
        }
    }

    /**
     *     Creates dynamic segy format
     *     @param traceHeaderFormat trace header
     */
    public DynamicSegyFormat( SegyHeaderFormat traceHeaderFormat ) {

        _traceHeaderFormats.addElement( traceHeaderFormat );
    }

    public Vector getDataHeaderFormats() {
        return _lineHeaderFormats;
    }
    public int getDataHeadersSize() {
        return _lineHeaderFormats.size();
    }
    public Vector getTraceHeaderFormats() {
        return _traceHeaderFormats;
    }
    public int getTraceHeadersSize() {
        return _traceHeaderFormats.size();
    }

    /**
     * Static method to find out the identifier of the named header.
     * @param name name of the requested header.
     * @param format the format of the header.
     * @return an Integer for the header identifier. If the named header
     *         is not found in the format, null will be returned.
     */
    public static Integer getFieldForName(String name, SegyHeaderFormat format) {
        Vector fields = format.getHeaderFields();
        HeaderFieldEntry headerField;
        for (int i=0; i<fields.size(); i++) {
            headerField = (HeaderFieldEntry) (fields.elementAt(i));
            if (headerField.getName().equals(name))
                return new Integer(headerField.getFieldId());
        }
        return null;
    }
}
