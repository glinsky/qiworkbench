/*
 bhpViewer - a 2D seismic viewer
 This program module Copyright (C) Interactive Network Technologies 2006

 The following is important information about the license which accompanies this copy of
 the bhpViewer in either source code or executable versions (hereinafter the "Software").
 The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
 is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
 http://www.gnu.org/licenses/gpl.txt.
 This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with this program;
 if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 For a license to use the Software under conditions other than in a way compliant with the
 GPL or the additional restrictions set forth in this license agreement or to purchase
 support for the Software, please contact: Interactive Network Technologies, Inc.,
 2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

 All licensees should note the following:

 *        In order to compile and/or modify the source code versions of the Software,
 a user may require one or more of INT's proprietary toolkits or libraries.
 Although all modifications or derivative works based on the source code are governed by the
 GPL, such toolkits are proprietary to INT, and a library license is required to make use of
 such toolkits when making modifications or derivatives of the Software.
 More information about obtaining such a license can be obtained by contacting sales@int.com.

 *        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
 serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
 submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
 This will allow the Custodian to consider integrating such revised versions into the
 version of the Software it distributes. Doing so will foster future innovation
 and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
 the Custodian is under no obligation to include modifications or revisions in future
 distributions.

 This program module may have been modified by BHP Billiton Petroleum,
 G&W Systems Consulting Corp or other third parties, and such portions are licensed
 under the GPL.
 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
 visit http://qiworkbench.org to learn more.
 */

package com.bhpBilliton.viewer2d.dataAdapter.segy;

import com.gwsys.seismic.reader.SeismicFormat;
import com.gwsys.seismic.indexing.SeismicConstants;

public class SegyConstantsParser {

	public static Integer getSegyField(String s) {
		if (s.equalsIgnoreCase(SeismicFormat.LINE_NUMBER)) {
			return new Integer(SeismicFormat.HEADER_LINE_NUMBER);
		} else if (s.equalsIgnoreCase(SeismicFormat.TRACES_PER_ENSEMBLE)) {
			return new Integer(SeismicFormat.HEADER_TRACES_PER_ENSEMBLE);
		} else if (s.equalsIgnoreCase(SeismicFormat.AUX_TRACES_PER_ENSEMBLE)) {
			return new Integer(SeismicFormat.HEADER_AUX_TRACES_PER_ENSEMBLE);
		} else if (s.equalsIgnoreCase(SeismicFormat.SAMPLE_INTERVAL)) {
			return new Integer(SeismicFormat.HEADER_SAMPLE_INTERVAL);
		} else if (s.equalsIgnoreCase(SeismicFormat.SAMPLES_PER_TRACE)) {
			return new Integer(SeismicFormat.HEADER_SAMPLES_PER_TRACE);
		} else if (s.equalsIgnoreCase(SeismicFormat.DATA_FORMAT_CODE)) {
			return new Integer(SeismicFormat.HEADER_DATA_FORMAT_CODE);
		} else if (s.equalsIgnoreCase(SeismicFormat.TRACES_PER_CDP_ENSEMBLE)) {
			return new Integer(SeismicFormat.HEADER_TRACES_PER_CDP_ENSEMBLE);
		} else if (s.equalsIgnoreCase(SeismicFormat.MEASUREMENT_SYSTEM)) {
			return new Integer(SeismicFormat.HEADER_MEASUREMENT_SYSTEM);
		} else if (s.equalsIgnoreCase(SeismicFormat.SEGY_REVISION_NUMBER)) {
			return new Integer(SeismicFormat.HEADER_SEGY_REVISION_NUMBER);
		} else if (s.equalsIgnoreCase(SeismicFormat.FIXED_LENGTH_FLAG)) {
			return new Integer(SeismicFormat.HEADER_FIXED_LENGTH_FLAG);
		} else if (s.equalsIgnoreCase(SeismicFormat.MAXIMUM_SAMPLES_PER_TRACE)) {
			return new Integer(SeismicFormat.HEADER_MAXIMUM_SAMPLES_PER_TRACE);
		} else if (s.equalsIgnoreCase(SeismicFormat.NUMBER_OF_EXTENSIONS)) {
			return new Integer(SeismicFormat.HEADER_NUMBER_OF_EXTENSIONS);
		} else if (s.equalsIgnoreCase(SeismicFormat.TRACE_SEQUENCE_NUMBER)) {
			return new Integer(SeismicFormat.HEADER_TRACE_SEQUENCE_NUMBER);
		} else if (s.equalsIgnoreCase(SeismicFormat.TRACE_NUMBER_ONE)) {
			return new Integer(SeismicFormat.HEADER_TRACE_NUMBER_ONE);
		} else if (s.equalsIgnoreCase(SeismicFormat.FIELD_RECORD)) {
			return new Integer(SeismicFormat.HEADER_FIELD_RECORD);
		} else if (s.equalsIgnoreCase(SeismicFormat.FIELD_TRACE)) {
			return new Integer(SeismicFormat.HEADER_FIELD_TRACE);
		} else if (s.equalsIgnoreCase(SeismicFormat.SHOTPOINT_ID)) {
			return new Integer(SeismicFormat.HEADER_SHOTPOINT_ID);
		} else if (s.equalsIgnoreCase(SeismicFormat.CDP_NUMBER)) {
			return new Integer(SeismicFormat.HEADER_CDP_NUMBER);
		} else if (s.equalsIgnoreCase(SeismicFormat.CDP_TRACE)) {
			return new Integer(SeismicFormat.HEADER_CDP_TRACE);
		} else if (s.equalsIgnoreCase(SeismicFormat.TRACE_ID)) {
			return new Integer(SeismicFormat.HEADER_TRACE_ID);
		} else if (s.equalsIgnoreCase(SeismicFormat.REC_ELEVATION)) {
			return new Integer(SeismicFormat.HEADER_REC_ELEVATION);
		} else if (s.equalsIgnoreCase(SeismicFormat.COORDINATE_SCALER)) {
			return new Integer(SeismicFormat.HEADER_COORDINATE_SCALER);
		} else if (s.equalsIgnoreCase(SeismicFormat.SOURCE_X_LOC)) {
			return new Integer(SeismicFormat.HEADER_SOURCE_X_LOC);
		} else if (s.equalsIgnoreCase(SeismicFormat.SOURCE_Y_LOC)) {
			return new Integer(SeismicFormat.HEADER_SOURCE_Y_LOC);
		} else if (s.equalsIgnoreCase(SeismicFormat.RECEIVER_X_LOC)) {
			return new Integer(SeismicFormat.HEADER_RECEIVER_X_LOC);
		} else if (s.equalsIgnoreCase(SeismicFormat.RECEIVER_Y_LOC)) {
			return new Integer(SeismicFormat.HEADER_RECEIVER_Y_LOC);
		} else if (s.equalsIgnoreCase(SeismicFormat.START_TIME)) {
			return new Integer(SeismicFormat.HEADER_START_TIME);
		} else if (s.equalsIgnoreCase(SeismicFormat.SAMPLES_IN_TRACE)) {
			return new Integer(SeismicFormat.HEADER_SAMPLES_IN_TRACE);
		} else if (s.equalsIgnoreCase(SeismicFormat.SAMPLE_RATE)) {
			return new Integer(SeismicFormat.HEADER_SAMPLE_RATE);
		} else if (s.equalsIgnoreCase(SeismicFormat.CDP_X_LOC)) {
			return new Integer(SeismicFormat.HEADER_CDP_X_LOC);
		} else if (s.equalsIgnoreCase(SeismicFormat.CDP_Y_LOC)) {
			return new Integer(SeismicFormat.HEADER_CDP_Y_LOC);
		} else if (s.equalsIgnoreCase(SeismicFormat.INLINE_NUMBER)) {
			return new Integer(SeismicFormat.HEADER_INLINE_NUMBER);
		} else if (s.equalsIgnoreCase(SeismicFormat.XLINE_NUMBER)) {
			return new Integer(SeismicFormat.HEADER_XLINE_NUMBER);
		} else if (s.equalsIgnoreCase(SeismicFormat.SHOTPOINT_NUMBER)) {
			return new Integer(SeismicFormat.HEADER_SHOTPOINT_NUMBER);
		}
		return null;
	}

	public static Integer format(String s) {
		if (s.equalsIgnoreCase("INT8")) {
			return new Integer(SeismicConstants.DATA_FORMAT_BYTE);
		} else if (s.equalsIgnoreCase("INT16")) {
			return new Integer(SeismicConstants.DATA_FORMAT_SHORT);
		} else if (s.equalsIgnoreCase("INT32")) {
			return new Integer(SeismicConstants.DATA_FORMAT_INT);
		} else if (s.equalsIgnoreCase("FLOAT32")) {
			return new Integer(SeismicConstants.DATA_FORMAT_FLOAT);
		} else if (s.equalsIgnoreCase("IBM_FLOAT32")) {
			return new Integer(SeismicConstants.DATA_FORMAT_IBM_FLOAT);
		} else if (s.equalsIgnoreCase("FLOAT64")) {
			return new Integer(SeismicConstants.DATA_FORMAT_DOUBLE);
		} else if (s.equalsIgnoreCase("INT4")) {
			return new Integer(SeismicConstants.DATA_FORMAT_4BIT);
		} else if (s.equalsIgnoreCase("UINT8")) {
			return new Integer(SeismicConstants.DATA_FORMAT_UNSIGNEDBYTE);
		} else if (s.equalsIgnoreCase("UINT16")) {
			return new Integer(SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT);
		} else if (s.equalsIgnoreCase("UINT32")) {
			return new Integer(SeismicConstants.DATA_FORMAT_UNSIGNEDINT);
		}
		return null;
	}

	public static String format(int fmt) {
		if (fmt == SeismicConstants.DATA_FORMAT_BYTE) {
			return "INT8";
		} else if (fmt == SeismicConstants.DATA_FORMAT_SHORT) {
			return "INT16";
		} else if (fmt == SeismicConstants.DATA_FORMAT_INT) {
			return "INT32";
		} else if (fmt == SeismicConstants.DATA_FORMAT_FLOAT) {
			return "FLOAT32";
		} else if (fmt == SeismicConstants.DATA_FORMAT_IBM_FLOAT) {
			return "IBM_FLOAT32";
		} else if (fmt == SeismicConstants.DATA_FORMAT_DOUBLE) {
			return "FLOAT64";
		} else if (fmt == SeismicConstants.DATA_FORMAT_4BIT) {
			return "INT4";
		} else if (fmt == SeismicConstants.DATA_FORMAT_UNSIGNEDBYTE) {
			return "UINT8";
		} else if (fmt == SeismicConstants.DATA_FORMAT_UNSIGNEDSHORT) {
			return "UINT16";
		} else if (fmt == SeismicConstants.DATA_FORMAT_UNSIGNEDINT) {
			return "UINT32";
		} else {
			return null;
		}
	}
}
