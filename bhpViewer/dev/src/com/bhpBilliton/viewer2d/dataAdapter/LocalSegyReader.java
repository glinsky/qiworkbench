/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter;


import java.util.*;
import java.io.*;

import javax.swing.JOptionPane;

import com.bhpBilliton.viewer2d.*;
import com.bhpBilliton.viewer2d.dataAdapter.segy.DynamicSegyFormat;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.gwsys.gw2d.util.GWUtilities;
import com.gwsys.seismic.indexing.*;
import com.gwsys.seismic.indexing.internal.DataSourceProfile;
import com.gwsys.seismic.reader.IndexedSegyReader;
import com.gwsys.seismic.io.SeismicMetaData;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.indexing.SegyBinaryEntry;
import com.gwsys.seismic.indexing.HeaderFieldEntry;
import com.gwsys.seismic.indexing.SegyHeaderFormat;
import com.gwsys.seismic.reader.SeismicFormat;
import com.gwsys.seismic.indexing.SeismicConstants;
import com.gwsys.seismic.reader.SeismicReader;
import com.gwsys.seismic.reader.StandardSegyFormat;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;
import com.gwsys.seismic.util.SeismicDataUtil;

import com.gwsys.gw2d.util.*;
import java.io.InputStream;

/**
 * <p>Title:It is local segy reader</p>
 * <p>Description: It reads segy indexed files directly.</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: Int Inc.</p>
 * @author Int
 * @version 1.0
 */

public class LocalSegyReader extends com.bhpBilliton.viewer2d.data.BhpDataReader {

  private SeismicReader _reader = null;
  private String _urlParameter;
  private String _queryParameter;
  private boolean _reversed;
  private boolean _reversedDirection;

  private double _timeStart  = 0;
  private double _timeEnd    = 0;

  private SeismicMetaData _metaData = null;
  private Bound2D _modelLimits = null;
  private int   _startSample = 0;
  private String _sampleName = null;
  private double _startValue = 0;

  /**
   * Creates reader
   * @param layer
   * @param par1
   * @param par2
   */
  public LocalSegyReader(BhpLayer layer, String par1, String par2, String formatName) throws java.lang.Exception {

        SeismicFormat segyFormat = null;

        boolean indexedFile = true;

        String transposedKey = null;
        String transposedName = null;

        SegyDataset dataset = null;
        String filename = layer.getPathlist();

        if (DataSourceFactory.isSegy(filename)) {

            String[] files = { filename };
            String[] orders = { "BIG_ENDIAN" };

            try {
                dataset = SegyDatasetFactory.getDefaultFactory().createDataset("", files, orders);
                _startValue = dataset.getStartValue();
            } catch (java.lang.Exception e) {
                JOptionPane.showMessageDialog(layer, "Cannot read segy file " + filename, "Read Error", JOptionPane.ERROR_MESSAGE);
                throw e;
            }

            segyFormat = new StandardSegyFormat();
            indexedFile = false;
        } else {

            try {
                dataset = SegyDatasetFactory.getDefaultFactory().createDataset(filename);
            } catch (java.lang.Exception e) {
                ErrorDialog.showErrorDialog(layer.getViewer(), QIWConstants.ERROR_DIALOG,
                                            Thread.currentThread().getStackTrace(),
                                            "Cannot read the indexed segy file " + filename,
                                            new String[] {"Possible data corruption"},
                                            new String[] {"Check data",
                                                          "If data is OK, contact workbench support"});
            }

            if (dataset.getSegyFormatFile() != null) {

                formatName = dataset.getSegyFormatFile();

                if (formatName != null) {
                    InputStream stream = null;
                    try {

                        DynamicSegyFormat dynamicSegyFormat = new DynamicSegyFormat();

                        File file = new File(formatName);

                        stream = new FileInputStream(file);

                        //load segy format from XML
                        dynamicSegyFormat.load(stream);
                        stream.close();
                        segyFormat = dynamicSegyFormat;

                    } catch (java.io.IOException ex) {
                        ErrorDialog.showInternalErrorDialog(layer.getViewer(), Thread.currentThread().getStackTrace(),
                                                            "Cannot find XML format file " + formatName);
                    } finally {
                        try {
                            if (stream != null)
                                stream.close();
                        } catch (IOException e1) {
                            ErrorDialog.showInternalErrorDialog(layer.getViewer(), Thread.currentThread().getStackTrace(),
                                                    "Could not close stream [segyreader].");
                        }
                    }
                } else {
                    segyFormat = new StandardSegyFormat();
                }
            }
        }

    if( segyFormat == null ) {
        ErrorDialog.showInternalErrorDialog(layer.getViewer(), Thread.currentThread().getStackTrace(),
                                            "Cannot define segy format file");
    }
    _startValue = dataset.getStartValue();
    _sampleName = SeismicDataUtil.getSampleKeyName(dataset);

    transposedKey = dataset.getTransposedKey();
    transposedName = dataset.getTransposedName();

    dataset = null;

    boolean isTraceIdKey = false;

    Vector formats = segyFormat.getTraceHeaderFormats();
    SegyHeaderFormat headerFormat = (SegyHeaderFormat) (formats.firstElement());
    Vector fields = headerFormat.getHeaderFields();

    HeaderFieldEntry headerField;

    for (int i = 0; i < fields.size(); i++) {
      headerField = (HeaderFieldEntry) (fields.elementAt(i));
      if (headerField.getName().equals(DataSourceProfile.TRACE_ID_KEY))
        isTraceIdKey = true;
    }

    if (!indexedFile && !isTraceIdKey) {
      headerFormat.addFieldDescription(new SegyBinaryEntry(0,
          SeismicConstants.DATA_FORMAT_DOUBLE,
          SeismicFormat.HEADER_USER_DEFINED + 100,
          DataSourceProfile.TRACE_ID_KEY, HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false));
    }
    // transpose keys
    SeismicDataUtil.transposeKeys(segyFormat, transposedKey, transposedName);

    _reversed = layer.getReversedPolarity();
    _reversedDirection = layer.getReversedSampleOrder();

    _urlParameter = par1;
    _queryParameter = par2;


    GWUtilities.info(par2);

    if( SeismicDataUtil.isPathData( par2 ) )
    {
      if( !indexedFile )
        throw new Exception( "System does not support selection by path for standard segy files!!!" );

      DataSourceKeys keys = SeismicDataUtil.parseKeys( par2 );
      if (keys != null) {
          DataSourceKey tmpSampleKey = keys.getItemByName( _sampleName );
          if (tmpSampleKey != null) {
              _timeStart = Double.valueOf( tmpSampleKey.getMinValue() ).doubleValue();
              _timeEnd   = Double.valueOf( tmpSampleKey.getMaxValue() ).doubleValue();
          }
      }
      DataSourcePath path = SeismicDataUtil.parsePath(keys );

      try {
        _reader = new IndexedSegyReader(layer.getPathlist(), segyFormat, path );
      }
      catch (java.lang.Exception ex) {
          ErrorDialog.showInternalErrorDialog(layer.getViewer(), Thread.currentThread().getStackTrace(),
                                              "Error creating IndexedSegyReader");
      }
    }
    else
    {
      DataSourceKeys keys = SeismicDataUtil.parseKeys( par2 );
      DataSourceKeys keyWithNoSample = new DataSourceKeys();

      if (keys != null) {
          DataSourceKey tmpKey = null;
          for (int i=0; i<keys.getCount(); i++) {
              tmpKey = keys.getItemByIndex(i);
              if (tmpKey.getName().equals(_sampleName)) {
                  _timeStart = Double.valueOf( tmpKey.getMinValue() ).doubleValue();
                  _timeEnd   = Double.valueOf( tmpKey.getMaxValue() ).doubleValue();
              }
              else keyWithNoSample.addItem( tmpKey );
          }
          SeismicDataUtil.checkFormatKeys(keys, segyFormat);
      }

      try {
        _reader = new IndexedSegyReader(layer.getPathlist(), segyFormat,
                                        keyWithNoSample, indexedFile);

      }
      catch (java.lang.Exception ex) {
          ErrorDialog.showInternalErrorDialog(layer.getViewer(), Thread.currentThread().getStackTrace(),
                                              "Error creating IndexedSegyReader");
      }

    }
  }

    public boolean isThreaded() { return false; }

    public TraceHeader getTraceMetaData(int traceId) {

        if (traceId < 0) return null;

        return _reader.getTraceMetaData( traceId );
    }
    public SeismicMetaData getMetaData() {
        if (_metaData == null) initMetaDataAndModelLimits();
        return _metaData;
    }
    public Bound2D getModelLimits() {
        if (_modelLimits == null) initMetaDataAndModelLimits();
        return _modelLimits;
    }

    public SeismicFormat getDataFormat() {
        return _reader.getDataFormat();
    }

    private void initMetaDataAndModelLimits() {

        _metaData = new SeismicMetaData( _reader.getMetaData() );

        _metaData.setStartValue( _timeStart );

        double num_samples = ( _timeEnd > _timeStart ) ? (_timeEnd -_timeStart) / _metaData.getSampleRate()
                                 : _metaData.getSamplesPerTrace();

        _startSample = (int)( (_timeStart -_startValue) / _metaData.getSampleRate());


        if( _startSample < 0 ) _startSample = 0;

        if( _startSample+num_samples > _metaData.getSamplesPerTrace() ) {
            num_samples = _metaData.getSamplesPerTrace()-_startSample;
            if( num_samples < 0 ) num_samples = 0;
        }
        _metaData.setSamplesPerTrace( (int)num_samples );

        _modelLimits = new Bound2D(0, 0,
                                  _metaData.getNumberOfTraces()-1,
                                  _metaData.getSamplesPerTrace()*
                                  _metaData.getSampleRate());
      }

      public boolean process(int traceId, NumberRange sampleRange, TraceData traceDataOut) {

        if (traceId < 0) return false;

        int sampleRangeMax = (sampleRange != null) ? sampleRange.getMax().intValue() : _reader.getMetaData().getSamplesPerTrace()-1;
        int sampleRangeMin = (sampleRange != null) ? sampleRange.getMin().intValue() : 0;

        int max = sampleRangeMax +_startSample;
        if( max > _reader.getMetaData().getSamplesPerTrace() ) max = _reader.getMetaData().getSamplesPerTrace()-1;
        int min = sampleRangeMin+_startSample;
        if( min > max ) min = max;

        NumberRange newRange = new NumberRange( min , max );

        _reader.process( traceId, newRange, traceDataOut );

        int numSamples = traceDataOut.getNumAppliedSamples();

        float[] dataArray = traceDataOut.getSamples();

        if ( _reversed ) {
            for (int i=0; i < numSamples; i++)
                dataArray[ i ] = -dataArray[ i ];
        }

        if ( _reversedDirection && numSamples > 1) {
          float temp;
          int mean = (int)(numSamples-1)/2;
          for( int i=0; i <= mean; i++ ) {
              temp = dataArray[ i ];
              dataArray[ i ] = dataArray[ numSamples - i - 1];
              dataArray[ numSamples - i - 1] = temp;
          }
        }
        return true;
    }

}
