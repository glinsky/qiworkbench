/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter;

import java.io.*;

import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.data.*;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.indexing.IndexedSegyException;

/**
 * <p>Title: Factory for creating data sources</p>
 * <p>Description: Factory creates data sources</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class DataSourceFactory {
    /**
     * Determine whether the implementation factory can use this data source
     * like standard segy
     * @param filename source source to test for acceptance
     * @return true if it is standard segy; false otherwise
     */
    public static boolean isSegy(String filename) {
        return  (filename.indexOf(".sgy") != -1) ||
                (filename.indexOf(".SGY") != -1) ||
                (filename.indexOf(".segy") != -1) ||
                (filename.indexOf(".SEGY") != -1) ||
                (filename.indexOf(".dat") != -1) ||
                (filename.indexOf(".DAT") != -1);
    }

    /**
     *  Determine whether the implementation factory accepts the extension as a valid
     *  data source.
     *
     * @param  source source to test for acceptance
     * @return    true if acceptable; false otherwise
     */
    public static boolean accepts( String source ) {
        String extension = source.substring( source.indexOf(".") );
        return ( extension.indexOf(".xgy") != -1 )
                || ( extension.indexOf(".sgy") != -1 )
                || ( extension.indexOf(".SGY") != -1 )
                || ( extension.indexOf(".SEGY") != -1 )
                || ( extension.indexOf(".dat") != -1 )
                || ( extension.indexOf(".DAT") != -1 )
                || ( extension.indexOf(".segy") != -1 );
            // QIW Disable references to XMLHorizon data
                /*|| ( extension.indexOf(".XHZ") != -1 )
                ||( extension.indexOf(".xhz") != -1 );*/
    }

    /**
     * Create and init local data source
     * @param filename - file name
     * @return - created data source
     */
    public static GeneralDataSource createDataSource(BhpViewerBase viewer, String filename)
            throws IndexedSegyException {

        AbstractDataSource source = null;
        try {
            if( filename.indexOf(".xgy") != -1 )
                    source = new SegyDataSource( filename );
            else if(isSegy(filename))
                source = new SegyDataSource( filename );
        } catch (Exception e2) {
            throw new IndexedSegyException(e2.getMessage());
        }

        if( source != null ) {
            InputStream stream=null;
            try {
                stream = new FileInputStream( filename );
                source.initWithStream( stream );
                stream.close();
            }
            catch( java.io.FileNotFoundException e ) {
                ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                    "createDataSource: File not found");
                return null;
            }
            catch( java.io.IOException e ) {
                ErrorDialog.showErrorDialog(viewer, QIWConstants.ERROR_DIALOG,
                                        Thread.currentThread().getStackTrace(),
                                        "createDataSource: IO Error",
                                        new String [] {"Permissions error","Empty file"},
                                        new String [] {"Fix permissions",
                                                       "If OK, contact workbench support"});
                return null;
            }
            finally {
                try {
                    if (stream != null)
                        stream.close();
                } catch (IOException e1) {
                    ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                        "Could not close stream");
                }
            }
        }
        return source;
    }

  /**
   * Create and init remote data source
   * @param filename - file name
   * @return - created data source
   */
   public static GeneralDataSource createRemoteDataSource( String filename )
          throws com.gwsys.seismic.indexing.IndexedSegyException {

        AbstractDataSource source = null;

        try {
        if ( filename.indexOf(".xgy") != -1 )
            source = new RemoteSegyDataSource( filename );
        else if (isSegy(filename))
            source = new RemoteSegyDataSource( filename );
        } catch (java.lang.Exception ex) {
            source = null;
        }

        return source;
    }
}
