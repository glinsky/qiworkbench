/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter;

import java.awt.Container;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import com.bhpBilliton.viewer2d.Bhp2DviewerConstants;
import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpSeismicLayer;
import com.bhpBilliton.viewer2d.BhpSeismicTableModel;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.BhpWindow;
import com.bhpBilliton.viewer2d.data.GeneralDataChooser;
import com.bhpBilliton.viewer2d.data.GeneralDataSource;
import com.bhpBilliton.viewer2d.ui.OpenDataPanel;
import com.bhpBilliton.viewer2d.ui.OpenEventPanel;
import com.bhpBilliton.viewer2d.ui.OpenGeneralPanel;
import com.bhpBilliton.viewer2d.util.ErrorDialog;

import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Choose a local/remote dataset.
 *
 * @author Gil Hansen
 * @version 1.1
 */
public abstract class SeismicChooser implements GeneralDataChooser {
    private static final Logger logger = Logger.getLogger(SeismicChooser.class.getName());

    private OpenDataPanel _opanel;
    private Container _parent;
    private String _filename;

    /**
     *  Create seismic chooser
     *
     * @param  parent  Container, i.e., the viewer
     * @param  p       data panel
     */
    public SeismicChooser(Container parent, OpenDataPanel p) {
      _opanel = p;
      _parent = parent;
    }

    public void openSelectedData(String filename) {
        _filename = filename;
        openSelectedData();
    }

    /**
     * Open selected dataset.
     */
    public void openSelectedData() {
        if (_parent == null || !(_parent instanceof BhpViewerBase)) {
            return;
        }

        BhpViewerBase theViewer = (BhpViewerBase)_parent;
        if (theViewer == null) {
            ErrorDialog.showInternalErrorDialog(theViewer, Thread.currentThread().getStackTrace(), "SeismicChooser: Cannot find BhpViewer instance");
            return;
        }

        if (_filename != null) {
            try {
                openDataset(theViewer);
            } catch ( Exception ex ) {
                ErrorDialog.showErrorDialog(theViewer, QIWConstants.ERROR_DIALOG, Thread.currentThread().getStackTrace(), "Error opening file: " + _filename, new String[] {ex.getMessage()}, new String[] {"Verify file and file permissions are OK", "If both are OK, contact workbench support"});
            }
        } else {
            ErrorDialog.showInternalErrorDialog(theViewer, Thread.currentThread().getStackTrace(), "No data file selected");
        }
    }

    protected Container getParent() { return _parent; }
    protected String getFilename() { return _filename; }
    protected void setFilename(String filename) { _filename = filename; }

    IMessagingManager messagingMgr;

    /**
     * Render the selected local/remote dataset
     *
     * @param theViewer The viewer's container.
     */
    private void openDataset(BhpViewerBase theViewer) {
        messagingMgr = theViewer.getAgent().getMessagingManager();

        boolean isLocal = messagingMgr.getLocationPref().equals(QIWConstants.LOCAL_SERVICE_PREF);

        if (isLocal) {
            openLocalData(theViewer);
        } else {
            openRemoteData(theViewer);
        }
    }

    private void openLocalData(BhpViewerBase theViewer) {
        GeneralDataSource source = null;
        try {
            source = DataSourceFactory.createDataSource(theViewer, _filename);
        } catch (com.gwsys.seismic.indexing.IndexedSegyException ex) {
            source = null;
            OpenGeneralPanel ogpanel = _opanel.getDataPanel();
            JOptionPane.showMessageDialog(ogpanel, ex.getMessage(), "Read Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (java.lang.Exception e) {
            source = null;
        }

        if (source != null) {
            if (source.getDataType() == BhpViewer.BHP_DATA_TYPE_EVENT) {
                BhpWindow theWindow = (BhpWindow) theViewer.getDesktop().getSelectedFrame();
                if (theWindow == null) {
                    ErrorDialog.showInternalErrorDialog(theViewer, Thread.currentThread().getStackTrace(), "SeismicChooser Exception: window not found");
                    return;
                }
                BhpLayer theLayer = theWindow.getFeaturedLayer();

                if (theLayer == null || !(theLayer instanceof BhpSeismicLayer)) {
                    ErrorDialog.showInternalErrorDialog(theViewer, Thread.currentThread().getStackTrace(), "SeismicChooser Exception: seismic layer not selected first");
                    return;
                }
                _opanel.describeData( source, false );

                String properties = source.getProperties();
                String[] selectedEvents = properties.split( " " );
                OpenGeneralPanel ogpanel = _opanel.getDataPanel();

                BhpSeismicTableModel parameter = ogpanel.getTableModelForNewLayer();

                for (int i = 0; i < selectedEvents.length; i++) {
                    theWindow.createXVLayer( theViewer,
                    GeneralDataSource.DATA_SOURCE_XMLHRZ,
                    source.getDataName(), source.getDataPath(),
                    selectedEvents[i], false, parameter, theLayer,
                    BhpViewer.BHP_DATA_TYPE_EVENT,
                    ((OpenEventPanel) ogpanel).getSampleEventShape() );
                }
                Container parent = _opanel.getParent();
                while (parent != null && !(parent instanceof JDialog)) {
                    parent = parent.getParent();
                }

                if ( parent != null ) {
                    parent.setVisible( false );
                }
            } else {
                _opanel.describeData( source );
            }
        }
    }

    private void openRemoteData(BhpViewerBase theViewer) {
        IQiWorkbenchMsg response = null;

        // form parameters for remote SEGY read command
        ArrayList<String> params = new ArrayList<String>();
        // [0] IO preference
        params.add(QIWConstants.REMOTE_PREF);
        // [1] read request
        params.add("readReq=readInfo");
        // [2] path of SEGY file
        params.add("filename="+_filename);

        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.READ_SEGY_DATA_CMD,
                 QIWConstants.ARRAYLIST_TYPE, params, true);

        // wait for the response.
        int k = 0;
        while (response == null) {
            response = messagingMgr.getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
            k++;
            if (k >= 4) break;
        }

        try {
            if (MsgUtils.isResponseAbnormal(response)) {
              //TODO: Warning dialog: notify user could not read SEGY data
              logger.finest("SEGY read data error:"+(String)MsgUtils.getMsgContent(response));
              return;
            }
        } catch (NullPointerException npe) {
            StackTraceElement[] stack = npe.getStackTrace();
            StringBuffer sbuf = new StringBuffer();
            for (int i=0; i<stack.length; i++)
                sbuf.append(stack[i].toString() + "\n");
            logger.severe("SYSTEM ERROR: Timed out waiting for response to read SEGY data. stack trace: \n"+sbuf.toString());
            return;
        }

        GeneralDataSource source = null;
        try {
            source = DataSourceFactory.createRemoteDataSource(_filename);
        } catch (com.gwsys.seismic.indexing.IndexedSegyException ex) {
            source = null;
            OpenGeneralPanel ogpanel = _opanel.getDataPanel();
            JOptionPane.showMessageDialog(ogpanel, ex.getMessage(), "Remote Read Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (java.lang.Exception e) {
            source = null;
        }

        if (source != null) {
            ((AbstractDataSource) source).initWithByteBuffer((ByteBuffer)response.getContent());

            if (source.getDataType() == BhpViewer.BHP_DATA_TYPE_EVENT) {
                BhpWindow theWindow = (BhpWindow) theViewer.getDesktop().getSelectedFrame();
                if (theWindow == null) {
                    ErrorDialog.showInternalErrorDialog(theViewer, Thread.currentThread().getStackTrace(), "SeismicChooser Exception: window not found");
                    return;
                }
                BhpLayer theLayer = theWindow.getFeaturedLayer();

                if (theLayer == null || !(theLayer instanceof BhpSeismicLayer)) {
                    ErrorDialog.showInternalErrorDialog(theViewer, Thread.currentThread().getStackTrace(), "SeismicChooser Exception: seismic layer not selected first");
                    return;
                }
                _opanel.describeData( source, false );

                String properties = source.getProperties();
                String[] selectedEvents = properties.split( " " );
                OpenGeneralPanel ogpanel = _opanel.getDataPanel();

                BhpSeismicTableModel parameter = ogpanel.getTableModelForNewLayer();

                for (int i = 0; i < selectedEvents.length; i++) {
                    theWindow.createXVLayer( theViewer,
                    GeneralDataSource.DATA_SOURCE_XMLHRZ,
                    source.getDataName(), source.getDataPath(),
                    selectedEvents[i], false, parameter, theLayer,
                    BhpViewer.BHP_DATA_TYPE_EVENT,
                    ((OpenEventPanel) ogpanel).getSampleEventShape() );
                }
                Container parent = _opanel.getParent();
                while (parent != null && !(parent instanceof JDialog)) {
                    parent = parent.getParent();
                }
                if ( parent != null ) {
                    parent.setVisible( false );
                }
            } else {
                _opanel.describeData( source );
            }

        }
    }

    public static GeneralDataSource createDataSource(BhpViewerBase viewer, String fname, String pathlist, Integer dtype) {
        try {
            return DataSourceFactory.createDataSource(viewer, pathlist);
        }
        catch (Exception ex) {
            return null;
        }
    }
}
