/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter;


import java.io.*;

import com.bhpBilliton.viewer2d.*;
import com.bhpBilliton.viewer2d.data.*;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.indexing.*;
import com.gwsys.seismic.io.SeismicMetaData;
import com.gwsys.seismic.util.SeismicDataUtil;
/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class represents the data source of INT segy indexed format. <br><br>
 *
 * @author
 * @version 1.0
 */

// Removed applet references
// TODO Implement Local/Remote file services
// TODO Remove servletReadURL

public class SegyDataSource extends AbstractDataSource {

    /**
     * Creates data source
     * @param filename name of the source
     * @param order  order
     * The valid values are:
     * <table border="1" cellpadding="3" cellspacing="0">
     * <tr>
     * <td>BhpViewer.BHP_DATA_ORDER_CROSSSECTION</td>
     * </tr>
     * <tr>
     * <td>BhpViewer.BHP_DATA_ORDER_MAPVIEW</td>
     * </tr>
     * </table>
     */
    public SegyDataSource( String filename, int order ) throws IndexedSegyException
    {
      super( order );

      SegyDataset dataset = null;

      if (DataSourceFactory.isSegy(filename)) {
        String[] files = {
            filename};
        String[] orders = {
            "BIG_ENDIAN"};

        try {
          dataset = SegyDatasetFactory.getDefaultFactory().createDataset("", files, orders);
        }
        catch (java.io.IOException ex) {
          throw new IndexedSegyException(ex.getMessage());
        }
      }
      else {

        try {
          dataset = SegyDatasetFactory.getDefaultFactory().createDataset( filename );
        }
        catch (java.io.IOException ex) {
          throw new IndexedSegyException(ex.getMessage()+"\n Please reindex or edit file "+filename);
        }

        String strName = dataset.getSegyFormatFile();
        if (strName != null) {
          File file = new File(strName);

          if (!file.exists()) {
            throw new IndexedSegyException("Cannot open XML file " + strName + "\n Please reindex or edit file "+filename );
          }
          if (!dataset.isSegyFilesExist()) {
            throw new IndexedSegyException(
                "Cannot find Segy files. Please reindex or edit file " + filename);
          }
        }

      }
      if (dataset.getProfileName() != null) {
        setDataName(dataset.getProfileName());
      }
      else {
        setDataName(filename);
      }

      setDataPath(filename);

      DataSourceKeys keys = dataset.getDatasourceKeys();
      DataSourceKey traclKey = null;
      String traceKeyAttr = null;

      for (int i = 0; i < keys.getCount(); ++i) {

        DataSourceKey key = keys.getItemByIndex(i);
        addHeader(key.getName(), getKeyAttributeString(key));
      }
      addSampleKey(dataset);
    }

    private void addSampleKey( SegyDataset dataset )
    {

        try {
            SeismicMetaData metaData = SeismicDataUtil.createMetaData( dataset, null );

            String sampleKey = SeismicDataUtil.getSampleKeyName( dataset );

            addHeader( sampleKey, SeismicDataUtil.getSamplesParameters( metaData ) );
        }
        catch( Exception ex ) {
            ex.printStackTrace();
/*
            ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                "SegyDataSource.addSampleKey Exception");
*/
        }
    }
    /**
    * Gets the name of the data set.
    */
   public String getDataSourceName()
   {
       return GeneralDataSource.DATA_SOURCE_ISEGY;
   }

    /**
     * Creates data source
     * @param filename - source
     */
    public SegyDataSource( String filename )  throws java.lang.Exception {
       this( filename, BhpViewer.BHP_DATA_TYPE_SEISMIC );
    }

    /**
       * Creates a <code>{@link BhpDataReader}</code> for this data source.
       * @param layer the <code>{@link BhpLayer}</code> that this reader will
       *        be associated with.
       * @param pmanager the property manager where program settings can be found.
       * @return the new data reader
       */
      public BhpDataReader createBhpDataReader(com.bhpBilliton.viewer2d.BhpLayer layer,
                                               com.bhpBilliton.viewer2d.BhpPropertyManager pmanager)
      {
          String servletReadURL;
          int chunkSz = 20;

          StringBuffer binIOPath = new StringBuffer();

              servletReadURL = "";

          binIOPath.append(" pathlist=");
          binIOPath.append(layer.getPathlist());
          binIOPath.append(" filename=");
          binIOPath.append(layer.getDataName());

          int dataType = layer.getDataType();
          // add missingData selection to bhpread command - missingdata=["ignore","fill"]
          if(dataType == BhpViewer.BHP_DATA_TYPE_SEISMIC) {
          binIOPath.append(" missingdata=");
          int md = layer.getMissingDataSelection();
          if(md == 0)
              binIOPath.append("ignore");
          else
              binIOPath.append("fill");
          }

          if (dataType == BhpViewer.BHP_DATA_TYPE_MODEL) {
              binIOPath.append(" properties=");
              binIOPath.append(layer.getPropertyName().trim());
          }

          binIOPath.append(layer.getParameter().getParameterString());

          String tempFilePath = pmanager.getProperty("temporaryDirectory");
          if (!tempFilePath.endsWith("/")) {
            tempFilePath = tempFilePath + "/";
          }

          String formatName;

          try
          {
              formatName = pmanager.getProperty( "IndexedSegyFormat" );
          }
          catch( Exception e ) // hack
          {
              formatName = "Standard";
          }
          try
          {
            return new LocalSegyReader(layer, servletReadURL, binIOPath.toString(), formatName);
          }
          catch( Exception e )
          {
            return null;
          }
      }

    private String getKeyAttributeString( DataSourceKey key ) {

        String keyAttr = key.getMinValue() + "-" + key.getMaxValue();
        if( key.getIncrement() != "" ) {
            keyAttr = keyAttr + "[" + key.getIncrement() + "]";
        }
        return keyAttr;

    }
}
