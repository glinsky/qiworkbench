/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter;

import com.bhpBilliton.viewer2d.BhpLayer;
import java.util.logging.Logger;
import java.util.Vector;
import com.bhpBilliton.viewer2d.BhpPropertyManager;
import com.bhpBilliton.viewer2d.data.BhpDataReader;
import com.bhpBilliton.viewer2d.dataAdapter.segy.DynamicSegyFormat;
import com.gwsys.seismic.indexing.SegyBinaryEntry;
import com.gwsys.seismic.indexing.HeaderFieldEntry;
import com.gwsys.seismic.indexing.SegyHeaderFormat;
import com.gwsys.seismic.indexing.SeismicConstants;
import com.gwsys.seismic.indexing.internal.DataSourceProfile;
import com.gwsys.seismic.io.SeismicMetaData;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.reader.SeismicFormat;
import com.gwsys.seismic.reader.StandardSegyFormat;
import com.gwsys.seismic.util.NumberRange;
import com.gwsys.gw2d.util.Bound2D;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Synthia Kong
 * @version 1.0
 */

public class RemoteSegyReader extends BhpDataReader {
    private static final Logger logger = Logger.getLogger(RemoteSegyReader.class.getName());

    private SeismicFormat _segyFormat;
    private Bound2D _modelLimits;
    private SeismicMetaData _metaData;
    private double _timeStart = 0;
    private double _timeEnd = 0;
    private int _startSample = 0;
    private double _startValue = 0;

    private boolean _transpose;
    private String _transposeKey;
    private String _transposeName;
    private String _sampleKey;

    private SeismicDataCache _cache;
    private SeismicDataLoader _thread;

    public RemoteSegyReader(BhpLayer layer, String urlString,
                            String par, int csz, String formatFile,
                            String sampleKey, double startValue, boolean trans,
                            String transKey, String transName, BhpPropertyManager pmanager) {
        _sampleKey = sampleKey;
        _startValue = startValue;
        _transpose = trans;
        _transposeKey = transKey;
        _transposeName = transName;
        _segyFormat = null;
        String formatFileName = "";
        String filename = layer.getPathlist();

        if (DataSourceFactory.isSegy(filename)) {
            _segyFormat = new StandardSegyFormat();
        } else {
            if (formatFile==null || formatFile.length()==0)
                _segyFormat = new StandardSegyFormat();
            else {
                try {
                    _segyFormat = loadDynamicFormat(urlString, formatFile);
                    formatFileName = formatFile;
                }
                catch (Exception ex) {
                    System.out.println("RemoteSegyReader.exception loading format " + formatFile);
                    _segyFormat = new StandardSegyFormat();
                }
            }
        }
        
        boolean isTraceIdKey = false;
        Vector formats = _segyFormat.getTraceHeaderFormats();
        SegyHeaderFormat headerFormat = (SegyHeaderFormat) (formats.firstElement());
        Vector fields = headerFormat.getHeaderFields();

        HeaderFieldEntry headerField;
        for (int i = 0; i < fields.size(); i++) {
            headerField = (HeaderFieldEntry) (fields.elementAt(i));
            if (headerField.getName().equals(DataSourceProfile.TRACE_ID_KEY))
                isTraceIdKey = true;
        }

        // for standard segy
        if (DataSourceFactory.isSegy(filename) && !isTraceIdKey) {
            headerFormat.addFieldDescription(new SegyBinaryEntry(0,
                    SeismicConstants.DATA_FORMAT_DOUBLE,
                    SeismicFormat.HEADER_USER_DEFINED + 100,
                    DataSourceProfile.TRACE_ID_KEY, HeaderFieldEntry.FIELD_TYPE_NOT_SPECIFIED, false));
        }

        // transpose keys
        IntSeismicUtil.transposeKeys(_segyFormat, _transposeKey, _transposeName);

        _cache = new SeismicDataCache(layer);
        _reversed = layer.getReversedPolarity();
        _reversedDirection = layer.getReversedSampleOrder();
        _thread = new SeismicDataLoader(_cache, urlString, par, formatFileName, _sampleKey, csz, pmanager);

        _metaData = null;
        _modelLimits = null;

        _thread.setPriority(Thread.MIN_PRIORITY);
        _thread.start();
    }

    public TraceHeader getTraceMetaData(int traceId) {
        if (traceId < 0) return null;
        if (_metaData != null) {
            if (traceId >= _metaData.getNumberOfTraces()) return null;
        }
        TraceObject traceObject = (TraceObject) _cache.get(new Integer(traceId));
        if (traceObject != null) {
            SegyHeaderFormat headerFormat = (SegyHeaderFormat)
                    (_segyFormat.getTraceHeaderFormats().elementAt(0));
            return traceObject.getTraceMetaData(
                    traceId, headerFormat.getHeaderFields());
        }
        return null;
    }
    public boolean process(int traceId, NumberRange sampleRange, TraceData traceDataOut) {

        if (traceId < 0) return false;
        if (_metaData != null) {
            if (traceId >= _metaData.getNumberOfTraces()) return false;
        }
        TraceObject traceObject = (TraceObject) _cache.get(new Integer(traceId));
        if (traceObject == null) return false;
        if (traceObject.getTraceData() == null) return false;

        int sampleRangeMax = (sampleRange != null) ? sampleRange.getMax().intValue() : _cache.getSamplesPerTrace()-1;
        int sampleRangeMin = (sampleRange != null) ? sampleRange.getMin().intValue() : 0;

        int max = sampleRangeMax +_startSample;
        if( max > _cache.getSamplesPerTrace() ) max = _cache.getSamplesPerTrace()-1;
        //if( max > getMetaData().getSamplesPerTrace() ) max = getMetaData().getSamplesPerTrace()-1;
        int min = sampleRangeMin+_startSample;
        if( min > max ) min = max;
        
        int numSamples = max - min;
        float[] dataArray = new float[numSamples];
        int startSample = min;
        if (_reversedDirection) {
            startSample = getMetaData().getSamplesPerTrace() - startSample - numSamples;
        }
        
        for (int i=0; i<numSamples; i++) {
            dataArray[i] = traceObject.getTraceData()[startSample+i];
        }
        
        if ( _reversed ) {
            for (int i=0; i < numSamples; i++)
                dataArray[ i ] = -dataArray[ i ];
        }

        if ( _reversedDirection && numSamples > 1) {
          float temp;
          int mean = (int)(numSamples-1)/2;
          for( int i=0; i <= mean; i++ ) {
              temp = dataArray[ i ];
              dataArray[ i ] = dataArray[ numSamples - i - 1];
              dataArray[ numSamples - i - 1] = temp;
          }
        }
        
        traceDataOut.setSamples(dataArray);
        traceDataOut.setNumAppliedSamples(numSamples);
        traceDataOut.setUniqueKey(new Integer(traceId));
        return true;
   }

    public boolean isThreaded() {
        return true;
    }

    public SeismicFormat getDataFormat() {
        return _segyFormat;
    }

    public SeismicMetaData getMetaData() {
        if (_metaData == null) initMetaDataAndModelLimits();
        return _metaData;
    }

    public Bound2D getModelLimits() {
        if (_modelLimits == null) initMetaDataAndModelLimits();
        return _modelLimits;
    }

    private void initMetaDataAndModelLimits() {
        _metaData = new SeismicMetaData();
        _cache.getMetaInitiated();
        if (_cache.getSampleRate() == 0) _metaData.setSampleRate(1.0);
        else _metaData.setSampleRate(_cache.getSampleRate());
        //_metaData.setSampleStart(_cache.getSampleStart());
        _metaData.setSampleStart(0.0);
        _metaData.setStartValue(_cache.getSampleStart());
        _metaData.setSampleUnits(_cache.getSampleUnit());

        _metaData.setSamplesPerTrace(_cache.getSamplesPerTrace());
        _metaData.setNumberOfTraces(_cache.getNumberOfTraces());
        _metaData.setDataStatistics(_cache.getMin(), _cache.getMax(),
                                    _cache.getAvg(), _cache.getRms());

        _timeStart = _cache.getSampleStart();
        _timeEnd = _timeStart + _cache.getSampleRate() * _cache.getSamplesPerTrace();
        _startSample = 0;
        try {
            _timeStart = _cache.getTimeStart();
            _timeEnd = _cache.getTimeEnd();
            _metaData.setStartValue( _timeStart );
            double num_samples = ( _timeEnd > _timeStart ) ?
                (_timeEnd -_timeStart) / _metaData.getSampleRate()
                : _metaData.getSamplesPerTrace();
            _startSample = (int)( (_timeStart-_startValue) / _metaData.getSampleRate());
            if( _startSample < 0 ) _startSample = 0;
            if( _startSample+num_samples > _metaData.getSamplesPerTrace() ) {
                num_samples = _metaData.getSamplesPerTrace()-_startSample;
                if( num_samples < 0 ) num_samples = 0;
            }
            _metaData.setSamplesPerTrace( (int)num_samples );
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        _modelLimits = new Bound2D(0, 0,
                                  _metaData.getNumberOfTraces()-1,
                                  _metaData.getSamplesPerTrace()*
                                  _metaData.getSampleRate());
    }

    private SeismicFormat loadDynamicFormat(String urlString, String fname)
            throws Exception{
        logger.finest("RemoteSegyReader::loadDynamicFormat: urlString="+urlString+", fname="+fname);
        DynamicSegyFormat dynamicSegyFormat = new DynamicSegyFormat();

        return dynamicSegyFormat;
    }
}