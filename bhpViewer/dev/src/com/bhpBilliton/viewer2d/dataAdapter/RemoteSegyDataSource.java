/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.dataAdapter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.bhpBilliton.viewer2d.*;
import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpPropertyManager;
import com.bhpBilliton.viewer2d.data.*;
import com.bhpBilliton.viewer2d.util.TextBuffer;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class represents the remote data source of INT segy indexed format
 *               and standard segy format. <br><br>
 *
 * @author Gil Hansen
 * @version 1.1
 */
public class RemoteSegyDataSource extends AbstractDataSource {
    private final static Logger logger = Logger.getLogger(RemoteSegyDataSource.class.getName());

    private String _formatFile = "";
    private String _sampleKey = "";
    private String _transposeKey = "";
    private String _transposeName = "";
    private double _startValue = 0;
    private boolean _transpose = false;
    private boolean _initialized = false;

    /**
     * Creates data source
     * @param filename - source
     */
    public RemoteSegyDataSource(String filename)  {
       this( filename, BhpViewer.BHP_DATA_TYPE_SEISMIC );
    }

    /**
     * Creates data source
     */
    public RemoteSegyDataSource(String filename, int type) {
        super( type );
        setDataName(filename);
    }

    /**
     * Initiates the data source with the stream.
     * @param ins the input stream where data information can be found.
     */
    public void initWithStream(InputStream ins) {
        _initialized = true;
        BufferedReader reader = new BufferedReader(new InputStreamReader(ins));
        _formatFile = "";
        while(true) {
            try {
                String line = reader.readLine();
                if (line == null) break;
                System.out.println("    " + line);

                if (line.indexOf("rinfodata :") != -1) {
                    this.setDataName(line.substring(11).trim());
                }
                else if (line.indexOf("rinfopath :") != -1) {
                    this.setDataPath(line.substring(11).trim());
                }
                else if (line.indexOf("rinfosamplekey :") != -1) {
                    _sampleKey = line.substring("rinfosamplekey :".length()).trim();
                }
                else if (line.indexOf("rinfostartvalue :") != -1) {
                    _startValue = Double.parseDouble(line.substring("rinfostartvalue :".length()).trim());
                }
                else if (line.indexOf("rinfotranspose :") != -1) {
                    String content = line.substring("rinfotranspose :".length()).trim();
                    String[] tokens = content.split(" ");
                    if (tokens != null && tokens.length==2) {
                        _transposeKey = tokens[0];
                        _transposeName = tokens[1];
                        _transpose = true;
                    }
                    else {
                        _transpose = false;
                    }
                }
                else if (line.indexOf("rinfokey :") != -1) {
                    String keyName = line.substring(10, line.indexOf(":", 10));
                    String keyAtt = line.substring(line.indexOf(":", 10)+1);
                    this.addHeader(keyName, keyAtt);
                }
                else if (line.indexOf("rinfoformat :") != -1) {
                    _formatFile = line.substring(13).trim();
                }
            }
            catch (Exception ex) {
                System.out.println("RemoteSegyDataSource.initWithStream Exception :");
                System.out.println("    " + ex.toString());
                break;
            }
        }
    }

    /**
     * Initiates the data source with the ByteBuffer.
     * @param bytebuf The ByteBuffer where data information can be found.
     */
    public void initWithByteBuffer(ByteBuffer bytebuf) {
        _initialized = true;
        TextBuffer txtbuf = new TextBuffer(bytebuf);
        _formatFile = "";

        while (true) {
            try {
                String line = txtbuf.readLine();

                if (line == null) break;

                if (line.indexOf("rinfodata :") != -1) {
                    this.setDataName(line.substring(11).trim());
                } else

                if (line.indexOf("rinfopath :") != -1) {
                    this.setDataPath(line.substring(11).trim());
                } else

                if (line.indexOf("rinfosamplekey :") != -1) {
                    _sampleKey = line.substring("rinfosamplekey :".length()).trim();
                } else

                if (line.indexOf("rinfostartvalue :") != -1) {
                    _startValue = Double.parseDouble(line.substring("rinfostartvalue :".length()).trim());
                } else

                if (line.indexOf("rinfotranspose :") != -1) {
                    String content = line.substring("rinfotranspose :".length()).trim();
                    String[] tokens = content.split(" ");
                    if (tokens != null && tokens.length==2) {
                        _transposeKey = tokens[0];
                        _transposeName = tokens[1];
                        _transpose = true;
                    } else {
                        _transpose = false;
                    }
                } else

                if (line.indexOf("rinfokey :") != -1) {
                    String keyName = line.substring(10, line.indexOf(":", 10));
                    String keyAtt = line.substring(line.indexOf(":", 10)+1);
                    this.addHeader(keyName, keyAtt);
                } else

                if (line.indexOf("rinfoformat :") != -1) {
                    _formatFile = line.substring(13).trim();
                }
            } catch (Exception ex) {
                System.out.println("RemoteSegyDataSource::initWithByteStream Exception :");
                System.out.println("    " + ex.toString());
                break;
            }
        }
    }

    /**
     * Gets the name of the data set.
     */
    public String getDataSourceName() {
        return GeneralDataSource.DATA_SOURCE_ISEGY;
    }

    IMessagingManager messagingMgr;

    /**
     * Creates a <code>{@link BhpDataReader}</code> for this data source.
     * @param layer the <code>{@link BhpLayer}</code> that this reader will
     *        be associated with.
     * @param pmanager the property manager where program settings can be found.
     * @return the new data reader
     */
    public BhpDataReader createBhpDataReader(BhpLayer layer, BhpPropertyManager pmanager) {
        messagingMgr = pmanager.getViewer().getViewerAgent().getMessagingManager();

        //DEPRECATED, but needed in signature of RemoteSegyReader since didn't need to change. gjh:11/7/06
        //TODO: remove urlString from RemoteSegyReader's signature
        String urlString = pmanager.getProperty("servletGeneralURL");

        if (!_initialized) {
            IQiWorkbenchMsg response = null;

            // form parameters for remote SEGY read command
            ArrayList<String> params = new ArrayList<String>();
            // [0] IO preference
            params.add(QIWConstants.REMOTE_PREF);
            // [1] read request
            params.add("readReq=readInfo");
            // [2] path of SEGY file
            params.add("filename="+layer.getPathlist());

            String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.READ_SEGY_DATA_CMD,
                     QIWConstants.ARRAYLIST_TYPE, params, true);

            // wait for the response.
            int k = 0;
            while (response == null) {
                response = messagingMgr.getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
                k++;
                if (k >= 4) break;
            }

            try {
                if (MsgUtils.isResponseAbnormal(response)) {
                  //TODO: Warning dialog: notify user could not read SEGY data
                  logger.finest("SEGY read data error:"+(String)MsgUtils.getMsgContent(response));
                  return null;
                }
            } catch (NullPointerException npe) {
                StackTraceElement[] stack = npe.getStackTrace();
                StringBuffer sbuf = new StringBuffer();
                for (int i=0; i<stack.length; i++)
                    sbuf.append(stack[i].toString() + "\n");
                logger.severe("SYSTEM ERROR: Timed out waiting for response to read SEGY data. stack trace: \n"+sbuf.toString());
                return null;
            }

            this.initWithByteBuffer((ByteBuffer)response.getContent());
        }

        int chunkSz = 100;
        //urlString = urlString + "servlet/intSeismicData";

        StringBuffer par2Buffer = new StringBuffer();
        par2Buffer.append(" pathlist=");
        par2Buffer.append(layer.getPathlist());
        par2Buffer.append(" filename=");
        par2Buffer.append(layer.getDataName());

        int dataType = layer.getDataType();
        if (dataType == BhpViewer.BHP_DATA_TYPE_MODEL) {
            par2Buffer.append(" properties=");
            par2Buffer.append(layer.getPropertyName().trim());
        }

        par2Buffer.append(layer.getParameter().getParameterString());

        try {
            return new RemoteSegyReader(layer, urlString, par2Buffer.toString(),
                                        chunkSz, _formatFile, _sampleKey, _startValue,
                                        _transpose, _transposeKey, _transposeName, pmanager);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
