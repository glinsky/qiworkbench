/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import com.bhpBilliton.viewer2d.data.BhpModelReaderInterface;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;
import com.gwsys.seismic.core.*;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is the customized data selector for model data.
 *               This data selector remembers a start depth and a end depth.
 *               These two values are used to determine the height of the
 *               display image. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpModelDataSelector extends TraceKeyRangeChooser {
    private float _startDepth;
    private float _endDepth;

    /**
     * Constructs a new instance.
     */
    public BhpModelDataSelector() {
        super();
        _startDepth = -1;
        _endDepth = -1;
    }

    /**
     * Sets the value of the start depth.
     */
    public void setStartDepth(float f) {
        _startDepth = f;
    }

    /**
     * Sets the value of the end depth.
     */
    public void setEndDepth(float f) {
        _endDepth = f;
    }

    /**
     * Returns the limits of seismic data in model space after selection and mapping. <br>
     * Overrides the implementation of its super class to take into
     * consideration the start and end depth.
     */
    public Bound2D getVirtualModelLimits() {
        Bound2D reRect = super.getVirtualModelLimits();
        if (_startDepth < 0 && _endDepth < 0) return reRect;

        //System.out.println("HHHHHHHHH11 " + reRect.getHeight());
        if (_startDepth < 0 || _endDepth < 0) {
            SeismicWorkflow pipeline = super.getWorkflow();
            BhpModelReaderInterface mreader = (BhpModelReaderInterface)
            pipeline.getDataLoader().getDataReader();
            if (_startDepth < 0) _startDepth = mreader.getMinDepth();
            else _endDepth = mreader.getMaxDepth();
        }

/*
        double suHeight = Math.abs(getStartSampleValue() - getEndSampleValue());
        if (suHeight == reRect.getHeight()) {
            reRect.height = Math.abs(_startDepth - _endDepth);
        }
*/
        reRect.height = Math.abs(_startDepth - _endDepth);
        //System.out.println("HHHHHHHHH22 " + reRect.getHeight());
        return reRect;
    }

    /**
     * Calculates the actual trace ID that should be read by the reader. <br>
     * This method overrides the implementation of its super class
     * to remove the calculation about sampleRange. Because for
     * vertically uniformly distributed model data,
     * the pipeline always needs to process the whole trace.
     * @param traceId the trace ID one base of which the actual trace ID is calculated.
     * @param sampleRange the range of required samples from this process.
     * @param traceDataOut the destination trace data for the trace process.
     * @return a flag indicates if the process is successful.
     */
    public boolean process(int virtualTraceId, NumberRange sampleRange, TraceData traceDataOut) {
        boolean success = false;
        int realTraceId = virtualToReal(virtualTraceId);
        if(realTraceId != -1) {
            success = getParentProcessor().process(realTraceId, sampleRange, traceDataOut);
            traceDataOut.setUniqueKey(new Integer(virtualTraceId));
        }
        return success;
    }
}
