/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;


import java.util.*;
import java.util.logging.Logger;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.text.NumberFormat;

/*
import javax.media.*;
import javax.media.format.*;
import javax.media.datasink.*;
import javax.media.protocol.*;
*/
//import bhpUtil.IconResource;

import com.bhpBilliton.viewer2d.Bhp2DviewerAgent;
import com.bhpBilliton.viewer2d.util.ErrorDialog;

import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Manage movie related settings
 * Copyright:    Copyright (c) 2001-2006
 * @author Synthia Kong
 * @author Gil Hansen
 * @version 1.1
 */
public class BhpMovieManager {
    // setup logging
    private static Logger logger = Logger.getLogger(BhpMovieManager.class.getName());

    private static final int MAX_ALLOWED = 50;

    private BhpViewer _viewer;
    private boolean _isRecording;
    private int _maxStep;
    private String _mediaFileName;
    private int _index;

    private NumberFormat _format;

    private List _images;
    private boolean _saveInMemory;
    private boolean _allFrames;
    private int _frameSpeed;

    //Get viewer's Messaging Manager
    IMessagingManager messagingMgr;

    /**
     * Constructs a new instance. <br>
     */
    public BhpMovieManager(BhpViewer v) {
        _viewer = v;
        _isRecording = true;
        _maxStep = 3;
        _index = 0;
        _mediaFileName = "";
        _format = NumberFormat.getIntegerInstance();
        _format.setMinimumIntegerDigits(4);
        _format.setGroupingUsed(false);

        _allFrames = true;
        _saveInMemory = true;
        _frameSpeed = 500;
        _images = new Vector();
        messagingMgr = ((BhpViewerBase)_viewer).getAgent().getMessagingManager();
    }

    public int getFrameSpeed() { return _frameSpeed; }
    public void setFrameSpeed(int s) { _frameSpeed = s; }

    public boolean isSaveInMemory() { return _saveInMemory; }
    public void setSaveInMemory(boolean m) { _saveInMemory = m; }

    public boolean isAllFrames() { return _allFrames; }
    public void setAllFrames(boolean f) { _allFrames = f; }

    public int getMaxStep  () { return _maxStep; }
    public void setMaxStep(int s) { _maxStep = s; }

    public void setMediaFileName(String s) { _mediaFileName = s; }
    public String getMediaFileName() { return _mediaFileName; }
/*
    public synchronized boolean isRecording() { return _isRecording; }
    public synchronized void setRecording(boolean r) {
        if (r == _isRecording) return;
        if (r) {
            _isRecording = r;
            _index = 0;
        }
        else {
            // stop recording
            boolean lastTime = true;
            captureScreen(lastTime);
            _isRecording = r;
        }
    }
*/
    public void createSnapshot() {
        captureScreen();
    }

    public synchronized int getImageNumber() { return _images.size(); }

    public synchronized Image getImageOfIndex(int index) {
        if (_images==null || _images.size()==0) {
            return null;
        }
        if (index < 0) {
            index = 0;
        }
        if (index >= _images.size()) {
            index = _images.size()-1;
        }
        return (Image)(_images.get(index));
    }

    public synchronized void updateImageList(int[] order) {
        Vector newImages = new Vector();
        for (int i=0; i<order.length; i++) {
            newImages.add(_images.get(order[i]));
        }
        _images.clear();
        _images = newImages;
    }

    public synchronized void saveVideoFile(String fname) throws Exception {

    }

    /**
     * Save images (in memory) to a file.
     *
     * @param fname Full path of the file to write images to
     */
    public synchronized void saveImageFiles(String fname) throws Exception {
        int idx = fname.lastIndexOf(".jpg");
        if(idx == -1)
            idx = fname.lastIndexOf(".JPG");

        if(idx > -1)
            fname = fname.substring(0,idx);

        for (int i=0; i<_images.size(); i++) {
            String pathname = fname+"_"+_format.format(i)+".jpg";

            BufferedImage bfImage = (BufferedImage)_images.get(i);
            // form parameters for binary IO command
            String locPref = messagingMgr.getLocationPref();
            //TEST: force to be remote
            //locPref = QIWConstants.REMOTE_PREF;
            ArrayList params = new ArrayList();
            params.add(locPref);    // [0] IO preference
            params.add(pathname);   // [1] file pathname
            params.add(QIWConstants.JPEG_FORMAT);   // [2] file's format
            params.add(bfImage);   // [3] binary data

            String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                     QIWConstants.BINARY_FILE_WRITE_CMD,
                     QIWConstants.ARRAYLIST_TYPE, params,true);
            // wait for the response.
            IQiWorkbenchMsg response = null;
            int ii = 0;
            while (response == null) {
                response = messagingMgr.getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
                ii++;
                if (ii >=4) break;
            }

            try {
                if (MsgUtils.isResponseAbnormal(response)) {
                  //TODO: Warning dialog: notify user could not write binary data to the file
                  logger.finest("Binary file write error:"+(String)MsgUtils.getMsgContent(response));
                  return;
                }
            } catch (NullPointerException npe) {
                logger.severe("SYSTEM ERROR: Timed out waiting for response to binary file write"+npe.getStackTrace());
                return;
            }
        }
    }

    private synchronized void addImages(BufferedImage bf) {
        if (_images.size() > MAX_ALLOWED)
            _images.remove(0);
        _images.add(bf);
    }

    private synchronized void captureScreen() {
        captureScreen(false);
    }

    /**
     * Capture all windows or just the active window and save as a JPEG
     * image to the specified media file (an implicit parameter).
     *
     * @param lastTime Always false; not used within method
     *
     */
    private synchronized void captureScreen(boolean lastTime) {
        if (!_isRecording) return;
        try {
            java.awt.Container imageComp = null;
            if (_allFrames) {
                imageComp = _viewer.getDesktop();
            }
            else {
               imageComp = _viewer.getDesktop().getSelectedFrame();
            }

            BufferedImage bfImage = new BufferedImage(
                      imageComp.getWidth(), imageComp.getHeight(),
                      BufferedImage.TYPE_INT_RGB);
            imageComp.update(bfImage.getGraphics());

            if (_saveInMemory) {
                addImages(bfImage);
            } else {
                if (_mediaFileName != null && _mediaFileName.length()>0) {
                    String indexString = _format.format(_index);
                    String pathname = _mediaFileName+"."+indexString+".jpg";
                    _index++;

                    // form parameters for binary IO command
                    String locPref = messagingMgr.getLocationPref();
                    //TEST: force to be remote
                    //locPref = QIWConstants.REMOTE_PREF;
                    ArrayList params = new ArrayList();
                    params.add(locPref);    // [0] IO preference
                    params.add(pathname);   // [1] file pathname
                    params.add(QIWConstants.JPEG_FORMAT);   // [2] file's format
                    params.add(bfImage);   // [3] binary data

                    String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.BINARY_FILE_WRITE_CMD,
                             QIWConstants.ARRAYLIST_TYPE, params, true);
                    // wait for the response.
                    IQiWorkbenchMsg response = null;
                    int i = 0;
                    while (response == null) {
                        response = messagingMgr.getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
                        i++;
                        if (i >=4) break;
                    }

                    try {
                        if (MsgUtils.isResponseAbnormal(response)) {
                          //TODO: Warning dialog: notify user could not write binary data to the file
                          logger.finest("Binary file write error:"+(String)MsgUtils.getMsgContent(response));
                          return;
                        }
                    } catch (NullPointerException npe) {
                        logger.severe("SYSTEM ERROR: Timed out waiting for response to binary file write"+npe.getStackTrace());
                        return;
                    }
                }
            }
        } catch (Exception ex) {
            //TODO Notify user of exception
            ErrorDialog.showInternalErrorDialog((BhpViewerBase)_viewer, Thread.currentThread().getStackTrace(),
                                                "BhpMovieManager.captureScreen exception: " + ex.getMessage());
        }
    }
    //public void setPickModeGUI(JButton btn) { _pickModeGUI = btn; }
/*
    public void changePickMode() {
        int newMode = ((_pickMode-1+1) % PICK_MODE_NUMBER) + 1;
        setPickMode(newMode);
    }
*/
/*
    public String getMediaLocatorString() {
        if (_mediaLocator == null) return "";
        return _mediaLocator.toExternalForm();
    }

    public void setMediaLocator(String m) {
        if (m == null) {
            System.out.println("BhpMovieManager.setMediaLocator failed:");
            System.out.println("    the media string cannot be null.");
            return;
        }
        m = m.trim();
        if (m.length() == 0) {
            System.out.println("BhpMovieManager.setMediaLocator failed:");
            System.out.println("    the media string cannot be empty.");
            return;
        }
        if (m.equals(getMediaLocatorString())) return;

        if (_isRecording) {
            System.out.println("BhpMovieManager.setMediaLocator failed:");
            System.out.println("    cannot change media locator during recording.");
        }
        _mediaLocator = new MediaLocator(m);
    }

    public synchronized void setRecording(boolean r) {
        if (r == _isRecording) return;
        if (r) {
            // start recording
            boolean successful = false;
            try {
                successful = setupVideoPipeline();
            }
            catch (Exception ex) {
                successful = false;
                System.out.println("BhpMovieManager.setRecording exception:");
                ex.printStackTrace();
            }
            _isRecording = successful;
        }
        else {
            // stop recording
            boolean lastTime = true;
            captureScreen(lastTime);
            _isRecording = r;
        }

        //if (_pickModeGUI != null) {
        //    _pickModeGUI.setIcon(_pickModeIcons[_pickMode-1]);
        //    String tip = PICK_MODE_TOOLTIP + PICK_MODE_TEXT[_pickMode-1];
        //    _pickModeGUI.setToolTipText(tip);
        //}
    }

    private void closeVideoPipeline() {
        _isRecording = false;
        if (_input != null) _input.disconnect();
        if (_processor != null) _processor.close();
        if (_filewriter != null) _filewriter.close();
    }

    public synchronized void captureScreen(boolean lastTime) {
        if (!_isRecording) return;
        if (lastTime) {
            closeVideoPipeline();
            return;
        }

        try {
            SwingUtilities.invokeAndWait(new MyCapturer(lastTime));
        }
        catch (Exception ex) {
            System.out.println("BhpMovieManager.captureScreen exception");
            ex.printStackTrace();
        }
    }

    private void internalCapture(boolean lastTime) {
        _viewer.getDesktop().paintImmediately(0, 0, _viewer.getWidth(), _viewer.getHeight());
        ((ScreenDataStream)(_input.getStreams()[0])).transferData(lastTime);
        if (lastTime) closeVideoPipeline();
    }

    private boolean setupVideoPipeline() {
        boolean re = true;

        _input = new ScreenDataSource(_viewer);
        try {
            _input.connect();
        }
        catch (Exception ex) {
            System.out.println("BhpMovieManager.setupVideoPipeline exception when connect input " + ex);
            ex.printStackTrace();
            re = false;
            return re;
        }

        Format formats[] = new Format[1];
        formats[0] = new VideoFormat(VideoFormat.CINEPAK);
        FileTypeDescriptor outputType = new FileTypeDescriptor(FileTypeDescriptor.QUICKTIME);

        try {
            _processor = Manager.createRealizedProcessor(new ProcessorModel(_input, formats, outputType));
        }
        catch (Exception ex) {
            System.out.println("BhpMovieManager.setupVideoPipeline exception when create processor " + ex);
            ex.printStackTrace();
            re = false;
            return re;
        }

        DataSource source = _processor.getDataOutput();
        try {
            re = checkMediaLocator();
            if (!re) {
                System.out.println("BhpMovieManager.setupVideoPipeline invalid media locator.");
                System.out.println("    " + _mediaLocator);
                return re;
            }
            _filewriter = Manager.createDataSink(source, _mediaLocator);
            //_filewriter.addDataSinkListener(new MyDataSinkListener());
            _filewriter.open();
        }
        catch (Exception ex) {
            System.out.println("BhpMovieManager.setupVideoPipeline exception when create data sink " + ex);
            ex.printStackTrace();
            re = false;
            return re;
        }

        try {
            _filewriter.start();
        }
        catch (Exception ex) {
            System.out.println("BhpMovieManager.setupVideoPipeline exception when start file writer " + ex);
            ex.printStackTrace();
            re = false;
            return re;
        }
        _processor.start();
        return re;
    }

    private boolean checkMediaLocator() {
        if (_mediaLocator == null) return false;
        boolean successful = true;
        String mediaString = getMediaLocatorString();
        if (mediaString.startsWith("file://")) {
            // a local file
            String fname = mediaString.substring(7);
            try {
                File file = new File(fname);
                if (file.exists()) successful = file.delete();
            }
            catch (Exception ex) {
                System.out.println("BhpMovieManager.checkMediaLocator exception:");
                ex.printStackTrace();
                successful = false;
            }
        }
        // need the implementation for remote file
        return successful;
    }

    private class MyCapturer implements Runnable {
        private boolean __endOfCapture;

        public MyCapturer(boolean e) {
            __endOfCapture = e;
        }

        public void run() {
            internalCapture(__endOfCapture);
        }
    }
*/
}
