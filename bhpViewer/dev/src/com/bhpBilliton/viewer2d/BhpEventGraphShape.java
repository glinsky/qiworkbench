/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.util.ArrayList;

import com.gwsys.gw2d.event.ShapeChangeEvent;
import com.gwsys.gw2d.event.ShapeChangeListener;
import com.gwsys.gw2d.model.Attribute2D;
import com.gwsys.gw2d.model.DataShape;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.gw2d.model.ScenePainter;
import com.gwsys.gw2d.shape.AbstractDataShape;
import com.gwsys.gw2d.shape.LocationBasedShape;
import com.gwsys.gw2d.shape.Polyline2D;
import com.gwsys.gw2d.shape.SymbolGroup;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class is the shape for event display.
 *               Three display options are possible for an event:
 *               a single polyline, a set of markers, and a set
 *               of polylines. <br>
 *               A separate copy of the data value is saved besides the real
 *               shapes. When method setValue is called, both the data value
 *               and the shape will be updated. Method resetValue is used
 *               to update the data value with that of the polylines. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpEventGraphShape extends AbstractDataShape
implements ShapeChangeListener {
    private static final double DEFAULT_SYMBOL_SIZE = 12;

    private double _nullValue;
    private ArrayList _shapes;
    private ArrayList _index;
    private Bound2D _bbox;
    private double[] _values;
    private double _start;
    private double _step;     // usually, it is 1
    private double _miny;
    private double _maxy;

    private Polyline2D _allLine;
    private SymbolGroup _allSymbol;
    private boolean _drawLine;
    private boolean _drawSymbol;
    private boolean _drawAllLine;
    private int _symbolStyle;
    private Attribute2D _symbolAttr;
    private double _symbolWidth = DEFAULT_SYMBOL_SIZE;
    private double _symbolHeight = DEFAULT_SYMBOL_SIZE;

    private int _shapeId;

    /**
     * Construct a new event shape.
     * @param values event data values. These are vertical positions.
     * @param start horizontal start value.
     * @param step horizontal step value.
     * @param miny minimum vertical value.
     * @param maxy maximum vertical value.
     * @param xs horizontal scale.
     * @param ys vertical scale.
     * @param nvalue value of null.
     */
    public BhpEventGraphShape(double[] values, double start, double step,
                         double miny, double maxy, int id, double nvalue) {
        _shapeId = id;
        _nullValue = nvalue;
        _bbox = null;
        _values = null;
        _index = null;
        _shapes = null;
        _start = 0;
        _step = 0;
        _miny = 0;
        _maxy = 0;

        _allLine = null;
        _allSymbol = null;
        _drawLine = true;
        _drawSymbol = false;
        _drawAllLine = false;
        _symbolStyle = BhpEventShape.SYMBOL_STAR;
        if (values != null) setValuesInternal(values, start, step, miny, maxy);
    }

    public int getShapeId() { return _shapeId; }

    /**
     * Initiates the graphic attribute of the shape.
     */
    public void initAttribute() {
        if (getAttribute() != null) return;
        RenderingAttribute newAtt = new RenderingAttribute();
        this.setAttribute(newAtt);
    }

    /**
     * Sets the attribute of the event shape.
     * This includes both polyline settings and
     * marker settings.
     * @param ref the reference shape where settings
     *        will be retrieved.
     */
    public void setEventAttribute(BhpEventShape ref) {
        if (getAttribute() == null) initAttribute();

        _drawLine = ref.isDrawLine();
        _drawSymbol = ref.isDrawSymbol();
        _drawAllLine = ref.isDrawAllLine();
        if (ref.getAttribute() != null) {
            getAttribute().setLineStyle(ref.getAttribute().getLineStyle());
			getAttribute().setLineWidth(ref.getAttribute().getLineWidth());
			getAttribute().setLineColor(ref.getAttribute().getLineColor());
        }

        if (_symbolStyle != ref.getSymbolStyle()) {
            _symbolStyle = ref.getSymbolStyle();
            if (_allSymbol != null)
                _allSymbol.setSymbolPainter(BhpEventShape.getSymbolPainter(_symbolStyle));
        }

        _symbolWidth = ref.getSymbolWidth();
        _symbolHeight = ref.getSymbolHeight();
        if (_allSymbol != null) _allSymbol.setSize(_symbolWidth, _symbolHeight);

        if (ref.getSymbolAttribute() != null) {
            _symbolAttr.setFillStyle(ref.getSymbolAttribute().getFillStyle());
            _symbolAttr.setLineColor(ref.getSymbolAttribute().getLineColor());
            _symbolAttr.setFillPaint(ref.getSymbolAttribute().getLineColor());
        }

        invalidateShape(this);
    }

    /**
     * Gets the specified null value of this event shape.
     */
    public double getNullValue() {
        return _nullValue;
    }

    /**
     * Changes one value in the event.
     * @param index the index of the value that needs to be changed.
     * @param value tne new value.
     * @param reset a boolean flag indicates if the shape needs
     *        to be updated accordingly.
     * @return a boolean flag indicates if this change involvs
     *         a null value, either null->non-null, or non-null->null.
     */
    public boolean setValueAt(int index, double value, boolean reset) {
        boolean nullValueChanged = false;
        if (BhpEventShape.isNullValue(_values[index], _nullValue))
            nullValueChanged = true;
        else if (BhpEventShape.isNullValue(value, _nullValue))
            nullValueChanged = true;

        _values[index] = value;
        if (reset) {
            resetShapes();
            for (int i=0; i<_shapes.size(); i++) {
                ((AbstractDataShape) _shapes.get(i)).setAttribute(getAttribute());
            }
            _allLine.setAttribute(getAttribute());
            _allSymbol.setAttribute(_symbolAttr);
            invalidateShape(this);
        }
        return nullValueChanged;
    }

    /**
     * Sets the bounding box of the shape.
     */
    public void setBboxRect(Bound2D bbox) {
        _bbox = bbox;
    }

    /**
     * Draws the shape. <br>
     * According to the user options, the event shape may be drawn as: <br>
     * (one polyline will all points connected <br> or
     * polylines connect only contiguiou point <br> or
     * no polyline at all) and <br>
     * (symbols for all non-null points <br> or
     * no symbol at all)
     */
    public void render(ScenePainter fp, Bound2D bbox) {
        DataShape shape;
        Bound2D shapeBox;
        if (_bbox != null) {
            if (_drawLine) {
                if (_drawAllLine) {
                    if (_allLine!=null && _allLine.isVisible()) {
                        shapeBox = _allLine.getBoundingBox(fp.getTransformation());
                        if (bbox.intersects(shapeBox.getX(), shapeBox.getY(),
                            shapeBox.getWidth(), shapeBox.getHeight())) {
                            if (fp instanceof ScenePainter)
                                ((ScenePainter)fp).render(_allLine, bbox);
                            else _allLine.render(fp, bbox);
                        }
                    }
                }
                else {
                    for (int i=0; i<_shapes.size(); i++) {
                        shape = (DataShape) _shapes.get(i);
                        if (shape==null || !shape.isVisible()) continue;

                        shapeBox = shape.getBoundingBox(fp.getTransformation());
                        if (bbox.intersects(shapeBox.getX(), shapeBox.getY(),
                            shapeBox.getWidth(), shapeBox.getHeight())) {
                            if (fp instanceof ScenePainter) {
                                ((ScenePainter)fp).render(shape, bbox);
                            }
                            else {
                                shape.render(fp, bbox);
                            }
                        }
                    }
                }
            }
            if (_drawSymbol) {
                if (_allSymbol!=null && _allSymbol.isVisible()) {
                    shapeBox = _allSymbol.getBoundingBox(fp.getTransformation());
                    if (bbox.intersects(shapeBox.getX(), shapeBox.getY(),
                        shapeBox.getWidth(), shapeBox.getHeight())) {
                        if (fp instanceof ScenePainter)
                            ((ScenePainter)fp).render(_allSymbol, bbox);
                        else _allSymbol.render(fp, bbox);
                    }
                }
            }
        }
    }

    /**
     * Gets the bounding box of the shape.
     */
    public Bound2D getBoundingBox(Transform2D tr) {
        return _bbox;
    }

    /**
     * Whe the shape is changed, invalidate it so that
     * the display updates properly.
     */
    public void shapeUpdated(ShapeChangeEvent e) {
        invalidateShape(((DataShape)e.getSource()));
    }

    /**
     * Sets the attribute of the event shape. <br>
     * This will update the attribute for polylines.
     * This only initiates the attribute for the symbols if
     * it is null. Otherwise, symbol attribute will not be
     * changed.
     */
    public void setAttribute(Attribute2D attr) {
        if (getAttribute()==null && attr==null)
			return;
        if (getAttribute()!=null && getAttribute().equals(attr))
			return;
        /*
		if (isNotificationEnabled()) {
            if (getAttribute() != null) {
				getAttribute().attributeChanged(
						new AttributeEvent(this, getAttribute(), AttributeEvent.CG_ATTR_DISCONNECT));
            }
            if (attr != null) {
                attr.attributeChanged(
						new AttributeEvent(this, attr, AttributeEvent.CG_ATTR_CONNECT));
            }
        }*/
		super.setAttribute(attr);
        if (_shapes != null) {
            for (int i=0; i<_shapes.size(); i++) {
                ((AbstractDataShape) _shapes.get(i)).setAttribute(getAttribute());
            }
        }
        if (_allLine != null) _allLine.setAttribute(getAttribute());

        if (_symbolAttr == null) {
            _symbolAttr = new RenderingAttribute(getAttribute());
            if (_allSymbol != null) _allSymbol.setAttribute(_symbolAttr);
        }
        invalidateShape(this);
    }

    private void setValuesInternal(double[] values, double start, double step,
                            double miny, double maxy) {
        _start = start;
        _step = step;
        _miny = miny;
        _maxy = maxy;
        _bbox = new Bound2D(start, _miny, start+step*values.length, _maxy);
        _values = new double[values.length];
        System.arraycopy(values, 0, _values, 0, values.length);
        _shapes = new ArrayList();
        _index = new ArrayList();
        resetShapes();
    }

    private void resetShapes() {
        _shapes.clear();
        _index.clear();
        if (_values==null || _values.length==0) return;
        int segLength = 0;
        int lastIndex = 0;
        //String nullString = "" + _nullValue;
        Polyline2D shape = null;
        double[] allxpos = new double[_values.length];
        double[] allypos = new double[_values.length];
        int allLength = 0;
        for(int i=0; i<_values.length; i++) {
            //if (nullString.equals(""+_values[i])) {
            if (BhpEventShape.isNullValue(_values[i], _nullValue)) {
                if (segLength == 0) {
                    // either null at the begining, or consecutive null values
                    lastIndex++;
                }
                else {  // create a polyline
                    double[] xpos = new double[segLength];
                    double[] ypos = new double[segLength];
                    for (int j=0; j<segLength; j++) {
                        xpos[j] = _start + 0.5 + (lastIndex+j) * _step;
                    }
                    System.arraycopy(_values, lastIndex, ypos, 0, segLength);
                    shape = new Polyline2D(segLength, xpos, ypos);
                    _shapes.add(shape);
                    _index.add(new Integer(lastIndex));
                    lastIndex = lastIndex + segLength + 1;
                    segLength = 0;
                }
            }
            else {
                segLength++;
                allxpos[allLength] = _start + 0.5 + i * _step;
                allypos[allLength] = _values[i];
                allLength++;
                if (_values[i] < _miny) _miny = _values[i];
                else if (_values[i] > _maxy) _maxy = _values[i];
            }
        }
        // need to create the last polyline
        if (segLength != 0) {
            double[] xpos = new double[segLength];
            double[] ypos = new double[segLength];
            for (int j=0; j<segLength; j++) {
                xpos[j] = _start + 0.5 + (lastIndex+j) * _step;
            }
            System.arraycopy(_values, lastIndex, ypos, 0, segLength);
            shape = new Polyline2D(segLength, xpos, ypos);
            _shapes.add(shape);
            _index.add(new Integer(lastIndex));
        }

        _allLine = new Polyline2D(allLength, allxpos, allypos);
        _allSymbol = new SymbolGroup(allxpos, allypos, allLength,
                            _symbolWidth, _symbolHeight,
                            LocationBasedShape.LOCATION_CENTER,
                            BhpEventShape.getSymbolPainter(_symbolStyle),
                            false, true);
        _bbox = new Bound2D(_bbox.getMinX(), _miny, _bbox.getMaxX(), _maxy);
    }
}
