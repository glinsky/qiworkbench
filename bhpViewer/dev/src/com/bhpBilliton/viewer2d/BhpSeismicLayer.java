/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.Dimension;
import java.util.ArrayList;

import org.w3c.dom.Node;

import com.bhpBilliton.viewer2d.ui.BhpBusyDialog;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.gw2d.model.CommonDataModel;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.gw2d.model.CommonShapeLayer;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;
import com.gwsys.seismic.core.DataChooser;
import com.gwsys.seismic.core.SegyTraceImage;
import com.gwsys.seismic.core.TraceInterpolator;
import com.gwsys.seismic.core.TraceNormalizer;
import com.gwsys.seismic.core.TraceRasterizer;
import com.gwsys.seismic.core.SeismicWorkflow;
import com.gwsys.seismic.core.DefaultWorkflow;
import com.gwsys.seismic.core.AutoGainController;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.SeismicColorMap;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class is used to display seismic data.
 *               It uses classes in JSeismic to make a standard seismic display.
 *               <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpSeismicLayer extends BhpLayer {
    /**
     *
     */
    private static final long serialVersionUID = 3258409534560220468L;
    public static final int SEISMIC_IMAGE_UPDATED = 1;
    public static final int SEISMIC_IMAGE_REGENERATED = 2;

    private boolean _adjustPlotVerticalScale = false;

    private ArrayList _listeners;

    private BhpViewerBase viewer;

    /**
     * Creates a seismic layer with a reference layer. <br>
     * This construtor is usually used when a new layer is created
     * by the users with GUI control.
     * @param viewer the parent viewer of the new layer.
     * @param win the parent window of the new layer.
     * @param id the identification number of the new layer.
     * @param dstype the name of the data source.
     * @param name name of the selected dataset.
     *        This is also the default name of the layer.
     * @param pathlist path of the selected dataset. <br>
     *        For bhpio data, it is the pathlist. For OpenSpirit data,
     *        it is the session name.
     * @param parameter a table model stores the user data selection.
     *        In the case of bhpio, it can be used to construct the
     *        query string used to run bhpio.
     * @param ref the reference layer. <br>
     *        The new layer will inherit from this reference layer things
     *        like scales, pipeline settings, display options, and etc.
     */
    public BhpSeismicLayer(BhpViewerBase viewer, BhpWindow win,
                            int id, String dstype, String name, String pathlist,
                            BhpSeismicTableModel parameter, BhpLayer ref) {
        super(viewer, win, id, dstype, name, name, pathlist,
              parameter, null, false, ref, BhpViewer.BHP_DATA_TYPE_SEISMIC);
        _listeners = new ArrayList();
        this.viewer = viewer;
    }

    /**
     * Creates a new model layer with specified attributes. <br>
     * This construtor is usually used when a new layer is created
     * parsing saved XML configuration file.
     * @param viewer the parent viewer of the new layer.
     * @param win the parent window of the new layer.
     * @param id the identification number of the new layer.
     * @param dstype the name of the data source.
     * @param dname name of the selected dataset.
     * @param name name of the new layer.
     * @param pathlist path of the selected dataset. <br>
     *        For bhpio data, it is the pathlist. For OpenSpirit data,
     *        it is the session name.
     * @param parameter a table model stores the user data selection.
     *        In the case of bhpio, it can be used to construct the
     *        query string used to run bhpio.
     * @param mupt horizontal scale with unit of model-unit-per-trace.
     * @param mups vertical scale with unit model-unit-per-sample.
     * @param talk a boolean indicates if the new layer should
     *        broadcase its change event.
     * @param syn the synchronization flag of the new layer.
     * @param trans transparency(opacity) of the new layer.
     * @param pnode the pipeline node in the DOM tree. <br>
     *        It is used to initialize the pipeline of the new layer.
     * @param rp a boolean for polarity.
     */
    public BhpSeismicLayer(BhpViewerBase viewer, BhpWindow win, int id,
                            String dstype, String dname, String name,
                            String pathlist, BhpSeismicTableModel parameter,
                            double mupt, double mups, boolean talk, int syn,
                            int trans, Node pipelineNode, boolean rp, boolean vsflag) {
        super(viewer, win, id, dstype, dname, name, pathlist,
              parameter, null, talk, syn, trans, false,
              pipelineNode, BhpViewer.BHP_DATA_TYPE_SEISMIC, rp, false);
        _hscaleFactor = mupt;
        _vscaleFactor = mups;
        _adjustPlotVerticalScale = vsflag;
        _listeners = new ArrayList();
        this.viewer = viewer;
    }

    /**
     * Converts the model layer object into an XML string. <br>
     * This is used when save the working session into an XML file.
     * Please refer to BhpViewer.dtd for the detail of the format.
     * @return an XML string for the object.
     */
    public String toXMLString() {
        StringBuffer content = new StringBuffer();
        content.append("        " + "<BhpSeismicLayer>\n");
        content.append(super.toXMLString());
        content.append("        " + "</BhpSeismicLayer>\n");
        return content.toString();
    }

    /**
     * Gets the vertical scale of the layer. <br>
     * The scale is calculated with the scale stored in
     * <code>{@link AbstractPlot}</code>
     * and the scale factor of this layer.
     */
    public double getLayerVerticalScale() {
        // modelUnitPerSample
        double re = _window.getBhpPlot().getVerticalScale() * _vscaleFactor;
        re = re * _reader.getMetaData().getSampleRate();
        return re;
    }

    /**
     * Transforms a vertical model space coordinate to virtual vertical value. <br>
     * This method is used for mouse click data synchronization.
     * It overrides the implementation in <code>{@link BhpLayer}</code>.
     * @param modelV vertical model sapce coordinate.
     * @return vertical axis reading.
     */
    public double findVerticalReadingFromModel(double modelV) {
        double[] vsettings = _parameter.getVerticalSettings();
        double startValue = _pipeline.getDataLoader().getDataReader().getMetaData().getStartValue();
        double mlt = _pipeline.getDataLoader().getDataSelector().getStartSampleValue() + startValue;
        double mlb = _pipeline.getDataLoader().getDataSelector().getEndSampleValue() + startValue;
        double verticalValue = mlt + modelV * vsettings[2];
        if (verticalValue > mlb) verticalValue = mlb;
        return verticalValue;
    }

    /**
     * Transforms a virtual vertical value to model space coordinate. <br>
     * This method is used for mouse click data synchronization.
     * It overrides the implementation in <code>{@link BhpLayer}</code>.
     * @param readingV vertical axis reading
     * @return model space coordinate
     */
    public int findModelFromVerticalReading(double readingV) {
        double[] vsettings = _parameter.getVerticalSettings();
        double startValue = _pipeline.getDataLoader().getDataReader().getMetaData().getStartValue();
        double mlt = _pipeline.getDataLoader().getDataSelector().getStartSampleValue();
        double mlb = _pipeline.getDataLoader().getDataSelector().getEndSampleValue();
        int result = (int) ((readingV-mlt-startValue) / vsettings[2]);
        return result;
    }

    /**
     * Finds out the read data value at the specific point. <br>
     * This default implementation simply returns 0.
     * Concrete class extends BhpLayer should have meaningful implemenation.
     * @param traceid trace number, the horizontal position.
     * @param vreading vertical axis reading
     * @return data value of the given trace at the specific vertical position.
     */
    public double findSpecificDataValue(int traceid, double vreading) {
        double re = 0;
        if (_reader != null) {
            TraceData traceData = new TraceData();
            int realid = _pipeline.getDataLoader().getDataSelector().virtualToReal(traceid);
            boolean presult = _reader.process(realid, null, traceData);
            if (presult) {
                float[] dataArray = traceData.getSamples();
                double startValue = _reader.getMetaData().getStartValue();
                double sampleRate = _reader.getMetaData().getSampleRate();
                int index = (int)Math.round((vreading - startValue) / sampleRate);
                if (index>=dataArray.length || index<0) {
                    //It is possible for the cursor to be in a reqion in one window that is
                    //not valid in aother window. Ingore.
/*
                    ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                        "BhpSeismicLayer.findSpecificDataValue outOfBounds: "
                                                            + vreading + "->" + index + " : " +
                                                            dataArray.length);
*/
                    index = 0;
                }
                re = dataArray[index];
            }
        }
        return re;
    }

    public void updateLayer(boolean redraw) {

        boolean oldNotify = _image.isNotificationEnabled();
        _image.setNotification(false);
        double modelUnitPerTrace = getLayerHorizontalScale();
        double modelUnitPerSample = getLayerVerticalScale();
        _image.setScale(modelUnitPerTrace, modelUnitPerSample);
        _image.setNotification(oldNotify);
        int actualSamplesPerTrace =
            (int) ((_pipeline.getDataLoader().getDataSelector().getEndSampleValue()-
                    _pipeline.getDataLoader().getDataSelector().getStartSampleValue()) /
                                           _reader.getMetaData().getSampleRate()) + 1;

        double modelWidth = _pipeline.getDataLoader().getDataSelector().getVirtualModelLimits().width * modelUnitPerTrace;
        double modelHeigth = (actualSamplesPerTrace-1) * modelUnitPerSample;
        _modelBox = new Bound2D(0, 0, modelWidth, modelHeigth);
        _containerModel.setBoundingBox(_modelBox);
        _shapeListLayer.setBoundingBox(_modelBox);
        if (redraw) {
            this.setTransformation(this.getTransformation());


            AbstractPlot ppp = _window.getBhpPlot();
            if (ppp.getAxisAssociateLayer() == this.getIdNumber()) {
                ppp.updateHAxes();
                ppp.updateVAxes();
                ppp.updateMiscAnnotation();
            }
        }
        notifySeismicLayerListeners(SEISMIC_IMAGE_UPDATED);
    }

    public void updateSeismicDisplay() {
        _pipeline.getDataLoader().setDataReader(_reader);
        BhpListenerDataSelector oldSelector = (BhpListenerDataSelector)
            _pipeline.getDataLoader().getDataSelector();
        BhpListenerDataSelector newSelector = new BhpListenerDataSelector();
        _pipeline.setDataSelector(newSelector);
        newSelector.applyGaps(oldSelector.isGapApplied());
        newSelector.setGapInTraces(oldSelector.getGapInTraces());
        if (oldSelector.getStartSampleChanged())
            {
            newSelector.setStartSampleValue(oldSelector.getStartSampleValue());
            }
        if (oldSelector.getEndSampleChanged() && oldSelector.getEndSampleValue()!=0)
            {
            newSelector.setEndSampleValue(oldSelector.getEndSampleValue());
            }
        if (oldSelector.getPrimaryKey() != DataChooser.TRACE_ID_AS_KEY) {
            newSelector.setPrimaryKey(oldSelector.getPrimaryKey());
            newSelector.setPrimaryKeyEnd(oldSelector.getPrimaryKeyEnd());
            newSelector.setPrimaryKeyStart(oldSelector.getPrimaryKeyStart());
            newSelector.setPrimaryKeyStep(oldSelector.getPrimaryKeyStep());
        }
        else {
            newSelector.setPrimaryKey(oldSelector.getPrimaryKey());
            newSelector.setPrimaryKeyStart(oldSelector.getPrimaryKeyStart());
            newSelector.setPrimaryKeyStep(oldSelector.getPrimaryKeyStep());
        }
        if (oldSelector.getSecondaryKey() != DataChooser.NO_SECONDARY_KEY) {
            newSelector.setSecondaryKey(oldSelector.getSecondaryKey());
            newSelector.setSecondaryKeyEnd(oldSelector.getSecondaryKeyEnd());
            newSelector.setSecondaryKeyStart(oldSelector.getSecondaryKeyStart());
            newSelector.setSecondaryKeyStep(oldSelector.getSecondaryKeyStep());
        }

        _shapeListLayer.removeShape(_image);
        double modelUnitPerTrace = getLayerHorizontalScale();
        double modelUnitPerSample = getLayerVerticalScale();
        _image = new SegyTraceImage(0, 0, modelUnitPerTrace, modelUnitPerSample, _pipeline);
        _image.setAttribute(new RenderingAttribute(_containerModel));
        _shapeListLayer.addShapeAtIndex(_image, 0);

        int actualSamplesPerTrace =
            (int) ((_pipeline.getDataLoader().getDataSelector().getEndSampleValue()-
                    _pipeline.getDataLoader().getDataSelector().getStartSampleValue()) /
                                           _reader.getMetaData().getSampleRate()) + 1;

        double modelWidth = _pipeline.getDataLoader().getDataSelector().getVirtualModelLimits().width * modelUnitPerTrace;
        double modelHeigth = (actualSamplesPerTrace-1) * modelUnitPerSample;
        _modelBox = new Bound2D(0, 0, modelWidth, modelHeigth);
        _containerModel.setBoundingBox(_modelBox);
        _shapeListLayer.setBoundingBox(_modelBox);

        AbstractPlot ppp = _window.getBhpPlot();
        if (ppp.getAxisAssociateLayer() == this.getIdNumber()) {
            ppp.updateHAxes();
            ppp.updateVAxes();
            ppp.updateMiscAnnotation();
        }
        this.setTransformationWithoutRepaint(this.getTransformation());
        _viewer.changeLoadingLayerNumber(BhpBusyDialog.RMV_LAYER);
        notifySeismicLayerListeners(SEISMIC_IMAGE_REGENERATED);

    }

    public double getTimeRangeStart() {
        double value = _pipeline.getDataLoader().getDataSelector().getStartSampleValue();
        return value;
    }

    public double getTimeRangeEnd() {
        double value = _pipeline.getDataLoader().getDataSelector().getEndSampleValue();
        return value;
    }

    public void setTimeRange(double start, double end) {
        _pipeline.getDataLoader().getDataSelector().setStartSampleValue(start);
        _pipeline.getDataLoader().getDataSelector().setEndSampleValue(end);
        _pipeline.invalidate(_pipeline.getDataLoader().getDataSelector());
    }

    // get missing data selection - 0=ignore, 1=fill
    public int getMissingDataSelection() {
      return _parameter.getMissingDataSelection();
    }

    public void addSeismicLayerListener(BhpSeismicLayerListener l) {
        if (l != null && !_listeners.contains(l))
            _listeners.add(l);
    }

    public void removeSeismicLayerListener(BhpSeismicLayerListener l) {
        if (l != null)
            _listeners.remove(l);
    }

    public boolean receiveBhpLayerEvent(BhpLayerEvent e) {
        BhpLayer layer = e.getSource();
        int flag = e.getTypeFlag();
        if (layer != null&&layer.getIdNumber() == this.getIdNumber()) {
            if ((flag & BhpViewer.COLORBAR_FLAG) != 0) { // use my transparent value
                updateColorMap((SeismicColorMap) e.getParameter());
            }
            //System.out.println("BhpSeismicLayer.receiveBhpLayerEvent myself");
            return false;
        }

        int mflag = super.handleBhpLayerEvent(e);
        boolean updated=false;
        if (((flag & BhpViewer.AGC_FLAG)!=0) && ((this.getSynFlag() & BhpViewer.AGC_FLAG)!=0)) {
            // AGC_FLAG can only be sent from a seismic layer
            // also, only seismic layer is interested in it
            this.updateAGC(layer.getPipeline().getInterpretation().getTraceAGC());
            getPipeline().invalidate(getPipeline().getInterpretation().getTraceAGC());
            mflag = mflag | BhpViewer.AGC_FLAG;
        }
        if (((flag & BhpViewer.WIGGLEDECIMATION_FLAG)!=0) && ((this.getSynFlag() & BhpViewer.WIGGLEDECIMATION_FLAG)!=0)) {
            //ignore this flag
        }

        if (((mflag & BhpViewer.DATAINCREMENT_FLAG)!=0) ||
            ((mflag & BhpViewer.DATASELECTION_FLAG)!=0) ||
            ((mflag & BhpViewer.NAME_FLAG)!=0)) {
            this.generateLayer();
            updated=true;
        }
        else if (mflag != 0) {
            this.updateLayer();
        }
        return updated;
    }

    public synchronized void setupSeismicDisplay() {
        _pipeline = new DefaultWorkflow(_reader);
        BhpListenerDataSelector newSelector = new BhpListenerDataSelector();
        _pipeline.setDataSelector(newSelector);
        _pipeline.setTraceAGC(new AutoGainController());

        // default settings for the seismic pipeline
        _pipeline.getInterpretation().getTraceAGC().setEnabled( false );
        _pipeline.getInterpretation().getTraceNormalization().setScale(0.2f);
        TraceRasterizer currentRasterizer = _pipeline.getTraceRasterizer();

        if (_pipelineNode != null) {
            if (_window.getBhpPlot().getAxisAssociateLayer() == getIdNumber() && _adjustPlotVerticalScale) {
                // this is the axis layer in the saved configuration file
                double newvs = _window.getBhpPlot().getVerticalScale() / _reader.getMetaData().getSampleRate();
                _window.getBhpPlot()._modelUnitPerSample = newvs;
                _adjustPlotVerticalScale = false;
            }
            BhpViewerHelper.initWithXML(viewer, _pipelineNode, _pipeline);
            _pipelineNode = null;
        }
        else {
            if (_sampleLayer != null) {
                boolean sampleLayerIsSeismic = false;
                if (_sampleLayer instanceof BhpSeismicLayer) sampleLayerIsSeismic = true;
                initPipelineWithAnother(_sampleLayer.getPipeline(), sampleLayerIsSeismic);
            }
        }

        int actualSamplesPerTrace =
            (int) ((_pipeline.getDataLoader().getDataSelector().getEndSampleValue()-
                    _pipeline.getDataLoader().getDataSelector().getStartSampleValue()) /
                                           _reader.getMetaData().getSampleRate()) + 1;
        //calculate the size on screen
        double modelUnitPerTrace = getLayerHorizontalScale();
        double modelUnitPerSample = getLayerVerticalScale();
        
        double modelWidth = _pipeline.getDataLoader().getDataSelector().getVirtualModelLimits().width * modelUnitPerTrace;
        double modelHeight = (actualSamplesPerTrace-1) * modelUnitPerSample;

        if ((modelHeight < 50) && (this._window.getBhpLayerList().getModel().getSize()==0)) {
            // first layer in the window. okay to adjust scale
            _window.getBhpPlot()._modelUnitPerSample = 500 * _window.getBhpPlot()._modelUnitPerSample;
            modelUnitPerSample = getLayerVerticalScale();
            modelHeight = (actualSamplesPerTrace-1) * modelUnitPerSample;
        }

        _modelBox = new Bound2D(0, 0, modelWidth, modelHeight);
        _deviceBox = new Bound2D(0, 0, modelWidth, modelHeight);
        _transformation = new Transform2D(_modelBox, _deviceBox, false, false);

        _shapeListLayer = new CommonShapeLayer();
        _containerModel = new CommonDataModel();
        _containerModel.addLayer(_shapeListLayer);
        _containerModel.setBoundingBox(_modelBox);
        _shapeListLayer.setBoundingBox(_modelBox);

        _image = new SegyTraceImage(0, 0, modelUnitPerTrace, modelUnitPerSample, _pipeline);
        _image.setPipeline(_pipeline);

        RenderingAttribute att = new RenderingAttribute(_containerModel);
        _image.setAttribute(att);

        if (Double.isNaN(_image.getHeight()))
            _image.setSize(_image.getWidth(), 500);

        _shapeListLayer.addShape(_image);
        this.setModel(_containerModel);
        this.setTransformation(_transformation);

        _viewer.addBhpLayer(_window, this);
        _viewer.changeLoadingLayerNumber(BhpBusyDialog.RMV_LAYER);
    }

    private void notifySeismicLayerListeners(int flag) {
        for (int i=0; i<_listeners.size(); i++) {
            ((BhpSeismicLayerListener)(_listeners.get(i))).seismicLayerChanged(this, flag);
        }
    }

    private void initPipelineWithAnother(SeismicWorkflow plR, boolean isSeismic) {
        DataChooser sltr = _pipeline.getDataLoader().getDataSelector();
        sltr.applyGaps(plR.getDataLoader().getDataSelector().isGapApplied());
        sltr.setGapInTraces(plR.getDataLoader().getDataSelector().getGapInTraces());
        if (isSeismic) {
            BhpListenerDataSelector oldSelector = (BhpListenerDataSelector)
            plR.getDataLoader().getDataSelector();
            if (oldSelector.getStartSampleChanged())
                sltr.setStartSampleValue(oldSelector.getStartSampleValue());
            if (oldSelector.getEndSampleChanged())
                sltr.setEndSampleValue(oldSelector.getEndSampleValue());
        }

        if (plR.getInterpretation().getTraceNormalization() != null) {
            TraceNormalizer norm = (_pipeline.getInterpretation().getTraceNormalization());
            TraceNormalizer normR = (plR.getInterpretation().getTraceNormalization());
            norm.setNormalizationMode(normR.getNormalizationMode());
            norm.setScale(normR.getScale());
            norm.setNormalizationLimits(normR.getNormLimitMin(), normR.getNormLimitMax());
        }

        if (plR.getInterpretation().getTraceInterpolator() != null) {
            TraceInterpolator inter = _pipeline.getInterpretation().getTraceInterpolator();
            inter.setInterpolationType(plR.getInterpretation().getTraceInterpolator().getInterpolationType());
            inter.setResamplingFactor(plR.getInterpretation().getTraceInterpolator().getResamplingFactor());
        }

        AutoGainController tagc = _pipeline.getInterpretation().getTraceAGC();
        tagc.setEnabled(false);
        AutoGainController anagc = plR.getInterpretation().getTraceAGC();
        if (isSeismic && anagc != null && anagc.isEnabled() == true) {
            tagc.setEnabled(true);
            tagc.setUnits(anagc.getUnits());
            tagc.setWindowLength(anagc.getWindowLength());
        }

        if (plR.getTraceRasterizer() != null) {
            TraceRasterizer rast = _pipeline.getTraceRasterizer();
            TraceRasterizer rrast = plR.getTraceRasterizer();
            rast.setClippingValue(rrast.getClippingValue());
            if (isSeismic) rast.setPlotType(rrast.getPlotType());
            BhpViewerHelper.initColormapWithAnother(rast.getColorMap(), rrast.getColorMap());
        }
    }

    private void updateAGC(AutoGainController src) {
        AutoGainController des = this.getPipeline().getInterpretation().getTraceAGC();
        des.setEnabled(src.isEnabled());
        des.setUnits(src.getUnits());
        des.setWindowLength(src.getWindowLength());
    }
}
