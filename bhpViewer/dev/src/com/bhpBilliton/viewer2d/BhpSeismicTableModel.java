/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.text.NumberFormat;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import org.w3c.dom.Node;

import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.data.GeneralDataSource;
import com.bhpBilliton.viewer2d.ui.OpenGeneralPanel;
import com.bhpBilliton.viewer2d.ui.VerticalRangePanel;
import com.bhpBilliton.viewer2d.ui.util.ElementAttributeReader;
import com.bhpBilliton.viewer2d.util.CaretStringArrayList;
import com.bhpBilliton.viewer2d.util.ErrorDialog;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class stores the data for the JTable in
 *               <code>{@link OpenGeneralPanel}</code>.
 *               There are totaly nine columns in this table model. They
 *               are created according to bhpio. The first two columns
 *               display the information about the header, the name and
 *               the available range, they don't allow editing.
 *               A user can change the values of all the
 *               other columns so that the query string of bhpread
 *               fits his needs. Method getParameterString will return a
 *               string that can be used in bhpread command directly.
 *               This class also keeps a string named _properties. This
 *               is the list of properties of model data, or list of
 *               available events in the case of event data.  <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpSeismicTableModel extends DefaultTableModel {
  private static final int _MAXROWNUMBER = 6;
  private static final String[] _COLUMNNAMES = {
    "Key", "FullRange", "ChosenRange", "Order", "Increment",
    "Offset", "Synchronize", "Interpolation", "Binsize"};

  private boolean[] _rowsChanged;
  private boolean _isSeismic;

  private String _properties;

  private GeneralDataSource _dataSource;

  // current setting of missingData options - 0=ignore, 1=fill
  private int missingDataSelection = 0;

  // a string from bhpio output with info about data type and storing order
  private String _dataAttributeString;

  private BhpViewerBase _viewer;

  /**
   * Calculates the position of "-" in a range of format "start-end[step]".
   * However, "-" can also presents in negative numbers. So, there
   * may be multiple dashes in the string. This method takes into
   * consideration all these possibilities.
   * @return the position of the range divider, -1 if it is not found.
   */
  public static int rangeIndex(String value) {
    int dashIndex = value.indexOf("-", 1);
    if (dashIndex >= 0) {
      // there must be at least one "-", not at the very begining
      int prevIndex = dashIndex - 1;
      char prevChar;
      while (prevIndex >= 0) {
        prevChar = value.charAt(prevIndex);
        if (Character.isDigit(prevChar)) return dashIndex;
        if (Character.isWhitespace(prevChar)) prevIndex--;
        else return -1;
      }
    }
    return -1;
  }

  /**
   * Creates a new table model.
   * @param An Instance of the viewer.
   * @param isSeismic a boolean indicates if the model is for seismic data.
   */
  public BhpSeismicTableModel(BhpViewerBase viewer, boolean isSeismic) {
    super(_COLUMNNAMES, 0);
    this._viewer = viewer;
    _isSeismic = isSeismic;
    _properties = "";
    if (_isSeismic == false) this.setColumnCount(_COLUMNNAMES.length-1);
    _rowsChanged = new boolean[_MAXROWNUMBER];
    for (int i=0; i<_MAXROWNUMBER; i++)
      _rowsChanged[i] = false;
  }

  /** This <code>clone()</code> implementation is broken.  However, it is widely used within
   *  the bhpViewer, which may depend on its quirks.  Therefore, this method
   *  has been marked deprecated and should be re-implemented or removed later.
   *  
   *  Problems
   *  <ol>
   *    <li>It does not call super.clone(), making it impossible to extend this class and use this implementation.<li>
   *    <li>It calls <code>new BhpSeismicTableModel()</code>, breaking the contract of Object.clone(), which states that no constructor is called.</li>
   *  </ol>
   *
   * @deprecated
   */
  public Object clone() {
    BhpSeismicTableModel newcopy = new BhpSeismicTableModel(this._viewer, this._isSeismic);
    newcopy._properties = this._properties;
    newcopy._dataAttributeString = this._dataAttributeString;
    newcopy._dataSource = this._dataSource; // might need clone it too.
    int expectedRowCount = this.getColumnCount();
    for (int i=0; i<this.getRowCount(); i++) {
      Vector row = getRowCopy(expectedRowCount, i);
      newcopy.addRow(row);
    }

    return newcopy;
  }

  /**
   * @param expectedRowCount
   * @param rowID
   * @return
   */
  private Vector getRowCopy(int expectedRowCount, int rowID) {
    Vector row = new Vector();
    row.add(this.getValueAt(rowID, 0).toString());
    row.add(this.getValueAt(rowID, 1).toString());
    row.add(this.getValueAt(rowID, 2).toString());
    row.add(new Integer(this.getValueAt(rowID, 3).toString()));
    row.add(new Double(this.getValueAt(rowID, 4).toString()));
    row.add(new Double(this.getValueAt(rowID, 5).toString()));
    row.add(new Boolean(this.getValueAt(rowID, 6).toString()));
    if (expectedRowCount > 7)
      row.add(new Boolean(this.getValueAt(rowID, 7).toString()));
    else row.add(new Boolean(true));
    if (expectedRowCount > 8)
      row.add(new Integer(this.getValueAt(rowID, 8).toString()));
    else if (_isSeismic) row.add(new Integer(0));
    return row;
  }

  /**
   * Sets the data source of the table model.
   */
  public void setDataSource(GeneralDataSource ds) { _dataSource = ds;   }
  /**
   * Gets the data source of the table model.
   */
  public GeneralDataSource getDataSource()        { return _dataSource; }

  /**
   * Initaites the model with an XML node. <br>
   */
  public void initWithXML(Node node, String dstype) {
    if (node==null || node.getNodeType() != Node.ELEMENT_NODE) {
      return;
    }

    int expectedRowCount = _COLUMNNAMES.length - 2;
    if (dstype.equals(GeneralDataSource.DATA_SOURCE_BHPSU)) {
      if (_isSeismic) {
        expectedRowCount = _COLUMNNAMES.length;
      } else {
        expectedRowCount = _COLUMNNAMES.length - 1;
      }
    }

    ElementAttributeReader emt = new ElementAttributeReader(node);
    if (_isSeismic == false) {
      String properties = emt.getString("properties");
      this.setProperties(properties);
    }
    this.setDataAttributeString(emt.getString("attributeString"));

    String content = emt.getString("content");
    StringTokenizer stk = new StringTokenizer(content, "|");
    try {
      while(stk.hasMoreTokens()) {
        this.addRow(parseRowFromTokenizer(expectedRowCount, stk));
      }
    }
    catch (Exception ex) {
        ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                            "BhpSeismicTableModel.initWithXML exception : Content " + content);
    }

    // missingData selection - 0(ignore) or 1(fill)
    String md = emt.getString("missingData");
    if(!md.equals("")) {
      if(md.equals("1"))
        setMissingDataSelection(1);
      else
        setMissingDataSelection(0);
    }
  }

  /**
   * @param expectedRowCount
   * @param stk
   * @return TODO
   */
  private Vector parseRowFromTokenizer(int expectedRowCount, StringTokenizer stk) {
    Vector row = new Vector();
    row.add(stk.nextToken());
    row.add(stk.nextToken());
    row.add(stk.nextToken());
    row.add(new Integer(stk.nextToken()));
    row.add(new Double(stk.nextToken()));
    row.add(new Double(stk.nextToken()));
    row.add(new Boolean(stk.nextToken()));
    if (expectedRowCount > 7) {
      row.add(new Boolean(stk.nextToken()));
    } else {
      row.add(new Boolean(true));
    }
    if (expectedRowCount > 8) {
      row.add(new Integer(stk.nextToken()));
    } else if (_isSeismic) {
      row.add(new Integer(0));
    }

    return (row);
  }

  /**
   * Converts the model into an XML string. <br>
   * This is used when save the working session into an XML file.
   * Please refer to BhpViewer.dtd for the detail of the format.
   * @return an XML string for the object.
   */
  public String toXMLString(String dstype) {
    StringBuffer tableContent = new StringBuffer();
    int expectedRowCount = _COLUMNNAMES.length - 2;
    if (dstype.equals(GeneralDataSource.DATA_SOURCE_BHPSU)) {
      if (_isSeismic) expectedRowCount = _COLUMNNAMES.length;
      else expectedRowCount = _COLUMNNAMES.length -1;
    }
    for (int i=0; i<this.getRowCount(); i++) {
      for (int j=0; j<expectedRowCount; j++) {
        tableContent.append(this.getValueAt(i, j));
        tableContent.append("|");   // as the separator
      }
    }

    StringBuffer content = new StringBuffer();
    content.append("                " + "<Parameter ");
    content.append("properties=\"" + _properties + "\" ");
    content.append("attributeString=\"" + _dataAttributeString + "\" ");
    content.append("content=\"" + tableContent.toString() + "\" ");

    // missingDataSelection=0(ignore) or =1(fill)
    content.append(" missingData=\"" + getMissingDataSelection() + "\"" + " />");

    return content.toString();
  }

  /**
   * Gets the data attribute.
   */
  public String getDataAttributeString()       { return _dataAttributeString; }
  /**
   * Sets the data attribute.
   */
  public void setDataAttributeString(String s) { if (s!=null) _dataAttributeString = s;}

  /**
   * Sets the properties string of the data. <br>
   * This is the property names for model data and event names
   * for horizon data. For seismic data, it is empty.
   */
  public void setProperties(String s) {
    if (s!=null) _properties = s.trim();
  }
  /**
   * Gets the properties string of the data. <br>
   * This is the property names for model data and event names
   * for horizon data. For seismic data, it is empty.
   */
  public String getProperties() { return _properties; }

  /**
   * A boolean array is used to indicates if a row in the table model
   * has been changed. This method reset all the values in the array
   * to false.
   */
  public void clearRowsChanged() {
    for (int i=0; i<_MAXROWNUMBER; i++)
      _rowsChanged[i] = false;
  }

  /**
   * A boolean array is used to indicates if a row in the table model
   * has been changed. This method sets the one at the given position
   * to true, indicating that row has been changed.
   * @param row the index of value that needs to be set to true.
   */
  public void setRowsChanged(int row) {
    _rowsChanged[row] = true;
  }

  /**
   * Finds out if a specific row has been changed.
   * @return true if the given row has been changed, false otherwise.
   */
  public boolean getRowsChanged(int row) {
    if (row >= _MAXROWNUMBER) return false;
    return _rowsChanged[row];
  }

  public String getRowHeaderName(int row) {
    return ((String)(this.getValueAt(row, 0))).trim();
  }

  /**
   * This method is called for data synchronizatioin. <br>
   * The hashtable is the parameter of a
   * <code>{@link BhpLayerEvent}</code>
   * of type DATASELECTION_FLAG.
   * The keys of the hashtable is the key names of the data
   * as displayed in the data table.
   * The values are either Integer specifying the value of the specific key,
   * or, in the case of arbitrary traverse, values are
   * <code>{@link CaretStringArrayList}</code>
   * of either integers or doubles.
   * The table model will be updated according to the data of the
   * Hashtable and the synchronization flag of each header.
   * @return a boolean flag indicates if the data in the table model has
   *         really been changed.
   */
  public boolean updateWithHashtable(Hashtable source) {
    int listInSource = 0;
    Iterator sviter = source.values().iterator();
    while(sviter.hasNext()) {
      if (sviter.next() instanceof List) {
        listInSource++;
      }
    }
    if (listInSource == 2)
      return updateWithHashtableArbitraryTraverse(source);


    boolean myChange = false;
    int changedKeys = 0;
    String[] oldValues = new String[this.getRowCount()];
    String name;
    boolean synch;
    //int index;
    for (int i=0; i<this.getRowCount(); i++) {
      oldValues[i] = null;
      name = (String) (this.getValueAt(i, 0));
      synch = ((Boolean) (this.getValueAt(i, 6))).booleanValue();
      if (synch == false) continue;
      if (source.containsKey(name) == false) continue;
      String valueString = source.get(name).toString();
      //Integer value = (Integer) (source.get(name));
      String oriValue = (String) (this.getValueAt(i, 2));
      if (oriValue.equals(valueString)) continue;
      this.setValueAt(valueString, i, 2);
      oldValues[i] = oriValue;
      changedKeys++;
      myChange = true;
    }
    if (changedKeys == 2) {
      boolean wasArbitraryTraverse = true;
      for (int i=0; i<oldValues.length; i++) {
        if (oldValues[i] != null) {
          if (oldValues[i].indexOf("^") <= 0)
            wasArbitraryTraverse = false;
        }
      }
      if (wasArbitraryTraverse) {
        if (this.estimateMaxTraceNumber() < 2) {
          for (int i=0; i<oldValues.length; i++) {
            if (oldValues[i] != null) {
              this.setValueAt(oldValues[i], i, 2);
            }
          }
          myChange = false;
        }
      }
    }
    return myChange;
  }

  private boolean updateWithHashtableArbitraryTraverse(Hashtable source) {
    Enumeration enumKeys = source.keys();
    String name1 = null;
    String name2 = null;
    Object tmpKey = null;
    String values1 = null;
    String values2 = null;
    int listIndex = 0;
    while(enumKeys.hasMoreElements()) {
      tmpKey = enumKeys.nextElement();
      if (source.get(tmpKey) instanceof List) {
        listIndex++;
        if (listIndex == 1) {
          name1 = tmpKey.toString();
          values1 = source.get(tmpKey).toString();
        }
        else if (listIndex == 2) {
          name2 = tmpKey.toString();
          values2 = source.get(tmpKey).toString();
          break;
        }
      }
    }
    //String name1 = enum.nextElement().toString();
    //String name2 = enum.nextElement().toString();
    //String values1 = source.get(name1).toString();
    //String values2 = source.get(name2).toString();
    int index1 = -1;
    int index2 = -1;
    String name;
    boolean synch;
    for (int i=0; i<this.getRowCount(); i++) {
      name = (String)(this.getValueAt(i, 0));
      synch = ((Boolean)(this.getValueAt(i, 6))).booleanValue();
      if (synch == false) continue;
      if (name.equals(name1)) index1 = i;
      else if (name.equals(name2)) index2 = i;
    }

    if (index1 < 0 || index2 < 0)
      return false;
    String oriValue1 = (String)(this.getValueAt(index1, 2));
    String oriValue2 = (String)(this.getValueAt(index2, 2));
    boolean myChange = false;
    if (!oriValue1.equals(values1)) {
      this.setValueAt(values1, index1, 2);
      myChange = true;
    }
    if (!oriValue2.equals(values2)) {
      this.setValueAt(values2, index2, 2);
      myChange = true;
    }
    return myChange;
  }

  /**
   * Initiates the table model with that value of another one. <br>
   * This method tries to update each row in the current table
   * with the settings of the same header in source table model.
   * @param source another table model where data will be retrieved.
   */
  public void initWithAnother(BhpSeismicTableModel source) {
    String name;
    int index;
    int minColCount = Math.min(this.getColumnCount(), source.getColumnCount());
    for (int i=0; i<source.getRowCount(); i++) {
      name = (String) source.getValueAt(i, 0);
      if (name.equals("tracl") && (_isSeismic != source._isSeismic))
        continue;

      index = this.findKey(name);
      if (index < 0) continue;
      for (int j=2; j<minColCount; j++) {
        if (j==3) continue; // do not inherit order
        this.setValueAt(source.getValueAt(i, j), index, j);
      }
    }
  }

  /**
   * This method is called for data synchronizatioin. <br>
   * When the user selection of the data set of a layer is changed,
   * a <code>{@link BhpLayerEvent}</code> of type DATASELECTION_FLAG
   * is created and broadcast. When another layer receives such an
   * event, it will asks its table model to update accordingly by
   * calling this method. This method will check which row is changed
   * in the source layer, if the corresponding header is found and set
   * to synchronize in this model, its settings will be updated.
   * @return a boolean flag indicates if the data in the table model has
   *         really been changed.
   */
  public boolean updateWithAnother(BhpSeismicTableModel source) {
    boolean myChange = false;
    boolean changed;
    String name;
    boolean synch;
    int index;
    int minColCount = Math.min(this.getColumnCount(), source.getColumnCount());
    for (int i=0; i<source.getRowCount(); i++) {
      changed = source.getRowsChanged(i);
      if (changed == false) continue;
      name = (String) source.getValueAt(i, 0);
      index = this.findKey(name);
      if (index < 0) continue;
      synch = ((Boolean) (this.getValueAt(index, 6))).booleanValue();
      if (synch == false) continue;
      if (2 < minColCount) this.setValueAt(source.getValueAt(i, 2), index, 2);
      if (4 < minColCount) this.setValueAt(source.getValueAt(i, 4), index, 4);
      if (7 < minColCount) this.setValueAt(source.getValueAt(i, 7), index, 7);
      myChange = true;
    }
    return myChange;
  }

  /**
   * Changes the selected value by the step if a header is set to synchronize. <br>
   * @param dir change direction. If dir=1, new_value = old_value + step.
   *        If dir=-1, new_value = old_value - step.
   * @return a boolean flag indicates if any data in the table model has
   *         really been changed.
   */
  public boolean stepChange(int dir) {
    // increase all the keys
    boolean reboolean = false;
    StringBuffer result;
    String original;
    Double increInt;
    Boolean synchronize;
    double increment;
    String incrementString;
    int incrementFracDigits = 0;
    NumberFormat formatter = NumberFormat.getNumberInstance();
    formatter.setGroupingUsed(false);
    for (int row=0; row<this.getRowCount(); row++) {
      synchronize = (Boolean) (this.getValueAt(row, 6));
      if (synchronize.booleanValue() == false) continue;
    // there is no synchronization in case of key=*
      original = (this.getValueAt(row, 2).toString().trim());
      if (original.equalsIgnoreCase("*")) continue;
      result = new StringBuffer();
      increInt = (Double) (this.getValueAt(row, 4));
      incrementString = increInt.toString();
      incrementFracDigits = 0;
      if (incrementString.indexOf(".") >= 0) {
        incrementFracDigits = incrementString.length() - 1 -
                              incrementString.indexOf(".");
      }
      formatter.setMaximumFractionDigits(incrementFracDigits);
      increment = increInt.doubleValue();
      try {
        if (BhpSeismicTableModel.rangeIndex(original) > 0) {
          // range
          int position1 = BhpSeismicTableModel.rangeIndex(original);
          int position2 = original.length();
          if (original.indexOf("[") > 0)
            position2 = original.indexOf("[");
          String token1 = original.substring(0, position1);
          String token2 = original.substring(position1+1, position2);
          double start = Double.parseDouble(token1.trim());
          double end = Double.parseDouble(token2.trim());
          result.append(formatter.format(start + increment * dir));
          result.append("-");
          result.append(formatter.format(end + increment * dir));
          if (original.indexOf("[") > 0) {
            String token3 = original.substring(position2, original.length());
            result.append(token3);
          }
        }
        else if (original.indexOf(",")>0 || original.indexOf(";")>0){
          // comma or semicolon list
          String separator;
          if (original.indexOf(",") > 0) separator = ",";
          else separator = ";";
          StringTokenizer stk = new StringTokenizer(original, ",;");
          while(stk.hasMoreTokens()) {
            String token = stk.nextToken();
            double value = Double.parseDouble(token);
            double offvalue = value + increment * dir;
            if (result.length() != 0) result.append(separator);
            result.append(formatter.format(offvalue));
          }
        }
        else {  // single number, * will be handled in exception
          double value = Double.parseDouble(original);
          double offvalue = value + increment * dir;
          result.append(formatter.format(offvalue));
        }
        boolean valid = checkValidChosenRange(result.toString().trim(),
            this.getValueAt(row, 1).toString().trim());
        if (valid) this.setValueAt(result.toString(), row, 2);
        reboolean = valid;
      } catch (Exception ex) {
          ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                              "BhpSeismicTableModel.getKeyValueString Exception :" +
                                                  ex.toString());
     }
    }
    return reboolean;
  }

  /**
   * Adds the named header to the model.
   * @param name name of the new header.
   * @param att original available range of the header.
   */
  public void addRowData(String name, String att) {
    double increment = 0;
    if (att.indexOf("[") > 0) {
      try {
        int index1 = att.indexOf("[")+1;
        int index2 = att.indexOf("]");
        increment = Double.parseDouble(att.substring(index1, index2).trim());
      } catch(Exception ex) {
          ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                              "Can't parseDouble on string.");
      }
    }

    Vector row = new Vector();
    row.add(name.trim());
    row.add(att);
    row.add("*");
    row.add(new Integer(this.getRowCount() + 1));
    row.add(new Double(increment));
    row.add(new Double(0));
    row.add(new Boolean(false));
    row.add(new Boolean(false));
    if (_isSeismic) row.add(new Integer(0));

    this.addRow(row);
  }

  /**
   * Removes all the data.
   */
  public void cleanupTableModel() {
    super.dataVector = new Vector();
  }

  /**
   * Gets the header name of the vertical dimension, which is
   * always the last row of the model.
   * @return header name of the vertical dimension.
   */
  public String getVerticalName() {
    int rowIndex = this.getRowCount() - 1;
    return this.getValueAt(rowIndex, 0).toString();
  }

  /**
   * Gets the current user selected value of the vertical dimension,
   * which is always the last row of the model.
   * @return a double array of size 3 for the settings. The first one
   *         is for the start value, the second for the end value,
   *         and the third for the step value.
   */
  public double[] getVerticalSettings() {   //walk backwards for vertical!
    for (int i=this.getRowCount()-1;i>=0;i--) {
      double[] thisrow = getKeySettingsAtIndex(i);
      if (thisrow[0]!=thisrow[1]) { // ranges have differing start and end values
        return thisrow;
      }
    }

    double[] row = {0,0,0};
    return row;
  }

  /**
   * Gets the current user selected value of the horizontal dimension,
   * which is the first row of the model. (for map views)
   * @return a double array of size 3 for the settings. The first one
   *         is for the start value, the second for the end value,
   *         and the third for the step value.
   */
  public double[] getHorizontalMapSettings() {
    return getKeySettingsOfName(getMapHorizontalKeyName());
  }

  /**
   * Checks if the current setting of the model is valid for a map layer. <br>
   * To make a map layer, two headers need to be a range rather
   * than single value. The last fields for vertical range and another
   * field as horizontal range.
   * @return true if the current setting is valid for a making a map layer,
   *         false otherwise.
   */
  public boolean isValidMapSetting() {
    int rangeNumber = 0;
    for (int i=0; i<this.getRowCount()-1; i++) {    // last row is always range
      // for model and event, first row does not count
      if (i == 0 && !_isSeismic) continue;
      if (BhpSeismicTableModel.rangeIndex(this.getValueAt(i,2).toString()) > 0 ||
          this.getValueAt(i, 2).toString().trim().equals("*"))
        rangeNumber++;
    }
    if (rangeNumber == 1) return true;
    return false;
  }

  /**
   * Finds the fields that can be used to draw a horizontal axis for a
   * cross-section plot. <br>
   * Usually, field with selected value being a range is good for the
   * purpose. However, if no such field is found, the very first field
   * is returned.
   */
  public String getXSectionHorizontalKeyName() {
    int rangeIndex = -1;
    for (int i=0; i<this.getRowCount()-1; i++) {    // last row does not count
      if (BhpSeismicTableModel.rangeIndex(this.getValueAt(i,2).toString()) > 0 ||
          this.getValueAt(i, 2).toString().trim().equals("*")) {
      rangeIndex = i;
      break;
    }
    }
    if (rangeIndex == -1) rangeIndex = 0;
    return this.getValueAt(rangeIndex, 0).toString();
  }

  /**
   * Finds the horizontal key for the map. <br>
   * For a valid map setting, two headers need to be a range rather
   * than single value. The last fields for vertical range and another
   * field as horizontal range. This method returns the field name of
   * that horizontal range. If no such field exists, it returns an
   * empty string.
   */
  public String getMapHorizontalKeyName() {
    int rangeIndex = -1;
    for (int i=0; i<this.getRowCount()-1; i++) {    // last row is always range
      if (i == 0 && !_isSeismic) continue;
      if (BhpSeismicTableModel.rangeIndex(this.getValueAt(i,2).toString()) > 0 ||
          this.getValueAt(i, 2).toString().trim().equals("*")) {
        rangeIndex = i;
        break;
      }
    }
    if (rangeIndex == -1) return "";
    return this.getValueAt(rangeIndex, 0).toString();
  }

  /**
   * Gets the settins of the given key. <br>
   * @return a double array of size 3 for the settings. The first one
   *         is for the start value, the second for the end value,
   *         and the third for the step value.
   */
  public double[] getKeySettingsOfName(String name) {
    int rangeIndex = this.findKey(name);
    if (rangeIndex == -1) {
      double[] result = {-1.0, -1.0, -1.0};
      return result;
    }
    return getKeySettingsAtIndex(rangeIndex);
  }

  /**
   * Finds the trace with a specific header field value. <br>
   * There might be multiple traces that carries the same value
   * for a specific header field. In this case, the first trace
   * that matches the condition will be returned.
   * To optimize performance, this method will first try to
   * calculate the number if the requested field is one of
   * the keys in the table model. It not, the trace meta data
   * is searched for the match.
   * @param fname name of the header field.
   * @param fvalue value of the header field.
   * @return the id number of the matched trace.
   *         If there is no such trace, -999 is returned.
   */
  public int findTraceIdFromField(String fname, double fvalue) {
    int result = -999;

    int findex = findKey(fname);
    if (findex == -1) {
      return result;
    }
    if (!isValid(fvalue, findex)) {
      return -998; // out of range value. A match won't be found.
    }

    int forder = ((Integer) (this.getValueAt(findex, 3))).intValue();

    int[] fieldNumber = new int[getRowCount()-1];
    int[] fieldOrder = new int[getRowCount()-1];
    int fvaluePosition = -1;
    double[] fieldSetting = new double[3];
    int tmpNumber = 0;
    for (int i=0; i<(getRowCount()-1); i++) {
      fieldSetting = getKeySettingsAtIndex(i);
      if (fieldSetting[0]==-1 || fieldSetting[1]==-1 || fieldSetting[2]==-1)
      {
        return result;
      }
      else if (fieldSetting[0] == fieldSetting[1])
      {
        tmpNumber = 1;
      }
      else
      {
        tmpNumber = (int) (1+(fieldSetting[1]-fieldSetting[0])/fieldSetting[2]);
      }
      fieldNumber[i] = tmpNumber;
      fieldOrder[i] = ((Integer) (this.getValueAt(i, 3))).intValue();
      if (fieldOrder[i] == forder) {
        if (fieldSetting[2] == 0) {
          if (fvalue == fieldSetting[0]) {
            fvaluePosition = 0;
          }
        }
        else
        {
          fvaluePosition = (int) ((fvalue - fieldSetting[0]) / fieldSetting[2]);
        }
      }
    }

    if (fvaluePosition == 0) {
      result = 0;
    }
    else if (fvaluePosition > 0) {
      result = fvaluePosition;
      for (int j=0; j<fieldOrder.length; j++) {
        if (fieldOrder[j] > forder) {
          result = result * fieldNumber[j];
        }
      }
    }

    return result;
  }

  private boolean isValid(double fvalue, int findex) {
    double[] keySettings = getKeySettingsAtIndex(findex);
    boolean validValue = false;
    if ((fvalue >= keySettings[0] && fvalue <= keySettings[1]) ||
        (fvalue >= keySettings[1] && fvalue <= keySettings[0])) {
          // value within the range, check step
//          if (keySettings[2] == 0) {
//              validValue = true;
//          } else if ((fvalue - keySettings[0]) % keySettings[2] == 0) {
//              validValue = true;
//          }
      validValue = true;
    }
    return validValue;
  }

  private double[] getKeySettingsAtOrder(int order) {
    double[] result = {-1.0, -1.0, -1.0};
    int rowIndex = -1;

    int keyOrder;
    for (int i=0; i<this.getRowCount(); i++) {
      keyOrder = Integer.parseInt(this.getValueAt(i, 3).toString());
      if (order == keyOrder) {
        rowIndex = i;
        break;
      }
    }

    if (rowIndex == -1) {
        ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                            "BhpSeismicTableModel.getKeySettingAtOrder error for order " +
                                                order);
      return result;
    }
    return getKeySettingsAtIndex(rowIndex);
  }

  // start, end, step
  private double[] getKeySettingsAtIndex(int rowIndex) {
    double[] result = {-1.0, -1.0, -1.0};
    String keyAtt = this.getValueAt(rowIndex, 1).toString();
    String keyValue = getKeyValueString(rowIndex);
    if (keyValue.equals("*"))  {
      int rangeIndex = BhpSeismicTableModel.rangeIndex(keyAtt);
      result[0] = Double.parseDouble(keyAtt.substring(0, rangeIndex).trim());
      result[1] = Double.parseDouble(keyAtt.substring(rangeIndex+1, keyAtt.indexOf("[")).trim());
      result[2] = Double.parseDouble(keyAtt.substring(keyAtt.indexOf("[")+1, keyAtt.indexOf("]")).trim());
    }
    else if (keyValue.indexOf(",") != -1) {     // key list
      result[0] = Double.parseDouble(keyValue.substring(0, keyValue.indexOf(",")).trim());
      result[1] = Double.parseDouble(keyValue.substring(keyValue.lastIndexOf(",")+1, keyValue.length()).trim());
      result[2] = -1.0;
    }
    else if (keyValue.indexOf("^") != -1) {     // key list
      result[0] = Double.parseDouble(keyValue.substring(0, keyValue.indexOf("^")).trim());
      result[1] = Double.parseDouble(keyValue.substring(keyValue.lastIndexOf("^")+1, keyValue.length()).trim());
      result[2] = -1.0;
    }
    else if (BhpSeismicTableModel.rangeIndex(keyValue) != -1) {     // range
      int rangeIndex = BhpSeismicTableModel.rangeIndex(keyValue);
      if (keyValue.indexOf("[") != -1) {
        result[0] = Double.parseDouble(keyValue.substring(0, rangeIndex).trim());
        result[1] = Double.parseDouble(keyValue.substring(rangeIndex+1, keyValue.indexOf("[")).trim());
        result[2] = Double.parseDouble(keyValue.substring(keyValue.indexOf("[")+1, keyValue.indexOf("]")).trim());
      }
      else {
        result[0] = Double.parseDouble(keyValue.substring(0, rangeIndex).trim());
        result[1] = Double.parseDouble(keyValue.substring(rangeIndex+1, keyValue.length()).trim());
        result[2] = Double.parseDouble(keyAtt.substring(keyAtt.indexOf("[")+1, keyAtt.indexOf("]")).trim());
      }
    }
    else {      // simple value
      double value = Double.parseDouble(keyValue);
      result[0] = value;
      result[1] = value;
      result[2] = 0.0;
    }

    return result;
  }

  public boolean checkMaxTraceNumber(BhpViewerBase viewer) {
    int maxTrace = this.estimateMaxTraceNumber();
    int traceLimit = BhpPropertyManager.DEFAULT_MAX_TRACE_NUM;
    try {
      traceLimit = Integer.parseInt(viewer.getPropertyManager().getProperty("bhpMaxTraceNum"));
    } catch (Exception ex) {
        ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                            "Can't parse integer value.");
    }
    if (maxTrace > traceLimit) {
      int confirmValue = JOptionPane.showConfirmDialog(viewer,
          "Load "+maxTrace+" traces?", "Please Confirm",
          JOptionPane.YES_NO_OPTION);
      if (confirmValue == JOptionPane.NO_OPTION) return false;
    }
    return true;
  }

  private int estimateMaxTraceNumber() {
    int maxNumber = 1;
    int rowFactor = 1;
    String original;
    String available;
    for(int i=0; i<getRowCount()-1; i++) {
      rowFactor = 1;
      available = getValueAt(i, 1).toString();
      original = getKeyValueString(i);
      if (original.equals("*")) original = available;
      try {
        if (BhpSeismicTableModel.rangeIndex(original) > 0) {
          int position1 = BhpSeismicTableModel.rangeIndex(original);
          int position2 = original.length();
          if (original.indexOf("[") > 0)
            position2 = original.indexOf("[");
          String token1 = original.substring(0, position1);
          String token2 = original.substring(position1+1, position2);
          double start = Double.parseDouble(token1.trim());
          double end = Double.parseDouble(token2.trim());
          double step = 1.0;
          if (original.indexOf("[") > 0) {
            String token3 = original.substring(position2+1, original.indexOf("]"));
            step = Double.parseDouble(token3.trim());
          }
          else if (available.indexOf("[") > 0) {
            String token3 = available.substring(available.indexOf("[")+1, available.indexOf("]"));
            step = Double.parseDouble(token3.trim());
          }
          rowFactor = (int) (Math.abs(start-end)/step);
        }
        else if (original.indexOf(",")>0 || original.indexOf(";")>0 ||
                 original.indexOf("^")>0){
          // comma or semicolon list, or ^ list for arbitrary traverse
          StringTokenizer stk = new StringTokenizer(original, ",;^");
          rowFactor = stk.countTokens();
        }
        else rowFactor = 1;
      }
      catch (Exception ex) {
          ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                              "Parse error");
        rowFactor = 1;
      }
      if (rowFactor <= 1) rowFactor = 1;
      maxNumber = maxNumber * rowFactor;
    }
    return maxNumber;
  }

  /**
   * Constructs a string that can be used to run bhpio
   * according to the data in the model.
   */
  public String getParameterString() {
    StringBuffer nameBuffer= new StringBuffer();
    StringBuffer valueBuffer = new StringBuffer();
    StringBuffer nearestBuffer = new StringBuffer();
    StringBuffer binsizeBuffer = new StringBuffer();
    int[] orders = new int[this.getRowCount()];
    for (int i=0; i<orders.length; i++) orders[i] = -1;
    for (int j=0; j<orders.length; j++) {
      int order = Integer.parseInt(this.getValueAt(j, 3).toString()) - 1;
      //Integer order = (Integer) this.getValueAt(j, 3);  // the value of order
      if (order < 0) order = 0;
      if (orders[order] != -1) {
        // user made some unreasonable choice
        order = 0;
        while(order < orders.length && orders[order] != -1)
          order++;
      }
      orders[order] = j;
    }
    for (int k=0; k<orders.length; k++) {
      String name = (String) (this.getValueAt(orders[k], 0));
      String value = getKeyValueString(orders[k]);
      //String value = (String) (this.getValueAt(orders[k], 3));
      Boolean nearest = new Boolean(false);
      if (7<getColumnCount())
      {
        nearest = (Boolean) (this.getValueAt(orders[k], 7));
      }
      if (nameBuffer.length() != 0) {
        nameBuffer.append(",");
      }
      nameBuffer.append(name.trim());
      if (valueBuffer.length() != 0) {
        valueBuffer.append(":");
      }
      valueBuffer.append(value);   // need apply offset first
      if (nearestBuffer.length() != 0) {
        nearestBuffer.append(",");
      }
      if (nearest.booleanValue()) {
        nearestBuffer.append("1");
      }
      else nearestBuffer.append("0");
      if (_isSeismic && (8<getColumnCount())) {
        Integer binsize = (Integer) (this.getValueAt(orders[k], 8));
        if (binsizeBuffer.length() != 0) {
          binsizeBuffer.append(",");
        }
        binsizeBuffer.append(binsize.intValue());
      }
    }
    if (valueBuffer.toString().indexOf("^") > 0) {
      adjustForArbitraryTraverse(nameBuffer, valueBuffer);
    }
    String re = " keys=" + nameBuffer +
                " keylist=" + valueBuffer +
                " endian=1" +
                " nearest=" + nearestBuffer;
    if (_isSeismic) {
      re = re + " binsize=" + binsizeBuffer;
    }

    return re;
  }

  public void matchArbitraryTraverse() {
    int index1 = -1;
    int index2 = -1;
    String value1 = null;
    String value2 = null;
    String value = null;
    for(int i=0; i<getRowCount()-1; i++) {
      value = this.getValueAt(i, 2).toString();
      if (value.indexOf("^") > 0) {
        if (value1 == null) {
          value1 = value;
          index1 = i;
        }
        else if (value2 == null) {
          value2 = value;
          index2 = i;
        }
        else {
          // allow no more
          this.setValueAt("*", i, 2);
        }
      }
    }
    if (value1!=null && value2!=null) {
      // match these two
      String[] values1 = value1.split("^");
      String[] values2 = value2.split("^");
      if (values1.length != values2.length) {
        if (values1.length > values2.length) {
          StringBuffer newValue = new StringBuffer();
          for (int j=0; j<values2.length; j++) {
            if (j!=0) newValue.append("^");
            newValue.append(values1[j]);
          }
          this.setValueAt(newValue, index1, 2);
        }
        else {
          StringBuffer newValue = new StringBuffer();
          for (int j=0; j<values1.length; j++) {
            if (j!=0) newValue.append("^");
            newValue.append(values2[j]);
          }
          this.setValueAt(newValue, index2, 2);
        }
      }
    }
    else if (value1!=null && value2==null) {
      // find only one, make value1 single value
      String firstValue = value1.substring(0, value1.indexOf("^"));
      this.setValueAt(firstValue, index1, 2);
    }
  }

  private void adjustForArbitraryTraverse(StringBuffer names, StringBuffer values) {
    boolean findKey1 = false;
    boolean findKey2 = false;
    boolean findKey3 = false;
    String name = "";
    String name1 = "";
    String name2 = "";
    String name3 = "";
    String value = "";
    String value1 = "";
    String value2 = "";
    String value3 = "";
    StringTokenizer stkv = new StringTokenizer(values.toString(), ":");
    StringTokenizer stkn = new StringTokenizer(names.toString(), ",");
    while(stkv.hasMoreTokens() && stkn.hasMoreTokens()) {
      value = stkv.nextToken();
      name = stkn.nextToken();
      if (value.indexOf("^") > 0) {
        if (!findKey1 && !findKey2) {
          findKey1 = true;
          value1 = value;
          name1 = name;
        }
        else if (findKey1 && !findKey2) {
          findKey2 = true;
          value2 = value;
          name2 = name;
        }
      }
      else {
        if (findKey1 && findKey2) {
          findKey3 = true;
          value3 = value;
          name3 = name;
        }
      }
    }

    if (!findKey1 || !findKey2) {
      // There is a problem, don't continue
      return;
    }
    // need to reconstruct names and values
    names.delete(0, names.length());
    values.delete(0, values.length());
    names.append(name1);
    names.append("," + name2);
    if (findKey3) names.append("," + name3);
    StringTokenizer stkv1 = new StringTokenizer(value1, "^");
    StringTokenizer stkv2 = new StringTokenizer(value2, "^");
    while(stkv1.hasMoreTokens() && stkv2.hasMoreTokens()) {
      if (values.length() != 0) values.append("\\^");
      values.append(stkv1.nextToken() + "," + stkv2.nextToken());
    }
    if (findKey3) values.append(":" + value3);
  }

  /**
   * Sets the chosen range of the given row. <br>
   * Chosen range is the third colume of the model.
   * Instead of call setValueAt directly, this method
   * validate the new value. Change is made only if
   * the new value is valid.
   * @param value new value for that cell.
   * @param row the row that needs to be changed.
   * @return a boolean flag indicates if the
   *         new value has been really applied.
   */
  public boolean setChosenRangeAt(Object value, int row) {
    String ov = this.getValueAt(row, 2).toString().trim();
    this.setValueAt(value, row, 2);
    String nv = this.getValueAt(row, 2).toString().trim();
    boolean changed = false;
    if (nv.equals(ov))  {
      changed = false;
      return changed;
    }
    else if (BhpSeismicTableModel.rangeIndex(nv) == -1) {
      // not a range
      changed = true;
      return changed;
    }

    // handle the range case
    if (BhpSeismicTableModel.rangeIndex(ov) == -1) {
      // was not a range
      changed = true;
      return changed;
    }

    String avaiRange = this.getValueAt(row, 1).toString().trim();
    String defaultStep = avaiRange.substring(avaiRange.indexOf("[")+1, avaiRange.indexOf("]")).trim();
    if (ov.equals("*")) ov = avaiRange;

    int orangeIndex = BhpSeismicTableModel.rangeIndex(ov);
    int nrangeIndex = BhpSeismicTableModel.rangeIndex(nv);

    String[] oStrings = {"", "", ""};
    String[] nStrings = {"", "", ""};
    oStrings[0] = ov.substring(0, orangeIndex).trim();
    nStrings[0] = nv.substring(0, nrangeIndex).trim();

    if (ov.indexOf("[") > 0) {
      oStrings[1] = ov.substring(orangeIndex+1, ov.indexOf("[")).trim();
      oStrings[2] = ov.substring(ov.indexOf("[")+1, ov.indexOf("]")).trim();
    }
    else {
      oStrings[1] = ov.substring(orangeIndex+1).trim();
      oStrings[2] = defaultStep;
    }
    if (nv.indexOf("[") > 0) {
      nStrings[1] = nv.substring(nrangeIndex+1, nv.indexOf("[")).trim();
      nStrings[2] = nv.substring(nv.indexOf("[")+1, nv.indexOf("]")).trim();
    }
    else {
      nStrings[1] = nv.substring(nrangeIndex+1).trim();
      nStrings[2] = defaultStep;
    }

    for (int k=0; k<3; k++) {
      if (!(oStrings[k].equals(nStrings[k]))) {
        try {
          double odouble = Double.parseDouble(oStrings[k]);
          double ndouble = Double.parseDouble(nStrings[k]);
          if (odouble != ndouble) return true;
        }
        catch (Exception ex) {
          return true;
        }
      }
    }
    //if (!(oStrings[0].equals(nStrings[0]))) return true;
    //if (!(oStrings[1].equals(nStrings[1]))) return true;
    //if (!(oStrings[2].equals(nStrings[2]))) return true;
    return changed;
  }

  /**
   * Sets the value of a specific cell. <br>
   * @param value the new cell value.
   * @param row the row index.
   * @param column the column index.
   */
  public void setValueAt(Object value, int row, int column) {
    if (value.toString().length() == 0) return;

    // check if the chosen range is within the available range
    if (column == 2) {
      //String avaiValue = this.getValueAt(row, 1).toString().trim();
      boolean valid = checkValidChosenRange(value.toString().trim(),
          this.getValueAt(row, 1).toString().trim());
      if (valid == false) return;
    }

    if (this.getColumnClass(column).getName().endsWith("Integer")) {
      try {
        Integer valueObject = new Integer(value.toString());
        super.setValueAt(valueObject, row, column);
      }
      catch (Exception ex) {
          ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                              "invalid integer " + value.toString());
        return;
      }
    }
    else if (this.getColumnClass(column).getName().endsWith("Boolean")) {
      try {
        Boolean valueObject = new Boolean(value.toString());
        super.setValueAt(valueObject, row, column);
      }
      catch (Exception ex) {
          ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                              "invalid boolean " + value.toString());
        return;
      }
    }
    else {
      super.setValueAt(value, row, column);
    }
  }

  /**
   * Finds out if a specific cell is editable. <br>
   * In this table, the first two columns are not editable.
   * They are header name and original range.
   * The last row is also not editable. The vertical range
   * is supposed to be changed with <code>{@link VerticalRangePanel}</code>.
   */
  public boolean isCellEditable(int row, int col) {
    if (col < 2) return false;
    if (row == this.getRowCount()-1) return false;
    return true;
  }

  /**
   * Gets the object class of a specific column.
   */
  public Class getColumnClass(int c) {
    String s = "";
    Integer i = new Integer(0);
    Double d = new Double(0);
    Boolean b = new Boolean(true);
    switch(c) {
      case 0: return s.getClass();    // header name
      case 1: return s.getClass();    // full range
      case 2: return s.getClass();    // chosen range
      case 3: return i.getClass();    // order
      case 4: return d.getClass();    // increment
      case 5: return d.getClass();    // offset
      case 6: return b.getClass();    // synchronize
      case 7: return b.getClass();    // interpolation
      case 8: return i.getClass();    // binsize
      default: return s.getClass();
    }
  }

  /**
   * Finds the row index of a specific header field.
   * @param name name of the header field.
   * @return the row index of the field.
   *         If the name is not presented in the model, -1 is returned.
   */
  public int findKey(String name) {
    String nm;
    for (int i = 0; i < this.getRowCount(); i++) {
      nm = (String) (this.getValueAt(i, 0));
      if (nm.equals(name)) {
        return i;
      }
    }
    return -1;
  }

  private boolean checkValidChosenRange(String svalue, String avaiValue) {
    //int avaiStart = Integer.parseInt(avaiValue.substring(0, avaiValue.indexOf("-")).trim());
    //int avaiEnd = Integer.parseInt(avaiValue.substring(avaiValue.indexOf("-")+1, avaiValue.indexOf("[")).trim());
    int avaiRangeIndex = BhpSeismicTableModel.rangeIndex(avaiValue);
    double avaiStart = Double.parseDouble(avaiValue.substring(0, avaiRangeIndex).trim());
    double avaiEnd = Double.parseDouble(avaiValue.substring(avaiRangeIndex+1, avaiValue.indexOf("[")).trim());
    double avaiStep = Double.parseDouble(avaiValue.substring(avaiValue.indexOf("[")+1, avaiValue.indexOf("]")).trim());
    if (avaiStart > avaiEnd) {
      double avaiTmp = avaiStart;
      avaiStart = avaiEnd;
      avaiEnd = avaiTmp;
    }
    boolean validRange = false;

    if (svalue.equals("*")) {
      validRange = true;
    }
    else if (svalue.indexOf(",") != -1) {   // key list
      boolean allOkay = true;
      StringTokenizer stk = new StringTokenizer(svalue, ",");
      String token;
      double tokenValue;
      try {
        while(stk.hasMoreTokens()) {
          token = stk.nextToken().trim();
          tokenValue = Double.parseDouble(token);
          if (tokenValue < avaiStart || tokenValue > avaiEnd) {
            allOkay = false;
            break;
          }
        }
      }
      catch (Exception ex) {
        allOkay = false;
      }
      if (allOkay) {
        validRange = true;
      }
    }
    else if (BhpSeismicTableModel.rangeIndex(svalue) != -1) {   // range
      try {
        int dashP = BhpSeismicTableModel.rangeIndex(svalue);
        double chosenS = Double.parseDouble(svalue.substring(0, dashP));
        double chosenE = -1;
        if (svalue.indexOf("[") != -1)
        {
          chosenE = Double.parseDouble(svalue.substring(dashP+1, svalue.indexOf("[")));
        }
        else
        {
          chosenE = Double.parseDouble(svalue.substring(dashP+1));
        }
        if (chosenS > chosenE) {
          double chosenTmp = chosenS;
          chosenS = chosenE;
          chosenE = chosenTmp;
        }
        if (chosenS >= avaiStart && chosenE <= avaiEnd) {
          validRange = true;
        }
        //System.out.println(avaiStart + " - " + avaiEnd + " : " + chosenS + " - " + chosenE);
      }
      catch (Exception ex) {
        validRange = false;
      }
    }
    else if (svalue.indexOf("^") != -1) {   // for arbitry traverse
      StringTokenizer stk = new StringTokenizer(svalue, "^");
      double tokenValue;
      boolean allOkay = true;
      while(stk.hasMoreTokens()) {
        try {
          tokenValue = Double.parseDouble(stk.nextToken());
          if (tokenValue < avaiStart || tokenValue > avaiEnd) {
            allOkay = false;
            //System.out.println(tokenValue + "out of range " + avaiStart + " " + avaiEnd);
            break;
          }
        }
        catch (Exception ex) {
          allOkay = false;
          //System.out.println("Exception " + ex.toString());
          break;
        }
      }
      if (allOkay) validRange = true;
    }
    else {  // a simple value
      try {
        double chosenV = Double.parseDouble(svalue);
        if (chosenV >= avaiStart && chosenV <= avaiEnd) {

          validRange = true;
        }
      } catch (Exception ex) {
        validRange = false;
      }
    }

    return validRange;
  }

  /**
   * Gets the chosen range with offset applied for the given row.
   */
  public String getKeyValueString(int row) {
    String original = ((String) (this.getValueAt(row, 2))).trim();
    double offset = 0;
    try {
      offset = Double.parseDouble(this.getValueAt(row, 5).toString());
    }
    catch (Exception offsetEx) {
        ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                            "BhpSeismicTableModel.getKeyValueString Exception:" +
                                            "    unrecognizable offset value " + this.getValueAt(row, 5));
      offset = 0;
    }
    if (offset == 0) return original;
    try {
      if (BhpSeismicTableModel.rangeIndex(original) > 0) {
        // range
        StringBuffer result = new StringBuffer();
        int position1 = BhpSeismicTableModel.rangeIndex(original);
        int position2 = original.length();
        if (original.indexOf("[") > 0)
          position2 = original.indexOf("[");
        String token1 = original.substring(0, position1);
        String token2 = original.substring(position1+1, position2);
        double start = Double.parseDouble(token1.trim());
        double end = Double.parseDouble(token2.trim());
        result.append(start+offset);
        result.append("-");
        result.append(end+offset);
        if (original.indexOf("[") > 0) {
          String token3 = original.substring(position2, original.length());
          result.append(token3);
        }
        return result.toString();
      }
      else if (original.indexOf(",")>0 || original.indexOf(";")>0 ||
               original.indexOf("^")>0){
        // comma or semicolon list, or ^ list for arbitrary traverse
        String separator;
        if (original.indexOf(",") > 0) separator = ",";
        else if (original.indexOf("^") > 0) separator = "^";
        else separator = ";";
        StringBuffer result = new StringBuffer();
        StringTokenizer stk = new StringTokenizer(original, ",;^");
        while(stk.hasMoreTokens()) {
          String token = stk.nextToken();
          double value = Double.parseDouble(token);
          double offvalue = value + offset;
          if (result.length() != 0) result.append(separator);
          result.append(offvalue);
        }
        return result.toString();
      }
      else if (original.equals("*")) {
        // handle * separately instead of using the exception to handle it
        return original;
      }
      else {  // single number
        double value = Double.parseDouble(original);
        double offvalue = value + offset;
        return Double.toString(offvalue);
      }
    }
    catch (Exception ex) {
        ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                            "BhpSeismicTableModel.getKeyValueString Exception :" +
                                            "    Fail to apply offset " + offset);
      return original;
    }
  }

  public int getMissingDataSelection() {
    return missingDataSelection;
  }
  public void setMissingDataSelection(int which) {
    missingDataSelection = which;
  }
}
