/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import com.gwsys.seismic.indexing.HeaderFieldEntry;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class is a container to store the settings of a
 *               horizontal trace axis used in
 *               <code>{@link BhpPlotXV}</code>.
 *               <br><br>
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpTraceAxisObject {
    /**
     * Header field based on which the trace axis is drawn.
     */
    public final HeaderFieldEntry _field;
    /**
     * The name of the header field.
     */
    public final String _name;
    /**
     * The angle of the axis labels. <br>
     * The default value is 0.0.
     */
    public double _labelAngle;
    /**
     * The step value of the axis,
     * the step between two neighboring ticks.
     * The default value is 10.0.
     */
    public double _step;
    /**
     * Space between two neighboring ticks in pixels. <br>
     * The default value is 60.
     */
    public double _tickSpace;
    /**
     * Flag fro drawing the axis base line. <br>
     * The default value is true.
     */
    public boolean _drawLine;
    /**
     * Flag for the label. <br>
     * ALL_LABELS, BIG_LABELS, and SML_LABELS.
     * The default value is ALL_LABELS.
     */
    public int _labelFlag;
    /**
     * Label limit used together with the label flag. <br>
     * The default value is 0.0.
     */
    public double _labelLimit;

    /**
     * Construts a new instance with the spcified header fields. <br>
     * The method set the other unspecified fields to their defaults.
     * @param field the header field based on which the axis will be drawn.
     */
    public BhpTraceAxisObject(HeaderFieldEntry field) {
        _field = field;
        _name = _field.getName();
        _labelAngle = 0.0;
        _tickSpace = 60.0;
        _drawLine = true;
        //_labelFlag = BhpLinearLabelGenerator.ALL_LABELS;
        _labelLimit = 0;
        _step = 10.0;
    }

    /**
     * Constructs a new instance with specified values.
     * @param name the name of the header fields based on which the axis will be drawn.
     * @param step the axis step value.
     * @param line the draw base line flag.
     * @param gap the gap between two neighboring ticks.
     * @param angle the label angle.
     * @param limit the label limit.
     * @param flag the label flag.
     */
    public BhpTraceAxisObject(String name, double step, boolean line, double gap,
                              double angle, double limit, int flag) {
        _field = null;
        _name = name;
        _labelAngle = angle;
        _tickSpace = gap;
        _step = step;
        _drawLine = line;
        _labelFlag = flag;
        _labelLimit = limit;
    }
}
