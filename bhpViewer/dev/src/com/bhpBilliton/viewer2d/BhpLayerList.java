/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import javax.help.CSH;

import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpBilliton.viewer2d.util.IconResource;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.util.Enumeration;

import java.util.Hashtable;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is the list displayed on the left panel in the BhpWindow.
 *               For each of the layer that displayed in the right side BhpPlot,
 *               there is a corresponding entry in this list. <br>
 *               The order of the list also indicates the layer stack order, with
 *               the layer on the top of the display stack at the top of the list.
 *               Each entry in the list takes the form "(S|M|E|U) num : name".
 *               The first letter indicates the data type, S for seismic, M for
 *               model, E for event, and U for unknow. "num" stands for the layer
 *               indentification number, which is an integer. "name" is the user
 *               defined name of the layer. Different icons are also used for
 *               different data types. In the case of event layer, the text
 *               will be drawn using the same color as the event polyline itself.
 *               When a layer is hidden, its entry in the list will be grayed out
 *               accordingly. <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpLayerList extends JList
        implements DropTargetListener, DragSourceListener, DragGestureListener {
    private ImageIcon _seismicIcon = null;
    private ImageIcon _modelIcon = null;
    private ImageIcon _eventIcon = null;
    private ImageIcon _unknownIcon = null;

    private DefaultListModel _model;
    private int _axisLayerId;

    private DropTarget _dropTarget = null;
    private DragSource _dragSource = null;
    private int _dragIndex = -1;
    private BhpWindow _window;

    /**
     * Constructs a new instance.
     */
    public BhpLayerList(BhpWindow win) {
        super();
        CSH.setHelpIDString(this,"layers_list");
        _window = win;
        _model = new DefaultListModel();
        _axisLayerId = -1;
        this.setModel(_model);
        this.setCellRenderer(new MyCellRenderer());

        //this.setDragEnabled(true);
        this.addMouseListener(new MyMouseListener());
        _dropTarget = new DropTarget(this, this);
        _dragSource = new DragSource();
        _dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_MOVE, this);

        try {
            _seismicIcon = IconResource.getInstance().getImageIcon(IconResource.SEISMICLAYER_ICON);
            _modelIcon = IconResource.getInstance().getImageIcon(IconResource.MODELLAYER_ICON);
            _eventIcon = IconResource.getInstance().getImageIcon(IconResource.EVENTLAYER_ICON);
            _unknownIcon = IconResource.getInstance().getImageIcon(IconResource.UNKNOWNLAYER_ICON);
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_window.getBhpViewerBase(), Thread.currentThread().getStackTrace(),
                                                "BhpLayerList.constructor failed to load images  " +
                                                    ex.toString());
        }
    }

    public void dragEnter(DropTargetDragEvent event) {
        event.acceptDrag(DnDConstants.ACTION_MOVE);
    }
    public void dragExit(DropTargetEvent event) {
    }
    public void dragOver(DropTargetDragEvent event) {
    }
    public void drop(DropTargetDropEvent event) {
        try {
            Transferable transferable = event.getTransferable();
            if (transferable.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                event.acceptDrop(DnDConstants.ACTION_MOVE);
                String transText = (String) (transferable.getTransferData(DataFlavor.stringFlavor));
                int transid = Integer.parseInt(transText);
                ListObject transobj = this.getListObjectForId(transid);
                //int sourceIndex = this.getSelectedIndex();
                int sourceIndex = _dragIndex;
                _dragIndex = -1;
                int destIndex = this.locationToIndex(event.getLocation());
                if (destIndex == -1) destIndex = _model.getSize();
                if ((sourceIndex != destIndex) && (destIndex >= 0)) {
                    //System.out.println(sourceIndex + "->" + destIndex + " : " + this.getSelectedIndex());
                    if (sourceIndex < destIndex) {
                        // move back
                        if (destIndex == _model.getSize())
                            _model.addElement(transobj);
                        else
                            _model.insertElementAt(transobj, destIndex);
                        _model.removeElementAt(sourceIndex);
                    }
                    else {
                        // move up
                        _model.removeElementAt(sourceIndex);
                        _model.insertElementAt(transobj, destIndex);
                    }
                    this.setSelectedValue(transobj, false);
                    BhpLayer layer = transobj._layer;
                    int order = this.getSelectedIndex();
                    order = _model.size() - order - 1;
                    //System.out.println("Move layer " + layer.getIdNumber() + " to " + order);
                    layer._window.getBhpPlot().getMainView().setBhpLayerOrder(layer, order);
                }
            }else{
              DataFlavor[] flavor=transferable.getTransferDataFlavors();
              if (transferable.getTransferData(flavor[0]) instanceof Hashtable){
                Hashtable table=(Hashtable)transferable.getTransferData(flavor[0]);
                // create a new layer
                _window.createLayerFromDragDrop(table);
              }
            }
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_window.getBhpViewerBase(), Thread.currentThread().getStackTrace(),
                                                "BhpLayerList.drop exception: " + ex.toString());
        }
    }
    public void dropActionChanged(DropTargetDragEvent event) {
    }
    public void dragDropEnd(DragSourceDropEvent event) {
        //System.out.println("dragDropEnd");
    }
    public void dragEnter(DragSourceDragEvent event) {
    }
    public void dragExit(DragSourceEvent event) {
    }
    public void dragOver(DragSourceDragEvent event) {
    }
    public void dropActionChanged(DragSourceDragEvent event) {
    }
    public void dragGestureRecognized(DragGestureEvent event) {
        //Object sobj = this.getSelectedValue();
        if (_dragIndex<0)
            return;
        Object sobj = this.getModel().getElementAt(_dragIndex);
        if (sobj != null) {
            StringSelection stext = new StringSelection("" + ((ListObject)sobj).getId());
            //System.out.println("dragGrstureRecognized " + ((ListObject)sobj).getId());
            _dragSource.startDrag(event, DragSource.DefaultMoveDrop, stext, this);
        }
    }

    /**
     * This method is called when a BhpLayer is changed and
     * a BhpLayerEvent is broadcasted.
     */
    public void receiveBhpLayerEvent(BhpLayerEvent e) {
        boolean update = false;
        if ((e.getTypeFlag() & BhpViewer.NAME_FLAG) != 0) {
            // layer name is changed
            int id = e.getSource().getIdNumber();
            String name = e.getSource().getLayerName();
            ListObject obj = getListObjectForId(id);
            if (obj != null) {
                obj.setName(name);
                update = true;
            }
        }
        if ((e.getTypeFlag() & BhpViewer.VISIBLE_FLAG) != 0) {
            update = true;
        }
        if ((e.getTypeFlag() & BhpViewer.EVENTATT_FLAG) != 0) {
            // event layer color changed
            int id = e.getSource().getIdNumber();
            ListObject obj = getListObjectForId(id);
            if (obj != null) {
                obj.setSymbolColor(e.getSource().getShapeListLayer().getShape(0).
                                    getAttribute().getLineColor());
                update = true;
            }
        }
        // it does not handle event BhpLayer.ORDER_FLAG. It is not a
        // synchronized attribute anymore.
        // No such event will be generated.
        // Also, change order changes the list order here first.
        // And the window will be called from here to change display order.

        if (update) {
            this.invalidate();
            this.repaint();
        }
    }

    /**
     * Adds a new layer to the list.
     */
    public void addBhpLayer(BhpLayer layer) {
        int type = BhpViewer.BHP_DATA_TYPE_UNKNOWN;
        type = layer.getDataType();
        Color clr = this.getForeground();
        if (layer instanceof BhpEventLayer) {
            clr = layer.getShapeListLayer().getShape(0).getAttribute().getLineColor();
        }
        if (_model.isEmpty() && _axisLayerId < 0) {
            _axisLayerId = layer.getIdNumber();
        }
        _model.add(0, new ListObject(
                    layer, layer.getIdNumber(), layer.getLayerName(), type, clr));
        // always do this set selection, otherwise, model changed, what is
        // thought to be selected and what is shown as selected will be asynchronize
        //this.setSelectedIndex(_model.getSize()-1);
        this.setSelectedIndex(0);
    }

    /**
     * Moves a specific entry up to the top.
     * @param layer the <code>{@link BhpLayer}</code> that will be moved.
     */
    public void moveLayerUp(BhpLayer layer) {
        layer._window.getBhpPlot().getMainView().movePlotViewUp(layer);
        ListObject obj = getListObjectForId(layer.getIdNumber());
        if (obj == null) return;
        _model.removeElement(obj);
        _model.insertElementAt(obj, 0);
        this.clearSelection();
        this.setSelectedIndex(0);
    }

    /**
     * Moves a specific entry down to the bottom.
     * @param layer the <code>{@link BhpLayer}</code> that will be moved.
     */
    public void moveLayerDown(BhpLayer layer) {
        layer._window.getBhpPlot().getMainView().movePlotViewBack(layer);
        ListObject obj = getListObjectForId(layer.getIdNumber());
        if (obj == null) return;
        _model.removeElement(obj);
        _model.addElement(obj);
        this.clearSelection();
        this.setSelectedIndex(_model.size()-1);
    }

    /**
     * Remove specific layers.
     * @param layer array of layers that need to be removed.
     */
    public void removeBhpLayer(BhpLayer[] layer) {
        int oldSelectIndex = this.getSelectedIndex();
        ListObject obj = null;
        for (int i=0; i<layer.length; i++) {
            obj = getListObjectForId(layer[i].getIdNumber());
            _model.removeElement(obj);
            if (layer[i].getIdNumber() == _axisLayerId) _axisLayerId = -1;
        }
        this.clearSelection();
        if (_model.size() > 0) {
            if (_model.size() > oldSelectIndex)
                this.setSelectedIndex(oldSelectIndex);
            else
                this.setSelectedIndex(0);
        }
    }

    /**
     * Gets the BhpLayer identification number for the list entry
     * with specific index number.
     */
    public int getLayerIdAt(int index) {
        ListObject lobj = (ListObject) _model.getElementAt(index);
        if (lobj == null) return -1;
        return lobj.getId();
    }

    /**
     * Gets the BhpLayer identification number of the selected list entry.
     */
    public int getSelectedLayerId() {
        ListObject lobj = (ListObject) this.getSelectedValue();
        if (lobj == null) return -1;
        return lobj.getId();
    }

    /**
     * Gets the BhpLayer identification numbers of the selected list entries.
     */
    public int[] getSelectedLayerIds() {
        Object[] selected = this.getSelectedValues();
        int[] re = new int[selected.length];
        for (int i=0; i<selected.length; i++) {
            re[i] = ((ListObject) selected[i]).getId();
        }
        return re;
    }

    /**
     * Sets the layer that based on which annotations are drawn. <br>
     * In the list, entry for this specific will be shown in bold.
     * @param id the identification number of the annotation layer.
     */
    public void setAxisAssociateLayer(int id) {
        if (_axisLayerId != id) {
            _axisLayerId = id;
            this.repaint();
        }
    }

    private ListObject getListObjectForId(int id) {
        ListObject re = null;
        ListObject obj;
        Enumeration elementList = _model.elements();
        while(elementList.hasMoreElements()) {
            obj = (ListObject) elementList.nextElement();
            if (obj != null && obj.getId() == id) {
                re = obj;
                break;
            }
        }
        return re;
    }

    private class ListObject {
        private int _id;
        private String _name;
        private int _type;
        private Color _symbolColor;
        private BhpLayer _layer;

        public ListObject(BhpLayer layer, int id, String name, int type, Color clr) {
            _id = id;
            _name = name;
            _type = type;
            _symbolColor = clr;
            _layer = layer;
        }

        public void setName(String name) { _name = name; }
        public int getId() { return _id; }
        public Color getSymbolColor() {
            Color clr = _symbolColor;
            if (_layer instanceof BhpEventLayer) {
                clr = _layer.getShapeListLayer().getShape(0).getAttribute().getLineColor();
            }
            return clr;
        }
        public void setSymbolColor(Color clr) { _symbolColor = clr; }
        public boolean isBhpLayerVisible() { return _layer.isBhpLayerVisible(); }

        public ImageIcon getSymbolIcon() {
            if (_type == BhpViewer.BHP_DATA_TYPE_SEISMIC) return _seismicIcon;
            else if (_type == BhpViewer.BHP_DATA_TYPE_MODEL) return _modelIcon;
            else if (_type == BhpViewer.BHP_DATA_TYPE_EVENT) return _eventIcon;
            else return _unknownIcon;
        }

        public String toString() {
            if (_type == BhpViewer.BHP_DATA_TYPE_SEISMIC) {
                String myself = "S " + _id + " : " + _name;
                return myself;
            }
            else if (_type == BhpViewer.BHP_DATA_TYPE_MODEL) {
                String myself = "M " + _id + " : " + _name;
                return myself;
            }
            else if (_type == BhpViewer.BHP_DATA_TYPE_EVENT) {
                String myself = "H " + _id + " : " + _name;
                return myself;
            }
            String myself = "U " + _id + " : " + _name;
            return myself;
        }
    }

    class MyCellRenderer extends JLabel implements ListCellRenderer {
    Color backInvisible = new Color(195,195,195);
    Color foreInvisible = new Color(160,160,160);
        public Component getListCellRendererComponent(JList list, Object value,
                            int index, boolean isSelected, boolean cellHasFocus) {
            if (value == null) return this;
            String s = value.toString();
            setOpaque(true);
            setText(s);
            setIcon(((ListObject)value).getSymbolIcon());
            if (!((ListObject)value).isBhpLayerVisible()) {
                if (isSelected) {
                    setBackground(foreInvisible);
            setForeground(backInvisible);
                }
                else{
            setBackground(backInvisible);
            setForeground(foreInvisible);
        }
                setEnabled(true);
            }
            else {
                if (isSelected) {
                    setBackground(list.getSelectionBackground());
                }
                else setBackground(list.getBackground());
                setForeground(((ListObject)value).getSymbolColor());
                setEnabled(list.isEnabled());
            }
            if (((ListObject)value).getId() == _axisLayerId)
                setFont(list.getFont().deriveFont(Font.BOLD));
            else if (index == _dragIndex)
                setFont(list.getFont().deriveFont(Font.ITALIC));
            else
                setFont(list.getFont().deriveFont(Font.PLAIN));
            return this;
        }
    }

    private class MyMouseListener extends MouseAdapter {
        public void mousePressed(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e))
                _dragIndex = locationToIndex(e.getPoint());
        }
    }
}
