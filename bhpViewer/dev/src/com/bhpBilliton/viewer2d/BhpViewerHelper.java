/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.ui.util.ElementAttributeReader;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.core.*;
import com.gwsys.seismic.util.SeismicColorMap;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This helper class provieds a bunch of static methods,
 *               mostly for XML serialization. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpViewerHelper {

    /**
     * Initiates the colors in a colormap with that of another one.
     * @param des the colormap that will be updated.
     * @param ori the colormap where color settings will be retrieved.
     */
    public static void initColormapWithAnother(
            SeismicColorMap des,
            SeismicColorMap ori) {
        des.setSize(ori.getSize());
        des.setDensityColorSize(ori.getDensityColorSize());
        des.setBackground(ori.getBackground());
        des.setForeground(ori.getForeground());
        des.setPositiveFill(ori.getPositiveFill());
        des.setNegativeFill(ori.getNegativeFill());
        des.setHighlight(ori.getHighlight());

        for (int i = 0; i < ori.getDensityColorSize(); i++) {
            des.setColor(i, ori.getColor(i), false);
        }
    }

    /**
     * Initiates a SeismicWorkflow with an XML node.
     */
    public static void initWithXML(BhpViewerBase viewer, Node node, SeismicWorkflow pipeline) {
        if (pipeline instanceof DefaultWorkflow) {
            initWithXML(viewer, node, (DefaultWorkflow) pipeline);
        } else if (pipeline instanceof BhpEventPipeline) {
            initWithXML(viewer, node, (BhpEventPipeline) pipeline);
        } else {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "Internal error in JSeismicHelper.initWithXML: " +
                                                    pipeline.getClass().getName() + " is not implemented");
        }
    }

    /**
     * Serializes the settings of a cgPipeline into an XML string.
     */
    public static String toXMLString(BhpViewerBase viewer, SeismicWorkflow pipeline) {
        if (pipeline instanceof DefaultWorkflow) {
            return toXMLString((DefaultWorkflow) pipeline);
        } else if (pipeline instanceof BhpEventPipeline) {
            return toXMLString((BhpEventPipeline) pipeline);
        } else {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "Internal error in JSeismicHelper.toXMLString: " +
                                                    pipeline.getClass().getName() + " is not implemented");
            return "";
        }
    }

    private static void initWithXML(BhpViewerBase viewer, Node node, BhpEventPipeline pipeline) {
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals("PlotDataSlector")) {
                    initPlotDataSelector(viewer, child, pipeline);
                    break;
                }
            }
        }
    }

    private static void initWithXML(BhpViewerBase viewer, Node node, DefaultWorkflow pipeline) {
        boolean selectorIni = false;
        boolean scalingIni = false;
        boolean agcIni = false;
        boolean rasterIni = false;
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals("PlotDataSlector")) {
                    initPlotDataSelector(viewer, child, pipeline);
                    selectorIni = true;
                } else if (child.getNodeName().equals("PlotScaling")) {
                    initPlotScaling(viewer, child, pipeline);
                    scalingIni = true;
                } else if (child.getNodeName().equals("PlotAGC")) {
                    initPlotAGC(viewer, child, pipeline);
                    agcIni = true;
                } else if (child.getNodeName().equals("PlotRasterizer")) {
                    initPlotRasterizer(viewer, child, pipeline, false);
                    rasterIni = true;
                } else if (child.getNodeName().equals("ModelRasterizer")) {
                    initPlotRasterizer(viewer, child, pipeline, true);
                    rasterIni = true;
                }
            }
            if (selectorIni && scalingIni && rasterIni && agcIni) {
                break;
            }
        }
    }

    private static String toXMLString(BhpEventPipeline pipeline) {
        StringBuffer content = new StringBuffer();
        content.append("                " + "<Pipeline>\n");
        content.append(toXMLStringPlotDataSelector(pipeline));
        content.append("                </Pipeline>\n");
        return content.toString();
    }

    private static String saveState(BhpEventPipeline pipeline) {
        StringBuffer content = new StringBuffer();
        content.append("                " + "<Pipeline>\n");
        content.append(saveStatePlotDataSelector(pipeline));
        content.append("                </Pipeline>\n");
        return content.toString();
    }

    private static String toXMLString(DefaultWorkflow pipeline) {
        StringBuffer content = new StringBuffer();

        content.append("                " + "<Pipeline>\n");

        content.append(toXMLStringPlotDataSelector(pipeline));
        content.append(toXMLStringPlotScaling(pipeline));
        content.append(toXMLStringPlotAGC(pipeline));
        content.append(toXMLStringPlotRasterizer(pipeline));

        content.append("                </Pipeline>\n");
        return content.toString();
    }

    private static String saveState(DefaultWorkflow pipeline) {
        StringBuffer content = new StringBuffer();

        content.append("                " + "<Pipeline>\n");

        content.append(saveStatePlotDataSelector(pipeline));
        content.append(saveStatePlotScaling(pipeline));
        content.append(saveStatePlotAGC(pipeline));
        content.append(saveStatePlotRasterizer(pipeline));

        content.append("                </Pipeline>\n");
        return content.toString();
    }

    /**
     * Serializes the annotation settings of a
     * <code>{@link AbstractPlot}</code> into an XML string.
     */
    public static String toXMLStringAnnotation(AbstractPlot plot) {
        StringBuffer content = new StringBuffer();

        content.append("            " + "<Annotation " + ">\n");

        if (plot instanceof BhpPlotXV) {
            content.append(toXMLStringAnnoTrace((BhpPlotXV) plot));
            content.append(toXMLStringAnnoSample((BhpPlotXV) plot));
        } else {
            content.append(toXMLStringAnnoGeneral(plot));
            //System.out.println("toXMLStringAnnotation has not been fully implemented for " + plot.getClass().getName());
        }
        content.append(toXMLStringAnnoMisc(plot));

        content.append("            </Annotation>\n");
        return content.toString();
    }

    /**
     * Initiates the annotation setting of a
     * <code>{@link AbstractPlot}</code> with an XML node.
     */
    public static void initAnnotationWithXML(BhpViewerBase viewer, Node node, AbstractPlot plot) {
        boolean traceIni = false;
        boolean sampleIni = false;
        boolean miscIni = false;
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals("AnnoGeneral")) {
                    initAnnoGeneral(viewer, child, plot);
                    traceIni = true;
                    sampleIni = true;
                } else if (child.getNodeName().equals("AnnoTrace")) {
                    initAnnoTrace(viewer, child, plot);
                    traceIni = true;
                } else if (child.getNodeName().equals("AnnoSample")) {
                    initAnnoSample(viewer, child, plot);
                    sampleIni = true;
                } else if (child.getNodeName().equals("AnnoMisc")) {
                    initAnnoMisc(viewer, child, plot);
                    miscIni = true;
                }
            }
            if (traceIni && sampleIni && miscIni) {
                break;
            }
        }
    }

    private static void initPlotDataSelector(BhpViewerBase viewer, Node node, SeismicWorkflow pipeline) {
        try {
            DataChooser str = pipeline.getDataLoader().getDataSelector();
            ElementAttributeReader emt = new ElementAttributeReader(node);
            int pkey = emt.getInt("primaryKey");
            double pstart = emt.getDouble("primaryKeyStart");
            double pend = emt.getDouble("primaryKeyEnd");
            double pstep = emt.getDouble("primaryKeyStep");

            str.setPrimaryKey(pkey);
            str.setPrimaryKeyStart(pstart);
            str.setPrimaryKeyEnd(pend);
            str.setPrimaryKeyStep(pstep);

            double sampleValueS = emt.getDouble("sampleValueStart");
            double sampleValueE = emt.getDouble("sampleValueEnd");
            String useNewBhpio = emt.getString("useNewBhpio");
            if (useNewBhpio.length() == 0) {
                if (!(pipeline instanceof BhpEventPipeline)) {
                    sampleValueS = sampleValueS * 1000;
                    sampleValueE = sampleValueE * 1000;
                }
        //This causes the horizon is not in the right place, move code out of "if"
                //str.setStartSampleValue(sampleValueS); //April 2006
                //str.setEndSampleValue(sampleValueE);
            }
        str.setStartSampleValue(sampleValueS);
            str.setEndSampleValue(sampleValueE);
            //************************************
            //Not set sample for map layer
            //************************************
            boolean applyGaps = emt.getBoolean("applyGaps");
            int gapSize = emt.getInt("gapSize");
            str.applyGaps(applyGaps);
            str.setGapInTraces(gapSize);

            int skey = emt.getInt("secondaryKey");
            if (skey != DataChooser.NO_SECONDARY_KEY) {
                double sstart = emt.getDouble("secondaryKeyStart");
                double send = emt.getDouble("secondaryKeyEnd");
                double sstep = emt.getDouble("secondaryKeyStep");

                str.setSecondaryKey(skey);
                str.setSecondaryKeyStart(sstart);
                str.setSecondaryKeyEnd(send);
                str.setSecondaryKeyStep(sstep);
            }

        } catch (Exception e) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "JSeismicHelper.initPlotDataSelector Exception");
        }
    }

    private static void initPlotScaling(BhpViewerBase viewer, Node node, DefaultWorkflow pipeline) {
        try {
            TraceNormalizer norm = pipeline.getInterpretation()
                    .getTraceNormalization();
            TraceInterpolator inter = pipeline.getInterpretation()
                    .getTraceInterpolator();

            ElementAttributeReader emt = new ElementAttributeReader(node);

            int ntype = emt.getInt("normType");
            if (ntype == TraceNormalizer.AMPLITUDE_LIMITS) {
                float min = emt.getFloat("normLimitMin");
                float max = emt.getFloat("normLimitMax");
                norm.setNormalizationMode(ntype);
                norm.setNormalizationLimits(min, max);
            } else {
                norm.setNormalizationMode(ntype);
            }

            float nscale = emt.getFloat("normScale");
            norm.setScale(nscale);

            int itype = emt.getInt("interpolationType");
            inter.setInterpolationType(itype);
        } catch (Exception e) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "JSeismicHelper.initPlotScaling Exception");
        }
    }

    private static void initPlotAGC(BhpViewerBase viewer, Node node, DefaultWorkflow pipeline) {
        try {
            AutoGainController agc = pipeline.getInterpretation().getTraceAGC();
            ElementAttributeReader emt = new ElementAttributeReader(node);

            boolean agcApply = emt.getBoolean("applyAGC");
            int agcUnits = emt.getInt("measureUnit");
            double agcLen = emt.getDouble("windowLength");

            agc.setEnabled(agcApply);
            agc.setUnits(agcUnits);
            agc.setWindowLength(agcLen);
        } catch (Exception e) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "JSeismicHelper.initPlotAGC Exception");
        }
    }

    private static void initPlotRasterizer(BhpViewerBase viewer, Node node, DefaultWorkflow pipeline,
            boolean isModel) {
        try {
            TraceRasterizer rast = pipeline.getTraceRasterizer();
            ElementAttributeReader emt = new ElementAttributeReader(node);

            int ptype = emt.getInt("plotType");
            rast.setPlotType(ptype);

            float clip = emt.getFloat("clippingValue");
            rast.setClippingValue(clip);

            //if (rast instanceof cgWiggleTraceSkip) {
            double wiggleDecimation = 1.0;
            wiggleDecimation = emt.numberAttribute("wiggleDecimation",
                    wiggleDecimation);
            (rast).setMinSpacing(wiggleDecimation);
            //}
            if (rast instanceof AbstractBhpColorInterpolatedRasterizer) {
                double cs = -1;
                double ce = 1;
                cs = emt.numberAttribute("colorInterpStart", cs);
                ce = emt.numberAttribute("colorInterpEnd", ce);
                ((AbstractBhpColorInterpolatedRasterizer) rast)
                        .setColorMapStartValue(cs);
                ((AbstractBhpColorInterpolatedRasterizer) rast)
                        .setColorMapEndValue(ce);
            }

            if (isModel) {
                BhpModelTraceRasterizer mrast = (BhpModelTraceRasterizer) rast;
                // reset it to default, user does not want to change these anymore
                mrast.setColorMapStartValue(-1.0);
                mrast.setColorMapEndValue(1.0);
                BhpModelDataSelector mselector = (BhpModelDataSelector) pipeline
                        .getDataLoader().getDataSelector();
                BhpModelTraceInterpolator minterpolator = (BhpModelTraceInterpolator) pipeline
                        .getInterpretation().getTraceInterpolator();
                boolean structuralInterp = emt.getBoolean("structuralInterp",
                        true);

                mrast.setStructuralInterpFlag(structuralInterp);
                float startDepth = emt.getFloat("startDepth");
                float endDepth = emt.getFloat("endDepth");
                // if start and end depth in xml are both zero, use values from reader summary
                if(startDepth == 0 && endDepth == 0) {
                  startDepth = mrast.getStartDepth();
                  endDepth = mrast.getEndDepth();
                }
                mrast.setStartDepth(startDepth);
                mselector.setStartDepth(startDepth);
                minterpolator.setStartDepth(startDepth);
                mrast.setEndDepth(endDepth);
                mselector.setEndDepth(endDepth);
                minterpolator.setEndDepth(endDepth);
            }
        } catch (Exception e) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "JSeismicHelper.initPlotRasterizer Exception");
        }

        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals("Colormap")) {
                    initColormap(viewer, child, pipeline.getTraceRasterizer());
                    break;
                }
            }
        }
    }

    private static void initColormap(BhpViewerBase viewer, Node node, TraceRasterizer rast) {
        SeismicColorMap cmp = rast.getColorMap();
        try {
            ElementAttributeReader emt = new ElementAttributeReader(node);

            cmp.setSize(emt.getInt("nColors"));
            cmp.setDensityColorSize(emt.getInt("rampSize"));
            cmp.setColorIndex(SeismicColorMap.BACKGROUND_COLOR, emt.getInt("backgroundIndex"));
            cmp.setColorIndex(SeismicColorMap.FOREGROUND_COLOR, emt.getInt("foregroundIndex"));
            cmp.setColorIndex(SeismicColorMap.POSITIVE_COLOR, emt.getInt("positiveIndex"));
            cmp.setColorIndex(SeismicColorMap.NEGATIVE_COLOR, emt.getInt("negativeIndex"));
            cmp.setColorIndex(SeismicColorMap.HIGHLIGHT_COLOR, emt.getInt("hilightIndex"));

            cmp.setBackground(emt.getColor("backgroundColor"));
            cmp.setForeground(emt.getColor("foregroundColor"));
            cmp.setPositiveFill(emt.getColor("positiveColor"));
            cmp.setNegativeFill(emt.getColor("negativeColor"));
            cmp.setHighlight(emt.getColor("hilightColor"));

            NodeList children = node.getChildNodes();
            int colorCounter = 0;
            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                if (child.getNodeType() == Node.ELEMENT_NODE) {
                    ElementAttributeReader cemt = new ElementAttributeReader(
                            child);
                    Color clr = cemt.getColor("color");
                    cmp.setColor(colorCounter, clr, false);
                    colorCounter++;
                }
            }
        } catch (Exception e) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "JSeismicHelper.initColormap Exception");
        }
    }

    private static void initAnnoTrace(BhpViewerBase viewer, Node node, AbstractPlot plot) {
        try {
            ElementAttributeReader emt = new ElementAttributeReader(node);

            int loc = 0;
            loc = emt.numberAttribute("location", loc);
            plot.setHAxesLoc(loc);

            String hSyncField = emt.getString("hSyncField");
            plot.setHAxesSyncName(hSyncField);

            NodeList children = node.getChildNodes();

            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                if (child.getNodeType() == Node.ELEMENT_NODE) {
                    if (child.getNodeName().equals("AnnoTraceField")) {
                        addAnnotationTraceToPlot(plot, (Element) child);
                    }
                }
            }
        } catch (Exception e) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "JSeismicHelper.initAnnoTrace Exception");
       }
    }

    private static void addAnnotationTraceToPlot(AbstractPlot plot, Element emt) {
        double step;
        boolean line;
        double gap;
        double angle;
        double limit;
        int flag = 0;
        ElementAttributeReader cemt = new ElementAttributeReader(emt);
        line = cemt.getBoolean("line");

        gap = 40.0;
        angle = 0;
        limit = 0;
        step = 5;
        //flag = BhpLinearLabelGenerator.ALL_LABELS;

        step = cemt.numberAttribute("step", step);
        gap = cemt.numberAttribute("gap", gap);
        angle = cemt.numberAttribute("angle", angle);
        limit = cemt.numberAttribute("limit", limit);
        flag = cemt.numberAttribute("flag", flag);

        ((BhpPlotXV) plot).addTraceAxis(cemt.getString("name"), step, line,
                gap, angle, limit, flag);
    }

    private static void initAnnoSample(BhpViewerBase viewer, Node node, AbstractPlot plot) {
        try {
            ElementAttributeReader emt = new ElementAttributeReader(node);

            int loc = 0;
            float minor = 1;
            float major = 10;

            boolean grid = emt.getBoolean("showGrid");
            boolean auto = emt.getBoolean("isAutomatic");
            loc = emt.numberAttribute("location", loc);
            major = emt.numberAttribute("majorStep", major);
            minor = emt.numberAttribute("minorStep", minor);

            plot.setVAxesLoc(loc);
            plot.setVMajorStep(major);
            plot.setVMinorStep(minor);
            ((BhpPlotXV) plot).setSampleGridLine(grid);
            ((BhpPlotXV) plot).setSampleStepAuto(auto);
        } catch (Exception e) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "JSeismicHelper.initAnnoSample Exception");
        }
    }

    private static void initAnnoGeneral(BhpViewerBase viewer, Node node, AbstractPlot plot) {
        try {
            ElementAttributeReader emt = new ElementAttributeReader(node);
            int hloc = 0;
            int vloc = 0;
            float hmajor = 0;
            float hminor = 0;
            float vmajor = 0;
            float vminor = 0;

            hloc = emt.numberAttribute("hlocation", hloc);
            vloc = emt.numberAttribute("vlocation", vloc);
            hmajor = emt.numberAttribute("hmajorStep", hmajor);
            hminor = emt.numberAttribute("hminorStep", hminor);
            vmajor = emt.numberAttribute("vmajorStep", vmajor);
            vminor = emt.numberAttribute("vminorStep", vminor);

            plot.setHAxesLoc(hloc);
            plot.setHMajorStep(hmajor);
            plot.setHMinorStep(hminor);
            plot.setVAxesLoc(vloc);
            plot.setVMajorStep(vmajor);
            plot.setVMinorStep(vminor);
        } catch (Exception e) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "JSeismicHelper.initAnnoSample Exception");
        }
    }

    private static void initAnnoMisc(BhpViewerBase viewer, Node node, AbstractPlot plot) {
        try {
            ElementAttributeReader emt = new ElementAttributeReader(node);

            int tloc = 0;
            tloc = emt.numberAttribute("titleLocation", tloc);
            plot.setTitleLoc(tloc);

            String tt = emt.getString("titleText");
            plot.setTitle(tt);

            int cloc = 0;
            cloc = emt.numberAttribute("colorbarLocation", cloc);
            plot.setColorbarLoc(cloc);

            plot.setLabelL(emt.getString("leftLabel"));
            plot.setLabelR(emt.getString("rightLabel"));
            plot.setLabelT(emt.getString("topLabel"));
            plot.setLabelB(emt.getString("bottomLabel"));
        } catch (Exception e) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "JSeismicHelper.initAnnoMisc Exception");
        }
    }

    private static String toXMLStringPlotDataSelector(SeismicWorkflow pipeline) {
        DataChooser str = pipeline.getDataLoader().getDataSelector();
        String secondaryKeySetting;

        if (str.getSecondaryKey() == DataChooser.NO_SECONDARY_KEY) {
            secondaryKeySetting = "secondaryKey=\"" + str.getSecondaryKey()
                    + "\"";
        } else {
            secondaryKeySetting = "secondaryKey=\"" + str.getSecondaryKey()
                    + "\" " + "secondaryKeyStart=\""
                    + str.getSecondaryKeyStart() + "\" " + "secondaryKeyEnd=\""
                    + str.getSecondaryKeyEnd() + "\" " + "secondaryKeyStep=\""
                    + str.getSecondaryKeyStep() + "\"";
        }

        String myself = "                        " + "<PlotDataSlector "
                + "primaryKey=\"" + str.getPrimaryKey() + "\" "
                + "primaryKeyStart=\"" + str.getPrimaryKeyStart() + "\" "
                + "primaryKeyEnd=\"" + str.getPrimaryKeyEnd() + "\" "
                + "primaryKeyStep=\"" + str.getPrimaryKeyStep() + "\" "
                + "applyGaps=\"" + str.isGapApplied() + "\" " + "gapSize=\""
                + str.getGapInTraces() + "\" " + secondaryKeySetting + " "
                + "useNewBhpio=\"" + "true" + "\" " + "sampleValueStart=\""
                + str.getStartSampleValue() + "\" " + "sampleValueEnd=\""
                + str.getEndSampleValue() + "\"" + "/>\n";
        return myself;
    }

    private static String saveStatePlotDataSelector(SeismicWorkflow pipeline) {
        DataChooser str = pipeline.getDataLoader().getDataSelector();
        String secondaryKeySetting;

        if (str.getSecondaryKey() == DataChooser.NO_SECONDARY_KEY) {
            secondaryKeySetting = "secondaryKey=\"" + str.getSecondaryKey()
                    + "\"";
        } else {
            secondaryKeySetting = "secondaryKey=\"" + str.getSecondaryKey()
                    + "\" " + "secondaryKeyStart=\""
                    + str.getSecondaryKeyStart() + "\" " + "secondaryKeyEnd=\""
                    + str.getSecondaryKeyEnd() + "\" " + "secondaryKeyStep=\""
                    + str.getSecondaryKeyStep() + "\"";
        }

        String myself = "                        " + "<PlotDataSlector "
                + "primaryKey=\"" + str.getPrimaryKey() + "\" "
                + "primaryKeyStart=\"" + str.getPrimaryKeyStart() + "\" "
                + "primaryKeyEnd=\"" + str.getPrimaryKeyEnd() + "\" "
                + "primaryKeyStep=\"" + str.getPrimaryKeyStep() + "\" "
                + "applyGaps=\"" + str.isGapApplied() + "\" " + "gapSize=\""
                + str.getGapInTraces() + "\" " + secondaryKeySetting + " "
                + "useNewBhpio=\"" + "true" + "\" " + "sampleValueStart=\""
                + str.getStartSampleValue() + "\" " + "sampleValueEnd=\""
                + str.getEndSampleValue() + "\"" + "/>\n";
        return myself;
    }

    private static String toXMLStringPlotScaling(DefaultWorkflow pipeline) {
        TraceNormalizer norm = pipeline.getInterpretation()
                .getTraceNormalization();
        TraceInterpolator inter = pipeline.getInterpretation()
                .getTraceInterpolator();

        String normTypeSetting;
        if (norm.getNormalizationMode() == TraceNormalizer.AMPLITUDE_LIMITS) {
            normTypeSetting = "normType=\"" + norm.getNormalizationMode()
                    + "\" " + "normLimitMin=\"" + norm.getNormLimitMin()
                    + "\" " + "normLimitMax=\"" + norm.getNormLimitMax() + "\"";
        } else {
            normTypeSetting = "normType=\"" + norm.getNormalizationMode()
                    + "\"";
        }

        String myself = "                        " + "<PlotScaling "
                + normTypeSetting + " " + "normScale=\"" + norm.getScale()
                + "\" " + "interpolationType=\"" + inter.getInterpolationType()
                + "\"" + "/>\n";
        return myself;
    }

    private static String saveStatePlotScaling(DefaultWorkflow pipeline) {
        TraceNormalizer norm = pipeline.getInterpretation()
                .getTraceNormalization();
        TraceInterpolator inter = pipeline.getInterpretation()
                .getTraceInterpolator();

        String normTypeSetting;
        if (norm.getNormalizationMode() == TraceNormalizer.AMPLITUDE_LIMITS) {
            normTypeSetting = "normType=\"" + norm.getNormalizationMode()
                    + "\" " + "normLimitMin=\"" + norm.getNormLimitMin()
                    + "\" " + "normLimitMax=\"" + norm.getNormLimitMax() + "\"";
        } else {
            normTypeSetting = "normType=\"" + norm.getNormalizationMode()
                    + "\"";
        }

        String myself = "                        " + "<PlotScaling "
                + normTypeSetting + " " + "normScale=\"" + norm.getScale()
                + "\" " + "interpolationType=\"" + inter.getInterpolationType()
                + "\"" + "/>\n";
        return myself;
    }

    private static String toXMLStringPlotAGC(DefaultWorkflow pipeline) {
        AutoGainController agc = pipeline.getInterpretation().getTraceAGC();
        if (agc == null) {
            return "";
        }
        String myself = "                        " + "<PlotAGC "
                + "applyAGC=\"" + agc.isEnabled() + "\" " + "measureUnit=\""
                + agc.getUnits() + "\" " + "windowLength=\""
                + agc.getWindowLength() + "\"" + "/>\n";
        return myself;
    }

    private static String saveStatePlotAGC(DefaultWorkflow pipeline) {
        AutoGainController agc = pipeline.getInterpretation().getTraceAGC();
        if (agc == null) {
            return "";
        }
        String myself = "                        " + "<PlotAGC "
                + "applyAGC=\"" + agc.isEnabled() + "\" " + "measureUnit=\""
                + agc.getUnits() + "\" " + "windowLength=\""
                + agc.getWindowLength() + "\"" + "/>\n";
        return myself;
    }

    private static String toXMLStringPlotRasterizer(DefaultWorkflow pipeline) {
        TraceRasterizer rast = pipeline.getTraceRasterizer();
        StringBuffer content = new StringBuffer();

        if (rast instanceof BhpModelTraceRasterizer) {
            BhpModelTraceRasterizer mrast = (BhpModelTraceRasterizer) rast;
            content.append("                        " + "<ModelRasterizer "
                    + "plotType=\"" + rast.getPlotType() + "\" "
                    + "clippingValue=\"" + rast.getClippingValue() + "\" "
                    + "structuralInterp=\"" + mrast.getStructuralInterpFlag()
                    + "\" " + "startDepth=\"" + mrast.getStartDepth() + "\" "
                    + "endDepth=\"" + mrast.getEndDepth() + "\" "
                    + "colorInterpStart=\"" + mrast.getColorMapStartValue()
                    + "\" " + "colorInterpEnd=\"" + mrast.getColorMapEndValue()
                    + "\"" + ">\n");
            content.append(toXMLStringColormap(rast.getColorMap()));
            content.append("                        </ModelRasterizer>\n");
        } else {
            double colors = -1;
            double colore = 1;
            double wiggleDecimation = 1.0;
            if (rast instanceof AbstractBhpColorInterpolatedRasterizer) {
                colors = ((AbstractBhpColorInterpolatedRasterizer) rast)
                        .getColorMapStartValue();
                colore = ((AbstractBhpColorInterpolatedRasterizer) rast)
                        .getColorMapEndValue();
            }
            //if (rast instanceof cgWiggleTraceSkip) {
            wiggleDecimation = (rast).getMinSpacing();
            //}
            content.append("                        " + "<PlotRasterizer "
                    + "plotType=\"" + rast.getPlotType() + "\" "
                    + "clippingValue=\"" + rast.getClippingValue() + "\" "
                    + "wiggleDecimation=\"" + wiggleDecimation + "\" "
                    + "colorInterpStart=\"" + colors + "\" "
                    + "colorInterpEnd=\"" + colore + "\"" + ">\n");
            content.append(toXMLStringColormap(rast.getColorMap()));
            content.append("                        </PlotRasterizer>\n");
        }

        return content.toString();
    }

    private static String saveStatePlotRasterizer(DefaultWorkflow pipeline) {
        TraceRasterizer rast = pipeline.getTraceRasterizer();
        StringBuffer content = new StringBuffer();

        if (rast instanceof BhpModelTraceRasterizer) {
            BhpModelTraceRasterizer mrast = (BhpModelTraceRasterizer) rast;
            content.append("                        " + "<ModelRasterizer "
                    + "plotType=\"" + rast.getPlotType() + "\" "
                    + "clippingValue=\"" + rast.getClippingValue() + "\" "
                    + "structuralInterp=\"" + mrast.getStructuralInterpFlag()
                    + "\" " + "startDepth=\"" + mrast.getStartDepth() + "\" "
                    + "endDepth=\"" + mrast.getEndDepth() + "\" "
                    + "colorInterpStart=\"" + mrast.getColorMapStartValue()
                    + "\" " + "colorInterpEnd=\"" + mrast.getColorMapEndValue()
                    + "\"" + ">\n");
            content.append(saveStateColormap(rast.getColorMap()));
            content.append("                        </ModelRasterizer>\n");
        } else {
            double colors = -1;
            double colore = 1;
            double wiggleDecimation = 1.0;
            if (rast instanceof AbstractBhpColorInterpolatedRasterizer) {
                colors = ((AbstractBhpColorInterpolatedRasterizer) rast)
                        .getColorMapStartValue();
                colore = ((AbstractBhpColorInterpolatedRasterizer) rast)
                        .getColorMapEndValue();
            }
            //if (rast instanceof cgWiggleTraceSkip) {
            wiggleDecimation = (rast).getMinSpacing();
            //}
            content.append("                        " + "<PlotRasterizer "
                    + "plotType=\"" + rast.getPlotType() + "\" "
                    + "clippingValue=\"" + rast.getClippingValue() + "\" "
                    + "wiggleDecimation=\"" + wiggleDecimation + "\" "
                    + "colorInterpStart=\"" + colors + "\" "
                    + "colorInterpEnd=\"" + colore + "\"" + ">\n");
            content.append(saveStateColormap(rast.getColorMap()));
            content.append("                        </PlotRasterizer>\n");
        }

        return content.toString();
    }

    private static String toXMLStringAnnoTrace(BhpPlotXV plot) {
        StringBuffer content = new StringBuffer();

        content.append("                " + "<AnnoTrace " + "hSyncField=\""
                + plot.getHAxesSyncName() + "\" " + "location=\""
                + plot.getHAxesLoc() + "\"" + ">\n");

        ArrayList selected = plot.getSelected();
        //ArrayList step = plot.getSteps();
        //ArrayList line = plot.getLines();
        String item;
        BhpTraceAxisObject taObject;
        for (int i = 0; i < selected.size(); i++) {
            taObject = (BhpTraceAxisObject) (selected.get(i));
            item = taObject._name;
            content.append("                    " + "<AnnoTraceField "
                    + "name=\"" + item + "\" " + "step=\"" + taObject._step
                    + "\" " + "line=\"" + taObject._drawLine + "\" "
                    + "angle=\"" + taObject._labelAngle + "\" " + "gap=\""
                    + taObject._tickSpace + "\" " + "limit=\""
                    + taObject._labelLimit + "\" " + "flag=\""
                    + taObject._labelFlag + "\"" + "/>\n");
        }

        content.append("                </AnnoTrace>\n");
        return content.toString();
    }

    private static String toXMLStringAnnoSample(BhpPlotXV plot) {
        String myself = "                " + "<AnnoSample " + "location=\""
                + plot.getVAxesLoc() + "\" " + "showGrid=\""
                + plot.getSampleGridLine() + "\" " + "isAutomatic=\""
                + plot.getSampleStepAuto() + "\" " + "majorStep=\""
                + plot.getVMajorStep() + "\" " + "minorStep=\""
                + plot.getVMinorStep() + "\"" + "/>\n";
        return myself;
    }

    private static String toXMLStringAnnoGeneral(AbstractPlot plot) {
        String myself = "                " + "<AnnoGeneral " + "hlocation=\""
                + plot.getHAxesLoc() + "\" " + "hmajorStep=\""
                + plot.getHMajorStep() + "\" " + "hminorStep=\""
                + plot.getHMinorStep() + "\" " + "vlocation=\""
                + plot.getVAxesLoc() + "\" " + "vmajorStep=\""
                + plot.getVMajorStep() + "\" " + "vminorStep=\""
                + plot.getVMinorStep() + "\"" + "/>\n";
        return myself;
    }

    private static String toXMLStringAnnoMisc(AbstractPlot plot) {
        String myself = "                " + "<AnnoMisc " + "titleLocation=\""
                + plot.getTitleLoc() + "\" " + "titleText=\"" + plot.getTitle()
                + "\" " + "colorbarLocation=\"" + plot.getColorbarLoc() + "\" "
                + "leftLabel=\"" + plot.getLabelL() + "\" " + "rightLabel=\""
                + plot.getLabelR() + "\" " + "topLabel=\"" + plot.getLabelT()
                + "\" " + "bottomLabel=\"" + plot.getLabelB() + "\"" + "/>\n";
        return myself;
    }

    private static String toXMLStringColormap(SeismicColorMap cmp) {
        StringBuffer content = new StringBuffer();
        content.append("                            " + "<Colormap "
                + "nColors=\"" + cmp.getSize() + "\" " + "rampSize=\""
                + cmp.getDensityColorSize() + "\" " + "backgroundIndex=\""
                + cmp.getBackgroundIndex() + "\" " + "foregroundIndex=\""
                + cmp.getForegroundIndex() + "\" " + "positiveIndex=\""
                + cmp.getPositiveFillIndex() + "\" " + "negativeIndex=\""
                + cmp.getNegativeFillIndex() + "\" " + "hilightIndex=\""
                + cmp.getHighlightIndex() + "\" " + "backgroundColor=\""
                + toStringColor(cmp.getBackground()) + "\" "
                + "foregroundColor=\"" + toStringColor(cmp.getForeground())
                + "\" " + "positiveColor=\""
                + toStringColor(cmp.getPositiveFill()) + "\" "
                + "negativeColor=\"" + toStringColor(cmp.getNegativeFill())
                + "\" " + "hilightColor=\"" + toStringColor(cmp.getHighlight())
                + "\" " + ">\n");

        for (int i = 0; i < cmp.getDensityColorSize(); i++) {
            content.append("                                "
                    + "<ColormapRampColor " + "color=\""
                    + toStringColor(cmp.getColor(i)) + "\"" + "/>\n");
        }

        content.append("                            </Colormap>\n");
        return content.toString();
    }

    private static String saveStateColormap(SeismicColorMap cmp) {
        StringBuffer content = new StringBuffer();
        content.append("                            " + "<Colormap "
                + "nColors=\"" + cmp.getSize() + "\" " + "rampSize=\""
                + cmp.getDensityColorSize() + "\" " + "backgroundIndex=\""
                + cmp.getBackgroundIndex() + "\" " + "foregroundIndex=\""
                + cmp.getForegroundIndex() + "\" " + "positiveIndex=\""
                + cmp.getPositiveFillIndex() + "\" " + "negativeIndex=\""
                + cmp.getNegativeFillIndex() + "\" " + "hilightIndex=\""
                + cmp.getHighlightIndex() + "\" " + "backgroundColor=\""
                + toStringColor(cmp.getBackground()) + "\" "
                + "foregroundColor=\"" + toStringColor(cmp.getForeground())
                + "\" " + "positiveColor=\""
                + toStringColor(cmp.getPositiveFill()) + "\" "
                + "negativeColor=\"" + toStringColor(cmp.getNegativeFill())
                + "\" " + "hilightColor=\"" + toStringColor(cmp.getHighlight())
                + "\" " + ">\n");

        for (int i = 0; i < cmp.getDensityColorSize(); i++) {
            content.append("                                "
                    + "<ColormapRampColor " + "color=\""
                    + toStringColor(cmp.getColor(i)) + "\"" + "/>\n");
        }

        content.append("                            </Colormap>\n");
        return content.toString();
    }

    /**
     * Serializes a java color into a string. <br>
     * The string has four integers for red, green, blue,
     * and alpha value, separated by space.
     */
    public static String toStringColor(Color color) {
        String myself = color.getRed() + " " + color.getGreen() + " "
                + color.getBlue() + " " + color.getAlpha();
        return myself;
    }

    public static int getBhpLayerCount(NodeList children) {
        int nl = 0;
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (isDataLayer(child.getNodeName())) {
                    nl++;
                }
            }
        }
        return nl;
    }

    public static boolean isDataLayer(String nodeName) {
        boolean isDataLayer;
        if (nodeName.equals("BhpModelLayer")
                || nodeName.equals("BhpSeismicLayer")
                || nodeName.equals("BhpEventLayer")
                || nodeName.equals("BhpMapLayer")
                || nodeName.equals("BhpLogLayer")) {
            isDataLayer = true;
        } else {
            isDataLayer = false;
        }
        return isDataLayer;
    }

    /**
     * @param ins
     * @return
     * @throws FactoryConfigurationError
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public static Element getXMLTree(InputStream ins)
            throws FactoryConfigurationError, ParserConfigurationException,
            SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        DocumentBuilder dbuilder = factory.newDocumentBuilder();
        Document doc = dbuilder.parse(ins);
        Element root = doc.getDocumentElement();
        root.normalize();
        return root;
    }

    /**
     * @return
     * @throws FactoryConfigurationError
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public static Element getXMLTree(File f) throws FactoryConfigurationError,
            ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        DocumentBuilder dbuilder = factory.newDocumentBuilder();
        Document doc = dbuilder.parse(f);
        Element root = doc.getDocumentElement();
        root.normalize();
        return root;
    }

    /**
     * @param dstypeString
     * @return
     */
    public static String readDSType(String dstypeString) {
        String dstype;
        if (dstypeString != null && dstypeString.length() > 0) {
            try {
                int typeid = Integer.parseInt(dstypeString);
                if (typeid == BhpViewer.BHP_DATA_SOURCE_LOCAL
                        || typeid == BhpViewer.BHP_DATA_SOURCE_REMOTE) {
                    dstype = com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_BHPSU;
                } else if (typeid == BhpViewer.BHP_DATA_SOURCE_OPENSPIRIT
                        || typeid == BhpViewer.BHP_DATA_SOURCE_OPENSPIRIT_REMOTE) {
                    dstype = "OpenSpirit";
                } else {
                    dstype = dstypeString;
                }
            } catch (Exception ex) {
                // assume it is original data source name string
                dstype = dstypeString;
            }
            if (dstype.equals("bhpsu")) {
                dstype = com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_BHPSU;
            }
        } else {
            dstype = com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_BHPSU;
        }
        return dstype;
    }
}
