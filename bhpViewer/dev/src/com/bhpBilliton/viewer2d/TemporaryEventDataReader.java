/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.Color;
import java.util.Hashtable;

import com.bhpBilliton.viewer2d.data.BhpDataReader;
import com.bhpBilliton.viewer2d.data.BhpEventReaderInterface;
import com.gwsys.gw2d.model.Attribute2D;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.io.SeismicMetaData;
import com.gwsys.seismic.reader.SeismicFormat;
import com.gwsys.seismic.util.NumberRange;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is the reader for the TemporaryEventDataSource.
 *               Method getEvent(String name) is used to get the whole
 *               event data.
 *               <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class TemporaryEventDataReader extends BhpDataReader implements BhpEventReaderInterface {
    //private float _minDepth;
    //private float _maxDepth;
    //private int _eventSize;
    private TemporaryEventDataSource _dataSource;
    private BhpLayer _refLayer;
    private SeismicMetaData _metaData;
    private Bound2D _modelLimits;
    private Attribute2D _cgAttr = null;

    /**
     * Constructs a new instance.
     * @param layer the layer where the data will be displayed.
     */
    public TemporaryEventDataReader(BhpLayer layer, TemporaryEventDataSource source) {
        _refLayer = layer;
        _dataSource = source;
        //_eventSize = _refLayer.getPipeline().getSeismicReader().getMetaData().getNumberOfTraces();
        //_minDepth = (float) _refLayer.getTimeRangeStart();
        //_maxDepth = (float) _refLayer.getTimeRangeEnd();
        _metaData = null;
        _modelLimits = null;
    }

    public int getValidEventSize() { return _dataSource.getEventDataList().size(); }

    public boolean isThreaded() { return false; }

    public double getDefinedNullValue() {
        return Double.NaN;
    }
    public float getDefinedFloatNullValue() {
        return Float.NaN;
    }
    public Attribute2D getEventAttribute(String name) {
        if (_cgAttr == null) {
            _cgAttr = new RenderingAttribute();
            _cgAttr.setLineColor(Color.red);
            _cgAttr.setLineStyle(RenderingAttribute.LINE_STYLE_SOLID);
            _cgAttr.setFillStyle(RenderingAttribute.FILL_STYLE_EMPTY);
        }
        return _cgAttr;
    }

    /**
     * Gets the lower bound of vertical display limits.
     */
    public float getMinDepth() { return ((float) _refLayer.getTimeRangeStart()); }
    /**
     * Gets the higher bound of vertical display limits.
     */
    public float getMaxDepth() { return ((float) _refLayer.getTimeRangeEnd()); }

    /**
     * Saves the event back with bhpio.
     */
    public void saveEvent(String ename, double[] values) {
        throw new java.lang.UnsupportedOperationException("Method saveEvent(String, double[]) not yet implemented by TemporaryEventDataReader.");
    }

    /**
     * Process the desired trace.
     * In the case of event data, this method should not be called
     * at all. Method getEvent should be used instead to retrieves
     * the event data.
     */
    public boolean process(int traceId, NumberRange sampleRange, TraceData traceDataOut) {
        throw new UnsupportedOperationException();
    }

    /**
     * Retrieves the event data.
     * @param name event name.
     * @return event data values in a float array.
     */
    public float[] getEvent(String name) {
        int eventSize = _refLayer.getPipeline().getDataLoader().getDataReader().getMetaData().getNumberOfTraces();
        float[] eventArray = new float[eventSize];
        for (int i=0; i<eventArray.length; i++) {
            eventArray[i] = Float.NaN;
        }
        return eventArray;
    }

    /**
     * Saves the event data in memory back to disk.
     */
    public void saveEvent() throws Exception {
        throw new java.lang.UnsupportedOperationException("Method saveEvent() not yet implemented by TemporaryEventDataReader.");
    }

    /**
     * Updates the value of the named event with specific header values.
     * @param name the name of the requested event.
     * @param value the new event value.
     * @param param a hashtalbe specify the name and value of the headers.
     */
    public void putEventValueAt(String name, double value, Hashtable param) {
        _dataSource.putEventValueAt(name, value, param);
    }

    /**
     * Retrieves the value of the named event with specific header values.
     * @param name the name of the requested event.
     * @param param a hashtalbe specify the name and value of the headers.
     * @return a float for the specific event point.
     */
    public float getEventValueAt(String name, Hashtable param) {
        return _dataSource.getEventValueAt(name, param);
    }

    public TraceHeader getTraceMetaData(int traceId) {
        return null;
    }

    public SeismicMetaData getMetaData() {
        if (_metaData == null) {
            _metaData = new SeismicMetaData();
        }
        int eventSize = _refLayer.getPipeline().
        getDataLoader().getDataReader().getMetaData().getNumberOfTraces();
        _metaData.setNumberOfTraces(eventSize);
        _metaData.setSamplesPerTrace(1);
        _metaData.setSampleRate(1.0);
        _metaData.setSampleUnits(_refLayer.getPipeline().getDataLoader().getDataReader().
                                 getMetaData().getSampleUnits());
        return _metaData;
    }

    public Bound2D getModelLimits() {
        _modelLimits = new Bound2D(0, 0,
                                  (_refLayer.getPipeline().getDataLoader().getDataReader().
                                  getMetaData().getNumberOfTraces()-1),
                                  (_refLayer.getTimeRangeEnd() -
                                  _refLayer.getTimeRangeStart()));
        return _modelLimits;
    }

    public SeismicFormat getDataFormat() {
        return null;
    }

}
