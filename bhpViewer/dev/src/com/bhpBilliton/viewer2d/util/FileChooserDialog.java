/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

* In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

* Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software. Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform. Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.util;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;
import java.util.logging.Logger;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;

import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

import com.bhpBilliton.viewer2d.Bhp2DviewerConstants;

/**
 * Dialog for selecting a file/dir. Uses qiWorkbench IO services to get the
 * contents of a directory. The contents may be filtered. The accessiblity
 * of the directory depends on the location of the Tomcat server. That is,
 * the directory must be accessible from the server.
 * <p>This file chooser does not use Java's JFileChooser, because it needs
 * to be able to use the remote IO service. This dialog is also used when
 * Tomcat is local so the UI is consistent regardless. In this case, the
 * local IO service is used.
 * <p>FileChooserDialog is NOT functionally equivalent to JFileChooser. It
 * does allow one to go up a directory, restrict content to just
 * directories and specify file extensions in the filter, but it does not
 * allow one to create a new directory or select multiple files.
 * <p>The dialog presents first sorted directories followed by sorted files
 * within the specified directory.
 *
 * @author Gil Hansen, LT Li
 * @version 1.0
 */

public class FileChooserDialog extends JDialog {

    private static final int _GENERAL_FILE = 2;
    private static final int _DIRECTORY = 3;
    private boolean _enableDirectorySelection = false;

    private static Logger logger = Logger.getLogger(FileChooserDialog.class.getName());

    private JList _list;
    private DefaultListModel _listModel;

    private JTextField _pathField;

    private JTextField _nameField;

    private JComboBox _filterCombo;
    private JButton _executeButton;
    private JButton _upButton;
    private String _homeDirectory;
    private int _returnValue;

    private Vector _filters;

    private ImageIcon _fileIcon = null;
    private ImageIcon _dirIcon = null;
    private ImageIcon _homeIcon = null;
    private ImageIcon _upIcon = null;
    private IMessagingManager messagingMgr = null;

    private boolean naviagateUpward = true;

    public void setUpwardNavigation(boolean naviagateUpward) {
        this.naviagateUpward = naviagateUpward;
    }

    /**
     * Constructor in which the starting directory is defaulted to the
     * project directory.
     */
    public FileChooserDialog(JInternalFrame componentGUI, String title,  IMessagingManager messagingMgr) {
        //TODO: use path of project as default instead of the user home dir
        this(componentGUI, title, messagingMgr.getProject(), messagingMgr);
        //this(componentGUI, title, MessageDispatcher.getInstance().getUserHOME(), messagingMgr);
    }

    /**
     * Constructor for the file chooser dialog.
     *
     * @param componentGUI JInternalFrame for the 2D viewer component
     * @param title Window title.
     * @param homeDir Directory to start choosing from.
     * @param messageManager Message Manager for the component GUI using this dialog.
     */
    public FileChooserDialog(JInternalFrame componentGUI, String title, String homeDir, IMessagingManager messagingMgr) {
        super();
        this.setTitle(title);
        this.setLocationRelativeTo(componentGUI);
        this.setModal(true);
        this.messagingMgr = messagingMgr;

        _returnValue = JFileChooser.CANCEL_OPTION;
        _homeDirectory = homeDir;
        _filters = new Vector();
        String[] allextensions = {};
        _filters.addElement(new GenericFileFilter(allextensions, "All Files"));
        try {
            _fileIcon = IconResource.getInstance().getImageIcon(IconResource.GENERALFILE_ICON);
            _dirIcon = IconResource.getInstance().getImageIcon(IconResource.DIRECTORY_ICON);
            _homeIcon = IconResource.getInstance().getImageIcon(IconResource.HOME_ICON);
            _upIcon = IconResource.getInstance().getImageIcon(IconResource.UP_ICON);
        }
        catch (Exception ex) {
            System.out.println("FileChooserDialog.constructor failed to load images");
            System.out.println("    " + ex.toString());
        }
        setupGUI();
    }

    public void enableDirectorySelection(boolean enable) {
        _enableDirectorySelection = enable;
    }

        public void addChoosableFileFilter(FileFilter filter) {
        if (filter!=null && !_filters.contains(filter)) {
            _filters.addElement(filter);
            _filterCombo.setSelectedItem(filter);
        }
    }

    public void setSelectedFile(String fname) {
        try {
            String path = fname.substring(0, fname.lastIndexOf("/"));
            String name = fname.substring(fname.lastIndexOf("/")+1);
            setPathFieldText(path);
            updateList();
            ListObject lobj = null;
            for (int i=0; i<_listModel.size(); i++) {
                lobj = (ListObject) _listModel.getElementAt(i);
                if (lobj.getType() != _DIRECTORY &&
                    lobj.toString().equals(name)) {
                    _list.setSelectedIndex(i);
                    _nameField.setText(_list.getSelectedValue().toString());
                    break;
                }
            }
            System.out.println("SSS " + fname + " : " + _list.getSelectedValue().toString());
        }
        catch (Exception ex) {
            System.out.println("RemoteFileChooser.setSelectedFile fails for file " + fname);
            ex.printStackTrace();
        }
    }

    public String getPath() { return _pathField.getText(); }
    public String getName() { return _nameField.getText(); }

    /** Get the full pathname of the selected file
     *
     * @return Full pathname of the selected file
     */
    public String getSelectedFileAbsolutePathname() {
        return _pathField.getText()+"/"+_nameField.getText();
    }

    /** Get the full pathname of the selected directory
     *
     * @return Full pathname of the selected directory
     */
    public String getSelectedDirAbsolutePathname() {
        return _pathField.getText();
    }

    public void updateList() {
        String pathString = _pathField.getText();
        _listModel.removeAllElements();

        try {
            boolean isLocal = messagingMgr.getLocationPref().equals(QIWConstants.LOCAL_SERVICE_PREF);
            IQiWorkbenchMsg request = null;
            IQiWorkbenchMsg response = null;

            if(isLocal){
                ArrayList<String> list = new ArrayList<String>();
                list.add(QIWConstants.LOCAL_SERVICE_PREF);
                list.add(pathString);
                String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                                                        QIWConstants.GET_DIR_FILE_LIST_CMD,
                                                        QIWConstants.ARRAYLIST_TYPE,list,true);
                response = messagingMgr.getMatchingResponseWait(msgId, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
            } else {
                request = new QiWorkbenchMsg(CompDescUtils.getDescCID(messagingMgr.getMyComponentDesc()),
                    CompDescUtils.getDescCID(messagingMgr.getComponentDescFromDisplayName(QIWConstants.SERVLET_DISPATCHER_NAME)),
                    QIWConstants.CMD_MSG,
                    QIWConstants.GET_REMOTE_DIR_FILE_LIST_CMD,
                    MsgUtils.genMsgID(),
                    QIWConstants.STRING_TYPE, pathString);

                response = messagingMgr.sendRemoteRequest(request);
            }

            if(response == null){
                logger.severe("SYSTEM ERROR: Timed out waiting for response to get directories and files. ");
                return;
            }

            if (!MsgUtils.isResponseAbnormal(response)){
                ArrayList<ArrayList> list = (ArrayList<ArrayList>)MsgUtils.getMsgContent(response);
                ArrayList<String> dirList = list.get(0);  //the first element is directory list
                ArrayList<String> fileList = list.get(1);  //the second element is file list

                for(String s : dirList)
                    _listModel.addElement(new ListObject(_DIRECTORY,s));

                for(String s : fileList) {
                    GenericFileFilter filter = (GenericFileFilter) _filterCombo.getSelectedItem();
                    if (_enableDirectorySelection) continue;
                    if (filter != null) {
                        if (filter.acceptFilename(s))
                            _listModel.addElement(new ListObject(_GENERAL_FILE,s));
                    }
                    else
                        _listModel.addElement(new ListObject(_GENERAL_FILE, s));
                }
            }

            if (_nameField.getText()!=null && _nameField.getText().length()>0) {
                ListObject lobj = null;

                String oldText = _nameField.getText();

                boolean containsOldText = false;
                for (int i=0; i<_listModel.size(); i++) {
                    lobj = (ListObject) _listModel.getElementAt(i);
                    if (lobj.getType() != _DIRECTORY &&
                        lobj.toString().equals(oldText)) {
                        containsOldText = true;
                        //_list.setSelectedIndex(i);
                        break;
                    }
                }
                if (!containsOldText) _nameField.setText("");
            }
            else  _nameField.setText("");
        }
        catch (Exception e) {
            System.out.println("OpenSessionAction.updateList Exception " + e.toString());
            e.printStackTrace();
        }

    }

    public boolean isExistingFile(String name) {
        Enumeration enumElements = _listModel.elements();
        ListObject obj = null;
        while(enumElements.hasMoreElements()) {
            obj = (ListObject) enumElements.nextElement();
            if (obj.getType() == _GENERAL_FILE && obj.toString().equals(name))
                return true;
        }
        return false;
    }

    public int showOpenDialog() {
        _executeButton.setText("  Open ");
        this.updateList();
        this.setVisible(true);
        return _returnValue;
    }

    public int showSaveDialog() {
        _executeButton.setText("  Save ");
        this.updateList();
        this.setVisible(true);
        return _returnValue;
    }

    public void hideDialog() {
        super.setVisible(false);
    }

    public void showMessage(String message){
        JOptionPane.showMessageDialog(this, message, "QI Workbench",
                                   JOptionPane.INFORMATION_MESSAGE);
    }
    public void setPathFieldText(String text) {
        text = text.trim();
        _pathField.setText(text);
        _upButton.setEnabled(true);
    }

    public int showConfirmBox(String message){
        return JOptionPane.showConfirmDialog(this,message,"File overwriting confirmation",JOptionPane.YES_NO_OPTION);
    }

    public int approveSelection() {
System.out.println("FileChooserDialog: approveSelection file="+_nameField.getText());
        return approveSelection(_nameField.getText());
        //return approveSelection(_pathField.getText());
    }
    private int approveSelection(String filename) {
        int re = JFileChooser.CANCEL_OPTION;
        GenericFileFilter filter = (GenericFileFilter) _filterCombo.getSelectedItem();
System.out.println("FileChooserDialog: filter.acceptFilename()="+filter.acceptFilename(filename));
        if (filter.acceptFilename(filename)) re = JFileChooser.APPROVE_OPTION;
        if (_executeButton.getText().indexOf("Open") > -1) {
            // must open an existing file
System.out.println("FileChooserDialog: isExistingFile="+isExistingFile(filename));
        } else {
            // saving an existing file is always okay
            if (!isExistingFile(filename)) re = JFileChooser.APPROVE_OPTION;
            else {
                String message = filename + " already exists. Do you want to replace it?";
                int action = showConfirmBox(message);
                if(action == JOptionPane.YES_OPTION )
                    re = JFileChooser.APPROVE_OPTION;
                else if(action == JOptionPane.CANCEL_OPTION)
                    re = JFileChooser.CANCEL_OPTION;
                else if(action == JOptionPane.NO_OPTION)
                    re = JOptionPane.NO_OPTION;
            }

        }
        return re;
    }

    private void setupGUI() {
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                _returnValue = JFileChooser.CANCEL_OPTION;
            }
        });

        JPanel extraButtonPanel = new JPanel(new FlowLayout());
        _upButton = new JButton(_upIcon);
        _upButton.setPreferredSize(new Dimension(25, 25));
        JButton hmButton = new JButton(_homeIcon);
        hmButton.setPreferredSize(new Dimension(25, 25));
        _upButton.addActionListener(new UpListener());
        hmButton.addActionListener(new HomeListener());
        extraButtonPanel.add(_upButton);
        extraButtonPanel.add(hmButton);

        JPanel p1 = new JPanel(new BorderLayout());
        p1.add(new JLabel("  Look in:    "), BorderLayout.WEST);
        _pathField = new JTextField("");
        _pathField.setEditable(false);
        p1.add(_pathField, BorderLayout.CENTER);
        p1.add(extraButtonPanel, BorderLayout.EAST);

        _pathField.addKeyListener(new MyPathKeyListener());

        _pathField.setText(_homeDirectory);

        JPanel p2 = new JPanel(new BorderLayout());
        _listModel = new DefaultListModel();
        _list = new JList(_listModel);
        _list.setCellRenderer(new MyCellRenderer());
        _list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        _list.addMouseListener(new MyListMouseAdapter());
        _list.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (_list.getSelectedValue() != null)
                    _nameField.setText(_list.getSelectedValue().toString());
            }
        });
        p2.add(new JScrollPane(_list), BorderLayout.CENTER);

        //JPanel p3 = new JPanel(new FlowLayout());
        _executeButton = new JButton("  Open ");
        _executeButton.addActionListener(new ExecuteListener());
        JButton b3 = new JButton("Cancel");
        b3.addActionListener(new CancelListener());
        //p3.add(b1);
        //p3.add(b3);

        JPanel p4 = new JPanel(new BorderLayout(10, 10));
        p4.add(new JLabel("  File name:      "), BorderLayout.WEST);
        _nameField = new JTextField("");
        p4.add(_nameField, BorderLayout.CENTER);
        p4.add(_executeButton, BorderLayout.EAST);

        JPanel p5 = new JPanel(new BorderLayout(10, 10));
        p5.add(new JLabel("  Files of type:  "), BorderLayout.WEST);
        //String[] types = {"All Files(*.*)"};
        //p5.add(new JComboBox(types));
        _filterCombo = new JComboBox(_filters);
        p5.add(_filterCombo);
        p5.add(b3, BorderLayout.EAST);

        JPanel panel = new JPanel(new BorderLayout(10, 10));
        panel.add(p2, BorderLayout.CENTER);
        panel.add(p1, BorderLayout.NORTH);

        Box panel2 = new Box(BoxLayout.Y_AXIS);
        panel2.add(Box.createVerticalStrut(10));
        panel2.add(p4);
        panel2.add(Box.createVerticalStrut(10));
        panel2.add(p5);
        panel2.add(Box.createVerticalStrut(10));

        this.getContentPane().add(panel, BorderLayout.CENTER);
        this.getContentPane().add(panel2, BorderLayout.SOUTH);

        this.pack();
        this.setSize(500, 350);
    }

    private class MyListMouseAdapter extends MouseAdapter {
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e) == false) return;
            if (e.getClickCount() < 2) return;

            JList source = (JList) e.getSource();
            int index = source.locationToIndex(e.getPoint());
            source.setSelectedIndex(index);

            Object selectedObj = source.getSelectedValue();
            if (selectedObj instanceof ListObject) {
                ListObject listObj = (ListObject) selectedObj;
                int type = listObj.getType();
                if ( type == _DIRECTORY) {

                    String oldPath = _pathField.getText();
                    if (!oldPath.endsWith("/")) oldPath = oldPath + "/";
                    setPathFieldText(oldPath + listObj.toString());
                    updateList();
                }
                else {
                    _nameField.setText((String)selectedObj.toString());
                    _returnValue = approveSelection(_nameField.getText());
                    if(_returnValue == JOptionPane.NO_OPTION)
                        return;
                    hideDialog();
                }
            }
        }
    }

    private class ExecuteListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (_enableDirectorySelection) {
                _returnValue = JFileChooser.APPROVE_OPTION;
                hideDialog();
                return;
            }
            Object selectedObj = _list.getSelectedValue();
            if (selectedObj == null) {
                String nameFieldText = _nameField.getText();

                if (nameFieldText != null) {
                    nameFieldText = nameFieldText.trim();
                    String userName = System.getProperty("user.name");
                    if(!nameFieldText.startsWith(userName))
                        nameFieldText = userName + "_" + nameFieldText;
                    if(nameFieldText.length() != 0 && !nameFieldText.endsWith(".cfg")  && !nameFieldText.endsWith(".CFG")){
                        nameFieldText += ".cfg";
                        _nameField.setText(nameFieldText);
                        repaint();
                    }
                    if (nameFieldText.length() > 0) {
                        _returnValue = approveSelection(nameFieldText);
                        if(_returnValue == JOptionPane.NO_OPTION)
                            return;
                        hideDialog();
                    }else{
                        String message = "File name field is empty.";
                        showMessage(message);
                    }
                }else{
                    String message = "File name field is empty.";
                    showMessage(message);
                }

            }
            else if (selectedObj instanceof ListObject) {
                ListObject listObj = (ListObject) selectedObj;
                int type = listObj.getType();
                if ( type == _DIRECTORY) {
                    String oldPath = _pathField.getText();
                    if (!oldPath.endsWith("/")) oldPath = oldPath + "/";
                    setPathFieldText(oldPath + listObj.toString());
                    updateList();
                }
                else {
                    String nameFieldText = _nameField.getText();
                    if (nameFieldText != null) {
                        nameFieldText = nameFieldText.trim();
                        if (nameFieldText.length() > 0) {
                            _returnValue = approveSelection(nameFieldText);
                        if (_returnValue == JOptionPane.NO_OPTION)
                            return;
                            hideDialog();
                        }else{
                            String message = "File name field is empty.";
                            showMessage(message);
                        }
                    }else{
                        String message = "File name field is empty.";
                        showMessage(message);
                    }
                }
            }
        }
    }

    private class CancelListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            _returnValue = JFileChooser.CANCEL_OPTION;
            hideDialog();
        }
    }

    private class UpListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String curDir = _pathField.getText();
            if(!naviagateUpward && curDir.equals(_homeDirectory))
                return;
            String upDir = curDir;
            if (curDir != null && curDir.lastIndexOf(File.separator) >= 0)
                upDir = curDir.substring(0, curDir.lastIndexOf(File.separator));
            if (upDir.length() == 0) {
                upDir = File.separator.equals("/") ? "/" : "C:\\";
            }
            setPathFieldText(upDir);
            updateList();
        }
    }

    private class HomeListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            setPathFieldText(_homeDirectory);
            updateList();
        }
    }

    private class ListObject {
        private int _type;
        private String _name;

        public ListObject(int type, String name) {
            _type = type;
            _name = name;
        }

        public int getType() { return _type; }

        public String toString() {
            return _name;
        }

        public ImageIcon getSymbolIcon() {
            if (_type == _DIRECTORY) return _dirIcon;
            else return _fileIcon;
        }
    }

    private class MyCellRenderer extends JLabel implements ListCellRenderer {
        public Component getListCellRendererComponent(JList list, Object value,
                            int index, boolean isSelected, boolean cellHasFocus) {
            if (value == null) return this;
            String s = value.toString();
            setOpaque(true);
            setText(s);
            if (value instanceof ListObject)
                setIcon(((ListObject)value).getSymbolIcon());
            if (isSelected) setBackground(list.getSelectionBackground());
            else setBackground(list.getBackground());
            return this;
        }
    }

    private class MyPathKeyListener extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
                updateList();
        }
    }
}
