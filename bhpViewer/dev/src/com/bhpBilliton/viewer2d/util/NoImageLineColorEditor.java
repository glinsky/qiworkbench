/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006
 
The following is important information about the license which accompanies this copy of 
the bhpViewer in either source code or executable versions (hereinafter the "Software").  
The Software, which is owned by Interactive Network Technologies, Inc. ("INT") 
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at 
http://www.gnu.org/licenses/gpl.txt.  
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
PURPOSE.  See the GNU General Public License for more details.  
You should have received a copy of the GNU General Public License along with this program; 
if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
For a license to use the Software under conditions other than in a way compliant with the 
GPL or the additional restrictions set forth in this license agreement or to purchase 
support for the Software, please contact: Interactive Network Technologies, Inc., 
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>
 
All licensees should note the following:
 
*        In order to compile and/or modify the source code versions of the Software, 
a user may require one or more of INT's proprietary toolkits or libraries.  
Although all modifications or derivative works based on the source code are governed by the 
GPL, such toolkits are proprietary to INT, and a library license is required to make use of 
such toolkits when making modifications or derivatives of the Software.  
More information about obtaining such a license can be obtained by contacting sales@int.com.
 
*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee 
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to 
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.  
This will allow the Custodian to consider integrating such revised versions into the 
version of the Software it distributes. Doing so will foster future innovation 
and overall development of the bhpViewer platform.  Notwithstanding the foregoing, 
the Custodian is under no obligation to include modifications or revisions in future 
distributions.
 
This program module may have been modified by BHP Billiton Petroleum, 
G&W Systems Consulting Corp or other third parties, and such portions are licensed 
under the GPL.  
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or 
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import com.gwsys.gw2d.model.DataShape;
import com.gwsys.gw2d.model.RenderingAttribute;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  Class for line color editing. It bases on cgLineColorEditor, and
 *               uses no image. (cgLineColorEditor uses an image but cannot load
 *               image correctly.) It also combines class cgLineColorDialog, which
 *               is not a public class. <br><br>
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */
public class NoImageLineColorEditor// implements cgAttributeEditor
{

    private Color _lineColor = Color.black;//default line color

    //listeners of this editor.
    private java.util.List _listeners = new ArrayList();

    //a handle to for line color choice dialog.
    private MyLineColorDialog _colorDg = null;

	//GUI stuff for this editor
    private JButton _buttonGUI;
    private JMenuItem _menuGUI;

    public NoImageLineColorEditor()
    {
        super();
        _buttonGUI = new LineColorButton();
        _menuGUI = new JMenuItem("Line Color");

        _colorDg = new MyLineColorDialog();

        LineColorHandler committor = new LineColorHandler();
        _buttonGUI.addActionListener(committor);
        _menuGUI.addActionListener(committor);
    }

    public void setAttribute(DataShape shape) {
        //_colorDg.setAttribute(shape);
        RenderingAttribute attr = (RenderingAttribute) (shape.getAttribute());
        if (attr == null)
            return;
        attr.setLineColor(_lineColor);
    }

    public JComponent getGUIAsButton(){
        return _buttonGUI;
    }

    public JComponent getGUIAsMenu(){
        return _menuGUI;
    }


    //======================================================
    //    set/get methods
    //======================================================
    public void setLineColor (Color color) {
         _lineColor = color;
    }

    public Color getLineColor () {
         return _lineColor;
    }

    //======================================================
    //    private methods/class
    //======================================================
	private class LineColorButton extends JButton {

        //private BufferedImage _lcImage = null;

        public LineColorButton(){
            setToolTipText("Line Color");
            //_lcImage = buildImage();
            //addMouseListener(this);
            setMaximumSize(new Dimension(25, 20));
            setMinimumSize(new Dimension(25, 20));

        }

        public void paint(Graphics g) {
            super.paint(g);
		    Dimension d = getSize();

		    Graphics2D g2 = (Graphics2D)g;
		    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
/*
		    g2.setPaint(getBackground());
		    Shape shape = new Rectangle(0, 0, d.width, d.height);
		    g2.fill(shape);

            g2.setPaint(Color.white);
            float[] dot = {1, 2};
            g2.setStroke(new BasicStroke(0.5f));
            Shape shape1 = new Rectangle(0, 0, d.width -1 , d.height - 1);
            g2.draw(shape1);
*/
		    g2.setPaint(_lineColor);
		    Shape shape = new Rectangle(6, d.height-10, d.width-12, 4);
		    g2.fill(shape);
	        //g2.drawImage(_lcImage, 3, 0, null);
	    }
    }// end of LineColorButton


    // private inner class to handle end user's action
    private class LineColorHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {

            _colorDg.setLocationRelativeTo(_buttonGUI);
            Point location =_colorDg.getLocation();
            location.translate(0, _buttonGUI.getHeight() + 30);
            _colorDg.setLocation(location);

            _colorDg.setVisible(true);
            _lineColor = _colorDg.getSelectedColor();
           	//fireAttributeEditorEvent(new cgAttributeEditorEvent(cgLineColorEditor.this));

            _buttonGUI.repaint();
		}
    }

    private class MyLineColorDialog extends JDialog {
        private JButton[] _buttonColors = new JButton[13];
        private JButton _otherColors;
        private Color _chosenColor = Color.black;
        private Color _prevColor;

        public MyLineColorDialog() {
            super();
            setTitle("Color Dialog");

            _prevColor = _chosenColor;
            setModal(true);
            setSize(120, 160);

            buildContent();
        }

        public void setAttribute(DataShape shape) {
            RenderingAttribute attr = (RenderingAttribute) (shape.getAttribute());
            attr.setLineColor(_chosenColor);
        }

        public Color getSelectedColor() { return _chosenColor; }

        private void buildContent() {
            JPanel p = new JPanel();
            p.setLayout(new GridLayout(4, 4, 5, 5));

            Color[] colors = {Color.orange, Color.pink, Color.red, Color.white,
                              Color.yellow, Color.black, Color.blue, Color.cyan,
                              Color.darkGray, Color.gray, Color.green,
                              Color.lightGray, Color.magenta};
            for (int i=0; i<colors.length; i++) {
                _buttonColors[i] = new JButton();
                _buttonColors[i].setBackground(colors[i]);
                _buttonColors[i].addActionListener(new BasicColorButtonHandler());
                p.add(_buttonColors[i]);
            }

            _otherColors = new JButton("More Colros...");
            _otherColors.addActionListener(new MoreColorHandler());

            Container content = getContentPane();
            content.setLayout(new BorderLayout(2, 5));
            content.add(p, "Center");
            content.add(_otherColors, "South");
        }

        private class BasicColorButtonHandler implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                JButton s = (JButton) (e.getSource());
                _chosenColor = s.getBackground();
                _prevColor = _chosenColor;
                setVisible(false);
            }
        }

        private class MoreColorHandler implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                _chosenColor = JColorChooser.showDialog(_otherColors, "More Colors", _prevColor);
                if (_chosenColor != null) {
                    _prevColor = _chosenColor;
                }
                else _chosenColor = _prevColor;

                setVisible(false);
            }
        }
    }
}
