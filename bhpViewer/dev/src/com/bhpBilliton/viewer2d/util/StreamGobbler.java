/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006
 
The following is important information about the license which accompanies this copy of 
the bhpViewer in either source code or executable versions (hereinafter the "Software").  
The Software, which is owned by Interactive Network Technologies, Inc. ("INT") 
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at 
http://www.gnu.org/licenses/gpl.txt.  
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
PURPOSE.  See the GNU General Public License for more details.  
You should have received a copy of the GNU General Public License along with this program; 
if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
For a license to use the Software under conditions other than in a way compliant with the 
GPL or the additional restrictions set forth in this license agreement or to purchase 
support for the Software, please contact: Interactive Network Technologies, Inc., 
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>
 
All licensees should note the following:
 
*        In order to compile and/or modify the source code versions of the Software, 
a user may require one or more of INT's proprietary toolkits or libraries.  
Although all modifications or derivative works based on the source code are governed by the 
GPL, such toolkits are proprietary to INT, and a library license is required to make use of 
such toolkits when making modifications or derivatives of the Software.  
More information about obtaining such a license can be obtained by contacting sales@int.com.
 
*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee 
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to 
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.  
This will allow the Custodian to consider integrating such revised versions into the 
version of the Software it distributes. Doing so will foster future innovation 
and overall development of the bhpViewer platform.  Notwithstanding the foregoing, 
the Custodian is under no obligation to include modifications or revisions in future 
distributions.
 
This program module may have been modified by BHP Billiton Petroleum, 
G&W Systems Consulting Corp or other third parties, and such portions are licensed 
under the GPL.  
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or 
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import com.bhpBilliton.viewer2d.ui.util.FileTools;



public class StreamGobbler extends Thread {
    private InputStream inStream;
	private String streamName = "outputStream:";
    private String outputType = "Error";
    private OutputStream outStream=null;
    private String initString = "";

    public StreamGobbler(InputStream inStream) {
    	super();
    	this.setName("StreamGobbler");
    	this.inStream = inStream;
    }
    
    public StreamGobbler(InputStream inStream, String streamName) {
    	this(inStream);
    	this.streamName = streamName;
    }
    
    public StreamGobbler(InputStream inStream, String outputType, String initString) {
    	this(inStream);
    	this.outputType = outputType;
    	this.initString = initString + "\n";
    }
    
    public StreamGobbler(InputStream inStream, String outputType, OutputStream outStream, String initString) {
        this(inStream,outputType,initString);
        this.outStream = outStream;
    }
    
    public StreamGobbler(InputStream inStream, String outputType, OutputStream outStream, String initString, String streamName) {
        this(inStream, outputType, outStream, initString);
        this.streamName = streamName;
    }

    
    public void run() {
    	BufferedReader reader=null;
//		StringBuffer output = new StringBuffer(streamName);
        Writer output = new StringWriter(2048);
        PrintWriter out = new PrintWriter(output);
		if (initString.length() > 0) 
			out.println ("\n" + initString);
		
        try {
            reader = new BufferedReader(new InputStreamReader(inStream));
            String line;
            while((line = reader.readLine()) != null)
            {
            	out.println(line);
            }
        }
        catch (Exception ex) {
        }
        finally {

        	outputStreamData(output.toString());

			FileTools.finalizeStream(reader);
			FileTools.finalizeStream(inStream);
		}
    }

	/**
	 */
	private void outputStreamData(String output) {
		BufferedWriter writer;
// configure writer
    	if (outStream == null) {
			if (outputType.equalsIgnoreCase("error")) {
				outStream = System.err;
			} else {
				outStream = System.out;
			}
		}
		writer = new BufferedWriter(new OutputStreamWriter(outStream));

//output the stream
		try {
			writer.write(output.toString() + "\n");
		} catch (IOException e) {
		}
		FileTools.finalizeStream(writer);
	}
}
