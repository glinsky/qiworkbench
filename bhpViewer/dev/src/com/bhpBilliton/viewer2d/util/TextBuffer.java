/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.util;

import java.nio.ByteBuffer;
import java.util.StringTokenizer;
import java.util.logging.Logger;

/**
 * Container for a ByteBuffer containing text.
 *
 * @author Gil Hansen.
 * @version 1.0
 */
public class TextBuffer {
    private static Logger logger = Logger.getLogger(TextBuffer.class.getName());

    /** ByteBuffer backing up the TextBuffer */
    ByteBuffer bytebuf = null;
    /** String representation of text buffer */
    String txtbuf = "";
    /** Line terminator */
    String eol = "";
    /** Length of line terminator */
    int eolSize = 0;
    /** String tokens */
    StringTokenizer tokenizer = null;

    public TextBuffer(ByteBuffer bytebuf) {
        this.bytebuf = bytebuf;
        byte[] bytes = new byte[bytebuf.position()];
        //get the bytes backing up the ByteBuffer
        bytebuf.flip();
        bytebuf.get(bytes);
        txtbuf = new String(bytes);
        eol = determineLineTerminator();
        tokenizer = new StringTokenizer(txtbuf, eol);
    }

    /**
     * Determine the line terminator.
     * @return Line terminator
     */
    private String determineLineTerminator() {
        String eol = "\r\n";   //Windows end a line with CRLF
        int eolSize = 2;
        int idx = txtbuf.indexOf(eol);
        if (idx == -1) {
            eol = "\r";    //Linux end a line with LF
            eolSize = 1;
            idx = txtbuf.indexOf(eol);
            if (idx == -1) {
                eol = "\n";    //Macs end a line with CR
                eolSize = 1;
                idx = txtbuf.indexOf(eol);
            }
        }
        return eol;
    }

    /**
     * Read the next line from the text buffer.
     * @return Next line if there is one; otherwise, null
     */
    public String readLine() {
        if (tokenizer.hasMoreTokens()) {
            return tokenizer.nextToken();
        } else return null;
    }

    /**
     * Display text buff
     */
    public String toString() {
        return "TextBuffer: txtbuf=\n"+txtbuf;
    }
}
