/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.util;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

import com.bhpBilliton.viewer2d.Bhp2DviewerConstants;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;

/**
 * ErrorDialog invokes workbench error service
 * @author ahmilb
 *
 */
public class ErrorDialog {
    /** @deprecated The viewer's display name is derivable from viewerGUI.*/
    private static String viewerDisplayName;

    /**
     * @deprecated Never used nor should it be used since all methods are static!
     */
    public ErrorDialog(BhpViewerBase viewerGUI, String viewerDisplayName) {
        this.viewerDisplayName = viewerDisplayName;
    }

    /**
     * Called when a workbench Error Dialog is needed
     * @param viewerGUI GUI instance of bhpViewer
     * @param type QIWConstants.ERROR_DIALOG or QIWConstants.WARNING_DIALOG
     * @param stack stack trace
     * @param message one-line error message
     * @param causes list of possible causes
     * @param suggestions list of suggested remedies
     */
    public static void showErrorDialog(BhpViewerBase viewerGUI, String type, StackTraceElement[] stack, String message,String[] causes, String[] suggestions) {
      ArrayList list = new ArrayList(7);
      // 0 component
      list.add(viewerGUI);
      // 1 message type
      list.add(type);
      // 2 caller's display name
      list.add(viewerGUI.getAgent().getComponentDescriptor().getDisplayName());
      // 3 stack trace
      list.add(stack);
      // 4 message
      list.add(message);
      // 5 possible causes
      list.add(causes);
      // 6 suggested remedies
      list.add(suggestions);
      // send message to start error service
      getErrorService(viewerGUI, list);
    }

    /**
     * showInternalErrorDialog starts error service for showing internal errors, for example unexpected NULL etc
     * @param viewerGUI GUI instance of bhpViewer
     * @param stack StackTrace
     * @param message one-line error message
     */
    public static void showInternalErrorDialog(BhpViewerBase viewerGUI, StackTraceElement[] stack, String message) {
        showErrorDialog(viewerGUI, QIWConstants.ERROR_DIALOG,stack,message,
                        new String[] {"Internal viewer error"},
                        new String[] {"Contact workbench support"});
    }

    /**
     * getErrorService starts WorkbenchErrorService
     * @param viewerGUI GUI instance of bhpViewer
     * @param list ArrayList of arguments
     */
    private static void getErrorService(BhpViewerBase viewerGUI, ArrayList list) {
        IMessagingManager agentMsgingMgr = viewerGUI.getAgent().getMessagingManager();
        String msgID = agentMsgingMgr.sendRequest(QIWConstants.CMD_MSG,
                                                QIWConstants.GET_ERROR_DIALOG_SERVICE_CMD,true);
        IQiWorkbenchMsg response = agentMsgingMgr.getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
        if(response == null || response.isAbnormalStatus()) {
            JOptionPane.showMessageDialog(null,"Error getting Error Service");
            return;
        }
        ComponentDescriptor errorServiceDesc = (ComponentDescriptor)response.getContent();
        msgID = agentMsgingMgr.sendRequest(QIWConstants.CMD_MSG,
                                         QIWConstants.INVOKE_ERROR_DIALOG_SERVICE_CMD,
                                         errorServiceDesc,QIWConstants.ARRAYLIST_TYPE,list,true);
        response = agentMsgingMgr.getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
    }
}
