/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.util;

//import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  Class to load image files to create icon. <br><br>
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class IconResource {
    public static final String LOGO_ICON_BHP = "icon/BHP_Logo.gif";
    public static final String LOGO_ICON_DUG = "icon/DownUnder_Logo.gif";
    public static final String LOGO_ICON = "icon/Logo_Set.gif";
    public static final String VIEWALL_ICON = "icon/ViewAll.gif";
    public static final String ZOOMRESET_ICON = "icon/ZoomReset.gif";
    public static final String ZOOMRUBBER_ICON = "icon/ZoomRubber.gif";
    public static final String ZOOMIN_ICON = "icon/ZoomIn.gif";
    public static final String ZOOMOUT_ICON = "icon/ZoomOut.gif";
    public static final String STEPPLUS_ICON = "icon/StepPlus.gif";
    public static final String STEPMINUS_ICON = "icon/StepMinus.gif";
    public static final String SHOWLAYER_ICON = "icon/ShowLayer.gif";
    public static final String HIDELAYER_ICON = "icon/HideLayer.gif";

    public static final String SEISMICLAYER_ICON = "icon/SeismicLayer.gif";
    public static final String MODELLAYER_ICON = "icon/ModelLayer.gif";
    public static final String EVENTLAYER_ICON = "icon/EventLayer.gif";
    public static final String UNKNOWNLAYER_ICON = "icon/UnknownLayer.gif";

    public static final String ELIGIBLEFILE_ICON = "icon/EligibleFile.gif";
    public static final String GENERALFILE_ICON = "icon/GeneralFile.gif";
    public static final String DIRECTORY_ICON = "icon/Directory.gif";
    public static final String HOME_ICON = "icon/Home.gif";
    public static final String UP_ICON = "icon/Up.gif";

    public static final String OSPCONNECT_ICON = "icon/OspConnect.gif";
    public static final String OSPDISCONNECT_ICON = "icon/OspConnectNo.gif";

    public static final String CSH_ICON = "icon/Help.gif";
    public static final String CSHSM_ICON = "icon/HelpSM.gif";

    public static final String PICKMODEPBP_ICON = "icon/PickPBP.gif";
    public static final String PICKMODEPTP_ICON = "icon/PickPTP.gif";
    public static final String PICKSNAPNO_ICON = "icon/PickSnapNo.gif";
    public static final String PICKSNAPMAX_ICON = "icon/PickSnapMax.gif";
    public static final String PICKSNAPMIN_ICON = "icon/PickSnapMin.gif";
    public static final String PICKSNAPZERO_ICON = "icon/PickSnapZero.gif";

    public static final String SNAPSHOT_ICON = "icon/Snapshot.gif";
    public static final String PLAY_ICON = "icon/Play.gif";
    public static final String PLAYNOW_ICON = "icon/PlayNow.gif";
    public static final String STOP_ICON = "icon/Stop.gif";
    public static final String SAVE_ICON = "icon/Save.gif";
    public static final String EDIT_ICON = "icon/Edit.gif";
    public static final String CLOSE_ICON = "icon/Close.gif";
    public static final String REWIND_ICON = "icon/Rewind.gif";
    public static final String FREWIND_ICON = "icon/Rewind2.gif";
    public static final String FORWARD_ICON = "icon/Forward.gif";
    public static final String FFORWARD_ICON = "icon/Forward2.gif";
    public static final String PREFERENCE_ICON = "icon/Option.gif";
    public static final String OPEN24_ICON = "icon/Open24.gif";


    public static final String ERROR_ICON = "icon/Error.gif";
    public static final String WARNING_ICON = "icon/Warning.gif";
    public static final String INFO_ICON = "icon/Info.gif";

    private static IconResource _instance = null;

    protected IconResource() {
    }

    public static IconResource getInstance() {
        if (_instance == null) _instance = new IconResource();
        return _instance;
    }

    public ImageIcon getImageIcon(String imageFile) {
        ImageIcon result = null;
        try {
            result = new ImageIcon(this.getClass().getResource(imageFile));
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(null,
                            "IconResource.getImageIcon exception: ["+imageFile+"]",
                            "Access Error", JOptionPane.ERROR_MESSAGE);
/*
            ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                "IconResource.getImageIcon exception: ["+imageFile+"]");
*/
            return null;
        }
        return result;
    }
}
