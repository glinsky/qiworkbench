/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.util;

import javax.swing.filechooser.*;
import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;

/**
 *  A fileFilter for any JFileChooser
 *
 * @author     Ruby Varghese
 * @created    May 12, 2003
 * @version    1.0
 */

public class GenericFileFilter extends javax.swing.filechooser.FileFilter implements FileFilter
{
  private String[] extensions;
  private String description;

  /**
   *  Constructor for the GenericFileFilter object
   *
   * @param  extensions   Description of the Parameter
   * @param  description  Description of the Parameter
   */
  public GenericFileFilter( String[] extensions, String description )
  {
    this.extensions = extensions;
    this.description = description;

    cleanExtensions ();
  }

  private void cleanExtensions () {
    //FIXME - this is temporary until all the generic file filters in their code get fixed
    for (int i=0;i<extensions.length;i++) {
        if (extensions[i].charAt(0) == '.' && extensions[i].length() > 2) {
            extensions[i] = extensions[i].substring(1);
        }
    }
  }

  /**
   * Gets the description attribute of the MyFileFilter object
   *
   * @return The description value
   */
    public String getDescription() {
        //    return nice description;
        return (this.description);
    }
    public String getNiceDescription() {
        //    return nice description;
        return (this.description + " (" + join(extensions, ", ") + ")");
    }

  private String join (String[] array, String c) {
      StringBuffer buf = new StringBuffer();
      for (int i=0; i < array.length; i++)
      {
          if (i != 0)
              buf.append (c);
          buf.append (array[i]);
      }
      return buf.toString();
  }

  public String toString() {
      return this.description;
  }

  public boolean acceptFilename(String filename) {
      return matchesExtensionOf(filename, Arrays.asList( extensions ));
  }

  /**
   *  Determine if file has the proper extensions
   *
   * @param  file  Description of the Parameter
   * @return       Description of the Return Value
   */
  public boolean accept( File file )
  {
    return accept( file, Arrays.asList( extensions ) );
  }

  /**
   *  Check whether the file selected matches the extension
   *
   * @param  file  Description of the Parameter
   * @param  ext   Description of the Parameter
   * @return       Description of the Return Value
   */
  private static boolean matchesExtensionOf( String filename, java.util.List ext )
  {
      // if there is no extension, everything is fine.
//      if (ext.size() == 0) return true;
      // Assume no match until determined otherwise
      boolean matches = false;

      // Pick off extension from file
      String extension = getExtension(filename);

      // Compare extension with all accepted extensions
      matches = ext.contains( extension );

      return matches;
  }

  private static String getExtension(String s) {
    int i = s.lastIndexOf('.');
        return (i>0 && i<s.length()-1) ? s.substring(i+1).toLowerCase().trim() :  "";
    }

  /**
   *  Returns true if this is a file that has a matching extension.
   *
   * @param  file  to test for acceptance
   * @param  ext   Description of the Parameter
   * @return       true if esri; false otherwise
   */
  public static boolean accept( File file, java.util.List ext )
  {
    if ( file.isDirectory() )   //WHY? CHECKING ON A FILE!
    {
      return true;              //SHOULD RETURN false!
    }
    return file.isFile() && file.canRead() && matchesExtensionOf( file.getAbsolutePath(), ext );
  }
}
