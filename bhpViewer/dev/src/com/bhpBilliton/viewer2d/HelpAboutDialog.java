/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.*;
import java.util.Properties;

import javax.swing.*;

import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.VersionInfo;
import com.bhpBilliton.viewer2d.actions.ViewLicenseAction;
import com.bhpBilliton.viewer2d.util.IconResource;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This action will pop up a dialog showing basic
 *               information of the program. <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class HelpAboutDialog extends JDialog {
    private Component _viewer;
    private ImageIcon _icon;
    private Properties props;
    
    public HelpAboutDialog(Component viewer, Properties props) {
        _viewer = viewer;
        _icon = IconResource.getInstance().getImageIcon(IconResource.LOGO_ICON);
        this.props = props;
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });    
    }

    public void createAndShowGUI() {
        //String viewerVersion = "bhpViewer version 1.0 ["+VersionInfo.dateString+"]";
    	String viewer = "bhpViewer";
        JEditorPane aboutTextEditorPane = new JEditorPane();
        aboutTextEditorPane.setEditable(false);
        aboutTextEditorPane.setContentType("text/html");

        StringBuffer sb = new StringBuffer();
        sb.append("<font face=\"Arial\" size=3><b>");
        sb.append(viewer +" -- </b><br>Copyright (C) 2006 Interactive Network Technologies, Inc.<br>");
        sb.append("<br>Portions of bhpViewer are owned by G&W Systems Consulting Corp. <br>");
        sb.append("and BHP Billiton Petroleum and licensed pursuant to the GPL.");
        sb.append("<br><br>");
        
        String versionProp = "";
       	String buildProp = "";
       	
       	if(props != null){
       		versionProp = props.getProperty("project.version");
       		buildProp = props.getProperty("project.build");
       	}
       	
        if(versionProp == null) {
             versionProp = "";
        }
        
        if(buildProp == null) {
            buildProp = "";
        }
            
        sb.append("Version: " + versionProp + "<br>");
        sb.append("Build id: " + buildProp + "<br><br>");
        
        
        sb.append("bhpViewer comes with ABSOLUTELY NO WARRANTY.<br><br>");
        sb.append("This is free software, and you are welcome to redistribute it <br>under certain conditions.<br>");
        sb.append("Click on the <b>License</b> button for more details.");
        sb.append("</font>");
        aboutTextEditorPane.setText(sb.toString());

        getContentPane().add(new JLabel(_icon), BorderLayout.WEST);
        getContentPane().add(aboutTextEditorPane, BorderLayout.EAST);
        JButton license = new JButton("License");
        license.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent evt) {
        		dispose();
        		JOptionPane.showMessageDialog(JOptionPane.getRootFrame(), 
        				new ViewLicenseAction(), "License",JOptionPane.INFORMATION_MESSAGE);
        	}
        });
        JButton okButton = new JButton("Close");
        okButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent evt) {
        		dispose();
        	}
        });
        JPanel bpanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        bpanel.add(license);
        bpanel.add(okButton);
        getContentPane().add(bpanel, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(_viewer);
        setVisible(true);
    }	
}
