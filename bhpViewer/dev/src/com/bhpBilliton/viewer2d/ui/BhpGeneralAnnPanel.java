/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


import com.bhpBilliton.viewer2d.AbstractPlot;
import com.bhpBilliton.viewer2d.BhpMapLayer;
import com.bhpBilliton.viewer2d.BhpWindow;
import com.bhpBilliton.viewer2d.ui.util.TextFieldDocument;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.util.PlotConstants;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This panel is used to view and change the miscellaneous
 *               annoation setting. This includs four labels one on each
 *               side, the title, and the color bar. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpGeneralAnnPanel extends JPanel {
    private BhpWindow _window;

    private TextFieldDocument _titleDocument;
    private String _oldTitle;
    private int _titleLoc;
    private int _oldTitleLoc;
    private TextFieldDocument _leftDocument;
    private String _oldLeft;
    private TextFieldDocument _rightDocument;
    private String _oldRight;
    private TextFieldDocument _topDocument;
    private String _oldTop;
    private TextFieldDocument _bottomDocument;
    private String _oldBottom;
    private int _cbarLoc;
    private int _oldCbarLoc;

    private JComboBox _titleLocation;
    private JLabel _titleLabel;
    private JTextField _titleField;
    private JTextField _leftField;
    private JTextField _rightField;
    private JTextField _topField;
    private JTextField _bottomField;
    private JComboBox _cbarField;

    /**
     * Constructs a BhpGeneralAnnPanel and sets up the GUI.
     */
    public BhpGeneralAnnPanel() {
        super();
        _window = null;

        _titleDocument = new TextFieldDocument();
        _titleDocument.setMaxLength(50);
        _titleLabel = new JLabel("Text");
        _titleField =new JTextField();
        _titleField.setDocument(_titleDocument);

        _titleLocation = new JComboBox();
        _titleLocation.setEditable(false);
        _titleLocation.addItem("NONE");
        _titleLocation.addItem("TOP");
        //_titleLocation.addItem("BOTTOM");
        _titleLocation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox) e.getSource();
                boolean enable = true;
                String selItem = (String) cb.getSelectedItem();
                if (selItem.equals("NONE")) enable = false;
                _titleLabel.setEnabled(enable);
                _titleField.setEnabled(enable);
            }
        });

        JPanel panel1 = new JPanel(new GridLayout(2, 2));
        panel1.setBorder(BorderFactory.createTitledBorder("Title"));
        panel1.setMaximumSize(new Dimension(300, 30));
        panel1.add(new JLabel("Location"));
        panel1.add(_titleLocation);
        panel1.add(_titleLabel);
        panel1.add(_titleField);

        _leftDocument = new TextFieldDocument();
        _leftDocument.setMaxLength(50);
        _rightDocument = new TextFieldDocument();
        _rightDocument.setMaxLength(50);
        _topDocument = new TextFieldDocument();
        _topDocument.setMaxLength(50);
        _bottomDocument = new TextFieldDocument();
        _bottomDocument.setMaxLength(50);
        _leftField = new JTextField();
        _leftField.setDocument(_leftDocument);
        _rightField = new JTextField();
        _rightField.setDocument(_rightDocument);
        _topField = new JTextField();
        _topField.setDocument(_topDocument);
        _bottomField = new JTextField();
        _bottomField.setDocument(_bottomDocument);
      _rightField.setEnabled(false); //disable
      _bottomField.setEnabled(false);//disable
        JPanel panel2 = new JPanel(new GridLayout(4, 2));
        panel2.setBorder(BorderFactory.createTitledBorder("Labels"));
        panel2.setMaximumSize(new Dimension(300, 60));
        panel2.add(new JLabel("Left"));
        panel2.add(_leftField);
        panel2.add(new JLabel("Right"));
        panel2.add(_rightField);
        panel2.add(new JLabel("Top"));
        panel2.add(_topField);
        panel2.add(new JLabel("Bottom"));
        panel2.add(_bottomField);

        _cbarField = new JComboBox();
        _cbarField.setEditable(false);
        _cbarField.addItem("NONE");
        _cbarField.addItem("LEFT");
        _cbarField.addItem("RIGHT");
        //_cbarField.addItem("TOP");
        //_cbarField.addItem("BOTTOM");

        JPanel panel3 = new JPanel(new GridLayout(1, 2));
        panel3.setMaximumSize(new Dimension(300, 15));
        panel3.add(new JLabel("   Color Bar"));
        panel3.add(_cbarField);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(Box.createGlue());
        this.add(panel1);
        add(Box.createGlue());
        this.add(panel2);
        add(Box.createGlue());
        this.add(panel3);
        add(Box.createGlue());
    }

    /**
     * Enables or diables the components in the panel for
     * either an active or a grayed-out state.
     */
    public void setAllComponentEnabled(boolean v) {
        _titleLocation.setEnabled(v);
        _titleField.setEnabled(v);
        _leftField.setEnabled(v);
        _rightField.setEnabled(v);
        _topField.setEnabled(v);
        _bottomField.setEnabled(v);
        _cbarField.setEnabled(v);
    }

    //LM
    public void initWithMapLayer(BhpMapLayer layer, AbstractPlot plot) {
        clearTitles(layer);
        if (layer != null) {
            String lTitle = getLeftAxisTitle(layer);
            String tTitle = getTopAxisTitle(layer);

            if (! plot.getLabelL().equals(lTitle)) {
                lTitle = plot.getLabelL();
            }
            if (! plot.getLabelT().equals(tTitle)) {
                tTitle = plot.getLabelT();
            }

            setTitles (layer, lTitle, tTitle);
        }
    }

    public void initWithMapLayer(BhpMapLayer layer) {
        clearTitles(layer);
        if (layer != null) {
            String lTitle = getLeftAxisTitle(layer);
            String tTitle = getTopAxisTitle(layer);

            setTitles (layer, lTitle, tTitle);
        }
    }

    private String getLeftAxisTitle (BhpMapLayer layer) {
        String rval;
        if (layer.isTransposeImage()) {
            rval = layer.getParameter().getMapHorizontalKeyName();
        } else {
            rval = layer.getParameter().getVerticalName();
        }
        return (rval);
    }
    private String getTopAxisTitle (BhpMapLayer layer) {
        String rval;
        if (layer.isTransposeImage()) {
            rval = layer.getParameter().getVerticalName();
        } else {
            rval = layer.getParameter().getMapHorizontalKeyName();
        }
        return (rval);
    }

    /**
     * Retrieves the <code>{@link BhpMapLayer}</code>
     * related information and fill the fields of the GUI.
     */
    private void setTitles(BhpMapLayer layer, String lTitle, String tTitle) {
        try {
            _leftDocument.insertString(0, lTitle, null);
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(layer.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpGeneralAnnPanel.initWithMapLayer exception");
        }
        try {
            _topDocument.insertString(0, tTitle, null);
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(layer.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpGeneralAnnPanel.initWithMapLayer exception");
        }
    }

    /**
     *
     */
    private void clearTitles(BhpMapLayer layer) {
        try {
            _leftDocument.remove(0, _leftDocument.getLength());
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(layer.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpGeneralAnnPanel.initWithMapLayer exception");
        }
        try {
            _topDocument.remove(0, _topDocument.getLength());
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(layer.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpGeneralAnnPanel.initWithMapLayer exception");
        }
    }

    /**
     * Retrieves the <code>{@link BhpWindow}</code>
     * related information and fill the fields of the GUI.
     */
    public void initWithBhpWindow(BhpWindow win) {
        _window = win;
        AbstractPlot plot = _window.getBhpPlot();

        _oldCbarLoc = _cbarLoc = plot.getColorbarLoc();
        switch (_cbarLoc) {
            case PlotConstants.LEFT:
                _cbarField.setSelectedItem("LEFT");
                break;
            case PlotConstants.RIGHT:
                _cbarField.setSelectedItem("RIGHT");
                break;
            case PlotConstants.TOP:
                _cbarField.setSelectedItem("TOP");
                break;
            case PlotConstants.BOTTOM:
                _cbarField.setSelectedItem("BOTTOM");
                break;
            default:
                _cbarField.setSelectedItem("NONE");
        }

        _oldTitleLoc = _titleLoc = plot.getTitleLoc();
        switch(_titleLoc) {
            case PlotConstants.TOP:
                _titleLocation.setSelectedItem("TOP");
                break;
            case PlotConstants.BOTTOM:
                _titleLocation.setSelectedItem("BOTTOM");
                break;
            default:
                _titleLocation.setSelectedItem("NONE");
        }

        try {
            _oldTitle = plot.getTitle();
            _titleDocument.remove(0, _titleDocument.getLength());
            _titleDocument.insertString(0, _oldTitle, null);
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(win.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpGeneralAnnPanel.initWithMapLayer exception");
        }
        try {
            _oldLeft = plot.getLabelL();
            _leftDocument.remove(0, _leftDocument.getLength());
            _leftDocument.insertString(0, _oldLeft, null);
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(win.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpGeneralAnnPanel.initWithMapLayer exception");
        }
        try {
            _oldRight = plot.getLabelR();
            _rightDocument.remove(0, _rightDocument.getLength());
            _rightDocument.insertString(0, _oldRight, null);
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(win.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpGeneralAnnPanel.initWithMapLayer exception");
        }
        try {
            _oldTop = plot.getLabelT();
            _topDocument.remove(0, _topDocument.getLength());
            _topDocument.insertString(0, _oldTop, null);
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(win.getViewer(), Thread.currentThread().getStackTrace(),
                                               "BhpGeneralAnnPanel.initWithMapLayer exception");
        }
        try {
            _oldBottom = plot.getLabelB();
            _bottomDocument.remove(0, _bottomDocument.getLength());
            _bottomDocument.insertString(0, _oldBottom, null);
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(win.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpGeneralAnnPanel.initWithMapLayer exception");
        }
    }

    /**
     * Retrieves valus from the GUI components and
     * updates the annotation settings of
     * <code>{@link AbstractPlot}</code>.
     * @return a boolean indicates if the annotation is changed
     * and needs an update.
     */
    public boolean updateBhpWindow() {
        boolean changed = false;
        AbstractPlot plot = _window.getBhpPlot();

        String selItem = (String) _titleLocation.getSelectedItem();
        if (selItem.equals("TOP")) _titleLoc = PlotConstants.TOP;
        else if (selItem.equals("BOTTOM")) _titleLoc = PlotConstants.BOTTOM;
        else _titleLoc = PlotConstants.NONE;
        if (_titleLoc != _oldTitleLoc) {
            plot.setTitleLoc(_titleLoc);
            _oldTitleLoc = _titleLoc;
            changed = true;
        }

        selItem = (String) _cbarField.getSelectedItem();
        if (selItem.equals("LEFT"))        _cbarLoc = PlotConstants.LEFT;
        else if (selItem.equals("RIGHT"))  _cbarLoc = PlotConstants.RIGHT;
        else if (selItem.equals("TOP"))    _cbarLoc = PlotConstants.TOP;
        else if (selItem.equals("BOTTOM")) _cbarLoc = PlotConstants.BOTTOM;
        else                               _cbarLoc = PlotConstants.NONE;
        if (_cbarLoc != _oldCbarLoc) {
            plot.setColorbarLoc(_cbarLoc);
            _oldCbarLoc = _cbarLoc;
            changed = true;
        }

        String value;
        try {
            value = _titleDocument.getText(0, _titleDocument.getLength());
            if (!value.equals(_oldTitle)) {
                plot.setTitle(value);
                _oldTitle = value;
                changed = true;
            }
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_window.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpGeneralAnnPanel.initWithMapLayer exception");
        }

        try {
            value = _leftDocument.getText(0, _leftDocument.getLength());
            if (!value.equals(_oldLeft)) {
                plot.setLabelL(value);
                _oldLeft = value;
                changed = true;
            }
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_window.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpGeneralAnnPanel.initWithMapLayer exception");
        }

        try {
            value = _rightDocument.getText(0, _rightDocument.getLength());
            if (!value.equals(_oldRight)) {
                plot.setLabelR(value);
                _oldRight = value;
                changed = true;
            }
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_window.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpGeneralAnnPanel.updateBhpWindow exception");
        }

        try {
            value = _topDocument.getText(0, _topDocument.getLength());
            if (!value.equals(_oldTop)) {
                plot.setLabelT(value);
                _oldTop = value;
                changed = true;
            }
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_window.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpGeneralAnnPanel.updateBhpWindow exception");
        }

        try {
            value = _bottomDocument.getText(0, _bottomDocument.getLength());
            if (!value.equals(_oldBottom)) {
                plot.setLabelB(value);
                _oldBottom = value;
                changed = true;
            }
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_window.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpGeneralAnnPanel.updateBhpWindow exception");
        }

        return changed;
    }
}
