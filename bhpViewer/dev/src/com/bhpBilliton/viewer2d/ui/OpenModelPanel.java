/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;

import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpMapLayer;
import com.bhpBilliton.viewer2d.BhpSeismicTableModel;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.data.GeneralDataSource;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This panel is for model data. <br>
 *               Besides whatever is contained in its parent class,
 *               <code>{@link OpenGeneralPanel}</code>,
 *               this panel uses a JCombobox to display available
 *               event names and available layers, where available
 *               layers is for map display only.
 *               There is also a checkbox named "Synchronize". Users use it
 *               to specify whether the layer should be synchronized
 *               with the other layers on the name of attribute to be displayed.
 *               <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class OpenModelPanel extends OpenGeneralPanel {
    private JPanel _propertyPanel;
    private JCheckBox _propertySync;
    private JCheckBox _layerSync;
    //private ButtonGroup _properties;
    private JComboBox _propertyCombo;
    private JComboBox _layerCombo;
    private int _dataOrder;

    private BhpLayer _refLayer;
    private BhpViewerBase viewer;

    /**
     * Constructs a new OpenModelPanel.
     * @param refLayer reference layer.
     * @param order data order. It can be BhpLayer.BHP_DATA_ORDER_MAPVIEW, or
     *        BhpLayer.BHP_DATA_ORDER_CROSSSECTION.
     */
    public OpenModelPanel(BhpViewerBase viewer, BhpLayer refLayer, int order) {
        super(refLayer);
        this.viewer = viewer;
        _refLayer = refLayer;
        _dataOrder = order;
        boolean grayFirstRow = false;
        if (_dataOrder == BhpViewer.BHP_DATA_ORDER_MAPVIEW) {
            grayFirstRow = true;
        }
        setupGUI(grayFirstRow);
    }

    /**
     * Finds out if the property is set to synchronize.
     * @return boolean flag for property synchronization
     */
    public boolean getPropertySyncValue() {
        return _propertySync.isSelected();
    }

    /**
     * Finds out if the layer is set to synchronize.
     * @return boolean flag for layer synchronization
     */
    public boolean getLayerSyncValue() {
        return _layerSync.isSelected();
    }

    /**
     * Retrieves the name of the selected property.
     * @return the name of the selected property.
     */
    public String getSelectedPropertyName() {
        if (_propertyCombo == null) return null;
        Object selectedObj = _propertyCombo.getSelectedItem();
        if (selectedObj == null) return null;
        String name = selectedObj.toString();
        if (name==null || name.length()==0) return null;
        return name;
    }

    /**
     * Retrieves the name of the selected layer. <br>
     * A JCombobox is displaying all the available layer and
     * uesers can choose one when creating a
     * <code>{@link BhpMapLayer}</code> with
     * transposed model data.
     * @return the name of the selected layer.
     */
    public String getSelectedLayerName() {
        if (_layerCombo == null) return null;
        Object selectedObj = _layerCombo.getSelectedItem();
        if (selectedObj == null) return null;
        String name = selectedObj.toString();
        if (name==null || name.length()==0) return null;
        return name;
    }

    public void initWithLayer(BhpLayer alayer) {
        if (alayer.getDataType() != BhpViewer.BHP_DATA_TYPE_MODEL) return;

        BhpSeismicTableModel model = alayer.getParameter();
        _propertyCombo.removeAllItems();
        buildPropertyCombo(model.getProperties(), alayer.getPropertyName());
        _propertySync.setSelected(alayer.getPropertySync());
        _layerCombo.removeAllItems();
        if (_dataOrder == BhpViewer.BHP_DATA_ORDER_MAPVIEW) {
            _layerSync.setSelected(((BhpMapLayer)alayer).getModelLayerSync());
            String traclAtt = model.getValueAt(0, 1).toString();
            int dashIndex = traclAtt.indexOf("-");
            int squareIndex = traclAtt.indexOf("[");
            if (squareIndex < 0) squareIndex = traclAtt.length() - 1;
            int layerNumber = Integer.parseInt(traclAtt.substring(dashIndex+1, squareIndex).trim());
            buildLayerCombo(layerNumber, alayer.getPropertyName());
        }

        _orderEditor.removeAllItems();
        _tableModel.cleanupTableModel();
        Vector data = model.getDataVector();
        for (int i=0; i<data.size(); i++) {
            _tableModel.addRow(new Vector((Vector) data.elementAt(i)));
            _orderEditor.addItem("" + (i+1));
        }
        _table.getColumnModel().getColumn(3).setCellEditor(new DefaultCellEditor(_orderEditor));

        _verticalPanel.initVertical(_tableModel, alayer.getDataSource());
    }

    public int applyToLayer(BhpLayer alayer) {
        if (alayer.getDataType() != BhpViewer.BHP_DATA_TYPE_MODEL) return 0;

        //BhpModelLayer layer = (BhpModelLayer) alayer;
        //layer.setPropertySync(_propertySync.isSelected());
        _verticalPanel.applyVertical(_tableModel);

        int changeFlag = 0;
        alayer.setPropertySync(_propertySync.isSelected());
        if (alayer instanceof BhpMapLayer)
            ((BhpMapLayer)alayer).setModelLayerSync(_layerSync.isSelected());

        String newPName = this.getSelectedPropertyName();
        String newLName = this.getSelectedLayerName();
        String newName = newPName;
        if (_dataOrder == BhpViewer.BHP_DATA_ORDER_MAPVIEW && newName != null && newLName != null) {
            if (newName.indexOf(':')>=0) {                           //LM: added to fix Issue 95
                newName = newName.substring(0,newName.indexOf(':')); //LM: added to fix Issue 91
            }
            newName = newName.trim() + ":" + newLName.trim();
        }
        if (!newName.equals(alayer.getPropertyName())) {
            changeFlag = changeFlag | BhpViewer.MODEL_FLAG;
            alayer.setPropertyName(newName);
        }

        BhpSeismicTableModel model = alayer.getParameter();
        model.clearRowsChanged();
        if (model.getRowCount() != _tableModel.getRowCount() ||
            model.getColumnCount() != _tableModel.getColumnCount())
            changeFlag = changeFlag | BhpViewer.DATASELECTION_FLAG;

        if (changeFlag != 0) {
            Vector colNames = new Vector();
            for (int i=0; i<_tableModel.getColumnCount(); i++)
                colNames.addElement(_tableModel.getColumnName(i));
            model.setDataVector(_tableModel.getDataVector(), colNames);
            return changeFlag;
        }

        Object original, current;
        for (int i=0; i<_tableModel.getRowCount(); i++) {
            for (int j=0; j<_tableModel.getColumnCount(); j++) {
                original = model.getValueAt(i, j);
                current = _tableModel.getValueAt(i, j);
                if (original.equals(current) == false) {
                    if (j == 2) {
                        boolean chosenRangeChanged = model.setChosenRangeAt(current, i);
                        if (!chosenRangeChanged) continue;
                    }
                    else {
                        model.setValueAt(current, i, j);
                    }
                    if (j==2 || j==4 || j==7 || j==8) model.setRowsChanged(i);
                    if (j!=1 && j!=6) changeFlag = changeFlag | BhpViewer.DATASELECTION_FLAG;
                }
            }
        }
        return changeFlag;
    }

    public void describeData(GeneralDataSource dataSource) {
        _tableModel.cleanupTableModel();
        _tableModel.setDataSource(dataSource);
        _propertyCombo.removeAllItems();
        _layerCombo.removeAllItems();

        int layerNumber = 0;
        String[] names = dataSource.getHeaders();
        String value = null;
        for (int i=0; i<names.length; i++) {
            value = dataSource.getHeaderSetting(names[i]);
            _tableModel.addRowData(names[i], value);
            if (names[i].indexOf("tracl") >= 0) {
                // find number of layers, it should not have negative number
                int dashIndex = value.indexOf("-");
                int squareIndex = value.indexOf("[");
                layerNumber = Integer.parseInt(value.substring(dashIndex+1, squareIndex).trim());
            }
        }
        String properties = dataSource.getProperties();
        buildPropertyCombo(properties, null);
        buildLayerCombo(layerNumber, null);
        _tableModel.setProperties(properties);
        _tableModel.setDataAttributeString(dataSource.getDataAtt());

        if (_refLayer != null) {
            _tableModel.initWithAnother(_refLayer.getParameter());
        }
        _propertyPanel.validate();
        _propertyPanel.repaint();

        _orderEditor.removeAllItems();
        for (int i=0; i<_tableModel.getRowCount(); i++) {
            _orderEditor.addItem("" + (i+1));
        }
        _table.getColumnModel().getColumn(3).setCellEditor(new DefaultCellEditor(_orderEditor));

        _verticalPanel.initVertical(_tableModel, dataSource.getDataSourceName());
    }

    /**
     * Builds the GUI of the panel itself.
     */
    protected void buildPanel() {
        JPanel tablePanel = new JPanel(new BorderLayout());
        _tableModel = new BhpSeismicTableModel(viewer, false);
        //_table = new JTable(_tableModel);
        _table.setModel(_tableModel);
        tablePanel.add(new JScrollPane(_table), BorderLayout.CENTER);

        _propertyPanel = new JPanel(new GridLayout(1, 5));
        _propertyPanel.setBorder(new TitledBorder("Selected Property"));
        _propertyPanel.setPreferredSize(new Dimension(300, 120));
        _propertySync = new JCheckBox("    Synchronize");
        _layerSync = new JCheckBox("    Synchronize");
        _propertyCombo = new JComboBox();
        _layerCombo = new JComboBox();

        _propertyPanel.add(new JLabel("    Property"));
        _propertyPanel.add(_propertyCombo);
        _propertyPanel.add(_propertySync);
        if (_dataOrder == BhpViewer.BHP_DATA_ORDER_MAPVIEW) {
            _propertyPanel.add(new JLabel("          Layer"));
            _propertyPanel.add(_layerCombo);
            _propertyPanel.add(_layerSync);
        }
        else {
            _propertyPanel.add(new JLabel(""));
            _propertyPanel.add(new JLabel(""));
            _propertyPanel.add(new JLabel(""));
        }

        Box box = new Box(BoxLayout.Y_AXIS);
        box.add(tablePanel);
        if (_dataOrder == BhpViewer.BHP_DATA_ORDER_MAPVIEW) box.add(_verticalPanel);
        box.add(_propertyPanel);

        this.add(box, BorderLayout.CENTER);
    }

    private void buildLayerCombo(int layers, String selectedP) {
        for (int i=0; i<layers; i++)
            _layerCombo.addItem("" + (i+1));
        if (selectedP != null) {
            if (selectedP.indexOf(":") > 0) {
                selectedP = selectedP.substring(selectedP.indexOf(":")+1).trim();
                _layerCombo.setSelectedItem(selectedP);
            }
        }
        if (_layerCombo.getSelectedItem() == null && _layerCombo.getItemCount() > 0) {
            //System.out.println("LayerCombo set to default 0");
            _layerCombo.setSelectedIndex(0);
        }
    }

    private void buildPropertyCombo(String properties, String selectedP) {
        StringTokenizer pstk = new StringTokenizer(properties, " ");
        int index = 0;
        while(pstk.hasMoreTokens()) {
            String pname = pstk.nextToken().trim();
            if (pname.length() == 0) continue;
            _propertyCombo.addItem(pname);
            //if (pname.equals(selectedP)) _propertyCombo.setSelectedIndex(index);
            index++;
        }
        if (selectedP != null) {
            if (selectedP.indexOf(":") > 0) {
                selectedP = selectedP.substring(0, selectedP.indexOf(":"));
            }
            _propertyCombo.setSelectedItem(selectedP);
        }
        if (_propertyCombo.getSelectedItem() == null) {
            //System.out.println("propertyCombo set to default 0");
            _propertyCombo.setSelectedIndex(0);
        }
/*
        // I think the check box should be there all the time
        // it should not hurt event in the open data dialog
        if (selectedP != null)  // only add this for the property dialog, not when open data
            _propertyPanel.add(_propertySync, 0);
*/
    }
}
