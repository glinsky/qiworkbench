/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui.util;

import javax.swing.text.AttributeSet;
import javax.swing.text.PlainDocument;
import javax.swing.text.BadLocationException;

import java.awt.Toolkit;
import java.text.*;

/**
 * Defines text field document. Based on INT
 */
public class TextFieldDocument extends PlainDocument {

	/** Declares the variable to keep the maximum allowed length of text field content */
	private int _maxLength = 0;


	/**
	 * Sets the maximum allowed length for text field document content.</p>
	 *
	 * @param columns the maximum number of symbols in text field
	 */
	public void setMaxLength(int columns) {
		_maxLength = columns;
	}

	/**
	 * Inserts some content into the document.</p>
	 *
	 * @param offs the starting offset <code>>= 0</code>
	 * @param str  the string to insert; does nothing with null/empty strings
	 * @param a    the attributes for the inserted content
	 */
	public void insertString(int offs, String str, AttributeSet a)
			throws BadLocationException {

		if (!checkLength())
			throw new BadLocationException(str, offs);

		super.insertString(offs, str, a);
	}

	/**
	 * Inserts some content into the document.</p>
	 *
	 * @param offs the starting offset <code>>= 0</code>
	 * @param value  the float to insert;
	 * @param a    the attributes for the inserted content
	 */
	public void insertFloat(int offs, float value, AttributeSet a)
			throws BadLocationException {
		DecimalFormat df = new DecimalFormat();
		String str = df.format(value);
		insertString(offs, str, a);
	}

	/**
	 * Inserts some content into the document.</p>
	 *
	 * @param offs the starting offset <code>>= 0</code>
	 * @param value  the int to insert;
	 * @param a    the attributes for the inserted content
	 */
	public void insertInt(int offs, int value, AttributeSet a)
			throws BadLocationException {
		DecimalFormat df = new DecimalFormat();
		String str = df.format(value);
		insertString(offs, str, a);
	}

	/**
	 * Gets text value as float.</p>
	 */
	public float getFloat() throws NumberFormatException {
		try {
			DecimalFormat df = new DecimalFormat();
			return df.parse(getText(0, this.getLength())).floatValue();
		} catch (ParseException e) {
			throw new NumberFormatException();
		} catch (BadLocationException e) {
			throw new NumberFormatException();
		}
	}

	/**
	 * Gets text value as int.</p>
	 */

	public int getInt() throws NumberFormatException {
		try {
			DecimalFormat df = new DecimalFormat();
			return df.parse(getText(0, this.getLength())).intValue();
		} catch (ParseException e) {
			throw new NumberFormatException();
		} catch (BadLocationException e) {
			throw new NumberFormatException();
		}
	}

	/**
	 * Checks the length of content of text field document.</p>
	 *
	 * @return <code>true</code> if length of content <= max allowed,
	 *         <code>false</code> otherwise
	 */
	protected boolean checkLength() {

		if (getLength() == _maxLength) {
			Toolkit.getDefaultToolkit().beep();
			return false;
		}

		return true;
	}

}
