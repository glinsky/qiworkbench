/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui.util;

import java.awt.Color;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;

import org.w3c.dom.*;

//import com.bhpBilliton.viewer2d.util.ErrorDialog;

/**
 * @author lukem
 *
 */
public class ElementAttributeReader implements Element {

    Element emt;

    public ElementAttributeReader(Element emt) {
        this.emt = emt;
    }
    public ElementAttributeReader(Node emt) {
        this.emt = (Element) emt;
    }
    public void setNode(Node emt) {
        this.emt = (Element) emt;
    }
    /* (non-Javadoc)
     * @see org.w3c.dom.Element#getTagName()
     */
    public String getTagName() {
        return emt.getTagName();
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#getAttribute(java.lang.String)
     */
    public String getAttribute(String name) {
        return emt.getAttribute( name);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#setAttribute(java.lang.String, java.lang.String)
     */
    public void setAttribute(String name, String value) throws DOMException {
        emt.setAttribute( name,  value);

    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#removeAttribute(java.lang.String)
     */
    public void removeAttribute(String name) throws DOMException {
        emt.removeAttribute( name);

    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#getAttributeNode(java.lang.String)
     */
    public Attr getAttributeNode(String name) {
        return emt.getAttributeNode( name);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#setAttributeNode(org.w3c.dom.Attr)
     */
    public Attr setAttributeNode(Attr newAttr) throws DOMException {
        return emt.setAttributeNode( newAttr);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#removeAttributeNode(org.w3c.dom.Attr)
     */
    public Attr removeAttributeNode(Attr oldAttr) throws DOMException {
        return emt.removeAttributeNode( oldAttr);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#getElementsByTagName(java.lang.String)
     */
    public NodeList getElementsByTagName(String name) {
        return emt.getElementsByTagName( name);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#getAttributeNS(java.lang.String, java.lang.String)
     */
    public String getAttributeNS(String namespaceURI, String localName) throws DOMException {
        return emt.getAttributeNS( namespaceURI,  localName);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#setAttributeNS(java.lang.String, java.lang.String, java.lang.String)
     */
    public void setAttributeNS(String namespaceURI, String qualifiedName, String value) throws DOMException {
        emt.setAttributeNS( namespaceURI,  qualifiedName,  value);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#removeAttributeNS(java.lang.String, java.lang.String)
     */
    public void removeAttributeNS(String namespaceURI, String localName) throws DOMException {
        emt.removeAttributeNS( namespaceURI,  localName);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#getAttributeNodeNS(java.lang.String, java.lang.String)
     */
    public Attr getAttributeNodeNS(String namespaceURI, String localName) throws DOMException {
        return emt.getAttributeNodeNS( namespaceURI,  localName);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#setAttributeNodeNS(org.w3c.dom.Attr)
     */
    public Attr setAttributeNodeNS(Attr newAttr) throws DOMException {
        return emt.setAttributeNodeNS( newAttr);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#getElementsByTagNameNS(java.lang.String, java.lang.String)
     */
    public NodeList getElementsByTagNameNS(String namespaceURI, String localName) throws DOMException {
        return emt.getElementsByTagNameNS( namespaceURI,  localName);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#hasAttribute(java.lang.String)
     */
    public boolean hasAttribute(String name) {
        return emt.hasAttribute( name);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#hasAttributeNS(java.lang.String, java.lang.String)
     */
    public boolean hasAttributeNS(String namespaceURI, String localName) throws DOMException {
        return emt.hasAttributeNS( namespaceURI,  localName);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getNodeName()
     */
    public String getNodeName() {
        return (emt.getNodeName());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getNodeValue()
     */
    public String getNodeValue() throws DOMException {
        return emt.getNodeValue();
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#setNodeValue(java.lang.String)
     */
    public void setNodeValue(String nodeValue) throws DOMException {
        emt.setNodeValue(nodeValue);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getNodeType()
     */
    public short getNodeType() {
        return (emt.getNodeType());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getParentNode()
     */
    public Node getParentNode() {
        return (emt.getParentNode());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getChildNodes()
     */
    public NodeList getChildNodes() {
        return (emt.getChildNodes());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getFirstChild()
     */
    public Node getFirstChild() {
        return (emt.getFirstChild());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getLastChild()
     */
    public Node getLastChild() {
        return (emt.getLastChild());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getPreviousSibling()
     */
    public Node getPreviousSibling() {
        return (emt.getPreviousSibling());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getNextSibling()
     */
    public Node getNextSibling() {
        return (emt.getNextSibling());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getAttributes()
     */
    public NamedNodeMap getAttributes() {
        return (emt.getAttributes());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getOwnerDocument()
     */
    public Document getOwnerDocument() {
        return (emt.getOwnerDocument());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#insertBefore(org.w3c.dom.Node, org.w3c.dom.Node)
     */
    public Node insertBefore(Node newChild, Node refChild) throws DOMException {
        return (emt.insertBefore(newChild, refChild));
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#replaceChild(org.w3c.dom.Node, org.w3c.dom.Node)
     */
    public Node replaceChild(Node newChild, Node oldChild) throws DOMException {
        return (emt.replaceChild(newChild, oldChild));
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#removeChild(org.w3c.dom.Node)
     */
    public Node removeChild(Node oldChild) throws DOMException {
        return (emt.removeChild(oldChild));
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#appendChild(org.w3c.dom.Node)
     */
    public Node appendChild(Node newChild) throws DOMException {
        return (emt.appendChild(newChild));
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#hasChildNodes()
     */
    public boolean hasChildNodes() {
        return (emt.hasChildNodes());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#cloneNode(boolean)
     */
    public Node cloneNode(boolean deep) {
        return (emt.cloneNode(deep));
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#normalize()
     */
    public void normalize() {
        emt.normalize();
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#isSupported(java.lang.String, java.lang.String)
     */
    public boolean isSupported(String feature, String version) {
        return (emt.isSupported(feature, version));
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getNamespaceURI()
     */
    public String getNamespaceURI() {
        return (emt.getNamespaceURI());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getPrefix()
     */
    public String getPrefix() {
        return (emt.getPrefix());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#setPrefix(java.lang.String)
     */
    public void setPrefix(String prefix) throws DOMException {
        emt.setPrefix( prefix);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getLocalName()
     */
    public String getLocalName() {
        return (emt.getLocalName());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#hasAttributes()
     */
    public boolean hasAttributes() {
        return (emt.hasAttributes());
    }

// New Functionality Below This Point
    public Number numberAttribute(String attributeName, Number typeVar) {
        if (attributeName == null || attributeName.length() == 0) {
            return (typeVar);
        }

        String valueString = emt.getAttribute(attributeName);
        if (valueString == null || valueString.length() == 0) {
            return (typeVar);
        }
        try {
            Number i = null;
            if (typeVar instanceof Integer) {
                i = new Integer(valueString);
            } else if (typeVar instanceof Float) {
                i = new Float(valueString);
            } else if (typeVar instanceof Double) {
                i = new Double(valueString);
            }
            return (i);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(null,
                            "NumberFormatException in XML numberAttribute: ["
                                              + attributeName + "]=" + emt.getAttribute(attributeName),
                            "Reader Error", JOptionPane.ERROR_MESSAGE);
/*
            ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                "NumberFormatException in XML numberAttribute: ["
                                              + attributeName + "]=" + emt.getAttribute(attributeName));
*/
            return (typeVar);
        }

    }

    public double numberAttribute (String attributeName, double var) {
        return (numberAttribute(attributeName,new Double(var)).doubleValue());
    }
    public int numberAttribute (String attributeName, int var) {
        return (numberAttribute(attributeName,new Integer(var)).intValue());
    }
    public float numberAttribute (String attributeName, float var) {
        return (numberAttribute(attributeName,new Float(var)).floatValue());
    }
    public int getInt(String attributeName) {
        int value=0;
        return (numberAttribute(attributeName,value));
    }
    public float getFloat(String attributeName) {
        float value=0;
        return (numberAttribute(attributeName,value));
    }
    public double getDouble(String attributeName) {
        double value=0;
        return (numberAttribute(attributeName,value));
    }
    public boolean getBoolean(String attributeName) {
        return Boolean.valueOf(getAttribute(attributeName)).booleanValue();
    }
    public boolean getBoolean(String attributeName, boolean noValueDefault) {
        String valueString = getString(attributeName);
        if (valueString.length()==0) {
            return noValueDefault;
        }
        return Boolean.valueOf(valueString).booleanValue();
    }
    public String getString(String attributeName) {
        return emt.getAttribute(attributeName);
    }
    public Color getColor (String attributeName) {
        return fromStringColor(getString(attributeName));
    }
    /**
     * Constructs a java color with string. <br>
     * The string has four integers for red, green, blue,
     * and alpha value, separated by space.
     */
    public static Color fromStringColor(String str) {
        Color re = null;
        if (str.length()==0) {
            re = Color.orange;
        } else {
            try {
                StringTokenizer stk = new StringTokenizer(str.trim());
                int r = Integer.parseInt(stk.nextToken());
                int g = Integer.parseInt(stk.nextToken());
                int b = Integer.parseInt(stk.nextToken());
                int a = Integer.parseInt(stk.nextToken());
                re = new Color(r, g, b, a);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null,
                             "ElementAttributeReader.fromStringColor Exception: ["+str+"]",
                            "Reader Error", JOptionPane.ERROR_MESSAGE);
/*
                ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                         "ElementAttributeReader.fromStringColor Exception: ["+str+"]");
*/
                re = Color.orange;
            }
        }

        return re;
    }

    //*********************
    //jdk 1.5.ask all methods
    //*************************
    public TypeInfo getSchemaTypeInfo() {
        // TODO Auto-generated method stub
        return null;
    }
    public void setIdAttribute(String arg0, boolean arg1) throws DOMException {
        // TODO Auto-generated method stub

    }
    public void setIdAttributeNS(String arg0, String arg1, boolean arg2) throws DOMException {
        // TODO Auto-generated method stub

    }
    public void setIdAttributeNode(Attr arg0, boolean arg1) throws DOMException {
        // TODO Auto-generated method stub

    }
    public String getBaseURI() {
        // TODO Auto-generated method stub
        return null;
    }
    public short compareDocumentPosition(Node arg0) throws DOMException {
        // TODO Auto-generated method stub
        return 0;
    }
    public String getTextContent() throws DOMException {
        // TODO Auto-generated method stub
        return null;
    }
    public void setTextContent(String arg0) throws DOMException {
        // TODO Auto-generated method stub

    }
    public boolean isSameNode(Node arg0) {
        // TODO Auto-generated method stub
        return false;
    }
    public String lookupPrefix(String arg0) {
        // TODO Auto-generated method stub
        return null;
    }
    public boolean isDefaultNamespace(String arg0) {
        // TODO Auto-generated method stub
        return false;
    }
    public String lookupNamespaceURI(String arg0) {
        // TODO Auto-generated method stub
        return null;
    }
    public boolean isEqualNode(Node arg0) {
        // TODO Auto-generated method stub
        return false;
    }
    public Object getFeature(String arg0, String arg1) {
        // TODO Auto-generated method stub
        return null;
    }
    public Object setUserData(String arg0, Object arg1, UserDataHandler arg2) {
        // TODO Auto-generated method stub
        return null;
    }
    public Object getUserData(String arg0) {
        // TODO Auto-generated method stub
        return null;
    }

}
