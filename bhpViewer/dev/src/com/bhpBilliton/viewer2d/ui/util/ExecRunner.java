/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006
 
The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>
 
All licensees should note the following:
 
 *        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.
 
 *        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.
 
This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
 */

package com.bhpBilliton.viewer2d.ui.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.bhpBilliton.viewer2d.StreamGobbler;
import java.util.logging.Logger;


/**
 * @author lukem
 *
 */
public class ExecRunner {
    Logger logger = Logger.getLogger(ExecRunner.class.toString());
    public static final int STREAM_ERROR = 1;
    public static final int STREAM_STDOUT = 1;
    
    private String[] commandSet;
    private String title = "processRunner:";
    private int streamCapture;
    private String outputType = "Error";
    private InputStream stderr = null;
    private InputStream stdout = null;
    private StringBuffer output = new StringBuffer();
    
    private StreamGobbler eater;
    private Process process;
    
    private ExecRunner() {
        streamCapture = STREAM_STDOUT;
    }
    
    public ExecRunner(String command) {
        this();
        commandSet = command.split("[\\t\\r\\n ]+");
    }
    
    public ExecRunner(String command, int stream) {
        this(command);
        streamCapture = stream;
    }
    
    public ExecRunner(String[] commandSet, int stream) {
        this();
        streamCapture = stream;
        this.commandSet = commandSet;
    }
    
    public ExecRunner(String command, int stream, String title, String outputType) {
        this(command, stream);
        this.title = title;
        this.outputType = outputType;
    }
    
    public void run() {
        try {
            process = Runtime.getRuntime().exec(commandSet);
            if (streamCapture == STREAM_STDOUT) {
                stderr = process.getErrorStream();
                stdout = process.getInputStream();
            } else {
                stdout = process.getErrorStream();
                stderr = process.getInputStream();
            }
            eater = new StreamGobbler(stderr, outputType, title);
            eater.start();
            
            capture(stdout);
        } catch (IOException e) {
            System.out.println("ExecRunner.run Exception :" + title + commandSet);
            System.out.println("    " + e.toString());
        } finally {
            try {
                process.destroy();
            } catch (Exception e) {
                logger.warning("ExecRunner consumed Exception: " + e.getMessage());
            }
            if (eater != null) {
                eater.interrupt();
                eater=null; // good night, sweet thread
            }
            FileTools.finalizeStream(stderr);
            FileTools.finalizeStream(stdout);
        }
    }
    
    private void capture(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        try {
            String line;
            while((line = reader.readLine()) != null) {
                output.append(line + "\n");
                System.out.println(line);
            }
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public String getOutput() {
        return output.toString();
    }
    
    public InputStream getOutputStream() {
        return new ByteArrayInputStream(output.toString().getBytes());
    }
}