/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import com.bhpBilliton.viewer2d.Bhp2DviewerConstants;
import com.bhpBilliton.viewer2d.BhpViewerBase;

import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

/**
 * Created on 20/08/2004
 * @author lukem
 *
 */
public class FileTools {
    public static final String BHPSU_HDR = "_0000.HDR";
    public static final String BHPSU_METAHDR = "_0000.0001.HDR";
    private static Logger logger = Logger.getLogger(FileTools.class.getName());
    static String URLFile = "";
    static String filesep = "";

    /** Set when instance of the GUI created. Only needs to be set once */
    static public void setFilesep(String fileSeparator) {
        filesep = fileSeparator;
    }

    //Note: only call is in the BhpPropertyManager
    //TODO: Deprecate once get rid of openConfig servlet usages
    /**
     * Made this method deprecated per the source comment TODO
     * @deprecated
     */
    public static void setURL(String baseURL) {
        //FIXME: FRAGILE! THIS IS GOING TO BREAK IN THE REFACTORED VERSION UNLESS IT IS FIXED FIRST
        //URLDirectory = baseURL + "servlet/readDirectory";
        URLFile = baseURL + "servlet/openConfig";
    }

    private static boolean remoteFiles = false;

    public static void setRemoteFiles (boolean remoteFilesValue) {
        remoteFiles = remoteFilesValue;
    }

    public static final class BhpHeaderFileNameFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            return name.endsWith(BHPSU_HDR) ? true : false;
        }
    }

    public static final class ContainsFileNameFilter implements FilenameFilter {
        private final String fileName;

        public ContainsFileNameFilter (String fileName) {
            this.fileName = fileName;
        }

        public boolean accept(File dir, String name) {
            int lastSlash = name.lastIndexOf(filesep);
            if (lastSlash>=0) {
                return name.substring(lastSlash).equals(fileName);
            } else {
                return name.equals(fileName);
            }
        }
    }

    public static final class ExactFileNameFilter implements FilenameFilter {
        private final String fileName;
        public ExactFileNameFilter (String fileName) {
            this.fileName = fileName;
        }
        public boolean accept(File dir, String name) {
            return fileName.equals(name) ? true : false;
        }
    }

    public static void removeSuffix(String[] filenames, String suffix) {
        for (int i = 0; i < filenames.length; i++) {
            filenames[i]=removeSuffix(filenames[i], suffix);
        }
        Arrays.sort(filenames);
    }
    
    /**
     * @param string
     * @param suffix
     */
    public static String removeSuffix(String string, String suffix) {
        if (string.endsWith(suffix)) {
            String newString = string.substring(0, string.indexOf(suffix));
            return (newString);
        }
        return string;
    }

    public static boolean isUsefulDirectory(String path) {
        if (path.length() ==0) {
            return false;
        } else {
            return isUsefulDirectory(new File(path));
        }
    }

    public static String getPathFromFilePath(String filePath) {
        String rPath = filePath;
        
        rPath = rPath.substring(0,rPath.lastIndexOf(filesep));
        return rPath;
    }

    public static String getStandardPath(String path) {
        String rPath = path;

        if (rPath.length() != 0 && !rPath.endsWith(filesep)) {
            rPath = rPath + filesep;
        }
        return rPath;
    }

    public static boolean isUsefulDirectory(File path) {
        boolean rval = false;

        if (!path.exists()) {
            System.out.println ("Path does not exist: " + path.getPath());
        } else if (!path.isDirectory()) {
            System.out.println ("Path is not a directory: " + path.getPath());
        } else if (!path.canRead()) {
            System.out.println ("Path is not readable: " + path.getPath());
        } else {
            rval = true;
        }
        return rval;
    }

    private static boolean pathContainsFilePatternLocal(File path, String filePattern) {
        boolean rval = false;

        if (isUsefulDirectory(path)) {
            FilenameFilter nameFilter = new ExactFileNameFilter(filePattern);
            if (path.listFiles(nameFilter).length > 0) {
                rval = true;
            }
        }
        return rval;
    }

    public static String[] getDirectoryContents(String pathString, BhpViewerBase viewer) {
        String[] contents = new String[0];

        if (remoteFiles) {
            List remoteContents = getRemoteDirectoryContents(pathString, viewer);
            contents = (String[]) remoteContents.toArray(new String[0]);
        } else {
            File nf = new File (pathString);
            contents = nf.list();
        }
        return contents;
    }

    public static List getDirectoryContents(String pathString, FilenameFilter f, BhpViewerBase viewer) {
        List contents = new ArrayList();

        if (remoteFiles) {
            List acontents = getRemoteDirectoryContents(pathString, f, viewer);
            for (Iterator i = acontents.iterator();i.hasNext();) {
                String j = (String) i.next();
                j = j.substring(1);
                contents.add(j);
            }
        } else {
            File nf = new File (pathString);
            String[] acontents = nf.list(f);
            contents = Arrays.asList(acontents);
        }
        return contents;
    }

    /**
     * Get the contents of a directory: files and directories.
     *
     * @param pathString Pathname of the directory
     * @returns List of sorted directories followed by sorted files.
     * Directories have a "D" prepended to the name, files a "F".
     */
    private static List getRemoteDirectoryContents(String pathString, BhpViewerBase viewer) {
        List results = new ArrayList();

        IMessagingManager messagingMgr = viewer.getAgent().getMessagingManager();
        try {

            boolean isLocal = messagingMgr.getLocationPref().equals(QIWConstants.LOCAL_SERVICE_PREF);
            IQiWorkbenchMsg request = null;
            IQiWorkbenchMsg response = null;

            if (isLocal) {
                ArrayList<String> list = new ArrayList<String>();
                list.add(QIWConstants.LOCAL_SERVICE_PREF);
                list.add(pathString);
                String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_DIR_FILE_LIST_CMD,QIWConstants.ARRAYLIST_TYPE,list);
                response = messagingMgr.getMatchingResponseWait(msgId, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
            } else {
                request = new QiWorkbenchMsg(CompDescUtils.getDescCID(messagingMgr.getMyComponentDesc()),
                    CompDescUtils.getDescCID(messagingMgr.getComponentDescFromDisplayName(QIWConstants.SERVLET_DISPATCHER_NAME)),
                    QIWConstants.CMD_MSG,
                    QIWConstants.GET_REMOTE_DIR_FILE_LIST_CMD,
                    MsgUtils.genMsgID(),
                    QIWConstants.STRING_TYPE, pathString);

                response = messagingMgr.sendRemoteRequest(request);
            }

            if(response == null){
                logger.severe("SYSTEM ERROR: Timed out waiting for response to GET_REMOTE_DIR_FILE_LIST_CMD ");
                return results;
            }

            if (!MsgUtils.isResponseAbnormal(response)) {
                ArrayList<ArrayList> list = (ArrayList<ArrayList>)MsgUtils.getMsgContent(response);
                ArrayList<String> dirList = list.get(0);  //directory list
                ArrayList<String> fileList = list.get(1);  //file list

                for (String s : dirList) {
                    results.add("D"+s);
                }

                for (String s : fileList) {
                    results.add("F"+s);
                }
                ((ArrayList)results).trimToSize();
            }
        }
        catch (Exception ex) {
            System.out.println("getRemoteDirectoryContents Exception " + ex.toString());
            ex.printStackTrace();
        }
        return results;
    }

    public static List getRemoteDirectoryContents(String pathString, FilenameFilter fileFilter, BhpViewerBase viewer) {
        List checkList = getRemoteDirectoryContents(pathString, viewer);
        List resultList = new ArrayList();

        for (Iterator i = checkList.iterator();i.hasNext();) {
            String fileString = (String) i.next();
            if (fileFilter.accept(null,fileString.substring(1))) {
                resultList.add(fileString);
            }
        }

        return resultList;
    }

    private static boolean pathContainsFilePatternRemote(String pathString, String filePattern, BhpViewerBase viewer) {
        boolean rval = false;

        FilenameFilter nameFilter = new ContainsFileNameFilter(filePattern);
        List resultList = getRemoteDirectoryContents(pathString, nameFilter, viewer);

        if (resultList.size() > 0) {
            rval = true;
        }

        return rval;
    }

    public static boolean pathContainsFilePattern(String path, String filePattern, BhpViewerBase viewer) {
        if (remoteFiles) {
            return pathContainsFilePatternRemote(path, filePattern, viewer);
        } else {
            return pathContainsFilePatternLocal(new File(path), filePattern);
        }
    }

    public static List removeSuffix(List filenames, String suffix) {
        String name;
        List newNames = new ArrayList();
        for (Iterator iter = filenames.iterator(); iter.hasNext();) {
            name = (String) iter.next();
            name = removeSuffix(name, suffix);
            newNames.add(name);
        }
        return (newNames);
    }

    public static String removeExtension(String fileName) {
        if (fileName.lastIndexOf('.') > -1) {
            return fileName.substring(0, fileName.lastIndexOf('.'));
        }
        return fileName;
    }

    public static String getFileNameFromPath(String fullFilePath) {

        int lastSlash = fullFilePath.lastIndexOf(filesep);
        String fileOnly = fullFilePath;

        if (lastSlash != -1)
            fileOnly = fileOnly.substring(lastSlash+1);
        return (fileOnly);
    }
    
    public static void finalizeStream(RandomAccessFile stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (Exception e) {
                //intentionally do nothing
            }
        }
    }
    
    public static void finalizeStream(InputStream stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (Exception e) {
                //intentionally do nothing
            }
        }
    }
    
    public static void finalizeStream(Reader stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (Exception e) {
                //intentionally do nothing
            }
        }
    }
    
    public static void finalizeStream(Writer stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (Exception e) {
                //intentionally do nothing
            }
        }
    }

    //TODO: Find uses and replace with IO service. Then deprecate this.
    /**
     * Deprecated per the source comment TODO.
     * @deprecated
     */
    public static Reader getRemoteFileReader(String filename) {
        Reader streamReader = null;
        try {
            String urlString = URLFile;
            URL url = new URL(urlString + "?FileName=" + URLEncoder.encode(filename, "iso-8859-1"));
            URLConnection conn = url.openConnection();

            System.out.println("slurpFileRemote [" + filename + "] contentLength " + conn.getContentLength() + " : " + conn.getInputStream().available());

            streamReader = new InputStreamReader(conn.getInputStream());
        }
        catch (Exception e) {
            System.out.println("OpenSessionTemplateAction.openRemote Exception " + e.toString());
            e.printStackTrace();
        }
        return (streamReader);
    }

    public static String[] slurpFile(String filename) {
        BufferedReader reader = null;
        Reader strmReader = null;
        StringBuffer contents = new StringBuffer();
        try {
            if (remoteFiles) {
                strmReader = getRemoteFileReader (filename);
            } else {
                strmReader = new FileReader(filename);
            }
            reader = new BufferedReader(strmReader);
            String oneLine;
            while ((oneLine = reader.readLine())!=null) {
                contents.append(oneLine + '\n');
            }
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        } finally {
            FileTools.finalizeStream(strmReader);
            FileTools.finalizeStream(reader);
        }
        String contentString = contents.toString();
        String[] split;
        if (contentString.length() > 0) {
            split = contentString.split("[\\r\\n]+");
        } else {
            split = new String[0];
        }
        return (split);
    }
}