/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import java.util.*;
import javax.swing.*;

import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpSeismicTableModel;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.data.GeneralDataSource;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This panel is for seismic data.
 *               It has no extra GUI component. <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class OpenSeismicPanel extends OpenGeneralPanel {
    private BhpLayer _refLayer;
    private BhpViewerBase viewer;
    /**
     * Constructs a new OpenSeismicPanel.
     * @param refLayer reference layer.
     */
    public OpenSeismicPanel(BhpViewerBase viewer, BhpLayer refLayer) {
        super(refLayer);
        this.viewer = viewer;
        this._refLayer = refLayer;
        setupGUI(false);
    }

    public void initWithLayer(BhpLayer alayer) {
        if (alayer.getDataType() != BhpViewer.BHP_DATA_TYPE_SEISMIC) return;

        _orderEditor.removeAllItems();
        _tableModel.cleanupTableModel();
        BhpSeismicTableModel model = alayer.getParameter();
        Vector data = model.getDataVector();
        for (int i=0; i<data.size(); i++) {
            _tableModel.addRow(new Vector((Vector) data.elementAt(i)));
            _orderEditor.addItem("" + (i+1));
        }
        if (!(alayer.getDataSource().equals(GeneralDataSource.DATA_SOURCE_BHPSU)))
            _tableModel.setColumnCount(7);
        _table.getColumnModel().getColumn(3).setCellEditor(new DefaultCellEditor(_orderEditor));

        _verticalPanel.initVertical(_tableModel, alayer.getDataSource());
        _missingDataPanel.initMissingData(model.getMissingDataSelection());
    }

    public int applyToLayer(BhpLayer alayer) {
        if (alayer.getDataType() != BhpViewer.BHP_DATA_TYPE_SEISMIC) return 0;
        
        _missingDataPanel.applyMissingData(_tableModel);
        _verticalPanel.applyVertical(_tableModel);
        
        int changeFlag = 0;

        BhpSeismicTableModel model = alayer.getParameter();
        
        model.clearRowsChanged();
        if (model.getRowCount() != _tableModel.getRowCount() ||
            model.getColumnCount() != _tableModel.getColumnCount() ||
            model.getMissingDataSelection() != _tableModel.getMissingDataSelection())
            changeFlag = changeFlag | BhpViewer.DATASELECTION_FLAG;

        if (changeFlag != 0) {
            Vector colNames = new Vector();
            for (int i=0; i<_tableModel.getColumnCount(); i++)
                colNames.addElement(_tableModel.getColumnName(i));
            model.setDataVector(_tableModel.getDataVector(), colNames);
            model.setMissingDataSelection(_tableModel.getMissingDataSelection());
            return changeFlag;
        }

        Object original, current;
        for (int i=0; i<_tableModel.getRowCount(); i++) {
            for (int j=0; j<_tableModel.getColumnCount(); j++) {
                original = model.getValueAt(i, j);
                current = _tableModel.getValueAt(i, j);
                if (original.equals(current) == false) {
                    if (j == 2) {
                        boolean chosenRangeChanged = model.setChosenRangeAt(current, i);
                        if (!chosenRangeChanged) continue;
                    }
                    else {
                        model.setValueAt(current, i, j);
                    }
                    if (j==2 || j==4 || j==7 || j==8) model.setRowsChanged(i);
                    if (j!=1 && j!=6) changeFlag = changeFlag | BhpViewer.DATASELECTION_FLAG;
                }
            }
        }
        
        return changeFlag;
    }

    public void describeData(GeneralDataSource dataSource) {
        _tableModel.cleanupTableModel();
        _tableModel.setDataSource(dataSource);

        String[] names = dataSource.getHeaders();
        for (int i=0; i<names.length; i++) {
            _tableModel.addRowData(names[i], dataSource.getHeaderSetting(names[i]));
        }
        _tableModel.setDataAttributeString(dataSource.getDataAtt());

        if (_refLayer != null) {
            _tableModel.initWithAnother(_refLayer.getParameter());
        }

        _orderEditor.removeAllItems();
        for (int i=0; i<_tableModel.getRowCount(); i++) {
            _orderEditor.addItem("" + (i+1));
        }
        _table.getColumnModel().getColumn(3).setCellEditor(new DefaultCellEditor(_orderEditor));
        if (!(dataSource.getDataSourceName().equals(GeneralDataSource.DATA_SOURCE_BHPSU)))
            _tableModel.setColumnCount(7);

        _verticalPanel.initVertical(_tableModel, dataSource.getDataSourceName());
    }

    protected void buildPanel() {
        JPanel tablePanel = new JPanel(new BorderLayout());
        _tableModel = new BhpSeismicTableModel(viewer, true);
        //_table = new JTable(_tableModel);
        _table.setModel(_tableModel);
        tablePanel.add(new JScrollPane(_table), BorderLayout.CENTER);

        Box box = new Box(BoxLayout.Y_AXIS);
        box.add(tablePanel);
        box.add(_missingDataPanel);
        box.add(_verticalPanel);
        this.add(box, BorderLayout.CENTER);
    }
}
