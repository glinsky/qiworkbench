/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.ui.util.FileTools;
import com.bhpBilliton.viewer2d.ui.util.FileTools.BhpHeaderFileNameFilter;



public class DataLayerHandle {
	private String name = null;
	private String dataname = null;
	private String pathlist = null;
	private BhpViewerBase viewer =null;
	private List dataSets;		// a list of the datasets in the .dat file

	public DataLayerHandle (BhpViewerBase viewer) {
		name = "";
		dataname = "";
		pathlist = "";
		dataSets = new ArrayList();
		this.viewer = viewer;
	}
	public DataLayerHandle (String sName, String sDataname, String sPathlist,BhpViewerBase viewer) {
		//this();
		name = sName;
		dataname = sDataname;
		pathlist = sPathlist;
		dataSets = new ArrayList();
		this.viewer = viewer;
	}

	public String getFull () {
		return (name + " (" + dataname + ") [" + pathlist + "]");
	}
	public String getShortened () {
		return (dataname + " [" + getShortPathlist() + "]");
	}

	public String getDataname() {
		return dataname;
	}
	public String getName() {
		return name;
	}
	public String getPathlist() {
		return pathlist;
	}
	public void setDataname(String dataname) {
		this.dataname = dataname;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPathlist(String pathlist) {
		this.pathlist = pathlist;
	}
        
        public String getShortPathlist () {
		if (pathlist.lastIndexOf('/') != -1)
			return (pathlist.substring(pathlist.lastIndexOf('/')+1));

		return (pathlist);
        }
        
	public String getIdentifier() {
		String identifier = getDataname() + getPathlist().replaceAll("[/\\\\ ]","\\$");
		return identifier;
	}
        
	public String getHeaderFilePath() {
		String pathListName = getPathlist();
		String headerFileName = getDataname() + FileTools.BHPSU_HDR;

		String[] pathList = FileTools.slurpFile(pathListName);

		String rval="";

		for (int i=0;i<pathList.length;i++) {
			//look in path and see if header file exists there
			if (FileTools.pathContainsFilePattern(pathList[i],headerFileName,viewer)) {
                            //this is the place
                            //File path = new File (pathList[i]);
                            //rval = path.getAbsolutePath();
                            rval = pathList[i];
                            if (!rval.endsWith("/")) {
                                rval += "/";
                            }
			}
		}

		return rval;
	}

        public String[] getDatasetsFromDat(String filename, BhpViewerBase viewer) {
            List arFiles = new ArrayList();
            
            String[] contents = FileTools.slurpFile(filename);
            for (int i=0;i<contents.length;i++) {
                File dataPath = new File(contents[i]);
                dataSets.add(contents[i].trim());
                BhpHeaderFileNameFilter bhpHeaderFileNameFilter = new BhpHeaderFileNameFilter();
                List asList = FileTools.getDirectoryContents(contents[i],bhpHeaderFileNameFilter, viewer);
                arFiles.addAll(asList );
            }
            if (arFiles.size() > 0) {
                arFiles=FileTools.removeSuffix(arFiles, FileTools.BHPSU_HDR);
            }
            return (String[]) (arFiles.toArray(new String[0]));
        }

        public Map extractHeaderKeys() {
            return (extractHeaderKeys(getHeaderFilePath() + getDataname() + FileTools.BHPSU_METAHDR));
        }
        
        public Map extractHeaderKeys(String filename) {
            HashMap headerKeys = new HashMap();
            String[] contents = FileTools.slurpFile(filename);
            
            HeaderKey thisKey = new HeaderKey();
            for (int i = 0; i < contents.length; i++) {
                String[] keyLine = contents[i].split("\\s*=\\s*", 2);
                if (keyLine[0].equalsIgnoreCase("HDRNAME")) {
                    //start a new key
                    if (thisKey.isValid()) {
                        headerKeys.put(thisKey.getName(), thisKey);
                    }
                    thisKey = new HeaderKey();
                }
                if (thisKey != null) {
                    thisKey.setValue(keyLine[0].trim(), keyLine[1].trim());
                }
            }
            
            if (thisKey.isValid()) {
                headerKeys.put(thisKey.getName(), thisKey);
            }
            
            return (headerKeys);
        }
}