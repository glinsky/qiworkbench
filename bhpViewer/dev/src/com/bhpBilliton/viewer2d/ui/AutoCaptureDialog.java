/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;

import com.bhpBilliton.viewer2d.util.ErrorDialog;

public class AutoCaptureDialog extends JDialog {
    private final BhpMovieDialog movieDialog;
    private JTextField __maxStepField;

    public AutoCaptureDialog(BhpMovieDialog dialog, Frame parent) {
        super(parent, "Automatic Capture", true);
        this.movieDialog = dialog;
        __maxStepField = new JTextField();

        JPanel panel1 = new JPanel(new FlowLayout());
        JButton okButton = new JButton("Ok");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                applyAutoSettings();
                setVisible(false);
            }
        });
        JButton clButton = new JButton("Cancel");
        clButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });
        panel1.add(okButton);
        panel1.add(clButton);

        JPanel panel2 = new JPanel(new GridLayout(3, 1));
        panel2.setBorder(new EtchedBorder());
        panel2.add(new JLabel("Maximum Step Number:"));
        panel2.add(__maxStepField);
        JButton autoButton = new JButton("Start Now");
        autoButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                applyAutoSettings();
                setVisible(false);
                movieDialog.startAutomaticCapture();
            }
        });
        JPanel autoButtonPanel = new JPanel(new FlowLayout());
        autoButtonPanel.add(autoButton);
        panel2.add(autoButtonPanel);

        getContentPane().add(panel2, BorderLayout.CENTER);
        getContentPane().add(panel1, BorderLayout.SOUTH);
        setSize(300, 200);
    }
    public void initAutoSettings() {
        __maxStepField.setText("" + movieDialog.getMaxStep());
    }
    public void applyAutoSettings() {
        try {
            int newStep = Integer.parseInt(__maxStepField.getText());
            movieDialog.setMaxStep(newStep);
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(null,
                            "BhpMovieDialog.invalid  max step " + __maxStepField.getText(),
                            "Access Error", JOptionPane.ERROR_MESSAGE);
/*
            ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                         "BhpMovieDialog.invalid  max step " + __maxStepField.getText());
*/
        }
    }
}
