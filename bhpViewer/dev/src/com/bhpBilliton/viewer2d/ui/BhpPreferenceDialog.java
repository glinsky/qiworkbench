/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.util.Hashtable;


import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.BhpWindow;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpBilliton.viewer2d.util.NoImageLineColorEditor;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.gw2d.gui.*;


/**
 * Title: BHP Viewer <br>
 * <br>
 * Description: This dialog is for viewing and changing the global preferences
 * of the viewer. Currently, it has two tabs for highlight settings and
 * tracking-cursor settings. Notice that these settings are global, affects all
 * the internal frames. <br>
 * <br>
 * Copyright: Copyright (c) 2001 <br>
 * Company: BHP INT <br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpPreferenceDialog extends JDialog {

    private static final float LINEWIDTH_LARGE = 4.0f;

    private static final float LINEWIDTH_DEFAULT = 1.0f;

    private static final int CROSSWIDTH_HUGE = 10000;

    private static final int CROSSWIDTH_NONE = 0;

    private static final int CROSSWIDTH_SMALL = 10;

    private static final int CROSSWIDTH_LARGE = 20;

    private LineStyleChooser lineStyleEditor;

    private NoImageLineColorEditor lineColourEditor;

    private LineWidthChooser lineWidthEditor;

    private JSlider dialogSlider;

    private JRadioButton _crossHairRadio;

    private JRadioButton _smallCrossRadio;

    private JRadioButton _largeCrossRadio;

    private JRadioButton _noCursorRadio;

    private JRadioButton _showPointerRadio;

    private NoImageLineColorEditor cursorColourEditor;

    //private JTabbedPane _tab;
    private JPanel highlightPrefPanel;

    private JPanel cursorPrefPanel;

    private BhpViewerBase _viewer;

    /**
     * Constructs a new dialog and sets up the GUI.
     */
    public BhpPreferenceDialog(BhpViewerBase viewer, String type) {
      // BhpViewerBase no longer a frame
      super();
      setTitle("Preferences");
      setLocation(viewer.getMyLocation().x + 10,viewer.getMyLocation().y + 10);
      //super(viewer, type + " Preferences", false);
      _viewer = viewer;
      //        highlightPrefPanel = null;
      //        cursorPrefPanel = null;
      buildPreferenceDialogGUI(type);
    }

    /**
     * Gets related information of graphic settings and fill the fields of GUI.
     * <br>
     */
    public void setPreferences() {
        if (highlightPrefPanel != null) {
            copyHighlightPrefs(_viewer.getHighlightAttribute());
        }
        if (cursorPrefPanel != null) {
            copyCursorPrefs(_viewer.getCursorAttribute());
        }
    }

    /**
     *
     */
    private void copyCursorPrefs(RenderingAttribute cursorAttribute) {
        double cursorWidth = _viewer.getCursorWidth();
        if (cursorWidth > 30) {
            _crossHairRadio.setSelected(true);
        } else if (cursorWidth > 10) {
            _largeCrossRadio.setSelected(true);
        } else if (cursorWidth > 1) {
            _smallCrossRadio.setSelected(true);
        } else {
            _noCursorRadio.setSelected(true);
        }
        _showPointerRadio.setSelected(_viewer.getShowPointer());
        //double cursorHeight = _viewer.getCursorHeight();
        //_symbolWidth.setText("" + cursorWidth);
        //_symbolHeight.setText("" + cursorHeight);
        //_slEditor.setLineStyle(cursorAttribute.getLineStyle());
        //_swEditor.setLineWidth(cursorAttribute.getLineWidth());
        cursorColourEditor.setLineColor(cursorAttribute.getLineColor());
    }

    /**
     * @param highlightAttribute
     *            TODO
     *
     */
    private void copyHighlightPrefs(RenderingAttribute highlightAttribute) {
        lineStyleEditor.setLineStyle(highlightAttribute.getLineStyle());
        lineWidthEditor.setLineWidth((int)highlightAttribute.getLineWidth());
        lineColourEditor.setLineColor(highlightAttribute.getLineColor());
        dialogSlider.setValue((int) (highlightAttribute.getLineColor().getAlpha() / 2.55));
    }

    /**
     * Retrieves valus from the GUI components and updates the graphic
     * attriubte.
     */
    public void apply() {
        boolean changedHighlight = false;
        boolean changedCursor = false;
        if (highlightPrefPanel != null) {
            changedHighlight = applyHighlightPrefs(_viewer.getHighlightAttribute());

            if (changedHighlight) {
                applyHighlightToViewerWindows(_viewer.getDesktop().getAllFrames());
            }
        }
        if (cursorPrefPanel != null) {
            RenderingAttribute cursorAttribute = _viewer.getCursorAttribute();
            double cursorWidth = _viewer.getCursorWidth();
            double newWidth = cursorWidth;
            float newLineWidth = LINEWIDTH_DEFAULT;

            if (_crossHairRadio.isSelected()) {
                newWidth = CROSSWIDTH_HUGE;
            } else if (_largeCrossRadio.isSelected()) {
                newWidth = CROSSWIDTH_LARGE;
                newLineWidth = LINEWIDTH_LARGE;
            } else if (_smallCrossRadio.isSelected()) {
                newWidth = CROSSWIDTH_SMALL;
            } else {
                newWidth = CROSSWIDTH_NONE;
            }

            boolean showPointer = _viewer.getShowPointer();
            changedCursor = applyCursorPrefsToViewer(cursorAttribute,
                    cursorWidth, newWidth, newLineWidth);
            if (showPointer != _showPointerRadio.isSelected()) {
                _viewer.setShowPointer(_showPointerRadio.isSelected());
                changedCursor = true;
            }
            if (changedCursor) {
                applyCursorPrefsToViewerWindows(_viewer.getDesktop()
                        .getAllFrames(), newWidth);
            }
        }
    }

    /**
     * @param changedCursor
     * @param cursorAttribute
     * @param cursorWidth
     * @param newWidth
     * @return
     */
    private boolean applyCursorPrefsToViewer(
            RenderingAttribute cursorAttribute, double cursorWidth,
            double newWidth, float newLineWidth) {
        boolean changedCursor = false;
        float lineWidth = cursorAttribute.getLineWidth();
        if (newWidth != cursorWidth) {
            _viewer.setCursorSize(newWidth, newWidth);
            changedCursor = true;
        }
        if (newLineWidth != lineWidth) {
            cursorAttribute.setLineWidth(newLineWidth);
            changedCursor = true;
        }
        if (!(cursorAttribute.getLineColor().equals(cursorColourEditor
                .getLineColor()))) {
            cursorAttribute.setLineColor(cursorColourEditor.getLineColor());
            changedCursor = true;
        }
        return changedCursor;
    }

    /**
     * @param frms
     * @param newWidth
     */
    private void applyCursorPrefsToViewerWindows(JInternalFrame[] frms,
            double newWidth) {
        BhpWindow win;
        for (int i = 0; i < frms.length; i++) {
            win = (BhpWindow) frms[i];
            win.getBhpPlot().updateCursorSetting(newWidth, newWidth,
                    _viewer.getShowPointer());
        }
    }

    /**
     * @param frms
     */
    private void applyHighlightToViewerWindows(JInternalFrame[] frms) {
        BhpWindow win;
        for (int i = 0; i < frms.length; i++) {
            win = (BhpWindow) frms[i];
            win.getBhpPlot().getMouseCenter().updateEventHighlight();
        }
    }

    /**
     * @param highlightAttribute
     * @return
     */
    private boolean applyHighlightPrefs(RenderingAttribute highlightAttribute) {
        boolean changedHighlight = false;
        if (highlightAttribute.getLineStyle() != lineStyleEditor.getLineStyle()) {
            highlightAttribute.setLineStyle(lineStyleEditor.getLineStyle());
            changedHighlight = true;
        }
        if (highlightAttribute.getLineWidth() != lineWidthEditor.getLineWidth()) {
            highlightAttribute.setLineWidth(lineWidthEditor.getLineWidth());
            changedHighlight = true;
        }
        int oldOpacity = (int) (highlightAttribute.getLineColor().getAlpha() / 2.55);
        if (!(highlightAttribute.getLineColor().equals(lineColourEditor
                .getLineColor()))
                || oldOpacity != dialogSlider.getValue()) {
            Color nc = lineColourEditor.getLineColor();
            int nalpha = (int) (dialogSlider.getValue() * 2.55);
            Color newColor = new Color(nc.getRed(), nc.getGreen(),
                    nc.getBlue(), nalpha);
            highlightAttribute.setLineColor(newColor);
            changedHighlight = true;
        }
        return changedHighlight;
    }

    private void buildPreferenceDialogGUI(String type) {
        this.getContentPane().add(buildContentPanel(type), BorderLayout.CENTER);
        this.getContentPane().add(buildControlPanel(), BorderLayout.SOUTH);
    }

    private JPanel buildContentPanel(String type) {
        JPanel repanel = null;
        if (type.equals("Horizon Highlight")) {
            lineStyleEditor = new LineStyleChooser();
            lineColourEditor = new NoImageLineColorEditor();
            lineWidthEditor = new LineWidthChooser();

            dialogSlider = new JSlider(0, 100, 100);
            Hashtable sliderLabel = new Hashtable();
            sliderLabel.put(new Integer(0), new JLabel("clear"));
            sliderLabel.put(new Integer(100), new JLabel("     opaque"));
            sliderLabel.put(new Integer(20), new JLabel("0.2"));
            sliderLabel.put(new Integer(40), new JLabel("0.4"));
            sliderLabel.put(new Integer(60), new JLabel("0.6"));
            sliderLabel.put(new Integer(80), new JLabel("0.8"));
            dialogSlider.setMajorTickSpacing(20);
            dialogSlider.setMinorTickSpacing(10);
            dialogSlider.setLabelTable(sliderLabel);
            dialogSlider.setPaintLabels(true);
            dialogSlider.setPaintTicks(true);

            highlightPrefPanel = new JPanel(new GridLayout(4, 2));
            highlightPrefPanel.add(new JLabel("    Line Style"));
            highlightPrefPanel.add(lineStyleEditor);
            highlightPrefPanel.add(new JLabel("    Line Width"));
            highlightPrefPanel.add(lineWidthEditor);
            highlightPrefPanel.add(new JLabel("    Line Color"));
            highlightPrefPanel.add(lineColourEditor.getGUIAsButton());
            highlightPrefPanel.add(new JLabel("    Color Opacity"));
            highlightPrefPanel.add(dialogSlider);
            repanel = highlightPrefPanel;
        } else {
            _crossHairRadio = new JRadioButton("Cross Hair", true);
            _largeCrossRadio = new JRadioButton("Large Cross", false);
            _smallCrossRadio = new JRadioButton("Small Cross", false);
            _noCursorRadio = new JRadioButton("No Cursor", false);
            _showPointerRadio = new JRadioButton("Show Pointer", true);
            cursorColourEditor = new NoImageLineColorEditor();

            ButtonGroup bgroup = new ButtonGroup();
            bgroup.add(_crossHairRadio);
            bgroup.add(_largeCrossRadio);
            bgroup.add(_smallCrossRadio);
            bgroup.add(_noCursorRadio);

            _noCursorRadio.addItemListener(new CursorStyleListener());

            JPanel panel1 = new JPanel();
            panel1.setLayout(new BoxLayout(panel1, BoxLayout.X_AXIS));
            panel1.setBorder(new TitledBorder("Cursor Style"));
            Box p11 = new Box(BoxLayout.Y_AXIS);
            p11.add(_crossHairRadio);
            p11.add(_largeCrossRadio);
            p11.add(_smallCrossRadio);
            p11.add(_noCursorRadio);
            p11.add(Box.createVerticalStrut(10));
            p11.add(_showPointerRadio);
            panel1.add(Box.createHorizontalStrut(20));
            panel1.add(p11);

            JPanel panel2 = new JPanel();
            //            BoxLayout boxer = new BoxLayout(panel2, BoxLayout.X_AXIS);
            panel2.setBorder(new TitledBorder("Cursor Color"));
            panel2.add(Box.createHorizontalStrut(30));
            JComponent scComponent = cursorColourEditor.getGUIAsButton();
            scComponent.setPreferredSize(new Dimension(160, 20));
            panel2.add(scComponent);

            Box box = new Box(BoxLayout.Y_AXIS);
            box.add(Box.createVerticalStrut(10));
            box.add(panel1);
            box.add(Box.createVerticalStrut(10));
            box.add(panel2);
            box.add(Box.createVerticalGlue());
            cursorPrefPanel = new JPanel();
            cursorPrefPanel.add(box);

            repanel = cursorPrefPanel;
        }
        //_tab = new JTabbedPane();
        //_tab.add("Horizon Highlight", linePanel);
        //_tab.add("Tracking-cursor", symbolPanel);
        //JPanel panel = new JPanel(new BorderLayout());
        //panel.add(_tab, BorderLayout.CENTER);
        return repanel;
    }

    private JPanel buildControlPanel() {
        JButton okb = new JButton("Ok");
        okb.addActionListener(new OkListener());
        JButton apb = new JButton("Apply");
        apb.addActionListener(new ApplyListener());
        JButton clb = new JButton("Cancel");
        clb.addActionListener(new CancelListener());
        JButton helpb = new JButton("Help");
        helpb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                showHelp();
            }
        });
        JPanel panel = new JPanel(new FlowLayout());
        panel.add(okb);
        panel.add(apb);
        panel.add(clb);
        panel.add(helpb);
        return panel;
    }

    private void showHelp() {
        try {
            if (highlightPrefPanel != null) {
                BhpViewerBase.broker
                        .setCurrentID("preferences_horizon_highlight");
            } else {
                BhpViewerBase.broker
                        .setCurrentID("preferences_tracking_cursor");
            }
            BhpViewerBase.broker.setDisplayed(true);
        } catch (Exception e) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "Couldn't show preference help");
        }
    }

    private void hideDialog() {
        this.setVisible(false);
    }

    private class CursorStyleListener implements ItemListener {
        public void itemStateChanged(ItemEvent e) {
            if (_noCursorRadio.isSelected()) {
                _showPointerRadio.setSelected(true);
                _showPointerRadio.setEnabled(false);
            } else {
                _showPointerRadio.setEnabled(true);
            }
        }
    }

    private class OkListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            apply();
            hideDialog();
        }
    }

    private class ApplyListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            apply();
        }
    }

    private class CancelListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            hideDialog();
        }
    }
}
