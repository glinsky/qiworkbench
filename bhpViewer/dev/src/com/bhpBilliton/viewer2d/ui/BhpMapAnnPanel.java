/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import javax.swing.*;
import com.bhpBilliton.viewer2d.AbstractPlot;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.util.PlotConstants;

import java.awt.*;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This panel is for viewing and changing the axis setting of
 *               map display. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpMapAnnPanel extends JPanel {
    private static final String[] _HLABELS = {"NONE", "TOP"};//, "BOTTOM", "BOTH"};
    private static final String[] _VLABELS = {"NONE", "LEFT"};//, "RIGHT", "BOTH"};

    private AbstractPlot _plot;

    private JTextField _vMajorField;
    private JTextField _vMinorField;
    private JTextField _hMajorField;
    private JTextField _hMinorField;
    private JComboBox _hLocationCombo;
    private JComboBox _vLocationCombo;

    private String _vMajorOld;
    private String _vMinorOld;
    private String _hMajorOld;
    private String _hMinorOld;
    private String _hLocationOld;
    private String _vLocationOld;

    /**
     * Constructs a BhpMapAnnPanel and sets up the GUI.
     */
    public BhpMapAnnPanel() {
        super();
        _plot = null;

        _vMajorField = new JTextField();
        _vMinorField = new JTextField();
        _hMajorField = new JTextField();
        _hMinorField = new JTextField();
        _vLocationCombo = new JComboBox(_VLABELS);
        _hLocationCombo = new JComboBox(_HLABELS);

        // trace tick generator does not take minor
        _hMinorField.setEnabled(false);

        JPanel panel = new JPanel(new GridLayout(6, 2));
        panel.add(new JLabel("Horizontal axis Location"));
        panel.add(_hLocationCombo);
        panel.add(new JLabel("Horizontal Major Step"));
        panel.add(_hMajorField);
        panel.add(new JLabel("Horizontal Minor Step"));
        panel.add(_hMinorField);
        panel.add(new JLabel("Vertical axis Location"));
        panel.add(_vLocationCombo);
        panel.add(new JLabel("Vertical Major Step"));
        panel.add(_vMajorField);
        panel.add(new JLabel("Vertical Minor Step"));
        panel.add(_vMinorField);

        this.setLayout(new BorderLayout());
        this.add(panel, BorderLayout.CENTER);
    }

    /**
     * Enables or diables the components in the panel for
     * either an active or a grayed-out state.
     */
    public void setAllComponentEnabled(boolean v) {
        _vMajorField.setEnabled(v);
        _vMinorField.setEnabled(v);
        _hMajorField.setEnabled(v);
        _hMinorField.setEnabled(v);
        _hLocationCombo.setEnabled(v);
        _vLocationCombo.setEnabled(v);
    }

    /**
     * Retrieves the <code>{@link AbstractPlot}</code>
     * related information and fill the fields of the GUI.
     */
    public void initWithBhpWindow(AbstractPlot p) {
        _plot = p;

        _vMajorOld = "" + _plot.getVMajorStep();
        _vMinorOld = "" + _plot.getVMinorStep();
        _hMajorOld = "" + _plot.getHMajorStep();
        _hMinorOld = "" + _plot.getHMinorStep();

        _vMajorField.setText(_vMajorOld);
        _vMinorField.setText(_vMinorOld);
        _hMajorField.setText(_hMajorOld);
        _hMinorField.setText(_hMinorOld);

        int hloc = _plot.getHAxesLoc();
        int vloc = _plot.getVAxesLoc();
        switch(vloc) {
            case PlotConstants.LEFT_RIGHT:
                _vLocationCombo.setSelectedItem("BOTH");
                _vLocationOld = "BOTH";
                break;
            case PlotConstants.RIGHT:
                _vLocationCombo.setSelectedItem("RIGHT");
                _vLocationOld = "RIGHT";
                break;
            case PlotConstants.LEFT:
                _vLocationCombo.setSelectedItem("LEFT");
                _vLocationOld = "LEFT";
                break;
            default:
                _vLocationCombo.setSelectedItem("NONE");
                _vLocationOld = "NONE";
        }
        switch(hloc) {
            case PlotConstants.TOP_BOTTOM:
                _hLocationCombo.setSelectedItem("BOTH");
                _hLocationOld = "BOTH";
                break;
            case PlotConstants.TOP:
                _hLocationCombo.setSelectedItem("TOP");
                _hLocationOld = "TOP";
                break;
            case PlotConstants.BOTTOM:
                _hLocationCombo.setSelectedItem("BOTTOM");
                _hLocationOld = "BOTTOM";
                break;
            default:
                _hLocationCombo.setSelectedItem("NONE");
                _hLocationOld = "NONE";
        }
    }

    /**
     * Retrieves valus from the GUI components and
     * updates the annotation settings of
     * <code>{@link AbstractPlot}</code>.
     * @return a boolean indicates if the annotation is changed
     * and needs an update.
     */
    public boolean updateBhpWindow() {
        boolean changed = false;
        double value = 0;

        if (!_vLocationCombo.getSelectedItem().toString().equals(_vLocationOld)) {
            int newLoc = PlotConstants.NONE;
            String locString = _vLocationCombo.getSelectedItem().toString();
            if (locString.equals("BOTH"))
                newLoc = PlotConstants.LEFT_RIGHT;
            else if (locString.equals("RIGHT"))
                newLoc = PlotConstants.RIGHT;
            else if (locString.equals("LEFT"))
                newLoc = PlotConstants.LEFT;
            _plot.setVAxesLoc(newLoc);
            _vLocationOld = locString;  //LM 20041022
            changed = true;
        }
        if (!_hLocationCombo.getSelectedItem().toString().equals(_hLocationOld)) {
            int newLoc = PlotConstants.NONE;
            String locString = _hLocationCombo.getSelectedItem().toString();
            if (locString.equals("BOTH"))
                newLoc = PlotConstants.TOP_BOTTOM;
            else if (locString.equals("TOP"))
                newLoc = PlotConstants.TOP;
            else if (locString.equals("BOTTOM"))
                newLoc = PlotConstants.BOTTOM;
            _plot.setHAxesLoc(newLoc);
            _hLocationOld = locString;  //LM 20041022
            changed = true;
        }

        try {
            if (!_vMajorField.getText().equals(_vMajorOld)) {
                value = Double.parseDouble(_vMajorField.getText());
                _plot.setVMajorStep(value);
                _vMajorOld = "" + value;
                changed = true;
            }
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_plot.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpMapAnnPanel updateBhpWindow exception:");
        }
        try {
            if (!_vMinorField.getText().equals(_vMinorOld)) {
                value = Double.parseDouble(_vMinorField.getText());
                _plot.setVMinorStep(value);
                _vMinorOld = "" + value;
                changed = true;
            }
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_plot.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpMapAnnPanel updateBhpWindow exception:");
        }
        try {
            if (!_hMajorField.getText().equals(_hMajorOld)) {
                value = Double.parseDouble(_hMajorField.getText());
                _plot.setHMajorStep(value);
                _hMajorOld = "" + value;
                changed = true;
            }
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_plot.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpMapAnnPanel updateBhpWindow exception:");
        }
        try {
            if (!_hMinorField.getText().equals(_hMinorOld)) {
                value = Double.parseDouble(_hMinorField.getText());
                _plot.setHMinorStep(value);
                _hMinorOld = "" + value;
                changed = true;
            }
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_plot.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpMapAnnPanel updateBhpWindow exception:");
        }

        return changed;
    }
}
