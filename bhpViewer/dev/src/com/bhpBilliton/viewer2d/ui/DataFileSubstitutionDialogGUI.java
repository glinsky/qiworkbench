/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui;


import javax.swing.JDialog;

import javax.swing.JScrollPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
/**
 * @author lukem
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class DataFileSubstitutionDialogGUI extends JDialog {

	private JPanel jPanel = null;
	private JTextField newFileName = null;
	private JButton jButton = null;
	private JPanel buttonPanel = null;  //  @jve:decl-index=0:visual-constraint="531,185"
	private JButton cancelButton = null;
	private JButton okButton = null;
	private JScrollPane scroller = null;
	private JPanel contentPanel = null;
	private JPanel columnHeadings = null;  //  @jve:decl-index=0:visual-constraint="547,73"
	private JLabel jLabel1 = null;
	private JLabel jLabel2 = null;
	private JPanel topFillPanel = null;

	private JLabel warningLabel = null;  //  @jve:decl-index=0:visual-constraint="969,98"
	/**
	 * This is the default constructor
	 */
	public DataFileSubstitutionDialogGUI() {
		super();
		initialize();
	}

	public void addToContents (Component comp) {
	    contentPanel.add(comp);
	}

	/**
	 * This method initializes this
	 */
	private void initialize() {
		this.setTitle("Substitute Data Sources");
		this.setContentPane(getJPanel());
		this.setSize(800, 400);
		this.setModal(true);
	}

	/**
	 * This method initializes jPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			jPanel = new JPanel();
			jPanel.setLayout(new BorderLayout());
			jPanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(5,5,5,5));
			jPanel.add(getColumnHeadings(),java.awt.BorderLayout.NORTH);
			jPanel.add(getScroller(), java.awt.BorderLayout.CENTER);
			jPanel.add(getButtonPanel(), java.awt.BorderLayout.SOUTH);
		}
		return jPanel;
	}

	/**
	 * This method initializes jPanel1
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getButtonPanel() {
		if (buttonPanel == null) {
			buttonPanel = new JPanel();
			java.awt.GridLayout gridLayout3 = new GridLayout();
			buttonPanel.setLayout(gridLayout3);
			gridLayout3.setRows(1);
			gridLayout3.setColumns(2);
			gridLayout3.setHgap(2);
			buttonPanel.add(getCancelButton(), null);
			buttonPanel.add(getOkButton(), null);
		}
		return buttonPanel;
	}
	/**
	 * This method initializes jButton1
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getCancelButton() {
		if (cancelButton == null) {
			cancelButton = new JButton();
			cancelButton.setName("cancelButton");
			cancelButton.setText("Cancel");
		}
		return cancelButton;
	}
	/**
	 * This method initializes jButton2
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getOkButton() {
		if (okButton == null) {
			okButton = new JButton();
			okButton.setName("okButton");
			okButton.setText("OK");
		}
		return okButton;
	}

	public void setOKButtonAction (ActionListener al) {
	    okButton.addActionListener(al);
	}

	public void setCancelButtonAction (ActionListener al) {
	    cancelButton.addActionListener(al);
	}

	public void setWindowEventAction (WindowListener wl) {
	    this.addWindowListener(wl);
	}
	/**
	 * This method initializes jScrollPane
	 *
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getScroller() {
		if (scroller == null) {
			scroller = new JScrollPane();
//			scroller.setBackground(java.awt.SystemColor.text);
			scroller.setViewportView(getTopFillPanel());
		}
		return scroller;
	}
	/**
	 * This method initializes jPanel2
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getContentPanel() {
		if (contentPanel == null) {
			contentPanel = new JPanel();
			contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		}
		return contentPanel;
	}
	/**
	 * This method initializes jPanel2
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getColumnHeadings() {
		if (columnHeadings == null) {
			columnHeadings = new JPanel();
			java.awt.GridLayout gridLayout1 = new GridLayout();
			columnHeadings.setLayout(gridLayout1);
			jLabel1 = new JLabel();
			jLabel2 = new JLabel();
//			columnHeadings.setSize(163, 54);
			gridLayout1.setRows(1);
			gridLayout1.setHgap(2);
			columnHeadings.add(jLabel1, null);
			columnHeadings.add(jLabel2, null);
			jLabel1.setText("Original File");
			jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
			jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
			jLabel2.setText("New File");
			jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
			jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		}
		return columnHeadings;
	}
	/**
	 * This method initializes jPanel1
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getTopFillPanel() {
		if (topFillPanel == null) {
			topFillPanel = new JPanel();
			topFillPanel.setLayout(new BorderLayout());
//			topFillPanel.setBackground(java.awt.SystemColor.controlShadow);
			topFillPanel.add(getContentPanel(), java.awt.BorderLayout.NORTH);
		}
		return topFillPanel;
	}
	/**
	 * This method initializes jLabel
	 *
	 * @return javax.swing.JLabel
	 */
	public JLabel getWarningLabel(String aString) {
//		if (warningLabel == null) {
			warningLabel = new JLabel();
			warningLabel.setText(aString);
			warningLabel.setBackground(new java.awt.Color(230,219,179));
			warningLabel.setForeground(new java.awt.Color(204,0,0));
			warningLabel.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 14));
			warningLabel.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
			warningLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
			warningLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
//		}
			warningLabel.setSize(500, 20);
			warningLabel.setPreferredSize(new java.awt.Dimension(500,20));
		return warningLabel;
	}
              }  //  @jve:decl-index=0:visual-constraint="121,39"
