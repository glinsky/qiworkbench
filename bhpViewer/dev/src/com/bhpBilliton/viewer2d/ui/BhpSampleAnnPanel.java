/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.bhpBilliton.viewer2d.BhpPlotXV;
//import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.core.*;
import com.gwsys.seismic.util.PlotConstants;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This panel is used to view and change the sample(vertical)
 *               axis setting for <code>{@link BhpPlotXV}</code>.
 *               It borrows part of its code from cgSampleAnnPanel
 *               of JSeismic. Its bahavior is also close to that of
 *               cgTraceAnnPanel<br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpSampleAnnPanel extends JPanel {
    //private BhpWindow _window;
    private BhpPlotXV _plotXV;

    private boolean _stepAuto;
    private boolean _oldStepAuto;
    private float _oldMajorStep;
    private float _oldMinorStep;
    private int _location;
    private int _oldLocation;
    private boolean _gridStatus;
    private boolean _oldGridStatus;

    private JRadioButton _autoRadioButton;
    private JRadioButton _userRadioButton;
    private JLabel _majorStepLabel;
    private JLabel _minorStepLabel;
    private JTextField _majorStepField;
    private JTextField _minorStepField;
    private JComboBox _locationField;
    private JCheckBox _gridCheckBox;

    /**
     * Constructs a BhpSampleAnnPanel and sets up the GUI.
     */
    public BhpSampleAnnPanel() {
        super();
        _plotXV = null;
        _autoRadioButton = new JRadioButton("Automatic");
        _autoRadioButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JRadioButton rb = (JRadioButton) e.getSource();
                boolean enable = true;
                if (rb.isSelected()) enable = false;

                _majorStepLabel.setEnabled(enable);
                _majorStepField.setEnabled(enable);
                _minorStepLabel.setEnabled(enable);
                _minorStepField.setEnabled(enable);
            }
        });

        _userRadioButton = new JRadioButton("User Defined");
        _userRadioButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JRadioButton rb = (JRadioButton) e.getSource();
                boolean enable = false;
                if (rb.isSelected()) enable = true;

                _majorStepLabel.setEnabled(enable);
                _majorStepField.setEnabled(enable);
                _minorStepLabel.setEnabled(enable);
                _minorStepField.setEnabled(enable);
            }
        });

        ButtonGroup rbGroup = new ButtonGroup();
        rbGroup.add(_autoRadioButton);
        rbGroup.add(_userRadioButton);

        _majorStepLabel = new JLabel("      Major Step");
        _minorStepLabel = new JLabel("      Minor Step");
        _majorStepField = new JTextField("");
        _minorStepField = new JTextField("");

        // Synthia: whenever minor!=major, there is exception.
        // disable them for now. Force major=minor in BhpPlot
        //_minorStepLabel.setEnabled(false);
        //_minorStepField.setEnabled(false);

        JPanel panel1 = new JPanel(new GridLayout(4, 2));
        panel1.setBorder(BorderFactory.createTitledBorder("Step Type"));
        panel1.setMaximumSize(new Dimension(300, 50));
        panel1.add(_autoRadioButton);
        panel1.add(new JLabel());
        panel1.add(_userRadioButton);
        panel1.add(new JLabel());
        panel1.add(_majorStepLabel);
        panel1.add(_majorStepField);
        panel1.add(_minorStepLabel);
        panel1.add(_minorStepField);

        _gridCheckBox = new JCheckBox("Show Grid");
        _locationField = new JComboBox();
        _locationField.setEditable(false);
        _locationField.addItem("NONE");
        _locationField.addItem("LEFT");
        //_locationField.addItem("RIGHT");
        //_locationField.addItem("LEFT & RIGHT");
        _locationField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox) e.getSource();
                boolean enable = true;
                String selItem = (String) cb.getSelectedItem();
                if (selItem.equals("NONE")) enable = false;
                _gridCheckBox.setEnabled(enable);
            }
        });

        JPanel panel2 = new JPanel(new GridLayout(2, 2));
        panel2.setBorder(BorderFactory.createTitledBorder("Appearance"));
        panel2.setMaximumSize(new Dimension(300, 30));
        panel2.add(new JLabel("      Location"));
        panel2.add(_locationField);
        //panel2.add(_gridCheckBox);
        panel2.add(new JLabel());

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(Box.createGlue());
        this.add(panel1);
        add(Box.createGlue());
        this.add(panel2);
        add(Box.createGlue());
    }

    /**
     * Enables or diables the components in the panel for
     * either an active or a grayed-out state.
     */
    public void setAllComponentEnabled(boolean v) {
        _autoRadioButton.setEnabled(v);
        _userRadioButton.setEnabled(v);
        _locationField.setEnabled(v);
        boolean enable = true;
        if (v == false) {
            enable = false;
        }
        else {
            if (_autoRadioButton.isSelected()) enable = false;
        }
        _majorStepField.setEnabled(enable);
        _minorStepField.setEnabled(enable);
        _majorStepLabel.setEnabled(enable);
        _minorStepLabel.setEnabled(enable);
    }

    /**
     * Retrieves the <code>{@link BhpPlotXV}</code>
     * related information and fill the fields of the GUI.
     */
    public void initWithBhpWindow(BhpPlotXV p) {
        _plotXV = p;

        _oldStepAuto = _stepAuto = _plotXV.getSampleStepAuto();
        if (_stepAuto) {
            _autoRadioButton.setSelected(true);
            _userRadioButton.setSelected(false);
            boolean enable = false;
            _majorStepLabel.setEnabled(enable);
            _majorStepField.setEnabled(enable);
            _minorStepLabel.setEnabled(enable);
            _minorStepField.setEnabled(enable);
        }
        else {
            _userRadioButton.setSelected(true);
            _autoRadioButton.setSelected(false);
            boolean enable = true;
            _majorStepLabel.setEnabled(enable);
            _majorStepField.setEnabled(enable);
            _minorStepLabel.setEnabled(enable);
            _minorStepField.setEnabled(enable);
        }

        _oldGridStatus = _plotXV.getSampleGridLine();
        _gridStatus = _plotXV.getSampleGridLine();
        if (_gridStatus) _gridCheckBox.setSelected(true);
        else _gridCheckBox.setSelected(false);

        Float stepValue = new Float(_plotXV.getVMajorStep());
        _oldMajorStep = stepValue.floatValue();
        if (_oldMajorStep == 0) _majorStepField.setText("");
        else _majorStepField.setText(stepValue.toString());
/*
        try {
            _majorStepDocument.remove(0, _majorStepDocument.getLength());
            _oldMajorStep = stepValue.floatValue();
            if (_oldMajorStep == 0) _majorStepDocument.insertString(0, "", null);
            else _majorStepDocument.insertString(0, stepValue.toString(), null);
        }
        catch (Exception ex) {
            System.out.println("BhpSampleAnnPanel initWithBhpWindow exception:");
            ex.printStackTrace();
        }
*/
        stepValue = new Float(_plotXV.getVMinorStep());
        _oldMinorStep = stepValue.floatValue();
        if (_oldMinorStep == 0)  _minorStepField.setText("");
        else _minorStepField.setText(stepValue.toString());
/*
        try {
            _minorStepDocument.remove(0, _minorStepDocument.getLength());
            _oldMinorStep = stepValue.floatValue();
            if (_oldMinorStep == 0) _minorStepDocument.insertString(0, "", null);
            else _minorStepDocument.insertString(0, stepValue.toString(), null);
        }
        catch (Exception ex) {
            System.out.println("BhpSampleAnnPanel initWithBhpWindow exception:");
            ex.printStackTrace();
        }
*/
        _oldLocation = _location = _plotXV.getVAxesLoc();
        switch(_location) {
            case PlotConstants.LEFT:
                _locationField.setSelectedItem("LEFT");
                break;
            case PlotConstants.RIGHT:
                _locationField.setSelectedItem("RIGHT");
                break;
            case PlotConstants.LEFT_RIGHT:
                _locationField.setSelectedItem("LEFT & RIGHT");
                break;
            default:
                _locationField.setSelectedItem("NONE");
        }
    }

    /**
     * Retrieves valus from the GUI components and
     * updates the annotation settings of
     * <code>{@link BhpPlotXV}</code>.
     * @return a boolean indicates if the annotation is changed
     * and needs an update.
     */
    public boolean updateBhpWindow() {
        boolean changed = false;

        String strSel = (String) _locationField.getSelectedItem();
        if (strSel.equals("LEFT")) _location = PlotConstants.LEFT;
        else if (strSel.equals("RIGHT")) _location = PlotConstants.RIGHT;
        else if (strSel.equals("LEFT & RIGHT")) _location = PlotConstants.LEFT_RIGHT;
        else _location = PlotConstants.NONE;
        if (_location != _oldLocation) {
            _plotXV.setVAxesLoc(_location);
            _oldLocation = _location;
            changed = true;
        }

        _gridStatus = _gridCheckBox.isSelected();
        if (_gridStatus != _oldGridStatus) {
            _plotXV.setSampleGridLine(_gridStatus);
            _oldGridStatus = _gridStatus;
            changed = true;
        }

        _stepAuto = _autoRadioButton.isSelected();
        if (_stepAuto != _oldStepAuto) {
            _plotXV.setSampleStepAuto(_stepAuto);
            _oldStepAuto = _stepAuto;
            changed = true;
        }

        if (!_stepAuto) {
            float value;
            try {

                value = Float.parseFloat(_majorStepField.getText());
                if (value != _oldMajorStep) {
                    _plotXV.setVMajorStep(value);
                    _oldMajorStep = value;
                    changed = true;
                }


                value = Float.parseFloat(_minorStepField.getText());
                if (value == _plotXV.getVMajorStep()) {
                            value = value/2f;
                            _minorStepField.setText(Float.toString(value));
                }

                if (value != _oldMinorStep) {
                    _plotXV.setVMinorStep(value);
                    _oldMinorStep = value;
                    changed = true;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
/*
                ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                    "BhpSampleAnnPanel updateBhpWindow exception");
*/
            }
        }

        return changed;
    }
}
