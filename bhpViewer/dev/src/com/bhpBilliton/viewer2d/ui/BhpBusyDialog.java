/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.actions.StopLoadAction;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpb.qiworkbench.compAPI.QIWConstants;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This dialog will be displayed when the program is busy
 *               loading data for layers. It will be hidden when all loading finishes.
 *               It displays the number of layers that is being loaded,
 *               and the number of layers that has failed.
 *               It has a buttons that users can use to force
 *               stopping all the loading threads. <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpBusyDialog extends JDialog {
    public static final int ADD_LAYER = 1;
    public static final int RMV_LAYER = -1;
    public static final int RMV_LAYER_ERROR = -2;
    public static final int CLEAR_LAYER = 0;

    private static final String MESSAGE = " layers loaded";
    private static final String ERRORMSG = " layers failed: ";

    private BhpViewerBase _viewer;
    private int _number;
    private int _numberError;
    private int _numberSuccess;
    private JTextField _message;
    private JProgressBar _pbar;
    private JButton _stopButton;

    /**
     * Constructs the dialog.
     */
    public BhpBusyDialog(BhpViewerBase viewer) {
      super();
      _viewer = viewer;
      _number = 0;
      _numberError = 0;
      setupGUI();
    }

    public synchronized int getLoadingLayerNumber() { return _number; }

    /**
     * Changes the number of loading layers. <br>
     * This is the only method that other classes use
     * to inform the dialog of any change regarding
     * the number of loading layers. The visibility
     * of the dialog depends on the number of loading
     * layers.
     * @param layerStatus an integer indicates the event. It can be: <br>
     * BhpBusyDialog.ADD_LAYER indicates one new loading process; <br>
     * BhpBusyDialog.RMV_LAYER indicates one loading process finishs successfully; <br>
     * BhpBusyDialog.RMV_LAYER_ERROR indicates one loading process fails; <br>
     * BhpBusyDialog.CLEAR_LAYER clean up everything; <br>
     */
    public synchronized void changeLoadingLayerNumber(int layerStatus) {
//System.out.println("changeLoadingLayerNumber: layerStatus="+layerStatus);
        if (layerStatus == CLEAR_LAYER) {
            this.hideDialog();
            _viewer.getStatusBar().showMessage(BhpViewer.INFO_MESSAGE, "Stop loading layers");
            return;
        }

        if (layerStatus > 0) {
            _number++;
        }
        else if (layerStatus == RMV_LAYER && _number > 0) {
            _numberSuccess++;
        }
        else if (layerStatus == RMV_LAYER_ERROR && _number > 0) {
            _numberError++;
        }

        if (_numberError > 0) {
            _message.setText("  " + _numberSuccess + " out of " + _number + MESSAGE +
                             " , " + _numberError + ERRORMSG);
        }
        else {
            _message.setText("      " + _numberSuccess + " out of " + _number + MESSAGE);
        }
        _pbar.setValue((int)(100*(_numberSuccess+_numberError)/_number));
        _viewer.getStatusBar().showMessage(BhpViewer.INFO_MESSAGE, _message.getText());
//System.out.println("changeLoadingLayerNumber: _number="+_number+", _numberSuccess="+_numberSuccess+", _numberError="+_numberError);
        if (_number == (_numberSuccess + _numberError) && _numberError == 0) {
            this.setVisible(false);
            _number = 0;
            _numberSuccess = 0;
        }
        else if (_number == (_numberSuccess + _numberError) && _numberError != 0) {
            this.setVisible(false);
            ErrorDialog.showErrorDialog(_viewer, QIWConstants.ERROR_DIALOG,
                                        Thread.currentThread().getStackTrace(),
                                        _numberError + ERRORMSG,
                                        new String[] {"Possible permissions problem",
                                                      "Possible missing files"},
                                        new String[] {"Fix any permissions errors",
                                                      "Restore missing files",
                                                      "If permissions are OK, contact workbench support"});

            _number = 0;
            _numberSuccess = 0;
            _numberError = 0;
        }
        else {
            if (this.isVisible() == false && _number > 0) {
                ActionListener taskPerformer = new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        showDialog();
                    }
                };
                Timer timer = new Timer(4000, taskPerformer);
                timer.setRepeats(false);
                timer.start();
            }
        }
    }

    private void showDialog() {
        if (_number > (_numberSuccess + _numberError))
            {
            this.setVisible(true);
            }
    }

    private void hideDialog() {
        this.setVisible(false);
        _number = 0;
        _numberSuccess = 0;
        _numberError = 0;
    }

    private void setupGUI() {
        JPanel control = new JPanel(new FlowLayout());
        _stopButton = new JButton("Stop Loading");
        _stopButton.addActionListener(new StopLoadAction(_viewer));
        control.add(_stopButton);

        Box content = new Box(BoxLayout.Y_AXIS);
        _message = new JTextField("      0 out of 0 " + MESSAGE);
        _message.setMaximumSize(new Dimension(250, 25));
        _message.setPreferredSize(new Dimension(250, 25));
        _message.setBackground(Color.lightGray);
        _pbar = new JProgressBar(0, 100);
        _pbar.setStringPainted(true);
        content.add(Box.createVerticalGlue());
        content.add(Box.createVerticalStrut(20));
        content.add(_message);
        content.add(Box.createVerticalStrut(20));
        content.add(_pbar);
        content.add(Box.createVerticalGlue());

        this.getContentPane().add(control, BorderLayout.SOUTH);
        this.getContentPane().add(content, BorderLayout.CENTER);
        this.pack();
        this.setSize(300, 200);
        this.setTitle("Loading Status");
        this.setModal(false);
        this.setLocation(_viewer.getMyLocation());
    }
}
