/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;

import com.bhpBilliton.viewer2d.BhpEventLayer;
import com.bhpBilliton.viewer2d.BhpEventShape;
import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpSeismicTableModel;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.data.GeneralDataSource;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This panel is for event data. <br>
 *               Besides the content of its parent class,
 *               <code>{@link OpenGeneralPanel}</code>,
 *               this panel displays all the available events.
 *               There is also a checkbox named "Synchronize". Users use it
 *               to specify whether the layer should be synchronized
 *               with the other layers on the name of event to be displayed.
 *               This panel has two different GUI configuration for
 *               cross-section display and map display respectively.
 *               For map, there is no extra GUI component other than
 *               the two mentioned above. While for cross-section, this
 *               panel has a table to display the actual values of the
 *               event. The table is editable, so that users can change
 *               the event by changing the values in the table.
 *               The graphic attribute of the event can  be set
 *               with the "Event Attribute..." button. <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class OpenEventPanel extends OpenGeneralPanel {
    private JPanel _eventPanel;
    private JPanel _dataPanel;

    private BhpEventDataTable _dataTable;
    private JCheckBox _propertySync;
    private JButton _eventAttButton;
    private JComboBox _eventCombo;
    private JList _eventList;
    private DefaultListModel _eventListModel;
    private JButton _attributeButton;
    int _dataOrder;

    private JDialog _eventAttDialog;
    private BhpEventShape _sampleEventShape;
    private BhpEventAttributePanel _sampleEventPanel;

    private BhpLayer refLayer;
    private BhpViewerBase viewer;

    /**
     * Constructs a new OpenEventPanel.
     * @param refLayer reference layer.
     * @param order data order. It can be BhpLayer.BHP_DATA_ORDER_MAPVIEW, or
     *        BhpViewer.BHP_DATA_ORDER_CROSSSECTION.
     */
    public OpenEventPanel(BhpViewerBase viewer, BhpLayer refLayer, int order) {
        super(refLayer);
        this.viewer = viewer;
        _refLayer = refLayer;
        _dataOrder = order;
        boolean grayFirstRow = false;
        if (_dataOrder == BhpViewer.BHP_DATA_ORDER_MAPVIEW) {
            grayFirstRow = true;
        }
        setupGUI(grayFirstRow);
        _eventAttDialog = null;
        //setupEventAttDialog(refLayer);
    }

    /**
     * Overrides the method of JPanel to make suer
     * that the selected event in the JList is visible.
     */
    protected void validateTree() {
        super.validateTree();
        if (_dataOrder==BhpViewer.BHP_DATA_ORDER_CROSSSECTION) {
            _eventList.ensureIndexIsVisible(_eventList.getSelectedIndex());
        }
    }

    /**
     * Retrieves the sample BhpEventShape. This shape is used to
     * hold the user's settings of the event graphic attribute.
     */
    public BhpEventShape getSampleEventShape() { return _sampleEventShape; }

    /**
     * Override the implementation of its super class.
     * This implementation commits any ongoing editing on the event data first
     * before it calls super.acceptTableChange().
     */
    public boolean acceptTableChange() {
        _dataTable.acceptTableChange();
        return super.acceptTableChange();
    }

    /**
     * Finds out if the property is set to synchronize.
     * @return boolean flag for property synchronization
     */
    public boolean getPropertySyncValue() {
        return _propertySync.isSelected();
    }
/*
    public String getSelectedEventName() {
        if (_eventCombo == null) return null;
        Object selectedObj = _eventCombo.getSelectedItem();
        if (selectedObj == null) return null;
        String name = selectedObj.toString();
        if (name==null || name.length()==0) return null;
        return name;
    }
*/
    /**
     * Retrieves the name of the selected event.
     * If multiple events are selected, the first one is returned.
     * @return the name of the selected event.
     */
    public String getSelectedEventName() {
        if (_dataOrder == BhpViewer.BHP_DATA_ORDER_MAPVIEW) {
            if (_eventCombo == null) return null;
            Object selectedObj = _eventCombo.getSelectedItem();
            if (selectedObj == null) return null;
            String name = selectedObj.toString();
            if (name==null || name.length()==0) return null;
            return name;
        }
        else {
            if (_eventList == null) return null;
            Object selectedObj = _eventList.getSelectedValue();
            if (selectedObj == null) return null;
            String name = selectedObj.toString();
            if (name==null || name.length()==0) return null;
            return name;
        }
    }

    /**
     * Retrieves the names of all the selected events.
     * @return names of all the selected events.
     */
    public String[] getSelectedEventNames() {
        if (_dataOrder == BhpViewer.BHP_DATA_ORDER_MAPVIEW) {
            System.out.println("OpenEventPanel.getSelectedEventNames warning:");
            System.out.println("    Please use getSelectedEventName for map display.");
            return null;
        }
        if (_eventList == null) return null;
        Object[] selectedObjs = _eventList.getSelectedValues();
        if (selectedObjs==null || selectedObjs.length==0) return null;
        String[] names = new String[selectedObjs.length];
        for (int i=0; i<names.length; i++) {
            names[i] = "";
            if (selectedObjs[i] != null) names[i] = selectedObjs[i].toString();
        }
        return names;
    }

    public void initWithLayer(BhpLayer alayer) {
        if (alayer.getDataType() != BhpViewer.BHP_DATA_TYPE_EVENT) return;

        //BhpEventLayer layer = (BhpEventLayer) alayer;
        _eventAttButton.setEnabled(false);
        BhpSeismicTableModel model = alayer.getParameter();

        buildEventCombo(model.getProperties(), alayer.getPropertyName());
        _propertySync.setSelected(alayer.getPropertySync());

        if (_dataOrder == BhpViewer.BHP_DATA_ORDER_MAPVIEW) {
            _dataPanel.setEnabled(false);
            _dataTable.initWithLayer(null);
        }
        else {
            _dataPanel.setEnabled(true);
            _dataTable.initWithLayer((BhpEventLayer)alayer);
        }

        _orderEditor.removeAllItems();
        _tableModel.cleanupTableModel();
        Vector data = model.getDataVector();
        for (int i=0; i<data.size(); i++) {
            _tableModel.addRow(new Vector((Vector) data.elementAt(i)));
            _orderEditor.addItem("" + (i+1));
        }
        _table.getColumnModel().getColumn(3).setCellEditor(new DefaultCellEditor(_orderEditor));

        _verticalPanel.initVertical(_tableModel, alayer.getDataSource());
    }

    public int applyToLayer(BhpLayer alayer) {
        if (alayer.getDataType() != BhpViewer.BHP_DATA_TYPE_EVENT) return 0;

        //if ((alayer instanceof BhpEventLayer) == false) return 0;
        //BhpEventLayer layer = (BhpEventLayer) alayer;
        //layer.setPropertySync(_propertySync.isSelected());
        _verticalPanel.applyVertical(_tableModel);

        int changeFlag = 0;
        alayer.setPropertySync(_propertySync.isSelected());
        if (this.getSelectedEventName().equals(alayer.getPropertyName()) == false) {
            changeFlag = changeFlag | BhpViewer.EVENTNAME_FLAG;
            alayer.setPropertyName(this.getSelectedEventName());
        }
        if (alayer instanceof BhpEventLayer) {
            // for the real data
            _dataTable.applyToLayer((BhpEventLayer)alayer);
        }

        BhpSeismicTableModel model = alayer.getParameter();
        model.clearRowsChanged();
        if (model.getRowCount() != _tableModel.getRowCount() ||
            model.getColumnCount() != _tableModel.getColumnCount())
            changeFlag = changeFlag | BhpViewer.DATASELECTION_FLAG;

        if (changeFlag != 0) {
            Vector colNames = new Vector();
            for (int i=0; i<_tableModel.getColumnCount(); i++)
                colNames.addElement(_tableModel.getColumnName(i));
            model.setDataVector(_tableModel.getDataVector(), colNames);
            return changeFlag;
        }

        Object original, current;
        for (int i=0; i<_tableModel.getRowCount(); i++) {
            for (int j=0; j<_tableModel.getColumnCount(); j++) {
                original = model.getValueAt(i, j);
                current = _tableModel.getValueAt(i, j);
                if (original.equals(current) == false) {
                    if (j == 2) {
                        boolean chosenRangeChanged = model.setChosenRangeAt(current, i);
                        if (!chosenRangeChanged) continue;
                    }
                    else {
                        model.setValueAt(current, i, j);
                    }
                    if (j==2 || j==4 || j==7 || j==8) model.setRowsChanged(i);
                    if (j!=1 && j!=6) changeFlag = changeFlag | BhpViewer.DATASELECTION_FLAG;
                }
            }
        }
        return changeFlag;
    }

    public void describeData(GeneralDataSource dataSource) {
        _eventAttButton.setEnabled(true);
        _tableModel.cleanupTableModel();
        _tableModel.setDataSource(dataSource);
        _dataPanel.setEnabled(false);

        String[] names = dataSource.getHeaders();
        for (int i=0; i<names.length; i++) {
            _tableModel.addRowData(names[i], dataSource.getHeaderSetting(names[i]));
        }
        String properties = dataSource.getProperties();
        buildEventCombo(properties, null);
        _tableModel.setProperties(properties);
        _tableModel.setDataAttributeString(dataSource.getDataAtt());

        if (_refLayer != null) {
            _tableModel.initWithAnother(_refLayer.getParameter());
        }
        _eventPanel.validate();
        _eventPanel.repaint();

        _orderEditor.removeAllItems();
        for (int i=0; i<_tableModel.getRowCount(); i++) {
            _orderEditor.addItem("" + (i+1));
        }
        _table.getColumnModel().getColumn(3).setCellEditor(new DefaultCellEditor(_orderEditor));

        _verticalPanel.initVertical(_tableModel, dataSource.getDataSourceName());
    }

    /**
     * Builds the GUI of the panel itself.
     */
    protected void buildPanel() {
        JPanel tablePanel = new JPanel(new BorderLayout());
        _tableModel = new BhpSeismicTableModel(viewer, false);
        //_table = new JTable(_tableModel);
        _table.setModel(_tableModel);
        tablePanel.add(new JScrollPane(_table), BorderLayout.CENTER);

        _eventPanel = new JPanel();
        _propertySync = new JCheckBox("Synchronize");
        _eventAttButton = new JButton("Display Style");
        _eventAttButton.addActionListener(new AttButtonListener());

        if (_dataOrder == BhpViewer.BHP_DATA_ORDER_MAPVIEW) {
            _eventCombo = new JComboBox();
            _eventListModel = null;
            _eventList = null;

            _eventPanel.setLayout(new GridLayout(1, 4));
            _eventPanel.setBorder(new TitledBorder("Horizon Setting"));
            _eventPanel.setPreferredSize(new Dimension(300, 120));

            _eventPanel.add(new JLabel("Event", JLabel.CENTER));
            _eventPanel.add(_eventCombo);
            _eventPanel.add(new JLabel(""));
            _eventPanel.add(_propertySync);
        }
        else {
            _eventCombo = null;
            _eventListModel = new DefaultListModel();
            _eventList = new JList(_eventListModel);

            Box epanel = new Box(BoxLayout.Y_AXIS);
            epanel.add(Box.createVerticalGlue());
            epanel.add(_propertySync);
            epanel.add(_eventAttButton);
            _eventPanel.setLayout(new BorderLayout());
            _eventPanel.setBorder(new TitledBorder("Horizon Setting"));
            _eventPanel.add(new JScrollPane(_eventList), BorderLayout.CENTER);
            _eventPanel.add(epanel, BorderLayout.EAST);
        }

        _dataPanel = new JPanel(new BorderLayout());
        _dataPanel.setBorder(new TitledBorder("Horizon Data"));
        _dataTable = new BhpEventDataTable();
        _dataPanel.add(new JScrollPane(_dataTable), BorderLayout.CENTER);

        Box box = new Box(BoxLayout.Y_AXIS);
        box.add(tablePanel);
        if (_dataOrder == BhpViewer.BHP_DATA_ORDER_MAPVIEW) {
            box.add(_verticalPanel);
            box.add(_eventPanel);
        }
        else {
            JPanel inbox = new JPanel(new GridLayout(1,2));
            inbox.add(_eventPanel);
            inbox.add(_dataPanel);
            box.add(inbox);
        }

        this.add(box, BorderLayout.CENTER);
    }

    private void setupEventAttDialog() {
        JPanel controlPanel = new JPanel(new FlowLayout());
        JButton okb = new JButton("Ok");
        okb.addActionListener(new OkListener());
        JButton ccb = new JButton("Cancel");
        ccb.addActionListener(new CancelListener());
        controlPanel.add(okb);
        controlPanel.add(ccb);

        _sampleEventShape = new BhpEventShape(null, 0, 0, 0, 0, 0, 0);
        _sampleEventShape.initAttribute();

        BhpLayer refLayer = super._refLayer;
        if (refLayer != null && (refLayer instanceof BhpEventLayer)) {
            BhpEventShape refShape = ((BhpEventLayer)refLayer).getEventShape();
            _sampleEventShape.setEventAttribute(refShape);
        }
        _eventAttDialog = new JDialog(new Frame(), "Event Attribute", true);
        _sampleEventPanel = new BhpEventAttributePanel();
        _eventAttDialog.getContentPane().add(_sampleEventPanel, BorderLayout.CENTER);
        _eventAttDialog.getContentPane().add(controlPanel, BorderLayout.SOUTH);
        _eventAttDialog.pack();
    }

    private void buildEventCombo(String properties, String selectedP) {
        if (_dataOrder == BhpViewer.BHP_DATA_ORDER_MAPVIEW) {
            _eventCombo.removeAllItems();
            StringTokenizer pstk = new StringTokenizer(properties, " ");
            int index = 0;
            while(pstk.hasMoreTokens()) {
                String pname = pstk.nextToken().trim();
                if (pname.length() == 0) continue; //jie
                _eventCombo.addItem(pname);
                if (pname.equals(selectedP)) _eventCombo.setSelectedIndex(index);
                index++;
            }
        }
        else {
            _eventListModel.removeAllElements();
            StringTokenizer pstk = new StringTokenizer(properties, " ");
            int index = 0;
            while(pstk.hasMoreTokens()) {
                String pname = pstk.nextToken().trim();
                if (pname.length() == 0) continue; //jie
                _eventListModel.addElement(pname);
                if (pname.equals(selectedP)) _eventList.setSelectedIndex(index);
                index++;
            }
            if(index == 0) return;
            if (_eventList.getSelectedValue() == null)
                _eventList.setSelectedIndex(0);
        }
    }

    private void showEventAttDialog() {
        if (_eventAttDialog == null) setupEventAttDialog();
        if (_eventAttDialog != null && (!_eventAttDialog.isVisible())) {
            if (_sampleEventShape != null)
                _sampleEventPanel.setShape(_sampleEventShape);
            _eventAttDialog.setVisible(true);
        }
    }

    private void applyEventAttDialog() {
        _sampleEventPanel.apply();
        hideEventAttDialog();
    }

    private void hideEventAttDialog() {
        _eventAttDialog.setVisible(false);
    }

    private class OkListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            applyEventAttDialog();
        }
    }

    private class CancelListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            hideEventAttDialog();
        }
    }

    private class AttButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            showEventAttDialog();
        }
    }
}
