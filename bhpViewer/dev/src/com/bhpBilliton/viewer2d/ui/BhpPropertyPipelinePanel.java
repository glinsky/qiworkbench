/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import javax.swing.*;

import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.border.TitledBorder;
import java.util.Hashtable;

import com.bhpBilliton.viewer2d.BhpEventLayer;
import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpLayerEvent;
import com.bhpBilliton.viewer2d.BhpMapLayer;
import com.bhpBilliton.viewer2d.BhpModelDataSelector;
import com.bhpBilliton.viewer2d.BhpModelLayer;
import com.bhpBilliton.viewer2d.BhpModelTraceInterpolator;
import com.bhpBilliton.viewer2d.BhpModelTraceNormalization;
import com.bhpBilliton.viewer2d.BhpModelTraceRasterizer;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.BhpWindow;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.core.*;
import com.gwsys.seismic.gui.AutoGainPanel;
import com.gwsys.seismic.gui.DataViewportPanel;
import com.gwsys.seismic.gui.NormalizationPanel;
import com.gwsys.seismic.gui.RasterizePanel;
import com.gwsys.seismic.util.SeismicColorMap;
import com.gwsys.util.colorbar.*;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This panel enables viewing and changing display and pipeline
 *               settings of a <code>{@link BhpLayer}</code>.
 *               Display settings include layer name, visibility, display order,
 *               scales and ect. According to different types of
 *               pipeline and the components the pipeline has, pipeline
 *               settings have different components.
 *               This panel is usually used in the
 *               <code>{@link BhpSLayerPropertyDialog}</code>
 *               with other panels.
 *               <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpPropertyPipelinePanel extends JPanel {
    private static final String[] CURSOR_VALUES = {"Default"};
    private static final String[] VISIBLE_VALUES = {"Visible", "Invisible"};
    private static final String[] POLARITY_VALUES = {"Normal", "Reversed"};

    private DisplayPropertyPanel _displayPanel;
    private AutoGainPanel _agcPanel;
    private DataViewportPanel _dsPanel;
    private RasterizePanel _rsPanel;
    private NormalizationPanel _scPanel;
    private BhpEventAttributePanel _attPanel;
    private BhpMapPipelinePanel _mapPanel;

    private JPanel _localSubsetPanel;
    private ColormapIO _colormapManager;
    private BhpViewerBase _viewer;

    // for model
    private JPanel _modelPanel;
    private JCheckBox _structuralInterpolation;
    private JTextField _startDepth;
    private JTextField _endDepth;
    private BhpLayer _layer;

    /**
     * Constructs a new instance.
     */
    public BhpPropertyPipelinePanel(ColormapIO cmanager, BhpViewerBase v) {
        // have to do the real thing in initWithLayer
        super(new BorderLayout());
        _colormapManager = cmanager;
        _viewer = v;
        _displayPanel = new DisplayPropertyPanel();
        _attPanel = new BhpEventAttributePanel();
        _mapPanel = null;
        _localSubsetPanel = new JPanel();
    }

    /**
     * Gets the panel for data sub-selection. <br>
     * In <code>{@link OpenDataPanel}</code>, users can
     * select to read in only part of the whole data set.
     * However, for all the data brought in, users can
     * choose to view only a subset of it. This panel
     * enables viewing and changing the setting of this
     * sub-selection.
     */
    public JPanel getLocalSubsetPanel() {
        return _localSubsetPanel;
    }

    private void initWithLayerEvent(BhpEventLayer layer, Box box) {
        _dsPanel = new DataViewportPanel(layer.getPipeline());
        // disable the panel for primary and secondary data selection.
        // these functionality is not supported by BhpEventDataSelector.
        Component tmpcomp = null;
        JPanel tmppanel = null;
        int panelcounter = 0;
        for(int i=0; i<_dsPanel.getComponentCount(); i++) {
            tmpcomp = _dsPanel.getComponent(i);
            if (tmpcomp instanceof JPanel) {
                tmppanel = (JPanel) tmpcomp;
                panelcounter++;
                if (panelcounter < 3) {
                    for (int j=0; j<tmppanel.getComponentCount(); j++) {
                        tmppanel.getComponent(j).setEnabled(false);
                    }
                }
            }
        }

        _localSubsetPanel = _dsPanel;
        try {
            if (layer.getPipeline().getDataLoader().getDataReader().getDataFormat() != null)
                {
                _dsPanel.initializeUIValues();
                }
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "BhpSLayerPropertyDialog.initWithLayerEvent...");
        }
        _attPanel.setShape(layer.getShapeListLayer().getShape(0));

        box.add(Box.createVerticalGlue());
        box.add(_attPanel);
        box.add(Box.createVerticalGlue());
    }

    private void initWithLayerMap(BhpMapLayer layer, Box box) {
        _mapPanel = new BhpMapPipelinePanel(_colormapManager,_viewer);
        _mapPanel.initWithLayer(layer);
        _localSubsetPanel = _mapPanel.getLocalSubsetPanel();
        box.add(Box.createHorizontalGlue());
        box.add(_mapPanel);
        box.add(Box.createHorizontalGlue());
    }

    /**
     * Initialize the panel with a
     * <code>{@link BhpLayer}</code> and a
     * <code>{@link BhpWindow}</code>. <br>
     * This method retrieves values from
     * the layer and the window to fill the fields in its GUI.
     * The window is needed to find out the displaying order
     * of the layer.
     */
    public void initWithLayer(BhpLayer layer, BhpWindow window) {
        this.removeAll();
        _layer = layer;
        _displayPanel.initWithLayer(layer, window);

        if (layer.getPipeline() == null)  {
            Box box = new Box(BoxLayout.X_AXIS);
            //box.add(box.createHorizontalGlue());
            box.add(_displayPanel);
            this.setLayout(new BorderLayout());
            this.add(box, BorderLayout.CENTER);

            return;
        }

        if (layer instanceof BhpEventLayer) {
            Box box = new Box(BoxLayout.Y_AXIS);
            box.add(_displayPanel);
            initWithLayerEvent((BhpEventLayer)layer, box);
            this.setLayout(new BorderLayout());
            this.add(box, BorderLayout.CENTER);

            return;
        }
        else if (layer instanceof BhpMapLayer) {
            Box box = new Box(BoxLayout.X_AXIS);
            box.add(_displayPanel);
            initWithLayerMap((BhpMapLayer)layer, box);
            this.setLayout(new BorderLayout());
            this.add(box, BorderLayout.CENTER);
            return;
        }

        Box box = new Box(BoxLayout.X_AXIS);
        //box.add(box.createHorizontalGlue());
        box.add(_displayPanel);
        this.setLayout(new BorderLayout());
        this.add(box, BorderLayout.CENTER);

        boolean isModel = false;
        if (layer instanceof BhpModelLayer) isModel = true;

        SeismicWorkflow pl = layer.getPipeline();
        //_dsPanel = new MyDataSelectorPanel(pl, isModel);
        _dsPanel = new DataViewportPanel(pl);
        _localSubsetPanel = _dsPanel;

        Component acomp = _dsPanel.getComponent(_dsPanel.getComponentCount()-2);
        if (acomp instanceof JPanel) {
            JPanel apanel = (JPanel) acomp;
            apanel.setBorder(new TitledBorder("Time/Depth Range"));
            if (isModel) {
                // disable editing start sample value and end sample value for model layer
                if (apanel.getComponentCount() == 4) {
                    for (int ii=0; ii<4; ii++)
                        apanel.getComponent(ii).setEnabled(false);
                }
            }
        }

        if (pl.getInterpretation().getTraceInterpolator() != null)
            _scPanel = new NormalizationPanel(pl);
        if (pl.getTraceRasterizer() != null) {
            _rsPanel = new RasterizePanel(pl);
            _rsPanel.setColorMapButtonListener(new MyColormapButtonListener(pl));
        }

        if (pl.getInterpretation().getTraceAGC() != null && isModel == false) {

            _agcPanel = new AutoGainPanel(pl);
        }
        else if (isModel) {
            _modelPanel = new JPanel();
            _modelPanel.setBorder(new TitledBorder("Model Vertical Range"));
            Box modelBox = new Box(BoxLayout.Y_AXIS);
            //JPanel modelPanel1 = new JPanel(new GridLayout(4, 2));
            JPanel modelPanel1 = new JPanel(new GridLayout(2, 2));
            BhpModelTraceRasterizer raster = (BhpModelTraceRasterizer)pl.getTraceRasterizer();
            _structuralInterpolation = new JCheckBox("Structural Interpolation", raster.getStructuralInterpFlag());
            _startDepth = new JTextField("" + raster.getStartDepth());
            _endDepth = new JTextField("" + raster.getEndDepth());

            BhpModelTraceNormalization mnorm = (BhpModelTraceNormalization)
            pl.getInterpretation().getTraceNormalization();
            double minAmp = mnorm.getRangeMin();
            double maxAmp = mnorm.getRangeMax();

            modelPanel1.add(new JLabel("Start Time"));
            modelPanel1.add(_startDepth);
            modelPanel1.add(new JLabel("End Time"));
            modelPanel1.add(_endDepth);

            modelBox.add(_structuralInterpolation);

            modelBox.add(modelPanel1);
            _modelPanel.add(modelBox);
        }

        try {
            _dsPanel.initializeUIValues();
            if(pl.getInterpretation().getTraceInterpolator() != null) {
                _scPanel.initializeUIValues();
            }
            if (pl.getTraceRasterizer() != null) {
                _rsPanel.initializeUIValues();
            }
            if (pl.getInterpretation().getTraceAGC() != null && isModel == false) {
                //_agcPanel.getPipelineParameters();
                _agcPanel.initializeUIValues();
            }
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "BhpSLayerPropertyDialog.pipelinePanel...");
        }

        if (pl instanceof DefaultWorkflow) {
            Box box2 = new Box(BoxLayout.Y_AXIS);
            box.add(Box.createVerticalGlue());
            box2.add(_scPanel);
            //box2.add(box2.createVerticalStrut(10));
            if (isModel == false) box2.add(_agcPanel);
            else box2.add(_modelPanel);
            box2.add(Box.createVerticalGlue());

            //box.add(_dsPanel);
            box.add(box2);
            box.add(_rsPanel);
        }
    }

    private int applyPropertyPipelinePanelMap(BhpLayer layer, boolean[] changed) {
        return _mapPanel.apply();
    }

    private int applyPropertyPipelinePanelEvent(BhpLayer layer, boolean[] changed) {
        int flag = 0;
        DataChooser dse = layer.getPipeline().getDataLoader().getDataSelector();
        double oldStartSample = dse.getStartSampleValue();
        double oldEndSample = dse.getEndSampleValue();
        boolean ds = false;
        try {
            if (layer.getPipeline().getDataLoader().getDataReader().getDataFormat() != null)
                ds = _dsPanel.commitUIValues();
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                   "BhpPropertyPipelinePanel.applyPropertyPipelinePanelEvent Exception");
        }
        double newStartSample = dse.getStartSampleValue();
        double newEndSample = dse.getEndSampleValue();
        if (ds) {
            // needs too much code to tell it is really changed,
            // may cause extra change. However, it should not hurt anything
            flag = flag | BhpViewer.SELECTOR_FLAG;
            if (oldStartSample != newStartSample || oldEndSample != newEndSample)
                flag = flag | BhpViewer.TIMERANGE_FLAG;
        }
        changed[0] = changed[0] || ds;

        boolean attFlag = _attPanel.apply();
        if (attFlag) flag = flag | BhpViewer.EVENTATT_FLAG;
        return flag;
    }

    /**
     * apply the changes to the
     * <code>{@link BhpLayer}</code>. <br>
     * This method retrieves values from
     * its GUI and apply it to the layer.
     * @param layer the layer where changes will be taken
     * @param window the window where the layer is shown.
     *        This window is needed to update the display order of the layer.
     * @param changed a boolean array of size 1 indicating whether the pipeline
     *        has been changed. If the return value is 0, this boolean may still
     *        be true, thus we still need to redraw the layer.
     * @return a flag indicating what has been changed.
     */
    public int applyToLayer(BhpLayer layer, BhpWindow window, boolean[] changed) {
        int displayFlag = _displayPanel.applyToLayer(layer, window, changed);
        if (layer.getPipeline() == null) return displayFlag;

        // colormap need more work
        if (layer instanceof BhpEventLayer) {
            int eventFlag = applyPropertyPipelinePanelEvent(layer, changed);
            return (eventFlag | displayFlag);
        }
        else if (layer instanceof BhpMapLayer) {
            int mapFlag = applyPropertyPipelinePanelMap(layer, changed);
            return (mapFlag | displayFlag);
        }

        boolean isModel = false;
        if (layer instanceof BhpModelLayer) isModel = true;
        SeismicWorkflow pl = layer.getPipeline();
        double oldStartSample = pl.getDataLoader().getDataSelector().getStartSampleValue();
        double oldEndSample = pl.getDataLoader().getDataSelector().getEndSampleValue();
        TraceNormalizer tn = pl.getInterpretation().getTraceNormalization();
        float oldScale = tn.getScale();
        int oldNorm = tn.getNormalizationMode();
        double oldNormMax = tn.getNormLimitMax();
        double oldNormMin = tn.getNormLimitMin();

        int oldDisplay = pl.getTraceRasterizer().getPlotType();
        double oldWiggleDecimation = 1.0;
            oldWiggleDecimation = (pl.getTraceRasterizer()).getMinSpacing();

        //float oldClip = pl.getTraceRasterizer().getClippingValue();
        boolean oldStructural = true;
        if (isModel) {
            oldStructural = ((BhpModelTraceRasterizer)pl.getTraceRasterizer()).getStructuralInterpFlag();
        }

        boolean ds = _dsPanel.commitUIValues();
        boolean sc = _scPanel.commitUIValues();
        boolean rs = _rsPanel.commitUIValues();
        boolean agc = false;
        boolean modelRange = false;
        boolean colorRange = false;
        if (isModel) {
            BhpModelTraceRasterizer raster = (BhpModelTraceRasterizer)pl.getTraceRasterizer();
            BhpModelDataSelector selector = (BhpModelDataSelector)pl.getDataLoader().getDataSelector();
            BhpModelTraceInterpolator interpolator = (BhpModelTraceInterpolator)
            pl.getInterpretation().getTraceInterpolator();
//            BhpModelTraceNormalization mnorm = (BhpModelTraceNormalization) pl.getTraceNormalization();
            if (raster.getStructuralInterpFlag() != _structuralInterpolation.isSelected()) {
                raster.setStructuralInterpFlag(_structuralInterpolation.isSelected());
                agc = true;
            }

            try {
                float newStartDepth = Float.parseFloat(_startDepth.getText());
                if (newStartDepth != raster.getStartDepth()) {
                    raster.setStartDepth(newStartDepth);
                    selector.setStartDepth(newStartDepth);
                    interpolator.setStartDepth(newStartDepth);
                    agc = true;
                    modelRange = true;
                }
                float newEndDepth = Float.parseFloat(_endDepth.getText());
                if (newEndDepth !=raster.getEndDepth()) {
                    raster.setEndDepth(newEndDepth);
                    selector.setEndDepth(newEndDepth);
                    interpolator.setEndDepth(newEndDepth);
                    agc = true;
                    modelRange = true;
                }
            }
            catch (Exception ex) {
                ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                        "BhpSLayerPropertyDialog.applyPropertyPipelinePanel Exception");
            }
        }
        else {
            agc = _agcPanel.commitUIValues();
        }
        //else agc = _agcPanel.updatePipelineParameters();

        changed[0] = changed[0] || ds || sc || rs || agc;

        double newStartSample = pl.getDataLoader().getDataSelector().getStartSampleValue();
        double newEndSample = pl.getDataLoader().getDataSelector().getEndSampleValue();
        TraceNormalizer tno = pl.getInterpretation().getTraceNormalization();
        float newScale = tno.getScale();
        float newNorm = tno.getNormalizationMode();
        int newInterp = pl.getInterpretation().getTraceInterpolator().getInterpolationType();
        int newDisplay = pl.getTraceRasterizer().getPlotType();
        double newWiggleDecimation = oldWiggleDecimation;
        //remove instanceof
        newWiggleDecimation = (pl.getTraceRasterizer()).getMinSpacing();

        //float newClip = pl.getTraceRasterizer().getClippingValue();

        int flag = 0;
        if (ds) {
            // needs too much code to tell it is really changed,
            // may cause extra change. However, it should not hurt anything
            flag = flag | BhpViewer.SELECTOR_FLAG;
            if (!isModel) {
                if (oldStartSample != newStartSample || oldEndSample != newEndSample)
                    {
                    flag = flag | BhpViewer.TIMERANGE_FLAG;
                    }
            }
        }
        if (sc) {   // scaling panel is changed
            if (oldScale != newScale) {
                flag = flag | BhpViewer.SCALEAMP_FLAG;
            }
            if (oldNorm != newNorm) {
                flag = flag | BhpViewer.NORMALIZE_FLAG;
            }
            else if (oldNorm==newNorm && newNorm==TraceNormalizer.AMPLITUDE_LIMITS) {
                double newNormMax = pl.getInterpretation().getTraceNormalization().getNormLimitMax();
                double newNormMin = pl.getInterpretation().getTraceNormalization().getNormLimitMin();
                if (newNormMax!=oldNormMax || newNormMin!=oldNormMin)
                    {
                    flag = flag | BhpViewer.NORMALIZE_FLAG;
                    }
            }
        }
        if(rs) {    // rasterizer panel is changed
            if (oldDisplay != newDisplay) {
                flag = flag | BhpViewer.DISPLAY_FLAG;
            }
            if (oldWiggleDecimation != newWiggleDecimation) {
                flag = flag | BhpViewer.WIGGLEDECIMATION_FLAG;
            }
        }
        if (isModel && agc) {
            if (modelRange) {
                flag = flag | BhpViewer.TIMERANGE_FLAG;
            }
            if (colorRange) {
                flag = flag | BhpViewer.COLORINTER_FLAG;
            }
            boolean newStructural = ((BhpModelTraceRasterizer)pl.getTraceRasterizer()).getStructuralInterpFlag();
            if (newStructural != oldStructural) {
                flag = flag | BhpViewer.DISPLAY_FLAG;
                pl.invalidate(pl.getInterpretation().getTraceNormalization());
            }
            else {
                pl.invalidate(pl.getInterpretation().getTraceInterpolator());
            }
        }
        else if (isModel==false && agc) {
            flag = flag | BhpViewer.AGC_FLAG;
        }
        flag = flag | displayFlag;
        return flag;
    }

    private class DisplayPropertyPanel extends JPanel {
        private JTextField _nameField;
        private JTextField _valueOrder;
        private JTextField _valueTrans;
        private JTextField _valueHscale;
        private JTextField _valueVscale;
        private JComboBox _valueVisible;
        private JComboBox _valuePolarity;
        //private JComboBox _valueCursor;
        private JLabel _vscaleLabel;
        private JSlider _slider;

        private JPanel _layerPanel;
        private JPanel _scalePanel;
        private JPanel _opacityPanel;
        private Box _leftBox;

        public DisplayPropertyPanel() {
            super();
            buildDisplayPropertyPanelGUI();
        }

        public void initWithLayer(BhpLayer layer, BhpWindow window) {
            // layer visibility and order are not longer displayed.
            _nameField.setText(layer.getLayerName());
            _valueHscale.setText("" + layer.getHorizontalScaleFactor());
            _valueVscale.setText("" + layer.getVerticalScaleFactor());
            int transValue = (int) (layer.getTransparent()/2.55);
            _valueTrans.setText("" + transValue/100.0);
            _slider.setValue(transValue);
            if (layer.getReversedPolarity()) {
                _valuePolarity.setSelectedIndex(1);
            } else {
                _valuePolarity.setSelectedIndex(0);
            }

            if (layer instanceof BhpEventLayer) {
                _layerPanel.removeAll();
                _layerPanel.setLayout(new GridLayout(1, 2));
                JLabel tmpLabel = new JLabel("  Layer Name");
                tmpLabel.setPreferredSize(new Dimension(205, 25));
                _layerPanel.add(tmpLabel);
                _layerPanel.add(_nameField);
                _leftBox.removeAll();
                _leftBox.add(Box.createVerticalStrut(10));
                _leftBox.add(_layerPanel);
            }
            else {
                _layerPanel.removeAll();
                _layerPanel.setLayout(new GridLayout(2, 2));
                _layerPanel.add(new JLabel("  Layer Name"));
                _layerPanel.add(_nameField);
                _layerPanel.add(new JLabel("  Polarity"));
                _layerPanel.add(_valuePolarity);

                _leftBox.removeAll();
                _leftBox.add(Box.createVerticalStrut(10));
                _leftBox.add(_layerPanel);
                _leftBox.add(Box.createVerticalStrut(30));
                _leftBox.add(_scalePanel);
                _leftBox.add(Box.createVerticalStrut(30));
                _leftBox.add(_opacityPanel);
                _leftBox.add(Box.createVerticalGlue());
            }
        }

        public int applyToLayer(BhpLayer layer, BhpWindow window, boolean[] changed) {
            int eventFlag = 0;
            if (!_nameField.getText().equals(layer.getLayerName())) {
                layer.setLayerName(_nameField.getText());
                eventFlag = eventFlag | BhpViewer.NAME_FLAG;
            }
            boolean lreversed = false;
            if (_valuePolarity.getSelectedIndex() != 0) {
                lreversed = true;
            }
            if (layer.getReversedPolarity() != lreversed) {
                layer.setReversedPolarity(lreversed);
                eventFlag = eventFlag | BhpViewer.POLARITY_FLAG;
                SeismicWorkflow pipeline = layer.getPipeline();
                if (pipeline != null)
                    {
                    pipeline.invalidate(pipeline.getDataLoader().getDataReader());
                    }
            }
            // layer visibility and order are not longer displayed.
            try {
                //int trans = Integer.parseInt(_valueTrans.getText());
                int trans = (int)(_slider.getValue()*2.55f);
                int oldTrans = layer.getTransparent();
                if (trans != oldTrans) {
                    layer.setTransparent(trans);
                    eventFlag = eventFlag | BhpViewer.TRANSPARENCY_FLAG;
                }
            }
            catch (Exception ex) {
                ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                  "BhpSLayerPropertyDialog.apply Transparency Exception");
            }
            // need add code to handle cursor, visible, order, polarity, trans.

            try {
                double vfactor = Double.parseDouble(_valueVscale.getText());
                if (vfactor != layer.getVerticalScaleFactor()) {
                    layer.setVerticalScaleFactor(vfactor);
                    changed[0] = true;
                }

            }
            catch (Exception ex) {
                ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                    "BhpSLayerPropertyDialog.apply Vscale Exception");
            }
            try {
                double hfactor = Double.parseDouble(_valueHscale.getText());
                if (hfactor != layer.getHorizontalScaleFactor()) {
                    layer.setHorizontalScaleFactor(hfactor);
                    changed[0] = true;
                }

            }
            catch (Exception ex) {
                ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                    "BhpSLayerPropertyDialog.apply Hscale Exception");
            }
            
            return eventFlag;
        }

        private void buildDisplayPropertyPanelGUI() {
            _nameField = new JTextField("");
            _valueOrder = new JTextField("");
            _valueTrans = new JTextField("");
            _valueTrans.setEditable(false);
            _valueHscale = new JTextField("");
            _valueVscale = new JTextField("");
            _valueVisible = new JComboBox(VISIBLE_VALUES);
            _valuePolarity = new JComboBox(POLARITY_VALUES);
            _valuePolarity.setEnabled(false);///Fix later
            _valueHscale.setPreferredSize(new Dimension(100, 20));
            _valueVscale.setPreferredSize(new Dimension(100, 20));
            _nameField.setPreferredSize(new Dimension(60, 20));
            _valueOrder.setPreferredSize(new Dimension(60, 20));

            _slider = new JSlider(0, 100, 100);
            Hashtable sliderLabel = new Hashtable();
            sliderLabel.put(new Integer(0), new JLabel("clear"));
            sliderLabel.put(new Integer(100), new JLabel("     opaque"));
            sliderLabel.put(new Integer(20), new JLabel("0.2"));
            sliderLabel.put(new Integer(40), new JLabel("0.4"));
            sliderLabel.put(new Integer(60), new JLabel("0.6"));
            sliderLabel.put(new Integer(80), new JLabel("0.8"));
            _slider.setMajorTickSpacing(20);
            _slider.setMinorTickSpacing(10);
            _slider.setLabelTable(sliderLabel);
            _slider.setPaintLabels(true);
            _slider.setPaintTicks(true);
            _slider.addChangeListener(new ChangeListener() {
                public void stateChanged(ChangeEvent e) {
                    JSlider source = (JSlider) (e.getSource());
                    _valueTrans.setText("" + source.getValue()/100.0);
                }
            });

            _opacityPanel = new JPanel();
            BoxLayout lp3 = new BoxLayout(_opacityPanel, BoxLayout.Y_AXIS);
            _opacityPanel.setLayout(lp3);
            _opacityPanel.setBorder(new TitledBorder("Opacity"));
            _opacityPanel.add(_valueTrans);
            _opacityPanel.add(_slider);

            _scalePanel = new JPanel(new GridLayout(4, 1));
            _scalePanel.setBorder(new TitledBorder("Scale"));
            _scalePanel.add(new JLabel("  Horizontal Scale Multiplier:  "));
            _scalePanel.add(_valueHscale);
            _vscaleLabel = new JLabel("  Vertical Scale Multiplier:  ");
            _scalePanel.add(_vscaleLabel);
            _scalePanel.add(_valueVscale);

            _layerPanel = new JPanel(new GridLayout(2, 2));
            _layerPanel.setBorder(new TitledBorder("Layer"));

            _layerPanel.add(new JLabel("  Layer Name"));
            _layerPanel.add(_nameField);
            _layerPanel.add(new JLabel("  Polarity"));
            _layerPanel.add(_valuePolarity);

            _leftBox = new Box(BoxLayout.Y_AXIS);
            _leftBox.add(Box.createVerticalStrut(10));
            _leftBox.add(_layerPanel);
            _leftBox.add(Box.createVerticalStrut(10));
            _leftBox.add(_scalePanel);
            _leftBox.add(Box.createVerticalStrut(10));
            _leftBox.add(_opacityPanel);
            _leftBox.add(Box.createVerticalGlue());
            add(_leftBox);
        }
    }

    private class MyColormapButtonListener implements ActionListener {
        private SeismicWorkflow _pl = null;
        private ColorBarDialog _editor = null;

        public MyColormapButtonListener(SeismicWorkflow pl) {
            super();
            _pl = pl;
            _editor = new ColorBarDialog();
            _editor.addColorChangeListener(new ColorEditorListener(_pl));
        }
        public void actionPerformed(ActionEvent e) {
            SeismicColorMap cmap = _pl.getTraceRasterizer().getColorMap();
            Color[] ramp = new Color[cmap.getDensityColorSize()];

            for (int i=0; i<cmap.getDensityColorSize(); i++) {
                ramp[i] = cmap.getColor(i);
            }
            _editor.getColorBar().setColorArray(ramp);
            _editor.setVisible(true);

        }
    }

    class ColorEditorListener implements ColorChangeListener{
        private SeismicWorkflow _pl = null;
        public ColorEditorListener(SeismicWorkflow pl){
            _pl = pl;

        }
        public void colormapChanged(ColorChangedEvent event) {
            Color[] ramp = event.getColorBar().getColorArray();
            SeismicColorMap cmap = _pl.getTraceRasterizer().getColorMap();
            cmap.setSize(ramp.length+SeismicColorMap.DEFAULT_WIGGLE_COLOR_SIZE);
            int rsize = cmap.getDensityColorSize();

            for (int i=0; i<rsize; i++) {
                if (i == rsize-1) {
                    cmap.setColor(i, ramp[i], true);
                }
                else {
                    cmap.setColor(i, ramp[i], false);
                }

            }
            if (_layer!=null)
            _layer.getBhpWindow().getBhpPlot().updateColorMap(ramp);
        }

    }
}
