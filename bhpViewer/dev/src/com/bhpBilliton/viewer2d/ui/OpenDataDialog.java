/*
 * OpenDataDialog.java
 *
 * Created on December 12, 2007, 5:14 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpBilliton.viewer2d.ui;

import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpPlotMV;
import com.bhpBilliton.viewer2d.BhpPlotXV;
import com.bhpBilliton.viewer2d.BhpSeismicLayer;
import com.bhpBilliton.viewer2d.BhpSeismicTableModel;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.BhpWindow;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * Extracted from NewLayerAction.  Refactored to begin the process of consolidating common code
 * from Swing-initiated layer loading, drag-and-drop layer loading, programmatic layer loading etc.
 *
 * @author folsw9
 */
public class OpenDataDialog extends JDialog {
    private OpenDataPanel cpanel;
    private BhpViewerBase _viewer;
    private BhpLayer _layer;
    
    public OpenDataDialog(BhpViewerBase v, BhpLayer l, boolean isMap) {
        super();
        _viewer = v;
        _layer = l;
        setTitle("Open Dataset");
        setModal(true);
        setLocation(_viewer.getMyLocation());
        setupGUI(v, l, isMap);
    }
    
    public boolean acceptTableChange() {
        return cpanel.acceptTableChange();
    }
    
    public OpenDataPanel getOpenPanel() {
        return cpanel;
    }
    
    private void setupGUI(Container parent, BhpLayer layer, boolean isMap) {
        cpanel = new OpenDataPanel(parent, layer, isMap);
        JPanel tpanel = new JPanel(new FlowLayout());
        JButton ob = new JButton("Ok");
        JButton cb = new JButton("Cancel");
        ob.addActionListener(new OkButtonListener());
        cb.addActionListener(new CancelButtonListener());
        tpanel.add(ob);
        tpanel.add(cb);
        
        this.getContentPane().add(tpanel, BorderLayout.SOUTH);
        this.getContentPane().add(cpanel, BorderLayout.CENTER);
    }
    
    private class OkButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            boolean valid = acceptTableChange();
            if (valid == false) return;
            
            BhpWindow win = (BhpWindow)_viewer.getDesktop().getSelectedFrame();
            OpenDataPanel opanel = getOpenPanel();
            OpenGeneralPanel ogpanel = opanel.getDataPanel();
            BhpSeismicTableModel parameter = ogpanel.getTableModelForNewLayer();
            boolean continueLoad = true;
            if (!(ogpanel instanceof OpenEventPanel) &&
                    (win.getWindowType()==BhpViewer.WINDOW_TYPE_MAPVIEWER)) /* QIW disable log data&&
                !(ogpanel instanceof com.bhpBilliton.viewer2d.dataAdapter.las.OpenLogPanel))*/
                continueLoad = parameter.checkMaxTraceNumber(_viewer);
            if (!continueLoad) return;
            
            setVisible(false);
            _viewer.repaint();
            
            String propertyName = "";
            boolean propertySync = false;
            boolean layerSync = false;
            String[] selectedEvents = null;
            int dataType = BhpViewer.BHP_DATA_TYPE_SEISMIC;
            if (ogpanel instanceof OpenModelPanel) {
                dataType = BhpViewer.BHP_DATA_TYPE_MODEL;
                layerSync = ((OpenModelPanel)ogpanel).getLayerSyncValue();
                propertySync = ((OpenModelPanel)ogpanel).getPropertySyncValue();
                propertyName = ((OpenModelPanel)ogpanel).getSelectedPropertyName();
                if ((win.getBhpPlot() instanceof BhpPlotMV) &&
                        (((OpenModelPanel)ogpanel).getSelectedLayerName() != null))
                    propertyName = propertyName + ":" +
                            ((OpenModelPanel)ogpanel).getSelectedLayerName();
            } else if (ogpanel instanceof OpenEventPanel) {
                dataType = BhpViewer.BHP_DATA_TYPE_EVENT;
                propertySync = ((OpenEventPanel)ogpanel).getPropertySyncValue();
                selectedEvents = ((OpenEventPanel)ogpanel).getSelectedEventNames();
                propertyName = ((OpenEventPanel)ogpanel).getSelectedEventName();
            }
            if (win.getBhpPlot() instanceof BhpPlotXV) {
                if (dataType == BhpViewer.BHP_DATA_TYPE_EVENT && selectedEvents != null) {
                    if (!(opanel.getDataSource().equals(com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_BHPSU))) {
                        // non bhpsu data, must have a seismic layer as reference
                        if (_layer == null || !(_layer instanceof BhpSeismicLayer)) {
                            JOptionPane.showMessageDialog(_viewer,"Select Seismic Layer First!");
                            dispose();
                            return;
                        }
                    }
                    for (int i=0; i<selectedEvents.length; i++) {
                        BhpSeismicTableModel theParameter = parameter;
                        if (i != 0)
                            theParameter = (BhpSeismicTableModel) parameter.clone();
                        win.createXVLayer(_viewer, opanel.getDataSource(),
                                opanel.getDatasetName(), opanel.getPathlist(),
                                selectedEvents[i], propertySync,
                                theParameter, _layer, dataType,
                                ((OpenEventPanel)ogpanel).getSampleEventShape());
                    }
                } else {
                    win.createXVLayer(_viewer, opanel.getDataSource(),
                            opanel.getDatasetName(), opanel.getPathlist(),
                            propertyName, propertySync,
                            parameter,
                            _layer, dataType, null);
                }
            } else {
                win.createBhpMapLayer(_viewer, opanel.getDataSource(),
                        opanel.getDatasetName(), opanel.getPathlist(),
                        propertyName, propertySync, layerSync,
                        parameter, _layer, dataType);
            }
            dispose();
        }
    }
    
    private class CancelButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            setVisible(false);
            dispose();
        }
    }
}