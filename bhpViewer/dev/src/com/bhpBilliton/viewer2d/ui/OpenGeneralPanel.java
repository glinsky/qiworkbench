/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpSeismicTableModel;
import com.bhpBilliton.viewer2d.MissingDataPanel;
import com.bhpBilliton.viewer2d.data.GeneralDataSource;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This abstract panel uses a table and a
 *               <code>{@link VerticalRangePanel}</code>
 *               to describe the basic information of the selected
 *               data set. It enables the user to consturct the
 *               bhpread query string as desired.
 *               The JTable it contained uses an instance of
 *               <code>{@link BhpSeismicTableModel}</code> as its model.
 *               There are subclasses of this class for different data types.
 *               Those suclasses may contain extra components. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public abstract class OpenGeneralPanel extends JPanel {
    protected JTable _table;
    protected BhpSeismicTableModel _tableModel;

    protected MissingDataPanel _missingDataPanel;
    protected VerticalRangePanel _verticalPanel;


    protected BhpLayer _refLayer;

    protected JComboBox _orderEditor;

    /**
     * Construct an OpenGeneralPanel with a reference layer.
     */
    public OpenGeneralPanel(BhpLayer refLayer) {
        super(new BorderLayout());
        _refLayer = refLayer;
    }

    /**
     * Retrieves the table model, which will be used to contruct a new
     * <code>{@link BhpLayer}</code>.
     */
    public BhpSeismicTableModel getTableModelForNewLayer() {
        _verticalPanel.applyVertical(_tableModel);
        return _tableModel;
    }

    /**
     * Check if the current setting of the panel
     * is a valid map setting. This method calls
     * <code>{@link BhpSeismicTableModel}</code>.isValidMapSetting().
     */
    public boolean isValidMapSetting() {
        return _tableModel.isValidMapSetting();
    }

    /**
     *  j2se1.4.0_01 tries to fix the problem of table focus. Instead of fixing
     *  the problem, it introduces new bug. Please refer to bug 4503845 and bug
     *  4330950 for more detail.
     *  This method here tries to edit a cell artifically before any user inter-
     *  action happens. It actually depends on of the bug feature to make the
     *  later editing accessible in acceptTableChange.
     *  In the future, if the dependent bug feature is gone, another fix may
     *  be needed. It's a jack anyway.
     */
    public void hackForJ2se14() {
        boolean editre = _table.editCellAt(0,2);
        _table.editingCanceled(null);
    }

    /**
     * This method takes care of the ongoing editing and commit it.
     * @return a boolean value indicates if set value succeeded.
     *         false if an invalid value is found.
     */
    public boolean acceptTableChange() {
        //Set whether dead traces will be displayed depending on which radio button
        //was selected.
        setMissingDataOption();
        
        boolean revalue = checkValidVerticalPanel();

        TableCellEditor editor = _table.getCellEditor();
        if (editor == null) return revalue;

        int row = _table.getEditingRow();
        int col = _table.getEditingColumn();
        String oldV = _table.getValueAt(row, col).toString().trim();

        String psbV = null;
        // editor.getCellEditorValue may return null for some implementation
        // such as JTable.NumberEditor. It keeps its own value to return.
        if (editor.getCellEditorValue() != null)
            psbV = editor.getCellEditorValue().toString();
        if (psbV != null) psbV = psbV.trim();

        editor.stopCellEditing();
        String newV = _table.getValueAt(row, col).toString().trim();

        if (psbV != null) {
            if (oldV.equals(psbV)) return revalue;
            else if (newV.equals(psbV)) return revalue;
            else return false;
        }
        return revalue;
    }

    /**
     * build the panel. Implementation of the method should
     * set up its own customized GUI.
     */
    protected abstract void buildPanel();
    /**
     * Initialize the panel with the
     * <code>{@link BhpLayer}</code>. <br>
     * Implementation of this method will retrieve values from
     * the layer to fill the fields in its GUI.
     */
    public abstract void initWithLayer(BhpLayer alayer);
    /**
     * apply the changes to the
     * <code>{@link BhpLayer}</code>. <br>
     * Implementation of this method will retrieve values from
     * its GUI and apply it to the layer.
     * @return a flag indicating what has been changed.
     */
    public abstract int applyToLayer(BhpLayer alayer);
    /**
     * Initialize the panel with the
     * <code>{@link GeneralDataSource}</code>. <br>
     * Implementation of this method will retrieve values from
     * the data source to fill the fields in its GUI.
     */
    public abstract void describeData(GeneralDataSource dataSource);

    /**
     * Set up the GUI for the panel.
     */
    protected void setupGUI(boolean grayFirstRow) {
        _missingDataPanel = new MissingDataPanel();
        _verticalPanel = new VerticalRangePanel();
        _table = new GrayLastRowTable(grayFirstRow);
        _table.setGridColor(new Color(205, 205, 205));
        buildPanel();
        _orderEditor = new JComboBox();

        _table.getColumnModel().getColumn(1).setPreferredWidth(130);
        _table.getColumnModel().getColumn(2).setPreferredWidth(90);
        _table.setRowHeight(20);
        
        MyCellEditor editor = new MyCellEditor(new JTextField());
        String string = "string";
        Integer integer = new Integer(0);
        _table.setDefaultEditor(string.getClass(), editor);
        _table.setDefaultEditor(integer.getClass(), editor);

        _table.setRowSelectionAllowed(false);
    }

    private void setMissingDataOption() {
        _missingDataPanel.applyMissingData(_tableModel);
    }
    
    private boolean checkValidVerticalPanel() {
        boolean re = true;
        if (!_verticalPanel.isShowing()) return re;

        re = _verticalPanel.applyVertical(_tableModel);
        return re;
    }

    private class MyCellEditor extends DefaultCellEditor {
        public MyCellEditor(JTextField t) {
            super(t);
        }

        public Component getTableCellEditorComponent(JTable table, Object value,
                            boolean isSelected, int row, int column) {
            ((JTextField)getComponent()).setText(value.toString());
            ((JTextField)getComponent()).selectAll();
            return getComponent();
        }
    }

    private class GrayLastRowStringRenderer extends DefaultTableCellRenderer {
        public Component getTableCellRendererComponent(JTable table, Object value,
                         boolean isSelected, boolean hasFocus, int row, int column) {
            Color backgray = new Color(205, 205, 205);
            setBackground(backgray);
            setForeground(backgray);
            value = null;
            return super.getTableCellRendererComponent(table, value, isSelected,
                                                        hasFocus, row, column);
        }
    }

    private class GrayFirstRowStringRenderer extends DefaultTableCellRenderer {
        public Component getTableCellRendererComponent(JTable table, Object value,
                         boolean isSelected, boolean hasFocus, int row, int column) {
            setBackground(Color.lightGray);
            setForeground(Color.gray);
            if (column == 3 || column == 5 || column == 7 || column ==8)
                value = null;
            return super.getTableCellRendererComponent(table, value, isSelected,
                                                        hasFocus, row, column);
        }
    }

    private class GrayLastRowTable extends JTable {
        private GrayLastRowStringRenderer __grayLastRenderer;
        private GrayFirstRowStringRenderer __grayFirstRenderer;
        private boolean __grayFirstRow;

        public GrayLastRowTable(boolean grayFirstRow) {
            super();
            __grayFirstRow = grayFirstRow;
            __grayLastRenderer = new GrayLastRowStringRenderer();
            __grayFirstRenderer = new GrayFirstRowStringRenderer();
        }

        public TableCellRenderer getCellRenderer(int row, int column) {
            if (row == this.getRowCount()-1) {
                return __grayLastRenderer;
            }
            if (__grayFirstRow && row==0) {
                return __grayFirstRenderer;
            }
            return super.getCellRenderer(row, column);
        }
    }
}
