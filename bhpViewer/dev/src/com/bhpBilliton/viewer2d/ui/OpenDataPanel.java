/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import javax.swing.border.EtchedBorder;

import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpMapLayer;
import com.bhpBilliton.viewer2d.BhpPropertyManager;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.data.*;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Window;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This panel displays all the data related entities.
 *               It has two text fields for and selected dataset name
 *               and pathlist, specifying where the data set can be found.
 *               A label is shown for the attribute of the selected dataset,
 *               including data type and order. Currently, there are 3
 *               data types. They are seismic data, model data, and event data.
 *               There are two data order, cross-section order, and
 *               map-view order.
 *               The panel has a button which can pop an
 *               <code>{@link AbstractDataChooser}</code> that
 *               enables the user to choose a dataset. It also contains a
 *               <code>{@link OpenGeneralPanel}</code>
 *               which describes the data about its headers
 *               and other important information once a dataset is selected.
 *               If OpenSpirit data is available, button "OpenSpirit Dataset..."
 *               will be enabled and user can use it to select OpenSpirit
 *               data.
 *               <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class OpenDataPanel extends JPanel {
    private static final Logger logger = Logger.getLogger(OpenDataPanel.class.toString());
    
    private Container _parent;
    private boolean _isMap;
    private JTextField _nameField;
    private JTextField _pathlistField;
    private JLabel _attLabel;
    private JPanel _centerPanel;
    private OpenGeneralPanel _dataPanel;

    private String _dataSource;
    private GeneralDataChooser _dataChooser;

    private BhpLayer _refLayer;

    /**
     * Construct a new OpenDataPanel.
     * @param parent the parent frame of the panel.
     * @param refLayer the reference layer. <br>
     *        The new layer will inherit its attribute from it.
     * @param isMap indicates if this panel is used for map layer creation
     *        or cross-section layer construction.
     */
    // Change Frame to Container
    public OpenDataPanel(Container parent, BhpLayer refLayer, boolean isMap) {
        super(new BorderLayout());
        _parent = parent;
        _refLayer = refLayer;
        _isMap = isMap;

        _dataSource = "";
        _dataChooser = null;

        BhpViewerBase theViewer = (BhpViewerBase)_parent;

        _dataPanel = null;
        setupGUI();
    }

    /**
     * Retrieves the type of the selected data source. <br>
     * Currently, its possible value are: <br>
     * bhpsu, OpenSpirit. <br>
     * However, user can always define their own. If a new data
     * adapter is available to the program, its name should be
     * included in the property "dataAdapter" of
     * <code>{@link BhpPropertyManager}</code>. There also
     * should have a property for the class name of the data chooser,
     * which implments the interface
     * <code>{@link GeneralDataChooser}</code>.
     */
    public String getDataSource() {
        return _dataSource;
    }

    /**
     * Retrieves the name of the selected dataset.
     */
    public String getDatasetName() {
        return _nameField.getText();
    }

    /**
     * Retrieves the pathlist of the selected dataset.
     */
    public String getPathlist() {
        return _pathlistField.getText();
    }

    /**
     * Retrieves the instance of <code>{@link OpenGeneralPanel}</code>
     * included in this panel
     */
    public OpenGeneralPanel getDataPanel() {
        return _dataPanel;
    }

    /**
     * Check if the current setting of the panel
     * is a valid map setting. This method
     * calls <code>{@link OpenGeneralPanel}</code>.isValidMapSetting().
     */
    public boolean isValidMapSetting() {
        if (_dataPanel == null) return false;
        return _dataPanel.isValidMapSetting();
    }

    /**
     * This method takes care of the ongoing editing and commit it.
     * @return a boolean value indicates if set value succeeded.
     *         false if an invalid value is found or the panel is abnormal.
     */
    public boolean acceptTableChange() {
        if (_dataPanel == null) return false;
        return _dataPanel.acceptTableChange();
    }

    public void describeData(GeneralDataSource dataSource) {
        describeData(dataSource, true);
    }

    /**
     * Initialize the panel with the
     * <code>{@link GeneralDataSource}</code>.
     */
    public void describeData(GeneralDataSource dataSource, boolean showDialog) {
        _nameField.setText(dataSource.getDataName());
        _pathlistField.setText(dataSource.getDataPath());
        _attLabel.setText(dataSource.getDataAtt());
        _centerPanel.removeAll();
        _dataPanel = null;

        int dataType = dataSource.getDataType();
        int dataOrder = dataSource.getDataOrder();

        if ((!_isMap && dataOrder == BhpViewer.BHP_DATA_ORDER_CROSSSECTION) ||
            (_isMap && dataOrder == BhpViewer.BHP_DATA_ORDER_MAPVIEW)) {
            // data order matches the viewer type
            if (dataType == BhpViewer.BHP_DATA_TYPE_SEISMIC) {
                _dataPanel = new OpenSeismicPanel((BhpViewerBase)_parent, _refLayer);
            }
            else if (dataType == BhpViewer.BHP_DATA_TYPE_MODEL) {
                _dataPanel = new OpenModelPanel((BhpViewerBase)_parent, _refLayer, dataOrder);
            }
            else if (dataType == BhpViewer.BHP_DATA_TYPE_EVENT) {
                _dataPanel = new OpenEventPanel((BhpViewerBase)_parent, _refLayer, dataOrder);
            }
        }
        else _dataPanel = new OpenSeismicPanel((BhpViewerBase)_parent, _refLayer);

        if (_dataPanel == null) return;
        _dataPanel.describeData(dataSource);
        if (_isMap && (_dataPanel instanceof OpenSeismicPanel)) {
            // set default value of the 1st key
            try {
                String keyName = _dataPanel._tableModel.getValueAt(0, 0).toString();
                double values[] = _dataPanel._tableModel.getKeySettingsOfName(keyName);
                _dataPanel._tableModel.setChosenRangeAt(""+values[0], 0);
            }
            catch (Exception ex) {
                logger.fine("OpenDataPanel.describeData() caught Exception: " + ex.getMessage());
            }
        }
        _centerPanel.add(_dataPanel, BorderLayout.CENTER);
        this.validate();

        _dataPanel.hackForJ2se14();

        if (showDialog) {
            Window window = SwingUtilities.windowForComponent(this);
            if (window instanceof JDialog) ((JDialog)window).setVisible(true);
        }
    }

    /**
     * Initialize the panel with the
     * <code>{@link BhpLayer}</code>
     */
    public void initWithLayer(BhpLayer alayer) {
        _nameField.setText(alayer.getDataName());
        _pathlistField.setText(alayer.getPathlist());
        _centerPanel.removeAll();
        _dataPanel = null;

        _attLabel.setText(alayer.getParameter().getDataAttributeString());
        int dataType = alayer.getDataType();
        int dataOrder = BhpViewer.BHP_DATA_ORDER_CROSSSECTION;
        if (alayer instanceof BhpMapLayer) dataOrder = BhpViewer.BHP_DATA_ORDER_MAPVIEW;
        if (dataType == BhpViewer.BHP_DATA_TYPE_SEISMIC)
            _dataPanel = new OpenSeismicPanel((BhpViewerBase)_parent, null);
        else if (dataType == BhpViewer.BHP_DATA_TYPE_MODEL)
            _dataPanel = new OpenModelPanel((BhpViewerBase)_parent, null, dataOrder);
        else if (dataType == BhpViewer.BHP_DATA_TYPE_EVENT)
            _dataPanel = new OpenEventPanel((BhpViewerBase)_parent, null, dataOrder);
        
        if (_dataPanel != null) {
            _dataPanel.initWithLayer(alayer);
            _centerPanel.add(_dataPanel, BorderLayout.CENTER);
        }
        this.validate();

        _dataPanel.hackForJ2se14();
    }

    /**
     * apply the changes to the
     * <code>{@link BhpLayer}</code>.
     * @return a flag indicating what has been changed.
     */
    public int applyToLayer(BhpLayer alayer) {
        int changeFlag = 0;
        if (_nameField.getText().equals(alayer.getDataName()) == false) {
            changeFlag = changeFlag | BhpViewer.NAME_FLAG;
            alayer.setDataName(_nameField.getText());
        }
        if (_pathlistField.getText().equals(alayer.getPathlist()) == false) {
            changeFlag = changeFlag | BhpViewer.NAME_FLAG;
            alayer.setPathlist(_pathlistField.getText());
        }

        if (_dataPanel != null) {
            changeFlag = changeFlag | _dataPanel.applyToLayer(alayer);
        }
        return changeFlag;
    }

    public void showChooser(String selectedState) {
        //String selectedState = _dataSourceCombo.getSelectedItem().toString().trim();
        if (selectedState.equals(_dataSource) && _dataChooser!=null) {
            _dataChooser.setDataChooserVisible(true);
        }
        else {
            _dataChooser = BhpDataAdapterFactory.createDataChooser(
                            ((BhpViewerBase)_parent), this, selectedState);
            if (_dataChooser != null) {
                _dataSource = selectedState;
                _dataChooser.setDataChooserVisible(true);
            }
            else {
                _dataSource = "";
                ErrorDialog.showInternalErrorDialog((BhpViewerBase) (_parent), Thread.currentThread().getStackTrace(),
                                   "OpenDataPanel.showChooser error: Cannot find the data chooser for "
                                   + selectedState);
            }
        }
    }

    private void setupGUI() {
        JPanel top = buildTopPanel();

        JPanel attPanel = new JPanel(new FlowLayout());
        _attLabel = new JLabel("");
        attPanel.add(_attLabel);

        _centerPanel = new JPanel();
        _centerPanel.setLayout(new BorderLayout());
        _centerPanel.setBorder(new EtchedBorder());

        Box box = new Box(BoxLayout.Y_AXIS);
        box.add(top);
        box.add(Box.createVerticalStrut(10));
        box.add(attPanel);
        box.add(Box.createVerticalStrut(10));
        this.add(box, BorderLayout.NORTH);
        this.add(_centerPanel, BorderLayout.CENTER);
    }

    private JPanel buildTopPanel() {
        Box box1 = new Box(BoxLayout.X_AXIS);
        Box box2 = new Box(BoxLayout.X_AXIS);
        box1.add(Box.createHorizontalStrut(20));
        box1.add(new JLabel("Data Name    : "));
        _nameField = new JTextField("");
        _nameField.setEditable(false);
        box1.add(_nameField);
        box1.add(Box.createHorizontalStrut(20));
        box2.add(Box.createHorizontalStrut(20));
        box2.add(new JLabel("Data File      : "));
        _pathlistField = new JTextField("");
        _pathlistField.setEditable(false);
        box2.add(_pathlistField);
        box2.add(Box.createHorizontalStrut(20));

        JPanel pp = new JPanel(new GridLayout(2, 1));
        pp.add(new JLabel(""));
        pp.add(box2);
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(pp, BorderLayout.CENTER);
        return panel;
    }
}
