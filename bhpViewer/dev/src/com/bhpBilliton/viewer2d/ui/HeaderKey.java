/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui;

/**
 * @author lukem
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class HeaderKey {
	private String name;
	private double minValue;
	private double maxValue;
	private double increment;
	private double scalar;
	private double type;
	private String datatype;
	private double vlen;
	private double vloc;
	private double num;
	private double order;


	public String getDatatype() {
		return datatype;
	}
	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}
	public double getNum() {
		return num;
	}
	public void setNum(double num) {
		this.num = num;
	}
	public double getOrder() {
		return order;
	}
	public void setOrder(double order) {
		this.order = order;
	}
	public double getType() {
		return type;
	}
	public void setType(double type) {
		this.type = type;
	}
	public double getVlen() {
		return vlen;
	}
	public void setVlen(double vlen) {
		this.vlen = vlen;
	}
	public double getVloc() {
		return vloc;
	}
	public void setVloc(double vloc) {
		this.vloc = vloc;
	}
	/**
	 * @param name
	 * @param minValue
	 * @param maxValue
	 * @param increment
	 * @param scalar
	 */
	public HeaderKey(String name, double minValue, double maxValue, double increment, double scalar) {
		super();
		this.name = name;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.increment = increment;
		this.scalar = scalar;
	}

	public HeaderKey(String name, double minValue, double maxValue, double increment) {
		this(name,minValue,maxValue, increment, 1);
	}
	public HeaderKey (String name, double minValue, double maxValue) {
		this(name, minValue, maxValue, 1);
	}
	public HeaderKey (String name) {
		this(name, 1, 1);
	}
	public HeaderKey () {
		this ("");
	}
	public double getIncrement() {
		return increment;
	}
	public void setIncrement(double increment) {
		this.increment = increment;
	}
	public double getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(double maxValue) {
		this.maxValue = maxValue;
	}
	public double getMinValue() {
		return minValue;
	}
	public void setMinValue(double minValue) {
		this.minValue = minValue;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getScalar() {
		return scalar;
	}
	public void setScalar(double scalar) {
		this.scalar = scalar;
	}
	public boolean isValid() {
		//valid keys have a name... at least
		if (name.length()>0) {
			return true;
		}
		return false;
	}
	public boolean setValue (String keyName, String keyValue) {
		boolean returnValue = true;
		if (keyName.equalsIgnoreCase("HDRNAME")) {
			setName(keyValue);
		} else {
			double newValue;
			try {
				newValue = Double.parseDouble (keyValue);
			} catch (NumberFormatException e) {
//				System.out.println ("HeaderKey.setValue: invalid value ["+keyName+"] ("+keyValue+")");
				newValue = -1;
			}
			if (keyName.equalsIgnoreCase("MIN")) {
				setMinValue(newValue);
			} else if (keyName.equalsIgnoreCase("MAX")) {
				setMaxValue(newValue);
			} else if (keyName.equalsIgnoreCase("INCR")) {
				setIncrement(newValue);
			} else if (keyName.equalsIgnoreCase("SCALAR")) {
				setScalar(newValue);
			} else if (keyName.equalsIgnoreCase("ORDER")) {
				setOrder(newValue);
			} else if (keyName.equalsIgnoreCase("TYPE")) {
				setType(newValue);
			} else if (keyName.equalsIgnoreCase("VLEN")) {
				setVlen(newValue);
			} else if (keyName.equalsIgnoreCase("VLOC")) {
				setVloc(newValue);
			} else if (keyName.equalsIgnoreCase("NUM")) {
				setNum(newValue);
			} else if (keyName.equalsIgnoreCase("DATATYPE")) {
				setDatatype(keyValue);
			} else {
//				System.out.println ("HeaderKey.setValue: invalid key ["+keyName+"] ("+keyValue+")");
				returnValue = false;
			}
		}
		return (returnValue);
	}
}
