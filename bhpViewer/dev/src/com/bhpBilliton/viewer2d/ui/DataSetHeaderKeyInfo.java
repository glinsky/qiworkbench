/*
 * DataSetHeaderKeyInfo.java
 *
 * Created on December 13, 2007, 12:30 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bhpBilliton.viewer2d.ui;

/*
 ###########################################################################
 # WaveletDecomp - A continuous time-frequency analysis technique that  
 # computes frequency spectrum for each time sample of a seismic trace.
 # Copyright (C) 2006  BHP Billiton Petroleum; BHP Billiton Confidential
 *
 * Copied from WaveletDecomp class of the same name pending analysis of moving
 * this class into CompAPI.
 ############################################################################
*/

public class DataSetHeaderKeyInfo {
	private String name;
	private double min;
	private double max;
	private double incr;
	
	public DataSetHeaderKeyInfo(String name, double min, double max, double incr){
		this.name = name;
		this.min = min;
		this.max = max;
		this.incr = incr;
	}
	public String getName(){
		return name;
	}
	public double getMax(){
		return max;
	}
	public double getMin(){
		return min;
	}
	public double getIncr(){
		return incr;
	}
	
	public String toString(){
		return "Name:" + name + " Min:" + min + " Max:" + max + " Incr:" + incr;
	}
}
