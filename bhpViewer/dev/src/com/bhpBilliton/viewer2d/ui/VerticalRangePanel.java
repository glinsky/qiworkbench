/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import javax.swing.JOptionPane;
import javax.swing.border.TitledBorder;

import com.bhpBilliton.viewer2d.Bhp2DviewerConstants;
import com.bhpBilliton.viewer2d.BhpSeismicTableModel;
import java.awt.Color;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This panel is for viewing and changing the setting of the
 *               vertical range, the lease significant header key. <br><br>
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class VerticalRangePanel extends JPanel {
    private JTextField _avaiRange;
    private JTextField _minField;
    private JTextField _maxField;
    private JTextField _incField;
    private JCheckBox _synCheckbox;
    private double _defaultStep;

    /**
     * Constructs a new instance and creates the GUI components.
     */
    public VerticalRangePanel() {
        super();
        _defaultStep = 0;
        this.setBorder(new TitledBorder("Vertical Range"));
        
        _avaiRange = new JTextField("");
        _avaiRange.setEditable(false);
        _avaiRange.setBackground(Color.lightGray);
        _minField = new JTextField("");
        _maxField = new JTextField("");
        _incField = new JTextField("");
        _synCheckbox = new JCheckBox("  Synchronize");

        Box b1 = new Box(BoxLayout.Y_AXIS);
        b1.add(new JLabel("  Available Range"));
        b1.add(_avaiRange);

        Box b2 = new Box(BoxLayout.Y_AXIS);
        b2.add(new JLabel("    Minimum"));
        b2.add(_minField);

        Box b3 = new Box(BoxLayout.Y_AXIS);
        b3.add(new JLabel("    Maximum"));
        b3.add(_maxField);

        Box b4 = new Box(BoxLayout.Y_AXIS);
        b4.add(new JLabel("    Step"));
        b4.add(_incField);

        this.add(b1);
        this.add(Box.createHorizontalStrut(10));
        this.add(b2);
        this.add(b3);
        this.add(b4);
        this.add(Box.createHorizontalStrut(10));
        this.add(_synCheckbox);
    }

    /**
     * Retrieves the related information from
     * the table model and fill the fields of the GUI.
     * @param model the table model where values will be retrieved.
     * @param dsType the data source type
     */
    public void initVertical(BhpSeismicTableModel model, String dsType) {
        int row = model.getRowCount() - 1;
        String fieldName;
        String avaiRangeString;
        String range;
        _defaultStep = 0;
        if (row>0) {
            fieldName = model.getValueAt(row, 0).toString();
            avaiRangeString = model.getValueAt(row, 1).toString();
            range = model.getValueAt(row, 2).toString();
            _synCheckbox.setSelected(Boolean.valueOf(model.getValueAt(row, 6).toString()).booleanValue());
            try {
                _defaultStep = Double.parseDouble(avaiRangeString.substring(avaiRangeString.indexOf("[")+1, avaiRangeString.indexOf("]")).trim());
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null,
                            "VerticalRangePanel.initVertical exception : cannot get integer default step value",
                            "Internal Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            fieldName = "N/A";
            avaiRangeString = "N/A";
            range = "0";
            _synCheckbox.setSelected(false);
        }
        this.setBorder(new TitledBorder("Vertical Range (" + fieldName + ")"));
        _avaiRange.setText(avaiRangeString);

        int rangeIndex = BhpSeismicTableModel.rangeIndex(range);
        if (rangeIndex < 0) {
            range = _avaiRange.getText();
            rangeIndex = BhpSeismicTableModel.rangeIndex(range);
            if (rangeIndex < 0) {
                JOptionPane.showMessageDialog(null,
                            "VerticalRangePanel.initPanel error range " + range,
                            "Internal Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        int endIndex = range.indexOf("[");
        _minField.setText(range.substring(0, rangeIndex).trim());
        if (endIndex < 0) {
            _maxField.setText(range.substring(rangeIndex+1).trim());
            _incField.setText("" + _defaultStep);
        }
        else {
            _maxField.setText(range.substring(rangeIndex+1, endIndex).trim());
            _incField.setText(range.substring(endIndex+1, range.indexOf("]")).trim());
        }
    }

    /**
     * Retrieves valus from the GUI components and
     * updates the corresponding values in the table model.
     * @param model the table model that will be updated.
     * @return a flag indicates if all the fields are applied succefully.
     *         If any invalid value is found, false is returned.
     */
    public boolean applyVertical(BhpSeismicTableModel model) {
        boolean re = true;
        int row = model.getRowCount() - 1;
        if (_synCheckbox.isSelected()) model.setValueAt(new Boolean(true), row, 6);
        else model.setValueAt(new Boolean(false), row, 6);

        double newStep = _defaultStep;
        if (_incField.getText().length() > 0) {
            try {
                newStep = Double.parseDouble(_incField.getText().trim());
                if (newStep < _defaultStep) {
                    newStep = _defaultStep;
                    _incField.setText("" + _defaultStep);
                    re = false;
                    if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
                        System.out.println("VerticalRangePanel.initVertical Warning : step >= defaultStep");
                }
                else if ((newStep % _defaultStep) != 0) {
                    newStep = _defaultStep;
                    _incField.setText("" + _defaultStep);
                    re = false;
                    if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
                        System.out.println("VerticalRangePanel.initVertical Warning : step = defaultStep * integer");
                }
            }
            catch (Exception ex) {
                JOptionPane.showMessageDialog(null,
                            "VerticalRangePanel.applyVertical Exception",
                            "Internal Error", JOptionPane.ERROR_MESSAGE);
                newStep = _defaultStep;
                _incField.setText("" + _defaultStep);
                re = false;
            }
        }

        String value = _minField.getText()+"-"+_maxField.getText();
        if (newStep != 0)
            value = value + "[" + newStep + "]";
        model.setValueAt(value, row, 2);

        return re;
    }
}