/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.bhpBilliton.viewer2d.BhpEventGraphPlot;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.util.ErrorDialog;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This dialog is for viewing and changing the
 *               settings of the event graph plot. <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpEventGraphPlotDialog extends JDialog {
    private BhpEventGraphPlot _eventGraphPlot;

    private JCheckBox _gridCheck;
    private JRadioButton _autoRadioButton;
    private JRadioButton _userRadioButton;
    private JTextField _maxField;
    private JTextField _minField;
    private JTextField _majorStepField;
    private JTextField _minorStepField;
    private JLabel _majorStepLabel;
    private JLabel _minorStepLabel;
    private JLabel _minLabel;
    private JLabel _maxLabel;

    // Save _viewer from constructor
    BhpViewerBase _viewer;

    /**
     * Constructs a new dialog and sets up the GUI.
     */
    // BhpViewerBase no longer a Frame
    public BhpEventGraphPlotDialog(BhpViewerBase viewer) {
      super();
      _viewer = viewer;
      setTitle("Graph Plot Settings");
      setModal(false);
      setLocation(viewer.getMyLocation().x + 10, viewer.getMyLocation().y + 10);
      //super(viewer, "Graph Plot Settings", false);
      _eventGraphPlot = null;
      buildGraphDialogGUI();
    }

    /**
     * Gets related information of the settings
     * and fill the fields of GUI. <br>
     */
    public void initWithEventGraphPlot(BhpEventGraphPlot plot) {
        if (plot == null) return;
        _eventGraphPlot = plot;

        _minField.setText("" + _eventGraphPlot.getMinValue());
        _maxField.setText("" + _eventGraphPlot.getMaxValue());
        _majorStepField.setText("" + _eventGraphPlot.getMajorStep());
        _minorStepField.setText("" + _eventGraphPlot.getMinorStep());
        _gridCheck.setSelected(_eventGraphPlot.getShowGrid());
        boolean enable = true;
        if (_eventGraphPlot.getVerticalAuto()) {
            _autoRadioButton.setSelected(true);
            _userRadioButton.setSelected(false);
            enable = false;
        }
        else {
            _autoRadioButton.setSelected(false);
            _userRadioButton.setSelected(true);
            enable = true;
        }
        enableContextSensitiveItems(enable);
    }

    /**
     * Retrieves valus from the GUI components and
     * updates the settings.
     */
    private void applyPlotSettings() {
        if (_eventGraphPlot == null) return;
        _eventGraphPlot.setShowGrid(_gridCheck.isSelected());
        boolean verticalAuto = _autoRadioButton.isSelected();
        _eventGraphPlot.setVerticalAuto(verticalAuto);
        if (!verticalAuto) {
            try {
                double min = Double.parseDouble(_minField.getText());
                double max = Double.parseDouble(_maxField.getText());
                double major = Double.parseDouble(_majorStepField.getText());
                double minor = Double.parseDouble(_minorStepField.getText());
                _eventGraphPlot.setMinValue(min);
                _eventGraphPlot.setMaxValue(max);
                _eventGraphPlot.setMajorStep(major);
                _eventGraphPlot.setMinorStep(minor);
            }
            catch (Exception ex) {
              String msg = "Error applying values in the graph setting dialog.";
              BhpViewer v = (BhpViewer)(SwingUtilities.windowForComponent(this));
              v.getStatusBar().showMessage(BhpViewer.ERROR_MESSAGE, msg);
              /*BhpViewerBase v = (BhpViewerBase)(SwingUtilities.windowForComponent(this));
              v.getStatusBar().showMessage(BhpStatusBar.ERROR_MESSAGE, msg);*/
              ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                  "BhpEventGraphPlotDialog.applyPlotSettings exception");
            }
        }
        _eventGraphPlot.updateTransformation();
    }

    private void enableContextSensitiveItems(boolean enable) {
        _majorStepLabel.setEnabled(enable);
        _majorStepField.setEnabled(enable);
        _minorStepLabel.setEnabled(enable);
        _minorStepField.setEnabled(enable);
        _minLabel.setEnabled(enable);
        _maxLabel.setEnabled(enable);
        _minField.setEnabled(enable);
        _maxField.setEnabled(enable);
    }

    private void buildGraphDialogGUI() {
        this.getContentPane().add(buildContentPanel(), BorderLayout.CENTER);
        this.getContentPane().add(buildControlPanel(), BorderLayout.SOUTH);
    }

    private JPanel buildContentPanel() {
        _gridCheck = new JCheckBox("Show Grid Lines");
        _minLabel = new JLabel("      Minimum Value");
        _maxLabel = new JLabel("      Maximum Value");
        _majorStepLabel = new JLabel("      Major Step");
        _minorStepLabel = new JLabel("      Minor Step");
        _minField = new JTextField("");
        _maxField = new JTextField("");
        _majorStepField = new JTextField("");
        _minorStepField = new JTextField("");

        _autoRadioButton = new JRadioButton("Automatic");
        _autoRadioButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JRadioButton rb = (JRadioButton) e.getSource();
                boolean enable = true;
                if (rb.isSelected()) enable = false;
                enableContextSensitiveItems(enable);
            }
        });

        _userRadioButton = new JRadioButton("User Defined");
        _userRadioButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JRadioButton rb = (JRadioButton) e.getSource();
                boolean enable = false;
                if (rb.isSelected()) enable = true;
                enableContextSensitiveItems(enable);
            }
        });

        ButtonGroup rbGroup = new ButtonGroup();
        rbGroup.add(_autoRadioButton);
        rbGroup.add(_userRadioButton);

        JPanel panel1 = new JPanel(new GridLayout(6, 2));
        panel1.setBorder(BorderFactory.createTitledBorder("Vertical Axis"));
        panel1.setMaximumSize(new Dimension(300, 70));
        panel1.add(_autoRadioButton);
        panel1.add(new JLabel());
        panel1.add(_userRadioButton);
        panel1.add(new JLabel());
        panel1.add(_minLabel);
        panel1.add(_minField);
        panel1.add(_maxLabel);
        panel1.add(_maxField);
        panel1.add(_majorStepLabel);
        panel1.add(_majorStepField);
        panel1.add(_minorStepLabel);
        panel1.add(_minorStepField);

        JPanel panel2 = new JPanel(new GridLayout(1, 2));
        panel2.setBorder(BorderFactory.createTitledBorder("Grid Setting"));
        panel2.setMaximumSize(new Dimension(300, 30));
        panel2.add(_gridCheck);
        panel2.add(new JLabel(""));

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(Box.createGlue());
        panel.add(panel1);
        panel.add(Box.createGlue());
        panel.add(panel2);
        panel.add(Box.createGlue());

        return panel;
    }

    private JPanel buildControlPanel() {
        JButton okb = new JButton("Ok");
        okb.addActionListener(new OkListener());
        JButton apb = new JButton("Apply");
        apb.addActionListener(new ApplyListener());
        JButton clb = new JButton("Cancel");
        clb.addActionListener(new CancelListener());
        JButton helpb = new JButton("Help");
        helpb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                showHelp();
            }
        });
        JPanel panel = new JPanel(new FlowLayout());
        panel.add(okb);
        panel.add(apb);
        panel.add(clb);
        panel.add(helpb);
        return panel;
    }

    private void showHelp() {
        try {
            BhpViewerBase.broker.setCurrentID("unknown");
            BhpViewerBase.broker.setDisplayed(true);
        }
        catch (Exception e) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                 "Couldn't show horizon graph plot setting dialog help");
        }
    }

    private void hideDialog() {
        this.setVisible(false);
    }

    private class OkListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            applyPlotSettings();
            hideDialog();
        }
    }

    private class ApplyListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            applyPlotSettings();
        }
    }

    private class CancelListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            hideDialog();
        }
    }
}
