/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.FileHandler;

import javax.swing.SwingUtilities;

import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.ui.util.FileTools;
import com.bhpBilliton.viewer2d.ui.util.SelectFileCommand;
import com.bhpBilliton.viewer2d.ui.util.SelectLocalDataSourceFile;
//import com.bhpBilliton.viewer2d.ui.util.SelectRemoteDataSourceFile;


/**
 * @author lukem
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class DataFileSubstitutionDialog {

	public static final int APPROVE_OPTION = 0;

	// Return Values
	public static final int CANCEL_OPTION = 1;

	public static final int ERROR_OPTION = -1;

	private DataFileSubstitutionDialogModel model;

	private DataFileSubstitutionDialogGUI view;

	private String currentPath;

	private int returnValue = ERROR_OPTION;

	private Map filePanels = new HashMap();

  // TODO Implement Local/Remote IO services
  // Disable references to remote IO
	//private boolean remoteFiles = false;

	private BhpViewerBase _viewer;

	public DataFileSubstitutionDialog(BhpViewerBase viewer) {
		_viewer = viewer;
		if (model == null) {
			model = new DataFileSubstitutionDialogModel(viewer);
		}
		if (view == null) {
			view = new DataFileSubstitutionDialogGUI();
		}

		view.setWindowEventAction(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				returnValue = CANCEL_OPTION;
			}
		});

		view.setOKButtonAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.setVisible(false);
				returnValue = APPROVE_OPTION;
			}
		});

		view.setCancelButtonAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.setVisible(false);
				returnValue = CANCEL_OPTION;
			}
		});

	}

  // Disable references to remote IO
	/*public void setRemoteFiles (boolean remote) {
		remoteFiles = remote;
	}*/

	public void setBase (BhpViewerBase v) {
		_viewer=v;
	}

	public SelectFileCommand getSelectFileCommand () {
    // Disable references to remote IO
		/*if (remoteFiles) {
			return new SelectRemoteDataSourceFile(_viewer);
		} else {*/
			return new SelectLocalDataSourceFile();
		//}
	}


	public Container getView () {
		return view;
	}

	public void addHandle(Collection handles) {
		for (Iterator iter = handles.iterator(); iter.hasNext();) {
			this.addHandle((DataLayerHandle) iter.next());
		}
	}

	public void addHandle(DataLayerHandle handle) {
		String id = model.addHandle(handle);
		if (id.length() > 0) {
			String visibleName = handle.getShortPathlist() + "  [" + handle.getDataname() + "]";
			FileSubstitutionPanel fsr = new FileSubstitutionPanel(id, visibleName, handle.getFull(), getSelectFileCommand());
			fsr.addPropertyChangeListener("NewFileName", new FileNameChangeListener());
			fsr.addPropertyChangeListener("NewDataName", new DataNameChangeListener());
			fsr.addPropertyChangeListener("CancelSubstition", new CancelSubstitionListener());
			view.addToContents(fsr);
			filePanels.put( id, fsr);
		}
	}

	public void addHandle(DataLayerHandle handle[]) {
		for (int i = 0; i < handle.length; i++) {
			this.addHandle(handle[i]);
		}
	}

	public String getCurrentPath() {
		return currentPath;
	}

	public Map getFileSubstitutionMap() {
		return (model.getValidSubstitions());
	}

	public void setCurrentPath(String currentPath) {
		this.currentPath = currentPath;
		FileSubstitutionPanel.setCurrentPath(currentPath);
	}

	public int showDialog() {
		view.setVisible(true);
		return returnValue;
	}

	public void pack() {
		view.pack();
	}

	public void setWarningState(String id, boolean state) {
		FileSubstitutionPanel filePanel = (FileSubstitutionPanel) filePanels.get(id);
		filePanel.setWarningState(state);
	}

	private final class CancelSubstitionListener implements PropertyChangeListener {
		public void propertyChange(PropertyChangeEvent evt) {
			String id = (String) evt.getOldValue();
			model.getSubstitutionHandle(id).cancelSubstitution();
			((FileSubstitutionPanel) evt.getSource()).setNameComboList(new String[0]);
			((FileSubstitutionPanel) evt.getSource()).setNameComboEnabled(false);
		}
	}

	private final class DataNameChangeListener implements PropertyChangeListener {
		public void propertyChange(PropertyChangeEvent evt) {
			String id = (String) evt.getOldValue();
			String newDataName = (String) evt.getNewValue();
			DataLayerHandle dlh = model.getNewHandle(id);
			dlh.setName(newDataName);
			dlh.setDataname(newDataName);
			model.setNewHandle(id, dlh);
		}
	}

	private final class FileNameChangeListener implements PropertyChangeListener {
		public void propertyChange(PropertyChangeEvent evt) {
			String id = (String) evt.getOldValue();
			String newFileName = (String) evt.getNewValue();
			DataLayerHandle dataLayerHandle = new DataLayerHandle("", "", newFileName,_viewer);
			String[] choices = dataLayerHandle.getDatasetsFromDat(newFileName,_viewer);
			model.setNewHandle(id, dataLayerHandle);
			FileSubstitutionPanel fileSubstutionPanel = ((FileSubstitutionPanel) evt.getSource());
			fileSubstutionPanel.setNameComboEnabled(false);
			fileSubstutionPanel.setNameComboList(choices);
			fileSubstutionPanel.setNameComboEnabled(true);
			fileSubstutionPanel.setSelectedName(FileTools.removeExtension(dataLayerHandle.getShortPathlist()));
			FileSubstitutionPanel.setCurrentPath(new File(newFileName).getAbsolutePath());
			//select a default entry that matches the selected pathlist
		}
	}
}
