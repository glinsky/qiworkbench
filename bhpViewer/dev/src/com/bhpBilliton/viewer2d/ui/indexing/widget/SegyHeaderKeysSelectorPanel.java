/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui.indexing.widget;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.filechooser.FileFilter;
import java.io.File;
import java.util.Iterator;
import org.w3c.dom.Node;
import java.util.ArrayList;
import javax.swing.border.*;
import java.util.Map;
import java.util.HashMap;

//import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.indexing.CompoundIndexTree;

import java.io.IOException;

/**
 *  A SegyIndexingProcessor which <br>
 *  1) creates the HeaderKeys from the *.xml file selected by the
 *  SegyFormatSelectorPanel <br>
 *  2)displays the CompoundIndexTree/HeaderKeys in a JList <br>
 *  3) allows user to select the HeaderKeys.
 *
 * @author     Ruby Varghese
 * @created    May 12, 2003
 * @version    1.0
 */

public class SegyHeaderKeysSelectorPanel extends JPanel implements SegyIndexingProcessor
{

  private JLabel keysLabel = new JLabel();
  private JList keysList = new JList();

  private JButton cancelButton = new JButton();
  private JButton nextButton = new JButton();
  private JButton previousButton = new JButton();

  //Indexer stuff
  private SegyIndexer segyIndexer;

  private java.util.List selKeysList;

  private File segyXmlFile;

  //segyFormat xml parser
  private SegyFormatXmlParser parser;
  private Map nodesMap;

  /**
   *  Default Constructor for the SegyHeaderKeysSelectorPanel
   */
  public SegyHeaderKeysSelectorPanel() {
    try {
      jbInit();
    } catch ( Exception e ) {
        e.printStackTrace();
/*
        ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                            "Error in SegyHeaderKeysSelectorPanel");
*/
    }
  }

  /**
   *  InitializeGUI
   *
   * @throws  Exception  Description of the Exception
   */
  private void jbInit() throws Exception
  {
    Border border1 = BorderFactory.createCompoundBorder( new EtchedBorder( EtchedBorder.RAISED, Color.white, new Color( 142, 142, 142 ) ), BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );
    this.setLayout( new BorderLayout( 20, 20 ) );
    this.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );

    JPanel overallPanel = new JPanel( new BorderLayout( 0, 20 ) );
    overallPanel.setBorder( border1 );
    this.add( overallPanel, BorderLayout.CENTER );

    //Labels Panel**************************************************************

    keysLabel.setText( "Select  Header  Keys  for  Indexing:" );
    overallPanel.add( keysLabel, BorderLayout.NORTH );

    //KeyList Panel*************************************************************
    JPanel keyListPanel = new JPanel();
    keyListPanel.setLayout( new BorderLayout( 40, 10 ) );
    overallPanel.add( keyListPanel, BorderLayout.CENTER );

    JScrollPane keysScrollPane = new JScrollPane();
    keyListPanel.add( keysScrollPane, BorderLayout.CENTER );
    keyListPanel.setBorder( BorderFactory.createEmptyBorder( 10, 35, 10, 35 ) );
    keysScrollPane.getViewport().add( keysList, null );

    keysList.setSelectionMode( ListSelectionModel.MULTIPLE_INTERVAL_SELECTION );
    keysList.setCellRenderer( new CustomizedListCellRenderer() );

    selKeysList = new ArrayList();
    keysList.addMouseListener(
      new MouseAdapter()
      {
        public void mouseClicked( MouseEvent e )
        {
          if ( e.getClickCount() == 1 || e.getClickCount() == 2 )
          {
            int cellIndex = keysList.locationToIndex( e.getPoint() );
            // We now have the index of the double-clicked cell..
            ListModel listModel = ( ListModel ) keysList.getModel();
            JCheckBox selCheckBox = ( JCheckBox ) listModel.getElementAt( cellIndex );
            selCheckBox.setSelected( !selCheckBox.isSelected() );
            keysList.repaint();
            String fieldName = selCheckBox.getText();
            if ( selCheckBox.isSelected() )
            {
              selKeysList.add( fieldName );
            }
            else
            {
              selKeysList.remove( fieldName );
            }
            //System.out.println( "fieldName selected. = " + fieldName );
          }

          //System.out.println( "selKeysList size. = " + selKeysList.size() );
          if ( selKeysList.size() != 0 )
          {
            nextButton.setEnabled( true );
          }
          else
          {
            nextButton.setEnabled( false );
          }
        }
      } );

    //Buttons Panel*************************************************************
    JPanel buttonsPanel = new JPanel();
    buttonsPanel.setLayout( new FlowLayout( FlowLayout.RIGHT ) );

    previousButton.setText( "Previous..." );
    previousButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          previousButtonActionPerformed( e );
        }
      } );

    nextButton.setText( "Next..." );
    nextButton.setEnabled( false );
    nextButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          nextButtonActionPerformed( e );
        }
      } );

    cancelButton.setText( "Cancel" );
    cancelButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          cancelButtonActionPerformed( e );
        }
      } );

    buttonsPanel.add( previousButton, null );
    buttonsPanel.add( nextButton, null );
    buttonsPanel.add( cancelButton, null );
    this.add( buttonsPanel, BorderLayout.SOUTH );
  }

  /**
   *  Gets the selectedFieldNames attribute of the SegyHeaderKeysSelectorPanel
   *  object
   *
   * @return    The selectedFieldNames value
   */
  public java.util.List getSelectedFieldNames()
  {
    return selKeysList;
  }

  /**
   *  Name of the SegyIndexingProcessor
   *
   * @return    "SegyHeaderKeysSelectorPanel"
   */
  public String toString()
  {
    return "SegyHeaderKeysSelectorPanel";
  }

  /**
   *  Sets the segyFormattedFile attribute of the SegyHeaderKeysSelectorPanel
   *  object
   *
   * @param  segyXmlFile  The new segyFormattedFile value
   */
  public void setSegyFormattedFile( File segyXmlFile )
  {
    this.segyXmlFile = segyXmlFile;
    //create the model for the List of Keys
    if ( this.segyXmlFile != null )
    {
      ListModel model = createListModel( this.segyXmlFile );
      keysList.setModel( model );
    }
  }

  /**
   *  Sets the segyFormattedFile attribute of the SegyHeaderKeysSelectorPanel
   *  object
   *
   * @return    The segyFormattedFile value
   */
  public File getSegyFormattedFile()
  {
    //System.out.println( this.segyXmlFile );
    return this.segyXmlFile;
  }

  /**
   *  Sets the segyIndexer attribute of the SegyIndexingProcessor object
   *
   * @param  segyIndexer  The new segyIndexer value
   */
  public void setSegyIndexer( SegyIndexer segyIndexer )
  {
    this.segyIndexer = segyIndexer;
  }

  /**
   *  Start the IndexingProcess
   */
  public void startIndexingProcess()
  {
    throw new UnsupportedOperationException( "Not enaough parameters to start the Indexing Process" );
  }

  /**
   *  Stop Indexing Process
   */
  public void stopIndexingProcess()
  {
    this.segyIndexer.cancelIndexing();
    this.segyIndexer = null;
  }

  /**
   *  Go to the next IndexingProcess
   */
  public void goToNextIndexingProcess()
  {
    //setHeaderKeys on the segyIndexer
    this.segyIndexer.setCompoundIndexTree( getCompoundIndexTree() );

    SegyOutputFormatEditorPanel panel = new SegyOutputFormatEditorPanel();
    panel.setSegyIndexer( this.segyIndexer );
    //System.out.println("segy file selected = " + this.getSelectedFile());
    String istSelectedsegyFileName = this.segyIndexer.getBasePathOfSegyFiles() + File.separator + this.segyIndexer.getSegyFiles()[0];
    panel.setSegyIndexerFile( new File( istSelectedsegyFileName ) );
    this.segyIndexer.addSegyIndexingProcessorComponent( panel );

  }


  /**
   *  Go to the previous IndexingProcess
   */
  public void goToPreviousIndexingProcess()
  {
    this.segyIndexer.showPreviousIndexingProcessorComponent();
    this.segyIndexer.removeSegyIndexingProcessorComponent( this );

    //setHeaderKeys as null on the segyIndexer
    this.segyIndexer.setCompoundIndexTree( null );

  }

  /**
   *  create a listModel
   *
   * @param  xmlFile  selected Xml File which needs to be parsed by
   *      SegyFormatXMLParser
   * @return          ListModel
   */
  private ListModel createListModel( File xmlFile )
  {

    parser = new SegyFormatXmlParser( xmlFile );
    java.util.List nodeList = parser.getListOfFieldElementNodes();
    Iterator iter = nodeList.iterator();

    nodesMap = new HashMap();
    DefaultListModel model = new DefaultListModel();
    while ( iter.hasNext() )
    {
      Node node = ( Node ) iter.next();
      String element = parser.getFieldNameAttr( node );
      nodesMap.put( element, node );
      String format = parser.getFieldFormatAttr( node );

      if ( ( format.equalsIgnoreCase( "IBM_FLOAT32" ) ) ||
          ( format.equalsIgnoreCase( "FLOAT32" ) ) ||
          ( format.equalsIgnoreCase( "FLOAT64" ) ) )
      {
        JCheckBox modelItem = new JCheckBox( element );
        modelItem.setEnabled( false );
        model.addElement( modelItem );
      }
      else
      {
        JCheckBox modelItem = new JCheckBox( element );
        //System.out.println( "name : " + element );
        model.addElement( modelItem );
      }
    }
    return model;
  }


  /**
   *  Action for the previousButton. This navigates to the previous stage of
   *  IndexingProcess
   *
   * @param  e  ActionEvent
   */
  private void previousButtonActionPerformed( ActionEvent e )
  {
    goToPreviousIndexingProcess();
  }

  /**
   *  Action for the Next Button. This navigates to the next statge of
   *  IndexingProcess
   *
   * @param  e  ActionEvent
   */

  private void nextButtonActionPerformed( ActionEvent e )
  {
    this.goToNextIndexingProcess();
  }

  /**
   *  Action for Cancel Button Cancels the SegyIndexer.
   *
   * @param  e  ActionEvent
   */
  private void cancelButtonActionPerformed( ActionEvent e )
  {
    this.stopIndexingProcess();
  }

  /**
   *  Create the CompoundIndexTree from the selected key list names from the
   *  ListModel
   *
   * @return    CompoundIndexTree[]
   */
  private CompoundIndexTree[] getCompoundIndexTree()
  {
    int size = this.selKeysList.size();
    CompoundIndexTree[] keys = new CompoundIndexTree[size];

    for ( int i = 0; i < size; i++ ) {
      //get the attributes of
      String name = this.selKeysList.get( i ).toString();
      Node fieldNode = ( Node ) this.nodesMap.get( name );
      String format = parser.getFieldFormatAttr( fieldNode );
      int offset = parser.getFieldOffsetAttr( fieldNode );
      try {
        keys[i] = new CompoundIndexTree( name, offset, format, i, keys );
      } catch ( IOException io ) {
          io.printStackTrace();
/*
          ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                              "IO Error in SegyHeaderKeysSelectorPanel");
*/
      }
    }
    return keys;
  }

}
