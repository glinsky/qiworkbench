/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui.indexing.widget;

import com.gwsys.seismic.indexing.DataSourceKey;
import com.gwsys.seismic.indexing.SegyProcessingOutputCallback;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.HeadlessException;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JPanel;

/**
 *  A JDialog which is a TimeSlicer.
 *
 * @author     Ruby Varghese
 * @created    May 12, 2003
 * @version    1.0
 */

public class TimeSliceDialog extends JDialog implements TimeSlicer
{
  //gui stuff
  private JPanel overallPanel = new JPanel();
  private CardLayout overallLayout = new CardLayout();
  private TimeSliceProcessor previous;
  private TimeSliceProcessor next;

  //SegyIndexer stuff
  private String xgy;
  private String outputFile;
  private SegyProcessingOutputCallback callback;
  private Thread indexerThread;


  private DataSourceKey _inlineKey;
  private DataSourceKey _xlineKey;

  /**
   *  Constructor for the SegyIndexerDialog object
   *  NOTE: only called by TimeSliceWizard main() - a test
   * @throws  HeadlessException  Description of the Exception
   */
  public TimeSliceDialog() throws HeadlessException {
    try {
      jbInit();
    } catch ( Exception e ) {
        e.printStackTrace();
    }
  }

  /**
   *  Description of the Method
   *
   * @throws  Exception  Description of the Exception
   */
  private void jbInit() throws Exception
  {

    this.setDefaultCloseOperation( JDialog.DISPOSE_ON_CLOSE );
    this.setTitle( "Segy Transposer Wizard" );

    //**************************************************************************

    overallPanel.setLayout( overallLayout );
    overallPanel.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );
    this.getContentPane().add( overallPanel, BorderLayout.CENTER );

    //**************************************************************************
    previous = new InputDataPanel();
    previous.setTimeSlicer( this );
    overallPanel.add( ( JPanel ) previous, previous.toString() );

    // Add window listener
    addWindowListener(
      new WindowAdapter()
      {
        public void windowClosing( WindowEvent e )
        {
          cancelIndexing();
        }
      } );

  }

  /**
   *  Set a file name with the extension *.xgy on the SegyIndexer
   *
   * @param  xgyFile  MasterIndexFile name
   */
  public void setMasterDataIndexFile( String xgyFile )
  {
    xgy = xgyFile;

  }

  /**
   *  Gets the masterDataIndexFile name of the SegyIndexer
   *
   * @return    The masterDataIndexFile name with the xgy extension
   */
  public String getMasterDataIndexFile()
  {
    return xgy;
  }

  /**
   *  Cancel the indexing process
   */
  public void cancelIndexing()
  {
    stopIndexingThread();
    this.dispose();
    //Commented by Woody Folsom 1/9/2008
    //Unless there is a good reason to do this here, GC should not be
    //explicitly requested.
    //System.gc();
  }

  /**
   *  Stop Indexing Thread
   */
  private void stopIndexingThread()
  {
    if ( indexerThread != null && indexerThread.isAlive() )
    {
      indexerThread.interrupt();
      indexerThread=null;
    }
  }

  /**
   *  Start indexing process.Note: that you need to set the Thread before
   *  starting the Indexing Process.
   */
  public void startIndexing()
  {
    if( getInlineKey() != null &&
        getXlineKey() != null )
    {
      if (this.indexerThread != null) {
        this.indexerThread.setPriority(Thread.MAX_PRIORITY);
        this.indexerThread.start();
      }
    }
  }

  /**
   *  Show the previous IndexingProcessorComponent control
   */
  public void showPreviousTimeSliceComponent()
  {
    overallLayout.previous( overallPanel );
  }

  /**
   *  Adds the specified IndexingProcessorComponent control
   *
   * @param  panel  TimeSliceProcessor
   */
  public void addTimeSliceProcessorComponent( TimeSliceProcessor panel )
  {

    if ( next != null )
    {
      previous = next;
    }
    next = panel;

    overallPanel.add( next.toString(), ( Component ) next );

    overallLayout.show( overallPanel, next.toString() );
  }

  /**
   *  Removes the specified IndexingProcessorComponent control
   *
   * @param  panel  SegyIndexingProcessor
   */
  public void removeTimeSliceProcessorComponent( TimeSliceProcessor panel )
  {
    if ( panel != null && panel instanceof Component )
    {
      overallLayout.removeLayoutComponent( ( Component ) panel );
    }
  }

  /**
   *  Sets indexing thread
   *
   * @param  thread  Thread which starts the indexing process. Note: that you
   *      need to set the Thread before starting the Indexing Process.
   */
  public void setIndexingThread( Thread thread )
  {
      indexerThread = thread;
  }

  /**
   *  Gets indexing thread
   *
   * @return    the indexing thread
   */
  public Thread getIndexingThread()
  {
    return indexerThread;
  }

  /**
   *  Sets the segyProcessingOutputCallback of the SegyIndexer
   *
   * @param  callBack  The new segyProcessingOutputCallback value
   */
  public void setSegyProcessingOutputCallback( SegyProcessingOutputCallback callBack ) { }

  /**
   *  Gets segyProcessingOutputCallback of the SegyIndexer object
   *
   * @return    The segyProcessingOutputCallback value
   */
  public SegyProcessingOutputCallback getSegyProcessingOutputCallback()
  {
    return null;
  }
  /**
   *  Set a output file name
   *
   * @param  file  file name
   */
  public void setOutputFile( String file )
  {
     outputFile = file;
  }

  /**
   *  Gets the output file name
   *
   * @return the output file name
   */
  public String getOutputFile()
  {
    return  outputFile;
  }
  
  /**
   * Gets inline key
   * @return the inline key
   */
  public DataSourceKey getInlineKey()
  {
    return _inlineKey;
  }

  /**
   * Sets inline key
   * @param key the inline key
   */
  public void setInlineKey(DataSourceKey key)
  {
    _inlineKey = key;
  }

  /**
   * Gets xline key
   * @return the xline key
   */
  public DataSourceKey getXlineKey()
  {
    return _xlineKey;
  }

  /**
   * Sets xline key
   * @param key the xline key
   */
  public void setXlineKey(DataSourceKey key)
  {
    _xlineKey = key;
  }
}