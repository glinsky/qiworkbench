/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui.indexing.widget;

import javax.swing.border.*;
import java.awt.*;
import java.io.File;

import javax.swing.filechooser.FileFilter;
import javax.swing.*;

//import com.bhpBilliton.viewer2d.util.ErrorDialog;

import java.awt.event.*;

/**
 *  A part of the SegyIndexingProcessor which allows user to select the
 *  MasterIndexFile and the index file for the SegyIndexer.
 *
 * @author     Ruby Varghese
 * @created    May 15, 2003
 * @version    1.0
 */

public class SegyOutputFormatEditorPanel extends JPanel implements SegyIndexingProcessor
{
  private JLabel heading = new JLabel();


  private JLabel indexLabel = new JLabel();
  private JTextField segyFileTextField = new JTextField();
  private JTextField indexTextField = new JTextField();

  private JButton cancelButton = new JButton();
  private JButton nextButton = new JButton();
  private JButton previousButton = new JButton();

  private JButton advanceButton = new JButton();

  //Indexer stuff
  private SegyIndexer segyIndexer;

  private File selSegyFile;
  private File selDatFile;

  /**
   *  Default Constructor for the  SegyOutputFormatEditorPanel
   */
  public SegyOutputFormatEditorPanel() {
    try {
      jbInit();
    } catch ( Exception e ) {
        e.printStackTrace();
/*
        ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                            "Error in SegyOutputFormatEditorPanel");
*/
    }
  }

  /**
   *  InitializeGUI
   *
   * @throws  Exception  Description of the Exception
   */
  private void jbInit() throws Exception
  {

    Border border1 = BorderFactory.createCompoundBorder( new EtchedBorder( EtchedBorder.RAISED, Color.white, new Color( 142, 142, 142 ) ), BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );
    this.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );
    this.setLayout( new BorderLayout( 0, 20 ) );
    JPanel overallPanel = new JPanel();
    overallPanel.setBorder( border1 );

    indexTextField.setText("*.xgy");
    this.add( overallPanel, BorderLayout.CENTER );
    overallPanel.setLayout( new GridLayout( 3, 1, 20, 20 ) );

    // Heading Panel***********************************************************
    JPanel headingPanel = new JPanel( new BorderLayout() );
    heading.setText("Select  Output  Dataset:" );
    headingPanel.add( heading, BorderLayout.WEST );
    overallPanel.add( headingPanel, null );

    //Master Index Panel*******************************************************

    JPanel indexPanel = new JPanel();
    indexPanel.setLayout( new BorderLayout( 20, 0 ) );
    indexLabel.setText("Master File" );
    indexLabel.setMaximumSize( new Dimension( 100, 27 ) );
    indexLabel.setMinimumSize( new Dimension( 100, 27 ) );
    indexLabel.setPreferredSize( new Dimension( 100, 27 ) );
    indexPanel.add( indexLabel, BorderLayout.WEST );
    indexPanel.add( indexTextField, BorderLayout.CENTER );

    //this panel is only for proper layout
    JPanel overallIndexPanel = new JPanel(new BorderLayout());
    overallIndexPanel.add(indexPanel, BorderLayout.NORTH);
    overallPanel.add( overallIndexPanel, null );

    //Advance Panel **********************************************************
    JPanel advancePanel = new JPanel();
    advancePanel.setLayout( new FlowLayout( FlowLayout.RIGHT ) );

    advanceButton.setText("Advanced Options..." );
    advanceButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          advanceButtonActionPerformed( e );
        }
      } );
    overallPanel.add( advancePanel, null );
    advancePanel.add( advanceButton, null );

    //Buttons Panel ***********************************************************

    JPanel buttonsPanel = new JPanel();
    previousButton.setText( "Previous..." );
    previousButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          previousButtonActionPerformed( e );
        }
      } );
    buttonsPanel.add( previousButton, null );

    nextButton.setText( "Next..." );
    nextButton.setEnabled( false );
    nextButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          nextButtonActionPerformed( e );
        }
      } );
    buttonsPanel.add( nextButton, null );

    cancelButton.setText( "Cancel" );
    cancelButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          cancelButtonActionPerformed( e );
        }
      } );
    buttonsPanel.add( cancelButton, null );

    buttonsPanel.setLayout( new FlowLayout( FlowLayout.RIGHT ) );
    this.add( buttonsPanel, BorderLayout.SOUTH );

  }


  /**
   *  Name of the SegyIndexingProcessor
   *
   * @return    "SegyOutputFormatEditorPanel"
   */
  public String toString()
  {
    return "SegyOutputFormatEditorPanel";
  }


  /**
   *  Sets the segyIndexerFile attribute of the SegyOutputFormatEditorPanel
   *  object
   *
   * @param  segyFile  The new segyIndexerFile value
   */
  public void setSegyIndexerFile( File segyFile )
  {
    selSegyFile = segyFile;
    if ( selSegyFile != null )
    {
      //System.out.println("^^^^^^selSegyFile^^^^^^^^^^" + selSegyFile);
      String selFileName = selSegyFile.getName();

      //get just the file name without the extension
      int index = selFileName.indexOf( "." );
      String substring = selFileName.substring( 0, index );
      //index file
      String basePath = ( segyIndexer.getBasePathOfSegyFiles().length() > 0 ) ?
          segyIndexer.getBasePathOfSegyFiles() + File.separator : "";
      String indexFileName = basePath + substring + ".igx";

      this.segyIndexer.setIndexFile( indexFileName );
      //master index file name
      String masterIndexFileName = substring + ".xgy";
      this.indexTextField.setText( masterIndexFileName );
      this.segyIndexer.setMasterDataIndexFile( masterIndexFileName );
      //enable the nextButton
      nextButton.setEnabled( true );
    }

  }

  /**
   *  Sets the segyIndexer attribute of the SegyIndexingProcessor object
   *
   * @param  segyIndexer  The new segyIndexer value
   */
  public void setSegyIndexer( SegyIndexer segyIndexer )
  {
    this.segyIndexer = segyIndexer;
  }

  /**
   *  Start the IndexingProcess
   */
  public void startIndexingProcess()
  {
    throw new UnsupportedOperationException( "Not enough parameters to start the Indexing Process" );
  }

  /**
   *  Stop Indexing Process
   */
  public void stopIndexingProcess()
  {
    this.segyIndexer.cancelIndexing();
    this.segyIndexer = null;
  }

  /**
   *  Go to the next IndexingProcess
   */
  public void goToNextIndexingProcess()
  {
    SegyIndexingStatusPanel panel = new SegyIndexingStatusPanel();
    panel.setSegyIndexer( this.segyIndexer );
    this.segyIndexer.addSegyIndexingProcessorComponent( panel );
  }


  /**
   *  Go to the previous IndexingProcess
   */
  public void goToPreviousIndexingProcess()
  {
    this.segyIndexer.showPreviousIndexingProcessorComponent();
    this.segyIndexer.removeSegyIndexingProcessorComponent( this );
    //set MasterIndexFile and Index files to null
    this.segyIndexer.setMasterDataIndexFile(null);
    this.segyIndexer.setIndexFile(null);

  }

  /**
   *  Action fro the Advance Button. This pops up a
   *  SegyOutputAdvanceEditorDialog.
   *
   * @param  e  ActionEvent
   */
  private void advanceButtonActionPerformed( ActionEvent e )
  {
    SegyOutputAdvanceEditorDialog dialog = new SegyOutputAdvanceEditorDialog();
    dialog.setSegyIndexer( this.segyIndexer );

    dialog.setLocationRelativeTo( this );
    dialog.setSize( 320, 290 );
    dialog.setVisible( true );

  }

  /**
   *  Action for the previousButton. This navigates to the previous stage of
   *  IndexingProcess
   *
   * @param  e  ActionEvent
   */
  private void previousButtonActionPerformed( ActionEvent e )
  {
    this.goToPreviousIndexingProcess();
  }

  /**
   *  Action for the Next Button. This navigates to the next statge of
   *  IndexingProcess
   *
   * @param  e  ActionEvent
   */
  private void nextButtonActionPerformed( ActionEvent e )
  {
    this.goToNextIndexingProcess();
  }

  /**
   *  Action for Cancel Button Cancels the SegyIndexer.
   *
   * @param  e  ActionEvent
   */
  private void cancelButtonActionPerformed( ActionEvent e )
  {
    this.stopIndexingProcess();
  }

}
