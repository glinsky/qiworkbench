/*
 bhpViewer - a 2D seismic viewer
 This program module Copyright (C) Interactive Network Technologies 2006

 The following is important information about the license which accompanies this copy of
 the bhpViewer in either source code or executable versions (hereinafter the "Software").
 The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
 is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
 http://www.gnu.org/licenses/gpl.txt.
 This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with this program;
 if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 For a license to use the Software under conditions other than in a way compliant with the
 GPL or the additional restrictions set forth in this license agreement or to purchase
 support for the Software, please contact: Interactive Network Technologies, Inc.,
 2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

 All licensees should note the following:

 *        In order to compile and/or modify the source code versions of the Software,
 a user may require one or more of INT's proprietary toolkits or libraries.
 Although all modifications or derivative works based on the source code are governed by the
 GPL, such toolkits are proprietary to INT, and a library license is required to make use of
 such toolkits when making modifications or derivatives of the Software.
 More information about obtaining such a license can be obtained by contacting sales@int.com.

 *        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
 serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
 submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
 This will allow the Custodian to consider integrating such revised versions into the
 version of the Software it distributes. Doing so will foster future innovation
 and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
 the Custodian is under no obligation to include modifications or revisions in future
 distributions.

 This program module may have been modified by BHP Billiton Petroleum,
 G&W Systems Consulting Corp or other third parties, and such portions are licensed
 under the GPL.
 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
 visit http://qiworkbench.org to learn more.
 */

package com.bhpBilliton.viewer2d.ui.indexing.widget;

import java.awt.*;

import javax.swing.*;

import java.awt.event.*;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.bhpBilliton.viewer2d.util.GenericFileFilter;
import com.bhpBilliton.viewer2d.util.IconResource;

import javax.swing.border.*;

/**
 *  A SegyIndexingProcessor which allows user to select the *xml file to be
 *  parsed by a SegyFormatXMLParser.
 *
 * @author     Ruby Varghese
 * @created    May 12, 2003
 * @version    1.1
 * @since      Dec. 2006
 */

public class SegyFormatSelectorPanel extends JPanel implements
		SegyIndexingProcessor {
	private static final long serialVersionUID = 1L;

	private JLabel segyFormatLabel = new JLabel();

	private JButton segyFormatFCButton = new JButton();

	private JTextField segyFormatTextField = new JTextField();

	private JTextArea commentsLabel = new JTextArea();

	private JButton cancelButton = new JButton();

	private JButton previousButton = new JButton();

	private JButton nextButton = new JButton();

	private JButton editXMLButton = new JButton();

	private JFileChooser fc;

	//Indexer stuff
	private SegyIndexer segyIndexer;

	private File segyXmlFile;

	/**
	 *  Constructor with SegyIndexer.
	 */
	public SegyFormatSelectorPanel(SegyIndexer segyIndexer) {
		try {
			this.segyIndexer = segyIndexer;
			jbInit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 *  InitializeGUI
	 *
	 * @throws  Exception  Description of the Exception
	 */
	private void jbInit() throws Exception {

		this.setLayout(new BorderLayout(0, 20));
		this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		JPanel overallPanel = new JPanel();
		overallPanel.setLayout(new BorderLayout());
		Border border2 = BorderFactory.createCompoundBorder(new EtchedBorder(
				EtchedBorder.RAISED, Color.white, new Color(142, 142, 142)),
				BorderFactory.createEmptyBorder(5, 5, 5, 5));
		overallPanel.setBorder(border2);

		this.add(overallPanel, BorderLayout.CENTER);

		//Label Panel***************************************************************

		segyFormatLabel.setText("Select  SEG - Y  Format  Descriptor:");
		overallPanel.add(segyFormatLabel, BorderLayout.NORTH);

		//FileChooser Panel*********************************************************

		JPanel segyFormatPanel = new JPanel();
		segyFormatPanel.setBorder(BorderFactory.createEmptyBorder(30, 30, 30,
				30));
		segyFormatPanel.setLayout(new BorderLayout());
		segyFormatTextField.setText("*.xml");
		segyFormatTextField.setMinimumSize(new Dimension(300, 30));
		segyFormatTextField.setPreferredSize(new Dimension(300, 30));

		segyFormatFCButton.setIcon(IconResource.getInstance().getImageIcon(
				IconResource.OPEN24_ICON));
		segyFormatFCButton.setMaximumSize(new Dimension(30, 30));
		segyFormatFCButton.setMinimumSize(new Dimension(30, 30));
		segyFormatFCButton.setPreferredSize(new Dimension(30, 30));
		segyFormatFCButton
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(ActionEvent e) {
						segyFormatFCButtonActionPerformed(e);
					}
				});
		segyFormatPanel.add(segyFormatTextField, BorderLayout.CENTER);
		segyFormatPanel.add(segyFormatFCButton, BorderLayout.EAST);

		commentsLabel
				.setText("Specify the XML file that describes how the keys are layed out in "
						+ "the trace header. You can select \"StandardSegy.xml\" or make your "
						+ "up own if you are using a different SEG-Y flavor.");
		commentsLabel.setLineWrap(true);
		commentsLabel.setWrapStyleWord(true);
		commentsLabel.setBackground(UIManager.getColor("Label.background"));
		commentsLabel.setFont(new java.awt.Font("Serif", 2, 12));
		commentsLabel.setBorder(BorderFactory.createEmptyBorder(0, 30, 30, 30));

		JPanel centerPanel = new JPanel(new BorderLayout(0, 20));
		centerPanel.add(segyFormatPanel, BorderLayout.NORTH);
		centerPanel.add(commentsLabel, BorderLayout.CENTER);
		overallPanel.add(centerPanel, BorderLayout.CENTER);
		//  ***********************Code from Mexico Team
		JPanel editPanel = new JPanel(new FlowLayout());
		editXMLButton.setText("Edit XML");
		editXMLButton.setEnabled(false);
		editXMLButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editXMLButtonActionPerformed(e);
			}
		});
		editPanel.add(editXMLButton, null);
		centerPanel.add(editPanel, BorderLayout.SOUTH);
		//Buttons Panel*************************************************************
		previousButton.setText("Previous...");
		previousButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				previousButtonActionPerformed(e);
			}
		});
		nextButton.setText("Next...");
		nextButton.setEnabled(false);
		nextButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nextButtonActionPerformed(e);
			}
		});
		cancelButton.setText("Cancel");
		cancelButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButtonActionPerformed(e);
			}
		});

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		buttonsPanel.add(previousButton, null);
		buttonsPanel.add(nextButton, null);
		buttonsPanel.add(cancelButton, null);
		//  since dec 2006
		if (segyIndexer.getXMLSegyFormatFile() != null) {
			segyXmlFile = new File(segyIndexer.getXMLSegyFormatFile());
			segyFormatTextField.setText(segyIndexer.getXMLSegyFormatFile());
			nextButton.setEnabled(true);
			editXMLButton.setEnabled(true);
		}
		this.add(buttonsPanel, BorderLayout.SOUTH);

	}

	/**
	 *  Name of the SegyIndexingProcessor
	 *
	 * @return    "SegyFormatSelectorPanel"
	 */
	public String toString() {
		return "SegyFormatSelectorPanel";
	}

	/**
	 *  Sets the segyFormattedFile attribute of the SegyHeaderKeysSelectorPanel
	 *  object
	 *
	 * @return    The segyFormattedFile value
	 */
	public File getSelectedSegyFormattedFile() {
		return this.segyXmlFile;
	}

	/**
	 *  Sets the segyIndexer attribute of the SegyIndexingProcessor object
	 *
	 * @param  segyIndexer  The new segyIndexer value
	 */
	public void setSegyIndexer(SegyIndexer segyIndexer) {
		this.segyIndexer = segyIndexer;
	}

	/**
	 *  Start the IndexingProcess
	 */
	public void startIndexingProcess() {
		throw new UnsupportedOperationException(
				"Not enaough parameters to start the Indexing Process");
	}

	/**
	 *  Stop Indexing Process
	 */
	public void stopIndexingProcess() {
		this.segyIndexer.cancelIndexing();
		this.segyIndexer = null;
	}

	/**
	 *  Go to the next IndexingProcess
	 */
	public void goToNextIndexingProcess() {
		SegyHeaderKeysSelectorPanel panel = new SegyHeaderKeysSelectorPanel();
		panel.setSegyIndexer(this.segyIndexer);
		panel.setSegyFormattedFile(this.getSelectedSegyFormattedFile());
		System.out.println(this.getSelectedSegyFormattedFile());
		this.segyIndexer.addSegyIndexingProcessorComponent(panel);
	}

	/**
	 *  Go to the previous IndexingProcess
	 */
	public void goToPreviousIndexingProcess() {
		this.segyIndexer.showPreviousIndexingProcessorComponent();
		this.segyIndexer.removeSegyIndexingProcessorComponent(this);

		//throw new UnsupportedOperationException( "There is no prior process to this Indexing Process" );
	}

	/**
	 *  Action for the previousButton. This navigates to the previous stage of
	 *  IndexingProcess
	 *
	 * @param  e  ActionEvent
	 */
	private void previousButtonActionPerformed(ActionEvent e) {
		goToPreviousIndexingProcess();
	}

	/**
	 *  Action for the Next Button. This navigates to the next statge of
	 *  IndexingProcess
	 *
	 * @param  e  ActionEvent
	 */
	private void nextButtonActionPerformed(ActionEvent e) {
		this.goToNextIndexingProcess();
	}

	/**
	 *  Action for Cancel Button Cancels the SegyIndexer.
	 *
	 * @param  e  ActionEvent
	 */
	private void cancelButtonActionPerformed(ActionEvent e) {
		this.stopIndexingProcess();
	}

	/**
	 *  Action for xml file chooser button
	 *
	 * @param  e  ActionEvent
	 */
	private void segyFormatFCButtonActionPerformed(ActionEvent e) {
		if (fc == null) {
			fc = new JFileChooser();
			// QIW Disable references to viewer3d
			//fc = new JFileChooser(com.gwsys.viewer3d.util.ViewerUtils.getDataDirectory());
		}

		String[] extensions = new String[] { "xml" };
		FileFilter filter = new GenericFileFilter(extensions, "*.xml");
		fc.setFileFilter(filter);

		fc.showOpenDialog(this);
		{
			fc.approveSelection();
			segyXmlFile = fc.getSelectedFile();
			if (segyXmlFile != null && filter.accept(segyXmlFile)) {
				segyFormatTextField.setText(segyXmlFile.getName());
				segyIndexer.setXMLSegyFormatFile(segyXmlFile.getPath());
				nextButton.setEnabled(true);
				editXMLButton.setEnabled(true);
			}
		}
	}

	private void editXMLButtonActionPerformed(ActionEvent e) {
		InnerXMLEditor XmlEditor = new InnerXMLEditor(this
				.getSelectedSegyFormattedFile());
		XmlEditor.setSize(640, 480);
		XmlEditor.setVisible(true);
		segyXmlFile = XmlEditor.getXMLFile();
		segyFormatTextField.setText(segyXmlFile.getName());
	}

	class InnerXMLEditor extends JDialog implements ActionListener {
		private static final long serialVersionUID = 1L;

		//private File Xmlfile;
		private File current;

		private FileReader XmlReader;

		private BufferedReader XmlBuffer;

		private JTextArea XmlText;

		private Boolean changed;

		private Boolean saveChanges;

		private JFileChooser fc;

		private JScrollPane editorScroll;

		private JButton saveButton;

		private JButton okButton;

		/* New Lines START	*/
		public JButton getSaveBtn() {
			return saveButton;
		}

		class MyDocumentListener implements DocumentListener {
			public void insertUpdate(DocumentEvent e) {
				updateLog(e);
			}

			public void removeUpdate(DocumentEvent e) {
				updateLog(e);
			}

			public void changedUpdate(DocumentEvent e) {
				//Plain text components don't fire these events.
				updateLog(e);
			}

			public void updateLog(DocumentEvent e) {
				if (changed == false) {
					changed = true;
					getSaveBtn().setEnabled(true);
					getSaveBtn().setText("*" + getSaveBtn().getText());
				}
			}
		}

		public InnerXMLEditor(File segyXmlFile) {
			try {
				jbInit(segyXmlFile);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private void jbInit(File segyXmlFile) throws Exception {
			//Xmlfile = segyXmlFile;
			current = segyXmlFile;//null;
			changed = false;
			saveChanges = false;

			System.out.println("Opening XML file: " + current.getName());
			this.getContentPane().setLayout(new BorderLayout(0, 20));
			this.setModal(true);
			this.setTitle("XML Editor - " + current.getName());

			Border editBorder = BorderFactory.createCompoundBorder(
					new EtchedBorder(EtchedBorder.RAISED, Color.white,
							new Color(142, 142, 142)), BorderFactory
							.createEmptyBorder(5, 5, 5, 5));
			JPanel over = new JPanel();
			over.setBorder(editBorder);
			over.setLayout(new BorderLayout(0, 10));

			XmlText = new JTextArea();
			XmlReader = new FileReader(current);
			XmlBuffer = new BufferedReader(XmlReader);

			boolean eof = false;
			String line;
			while (!eof) {
				line = XmlBuffer.readLine();
				if (line == null)
					eof = true;
				else
					XmlText.append(line + "\n");
			}
			XmlBuffer.close();
			XmlText.getDocument().addDocumentListener(new MyDocumentListener());
			fc = new JFileChooser();
			//current.getPath());

			// File filters
			String[] extensions = new String[] { ".xml", ".xgy" };
			FileFilter filter = new GenericFileFilter(extensions,
					"Format Descriptor (*.xml)");
			fc.setFileFilter(filter);

			editorScroll = new JScrollPane(XmlText);
			over.add(editorScroll, BorderLayout.CENTER);
			//this.getContentPane().add(editorScroll, BorderLayout.CENTER);
			saveButton = new JButton("Save");
			saveButton.setEnabled(false);
			saveButton.addActionListener(this);
			okButton = new JButton("OK");
			okButton.addActionListener(this);

			JPanel buttonsPanel = new JPanel();
			buttonsPanel.setLayout(new FlowLayout());//( 1, 2, 4, 0) );
			buttonsPanel.add(saveButton, null);
			buttonsPanel.add(okButton, null);
			//this.getContentPane().add(buttonsPanel, BorderLayout.SOUTH);
			over.add(buttonsPanel, BorderLayout.SOUTH);
			this.getContentPane().add(over);
		}

		public void actionPerformed(ActionEvent e) {
			//	System.out.println("Nothing matter to do... for now");
			if (e.getSource() == saveButton) {
				if (!saveChanges) {
					//int returnVal = fc.showSaveDialog(null);

					//if (returnVal == JFileChooser.APPROVE_OPTION) {
					File tmp = getFileSaveDialog();
					if (tmp != null) {
						//current = fc.getSelectedFile();
						current = tmp;
						System.out.println("File saved at: "
								+ current.getPath());
						this.setTitle("XML Editor - " + current.getName());
						saveButton.setText("Save");
						saveChanges = true;
						saveFile(current, XmlText.getText());
						saveButton.setEnabled(false);
						changed = false;
					}
				} else {
					saveFile(current, XmlText.getText());
					saveButton.setText("Save");
					changed = false;
				}
			}

			if (e.getSource() == okButton) {
				if (changed == true) {
					int response = JOptionPane.showConfirmDialog(this,
							"Do you wish save changes in this file?");
					if (response == JOptionPane.OK_OPTION) {
						if (!saveChanges) {
							File tmp = getFileSaveDialog();
							if (tmp != null)
								saveFile(tmp, XmlText.getText());
						} else
							saveFile(current, XmlText.getText());
					}
				}

				System.out.println("Closing Editor ...");
				this.dispose();
			}
		}

		public File getFileSaveDialog() {
			int retVal = fc.showSaveDialog(null);
			if (retVal == JFileChooser.APPROVE_OPTION)
				return fc.getSelectedFile();

			return null;
		}

		public void saveFile(File file, String data) {
			FileWriter fw;
			try {
				fw = new FileWriter(file);
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(data, 0, data.length());
				bw.close();
				System.out.println("Data has been wrote");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public File getXMLFile() {
			return current;
		}
	}
}
