/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui.indexing.widget;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.border.EtchedBorder;
import javax.swing.filechooser.FileFilter;
import java.io.File;
import java.io.CharArrayWriter;
import java.io.PrintWriter;


//import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpBilliton.viewer2d.util.GenericFileFilter;
import com.bhpBilliton.viewer2d.util.IconResource;
import com.gwsys.seismic.indexing.CompoundIndexTree;

/**
 *  Output data File(segy) Panel
 *
 * @author     Ruby Varghese
 * @created    July 21, 2003
 * @version    1.0
 */

public class OutputDataPanel extends JPanel implements TimeSliceProcessor
{

  private JTextField outputFileTextField = new JTextField();
  private JButton outputFileButton = new JButton();
  private JFileChooser fc;


  private JButton cancelButton = new JButton();
  private JButton nextButton = new JButton();
  private JButton previousButton = new JButton();

  //Indexer stuff
  private TimeSlicer timeSlicer;

  /**
   *  Constructor for the outputDataPanel object
   * @param timeSlicer the time slicer
   */
  public OutputDataPanel( TimeSlicer timeSlicer ) {
    try {
      this.timeSlicer = timeSlicer;
      jbInit();
    } catch ( Exception e ) {
        e.printStackTrace();
/*
        ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                            "Error in OutputDataPanel");
*/
    }
  }

  /**
   *  InitializeGUI
   *
   * @throws  Exception  Description of the Exception
   */
  private void jbInit() throws Exception
  {

    this.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );
    this.setLayout( new BorderLayout() );

    JPanel overallPanel = new JPanel();
    overallPanel.setLayout( new GridLayout( 1, 1, 0, 0 ) );
    overallPanel.setBorder( BorderFactory.createCompoundBorder( new EtchedBorder( EtchedBorder.RAISED, Color.white, new Color( 142, 142, 142 ) ), BorderFactory.createEmptyBorder( 20, 5, 20, 5 ) ) );

    this.add( overallPanel, BorderLayout.CENTER );

    //components Panel --like the chooser and text field for time/depth********
    JPanel componentsPanel = new JPanel( new BorderLayout() );

    //label panel---for segy filechooser and time/depth****************

    JPanel labelPanel = new JPanel( new BorderLayout() );

    JLabel outputFileLabel = new JLabel( "Choose SEG-Y File :" );
    labelPanel.add( outputFileLabel, BorderLayout.NORTH );

    overallPanel.add( labelPanel );

    //SEGY File Chooser Panel**************************************************
    JPanel outputFilePanel = new JPanel();
    outputFilePanel.setLayout( new BorderLayout() );

    outputFileButton.setIcon(IconResource.getInstance().getImageIcon(IconResource.OPEN24_ICON));

    outputFileButton.setMaximumSize( new Dimension( 30, 30 ) );
    outputFileButton.setMinimumSize( new Dimension( 30, 30 ) );
    outputFileButton.setPreferredSize( new Dimension( 30, 30 ) );
    outputFileButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          outputFileButtonActionPerformed( e );
        }
      } );
    outputFilePanel.add( outputFileButton, BorderLayout.EAST );

    outputFileTextField.setText( "output.xgy" );
    outputFilePanel.add( outputFileTextField, BorderLayout.CENTER );

    componentsPanel.add( outputFilePanel, BorderLayout.NORTH );

    //timeDepth Panel

    overallPanel.add( componentsPanel );
    //Buttons Panel************************************************************

    cancelButton.setText( "Cancel" );
    cancelButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          cancelButtonActionPerformed( e );
        }
      } );
    nextButton.setText( "Next" );
    nextButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
           nextButtonActionPerformed( e );
        }
      } );
    previousButton.setText( "Previous..." );
    previousButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          previousButtonActionPerformed( e );
        }
      } );
    JPanel buttonsPanel = new JPanel();
    buttonsPanel.setLayout( new FlowLayout( FlowLayout.RIGHT ) );
    buttonsPanel.add( previousButton, null );
    buttonsPanel.add( nextButton, null );
    buttonsPanel.add( cancelButton, null );

    this.add( buttonsPanel, BorderLayout.SOUTH );
  }

  /**
   *  Action for segy fileChooserPanel Button
   *
   * @param  e  ActionEvent
   */
  private void outputFileButtonActionPerformed( ActionEvent e )
  {
    if ( fc == null )
    {
      // QIW Disable regerences to viewer3d
      fc = new JFileChooser();
      //fc = new JFileChooser(com.gwsys.viewer3d.util.ViewerUtils.getDataDirectory());
    }

    fc.setDialogTitle( "Select output XGY file To Index" );
    fc.setMultiSelectionEnabled( false );

    String[] extensions = new String[]{"xgy"};
    FileFilter filter = new GenericFileFilter( extensions, "*.xgy" );
    fc.setFileFilter( filter );

    fc.showOpenDialog( this );

    fc.approveSelection();
    File selFile = fc.getSelectedFile();

    if ( selFile != null && filter.accept( selFile ) )
    {
      this.timeSlicer.setOutputFile( selFile.getAbsolutePath() );
      this.outputFileTextField.setText( selFile.getAbsolutePath() );
    }

  }

  /**
   *  Name of the SegyIndexingProcessor
   *
   * @return    "OutputDataPanel"
   */
  public String toString()
  {
    return "OutputDataPanel";
  }

  /**
   *  Sets the TimeSlicer attribute of the TimeSliceProcessor object
   *
   * @param  timeSlicer  The new timeSlicer value
   */
  public void setTimeSlicer( TimeSlicer timeSlicer )
  {
    final String DEF_NAME = "transposed.xgy";

    this.timeSlicer = timeSlicer;
    if( timeSlicer != null ) {
      String str = timeSlicer.getMasterDataIndexFile();
      if (str != null && str.length() > 0 ) {
        try {
          File file = new File( str );
          String path    = file.getParent();

          if( path != null ) {

            String defName = file.getName();

            if( defName != null && defName.length() > 0 && defName.indexOf(".") != -1) {
              defName = defName.substring(0, defName.indexOf(".")) + "_";
            }
            String defFileName = path + File.separator + defName + DEF_NAME;
            outputFileTextField.setText( defFileName );
          }
        }
        catch(Exception ex)
        {

        }
      }
    }
  }


  /**
   *  Start the IndexingProcess
   */
  public void startIndexingProcess() { }

  /**
   *  Stop Indexing Process
   */
  public void stopIndexingProcess()
  {
    this.timeSlicer.cancelIndexing();
  }

  /**
   *  Go to the next IndexingProcess
   */
  public void goToNextIndexingProcess()
  {
    // gets text
    timeSlicer.setOutputFile( outputFileTextField.getText() );

   //Create the next statge of Indexing
    TimeSliceStatusPanel panel = new TimeSliceStatusPanel();
    panel.setTimeSlicer( this.timeSlicer );
    this.timeSlicer.addTimeSliceProcessorComponent( panel );

  }


  /**
   *  Go to the previous IndexingProcess
   */
  public void goToPreviousIndexingProcess()
  {
    this.timeSlicer.showPreviousTimeSliceComponent();
    this.timeSlicer.removeTimeSliceProcessorComponent( this );
  }

  /**
   *  Sets the compoundIndexTree attribute of the OutputDataPanel object
   *
   * @param  tree  The new compoundIndexTree value
   */
  public void setCompoundIndexTree( CompoundIndexTree tree ) { }

  /**
   *  Action for Cancel Button Cancels the timeSlicer.
   *
   * @param  e  ActionEvent
   */
  private void cancelButtonActionPerformed( ActionEvent e )
  {
    this.stopIndexingProcess();
  }

  /**
   *  Action for the start Button. This method starts the indexing process
   *
   * @param  e  ActionEvent
   */
  private void  nextButtonActionPerformed( ActionEvent e )
  {
    this.goToNextIndexingProcess();
  }

  /**
   *  Action for the previousButton. This navigates to the previous stage of
   *  IndexingProcess
   *
   * @param  e  ActionEvent
   */
  private void previousButtonActionPerformed( ActionEvent e )
  {
    this.goToPreviousIndexingProcess();
  }

}
