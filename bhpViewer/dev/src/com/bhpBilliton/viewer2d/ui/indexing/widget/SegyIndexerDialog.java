/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui.indexing.widget;

import javax.swing.*;
import java.awt.*;
import com.gwsys.seismic.indexing.*;

import java.nio.ByteOrder;
import java.awt.event.*;
import java.util.logging.Logger;

/**
 *  A JDialog which is a SegyIndexer.
 *
 * @author     Ruby Varghese
 * @created    May 12, 2003
 * @version    1.0
 */

public class SegyIndexerDialog extends JDialog implements SegyIndexer
{
  private static final Logger logger = Logger.getLogger(SegyIndexerDialog.class.toString());
    
  //gui stuff
  private JPanel overallPanel = new JPanel();
  private CardLayout overallLayout = new CardLayout();
  private SegyIndexingProcessor previous;
  private SegyIndexingProcessor next;
  private Thread indexerThread;

  //SegyIndexer stuff
  private String xgy;
  private String[] segy;
  private String indexFile;
  private CompoundIndexTree[] keys;
  private String basepath;
  private String xmlSegyFormatFile = null;
  private ByteOrder order = ByteOrder.BIG_ENDIAN;
  private int pageSize = 1024;
  private SegyProcessingOutputCallback callback;


  /**
   *  Constructor for the SegyIndexerDialog object
   *
   * @throws  HeadlessException  Description of the Exception
   */
  public SegyIndexerDialog() throws HeadlessException {
    try {
      jbInit();
    } catch ( Exception e ) {
        e.printStackTrace();
    }
  }

  /**
   *  Description of the Method
   *
   * @throws  Exception  Description of the Exception
   */
  private void jbInit() throws Exception
  {
    this.setTitle( "SEG - Y  Indexer" );

    //**************************************************************************

    overallPanel.setLayout( overallLayout );
    overallPanel.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );
    this.getContentPane().add( overallPanel, BorderLayout.CENTER );

    //**************************************************************************
    previous = new SegyFileSelectorPanel();
    previous.setSegyIndexer( this );
    overallPanel.add( (JPanel)previous, previous.toString());

    // Add window listener
    addWindowListener(
      new WindowAdapter()
      {
        public void windowClosing( WindowEvent e )
        {
          cancelIndexing();
        }
      } );
  }


  /**
   *  Set a file name with the extension *.xgy on the SegyIndexer
   *
   * @param  xgyFile  MasterIndexFile name
   */
  public void setMasterDataIndexFile( String xgyFile )
  {
    xgy = xgyFile;
  }


  /**
   *  Gets the masterDataIndexFile name of the SegyIndexer
   *
   * @return    The masterDataIndexFile name with the xgy extension
   */
  public String getMasterDataIndexFile()
  {
    return xgy;
  }

  /**
   *  Set a Index file name with the extension *.igx on the SegyIndexer
   *
   * @param  datFile  The new indexFile value
   */
  public void setIndexFile( String datFile )
  {
    indexFile = datFile;
  }


  /**
   *  Gets the IndexFile name of the SegyIndexer
   *
   * @return    The IndexFile name with the *.igx extension
   */
  public String getIndexFile()
  {
    return indexFile;
  }

  /**
   *  Set segy file names with the extension *.segy on the SegyIndexer
   *
   * @param  segyFiles  The new segyFile names
   */
  public void setSegyFiles( String[] segyFiles )
  {
    segy = segyFiles;

  }

  /**
   *  Gets the SegyFile names of the SegyIndexer
   *
   * @return    The SegyFile names with the *.SEG-Y extension
   */
  public String[] getSegyFiles()
  {
    return segy;
  }

  /**
   *  Set selected Indexed keys on the SegyIndexer
   *
   * @param  keys  The new array of Indexed Keys
   */
  public void setCompoundIndexTree( CompoundIndexTree[] keys )
  {
    this.keys = keys;
  }

  /**
   *  Gets the Indexed keys of the SegyIndexer
   *
   * @return    The selected Indexed keys
   */
  public CompoundIndexTree[] getCompoundIndexTree()
  {
    return keys;
  }

  /**
   *  Set basePath of SegyFiles on the SegyIndexer
   *
   * @param  basePath  The parent Path of the selected Segy Files set on this
   *      SegyIndexer
   */
  public void setBasePathOfSegyFiles( String basePath )
  {
    this.basepath = basePath;

  }

  /**
   *  Gets the parent Path od the selected Segy Files set on this SegyIndexer
   *
   * @return    The basePath
   */
  public String getBasePathOfSegyFiles()
  {
    return this.basepath;
  }

  /**
   *  Set a ByteOrder on the SegyIndexer. This could be Big-Endian or
   *  Little-Endian. By default it is Big-Endian
   *
   * @param  byteOrder  The new byteOrder value
   */
  public void setByteOrder( ByteOrder byteOrder )
  {
    this.order = byteOrder;

  }

  /**
   *  Gets the ByteOrder of the SegyIndexer
   *
   * @return    This could be Big-Endian or Little-Endian. By default it is
   *      Big-Endian.
   */
  public ByteOrder getByteOrder()
  {
    return order;
  }

  /**
   *  Set a page size on the SegyIndexer
   *
   * @param  pageSize  The new pageSize. By default it is 1024
   */
  public void setPageSize( int pageSize )
  {
    this.pageSize = pageSize;
  }

  /**
   * Sets the XML Segy format fiel
   * @param xmlFormat the xml format file
   */
  public void setXMLSegyFormatFile( String xmlFormat )
  {
      xmlSegyFormatFile = xmlFormat;
  }

  /**
   * Gets the XML Segy format file
   * @return the xml format file
   */
  public String getXMLSegyFormatFile( )
  {
      return xmlSegyFormatFile;
  }

  /**
   *  Gets the page size of the SegyIndexer
   *
   * @return    By default it is 1024
   */
  public int getPageSize()
  {
    return this.pageSize;
  }

  /**
   *  Sets the segyProcessingOutputCallback of the SegyIndexer
   *
   * @param  callBack  The new segyProcessingOutputCallback value
   */
  public void setSegyProcessingOutputCallback( SegyProcessingOutputCallback callBack )
  {
    this.callback = callBack;
  }

  /**
   *  Gets segyProcessingOutputCallback of the SegyIndexer object
   *
   * @return    The segyProcessingOutputCallback value
   */
  public SegyProcessingOutputCallback getSegyProcessingOutputCallback()
  {
    return this.callback;
  }


  /**
   *  Cancel the indexing process
   */
  public void cancelIndexing()
  {
    stopIndexingThread();

    if( getContentPane() != null )  getContentPane().removeAll();

    this.removeAll();

    if( previous != null ) {
      previous.setSegyIndexer(null);
      previous = null;
    }

    if(next != null ) {
      next.setSegyIndexer(null);
      next = null;
    }

    if(overallPanel != null ) {
      Component[] component = overallPanel.getComponents();
      for(int i = 0; i < component.length; i++ )
      {
        if( component[i] != null && component[i] instanceof SegyIndexingProcessor )
        {
          ((SegyIndexingProcessor)component[i]).setSegyIndexer(null);
          component[i] = null;
        }
      }
      overallPanel.removeAll();
      overallPanel = null;
    }
    segy         = null;
    indexFile    = null;
    keys         = null;
    basepath     = null ;
    xmlSegyFormatFile = null;
    callback     = null;
    overallPanel = null;

    this.dispose();
    //Commented by Woody Folsom 1/9/2008
    //Unless there is a good reason to do this here, GC should not be explicitly
    //requested.
    //System.gc();
  }

  /**
   *  Start indexing process.Note: that you need to set the Thread before
   *  starting the Indexing Process.
   */
  public void startIndexing()
  {
    if ( ( this.getMasterDataIndexFile() != null ) &&
        ( this.getSegyFiles() != null ) &&
        ( this.getIndexFile() != null ) &&
        ( this.getCompoundIndexTree() != null ) &&
        ( this.getBasePathOfSegyFiles() != null ) &&
        ( this.getByteOrder() != null ) &&
        ( this.getPageSize() != 0 ) &&
        ( this.getSegyProcessingOutputCallback() != null ) )
    {
      if ( this.indexerThread != null )
      {
        this.indexerThread.setPriority( Thread.MAX_PRIORITY );
        this.indexerThread.start();
      }
    }
  }

  protected void finalize() throws Throwable
  {
    super.finalize();
    stopIndexingThread();
    logger.info("Exit...");
  }

  /**
   *  Sets indexing thread
   *
   * @param  thread  Threadon which the indexingProcess is started
   */
  public synchronized void setIndexingThread( Thread thread )
  {
    indexerThread = thread;
  }

  /**
   *  Gets indexing thread
   *
   * @return    the indexing thread
   */
  public Thread getIndexingThread()
  {
    return indexerThread;
  }

  /**
   *  Stop Indexing Thread
   */
  private void stopIndexingThread() {
        if (indexerThread != null) {
            if (indexerThread.isAlive()) {
                indexerThread.interrupt();
            }
            indexerThread = null;
        }
    }

  /**
   * Show the previous IndexingProcessorComponent control
   */
  public void showPreviousIndexingProcessorComponent()
  {
    overallLayout.previous( overallPanel );
  }

  /**
   *  Adds the specified IndexingProcessorComponent control
   *
   * @param  panel  SegyIndexingProcessor
   */
  public void addSegyIndexingProcessorComponent( SegyIndexingProcessor panel )
  {

    if ( next != null )
    {
      previous = next;
    }
    next = panel;

    overallPanel.add(  next.toString(),( Component ) next );

    overallLayout.show( overallPanel, next.toString() );
  }

  /**
   *  Removes the specified IndexingProcessorComponent control
   *
   * @param  panel  SegyIndexingProcessor
   */
  public void removeSegyIndexingProcessorComponent( SegyIndexingProcessor panel )
  {
    if ( panel != null && panel instanceof Component )
    {
      overallLayout.removeLayoutComponent( ( Component ) panel );
    }
  }
}