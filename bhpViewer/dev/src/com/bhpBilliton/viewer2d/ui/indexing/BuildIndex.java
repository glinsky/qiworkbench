/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui.indexing;

import com.gwsys.seismic.indexing.*;
import com.gwsys.seismic.indexing.util.*;

import java.io.*;
import java.nio.*;
import java.util.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;

/**
 * Build index file
 * @version 1.0
 */
public class BuildIndex implements SegyProcessingOutputCallback {
  private String[] _segy = null;
  private int _page_size;
  private ByteOrder _order;
  private String _xgy, _index, _basepath,  _profilename, _tmpfolder;
  private String _segyFormatFile = null;
  private SegyProcessingOutputCallback _outputCallback = null;
  private Integer _dataOrder =  new Integer(DataSliceType.DATA_CROSSECTION);
  private String _transposedKey = null;
  private String _transposedName = null;
  private String _transposedRate = null;
  private String [] _keys           = null;

  private Integer _max_items=new Integer(-1);

  private void print_usage() {
  }
  // -endian=L -segys=shots.segy -index=index.dat -pagesize=1024 -xgy=shots.xgy -basepath=.
  private boolean parse_args(String[] args) {
    String [] keys = {"-endian", "-segys", "-index", "-pagesize", "-xgy", "-basepath", "-keys", "-profilename", "-segyFormat" };
    String [] addkeys = {"-tmppath", "-maxitems" };

    Map map = ArgumentsParser.parse(args);
    String [] vals;

    if( map.size() < keys.length) {
      print_usage();
      return false;
    }
    // temp path
    if( map.containsKey( addkeys[ 0 ] ) ) {
      vals = (String[]) map.get(addkeys[0]);
      if( vals.length == 1 ) {
        _tmpfolder = vals[0];
      }
      else {
        print_usage();
        return false;
      }
    }
    // max items
    if (map.containsKey(addkeys[1])) {
      vals = (String[]) map.get(addkeys[1]);
      if (vals.length == 1) {
        String tmpitems = vals[0];
        tmpitems = tmpitems.trim();
        try {
          _max_items = new Integer(tmpitems);
        }
        catch( java.lang.Exception e)
        {
          return false;
        }
      }
      else {
        return false;
      }
    }

    for (int i = 0; i < keys.length; i++)
      if(!map.containsKey(keys[i])) {
        print_usage();
        return false;
      }

    vals = (String[]) map.get(keys[0]);
    if(vals.length != 1 || vals[0] == null) {
      print_usage();
      return false;
    }

    if(vals[0].compareToIgnoreCase("B") == 0)
      _order = ByteOrder.BIG_ENDIAN;
    else if (vals[0].compareToIgnoreCase("L") == 0)
      _order = ByteOrder.LITTLE_ENDIAN;
    else {
      print_usage();
      return false;
    }

    vals = (String[]) map.get(keys[1]);
    if(vals.length == 0 || vals[0] == null ) {
      print_usage();
      return false;
    }

    String[] files = vals[0].split(";");

    if( files == null || files.length == 0 )
    {

      print_usage();
     return false;
    }

    _segy = files;

    vals = (String[]) map.get(keys[2]);
    if(vals.length != 1 || vals[0] == null) {
      print_usage();
      return false;
    }

    _index = vals[0];

    vals = (String[]) map.get(keys[8]);
    if(vals.length != 1 || vals[0] == null) {
      print_usage();
      return false;
    }
    _segyFormatFile = vals[0];

    vals = (String[]) map.get(keys[3]);
    if(vals.length != 1 || vals[0] == null) {
      print_usage();
      return false;
    }

    try {
      _page_size = new Integer(vals[0]).intValue();
    }
    catch (NumberFormatException e) {
      print_usage();
      return false;
    }

    vals = (String[]) map.get(keys[4]);
    if(vals.length != 1 || vals[0] == null) {
      print_usage();
      return false;
    }

    _xgy = vals[0];

    vals = (String[]) map.get(keys[5]);
    if(vals.length != 1 || vals[0] == null) {
      print_usage();
      return false;
    }

    _basepath = vals[0];

    File f = new File(_basepath + File.separator + _segy[0]);
    try{
      _basepath = f.getParentFile().getCanonicalPath();
    } catch (IOException e) {
     print_usage();
      return false;
    }

    vals = (String[]) map.get(keys[6]);
    if(vals.length < 1 ) {
      print_usage();
      return false;
    }

    _keys = vals;

    vals = (String[]) map.get(keys[7]);
    if(vals.length != 1 || vals[0] == null) {
      print_usage();
      return false;
    }

    _profilename = vals[0];

    return true;
  }

  private void show_index_info(CompoundIndexTree[] tree, SegyProcessingOutputCallback callback) {
    try{
    if(callback != null )
    {
      callback.addProcessingMessage("Index statistics:");
      int size = tree.length;
      callback.addProcessingMessage("\tNumber of indexes: "+Integer.toString(size) );

      for (int i = 0; i < size; i++) {
        CompoundIndexTree index = tree[i];
        callback.addProcessingMessage("\tIndex name: \t\t"+index.getName());
        callback.addProcessingMessage("\tUnique values: \t\t"+Integer.toString( index.getNumberOfKeys() ));
        callback.addProcessingMessage("\tMin value: \t\t"+(Number)index.getMinKey().getValue());
        callback.addProcessingMessage("\tMax value: \t\t"+(Number)index.getMaxKey().getValue());
        callback.addProcessingMessage("\tIncrement: \t\t"+(Number)index.getKeyStep().getValue());
        callback.addProcessingMessage("");
      }
    }
    } catch(Exception e) {e.printStackTrace();}
  }

    /**
     * Check free space ont he disk
     * @param spaceRequired the required space
     * @return the result of checking
     */
    public static boolean checkFreeSpace(long spaceRequired) {
      if (spaceRequired < 0)
        return false;
      if (spaceRequired == 0)
        return true;

      try {
        File f = SegyTempFile.createTempFile("chk", null);
        RandomAccessFile rf = new RandomAccessFile(f, "rw");
        rf.setLength(spaceRequired);

        rf.close();
        rf = null;
        return f.delete();
      }
      catch (IOException e) {
        return false;
      }

    }

    /**
     * Check access to files
     * @param xgy       the xgy file
     * @param indexFile index file
     * @return  if access is denied then return false else true
     */
   private boolean checkAccess( String xgy, String indexFile)
   {
     try {

       File file = new File( xgy );
       java.io.FileOutputStream stream = new java.io.FileOutputStream( file );

       if( stream == null ) return false;

       stream.close();

       stream = null;
       file   = null;
       stream = new java.io.FileOutputStream( new File( indexFile ) );

       if( stream == null ) return false;

       stream.close();
       stream = null;

     }
     catch( Exception e )
     {
       return false;
     }
     return true;
   }

   /**
    * Sets data order
    * The following values are possible:
    * <table border="1" cellpadding="3" cellspacing="0">
    * <tr><td>DataSliceType.DATA_CROSSECTION</td><td>Crossection mode</td></tr>
    * <tr><td>DataSliceType.DATA_MAP</td><td>Map mode</td></tr>
    * @param dataOrder the data order
    * </table>
    */
  public void setDataOrder( int dataOrder )
  {
    _dataOrder =  new Integer(dataOrder);
  }

  /**
   * Set transposed parameters
   * @param transposedKey the name of the key
   * @param transposedName the name of the transposed field
   * @param transposedRate the rate of the transposed field
   */
  public void setTransposedParameters( String transposedKey, String transposedName, String transposedRate )
  {
    _transposedKey = transposedKey;
    _transposedName = transposedName;
    _transposedRate = transposedRate;
  }

  /**
   * Build index
   * @param xgy the name of the xgy file
   * @param segy the array of segy files
   * @param indexFile the name of segy file
   * @param lkeys  the indexed keys
   * @param basePath the basepath
   * @param order the byte order
   * @param pageSize the page size of the B-tree
   * @param callback callback object
   * @param segyFormatFile the name of the sefy format xml file
   * @param format custom SEGY data format
   * @throws Exception the common exception
   */
  public void build(String xgy, String[] segy, String indexFile,
                    CompoundIndexTree[] lkeys, String basePath, ByteOrder order,
                    int pageSize, SegyProcessingOutputCallback callback,
                    String segyFormatFile, SegyDataFormat format) throws
      Exception {

    long l1, l2, l3, l4;

    _order          = order;
    _basepath       = basePath;
    _segy           = segy;
    _page_size      = pageSize;
    _xgy            = xgy;
    _index          = indexFile;
    _segyFormatFile = segyFormatFile;
    _outputCallback = callback;

    // check access
    if( checkAccess( _basepath + File.separator + xgy, indexFile  ) )
    {
      if (callback != null)
        callback.addProcessingMessage("Collecting index data...");

      // Print temporary folder
      File tmpFile = SegyTempFile.createTempFile( "sgy", null );

      if( tmpFile != null  ) {

        if (callback != null && tmpFile.getParentFile() != null && tmpFile.getParentFile().getPath() != null ) {
          callback.addProcessingMessage( new String( tmpFile.getParentFile().getPath() ) +
              "  will be used for keeping temporary files....");
        }
        tmpFile.delete();
        tmpFile = null;
      }
      else {

        if (callback != null)
          callback.addProcessingMessage("failed");

          return;
      }

      SegyIndexBuilder btreebuilder = new SegyIndexBuilder( this );

      SegyIndexBuilder.setTempFolder(_tmpfolder);
      SegyIndexBuilder.setMaxItems(_max_items.intValue());

      long maxTmpFilesSize = getRequiredTempSpaceApproximation( segy, lkeys, _basepath, order ) / 1024;

      if (callback != null)
        callback.addProcessingMessage("The max required temporary disk space is "+Long.toString( maxTmpFilesSize )+" KB for this job.");

      if( !checkFreeSpace( maxTmpFilesSize * 1024L ) ) {

        if (callback != null)
          callback.addProcessingMessage("Indexing stoped.\nThere is not enough disk space for temporary files!!\n");
          return;
      }

      l1 = new java.util.Date().getTime();

      btreebuilder.buildIndexCollection( _segy, _basepath, _order, format, lkeys, _segyFormatFile, _dataOrder );

      l2 = new java.util.Date().getTime();

      //Another instance of System.gc() commented by Woody Folsom on 1/9/8
      //System.gc();

      if (callback != null)
        callback.addProcessingMessage("done in " + (l2 - l1) + " ms");
      if (callback != null)
        callback.addProcessingMessage("Building index..."+_index );

      btreebuilder.writeIndexFile( _index, _page_size);

      l3 = new java.util.Date().getTime();

      if (callback != null)
        callback.addProcessingMessage("done in " + (l3 - l2) + " ms");
      if (callback != null)
        callback.addProcessingMessage("Writing config...");

      Document doc = btreebuilder.buildXMLConfig( _profilename, _transposedKey, _transposedName, _transposedRate);

      if (saveXMLDocument( doc, _basepath + File.separator + _xgy)) {

        l4 = new java.util.Date().getTime();

        if (callback != null)
          callback.addProcessingMessage("done in " + (l4 - l3) + " ms");
      }
      else
      if (callback != null)
        callback.addProcessingMessage( "failed" );

      CompoundIndexTree [] indexes = btreebuilder.getIndexes();

      show_index_info( indexes, callback );

      try {
        if (indexes != null) {

          for (int i = 0; i < indexes.length; ++i) {
            if(indexes[i] != null ) indexes[ i ].delete();

            indexes[i] = null;
          }
        }
      }
      catch( Exception ex )
      {
        //...
      }

      btreebuilder.clear();

      // force GC -- It is not technically possible to 'force' GC in Java,
      // at least not on the Sun JVM.  See comment below.
      btreebuilder    = null;
      doc             = null;
      indexes         = null;
      callback        = null;
      _segy           = null;
      _xgy            = null;
     _index           = null;
     _outputCallback  = null;

      //Commented by Woody Folsom on 1/9/2008
      //Unless there is a specific reason to do this, garbage collection should
      //probably not be explicitly requested here.
      //System.gc();
    }
    else {
      throw new Exception(" File access is denied!!!");
    }

  }

  public static long getRequiredTempSpaceApproximation(String[] segyFiles, CompoundIndexTree[] lkeys, String basePath, ByteOrder order) {

     long sum_keysize = 0, num_traces = 0, numkeys = lkeys.length;

     try{
       SegyReader r = SegyReaderFactory.getDefaultFactory().createSegyReader(segyFiles, order, basePath);
       num_traces = r.getNumberOfTraces();
     } catch(IOException e) {return 0;}

     for(int i=0; i<numkeys; i++) {
       sum_keysize += lkeys[i].getKeySize();
     }
     return num_traces * (sum_keysize * ( 4 + numkeys ) + 16 * numkeys);
  }

  public void printMessage( String str )
  {
    if( _outputCallback != null ) _outputCallback.addProcessingMessage( str );
  }

  private void build(String[] args) throws Exception {
    if (!parse_args(args)) {
      return;
    }
    if( _segyFormatFile != null ) {
      CompoundIndexTree [] lkeys = SegyIndexBuilder.parseSegyKeys(_keys, _segyFormatFile);
      build(_xgy, _segy, _index, lkeys, _basepath, _order, _page_size, this, _segyFormatFile, null);
    }
  }


  private boolean saveXMLDocument(Document doc, String path) throws TransformerException {
    Node root = doc.getDocumentElement();
    TransformerFactory tfactory = TransformerFactory.newInstance();

    if (tfactory.getFeature(DOMSource.FEATURE) && tfactory.getFeature(StreamResult.FEATURE)) {

      DOMSource src = new DOMSource(root);
      StreamResult dst = new StreamResult( new File( path ));
      Transformer trans = tfactory.newTransformer();
      trans.transform(src, dst);
      dst = null;

      return true;
    }
    else
      return false;
  }


  private Vector _segy_info = new Vector();
  private int _sample_units, _sample_interval;


  public void addProcessingMessage( String str )
  {
    if (_outputCallback != null && _outputCallback != this)
      _outputCallback.addProcessingMessage(str);
    else
      System.out.println(str);
  }

  public void addSegy(String name, int index, int start_trace, int end_trace, int sample_units, int sample_interval, ByteOrder order) {
    _sample_units = sample_units;
    _sample_interval = sample_interval;
    DataSourceFile f = new DataSourceFile();
    f.setOrder(order);
    f.setFileName(name);
    f.setStartTrace(start_trace);
    f.setEndTrace(end_trace);
    f.setSegyOrdinal(index);
    _segy_info.add(f);
  }

  public static void main(String[] args) throws Exception {
    BuildIndex bi = new BuildIndex();
    bi.build(args);
    bi = null;
  }
}