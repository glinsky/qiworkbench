/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui.indexing.widget;

import javax.swing.border.*;
import java.awt.*;
import java.io.File;

import javax.swing.filechooser.FileFilter;
import javax.swing.*;
import javax.swing.JOptionPane;
import java.awt.event.*;
import java.nio.ByteOrder;

//import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.indexing.SegyIndexBuilder;
import com.gwsys.seismic.indexing.util.SegyTempFile;
/**
 *  A part of the SegyIndexingProcessor which allows user to set the page size
 *  and the ByteOrder the SegyIndexer.
 *
 * @author     Ruby Varghese
 * @created    May 15, 2003
 * @version    1.0
 */

public class SegyOutputAdvanceEditorDialog extends JDialog
{
  JLabel headingLabel = new JLabel();

  JLabel pageLabel = new JLabel();
  JTextField pageTextField = new JTextField();

  JLabel folderLabel = new JLabel();
  JTextField folderTextField = new JTextField();

  JLabel ByteLabel = new JLabel();
  JComboBox byteComboBox = new JComboBox();

  JLabel tracesLabel = new JLabel();
  JTextField tracesTextField = new JTextField();

  JButton closeButton = new JButton();
  JButton applyButton = new JButton();

  //Indexer stuff
  private SegyIndexer segyIndexer;

  /**
   *  Default Constructor for the  SegyOutputAdvanceEditorPanel
   */
  public SegyOutputAdvanceEditorDialog() {
    try {
      jbInit();
    } catch ( Exception e ) {
        e.printStackTrace();
    }
  }

  /**
   *  InitializeGUI
   *
   * @throws  Exception  Description of the Exception
   */
  private void jbInit() throws Exception
  {
    this.getContentPane().setLayout( new BorderLayout( 0, 20 ) );
    this.setModal( true );
    this.setTitle( "SEG-Y Indexer" );

    Border border1 = BorderFactory.createCompoundBorder( new EtchedBorder( EtchedBorder.RAISED, Color.white, new Color( 142, 142, 142 ) ), BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );
    JPanel overallPanel = new JPanel();
    overallPanel.setBorder( border1 );
    this.getContentPane().add( overallPanel, BorderLayout.CENTER );
    overallPanel.setLayout( new GridLayout( 5, 1, 0, 20 ) );

    // Heading Panel***********************************************************

    headingLabel.setText("Indexing  Options:" );
    overallPanel.add( headingLabel, null );

    //Page Panel***************************************************************

    JPanel pagePanel = new JPanel();
    pagePanel.setLayout( new BorderLayout( 20, 0 ) );
    pageLabel.setText( "Page Size   " );
    pageLabel.setMaximumSize( new Dimension( 100, 17 ) );
    pageLabel.setMinimumSize( new Dimension( 100, 17 ) );
    pageLabel.setPreferredSize( new Dimension( 100, 17 ) );
    pagePanel.add( pageLabel, BorderLayout.WEST );

    pageTextField.setText( "1024" );
    pagePanel.add( pageTextField, BorderLayout.CENTER );
    overallPanel.add( pagePanel, null );

    //Byte Format Panel********************************************************

    JPanel bytePanel = new JPanel();
    bytePanel.setLayout( new BorderLayout( 20, 0 ) );
    ByteLabel.setText( "Byte Format " );
    ByteLabel.setMaximumSize( new Dimension( 100, 17 ) );
    ByteLabel.setMinimumSize( new Dimension( 100, 17 ) );
    ByteLabel.setPreferredSize( new Dimension( 100, 17 ) );
    bytePanel.add( ByteLabel, BorderLayout.WEST );

    ComboBoxModel comboModel = new DefaultComboBoxModel( new Object[]{ByteOrder.BIG_ENDIAN, ByteOrder.LITTLE_ENDIAN} );
    byteComboBox.setModel( comboModel );
    byteComboBox.setSelectedIndex( 0 );
    bytePanel.add( byteComboBox, BorderLayout.CENTER );
    overallPanel.add( bytePanel, null );

    //Folder Panel***************************************************************

    JPanel folderPanel = new JPanel();
    folderPanel.setLayout( new BorderLayout( 20, 0 ) );
    folderLabel.setText( "Temporary folder" );
    folderLabel.setMaximumSize( new Dimension( 100, 17 ) );
    folderLabel.setMinimumSize( new Dimension( 100, 17 ) );
    folderLabel.setPreferredSize( new Dimension( 100, 17 ) );
    folderPanel.add( folderLabel, BorderLayout.WEST );

    String folder = null;

    if( SegyTempFile.isDefaultTempFolder()  ) {
      // Print temporary folder
      File tmpFile = SegyTempFile.createTempFile( "sgy", null );
      if ( tmpFile.getParentFile() != null && tmpFile.getParentFile().getPath() != null )
        folder = new String( tmpFile.getParentFile().getPath() );
        tmpFile = null;
    }
    else
    {
      folder = SegyTempFile.getTempFolder();
    }
    if( folder == null ) folder = new String("");
    folderTextField.setText( folder );
    folderPanel.add( folderTextField, BorderLayout.CENTER );
    overallPanel.add( folderPanel, null );

    //traces Panel***************************************************************

    JPanel tracesPanel = new JPanel();
    tracesPanel.setLayout( new BorderLayout( 20, 0 ) );
    tracesLabel.setText( "Max items " );
    tracesLabel.setMaximumSize( new Dimension( 100, 17 ) );
    tracesLabel.setMinimumSize( new Dimension( 100, 17 ) );
    tracesLabel.setPreferredSize( new Dimension( 100, 17 ) );
    tracesPanel.add( tracesLabel, BorderLayout.WEST );

    tracesTextField.setText( Integer.toString( SegyIndexBuilder.getMaxItems() ) );
    tracesPanel.add( tracesTextField, BorderLayout.CENTER );
    overallPanel.add( tracesPanel, null );

    //Buttons Panel ***********************************************************

    JPanel buttonsPanel = new JPanel();
    buttonsPanel.setLayout( new FlowLayout( FlowLayout.RIGHT ) );
    applyButton.setText( "Apply" );
    applyButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          applyButtonActionPerformed( e );
        }
      } );
    closeButton.setText( "Close" );
    closeButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          closeButtonActionPerformed( e );
        }
      } );

    this.getContentPane().add( buttonsPanel, BorderLayout.SOUTH );
    buttonsPanel.add( applyButton, null );
    buttonsPanel.add( closeButton, null );
  }

  /**
   *  Name of the SegyIndexingProcessor
   *
   * @return    "SegyOutputAdvanceEditorPanel"
   */
  public String toString()
  {
    return "SegyOutputAdvanceEditorPanel";
  }


  /**
   *  Sets the segyIndexer attribute of the SegyIndexingProcessor object
   *
   * @param  segyIndexer  The new segyIndexer value
   */
  public void setSegyIndexer( SegyIndexer segyIndexer )
  {
    this.segyIndexer = segyIndexer;
  }


  /**
   *  This method sets the byteOrder and the page size on the SegyIndexer
   *
   * @param  e  ActionEvent
   */
  private void applyButtonActionPerformed( ActionEvent e )
  {
    apply();
  }

  /**
   *  Dispose the dialog after applying the parameters on the SegyIndexer
   *
   * @param  e  ActionEvent
   */
  private void closeButtonActionPerformed( ActionEvent e )
  {
    if( !apply() ) return;
    this.dispose();
    this.segyIndexer = null;
  }

  /**
   *  Apply Action
   * @return success
   */
  private boolean apply()
  {
    int DEF_COUNT = 100000;

    ByteOrder selByteOrder = ( ByteOrder ) byteComboBox.getSelectedItem();
    this.segyIndexer.setByteOrder( selByteOrder );
    Integer pageSize = new Integer( this.pageTextField.getText() );
    this.segyIndexer.setPageSize( pageSize.intValue() );
    String folder = this.folderTextField.getText();
    if( folder != null ) {
        folder = folder.trim();
    }

    if( folder != null && folder.length() > 0 ) {
      File f = new File( folder );

      if( f == null || !f.isDirectory() ) {
          JOptionPane.showMessageDialog(null,
                            "Could not find temporary SEGY file",
                            "File Error", JOptionPane.ERROR_MESSAGE);
        return false;
      }
      SegyTempFile.setTempFolder( folder );
    } else {
      SegyTempFile.setTempFolder( null );
    }
    int count = DEF_COUNT;
    try {
      Integer tracesSize = new Integer(this.tracesTextField.getText());

      count = tracesSize.intValue();

      if (count < 1000 || count > 1000000) {
        count = DEF_COUNT;
      }
    }
    catch( Exception e )
    {
      count = DEF_COUNT;
    }

    SegyIndexBuilder.setMaxItems(count);

    return true;
  }
}