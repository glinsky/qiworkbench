/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui.indexing;

//import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.indexing.*;
import com.gwsys.seismic.indexing.util.*;

import java.util.*;
import java.io.*;
import java.nio.*;

/**
 * Creates time slice from indexed segy
 * @author INT
 * @version 1.0
 */
public class TimeSlice implements SegyProcessingOutputCallback  {

  private static final int MAX_PARALLEL_READS = 64;
  private static final float DEFAULT_SAMPLE_RATE =  0.004f;
  private static final float DEFAULT_FACTOR      =  1000000.0f;
  private float  _factor = DEFAULT_FACTOR;

  private SegyDataset dataset        = null;
  private SegyReader segy_in = null;

  private int xline_header_offset, inline_header_offset;
  private int in_num_samples, sample_size, window_size;
  private int window_index, out_num_samples, out_trace_size;
  private int cube_primary_num, cube_secondary_num;
  private int _inlineMin = 0;

  private int[] trace_ids = null;
  private byte[] out_buffer = null;
  private byte[][] in_buffers = null;

  private SegyProcessingOutputCallback _outputCallback = null;

  private static final int samples_per_trace_header_offset = 3220;
  private static final int traces_per_record_header_offset = 3212;
  private static final int sample_interval_header_offset   = 3216;
  private static final int start_time_offset               = 108;

  private float _startTime     = 0;
  private float _sampleRate    = 0;

  /**
   * Sets output callback
   * @param outputCallback the output callback
   */
  public void setOutputCallback( SegyProcessingOutputCallback outputCallback )
  {
    _outputCallback = outputCallback;
  }

  private float getFactor( float srate )
  {
    float factor = DEFAULT_FACTOR;

    if( srate <=0 || srate > 1 ) {
      factor = DEFAULT_FACTOR;
    }
    else {
      long rate = Math.round( srate * DEFAULT_FACTOR );
      factor = DEFAULT_FACTOR;

      while( rate > 0 && (rate & 0x01) == 0 ) {
        rate /= 10;
        if(rate > 0) factor /= 10;
      }
    }

    return factor;
  }
  /**
   * Prepare output data
   * @param dataset the input data set
   * @param output_samples the count of the output samples
   * @throws FileNotFoundException
   * @throws IOException
   */
  private void prepare_output( SegyDataset dataset, int output_samples ) throws FileNotFoundException, IOException
  {
    segy_in = SegyReaderFactory.getDefaultFactory().createSegyReader( dataset.getSegys() );

    in_num_samples   = segy_in.getNumSamplesPerTrace();
    sample_size      = segy_in.getSampleSize();
    cube_primary_num = in_num_samples;

    float sampleInterval = dataset.getSampleInterval();

    // Sets time parameters
    if( sampleInterval <= 0 ) {
        _sampleRate = DEFAULT_SAMPLE_RATE;
    } // convert to seconds only for default sample rate
    else {
      _sampleRate = sampleInterval / DEFAULT_FACTOR;
    }
    _startTime =  (float)dataset.getStartValue();

    _factor = getFactor( _sampleRate );

    // cout output samples
    out_num_samples = output_samples;

    out_trace_size = SegyReaderFactory.getDefaultFactory().getSegyDataFormat().getTraceHeaderSize() +
        out_num_samples * sample_size;

    // alloctae memory
    allocate_memory();

    // creates output segy
    prepare_outsegy();
  }

  private File out_segy_file = null;
  private BufferedOutputStream out_segy_stream = null;

  private void prepare_outsegy() throws IOException {
    out_segy_stream = new BufferedOutputStream(new FileOutputStream(_segy), 65536);
  }

  /**
   * Gets the initial sample rate
   * @return initial sample rate
   */
  public float getInitialSampleRate()
  {
    return _sampleRate;
  }
  /**
   * Adds processing message
   * @param str the message
   */
  public void addProcessingMessage( String str )
  {
     if( _outputCallback != null ) {
       _outputCallback.addProcessingMessage(str);
     }
     else {
         System.out.println(str);
     }
  }

  private SegyIndex xlineIndex = null, inlineIndex = null;
  private SegyCompoundIndex lineIndex = null;
  private DataSourceKeys line_keys = null;
  private DataSourceKey inlineDSKey = null, xlineDSKey = null;
  private IndexKey inlineIKey = null, inlineIKeyMax = null;
  /**
   * Do slice
   * @param inputXgy  - input XGY file
   * @param inlineKey - inline key
   * @param xlineKey  - xline key
   * @param outputSegy - output segy file
   * @throws FileNotFoundException if file is not found
   * @throws IOException if system can not read or write data
   */
  public void do_slice( String inputXgy, DataSourceKey inlineKey,
                       DataSourceKey xlineKey, String outputSegy) throws
      FileNotFoundException,
      IOException {

    _xgy = inputXgy;

    _segy = outputSegy;
    long l2 = new Date().getTime();

    addProcessingMessage("Slice in progress...");

    SegyDataset dataset = SegyDatasetFactory.getDefaultFactory().createDataset( inputXgy );

    if( dataset == null ) throw new FileNotFoundException("Cannot open file "+ inputXgy);

    // get xlineKey parameters
    xlineIndex = dataset.getIndex( xlineKey.getName() );
    inlineIndex = dataset.getIndex( inlineKey.getName() );

    SegyIndex [] ic = new SegyIndex[2];
    ic[0] = xlineIndex;
    ic[1] = inlineIndex;
    lineIndex = new SegyCompoundIndex(ic);

    xline_header_offset  = xlineIndex.getSegyHeaderOffset();

    inline_header_offset = inlineIndex.getSegyHeaderOffset();

    double xlineMin  = Double.valueOf( xlineKey.getMinValue() ).doubleValue();
    double xlineMax  = Double.valueOf( xlineKey.getMaxValue() ).doubleValue();
    double xlineStep = Double.valueOf( xlineKey.getIncrement() ).doubleValue();

    double xlineCount = (( xlineMax-xlineMin) / xlineStep+1);

    double inlineMin  = Double.valueOf( inlineKey.getMinValue() ).doubleValue();
    double inlineMax  = Double.valueOf( inlineKey.getMaxValue() ).doubleValue();
    double inlineStep = Double.valueOf( inlineKey.getIncrement() ).doubleValue();

    double inlineCount = Math.ceil(( inlineMax-inlineMin) / inlineStep+1);

    _inlineMin = (int)inlineMin;
    window_size = (int)inlineCount;

    // prepare output file
    prepare_output( dataset, (int)inlineCount );

    // XLINE
    IndexKey xlineIKeyMax = IndexKey.createKey( xlineIndex.getKeyMin().getType() );
    IndexKey xlineIKey    = IndexKey.createKey( xlineIndex.getKeyMin().getType() );

    xlineIKey.setValue( xlineKey.getMinValue() );
    xlineIKeyMax.setValue( xlineKey.getMaxValue() );

    // INLINE
    inlineIKeyMax = IndexKey.createKey( inlineIndex.getKeyMin().getType() );
    inlineIKey    = IndexKey.createKey( inlineIndex.getKeyMin().getType() );
    inlineIKeyMax.setValue( inlineKey.getMaxValue() );

    // LINE KEYS
    line_keys = new DataSourceKeys(2);
    inlineDSKey = new DataSourceKey();
    inlineDSKey.setIncrement( inlineKey.getIncrement() );
    inlineDSKey.setName( inlineKey.getName() );
    inlineDSKey.setType( inlineIndex.getKeyMin().getType() );
    line_keys.addItem(inlineDSKey);
    xlineDSKey = new DataSourceKey();
    xlineDSKey.setIncrement( xlineKey.getIncrement() );
    xlineDSKey.setName( xlineKey.getName() );
    xlineDSKey.setType( xlineIndex.getKeyMin().getType() );
    line_keys.addItem(xlineDSKey);

    // calculate the new count of the traces
    int out_num_traces = (int)(dataset.getNumSamplesPerTrace() * inlineCount);

    cube_secondary_num = (int)in_num_samples;

    // write output segy header
    write_segy_header( out_num_samples, out_num_traces, inlineIndex.getKeyStep() );

    int xline = 0, got_traces = 0;

    // look through xline
    while( xlineIKey.compareTo( xlineIKeyMax ) <= 0 ) {

      Arrays.fill(trace_ids, 0);

      // gets a list of the traces in the slice
      try {
        got_traces = xlineIndex.getTraceList(trace_ids, xlineIKey);
      } catch (ArrayIndexOutOfBoundsException e) {
        throw new IOException("Segy data is not cube or wrong input parameters!!!");
      }

      if( got_traces != 0 ) {

        if (got_traces < inlineCount)
          repos_traces(xlineIKey, inlineKey.getMinValue());

          // process current group of the traces
        transposeTraces(xlineIKey);
      }

      if ( xline != 0 && xline % 10 == 0 )
        addProcessingMessage("Transposing " + xline + " slice from " +
                           ( (int) xlineCount) + " slices )");
      xline++;
      xlineIKey.addStep( xlineIndex.getKeyStep() );
    }

    out_segy_stream.flush();
    long l3 = new Date().getTime();

    addProcessingMessage("done in " + Long.toString(l3 - l2) + " msec");
  }

  private void repos_traces(IndexKey xlineIKey, String inlineKeyMin) throws IOException {
    xlineDSKey.setMinValue( xlineIKey.getValue().toString() );
    xlineDSKey.setMaxValue( xlineIKey.getValue().toString() );
    inlineIKey.setValue( inlineKeyMin );
    int index = 0;
    while( inlineIKey.compareTo( inlineIKeyMax ) <= 0 ) {
      inlineDSKey.setMinValue( inlineIKey.getValue().toString() );
      inlineDSKey.setMaxValue( inlineIKey.getValue().toString() );
      int [] cur_trace = lineIndex.getTraceList(line_keys);
      if(cur_trace == null || cur_trace.length == 0)
        trace_ids[index] = 0;
      else if(cur_trace.length != 1)
        throw new IOException("Segy data is not cube or wrong input parameters!!!");
      else
        trace_ids[index] = cur_trace[0];
      index++;
      inlineIKey.addStep( inlineIndex.getKeyStep() );
    }
  }

  /**
   * Write segy file header
   * @param out_num_samples the output count of the smaples
   * @param out_num_traces  the putput number of the traces
   * @param output_step  thd output step
   * @throws IOException
   */
  private void write_segy_header( int out_num_samples, int out_num_traces, IndexKeyStep output_step ) throws IOException {

    ByteBuffer in_segy_header = segy_in.getSegyHeader();

    byte [] segy_header_buffer = new byte [segy_header_size];
    ByteBuffer out_segy_header = ByteBuffer.wrap(segy_header_buffer);

    // save the old header
    out_segy_header.put(in_segy_header);

    // save samples per trace
    out_segy_header.position(samples_per_trace_header_offset);
    out_segy_header.putShort((short)(out_num_samples));

    // save trace count
    out_segy_header.position( traces_per_record_header_offset);
    out_segy_header.putShort( (short) out_num_traces);

    // save sampes interval
    out_segy_header.position( sample_interval_header_offset );

    int istep    = output_step.getValue().intValue();
    double dstep = output_step.getValue().doubleValue();

    if(dstep != (double)istep || istep < 0 || istep > 65535) {
      addProcessingMessage("Invalid slice increment value.");
      out_segy_header.putShort( (short) 0);
    } else {
      out_segy_header.putShort( (short) istep);
    }
    out_segy_stream.write(segy_header_buffer);
  }

  /**
     * Loads data from input file
     * @param traces aggay of requested trace IDs
     * @throws IOException
     */
    private void loadSlice( int[] traces ) throws
        IOException {

      int num = traces.length;

      for (int i = 0; i < num; i++) {
        if(traces[ i ] != 0)
          segy_in.readTraceSamples( in_buffers[ i ], 0, traces[ i ]);
        else
          Arrays.fill(in_buffers[i], (byte)0);
      }

    }

  byte [] header_buffer = null;
  ByteBuffer bb_header = null, bb_samples = null;

  private void save_slice( IndexKey xline_current_key )throws
        IOException
  {
    int num = this.in_num_samples;
    float val = 0;

    for (int i = 0; i < num; i++) {

       for (int j = 0; j < out_num_samples; j++) {

            for(int k = 0; k < sample_size; k++)
              out_buffer[ j * sample_size + k ] = in_buffers[j][i*sample_size+k];
        }
        write_trace(xline_current_key, i);
    }
  }

  private void write_trace(IndexKey xline_key, int trace_index) throws IOException {
    bb_header.position(xline_header_offset);
    xline_key.write(bb_header);
    bb_header.position(inline_header_offset);
    bb_header.putFloat(Math.round((_startTime + trace_index * _sampleRate) * _factor) / _factor);
    bb_header.position(start_time_offset);
    bb_header.putInt(_inlineMin);
    out_segy_stream.write(header_buffer);
    out_segy_stream.write(out_buffer);
  }

  private void transposeTraces( IndexKey currentKey ) throws IOException {

   loadSlice( trace_ids );
   save_slice( currentKey );
  }

  /**
   * Allocate memory for read/write buffers
   */
  private void allocate_memory()
  {
    if (window_size == 0)  window_size = MAX_PARALLEL_READS;

    trace_ids = new int[out_num_samples];
    out_buffer = new byte[window_size * sample_size];
    in_buffers = new byte[window_size][];
    header_buffer = new byte[SegyReaderFactory.getDefaultFactory().getSegyDataFormat().getTraceHeaderSize()];

    bb_samples = ByteBuffer.wrap(out_buffer);
    bb_header = ByteBuffer.wrap(header_buffer);

    for (int i = 0; i < window_size; i++) {
      in_buffers[ i ] = new byte[ in_num_samples * sample_size ];
    }

  }

  public static void main(String[] args) {

    TimeSlice t = new TimeSlice();

    try {
      t.do_slice( args );
    } catch (Exception e) {
        e.printStackTrace();
/*
        ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                            "Error in TimeSlice");
*/
    }
  }

  private String _xgy, _segy;
  private String _disp_name, _disp_from, _disp_to, _disp_step;
  private String _slice_name, _slice_from, _slice_to, _slice_step;

  private boolean parse_args(String[] args) {
    String[] keys = {
        "-xgy", "-dispkey", "-slicekey", "-outsegy"};
    Map map = ArgumentsParser.parse(args, "=", ",");
    String[] vals;

    if (map.size() != keys.length) {
      return false;
    }

    String[] key = (String[]) map.get(keys[0]);
    if (key == null || key.length != 1) {
      return false;
    }
    _xgy = key[0];

    key = (String[]) map.get(keys[1]);
    if (key == null || key.length != 4) {
      return false;
    }
    _disp_name = key[0];
    _disp_from = key[1];
    _disp_to   = key[2];
    _disp_step = key[3];

    key = (String[]) map.get(keys[2]);
    if (key == null || key.length != 4) {
      return false;
    }
    _slice_name = key[0];
    _slice_from = key[1];
    _slice_to   = key[2];
    _slice_step = key[3];

    key = (String[]) map.get(keys[3]);
    if (key == null || key.length != 1) {
      return false;
    }
    _segy = key[0];

    return true;
  }

  private void show_usage() {
    addProcessingMessage(
        "Usage: timeslice -xgy=shots.xgy -dispkey=ep,5,20,1 -slicekey=cdp,0,100,5 -outsegy=slice.segy");
  }

  /**
   * Do slice
   * @param args the args
   * @throws FileNotFoundException if file is not found
   * @throws IOException if system can not read or write data
   */
  public void do_slice( String[] args ) throws FileNotFoundException,
      IOException {

    if (!parse_args(args)) {
      addProcessingMessage("error");
      show_usage();
      return;
    }

    DataSourceKey inlineKey = new DataSourceKey();
    DataSourceKey xlineKey  = new DataSourceKey();

    inlineKey.setName( _disp_name );
    inlineKey.setMinValue( _disp_from );
    inlineKey.setMaxValue( _disp_to );
    inlineKey.setIncrement( _disp_step );

    xlineKey.setName( _slice_name );
    xlineKey.setMinValue( _slice_from );
    xlineKey.setMaxValue( _slice_to );
    xlineKey.setIncrement( _slice_step );

    do_slice( _xgy, inlineKey, xlineKey, _segy );

  }

  private int segy_header_size = SegyReaderFactory.getDefaultFactory().getSegyDataFormat().getEbcdicReelHeaderSize() +
      SegyReaderFactory.getDefaultFactory().getSegyDataFormat().getBinaryReelHeaderSize();


  private void copy_sample(int i, int j) {
    for(int k = 0; k < sample_size; k++)
      out_buffer[j * sample_size + k] = in_buffers[j][i+k];
  }

}
