/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui.indexing.widget;

import javax.swing.*;
import javax.swing.JOptionPane;

import java.awt.*;
import java.awt.event.*;
import javax.swing.border.EtchedBorder;
import javax.swing.filechooser.FileFilter;
import java.io.File;

//import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpBilliton.viewer2d.util.GenericFileFilter;
import com.bhpBilliton.viewer2d.util.IconResource;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.gwsys.seismic.indexing.*;
import com.gwsys.seismic.indexing.util.SegyConstantsParser;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.HashMap;
import org.w3c.dom.Node;
import javax.swing.ComboBoxModel;

/**
 *  Input data File(xgy) Panel
 *   NOTE: Only called by TimeSliceDialog which in turn is only  clled byTimeSliceWizard main() - a test
 * @author     Ruby Varghese
 * @created    July 21, 2003
 * @version    1.0
 */

public class InputDataPanel extends JPanel implements TimeSliceProcessor {
  private JLabel inputFileLabel = new JLabel();
  private JTextField inputFileTextField = new JTextField();
  private JButton inputFileButton = new JButton();
  private JFileChooser fc;
  private File selFile;

  private JComboBox inLineCombo = new JComboBox();

  private JComboBox xLineCombo = new JComboBox();

  private JButton cancelButton = new JButton();
  private JButton nextButton = new JButton();

//Indexer stuff
  private TimeSlicer timeSlicer;
  private HashMap nodesMap;
  private DataSourceKey selectedInLineKey;
  private DataSourceKey selectedXLineKey;
  private SegyFormatXmlParser parser;

  /**
   *  Constructor for the InputDataPanel object
   */
  public InputDataPanel() {
    try {
      jbInit();
    } catch ( Exception e ) {
        e.printStackTrace();
/*        ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                            "Error in InputDataPanel");
*/
    }
  }

  /**
   *  InitializeGUI
   *
   * @throws  Exception  Description of the Exception
   */
  private void jbInit() throws Exception
  {

    this.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );
    this.setLayout( new BorderLayout() );

    JPanel overallPanel = new JPanel();
    overallPanel.setLayout( new BorderLayout( 0, 30 ) );
    overallPanel.setBorder( BorderFactory.createCompoundBorder( new EtchedBorder( EtchedBorder.RAISED, Color.white, new Color( 142, 142, 142 ) ), BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) ) );

    this.add( overallPanel, BorderLayout.CENTER );

    //XGY File Chooser Panel**************************************************
    JPanel inputFilePanel = new JPanel();
    inputFilePanel.setLayout( new BorderLayout() );

    inputFileLabel.setText( "Choose xgy File :" );

    inputFileButton.setIcon(IconResource.getInstance().getImageIcon(IconResource.OPEN24_ICON));
    inputFileButton.setMaximumSize( new Dimension( 30, 30 ) );
    inputFileButton.setMinimumSize( new Dimension( 30, 30 ) );
    inputFileButton.setPreferredSize( new Dimension( 30, 30 ) );
    inputFileButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          inputFileButtonActionPerformed( e );
        }
      } );
    inputFilePanel.add( inputFileButton, BorderLayout.EAST );

    inputFileTextField.setText( "*.xgy" );
    inputFilePanel.add( inputFileTextField, BorderLayout.CENTER );

    JPanel fileChooserPanel = new JPanel();
    fileChooserPanel.setLayout( new BorderLayout() );
    fileChooserPanel.add( inputFileLabel, BorderLayout.WEST );
    fileChooserPanel.add( inputFilePanel, BorderLayout.CENTER );
    overallPanel.add( fileChooserPanel, BorderLayout.NORTH );
    //overAllDataPanel**********************************************

    JPanel overallDataPanel = new JPanel( new BorderLayout() );
    overallPanel.add( overallDataPanel, BorderLayout.CENTER );
    //InLine Panel**************************************************
    JPanel inLinePanel = new JPanel( new GridLayout( 1, 3 ) );

    JPanel inLineComboPanel = new JPanel( new GridLayout( 1, 2 ) );
    JLabel inLineLabel = new JLabel( "InLine" );
    inLineComboPanel.add( inLineLabel );

    inLineCombo.addItemListener(
      new ItemListener()
      {
        public void itemStateChanged( ItemEvent e )
        {
          Object selectedKeyName = inLineCombo.getSelectedItem();
          selectedInLineKey = (  DataSourceKey ) getSelectedKey( inLineCombo, selectedKeyName.toString() );
        }
      } );

    inLineComboPanel.add( inLineCombo );
    inLinePanel.add( inLineComboPanel );



    overallDataPanel.add( inLinePanel, BorderLayout.NORTH );

    //XLine Panel**************************************************
    JPanel xLinePanel = new JPanel( new GridLayout( 1, 3 ) );
    JPanel xLineComboPanel = new JPanel( new GridLayout( 1, 2 ) );
    JLabel xLineLabel = new JLabel( "XLine" );
    xLineComboPanel.add( xLineLabel );

    xLineCombo.addItemListener(
      new ItemListener()
      {
        public void itemStateChanged( ItemEvent e )
        {
          Object selectedKeyName = xLineCombo.getSelectedItem();
          selectedXLineKey = (  DataSourceKey ) getSelectedKey( xLineCombo, selectedKeyName.toString() );
        }
      } );

    xLineComboPanel.add( xLineCombo );
    xLinePanel.add( xLineComboPanel );



    //overallPanel.add( xLinePanel );
    overallDataPanel.add( xLinePanel, BorderLayout.SOUTH );
    //Buttons Panel ***********************************************************

    JPanel buttonsPanel = new JPanel();

    nextButton.setText( "Next..." );
    nextButton.setEnabled( false );
    nextButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          nextButtonActionPerformed( e );
        }
      } );

    cancelButton.setText( "Cancel" );
    cancelButton.addActionListener(
      new ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          cancelButtonActionPerformed( e );
        }
      } );

    buttonsPanel.setLayout( new FlowLayout( FlowLayout.RIGHT ) );

    buttonsPanel.add( nextButton, null );
    buttonsPanel.add( cancelButton, null );

    this.add( buttonsPanel, BorderLayout.SOUTH );
  }

  /**
   *  Action for xgy fileChooserPanel Button
   *
   * @param  e  ActionEvent
   */
  private void inputFileButtonActionPerformed( ActionEvent e )
  {
    if ( fc == null )
    {
      // QIW Disable references to viewer3d
      fc = new JFileChooser();
      //fc = new JFileChooser(com.gwsys.viewer3d.util.ViewerUtils.getDataDirectory());

    }
    //initialize file
    selFile = null;
    fc.setDialogTitle( "Select XGY Input File" );
    fc.setMultiSelectionEnabled( false );

    String[] extensions = new String[]{"xgy"};
    FileFilter filter = new GenericFileFilter( extensions, "*.xgy" );
    fc.setFileFilter( filter );

    fc.showOpenDialog( this );

    fc.approveSelection();

    selFile = fc.getSelectedFile();

    if ( selFile != null && filter.accept( selFile ) && selFile.getName() != null )
    {
      try
      {
        SegyDataset dataSet = SegyDatasetFactory.getDefaultFactory().createDataset( selFile.getAbsolutePath() );

        if( dataSet == null )  throw new IOException("Cannot open file");

        if( dataSet.getDatasourceKeys().getCount() < 2 ) {
          JOptionPane.showMessageDialog( this, " File "+selFile.getAbsolutePath()+" must have more or equal to 2 keys " );
          return;
        }
      } catch ( IOException io ) {
          JOptionPane.showMessageDialog( this, "Cannot open " + selFile.getAbsolutePath() );
/*
          ErrorDialog.showErrorDialog(QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                                      "Cannot open " + selFile.getAbsolutePath(),
                                      new String[] {"Possible permissions problem",
                                                    "Possible corrupted file"},
                                      new String[] {"Fix any permissions errors",
                                                    "Verify file is Ok",
                                                    "If OK, contact workbench support"});
*/
        return;
      } catch ( Exception io ) {
        JOptionPane.showMessageDialog( this, "Wrong format of the indexed file "+selFile.getAbsolutePath()+" Index it again, please!!" );
        return;
      }

      this.inputFileTextField.setText( selFile.getAbsolutePath() );

      //list the key names in the InLineCombo
      inLineCombo.setModel( createCompoundIndexTreeComboModel( false ) );
      inLineCombo.setSelectedIndex( 0 );

      if( inLineCombo.getModel().getSize() > 0 ) {
        selectedInLineKey = this.getSelectedKey(inLineCombo,(String)inLineCombo.getSelectedItem());
      }

      //list the key names in the xLineCombo
      xLineCombo.setModel( createCompoundIndexTreeComboModel( false ) );
      xLineCombo.setSelectedIndex( xLineCombo.getModel().getSize() > 1 ? 1 : 0 );

      if( xLineCombo.getModel().getSize() > 0 ) {
        selectedXLineKey = this.getSelectedKey(inLineCombo,(String)xLineCombo.getSelectedItem());
      }

      this.timeSlicer.setMasterDataIndexFile( selFile.getAbsolutePath() );

      nextButton.setEnabled( true );
    }

  }


  /**
   *  Name of the SegyIndexingProcessor
   *
   * @return    "InputDataPanel"
   */
  public String toString()
  {
    return "InputDataPanel";
  }

  /**
   *  Sets the timeSlicer attribute of the  object
   *
   * @param  timeSlicer  The new timeSlicer value
   */
  public void setTimeSlicer( TimeSlicer timeSlicer )
  {
    this.timeSlicer = timeSlicer;

    if( timeSlicer != null ) {

      selectedInLineKey  = timeSlicer.getInlineKey();
      selectedXLineKey   = timeSlicer.getXlineKey();

      if( timeSlicer.getMasterDataIndexFile() != null ) {
        inputFileTextField.setText(timeSlicer.getMasterDataIndexFile());
      }
    }
  }

  /**
   *  Start the IndexingProcess
   */
  public void startIndexingProcess()
  {
    throw new UnsupportedOperationException( "Not enaough parameters to start the Indexing Process" );
  }

  /**
   *  Stop Indexing Process
   */
  public void stopIndexingProcess()
  {
    this.timeSlicer.cancelIndexing();
    this.timeSlicer = null;
  }

  /**
   *  Go to the next IndexingProcess
   */
  public void goToNextIndexingProcess()
  {
    setSelectedData();
    //Create the next statge of Indexing
    OutputDataPanel panel = new OutputDataPanel( timeSlicer );
    panel.setTimeSlicer( this.timeSlicer );
    this.timeSlicer.addTimeSliceProcessorComponent( panel );
  }


  /**
   *  Go to the previous IndexingProcess
   */
  public void goToPreviousIndexingProcess()
  {
    throw new UnsupportedOperationException( "There is no prior process to this Indexing Process" );
  }


  /**
   *  Action for the Next Button. This navigates to the next statge of
   *  IndexingProcess
   *
   * @param  e  ActionEvent
   */
  private void nextButtonActionPerformed( ActionEvent e )
  {
    this.goToNextIndexingProcess();
  }

  /**
   *  Action for Cancel Button Cancels the timeSlicer.
   *
   * @param  e  ActionEvent
   */
  private void cancelButtonActionPerformed( ActionEvent e )
  {
    stopIndexingProcess();
  }


  /**
   *  Sets the selectedFilesOnIndexer attribute of the SegyFileSelectorPanel
   *  object
   * @param allkeys include all keys
   * @return    The compoundIndexTree value
   */
  private ComboBoxModel createCompoundIndexTreeComboModel( boolean allkeys )
  {
    DefaultComboBoxModel model = null;
    try
    {
      SegyDataset dataSet = SegyDatasetFactory.getDefaultFactory().createDataset( selFile.getAbsolutePath() );

      //create segyXMLFile
      File segyFormatFile = new File( dataSet.getSegyFormatFile() );

      parser = new SegyFormatXmlParser( segyFormatFile );
      java.util.List nodeList = parser.getListOfFieldElementNodes();
      Iterator iter = nodeList.iterator();

      nodesMap = new HashMap();
      model = new DefaultComboBoxModel();
      while ( iter.hasNext() )
      {
        Node node = ( Node ) iter.next();
        String element = parser.getFieldNameAttr( node );
        nodesMap.put( element, node );
        if( allkeys ) {
          model.addElement( element );
        }
      }

      if( !allkeys ) {

        DataSourceKeys keys = dataSet.getDatasourceKeys();

        if( keys.getCount() > 0 ) {

        for(int i=0; i < keys.getCount(); i++ ) {
          DataSourceKey key = keys.getItemByIndex(i);
          model.addElement( new String( key.getName() ) );
        }

       }
      }

    } catch ( FileNotFoundException e ) {
        JOptionPane.showMessageDialog( this, "Selected file not found" );
/*
        ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                            "Selected file not found");
*/
    } catch ( IOException io ) {
        JOptionPane.showMessageDialog( this, "Error reading selected filend" );
/*
        ErrorDialog.showErrorDialog(QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                                    "Error reading selected file",
                                    new String[] {"Possible permissions problem",
                                                  "Possible corrupted file"},
                                    new String[] {"Fix any permissions errors",
                                                  "Verify file is Ok",
                                                  "If OK, contact workbench support"});
*/
    }
    return model;
  }

  /**
   *  Create the CompoundIndexTree from the selected key name from the Specified
   *  JComboBox
   *
   * @param  selComboBox      Description of the Parameter
   * @param  SelectedKeyName  Description of the Parameter
   * @return                  CompoundIndexTree
   */
  private DataSourceKey getSelectedKey( JComboBox selComboBox, String SelectedKeyName )
  {
    DataSourceKey key = new DataSourceKey();
    int size = selComboBox.getModel().getSize();

    Node fieldNode = ( Node ) this.nodesMap.get( SelectedKeyName );

    String format = parser.getFieldFormatAttr( fieldNode );

    key.setName( SelectedKeyName );
    key.setType( SegyConstantsParser.getFormat( format ).shortValue() );

    return key;
  }

  private void setSelectedData()
  {
    this.timeSlicer.setMasterDataIndexFile( inputFileTextField.getText() );
    this.timeSlicer.setInlineKey( selectedInLineKey );
    this.timeSlicer.setXlineKey( selectedXLineKey );
  }
  /**
   *  Set CompoundIndexTree[] on the timeSlicer. This is the CompundIndexTree
   *  created by selecting the keys from the InLine and XLine JComboBox
   */
  private void setSelectedCompoundIndexTree()
  {
  }
}
