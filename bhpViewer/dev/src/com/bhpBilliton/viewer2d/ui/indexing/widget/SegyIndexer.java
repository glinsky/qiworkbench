/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui.indexing.widget;

import java.nio.ByteOrder;

import com.gwsys.seismic.indexing.*;

import java.awt.Container;

/**
 *  The SegyIndexer. An implementation of the any GUI which helps in Indexing
 *  should implement this interface.
 *
 * @author     Ruby Varghese
 * @created    May 19, 2003
 * @version    1.0
 */

public interface SegyIndexer
{

  /**
   *  Set a file name with the extension *.xgy on the SegyIndexer
   *
   * @param  xgyFile  MasterIndexFile name
   */
  public void setMasterDataIndexFile( String xgyFile );


  /**
   *  Gets the masterDataIndexFile name of the SegyIndexer
   *
   * @return    The masterDataIndexFile name with the xgy extension
   */
  public String getMasterDataIndexFile();

  /**
   *  Set a Index file name with the extension *.igx on the SegyIndexer
   *
   * @param  datFile  The new indexFile value
   */
  public void setIndexFile( String datFile );


  /**
   * Sets the XML format file
   * @param xmlFormat the xml format file
   */
  public void setXMLSegyFormatFile( String xmlFormat );

  /**
   * Gets the XML format file
   * @return the xml format file
   */
  public String getXMLSegyFormatFile( );

  /**
   *  Gets the IndexFile name of the SegyIndexer
   *
   * @return    The IndexFile name with the *.igx extension
   */
  public String getIndexFile();


  /**
   *  Set segy file names with the extension *.segy on the SegyIndexer
   *
   * @param  segyFiles  The new segyFile names
   */
  public void setSegyFiles( String[] segyFiles );


  /**
   *  Gets the SegyFile names of the SegyIndexer
   *
   * @return    The SegyFile names with the *.SEG-Y extension
   */
  public String[] getSegyFiles();

  /**
   *  Set selected Indexed keys on the SegyIndexer
   *
   * @param  keys  The new array of Indexed Keys
   */
  public void setCompoundIndexTree( CompoundIndexTree[] keys );


  /**
   *  Gets the Indexed keys of the SegyIndexer
   *
   * @return    The selected Indexed keys
   */
  public CompoundIndexTree[] getCompoundIndexTree();

  /**
   *  Set basePath of SegyFiles on the SegyIndexer
   *
   * @param  basePath  The parent Path of the selected Segy Files set on this
   *      SegyIndexer
   */
  public void setBasePathOfSegyFiles( String basePath );


  /**
   *  Gets the parent Path od the selected Segy Files set on this SegyIndexer
   *
   * @return    The basePath
   */
  public String getBasePathOfSegyFiles();

  /**
   *  Set a ByteOrder on the SegyIndexer. This could be Big-Endian or
   *  Little-Endian. By default it is Big-Endian
   *
   * @param  byteOrder  The new byteOrder value
   */
  public void setByteOrder( ByteOrder byteOrder );


  /**
   *  Gets the ByteOrder of the SegyIndexer
   *
   * @return    This could be Big-Endian or Little-Endian. By default it is
   *      Big-Endian.
   */
  public ByteOrder getByteOrder();

  /**
   *  Set a page size on the SegyIndexer
   *
   * @param  pageSize  The new pageSize. By default it is 1024
   */
  public void setPageSize( int pageSize );


  /**
   *  Gets the page size of the SegyIndexer
   *
   * @return    By default it is 1024
   */
  public int getPageSize();

  /**
   *  Sets the segyProcessingOutputCallback of the SegyIndexer
   *
   * @param  callBack  The new segyProcessingOutputCallback value
   */
  public void setSegyProcessingOutputCallback( SegyProcessingOutputCallback callBack );

  /**
   *  Gets segyProcessingOutputCallback of the SegyIndexer object
   *
   * @return    The segyProcessingOutputCallback value
   */
  public SegyProcessingOutputCallback getSegyProcessingOutputCallback();

  /**
   *  Cancel the indexing process
   */
  public void cancelIndexing();

  /**
   *  Start indexing process.Note: that you need to set the Thread before
   *  starting the Indexing Process.
   */
  public void startIndexing();

  /**
   *  Show the previous IndexingProcessorComponent control
   */
  public void showPreviousIndexingProcessorComponent();

  /**
   *  Adds the specified IndexingProcessorComponent control
   *
   * @param  panel  SegyIndexingProcessor
   */
  public void addSegyIndexingProcessorComponent( SegyIndexingProcessor panel );

  /**
   *  Removes the specified IndexingProcessorComponent control
   *
   * @param  panel  SegyIndexingProcessor
   */
  public void removeSegyIndexingProcessorComponent( SegyIndexingProcessor panel );

  /**
   *  Sets indexing thread
   *
   * @param  thread  Thread which starts the indexing process. Note: that you
   *      need to set the Thread before starting the Indexing Process.
   */
  public void setIndexingThread( Thread thread );

  /**
   *  Gets indexing thread
   *
   * @return    the indexing thread
   */
  public Thread getIndexingThread();

}
