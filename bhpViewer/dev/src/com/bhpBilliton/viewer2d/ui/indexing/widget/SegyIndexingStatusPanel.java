/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui.indexing.widget;

import javax.swing.border.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.JOptionPane;
import java.awt.event.*;


import com.bhpBilliton.viewer2d.ui.indexing.BuildIndex;
import com.gwsys.seismic.indexing.*;

import java.io.CharArrayWriter;
import java.io.PrintWriter;

/**
 *  A SegyIndexingProcessor which allows user to start the SegyIndexing process
 *  .
 *
 * @author     Ruby Varghese
 * @created    May 12, 2003
 * @version    1.0
 */

public class SegyIndexingStatusPanel extends JPanel implements SegyIndexingProcessor, SegyProcessingOutputCallback
{
  JLabel statusLabel = new JLabel();

  JTextArea statusTextArea = new JTextArea();

  JButton cancelButton = new JButton();
  JButton startButton = new JButton();
  JButton previousButton = new JButton();

  //Indexer stuff
  private SegyIndexer segyIndexer;

  /**
   *  Default Constructor for the  SegyIndexingStatusPanel
   */
  public SegyIndexingStatusPanel() {
    try {
      jbInit();
    } catch ( Exception e ) {
        e.printStackTrace();
    }
  }

  /**
   * Gets segy indexer
   * @return the indexer
   */
  public SegyIndexer getIndexer()
  {
    return segyIndexer;
  }
  /**
   *  InitializeGUI
   *
   * @throws  Exception  Description of the Exception
   */
  private void jbInit() throws Exception
  {
    this.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );
    this.setLayout( new BorderLayout( 20, 20 ) );
    JPanel overallPanel = new JPanel();
    overallPanel.setBorder( BorderFactory.createCompoundBorder( new EtchedBorder( EtchedBorder.RAISED, Color.white, new Color( 142, 142, 142 ) ), BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) ) );
    overallPanel.setLayout( new BorderLayout() );
    this.add( overallPanel, BorderLayout.CENTER );

    //Status Panel ************************************************************

    statusLabel.setText( "Status:" );
    overallPanel.add( statusLabel, BorderLayout.NORTH );

    //TextArea Panel **********************************************************

    JScrollPane statusScrollPane = new JScrollPane();
    statusTextArea.setLineWrap( true );
    statusTextArea.setWrapStyleWord( true );
    statusTextArea.setCaretPosition( statusTextArea.getDocument().getLength() );

    statusScrollPane.getViewport().add( statusTextArea, null );
    overallPanel.add( statusScrollPane, BorderLayout.CENTER );

    //Buttons Panel************************************************************

    cancelButton.setText( "Cancel" );
    cancelButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          cancelButtonActionPerformed( e );
        }
      } );
    startButton.setText( "Start" );
    startButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          startButtonActionPerformed( e );
        }
      } );
    previousButton.setText( "Previous..." );
    previousButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          previousButtonActionPerformed( e );
        }
      } );
    JPanel buttonsPanel = new JPanel();
    buttonsPanel.setLayout( new FlowLayout( FlowLayout.RIGHT ) );
    buttonsPanel.add( previousButton, null );
    buttonsPanel.add( startButton, null );
    buttonsPanel.add( cancelButton, null );

    this.add( buttonsPanel, BorderLayout.SOUTH );

  }

  /**
   *  Name of the SegyIndexingProcessor
   *
   * @return    "SegyIndexingStatusPanel"
   */
  public String toString()
  {
    return "SegyIndexingStatusPanel";
  }

  /**
   *  Sets the segyIndexer attribute of the SegyIndexingProcessor object
   *
   * @param  segyIndexer  The new segyIndexer value
   */
  public void setSegyIndexer( SegyIndexer segyIndexer )
  {
    this.segyIndexer = segyIndexer;
  }


  /**
   *  Start the IndexingProcess
   */
  public void startIndexingProcess()
  {
    Thread indexerThread = new InternalIndexer( this );
    this.segyIndexer.setIndexingThread( indexerThread );
    this.segyIndexer.startIndexing();
    indexerThread = null;
  }

  /**
   *  Stop Indexing Process
   */
  public void stopIndexingProcess()
  {
    segyIndexer.cancelIndexing();
    this.segyIndexer = null;
  }

  /**
   *  Go to the next IndexingProcess
   */
  public void goToNextIndexingProcess()
  {
    throw new UnsupportedOperationException( "This is the end of Indexing Process" );
  }

  /**
   *  Go to the previous IndexingProcess
   */
  public void goToPreviousIndexingProcess()
  {
    this.segyIndexer.showPreviousIndexingProcessorComponent();
    this.segyIndexer.removeSegyIndexingProcessorComponent( this );
    this.segyIndexer.setSegyProcessingOutputCallback( null );
    this.segyIndexer.setIndexingThread( null );
  }


  /**
   *  Action for Cancel Button Cancels the SegyIndexer.
   *
   * @param  e  ActionEvent
   */
  private void cancelButtonActionPerformed( ActionEvent e )
  {
    this.stopIndexingProcess();
  }

  /**
   *  Action for the start Button. This method starts the indexing process
   *
   * @param  e  ActionEvent
   */
  private void startButtonActionPerformed( ActionEvent e )
  {

    long maxTmpFilesSize = BuildIndex.getRequiredTempSpaceApproximation( segyIndexer.getSegyFiles(),
        segyIndexer.getCompoundIndexTree(),
        segyIndexer.getBasePathOfSegyFiles(),
        segyIndexer.getByteOrder() );

    if( !BuildIndex.checkFreeSpace( maxTmpFilesSize ) ) {
      JOptionPane.showMessageDialog(this, "It is not enough disk space for temporary files!!\nThe available disk space must be "+Long.toString( maxTmpFilesSize / 1024L )+ " Kb\nCheck the space!", "Indexer Status", JOptionPane.INFORMATION_MESSAGE );
      return;
    }

    this.segyIndexer.setSegyProcessingOutputCallback( this );
    this.startIndexingProcess();
  }

  /**
   *  Action for the previousButton. This navigates to the previous stage of
   *  IndexingProcess
   *
   * @param  e  ActionEvent
   */
  private void previousButtonActionPerformed( ActionEvent e )
  {
    this.goToPreviousIndexingProcess();
  }

  /**
   *  Adds a feature to the ProcessingMessage attribute of the
   *  SegyIndexingStatusPanel object
   *
   * @param  str  The feature to be added to the ProcessingMessage attribute
   */
  public synchronized void addProcessingMessage( String str )
  {
    this.statusTextArea.append( "\n" );
    this.statusTextArea.append( str );
  }

  /**
   *  This class enables the indexer to start on a seperate thread
   *
   * @author     Dimitiry
   * @created    May 20, 2003
   */
  private class InternalIndexer extends Thread
  {
    private SegyIndexingStatusPanel _panel;

    /**
     *  Constructor for the InternalIndexer
     *
     * @param  panel  SegyIndexingStatusPanel
     */
    public InternalIndexer( SegyIndexingStatusPanel panel )
    {
      _panel = panel;
    }

    /**
     *  Main processing method for the InternalIndexer object
     */
    public void run()
    {
      _panel.startButton.setEnabled( false );
      _panel.previousButton.setEnabled( false );

      BuildIndex index = new BuildIndex();

      try {
        index.build( _panel.segyIndexer.getMasterDataIndexFile(),
            _panel.segyIndexer.getSegyFiles(),
            _panel.segyIndexer.getIndexFile(),
            _panel.segyIndexer.getCompoundIndexTree(),
            _panel.segyIndexer.getBasePathOfSegyFiles(),
            _panel.segyIndexer.getByteOrder(),
            _panel.segyIndexer.getPageSize(), _panel,
            _panel.segyIndexer.getXMLSegyFormatFile(),
            null );
        _panel.statusTextArea.append( "Finished \n" );
        _panel.cancelButton.setText("Done");
        JOptionPane.showMessageDialog( SegyIndexingStatusPanel.this, "Segy Indexing Task is over", "Indexer Status", JOptionPane.INFORMATION_MESSAGE );
      } catch ( Exception e ) {
        _panel.statusTextArea.append( "\n=======================Fatal error============================\n" );
         CharArrayWriter c = new CharArrayWriter();
         e.printStackTrace( new PrintWriter( c ) );
        //_panel.statusTextArea.append( e.getMessage() );
        _panel.statusTextArea.append( c.toString() );
        c.close();
        JOptionPane.showMessageDialog( SegyIndexingStatusPanel.this, "Segy Indexing Task has encountered some fatal error \n Start the process again!", "Task Error", JOptionPane.ERROR_MESSAGE );

      }
      _panel.startButton.setEnabled( false );
      _panel.previousButton.setEnabled( true );

      _panel.getIndexer().setIndexingThread( null );

      _panel = null;
      index  = null;
    }
  }
}