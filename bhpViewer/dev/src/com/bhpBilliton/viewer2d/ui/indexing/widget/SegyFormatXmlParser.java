/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui.indexing.widget;

import java.io.File;
import org.w3c.dom.Document;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


import com.bhpBilliton.viewer2d.BhpViewerHelper;
//import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.gwsys.seismic.core.*;

import java.net.URL;
import java.net.URI;
import org.xml.sax.InputSource;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import javax.swing.JOptionPane;
import java.util.ArrayList;
import java.util.List;

/**
 *  An XML Parser for the SegyFormatXML file.
 *
 * @author     Ruby Varghese
 * @created    May 12, 2003
 * @version    1.0
 */

public class SegyFormatXmlParser
{
  private java.util.List fieldNodesList = new ArrayList();

  /**
   *  Constructs a SegyFormatXmlParser for the specified xml file
   *
   * @param  xmlFile  File containing the Segy xml format
   */
  public SegyFormatXmlParser( File xmlFile ) {
    try {
      InputStream is = new FileInputStream( xmlFile );
      load( is );
    } catch ( IOException io ) {
        JOptionPane.showMessageDialog( null, "Unable to Open an InputStream with the file : " + xmlFile );
/*
        ErrorDialog.showErrorDialog(QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                                    "Error reading selected file",
                                     new String[] {"Possible permissions problem",
                                                   "Possible corrupted file"},
                                     new String[] {"Fix any permissions errors",
                                                   "Verify file is Ok",
                                                   "If OK, contact workbench support"});
*/
    }
  }

  /**
   *  Constructs a SegyFormatXmlParser for the specified InputStream
   *
   * @param  is  InputStream
   */
  public SegyFormatXmlParser( InputStream is )
  {
    load( is );
  }

  /**
   *  Load the InputStream
   *
   * @param  is  InputStream
   */
  protected void load(InputStream is) {
        try {

            Element root = BhpViewerHelper.getXMLTree(is);
            Document doc = root.getOwnerDocument();

            //System.out.println( "Root element of the doc is " + doc.getDocumentElement().getNodeName() );

            NodeList listOfFields = doc.getElementsByTagName("Field");
            int totalFields = listOfFields.getLength();
            //System.out.println( "Total no of fields : " + totalFields );
            for (int s = 0; s < totalFields; s++) {
                Node fieldElementNode = listOfFields.item(s);
                fieldNodesList.add(fieldElementNode);
            }
            //end of for loop with s var
        } catch (SAXParseException err) {
            JOptionPane.showMessageDialog( null, "** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId() );
/*
            ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                "** Parsing error" + ", line " + err.getLineNumber() +
                                                ", uri " + err.getSystemId());
*/
        } catch (SAXException e) {
            JOptionPane.showMessageDialog( null, "Error in SegyFormatXmlParser" );
/*
            ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                "Error in SegyFormatXmlParser");
*/
        } catch (Throwable t) {
            JOptionPane.showMessageDialog( null, "Error in SegyFormatXmlParser" );

/*
            ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                "Error in SegyFormatXmlParser");
*/
        }

    }

  /**
   * Gets the list of Nodes with the element tag name as "Field". object
   *
   * @return The list of FieldElement Nodes
   */
  public List getListOfFieldElementNodes()
  {
    return this.fieldNodesList;
  }

  /**
   *  Gets the value of the Attribute "Name" of the element Node
   *
   * @param  fieldNode  Node the represents the Element "Field"
   * @return            The value of the attr named "Name"
   */
  public String getFieldNameAttr( Node fieldNode )
  {
    FieldElement element = new FieldElement( fieldNode );
    return element.getFieldNameAttr();
  }

  /**
   *  Gets the value of the Attribute "Format" of the element Node
   *
   * @param  fieldNode  Node the represents the Element "Field"
   * @return            The value of the attr named "Format"
   */
  public String getFieldFormatAttr( Node fieldNode )
  {
    FieldElement element = new FieldElement( fieldNode );
    return element.getFieldFormatAttr();
  }

  /**
   *  Gets the value of the Attribute "Offset" of the element Node
   *
   * @param  fieldNode  Node the represents the Element "Field"
   * @return            The value of the attr named "Offset"
   */
  public int getFieldOffsetAttr( Node fieldNode )
  {
    FieldElement element = new FieldElement( fieldNode );
    return element.getFieldOffsetAttr();
  }

  /**
   *  class to define the element "Field"
   *
   * @author     Ruby
   * @created    May 12, 2003
   */
  private class FieldElement
  {
    String name = "";
    String format = "";
    int offset = 0;

    /**
     *  Constructor a FieldElement for the specified Node
     *
     * @param  fieldNode  Node with tag name as "Field"
     */
    public FieldElement( Node fieldNode )
    {
      if ( fieldNode.getNodeType() == Node.ELEMENT_NODE )
      {
        //get attrs of "field"
        NamedNodeMap attrs = fieldNode.getAttributes();

        if ( attrs == null || attrs.getLength() == 0 )
        {
          return;
        }
        for ( int i = 0; i < attrs.getLength(); ++i )
        {
          Node node = attrs.item( i );

          if ( node.getNodeName().compareToIgnoreCase( "Name" ) == 0 )
          {
            name = node.getNodeValue().trim();
            //System.out.println( "name : " + name );
          }
          if ( node.getNodeName().compareToIgnoreCase( "Format" ) == 0 )
          {
            format = node.getNodeValue().trim();
            //System.out.println( "Format : " + format );
          }
          if ( node.getNodeName().compareToIgnoreCase( "Offset" ) == 0 )
          {
            offset = Integer.parseInt( node.getNodeValue().trim() );
            //System.out.println( "Offset : " + offset );
          }
        }
      }
    }

    /**
     *  Gets the value of the Attribute "Name" of the element Node
     *
     * @return    The value of the attr named "Name"
     */
    public String getFieldNameAttr()
    {
      return name;
    }

    /**
     *  Gets the value of the Attribute "Format" of the element Node
     *
     * @return    The value of the attr named "Format"
     */
    public String getFieldFormatAttr()
    {
      return format;
    }

    /**
     *  Gets the value of the Attribute "Offset" of the element Node
     *
     * @return    The value of the attr named "Offset"
     */
    public int getFieldOffsetAttr()
    {
      return offset;
    }
  }

}
