/*
 bhpViewer - a 2D seismic viewer
 This program module Copyright (C) Interactive Network Technologies 2006

 The following is important information about the license which accompanies this copy of
 the bhpViewer in either source code or executable versions (hereinafter the "Software").
 The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
 is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
 http://www.gnu.org/licenses/gpl.txt.
 This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with this program;
 if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 For a license to use the Software under conditions other than in a way compliant with the
 GPL or the additional restrictions set forth in this license agreement or to purchase
 support for the Software, please contact: Interactive Network Technologies, Inc.,
 2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

 All licensees should note the following:

 *        In order to compile and/or modify the source code versions of the Software,
 a user may require one or more of INT's proprietary toolkits or libraries.
 Although all modifications or derivative works based on the source code are governed by the
 GPL, such toolkits are proprietary to INT, and a library license is required to make use of
 such toolkits when making modifications or derivatives of the Software.
 More information about obtaining such a license can be obtained by contacting sales@int.com.

 *        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
 serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
 submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
 This will allow the Custodian to consider integrating such revised versions into the
 version of the Software it distributes. Doing so will foster future innovation
 and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
 the Custodian is under no obligation to include modifications or revisions in future
 distributions.

 This program module may have been modified by BHP Billiton Petroleum,
 G&W Systems Consulting Corp or other third parties, and such portions are licensed
 under the GPL.
 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
 visit http://qiworkbench.org to learn more.
 */

package com.bhpBilliton.viewer2d.ui.indexing.widget;

import javax.swing.border.*;
import java.awt.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import javax.swing.filechooser.FileFilter;
import java.io.File;

import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpBilliton.viewer2d.util.GenericFileFilter;
import com.bhpBilliton.viewer2d.util.IconResource;
import com.gwsys.seismic.gui.TraceHeaderDialog;

import java.awt.event.ActionListener;

/**
 *  A SegyIndexingProcessor which allows user to select *.segy files
 *
 * @author     Ruby Varghese
 * @created    May 12, 2003
 * @version    1.1
 * @since      Dec, 2006
 */

public class SegyFileSelectorPanel extends JPanel implements
        SegyIndexingProcessor {

    private static final long serialVersionUID = 1L;

    private JLabel titleLabel = new JLabel();

    private JLabel directoryLabel = new JLabel();

    private JTextField dirTextField = new JTextField();

    private JButton dirButton = new JButton();

    private JList selectedFilesList = new JList();

    private JButton downButton = new JButton();

    private JButton upButton = new JButton();

    private JButton removeButton = new JButton();

    private JButton cancelButton = new JButton();

    private JButton nextButton = new JButton();

    private JButton viewEBCDICButton = new JButton();

    private JButton viewTracesButton = new JButton();

    private JFileChooser fc;

    //Indexer stuff
    private SegyIndexer segyIndexer;

    /**
     *  Default Constructor for the SegyFileSelectorPanel
     */
    public SegyFileSelectorPanel() {
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
/*
            ErrorDialog.showInternalErrorDialog(Thread.currentThread()
                    .getStackTrace(), "Error in SegyFileSelectorPanel");
*/
        }
    }

    /**
     *  InitializeGUI
     *
     * @throws  Exception  Description of the Exception
     */
    private void jbInit() throws Exception {

        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        JPanel overallPanel = new JPanel();
        overallPanel.setLayout(new BorderLayout(0, 5));
        overallPanel.setBorder(BorderFactory.createCompoundBorder(
                new EtchedBorder(EtchedBorder.RAISED, Color.white, new Color(
                        142, 142, 142)), BorderFactory.createEmptyBorder(5, 5,
                        5, 5)));

        this.add(overallPanel);

        JPanel overallFileChooserPanel = new JPanel();
        overallFileChooserPanel.setLayout(new BorderLayout(0, 10));
        overallPanel.add(overallFileChooserPanel, BorderLayout.CENTER);

        //Heading Panel***********************************************************
        titleLabel.setText("Select  SEG-Y  Input  Files  for  Indexing:");
        overallPanel.add(titleLabel, BorderLayout.NORTH);

        //Directory Chooser Panel**************************************************
        JPanel dirChooserPanel = new JPanel();
        dirChooserPanel.setLayout(new BorderLayout());

        directoryLabel.setText("Choose SEG-Y Files from Dir:");

        dirButton.setIcon(IconResource.getInstance().getImageIcon(
                IconResource.OPEN24_ICON));
        dirButton.setMaximumSize(new Dimension(30, 30));
        dirButton.setMinimumSize(new Dimension(30, 30));
        dirButton.setPreferredSize(new Dimension(30, 30));
        dirButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dirButtonActionPerformed(e);
            }
        });
        dirChooserPanel.add(dirButton, BorderLayout.EAST);

        dirTextField.setText("*.Segy");
        dirChooserPanel.add(dirTextField, BorderLayout.CENTER);

        JPanel dirPanel = new JPanel();
        dirPanel.setLayout(new BorderLayout(20, 0));
        dirPanel.add(directoryLabel, BorderLayout.WEST);
        dirPanel.add(dirChooserPanel, BorderLayout.CENTER);
        overallFileChooserPanel.add(dirPanel, BorderLayout.NORTH);

        //SelectedFilesPanel*****************************************************

        JPanel selectedFilesPanel = new JPanel();
        selectedFilesPanel.setLayout(new BorderLayout());
        selectedFilesPanel.setBorder(BorderFactory
                .createCompoundBorder(BorderFactory.createEtchedBorder(
                        Color.white, new Color(142, 142, 142)), BorderFactory
                        .createEmptyBorder(5, 5, 5, 5)));

        JScrollPane selectedFilesScrollPane = new JScrollPane();
        DefaultListModel model = new DefaultListModel();
        selectedFilesList.setModel(model);
        selectedFilesScrollPane.getViewport().add(selectedFilesList, null);

        selectedFilesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        selectedFilesPanel.add(selectedFilesScrollPane, BorderLayout.CENTER);

        overallFileChooserPanel.add(selectedFilesPanel, BorderLayout.CENTER);

        //Up Down Remove Buttons Panel***************************************************
        upButton.setText("UP");
        upButton.setEnabled(false);
        upButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                upButtonActionPerformed(e);
            }
        });
        downButton.setText("Down");
        downButton.setEnabled(false);
        downButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                downButtonActionPerformed(e);
            }
        });

        removeButton.setText("Remove");
        removeButton.setEnabled(false);
        removeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                removeButtonActionPerformed(e);
            }
        });

        viewEBCDICButton.setText("View EBCDIC Header");
        viewEBCDICButton.setEnabled(false);
        viewEBCDICButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                viewEBCDICButtonActionPerformed(e);
            }
        });

        JPanel upDownPanel = new JPanel(new GridLayout(4, 1, 10, 5));
        upDownPanel.setBorder(BorderFactory.createEmptyBorder(20, 10, 20, 0));

        upDownPanel.add(upButton, null);
        upDownPanel.add(downButton, null);
        upDownPanel.add(removeButton, null);
        upDownPanel.add(viewEBCDICButton, null);

        selectedFilesPanel.add(upDownPanel, BorderLayout.EAST);

        JPanel viewTrace = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        viewTrace.setBorder(BorderFactory.createTitledBorder(
                "Check trace headers/create XML format description file"));

        viewTracesButton.setText("View Trace Headers");
        viewTracesButton.setEnabled(false);
        viewTracesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                viewTracesButtonActionPerformed();
            }
        });
        viewTrace.add(viewTracesButton);
        this.add(viewTrace);
        //Buttons Panel ***********************************************************

        JPanel buttonsPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        nextButton.setText("Next...");
        nextButton.setEnabled(false);
        nextButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                nextButtonActionPerformed(e);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cancelButtonActionPerformed(e);
            }
        });

        buttonsPanel.add(nextButton, null);
        buttonsPanel.add(cancelButton, null);

        this.add(buttonsPanel);
    }

    /**
     *  Name of the SegyIndexingProcessor
     *
     * @return    "SegyFileSelectorPanel"
     */
    public String toString() {
        return "SegyFileSelectorPanel";
    }

    /**
     *  Sets the segyIndexer attribute of the SegyIndexingProcessor object
     *
     * @param  segyIndexer  The new segyIndexer value
     */
    public void setSegyIndexer(SegyIndexer segyIndexer) {
        this.segyIndexer = segyIndexer;
    }

    /**
     *  Start the IndexingProcess
     */
    public void startIndexingProcess() {
        throw new UnsupportedOperationException(
                "Not enaough parameters to start the Indexing Process");
    }

    /**
     *  Stop Indexing Process
     */
    public void stopIndexingProcess() {
        this.segyIndexer.cancelIndexing();
        this.segyIndexer = null;
    }

    /**
     *  Go to the next IndexingProcess
     */
    public void goToNextIndexingProcess() {
        //set all the segy files selcted on the Indexer
        setSelectedFilesOnIndexer();

        //Create the next statge of Indexing
        SegyFormatSelectorPanel panel = new SegyFormatSelectorPanel(segyIndexer);

        this.segyIndexer.addSegyIndexingProcessorComponent(panel);

    }

    /**
     *  Go to the previous IndexingProcess
     */
    public void goToPreviousIndexingProcess() {
        /*
         *  this.segyIndexer.showPreviousIndexingProcessorComponent();
         *  this.segyIndexer.removeSegyIndexingProcessorComponent( this );
         *  /setsegyFiles as null on the segyIndexer
         *  this.segyIndexer.setSegyFiles( null );
         */
        throw new UnsupportedOperationException(
                "There is no prior process to this Indexing Process");
    }

    /**
     *  Action for Remove Button
     *
     * @param  e  ActionEvent
     */
    private void removeButtonActionPerformed(ActionEvent e) {
        DefaultListModel model = (DefaultListModel) this.selectedFilesList
                .getModel();
        Object selObj = selectedFilesList.getSelectedValue();
        model.removeElement(selObj);
        enableButtons();
    }

    /**
     *  Action to rearrange the elements in the JList
     *
     * @param  e  ActionEvent
     */
    private void downButtonActionPerformed(ActionEvent e) {
        DefaultListModel model = (DefaultListModel) this.selectedFilesList
                .getModel();
        int selIndex = this.selectedFilesList.getSelectedIndex();
        Object selObj = selectedFilesList.getSelectedValue();
        if (!(selIndex >= model.getSize() - 1) && (selIndex != -1)) {
            model.remove(selIndex);
            model.add(selIndex + 1, selObj);

        }
    }

    /**
     *  Action to rearrange the elements in the JList
     *
     * @param  e  ActionEvent
     */
    private void upButtonActionPerformed(ActionEvent e) {
        DefaultListModel model = (DefaultListModel) this.selectedFilesList
                .getModel();

        int selIndex = this.selectedFilesList.getSelectedIndex();
        Object selObj = selectedFilesList.getSelectedValue();
        if (!(selIndex <= 0) && (selIndex != -1)) {
            model.remove(selIndex);
            model.add(selIndex - 1, selObj);

        }
    }

    /**
     *  Action to rearrange the elements in the JList
     *
     * @param  e  ActionEvent
     */
    private void viewEBCDICButtonActionPerformed(ActionEvent e) {

        Object selObj = selectedFilesList.getSelectedValue();
        if (selObj != null) {
            // File segyFile = new File(this.dirTextField.getText()+ selObj);
            //show the ebcdicHeader in a textField
            SegyEbcdicHeaderInfoDialog dialog = new SegyEbcdicHeaderInfoDialog();
            dialog.setSegyFileName(this.dirTextField.getText() + File.separator
                    + selObj.toString());
            dialog.setSize(500, 435);
            dialog.setLocationRelativeTo(this);
            dialog.setVisible(true);
        }
    }

    /**
     *  Action for the Next Button. This navigates to the next statge of
     *  IndexingProcess
     *
     * @param  e  ActionEvent
     */
    private void nextButtonActionPerformed(ActionEvent e) {
        this.goToNextIndexingProcess();
    }

    /**
     *  Action for Cancel Button Cancels the SegyIndexer.
     *
     * @param  e  ActionEvent
     */
    private void cancelButtonActionPerformed(ActionEvent e) {
        stopIndexingProcess();
    }

    /**
     *  Action for segyFileChooser Button
     *
     * @param  e  ActionEvent
     */
    private void dirButtonActionPerformed(ActionEvent e) {
        if (fc == null) {
            // QIW Disable references to viewer3d
            fc = new JFileChooser();
            //fc = new JFileChooser(com.gwsys.viewer3d.util.ViewerUtils.getDataDirectory());
        }

        fc.setDialogTitle("Select SEG-Y Files To Index");
        fc.setMultiSelectionEnabled(true);

        String[] extensions = new String[] { "segy", ".sgy" };
        FileFilter filter = new GenericFileFilter(extensions, "*.segy");
        fc.setFileFilter(filter);

        fc.showOpenDialog(this);

        fc.approveSelection();
        File[] selFiles = fc.getSelectedFiles();
        if (selFiles.length != 0 && filter.accept(selFiles[0])) {
            //add all selected files to the SelectedFilesList
            int selFilesSize = selFiles.length;
            DefaultListModel model = (DefaultListModel) this.selectedFilesList
                    .getModel();
            for (int i = 0; i < selFilesSize; i++) {
                model.addElement(((File) selFiles[i]).getName());
            }
            this.selectedFilesList.setModel(model);
            //set the basepath
            File currentDir = fc.getCurrentDirectory();
            this.dirTextField.setText(currentDir.getAbsolutePath());
            //set the base path on the segyIndexer
            this.segyIndexer.setBasePathOfSegyFiles(currentDir
                    .getAbsolutePath());
            // QIW Disable references to viewer3d
            //com.gwsys.viewer3d.util.ViewerUtils.setDataDirectory(currentDir);
            //enable all the Buttons
            enableButtons();
        }

    }

    /**
     *  Enable the Buttons according to the required conditions
     */
    private void enableButtons() {
        int size = this.selectedFilesList.getModel().getSize();
        if (size != 0) {
            nextButton.setEnabled(true);
            removeButton.setEnabled(true);
            viewEBCDICButton.setEnabled(true);
            viewTracesButton.setEnabled(true);
            this.selectedFilesList.setSelectedIndex(0);
            if (size > 1) {
                upButton.setEnabled(true);
                downButton.setEnabled(true);
            } else {
                upButton.setEnabled(false);
                downButton.setEnabled(false);
            }
        }
        //if no files are selected/or all the files are removed
        else {
            nextButton.setEnabled(false);
            removeButton.setEnabled(false);
            viewEBCDICButton.setEnabled(false);
            viewTracesButton.setEnabled(false);
            upButton.setEnabled(false);
            downButton.setEnabled(false);
        }
    }

    /**
     *  Sets the selectedFilesOnIndexer attribute of the SegyFileSelectorPanel
     *  object
     */
    private void setSelectedFilesOnIndexer() {

        DefaultListModel listModel = (DefaultListModel) this.selectedFilesList
                .getModel();

        int noOfFiles = listModel.size();
        String[] segyFiles = new String[noOfFiles];
        for (int i = 0; i < noOfFiles; i++) {
            segyFiles[i] = (new File(listModel.get(i).toString())).getName();

        }
        this.segyIndexer.setSegyFiles(segyFiles);
    }

    /**
     * @since Dec 2006
     *
     */
    private void viewTracesButtonActionPerformed() {
        Object selObj = selectedFilesList.getSelectedValue();
        if (selObj != null) {
            String fullName = this.dirTextField.getText() + File.separator
                    + selObj.toString();
            TraceHeaderDialog dialog = new TraceHeaderDialog(fullName);
            dialog.setVisible(true);
            if (dialog.getSavedFormatFile() != null)
                segyIndexer.setXMLSegyFormatFile(dialog.getSavedFormatFile());
        }
    }
}
