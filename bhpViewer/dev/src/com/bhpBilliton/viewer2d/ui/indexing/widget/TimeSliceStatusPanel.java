/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui.indexing.widget;
import com.bhpBilliton.viewer2d.ui.indexing.BuildIndex;
import com.bhpBilliton.viewer2d.ui.indexing.TimeSlice;
import com.gwsys.seismic.indexing.CompoundIndexTree;
import com.gwsys.seismic.indexing.DataSliceType;
import com.gwsys.seismic.indexing.DataSourceKey;
import com.gwsys.seismic.indexing.SegyDataset;
import com.gwsys.seismic.indexing.SegyDatasetFactory;
import com.gwsys.seismic.indexing.SegyProcessingOutputCallback;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.PrintWriter;
import org.w3c.dom.Node;
import java.util.Iterator;
import java.util.HashMap;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;

/**
 *  A TimeSliceStatusPanel which allows user to start the SegyIndexing process.
 *
 * @author     Ruby Varghese
 * @created    May 12, 2003
 * @version    1.0
 */

public class TimeSliceStatusPanel extends JPanel implements TimeSliceProcessor, SegyProcessingOutputCallback
{
  public static final String TRANSPOSED_KEY_NAME   = "Time";
  public static final String TRANSPOSED_KEY_FORMAT = "FLOAT32";

  JLabel statusLabel = new JLabel();

  JTextArea statusTextArea = new JTextArea();

  JButton cancelButton = new JButton();
  JButton startButton = new JButton();
  JButton previousButton = new JButton();

  //Indexer stuff
  private TimeSlicer timeSlicer;

  /**
   *  Default Constructor for the penel
   */
  public TimeSliceStatusPanel() {
    try {
      jbInit();
    } catch ( Exception e ) {
        e.printStackTrace();
/*
        ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                            "Error in TimeSliceStatusPanel");
*/
    }
  }

  /**
   * Gets Time Slicer
   * @return the Time Slicer
   */
  public TimeSlicer getTimeSlicer()
  {
    return timeSlicer;
  }
  /**
   *  InitializeGUI
   *
   * @throws  Exception  Description of the Exception
   */
  private void jbInit() throws Exception
  {
    this.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );
    this.setLayout( new BorderLayout( 20, 20 ) );
    JPanel overallPanel = new JPanel();
    overallPanel.setBorder( BorderFactory.createCompoundBorder( new EtchedBorder( EtchedBorder.RAISED, Color.white, new Color( 142, 142, 142 ) ), BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) ) );
    overallPanel.setLayout( new BorderLayout() );
    this.add( overallPanel, BorderLayout.CENTER );

    //Status Panel ************************************************************

    statusLabel.setText( "Status:" );
    overallPanel.add( statusLabel, BorderLayout.NORTH );

    //TextArea Panel **********************************************************

    JScrollPane statusScrollPane = new JScrollPane();
    statusTextArea.setLineWrap( true );
    statusTextArea.setWrapStyleWord( true );
    statusTextArea.setCaretPosition( statusTextArea.getDocument().getLength() );

    statusScrollPane.getViewport().add( statusTextArea, null );
    overallPanel.add( statusScrollPane, BorderLayout.CENTER );

    //Buttons Panel************************************************************

    cancelButton.setText( "Cancel" );
    cancelButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          cancelButtonActionPerformed( e );
        }
      } );
    startButton.setText( "Start" );
    startButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          startButtonActionPerformed( e );
        }
      } );
    previousButton.setText( "Previous..." );
    previousButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          previousButtonActionPerformed( e );
        }
      } );
    JPanel buttonsPanel = new JPanel();
    buttonsPanel.setLayout( new FlowLayout( FlowLayout.RIGHT ) );
    buttonsPanel.add( previousButton, null );
    buttonsPanel.add( startButton, null );
    buttonsPanel.add( cancelButton, null );

    this.add( buttonsPanel, BorderLayout.SOUTH );

  }

  /**
   *  Name of the SegyIndexingProcessor
   *
   * @return    "SegyIndexingStatusPanel"
   */
  public String toString()
  {
    return "SegyIndexingStatusPanel";
  }

  /**
   *  Sets the TimeSlicer attribute of the TimeSlicerProcessor object
   *
   * @param  timeSlicer  The new TimeSlicer value
   */
  public void setTimeSlicer( TimeSlicer timeSlicer )
  {
    this.timeSlicer = timeSlicer;
  }


  /**
   *  Start the IndexingProcess
   */
  public void startIndexingProcess()
  {
    Thread indexerThread = new InternalIndexer( this );
    this.timeSlicer.setIndexingThread( indexerThread );
    this.timeSlicer.startIndexing();
    indexerThread = null;
  }

  /**
   *  Stop Indexing Process
   */
  public void stopIndexingProcess()
  {
    timeSlicer.cancelIndexing();
    timeSlicer.setIndexingThread( null );
    this.timeSlicer = null;
  }

  /**
   *  Go to the next IndexingProcess
   */
  public void goToNextIndexingProcess()
  {
    throw new UnsupportedOperationException( "This is the end of Indexing Process" );
  }

  /**
   *  Go to the previous IndexingProcess
   */
  public void goToPreviousIndexingProcess()
  {
    this.timeSlicer.showPreviousTimeSliceComponent();
    this.timeSlicer.removeTimeSliceProcessorComponent( this );

    //setcallback to null
    this.timeSlicer.setSegyProcessingOutputCallback( null );
    this.timeSlicer.setIndexingThread( null );
  }

  /**
   *  Action for Cancel Button Cancels the SegyIndexer.
   *
   * @param  e  ActionEvent
   */
  private void cancelButtonActionPerformed( ActionEvent e )
  {
    this.stopIndexingProcess();
  }

  /**
   *  Action for the start Button. This method starts the indexing process
   *
   * @param  e  ActionEvent
   */
  private void startButtonActionPerformed( ActionEvent e )
  {
    //set the callback on SegyIndexer
    this.timeSlicer.setSegyProcessingOutputCallback( this );
    this.startIndexingProcess();
  }

  /**
   *  Action for the previousButton. This navigates to the previous stage of
   *  IndexingProcess
   *
   * @param  e  ActionEvent
   */
  private void previousButtonActionPerformed( ActionEvent e )
  {
    this.goToPreviousIndexingProcess();
  }

  /**
   *  Adds a feature to the ProcessingMessage attribute of the
   *  SegyIndexingStatusPanel object
   *
   * @param  str  The feature to be added to the ProcessingMessage attribute
   */
  public synchronized void addProcessingMessage( String str )
  {
    this.statusTextArea.append( "\n" );
    this.statusTextArea.append( str );
  }

  /**
   *  This class enables the indexer to start on a seperate thread
   *
   * @author     Dimitry
   * @created    May 20, 2003
   */
  private class InternalIndexer extends Thread
  {
    private TimeSliceStatusPanel _panel;

    /**
     *  Constructor for the InternalIndexer
     *
     * @param  panel  TimeSliceStatusPanel
     */
    public InternalIndexer( TimeSliceStatusPanel panel )
    {
      _panel = panel;
    }

    /**
     *  Main processing method for the InternalIndexer object
     */
    public void run()
    {
      _panel.startButton.setEnabled( false );
      _panel.previousButton.setEnabled( false );

      TimeSlice timeslice = new TimeSlice();
      BuildIndex index = new BuildIndex();

      try
      {
         _panel.addProcessingMessage("Start transformation....\n");

         timeslice.setOutputCallback( _panel );

         String inputFileName      = _panel.timeSlicer.getMasterDataIndexFile();
         String outputXGYFileName  = _panel.timeSlicer.getOutputFile();
         String outputSEGYFileName = outputXGYFileName.substring(0, outputXGYFileName.lastIndexOf("."));

         outputSEGYFileName = outputSEGYFileName +".segy";

         SegyDataset dataSet = SegyDatasetFactory.getDefaultFactory().createDataset( inputFileName );

         DataSourceKey inlineKey = _panel.timeSlicer.getInlineKey();
         DataSourceKey xlineKey  = _panel.timeSlicer.getXlineKey();

         DataSourceKey key = dataSet.getDatasourceKeys().getItemByName( inlineKey.getName() );

         if( key == null ) throw new Exception("Wrong type of the "+inlineKey.getName() );

         inlineKey.setMaxValue( new String( key.getMaxValue() ) );
         inlineKey.setMinValue( new String( key.getMinValue() ) );
         inlineKey.setIncrement( new String( key.getIncrement() ) );

         key = dataSet.getDatasourceKeys().getItemByName( xlineKey.getName() );
         if( key == null ) throw new Exception("Wrong type of the "+xlineKey.getName() );

         xlineKey.setMaxValue( new String(key.getMaxValue()) );
         xlineKey.setMinValue( new String(key.getMinValue()) );
         xlineKey.setIncrement( new String(key.getIncrement()) );

         dataSet = null;

         timeslice.do_slice( inputFileName,
                             inlineKey,
                             xlineKey,
                             outputSEGYFileName );

         float sampleRate = timeslice.getInitialSampleRate();

         _panel.addProcessingMessage("Start indexing....\n");

         dataSet = SegyDatasetFactory.getDefaultFactory().createDataset( inputFileName );

         File file = new File( outputSEGYFileName );

         String dir = file.getParent() != null ? file.getParent() : "";

         String[] segyFiles         = new String[] { file.getName() };
         String outputIndexFileName = outputSEGYFileName.substring(0, outputSEGYFileName.lastIndexOf(".")) +".igx";

         CompoundIndexTree[] indexTree = new CompoundIndexTree[ 2 ];

         //create segyXMLFile
         File segyFormatFile = new File( dataSet.getSegyFormatFile() );
         SegyFormatXmlParser parser = new SegyFormatXmlParser( segyFormatFile );

         java.util.List nodeList = parser.getListOfFieldElementNodes();
         Iterator iter = nodeList.iterator();

         HashMap nodesMap = new HashMap();
         while ( iter.hasNext() )
         {
           Node node = ( Node ) iter.next();
           String element = parser.getFieldNameAttr( node );
           nodesMap.put( element, node );
         }

         Node fieldNode = ( Node ) nodesMap.get( inlineKey.getName() );

         int offset = parser.getFieldOffsetAttr( fieldNode );

         // we change format
         String format = TRANSPOSED_KEY_FORMAT;
         //parser.getFieldFormatAttr( fieldNode );

         indexTree[0] = new CompoundIndexTree( TRANSPOSED_KEY_NAME, offset, format, 0, indexTree);

         fieldNode = ( Node ) nodesMap.get( xlineKey.getName() );

         offset = parser.getFieldOffsetAttr( fieldNode );
         format = parser.getFieldFormatAttr( fieldNode );

         indexTree[1] = new CompoundIndexTree( xlineKey.getName(), offset, format, 0, indexTree);


         // Sets map mode for time slice
         index.setDataOrder( DataSliceType.DATA_MAP );
         index.setTransposedParameters( inlineKey.getName(), TRANSPOSED_KEY_NAME, Float.toString( sampleRate ) );

         File xgyFile = new File( outputXGYFileName );

         index.build( xgyFile.getName(),
            segyFiles,
            outputIndexFileName,
            indexTree,
            dir,
            dataSet.getDataFormat(),
            1024,
            _panel,
            dataSet.getSegyFormatFile(),
            null );

        _panel.statusTextArea.append( "Finished \n" );
        _panel.cancelButton.setText("Done");
        JOptionPane.showMessageDialog( TimeSliceStatusPanel.this, "Time Slice Task is over", "Time Slice Status", JOptionPane.INFORMATION_MESSAGE );
      } catch ( Exception e ) {
        _panel.statusTextArea.append( "\n=======================Fatal error============================\n" );
         CharArrayWriter c = new CharArrayWriter();
         e.printStackTrace( new PrintWriter( c ) );
        _panel.statusTextArea.append( c.toString() );
        c.close();
        JOptionPane.showMessageDialog( TimeSliceStatusPanel.this, "Time Slice Task has encountered some fatal error \n Start the process again!", "Time Slice Status", JOptionPane.INFORMATION_MESSAGE );
      }
      _panel.startButton.setEnabled( false );
      _panel.previousButton.setEnabled( true );

      _panel.getTimeSlicer().setIndexingThread( null );

      _panel      = null;
      index       = null;
      timeslice   = null;
    }
  }
}