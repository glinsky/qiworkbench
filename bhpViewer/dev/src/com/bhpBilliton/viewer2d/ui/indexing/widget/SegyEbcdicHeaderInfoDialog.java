/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui.indexing.widget;

import javax.swing.*;
import java.awt.*;
import javax.swing.border.EtchedBorder;
import java.awt.event.*;

//import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.reader.SegyReader;

import java.io.FileNotFoundException;
import java.io.CharArrayWriter;
import java.io.PrintWriter;

/**
 *  A dialog to view the EBCDIC Header info of the SegyFile
 *
 * @author     Ruby Varghese
 * @created    May 27, 2003
 * @version    1.0
 */

public class SegyEbcdicHeaderInfoDialog extends JDialog
{

  private JButton okButton = new JButton();

  private JTextArea ebcdicHeaderInfoTextArea = new JTextArea();


  /**
   *  Default constructor for SegyEbcdicHeaderInfoDialog
   *
   * @throws  HeadlessException  Description of the Exception
   */
  public SegyEbcdicHeaderInfoDialog() throws HeadlessException {
    try {
      jbInit();
    } catch ( Exception e ) {
        e.printStackTrace();
/*
        ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                            "Error in SegyEbcdicHeaderInfoDialog");
*/
    }
  }

  /**
   *  Initialize GUI
   *
   * @throws  Exception  Description of the Exception
   */
  private void jbInit() throws Exception
  {

    this.getContentPane().setLayout( new BorderLayout( 20, 20 ) );
    JPanel overallPanel = new JPanel();
    overallPanel.setBorder( BorderFactory.createCompoundBorder( new EtchedBorder( EtchedBorder.RAISED, Color.white, new Color( 142, 142, 142 ) ), BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) ) );
    overallPanel.setLayout( new BorderLayout() );

    this.getContentPane().add( overallPanel, BorderLayout.CENTER );

    //TextArea*************************************************************
    JScrollPane ebcdicViewScrollPane = new JScrollPane();
    overallPanel.add( ebcdicViewScrollPane, BorderLayout.CENTER );
    ebcdicViewScrollPane.getViewport().add( ebcdicHeaderInfoTextArea, null );

    //Button Panel**********************************************************

    JPanel buttonPanel = new JPanel();
    buttonPanel.setLayout( new FlowLayout( FlowLayout.RIGHT ) );
    okButton.setHorizontalAlignment( SwingConstants.RIGHT );
    okButton.setText( "OK" );
    okButton.addActionListener(
      new java.awt.event.ActionListener()
      {
        public void actionPerformed( ActionEvent e )
        {
          okButton_actionPerformed( e );
        }
      } );
    buttonPanel.add( okButton, null );

    this.getContentPane().add( buttonPanel, BorderLayout.SOUTH );

  }


  /**
   *  Sets the segyFileName attribute of the SegyEbcdicHeaderInfoDialog object
   *
   * @param  segyFileName  The new segyFileName value
   */
  public void setSegyFileName( String segyFileName )
  {
    try
    {
      //System.out.println( "segyFileName = " + segyFileName );
      SegyReader reader = new SegyReader( segyFileName );
      String[] headerInfo = reader.ebcdicBytesToStringArray();
      int noOfHeader = headerInfo.length;
      for ( int i = 0; i < noOfHeader; i++ )
      {
        //ebcdicHeaderInfoTextArea.append( "\n" );
        ebcdicHeaderInfoTextArea.append( headerInfo[i] );
        ebcdicHeaderInfoTextArea.append( "\n" );
      }
    }
    catch ( FileNotFoundException e )
    {
      //e.printStackTrace();
      CharArrayWriter c = new CharArrayWriter();
      e.printStackTrace( new PrintWriter( c ) );
      ebcdicHeaderInfoTextArea.append( c.toString() );
      c.close();
    }

  }

  /**
   *  Action for OKButton. This diaposes this dialog
   *
   * @param  e  ActionEvent
   */
  void okButton_actionPerformed( ActionEvent e )
  {
    this.dispose();
  }
}
