/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JComboBox;



import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.ImageIcon;

import com.bhpBilliton.viewer2d.ui.util.FileTools;
import com.bhpBilliton.viewer2d.ui.util.SelectFileCommand;
import com.bhpBilliton.viewer2d.ui.util.SelectLocalDataSourceFile;
import com.bhpBilliton.viewer2d.util.IconResource;
/**
 * @author lukem
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FileSubstitutionPanel extends JPanel {

	private JTextField oldFileNameBox = null;
	private JTextField newFileNameBox = null;
    private JButton selectFileButton = null;
	private JComboBox dataNameSelector = null;

	private JPanel textPanel = null;

	private SelectFileCommand fileSelector = null;
	private static String currentPath = null;

	private JPanel controlPanel = null;  //  @jve:decl-index=0:visual-constraint="228,76"

	public String idDLSH;
	/**
	 * This is the default constructor
	 */
	public FileSubstitutionPanel() {
		super();
		initialize();
	}

	public FileSubstitutionPanel(String id, String oldFileName) {
	    this();
	    idDLSH = id;
	    setOldFileName (oldFileName);
	    setOldFileTip (oldFileName);
	}

	public FileSubstitutionPanel(String id,String oldFileName, SelectFileCommand sfs) {
	    this(id, oldFileName);
	    fileSelector = sfs;
	}

	public FileSubstitutionPanel(String id, String oldFileName, String oldFileTip, SelectFileCommand sfs) {
	    this(id, oldFileName,sfs);
	    setOldFileTip (oldFileTip);
	}

	public void addNewFileNameListener (ActionListener al) {
	    newFileNameBox.addActionListener(al);
	}
	public void addSelectFileButtonListener (ActionListener al) {
	    selectFileButton.addActionListener(al);
	}

	/**
	 * This method initializes this
	 */
	private final void initialize() {
		this.setLayout(new BorderLayout());
		this.add(getTextPanel(), java.awt.BorderLayout.CENTER);
		this.add(getControlPanel(), java.awt.BorderLayout.EAST);
//		setCurrentPath(null);
	}
	/**
	 * This method initializes jTextField
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getOldFileNameBox() {
		if (oldFileNameBox == null) {
		    oldFileNameBox = new JTextField();
		    oldFileNameBox.setName("oldFileName");
		    oldFileNameBox.setPreferredSize(new java.awt.Dimension(150,25));
		    oldFileNameBox.setBackground(java.awt.SystemColor.control);
		    oldFileNameBox.setEditable(false);
		    oldFileNameBox.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12));
		}
		return oldFileNameBox;
	}
	/**
	 * This method initializes jTextField1
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getNewFileNameBox() {
		if (newFileNameBox == null) {
		    newFileNameBox = new JTextField();
		    newFileNameBox.setName("newFileName");
		    newFileNameBox.setPreferredSize(new java.awt.Dimension(150,25));
		    newFileNameBox.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
		    newFileNameBox.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12));
		    newFileNameBox.setEditable(false);
		    newFileNameBox.setBackground(java.awt.SystemColor.text);
		    newFileNameBox.setToolTipText(".dat file to substitute");
		}
		return newFileNameBox;
	}
	/**
	 * This method initializes jButton
	 *
	 * @return javax.swing.JButton
	 */

	private JButton getSelectFileButton() {
	    if (selectFileButton == null) {
	        selectFileButton = new JButton();
	        selectFileButton.setToolTipText("Select Pathlist File");
	        selectFileButton.setPreferredSize(new java.awt.Dimension(25,25));
//	        selectFileButton.setIcon(new ImageIcon(getClass().getResource(DIRECTORY_ICON)));
	        selectFileButton.setIcon(IconResource.getInstance().getImageIcon(IconResource.DIRECTORY_ICON));
	        selectFileButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                selectNewFileName();
            }});
	    }
	    return selectFileButton;
	}

	public void selectNameOption () {
		if (dataNameSelector.getSelectedIndex()==0) {
			//selected the cancel option
			setNewFileName("");
			setNewFileTip("");
			firePropertyChange("CancelSubstition", idDLSH,"");
		} else {
			String newDataName = (String) dataNameSelector.getSelectedObjects()[0];
			firePropertyChange("NewDataName", idDLSH, newDataName);
		}
	}

	public void selectNewFileName() {
	    if (fileSelector == null) {
	        fileSelector = new SelectLocalDataSourceFile();
	    }

	    String newFileName = fileSelector.selectFile(FileSubstitutionPanel.currentPath);
	    if (newFileName!=null && newFileName.length()!=0) {
	        setNewFileName(FileTools.getFileNameFromPath(newFileName));
	        setNewFileTip(newFileName);
	        FileSubstitutionPanel.currentPath = FileTools.getPathFromFilePath(newFileName);
	        firePropertyChange("NewFileName", idDLSH,getNewFileName());
	    }
	}

    public void setNewFileName(String newFileName) {
        newFileNameBox.setText(newFileName);
    }
    public void setOldFileName(String oldFileName) {
        oldFileNameBox.setText(oldFileName);
    }
    public String getNewFileName() {
        return newFileNameBox.getToolTipText();
    }
    public String getOldFileName() {
        return oldFileNameBox.getToolTipText();
    }
    public void setNewFileTip(String newFileTip) {
    	newFileNameBox.setToolTipText(newFileTip);
    }
    public void setOldFileTip(String oldFileTip) {
    	oldFileNameBox.setToolTipText(oldFileTip);
    }
    public void setNameComboEnabled (boolean value) {
    	dataNameSelector.setEnabled(value);
    	if (value) {
    		dataNameSelector.setBackground(new Color(255,255,255));
    	} else {
    		dataNameSelector.setBackground(java.awt.SystemColor.text);
    	}
    }
    public void setNameComboList (String[] names) {
    	dataNameSelector.setEnabled(false);
    	dataNameSelector.removeAllItems();
    	if (names.length>0) {
	    	dataNameSelector.addItem ("<< cancel >>");
	    	for (int i=0;i<names.length;i++) {
	    		dataNameSelector.addItem (names[i]);
	    	}
    	}
    }
    public void setSelectedName (String name) {
		int selectID = 0;
		for (int i=0;i<dataNameSelector.getItemCount();i++) {
			if (name.equalsIgnoreCase((String) dataNameSelector.getItemAt(i))) {
				selectID = i;
				break;
			}
		}
		if (selectID>0) {
			dataNameSelector.setSelectedIndex(selectID);
		} else {
			//select the first entry
			selectID = dataNameSelector.getItemCount();
			if (selectID > 0) {
				dataNameSelector.setSelectedIndex(1);
			}
		}
    }
	/**
	 * This method initializes jPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getTextPanel() {
		if (textPanel == null) {
			textPanel = new JPanel();
			GridLayout gridLayout5 = new GridLayout();
			textPanel.setLayout(gridLayout5);
			gridLayout5.setColumns(2);
			gridLayout5.setRows(0);
			gridLayout5.setHgap(4);
			textPanel.add(getOldFileNameBox(), null);
			textPanel.add(getNewFileNameBox(), null);
		}
		return textPanel;
	}
	public String getCurrentPath() {
		return currentPath;
	}

	public static void setCurrentPath(String newPath) {
		if (newPath != null && newPath.length()!=0) {
			FileSubstitutionPanel.currentPath = newPath;
		} else {
			FileSubstitutionPanel.currentPath = ".";
		}
	}
	/**
	 * This method initializes jComboBox
	 *
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getDataNameSelector() {
		if (dataNameSelector == null) {
			dataNameSelector = new JComboBox();
			dataNameSelector.setPreferredSize(new java.awt.Dimension(150,25));
			dataNameSelector.setFont(new Font("Dialog", Font.PLAIN, 12));
			dataNameSelector.setEnabled(false);
			dataNameSelector.setMaximumRowCount(12);
			dataNameSelector.setToolTipText("Select dataset name (filename)");
			dataNameSelector.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (dataNameSelector.isEnabled())
					selectNameOption();
            }});
        }
		return dataNameSelector;
	}
	/**
	 * This method initializes jPanel1
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getControlPanel() {
		if (controlPanel == null) {
			controlPanel = new JPanel();
			controlPanel.setLayout(new BorderLayout());
			controlPanel.add(getSelectFileButton(), java.awt.BorderLayout.EAST);
			controlPanel.add(getDataNameSelector(), java.awt.BorderLayout.CENTER);
		}
		return controlPanel;
	}

	public void setWarningState (boolean state) {
    	if (state) {
    		dataNameSelector.setBackground(new Color(255,128,128));
    	} else {
    		dataNameSelector.setBackground(java.awt.SystemColor.text);
    	}
	}
   }  //  @jve:decl-index=0:visual-constraint="10,10"
