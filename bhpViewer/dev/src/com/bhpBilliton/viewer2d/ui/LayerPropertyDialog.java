/*
 * LayerPropertyDialog.java
 *
 * Created on January 7, 2008, 3:16 PM
 */

package com.bhpBilliton.viewer2d.ui;

import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpSeismicLayer;
import com.bhpBilliton.viewer2d.BhpSeismicTableModel;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.BhpWindow;
import com.bhpBilliton.viewer2d.data.GeneralDataSource;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * @author folsw9
 */
public class LayerPropertyDialog extends JDialog {
    private static final Logger logger = Logger.getLogger(LayerPropertyDialog.class.toString());
    
    public LayerPropertyDialog(OpenGeneralPanel _dataPanel, BhpWindow win, 
            BhpViewerBase viewer, BhpLayer layer,
            int dataType, boolean isMap, GeneralDataSource dataSource) {
        super();
        
        setLayout(new BorderLayout());
        add(_dataPanel, BorderLayout.CENTER);
        
        add(getBtnPanel(win, viewer, layer, dataType, isMap, dataSource, _dataPanel), BorderLayout.SOUTH);
        
        setSize(600,300);
    }
    
    private JPanel getBtnPanel(BhpWindow win, 
            BhpViewerBase viewer, BhpLayer layer,
            int dataType, boolean isMap, GeneralDataSource dataSource, 
            OpenGeneralPanel _dataPanel) {
        JPanel panel = new JPanel();
        
        panel.setLayout(new FlowLayout());
        
        JButton okBtn = new JButton("OK");
        okBtn.addActionListener(new OkActionListener(win, viewer, layer, 
                dataType, this, isMap, dataSource, _dataPanel));
        
        panel.add(okBtn);
        
        JButton cancelBtn = new JButton("Cancel");
        cancelBtn.addActionListener(new CancelActionListener(this));
        panel.add(cancelBtn);
        
        return panel;
    }
    
    private class CancelActionListener implements ActionListener {
        private JDialog dialog;
        public CancelActionListener(JDialog dialog) {
            this.dialog = dialog;
        }
        
        public void actionPerformed(ActionEvent ae) {
            dialog.dispose();
        }
    }
    
    private class OkActionListener implements ActionListener {
        private BhpWindow _win;
        private BhpViewerBase _viewer;
        private BhpLayer _layer;
        private int _dataType;
        private JDialog dialog;
        private boolean _isMap;
        GeneralDataSource _dataSource;
        OpenGeneralPanel _dataPanel;
        private String propertyString = "";
        private boolean layerSync = false;
        private boolean propertySync = false;
        private String _dataSourceName = "";
        
        public OkActionListener(BhpWindow win, BhpViewerBase viewer, 
                BhpLayer layer,
                int dataType, 
                JDialog dialog, 
                boolean isMap, 
                GeneralDataSource dataSource,
                OpenGeneralPanel _dataPanel) {
            this._win = win;
            this._viewer = viewer;
            this._layer = layer;
            this._dataType = dataType;
            this.dialog = dialog;
            this._isMap = isMap;
            this._dataSource = dataSource;
            this._dataPanel = _dataPanel;
            this._dataSourceName = _dataSource.getDataSourceName();
        }
        
        public void actionPerformed(ActionEvent ae) {
            BhpSeismicTableModel parameter = _dataPanel.getTableModelForNewLayer();
            boolean continueLoad = true;
            
            if (!(_dataType == BhpViewer.BHP_DATA_TYPE_EVENT) && (_isMap))
                continueLoad = parameter.checkMaxTraceNumber(_viewer);
            
            if (!continueLoad) {
                String errorMsg = "Cannot add seismic/model layer to map view - checkMaxTraceNumber() failed.";
                logger.warning(errorMsg);
                JOptionPane.showMessageDialog(_viewer,errorMsg);
                dispose();
                return;
            }
            
            String propertyName = "";
            boolean propertySync = false;
            boolean layerSync = false;
            String[] selectedEvents = null;
            
            if (_dataType == BhpViewer.BHP_DATA_TYPE_MODEL) {
                layerSync = ((OpenModelPanel)_dataPanel).getLayerSyncValue();
                propertySync = ((OpenModelPanel)_dataPanel).getPropertySyncValue();
                propertyName = ((OpenModelPanel)_dataPanel).getSelectedPropertyName();
                if ((_isMap) && (((OpenModelPanel)_dataPanel).getSelectedLayerName() != null))
                    propertyName = propertyName + ":" +
                            ((OpenModelPanel)_dataPanel).getSelectedLayerName();
            } else if (_dataType == BhpViewer.BHP_DATA_TYPE_EVENT) {
                propertySync = ((OpenEventPanel)_dataPanel).getPropertySyncValue();
                selectedEvents = ((OpenEventPanel)_dataPanel).getSelectedEventNames();
                propertyName = ((OpenEventPanel)_dataPanel).getSelectedEventName();
            }
            
            if (!_isMap) {
                if (_dataType == BhpViewer.BHP_DATA_TYPE_EVENT && selectedEvents != null) {
                    if (!(_dataSourceName.equals(com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_BHPSU))) {
                        // non bhpsu data, must have a seismic layer as reference
                        if (_layer == null || !(_layer instanceof BhpSeismicLayer)) {
                            JOptionPane.showMessageDialog(_viewer,"Select Seismic Layer First!");
                            dispose();
                            return;
                        }
                    }
                    for (int i=0; i<selectedEvents.length; i++) {
                        BhpSeismicTableModel theParameter = parameter;
                        
                        if (i != 0)
                            theParameter = (BhpSeismicTableModel) parameter.clone();
                        
                        _win.createXVLayer(_viewer, 
                                _dataSourceName,
                                _dataSource.getDataName(), 
                                _dataSource.getDataPath(),
                                selectedEvents[i], 
                                propertySync,
                                theParameter, 
                                _layer, 
                                _dataType,
                                ((OpenEventPanel)_dataPanel).getSampleEventShape());
                    }
                } else {
                    _win.createXVLayer(_viewer, 
                            _dataSourceName,
                            _dataSource.getDataName(), 
                            _dataSource.getDataPath(),
                            propertyName, 
                            propertySync,
                            parameter,
                            _layer, 
                            _dataType, 
                            null);
                }
            } else {
                _win.createBhpMapLayer(_viewer, 
                        _dataSourceName,
                        _dataSource.getDataName(), 
                        _dataSource.getDataPath(),
                        propertyName, 
                        propertySync, 
                        layerSync,
                        parameter, 
                        _layer, 
                        _dataType);
            }
            dispose();
        }
    }
}