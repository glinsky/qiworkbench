/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpMapLayer;
import com.bhpBilliton.viewer2d.BhpMapRasterizer;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.data.BhpDataReader;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.core.DataChooser;
import com.gwsys.seismic.core.SeismicWorkflow;
import com.gwsys.seismic.util.SeismicColorMap;
import com.gwsys.util.colorbar.ColorBar;
import com.gwsys.util.colorbar.ColorBarDialog;
import com.gwsys.util.colorbar.ColorChangeListener;
import com.gwsys.util.colorbar.ColorChangedEvent;
import com.gwsys.util.colorbar.ColormapIO;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This panel enables viewing and changing map-related
 *               settings of a <code>{@link BhpMapLayer}</code>.
 *               This includes axis direction, image transpose,
 *               lock aspect ratio, color map settings.
 *               This panel is usually used in the
 *               <code>{@link BhpSLayerPropertyDialog}</code>
 *               with other panels as an addition to
 *               <code>{@link BhpPropertyPipelinePanel}</code>.
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpMapPipelinePanel extends JPanel {
    private static final String[] _AXISLABELTEXT = { " Axis Start",
            " Axis End", " Axis Direction" };

    private BhpMapLayer _layer;

    private JTextField _colorStartField;

    private JTextField _colorEndField;

    private JTextField _xStartField;

    private JTextField _xEndField;

    private JTextField _yStartField;

    private JTextField _yEndField;

    private JCheckBox _xDirCheck;

    private JCheckBox _yDirCheck;

    private JCheckBox _transposeCheck;

    private JCheckBox _interpolateCheck;

    private JLabel[] _xLabels;

    private JLabel[] _yLabels;

    private JPanel _localSubsetPanel;

    private double _originalColorStart;

    private double _originalColorEnd;

    private ColormapIO _colormapManager;

    private BhpViewerBase _viewer;

    /**
     * Constructs a new instance.
     */
    public BhpMapPipelinePanel(ColormapIO cmanager, BhpViewerBase v) {
        super();
        _colormapManager = cmanager;
        _viewer = v;
        buildMapAttPanelGUI();
    }

    /**
     * Gets the panel for data sub-selection. <br>
     * In <code>{@link OpenDataPanel}</code>, users can
     * select to read in only part of the whole data set.
     * However, for all the data brought in, users can
     * choose to view only a subset of it. This panel
     * enables viewing and changing the setting of this
     * sub-selection.
     */
    public JPanel getLocalSubsetPanel() {
        return _localSubsetPanel;
    }

    /**
     * Initialize the panel with a
     * <code>{@link BhpLayer}</code>. <br>
     * This method retrieves values from
     * the layer to fill the fields in its GUI.
     */
    public void initWithLayer(BhpMapLayer layer) {
        _layer = layer;
        String xkey = layer.getParameter().getMapHorizontalKeyName();
        String ykey = layer.getParameter().getVerticalName();
        for (int i = 0; i < 3; i++) {
            _xLabels[i].setText(xkey + _AXISLABELTEXT[i]);
            _yLabels[i].setText(ykey + _AXISLABELTEXT[i]);
        }
        _transposeCheck.setSelected(layer.isTransposeImage());
        _xDirCheck.setSelected(layer.isReversedHorizontalDirection());
        _yDirCheck.setSelected(layer.getReversedSampleOrder());

        DataChooser selector = layer.getPipeline().getDataLoader()
                .getDataSelector();
        _yStartField.setText("" + selector.getStartSampleValue());
        _yEndField.setText("" + selector.getEndSampleValue());
        _xStartField.setText("" + selector.getPrimaryKeyStart());
        _xEndField.setText("" + selector.getPrimaryKeyEnd());

        BhpMapRasterizer rasterizer = (BhpMapRasterizer) (_layer.getPipeline()
                .getTraceRasterizer());
        double minValue = layer.getPipeline().getDataLoader().getDataReader()
                .getMetaData().getMinimumAmplitude();
        double maxValue = layer.getPipeline().getDataLoader().getDataReader()
                .getMetaData().getMaximumAmplitude();
        _originalColorStart = rasterizer.getTranslatedColorMapStartValue(
                minValue, maxValue);
        _originalColorEnd = rasterizer.getTranslatedColorMapEndValue(minValue,
                maxValue);
        java.text.NumberFormat formatter = java.text.NumberFormat.getInstance();
        formatter.setMaximumFractionDigits(4);
        formatter.setGroupingUsed(false);
        _colorStartField.setText("" + formatter.format(_originalColorStart));
        _colorEndField.setText("" + formatter.format(_originalColorEnd));

    }

    /**
     * apply the changes to the
     * <code>{@link BhpLayer}</code>. <br>
     * This method retrieves values from
     * its GUI and apply it to the layer.
     * @return a flag indicating what has been changed.
     */
    public int apply() {
        int changeFlag = 0;
        boolean selectorChanged = false;
        boolean readerChanged = false;

        BhpMapLayer mlayer = (BhpMapLayer) _layer;
        SeismicWorkflow pipeline = mlayer.getPipeline();
        BhpDataReader reader = (BhpDataReader) pipeline.getDataLoader()
                .getDataReader();
        DataChooser selector = pipeline.getDataLoader().getDataSelector();

        if (_transposeCheck.isSelected() != mlayer.isTransposeImage()) {
            mlayer.setTransposeImage(_transposeCheck.isSelected());
            changeFlag = changeFlag | BhpViewer.MAPSETTING_FLAG;
        }
        if (_xDirCheck.isSelected() != mlayer.isReversedHorizontalDirection()) {
            mlayer.setReversedHorizontalDirection(_xDirCheck.isSelected());
            changeFlag = changeFlag | BhpViewer.MAPSETTING_FLAG;
        }
        if (_yDirCheck.isSelected() != mlayer.getReversedSampleOrder()) {
            //reader.setReversedDirection(_yDirCheck.isSelected());
            mlayer.setReversedSampleOrder(_yDirCheck.isSelected());
            changeFlag = changeFlag | BhpViewer.MAPSETTING_FLAG;
            readerChanged = true;
        }
        try {
            double newStart = Double.parseDouble(_yStartField.getText());
            if (newStart != selector.getStartSampleValue()) {
                selector.setStartSampleValue(newStart);
                changeFlag = changeFlag | BhpViewer.TIMERANGE_FLAG;
                selectorChanged = true;
            }
        } catch (Exception ex3) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "BhpMapPipelinePanel.apply invalid y start value");
        }
        try {
            double newEnd = Double.parseDouble(_yEndField.getText());
            if (newEnd != selector.getEndSampleValue()) {
                selector.setEndSampleValue(newEnd);
                changeFlag = changeFlag | BhpViewer.TIMERANGE_FLAG;
                selectorChanged = true;
            }
        } catch (Exception ex4) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "BhpMapPipelinePanel.apply invalid y end value");
        }
        try {
            double newValue = Double.parseDouble(_xStartField.getText());
            if (newValue != selector.getPrimaryKeyStart()) {
                selector.setPrimaryKeyStart(newValue);
                changeFlag = changeFlag | BhpViewer.MAPSETTING_FLAG;
                selectorChanged = true;
            }
        } catch (Exception ex5) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "BhpMapPipelinePanel.apply invalid x start value");
        }
        try {
            double newValue = Double.parseDouble(_xEndField.getText());
            if (newValue != selector.getPrimaryKeyEnd()) {
                selector.setPrimaryKeyEnd(newValue);
                changeFlag = changeFlag | BhpViewer.MAPSETTING_FLAG;
                selectorChanged = true;
            }
        } catch (Exception ex6) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "BhpMapPipelinePanel.apply invalid x end value");
        }

        BhpMapRasterizer rasterizer = (BhpMapRasterizer) (_layer.getPipeline()
                .getTraceRasterizer());
        //        double colorStart = rasterizer.getColorMapStartValue();
        //        double colorEnd = rasterizer.getColorMapEndValue();
        double minValue = _layer.getPipeline().getDataLoader().getDataReader()
                .getMetaData().getMinimumAmplitude();
        double maxValue = _layer.getPipeline().getDataLoader().getDataReader()
                .getMetaData().getMaximumAmplitude();
        try {
            double value = Double.parseDouble(_colorStartField.getText());
            if (value != _originalColorStart) {
                double newStart = rasterizer.getReverseTranslatedValue(value,
                        minValue, maxValue);
                //double newStart = -1 + 2*(value-minValue)/(maxValue-minValue);
                if (newStart != rasterizer.getColorMapStartValue()) {
                    rasterizer.setColorMapStartValue(newStart);
                    _originalColorStart = value;
                    changeFlag = changeFlag | BhpViewer.COLORINTER_FLAG;
                }
            }
        } catch (Exception ex1) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "BhpMapPipelinePanel.apply invalid start value");
        }
        try {
            double value = Double.parseDouble(_colorEndField.getText());
            if (value != _originalColorEnd) {
                double newEnd = rasterizer.getReverseTranslatedValue(value,
                        minValue, maxValue);
                //double newEnd = -1 + 2*(value-minValue)/(maxValue-minValue);
                if (newEnd != rasterizer.getColorMapEndValue()) {
                    rasterizer.setColorMapEndValue(newEnd);
                    _originalColorEnd = value;
                    changeFlag = changeFlag | BhpViewer.COLORINTER_FLAG;
                }
            }
        } catch (Exception ex2) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "BhpMapPipelinePanel.apply invalid end value");
        }

        if (readerChanged) {
            pipeline.invalidate(reader);
        } else if (selectorChanged) {
            pipeline.invalidate(selector);
        }
        /*
         BhpMapSegyReader reader = (BhpMapSegyReader)
         (_layer.getPipeline().getSeismicReader());
         if (reader.getReverseX() != _xDirCheck.isSelected()) {
         reader.setReverseX(_xDirCheck.isSelected());
         changeFlag = changeFlag | BhpViewer.MAPSETTING_FLAG;
         }
         if (reader.getReverseY() != _yDirCheck.isSelected()) {
         reader.setReverseY(_yDirCheck.isSelected());
         changeFlag = changeFlag | BhpViewer.MAPSETTING_FLAG;
         }

         try {
         double newValue = Double.parseDouble(_xStartField.getText());
         if (newValue != reader.getXStart())
         reader.setXStart(newValue);
         changeFlag = changeFlag | BhpViewer.MAPSETTING_FLAG;
         }
         catch (Exception ex3) {
         System.out.println("BhpMapPipelinePanel.apply invalid x start value");
         }
         try {
         double newValue = Double.parseDouble(_xEndField.getText());
         if (newValue != reader.getXEnd())
         reader.setXEnd(newValue);
         changeFlag = changeFlag | BhpViewer.MAPSETTING_FLAG;
         }
         catch (Exception ex4) {
         System.out.println("BhpMapPipelinePanel.apply invalid x end value");
         }
         try {
         double newValue = Double.parseDouble(_yStartField.getText());
         if (newValue != reader.getYStart())
         reader.setYStart(newValue);
         changeFlag = changeFlag | BhpViewer.MAPSETTING_FLAG;
         }
         catch (Exception ex5) {
         System.out.println("BhpMapPipelinePanel.apply invalid y start value");
         }
         try {
         double newValue = Double.parseDouble(_yEndField.getText());
         if (newValue != reader.getYEnd())
         reader.setYEnd(newValue);
         changeFlag = changeFlag | BhpViewer.MAPSETTING_FLAG;
         }
         catch (Exception ex6) {
         System.out.println("BhpMapPipelinePanel.apply invalid secondary end value");
         }
         */
        return changeFlag;
    }

    private void buildMapAttPanelGUI() {
        JPanel panel11 = new JPanel(new GridLayout(2, 2));
        _colorStartField = new JTextField();
        _colorEndField = new JTextField();
        panel11.add(new JLabel("Colormap Min"));
        panel11.add(_colorStartField);
        panel11.add(new JLabel("Colormap Max"));
        panel11.add(_colorEndField);

        JButton colormapButton = new JButton("Edit Colormap ...");
        colormapButton.addActionListener(new MyColormapButtonListener());

        Box box1 = new Box(BoxLayout.Y_AXIS);
        box1.add(panel11);
        box1.add(Box.createVerticalStrut(10));
        box1.add(colormapButton);
        box1.add(Box.createVerticalGlue());

        JPanel panel1 = new JPanel(new FlowLayout());
        panel1.setBorder(new TitledBorder("Color Attribute"));
        panel1.add(box1);

        _xStartField = new JTextField();
        _xStartField.setPreferredSize(new Dimension(150, 20));
        _xEndField = new JTextField();
        _yStartField = new JTextField();
        _yEndField = new JTextField();
        _xDirCheck = new JCheckBox("Reversed");
        _yDirCheck = new JCheckBox("Reversed");
        _transposeCheck = new JCheckBox("Transpose");
        _interpolateCheck = new JCheckBox("Interpolate");
        _interpolateCheck.setEnabled(false);
        _xLabels = new JLabel[3];
        _yLabels = new JLabel[3];
        for (int i = 0; i < 3; i++) {
            _xLabels[i] = new JLabel("Horizontal" + _AXISLABELTEXT[i]);
            _yLabels[i] = new JLabel("Vertical" + _AXISLABELTEXT[i]);
        }

        JPanel lsp = new JPanel(new GridLayout(4, 2));
        lsp.setBorder(new TitledBorder("Local Subset"));
        lsp.add(_xLabels[0]);
        lsp.add(_xStartField);
        lsp.add(_xLabels[1]);
        lsp.add(_xEndField);
        lsp.add(_yLabels[0]);
        lsp.add(_yStartField);
        lsp.add(_yLabels[1]);
        lsp.add(_yEndField);
        _localSubsetPanel = new JPanel();
        Box lspBox = new Box(BoxLayout.Y_AXIS);
        lspBox.add(Box.createVerticalStrut(15));
        lspBox.add(lsp);
        _localSubsetPanel.add(lspBox);

        JPanel panel3 = new JPanel(new GridLayout(3, 2));
        panel3.setBorder(new TitledBorder("Display Attribute"));
        panel3.add(_xLabels[2]);
        panel3.add(_xDirCheck);
        panel3.add(_yLabels[2]);
        panel3.add(_yDirCheck);
        panel3.add(_transposeCheck);
        panel3.add(new JLabel(""));
        //panel3.add(_interpolateCheck);
        //panel3.add(new JLabel(""));

        Box rightBox = new Box(BoxLayout.Y_AXIS);
        rightBox.add(Box.createVerticalStrut(15));
        rightBox.add(panel3);
        rightBox.add(Box.createVerticalStrut(10));
        rightBox.add(panel1);
        rightBox.add(Box.createVerticalGlue());

        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        //this.add(Box.createHorizontalGlue());
        //this.add(leftBox);
        this.add(Box.createHorizontalGlue());
        this.add(rightBox);
        this.add(Box.createHorizontalGlue());
    }

    private class MyColormapButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            SeismicColorMap cmap = _layer.getPipeline().getTraceRasterizer()
                    .getColorMap();
            Color[] ramp = new Color[cmap.getDensityColorSize()];
            Color[] bgr = new Color[1];

            for (int i = 0; i < cmap.getDensityColorSize(); i++) {
                ramp[i] = cmap.getColor(i);
            }
            bgr[0] = cmap.getBackground();

            ColorBarDialog editor = new ColorBarDialog();
            editor.getColorBar().setColorArray(ramp);
            editor.addColorChangeListener(new ColormapListener());
            editor.setVisible(true);
        }
    }

    private class ColormapListener implements ColorChangeListener {

        public void colormapChanged(ColorChangedEvent event) {
            ColorBar colors = event.getColorBar();
            SeismicColorMap cmap = _layer.getPipeline().getTraceRasterizer().getColorMap();
            //cmap.setColorRamp(colors.getColorArray());
            Color[] ramp = colors.getColorArray();
            cmap.setNotify(false);

            cmap.setSize(ramp.length + SeismicColorMap.DEFAULT_WIGGLE_COLOR_SIZE);

            cmap.setDensityColorSize(ramp.length);

            cmap.setNotify(true);
            int rsize = ramp.length;

            for (int i=0; i<rsize; i++) {

                if (i == rsize-1)
                    cmap.setColor(i, ramp[i], true);

                else
                    cmap.setColor(i, ramp[i], false);
                cmap.setInterpolationPoint(i, false);
            }
            if (_layer!=null)
            _layer.getBhpWindow().getBhpPlot().updateColorMap(ramp);
        }

    }
}
