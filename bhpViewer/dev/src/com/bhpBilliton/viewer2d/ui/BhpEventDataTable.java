/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import javax.swing.*;
import javax.swing.JOptionPane;
import javax.swing.table.*;

import com.bhpBilliton.viewer2d.BhpEventLayer;
import com.bhpBilliton.viewer2d.BhpSeismicLayer;
//import com.bhpBilliton.viewer2d.util.ErrorDialog;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This table is used to display the actual values of the
 *               event. The table is editable, so that users can change
 *               the event by changing the values in the table.<br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpEventDataTable extends JTable {
    private static final String[] _COLUMNNAMES = {"Index", "Data"};

    private EventDataTableModel _dataTableModel;
    private boolean _dataTableChanged;

    public BhpEventDataTable() {
        super();
        _dataTableModel = new EventDataTableModel();
        this.setModel(_dataTableModel);
        getColumnModel().getColumn(0).setPreferredWidth(90);
        getColumnModel().getColumn(0).setMaxWidth(90);
        getColumnModel().setColumnMargin(10);
        MyCellEditor editor = new MyCellEditor(new JTextField());
        setDefaultEditor(new String().getClass(), editor);
    }

    // table displays vertical reading
    // the values retrieved from the event layer is model value
    public void initWithLayer(BhpEventLayer layer) {
        cleanupTable();
        if (layer != null) {
            double[] values = layer.getEventValues();
/*
            BhpSeismicLayer seismic = layer.getRefSeismicLayer();
            double[] vsettings = seismic.getParameter().getVerticalSettings();
            if (seismic != null) {
                for (int i=0; i<values.length; i++)
                    values[i] = values[i] + vsettings[0];
            }
*/
            _dataTableModel.setEventDataValuesInModel(values);
        }
    }

    public void applyToLayer(BhpEventLayer layer) {
        if (_dataTableChanged) {
            double[] values = _dataTableModel.getEventDataValuesInModel();
            BhpSeismicLayer seismic = layer.getRefSeismicLayer();
            if (seismic != null) {
                double[] vsettings = seismic.getParameter().getVerticalSettings();
                for (int i=0; i<values.length; i++) {
                    values[i] = values[i] - vsettings[0];
                }
            }
            layer.setEventValues(values);
        }
    }

    public void acceptTableChange() {
        TableCellEditor editor = getCellEditor();
        if (editor != null) {
            editor.stopCellEditing();
        }
    }

    private void cleanupTable() {
        _dataTableChanged = false;
        _dataTableModel.getDataVector().removeAllElements();
    }

    private class MyCellEditor extends DefaultCellEditor {
        public MyCellEditor(JTextField t) {
            super(t);
        }

        public Component getTableCellEditorComponent(JTable table, Object value,
                            boolean isSelected, int row, int column) {
            ((JTextField)getComponent()).setText(value.toString());
            ((JTextField)getComponent()).selectAll();
            return getComponent();
        }
    }

    private class EventDataTableModel extends DefaultTableModel {
        public EventDataTableModel() {
            super(_COLUMNNAMES, 0);
        }

        public void setValueAt(Object value, int row, int column) {
            if (value.toString().length() == 0) {
                return;
            }

            if (column == 1) {
                try {
                    Double valueObject = new Double(value.toString());
                    super.setValueAt(valueObject, row, column);
                    _dataTableChanged = true;
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null,
                            "invalid double " + value.toString(),
                            "Internal Error", JOptionPane.ERROR_MESSAGE);
/*
                    ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                        "invalid double " + value.toString());
*/
                    return;
                }
            }
        }

        public boolean isCellEditable(int row, int col) {
            if (col < 1) {
                return false;
            }
            return true;
        }

        public Class getColumnClass(int c) {
            switch(c) {
                case 0: return (new Integer(0).getClass());    // index
                case 1: return (new String("").getClass());    // value
                default: return (new String("").getClass());
            }
        }

        public void setEventDataValuesInModel(double[] values) {
            Object[] rowData = new Object[2];
            for (int i=0; i<values.length; i++) {
                rowData[0] = new Integer(i);
                rowData[1] = new Double(values[i]);
                this.addRow(rowData);
            }
        }

        public double[] getEventDataValuesInModel() {
            double[] values = new double[this.getRowCount()];
            for (int i=0; i<values.length; i++) {
                values[i] = Double.parseDouble(this.getValueAt(i, 1).toString());
            }
            return values;
        }
    }
}
