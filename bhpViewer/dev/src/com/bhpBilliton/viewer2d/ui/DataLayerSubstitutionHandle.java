/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/

package com.bhpBilliton.viewer2d.ui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.bhpBilliton.viewer2d.BhpViewerBase;



public class DataLayerSubstitutionHandle {
	/**
	 */
	private DataLayerHandle oldHandle;
	private DataLayerHandle newHandle;
	private boolean OK;
	private BhpViewerBase viewer;

	public DataLayerSubstitutionHandle(DataLayerHandle oldHandle, DataLayerHandle newHandle, BhpViewerBase viewer) {
		this.oldHandle = oldHandle;
		this.newHandle = newHandle;
		this.viewer = viewer;
	}
	public DataLayerSubstitutionHandle(DataLayerHandle oldHandle,BhpViewerBase viewer) {
		this.oldHandle = oldHandle;
		this.newHandle = new DataLayerHandle(viewer);
		this.viewer = viewer;
	}
	public DataLayerSubstitutionHandle(String oldName, String oldPathlist,BhpViewerBase viewer) {
		this.oldHandle = new DataLayerHandle(oldName, oldName, oldPathlist,viewer);
		this.newHandle = new DataLayerHandle(viewer);
	}
	public DataLayerHandle getNewHandle() {
		return newHandle;
	}
	public void setNewHandle(DataLayerHandle newHandle) {
		this.newHandle = newHandle;
		this.OK = true;
	}
	public void cancelSubstitution () {
		this.newHandle = new DataLayerHandle(viewer);
		this.OK = false;
	}
	public boolean isValid () {
		return OK;
	}
	public DataLayerHandle getOldHandle() {
		return oldHandle;
	}
	public void setOldHandle(DataLayerHandle oldHandle) {
		this.oldHandle = oldHandle;
	}

	public String getIdentifier() {
		return oldHandle.getIdentifier();
	}

	public String getOldName () {
		return (oldHandle.getName());
	}
	public String getOldPathlist () {
		return (oldHandle.getPathlist());
	}
	public String getNewName () {
		return (newHandle.getName());
	}
	public String getNewPathlist () {
		return (newHandle.getPathlist());
	}
	public List getMissingKeys() {
		List missingKeys = new ArrayList();
		Map newHeaders = newHandle.extractHeaderKeys();
		Map oldHeaders = oldHandle.extractHeaderKeys();
		for (Iterator iter = oldHeaders.keySet().iterator();iter.hasNext();) {
			Object key = iter.next();
			if (newHeaders.containsKey(key)) {
//				System.out.println ("Key found in substituted map [" + (String) key +"]");
			} else {
				String theMissing = (String) key + '$' + newHandle.getShortened();
				missingKeys.add(theMissing);
//				System.out.println ("Key not found [" + (String) key +"]");
			}
		}
		return missingKeys;
	}
}
