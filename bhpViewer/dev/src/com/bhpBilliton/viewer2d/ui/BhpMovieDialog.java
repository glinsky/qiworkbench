/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.Timer;
import javax.swing.border.BevelBorder;

import com.bhpBilliton.viewer2d.BhpMovieManager;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpBilliton.viewer2d.util.IconResource;

import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.client.util.GenericFileFilter;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This dialog is for viewing and changing the movie
 *               settings of the viewer. <br><br>
 * Copyright:    Copyright (c) 2001-2006 <br>
 * @author Synthia Kong
 * @author Gil Hansen
 * @version 1.1
 */

public class BhpMovieDialog extends JFrame {
/*
    //private JCheckBox _recordingCheck;
    private JTextField _maxStepField;
    private JTextField _fileField;
*/
    private BhpViewerBase _viewer;
    private BhpMovieManager _mmanager;
    private ActionListener _autoAct;

    private JToolBar _toolbar;
    //private JButton _playButton;
    private Timer _playTimer;
    private int _frameIndex;
    private int _playDirection;

    private BhpMovieOptionDialog _optionDialog = null;
    private BhpMovieEditDialog _editDialog = null;
    private AutoCaptureDialog _autoDialog = null;

    private ImageIcon _prefIcon = null;
    private ImageIcon _playIcon = null;
    private ImageIcon _helpIcon = null;
    private ImageIcon _stopIcon = null;
    private ImageIcon _closeIcon = null;
    private ImageIcon _rewindIcon = null;
    private ImageIcon _frewindIcon = null;
    private ImageIcon _forwardIcon = null;
    private ImageIcon _fforwardIcon = null;
    private ImageIcon _playNowIcon = null;

    public BhpViewerBase getViewer() {
        return _viewer;
    }

    /**
     * Constructs a new dialog and sets up the GUI.
     */
    public BhpMovieDialog(BhpViewerBase viewer, ActionListener autoAct) {
        //super(viewer, "Movie", false);
        super("Movie");
        _viewer = viewer;
        _autoAct = autoAct;
        _mmanager = viewer.getMovieManager();
        loadIcons();
        buildMovieDialogGUI();
        setSize(800, 600);

        //buildExtraDialogs(viewer);
    }

    /**
     * Gets related information of movie settings
     * and fill the fields of GUI. <br>
     */
    public synchronized void setMovieSettings() {
        _frameIndex = 0;
        _playDirection = 1;
        ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                changeImageFrame(_playDirection);
            }
        };
        if (_playTimer==null ) {
            _playTimer = new Timer(_mmanager.getFrameSpeed(), taskPerformer);
        } else {
            _playTimer.setDelay(_mmanager.getFrameSpeed());
            if (_playTimer.isRunning()) {
                _playTimer.restart();
            }
        }
    }

    private void buildMovieDialogGUI() {
        buildMenubar();
        buildToolbar();
        this.getContentPane().add(_toolbar, BorderLayout.NORTH);
        this.getContentPane().add(buildContentPanel(), BorderLayout.CENTER);
        //this.getContentPane().add(buildControlPanel(), BorderLayout.SOUTH);
    }

    private JPanel buildContentPanel() {
        JPanel displayPanel = new MyMovieDisplayPanel();
        displayPanel.setBorder(new BevelBorder(BevelBorder.RAISED));
        return displayPanel;
    }

    private void buildMenubar() {
        JMenuBar menubar = new JMenuBar();
        JMenu fmenu = new JMenu("File");
        JMenu emenu = new JMenu("Edit");
        menubar.add(fmenu);
        menubar.add(emenu);
        this.setJMenuBar(menubar);

        JMenuItem miExportJPEG = new JMenuItem("Save As Multiple JPEG Files ...");
        miExportJPEG.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveImageFiles();
            }
        });
        fmenu.add(miExportJPEG);
        JMenuItem miExportAVI = new JMenuItem("Save As AVI File ...");
        miExportAVI.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveVideoFile();
            }
        });
        miExportAVI.setEnabled(false);
        fmenu.add(miExportAVI);
        JMenuItem miClose = new JMenuItem("Close");
        miClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                stopMovie();
                hideDialog();
            }
        });
        fmenu.add(miClose);

        JMenuItem miFrames = new JMenuItem("Frames ...");
        miFrames.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showEditDialog();
            }
        });
        emenu.add(miFrames);
        JMenuItem miOptions = new JMenuItem("Preferences ...");
        miOptions.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showOptionDialog();
            }
        });
        emenu.add(miOptions);
        JMenuItem miAuto = new JMenuItem("Automatic Capture ...");
        miAuto.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showAutoDialog();
            }
        });
        emenu.add(miAuto);
    }

    private void buildToolbar() {
        _toolbar = new JToolBar();
        _toolbar.setBorder(new javax.swing.border.EtchedBorder());

        JButton btnfb = new JButton(_frewindIcon);
        btnfb.setToolTipText("Go to the previous frame");
        btnfb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                changeImageFrame(-1);
            }
        });
        JButton btnb = new JButton(_rewindIcon);
        btnb.setToolTipText("Play movie backward");
        btnb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                playMovie(-1);
            }
        });
        JButton btnStop = new JButton(_stopIcon);
        btnStop.setToolTipText("Stop auto play");
        btnStop.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                stopMovie();
            }
        });
        JButton btnf = new JButton(_forwardIcon);
        btnf.setToolTipText("Play movie forward");
        btnf.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                playMovie(1);
            }
        });
        JButton btnff = new JButton(_fforwardIcon);
        btnff.setToolTipText("Go to the next frame");
        btnff.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                changeImageFrame(1);
            }
        });
        JButton btnHelp = new JButton(_helpIcon);
        btnHelp.setToolTipText("Help information");
        btnHelp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                showHelp();
            }
        });

        JButton btnClose = new JButton(_closeIcon);
        btnClose.setToolTipText("Close the movie window");
        btnClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                stopMovie();
                hideDialog();
            }
        });

        _toolbar.add(btnfb);
        _toolbar.add(btnb);
        _toolbar.add(btnStop);
        _toolbar.add(btnf);
        _toolbar.add(btnff);
        _toolbar.addSeparator();
        _toolbar.add(btnHelp);
        _toolbar.add(Box.createHorizontalGlue());
        _toolbar.add(btnClose);
    }

    private void loadIcons() {
        try {
            IconResource instance = IconResource.getInstance();
            _prefIcon = instance.getImageIcon(IconResource.PREFERENCE_ICON);
            _helpIcon = instance.getImageIcon(IconResource.CSHSM_ICON);
            _playIcon = instance.getImageIcon(IconResource.PLAY_ICON);
            _stopIcon = instance.getImageIcon(IconResource.STOP_ICON);
            _closeIcon = instance.getImageIcon(IconResource.CLOSE_ICON);
            _rewindIcon = instance.getImageIcon(IconResource.REWIND_ICON);
            _frewindIcon = instance.getImageIcon(IconResource.FREWIND_ICON);
            _forwardIcon = instance.getImageIcon(IconResource.FORWARD_ICON);
            _fforwardIcon = instance.getImageIcon(IconResource.FFORWARD_ICON);
            _playNowIcon = instance.getImageIcon(IconResource.PLAYNOW_ICON);
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "BhpMovieDialog.constructor failed to load images");
        }
    }

    private void showHelp() {
        try {
            BhpViewerBase.broker.setCurrentID("tools_movie");
            BhpViewerBase.broker.setDisplayed(true);
        }
        catch (Exception e) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "Couldn't show movie dialog help");
        }
    }

    private void hideDialog() {
        this.setVisible(false);
    }

    private synchronized Image getImageFrame() {
        try {
            int imageNumber = _mmanager.getImageNumber();
            if (_frameIndex>=0 && _frameIndex<imageNumber) {
                return ((Image) _mmanager.getImageOfIndex(_frameIndex));
            }
            return null;
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "Failed to get image " + _frameIndex);
           return null;
        }
    }

    public synchronized void setImageFrame(int newIndex) {
        if (_playTimer.isRunning())
            {
            this.stopMovie();
            }
        if (newIndex == _frameIndex) {
            return;
        }
//        if (newIndex < 0) newIndex = 0;
//        int imageNumber = _mmanager.getImageNumber();
//        if (newIndex >= imageNumber)
//            newIndex = imageNumber - 1;
        _frameIndex = newIndex;
        this.repaint();
    }

    public int getMaxStep() {
        return _mmanager.getMaxStep();
    }
    public void setMaxStep(int maxStep) {
        _mmanager.setMaxStep(maxStep);
    }

    private synchronized void changeImageFrame(int dindex) {
        int imagesSize = _mmanager.getImageNumber();
        if (imagesSize == 0) {
            return;
        }
        int newIndex = _frameIndex + dindex;
        if (dindex > 0) {
            newIndex = newIndex % imagesSize;
        }
        else {
            if (newIndex < 0) {
                newIndex = imagesSize - 1;
            }
        }
        _frameIndex = newIndex;
        this.repaint();
    }

    private void playMovie(int direction) {
        //setImageFrame(0);
        _playDirection = direction;
        _playTimer.setDelay(_mmanager.getFrameSpeed());
        if (_playTimer.isRunning())
            {
            _playTimer.restart();
            }
        else
            {
            _playTimer.start();
            }
    }

    private void stopMovie() {
        if (_playTimer.isRunning())
            {
            _playTimer.stop();
            }
    }

    public void startAutomaticCapture() {
        this.setVisible(false);
        _autoAct.actionPerformed(null);
    }

    private void showEditDialog() {
        if (_editDialog == null)
            {
            _editDialog = new BhpMovieEditDialog(this, _mmanager);
            }
        _editDialog.initEditDialog();
        _editDialog.setVisible(true);
        double x = getLocation().getX();
        double y = getLocation().getY();
        x = x - 200;
        if (x < 0) {
            x = 0;
        }
        _editDialog.setLocation((int)x, (int)y);
    }

    private void showAutoDialog() {
        if (_autoDialog == null)
            {
            _autoDialog = new AutoCaptureDialog(this, this);
            }
        _autoDialog.initAutoSettings();
        _autoDialog.setVisible(true);
    }

    private void showOptionDialog() {
        if (_optionDialog == null)
            {
            _optionDialog = new BhpMovieOptionDialog(this, _mmanager);
            }
        _optionDialog.initOptionSettings();
        _optionDialog.setVisible(true);
    }

    private void saveVideoFile() {
      // TODO Implement local/remote IO services
/* qiwb deprecates
      JFileChooser fchooser = new JFileChooser(_viewer.getPropertyManager().getProperty("bhpFilecfgPath"));
      String[] extensions = {".mov", ".MOV"};
      fchooser.addChoosableFileFilter(new GenericFileFilter(extensions,"QuickTime file (*.mov)"));
      fchooser.setDialogTitle("Save as video file");
      fchooser.setMultiSelectionEnabled(false);

      int reFchooser = fchooser.showSaveDialog(this);
*/
        ArrayList list = new ArrayList();
        //1st element the parent GUI object
        list.add(_viewer);
        //2nd element is the dialog title
        list.add("Save as video file");

        // 3rd element is a list that contains current directory to start with
        //and a flag (yes or no) indicating if a already remembered directory
        //should be used instead
        ArrayList lst = new ArrayList();
        lst.add(_viewer.getAgent().getMessagingManager().getProject());
        lst.add("yes");
        list.add(lst);
        //4th element is the file filter
        String[] extensions = {".mov", ".MOV"};
        list.add(new GenericFileFilter(extensions,"QuickTime file (*.mov)"));
        //5th element is the navigation flag
        list.add(true);
        //6th element is the producer component descriptor
        list.add(_viewer.getAgent().getMessagingManager().getMyComponentDesc());
        //7th element is the type of file chooser either Open or Save
        list.add(QIWConstants.FILE_CHOOSER_TYPE_SAVE);
        //8th element is the message command
        list.add(QIWConstants.SAVE_MOVIE_VIDEO_ACTION_BY_2D_VIEWER);
        //9th element is the default pretext for the file name if user fails to pretext it. If no pretext is required then use empty String
        list.add("");
        //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
        list.add("");
        //11th element is the target tomcat url where the file chooser chooses
        list.add(_viewer.getAgent().getMessagingManager().getTomcatURL());
        try{
        _viewer.getAgent().doFileChooser(list);
        }catch(Exception ex){
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "Failed to start file chooser service");
        }

      ///FileChooserDialog fchooser = new FileChooserDialog(_viewer, "Save as video file", _viewer.getMsgMgr());
      ///String[] extensions = {".mov", ".MOV"};
      ///fchooser.addChoosableFileFilter(new GenericFileFilter(extensions,"QuickTime file (*.mov)"));
//      int reFchooser = fchooser.showSaveDialog(this);
      ///int reFchooser = fchooser.showSaveDialog();

      ///if(reFchooser == JFileChooser.APPROVE_OPTION) {
//        String fname = fchooser.getSelectedFile().getAbsolutePath();
        ///String fname = fchooser.getSelectedFileAbsolutePathname();
        ///File file = new File(fname);
        ///if (file.exists()) {
          ///int reConfirm = JOptionPane.showConfirmDialog(this,"Overwrite the existing file?","File overwriting confirmation",
          ///                                              JOptionPane.YES_NO_OPTION);
          ///if(reConfirm == JOptionPane.NO_OPTION) {
            ///JOptionPane.showMessageDialog(this, "Failed saving QuickTime file.");
            ///return;
          ///}
          ///else
            ///file.delete();
        ///}
        ///try {
        ///  _mmanager.saveVideoFile(fname);
        ///  String msg = "Save QuickTime file " + fname;
        ///  _viewer.getStatusBar().showMessage(BhpViewer.INFO_MESSAGE, msg);
        ///}
        ///catch (Exception ex) {
        ///  String msg = "Failed saving QuickTime file " + fname;
        ///  _viewer.getStatusBar().showMessage(BhpViewer.ERROR_MESSAGE, msg);
        ///  System.out.println("Failed to save QuickTime file " + fname);
        ///  System.out.println("    " + ex.toString());
        ///}
      ///}
      //_mmanager.saveVideoFile(fname);
    }

    private void saveImageFiles() {
    /* qiwb deprecates
      JFileChooser fchooser = new JFileChooser(_viewer.getPropertyManager().getProperty("bhpFilecfgPath"));
      String[] extensions = {".jpg", ".JPG"};
      fchooser.addChoosableFileFilter(new GenericFileFilter(extensions,"JPEG file (*.jpg)"));
      fchooser.setDialogTitle("Save as multiple JPEG files");
      fchooser.setMultiSelectionEnabled(false);
    */
        ArrayList list = new ArrayList();
        //1st element the parent GUI object
        list.add(_viewer);
        //2nd element is the dialog title
        list.add("Save as multiple JPEG files");
        //3rd element is a list that contains current directory to start with
        //and a flag (yes or no) indicating if a already remembered directory
        //should be used instead
        ArrayList lst = new ArrayList();
        lst.add(_viewer.getAgent().getMessagingManager().getProject());
        lst.add("yes");
        list.add(lst);

        //4th element is the file filter
        String[] extensions = {".jpg", ".JPG"};
        list.add(new GenericFileFilter(extensions,"JPEG file (*.jpg)"));
        //5th element is the navigation flag
        list.add(true);
        //6th element is the producer component descriptor
        list.add(_viewer.getAgent().getMessagingManager().getMyComponentDesc());
        //7th element is the type of file chooser either Open or Save
        list.add(QIWConstants.FILE_CHOOSER_TYPE_SAVE);
        //8th element is the message command
        list.add(QIWConstants.SAVE_MOVIE_IMAGE_ACTION_BY_2D_VIEWER);
        //9th element is the default pretext for the file name if user fails to pretext it. If no pretext is required then use empty String
        list.add("");
        //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
        list.add("");
        //11th element is the target tomcat url where the file chooser chooses
        list.add(_viewer.getAgent().getMessagingManager().getTomcatURL());
        try{
        _viewer.getAgent().doFileChooser(list);
        }catch(Exception ex){
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "Failed to start file chooser service");
        }



      ///FileChooserDialog fchooser = new FileChooserDialog(_viewer, "Save as multiple JPEG files", _viewer.getMsgMgr());
      ///String[] extensions = {".jpg", ".JPG"};
      ///fchooser.addChoosableFileFilter(new GenericFileFilter(extensions,"JPEG file (*.jpg)"));

//      int reFchooser = fchooser.showSaveDialog(this);
      ///int reFchooser = fchooser.showSaveDialog();
      ///if(reFchooser == JFileChooser.APPROVE_OPTION) {
//        String fname = fchooser.getSelectedFile().getAbsolutePath();
        ///String fname = fchooser.getSelectedFileAbsolutePathname();
        //File file = new File(fname);
        ///int reConfirm = JOptionPane.showConfirmDialog(this,"Images will be saved as " + fname+"_0000.jpg, " +
        ///                                              fname+"_0001.jpg ...","Please Confirm", JOptionPane.YES_NO_OPTION);
        ///if(reConfirm == JOptionPane.NO_OPTION) {
        ///  JOptionPane.showMessageDialog(this, "Failed saving JPEG files.");
        ///  return;
        ///}
        ///try {
        ///  _mmanager.saveImageFiles(fname);
        ///  String msg = "Save multiple JPEG files " + fname;
        ///  _viewer.getStatusBar().showMessage(BhpViewer.INFO_MESSAGE, msg);
        ///}
        ///catch (Exception ex) {
        ///  String msg = "Failed saving JPEG files " + fname;
        ///  _viewer.getStatusBar().showMessage(BhpViewer.ERROR_MESSAGE, msg);
        ///  System.out.println("Failed to save JPEG file " + fname);
        ///  System.out.println("    " + ex.toString());
        ///}
      ///}
    }

    private class MyMovieDisplayPanel extends JPanel {
        public void paintComponent(Graphics g) {
            super.paintComponent(g);    // paint background
            Image imageFrame = getImageFrame();
            if (imageFrame != null)
                g.drawImage(imageFrame, 0, 0, this);
        }
    }
}
