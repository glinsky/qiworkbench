/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.bhpBilliton.viewer2d.AbstractPlot;
import com.bhpBilliton.viewer2d.BhpEventLayer;
import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpMapLayer;
import com.bhpBilliton.viewer2d.BhpPlotMV;
import com.bhpBilliton.viewer2d.BhpPlotXV;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.BhpWindow;
import com.bhpBilliton.viewer2d.util.ErrorDialog;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This dialog is used to view and change the
 *               annotation settingsfor a
 *               <code>{@link AbstractPlot}</code>, either
 *               <code>{@link BhpPlotXV}</code>, or
 *               <code>{@link BhpPlotMV}</code>.
 *               All the layers displayed in the plot is listed in
 *               a JCombobox, plus "No Annotation". Users can choose
 *               anyone of the layers so that annotation will be
 *               drawn according to it, or choose "No Annotation".
 *               In the case of <code>{@link BhpPlotXV}</code>,
 *               this dialog has 3 panels in its tabbed pane,
 *               <code>{@link BhpTraceAnnPanel}</code>
 *               for horizontal axis setting,
 *               <code>{@link BhpSampleAnnPanel}</code>
 *               for vertical axis setting, and
 *               <code>{@link BhpGeneralAnnPanel}</code>
 *               for miscellaneous annotation setting. <br>
 *               In the case of <code>{@link BhpPlotMV}</code>,
 *               this dialog has only 2 panels in its tabbed pane,
 *               <code>{@link BhpMapAnnPanel}</code>
 *               for general axes setting, and
 *               <code>{@link BhpGeneralAnnPanel}</code>
 *               for miscellaneous annotation setting. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpAnnotationDialog extends JDialog {

    private BhpWindow _bhpWindow;

    private JTabbedPane _tab;
    private BhpTraceAnnPanel _traceAnnPanel;
    private BhpSampleAnnPanel _sampleAnnPanel;
    private BhpGeneralAnnPanel _generalAnnPanel;
    private BhpMapAnnPanel _mapAnnPanel;

    private JComboBox _layerField;

    BhpViewerBase viewer;

    /**
     * Constructor which taks a
     * <code>{@link BhpViewerBase}</code>
     * as the parent of the dialog.
     */
    public BhpAnnotationDialog(BhpViewerBase viewer) {
      // Change super call since parent no longer a Frame
      super();
      setTitle("Annotation Editor");
      setModal(false);
      setLocation(viewer.getMyLocation().x + 10,viewer.getMyLocation().y + 10);
      this.viewer = viewer;
      //super(viewer, "Annotation Editor", false);
      //_viewer = viewer;
      setupGUI();
    }

    /**
     * Initialize the panel with the
     * <code>{@link BhpWindow}</code>. <br>
     * This method retrieves values from
     * the BhpWindow to fill the fields in its GUI.
     */
    public void initWithBhpWindow(BhpWindow win) {
        if (win == null) {
            JOptionPane.showMessageDialog(viewer,"Please select a window to perform the action.");
            return;
        }
        _bhpWindow = win;
        _layerField.removeAllItems();
        _layerField.addItem("No Annotation");
        BhpLayer layer = null;
        BhpLayer axisLayer = null;
        Object[] views = win.getBhpPlot().getMainView().getAllBhpLayers();
        for (int i=0; i<views.length; i++) {
            layer = (BhpLayer) views[i];
            if ((layer instanceof BhpEventLayer) &&
                (layer.getPipeline().getDataLoader().getDataReader().getDataFormat()==null)) {
                continue;
            }
            int layerId = layer.getIdNumber();
            _layerField.addItem(layerId + " " + layer.getLayerName());
            if (layerId == win.getBhpPlot().getAxisAssociateLayer()) {
                axisLayer = layer;
                _layerField.setSelectedItem(layerId + " " + layer.getLayerName());
            }
        }
        //_layerField.setSelectedItem("" + win.getBhpPlot().getAxisAssociateLayer());

        //_tab.removeAll();
        // remove all but the first tab for Misc
        while(_tab.getTabCount() > 1) {
            _tab.removeTabAt(1);
        }
        //_tab.add("Misc.", _generalAnnPanel);
        _generalAnnPanel.initWithBhpWindow(_bhpWindow);
        if (_bhpWindow.getWindowType() == BhpViewer.WINDOW_TYPE_XSECTION) {
            _tab.add("Horizontal", _traceAnnPanel);
            _tab.add("Vertical", _sampleAnnPanel);
            BhpPlotXV plotXV = (BhpPlotXV) (_bhpWindow.getBhpPlot());
            _traceAnnPanel.initWithBhpWindow(plotXV);
            _sampleAnnPanel.initWithBhpWindow(plotXV);
        }
        else {
            _tab.add("Axes", _mapAnnPanel);
            _mapAnnPanel.initWithBhpWindow(_bhpWindow.getBhpPlot());
        }
        updateSettings(axisLayer);
    }

    /**
     * apply the changes of annotation settings to the
     * <code>{@link BhpWindow}</code>. <br>
     * This method will retrieve values from
     * its GUI and update the annotation.
     */
    private void apply() {
        if (_bhpWindow == null) {
            return;
        }
        AbstractPlot plot = _bhpWindow.getBhpPlot();
        try {
            boolean layerIdChanged = false;
            int lid = -1;
            String selString = _layerField.getSelectedItem().toString();
            if (!selString.equals("No Annotation")) {
                lid = Integer.parseInt(selString.substring(0, selString.indexOf(" ")));
            }
            if (lid != plot.getAxisAssociateLayer()) {
                _bhpWindow.setAxisAssociateLayer(lid);
                layerIdChanged = true;
/*
                _generalAnnPanel.initWithBhpWindow(_bhpWindow);
                if (_bhpWindow.getBhpPlot() instanceof BhpPlotXV) {
                    BhpPlotXV plotXV = (BhpPlotXV)(_bhpWindow.getBhpPlot());
                    _traceAnnPanel.initWithBhpWindow(plotXV);
                    _sampleAnnPanel.initWithBhpWindow(plotXV);
                }
                else {
                    _mapAnnPanel.initWithBhpWindow(_bhpWindow.getBhpPlot());
                }
                _hSyncField.setText(plot.getHAxesSyncName());
*/
            }
//            else {
            if (lid == -1) {
                return;
            }

            boolean mchange = _generalAnnPanel.updateBhpWindow();
            boolean tchange = false;
            boolean schange = false;
            boolean mapchange = false;
            if (plot instanceof BhpPlotXV) {
                tchange = _traceAnnPanel.updateBhpWindow();
                schange = _sampleAnnPanel.updateBhpWindow();
                if (layerIdChanged || tchange) {
                    plot.updateHAxes();
                }
                if (layerIdChanged || schange) {
                    plot.updateVAxes();
                }
                if (layerIdChanged || mchange) {
                    plot.updateMiscAnnotation();
                }
            }
            else {
                mapchange = _mapAnnPanel.updateBhpWindow();
                if (layerIdChanged || mapchange) {
                    plot.updateHAxes();
                    plot.updateVAxes();
                    plot.updateMiscAnnotation();
                }
                else if (mchange) {
                    plot.updateMiscAnnotation();
                }
            }
            if (layerIdChanged || tchange || schange || mchange || mapchange) {
                plot.invalidate();
                plot.validate();
            }
//            }
        }
        catch (Exception ex) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "BhpAnnotationDialog.apply Exception");
        }
    }

    private void updateSettings(BhpLayer layer) {
        if (_bhpWindow.getBhpPlot() instanceof BhpPlotXV) {
            _traceAnnPanel.initWithLayer(layer);
        }
        else {
            _generalAnnPanel.initWithMapLayer((BhpMapLayer)layer,_bhpWindow.getBhpPlot());
        }
        if (layer == null) {
            _tab.setEnabled(false);
            _traceAnnPanel.setAllComponentEnabled(false);
            _sampleAnnPanel.setAllComponentEnabled(false);
            _generalAnnPanel.setAllComponentEnabled(false);
            _mapAnnPanel.setAllComponentEnabled(false);
        }
        else {
            _tab.setEnabled(true);
            _traceAnnPanel.setAllComponentEnabled(true);
            _sampleAnnPanel.setAllComponentEnabled(true);
            _generalAnnPanel.setAllComponentEnabled(true);
            _mapAnnPanel.setAllComponentEnabled(true);
        }
    }

    private void hideDialog() {
        _bhpWindow = null;
        this.setVisible(false);
    }

    private void setupGUI() {
        this.getContentPane().add(buildContentPanel(), BorderLayout.CENTER);
        this.getContentPane().add(buildControlPanel(), BorderLayout.SOUTH);
    }

    private JPanel buildContentPanel() {
        _traceAnnPanel = new BhpTraceAnnPanel();
        _sampleAnnPanel = new BhpSampleAnnPanel();
        _generalAnnPanel = new BhpGeneralAnnPanel();
        _mapAnnPanel = new BhpMapAnnPanel();

        _tab = new JTabbedPane();
        //_tab.add("Trace", _traceAnnPanel);
        //_tab.add("Sample", _sampleAnnPanel);
        _tab.add("Misc.", _generalAnnPanel);

        JPanel p1 = new JPanel(new GridLayout(1, 2));
        _layerField = new JComboBox();
        _layerField.setEditable(false);
        p1.add(new JLabel("    Annotated Layer"));
        p1.add(_layerField);

        _layerField.addActionListener(new LayerActionListener());

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(_tab, BorderLayout.CENTER);
        panel.add(p1, BorderLayout.NORTH);
        return panel;
    }

    private JPanel buildControlPanel() {
        JButton okb = new JButton("Ok");
        okb.addActionListener(new OkListener());
        JButton apb = new JButton("Apply");
        apb.addActionListener(new ApplyListener());
        JButton clb = new JButton("Cancel");
        clb.addActionListener(new CancelListener());
        JButton helpb = new JButton("Help");
        helpb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                showHelp();
            }
        });
        JPanel panel = new JPanel(new FlowLayout());
        panel.add(okb);
        panel.add(apb);
        panel.add(clb);
        panel.add(helpb);
        return panel;
    }

    private void showHelp() {
        try {
            BhpViewerBase.broker.setCurrentID("window_annotation");
            BhpViewerBase.broker.setDisplayed(true);
        }
        catch (Exception e) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "Couldn't show help for annotation dialog.");
        }
    }

    private class LayerActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (_layerField.getSelectedItem() == null) {
                return;
            }
            String selString = _layerField.getSelectedItem().toString();
            BhpLayer axisLayer = null;
            if (!selString.equals("No Annotation")) {
                try {
                    int layerId = Integer.parseInt(selString.substring(0, selString.indexOf(" ")));
                    axisLayer = _bhpWindow.getBhpPlot().getBhpLayer(layerId);
                }
                catch (Exception ex) {
                    ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                   "BhpAnnotationDialog cannot find layer " + selString);
                    axisLayer = null;
                }
            }
            updateSettings(axisLayer);
        }
    }

    private class OkListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            apply();
            hideDialog();
        }
    }

    private class ApplyListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            apply();
        }
    }

    private class CancelListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            hideDialog();
        }
    }
}
