/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.bhpBilliton.viewer2d.BhpEventLayer;
import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpLayerEvent;
import com.bhpBilliton.viewer2d.BhpMapLayer;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.BhpWindow;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.util.colorbar.ColormapIO;
import com.gwsys.gw2d.view.MultipleViewContainer;
import com.gwsys.util.colorbar.ColorBarDialog;
/**
 * Title:        BHP Viewer <br><br>
 * Description:  This dialog is used to view and modify properties of a
 *               <code>{@link BhpLayer}</code>.
 *               It consists of <code>{@link OpenDataPanel}</code>,
 *               <code>{@link BhpPropertyGeneralPanel}</code>, and
 *               <code>{@link BhpPropertyPipelinePanel}</code>.
 *               For the four tabs in the dialog:<br>
 *               "Dataset" is for data selection using OpenDataPanel; <br>
 *               "Local Subset" is for data sub-selection with the
 *               local subset panel of BhpPropertyPipelinePanel; <br>
 *               "Display Parameters" is for general layer display
 *               and pipeline settings using BhpPropertyPipelinePanel; <br>
 *               "Synchronization" is for layer synchronization settings
 *               with BhpPropertyGeneralPanel.
 *               <br><br>
 *
 * Copyright:    Copyright (c) 2001-2006 <br>
 * @author Synthia Kong
 * @author Gil Hansen
 * @version 1.1
 */

public class BhpSLayerPropertyDialog extends JDialog {

    private BhpLayer _layer;
    private BhpWindow _window;

    private BhpViewerBase _viewer;
    private JTabbedPane _tabPane;
    private OpenDataPanel _dataPanel;
    private BhpPropertyGeneralPanel _generalPanel;
    private BhpPropertyPipelinePanel _pipelinePanel;

    private BhpEventDataTable _eventDataTable;
    private JPanel _eventDataPanel;

    /**
     * Constructs a new instance and sets up its GUI.
     */
    public BhpSLayerPropertyDialog(BhpViewerBase viewer) {
      // super(frame,title) not usable since viewer is JInternalFrame
      //super(viewer, "Layer Properties");
      super();
      setTitle("Layer Properties");
      _viewer = viewer;
      // set my location relative to BhpViewerbase
      setLocation(viewer.getMyLocation().x + 10, viewer.getMyLocation().y + 10);
      setupGUI();
    }

    /**
     * Initialize the dialog with a
     * <code>{@link BhpLayer}</code> and a
     * <code>{@link BhpWindow}</code>. <br>
     * This method retrieves values from
     * the layer and the window to fill the fields in its GUI.
     * The window is needed to find out the displaying order
     * of the layer.
     */
    public void initWithLayer(BhpLayer layer, BhpWindow win) {
        _layer = layer;
        _window = win;

        String dsName = _layer.getParameter().getDataSource().getDataSourceName();
        if ((_layer instanceof BhpEventLayer) &&
            !dsName.equals(com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_BHPSU)) {
            // event data other than bhpsu
            _dataPanel = null;
            if (_tabPane.getTitleAt(_tabPane.getTabCount()-1).equals("Synchronization")) {
                _tabPane.removeTabAt(_tabPane.getTabCount()-1);
                _tabPane.addTab("Event Data", _eventDataPanel);
            }
            if (_tabPane.getTitleAt(1).equals("Local Subset")) {
                _tabPane.removeTabAt(1);
            }
            if (_tabPane.getTitleAt(0).equals("Dataset")) {
                _tabPane.removeTabAt(0);
            }
            _eventDataTable.initWithLayer((BhpEventLayer)_layer);
            _pipelinePanel.initWithLayer(_layer, _window);
            _generalPanel.initWithLayer(_layer);
        }
        else {
            boolean isMap = false;
            if (layer instanceof BhpMapLayer) {
                isMap = true;
            }
            _dataPanel = new OpenDataPanel(_viewer, null, isMap);
            if (_tabPane.getTitleAt(0).equals("Dataset")) {
                _tabPane.removeTabAt(0);
            }

            _tabPane.insertTab("Dataset", null, _dataPanel, "Dataset", 0);
            _dataPanel.initWithLayer(layer);
            _generalPanel.initWithLayer(_layer);
            if (!_tabPane.getTitleAt(_tabPane.getTabCount()-1).equals("Synchronization")) {
                _tabPane.removeTabAt(_tabPane.getTabCount()-1);
                _tabPane.add("Synchronization", _generalPanel);
            }
            if (_tabPane.getTitleAt(1).equals("Local Subset")) {
                _tabPane.removeTabAt(1);
            }
            _pipelinePanel.initWithLayer(_layer, _window);
            if (layer.getPipeline() != null) {
                _tabPane.insertTab("Local Subset", null,
                                   _pipelinePanel.getLocalSubsetPanel(),
                                   "Local Subset", 1);
            }
        }
    }

    private boolean apply() {
        if (_layer == null) {
            return true;
        }

        if ((_tabPane.getTitleAt(_tabPane.getTabCount()-1).equals("Event Data")) &&
            (_layer instanceof BhpEventLayer)) {
            _eventDataTable.applyToLayer((BhpEventLayer)_layer);
        }

        int pdFlag = 0;
        if (_dataPanel != null) {
            _dataPanel.acceptTableChange();
            pdFlag = _dataPanel.applyToLayer(_layer);
        }

        if (pdFlag != 0) {
            boolean continueLoad = true;
            if (!(_dataPanel.getDataPanel() instanceof OpenEventPanel) ||
                (_window.getWindowType()==BhpViewer.WINDOW_TYPE_MAPVIEWER))
                {
                continueLoad = _layer.getParameter().checkMaxTraceNumber(_viewer);
                }
            if (!continueLoad) {
                return false;
            }
        }

        boolean[] pipelineChanged = {false};
        int ppFlag = _pipelinePanel.applyToLayer(_layer, _window, pipelineChanged);
        int pgFlag = _generalPanel.applyToLayer(_layer);

        int eventFlag = pdFlag | ppFlag | pgFlag;

        if (eventFlag != 0) {
            if (pdFlag != 0) {
                _layer.generateLayer();
            }
            else {
                _layer.updateLayer();
            }
            if (_layer.getEnableTalk()) {
                BhpLayerEvent event = new BhpLayerEvent(eventFlag, _layer);
                _viewer.broadcastBhpLayerEvent(event);
            }
            else if ((eventFlag & BhpViewer.NAME_FLAG) != 0) {
                BhpLayerEvent event = new BhpLayerEvent(eventFlag, _layer);
                _window.getBhpLayerList().receiveBhpLayerEvent(event);
            }

            MultipleViewContainer spv = _window.getBhpPlot().getMainView();
            spv.setTransformationWithoutPropogation(spv.getTransformation());
        }
        else if (pipelineChanged[0]) {
            _layer.updateLayer();

            MultipleViewContainer spv = _window.getBhpPlot().getMainView();
            spv.setTransformationWithoutPropogation(spv.getTransformation());
        }
        return true;
    }

    private void hideDialog() {
        this.setVisible(false);
    }

    private void setupGUI() {
        ColormapIO cmanager = null;

/* QIW Use ColorBarDialog for local and remote colormaps
        if (BhpViewerBase.getMsgMgr().getLocationPref().equals(QIWConstants.REMOTE_SERVICE_PREF))
            {
            cmanager = new BhpRemoteColormapManager(_viewer);
            }
        else {
            cmanager = new ColorBarDialog();
        }
QIW */
        cmanager = new ColorBarDialog();

        _tabPane = new JTabbedPane();

        _generalPanel = new BhpPropertyGeneralPanel();
        _pipelinePanel = new BhpPropertyPipelinePanel(cmanager,_viewer);
        _tabPane.add("Display Parameters", _pipelinePanel);   // old tab name: Property Pipeline
        _tabPane.add("Synchronization", _generalPanel);   // old tab name: Property General

        _eventDataPanel = new JPanel(new BorderLayout());
        _eventDataTable = new BhpEventDataTable();
        _eventDataPanel.add(new JScrollPane(_eventDataTable), BorderLayout.CENTER);

        JPanel cpanel = buildControlPanel();
        this.getContentPane().add(_tabPane, BorderLayout.CENTER);
        this.getContentPane().add(cpanel, BorderLayout.SOUTH);
    }

    private JPanel buildControlPanel() {
        JButton okb = new JButton("Ok");
        okb.addActionListener(new OkListener());
        JButton applyb = new JButton("Apply");
        applyb.addActionListener(new ApplyListener());
        JButton cancelb = new JButton("Cancel");
        cancelb.addActionListener(new CancelListener());
        JButton helpb = new JButton("Help");
        helpb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                showHelp();
            }
        });

        JPanel panel = new JPanel(new FlowLayout());
        panel.add(okb);
        panel.add(applyb);
        panel.add(cancelb);
        panel.add(helpb);
        return panel;
    }

    private void showHelp() {
        try {
            BhpViewerBase.broker.setCurrentID("layers_property");
            BhpViewerBase.broker.setDisplayed(true);
        }
        catch (Exception e) {
            _viewer.getStatusBar().showMessage(
                    BhpViewer.WARNING_MESSAGE,
                    "Couldn't show help for property dialog");
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "Couldn't show properties help");
        }
    }

    private class OkListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            boolean success = apply();
            if (success) {
                hideDialog();
            }
        }
    }

    private class ApplyListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            apply();
        }
    }

    private class CancelListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            hideDialog();
        }
    }
}
