/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.bhpBilliton.viewer2d.Bhp2DviewerConstants;
import com.bhpBilliton.viewer2d.BhpEventLayer;
import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.BhpWindow;
import com.bhpBilliton.viewer2d.data.BhpDataAdapterFactory;
import com.bhpBilliton.viewer2d.data.EventDataSaver;
import com.bhpBilliton.viewer2d.data.GeneralDataChooser;
import com.bhpBilliton.viewer2d.data.GeneralDataSource;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.gw2d.model.Attribute2D;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This dialog is used to select event layers so that
 *               their data will be saved into one file. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpEventSavingDialog extends JDialog {
    private BhpViewerBase _viewer;
    private DefaultListModel _available    = new DefaultListModel();
    private DefaultListModel _selected     = new DefaultListModel();

    private JList _availableLayers;
    private JList _selectedLayers;
    private JButton _addButton;
    private JButton _removeButton;

    /**
     * Constructs a BhpTraceAnnPanel and sets up the GUI.
     */
    // BhpViewerBase no longer a frame
    public BhpEventSavingDialog(BhpViewerBase parent) {
      super();
      setTitle("Save Horizons");
      setLocation(parent.getMyLocation().x + 10, parent.getMyLocation().y + 10);
      //super(parent, "Save Horizons");

        _viewer = parent;
        int fixedCellWidth = 100;
        _availableLayers = new JList(_available);
        _availableLayers.setCellRenderer(new EventLayerFieldCellRenderer());
        _availableLayers.setFixedCellWidth(fixedCellWidth);
        _availableLayers.getSelectionModel().addListSelectionListener(
                                            new AvailableListSelectionListener());
        JScrollPane scrollPane1 = new JScrollPane(_availableLayers);

        JPanel panel1 = new JPanel();
        panel1.setLayout(new BoxLayout(panel1, BoxLayout.X_AXIS));
        panel1.setBorder(BorderFactory.createTitledBorder("Available Horizons"));
        panel1.add(scrollPane1);

        _addButton = new JButton("==>");
        _addButton.setEnabled(false);
        _addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int[] sel = _availableLayers.getSelectedIndices();
                for (int i=0; i<sel.length; i++) {
                    _selected.addElement(_available.elementAt(sel[i]));
                }
                for (int i=sel.length-1; i>=0; i--)
                    _available.remove(sel[i]);
            }
        });

        _removeButton = new JButton("<==");
        _removeButton.setEnabled(false);
        _removeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int[] sel = _selectedLayers.getSelectedIndices();
                for (int i=0; i<sel.length; i++) {
                    _available.addElement(_selected.elementAt(sel[i]));
                }
                for (int i=sel.length-1; i>=0; i--) {
                    _selected.remove(sel[i]);
                }
            }
        });

        JPanel panel2 = new JPanel();
        panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));
        panel2.add(Box.createGlue());
        panel2.add(_addButton);
        panel2.add(Box.createGlue());
        panel2.add(_removeButton);
        panel2.add(Box.createGlue());

        _selectedLayers = new JList(_selected);
        _selectedLayers.setCellRenderer(new EventLayerFieldCellRenderer());
        JScrollPane scrollPane2 = new JScrollPane(_selectedLayers);
        _selectedLayers.getSelectionModel().addListSelectionListener(
                                            new SelectedListSelectionListener());
        _selectedLayers.setFixedCellWidth(fixedCellWidth);

        JPanel panel3 = new JPanel();
        panel3.setLayout(new BoxLayout(panel3, BoxLayout.X_AXIS));
        panel3.setBorder(BorderFactory.createTitledBorder("Selected Horizons"));
        panel3.add(scrollPane2);

        JPanel panelUp = new JPanel();
        panelUp.setLayout(new BoxLayout(panelUp, BoxLayout.X_AXIS));
        panelUp.add(Box.createGlue());
        panelUp.add(panel1);
        panelUp.add(Box.createGlue());
        panelUp.add(panel2);
        panelUp.add(Box.createGlue());
        panelUp.add(panel3);
        panelUp.add(Box.createGlue());

        JPanel panelDn = new JPanel(new FlowLayout());
        JButton saveBtn = new JButton("Save");
        saveBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                saveEvents();
            }
        });
        JButton cancelBtn = new JButton("Cancel");
        cancelBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });
        panelDn.add(saveBtn);
        panelDn.add(cancelBtn);

        this.getContentPane().add(panelUp, BorderLayout.CENTER);
        this.getContentPane().add(panelDn, BorderLayout.SOUTH);
        setSize(400, 300);
    }

    /**
     * Initialize the dialog with a BhpWindow
     */
    public void initWithBhpWindow(BhpWindow win) {
        _available.clear();
        _selected.clear();

        Object[] views = win.getBhpPlot().getMainView().getAllBhpLayers();
        BhpEventLayer elayer;
        for (int i=0; i<views.length; i++) {
            if (views[i] instanceof BhpEventLayer) {
                elayer = (BhpEventLayer) views[i];
                if (!elayer.getDataSource().equals(
                        com.bhpBilliton.viewer2d.data.GeneralDataSource.DATA_SOURCE_BHPSU))
                    _available.addElement(elayer);
            }
        }
        BhpLayer[] layers = win.getSelectedLayers();
        for (int j=0; j<layers.length; j++) {
            if (_available.contains(layers[j])) {
                _selected.addElement(layers[j]);
                _available.removeElement(layers[j]);
            }
        }
    }

    private void saveEvents() {
        if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("They'll be saved as xml-horizon");
        if (_selected.size() == 0) {
            _viewer.getStatusBar().showMessage(BhpViewer.INFO_MESSAGE,
                    "Horizon saving cancelled, no horizon is selected.");
            return;
        }

        String fileName = ((BhpEventLayer)_selected.get(0)).getPathlist();
        GeneralDataChooser chooser = BhpDataAdapterFactory.createDataChooser(
                            _viewer, null, GeneralDataSource.DATA_SOURCE_XMLHRZ);
        if (chooser instanceof EventDataSaver) {
            Attribute2D attr2d = null;
            ArrayList layers = new ArrayList();
            for (int i=0; i<_selected.size(); i++) layers.add(_selected.get(i));
            try {
                EventDataSaver eventDataSaver = ((EventDataSaver)chooser);
                fileName = eventDataSaver.saveEventDataToFile(layers, fileName);
                String msg = "BhpEventSavingDialog.saveEvents() finished saving file : " + fileName;
                _viewer.getStatusBar().showMessage(BhpViewer.INFO_MESSAGE, msg);
            }
            catch (Exception ex) {
                String msg = "BhpEventSavingDialog.saveEvents() failed : " + ex;
                _viewer.getStatusBar().showMessage(BhpViewer.ERROR_MESSAGE, msg);
                ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                    "BhpEventSavingDialog.saveEvents() failed");
            }
        }
        if (fileName == null) return;
        BhpEventLayer elayer = null;
        for (int i=0; i<_selected.size(); i++) {
            elayer = (BhpEventLayer) _selected.get(i);
            if (elayer.getDataSource().equals("TemporaryEventDataSource")) {
                elayer.changeTemporaryDataSource(fileName);
            }
        }
    }

    // Inner classes
    private class EventLayerFieldCellRenderer extends JLabel implements ListCellRenderer {
        public EventLayerFieldCellRenderer() {
            setOpaque(true);
        }
        public Component getListCellRendererComponent( JList list, Object value,
                        int index, boolean isSelected, boolean cellHasFocus) {

            BhpEventLayer elayer = (BhpEventLayer) value;
            setText("Layer " + elayer.getIdNumber() + " : " + elayer.getLayerName());
            setToolTipText("Loaded from : " + elayer.getPathlist());
            if (isSelected) {
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
            }
            else {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }
            setEnabled(list.isEnabled());
            setFont(list.getFont());
            return this;
        }
    }

    private class AvailableListSelectionListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent e) {
            if (_availableLayers.isSelectionEmpty()) _addButton.setEnabled(false);
            else _addButton.setEnabled(true);
        }
    }

    private class SelectedListSelectionListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent e) {
            if (_selectedLayers.isSelectionEmpty()) {
                _removeButton.setEnabled(false);
            }
            else {
                _removeButton.setEnabled(true);
            }
        }
    }
}
