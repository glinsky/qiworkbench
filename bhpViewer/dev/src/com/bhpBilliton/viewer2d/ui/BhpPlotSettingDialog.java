/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.EtchedBorder;

import com.bhpBilliton.viewer2d.AbstractPlot;
import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpLayerEvent;
import com.bhpBilliton.viewer2d.BhpPlotMV;
import com.bhpBilliton.viewer2d.BhpPlotXV;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.BhpWindow;
import com.bhpBilliton.viewer2d.util.ErrorDialog;

import java.text.*;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This dialog is used to view and change the
 *               settings for a
 *               <code>{@link AbstractPlot}</code>, either
 *               <code>{@link BhpPlotXV}</code>, or
 *               <code>{@link BhpPlotMV}</code>.
 *               The synchronization tab is used to specify window-level
 *               synchronization, while layer-level synchronization
 *               can be set in
 *               <code>{@link BhpPropertyGeneralPanel}</code>. <br>
 *               <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpPlotSettingDialog extends JDialog {
    private static final int METRIC = 1;
    private static final int IMPERIAL = 2;

    private BhpViewerBase _viewer;
    private BhpWindow _bhpWindow;

    //private JTabbedPane _tab;
    private JPanel _scalePanel;
    private JPanel _synPanel;

    private JLabel _vscaleLabel;
    private JLabel _hscaleLabel;
    private JTextField _vscale;
    private JTextField _hscale;
    private JCheckBox _lockRatio;
    private int _unitFlag;
    private NumberFormat _scaleFormat = null;

    //private JCheckBox _mouseTrackingCheck;
    private JCheckBox _broadcastCheck;
    private JCheckBox _cursorSyncCheck;
    private JCheckBox _windowSyncCheck;
    private JCheckBox _vscaleCheck;
    private JCheckBox _hscaleCheck;

    private JCheckBox _hwindowSynCheck;
    private JCheckBox _vwindowSynCheck;

    /**
     * Constructor which taks a
     * <code>{@link BhpViewerBase}</code>
     * as the parent of the dialog.
     */
    public BhpPlotSettingDialog(BhpViewerBase viewer, String type) {
      // BhpViewerBase no longer a frame
      super();
      setTitle("Window");
      setLocation(viewer.getMyLocation().x + 10,viewer.getMyLocation().y + 10);
      //super(viewer, "Window "+type, false);
      _viewer = viewer;
      _scalePanel = null;
      _synPanel = null;
      setupGUI(type);
    }

    /**
     * Initialize the panel with the
     * <code>{@link BhpWindow}</code>. <br>
     * This method retrieves values from
     * the BhpWindow to fill the fields in its GUI.
     */
    public void initWithBhpWindow(BhpWindow win) {
        if (win == null) {
            JOptionPane.showMessageDialog(_viewer,"Please select a window");
            return;
        }

        _bhpWindow = win;

        AbstractPlot plot = _bhpWindow.getBhpPlot();
        //_hscale.setText("" + plot.getHorizontalScale());
        //_vscale.setText("" + plot.getVerticalScale());

        if (_scalePanel != null) {
            double tmpScaleValue = 1.0/convertPixelToCM(plot.getHorizontalScale());
            if (tmpScaleValue<1000 && tmpScaleValue>0.01) {
                tmpScaleValue = Math.round(tmpScaleValue*1000.0)/1000.0;
                _hscale.setText("" + tmpScaleValue);
            }
            else
                {
                _hscale.setText("" + _scaleFormat.format(tmpScaleValue));
                }
            //tmpScaleValue = 1.0 / convertPixelToCM(plot.getVerticalScale());

            if (_bhpWindow.getWindowType() == BhpViewer.WINDOW_TYPE_MAPVIEWER) {
                _vscaleLabel.setText("  Vertical Scale (Traces/CM):  ");
                if (_unitFlag == IMPERIAL) {
                    _vscaleLabel.setText("  Vertical Scale (Traces/Inch)");
                }
                tmpScaleValue = 1.0/convertPixelToCM(plot.getVerticalScale());
            }
            else {
                _vscaleLabel.setText("  Vertical Scale (CMs/Unit):  ");
                if (_unitFlag == IMPERIAL) {
                    _vscaleLabel.setText("  Vertical Scale (Inches/Unit)");
                }
                tmpScaleValue = convertPixelToCM(plot.getVerticalScale());
            }
            if (tmpScaleValue<1000 && tmpScaleValue>0.01) {
                tmpScaleValue = Math.round(tmpScaleValue*1000.0)/1000.0;
                _vscale.setText("" + tmpScaleValue);
            }
            else
                {
                _vscale.setText("" + _scaleFormat.format(tmpScaleValue));
                }

            _lockRatio.setSelected(plot.getLockAspectRatio());
            if (_bhpWindow.getWindowType() == BhpViewer.WINDOW_TYPE_MAPVIEWER)
                {
                _lockRatio.setEnabled(true);
                }
            else {
                _lockRatio.setEnabled(false);
            }
        }
        else if (_synPanel != null) {
            _broadcastCheck.setSelected(plot.getEnablePlotTalk());
            int synFlag = plot.getSynFlag();
            //_mouseTrackingCheck.setSelected(_bhpWindow.isMouseTracking());
            _hscaleCheck.setSelected(false);
            _vscaleCheck.setSelected(false);
            //_cursorSyncCheck.setSelected(false);
            _cursorSyncCheck.setSelected(_bhpWindow.isMouseTracking());
            _windowSyncCheck.setSelected(false);
            _hwindowSynCheck.setEnabled(false);
            _vwindowSynCheck.setEnabled(false);
            _hwindowSynCheck.setSelected(false);
            _vwindowSynCheck.setSelected(false);
            //if ((synFlag & BhpViewer.CURSOR_FLAG) != 0)
            //    _cursorSyncCheck.setSelected(true);
            if ((synFlag & BhpViewer.WINDOW_VIEWPOS_FLAG) != 0) {
                _windowSyncCheck.setSelected(true);
                _hwindowSynCheck.setEnabled(true);
                _vwindowSynCheck.setEnabled(true);
                _hwindowSynCheck.setSelected(plot.getSynScrollHorizontal());
                _vwindowSynCheck.setSelected(plot.getSynScrollVertical());
            }
            if ((synFlag & BhpViewer.SCALEH_FLAG) != 0)
                {
                _hscaleCheck.setSelected(true);
                }
            if ((synFlag & BhpViewer.SCALEV_FLAG) != 0)
                {
                _vscaleCheck.setSelected(true);
                }
        }
    }

    /**
     * apply the changes of annotation settings to the
     * <code>{@link BhpWindow}</code>. <br>
     * This method will retrieve values from
     * its GUI and update the annotation.
     */
    private void apply() {
        if (_bhpWindow == null) return;
        AbstractPlot plot = _bhpWindow.getBhpPlot();

        if (_synPanel != null) {
            //_bhpWindow.setMouseTracking(_mouseTrackingCheck.isSelected());
            plot.setEnablePlotTalk(_broadcastCheck.isSelected());
            int syncFlag = 0;
            if (_hscaleCheck.isSelected()) syncFlag = syncFlag | BhpViewer.SCALEH_FLAG;
            if (_vscaleCheck.isSelected()) syncFlag = syncFlag | BhpViewer.SCALEV_FLAG;
            if (_cursorSyncCheck.isSelected()) syncFlag = syncFlag | BhpViewer.CURSOR_FLAG;
            if (_windowSyncCheck.isSelected()) syncFlag = syncFlag | BhpViewer.WINDOW_VIEWPOS_FLAG;
            plot.setSynFlag(syncFlag);
            if (_windowSyncCheck.isSelected()) {
                plot.setSynScrollHorizontal(_hwindowSynCheck.isSelected());
                plot.setSynScrollVertical(_vwindowSynCheck.isSelected());
            }
            _bhpWindow.setMouseTracking(_cursorSyncCheck.isSelected());
        }
        else if (_scalePanel != null) {
            plot.setLockAspectRatio(_lockRatio.isSelected());
            int eventFlag = 0;
            try {
                double vfactor = 1.0;
                double hfactor = 1.0;
                double nvscale = Double.parseDouble(_vscale.getText());
                if (_bhpWindow.getWindowType() == BhpViewer.WINDOW_TYPE_MAPVIEWER)
                    nvscale = 1.0 / nvscale;
                nvscale = convertCMToPixel(nvscale);
                double nhscale = Double.parseDouble(_hscale.getText());
                nhscale = 1.0 / nhscale;
                nhscale = convertCMToPixel(nhscale);

                //if (layer instanceof BhpSeismicLayer) muSample = muSample * layer.getPipeline().getSeismicReader().getMetaData().getSampleRate();
                if (nvscale != plot.getVerticalScale()) {
                    eventFlag = eventFlag | BhpViewer.SCALEV_FLAG;
                    vfactor = nvscale / plot.getVerticalScale();
                }
                if (nhscale != plot.getHorizontalScale()) {
                    eventFlag = eventFlag | BhpViewer.SCALEH_FLAG;
                    hfactor = nhscale / plot.getHorizontalScale();
                }
                if (eventFlag != 0) {
                    BhpLayer layer = _bhpWindow.getFeaturedLayer();
                    _bhpWindow.getBhpPlot().scaleToWithLayer(layer, hfactor, vfactor);
                    BhpLayerEvent event = new BhpLayerEvent(eventFlag, layer);
                    if (_bhpWindow.getBhpPlot().getEnablePlotTalk())
                        _viewer.broadcastBhpLayerEvent(event);
                    _bhpWindow.getBhpPlot().repaint();
                }
            }
            catch (Exception ex) {
                ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                    "BhpPlotSettingDialog.apply scales Exception");
            }
        }
    }

    private void hideDialog() {
        _bhpWindow = null;
        this.setVisible(false);
    }

    private void setupGUI(String type) {
        this.getContentPane().add(buildContentPanel(type), BorderLayout.CENTER);
        this.getContentPane().add(buildControlPanel(), BorderLayout.SOUTH);
    }

    private JPanel buildContentPanel(String type) {
        if (type!=null && type.equals("Synchronization")) {
            JPanel p1 = new JPanel(new GridLayout(6, 1));
            //_mouseTrackingCheck = new JCheckBox("Cursor Tracking");
            _broadcastCheck = new JCheckBox("Broadcast");
            _cursorSyncCheck = new JCheckBox("Listen To Cursor Position");
            _windowSyncCheck = new JCheckBox("Listen To Scroll Position");
            _vscaleCheck = new JCheckBox("Listen To Vertical Scale");
            _hscaleCheck = new JCheckBox("Listen To Horizontal Scale");
            _hwindowSynCheck = new JCheckBox("Synchronize Horizontal Scrolling");
            _vwindowSynCheck = new JCheckBox("Synchronize Vertical Scrolling");
            JPanel synPanel = new JPanel(new GridLayout(2, 1));
            synPanel.add(_hwindowSynCheck);
            synPanel.add(_vwindowSynCheck);
/*
            _windowSyncCheck.addChangeListener(new ChangeListener() {
                public void stateChanged(ChangeEvent e) {
                    if (_windowSyncCheck.isSelected()) {
                        _hwindowSynCheck.setSelected(true);
                        _vwindowSynCheck.setSelected(true);
                        _hwindowSynCheck.setEnabled(true);
                        _vwindowSynCheck.setEnabled(true);
System.out.println("Selecting H and V scrolling");
                    }
                    else {
                        _hwindowSynCheck.setSelected(false);
                        _vwindowSynCheck.setSelected(false);
                        _hwindowSynCheck.setEnabled(false);
                        _vwindowSynCheck.setEnabled(false);
                    }
                }
            });
*/
            _windowSyncCheck.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    AbstractPlot plot = _bhpWindow.getBhpPlot();
                    if (_windowSyncCheck.isSelected()) {
                        _hwindowSynCheck.setEnabled(true);
                        _vwindowSynCheck.setEnabled(true);
                        _hwindowSynCheck.setSelected(plot.getSynScrollHorizontal());
                        _vwindowSynCheck.setSelected(plot.getSynScrollVertical());
                    }
                    else {
                        //leave the Vectical and Horizontal Scroll checkbox setting alone
                        _hwindowSynCheck.setEnabled(false);
                        _vwindowSynCheck.setEnabled(false);
                    }
                }
            });
            //p1.add(_mouseTrackingCheck);
            p1.add(_broadcastCheck);
            p1.add(new JLabel(""));
            p1.add(_hscaleCheck);
            p1.add(_vscaleCheck);
            p1.add(_cursorSyncCheck);
            p1.add(_windowSyncCheck);
            Box box1 = new Box(BoxLayout.X_AXIS);
            box1.add(Box.createHorizontalStrut(10));
            box1.add(p1);
            box1.add(Box.createHorizontalGlue());
            Box box2 = new Box(BoxLayout.X_AXIS);
            box2.add(Box.createHorizontalStrut(30));
            box2.add(synPanel);
            box2.add(Box.createHorizontalGlue());
            Box box11 = new Box(BoxLayout.Y_AXIS);
            box11.add(Box.createVerticalStrut(30));
            box11.add(box1);
            box11.add(box2);
            box11.add(Box.createVerticalGlue());

            _synPanel = new JPanel();
            _synPanel.add(box11);
            _synPanel.setBorder(new EtchedBorder());
            return _synPanel;
        }

        _scaleFormat = DecimalFormat.getInstance();
        if (_scaleFormat instanceof DecimalFormat) {
            ((DecimalFormat)_scaleFormat).applyPattern("0.###E0");
        }

        JPanel panel2 = new JPanel(new GridLayout(3, 2));
        _hscale = new JTextField();
        _vscale = new JTextField();
        _lockRatio = new JCheckBox("Lock Aspect Ratio");
        _hscaleLabel = new JLabel("  Horizontal Scale (Traces/CM)");
        //_vscaleLabel = new JLabel("  Vertical Scale (Units/CM)");
        _vscaleLabel = new JLabel("  Vertical Scale (CMs/Unit)");
        _unitFlag = METRIC;
        if (!(_viewer.getPropertyManager().getProperty("unitOption").equals("metric"))) {
            _hscaleLabel = new JLabel("  Horizontal Scale (Traces/Inch)");
            _vscaleLabel = new JLabel("  Vertical Scale (Inches/Unit)");
            //_vscaleLabel = new JLabel("  Vertical Scale (Units/Inch)");
            _unitFlag = IMPERIAL;
        }
        panel2.add(_hscaleLabel);
        panel2.add(_hscale);
        panel2.add(_vscaleLabel);
        panel2.add(_vscale);
        panel2.add(_lockRatio);
        panel2.add(new JLabel(""));
        Box box22 = new Box(BoxLayout.X_AXIS);
        box22.add(Box.createHorizontalStrut(10));
        box22.add(panel2);
        box22.add(Box.createHorizontalGlue());
        Box box2 = new Box(BoxLayout.Y_AXIS);
        box2.add(Box.createVerticalStrut(30));
        box2.add(box22);
        box2.add(Box.createVerticalGlue());
        _scalePanel = new JPanel();
        _scalePanel.add(box2);
        _scalePanel.setBorder(new EtchedBorder());
        return _scalePanel;
    }

    private JPanel buildControlPanel() {
        JButton okb = new JButton("Ok");
        okb.addActionListener(new OkListener());
        JButton apb = new JButton("Apply");
        apb.addActionListener(new ApplyListener());
        JButton clb = new JButton("Cancel");
        clb.addActionListener(new CancelListener());
        JButton helpb = new JButton("Help");
        helpb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                showHelp();
            }
        });
        JPanel panel = new JPanel(new FlowLayout());
        panel.add(okb);
        panel.add(apb);
        panel.add(clb);
        panel.add(helpb);
        return panel;
    }

    private double convertCMToPixel(double ori) {
        int pixelsPerInch = Toolkit.getDefaultToolkit().getScreenResolution();
        double factor = 2.54;
        if (_unitFlag == BhpPlotSettingDialog.IMPERIAL) factor = 1.0;
        double pixelsPerCM = pixelsPerInch / factor;
        return (ori * pixelsPerCM);
    }

    private double convertPixelToCM(double ori) {
        int pixelsPerInch = Toolkit.getDefaultToolkit().getScreenResolution();
        double factor = 2.54;
        if (_unitFlag == BhpPlotSettingDialog.IMPERIAL) factor = 1.0;
        double pixelsPerCM = pixelsPerInch / factor;
        return (ori / pixelsPerCM);
    }

    private void showHelp() {
        try {
            if (_synPanel != null)
                {
                BhpViewerBase.broker.setCurrentID("window_synchronization");
                }
            else
                {
                BhpViewerBase.broker.setCurrentID("window_scale");
                }
            BhpViewerBase.broker.setDisplayed(true);
        }
        catch (Exception e) {
            _viewer.getStatusBar().showMessage(
                    BhpViewer.WARNING_MESSAGE,
                    "Couldn't show help for the" + this.getTitle() + "dialog.");
            ErrorDialog.showInternalErrorDialog(_viewer, Thread.currentThread().getStackTrace(),
                                                "Couldn't show plot setting help");
        }
    }

    private class OkListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            apply();
            hideDialog();
        }
    }

    private class ApplyListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            apply();
        }
    }

    private class CancelListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            hideDialog();
        }
    }
}
