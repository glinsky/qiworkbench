/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;

import com.bhpBilliton.viewer2d.BhpEventLayer;
import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpMapLayer;
import com.bhpBilliton.viewer2d.BhpSeismicLayer;
import com.bhpBilliton.viewer2d.BhpViewer;
import com.gwsys.seismic.core.*;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is the panel enables viewing and changing general
 *               layer properties that include various synchronization
 *               flags, layer id, and layer broadcast flag.
 *               This panel is usually used in the
 *               <code>{@link BhpSLayerPropertyDialog}</code>
 *               with other panels. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpPropertyGeneralPanel extends JPanel {
    private JPanel _panel;

    private SynCheckBox[] _synMap;
    private SynCheckBox[] _synNonMap;
    private SynCheckBox[] _synEvent;

    private JCheckBox _broadcastCheck;
    private JTextField _idField;

    /**
     * Constructs a new instance and builds the GUI for the panel.
     */
    public BhpPropertyGeneralPanel() {
        super(new BorderLayout());

        _idField = new JTextField("");
        _idField.setEditable(false);
        _broadcastCheck = new JCheckBox("  Broadcast");
        //_broadcastCheck.setHorizontalAlignment(SwingConstants.CENTER);

        buildSynMap();
        buildSynNonMap();
        buildSynEvent();


        _panel = new JPanel();
        //_panel.setBorder(new EtchedBorder());

        Box inbox = new Box(BoxLayout.X_AXIS);
        inbox.setBorder(new TitledBorder(""));
        inbox.add(Box.createHorizontalStrut(10));
        inbox.add(_panel);

        Box outbox = new Box(BoxLayout.Y_AXIS);
        outbox.add(Box.createVerticalGlue());
        JPanel bcPanel = new JPanel(new GridLayout(1, 3));
        bcPanel.add(new JLabel(""));
        bcPanel.add(_broadcastCheck);
        bcPanel.add(new JLabel(""));
        outbox.add(bcPanel);
        outbox.add(Box.createVerticalStrut(10));
        outbox.add(inbox);
        outbox.add(Box.createVerticalGlue());

        Box box1 = new Box(BoxLayout.X_AXIS);
        box1.add(Box.createHorizontalGlue());
        box1.add(outbox);
        box1.add(Box.createHorizontalGlue());

        this.add(box1, BorderLayout.CENTER);
    }

    /**
     * Initialize the panel with the
     * <code>{@link BhpLayer}</code>. <br>
     * This method retrieves values from
     * the layer to fill the fields in its GUI.
     */
    public void initWithLayer(BhpLayer layer) {
        _panel.removeAll();
        _idField.setText("" + layer.getIdNumber());
        _broadcastCheck.setSelected(layer.getEnableTalk());

        int sFlag = layer.getSynFlag();
        if (layer instanceof BhpMapLayer) {
            String xkey = layer.getParameter().getMapHorizontalKeyName();
            String ykey = layer.getParameter().getVerticalName();
            BhpMapLayer mlayer = (BhpMapLayer) layer;
            int msFlag = mlayer.getMapSynFlag();
            _panel.setLayout(new GridLayout(4, 2));
            _synMap[6].setText("Listen To " + xkey + " Axis Direction");
            _synMap[7].setText("Listen To " + ykey + " Axis Direction");
            for (int i=0; i<_synMap.length-4; i++) {
                // axis start and end synchronization if not useful
                // so, don't add them
                _panel.add(_synMap[i]);
                _synMap[i].setSelected(false);
                if (i < 5) {
                    if ((sFlag & _synMap[i].getSynValue()) != 0)
                        {
                        _synMap[i].setSelected(true);
                        }
                }
                else {
                    if ((msFlag & _synMap[i].getSynValue()) != 0)
                        {
                        _synMap[i].setSelected(true);
                        }
                }
            }
        }
        else if (layer instanceof BhpEventLayer) {
            _panel.setLayout(new GridLayout(4, 1));
            for (int i=0; i<_synEvent.length; i++) {
                _panel.add(_synEvent[i]);
                _synEvent[i].setSelected(false);
                if ((sFlag & _synEvent[i].getSynValue()) != 0)
                    {
                    _synEvent[i].setSelected(true);
                    }
            }
        }
        else {
            _panel.setLayout(new GridLayout(6, 2));
            for (int i=0; i<_synNonMap.length; i++) {
                _panel.add(_synNonMap[i]);
                _synNonMap[i].setSelected(false);
                if ((sFlag & _synNonMap[i].getSynValue()) != 0)
                    {
                    _synNonMap[i].setSelected(true);
                    }
            }
            _synNonMap[9].setEnabled(false);
            if (layer instanceof BhpSeismicLayer) {
                _synNonMap[9].setEnabled(true);
                _synNonMap[11].setEnabled(false);
            }
            else {
                _synNonMap[11].setEnabled(true);
            }
        }
    }

    /**
     * apply the changes to the
     * <code>{@link BhpLayer}</code>. <br>
     * This method retrieves values from
     * its GUI and apply it to the layer.
     * @return a flag indicating what has been changed.
     */
    public int applyToLayer(BhpLayer layer) {
        int synFlag = 0;
        if (layer instanceof BhpMapLayer) {
            int msynFlag = 0;
            for (int i=0; i<_synMap.length; i++) {
                if (i < 5) {
                    if (_synMap[i].isSelected())
                        {
                        synFlag = synFlag | _synMap[i].getSynValue();
                        }
                }
                else {
                    if (_synMap[i].isSelected())
                        {
                        msynFlag = msynFlag | _synMap[i].getSynValue();
                        }
                }
            }
            ((BhpMapLayer)layer).setMapSynFlag(msynFlag);
        }
        else if (layer instanceof BhpEventLayer) {
            for (int i=0; i<_synEvent.length; i++) {
                if (_synEvent[i].isSelected())
                    {
                    synFlag = synFlag | _synEvent[i].getSynValue();
                    }
            }
        }
        else {
            for (int i=0; i<_synNonMap.length; i++) {
                if (_synNonMap[i].isSelected())
                    {
                    synFlag = synFlag | _synNonMap[i].getSynValue();
                    }
            }
        }

        if (synFlag != layer.getSynFlag()) {
            layer.setSynFlag(synFlag); // this is personal issue, no broadcast
        }

        if (_broadcastCheck.isSelected() != layer.getEnableTalk()) {
            // this does not need to be broadcasted
            layer.setEnableTalk(_broadcastCheck.isSelected());
        }
        return 0;
    }

    private void buildSynMap() {
        _synMap = new SynCheckBox[12];
        _synMap[0] = new SynCheckBox("Listen To Visibility", BhpViewer.VISIBLE_FLAG);
        _synMap[1] = new SynCheckBox("Listen To Polarity", BhpViewer.POLARITY_FLAG);
        _synMap[2] = new SynCheckBox("Listen To Opacity", BhpViewer.TRANSPARENCY_FLAG);
        //_synMap[3] = new SynCheckBox("Listen To Data Set", BhpLayer.NAME_FLAG);
        _synMap[3] = new SynCheckBox("Listen To Color Map", BhpViewer.COLORBAR_FLAG);
        _synMap[4] = new SynCheckBox("Listen To Color Interpolation", BhpViewer.COLORINTER_FLAG);
        _synMap[5] = new SynCheckBox("Listen To Transpose", BhpMapLayer.MAP_TRANSPOSE);
        _synMap[6]= new SynCheckBox("Listen To Horizontal Axis Direction", BhpMapLayer.MAP_X_DIR_FLAG);
        _synMap[7]= new SynCheckBox("Listen To Vertical Axis Direction", BhpMapLayer.MAP_Y_DIR_FLAG);
        _synMap[8] = new SynCheckBox("Listen To Horizontal Axis Start", BhpMapLayer.MAP_X_START_FLAG);
        _synMap[9] = new SynCheckBox("Listen To Vertical Axis Start", BhpMapLayer.MAP_Y_START_FLAG);
        _synMap[10] = new SynCheckBox("Listen To Horizontal Axis End", BhpMapLayer.MAP_X_END_FLAG);
        _synMap[11] = new SynCheckBox("Listen To Vertical Axis End", BhpMapLayer.MAP_Y_END_FLAG);
        //_synMap[12]= new SynCheckBox("Listen To Lock Aspect Ratio", BhpMapLayer.MAP_ASPECTRATIO_FLAG);
    }

    private void buildSynNonMap() {
        _synNonMap = new SynCheckBox[12];
        _synNonMap[0] = new SynCheckBox("Listen To Visibility", BhpViewer.VISIBLE_FLAG);
        _synNonMap[1] = new SynCheckBox("Listen To Polarity", BhpViewer.POLARITY_FLAG);
        _synNonMap[2] = new SynCheckBox("Listen To Opacity", BhpViewer.TRANSPARENCY_FLAG);
        //_synNonMap[3] = new SynCheckBox("Listen To Data Set", BhpLayer.NAME_FLAG);
        _synNonMap[3] = new SynCheckBox("Listen To Time/Depth Range", BhpViewer.TIMERANGE_FLAG);
        //_synNonMap[5] = new SynCheckBox("Listen To Data Selector", BhpLayer.SELECTOR_FLAG);
        _synNonMap[4] = new SynCheckBox("Listen To Normalization Scale", BhpViewer.SCALEAMP_FLAG);
        _synNonMap[5] = new SynCheckBox("Listen To Normalization Type", BhpViewer.NORMALIZE_FLAG);
        _synNonMap[6] = new SynCheckBox("Listen To Interpolation", BhpViewer.INTERPOLATION_FLAG);
        _synNonMap[7] = new SynCheckBox("Listen To AGC", BhpViewer.AGC_FLAG);
        _synNonMap[8]= new SynCheckBox("Listen To Plot Type", BhpViewer.DISPLAY_FLAG);
        _synNonMap[9]= new SynCheckBox("Listen To Wiggle Decimation", BhpViewer.WIGGLEDECIMATION_FLAG);
        _synNonMap[10]= new SynCheckBox("Listen To Color Map", BhpViewer.COLORBAR_FLAG);
        _synNonMap[11]= new SynCheckBox("Listen To Color Interpolation", BhpViewer.COLORINTER_FLAG);
    }

    private void buildSynEvent() {
        _synEvent = new SynCheckBox[4];
        _synEvent[0] = new SynCheckBox("Listen To Visibility", BhpViewer.VISIBLE_FLAG);
        _synEvent[1] = new SynCheckBox("Listen To Opacity", BhpViewer.TRANSPARENCY_FLAG);
        //_synEvent[2] = new SynCheckBox("Listen To Data Set", BhpLayer.NAME_FLAG);
        _synEvent[2] = new SynCheckBox("Listen To Time/Depth Range", BhpViewer.TIMERANGE_FLAG);
        _synEvent[3] = new SynCheckBox("Listen To Event Attribute", BhpViewer.EVENTATT_FLAG);
    }

    private class SynCheckBox extends JCheckBox {
        private int __synValue;
        public SynCheckBox(String text, int value) {
            super(text, false);
            //this.setHorizontalAlignment(SwingConstants.CENTER);
            __synValue = value;
        }

        public int getSynValue() { return __synValue; }
    }
}
