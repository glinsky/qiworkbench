/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.bhpBilliton.viewer2d.BhpEventShape;
//import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.bhpBilliton.viewer2d.util.NoImageLineColorEditor;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.gwsys.gw2d.gui.LineStyleChooser;
import com.gwsys.gw2d.gui.LineWidthChooser;
import com.gwsys.gw2d.model.DataShape;
import com.gwsys.gw2d.model.Attribute2D;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This panel is for viewing and changing the graphic
 *               attribute of a event. The event graphic attribute
 *               includes event line settings and event symbol
 *               settings. <br><br>
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpEventAttributePanel extends JPanel {
    private static final String[] SYMBOL_TYPES = {
                        "Cross", "Circle", "Dot", "Star", "Plus",
                        "Square", "Diamond", "Triangle"};
    private BhpEventShape _shape;

    private JCheckBox _drawLine;
    private JCheckBox _allLine;
    private LineStyleChooser _lsEditor;
    private NoImageLineColorEditor _lcEditor;
    private LineWidthChooser _lwEditor;

    private JCheckBox _drawSymbol;
    private JComboBox _symbolStyle;
    private JTextField _symbolWidth;
    private JTextField _symbolHeight;
    private JCheckBox _symbolFill;
    private NoImageLineColorEditor _scEditor;

    /**
     * Constructs a new BhpEventAttributePanel and
     * sets up the GUI.
     */
    public BhpEventAttributePanel() {
        super();
        buildAttributePanelGUI();
        //this.setBorder(new EtchedBorder());
    }

    /**
     * Gets related information of graphic attribute from
     * the shape and fill the fields of GUI.
     * @param shape a <code>{@link BhpEventShape}</code>
     * where information about grphic attribute will be found.
     */
    public void setShape(DataShape shape) {
        if (shape instanceof BhpEventShape) {
            _shape = (BhpEventShape) shape;
            _drawLine.setSelected(_shape.isDrawLine());
            _drawSymbol.setSelected(_shape.isDrawSymbol());
            _allLine.setSelected(_shape.isDrawAllLine());

            _lsEditor.setLineStyle(_shape.getAttribute().getLineStyle());
            _lwEditor.setLineWidth((int)_shape.getAttribute().getLineWidth());
            _lcEditor.setLineColor(_shape.getAttribute().getLineColor());

            _symbolStyle.setSelectedIndex(_shape.getSymbolStyle());
            _symbolWidth.setText("" + _shape.getSymbolWidth());
            _symbolHeight.setText("" + _shape.getSymbolHeight());
            //_slEditor.setLineStyle(_shape.getSymbolAttribute().getLineStyle());
            //_sfEditor.setFillStyle(_shape.getSymbolAttribute().getFillStyle());
            _symbolFill.setSelected(true);
            if (_shape.getSymbolAttribute().getFillStyle() == Attribute2D.FILL_STYLE_EMPTY) {
                _symbolFill.setSelected(false);
            }
            _scEditor.setLineColor(_shape.getSymbolAttribute().getLineColor());
        } else {
            JOptionPane.showMessageDialog(null,
                            "BhpEventAttributePanel.setShape error",
                            "Internal Error", JOptionPane.ERROR_MESSAGE);
/*
            ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                "BhpEventAttributePanel.setShape error");
*/
        }
    }

    /**
     * Retrieves valus from the GUI components and
     * updates the graphic attriubte of the
     * <code>{@link BhpEventShape}</code>.
     * @return a boolean indicates if the graphic attribute
     * of the event shape is changed
     */
    public boolean apply() {
        boolean changed = false;
        if (_shape.getAttribute().getLineStyle() != _lsEditor.getLineStyle()) {
            _lsEditor.setAttribute(_shape);
            changed = true;
        }
        if (_shape.getAttribute().getLineWidth() != _lwEditor.getLineWidth()) {
            _lwEditor.commitValue(_shape);
            changed = true;
        }
        if (!(_shape.getAttribute().getLineColor().equals(_lcEditor.getLineColor()))) {
            _lcEditor.setAttribute(_shape);
            changed = true;
        }

        if (_shape.isDrawLine() != _drawLine.isSelected()) {
            _shape.setDrawLine(_drawLine.isSelected());
            changed = true;
        }
        if (_shape.isDrawSymbol() != _drawSymbol.isSelected()) {
            _shape.setDrawSymbol(_drawSymbol.isSelected());
            changed = true;
        }
        if (_shape.isDrawAllLine() != _allLine.isSelected()) {
            _shape.setDrawAllLine(_allLine.isSelected());
            changed = true;
        }
        if (_shape.getSymbolStyle() != _symbolStyle.getSelectedIndex()) {
            _shape.setSymbolStyle(_symbolStyle.getSelectedIndex());
            changed = true;
        }
        int newFill = Attribute2D.FILL_STYLE_EMPTY;
        if (_symbolFill.isSelected()) {
            newFill = Attribute2D.FILL_STYLE_SOLID;
        }
        if ((_shape.getSymbolAttribute().getFillStyle() != newFill) ||
            (!(_shape.getSymbolAttribute().getLineColor().equals(_scEditor.getLineColor())))) {
            int oldLine = _shape.getSymbolAttribute().getLineStyle();
            _shape.setSymbolAttribute(oldLine, newFill, _scEditor.getLineColor());
            changed = true;
        }

        double neww = _shape.getSymbolWidth();
        double newh = _shape.getSymbolHeight();
        try {
            neww = Double.parseDouble(_symbolWidth.getText());
        }
        catch (Exception wex) {
            JOptionPane.showMessageDialog(null,
                           "BhpEventAttributePanel.apply error: wrong symbol width" +
                                                    _symbolWidth.getText(),
                            "Internal Error", JOptionPane.ERROR_MESSAGE);
/*
            ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                "BhpEventAttributePanel.apply error: wrong symbol width" +
                                                    _symbolWidth.getText());
*/
            neww = _shape.getSymbolWidth();
        }
        try {
            newh = Double.parseDouble(_symbolHeight.getText());
        }
        catch (Exception hex) {
            JOptionPane.showMessageDialog(null,
                            "BhpEventAttributePanel.apply error: wrong symbol height" +
                                                    _symbolHeight.getText(),
                            "Internal Error", JOptionPane.ERROR_MESSAGE);
/*
            ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                "BhpEventAttributePanel.apply error: wrong symbol height" +
                                                    _symbolHeight.getText());
*/
           newh = _shape.getSymbolHeight();
        }

        if ((neww!=_shape.getSymbolWidth()) || (newh!=_shape.getSymbolHeight())) {
            _shape.setSymbolSize(neww, newh);
            changed = true;
        }
        return changed;
    }

    private void buildAttributePanelGUI() {
        _drawLine = new JCheckBox("Draw Line", true);
        _allLine = new JCheckBox("Connect Non-contiguous Points", false);
        _lsEditor = new LineStyleChooser();
        _lcEditor = new NoImageLineColorEditor();
        _lwEditor = new LineWidthChooser();

        _drawSymbol = new JCheckBox("Draw Symbol", true);
        _symbolStyle = new JComboBox(SYMBOL_TYPES);
        _scEditor = new NoImageLineColorEditor();
        _symbolWidth = new JTextField("");
        _symbolHeight = new JTextField("");
        _symbolFill = new JCheckBox("Solid Fill", true);
        _scEditor = new NoImageLineColorEditor();

        JPanel linePanel = new JPanel(new GridLayout(4, 2));
        linePanel.setBorder(new TitledBorder("Line Settings"));
        linePanel.add(_drawLine);
        linePanel.add(_allLine);
        linePanel.add(new JLabel("    Line Style"));
        linePanel.add(_lsEditor);
        linePanel.add(new JLabel("    Line Width"));
        linePanel.add(_lwEditor);
        linePanel.add(new JLabel("    Line Color"));
        linePanel.add(_lcEditor.getGUIAsButton());

        JPanel symbolPanel = new JPanel(new GridLayout(5, 2));
        symbolPanel.setBorder(new TitledBorder("Symbol Settings"));
        symbolPanel.add(_drawSymbol);
        symbolPanel.add(_symbolFill);
        symbolPanel.add(new JLabel("    Symbol Style"));
        symbolPanel.add(_symbolStyle);
        symbolPanel.add(new JLabel("    Symbol Width"));
        symbolPanel.add(_symbolWidth);
        symbolPanel.add(new JLabel("    Symbol Height"));
        symbolPanel.add(_symbolHeight);
        symbolPanel.add(new JLabel("    Symbol Color"));
        symbolPanel.add(_scEditor.getGUIAsButton());

        Box box = new Box(BoxLayout.Y_AXIS);
        box.add(Box.createVerticalStrut(10));
        box.add(linePanel);
        box.add(Box.createVerticalStrut(10));
        box.add(symbolPanel);
        box.add(Box.createVerticalGlue());

        this.setLayout(new FlowLayout());
        this.add(box);
    }
}
