/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.bhpBilliton.viewer2d.BhpEventLayer;
import com.bhpBilliton.viewer2d.BhpPickingManager;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.reader.TracePickingBehavior;
import java.text.NumberFormat;
import java.util.logging.Logger;
import javax.swing.border.EtchedBorder;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This dialog is for viewing and changing the picking
 *               settings of the viewer. Notice that the picking
 *               settings are global, affects all the internal frames.
 *               <br><br>
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @author Woody Folsom
 * @version 1.0
 */

public class BhpPickingDialog extends JDialog {
    private JRadioButton[] _pickModeRadios;
    private JRadioButton[] _snapModeRadios;

    private BhpPickingManager _pmanager;
    private BhpEventLayer _elayer;
    
    private BhpViewerBase viewer;

    private JTextField _snapLimitField;
    
    private static final Logger logger = Logger.getLogger(BhpPickingDialog.class.toString());
    /**
     * Constructs a new dialog and sets up the GUI.
     */
    public BhpPickingDialog(BhpViewerBase viewer) {
      super();
      this.viewer = viewer;
      setTitle("Picking");
      setLocation(viewer.getMyLocation().x + 10,viewer.getMyLocation().y + 10);
      _pmanager = viewer.getPickingManager();
      _elayer = null;
      buildPickingDialogGUI();
      pack();
    }

    /**
     * Gets related information of graphic settings
     * and fill the fields of GUI. <br>
     */
    public void setPickingSettings(BhpEventLayer elayer) {
        _elayer = elayer;
        int pmode = _pmanager.getPickMode();
        int smode = _pmanager.getSnapMode();
        if (elayer != null) smode = elayer.getLayerSnapMode();

        if (pmode == BhpPickingManager.PICK_POINT_TO_POINT)
            _pickModeRadios[1].setSelected(true);
        else _pickModeRadios[0].setSelected(true);

        switch (smode) {
            case BhpPickingManager.SNAP_MAX:
                _snapModeRadios[1].setSelected(true);
                break;
            case BhpPickingManager.SNAP_MIN:
                _snapModeRadios[2].setSelected(true);
                break;
            case BhpPickingManager.SNAP_ZERO:
                _snapModeRadios[3].setSelected(true);
                break;
            default:
                _snapModeRadios[0].setSelected(true);
                break;
        }
    }

    /**
     * Update the picking mode in the BhpPickingManager.
     */
    private void applyPickingMode() {
        int pmode = _pmanager.getPickMode();
        if (_pickModeRadios[1].isSelected())
            pmode = BhpPickingManager.PICK_POINT_TO_POINT;
        else pmode = BhpPickingManager.PICK_POINT_BY_POINT;
        _pmanager.setPickMode(pmode);
    }

    /**
     * Update the snapping mode in the BhpPickingManager.
     */
    private void applySnappingMode() {
        int smode = _pmanager.getSnapMode();
        if (_elayer != null) smode = _elayer.getLayerSnapMode();
        if (_snapModeRadios[0].isSelected())
            smode = BhpPickingManager.SNAP_NO;
        else if (_snapModeRadios[1].isSelected())
            smode = BhpPickingManager.SNAP_MAX;
        else if (_snapModeRadios[2].isSelected())
            smode = BhpPickingManager.SNAP_MIN;
        else if (_snapModeRadios[3].isSelected())
            smode = BhpPickingManager.SNAP_ZERO;
        else smode = BhpPickingManager.SNAP_NO;

        if (_elayer != null) _elayer.setLayerSnapMode(smode);
        else _pmanager.setSnapMode(smode);
        
        try {
            Integer SnapLimit = Integer.parseInt(_snapLimitField.getText());
            int snapLimit = SnapLimit.intValue();
            if (_elayer != null) {
                _elayer.setLayerSnapLimit(snapLimit);
            } else {
                _pmanager.setSnapLimit(snapLimit);
            }
        } catch (Exception ex) {
            logger.info("Unable to set snap limit from BhpPickingDialog: " + ex.getMessage());
        }
    }

    private void buildPickingDialogGUI() {
        this.getContentPane().add(buildContentPanel(), BorderLayout.CENTER);
        this.getContentPane().add(buildControlPanel(), BorderLayout.SOUTH);
    }

    private JPanel buildContentPanel() {
        _pickModeRadios = new JRadioButton[2];
        _pickModeRadios[0] = new JRadioButton(" Mouse Drag ");
        _pickModeRadios[1] = new JRadioButton(" Mouse Click");

        _snapModeRadios = new JRadioButton[4];
        _snapModeRadios[0] = new JRadioButton(" No Snap");
        _snapModeRadios[1] = new JRadioButton(" Snap Nearest Maximum");
        _snapModeRadios[2] = new JRadioButton(" Snap Nearest Minimum");
        _snapModeRadios[3] = new JRadioButton(" Snap Nearest Zero Crossing");

        JLabel[] tlabels = new JLabel[2];
        tlabels[0] = new JLabel("(Btn1+Drag to add points, Btn2+Drag to erase points)");
        tlabels[1] = new JLabel("(Btn1 to add points, Btn2 to erase points)");
        Font tfont = new Font(_pickModeRadios[0].getFont().getName(), Font.ITALIC, 10);
        tlabels[0].setFont(tfont);
        tlabels[1].setFont(tfont);

        JPanel panel1 = new JPanel(new GridLayout(_pickModeRadios.length, 1));
        panel1.setBorder(new javax.swing.border.TitledBorder("Picking Mode"));
        ButtonGroup pmode = new ButtonGroup();
        PickModeListener plistener = new PickModeListener();
        for(int i=0; i<_pickModeRadios.length; i++) {
            pmode.add(_pickModeRadios[i]);
            _pickModeRadios[i].addActionListener(plistener);
            //panel1.add(_pickModeRadios[i]);
            Box tbox = new Box(BoxLayout.X_AXIS);
            tbox.add(_pickModeRadios[i]);
            tbox.add(tlabels[i]);
            panel1.add(tbox);
        }
        SnapModeActionListener slistener = new SnapModeActionListener();
        JPanel panel2 = new JPanel(new GridLayout(_snapModeRadios.length, 1));
        panel2.setBorder(new javax.swing.border.TitledBorder("Snapping Mode"));
        
        ButtonGroup smode = new ButtonGroup();
        
        for(int i=0; i<_snapModeRadios.length; i++) {
            smode.add(_snapModeRadios[i]);
            _snapModeRadios[i].addActionListener(slistener);
            panel2.add(_snapModeRadios[i]);
        }

        Box box1 = new Box(BoxLayout.Y_AXIS);
        box1.add(panel1);
        box1.add(Box.createVerticalStrut(10));
        box1.add(panel2);
        box1.add(Box.createVerticalStrut(10));
        box1.add(createSnapLimitBox(slistener));
        box1.add(Box.createVerticalGlue());

        JPanel panel = new JPanel(new BorderLayout(/*20, 20*/));
        panel.setBorder(new EtchedBorder());
        panel.add(box1, BorderLayout.CENTER);

        return panel;
    }

    private JPanel buildControlPanel() {
        JButton clb = new JButton("Done");
        clb.addActionListener(new CancelListener());
        JButton helpb = new JButton("Help");
        helpb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                showHelp();
            }
        });
        JPanel panel = new JPanel(new FlowLayout());
        panel.add(clb);
        panel.add(helpb);
        return panel;
    }

    private void showHelp() {
        try {
            BhpViewerBase.broker.setCurrentID("preferences_picking");
            BhpViewerBase.broker.setDisplayed(true);
        }
        catch (Exception e) {
            ErrorDialog.showInternalErrorDialog(viewer, Thread.currentThread().getStackTrace(),
                                                "Couldn't show picking dialog help");
        }
    }

    private void hideDialog() {
        this.setVisible(false);
    }

    private class PickModeListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            applyPickingMode();
        }
    }

    private class SnapModeActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            applySnappingMode();
        }
    }

    private class SnapLimitFocusListener implements FocusListener {
        public void focusLost(FocusEvent fe) {
            applySnappingMode();
        }
        public void focusGained(FocusEvent fe) {
            //do nothing
        }
    }
    
    private class CancelListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            hideDialog();
        }
    }
    
    private JTextField createSnapLimitTextField(int defaultSnapLimit, SnapModeActionListener slistener) {
        JTextField textField = new JFormattedTextField(NumberFormat.getIntegerInstance());        
        textField.setColumns(5);
        textField.setText(new Integer(defaultSnapLimit).toString());
        textField.addActionListener(slistener);
        textField.addFocusListener(new SnapLimitFocusListener());
        return textField;
    }
    
    private JLabel createSnapLimitLabel() {
        return new JLabel("Snap Limit:");
    }
    
    private JPanel createSnapLimitBox(SnapModeActionListener slistener) {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.add(createSnapLimitLabel());
        _snapLimitField = createSnapLimitTextField(TracePickingBehavior.SNAP_DEFAULT, slistener);
        panel.add(_snapLimitField);   
        return panel;
    }
}