/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d.ui;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.JOptionPane;
import javax.swing.event.*;

import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpPlotXV;
import com.bhpBilliton.viewer2d.BhpTraceAxisObject;
//import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.indexing.SegyBinaryEntry;
import com.gwsys.seismic.indexing.HeaderFieldEntry;
import com.gwsys.seismic.util.PlotConstants;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This panel is used to view and change the trace (horizontal)
 *               axis setting for a <code>{@link BhpPlotXV}</code>.
 *               It borrows part of its code
 *               from cgTraceAnnPanel of JSeismic.
 *               Its bahavior is also close to that of cgTraceAnnPanel.
 *               Users can choose what to annotate horizontally
 *               and where to annotate. From fields chosen to be
 *               annotated, users can also choose one as the
 *               synchornization field. This determines how
 *               mouse and window position synchronization will
 *               be carried on if they are truned on. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpTraceAnnPanel extends JPanel {
    //private BhpWindow _window;
    private BhpPlotXV _plotXV;

    private DefaultListModel _available    = new DefaultListModel();
    private DefaultListModel _oldAvailable = new DefaultListModel();
    private DefaultListModel _selected     = new DefaultListModel();
    private DefaultListModel _oldSelected  = new DefaultListModel();
    private int    _location;
    private int    _oldLocation;

    private JList _availableFields;
    private JList _selectedFields;
    private JButton _addButton;
    private JButton _removeButton;
    private JButton _upButton;
    private JButton _downButton;
    private JButton _editButton;
    //private JTextField _stpField;
    private JComboBox _locationField;

    private JComboBox _syncField;

    /**
     * Constructs a BhpTraceAnnPanel and sets up the GUI.
     */
    public BhpTraceAnnPanel() {
        super();
        _plotXV = null;
        int fixedCellWidth = 100;

        _available.addListDataListener(new AvailableListDataListener());
        _availableFields = new JList(_available);
        _availableFields.setCellRenderer(new AvailableFieldCellRenderer());
        _availableFields.setFixedCellWidth(fixedCellWidth);
        _availableFields.getSelectionModel().addListSelectionListener(
                                            new AvailableListSelectionListener());
        JScrollPane scrollPane1 = new JScrollPane(_availableFields);

        JPanel panel1 = new JPanel();
        panel1.setLayout(new BoxLayout(panel1, BoxLayout.X_AXIS));
        panel1.setBorder(BorderFactory.createTitledBorder("Available Keys"));
        panel1.add(scrollPane1);

        _addButton = new JButton("==>");
        _addButton.setEnabled(false);
        _addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int[] sel = _availableFields.getSelectedIndices();
                for (int i=0; i<sel.length; i++) {
                    //StringBuffer miscDesc = new StringBuffer();
                    HeaderFieldEntry fldDesc = (HeaderFieldEntry) _available.elementAt(sel[i]);
                    _selected.addElement(new BhpTraceAxisObject(fldDesc));
                    _syncField.addItem(fldDesc.getName());
                }
                for (int i=sel.length-1; i>=0; i--)
                    _available.remove(sel[i]);
            }
        });

        _removeButton = new JButton("<==");
        _removeButton.setEnabled(false);
        _removeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int[] sel = _selectedFields.getSelectedIndices();
                for (int i=0; i<sel.length; i++) {
                    BhpTraceAxisObject traceAxisObj = (BhpTraceAxisObject) (_selected.elementAt(sel[i]));
                    _available.addElement(traceAxisObj._field);
                    _syncField.removeItem(traceAxisObj._name);
                }
                for (int i=sel.length-1; i>=0; i--) {
                    _selected.remove(sel[i]);
                }
            }
        });

        JPanel panel2 = new JPanel();
        panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));
        panel2.add(Box.createGlue());
        panel2.add(_addButton);
        panel2.add(Box.createGlue());
        panel2.add(_removeButton);
        panel2.add(Box.createGlue());

        _selected.addListDataListener(new SelectedListDataListener());
        _selectedFields = new JList(_selected);
        _selectedFields.setCellRenderer(new SelectedFieldCellRenderer());
        JScrollPane scrollPane2 = new JScrollPane(_selectedFields);
        _selectedFields.getSelectionModel().addListSelectionListener(
                                            new SelectedListSelectionListener());
        _selectedFields.addMouseListener(new SelectedListMouseListener());
        _selectedFields.setFixedCellWidth(fixedCellWidth);

        _upButton = new JButton("Up");
        _upButton.setEnabled(false);
        _upButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int idx = _selectedFields.getSelectedIndex();
                Object objFld = _selected.get(idx-1);
                _selected.set(idx-1, _selected.get(idx));
                _selected.set(idx, objFld);
                _selectedFields.setSelectedIndex(idx-1);
            }
        });

        _downButton = new JButton("Dn");
        _downButton.setEnabled(false);
        _downButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int idx = _selectedFields.getSelectedIndex();
                Object objFld = _selected.get(idx+1);
                _selected.set(idx+1, _selected.get(idx));
                _selected.set(idx, objFld);
                _selectedFields.setSelectedIndex(idx+1);
            }
        });

        _editButton = new JButton("Edit");
        _editButton.setEnabled(false);
        _editButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int idx = _selectedFields.getSelectedIndex();
                createAxisSettingEditor(idx);
            }
        });

        JPanel panel31 = new JPanel();
        panel31.setLayout(new BoxLayout(panel31, BoxLayout.Y_AXIS));
        panel31.add(_upButton);
        panel31.add(_downButton);
        panel31.add(_editButton);
        panel31.add(Box.createGlue());
        JPanel panel3 = new JPanel();
        panel3.setLayout(new BoxLayout(panel3, BoxLayout.X_AXIS));
        panel3.setBorder(BorderFactory.createTitledBorder("Selected Keys"));
        panel3.add(scrollPane2);
        panel3.add(panel31);

        _locationField = new JComboBox();
        _locationField.setEditable(false);
        _locationField.addItem("NONE");
        _locationField.addItem("TOP");
        //_locationField.addItem("BOTTOM");
        //_locationField.addItem("TOP & BOTTOM");

        _syncField = new JComboBox();
        _syncField.setEditable(false);

        JPanel panelUp = new JPanel();
        panelUp.setLayout(new BoxLayout(panelUp, BoxLayout.X_AXIS));
        panelUp.add(Box.createGlue());
        panelUp.add(panel1);
        panelUp.add(Box.createGlue());
        panelUp.add(panel2);
        panelUp.add(Box.createGlue());
        panelUp.add(panel3);
        panelUp.add(Box.createGlue());

        JPanel panelDn = new JPanel();
        panelDn.setLayout(new GridLayout(2, 2));
        panelDn.add(new JLabel("    Location"));
        panelDn.add(_locationField);
        panelDn.add(new JLabel("    Synchronization Key"));
        panelDn.add(_syncField);
/*
        panelDn.setLayout(new BoxLayout(panelDn, BoxLayout.X_AXIS));
        panelDn.add(Box.createGlue());
        panelDn.add(new JLabel());
        panelDn.add(Box.createGlue());
        panelDn.add(new JLabel("Location"));
        panelDn.add(Box.createGlue());
        panelDn.add(_locationField);
        panelDn.add(Box.createGlue());
*/

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(Box.createGlue());
        add(panelUp);
        add(Box.createGlue());
        add(panelDn);
        add(Box.createGlue());
    }

    /**
     * Enables or diables the components in the panel for
     * either an active or a grayed-out state.
     */
    public void setAllComponentEnabled(boolean v) {
        _locationField.setEnabled(v);
        _syncField.setEnabled(v);
    }

    /**
     * Retrieves the <code>{@link BhpLayer}</code>
     * related information and fill the fields of the GUI.
     */
    public void initWithLayer(BhpLayer layer) {
        _available.clear();
        _selected.clear();
        _syncField.removeAllItems();

        if (layer == null || _plotXV == null) return;
        if (layer.getPipeline().getDataLoader().getDataReader().getDataFormat()==null) return;
        if (layer.getIdNumber() == _plotXV.getAxisAssociateLayer()) {
            for (int i=0; i<_oldAvailable.size(); i++)
                _available.addElement(_oldAvailable.getElementAt(i));
            for (int j=0; j<_oldSelected.size(); j++)
                _selected.addElement(_oldSelected.getElementAt(j));
        }
        else {
            Vector original = layer.getPipeline().getDataLoader().getDataReader().getKeys();
            String defaultHName = layer.getParameter().getXSectionHorizontalKeyName();
            HeaderFieldEntry tmpField = null;
            for (int i=0; i<original.size(); i++) {
                tmpField = (SegyBinaryEntry) original.get(i);
                if (tmpField.getName().equals(defaultHName))
                    _selected.addElement(new BhpTraceAxisObject(tmpField));
                else _available.addElement(tmpField);
            }

        }

        for (int i=0; i<_selected.size(); i++) {
            _syncField.addItem(((BhpTraceAxisObject)_selected.getElementAt(i))._name);
        }
        String currentSelection = _plotXV.getHAxesSyncName();
        _syncField.setSelectedItem(currentSelection);
        if(_syncField.getSelectedItem() == null && _syncField.getItemCount() > 0)
            _syncField.setSelectedIndex(0);
    }

    /**
     * Retrieves the <code>{@link BhpPlotXV}</code>
     * related information and fill the fields of the GUI.
     */
    public void initWithBhpWindow(BhpPlotXV p) {
        _plotXV = p;

        ArrayList original;

        //_hSyncField.setText(p.getHAxesSyncName());
        original = _plotXV.getAvailable();
        _available.clear();
        _oldAvailable.clear();
        for (int i=0; i<original.size(); i++) {
            _available.addElement(original.get(i));
            _oldAvailable.addElement(original.get(i));
        }
        original = _plotXV.getSelected();
        _selected.clear();
        _oldSelected.clear();
        for (int i=0; i<original.size(); i++) {
            _selected.addElement(original.get(i));
            _oldSelected.addElement(original.get(i));
        }

        _syncField.removeAllItems();
        for (int i=0; i<_selected.size(); i++) {
            _syncField.addItem(((BhpTraceAxisObject)_selected.getElementAt(i))._name);
        }
        String currentSelection = _plotXV.getHAxesSyncName();
        _syncField.setSelectedItem(currentSelection);
        if(_syncField.getSelectedItem() == null && _syncField.getItemCount() > 0)
            _syncField.setSelectedIndex(0);

        _oldLocation = _location = _plotXV.getHAxesLoc();
        switch( _location ) {
            case PlotConstants.TOP:
                _locationField.setSelectedItem("TOP");
                break;
            case PlotConstants.BOTTOM:
                _locationField.setSelectedItem("BOTTOM");
                break;
            case PlotConstants.TOP_BOTTOM:
                _locationField.setSelectedItem("TOP & BOTTOM");
                break;
            default:
                _locationField.setSelectedItem("NONE");
        }
    }

    /**
     * Retrieves valus from the GUI components and
     * updates the annotation settings of
     * <code>{@link BhpPlotXV}</code>.
     * @return a boolean indicates if the annotation is changed
     * and needs an update.
     */
    public boolean updateBhpWindow() {
        boolean changed = false;

        if (_syncField.getSelectedItem() != null)
            _plotXV.setHAxesSyncName(_syncField.getSelectedItem().toString());
        else _plotXV.setHAxesSyncName("");
        if( !_available.equals( _oldAvailable ) ) {
            Vector fldDesc = new Vector();
            _oldAvailable.removeAllElements();
            for( int i=0; i < _available.size(); i++ ) {
                fldDesc.add( _available.get( i ));
                _oldAvailable.addElement( _available.get( i ));
            }
            //plot.setAvailable( fldDesc );
            changed = true;
        }

        if( !_selected.equals( _oldSelected ) ) {
            ArrayList fldDesc = new ArrayList();
            _oldSelected.clear();
            for( int i=0; i < _selected.size(); i++ ) {
                fldDesc.add( _selected.get( i ));
                _oldSelected.addElement( _selected.get( i ));
            }
            _plotXV.setSelectedAndAvailable( fldDesc );
            changed = true;
        }

        String strSel = (String)_locationField.getSelectedItem();
        if (strSel.equals("TOP"))               _location = PlotConstants.TOP;
        else if (strSel.equals("BOTTOM"))       _location = PlotConstants.BOTTOM;
        else if (strSel.equals("TOP & BOTTOM")) _location = PlotConstants.TOP_BOTTOM;
        else                                    _location = PlotConstants.NONE;
        if( _location != _oldLocation ) {
            _plotXV.setHAxesLoc(_location);
            _oldLocation = _location;
            changed = true;
        }

        //if(changed) plot.updateTraceAxes();
        return changed;
    }

    private void createAxisSettingEditor(int idx) {
        Container c = getParent();
        while((c!=null) && !(c instanceof JDialog)) c = c.getParent();
        JDialog dlg;
        if (c != null) dlg = (JDialog) c;
        else dlg = null;

        BhpTraceAxisObject axis = (BhpTraceAxisObject)(_selected.get(idx));
        cgAxisSettingsEditor axisEditor = new cgAxisSettingsEditor(dlg, axis._name, idx);
        if (dlg != null) axisEditor.setLocationRelativeTo(dlg);
        axisEditor.pack();
        axisEditor.setVisible(true);
    }

    // Inner classes
    private class AvailableFieldCellRenderer extends JLabel implements ListCellRenderer {
        public AvailableFieldCellRenderer() {
            setOpaque(true);
        }
        public Component getListCellRendererComponent( JList list, Object value,
                        int index, boolean isSelected, boolean cellHasFocus) {

            setText(((HeaderFieldEntry)value).getName());
            if (isSelected) {
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
            }
            else {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }
            setEnabled(list.isEnabled());
            setFont(list.getFont());
            return this;
        }
    }

    private class SelectedFieldCellRenderer extends JLabel implements ListCellRenderer {
        public SelectedFieldCellRenderer() {
            setOpaque(true);
        }

        public Component getListCellRendererComponent(JList list, Object value,
                        int index, boolean isSelected, boolean cellHasFocus) {
/*
            if (value instanceof String) setText((String)value);
            else {
                String s = ((HeaderFieldEntry)value).getName();
                Double step = (Double)_steps.get(index);
                if (step.doubleValue() == 0) setText(s + " [Any]");
                else setText(s + " [" + step.toString() + "]");
            }
*/
            if (value != null && value instanceof BhpTraceAxisObject)
                setText(((BhpTraceAxisObject)value)._name);
            if (isSelected) {
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
            }
            else {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }
            setEnabled(list.isEnabled());
            setFont(list.getFont());
            return this;
        }
    }

    private class AvailableListDataListener implements ListDataListener {
        public void contentsChanged(ListDataEvent e) {
            if (_available.getSize() == 0) _addButton.setEnabled(false);
        }
        public void intervalAdded(ListDataEvent e) {
        }
        public void intervalRemoved(ListDataEvent e) {
            if (_available.getSize() == 0) _addButton.setEnabled(false);
        }
    }

    private class AvailableListSelectionListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent e) {
            if (_availableFields.isSelectionEmpty()) _addButton.setEnabled(false);
            else _addButton.setEnabled(true);
        }
    }

    private class SelectedListDataListener implements ListDataListener {
        public void contentsChanged(ListDataEvent e) {
            if (_selected.getSize() == 0) {
                _removeButton.setEnabled(false);
                _upButton.setEnabled(false);
                _downButton.setEnabled(false);
                _editButton.setEnabled(false);
            }
            if (_selected.getSize() == 1) {
                _upButton.setEnabled(false);
                _downButton.setEnabled(false);
            }
        }
        public void intervalAdded(ListDataEvent e) {
        }
        public void intervalRemoved(ListDataEvent e) {
            if (_selected.getSize() == 0) {
                _removeButton.setEnabled(false);
                _upButton.setEnabled(false);
                _downButton.setEnabled(false);
                _editButton.setEnabled(false);
            }
            if (_selected.getSize() == 1) {
                _upButton.setEnabled(false);
                _downButton.setEnabled(false);
            }
        }
    }

    private class SelectedListSelectionListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent e) {
            if (_selectedFields.isSelectionEmpty()) {
                _removeButton.setEnabled(false);
                _upButton.setEnabled(false);
                _downButton.setEnabled(false);
                _editButton.setEnabled(false);
            }
            else {
                _removeButton.setEnabled(true);
                int selNum = 0;
                for (int i=0; i<_selected.getSize(); i++)
                    if (_selectedFields.isSelectedIndex(i)) selNum++;
                if (selNum > 1) {
                    _upButton.setEnabled(false);
                    _downButton.setEnabled(false);
                    _editButton.setEnabled(false);
                }
                else {
                    _editButton.setEnabled(true);
                    if (_selected.getSize() == 1) {
                        _upButton.setEnabled(false);
                        _downButton.setEnabled(false);
                    }
                    else if (_selectedFields.isSelectedIndex(0)) {
                        _upButton.setEnabled(false);
                        _downButton.setEnabled(true);
                    }
                    else if (_selectedFields.isSelectedIndex(_selected.getSize()-1)) {
                        _upButton.setEnabled(true);
                        _downButton.setEnabled(false);
                    }
                    else {
                        _upButton.setEnabled(true);
                        _downButton.setEnabled(true);
                    }
                }
            }
        }
    }

    private class SelectedListMouseListener extends MouseAdapter {
        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() == 2) {
                int idx = _selectedFields.locationToIndex(e.getPoint());
                _selectedFields.clearSelection();
                _selectedFields.setSelectedIndex(idx);
                createAxisSettingEditor(idx);
            }
        }
    }

    private class cgAxisSettingsEditor extends JDialog {
        private int _idx;
        private String _fldName;
        private double _oldStep;
        private double _oldAngle;
        private boolean _oldLineStatus;
        private double _oldGap;
        private int _oldFlag;
        private double _oldLimit;

        private JTextField _stepField;
        private JTextField _gapField;
        private JTextField _angleField;
        private JCheckBox _drawLines;

        private JTextField _limitField;
        private JComboBox _flagCombo;

        public cgAxisSettingsEditor(Dialog owner, String fldName, int idx) {
            super(owner, fldName, false);
            _fldName = fldName;
            _idx = idx;

            _stepField = new JTextField();
            _angleField = new JTextField();
            _gapField = new JTextField();

            String[] flagValues = {"All labels", "Labels bigger than limit",
                                    "Labels smaller than limit"};
            _limitField = new JTextField();
            _flagCombo = new JComboBox(flagValues);

            JPanel panel1 = new JPanel(new GridLayout(5, 2));
            panel1.setMaximumSize(new Dimension(300, 200));
            panel1.add(new JLabel("  Label Step (in traces)"));
            panel1.add(_stepField);
            panel1.add(new JLabel("  Label Angle"));
            panel1.add(_angleField);
            panel1.add(new JLabel("  Min Label Space (in cm)"));
            panel1.add(_gapField);
            panel1.add(new JLabel("  Label Flag"));
            panel1.add(_flagCombo);
            panel1.add(new JLabel("  Label Limit"));
            panel1.add(_limitField);

            _drawLines = new JCheckBox("Show lines");
            JPanel panel2 = new JPanel();
            panel2.setLayout(new BoxLayout(panel2, BoxLayout.X_AXIS));
            panel2.setMaximumSize(new Dimension(200, 15));
            panel2.add(Box.createGlue());
            panel2.add(_drawLines);
            panel2.add(Box.createGlue());

            JPanel panel = new JPanel();
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            panel.add(Box.createVerticalStrut(10));
            panel.add(panel1);
            panel.add(Box.createVerticalStrut(10));
            panel.add(panel2);
            panel.add(Box.createVerticalStrut(10));

            getContentPane().add("Center", panel);
            getContentPane().add("South", buildControlPanel());

            try {
                getAxisSettings();
            }
            catch (Exception ex) {
                JOptionPane.showMessageDialog(null,
                            "BhpTraceAnnPanel axis editor error " + _fldName + "Getting of axis settings failed",
                            "Internal Error", JOptionPane.ERROR_MESSAGE);
/*
                ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                "BhpTraceAnnPanel axis editor error " + _fldName + "Getting of axis settings failed");
*/
            }
        }

        protected void getAxisSettings() throws Exception {
            BhpTraceAxisObject taObject = (BhpTraceAxisObject) _selected.elementAt(_idx);
            if (taObject._field.getFieldState()) {
                _stepField.setEnabled(true);
                _gapField.setEnabled(false);
            }
            else {
                _stepField.setEnabled(false);
                _gapField.setEnabled(true);
            }
            _oldStep = taObject._step;
            _oldAngle = taObject._labelAngle * 180 / (-Math.PI);
            _oldFlag = taObject._labelFlag;
            _oldLimit = taObject._labelLimit;
            _oldGap = taObject._tickSpace;
            _oldLineStatus = taObject._drawLine;
            _limitField.setText("" + _oldLimit);
            _flagCombo.setSelectedIndex(_oldFlag);
            _gapField.setText("" + convertPixelToCM(_oldGap));
            _stepField.setText("" + _oldStep);
            _angleField.setText("" + _oldAngle);
            if (_oldLineStatus) _drawLines.doClick();
        }

        protected void updateAxisSettings() {
            BhpTraceAxisObject taObject = (BhpTraceAxisObject) _selected.elementAt(_idx);

            try {
                float step = Float.parseFloat(_stepField.getText());
                taObject._step = step;
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null,
                            "BhpTraceAnnPanel axis editor error " + _fldName + "updateAxisSettings failed for step",
                            "Internal Error", JOptionPane.ERROR_MESSAGE);
/*
                ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                "BhpTraceAnnPanel axis editor error " + _fldName + "updateAxisSettings failed for step");
*/
            }
            try {
                double newGap = convertCMToPixel(Double.parseDouble(_gapField.getText()));
                taObject._tickSpace = newGap;
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null,
                            "BhpTraceAnnPanel axis editor error " + _fldName + "updateAxisSettings failed for gap",
                            "Internal Error", JOptionPane.ERROR_MESSAGE);
/*
                ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                 "BhpTraceAnnPanel axis editor error " + _fldName + "updateAxisSettings failed for gap");
*/
            }
            try {
                float angle = Float.parseFloat(_angleField.getText());
                taObject._labelAngle = angle * Math.PI / (-180);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null,
                            "BhpTraceAnnPanel axis editor error " + _fldName +
                                                        "updateAxisSettings failed for label angle",
                            "Internal Error", JOptionPane.ERROR_MESSAGE);
/*
                ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                    "BhpTraceAnnPanel axis editor error " + _fldName +
                                                        "updateAxisSettings failed for label angle");
*/
            }

            boolean lineStatus = _drawLines.isSelected();
            taObject._drawLine = lineStatus;
            int newFlag = _flagCombo.getSelectedIndex();
            taObject._labelFlag = newFlag;
            try {
                double newLimit = Double.parseDouble(_limitField.getText());
                taObject._labelLimit = newLimit;
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null,
                            "BhpTraceAnnPanel axis editor error " + _fldName +
                                                        "updateAxisSettings failed for limit",
                            "Internal Error", JOptionPane.ERROR_MESSAGE);
/*
                ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
                                                    "BhpTraceAnnPanel axis editor error " + _fldName +
                                                        "updateAxisSettings failed for limit");
*/
            }
        }

        protected JPanel buildControlPanel() {
            JPanel panel = new JPanel(new GridLayout());
            JButton okBtn = new JButton("Ok");
            JButton clBtn = new JButton("Cancel");
            panel.add(okBtn);
            panel.add(clBtn);

            okBtn.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    updateAxisSettings();
                    dispose();
                }
            });
            clBtn.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });

            return panel;
        }

        private double convertCMToPixel(double ori) {
            int pixelsPerInch = Toolkit.getDefaultToolkit().getScreenResolution();
            double pixelsPerCM = pixelsPerInch / 2.54;
            return (ori * pixelsPerCM);
        }

        private double convertPixelToCM(double ori) {
            int pixelsPerInch = Toolkit.getDefaultToolkit().getScreenResolution();
            double pixelsPerCM = pixelsPerInch / 2.54;
            double value = Math.round(100*ori/pixelsPerCM)/100.0;
            return value;
        }
    }
}
