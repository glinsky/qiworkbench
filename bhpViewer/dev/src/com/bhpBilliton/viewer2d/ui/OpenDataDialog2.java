package com.bhpBilliton.viewer2d.ui;

import com.bhpBilliton.viewer2d.BhpLayer;
import com.bhpBilliton.viewer2d.BhpViewerBase;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

/**
 * Extracted from OpenDataAction.  Refactored to continue the process of extricating common code
 * from Swing-initiated layer loading, drag-and-drop layer loading, programmatic layer loading etc.
 *
 * @author folsw9
 */

public class OpenDataDialog2 extends JDialog {
        private OpenDataPanel cpanel;

        public OpenDataDialog2(BhpViewerBase v, BhpLayer l, ActionListener okListener, 
                ActionListener cancelListener) {
          // cant use super since parent is not a Frame
          setTitle("Open Dataset in New X-Section Viewer");
          setModal(true);
          setLocation(v.getMyLocation());
          setupGUI(v, l, okListener, cancelListener);
        }

        public OpenDataPanel getOpenPanel() {
            return cpanel;
        }

        public boolean acceptTableChange() {
            return cpanel.acceptTableChange();
        }

        private void setupGUI(Container parent, BhpLayer layer,  
                ActionListener okListener, ActionListener cancelListener) {
            cpanel = new OpenDataPanel(parent, layer, false);
            JPanel tpanel = new JPanel(new FlowLayout());
            JButton ob = new JButton("Ok");
            JButton cb = new JButton("Cancel");
            ob.addActionListener(okListener);
            cb.addActionListener(cancelListener);
            tpanel.add(ob);
            tpanel.add(cb);

            this.getContentPane().add(tpanel, BorderLayout.SOUTH);
            this.getContentPane().add(cpanel, BorderLayout.CENTER);
        }
    }