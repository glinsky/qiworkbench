/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.util.List;

import javax.swing.JComponent;

import com.gwsys.gw2d.model.CommonDataModel;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.gw2d.model.CommonShapeLayer;
import com.gwsys.gw2d.shape.Polyline2D;
import com.gwsys.gw2d.util.Transform2D;
import com.gwsys.gw2d.view.VisuableComponent;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is a customized cgEditController of JCarnacPro.
 *               Because the JCarnacPro class cannot be easily adapted for
 *               the required behavior, this class is created with most of
 *               its code borrowing from its JCarnacPro conterpart. Please
 *               refer to the JCarnacPro documentation for more detail. <br>
 *               This class is used for event shape editing only. When a
 *               <code>{@link BhpEventLayer}</code> is selected, its
 *               <code>{@link BhprEventShape}</code> will be selected
 *               automatically. The shape can be editted with mouse.
 *               The shape is highlighted by drawing a big fat line
 *               along the original path with user defined highlight color.
 *               <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpEditorController {
    private JComponent        _view;

    private RenderingAttribute _attribute;

   /**
    * Constructs an object which contains methods for handling editing operations.
    * @param view the View associated with the shapes being edited.
    */
    public BhpEditorController(JComponent view, RenderingAttribute att) {
        _view = view;
        _attribute = att;
        calcTransformation();
    }

   /*
    * Uses the view to get the transformation
    */
    private Transform2D calcTransformation() {
        Transform2D transform;
        if (_view instanceof VisuableComponent) {
            transform = ((VisuableComponent)_view).getTransformation();
        } else {
            transform = new Transform2D();
        }
        return transform;
    }

    /////////////////////////////
    // editing
    /////////////////////////////

   /**
    * This is a utility method used to construct a layer containing control elements.
    * These control elements are shape bounding boxes and manipulation control handle boxes
    * used to highlight selected shapes.  This method is typically used to create or
    * refresh a control layer after some editing operations have taken place.  The
    * current control layer is passed in (or null if none existed and you are creating one)
    * so that it can be removed from the model.  A new layer is created and populated
    * with control elements for the selected shapes by instantiating a new
    * {@link com.interactive.jcarnac2d.edit.DataShapeHighlighter <code>DataShapeHighlighter</code>}
    * object.  The layer is added to the model and returned.  The calling object should
    * then make the returned new layer the current one.
    * @param selectedShapes A collection of shapes to be highlighted.
    * @param model the model which will contain the control objects.
    * @param controlLayer The old control layer (or null) which will be removed from the model.
    * @return The new control layer.
    */
    public CommonShapeLayer createCtlHandlesBBoxes(List selectedShapes,
                                                   CommonDataModel  model,
                                                   CommonShapeLayer controlLayer) {

        if (selectedShapes == null) {return controlLayer;}

        calcTransformation();

        // Remove the old layer from the model
        if (controlLayer != null) {
            model.removeLayer(controlLayer);
            controlLayer = null;
        }

        // Regenerate the control layer
        controlLayer = new CommonShapeLayer();
        Object shape = null;
        for (int i=0; i<selectedShapes.size(); i++) {
            shape = selectedShapes.get(i);
            if (shape instanceof Polyline2D) {
                Polyline2D pl = (Polyline2D)(((Polyline2D)shape).getGeometry());
                pl.setAttribute(_attribute);
                controlLayer.addShape(pl);
            }
        }
        //DataShapeHighlighter newControlLayer = new DataShapeHighlighter(selectedShapes, model, controlLayer, _tr);
        model.addLayer(controlLayer);
        _view.repaint();

        return controlLayer;
    }

}
