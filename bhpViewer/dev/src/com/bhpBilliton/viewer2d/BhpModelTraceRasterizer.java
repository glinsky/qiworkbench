/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;


import java.awt.Dimension;
import javax.swing.JOptionPane;

import com.bhpBilliton.viewer2d.data.BhpModelReaderInterface;
import com.gwsys.seismic.core.DefaultRasterizer;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.NumberRange;
import com.gwsys.seismic.util.SeismicColorMap;
import com.gwsys.seismic.util.SeismicColorEvent;
import com.gwsys.seismic.util.SeismicColorListener;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is the rasterizer that really draws the model display image.
 *               Besides general seismic display types, it can draw the model
 *               display as structural interpolated. With Structual interpolated
 *               density, model layers are clearly displayed.
 *               This rasterizer uses the depth array of the corresponding
 *               trace to determine the value at certain vertical position
 *               in a trace. <br><br>
 *
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public abstract class BhpModelTraceRasterizer extends AbstractBhpColorInterpolatedRasterizer {
    protected BhpModelReaderInterface _reader;

    private float _startDepth;
    private float _endDepth;

    protected byte[] _imageData = null;
    protected int _imageWidth;
    protected int _imageHeight;
    protected int _scanLineSize;
    private int _plotType;

    protected boolean _densityPlot                = false;
    protected boolean _interpolatedDensityPlot    = false;
    protected boolean _wigglePlot                 = false;
    private boolean _lobeFillPlot               = false;
    protected boolean _positiveFillPlot           = false;
    protected boolean _negativeFillPlot           = false;
    protected boolean _positiveColorFillPlot      = false;
    protected boolean _negativeColorFillPlot      = false;

    protected double _deltaPixels;
    private float _clippingValue;
    protected byte _foregroundPixel;
    protected byte _backgroundPixel;
    protected byte _negativeFillPixel;
    protected  byte _positiveFillPixel;
    protected byte _highlightPixel;

    protected int _numColors;
    protected boolean _reverseFill = false;
    protected SeismicColorMap _colorMap = null;

    private SeismicColorListener _cml = new SeismicColorListener() {
        public void colorMapUpdated(SeismicColorEvent e) {
            updateColorMap();
        }
    };

    /**
     * Constructs a new instance.
     * @param reader the model reader of the pipeline.
     */
    public BhpModelTraceRasterizer(BhpModelReaderInterface reader) {
        super();
        _reader = reader;
        _startDepth = _reader.getMinDepth();
        _endDepth = _reader.getMaxDepth();
        setPlotType(DefaultRasterizer.WIGGLE_TRACE);
        setColorMap(new SeismicColorMap());
        setClippingValue(4);
        //setReverseFill(false);
    }

    /**
     * Sets the model reader of the pipeline.
     */
    public void setBhpModelReader(BhpModelReaderInterface reader) {
        _reader = reader;
        _startDepth = _reader.getMinDepth();
        _endDepth = _reader.getMaxDepth();
    }

    /**
     * Gets the flag of structural interpolation.
     */
    abstract public boolean getStructuralInterpFlag();
    /**
     * Sets the flag of structural interpolation.
     */
    abstract public void setStructuralInterpFlag(boolean f);

    /**
     * Gets the value of the start depth.
     */
    public float getStartDepth()       { return _startDepth; }
    /**
     * Gets the value of the end depth.
     */
    public float getEndDepth()         { return _endDepth; }
    /**
     * Sets the value of the start depth.
     */
    public void setStartDepth(float d) { _startDepth = d; }
    /**
     * Sets the value of the end depth.
     */
    public void setEndDepth(float d)   { _endDepth = d; }

    /**
     * Retrieves the clipping value that determines to
     * which extent traces are displayed. <br>
     * Please refer to JSeismic documentation for more detail.
     */
    public float getClippingValue() {
        if (_plotType == DefaultRasterizer.DENSITY_TRACE ||
            _plotType == DefaultRasterizer.INTERPOLATED_DENSITY_TRACE) return 1;
        else return _clippingValue;
    }

    /**
     * Retrieves the color map currently used by the rasterizer. <br>
     * Please refer to JSeismic documentation for more detail.
     */
    public SeismicColorMap getColorMap() {
        return _colorMap;
    }

    /**
     * Gets the plot type. <br>
     * Please refer to JSeismic documentation for more detail.
     */
    public int getPlotType() {
        return _plotType;
    }

    /**
     * Sets the clipping value that determines to
     * which extent traces are displayed. <br>
     * Please refer to JSeismic documentation for more detail.
     */
    public void setClippingValue(float cv) {
        if (cv < 0) {
            JOptionPane.showMessageDialog(null,"Only non-negative clipping values are allowed");
            System.out.println("BhpModelTraceRasterizer.setClippingValue Warning:");
            System.out.println("    Only non-negative clipping values are allowed");
            return;
        }
        _clippingValue = cv;
    }

    /**
     * Sets the color map used by the rasterizer. <br>
     * Please refer to JSeismic documentation for more detail.
     */
    public void setColorMap(SeismicColorMap colorMap) {
        if (_colorMap != null) _colorMap.removeColorMapListener(_cml);
        _colorMap = colorMap;
        if (_colorMap != null) {
            updateColorMap();
            _colorMap.addColorMapListener(_cml);
        }
    }

    /**
     * Sets the spacing between traces in screen coordinates(pixels). <br>
     * Please refer to JSeismic documentation for more detail.
     */
    public void setTraceSpace(double d) {
        _deltaPixels = d;
    }
/*
    public void setReverseFill(boolean status) {
        _reverseFill = status;
    }
*/
    /**
     * Sets the flag of reverse fill. Not supported by this class.
     */
    public void setReverseFill(boolean r) {}
    /**
     * Gets the flag of reverse fill. Not supported by this class.
     */
    public boolean isReverseFill() { return false; }

    /**
     * Defines the destination image data array for rasterizer. <br>
     * Please refer to JSeismic documentation for more detail.
     * @param imageData the destination image data array.
     * @param w the width, in bytes, of the data array.
     * @param h the height, in pixels, of the drawable area.
     */
    public void setImageData(byte[] imageData, int w, int h) {
        _imageData = imageData;
        _imageWidth = w;
        _imageHeight = h;
        _scanLineSize = ((_imageWidth - 1) / 4 + 1) * 4;
    }

    /**
     * Sets the plot type. <br>
     * Please refer to JSeismic documentation for more detail.
     */
    public void setPlotType(int plotType) {
        _plotType = plotType;
        _densityPlot = false;
        _interpolatedDensityPlot = false;
        _wigglePlot = false;
        _positiveFillPlot = false;
        _negativeFillPlot = false;
        _positiveColorFillPlot = false;
        _negativeColorFillPlot = false;
        _lobeFillPlot = false;

        if ((_plotType & DefaultRasterizer.DENSITY_TRACE) == DefaultRasterizer.DENSITY_TRACE)
            _densityPlot = true;
        if ((_plotType & DefaultRasterizer.INTERPOLATED_DENSITY_TRACE) == DefaultRasterizer.INTERPOLATED_DENSITY_TRACE)
            _interpolatedDensityPlot = true;
        if ((_plotType & DefaultRasterizer.WIGGLE_TRACE) == DefaultRasterizer.WIGGLE_TRACE)
            _wigglePlot = true;
        if ((_plotType & DefaultRasterizer.POSITIVE_FILL_TRACE) == DefaultRasterizer.POSITIVE_FILL_TRACE)
            _positiveFillPlot = true;
        if ((_plotType & DefaultRasterizer.NEGATIVE_FILL_TRACE) == DefaultRasterizer.NEGATIVE_FILL_TRACE)
            _negativeFillPlot = true;

        if (_densityPlot) {
            _interpolatedDensityPlot = false;
            _lobeFillPlot = false;
            _positiveColorFillPlot = false;
            _negativeColorFillPlot = false;
        }
        else if (_interpolatedDensityPlot) {
            _densityPlot = false;
            _lobeFillPlot = false;
            _positiveColorFillPlot = false;
            _negativeColorFillPlot = false;
        }
        else if (_lobeFillPlot) {
            _densityPlot = false;
            _interpolatedDensityPlot = false;
            _positiveFillPlot = false;
            _negativeFillPlot = false;
            _positiveColorFillPlot = false;
            _negativeColorFillPlot = false;
        }
        else if (_positiveColorFillPlot || _negativeColorFillPlot) {
            _densityPlot = false;
            _interpolatedDensityPlot = false;
            _lobeFillPlot = false;
            _positiveFillPlot = false;
            _negativeFillPlot = false;
        }
        else if (_positiveFillPlot || _negativeFillPlot) {
            _lobeFillPlot = false;
            _positiveColorFillPlot = false;
            _negativeColorFillPlot = false;
        }
    }

    /**
     * Rasterizes a seismic trace, no highlight. <br>
     * Please refer to JSeismic documentation for more detail.
     * @param prevTrace the trace previous to this trace, used for interpolation.
     * @param thisTrace the trace that is being rasterized.
     * @param sampleRange the sample range thiat is being rasterized.
     * @param lastTraceLoc the horizontal location (in pixels) of the previous trace.
     * @param thisTraceLoc the horizontal location (in pixels) of this trace.
     */
    public void rasterizeTrace(TraceData prevTrace, TraceData thisTrace,
                                NumberRange sampleRange,
                                int lastTraceLoc, int thisTraceLoc) {
        rasterizeTrace(prevTrace, thisTrace, sampleRange,
                        lastTraceLoc, thisTraceLoc, false);
    }

    /**
     * Rasterizes a seismic trace. <br>
     * Please refer to JSeismic documentation for more detail.
     * @param prevTrace the trace previous to this trace, used for interpolation.
     * @param thisTrace the trace that is being rasterized.
     * @param sampleRange the sample range thiat is being rasterized.
     * @param lastTraceLoc the horizontal location (in pixels) of the previous trace.
     * @param thisTraceLoc the horizontal location (in pixels) of this trace.
     * @param isHighlighted flag indicates if the trace should be highlighted.
     */
    public void rasterizeTrace(TraceData prevTrace, TraceData thisTrace,
                                NumberRange sampleRange, int lastTraceLoc,
                                int thisTraceLoc, boolean isHighlighted) {
        int i, j, k;
        boolean plotType1, plotType2;
        byte pixelType1, pixelType2;

        double theColorMapStartValue = this.getColorMapStartValue();
        double theColorMapEndValue = this.getColorMapEndValue();

        if (_colorMap == null) return;
        if ((lastTraceLoc==Integer.MIN_VALUE) || (lastTraceLoc==Integer.MAX_VALUE)) {
            lastTraceLoc = thisTraceLoc - (int)_deltaPixels;
        }

        int deltaPixels = thisTraceLoc - lastTraceLoc;
        float[] thisData = thisTrace.getSamples();
/*
        int thisTraceId = ((Integer) thisTrace.getKey()).intValue();
        if (thisTraceId  == 0)
            System.out.println("MRasterizerRRRRRRRRRR " + thisTraceId);
        else if (thisTraceId % 10  == 0)
            System.out.println("                      " + thisTraceId);
*/
        // do something of the sampleRange according to _startDepth and _endDepth
        int indexSD = findIndexForDepth(_startDepth, thisTrace.getNumAppliedSamples());
        int indexED = findIndexForDepth(_endDepth, thisTrace.getNumAppliedSamples());
        int sampleRangeStart = Math.min(indexSD, indexED) + sampleRange.getMin().intValue();
        int sampleRangeEnd = Math.min(Math.min(indexSD, indexED)+sampleRange.getMax().intValue(), indexED);
        //int sampleRangeStart = Math.max(Math.min(indexSD, indexED), sampleRange.getMin());
        //int sampleRangeEnd = Math.min(Math.max(indexSD, indexED), sampleRange.getMax());
        //if (thisTraceId == 0) System.out.println(thisTraceId + " : " + thisData.length + " RRRRRRR " + sampleRange.getMin() + " " + sampleRange.getMax() + " : " + indexSD + " " + indexED);
        //sampleRange.setRange(sampleRangeStart, sampleRangeEnd);
        NumberRange mySampleRange = new NumberRange(sampleRangeStart, sampleRangeEnd);

        if (mySampleRange.getRange() < _imageHeight)
            _imageHeight = mySampleRange.getRange();

        int imageStartV = mySampleRange.getMin().intValue();
        int imageEndV = imageStartV + _imageHeight;

        //if (thisTraceId == 0) System.out.println(thisTraceId + " : " + thisData.length + " RRRRRRR " + imageStartV + " " + imageEndV);
        if (_densityPlot) {
            int lastDensityLoc = lastTraceLoc + (int)(0.5*_deltaPixels);
            int thisDensityLoc = thisTraceLoc + (int)(0.5*_deltaPixels);
            if (lastDensityLoc < 0) lastDensityLoc = 0;
            if (thisDensityLoc > _imageWidth) thisDensityLoc = _imageWidth;
            byte pixel = 0;
            int sampleValue = 0;
            int bitmapLineIndex = 0;
            //double varC = _numColors/2.0; // interpolation make the value -1~1
            double varC = _numColors / (theColorMapEndValue - theColorMapStartValue);
            for (i=imageStartV; i<imageEndV; i++) {
                if ((thisData[i] < BhpModelTraceNormalization.NULL_LIMIT) &&
                    (thisData[i] > BhpModelTraceNormalization.NULL_LIMIT2)) {
                    bitmapLineIndex += _scanLineSize;
                    continue;
                }

                //sampleValue = (int)((thisData[i]+1) * varC);
                sampleValue = (int)((thisData[i]-theColorMapStartValue) * varC);
                if (_reverseFill) sampleValue = _numColors - sampleValue -1;
                if (sampleValue < 0)           sampleValue = 0;
                if (sampleValue >= _numColors) sampleValue = _numColors -1;
                pixel = (byte)sampleValue;
                for (j=lastDensityLoc; j<thisDensityLoc; j++) {
                    if (_imageData[bitmapLineIndex+j] == _backgroundPixel)
                        _imageData[bitmapLineIndex+j] = pixel;
                }
                bitmapLineIndex += _scanLineSize;
            }
        }
        else if (_interpolatedDensityPlot) {
            int lastDensityLoc = lastTraceLoc;
            int thisDensityLoc = thisTraceLoc;
            int startOffset = 0;
            if (lastDensityLoc < 0) {
                startOffset = -lastDensityLoc;
                lastDensityLoc = 0;
            }
            if (thisDensityLoc > _imageWidth) thisDensityLoc = _imageWidth;

            float[] prevData;
            if (prevTrace == null) prevData = thisData;
            else prevData = prevTrace.getSamples();
            double[] varA = new double[deltaPixels];
            double[] varB = new double[deltaPixels];
            //double   varC = _numColors / 2.;
            double varC = _numColors / (theColorMapEndValue - theColorMapStartValue);
            double   b;
            for (i=0; i<deltaPixels; i++) {
                b = ((double)(i + startOffset)) / (deltaPixels - 1);
                varA[i] = (1. - b);
                varB[i] = b;
            }
            double prevSampleValue, thisSampleValue;
            byte pixel = 0;
            int sampleValue = 0;
            int bitmapLineIndex = 0;
            for (i=imageStartV; i<imageEndV; i++) {
                if (((prevData[i] < BhpModelTraceNormalization.NULL_LIMIT) &&
                     (prevData[i] > BhpModelTraceNormalization.NULL_LIMIT2)) ||
                    ((thisData[i] < BhpModelTraceNormalization.NULL_LIMIT) &&
                     (thisData[i] > BhpModelTraceNormalization.NULL_LIMIT2))) {
                    bitmapLineIndex += _scanLineSize;
                    continue;
                }

                prevSampleValue = (prevData[i] - theColorMapStartValue) * varC;
                thisSampleValue = (thisData[i] - theColorMapStartValue) * varC;
                for (j=lastDensityLoc, k=0; j<thisDensityLoc; j++, k++) {
                    sampleValue = (int)(varA[k] * prevSampleValue + varB[k] * thisSampleValue);
                    if (_reverseFill) sampleValue = _numColors - sampleValue - 1;
                    if (sampleValue < 0) sampleValue = 0;
                    else if (sampleValue >= _numColors) sampleValue = _numColors-1;
                    pixel = (byte)sampleValue;
                    if (_imageData[bitmapLineIndex+j] == _backgroundPixel)
                        _imageData[bitmapLineIndex+j] = pixel;
                }
                bitmapLineIndex += _scanLineSize;
            }
        }
/*
        else if (_lobeFillPlot) {
        }
*/
        else if (_positiveColorFillPlot || _negativeColorFillPlot) {
            //double varC = _numColors / 2.;
            double varC = _numColors / (theColorMapEndValue - theColorMapStartValue);
            int clipLeft = thisTraceLoc - (int)(_clippingValue*_deltaPixels+0.5);
            int clipRight = thisTraceLoc + (int)(_clippingValue*_deltaPixels+0.5);
            if (clipLeft < 0) clipLeft = 0;
            if (clipRight > _imageWidth) clipRight = _imageWidth;
            int wiggleLeft = 0;
            int wiggleRight = 0;
            int fillLeft, fillRight;
            int fillColor = -1;
            int bitmapLineIndex = 0;
            double scaler = _deltaPixels;
            int thisValue, nextValue, tempValue;
            double amp = thisData[0] * scaler;
            thisValue = mathFloor(amp);
            nextValue = thisValue;

            for (i=imageStartV; i<imageEndV; i++) {
                if ((i+1) < thisTrace.getNumAppliedSamples()) {
                    amp = thisData[i+1] * scaler;
                    nextValue = (int) Math.floor(amp);
                }
                if (_wigglePlot) {
                    if (thisValue < nextValue) {
                        wiggleLeft = thisTraceLoc + thisValue;
                        wiggleRight = thisTraceLoc + nextValue;
                    }
                    else {
                        wiggleLeft = thisTraceLoc + nextValue;
                        wiggleRight = thisTraceLoc + thisValue;
                    }
                    if (wiggleLeft < clipLeft) {
                        wiggleLeft = clipLeft;
                        if (wiggleRight < clipLeft) wiggleRight = clipLeft - 1;
                    }
                    if (wiggleRight >= clipRight) {
                        wiggleRight = clipRight - 1;
                        if (wiggleLeft >= clipRight) wiggleLeft = clipRight;
                    }

                    if (isHighlighted)
                        for (j=wiggleLeft; j<=wiggleRight; j++)
                            _imageData[bitmapLineIndex+j] = _highlightPixel;
                    else
                        for (j=wiggleLeft; j<=wiggleRight; j++)
                            _imageData[bitmapLineIndex+j] = _foregroundPixel;
                }

                fillColor = (int)((thisData[i]-theColorMapStartValue) * varC);
                if (_reverseFill) {
                    fillColor = _numColors - fillColor - 1;
                    plotType1 = _positiveColorFillPlot;
                    plotType2 = _negativeColorFillPlot;
                }
                else {
                    plotType1 = _negativeColorFillPlot;
                    plotType2 = _positiveColorFillPlot;
                }
                if (fillColor < 0) fillColor = 0;
                else if (fillColor >= _numColors) fillColor = _numColors - 1;
                if (thisValue < 0 && plotType1) {
                    if (_wigglePlot) fillLeft = wiggleRight + 1;
                    else             fillLeft = thisTraceLoc + thisValue;
                    fillRight = thisTraceLoc;
                    if (fillLeft < clipLeft)    fillLeft = clipLeft;
                    if (fillRight >= clipRight) fillRight = clipRight - 1;
                    for (j=fillLeft; j<=fillRight; j++)
                        _imageData[bitmapLineIndex+j] = (byte)fillColor;
                }
                else if (thisValue > 0 && plotType2){
                    fillLeft = thisTraceLoc;
                    if (_wigglePlot) fillRight = wiggleLeft - 1;
                    else             fillRight = thisTraceLoc + thisValue;
                    if (fillLeft < clipLeft)    fillLeft = clipLeft;
                    if (fillRight >= clipRight) fillRight = clipRight - 1;
                    for (j=fillLeft; j<=fillRight; j++) {
                        if (_imageData[bitmapLineIndex+j] == _backgroundPixel)
                            _imageData[bitmapLineIndex+j] = (byte) fillColor;
                    }
                }
                tempValue = nextValue;
                if ((i+2) < thisTrace.getNumAppliedSamples()) {
                    amp = thisData[i+2] * scaler;
                    tempValue = mathFloor(amp);
                }
                if (tempValue > nextValue) thisValue = nextValue + 1;
                else if (tempValue < nextValue) thisValue = nextValue - 1;
                else thisValue = nextValue;
                bitmapLineIndex += _scanLineSize;
            }
            return;
        }

        // wiggle plot, can be combined with negative and positive monochrome filling
        int clipLeft  = thisTraceLoc - (int)(_clippingValue * _deltaPixels + 0.5);
        int clipRight = thisTraceLoc + (int)(_clippingValue * _deltaPixels + 0.5);
        if (clipLeft < 0) clipLeft = 0;
        if (clipRight > _imageWidth ) clipRight = _imageWidth;
        int wiggleLeft, wiggleRight;
        int fillLeft, fillRight;
        int bitmapLineIndex = 0;
        double scaler = _deltaPixels;
        int thisValue, nextValue, tempValue;
        //double amp = thisData[0] * scaler;
        double amp = thisData[imageStartV] * scaler;
        thisValue = mathFloor(amp);
        nextValue = thisValue;

        if (_wigglePlot) {
            for (i=imageStartV; i<imageEndV; i++) {
                if ((i+1) < thisTrace.getNumAppliedSamples()) {
                    amp = thisData[i+1] * scaler;
                    nextValue = mathFloor(amp);
                }
                if (thisValue < nextValue) {
                    wiggleLeft = thisTraceLoc + thisValue;
                    wiggleRight = thisTraceLoc + nextValue;
                }
                else {
                    wiggleLeft = thisTraceLoc + nextValue;
                    wiggleRight = thisTraceLoc + thisValue;
                }
                if (wiggleLeft < clipLeft) {
                    wiggleLeft = clipLeft;
                    if (wiggleRight < clipLeft)
                        wiggleRight = clipLeft - 1;
                }
                if (wiggleRight >= clipRight) {
                    wiggleRight = clipRight - 1;
                    if (wiggleLeft >= clipRight)
                        wiggleLeft = clipRight;
                }
/*
                if (Math.abs(wiggleRight-wiggleLeft) > 2) {
                    System.out.println(thisTraceId + " : " + i + "BBBBB " + wiggleRight + " - " + wiggleLeft);
                }
*/
                if (isHighlighted) {
                    for (j=wiggleLeft; j<=wiggleRight; j++)
                        _imageData[bitmapLineIndex + j] = _highlightPixel;
                }
                else {
                    for (j=wiggleLeft; j<=wiggleRight; j++)
                        _imageData[bitmapLineIndex + j] = _foregroundPixel;
                }

                if (_positiveFillPlot || _negativeFillPlot) {
                    if (_reverseFill) {
                        plotType1 = _positiveFillPlot;
                        plotType2 = _negativeFillPlot;
                        pixelType1 = _positiveFillPixel;
                        pixelType2 = _negativeFillPixel;
                    }
                    else {
                        plotType1 = _negativeFillPlot;
                        plotType2 = _positiveFillPlot;
                        pixelType1 = _negativeFillPixel;
                        pixelType2 = _positiveFillPixel;
                    }
                    if (thisValue<0 && plotType1) {
                        fillLeft = wiggleRight + 1;
                        fillRight = thisTraceLoc;
                        if (fillLeft < clipLeft) fillLeft = clipLeft;
                        if (fillRight >= clipRight) fillRight = clipRight - 1;
                        if (fillLeft != fillRight) {
                            if (isHighlighted) {
                                for (j=fillLeft; j<=fillRight; j++)
                                    _imageData[bitmapLineIndex+j] = _highlightPixel;
                            }
                            else {
                                for (j=fillLeft; j<=fillRight; j++)
                                    _imageData[bitmapLineIndex+j] = pixelType1;
                            }
                        }
                    }
                    else if (thisValue>0 && plotType2) {
                        fillLeft = thisTraceLoc;
                        fillRight = wiggleLeft - 1;
                        if (fillLeft < clipLeft) fillLeft = clipLeft;
                        if (fillRight >= clipRight) fillRight = clipRight-1;
                        if (fillLeft != fillRight) {
                            if (isHighlighted) {
                                for (j=fillLeft; j<=fillRight; j++)
                                    if (_imageData[bitmapLineIndex+j] != _foregroundPixel)
                                        _imageData[bitmapLineIndex+j] = _highlightPixel;
                            }
                            else {
                                for (j=fillLeft; j<=fillRight; j++)
                                    if (_imageData[bitmapLineIndex+j] != _foregroundPixel)
                                        _imageData[bitmapLineIndex+j] = pixelType2;
                            }
                        }
                    }
                }

                tempValue = nextValue;
                if ((i+2) < thisTrace.getNumAppliedSamples()) {
                    amp = thisData[i+2] * scaler;
                    tempValue = mathFloor(amp);
                }
                if (tempValue > nextValue) thisValue = nextValue + 1;
                else if (tempValue < nextValue) thisValue = nextValue - 1;
                else thisValue = nextValue;
                bitmapLineIndex += _scanLineSize;
            }
        }
        else if (_positiveFillPlot || _negativeFillPlot) {
            for (i=imageStartV; i<imageEndV; i++) {
                amp = thisData[i] * scaler;
                thisValue = mathFloor(amp);
                if (_reverseFill) {
                    plotType1 = _positiveFillPlot;
                    plotType2 = _negativeFillPlot;
                    pixelType1 = _positiveFillPixel;
                    pixelType2 = _negativeFillPixel;
                }
                else {
                    plotType1 = _negativeFillPlot;
                    plotType2 = _positiveFillPlot;
                    pixelType1 = _negativeFillPixel;
                    pixelType2 = _positiveFillPixel;
                }
                if (thisValue < 0 && plotType1) {
                    fillLeft = thisTraceLoc + thisValue;
                    fillRight = thisTraceLoc;
                    if (fillLeft < clipLeft) fillLeft = clipLeft;
                    if (fillRight >= clipRight) fillRight = clipRight - 1;

                    if (fillLeft != fillRight) {
                        if (isHighlighted) {
                            for (j=fillLeft; j<=fillRight; j++)
                                _imageData[bitmapLineIndex + j] = _highlightPixel;
                        }
                        else {
                            for (j=fillLeft; j<=fillRight; j++)
                                _imageData[bitmapLineIndex + j] = pixelType1;
                        }
                    }
                }
                else if (thisValue >= 0 && plotType2) {
                    fillLeft = thisTraceLoc;
                    fillRight = thisTraceLoc + thisValue;
                    if (fillLeft < clipLeft) fillLeft = clipLeft;
                    if (fillRight >= clipRight) fillRight = clipRight - 1;
                    if (fillLeft != fillRight) {
                        if (isHighlighted) {
                            for(j=fillLeft; j<=fillRight; j++)
                                if (_imageData[bitmapLineIndex+j] != _foregroundPixel)
                                    _imageData[bitmapLineIndex+j] = _highlightPixel;
                        }
                        else {
                            for (j=fillLeft; j<=fillRight; j++)
                                if (_imageData[bitmapLineIndex + j] != _foregroundPixel)
                                    _imageData[bitmapLineIndex + j] = pixelType2;
                        }
                    }
                }
                bitmapLineIndex += _scanLineSize;
            }
        }
    }

    /**
     * @param amp
     * @return
     */
    private int mathFloor(double amp) {
        //FIXME: check this with a profiler... is it worth the custom stupidness?
        int thisValue;
        if (amp > 0) thisValue = (int)(amp + 0.5);
        else if (amp < 0) thisValue = (int) (amp - 0.5);
        else thisValue = (int) amp;
        return thisValue;
    }

    protected void updateColorMap() {
        _numColors = _colorMap.getDensityColorSize();
        _foregroundPixel = _colorMap.getForegroundIndex();
        _backgroundPixel = _colorMap.getBackgroundIndex();
        _positiveFillPixel = _colorMap.getPositiveFillIndex();
        _negativeFillPixel = _colorMap.getNegativeFillIndex();
        _highlightPixel = _colorMap.getHighlightIndex();
    }
    private int findIndexForDepth(float depth, int length) {
        float depthMin = _reader.getMinDepth();
        float depthMax = _reader.getMaxDepth();

        depthMin = Math.min(depthMin, _startDepth);
        depthMax = Math.max(depthMax, _endDepth);

        double depthRange = depthMax - depthMin;
        int re = (int) Math.ceil(length*(depth-depthMin) / depthRange);
        if (re < 0) re = 0;
        if (re > length-1) re = length - 1;
        return re;
    }

    public Dimension getRasterSize(){
    	return new Dimension(_imageWidth, _imageHeight);
    }
}
