/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bhpBilliton.viewer2d.ui.BhpBusyDialog;
import com.bhpBilliton.viewer2d.ui.util.ElementAttributeReader;
import com.gwsys.gw2d.model.CommonDataModel;
import com.gwsys.gw2d.model.CommonShapeLayer;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.gw2d.shape.Polyline2D;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;
import com.gwsys.seismic.core.DataChooser;
import com.gwsys.seismic.core.DefaultWorkflow;
import com.gwsys.seismic.core.SeismicWorkflow;
import com.gwsys.seismic.core.TraceImage;
import com.gwsys.seismic.core.TraceInterpolator;
import com.gwsys.seismic.core.TraceNormalizer;
import com.gwsys.seismic.core.TraceRasterizer;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.SeismicColorMap;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class is used to display data in the map viewer.
 *               Here, data is displayed in a two-dimensional way.
 *               The unit is traces along both axes. The map layer displays
 *               only one slice of the data. In the case of seismic data,
 *               it may display the data at a specif time or depth. For model
 *               data, it may display a specific layer value or boundary.
 *               For event data, it may display a single event. <br>
 *               User can use mouse to draw arbitray polyline on the map
 *               layer. These polylines can be used for data synchronization.
 *               <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpMapLayer extends BhpLayer {
    /**
     *
     */
    private static final long serialVersionUID = 3905800885778329911L;
    public static final int MAP_X_START_FLAG     = 0x0000002;
    public static final int MAP_X_END_FLAG       = 0x0000004;
    public static final int MAP_Y_START_FLAG     = 0x0000008;
    public static final int MAP_Y_END_FLAG       = 0x0000010;
    public static final int MAP_X_DIR_FLAG       = 0x0000020;
    public static final int MAP_Y_DIR_FLAG       = 0x0000040;
    public static final int MAP_TRANSPOSE        = 0x0000080;
    public static final int MAP_ASPECTRATIO_FLAG = 0x0000100;

    private int _mapSynFlag;
    private boolean _modelLayerSync;
    private BhpMapRasterizer _rasterizer;

    private Node _polylineNode;
    private Boolean _transposeXML;
    private Boolean _reverseHXML;

    private BhpViewerBase viewer;

    /**
     * Creates a model layer with a reference layer. <br>
     * This construtor is usually used when a new layer is created
     * by the users with GUI control.
     * @param viewer the parent viewer of the new layer.
     * @param win the parent window of the new layer.
     * @param id the identification number of the new layer.
     * @param dstype the name of the data source.
     * @param dname name of the selected dataset.
     * @param name name of the layer.
     * @param pathlist path of the selected dataset. <br>
     *        For bhpio data, it is the pathlist. For OpenSpirit data,
     *        it is the session name.
     * @param property the name of the selected property. <br>
     * @param propertySync the boolean flag for property synchronization.
     * @param layerSync the boolean flag for model layer number synchronization.
     * @param parameter a table model stores the user data selection.
     *        In the case of bhpio, it can be used to construct the
     *        query string used to run bhpio.
     * @param ref the reference layer. <br>
     *        The new layer will inherit from this reference layer things
     *        like scales, pipeline settings, display options, and etc.
     * @param type data type. <br>
     *        It can be BHP_DATA_TYPE_SEISMIC, BHP_DATA_TYPE_MODEL,
     *        BHP_DATA_TYPE_EVENT, and BHP_DATA_TYPE_UNKNOWN.
     */
    public BhpMapLayer(BhpViewerBase viewer, BhpWindow win, int id,
                         String dstype, String dname, String name, String pathlist,
                         String property, boolean propertySync, boolean layerSync,
                         BhpSeismicTableModel parameter, BhpLayer ref, int type) {
        super(viewer, win, id, dstype, dname, name, pathlist,
              parameter, property, propertySync, ref, type);
        this.viewer = viewer;
        _modelLayerSync = layerSync;
        _mapSynFlag = 0;
        _polylineNode = null;
        _transposeXML = null;
        _reverseHXML = null;
        
        if (_sampleLayer != null) {
            if (_sampleLayer instanceof BhpMapLayer) {
                BhpMapLayer mslayer = (BhpMapLayer) _sampleLayer;
                _mapSynFlag = mslayer.getMapSynFlag();
            }
        }
        //_modelUnitPerTrace = _defaultModelUnitPerTrace;
        //_modelUnitPerSample = _defaultModelUnitPerSample;   // it is model unit per foot now
    }

    /**
     * Creates a new model layer with specified attributes. <br>
     * This construtor is usually used when a new layer is created
     * parsing saved XML configuration file.
     * @param viewer the parent viewer of the new layer.
     * @param win the parent window of the new layer.
     * @param id the identification number of the new layer.
     * @param dstype the name of the data source.
     * @param dname name of the selected dataset.
     * @param name name of the new layer.
     * @param pathlist path of the selected dataset. <br>
     *        For bhpio data, it is the pathlist. For OpenSpirit data,
     *        it is the session name.
     * @param property the name of the selected property or event.
     * @param parameter a table model stores the user data selection.
     *        In the case of bhpio, it can be used to construct the
     *        query string used to run bhpio.
     * @param mupt horizontal scale with unit of model-unit-per-trace.
     * @param mups vertical scale with unit model-unit-per-trace.
     * @param talk a boolean indicates if the new layer should
     *        broadcase its change event.
     * @param syn the synchronization flag of the new layer.
     * @param trans transparency(opacity) of the new layer.
     * @param layerSync the boolean flag for model layer number synchronization.
     * @param pipelineNode the pipeline node in the DOM tree.
     *        It is used to initialize the pipeline of the new layer.
     * @param rp a boolean for polarity.
     * @param ro a boolean for sample order, reversed or not.
     * @param polylineNode the node in the DOM tree with all the user
     *        created polylines of the map layer.
     * @param transpose a boolean for image transpose.
     */
    public BhpMapLayer(BhpViewerBase viewer, BhpWindow win, int id, String dstype,
                         String dname, String name, String pathlist,
                         String property, BhpSeismicTableModel parameter,
                         double mupt, double mups, boolean talk, int syn,
                         int trans, boolean propertySyn, boolean layerSync,
                         Node pipelineNode, int type, int mapSyn, boolean lockAspectRatio,
                         boolean rp, boolean ro, Node polylineNode,
                         boolean transpose, boolean reverseh) {
        super(viewer, win, id, dstype, dname, name, pathlist, parameter,
              property, talk, syn, trans, propertySyn, pipelineNode, type, rp, ro);
        _hscaleFactor = mupt;
        _vscaleFactor = mups;
        _modelLayerSync = layerSync;
        _mapSynFlag = mapSyn;
        _polylineNode = polylineNode;
        _transposeXML = new Boolean(transpose);
        _reverseHXML =  new Boolean(reverseh);
    }

    /**
     * Converts the model layer object into an XML string. <br>
     * This is used when save the working session into an XML file.
     * Please refer to BhpViewer.dtd for the detail of the format.
     * @return an XML string for the object.
     */
    public String toXMLString() {
        StringBuffer content = new StringBuffer();
        content.append("        " + "<BhpMapLayer ");
        content.append("dataType=\"" + _dataType + "\" ");
        content.append("modelLayerSync=\"" + _modelLayerSync + "\" ");
        content.append("mapSynFlag=\"" + _mapSynFlag + "\" ");
        content.append("reversedHDirection=\"" + this.isReversedHorizontalDirection() + "\" ");
        content.append("transposeImage=\"" + this.isTransposeImage() + "\">\n");
        content.append(super.toXMLString());

        content.append("            " + "<BhpMapPolylines>\n");
        Iterator iter = _shapeListLayer.getShapes();
        Polyline2D pl = null;
        Object iterObj = null;
        int plSize = 0;
        StringBuffer plPoints = new StringBuffer();
        while(iter.hasNext()) {
            iterObj = iter.next();
            if (iterObj instanceof Polyline2D) {
                plPoints.delete(0, plPoints.length());
                pl = (Polyline2D) iterObj;
                plSize = pl.getSize();
                for (int i=0; i<plSize; i++) {
                    plPoints.append(pl.getXAt(i));
                    plPoints.append(",");
                    plPoints.append(pl.getYAt(i));
                    plPoints.append(",");
                }
                content.append("                " + "<Polyline points=\"" +
                                plPoints.toString() + "\"/>\n");
            }
        }
        content.append("            " + "</BhpMapPolylines>\n");

        content.append("        " + "</BhpMapLayer>\n");
        return content.toString();
    }

    /**
     * Retrieves the synchronization flag for map display settings.
     */
    public int getMapSynFlag()       { return _mapSynFlag; }
    /**
     * Retrieves the flag for model layer synchronization.
     */
    public boolean getModelLayerSync()       { return _modelLayerSync; }
    /**
     * Sets the synchronization flag for map display settings.
     */
    public void setMapSynFlag(int f) { _mapSynFlag = f; }
    /**
     * Sets the flag for model layer synchronization.
     */
    public void setModelLayerSync(boolean m) { _modelLayerSync = m; }
    /**
     * Sets the horizontal order, reversed or not.
     */
    public void setReversedHorizontalDirection(boolean re) {
        if (re) {
            _image.setDirection(TraceImage.DRAW_TRACE_RIGHT_TO_LEFT);
        } else {
            _image.setDirection(TraceImage.DRAW_TRACE_LEFT_TO_RIGHT);
        }
    }
    /**
     * Finds out if the horizontal order is reversed.
     */
    public boolean isReversedHorizontalDirection() {
        boolean re = false;
        if (_image.getDirection() == TraceImage.DRAW_TRACE_RIGHT_TO_LEFT) {
            re = true;
        }
        return re;
    }


    /**
     * Sets the image transpose flag.
     */
    public void setTransposeImage(boolean t) {
        ((BhpMapPainter)_image).setTranspose(t);
    }
    /**
     * Finds out if the map image is transposed.
     */
    public boolean isTransposeImage() {
        return ((BhpMapPainter)_image).getTranspose();
    }

    public double getLayerHorizontalScale() {
        // modelUnitPerTrace
        double re = _window.getBhpPlot().getHorizontalScale() * _hscaleFactor;
        return re;
    }
    public double getLayerVerticalScale() {
        // modelUnitPerSample
        double re = _window.getBhpPlot().getVerticalScale() * _vscaleFactor;
        return re;
    }
    /**
     * Finds out the read data value at the specific point. <br>
     * This default implementation simply returns 0.
     * Concrete class extends BhpLayer should have meaningful implemenation.
     * @param traceid trace number, the horizontal position.
     * @param vreading vertical axis reading
     * @return data value of the given trace at the specific vertical position.
     */
    public double findSpecificDataValue(int traceid, double vreading) {
        double re = 0;
        if (_reader != null) {
            TraceData traceData = new TraceData();
            boolean presult = _reader.process(traceid, null, traceData);
            if (presult) {
                float[] dataArray = traceData.getSamples();
                double startValue = _reader.getMetaData().getStartValue();
                double sampleRate = _reader.getMetaData().getSampleRate();
                int index = (int)Math.round((vreading - startValue) / sampleRate);
                if (getReversedSampleOrder()) {
                    index = dataArray.length - 1 - index;
                }
                //System.out.println("BhpSeismicLayer.findSpecificDataValue : " + vreading + "->" + index);
                if (index>-1 && index<dataArray.length) {
                    re = dataArray[index];
                }
            }
        }
        return re;
    }

    /**
     * This method calculates the device position according
     * to the trace id and the virtual vertical value.
     * @param tid trace id.
     * @param vvalue virtual vertical reading.
     * @return a Point in the device space.
     */
    public Point reverseSelectByPoint(int tid, double vvalue) {
        int resultx = 0;
        int resulty = 0;

        DataChooser dataSelector = _pipeline.getDataLoader().getDataSelector();
        if (tid == -999) {
            tid = 0;
        }
        else if (tid > dataSelector.getVirtualModelLimits().getMaxX()) {
            tid = (int) (dataSelector.getVirtualModelLimits().getMaxX());
        }

        if (this.isReversedHorizontalDirection()) {
            tid = (int)(dataSelector.getVirtualModelLimits().getMaxX()) - 1 - tid;
        }
        if (this.getReversedSampleOrder()) {
            vvalue = _pipeline.getDataLoader().getDataReader().getMetaData().getSamplesPerTrace() - vvalue;
        }

        double interx = (tid+0.5) * getLayerHorizontalScale();
        double intery = (vvalue+0.5) * getLayerVerticalScale();
        java.awt.geom.Point2D originalPos = new java.awt.geom.Point2D.Double(interx, intery);
        if (this.isTransposeImage()) {
            originalPos = new java.awt.geom.Point2D.Double(intery, interx);
        }
        java.awt.geom.Point2D resultPos = _transformation.transform(originalPos,
                                                    new Point(0, 0));

        resultx = (int) resultPos.getX();
        resulty = (int) resultPos.getY();
        return new Point(resultx, resulty);
    }

    // since map layer is different from other xv layers and it can
    // be transposed, axis flipped, it needs to overwrite this method
    // in BhpLayer to provide customized behavior
    /**
     * This method calculates the virtual position according to
     * the device position. <br>
     * This method overrides the implementation in its super
     * class since map layer can have flipped axis, and
     * transposed image
     * @return a Point where x is the virtual trace id,
     * and y is the virtual vertical position
     */
    public Point selectByPoint(Point deviceP) {
        int virtTraceId = -999;
        double scalex = getLayerHorizontalScale();
        double scaley = getLayerVerticalScale();
        Bound2D box = _transformation.inverseTransform(
                    new Bound2D(0, 0, deviceP.x, deviceP.y));
        int finalx = (int) (Math.floor(box.getMaxX() / scalex));
        int finaly = (int) (Math.floor(box.getMaxY() / scaley));
        if (this.isTransposeImage()) {
            finalx = (int) (Math.floor(box.getMaxY() / scalex));
            finaly = (int) (Math.floor(box.getMaxX() / scaley));
        }
        DataChooser dataSelector = _pipeline.getDataLoader().getDataSelector();
        if (finalx >= dataSelector.getVirtualModelLimits().getMaxX()) {
            virtTraceId = (int)(dataSelector.getVirtualModelLimits().getMaxX()) - 1;
        }
        else {
            virtTraceId = finalx;
        }
        if (this.isReversedHorizontalDirection()) {
            virtTraceId = (int)(dataSelector.getVirtualModelLimits().getMaxX()) - 1 - virtTraceId;
        }
        if (this.getReversedSampleOrder()) {
            finaly = _pipeline.getDataLoader().getDataReader().getMetaData().getSamplesPerTrace() - finaly;
        }
        finaly = finaly + (int)(_pipeline.getDataLoader().getDataSelector().getStartSampleValue());

        return (new Point(virtTraceId, finaly));
    }
    
    public void updateLayer(boolean redraw) {
        updatePolylines();
        double modelUnitPerTrace = getLayerHorizontalScale();
        double modelUnitPerSample = getLayerVerticalScale();
        boolean oldNotify = _image.isNotificationEnabled();
        _image.setNotification(false);
        _image.setScale(modelUnitPerTrace, modelUnitPerSample);
        ((BhpMapPainter)_image).updateMapImageSize();
        _image.setNotification(oldNotify);

        double modelWidth = _reader.getMetaData().getNumberOfTraces() * modelUnitPerTrace;
        double modelHeigth = _reader.getMetaData().getSamplesPerTrace() * modelUnitPerSample;
        if (((BhpMapPainter)_image).getTranspose()) {
            double swapTemp = modelWidth;
            modelWidth = modelHeigth;
            modelHeigth = swapTemp;
        }
        _modelBox = new Bound2D(0, 0, modelWidth, modelHeigth);
        _deviceBox = new Bound2D(0, 0, modelWidth, modelHeigth);
        _containerModel.setBoundingBox(_modelBox);
        _shapeListLayer.setBoundingBox(_modelBox);
        if (redraw) {
            this.setTransformation(this.getTransformation());

            AbstractPlot ppp = _window.getBhpPlot();
            if (ppp.getAxisAssociateLayer() == this.getIdNumber()) {
                ppp.updateVAxes();
                ppp.updateHAxes();
                ppp.updateMiscAnnotation();
            }
        }
    }

    public void updateSeismicDisplay() {
        updatePolylines();
        BhpListenerDataSelector oldSelector = (BhpListenerDataSelector) _pipeline
                .getDataLoader().getDataSelector();

        BhpListenerDataSelector newSelector = new BhpListenerDataSelector();
        _pipeline.getDataLoader().setDataReader(_reader);
        _pipeline.setDataSelector(newSelector);
        if (oldSelector.getStartSampleChanged())
            newSelector.setStartSampleValue(oldSelector.getStartSampleValue());

        if (oldSelector.getEndSampleChanged())
            newSelector.setEndSampleValue(oldSelector.getEndSampleValue());

        newSelector.setPrimaryKeyStep(oldSelector.getPrimaryKeyStep());


        _shapeListLayer.removeShape(_image);
        int oldImageDirection = _image.getDirection();
        double modelUnitPerTrace = getLayerHorizontalScale();
        double modelUnitPerSample = getLayerVerticalScale();
        boolean oldImageTranspose = ((BhpMapPainter)_image).getTranspose();
        _image = new BhpMapPainter(0, 0, modelUnitPerTrace, modelUnitPerSample, _pipeline);
        _image.setDirection(oldImageDirection);
        ((BhpMapPainter)_image).setTranspose(oldImageTranspose);
        ((BhpMapPainter)_image).updateMapImageSize();
        _image.setAttribute(new RenderingAttribute(_containerModel));
        _shapeListLayer.addShapeAtIndex(_image, 0);

        double modelWidth = _reader.getMetaData().getNumberOfTraces() * modelUnitPerTrace;
        double modelHeight = _reader.getMetaData().getSamplesPerTrace() * modelUnitPerSample;
        if (((BhpMapPainter)_image).getTranspose()) {
            double swapTemp = modelWidth;
            modelWidth = modelHeight;
            modelHeight = swapTemp;
        }

        _modelBox = new Bound2D(0, 0, modelWidth, modelHeight);
        _containerModel.setBoundingBox(_modelBox);
        _shapeListLayer.setBoundingBox(_modelBox);

        AbstractPlot ppp = _window.getBhpPlot();
        if (ppp.getAxisAssociateLayer() == this.getIdNumber()) {
            ppp.updateHAxes();
            ppp.updateVAxes();
            String title = "title";
            if (getDataType() == BhpViewer.BHP_DATA_TYPE_SEISMIC) {
                String value = getParameter().getValueAt(0, 2).toString();
                title = "Time/Depth: " + value;
            }
            else {
                title = getPropertyName();
            }
            ppp.setTitle(title);
            ppp.updateMiscAnnotation();
        }
        this.setTransformationWithoutRepaint(this.getTransformation());
        _viewer.changeLoadingLayerNumber(BhpBusyDialog.RMV_LAYER);
    }

    public double getTimeRangeStart() { return 0; }
    public double getTimeRangeEnd()   { return 0; }
    public void setTimeRange(double start, double end) {}

    public boolean receiveBhpLayerEvent(BhpLayerEvent e) {
        BhpLayer layer = e.getSource();
        int flag = e.getTypeFlag();

        if (layer!=null&&layer.getIdNumber() == this.getIdNumber()) {
            if ((flag & BhpViewer.COLORBAR_FLAG) != 0) { // use my transparent value
                updateColorMap((SeismicColorMap) e.getParameter());
            }
            //System.out.println("BhpSeismicLayer.receiveBhpLayerEvent myself");
            return false;
        }

        int mflag = super.handleBhpLayerEvent(e);
        boolean updated=false;
        // need code for MAPSETTING_FLAG to synchronize map settings
        if ((flag & BhpViewer.MAPSETTING_FLAG)!=0) {
            BhpMapLayer mapSource = (BhpMapLayer)layer;
            int mapFlag = getMapSynFlag();
            if (((mapFlag & MAP_TRANSPOSE)!=0) &&
                (mapSource.isTransposeImage()!=this.isTransposeImage()))  {
                setTransposeImage(mapSource.isTransposeImage());
                mflag = mflag | BhpViewer.MAPSETTING_FLAG;
            }
            if (((mapFlag & MAP_X_DIR_FLAG)!=0) &&
                (mapSource.isReversedHorizontalDirection()!=isReversedHorizontalDirection()))  {
                setReversedHorizontalDirection(mapSource.isReversedHorizontalDirection());
                mflag = mflag | BhpViewer.MAPSETTING_FLAG;
            }
            if (((mapFlag & MAP_Y_DIR_FLAG)!=0) &&
                (mapSource.getReversedSampleOrder()!=getReversedSampleOrder()))  {
                this.setReversedSampleOrder(mapSource.getReversedSampleOrder());
                mflag = mflag | BhpViewer.MAPSETTING_FLAG;
            }
        }

        if ((((flag & BhpViewer.MODEL_FLAG)!=0) && (this.getPropertySync())) ||
            (((flag & BhpViewer.MODEL_FLAG)!=0) && (this.getModelLayerSync()))) {
            String properties = this.getParameter().getProperties();
            String currentProperty = this.getPropertyName();
            String currentLayer = "";
            int colonIndex = currentProperty.lastIndexOf(":");
            if (colonIndex > 0) {
                currentLayer = currentProperty.substring(colonIndex+1).trim();
                currentProperty = currentProperty.substring(0, colonIndex);
            }
            String newProperty = layer.getPropertyName();
            String newLayer = "";
            if (layer instanceof BhpMapLayer) {
                colonIndex = newProperty.lastIndexOf(":");
                if (colonIndex > 0) {
                    newLayer = newProperty.substring(colonIndex+1).trim();
                    newProperty = newProperty.substring(0, colonIndex);
                }
            }
            if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
                System.out.println("Current " + currentProperty + " || New " + newProperty);

            if (properties!=null && properties.length()>0 &&
                this.getPropertySync() && properties.indexOf(newProperty)!=-1) {
                //this.setPropertyName(np);
                if (!currentProperty.equals(newProperty)) {
                    currentProperty = newProperty;
                    mflag = mflag | BhpViewer.MODEL_FLAG;
                }
            }
            if (this.getModelLayerSync() && newLayer.length()>0) {
                String traclAtt = this.getParameter().getValueAt(0, 1).toString();
                int dashIndex = traclAtt.indexOf("-");
                int squareIndex = traclAtt.indexOf("[");
                if (squareIndex < 0) {
                    squareIndex = traclAtt.length() - 1;
                }
                int layerNumberR = Integer.parseInt(traclAtt.substring(dashIndex+1, squareIndex).trim());
                int layerNumberL = Integer.parseInt(traclAtt.substring(0, dashIndex).trim());
                int newLayerNumber = Integer.parseInt(newLayer);
                if (newLayerNumber>=layerNumberL &&
                    newLayerNumber<=layerNumberR &&
                    (!newLayer.equals(currentLayer))) {
                    currentLayer = newLayer;
                    mflag = mflag | BhpViewer.MODEL_FLAG;
                }
            }
            this.setPropertyName(currentProperty+ ":" + currentLayer);
        }
        else if (((flag & BhpViewer.EVENTNAME_FLAG)!=0) && (this.getPropertySync())) {
            String properties = this.getParameter().getProperties();
            String np = layer.getPropertyName();
            if (properties!=null && properties.length()>0 &&
                properties.indexOf(np)!=-1) {
                this.setPropertyName(np);
                mflag = mflag | BhpViewer.EVENTNAME_FLAG;
            }
        }

        if (((mflag & BhpViewer.DATAINCREMENT_FLAG)!=0) ||
            ((mflag & BhpViewer.DATASELECTION_FLAG)!=0) ||
            ((mflag & BhpViewer.MODEL_FLAG)!=0) ||
            ((mflag & BhpViewer.NAME_FLAG)!=0)) {
            this.generateLayer();
            updated=true;
        }
        else if (mflag != 0) {
            this.updateLayer();
        }
        return updated;
    }

    public void setupSeismicDisplay() {
        _pipeline = new DefaultWorkflow(_reader);
        _pipeline.setDataSelector(new BhpListenerDataSelector());
        _pipeline.getInterpretation().getTraceNormalization().setNormalizationMode(
                TraceNormalizer.AMPLITUDE_MAXIMUM);
    _pipeline.getInterpretation().getTraceInterpolator().
        setInterpolationType(TraceInterpolator.STEP);
        _rasterizer = new BhpMapRasterizer();
        _pipeline.setTraceRasterizer(_rasterizer);

        if (_pipelineNode != null) {
            BhpViewerHelper.initWithXML(viewer, _pipelineNode, _pipeline);
            _pipelineNode = null;
        }
        else {
            if (_sampleLayer != null) {
                boolean sampleLayerIsMap = false;
                if (_sampleLayer instanceof BhpMapLayer) {
                    sampleLayerIsMap = true;
                }
                initPipelineWithAnother(_sampleLayer.getPipeline(), sampleLayerIsMap);
            }
        }

        double modelUnitPerTrace = getLayerHorizontalScale();
        double modelUnitPerSample = getLayerVerticalScale();
        _image = new BhpMapPainter(0, 0, modelUnitPerTrace, modelUnitPerSample, _pipeline);

        if (_transposeXML != null) {
            this.setTransposeImage(_transposeXML.booleanValue());
            _transposeXML = null;
        }
        if (_reverseHXML != null) {
            this.setReversedHorizontalDirection(_reverseHXML.booleanValue());
            _reverseHXML = null;
        }

        if (_sampleLayer != null && (_sampleLayer instanceof BhpMapLayer)) {
            this.setTransposeImage(((BhpMapLayer)_sampleLayer).isTransposeImage());
            this.setReversedHorizontalDirection(((BhpMapLayer)_sampleLayer).isReversedHorizontalDirection());
        }
        modelUnitPerTrace = getLayerHorizontalScale();
        modelUnitPerSample = getLayerVerticalScale();
        _image.setScale(modelUnitPerTrace, modelUnitPerSample);
        double modelWidth = _reader.getMetaData().getNumberOfTraces() * modelUnitPerTrace;
        double modelHeigth = _reader.getMetaData().getSamplesPerTrace() * modelUnitPerSample;
        if (((BhpMapPainter)_image).getTranspose()) {
            double swapTemp = modelWidth;
            modelWidth = modelHeigth;
            modelHeigth = swapTemp;
        }
        _modelBox = new Bound2D(0, 0, modelWidth, modelHeigth);
        _deviceBox = new Bound2D(0, 0, modelWidth, modelHeigth);
        _transformation = new Transform2D(_modelBox, _deviceBox, false, false);

        _shapeListLayer = new CommonShapeLayer();
        _containerModel = new CommonDataModel();
        _containerModel.addLayer(_shapeListLayer);
        _containerModel.setBoundingBox(_modelBox);
        _shapeListLayer.setBoundingBox(_modelBox);

        RenderingAttribute att = new RenderingAttribute(_containerModel);
        _image.setAttribute(att);

        _shapeListLayer.addShape(_image);
        this.setModel(_containerModel);
        this.setTransformation(_transformation);

        if (_polylineNode != null) {
            RenderingAttribute lineAtt = new RenderingAttribute();
            lineAtt.setLineColor(Color.red);
            insertPolylinesWithXML(lineAtt);
            _polylineNode = null;
        }
        _viewer.addBhpLayer(_window, this);
        _viewer.changeLoadingLayerNumber(BhpBusyDialog.RMV_LAYER);
    }

    private void initPipelineWithAnother(SeismicWorkflow plR, boolean isMap) {
        if (plR.getTraceRasterizer() != null) {
            TraceRasterizer rastR = plR.getTraceRasterizer();
            if (rastR instanceof AbstractBhpColorInterpolatedRasterizer) {
                AbstractBhpColorInterpolatedRasterizer rasterizer = (AbstractBhpColorInterpolatedRasterizer)rastR;
                _rasterizer.setColorMapStartValue((rasterizer).getColorMapStartValue());
                _rasterizer.setColorMapEndValue((rasterizer).getColorMapEndValue());
            }
            BhpViewerHelper.initColormapWithAnother(_rasterizer.getColorMap(), rastR.getColorMap());
        }
    }

    private void insertPolylinesWithXML(RenderingAttribute att) {
        Node tmpNode;
        NodeList nodeList = _polylineNode.getChildNodes();
        for (int i=0; i<nodeList.getLength(); i++) {
            tmpNode = nodeList.item(i);
            if (tmpNode.getNodeName().equals("Polyline") &&
                tmpNode.getNodeType()==Node.ELEMENT_NODE) {
                ElementAttributeReader emt = new ElementAttributeReader(tmpNode);
                insertPolylineWithString(emt.getString("points"), att);
            }
        }
    }

    private void insertPolylineWithString(String pstring, RenderingAttribute att) {
        StringTokenizer stk = new StringTokenizer(pstring, ",");
        int size = stk.countTokens() / 2;
        double xs[] = new double[size];
        double ys[] = new double[size];
        int index = 0;
        while(stk.hasMoreTokens()) {
            xs[index] = Double.parseDouble(stk.nextToken());
            ys[index] = Double.parseDouble(stk.nextToken());
            index++;
        }
        Polyline2D pl = new Polyline2D(size, xs, ys);
        pl.setAttribute(att);
        _shapeListLayer.addShape(pl);
    }

    private void updatePolylines() {
        double plXFactor = getLayerHorizontalScale() / _image.getModelUnitsPerTrace();
        double plYFactor = getLayerVerticalScale() / _image.getModelUnitsPerSample();
        if (((BhpMapPainter)_image).getTranspose()) {
            double tempFactor = plXFactor;
            plXFactor = plYFactor;
            plYFactor = tempFactor;
        }
        //TODO: This is not a safe comparison annd may be resulting in
        //unnecessary updates due to floating-point precision errors.
        //Analyze this.
        if (plXFactor==1 && plYFactor==1) {
            return;
        }
        if (plXFactor==0 || plYFactor==0) {
            return;
        }

        if (_window.getSelectedLayer() == this) {
            // do the clean up
            _window.getBhpPlot().getMouseCenter().valueChanged(
                    new javax.swing.event.ListSelectionEvent(
                    _window.getBhpLayerList(), 0, 0, true));
        }
        Iterator shapeIter = _shapeListLayer.getShapes();
        Object tmpShape = null;
        Polyline2D tmpPolyline = null;
        int tmpPlSize = 0;
        while (shapeIter.hasNext()) {
            tmpShape = shapeIter.next();
            if (tmpShape instanceof Polyline2D) {
                tmpPolyline = (Polyline2D) tmpShape;
                tmpPlSize = tmpPolyline.getSize();
                double tmpx[] = new double[tmpPlSize];
                double tmpy[] = new double[tmpPlSize];
                for (int i=0; i<tmpPlSize; i++) {
                    tmpx[i] = tmpPolyline.getXAt(i) * plXFactor;
                    tmpy[i] = tmpPolyline.getYAt(i) * plYFactor;
                }
                tmpPolyline.setCoordinates(tmpPlSize, tmpx, tmpy);
            }
        }
    }

    /**
     * Fix for Issue BHPVIEWER-8: removed stub.
     *
     * Returns 1 if option "show dead traces" is enabled, otherwise, 0.
     */
    public int getMissingDataSelection() {
      return _parameter.getMissingDataSelection();
    }
}