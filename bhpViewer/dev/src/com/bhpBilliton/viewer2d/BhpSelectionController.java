/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

// QIW Disable references to xml horizon data
//import com.bhpBilliton.viewer2d.dataAdapter.XmlHorizonWriter;

import com.bhpBilliton.viewer2d.util.ErrorDialog;

import com.gwsys.gw2d.model.DataLayer;
import com.gwsys.gw2d.model.DataShape;
import com.gwsys.gw2d.model.CommonDataModel;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.gw2d.model.CommonShapeLayer;
import com.gwsys.gw2d.shape.RectangleShape;
import com.gwsys.gw2d.util.Bound2D;
import com.gwsys.gw2d.util.Transform2D;
import com.gwsys.gw2d.view.SimpleViewContainer;
import com.gwsys.gw2d.view.VisuableComponent;
import com.gwsys.seismic.reader.TracePickingBehavior;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is a customized cgSelectionController of JCarnacPro.
 *               Because the JCarnacPro class cannot be easily adapted for
 *               the required behavior, this class is created with most of
 *               its code borrowing from its JCarnacPro conterpart. Please
 *               refer to the JCarnacPro documentation for more detail. <br>
 *               This class is used for event shape editing only. When a
 *               <code>{@link BhpEventLayer}</code> is selected, its
 *               <code>{@link BhprEventShape}</code> will be selected
 *               automatically. The shape can be editted with mouse.
 *               A user can press and drag to
 *               redefine the value along the path. Right mouse press will
 *               set the value at that horizontal position to null. <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpSelectionController implements BhpEventLayerListener {

   // Editing Modes
   private final static int SELECTION_MODE            = 1;
   private final static int EDIT_MOVE_MODE            = 2;
   private final static int EDIT_SCALE_MODE           = 3;
   private final static int EDIT_ROTATE_MODE          = 4;
   private final static int EDIT_DRAG_VERTEX_MODE     = 5;
   private final static int EDIT_DRAG_EDGE_MODE       = 6;
   private final static int EDIT_INSERT_VERTEX_MODE   = 7;
   private final static int EDIT_DELETE_VERTEX_MODE   = 8;
   private final static int GROUP_EDIT_MOVE_MODE      = 9;
   private final static int GROUP_EDIT_ROTATE_MODE    = 10;
   private final static int GROUP_EDIT_SCALE_MODE     = 11;
   private final static int EVENT_EDIT_MODE           = 12;
   // mode to edit event. If the event is selected, press within the left and right
   // boundary will set the value of that point. If dragged, setting the point values
   // along the line.

   private final static int EVENT_DEFINE        = 1;
   private final static int EVENT_DELETE        = -1;
   private final static int SELECT_BY_POINT     = 1;  // Default, only supported selection

   private AbstractPlot _plot;
   private TracePickingBehavior _snapper;
   private int _snapMode;
   private int _snapLimit;
   private BhpPickingManager  _pickingManager;
   private CommonShapeLayer   _layer;
   private CommonShapeLayer   _controlLayer;
   private CommonDataModel   _model;
   private CommonDataModel   _controlModel;
   //private cgCarnacComponent  _view;
   private BhpEventLayer  _view;
   private Transform2D   _tr;
   private RectangleShape        _rband;
   private double             _xrub;
   private double             _yrub;
   private Bound2D             _originalBBox;
   private BhpEditorController _editController;

   //for multiple selection
   private List _selectedShapes       = new ArrayList();
   private List _selectedShapesBBoxes = new ArrayList();

   private DataShape _lastSelectedShape = null;
   private DataShape _newSelectedShape  = null; //used to dermine if mouse is pressed on the same shape
   //private ShapeSelectionControlPoint _controlPoint = null;

   private BhpEventShape _eventShape = null;
   private int _eventMode = EVENT_DEFINE;
   private int _lastEventPoint = -1;

   private double       _x;
   private double       _y;
   private int          _mode          = SELECTION_MODE;
   private boolean      _deviceSpace   = true;
   private int          _coarseness    = 3;

   private BhpSelectionController() {}//disable default constructor

  /**
   * Constructor for the BhpSelectionController object.
   *
   * @param model the Model which will contain the shape objects.
   * @param view the View associated with the model.
   * @param layer the layer within the model upon which editing operations will be performed.
   */
   public BhpSelectionController(CommonDataModel model, BhpEventLayer view,
                                 CommonShapeLayer layer, RenderingAttribute att,
                                 BhpPickingManager pm, AbstractPlot plot) {
        _plot = plot;
        _snapper = new TracePickingBehavior();
        _snapMode = TracePickingBehavior.SNAP_NO;
        _snapLimit = TracePickingBehavior.SNAP_DEFAULT;
        _pickingManager = pm;
        _model = model;
        _view  = view;
        _layer = layer;
        _tr    = calcTransformation();

        _controlLayer = null;
        _controlModel = _model;

        _editController = new BhpEditorController(_view, att);
        if (_layer.isVisible()) {
            selectEventShape();
            _mode = SELECTION_MODE;
        }
   }

    /**
     * Gets the current model being operating on.
     * @return the model that is currently active for selection of shapes.
     */
    public CommonDataModel getModel() { return _model; }
    /**
     * Sets the model to be operated on.
     * @param model the model that is to be made active for selection of shapes.
     */
    public void setModel(CommonDataModel model) { _model = model; }
    /**
     * Gets the current layer being operating on.
     * @return the layer that is currently active for selection of shapes.
     */
    public DataLayer getLayer() { return _layer; }
    //public void setLayer(cgLayer layer) { _layer = layer; }
    /**
     * Gets the current view being operating on.
     * @return the view that is currently active for selection of shapes.
     */
    public VisuableComponent getView() { return _view; }
    /**
     * Sets the view to be operated on.
     * @param view the view that is to be made active for shape selection.
     */
    //public void setView(cgCarnacComponent view) { _view = view; }

    /**
     * Gets the shape that is currently selected.
     * @return the shape that is currently active for editing.
     */
    public DataShape getLastSelectedShape() { return _lastSelectedShape; }

    /**
     * This method is called when the event layer and its shape is changed. <br>
     * It clears the current selection and re-selects the shape thus
     * selection object will be updated accordingly.
     * @param source the layer that has changed.
     */
    public void eventLayerChanged(BhpLayer source) {
        removeControlLayer();
        unselectAllShapes();
        _controlLayer      = null; //reset it for next shape picking
        _lastSelectedShape = null;
        _lastEventPoint = -1;
        _mode              = SELECTION_MODE;
        if (source.isBhpLayerVisible()) {
            selectEventShape();
        }
        _view.repaint();
    }


  /**
   * Removes the cgPlotView used for manipulating shapes when utilizing a
   * cgStackPlotView for the main view. <br>
   * The various control layers used for editing are removed.
   * This method should be called before the removal of this object.
   */
   public void removeManipulationObjects() {
      removeControlLayer();
      _selectedShapes.clear();
      _selectedShapesBBoxes.clear();
      _lastSelectedShape = null;
      //_geometry_editor   = null;
      _mode              = SELECTION_MODE;

      _view.repaint();
  }


  /**
   * The MouseEvent passed in will decide shape selection/deselection and editing
   * operations.
   *
   * @param e A MouseEvent to decide shape selection/deselection and editing operations.
   * @param MouseEventType the type of mouse event
   */
   public void processMouseEvent(MouseEvent e) {
       int MouseEventType = e.getID();
       switch (MouseEventType) {

         case MouseEvent.MOUSE_PRESSED:{
             int eventFlag = e.getModifiers();
         if ( eventFlag==InputEvent.BUTTON1_MASK)
                 shapeSelection(e, eventFlag);
             else if (eventFlag==InputEvent.BUTTON2_MASK)
                 shapeSelection(e, eventFlag);

             else if (eventFlag==InputEvent.BUTTON3_MASK)
                 saveEvent();

            break;
    }
         case MouseEvent.MOUSE_MOVED:
            break;
         case MouseEvent.MOUSE_DRAGGED:
            if (_pickingManager.getPickMode() == BhpPickingManager.PICK_POINT_BY_POINT)
                shapeEdition(e, MouseEventType);
            break;
         case MouseEvent.MOUSE_RELEASED:
            shapeEdition(e, MouseEventType);
            break;
       }
   }

   /**
    * Save eVENT, Disabled by QIWB
    */
   private void saveEvent(){
/*
     String fileName = _view.getPathlist();
     if (fileName == null) fileName="tempHorizon.xhz";
     java.io.File dataFile=new java.io.File(fileName);
     try{
       if ( !dataFile.exists() )
         //create a new file if this file does not already exit
         dataFile.createNewFile();
       int saveApproval = JOptionPane.showConfirmDialog(
           _view._viewer, fileName + " already exits. \n" + "Do you want to replace it?",
           "Save Horizon as.....", JOptionPane.YES_NO_OPTION );

       if ( saveApproval == JOptionPane.YES_OPTION ) {
         ArrayList layers=new ArrayList();
         layers.add(_view);
         XmlHorizonWriter.writeHorizons(dataFile.getAbsolutePath(), layers);

         String msg = "Event is saved as file : " + fileName;
         _view._viewer.getStatusBar().showMessage(BhpViewer.INFO_MESSAGE, msg);
       }
     }catch(Exception e){
       String msg = "Saving event fails : " + e.getMessage();
       _view._viewer.getStatusBar().showMessage(BhpViewer.ERROR_MESSAGE, msg);
     }*/
   }

   /**
    * Processes key event. <br>
    * Currently, this controller handles no key event. They are all ignored.
    */
    public void processKeyEvent(KeyEvent e) {}
    protected void shapeHasBeenSelected( DataShape shape ) {}
    protected void shapeHasBeenDeselected( DataShape shape ) {}

  /**
   * Produces a List containing the highlighted shapes.
   * @return a collection of shapes that are currently selected.
   */
   public List getSelectedShapes() {
      List listOfShapes = new ArrayList();
      for (int j=0; j < _selectedShapes.size(); j++) {
         DataShape shape = (DataShape)(_selectedShapes.get(j));
         listOfShapes.add(((DataShape)(shape)));
      }

      return listOfShapes;
   }

   /**
    * Adds the shape to the selected list and hightlight it.
    * @param shape shape that is newly selected.
    */
   public void addShapeToSelected(DataShape shape) {
      if ( _selectedShapes.contains(shape) ) //hit a shape that already been highlighted
         return;
      _selectedShapes.add(((DataShape)(shape)));
      shapeHasBeenSelected(shape);
      refreshControlLayer();
   }

   /**
    * Deletes the shape from the selected list.
    * @param shape the shape to be deselected.
    */
   public void unselectShape(DataShape shape) {
      if ( _selectedShapes.contains(shape) ) {
         shapeHasBeenDeselected(shape);
         _selectedShapes.remove(shape);
         refreshControlLayer();
      }
   }

   /**
    * Deletes all the shapes from the selected list.
    */
   public void unselectAllShapes() {
      for (int j = (_selectedShapes.size() - 1); j >= 0; j--) {
         DataShape shape = (DataShape)(_selectedShapes.get(j));
         shapeHasBeenDeselected(shape);
      }
      _selectedShapes.clear();
      _selectedShapesBBoxes.clear();
   }


  /**
   * Deletes the control layer containing the currently selected control elements.
   * The layer is then regenerated.
   */
   public void refreshControlLayer() {
      removeControlLayer();
      _controlLayer = _editController.createCtlHandlesBBoxes(_selectedShapes,
                                                             getControlModel(),
                                                             _controlLayer);
   }


  /**
   * Sets shape selection to be by point.
   * This is the default mode of selection.
   *
   * @param coarseness the radius in pixels away from the selection point that will
   * be considered for shape selection.
   */
   public void setSelectionByPoint(int coarseness) {
      _coarseness = coarseness;
   }

   //////////////////////////
   //private methods
   /////////////////////////
   private Transform2D calcTransformation() {
      Transform2D tr;
      if (_view instanceof SimpleViewContainer) {
          tr = ((SimpleViewContainer)_view).getTransformation();
      } else {
          tr = new Transform2D();
      }
      return tr;
   }

   // Given a MouseEvent extracts the x, y coordinates.
   private Point calcPoint(MouseEvent e) {

      _tr = calcTransformation();
      return new Point(e.getX(), e.getY());
   }

  /*
   * Removes the DataShapeListLayer containing the bounding boxes and control handles.
   */
   private void removeControlLayer() {
      if (_controlLayer != null) {
          _model.removeLayer(_controlLayer);
         _controlLayer = null; //reset it for next shape picking
      }
   }


  /*
   * Returns the model containing the bounding boxes and control handles.
   */
   private CommonDataModel getControlModel() {
       return _model;
   }

    // check if the mouse position is within the left and right boundary of selected shapes.
    // Basically, we'll do a selection according to the x position only.
    // If succeed, set _newSelectedShape to that shape and _eventEditPoint to the vertex
    private int pickEventEditingPoint(DataLayer layer, double x, double y) {
        int eventPointIndex = -1;

        try {
            if (_eventShape != null) {
                Bound2D transRect = new Bound2D(x, y, x+1, y+1);
                transRect = _tr.inverseTransform(transRect);
                double transformedX = transRect.x;
                double[] xvalues = _eventShape.getXValues();
                for (int i=0; i<xvalues.length-1; i++) {
                    if (xvalues[i] <=transformedX && xvalues[i+1]>=transformedX) {
                        double delta1 = Math.abs(transformedX - xvalues[i]);
                        double delta2 = Math.abs(transformedX - xvalues[i+1]);
                        if (delta1 <= delta2) eventPointIndex = i;
                        else eventPointIndex = i + 1;
                        break;
                    }
                }
            }
        }
        catch (Exception ex) {
            eventPointIndex = -1;
            //ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
            System.out.println("BhpSelectionController.pickEventEditingPoint Exception: "+ ex.getMessage());
        }
        return eventPointIndex;

    }

    private void selectEventShape() {
        DataShape shape = _layer.getShape(0);
        if (shape instanceof BhpEventShape) {
            _newSelectedShape = shape;
            _eventShape = (BhpEventShape) shape;
            DataShape theShape = null;
            if (_eventShape.isDrawAllLine()) {
                theShape = _eventShape.getAllLine();
                _selectedShapes.add(theShape);
                shapeHasBeenSelected(theShape);
            }
            else {
                for (int iii=0; iii<_eventShape.getShapeNumber(); iii++) {
                    theShape = _eventShape.getShapeAt(iii);
                    _selectedShapes.add(theShape);
                    shapeHasBeenSelected(theShape);
                }
            }

             _lastSelectedShape = _newSelectedShape;
             _controlLayer = _editController.createCtlHandlesBBoxes(_selectedShapes,
                                                                    getControlModel(),
                                                                    _controlLayer);
        }
        else {
            ErrorDialog.showInternalErrorDialog(_view.getViewer(), Thread.currentThread().getStackTrace(),
                                                "BhpSelectionController Error : Cannot find the event shape");
        }
    }

    //Fix for BHPVIEWER-2.  The G&W Seismic Lib never implemented snap-to-min or snap-to-zero.  However, the INT viewer did.
    //I extracted an interface for a class which implements only int calculateSnapSample(int index, float[] samples).
    //The implementations of NearMinPicking and NearZeroPicking were copied from the G&W NearMaxPicking class, and should probably be rewritten.
    private double applySnapping(int index, double value, Point mp) {
        //System.out.println("    applySnapping 11 " + index + " : " + value);
        if (Double.isNaN(value) || Float.isNaN((float)value)) return value;
        if (value == _eventShape.getNullValue()) return value;

        //int snapmode = _pickingManager.getSnapMode();
        int snapmode = _view.getLayerSnapMode();
        int snaplimit = _view.getLayerSnapLimit();
        
        if ((snapmode == BhpPickingManager.SNAP_NO) || (snaplimit == 0)) return value;

        double re = value;
        BhpLayer refLayer = _plot.getBhpLayer(_plot.getAxisAssociateLayer());
        if (refLayer.getDataType() == BhpViewer.BHP_DATA_TYPE_SEISMIC) {
            try{
                _snapper.setSeismicReader(refLayer.getPipeline().getDataLoader().getDataReader());
                if ((snapmode != _snapMode) || (snaplimit != _snapLimit)) {
                    _snapMode = snapmode;
                    _snapLimit = snaplimit;
                    _snapper.setPickMode(_snapMode, _snapLimit);
                }
                index = refLayer.getPipeline().getDataLoader().getDataSelector().virtualToReal(index);
                re = _snapper.getSnapValue(index, value);
            }
            catch (Exception ex) {
                ErrorDialog.showInternalErrorDialog(_view.getViewer(), Thread.currentThread().getStackTrace(),
                                                    "BhpSelectionController Exception: ");
                re = value;
            }
        }
        else
            JOptionPane.showMessageDialog(null,"For snapping, the annotation layer must be a seismic layer.");
        return re;
    }

    private void adjustEventShape(int thisIndex, int lastIndex, double value, Point mp) {
        //System.out.println("adjustEventShape " + thisIndex + "~" + lastIndex + " : " + value);
        if (lastIndex < 0 || lastIndex == thisIndex) {
            double tmpValue = applySnapping(thisIndex, value, mp);
            _view.setEventValueAt(thisIndex, tmpValue, true);
            return;
        }
        double lastValue = _eventShape.getValues()[lastIndex];
        double valueStep = (value-lastValue) / Math.abs(lastIndex-thisIndex);
        double tmpValue = 0.0;
        int indexStep = 1;
        if (lastIndex > thisIndex) indexStep = -1;
        int totalCount = (int) (Math.abs(lastIndex-thisIndex));
        int curIndex = 0;
        for (int i=1; i<=totalCount; i++) {
            curIndex = lastIndex+i*indexStep;
            tmpValue = applySnapping(curIndex, lastValue + valueStep * i, mp);

            if (i == totalCount) {
                //_eventShape.setValueAt(curIndex, tmpValue, true);
                _view.setEventValueAt(curIndex, tmpValue, true);
            }
            else {
                //_eventShape.setValueAt(curIndex, tmpValue, false);
                _view.setEventValueAt(curIndex, tmpValue, false);
            }
        }
    }

  /*
   * This method decides what edit mode to assume.
   * The MouseEvent passed in will decide shape selection/deselection
   * Selected shapes are added to a list to be acted upon.
   * For highlight boxes a separate control layer is created to contain them.
   *
   * parameter: e A MouseEvent to decide shape selection/deselection
   *
   */
   private void shapeSelection(MouseEvent e, int mouseMask) {

      Point p = calcPoint(e);

      if (_deviceSpace) {
         _x = p.x;
         _y = p.y;
      }
      else {
         Bound2D transBox = new Bound2D(p.x, p.y, p.x + 1, p.y + 1);
         transBox = _tr.inverseTransform(transBox);
         _x = transBox.x;
         _y = transBox.y;
      }

      if (_controlLayer != null) {

            // Then we need to check if the mouse is between the left and right bound.
            // If true, go to event editing mode. Otherwise, deselect.
            _newSelectedShape = null;// reset _newSelectedShape to null before new picking


            int eventPointIndex = pickEventEditingPoint(_controlLayer, _x, _y);

            if (eventPointIndex >= 0) {
                _mode = EVENT_EDIT_MODE;
                int newEventMode = EVENT_DEFINE;
                if (mouseMask == MouseEvent.BUTTON2_MASK)
                    newEventMode = EVENT_DELETE;
                if (newEventMode != _eventMode) {
                    // this makes sure in point-to-point mode, mouse3 press
                    // and mouse1 press won't be connected together.
                    _lastEventPoint = -1;
                }
                _eventMode = newEventMode;
                this.shapeEdition(e, mouseMask);
            }
            else {
                // outside left right right bounds, do nothing
                return;

            }

      }
      else {  //because _controlLayer == null, which means no shape has been highlighted,
              //something is wrong.
         // ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(),
          System.out.println("BhpSelectionController error : no event shape is selected");
         return;
      }
   }


  /*
   * Passes off editing commands to the Editor Controller object.
   *
   * @param e A MouseEvent for determining how a shape is affected
   * @param MouseEventType A constant for determining what kind of MouseEvent this is.
   */
   private void shapeEdition(MouseEvent e, int mouseMask) {
       int MouseEventType = e.getID();

      Point p = calcPoint(e);

      double newX;
      double newY;
      double oldX;
      double oldY;

      if (_deviceSpace) {
         Bound2D transBox;
         transBox = new Bound2D(p.x, p.y, p.x + 1, p.y + 1);
         transBox = _tr.inverseTransform(transBox);
         newX = transBox.x;
         newY = transBox.y;

         transBox = new Bound2D(_x, _y, _x + 1, _y + 1);
         transBox = _tr.inverseTransform(transBox);
         oldX = transBox.x;
         oldY = transBox.y;

         _x = p.x;
         _y = p.y;
      }
      else {
         Bound2D transBox = new Bound2D(p.x, p.y, p.x + 1, p.y + 1);
         transBox = _tr.inverseTransform(transBox);
         newX = transBox.x;
         newY = transBox.y;

         _x = newX;
         _y = newY;
      }


      switch(_mode) {
         case SELECTION_MODE:
            // the default implementation process the event for selection mode
            // other than SELECT_BY_POINT. Here, we always do SELECT_BY_POINT.
            // So, no action is needed
            break;
         case EVENT_EDIT_MODE:
            if (MouseEventType == MouseEvent.MOUSE_RELEASED) {
               _mode = SELECTION_MODE;
               //_lastEventPoint = -1;
               return;
            }

            _newSelectedShape = null;
            int eventPointIndex = pickEventEditingPoint(_controlLayer, _x, _y);


            if (eventPointIndex >= 0) {
                int appliableLastEventPoint = _lastEventPoint;
                if (MouseEventType == MouseEvent.MOUSE_PRESSED &&
                    ((_pickingManager.getPickMode() == BhpPickingManager.PICK_POINT_BY_POINT) ||
                    (_pickingManager.checkReset())))
                    appliableLastEventPoint = -1;

                if (_eventShape != null) {
                    unselectAllShapes();
                    removeControlLayer();
                    if (_eventMode != EVENT_DEFINE) {
                        newY = _eventShape.getNullValue();

                    }
        adjustEventShape(eventPointIndex, appliableLastEventPoint, newY, e.getPoint());
                    _lastEventPoint = eventPointIndex;
                    selectEventShape();
                }

            }
            break;
         case EDIT_DRAG_VERTEX_MODE:

            break;
         default:
            if (MouseEventType == MouseEvent.MOUSE_RELEASED) {
               _mode = SELECTION_MODE;
            }
            break;
      }

   }


}
