/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006
 
The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>
 
All licensees should note the following:
 
 *        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.
 
 *        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.
 
This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
 */

package com.bhpBilliton.viewer2d;

import com.gwsys.seismic.core.TraceRasterizer;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This abstract class extends TraceRasterizer of JSeismic
 *               and provides related fields and methods for linear color
 *               map interpolation. <br><br>
 * Copyright:    Copyright (c) 2001 <br>
 * Company:      BHP INT <br>
 * @author Synthia Kong
 * @version 1.0
 */

public abstract class AbstractBhpColorInterpolatedRasterizer implements TraceRasterizer {
    private double colorMapStartValue;
    private double colorMapEndValue;
    
    /**
     * Creates a BhpColorInterpolatedRasterizer and sets
     * the range to the default: (-1, 1).
     */
    public AbstractBhpColorInterpolatedRasterizer() {
        colorMapStartValue = -1;
        colorMapEndValue = 1;
    }
    
    /**
     * Calculates the translated value of color map start
     * related to the amplitude limits. If the amplitude limits
     * are on both sides of zero, the interpolation will be
     * done with a symmetry range of (-v, v) while
     * v=max(abs(min), abs(max)).
     * @param min minimum amplitude
     * @param max maximum amplitude
     * @return relative color map start value
     */
    public double getTranslatedColorMapStartValue(double min, double max) {
        double localMin = min;
        double localMax = max;
        if (min < 0 && max > 0) {
            double maxAmp = Math.max(Math.abs(min), Math.abs(max));
            localMin = -maxAmp;
            localMax = maxAmp;
        }
        double value = localMin + (localMax-localMin)*(colorMapStartValue+1)/2;
        return value;
    }
    
    /**
     * Calculates the translated value of color map end
     * related to the amplitude limits. If the amplitude limits
     * are on both sides of zero, the interpolation will be
     * done with a symmetry range of (-v, v) while
     * v=max(abs(min), abs(max)).
     * @param min minimum amplitude
     * @param max maximum amplitude
     * @return relative color map end value
     */
    public double getTranslatedColorMapEndValue(double min, double max) {
        double localMin = min;
        double localMax = max;
        if (min < 0 && max > 0) {
            double maxAmp = Math.max(Math.abs(min), Math.abs(max));
            localMin = -maxAmp;
            localMax = maxAmp;
        }
        double value = localMin + (localMax-localMin)*(colorMapEndValue+1)/2;
        return value;
    }
    
    /**
     * Calcuates the absolute color map value of a specific value
     * within the specified range. If the amplitude limits
     * are on both sides of zero, the interpolation will be
     * done with a symmetry range of (-v, v) while
     * v=max(abs(min), abs(max)).
     * @param value the relative value
     * @param min minimum amplitude
     * @param max maximum amplitude
     * @return the absolute color map value of "value".
     */
    public double getReverseTranslatedValue(double value, double min, double max) {
        double localMin = min;
        double localMax = max;
        if (min < 0 && max > 0) {
            double maxAmp = Math.max(Math.abs(min), Math.abs(max));
            localMin = -maxAmp;
            localMax = maxAmp;
        }
        double rvalue = -1 + 2 * (value-localMin)/(localMax-localMin);
        return rvalue;
    }
    
    /**
     * Retrieves the color map start value.
     * @return absolute color map start value.
     */
    public double getColorMapStartValue() {
        return colorMapStartValue;
    }
    
    /**
     * Retrieves the color map end value.
     * @param absolute color map end value.
     */
    public double getColorMapEndValue() {
        return colorMapEndValue;
    }
    
    /**
     * Sets the absolute color map start value.
     */
    public void setColorMapStartValue(double startValue) {
        colorMapStartValue = startValue;
    }
    
    /**
     * Sets the absolute color map end value.
     */
    public void setColorMapEndValue(double endValue) {
        colorMapEndValue = endValue;
    }
    
    /**
     * This method is required by TraceRasterizer of JSeismic.  Having a clone()
     * implementation that returns null definitely violates the contract for Object.clone(),
     * however, the preceeding documentation indicates that this behavior is required
     * by the TraceRasterizer, therefore, this method has been deprecated until
     * the side-effects of removing it can be analyzed.
     * @deprecated
     */
    public java.lang.Object clone() {
        return null;
    }
    
    public double getMinSpacing(){
        return 5;
    }
    
    public void setMinSpacing(double decThreshold){
        
    }
}