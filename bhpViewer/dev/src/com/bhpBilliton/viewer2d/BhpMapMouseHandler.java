/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006
 
The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>
 
All licensees should note the following:
 
 *        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.
 
 *        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.
 
This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
 */


package com.bhpBilliton.viewer2d;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;


import com.bhpBilliton.viewer2d.util.CaretStringArrayList;
import com.bhpBilliton.viewer2d.util.ErrorDialog;
import com.gwsys.seismic.indexing.HeaderFieldEntry;
import com.gwsys.seismic.indexing.SegyHeaderFormat;
import com.gwsys.seismic.indexing.SeismicConstants;
import com.gwsys.seismic.core.DataChooser;
import com.gwsys.seismic.core.SeismicWorkflow;
import com.gwsys.seismic.io.TraceHeader;

import com.gwsys.gw2d.gui.DynamicLineCreator;
import com.gwsys.gw2d.gui.DynamicLineEditor;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.gw2d.model.DataShape;
import com.gwsys.gw2d.model.CommonShapeLayer;
import com.gwsys.gw2d.model.CommonDataModel;
import com.gwsys.gw2d.shape.Polyline2D;
import java.awt.Component;
import java.util.logging.Logger;

/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class will handle the mouse event for BhpMapLayer.
 *               In map layer, users can make polyline insertion, edition,
 *               deleting, and broadcasting with mouse movements.
 *               cgPolylineShapeCreator is used for polyline insertion.
 *               cgSelectionController is used for polyline edition. <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpMapMouseHandler implements KeyListener {
    private static final int IN_DEFAULT = 0;
    private static final int IN_EDITING = 1;
    private static final int IN_CREATING = 2;
    
    private BhpMapLayer _layer;
    private RubberView _controlLayer;
    private int _mode;
    private DynamicLineCreator _creator;
    private DynamicLineEditor _editor;
    private RenderingAttribute _attribute;
    
    private JPopupMenu _popUpMenu;
    private static final Logger logger = Logger.getLogger(BhpMapMouseHandler.class.toString());
    private volatile int _popupX;
    private volatile int _popupY;
    /**
     * Constructs a new instance.
     * @param layer the map layer with which this handler is associated.
     * @param control the layer where temporary control objects will be placed.
     */
    public BhpMapMouseHandler(BhpMapLayer layer, RubberView control) {
        _layer = layer;
        _controlLayer = control;
        CommonDataModel controlModel = (CommonDataModel) (_controlLayer.getModel());
        CommonShapeLayer controlSPLayer = (CommonShapeLayer) (controlModel.getLayer(0));
        _mode = IN_DEFAULT;
        _creator = new DynamicLineCreator(_layer.getContainerModel(),
                _layer.getShapeListLayer(), _layer,
                controlModel, controlSPLayer, _controlLayer);
        _editor = new DynamicLineEditor(_layer.getContainerModel(),
                _layer, _layer.getShapeListLayer());
        _attribute = new RenderingAttribute();
        _attribute.setLineColor(Color.red);
        _creator.setAttribute(_attribute);
        
        JMenuItem broadcastTraverseMenuItem = new JMenuItem("Broadcast traverse");
        broadcastTraverseMenuItem.addActionListener(new BroadcastTraverseListener());
        
        JMenuItem deleteTraverseMenuItem = new JMenuItem("Delete traverse");
        deleteTraverseMenuItem.addActionListener(new DeleteTraverseListener());
        
        JMenuItem broadcastPointMenuItem = new JMenuItem("Broadcast point");
        broadcastPointMenuItem.addActionListener(new BroadcastPointListener());
        
        _popUpMenu = new JPopupMenu("");
        _popUpMenu.add(broadcastTraverseMenuItem);
        _popUpMenu.add(deleteTraverseMenuItem);
        _popUpMenu.add(broadcastPointMenuItem);
        
        javax.swing.SwingUtilities.windowForComponent(_layer).addKeyListener(this);
    }
    
    /**
     * Clean up. This method will remove all temporary objects
     * and cancel any ongoing action.
     */
    public void cleanupMapMouseHandler() {
        javax.swing.SwingUtilities.windowForComponent(_controlLayer).removeKeyListener(this);
        if (_mode == IN_EDITING) {
            _editor.doDeletion();
            _mode = IN_DEFAULT;
        } else if (_mode == IN_CREATING) {
            _creator.removeAllPoints();
        }
    }
    
    /**
     * Handles the key press event.
     * Currently, only delete key is mapped to delete the selected polyline.
     */
    public void keyPressed(KeyEvent e) {
        if (_mode==IN_EDITING && e.getKeyCode()==KeyEvent.VK_DELETE) {
            _editor.doDeletion();
            _mode = IN_DEFAULT;
        }
    }
    
    /**
     * Handles the key release event.
     * Currently, no action is taken.
     */
    public void keyReleased(KeyEvent e) {
    }
    
    /**
     * Handles the key type event.
     * Currently, no action is taken.
     */
    public void keyTyped(KeyEvent e) {
    }
    
    /**
     * Process the mouse event.
     */
    public void processMouseEvent(MouseEvent me) {
        int type = me.getID();
        switch (type) {
            case MouseEvent.MOUSE_PRESSED:{
                int mask = me.getModifiers();
                // This not the correct way to check for bitmasks, but
                // changing it to test for bitwise & != 0
                // breaks other behavior.  Leaving it for now.
                if (mask == MouseEvent.BUTTON1_MASK)
                    processMouseButton1PressEvent(me);
                else if (mask == MouseEvent.BUTTON3_MASK){
                    if (_mode == IN_EDITING) {
                        if (_popUpMenu != null) {
                            _popupX = me.getX();
                            _popupY = me.getY(); // workaround for Swing bug #4527633
                            _popUpMenu.show(me.getComponent(), _popupX, _popupY);
                        }
                    } else if (_mode == IN_CREATING) {
                        _creator.commitPoint(calculatePoint(me));
                        DataShape shapeCreated = _creator.getLastShape();
                        if (_creator.isEnabled() == false) {
                            if (shapeCreated != null) {
                                _editor.addShapeToSelected(shapeCreated);
                                _mode = IN_EDITING;
                            } else
                                _mode = IN_DEFAULT;
                        }
                    }
                }else if (mask == MouseEvent.BUTTON2_MASK){
                    if (_mode == IN_CREATING)
                        _creator.movePoint(calculatePoint(me));
                }
            }
            break;
            case MouseEvent.MOUSE_MOVED:
                if (_mode == IN_CREATING)
                    _creator.movePoint(calculatePoint(me));
                break;
            case MouseEvent.MOUSE_DRAGGED:
            case MouseEvent.MOUSE_RELEASED:
                if (_mode == IN_EDITING)
                    _editor.doEdition(me);
                else if (_mode == IN_CREATING) {
                    //    _creator.ProcessMouseEvent(calculatePoint(me), type);
                    if (_creator.isEnabled() == false)
                        _mode = IN_DEFAULT;
                }
                break;
        }
    }
    
    /**
     * Process the mouse double click event by
     * broadcast the selected point for data synchronization.
     * This will eventually create and broadcast a
     * <code>{@link BhpLayerEvent}</code> with
     * DATASELECTION_FLAG, selected trace id and
     * vertical position.
     */
    public void processMouseDoubleClick(MouseEvent me) {
        broadcastSelectedPoint(me);
    }
    
    private void broadcastTraverse() {
        Object selectedObj = _editor.getLastSelectedShape();
        if (selectedObj == null) return;
        if (!(selectedObj instanceof Polyline2D)) return;
        
        DataChooser dataSelector = _layer.getPipeline().getDataLoader().getDataSelector();
        double scalex = _layer.getLayerHorizontalScale();
        double scaley = _layer.getLayerVerticalScale();
        Point position = _layer.getViewPosition();
        Polyline2D line = (Polyline2D) selectedObj;
        int lineSize = line.getSize();
        int[] xvalues = new int[lineSize];
        int[] yvalues = new int[lineSize];
        for (int i=0; i<lineSize; i++) {
            if (_layer.isTransposeImage()) {
                xvalues[i] = (int) (Math.floor(line.getYAt(i) / scalex));
                yvalues[i] = (int) (Math.floor(line.getXAt(i) / scaley));
            } else {
                xvalues[i] = (int) (Math.floor(line.getXAt(i) / scalex));
                yvalues[i] = (int) (Math.floor(line.getYAt(i) / scaley));
            }
            if (xvalues[i] >= dataSelector.getVirtualModelLimits().getMaxX()) {
                xvalues[i] = (int)(dataSelector.getVirtualModelLimits().getMaxX()) - 1;
            }
            if (_layer.isReversedHorizontalDirection()) {
                xvalues[i] = (int)(dataSelector.getVirtualModelLimits().getMaxX()) - 1 - xvalues[i];
            }
            if (_layer.getReversedSampleOrder()) {
                yvalues[i] = _layer.getPipeline().getDataLoader().getDataReader().getMetaData().getSamplesPerTrace() - yvalues[i];
            }
            yvalues[i] = yvalues[i] + (int)(_layer.getPipeline().getDataLoader().getDataSelector().getStartSampleValue());
        }
        
        BhpSeismicTableModel layerParameter = _layer.getParameter();
        Hashtable selectParameter = new Hashtable();
        CaretStringArrayList hlist = new CaretStringArrayList();
        CaretStringArrayList vlist = new CaretStringArrayList();
        String hname = layerParameter.getMapHorizontalKeyName();
        String vname = layerParameter.getVerticalName();
        
        SeismicWorkflow pipeline = _layer.getPipeline();
        Vector formats = pipeline.getDataLoader().getDataReader().getDataFormat().getTraceHeaderFormats();
        SegyHeaderFormat headerFormat = (SegyHeaderFormat) (formats.firstElement());
        Vector headerFields = headerFormat.getHeaderFields();
        HeaderFieldEntry headerField;
        int hkeyIdentifier = -1;
        int hkeyType = SeismicConstants.DATA_FORMAT_INT;
        int vkeyType = SeismicConstants.DATA_FORMAT_INT;
        boolean findVkey = false;
        boolean findHkey = false;
        for (int j=0; j<headerFields.size(); j++) {
            headerField = (HeaderFieldEntry) (headerFields.elementAt(j));
            if(headerField.getName().equals(hname)) {
                hkeyIdentifier = headerField.getFieldId();
                hkeyType = headerField.getValueType();
                findHkey = true;
            }
            if(headerField.getName().equals(vname)) {
                vkeyType = headerField.getValueType();
                findVkey = true;
                //break;
            }
            if (findHkey && findVkey) break;
        }
        if (hkeyIdentifier < 0) {
            ErrorDialog.showInternalErrorDialog(_layer.getViewer(), Thread.currentThread().getStackTrace(),
                    "BhpMapMouseHandler.broadcastSelectedLine fails : Cannot find key " +
                    hname);
            return;
        }
        
        double[] vsettings = layerParameter.getVerticalSettings();
        Integer hkeyIdentifierObj = new Integer(hkeyIdentifier);
        TraceHeader tmeta;
        Hashtable headerValues;
        Object headerValue;
        double verticalValue;
        for (int i=0; i<lineSize; i++) {
            verticalValue = vsettings[0] + yvalues[i] * vsettings[2];
            if (verticalValue > vsettings[1]) verticalValue = vsettings[1];
            if (vkeyType == SeismicConstants.DATA_FORMAT_DOUBLE ||
                    vkeyType == SeismicConstants.DATA_FORMAT_FLOAT ||
                    vkeyType == SeismicConstants.DATA_FORMAT_IBM_FLOAT) {
                vlist.add(new Double(verticalValue));
            } else {
                vlist.add(new Integer((int)verticalValue));
            }
            
            tmeta = pipeline.getDataLoader().getDataSelector().
                    getTraceHeader(xvalues[i]);
            headerValues = tmeta.getDataValues();
            headerValue = headerValues.get(hkeyIdentifierObj);
            if (hkeyType == SeismicConstants.DATA_FORMAT_DOUBLE ||
                    hkeyType == SeismicConstants.DATA_FORMAT_FLOAT ||
                    hkeyType == SeismicConstants.DATA_FORMAT_IBM_FLOAT) {
                hlist.add(new Double(headerValue.toString()));
            } else {
                hlist.add(new Integer(headerValue.toString()));
            }
        }
        if(Bhp2DviewerConstants.DEBUG_PRINT > 0) {
            System.out.println("hlist " + hlist.toString());
            System.out.println("vlist " + vlist.toString());
        }
        
        selectParameter.put(hname, hlist);
        selectParameter.put(vname, vlist);
        
        TraceHeader tmeta2 = pipeline.getDataLoader().getDataSelector().
                getTraceHeader(0);
        Hashtable headerValues2 = tmeta2.getDataValues();
        Integer headerId;
        String headerName;
        Object headerValue2;
        for (int j=0; j<layerParameter.getRowCount(); j++) {
            headerName = ((String)(layerParameter.getValueAt(j, 0))).trim();
            if (headerName.equals(hname) || headerName.equals(vname)) continue;
            if (headerName.equals("tracl")) {
                if (_layer.getDataType() != BhpViewer.BHP_DATA_TYPE_SEISMIC)
                    continue;
            }
            headerId = BhpSegyFormat.getFieldForName(headerName, headerFormat);
            headerValue2 = null;
            if (headerId!=null && headerValues2.containsKey(headerId)) {
                headerValue2 = headerValues2.get(headerId);
            }
            if (headerValue2 != null) {
                selectParameter.put(headerName, headerValue2);
            } else {
                ErrorDialog.showInternalErrorDialog(_layer.getViewer(), Thread.currentThread().getStackTrace(),
                        "BhpMapMouseHandler.broadcastSelectedLine error: + " +
                        "Cannot find key " + headerName);
            }
        }
        BhpLayerEvent event = new BhpLayerEvent(
                BhpViewer.DATASELECTION_FLAG, _layer, selectParameter);
        _layer._viewer.broadcastBhpLayerEvent(event);
    }
    
    private void broadcastSelectedPoint(MouseEvent me) {
        int virtTraceId = -999;
        int finaly = 0;
        if (_layer != null) {
            //Fix for Jira Issue #bhpViewer-32.  Addition of the viewPos to the mousePos caused the wrong
            //header key values to be broadcast to synchronized viewer windows.  Commenting out
            //the addition of the viewPos is similar to the fix for incorrect arbitrary traverse
            //sync at BhpMouseCenter line #335.  Woody Folsom
            //Point viewPos = _layer.getViewPosition();
            Point mousePos = me.getPoint();
            Point sp = _layer.selectByPoint(new Point(mousePos.x/*+viewPos.x*/, mousePos.y/*+viewPos.y*/));
            if (sp != null) {
                virtTraceId = sp.x;
                finaly = sp.y;
            }
        }
        
        broadcastPoint(virtTraceId, finaly);
        
    }
    
    private void broadcastPoint(int virtTraceId, int finaly) {
        if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("BhpMapMouseHandler select trace " + virtTraceId + " : " + finaly);
        if (virtTraceId == -999 || _layer == null) {
            JOptionPane.showMessageDialog(_layer._viewer,"No trace is selected");
            return;
        }
        
        SeismicWorkflow pipeline = _layer.getPipeline();
        BhpSeismicTableModel layerParameter = _layer.getParameter();
        
        Hashtable selectParameter = new Hashtable();
        // eventually data selector will translate virtual trace id to real trace id
        TraceHeader tmeta = pipeline.getDataLoader().getDataSelector().
                getTraceHeader(virtTraceId);
        Hashtable headerValues = tmeta.getDataValues();
        Vector formats = pipeline.getDataLoader().getDataReader().getDataFormat().getTraceHeaderFormats();
        SegyHeaderFormat headerFormat = (SegyHeaderFormat) (formats.firstElement());
        Integer headerId;
        String headerName;
        Object headerValue;
        for (int j=0; j<layerParameter.getRowCount(); j++) {
            headerName = ((String)(layerParameter.getValueAt(j, 0))).trim();
            if (headerName.equals("tracl")) {
                if (_layer.getDataType() != BhpViewer.BHP_DATA_TYPE_SEISMIC)
                    continue;
            }
            headerId = BhpSegyFormat.getFieldForName(headerName, headerFormat);
            headerValue = null;
            if (headerId!=null && headerValues.containsKey(headerId)) {
                headerValue = headerValues.get(headerId);
            }
            if (headerName.equals(_layer.getParameter().getVerticalName())) {
                double[] vsettings = _layer.getParameter().getVerticalSettings();
                double verticalValue = vsettings[0] + finaly * vsettings[2];
                if (verticalValue > vsettings[1]) verticalValue = vsettings[1];
                headerValue = new Double(verticalValue);
            }
            if (headerValue != null) {
                selectParameter.put(headerName, headerValue);
            } else {
                ErrorDialog.showInternalErrorDialog(_layer.getViewer(), Thread.currentThread().getStackTrace(),
                        "BhpMapMouseHandler.broadcastSelectedPoint " +
                        "error: Cannot find key " + headerName);
            }
        }
        
        BhpLayerEvent event = new BhpLayerEvent(
                BhpViewer.DATASELECTION_FLAG, _layer, selectParameter);
        _layer._viewer.broadcastBhpLayerEvent(event);    
    }
    
    private void processMouseButton1PressEvent(MouseEvent me) {
        if (_mode == IN_EDITING) {
            _editor.doSelection(me);
            if (_editor.getLastSelectedShape() == null) {
                // we have a deselect
                _mode = IN_DEFAULT;
            }
        } else if (_mode == IN_CREATING) {
            _creator.addPoint(calculatePoint(me));
        } else { // default mode
            _editor.doSelection(me);
            if (_editor.getLastSelectedShape() != null) {
                // some shape has been selected
                _mode = IN_EDITING;
            } else {
                _creator.setEnabled(true);
                _creator.addPoint(calculatePoint(me));
                _mode = IN_CREATING;
            }
        }
    }
    
    private Point calculatePoint(MouseEvent e) {
        return new Point(e.getX(), e.getY() );
        
    }
    
    private class DeleteTraverseListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            _editor.doDeletion();            
            _mode = IN_DEFAULT;
        }
    }
    
    private class BroadcastTraverseListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            broadcastTraverse();
        }
    }
    
    private class BroadcastPointListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //use the saved X, Y coords of the MouseEvent that caused the creation of the popupMenu
            Point mousePoint = new Point(_popupX, _popupY); // workaround for Swing bug 4527633
            Point tracePoint = _layer.selectByPoint(mousePoint);
            
            if (tracePoint != null) {
                int virtTraceId = tracePoint.x;
                int finaly = tracePoint.y;
                broadcastPoint(virtTraceId, finaly);
            } else {
                JOptionPane.showMessageDialog(null, "Error: unable to determine X, Y coordinates of popUpMenu for broadcast.");
            }
        }
    }
}