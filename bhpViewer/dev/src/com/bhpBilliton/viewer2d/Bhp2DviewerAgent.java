/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.Component;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.FileChooserDescriptor;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.workbench.QiComponentBase;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ComponentStateUtils;
import com.bhpb.qiworkbench.compAPI.ControlConstants;
import com.bhpb.qiworkbench.compAPI.FileChooserDescUtils;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.compAPI.XmlUtils;

import com.bhpBilliton.viewer2d.dataAdapter.SegyChooser;
import com.bhpBilliton.viewer2d.dataAdapter.bhpsu.SeismicChooser;
import com.bhpBilliton.viewer2d.ui.CreateHorizonsDialog;
import com.bhpBilliton.viewer2d.util.ErrorDialog;

/**
 * BHP seismic viewer agent. The viewer is started as a thread by the
 * Messenger Dispatcher when it executes the "Activate viewer agent"
 * command.
 *
 * @author Gil Hansen
 * @version 1.3
 */
public class Bhp2DviewerAgent extends QiComponentBase implements IqiWorkbenchComponent, Runnable {
    private static Logger logger = Logger.getLogger(Bhp2DviewerAgent.class.getName());
	
    // messages used for file chooser service
    public static final String SELECT_SEISMIC_CUBE = "Select Seismic_Cube";

    // Messaging Manager for this adaptor
    private MessagingManager messagingMgr;
    // flag to tell run method to exit
    private boolean stopThread = false;
    private ComponentDescriptor fileChooser = null;
    // Viewer's GUI.
    private BhpViewerBase viewerGUI;
    private ArrayList paramListForFileChooser;

    /** Component descriptor of associated qiProject Manager. null means no association with a PM has been made. */
    private ComponentDescriptor projMgrDesc = null;

    /** Metadata of project associated with */
    private QiProjectDescriptor qiProjDesc = new QiProjectDescriptor();

    public QiProjectDescriptor getQiProjectDescriptor() {
        return qiProjDesc;
    }

    /** Unique, system wide ID (CID) */
    private String myCID = "";
    private static int saveAsCount = 0;

    public String getCID() {
        return myCID;
    }

    public IComponentDescriptor getComponentDescriptor() {
        return messagingMgr.getMyComponentDesc();
    }

    public IMessagingManager getMessagingManager() {
      return messagingMgr;
    }

    private static int INTERACTIVE_MODE = 0;
    private static int CONTROL_MODE = 1;
    /** Execution mode */
    private int execMode = INTERACTIVE_MODE;
    public boolean isInteractiveMode() {
        return (execMode == INTERACTIVE_MODE) ? true : false;
    }
    public boolean isControlMode() {
        return (execMode == CONTROL_MODE) ? true : false;
    }
    ControlSessionManager controlSessionMgr = null;
    String controlSessionID = "";

    /** File chooser requester who registered themselves. Keyed on ID of message containing the user's selection. */
    private HashMap<String, FileChooserDescriptor> fileChooserRegistry = new HashMap<String, FileChooserDescriptor>();

    /**
     * Initialize the viewer adaptor component:
     * <ul>
     * <li>Create its messaging manager</li>
     * <li>Register it with the Message Dispatcher</li>
     * </ul>
     */
    public void init() {
        try {
            QIWConstants.SYNC_LOCK.lock();
            
            messagingMgr = new MessagingManager();
            
            myCID = Thread.currentThread().getName();
            
            // register self with the Message Dispatcher
            messagingMgr.registerComponent(QIWConstants.VIEWER_AGENT_COMP, QIWConstants.BHP_VIEWER_NAME, myCID);
            
            // notify the startup method initialization finished
            QIWConstants.SYNC_LOCK.unlock();
            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in Bhp2DviewerAgent.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

    /** Initialize the 2D viewer, then start processing messages it receives.
     */
    public void run() {
        // initialize the 2D viewer
        init();

        // process any messages received from other components
        String myPid = QiProjectDescUtils.getPid(qiProjDesc);
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_ASSOC_PROJMGR_CMD, QIWConstants.STRING_TYPE, myPid);
                
        while (stopThread == false) {
            //Check if associated with a PM. If not (because was restored), discover the PM
            //with a matching PID. There will be one once its GUI comes up.
            while (projMgrDesc == null && !myPid.equals("")) {
                //NOTE: Request will stay forwever on outstanding request list if no PM with matching PID.
                //      This can occur when restoring the workbench and the PM hasn't been restored
                //      or it is being restored but its GUI is not yet up.
                //Message Dispatcher will broadcast command to all PMs. Only PM with matching PID
                //will send back a response.
                IQiWorkbenchMsg resp = messagingMgr.getMatchingResponse(msgID);
                if (resp != null) {
                    processMsg(resp);
                }
                try {
                    //wait and then try again
                    Thread.sleep(1000);
                    //if a message arrived, process it. It probably is the response from a PM.
                    if (!messagingMgr.isMsgQueueEmpty()) break;
                } catch (InterruptedException ie) {
                    continue;
                }
            }

            IQiWorkbenchMsg msg = messagingMgr.peekNextMsg();
            //if a data message with skip flag set to true, it will not be processed here
            //instead it will be claimed by calling getMatchingResponseWait
            if (msg != null &&  !(msg.getMsgKind().equals(QIWConstants.DATA_MSG) && msg.skip())) {
                msg = messagingMgr.getNextMsgWait();
            //if (msg != null) {
                processMsg(msg);
            }
        }
    }

    /**
     * Find the request matching the response and process the response based on the request.
     */
    public void processMsg(IQiWorkbenchMsg msg) {
      /** Request that matches the response */
      IQiWorkbenchMsg request = null;

      //log message traffic
      logger.fine("Bhp2DviewerAgent::processMsg: msg="+msg.toString());

       // Check if a response. If so, process and consume response
      if (messagingMgr.isResponseMsg(msg)) {
        request = messagingMgr.checkForMatchingRequest(msg);
        // check if from the message dispatcher
        if (messagingMgr.isResponseFromMsgDispatcher(msg)) {
          String cmd = MsgUtils.getMsgCommand(request);
          if (cmd.equals(QIWConstants.NULL_CMD))
            return;
          else if (MsgUtils.getMsgCommand(msg).equals(QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD)) {
            fileChooser = (ComponentDescriptor)MsgUtils.getMsgContent(msg);
            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_FILE_CHOOSER_CMD,fileChooser,QIWConstants.ARRAYLIST_TYPE,paramListForFileChooser);
            messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD,fileChooser);
            return;
          }
        } else

        if (MsgUtils.getMsgCommand(msg).equals(QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD)) {
            ArrayList list = (ArrayList)MsgUtils.getMsgContent(msg);
            final String selectionPath = (String)list.get(1);

            if (((Integer)list.get(0)).intValue() == JFileChooser.APPROVE_OPTION) {
                String fileName = (String)list.get(1);
                String action = (String)list.get(3);

                if (action.equals(QIWConstants.OPEN_SESSION_ACTION_BY_2D_VIEWER)) {
                    String contents = getFileContentsAsAscii(fileName);
                    doRestoreSession(fileName,contents);
                } else

                if(action.equals(QIWConstants.SAVE_MOVIE_VIDEO_ACTION_BY_2D_VIEWER)) {
                    try {
                        viewerGUI.getMovieManager().saveVideoFile(fileName);
                        final String msgStr = "Save QuickTime file " + fileName;
                        Runnable updateAComponent = new Runnable(){
                            public void run(){
                                viewerGUI.getStatusBar().showMessage(BhpViewer.INFO_MESSAGE, msgStr);
                            }
                        };
                        SwingUtilities.invokeLater(updateAComponent);
                    } catch (Exception ex) {
                        final String msgStr = "Failed saving QuickTime file " + fileName;
                        Runnable updateAComponent = new Runnable(){
                            public void run(){
                                viewerGUI.getStatusBar().showMessage(BhpViewer.ERROR_MESSAGE, msgStr);
                            }
                        };
                        SwingUtilities.invokeLater(updateAComponent);
                        logger.warning("Failed to save QuickTime file " + fileName);
                        logger.warning("    " + ex.toString());
                    }
                } else

                if (action.equals(QIWConstants.SAVE_MOVIE_IMAGE_ACTION_BY_2D_VIEWER)) {
                    try {
                        viewerGUI.getMovieManager().saveImageFiles(fileName);
                        final String msgStr = "Save multiple JPEG files " + fileName;
                        Runnable updateAComponent = new Runnable(){
                            public void run(){
                                viewerGUI.getStatusBar().showMessage(BhpViewer.INFO_MESSAGE, msgStr);
                            }
                        };
                        SwingUtilities.invokeLater(updateAComponent);
                    } catch (Exception ex) {
                        final String msgStr = "Failed to save multiple JPEG files " + fileName;
                        Runnable updateAComponent = new Runnable(){
                            public void run(){
                                viewerGUI.getStatusBar().showMessage(BhpViewer.ERROR_MESSAGE, msgStr);
                            }
                        };
                        SwingUtilities.invokeLater(updateAComponent);
                        logger.warning("Failed to save JPEG file " + fileName);
                        logger.warning("    " + ex.toString());
                    }
                } else

                if (action.equals(SELECT_SEISMIC_CUBE)) {
                    //Note: message ID of response same as request's
                    String msgID = MsgUtils.getMsgID(msg);
                    //make sure file chooser request registered
                    if (isFileChooserRequestRegistered(msgID, 3)) {
                        FileChooserDescriptor desc = getFileChooserDesc(msgID);
                        CreateHorizonsDialog chooser = (CreateHorizonsDialog)FileChooserDescUtils.getRequesterInstance(desc);
                        //unregister file chooser request
                        unregisterFileChooserRequest(desc);
                        //process the selected Segy file
                        chooser.setSelectedDataset(fileName);
                    } else {
                        //TODO: retry, but not endlessly
                        logger.severe("INTERNAL ERROR: cannot find registered file chooser request. msgID="+msgID);
                    }
                } else

                if (action.equals(QIWConstants.OPEN_X_SECTION)) {
                    //Note: message ID of response same as request's
                    String msgID = MsgUtils.getMsgID(msg);
                    //make sure file chooser request registered
                    if (isFileChooserRequestRegistered(msgID, 3)) {
                        FileChooserDescriptor desc = getFileChooserDesc(msgID);
                        SeismicChooser chooser = (SeismicChooser)FileChooserDescUtils.getRequesterInstance(desc);
                        //unregister file chooser request
                        unregisterFileChooserRequest(desc);
                        //process the selected Segy file
                        chooser.openSelectedData(fileName);
                    } else {
                        //TODO: retry, but not endlessly
                        logger.severe("INTERNAL ERROR: cannot find registered file chooser request. msgID="+msgID);
                    }
                } else

                if (action.equals(QIWConstants.EXPORT_COMP_STATE)) {
                    //Write XML state to the specified file and location
                    String pmState = genState();
                    String xmlHeader = "<?xml version=\"1.0\" ?>\n";
//TODO: handle remote case - use an IO service
                    try {
                        ComponentStateUtils.writeState(xmlHeader+pmState, selectionPath, messagingMgr);
                    } catch (QiwIOException qioe) {
                        //TODO notify user cannot write state file
                        logger.finest(qioe.getMessage());
                    }
                } else

                if (action.equals(QIWConstants.IMPORT_COMP_STATE)) {
//TODO: handle remote case - use an IO service
                    //check if file exists
                    if (ComponentStateUtils.stateFileExists(selectionPath,messagingMgr)) {
                        try {
                            String pmState = ComponentStateUtils.readState(selectionPath,messagingMgr);
                            Node compNode = ComponentStateUtils.getComponentNode(pmState);
                            if (compNode == null) {
                                JOptionPane.showMessageDialog(viewerGUI, "Can't convert XML state to XML object", "Internal Error", JOptionPane.ERROR_MESSAGE);
                            } else {
                                //check for matching component type
                                String compType = ((Element)compNode).getAttribute("componentType");
                                String expectedCompType = messagingMgr.getRegisteredComponentDisplayNameByDescriptor(messagingMgr.getMyComponentDesc());
                                if (!compType.equals(expectedCompType)) {
                                    JOptionPane.showMessageDialog(viewerGUI, "State file not for this qiComponent type. Select again", "Selection Error", JOptionPane.ERROR_MESSAGE);
                                } else {
                                    //Note: don't ask if want to save state before importing because
                                    //      for future saves, the imported state will be saved.

                                    //import state
                                    importState(compNode);
                                }
                            }
                        } catch (QiwIOException qioe) {
                            JOptionPane.showMessageDialog(viewerGUI, "Can't read state file. Select again.", "Processing Error", JOptionPane.ERROR_MESSAGE);
                            logger.finest(qioe.getMessage());
                        }
                    } else {
                        JOptionPane.showMessageDialog(viewerGUI, "State file doesn't exist or is not a file. Select again.", "Selection Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else
				
                if (action.equals(QIWConstants.OPEN_SEGY_DATA_BY_2D_VIEWER)) {
                    //Note: message ID of response same as request's
                    String msgID = MsgUtils.getMsgID(msg);
                    //make sure file chooser request registered
                    if (isFileChooserRequestRegistered(msgID, 3)) {
                        FileChooserDescriptor desc = getFileChooserDesc(msgID);
                        SegyChooser chooser = (SegyChooser)FileChooserDescUtils.getRequesterInstance(desc);
                        //unregister file chooser request
                        unregisterFileChooserRequest(desc);
                        //process the selected Segy file
                        chooser.openSelectedData(fileName);
                    } else {
                        //TODO: retry, but not endlessly
                        logger.severe("INTERNAL ERROR: cannot find registered file chooser request. msgID="+msgID);
                    }
                }
            } else {
                logger.info("File Chooser canceled by the user");
            }
            return;
        } else

        if (MsgUtils.getMsgCommand(msg).equals(QIWConstants.GET_PROJ_INFO_CMD)) {
            ArrayList projInfo = (ArrayList)msg.getContent();
            qiProjDesc = (QiProjectDescriptor)projInfo.get(1);

            //reset window title (if GUI up)
            if (viewerGUI != null) {
                String projName = QiProjectDescUtils.getQiProjectName(qiProjDesc);
                viewerGUI.resetTitle(projName);
            }
        } else

        if (MsgUtils.getMsgCommand(msg).equals(QIWConstants.GET_ASSOC_PROJMGR_CMD)) {
            ArrayList projInfo = (ArrayList)msg.getContent();
            projMgrDesc = (ComponentDescriptor)projInfo.get(0);
            qiProjDesc = (QiProjectDescriptor)projInfo.get(1);
            String projName = QiProjectDescUtils.getQiProjectName(qiProjDesc);
            if (viewerGUI != null) {
                viewerGUI.resetTitle(projName);
            }
        } else

        logger.warning("Bhp2DviewerAgent: Response message not processed: " + msg.toString());
        return;
      } else

      //Check if a request. If so, process and send back a response
      if (messagingMgr.isRequestMsg(msg)) {
        String cmd = MsgUtils.getMsgCommand(msg);
        if (Bhp2DviewerConstants.DEBUG_PRINT > 0)
            System.out.println("ViewerAgent received request: "+msg);
        if (cmd.equals(QIWConstants.OPEN_COMPONENT_GUI_CMD)) {
            if (viewerGUI != null )
                viewerGUI.setVisible(true);
        } else
        if (cmd.equals(QIWConstants.CLOSE_COMPONENT_GUI_CMD)) {
          //close viewer GUI
          if (Bhp2DviewerConstants.DEBUG_PRINT > 0)
              System.out.println("Bhp2DviewerAgent closing GUI");
          if (viewerGUI != null && viewerGUI.isVisible()){
        	  messagingMgr.sendResponse(msg,Component.class.getName(),(Component)viewerGUI);
              viewerGUI.closeViewerGUI();
          }
        } else

        if (cmd.equals(QIWConstants.DEACTIVATE_VIEWER_AGENT_CMD) || cmd.equals(QIWConstants.REMOVE_COMPONENT_CMD) || cmd.equals(QIWConstants.DEACTIVATE_COMPONENT_CMD) ) {
            // unregister viewer and viewer GUI
            if (Bhp2DviewerConstants.DEBUG_PRINT > 0) {
                System.out.println("Bhp2DviewerAgent: deactivate self");
                System.out.println("Bhp2DviewerAgent: Unregister self, desc=: " + messagingMgr.getComponentDesc(myCID));
            }
            messagingMgr.unregisterComponent(messagingMgr.getComponentDesc(myCID));
            if (Bhp2DviewerConstants.DEBUG_PRINT > 0)
                System.out.println("Bhp2DviewerAgent: closing GUI");
            if (viewerGUI != null && viewerGUI.isVisible())
                viewerGUI.closeViewerGUI();
            // tell plugin thread to quit
            stopThread = true;
            messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,myCID + "is successfully deactivated");
        } else

        if (cmd.equals(QIWConstants.SET_PREFERRED_DISPLAY_NAME_CMD)) {
            String preferredDisplayName = (String)MsgUtils.getMsgContent(msg);
            if(preferredDisplayName != null && preferredDisplayName.trim().length() > 0) {
                CompDescUtils.setDescPreferredDisplayName(messagingMgr.getMyComponentDesc(), preferredDisplayName);
                viewerGUI.renameViewer(preferredDisplayName);
            }
            messagingMgr.sendResponse(msg,QIWConstants.COMP_DESC_TYPE,messagingMgr.getMyComponentDesc());
        }
        else if (cmd.equals(QIWConstants.INVOKE_SELF_CMD)) {
          // Create the viewer's GUI, but don't make it visible. That is
          // up to the Workbench Manager who requested the viewer be
          // activated. Pass GUI the CID of its parent.
          // Check to see if content incudes XML information which means invoke
          // self will do the restoration
          ArrayList msgList = (ArrayList)msg.getContent();
          String invokeType = (String)msgList.get(0);
          if (invokeType.equals(QIWConstants.COMPONENT_INVOKE_RESTORED)) {
              Node node = ((Node)((ArrayList)MsgUtils.getMsgContent(msg)).get(2));
              //Note: restoreState() will restore the project descriptor needed by buildGUI
              restoreState(node);
          } else if (invokeType.equals(QIWConstants.COMPONENT_INVOKE_NEW)) {
//              qiProjDesc = ((QiProjectDescriptor)((ArrayList)msg.getContent()).get(2));
              viewerGUI = new BhpViewerBase(msg.getProducerCID(), this);
              java.awt.Point p = (java.awt.Point)msgList.get(1);
              if(p != null)
            	  viewerGUI.setLocation(p);
          }
          //Send a normal response back with the plugin's JInternalFrame and
          //let the Workbench Manager add it to the desktop and make
          //it visible.
          messagingMgr.sendResponse(msg, BhpViewerBase.class.getName(), viewerGUI);
/*
          if(viewerGUI != null) {
            synchronized(WorkbenchStateManager.getInstance()) {
                WorkbenchStateManager.getInstance().setWait(false);
                logger.info("notifying the WorkbenchStateManager not to wait anymore");
              WorkbenchStateManager.getInstance().notify();
            }
          }
*/
        } else

        if (cmd.equals(QIWConstants.SAVE_COMP_CMD)) {
          messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,genState());
        } else

        // save component state as clone
        if (cmd.equals(QIWConstants.SAVE_COMP_AS_CLONE_CMD)){
            messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,genStateAsClone());

        } else

        // restore state
        if (cmd.equals(QIWConstants.RESTORE_COMP_CMD)) {
          Node node = (Node)MsgUtils.getMsgContent(msg);
          restoreState(node);
          messagingMgr.sendResponse(msg,BhpViewerBase.class.getName(),viewerGUI);
        } else

        if (cmd.equals(QIWConstants.GET_VIEWER_GUI_CMD)) {
            messagingMgr.sendResponse(msg,BhpViewerBase.class.getName(),viewerGUI);
        } else

        if (cmd.equals(QIWConstants.RENAME_COMPONENT)) {
          ArrayList<String> names = new ArrayList<String>(2);
          names = (ArrayList)MsgUtils.getMsgContent(msg);
          if(Bhp2DviewerConstants.DEBUG_PRINT > 0)
              System.out.println("Rename myself from " + names.get(0) + " to " + names.get(1));
          viewerGUI.renameViewer(names.get(1));
        }  else

        if (cmd.equals(QIWConstants.NOTIFY_ASSOC_PROJMGR_CMD)) {
          //Just a notification. No response required or expected.
          projMgrDesc = (ComponentDescriptor)msg.getContent();
          //Note: Cannot get info about PM's project because GUI may not be up yet.
          if (projMgrDesc != null)
              messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_PROJ_INFO_CMD, projMgrDesc);
        } else

        if (cmd.equals(QIWConstants.NOTIFY_PROJ_INFO_CHANGED_CMD)) {
          ArrayList info = (ArrayList)msg.getContent();
          ComponentDescriptor pmDesc = (ComponentDescriptor)info.get(0);
          QiProjectDescriptor projDesc = (QiProjectDescriptor)info.get(1);
          //ignore message if not from associated PM
          if (projMgrDesc != null && pmDesc.getCID().equals(projMgrDesc.getCID())) {
              qiProjDesc = projDesc;
//TODO: update PID [get from projDesc?]
              //update window title
              String projName = QiProjectDescUtils.getQiProjectName(qiProjDesc);

              viewerGUI.resetTitle(projName);
          }
        } else

        if (cmd.equals(QIWConstants.CONTROL_COMPONENT_CMD)) {
            ArrayList actions = (ArrayList)msg.getContent();
            String action = (String)actions.get(0);
            ArrayList actionParams = (ArrayList)actions.get(1);

            if (action.indexOf(ControlConstants.START_CONTROL_SESSION_ACTION) != -1) {
                execMode = CONTROL_MODE;
                controlSessionMgr = new ControlSessionManager(viewerGUI, messagingMgr);
                //return the session ID and the bhpViewer component descriptor
                //NOTE: The bhpViewer was launched by a qiComponent and the request contains
                //      its component descriptor in the action parameters.
                this.controlSessionID = controlSessionMgr.genControlSessionID();
                actionParams.add(0, this.controlSessionID);
                messagingMgr.sendResponse(msg, QIWConstants.ARRAYLIST_TYPE, actionParams);
            } else if (action.indexOf(ControlConstants.END_CONTROL_SESSION_ACTION) != -1) {
                String sessionID = (String)actionParams.get(0);
                if (sessionID.equals(this.controlSessionID)) {
                    execMode = INTERACTIVE_MODE;
                    controlSessionMgr = null;
                }
            } else {
                controlSessionMgr.processReq(msg);
            }
        }

        else
          logger.warning("Bhp2DviewerAgent: Request message not processed: " + msg.toString());
        return;
      }
    }

    /**
     * Generate state information into xml string format
     * @return  String
     */
    public String genState() {
        StringBuffer content = new StringBuffer();
        //TODO WorkbenchStateManager will need to be changed to conform to the package change of AmpExtPlugin in ServletDispatcher
        IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
        content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc)  + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + CompDescUtils.getDescPreferredDisplayName(desc) + "\">\n");
        content.append(viewerGUI.saveState());
        content.append("</component>\n");
        return content.toString();
    }

    /**
     * Have the State Manager save the state of this qiComponent.
     */
    public void saveState() {
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_CMD,stMgrDesc,
                QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
    }

    /**
     * Have the State Manager save the state of this qiComponent.
     */
    public void saveStateAsClone() {
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_AS_CLONE_CMD,stMgrDesc,
                QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
    }
    
    /**
     * Have the State Manager save the state of this qiComponent and then quit the component.
     */
    public void saveStateThenQuit() {
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_THEN_QUIT_CMD,stMgrDesc,
                QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
    }   
    
    /**
     * Generate state information into xml string format in repsonse to save as command
     * @return  String
     */
    public String genStateAsClone() {
        StringBuffer content = new StringBuffer();
        //TODO WorkbenchStateManager will need to be changed to conform to the package change of AmpExtPlugin in ServletDispatcher
        IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
        String displayName = "";
        saveAsCount++;
        if(saveAsCount == 1)
            displayName = "CopyOf" + CompDescUtils.getDescPreferredDisplayName(desc);
        else
            displayName = "Copy(" + saveAsCount + ")Of" + CompDescUtils.getDescPreferredDisplayName(desc);
        content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc)  + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + displayName + "\">\n");
        content.append(viewerGUI.saveState());
        content.append("</component>\n");
        return content.toString();
    }

    /** Restore plugin and it's GUI to previous condition.
     *
     * @param node xml containing saved state variables
     */
    void restoreState(Node node) {
        String preferredDisplayName = ((Element)node).getAttribute("preferredDisplayName");
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals(BhpViewerBase.class.getName())) {
                    //get project descriptor from state since buildGUI needs it
                    qiProjDesc = getProjectDesc(child);
                    viewerGUI = new BhpViewerBase(this);
                    viewerGUI.restoreState(child);
                    if (preferredDisplayName != null && preferredDisplayName.trim().length() > 0) {
                        CompDescUtils.setDescPreferredDisplayName(messagingMgr.getMyComponentDesc(), preferredDisplayName);
                        viewerGUI.renameViewer(preferredDisplayName);
                    }
                    break;
                }
            }
        }
    }

    /**
     * Import state saved in a XML file. Use existing GUI. User not asked to save current
     * state first, because after import, saving state will save imported state. The preferred
     * display name is not changed. Therefore, the name in the GUI title and component tree node
     * don't have to change.
     *
     * @param node XML containing saved state variables
     */
    void importState(Node node) {
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals(BhpViewerBase.class.getName())) {
                    //keep the associated project
                    viewerGUI.restoreState(child);

                    //Not necessary to reset the title of the window, for the project didn't change
                }
            }
        }
    }

    /**
     * Extract the project descriptor from the saved state. If the state
     * does not contain a project descriptor, form one from the implicit
     * project so backward compatible.
     *
     * @return Project descriptor for project associated with the bhpViewer.
     */
    private QiProjectDescriptor getProjectDesc(Node node) {
        QiProjectDescriptor projDesc = null;
/*
        if (!node.getNodeName().equals(BhpViewerBase.class.getName())) {
            ErrorDialog.showInternalErrorDialog(Thread.currentThread().getStackTrace(), "restoreState error: State not bhpViewer's but of " + node.getNodeName());
            return;
        }
*/
        NodeList children = node.getChildNodes();
        String nodeName;
        Node child;
        for (int i = 0; i < children.getLength(); i++) {
          child = children.item(i);
          if (child.getNodeType() == Node.ELEMENT_NODE) {
            nodeName = child.getNodeName();
            if (nodeName.equals("QiProjectDescriptor")) {
                projDesc = (QiProjectDescriptor)XmlUtils.XmlToObject(child);
                break;
            }
          }
        }

        //Check if saved state has a project descriptor. If not, create
        //one using the implicit project so backward compatible.
        if (projDesc == null) {
            projDesc = new QiProjectDescriptor();
            //get path of implicit project from Message Dispatcher
            String projPath = getMessagingManager().getProject();
            QiProjectDescUtils.setQiSpace(projDesc, projPath);
            //leave relative location of project as "/"
            int idx = projPath.lastIndexOf("\\");
            if (idx == -1) idx = projPath.lastIndexOf("/");
            String projectName = projPath.substring(idx+1);
            QiProjectDescUtils.setQiProjectName(projDesc, projectName);
        }

        return projDesc;
    }

    private boolean wait = false;
    public void setWait(boolean wait) {
        this.wait = wait;
    }

    /*
     * openSessionFile replaces openRemote and openLocal
     * @param fileName is /path/file from file chooser
    */
    private String getFileContentsAsAscii(String fileName) {
      String msgID = "";
      ArrayList<String> params = new ArrayList<String>();
      params.add(messagingMgr.getLocationPref());
      params.add(fileName);
      msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.FILE_READ_CMD,
                                                    QIWConstants.ARRAYLIST_TYPE,params);

      IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,  Bhp2DviewerConstants.MINUTE_WAIT_TIME);
      if (response == null || response.isAbnormalStatus()) {
          ErrorDialog.showErrorDialog(viewerGUI, QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                                      "IO error: " + (String)response.getContent(),
                                      new String[] {"Permissions not set for reading"},
                                      new String[] {"Fix permissions",
                                                    "If already OK, contact workbench support"});
          return "";
      }
      ArrayList<String> xml = (ArrayList<String>)MsgUtils.getMsgContent(response);

      if ( Bhp2DviewerConstants.DEBUG_PRINT > 0) {
          for (int i=0; i<xml.size(); i++)
              System.out.println(xml.get(i));
      }

      StringBuffer xmlContent = new StringBuffer();
      for (int i=0; i<xml.size(); i++)
          xmlContent.append(xml.get(i));

      return xmlContent.toString();
    }

    private boolean doRestoreSession(String fileName, String contents) {
        // empty contents means io error reading session file
        if (contents.equals(""))
            return false;
        InputStream ins = new ByteArrayInputStream(contents.getBytes());
        try {
            final Element root = BhpViewerHelper.getXMLTree(ins);
            if (!root.getNodeName().equals("BhpViewer")){
                JOptionPane.showMessageDialog(viewerGUI,"The selected file " + fileName +
                            " does not have a compatible format required by the viewer.",
                            "Incompatible File Format",JOptionPane.WARNING_MESSAGE);
                    return false;
            }
            root.setAttribute("title",fileName);
            Runnable updateAComponent = new Runnable() {
                public void run() {
                    viewerGUI.initWithXML(root);
                }
            };
            //was coded for backward compatibility, requirement changed
            //if(root.getNodeName().equals("desktop")) {
            //    ComponentDescriptor stMgrDesc = MessageDispatcher.getInstance().getComponentDescByName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
            //    messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.RESTORE_DESKTOP_CMD,stMgrDesc,Node.class.getName(),root);
            //}else
            SwingUtilities.invokeLater(updateAComponent);
        } catch(Exception e) {
            ErrorDialog.showErrorDialog(viewerGUI, QIWConstants.ERROR_DIALOG,Thread.currentThread().getStackTrace(),
                                      "Error: loading session: " + fileName,
                                      new String[] {"Permissions not set for reading"},
                                      new String[] {"Fix permissions",
                                                    "If already OK, contact workbench support"});
            return false;
        }
        return true;
    }

    /*
     * Get a file chooser service, invoke a file chooser, and get the user's selection
     * @param list File chooser input parameters
    */
    public void doFileChooser(ArrayList list) {
        paramListForFileChooser = list;
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD, true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
        if (response == null) {
            logger.warning("Internal error occurring in getting file chooser service returning null");
            ErrorDialog.showInternalErrorDialog(viewerGUI, Thread.currentThread().getStackTrace(), "Failed to start file chooser service");
            return;
        }

        if (response.isAbnormalStatus()) {
            logger.warning("Internal error occurring in getting file chooser service. Cause: " + response.getContent());
            ErrorDialog.showInternalErrorDialog(viewerGUI, Thread.currentThread().getStackTrace(), "Failed to start file chooser service");
            return;
        } else {
            ComponentDescriptor cd = (ComponentDescriptor)response.getContent();

            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_FILE_CHOOSER_CMD, cd, QIWConstants.ARRAYLIST_TYPE,list);

            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD, cd);

            return;
        }
    }

    /*
     * Get a file chooser service, invoke a file chooser, request the user's
     * selection and register the request so the selector action is processed
     * by the requester.
     * @param list File chooser input parameters
     * @param requesterType Data type of the requester
     * @param requesterInstance Instance of the requester
    */
    public void doFileChooser(ArrayList list, String requesterType, Object requesterInstance) {
        paramListForFileChooser = list;
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD, true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, Bhp2DviewerConstants.MINUTE_WAIT_TIME);
        if (response == null) {
            logger.warning("Internal error occurring in getting file chooser service returning null");
            ErrorDialog.showInternalErrorDialog(viewerGUI, Thread.currentThread().getStackTrace(), "Failed to start file chooser service");
            return;
        }

        if (response.isAbnormalStatus()) {
            logger.warning("Internal error occurring in getting file chooser service. Cause: " + response.getContent());
            ErrorDialog.showInternalErrorDialog(viewerGUI, Thread.currentThread().getStackTrace(), "Failed to start file chooser service");
            return;
        } else {
            ComponentDescriptor cd = (ComponentDescriptor)response.getContent();

            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_FILE_CHOOSER_CMD, cd, QIWConstants.ARRAYLIST_TYPE,list);

            msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD, cd);

            FileChooserDescriptor desc = new FileChooserDescriptor(msgID, requesterType, requesterInstance);
            registerFileChooserRequest(desc);

            return;
        }
    }

    /** when the user click the Exit button on the Viewer calling to deactivate self.
     */
    public void deactivateSelf() {
        //ask WorkbenchManager to remove it from workbench GUI then send it back to self to deactivate self
        IComponentDescriptor wbMgr = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_MGR_NAME);
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.QUIT_COMPONENT_CMD,wbMgr,QIWConstants.STRING_TYPE,
                                 messagingMgr.getMyComponentDesc());
    }

    /**
     * Register the request for a file chooser's result so the agent can invoke the file
     * chooser action. Usually performed when the request is sent.
     * <p>
     * The file chooser's descriptor is kept in a hashmap keyed on the ID of the response.
     *
     * @param desc File chooser descriptor for the requester.
     * @return Empty string if successfully registerd or null if the file chooser request is already registered.
     */
    synchronized public String registerFileChooserRequest(FileChooserDescriptor desc) {
        // check if response is already registered
        if (fileChooserRegistry.containsKey(desc.getMsgID())) return null;

        fileChooserRegistry.put(desc.getMsgID(), desc);

        logger.config("registered:"+desc);

        return "";
    }

    /**
     * Unregister a registered file chooser request. Usually performed when the file chooser action is invoked.
     *
     * @param desc Descriptor of the registered file chooser request.
     */
    synchronized public void unregisterFileChooserRequest(FileChooserDescriptor desc) {
        logger.config("unregistering:"+desc);
        fileChooserRegistry.remove(desc.getMsgID());
    }

    /**
     * Get the descriptor for a registered file chooser request based on its message ID.
     *
     * @param msgID ID of the file chooser response (same as the request)
     * @return File chooser descriptor if request registered; otherwise, null.
     */
     public FileChooserDescriptor getFileChooserDesc(String msgID) {
         if (!fileChooserRegistry.containsKey(msgID)) return null;

         return fileChooserRegistry.get(msgID);
     }

    /**
     * Check if file chooser request registered. Need to make this check since
     * registration occurs after the request is sent to the file chooser.
     * Note: Sending the request returns the message ID needed for registration.
     *
     * @param msgID ID of response (same as request)
     * @param tries The number of times to try.
     * @return true if registered; otherwise, false
     */
    private boolean isFileChooserRequestRegistered(String msgID, int tries) {
        while (tries > 0) {
            if (fileChooserRegistry.containsKey(msgID)) return true;
            try {
                Thread.sleep(100);
            } catch (InterruptedException ie) {
            }
            tries--;
        }

        return false;
    }

    /** Launch the 2D viewer:
     *  <ul>
     *  <li>Start up the viewer's thread which will initialize the viewer.</li>
     *  </ul>
     * <p>
     * NOTE: Each thread's init() must finish before another thread is
     * started. This is accomplished by monitoring the SYNC_LOCK object.
     */
    public static void main(String[] args) {
        Bhp2DviewerAgent bhp2DviewerInstance = new Bhp2DviewerAgent();
        //CID has to be passed in since no way to get it back out
        String cid = args[0];
        // use the CID as the name of the thread
        Thread viewerThread = new Thread(bhp2DviewerInstance, cid);
        viewerThread.start();
        long threadId = viewerThread.getId();
        logger.info("bhpViewer Thread-"+Long.toString(threadId)+" started");
        // When the viewer's init() is finished, it will release the lock
        // wait until the viewers's init() has finished
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();
        logger.info("bhpViewer main finished");
    }
}
